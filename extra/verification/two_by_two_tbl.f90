PROGRAM two_by_two_tbl

  USE wrf_io
  USE netcdf_io

  IMPLICIT NONE

  CHARACTER(LEN=256) :: truth_file, bkgrd_file
  CHARACTER(LEN=256) :: frcst_file
  CHARACTER(LEN=256) :: output_file
  INTEGER            :: start_minute,forecast_min

  REAL               :: thresh
  INTEGER            :: neighbor_pts
  LOGICAL            :: read_ref,debug
  INTEGER            :: truth_stride

  INTEGER :: nx, ny, nz, ii, jj, kk
  INTEGER :: ista,iend,jsta,jend,ksta,kend
  REAL, ALLOCATABLE, DIMENSION(:,:,:)  :: trho,tpt,tpres,ttk,tqr,tqs,tqg,trf
  REAL, ALLOCATABLE, DIMENSION(:,:,:)  :: brho,bpt,bpres,btk,bqr,bqs,bqg,brf
  INTEGER  :: nhit,nmiss,nfalse,nnull,npts
  REAL     :: maxrf,minrf

  LOGICAL  :: read_bkg, fexist

  INTEGER  :: namelist_unit=11, output_veri_unit=21


  NAMELIST /misc/ truth_file,bkgrd_file,frcst_file,truth_stride,neighbor_pts,   &
                  output_file,start_minute,forecast_min,thresh,read_ref,debug

!-----------------------------------------------------------------------

  INTEGER :: i,j,k
  INTEGER :: istatus

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Main Program
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! Step 1. Read namelist
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  read_ref=.false.
  debug=.false.
  truth_file='NULL'
  bkgrd_file='NULL'
  frcst_file='NULL'
  output_file='NULL'
  start_minute=0;forecast_min=0
  nx=0;ny=0;nz=0
  nhit=0;nmiss=0;nfalse=0;nnull=0
  thresh=0
  neighbor_pts=0

  read_bkg = .FALSE.
  truth_stride = 1

  open (unit = namelist_unit, file = "gen_table.input", &
        status = 'old', form = 'formatted', iostat = istatus)
  if ( istatus /=0 ) then
     write (unit=6, fmt=*) 'Error to open namelist file: gen_table.input. Stopped.'
     stop
  else
     read (unit=namelist_unit, nml = misc, iostat = istatus)

     if ( istatus /= 0 ) then
        write (unit=6, fmt=*) 'Error to read namelist misc. Stopped.'
        stop
     else if ( thresh == 0 ) then
        write (unit=6, fmt=*) 'Error to read threshold for verification. Stopped.'
        stop
     end if

     write (unit=6, fmt='(1x,2a)')   'Truth file is: ', trim(truth_file)
     INQUIRE(FILE=TRIM(bkgrd_file),EXIST=fexist)
     IF (fexist ) THEN
       read_bkg = .TRUE.
       write (unit=6, fmt='(1x,2a)') 'Background is: ', trim(bkgrd_file)
     END IF
     write (unit=6, fmt='(1x,2a)')   'Forecast   is: ', trim(frcst_file)
     IF (neighbor_pts>0) THEN
       write (unit=6, fmt='(1x,a,i2)') 'Number of points used for neighbourhood verification is ', neighbor_pts
       npts=neighbor_pts
     ELSE
       write (unit=6, fmt='(1x,a)') 'Number of points used for neighbourhood verification is Valid.'
       write (unit=6, fmt='(1x,a)') 'Set to default value (15 points).'
       npts=15
     END IF
     IF (read_ref) THEN
       write (unit=6, fmt='(1x,a)') 'Reflectivity is read from forecast output (require do_radar_ref=1 in wrf) / REFMOSAIC3D.'
     ELSE
       write (unit=6, fmt='(1x,a)') 'Reflectivity is calculated based on state variables.'
     END IF
  end if

  close (unit = namelist_unit)

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! Step 2. Read/Calculate Reflectivity
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! Step a. read wrf dimension first
  CALL get_dims_wrf(frcst_file,'T',dims,ndims,debug)
  nx=dims(1);ny=dims(2);nz=dims(3)
  write (unit=6, fmt='(1x,3(a,I3))') 'Model dimension is: ',nx,', ', ny,', ',nz

  ! Step b. allocate arrays for state variables
  ALLOCATE(trf(nx,ny,nz))
  ALLOCATE(brf(nx,ny,nz))
  IF ( .not. read_ref ) THEN
    ALLOCATE(trho(nx,ny,nz))
    ALLOCATE(tpt(nx,ny,nz))
    ALLOCATE(tpres(nx,ny,nz))
    ALLOCATE(ttk(nx,ny,nz))
    ALLOCATE(tqr(nx,ny,nz))
    ALLOCATE(tqs(nx,ny,nz))
    ALLOCATE(tqg(nx,ny,nz))
    ALLOCATE(brho(nx,ny,nz))
    ALLOCATE(bpt(nx,ny,nz))
    ALLOCATE(bpres(nx,ny,nz))
    ALLOCATE(btk(nx,ny,nz))
    ALLOCATE(bqr(nx,ny,nz))
    ALLOCATE(bqs(nx,ny,nz))
    ALLOCATE(bqg(nx,ny,nz))
  END IF

  IF ( .not. read_ref ) THEN

    ! Step c. obtain state variables from truth and background
    CALL readvar4tbl(truth_file,bkgrd_file,read_bkg,truth_stride,         &
                     nx,ny,nz,trho,tpt,tpres,tqr,tqs,tqg,debug)
    CALL readvar4tbl(frcst_file,bkgrd_file,.FALSE.,1,                     &
                     nx,ny,nz,brho,bpt,bpres,bqr,bqs,bqg,debug)

    ! Step d. Convert potential temperature to temperature in C degree
    CALL temperwrf(nx,ny,nz,tpt,tpres,ttk)
    CALL temperwrf(nx,ny,nz,bpt,bpres,btk)

    ! Step e. Calculate reflectivities
    CALL reflec_ferrier2(nx,ny,nz,trho,tqr,tqs,tqg,ttk,trf)
    CALL reflec_ferrier2(nx,ny,nz,brho,bqr,bqs,bqg,btk,brf)

    ! Step f. deallocate unnecessary variables
    DEALLOCATE(trho,tqr,tqs,tqg,ttk)
    DEALLOCATE(brho,bqr,bqs,bqg,btk)

  ELSE
    ! Step c. directly obtain reflectivity from wrf output and REFMOSAIC
    CALL get_real_var(truth_file,'REFMOSAIC3D',trf,dims,debug)
    CALL get_real_var(frcst_file,'REFL_10CM',brf,dims,debug)
  END IF

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! Step 4. Statistics for hit and miss
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        maxrf=0.0;minrf=100.0
        jsta=j-npts;jend=j+npts;ista=i-npts;iend=i+npts
        !IF (ksta<1) ksta=1
        !IF (kend>nz) kend=nz
        IF (jsta<1) jsta=1
        IF (jend>ny) jend=ny
        IF (ista<1) ista=1
        IF (iend>nx) iend=nx
        maxrf=MAXVAL(trf(ista:iend,jsta:jend,k))
        !DO kk=ksta,kend
        DO jj=jsta,jend
          DO ii=ista,iend
            IF (trf(ii,jj,k)>-100) THEN
              IF (trf(ii,jj,k)<minrf) minrf=trf(ii,jj,k)
            END IF
          END DO
        END DO
        !END DO
        IF (ANY(trf(ista:iend,jsta:jend,k)>=0)) THEN
          IF ( brf(i,j,k)>=thresh .AND. maxrf>=thresh ) THEN
            nhit=nhit+1
          ELSE IF ( brf(i,j,k)>=thresh .AND. maxrf<thresh ) THEN
            nfalse=nfalse+1
          ELSE IF ( brf(i,j,k)<thresh .AND. minrf>=thresh ) THEN
            nmiss=nmiss+1
          ELSE IF ( brf(i,j,k)<thresh .AND. minrf<thresh ) THEN
            nnull=nnull+1
          END IF
        END IF
      END DO
    END DO
  END DO

  DEALLOCATE(trf,brf)

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! Step 5. Write standard output
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  OPEN( output_veri_unit, file=output_file, position='APPEND' )
  WRITE(output_veri_unit,'(2I5,5I12)') start_minute,forecast_min,nhit,nfalse,nmiss,nnull
  CLOSE( output_veri_unit )

END PROGRAM two_by_two_tbl
