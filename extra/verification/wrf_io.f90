MODULE wrf_io

  USE netcdf_io

  IMPLICIT NONE

  !-------------------------------------------------------------------------------
  !
  ! Variable Delaration for Whole Module
  !
  !-------------------------------------------------------------------------------

  integer, parameter     :: max_3d_variables = 50
  CHARACTER(LEN=20)      :: var3d(max_3d_variables)

  INTEGER                :: ndims, nvars
  INTEGER, PARAMETER     :: num3d = 11
  INTEGER                :: time_level
  !INTEGER                :: i, j, k, l, m, n

  INTEGER, DIMENSION(4)  :: dims
  INTEGER, DIMENSION(3)  :: dims2d
  INTEGER, DIMENSION(2)  :: dims1d
  INTEGER                :: dimss


  CONTAINS

  SUBROUTINE readvar4tbl(filename,bkfilename,read_bkg,truth_stride,     &
                         nx,ny,nz,rho,pt,pres,qr,qs,qg,debug)

    CHARACTER(LEN=256)  :: filename
    CHARACTER(LEN=256)  :: bkfilename
    LOGICAL, INTENT(IN) :: read_bkg
    INTEGER, INTENT(IN) :: truth_stride

    INTEGER, INTENT(IN) :: nx,ny,nz
    REAL, DIMENSION(nx,ny,nz)  :: rho,pt,pres,qr,qs,qg
    LOGICAL, INTENT(IN) :: debug

    INTEGER :: istatus

  !---------------------------------------------------------------------

    INTEGER :: nxin, nyin, nzin
    REAL, ALLOCATABLE, DIMENSION(:,:,:) :: rhoin,ptin,presin,qrin,qsin,qgin
    REAL, ALLOCATABLE, DIMENSION(:,:,:) :: qvin,phin,pbin
    REAL, ALLOCATABLE, DIMENSION(:,:)   :: mub,mu
    REAL, ALLOCATABLE, DIMENSION(:)     :: rdnw,znu
    REAL                :: p_top

    INTEGER :: i,j,k, istride, jstride
    INTEGER, ALLOCATABLE :: ii(:),jj(:)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    !ilen = 0
    !idx = INDEX(filename,'news3dvar',.TRUE.)
    !IF (idx > 0) THEN
    !  ilen = LEN_TRIM('news3dvar')
    !  WRITE(bkfilename,'(3a)') filename(1:idx-1),'wrf4',TRIM(filename(idx+ilen:))
    !ELSE
    !  bkfilename = filename
    !END IF

    !
    ! static fields
    !
    IF (read_bkg) THEN
      WRITE(*,'(1x,3a)') 'Reading static from file <',TRIM(bkfilename),'>'
    ELSE
      WRITE(*,'(1x,3a)') 'Reading from file <',TRIM(bkfilename),'>'
    END IF
    CALL get_dims_wrf_scalar(bkfilename, 'P_TOP', dimss, ndims, debug)
    CALL get_real_var_scalar(bkfilename, 'P_TOP', p_top, dimss, debug)

    CALL get_dims_wrf_2d(bkfilename, 'MUB', dims2d, ndims, debug)
    ALLOCATE(mub(dims2d(1),dims2d(2)))
    ALLOCATE(mu(dims2d(1),dims2d(2)))
    CALL get_real_var_2d(bkfilename, 'MUB', mub, dims2d, debug)
    CALL get_real_var_2d(bkfilename, 'MU', mu, dims2d, debug)

    CALL get_dims_wrf_1d(bkfilename, 'RDNW', dims1d, ndims, debug)
    ALLOCATE(rdnw(dims1d(1)))
    CALL get_real_var_1d(bkfilename, 'RDNW', rdnw, dims1d, debug)

    CALL get_dims_wrf_1d(bkfilename, 'ZNU', dims1d, ndims, debug)
    ALLOCATE(znu(dims1d(1)))
    CALL get_real_var_1d(bkfilename, 'ZNU', znu, dims1d, debug)

    !
    ! time dependent fields
    !
    IF (read_bkg) THEN
      WRITE(*,'(1x,3a)') 'Reading time dependent variables from file <',TRIM(filename),'>'
    END IF
    CALL get_dims_wrf(filename,'QVAPOR', dims, ndims, debug)
    nxin = dims(1)
    nyin = dims(2)
    nzin = dims(3)
    ALLOCATE(qvin(dims(1),dims(2),dims(3)))
    ALLOCATE(pbin(dims(1),dims(2),dims(3)))
    ALLOCATE(rhoin (nxin,nyin,nzin), STAT = istatus)
    ALLOCATE(ptin  (nxin,nyin,nzin), STAT = istatus)
    ALLOCATE(presin(nxin,nyin,nzin), STAT = istatus)
    ALLOCATE(qrin  (nxin,nyin,nzin), STAT = istatus)
    ALLOCATE(qsin  (nxin,nyin,nzin), STAT = istatus)
    ALLOCATE(qgin  (nxin,nyin,nzin), STAT = istatus)

    CALL get_real_var(filename,'QVAPOR',qvin,  dims,debug)
    CALL get_real_var(filename,'T',     ptin,  dims,debug)
    CALL get_real_var(filename,'P',     presin,dims,debug)
    CALL get_real_var(filename,'PB',    pbin,  dims,debug)
    CALL get_real_var(filename,'QRAIN', qrin,  dims,debug)
    CALL get_real_var(filename,'QSNOW', qsin,  dims,debug)
    CALL get_real_var(filename,'QGRAUP',qgin,  dims,debug)

    CALL get_dims_wrf(filename,'PH', dims, ndims, debug)
    ALLOCATE(phin(dims(1),dims(2),dims(3)))
    CALL get_real_var(filename,'PH',phin,dims,debug)

    CALL cal_rho_p(nxin,nyin,nzin,mub,mu,rdnw,p_top,znu,qvin,phin,ptin,rhoin)

    presin = presin+pbin
    ptin   = ptin+300.0

    !
    ! set output arrays
    !
    istride = truth_stride
    jstride = truth_stride

    ALLOCATE(ii(nx), STAT = istatus)
    ALLOCATE(jj(ny), STAT = istatus)

    DO i = 0, nx-1
      ii(i+1) = i*istride+1
    END DO
    DO j = 0, ny-1
      jj(j+1) = j*jstride+1
    END DO

    DO k = 1, nz
      DO j = 1, ny
        DO i = 1, nx
          !print *, i,j,k, ii(i),jj(j),k
          rho (i,j,k) = rhoin (ii(i),jj(j),k)
          pt  (i,j,k) = ptin  (ii(i),jj(j),k)
          pres(i,j,k) = presin(ii(i),jj(j),k)
          qr  (i,j,k) = qrin  (ii(i),jj(j),k)
          qs  (i,j,k) = qsin  (ii(i),jj(j),k)
          qg  (i,j,k) = qgin  (ii(i),jj(j),k)
        END DO
      END DO
    END DO

    DEALLOCATE(mub,mu,rdnw,znu,qvin,pbin,phin)
    DEALLOCATE(rhoin,ptin,presin,qrin,qsin,qgin)

  END SUBROUTINE readvar4tbl

  SUBROUTINE temperwrf(nx,ny,nz,theta,ptotal,t)

  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz
!
  REAL :: theta(nx,ny,nz)      ! potential temperature (degrees Kelvin)
  REAL :: ptotal(nx,ny,nz)      ! total pressure (Pascals)
  REAL :: ptbar(nx,ny,nz)      ! base state potential temperature (K)
  REAL :: t    (nx,ny,nz)      ! temperature (degrees Kelvin)

  REAL :: p0        ! Surface reference pressure, is 100000 Pascal.
  PARAMETER( p0     = 1.0E5 )
  REAL :: cp        ! Specific heat of dry air at constant pressure
                    ! (m**2/(s**2*K)).
  PARAMETER( cp     = 1004.0 )
  REAL :: rd        ! Gas constant for dry air  (m**2/(s**2*K))
  PARAMETER( rd     = 287.0 )
  REAL :: rddcp
  PARAMETER( rddcp  = rd/cp )

  INTEGER :: i,j,k
!-----------------------------------------------------------------------
!
!  Calculate the temperature using Poisson's formula.
!
!-----------------------------------------------------------------------
!
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        t(i,j,k) = theta(i,j,k) *                         &
             ((ptotal(i,j,k) / p0) ** rddcp)
      END DO
    END DO
  END DO

  RETURN
  END SUBROUTINE temperwrf

  SUBROUTINE cal_rho_p(nx,ny,nz,mub,mu,rdnw,p_top,znu,qv,ph,pt,rhobar)

  IMPLICIT NONE
  ! input varibles
  integer , INTENT(IN) ::  nx,ny,nz
  real,     INTENT(IN) ::  mub(nx,ny),mu(nx,ny)
  REAL,     INTENT(IN) ::  qv(nx,ny,nz)
  REAL,     INTENT(IN) ::  ph(nx,ny,nz+1),pt(nx,ny,nz)  ! should be perturbations
  REAL,     INTENT(IN) ::  rdnw(nz),znu(nz)
  real,     INTENT(IN) ::  p_top

  ! output varibles
  real,     INTENT(OUT) :: rhobar(nx,ny,nz)

!-----------------------------------------------------------------------
  ! local varibles
  REAL, PARAMETER :: pi = 3.1415926535897932346
  REAL, PARAMETER :: gas_constant = 287.     ! Value used in WRF.
  REAL, PARAMETER :: gas_constant_v = 461.6  ! Value used in WRF.
  REAL, PARAMETER :: cp = 7.*gas_constant/2. ! Value used in WRF.
  REAL, PARAMETER :: t_kelvin = 273.15
  REAL, PARAMETER :: kappa = gas_constant / cp
  REAL, PARAMETER :: rd_over_rv = gas_constant / gas_constant_v
  REAL, PARAMETER :: gravity = 9.81        ! m/s - value used in MM5.

!-----------------------------------------------------------------------

  real    :: albn,aln,ppb,ttb,qvf1
  real    :: cvpm,cpovcv,ps0,ts0,tis0,tlp
  real    :: iso_temp, tlp_strat, ps_strat
  real    :: rho, p
  real    :: temp

  INTEGER :: i,j,k

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  !istatus = 0

  ps0  = 100000.0    ! Base sea level pressure
  ts0  = 300.0       ! Base potential temperature for all levels.
  tis0 = 290.0       ! Base sea level temperature
  tlp  = 50.0        ! temperature difference from 1000mb to 300mb

  iso_temp = 200.0   ! isothermal temperature in statosphere
  tlp_strat = -11.0  ! base state lapse rate (dT/d(lnP)) in stratosphere
  ps_strat = 55      ! base state pressure (Pa) at bottom of the stratosphere,
                     ! US Standard atmosphere 55 hPa.

  cvpm =  - (1. - gas_constant/cp)
  cpovcv = cp / (cp - gas_constant)

  DO k = 1, nz
    DO j = 1, ny
      DO i = 1, nx
        ppb  = znu(k) * mub(i,j) + p_top
        temp = MAX( iso_temp, tis0+tlp*log(ppb/ps0) )
        IF ( ppb < ps_strat ) then
          temp = iso_temp + tlp_strat*log(ppb/ps_strat)
        END IF
        ttb  = temp * (ps0/ppb)**kappa
        albn = (gas_constant/ps0) * ttb * (ppb/ps0)**cvpm

        qvf1 = 1. + qv(i,j,k) / rd_over_rv
        aln  = -1. / (mub(i,j)+mu(i,j)) * ( albn*mu(i,j) + rdnw(k) *(ph(i,j,k+1) - ph(i,j,k)) )

        !  total pressure and density
        !WYH p = ps0 * ( (gas_constant*(ts0+pt(i,j,k))*qvf1) / &
        !WYH             (ps0*(aln+albn)) )**cpovcv
        rho = 1.0 / (albn+aln)
        !  perturbation pressure
        !WYH pp(i,j,k) = p  ! -ppb   ! we need total pressure
        !  base-state pressure
        !WYH pbar(i,j,k) = ppb
        !  base-state density
        !WTH rhobar(i,j,k) = 1.0 / albn
        rhobar(i,j,k) = rho
      END DO
    END DO
  END DO

  RETURN
  END SUBROUTINE cal_rho_p

  SUBROUTINE reflec_ferrier2(nx,ny,nz, rho, qr, qs, qh, t, rff)

  IMPLICIT NONE

!-----------------------------------------------------------------------
! Declare arguments.
!-----------------------------------------------------------------------

  INTEGER, INTENT(IN) :: nx,ny,nz ! Dimensions of grid

  REAL, INTENT(IN) :: rho(nx,ny,nz) ! Air density (kg m**-3)
  REAL, INTENT(IN) :: qr(nx,ny,nz) ! Rain mixing ratio (kg kg**-1)
  REAL, INTENT(IN) :: qs(nx,ny,nz) ! Snow mixing ratio (kg kg**-1)
  REAL, INTENT(IN) :: qh(nx,ny,nz) ! Hail mixing ratio (kg kg**-1)
  REAL, INTENT(IN) :: t(nx,ny,nz) ! Temperature (K)

  REAL, INTENT(INOUT) :: rff(nx,ny,nz) ! Reflectivity (dBZ)
!-----------------------------------------------------------------------
! Declare local parameters.
!-----------------------------------------------------------------------

  REAL,PARAMETER :: ki2 = 0.176 ! Dielectric factor for ice if other
                                !   than melted drop diameters are used.
  REAL,PARAMETER :: kw2=0.93 ! Dielectric factor for water.

  REAL,PARAMETER :: degKtoC=273.15 ! Conversion factor from degrees K to
                                   !   degrees C

  REAL,PARAMETER :: m3todBZ=1.0E+18 ! Conversion factor from m**3 to
                                    !   mm**6 m**-3.

  REAL,PARAMETER :: pi=3.1415926 ! Pi.

  REAL,PARAMETER :: pipowf=7.0/4.0 ! Power to which pi is raised.

  REAL,PARAMETER :: N0r=8.0E+06 ! Intercept parameter in 1/(m^4) for rain.
  REAL,PARAMETER :: N0s=3.0E+06 ! Intercept parameter in 1/(m^4) for snow.
  REAL,PARAMETER :: N0h=4.0E+04 ! Intercept parameter in 1/(m^4) for hail.

  REAL,PARAMETER :: N0xpowf=3.0/4.0 ! Power to which N0r,N0s & N0h are
                                    !   raised.

  REAL,PARAMETER :: approxpow=0.95 ! Approximation power for hail
                                   !   integral.

  REAL,PARAMETER :: rqrpowf=7.0/4.0 ! Power to which product rho * qr
                                    !   is raised.
  REAL,PARAMETER :: rqsnpowf=7.0/4.0 ! Power to which product rho * qs
                                     !   is raised (dry snow).
  REAL,PARAMETER :: rqsppowf=7.0/4.0 ! Power to which product rho * qs
                                     !   is raised (wet snow).

  REAL,PARAMETER :: rqhpowf=(7.0/4.0)*approxpow ! Power to which product
                                                !   rho * qh is raised.

  REAL,PARAMETER :: rhoi=917.  ! Density of ice (kg m**-3)
  REAL,PARAMETER :: rhor=1000. ! Density of rain (kg m**-3)
  REAL,PARAMETER :: rhos=100.  ! Density of snow (kg m**-3)
  REAL,PARAMETER :: rhoh=913.  ! Density of hail (kg m**-3)

  REAL,PARAMETER :: rhoipowf=2.0     ! Power to which rhoi is raised.
  REAL,PARAMETER :: rhospowf=1.0/4.0 ! Power to which rhos is raised.
  REAL,PARAMETER :: rhoxpowf=7.0/4.0 ! Power to which rhoh is raised.

  REAL,PARAMETER :: Zefact=720.0 ! Multiplier for Ze components.

  REAL,PARAMETER :: lg10mul=10.0 ! Log10 multiplier
!-----------------------------------------------------------------------
! Declare local variables.
!-----------------------------------------------------------------------

  REAL :: rcomp,scomp,hcomp,sumcomp
  INTEGER :: i,j,k
  REAL(8),PARAMETER :: Zerf=3630803470.863780
  REAL(8),PARAMETER :: Zesnegf=958893421.2925436
  REAL(8),PARAMETER :: Zesposf=426068366492.9870
  REAL(8),PARAMETER :: Zehf   =61263782772.99985
  REAL,   PARAMETER :: intvl=3.0
  REAL :: rtem

!-----------------------------------------------------------------------
! Now loop through the scalar grid points.
!-----------------------------------------------------------------------
  rcomp = 0.0
  scomp = 0.0
  hcomp = 0.0

  DO k = 1,nz         ! Eric 8/8/03
    DO j = 1,ny
      DO i = 1,nx

!-----------------------------------------------------------------------
! Check for bad air density value.
!-----------------------------------------------------------------------

        IF (rho(i,j,k) <= 0.0) THEN
          rff(i,j,k) = 0.0
        ELSE

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from rain.
!-----------------------------------------------------------------------

          IF (qr(i,j,k) <= 0.0) THEN
            rcomp = 0.0
          ELSE
            rcomp = Zerf*((qr(i,j,k)*rho(i,j,k)) ** rqrpowf)
          END IF

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from snow (dry or wet).
!-----------------------------------------------------------------------
          IF (qs(i,j,k) <= 0.0) THEN
            scomp = 0.0
          ELSE IF (t(i,j,k) <= degKtoC) THEN
            scomp = Zesnegf*((qs(i,j,k)*rho(i,j,k)) ** rqsnpowf)
          ELSE
            scomp = Zesposf*((qs(i,j,k)*rho(i,j,k)) ** rqsppowf)
          END IF

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from hail.
!-----------------------------------------------------------------------

          IF (qh(i,j,k) <= 0.0) THEN
            hcomp = 0.0
          ELSE
            hcomp = Zehf*((qh(i,j,k)*rho(i,j,k)) ** rqhpowf)
          END IF
!-----------------------------------------------------------------------
! Now add the contributions and convert to logarithmic reflectivity
! factor dBZ.
!-----------------------------------------------------------------------
!         sumcomp = rcomp + scomp + hcomp
!
          if (t(i,j,k) >= (degKtoC + intvl ) ) then
            sumcomp = rcomp
          elseif (t(i,j,k) <= (degKtoC - intvl )) then
            sumcomp = scomp + hcomp
          else
            rtem=(t(i,j,k)-degKtoC+intvl)/(2*intvl)
            sumcomp=rtem*rcomp+(1-rtem)*(scomp+hcomp)
          end if
!
!          if(sumcomp>1.0) print*,'sumcomp===',sumcomp
!
         IF( sumcomp>1.0 ) THEN
           rff(i,j,k) = lg10mul * LOG10(sumcomp)
         ELSE
           rff(i,j,k) = 0.0
         END IF
        END IF !  IF (rho(i,j,k) <= 0.0) ... ELSE ...

      END DO ! DO i
    END DO ! DO j
  END DO ! DO k

  RETURN
  END SUBROUTINE reflec_ferrier2

END MODULE wrf_io
