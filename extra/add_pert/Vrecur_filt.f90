MODULE recur_filt

  IMPLICIT NONE
  CONTAINS

  SUBROUTINE recurfilt3d(nx,ny,nz,pgrd,ipass_filt,ipass_loop,stagdim,     &
                           hradius,radius_z,alpha,beta,tem1,tem2)

  IMPLICIT NONE

  INTEGER, INTENT(IN) :: nx,ny,nz
  INTEGER, INTENT(IN) :: ipass_filt, ipass_loop
  REAL,    INTENT(IN) :: hradius
  REAL,    INTENT(IN) :: radius_z(nx,ny,nz)

  INTEGER, INTENT(IN) :: stagdim
  !INTEGER, INTENT(IN) :: ips,ipe, jps,jpe,kps,kpe


  REAL,    INTENT(INOUT) :: pgrd(nx,ny,nz)

  ! working arrays
  REAL,    INTENT(INOUT) :: alpha(nx,ny,nz)
  REAL,    INTENT(INOUT) :: beta (nx,ny,nz)

  REAL,    INTENT(INOUT) :: tem1(nx,ny,nz)   ! Temporary array for MPI
                                             ! Can be two dimensions as
                                             ! (max(nx,ny),nz)
  REAL,    INTENT(INOUT) :: tem2(nx,ny,nz)   ! Temporary array for MPI
                                             ! Can be two dimensions as
                                             ! (max(nx,ny),nz)

!-----------------------------------------------------------------------
!
! Misc local variables
!
!-----------------------------------------------------------------------

  REAL :: ee
  REAL :: temp1,temp2

  INTEGER :: i,j,k,n

  INTEGER :: ic, jc

  INTEGER :: ibgn, iend, jbgn, jend, kbgn, kend

  IF( hradius == 0 ) return

  ibgn = 1; iend = nx
  jbgn = 1; jend = ny
  kbgn = 1; kend = nz

  DO n = 1, ipass_loop ! ipass_filt/2

!-----------------------------------------------------------------------
! X direction - forward
!-----------------------------------------------------------------------
    DO k = 1, nz
      DO j = 1, ny
        DO i = 1, nx
          ee = REAL(ipass_filt) / (hradius*hradius)
          alpha(i,j,k) = 1+ee-SQRT( ee*(ee+2.) )
          beta(i,j,k)  = 1.-alpha(i,j,k)
        END DO
      END DO
    END DO

    CALL set_bdyxs(pgrd,nx,ny,nz,1,jbgn,jend,kbgn,kend,n,alpha,beta)
    DO k = kbgn,kend
      DO j = jbgn, jend
        DO i = 2, iend
          pgrd(i,j,k)=alpha(i,j,k)*pgrd(i-1,j,k)+beta(i,j,k)*pgrd(i,j,k)
        END DO
      END DO
    END DO

!-----------------------------------------------------------------------
! X direction - backward
!-----------------------------------------------------------------------

    CALL set_bdyxe(pgrd,nx,ny,nz,iend,jbgn,jend,kbgn,kend,n,alpha,beta)
    DO k = kbgn,kend
      DO j = jbgn,jend
        DO i = iend-1, ibgn, -1
          pgrd(i,j,k)=alpha(i,j,k)*pgrd(i+1,j,k)+beta(i,j,k)*pgrd(i,j,k)
        END DO
      END DO
    END DO

!-----------------------------------------------------------------------
! Y direction - forward
!-----------------------------------------------------------------------

    CALL set_bdyys(pgrd,nx,ny,nz,1,ibgn,iend,kbgn,kend,n,alpha,beta)
    DO k = kbgn,kend
      DO j = 2, jend
        DO i = ibgn,iend
          pgrd(i,j,k)=alpha(i,j,k)*pgrd(i,j-1,k)+beta(i,j,k)*pgrd(i,j,k)
        END DO
      END DO
    END DO

!-----------------------------------------------------------------------
! Y direction - backword
!-----------------------------------------------------------------------

    CALL set_bdyye(pgrd,nx,ny,nz,jend,ibgn,iend,kbgn,kend,n,alpha,beta)
    DO k = kbgn,kend
      DO j = jend-1, jbgn, -1
        DO i = ibgn,iend
          pgrd(i,j,k)=alpha(i,j,k)*pgrd(i,j+1,k)+beta(i,j,k)*pgrd(i,j,k)
        END DO
      END DO
    END DO

!-----------------------------------------------------------------------
! Z direction - forward
!-----------------------------------------------------------------------

      DO k = 1, nz
        DO j = 1, ny
          DO i = 1, nx
            ee = REAL(ipass_filt)/(radius_z(i,j,k)*radius_z(i,j,k))
            alpha(i,j,k) = 1+ee-SQRT( ee*(ee+2.) )
             beta(i,j,k) = 1.-alpha(i,j,k)
          END DO
        END DO
      END DO

      SELECT CASE (n)
      CASE (1)
        DO j = jbgn, jend
          DO i = ibgn, iend
            pgrd(i,j,kbgn) = beta(i,j,kbgn) * pgrd(i,j,kbgn)
          END DO
        END DO
      CASE (2)
        DO j = jbgn, jend
          DO i = ibgn, iend
            temp1 = 1.-alpha(i,j,kbgn)*alpha(i,j,kbgn)
            pgrd(i,j,kbgn) = beta(i,j,kbgn)/temp1 * pgrd(i,j,kbgn)
          END DO
        END DO
      CASE (3)
        DO j = jbgn, jend
          DO i = ibgn, iend
            temp1 = (1-alpha(i,j,kbgn))/        &
                ((1-alpha(i,j,kbgn)*alpha(i,j,kbgn))*(1-alpha(i,j,kbgn)*alpha(i,j,kbgn)))
            temp2 =alpha(i,j,kbgn)*alpha(i,j,kbgn)*alpha(i,j,kbgn)
            pgrd(i,j,kbgn) = temp1 * (pgrd(i,j,kbgn)-temp2*pgrd(i,j,kbgn+1))
          END DO
        END DO
      CASE DEFAULT
        DO j = jbgn, jend
          DO i = ibgn, iend
            temp2 =alpha(i,j,kbgn)*alpha(i,j,kbgn)*alpha(i,j,kbgn)
            temp1 = (1-alpha(i,j,kbgn))/        &
               (1-3*alpha(i,j,kbgn)*alpha(i,j,kbgn)+3*temp2*alpha(i,j,kbgn)-temp2*temp2)
            pgrd(i,j,kbgn) = temp1 * (pgrd(i,j,kbgn)-3*temp2*pgrd(i,j,kbgn+1)+  &
               temp2*alpha(i,j,kbgn)*alpha(i,j,kbgn)*pgrd(i,j,kbgn+1)   &
               +temp2*alpha(i,j,kbgn)*pgrd(i,j,kbgn+2))
          END DO
        END DO
      END SELECT

      DO k = 2, kend, 1
        DO j = jbgn, jend
          DO i = ibgn, iend
           pgrd(i,j,k) = alpha(i,j,k)*pgrd(i,j,k-1)+beta(i,j,k)*pgrd(i,j,k)
          END DO
        END DO
      END DO

!-----------------------------------------------------------------------
! Z direction - backward
!-----------------------------------------------------------------------

      SELECT CASE (n)
      CASE (0)
        DO j = jbgn, jend
          DO i = ibgn, iend
            pgrd(i,j,kend) = beta(i,j,kend) * pgrd(i,j,kend)
          END DO
        END DO
      CASE (1)
        DO j = jbgn, jend
          DO i = ibgn, iend
            temp1 = (1.-alpha(i,j,kend)*alpha(i,j,kend))
            pgrd(i,j,kend) = beta(i,j,kend)/temp1 * pgrd(i,j,kend)
          END DO
        END DO
      CASE (2)
        DO j = jbgn, jend
          DO i = ibgn, iend
            temp1 = (1-alpha(i,j,kend))/        &
             ((1-alpha(i,j,kend)*alpha(i,j,kend))*(1-alpha(i,j,kend)*alpha(i,j,kend)))
            temp2 = alpha(i,j,kend)*alpha(i,j,kend)*alpha(i,j,kend)
            pgrd(i,j,kend) = temp1 * (pgrd(i,j,kend)-temp2*pgrd(i,j,kend-1))
          END DO
        END DO
      CASE DEFAULT
        DO j = jbgn, jend
          DO i = ibgn, iend
            temp2 = alpha(i,j,kend)*alpha(i,j,kend)*alpha(i,j,kend)
            temp1 = (1-alpha(i,j,kend))/(1-3*alpha(i,j,kend)*alpha(i,j,kend)+   &
                              3*temp2*alpha(i,j,kend)-temp2*temp2)
            pgrd(i,j,kend) = temp1 * (pgrd(i,j,kend)-3*temp2*pgrd(i,j,kend-1)+  &
                   temp2*alpha(i,j,kend)*alpha(i,j,kend)*pgrd(i,j,kend-1)+      &
                   temp2*alpha(i,j,kend)*pgrd(i,j,kend-2))
          END DO
        END DO
      END SELECT

      DO k = kend-1, kbgn, -1
        DO j = jbgn, jend
          DO i = ibgn, iend
            pgrd(i,j,k)=alpha(i,j,k)*pgrd(i,j,k+1)+beta(i,j,k)*pgrd(i,j,k)
          END DO
        END DO
      END DO

  END DO

  RETURN
END SUBROUTINE recurfilt3d

SUBROUTINE set_bdyxs(pgrd,nx,ny,nz,bdyi,jbgn,jend,kbgn,kend,n,alpha,beta)

! Set XS boundary
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: nx, ny, nz, n
  INTEGER, INTENT(IN)  :: bdyi                  ! should be 1
  INTEGER, INTENT(IN)  :: jbgn,jend,kbgn,kend
  REAL,    INTENT(IN)  :: alpha(nx,ny,nz)
  REAL,    INTENT(IN)  ::  beta(nx,ny,nz)
  REAL,    INTENT(INOUT) :: pgrd(nx,ny,nz)

  ! local variables

  REAL    :: temp1, temp2
  INTEGER :: k,j

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!XS

      SELECT CASE (n)
        CASE (1)
          DO k = kbgn,kend
            DO j = jbgn,jend
              pgrd(bdyi,j,k) = beta(bdyi,j,k)*pgrd(bdyi,j,k)
            END DO
          END DO
        CASE (2)
          DO k = kbgn,kend
            DO j = jbgn,jend
              temp1 = (1.-alpha(bdyi,j,k)*alpha(bdyi,j,k))
              pgrd(bdyi,j,k) = beta(bdyi,j,k)/temp1 * pgrd(bdyi,j,k)
            END DO
          END DO
        CASE (3)
          DO k = kbgn,kend
            DO j = jbgn,jend
              temp1 = (1-alpha(bdyi,j,k))/      &
                  ((1-alpha(bdyi,j,k)*alpha(bdyi,j,k))*(1-alpha(bdyi,j,k)*alpha(bdyi,j,k)))
              temp2 = alpha(bdyi,j,k)*alpha(bdyi,j,k)*alpha(bdyi,j,k)
              pgrd(bdyi,j,k) = temp1 * (pgrd(bdyi,j,k)-temp2*pgrd(bdyi+1,j,k))
            END DO
          END DO
        CASE DEFAULT
          DO k = kbgn,kend
            DO j = jbgn,jend
              temp2 = alpha(bdyi,j,k)*alpha(bdyi,j,k)*alpha(bdyi,j,k)
              temp1 = (1-alpha(bdyi,j,k))/      &
                  (1-3*alpha(bdyi,j,k)*alpha(bdyi,j,k)+3*temp2*alpha(bdyi,j,k)-temp2*temp2)
              pgrd(bdyi,j,k) = temp1 * (pgrd(bdyi,j,k)-3*temp2*pgrd(bdyi+1,j,k)+        &
                            temp2*alpha(bdyi,j,k)*alpha(bdyi,j,k)*pgrd(bdyi+1,j,k)+     &
                            temp2*alpha(bdyi,j,k)*pgrd(bdyi+2,j,k))
            END DO
          END DO
      END SELECT

   RETURN
END SUBROUTINE set_bdyxs

SUBROUTINE set_bdyxe(pgrd,nx,ny,nz,bdyi,jbgn,jend,kbgn,kend,n,alpha,beta)

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: nx, ny, nz, n
  INTEGER, INTENT(IN)  :: bdyi   ! nx-1 for scalar and internal domains
                                 ! nx for east domains and U stagger
  INTEGER, INTENT(IN)  :: jbgn,jend,kbgn,kend
  REAL,    INTENT(IN)  :: alpha(nx,ny,nz)
  REAL,    INTENT(IN)  ::  beta(nx,ny,nz)

  REAL,    INTENT(INOUT)  :: pgrd(nx,ny,nz)

  ! local variables

  REAL    :: temp1, temp2
  INTEGER :: k,j

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!XE
  SELECT CASE (n)
    CASE (0)
      DO k = kbgn,kend
        DO j = jbgn,jend
          pgrd(bdyi,j,k) = beta(bdyi,j,k) * pgrd(bdyi,j,k)
        END DO
      END DO
    CASE (1)
      DO k = kbgn,kend
        DO j = jbgn,jend
          temp1 = 1-alpha(bdyi,j,k)*alpha(bdyi,j,k)
          pgrd(bdyi,j,k) = beta(bdyi,j,k)/temp1 * pgrd(bdyi,j,k)
        END DO
      END DO
    CASE (2)
      DO k = kbgn,kend
        DO j = jbgn,jend
          temp1 = (1-alpha(bdyi,j,k))/  &
     ((1-alpha(bdyi,j,k)*alpha(bdyi,j,k))*(1-alpha(bdyi,j,k)*alpha(bdyi,j,k)))
          temp2 = alpha(bdyi,j,k)*alpha(bdyi,j,k)*alpha(bdyi,j,k)
          pgrd(bdyi,j,k) = temp1 * (pgrd(bdyi,j,k)-temp2*pgrd(bdyi-1,j,k))
        END DO
      END DO
  CASE DEFAULT
    DO k = kbgn,kend
      DO j = jbgn,jend
        temp2 = alpha(bdyi,j,k)*alpha(bdyi,j,k)*alpha(bdyi,j,k)
        temp1 = (1-alpha(bdyi,j,k))/    &
      (1-3*alpha(bdyi,j,k)*alpha(bdyi,j,k)+3*temp2*alpha(bdyi,j,k)-temp2*temp2)
        pgrd(bdyi,j,k) = temp1 * (pgrd(bdyi,j,k)-3*temp2*pgrd(bdyi-1,j,k)+      &
             temp2*alpha(bdyi,j,k)*alpha(bdyi,j,k)*pgrd(bdyi-1,j,k)+    &
             temp2*alpha(bdyi,j,k)*pgrd(bdyi-2,j,k))
      END DO
    END DO
  END SELECT

  RETURN
END SUBROUTINE set_bdyxe

SUBROUTINE set_bdyys(pgrd,nx,ny,nz,bdyj,ibgn,iend,kbgn,kend,n,alpha,beta)

! Set XS boundary
  IMPLICIT NONE

  INTEGER, INTENT(IN)    :: nx, ny, nz, n
  INTEGER, INTENT(IN)    :: bdyj                  ! should be 1
  INTEGER, INTENT(IN)    :: ibgn,iend,kbgn,kend
  REAL,    INTENT(IN)    :: alpha(nx,ny,nz)
  REAL,    INTENT(IN)    ::  beta(nx,ny,nz)
  REAL,    INTENT(INOUT) :: pgrd(nx,ny,nz)

  ! local variables

  REAL    :: temp1, temp2
  INTEGER :: k,i

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!YS

  SELECT CASE (n)
    CASE (1)
      DO k = kbgn,kend
        DO i = ibgn,iend
          pgrd(i,bdyj,k) = beta(i,bdyj,k) * pgrd(i,bdyj,k)
        END DO
      END DO
    CASE (2)
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp1 = (1.-alpha(i,bdyj,k)*alpha(i,bdyj,k))
          pgrd(i,bdyj,k) = beta(i,bdyj,k)/temp1 * pgrd(i,bdyj,k)
        END DO
      END DO
    CASE (3)
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp1 = (1-alpha(i,bdyj,k))/  &
               ((1-alpha(i,bdyj,k)*alpha(i,bdyj,k))*(1-alpha(i,bdyj,k)*alpha(i,bdyj,k)))
          temp2 = alpha(i,bdyj,k)*alpha(i,bdyj,k)*alpha(i,bdyj,k)
          pgrd(i,bdyj,k) = temp1 * (pgrd(i,bdyj,k)-temp2*pgrd(i,bdyj+1,k))
        END DO
      END DO
    CASE DEFAULT
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp2 = alpha(i,bdyj,k)*alpha(i,bdyj,k)*alpha(i,bdyj,k)
          temp1 = (1-alpha(i,bdyj,k))/  &
             (1-3*alpha(i,bdyj,k)*alpha(i,bdyj,k)+3*temp2*alpha(i,bdyj,k)-temp2*temp2)
          pgrd(i,bdyj,k) = temp1 * (pgrd(i,bdyj,k)-3*temp2*pgrd(i,bdyj+1,k)+    &
              temp2*alpha(i,bdyj,k)*alpha(i,bdyj,k)*pgrd(i,bdyj+1,k)+   &
              temp2*alpha(i,bdyj,k)*pgrd(i,bdyj+2,k))
        END DO
      END DO
  END SELECT

  RETURN
END SUBROUTINE set_bdyys

SUBROUTINE set_bdyye(pgrd,nx,ny,nz,bdyj,ibgn,iend,kbgn,kend,n,alpha,beta)

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: nx, ny, nz, n
  INTEGER, INTENT(IN)  :: bdyj   ! ny-1 for scalar and internal domains
                                 ! ny for east domains and V stagger
  INTEGER, INTENT(IN)  :: ibgn,iend,kbgn,kend
  REAL,    INTENT(IN)  ::  alpha(nx,ny,nz)
  REAL,    INTENT(IN)  ::   beta(nx,ny,nz)
  REAL,    INTENT(INOUT) :: pgrd(nx,ny,nz)

  ! local variables

  REAL    :: temp1, temp2
  INTEGER :: k,i

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!YE
  SELECT CASE ( n )

    CASE (0)
      DO k = kbgn,kend
        DO i = ibgn,iend
          pgrd(i,bdyj,k) = beta(i,bdyj,k) * pgrd(i,bdyj,k)
        END DO
      END DO
    CASE ( 1)
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp1 = (1.-alpha(i,bdyj,k)*alpha(i,bdyj,k))
          pgrd(i,bdyj,k) = beta(i,bdyj,k)/temp1 * pgrd(i,bdyj,k)
        END DO
      END DO
    CASE ( 2)
      DO k = kbgn,kend
        DO i = ibgn,iend
         temp1 = (1-alpha(i,bdyj,k))/   &
           ((1-alpha(i,bdyj,k)*alpha(i,bdyj,k))*(1-alpha(i,bdyj,k)*alpha(i,bdyj,k)))
         temp2 = alpha(i,bdyj,k)*alpha(i,bdyj,k)*alpha(i,bdyj,k)
         pgrd(i,bdyj,k) = temp1 * (pgrd(i,bdyj,k)-temp2*pgrd(i,bdyj-1,k))
        END DO
      END DO
    CASE DEFAULT
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp2 = alpha(i,bdyj,k)*alpha(i,bdyj,k)*alpha(i,bdyj,k)
          temp1 = (1-alpha(i,bdyj,k))/(1-3*alpha(i,bdyj,k)*alpha(i,bdyj,k)+     &
                   3*temp2*alpha(i,bdyj,k)-temp2*temp2)
          pgrd(i,bdyj,k) = temp1 * (pgrd(i,bdyj,k)-3*temp2*pgrd(i,bdyj-1,k)+    &
               temp2*alpha(i,bdyj,k)*alpha(i,bdyj,k)*pgrd(i,bdyj-1,k)+          &
               temp2*alpha(i,bdyj,k)*pgrd(i,bdyj-2,k))
        END DO
      END DO
  END SELECT

  RETURN
END SUBROUTINE  set_bdyye

END MODULE recur_filt
