MODULE module_cwp

  IMPLICIT NONE
  !-------------------------------------------------------------------------------
  !
  ! Variable Delaration
  !
  !-------------------------------------------------------------------------------
  CHARACTER(LEN=512)                    :: wrf_input, ob_input, bias_file, rms_file, mode
  INTEGER                               :: rms_unit
  LOGICAL                               :: debug
  INTEGER                               :: iter, istatus
  REAL                                  :: rms, bias, ncount

  INTEGER, PARAMETER                    :: namelist_unit=11

  namelist /ctrl_para/ wrf_input, ob_input, rms_file, mode, iter, debug

  CONTAINS

  SUBROUTINE namelist_read
  
    !------------------------------------------------------------------
    ! Set default parameters
    !------------------------------------------------------------------
  
    wrf_input             = 'wrfinput_d01'
    ob_input              = 'obwrf'
    rms_file              = 'rms_output'
    mode                  = 'rms_calc'
    iter  = -1
    debug =.FALSE.
  
    istatus = 0
  
    !------------------------------------------------------------------
    ! Read namelist
    !------------------------------------------------------------------
  
    open (unit = namelist_unit, file = "namelist.cwp", &
            status = 'old', form = 'formatted', iostat = istatus)
  
    if ( istatus /=0 ) then
       write (unit=6, fmt=*) 'Error to open namelist file: namelist.ptb. Stopped.'
       stop
    else
       !----------------------------------------------------------------------------
       ! Read control parameters
       !----------------------------------------------------------------------------
       read (unit=namelist_unit, nml = ctrl_para, iostat = istatus)
  
       if ( istatus /= 0 ) then
          write (unit=6, fmt=*) 'Error to read namelist ctrl_para. Stopped.'
          stop
       end if

       if ( iter <= 0 ) then
          write (unit=6, fmt=*) 'Iteration number is not properly set.'
          stop
       end if
  
       write (unit=6, fmt='(a)') 'Read ctrl_para namelist successfully.'
       write (unit=6, fmt='(2a)') &
              'wrf_input                =', trim(wrf_input)
       write (unit=6, fmt='(2a)') &
              'ob_input                 =', trim(ob_input)
       if ( mode == 'rms_calc' ) then
         write (unit=6, fmt='(a)') 'running mode is rms calculation.'
       else if ( mode == 'sim_cwp' ) then
         write (unit=6, fmt='(a)') 'running mode is cloud water path simulation.'
       end if
       write (unit=6, fmt='(a,L13)') &
              'debug                    =', debug
  
    end if
  
    close (unit = namelist_unit)

  END SUBROUTINE namelist_read
  
  SUBROUTINE cwp_forward(nx,ny,nz,ipt,jpt,pp,wgt,phase,  &
                        qc,qi,qr,qs,qg,cldbslv,cldtplv,hcwp,tc)

    IMPLICIT NONE

    INTEGER        :: nx,ny,nz,ipt,jpt,k,n,nqs,nqe,phase,cldbslv,cldtplv
    REAL           :: wgt(4)
    REAL, DIMENSION(nx,ny,nz) :: qc,qi,qr,qs,qg
    REAL           :: pp(nx,ny,nz),tc(nx,ny,nz)
    REAL           :: hcwp
    REAL           :: tmp1(4),tmp2(4)
    REAL, PARAMETER :: g=9.81, zero=273.15

    tmp2=0.

    IF ( ipt/=nx .and. jpt/=ny ) THEN

      DO k=cldbslv,cldtplv-1
        tmp1=0.
        tmp1(1)=tmp1(1)+(qc(ipt,jpt,k)+qc(ipt,jpt,k+1))*0.5
        tmp1(2)=tmp1(2)+(qc(ipt+1,jpt,k)+qc(ipt+1,jpt,k+1))*0.5
        tmp1(3)=tmp1(3)+(qc(ipt+1,jpt+1,k)+qc(ipt+1,jpt+1,k+1))*0.5
        tmp1(4)=tmp1(4)+(qc(ipt,jpt+1,k)+qc(ipt,jpt+1,k+1))*0.5
        tmp1(1)=tmp1(1)+(qr(ipt,jpt,k)+qr(ipt,jpt,k+1))*0.5
        tmp1(2)=tmp1(2)+(qr(ipt+1,jpt,k)+qr(ipt+1,jpt,k+1))*0.5
        tmp1(3)=tmp1(3)+(qr(ipt+1,jpt+1,k)+qr(ipt+1,jpt+1,k+1))*0.5
        tmp1(4)=tmp1(4)+(qr(ipt,jpt+1,k)+qr(ipt,jpt+1,k+1))*0.5
        IF ( phase == 2 ) THEN
          IF (tc(ipt,jpt,k)<=zero) tmp1(1)=tmp1(1)+(qi(ipt,jpt,k)+qi(ipt,jpt,k+1))*0.5
          IF (tc(ipt+1,jpt,k)<=zero) tmp1(2)=tmp1(2)+(qi(ipt+1,jpt,k)+qi(ipt+1,jpt,k+1))*0.5
          IF (tc(ipt+1,jpt+1,k)<=zero) tmp1(3)=tmp1(3)+(qi(ipt+1,jpt+1,k)+qi(ipt+1,jpt+1,k+1))*0.5
          IF (tc(ipt,jpt+1,k)<=zero) tmp1(4)=tmp1(4)+(qi(ipt,jpt+1,k)+qi(ipt,jpt+1,k+1))*0.5

          IF (tc(ipt,jpt,k)<=zero) tmp1(1)=tmp1(1)+(qs(ipt,jpt,k)+qs(ipt,jpt,k+1))*0.5
          IF (tc(ipt+1,jpt,k)<=zero) tmp1(2)=tmp1(2)+(qs(ipt+1,jpt,k)+qs(ipt+1,jpt,k+1))*0.5
          IF (tc(ipt+1,jpt+1,k)<=zero) tmp1(3)=tmp1(3)+(qs(ipt+1,jpt+1,k)+qs(ipt+1,jpt+1,k+1))*0.5
          IF (tc(ipt,jpt+1,k)<=zero) tmp1(4)=tmp1(4)+(qs(ipt,jpt+1,k)+qs(ipt,jpt+1,k+1))*0.5

          IF (tc(ipt,jpt,k)<=zero) tmp1(1)=tmp1(1)+(qg(ipt,jpt,k)+qg(ipt,jpt,k+1))*0.5
          IF (tc(ipt+1,jpt,k)<=zero) tmp1(2)=tmp1(2)+(qg(ipt+1,jpt,k)+qg(ipt+1,jpt,k+1))*0.5
          IF (tc(ipt+1,jpt+1,k)<=zero) tmp1(3)=tmp1(3)+(qg(ipt+1,jpt+1,k)+qg(ipt+1,jpt+1,k+1))*0.5
          IF (tc(ipt,jpt+1,k)<=zero) tmp1(4)=tmp1(4)+(qg(ipt,jpt+1,k)+qg(ipt,jpt+1,k+1))*0.5
        END IF
        tmp2(1)=tmp2(1)+tmp1(1)*(pp(ipt,jpt,k)-pp(ipt,jpt,k+1))
        tmp2(2)=tmp2(2)+tmp1(2)*(pp(ipt+1,jpt,k)-pp(ipt+1,jpt,k+1))
        tmp2(3)=tmp2(3)+tmp1(3)*(pp(ipt+1,jpt+1,k)-pp(ipt+1,jpt+1,k+1))
        tmp2(4)=tmp2(4)+tmp1(4)*(pp(ipt,jpt+1,k)-pp(ipt,jpt+1,k+1))
      END DO
      hcwp=(wgt(1)*tmp2(1)+wgt(2)*tmp2(2)+      &
           wgt(3)*tmp2(3)+wgt(4)*tmp2(4))/g

    ELSE IF ( ipt==nx .and. jpt/=nx ) THEN
      DO k=cldbslv,cldtplv-1
        tmp1=0.
        tmp1(1)=tmp1(1)+(qc(ipt,jpt,k)+qc(ipt,jpt,k+1))*0.5
        tmp1(4)=tmp1(4)+(qc(ipt,jpt+1,k)+qc(ipt,jpt+1,k+1))*0.5
        tmp1(1)=tmp1(1)+(qr(ipt,jpt,k)+qr(ipt,jpt,k+1))*0.5
        tmp1(4)=tmp1(4)+(qr(ipt,jpt+1,k)+qr(ipt,jpt+1,k+1))*0.5
        IF ( phase == 2 ) THEN
          IF (tc(ipt,jpt,k)<=zero) tmp1(1)=tmp1(1)+(qi(ipt,jpt,k)+qi(ipt,jpt,k+1))*0.5
          IF (tc(ipt,jpt+1,k)<=zero) tmp1(4)=tmp1(4)+(qi(ipt,jpt+1,k)+qi(ipt,jpt+1,k+1))*0.5
          IF (tc(ipt,jpt,k)<=zero) tmp1(1)=tmp1(1)+(qs(ipt,jpt,k)+qs(ipt,jpt,k+1))*0.5
          IF (tc(ipt,jpt+1,k)<=zero) tmp1(4)=tmp1(4)+(qs(ipt,jpt+1,k)+qs(ipt,jpt+1,k+1))*0.5
          IF (tc(ipt,jpt,k)<=zero) tmp1(1)=tmp1(1)+(qg(ipt,jpt,k)+qg(ipt,jpt,k+1))*0.5
          IF (tc(ipt,jpt+1,k)<=zero) tmp1(4)=tmp1(4)+(qg(ipt,jpt+1,k)+qg(ipt,jpt+1,k+1))*0.5
        END IF
        tmp2(1)=tmp2(1)+tmp1(1)*(pp(ipt,jpt,k)-pp(ipt,jpt,k+1))
        tmp2(4)=tmp2(4)+tmp1(4)*(pp(ipt,jpt+1,k)-pp(ipt,jpt+1,k+1))
      END DO
      hcwp=(wgt(1)*tmp2(1)+wgt(4)*tmp2(4))/g
    ELSE IF ( ipt/=nx .and. jpt==nx ) THEN
      DO k=cldbslv,cldtplv-1
        tmp1=0.
        tmp1(1)=tmp1(1)+(qc(ipt,jpt,k)+qc(ipt,jpt,k+1))*0.5
        tmp1(2)=tmp1(2)+(qc(ipt+1,jpt,k)+qc(ipt+1,jpt,k+1))*0.5
        tmp1(1)=tmp1(1)+(qr(ipt,jpt,k)+qr(ipt,jpt,k+1))*0.5
        tmp1(2)=tmp1(2)+(qr(ipt+1,jpt,k)+qr(ipt+1,jpt,k+1))*0.5
        IF ( phase == 2 ) THEN
          IF (tc(ipt,jpt,k)<=zero) tmp1(1)=tmp1(1)+(qi(ipt,jpt,k)+qi(ipt,jpt,k+1))*0.5
          IF (tc(ipt+1,jpt,k)<=zero) tmp1(2)=tmp1(2)+(qi(ipt+1,jpt,k)+qi(ipt+1,jpt,k+1))*0.5
          IF (tc(ipt,jpt,k)<=zero) tmp1(1)=tmp1(1)+(qs(ipt,jpt,k)+qs(ipt,jpt,k+1))*0.5
          IF (tc(ipt+1,jpt,k)<=zero) tmp1(2)=tmp1(2)+(qs(ipt+1,jpt,k)+qs(ipt+1,jpt,k+1))*0.5
          IF (tc(ipt,jpt,k)<=zero) tmp1(1)=tmp1(1)+(qg(ipt,jpt,k)+qg(ipt,jpt,k+1))*0.5
          IF (tc(ipt+1,jpt,k)<=zero) tmp1(2)=tmp1(2)+(qg(ipt+1,jpt,k)+qg(ipt+1,jpt,k+1))*0.5
        END IF
        tmp2(1)=tmp2(1)+tmp1(1)*(pp(ipt,jpt,k)-pp(ipt,jpt,k+1))
        tmp2(2)=tmp2(2)+tmp1(2)*(pp(ipt+1,jpt,k)-pp(ipt+1,jpt,k+1))
      END DO
      hcwp=(wgt(1)*tmp2(1)+wgt(2)*tmp2(2))/g
    ELSE
      DO k=cldbslv,cldtplv-1
        tmp1=0.
        tmp1(1)=tmp1(1)+(qc(ipt,jpt,k)+qc(ipt,jpt,k+1))*0.5
        tmp1(1)=tmp1(1)+(qr(ipt,jpt,k)+qr(ipt,jpt,k+1))*0.5
        IF ( phase == 2 ) THEN
          IF (tc(ipt,jpt,k)<=zero) tmp1(1)=tmp1(1)+(qi(ipt,jpt,k)+qi(ipt,jpt,k+1))*0.5
          IF (tc(ipt,jpt,k)<=zero) tmp1(1)=tmp1(1)+(qs(ipt,jpt,k)+qs(ipt,jpt,k+1))*0.5
          IF (tc(ipt,jpt,k)<=zero) tmp1(1)=tmp1(1)+(qg(ipt,jpt,k)+qg(ipt,jpt,k+1))*0.5
        END IF
        tmp2(1)=tmp2(1)+tmp1(1)*(pp(ipt,jpt,k)-pp(ipt,jpt,k+1))
      END DO
      hcwp=wgt(1)*tmp2(1)/g
    END IF

    RETURN
  END SUBROUTINE cwp_forward

END MODULE module_cwp
