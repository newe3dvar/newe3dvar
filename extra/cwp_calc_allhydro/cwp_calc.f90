  PROGRAM cwp_calc

  USE module_mapproj
  !USE module_cwp
  !USE netcdf_io, only: get_dims_wrf
  USE wrf_io

  IMPLICIT NONE

  !-------------------------------------------------------------------------------
  !
  ! Include Files / Variable Delaration
  !
  !-------------------------------------------------------------------------------

  REAL, ALLOCATABLE, DIMENSION(:,:)            :: hcwp
  REAL, ALLOCATABLE, DIMENSION(:,:)            :: lat, long, top, base, obcwp
  REAL, ALLOCATABLE, DIMENSION(:,:)            :: xloc, yloc
  REAL, ALLOCATABLE, DIMENSION(:)                 :: x, y
  INTEGER, ALLOCATABLE, DIMENSION(:,:)            :: phase
  REAL, ALLOCATABLE, DIMENSION(:,:,:)          :: qc, qi, qr, qs, qg, pres, tc

  INTEGER                                         :: nx, ny, nz, time, numtot
  INTEGER                                         :: nx_ob, ny_ob

  INTEGER                                         :: cldbslv, cldtplv
  REAL                                         :: wgt(4),delx,dely,lvhgt(2)

  !-------------------------------------------------------------------------------
  !
  ! Main Program
  !
  !-------------------------------------------------------------------------------

  CALL namelist_read

print*,'getting observation dimensions.....'
  CALL get_dims_ob(ob_input,'cloud_phase',dimsob,ndimsob,debug)
print*,'dimsob===========',dimsob

  nx_ob=dimsob(1); ny_ob=dimsob(2)
  IF (mode=='rms_calc') THEN
    rms=0.0;ncount=0.0
    allocate(obcwp(nx_ob,ny_ob))
    CALL read_real_ob(ob_input,obcwp,"cloud_lwp_iwp",debug)
  END IF
  allocate(hcwp(nx_ob,ny_ob))
  allocate(lat(nx_ob,ny_ob))
  allocate(long(nx_ob,ny_ob))
  allocate(top(nx_ob,ny_ob))
  allocate(base(nx_ob,ny_ob))
  allocate(phase(nx_ob,ny_ob))
  hcwp=fill_value; lat=-9999.0; long=-9999.0; top=-99.0; base=-99.0
  phase=-1
  allocate(xloc(nx_ob,ny_ob))
  allocate(yloc(nx_ob,ny_ob))

  CALL read_real_ob(ob_input,lat,"latitude",debug)
  CALL read_real_ob(ob_input,long,"longitude",debug)
  CALL read_real_ob(ob_input,top,"cloud_top_pressure",debug)
  CALL read_real_ob(ob_input,base,"cloud_bottom_pressure",debug)
  CALL read_int_ob(ob_input,phase,"cloud_phase",debug)
  base=base*100; top=top*100

print*,'getting wrf projection.....'
  CALL get_wrf_grid(wrf_input,istatus)
print*,'getting wrf dimensions.....'
  CALL get_dims_wrf(wrf_input,'QCLOUD',dims,ndims,debug)
  nx=dims(1); ny=dims(2); nz=dims(3)
print*,'allocate variables.....'
  ALLOCATE(x(nx))
  ALLOCATE(y(ny))
  ALLOCATE(qc(nx,ny,nz))
  ALLOCATE(qi(nx,ny,nz))
  ALLOCATE(qr(nx,ny,nz))
  ALLOCATE(qs(nx,ny,nz))
  ALLOCATE(qg(nx,ny,nz))
  ALLOCATE(tc(nx,ny,nz))
  ALLOCATE(pres(nx,ny,nz))
print*,'set grid point location.....'
  CALL setgrd(nx,ny,x,y)
print*,'getting wrf variables.....'
  CALL readvar4cwp(wrf_input,nx,ny,nz,pres,tc,qc,qi,qr,qs,qg,debug)
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tc(i,j,k)=tc(i,j,k)*(pres(i,j,k)/1000)**0.286
      END DO
    END DO
  END DO
  DO j=1,ny_ob
    DO i=1,nx_ob
      IF (phase(i,j)<=7 .and. phase(i,j)>0 .and.        &
          phase(i,j)/=3 .and. phase(i,j)/=5) then
        CALL lltoxy(1,1,lat(i,j),long(i,j),xloc(i,j),yloc(i,j))
        IF ( xloc(i,j)>=0 .and. xloc(i,j)<=x(nx) .and.    &
             yloc(i,j)>=0 .and. yloc(i,j)<=y(ny) ) THEN
          a=FLOOR(xloc(i,j)/dx)+1; b=a+1
          c=FLOOR(yloc(i,j)/dy)+1; d=c+1
          delx=x(b)-x(a); dely=y(d)-y(c)
          wgt(1)=(1-(xloc(i,j)-x(a))/delx)*(1-(yloc(i,j)-y(c))/dely)
          wgt(2)=((xloc(i,j)-x(a))/delx)*(1-(yloc(i,j)-y(c))/dely)
          wgt(3)=((xloc(i,j)-x(a))/delx)*((yloc(i,j)-y(c))/dely)
          wgt(4)=(1-(xloc(i,j)-x(a))/delx)*((yloc(i,j)-y(c))/dely)
          DO k=1,nz-1
            lvhgt(1)=wgt(1)*pres(a,c,k)+          &
                     wgt(2)*pres(b,c,k)+          &
                     wgt(3)*pres(b,d,k)+          &
                     wgt(4)*pres(a,d,k)
            lvhgt(2)=wgt(1)*pres(a,c,k+1)+        &
                     wgt(2)*pres(b,c,k+1)+        &
                     wgt(3)*pres(b,d,k+1)+        &
                     wgt(4)*pres(a,d,k+1)
            IF (lvhgt(1)>=base(i,j) .and. lvhgt(2)<=base(i,j)) THEN
              cldbslv=k+1
              EXIT
            ELSE
              cldbslv=-1
              CONTINUE
            END IF
          END DO
          DO k=nz-1,1,-1
            lvhgt(1)=wgt(1)*pres(a,c,k)+          &
                     wgt(2)*pres(b,c,k)+          &
                     wgt(3)*pres(b,d,k)+          &
                     wgt(4)*pres(a,d,k)
            lvhgt(2)=wgt(1)*pres(a,c,k-1)+        &
                     wgt(2)*pres(b,c,k-1)+        &
                     wgt(3)*pres(b,d,k-1)+        &
                     wgt(4)*pres(a,d,k-1)
            IF (lvhgt(1)<=top(i,j) .and. lvhgt(2)>=top(i,j)) THEN
              cldtplv=k-1
              EXIT
            ELSE
              cldtplv=-1
              CONTINUE
            END IF
          END DO
          IF ( cldtplv>=cldbslv .and. cldbslv>0    &
               .and. cldtplv>0 ) THEN
            CALL cwp_forward(nx,ny,nz,a,c,pres,wgt,phase(i,j),      &
                             qc,qi,qr,qs,qg,cldbslv,cldtplv,hcwp(i,j),tc)
            hcwp(i,j)=hcwp(i,j)*1000
          !ELSE
          !  hcwp(i,j)=0
          END IF
          IF (mode=='rms_calc') THEN
            IF (hcwp(i,j)>50000) hcwp(i,j)=0
            IF (obcwp(i,j)>50000) obcwp(i,j)=0
            IF (hcwp(i,j)==0 .and. obcwp(i,j)==0) THEN
              CONTINUE
            ELSE
              rms=rms+(hcwp(i,j)-obcwp(i,j))**2
              ncount=ncount+1
            END IF
          END IF
        END IF
      END IF
    END DO
  END DO

  IF (mode=='rms_calc') THEN
    rms=sqrt(rms)/(ncount-1)
    open(rms_unit,file=rms_file,position='APPEND')
    write(rms_unit,'(I8,F10.2)') iter,rms
    close(rms_unit)
  ELSE IF (mode=='sim_cwp') THEN
    CALL dump_real_var(ob_input, 'SIM_CWP', hcwp, nx_ob, ny_ob, debug)
  END IF

  DEALLOCATE(pres,tc,qc,qi,x,y,hcwp)
  IF (mode=='rms_calc') DEALLOCATE(obcwp)

  print*,"Model CWP program has been finished successfully!"

  END PROGRAM cwp_calc
