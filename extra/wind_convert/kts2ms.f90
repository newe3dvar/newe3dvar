PROGRAM convert
  IMPLICIT NONE
  INTEGER            :: i,j,rdstat
  INTEGER, PARAMETER :: inout_unit=20

  INTEGER :: numsta, nlevs, ilev
  REAL :: lat, lon, elev
  CHARACTER(LEN=10) :: stn

  REAL, ALLOCATABLE, DIMENSION(:) :: hgt, pres, t, dewt, wdir, wspd

  CHARACTER(LEN=40)  :: inout_file

! Main Program

     CALL getarg(1,inout_file)
     OPEN(inout_unit,FILE=trim(inout_file))

     READ(inout_unit,'(i12,i12,f11.4,f15.4,f15.0,5x,a5)',iostat=rdstat) &
          numsta,nlevs,lat,lon,elev,stn

     ALLOCATE(hgt(nlevs))
     ALLOCATE(pres(nlevs))
     ALLOCATE(t(nlevs))
     ALLOCATE(dewt(nlevs))
     ALLOCATE(wdir(nlevs))
     ALLOCATE(wspd(nlevs))

     DO ilev=1,nlevs
       READ(inout_unit,'(i7,3f8.1,i7,f6.0)',iostat=rdstat) hgt(ilev),pres(ilev),t(ilev), &
            dewt(ilev),wdir(ilev),wspd(ilev)
     END DO

     CLOSE(inout_unit)

     wspd=0.5144444*wspd
          

     OPEN(inout_unit,FILE=trim(inout_file))
     WRITE(inout_unit,'(i12,i12,f11.4,f15.4,f15.0,5x,a5)') &
           numsta,nlevs,lat,lon,elev,stn
     DO ilev=1,nlevs
       WRITE(inout_unit,'(i7,3f8.1,i7,f8.2)') hgt(ilev),pres(ilev),t(ilev), &
             dewt(ilev),wdir(ilev),wspd(ilev)
     END DO

   CLOSE(inout_unit)

END PROGRAM convert
