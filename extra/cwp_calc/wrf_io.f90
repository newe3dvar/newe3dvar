  MODULE wrf_io

  USE module_cwp
  USE netcdf_io

  IMPLICIT NONE

  !-------------------------------------------------------------------------------
  !
  ! Variable Delaration for Whole Module
  !
  !-------------------------------------------------------------------------------


  INTEGER                                       :: ndims, nvars, ndimsob
  INTEGER, PARAMETER                            :: num3d = 11
  INTEGER                                       :: time_level
  INTEGER                                       :: i, j, k, l, m, n
  INTEGER                                       :: a, b, c, d

  INTEGER, DIMENSION(4)                         :: dims
  INTEGER, DIMENSION(3)                         :: dims2d, dimsob
  INTEGER, DIMENSION(2)                         :: dims1d
  INTEGER                                       :: dimss

  INTEGER                                       :: proj_wrf
  REAL, DIMENSION(1)                            :: trulat1, trulat2
  REAL, DIMENSION(1)                            :: ctrlat, ctrlon
  REAL                                          :: dx, dy, trulon, sclfct

  CONTAINS

        SUBROUTINE read_real_ob(ob_input,var,var_name,debug)
    
      IMPLICIT NONE
      !-------------------------------------------------------------------------------
      !
      ! Variable Delaration
      !
      !-------------------------------------------------------------------------------
    
      CHARACTER(LEN=512), INTENT(IN)                :: ob_input
      LOGICAL, INTENT(IN)                           :: debug
    
    ! REAL(8), ALLOCATABLE, DIMENSION(:,:,:)        :: utmp, vtmp, wtmp, ptmp
      REAL, INTENT(INOUT)                           :: var(dimsob(1),dimsob(2))
      CHARACTER(LEN=*), INTENT(IN)                  :: var_name
    
      !-------------------------------------------------------------------------------
      !
      ! Get 3D Variables
      !
      !-------------------------------------------------------------------------------
    
    !Get variable
      write(6,*) "Reading variable ", var_name
      call get_real_var_2d(ob_input,var_name, var,  dimsob,  debug)

    END SUBROUTINE read_real_ob

    SUBROUTINE read_int_ob(ob_input,var,var_name,debug)

      IMPLICIT NONE
      !-------------------------------------------------------------------------------
      !
      ! Variable Delaration
      !
      !-------------------------------------------------------------------------------
    
      CHARACTER(LEN=512), INTENT(IN)                :: ob_input
      LOGICAL, INTENT(IN)                           :: debug
    
    ! REAL(8), ALLOCATABLE, DIMENSION(:,:,:)        :: utmp, vtmp, wtmp, ptmp
      INTEGER, INTENT(INOUT)                        :: var(dimsob(1),dimsob(2))
      CHARACTER(LEN=*), INTENT(IN)                  :: var_name
    
      !-------------------------------------------------------------------------------
      !
      ! Get 3D Variables
      !
      !-------------------------------------------------------------------------------
    
    !Get variable
      write(6,*) "Reading variable ", var_name
      call get_dims_ob(ob_input,var_name, dimsob, ndimsob, debug)
      call get_int_var_2d(ob_input,var_name, var,  dimsob,  debug)

    END SUBROUTINE read_int_ob

    SUBROUTINE readvar4cwp(filename,nx,ny,nz,pres,t,qc,qi,debug)
   
      CHARACTER(LEN=256)  :: filename
      INTEGER             :: nx,ny,nz
      REAL, DIMENSION(nx,ny,nz)  :: pres,t,qc,qi
      LOGICAL             :: debug
   
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: pb
   
      ALLOCATE(pb(dims(1),dims(2),dims(3)))
      CALL get_real_var(filename,'P',pres,dims,debug)
      CALL get_real_var(filename,'PB',pb,dims,debug)
      CALL get_real_var(filename,'T',t,dims,debug)
      CALL get_real_var(filename,'QCLOUD',qc,dims,debug)
      CALL get_real_var(filename,'QICE',qi,dims,debug)
   
      pres=pres+pb
      t=t+300.0
   
      DEALLOCATE(pb)
   
    END SUBROUTINE readvar4cwp

    SUBROUTINE get_wrf_grid(filename,istatus)
  
      IMPLICIT NONE
    
      CHARACTER(LEN=*), INTENT(IN)  :: filename
    
      INTEGER, INTENT(OUT) :: istatus
    
    !-----------------------------------------------------------------------
    
      INCLUDE 'netcdf.inc'
    
      INTEGER  :: ncid
    
      REAL(4) :: trlat1in
      REAL(4) :: trlat2in
      REAL(4) :: trlonin
      REAL(4) :: ctrlatin
      REAL(4) :: ctrlonin
      REAL(4) :: dxin
      REAL(4) :: dyin
    
      LOGICAL  :: fexists
    
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    !
    !  Beginning of executable code...
    !
    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    
      INQUIRE(FILE = filename, EXIST = fexists)
      IF (fexists) THEN
        istatus = NF_OPEN(TRIM(filename),NF_NOWRITE,ncid)
        CALL nf_handle_error(istatus,'NF_OPEN in get_wrf_dimension')
      ELSE
        WRITE(6,'(2a)') 'File not found: ', filename
        istatus = -1
        RETURN
      ENDIF
    
      ! Get Map projection attributes from NetCDF file
      istatus = NF_GET_ATT_INT(ncid,NF_GLOBAL,'MAP_PROJ',proj_wrf)
      CALL nf_handle_error(istatus,'NF_GET_ATT_INT in get_wrf_grid')
      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'TRUELAT1',trlat1in)
      CALL nf_handle_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid')
      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'TRUELAT2',trlat2in)
      CALL nf_handle_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid')
      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'STAND_LON',trlonin)  ! ????
      CALL nf_handle_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid')
    
      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'CEN_LAT',ctrlatin)
      CALL nf_handle_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid')
      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'CEN_LON',ctrlonin)
      CALL nf_handle_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid')
    
      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'DX',dxin)
      CALL nf_handle_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid')
      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'DY',dyin)
      CALL nf_handle_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid')
    
      istatus = NF_CLOSE(ncid)
      CALL nf_handle_error(istatus,'NF_CLOSE in get_wrf_dimension')
    
      sclfct = 1.0
    
      trulat1 = trlat1in
      trulat2 = trlat2in
      trulon  = trlonin
      ctrlat = ctrlatin
      ctrlon = ctrlonin
      dx     = dxin
      dy     = dyin
    
      RETURN
    END SUBROUTINE get_wrf_grid

    SUBROUTINE setgrd( nx,ny, x, y )

      USE module_mapproj

      IMPLICIT NONE

      INTEGER       :: nx, ny
      REAL          :: x(nx), y(ny)

      INTEGER       :: i,j
      REAL          :: alatpro(2), sclf, dxscl, dyscl
      REAL, DIMENSION(1)        :: ctrx, ctry, swx, swy
      REAL          :: xsub0, ysub0

      alatpro(1) = trulat1(1)
      alatpro(2) = trulat2(1)

      IF( sclfct /= 1.0) THEN
        sclf  = 1.0/sclfct
        dxscl = dx*sclf
        dyscl = dy*sclf
      ELSE
        sclf  = 1.0
        dxscl = dx
        dyscl = dy
      END IF

      xsub0 = 0; ysub0 = 0

      CALL setmapr( proj_wrf,sclf,alatpro,trulon )
      CALL lltoxy( 1,1, ctrlat,ctrlon, ctrx, ctry )

      swx(1) = ctrx(1) - (REAL((nx-1))/2.) * dxscl
      swy(1) = ctry(1) - (REAL((ny-1))/2.) * dyscl

      CALL setorig( 1, swx, swy)

      DO i=1,nx
        x(i) = sclf*xsub0 + dxscl * (i-1)
      END DO

      DO j=1,ny
        y(j) = sclf*ysub0 + dyscl * (j-1)
      END DO

      CALL setcornerll(nx,ny,x,y,dx,dy)

      RETURN
    END SUBROUTINE setgrd

  END MODULE wrf_io
