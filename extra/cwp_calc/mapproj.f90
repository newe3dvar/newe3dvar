MODULE module_mapproj

  IMPLICIT NONE
  INTEGER :: jproj,jpole
  REAL    :: trulat(2),rota,scmap,xorig,yorig
  REAL    :: projc1,projc2,projc3,projc4,projc5
  REAL    :: d2rad,r2deg,eradius
  PARAMETER (d2rad=3.141592654/180., r2deg=180./3.141592654,  &
             eradius = 6371000. )  ! mean earth radius in m

  REAL, DIMENSION(1,1)    :: swlats,swlons,swlatu,swlonu,swlatv,swlonv
  REAL, DIMENSION(1,1)    :: nelats,nelons,nelatu,nelonu,nelatv,nelonv
  
  CONTAINS

  SUBROUTINE setmapr(iproj,scale,latnot,orient)
  
    IMPLICIT NONE
    INTEGER :: iproj
    REAL :: scale                       ! map scale factor
    REAL :: latnot(2)                   ! true latitude (degrees N)
    REAL :: orient                      ! orientation longitude (degrees E)
  
    REAL :: denom1,denom2,denom3
  
    xorig=0.
    yorig=0.
    jproj=IABS(iproj)
    jpole=ISIGN(1,iproj)
  
    IF ( jproj == 0 ) THEN
      WRITE(6,'(a)') '  No map projection will be used.'
  
    ELSE IF( jproj == 2 ) THEN
      trulat(1)=latnot(1)
      rota=orient
      scmap=scale
      projc1=scale*eradius
      projc2=(1. + SIN(d2rad*jpole*trulat(1)) )
      projc3=projc1*projc2
      IF(jpole > 0) THEN
        WRITE(6,'(a/,a)')  &
            '  Map projection set to Polar Stereographic',                &
            '  X origin, Y origin set to 0.,0. at the North Pole.'
      ELSE
        WRITE(6,'(a/,a)')  &
            '  Map projection set to Polar Stereographic',                &
            '  X origin, Y origin set to 0.,0. at the South Pole.'
      END IF
  
    ELSE IF( jproj == 1 ) THEN
      trulat(1)=latnot(1)
      trulat(2)=latnot(2)
      rota=orient
      scmap=scale
      projc2=COS(d2rad*trulat(1))
      projc3=TAN(d2rad*(45.-0.5*jpole*trulat(1)))
      denom1=COS(d2rad*trulat(2))
      denom2=TAN(d2rad*(45.-0.5*jpole*trulat(2)))
      IF (ABS(trulat(1)-trulat(2)) > 0.01 .AND. denom2 /= 0.) THEN
        denom3=ALOG( projc3/denom2 )
      ELSE
        denom3=0.
      END IF
      IF(denom1 /= 0 .AND. denom3 /= 0.) THEN
        projc4=ALOG( projc2/denom1 ) / denom3
        IF( projc4 < 0.) THEN
          WRITE(6,'(a/,a,f9.2,a,f9.2,/a)')  &
              '  Warning in SETMAPR for Lambert Projection',              &
              '  For the true latitudes provided, ',                      &
              trulat(1),' and ',trulat(2),                                &
              '  projection must be from opposite pole...changing pole.'
          jpole=-jpole
          projc3=TAN(d2rad*(45.-0.5*jpole*trulat(1)) )
          denom2=TAN(d2rad*(45.-0.5*jpole*trulat(2)))
          IF(denom2 /= 0.) THEN
            denom3=ALOG( projc3/denom2 )
          ELSE
            denom3=0.
          END IF
          IF(denom1 /= 0 .AND. denom3 /= 0.) THEN
            projc4=ALOG( projc2/denom1 ) / denom3
          ELSE
            WRITE(6,'(a/,a,f9.2,a,f9.2)')  &
                '  Error (1) in SETMAPR for Lambert Projection',          &
                '  Illegal combination of trulats one: ',                 &
                trulat(1),' and two: ',trulat(2)
            stop
          END IF
        END IF
        projc1=scale*eradius/projc4
      ELSE IF(denom3 == 0. .AND. denom2 /= 0.) THEN
        projc4=SIN(d2rad*jpole*trulat(1))
        IF( projc4 < 0.) THEN
          WRITE(6,'(a/,a,f9.2,a,f9.2,/a)')  &
              '  Warning in SETMAPR for Lambert Projection',              &
              '  For the true latitudes provided, ',                      &
              trulat(1),' and ',trulat(2),                                &
              '  projection must be from opposite pole...changing pole.'
          jpole=-jpole
          projc4=SIN(d2rad*jpole*trulat(1))
        END IF
        projc1=scale*eradius/projc4
      ELSE
        WRITE(6,'(a/,a,f9.2,a,f9.2)')  &
            '  Error (1) in SETMAPR for Lambert Projection',              &
            '  Illegal combination of trulats one: ',                     &
            trulat(1),' and two: ',trulat(2)
        stop
      END IF
  
      IF(jpole > 0) THEN
  !     IF (myproc == 0) WRITE(6,'(a/,a)')  &
  !         '  Map projection set to Lambert Conformal',                  &
  !         '  X origin, Y origin set to 0.,0. at the North Pole.'
      ELSE
        WRITE(6,'(a/,a)')  &
            '  Map projection set to Lambert Conformal',                  &
            '  X origin, Y origin set to 0.,0. at the South Pole.'
      END IF
  
    ELSE IF( jproj == 3 ) THEN
      trulat(1)=latnot(1)
      rota=orient
      scmap=scale
      projc1=scale*eradius
      projc2=COS(d2rad*trulat(1))
      projc3=projc1*projc2
      IF(projc2 <= 0.) THEN
        WRITE(6,'(a/,a,f9.2,a,f9.2)')  &
            '  Error (1) in SETMAPR for Mercator Projection',             &
            '  Illegal true latitude provided: ',trulat(1)
        stop
      END IF
      WRITE(6,'(a/,a,f6.1/,a)')  &
          '  Map projection set to Mercator',                             &
          '  X origin, Y origin set to 0.,0. at the equator,',rota,       &
          '  Y positive toward the North Pole.'
  
    ELSE IF( jproj == 6 ) THEN
      trulat(1)=latnot(1)
      rota=orient
      scmap=scale
      projc1=scale*eradius
      projc2=COS(d2rad*trulat(1))
      IF(projc2 <= 0.) THEN
        WRITE(6,'(a/,a,f9.2,a,f9.2)')  &
            '  Error (1) in SETMAPR for Lat,Lon Projection',              &
            '  Illegal true latitude provided: ',trulat(1)
        stop
      END IF
      projc3=projc1*projc2/d2rad
      WRITE(6,'(a/,a,/a)')  &
          '  Map projection set to Lat, Lon',                             &
          '  X origin, Y origin set to 0.,0. at the equator, 0. long',    &
          '  Y positive toward the North Pole.'
    ELSE
      WRITE(6,'(i4,a)') iproj,' projection is not supported'
      stop
    END IF
  
    RETURN
  END SUBROUTINE setmapr
  
  SUBROUTINE getmapr(iproj,scale,latnot,orient,x0,y0)
  
    IMPLICIT NONE
    INTEGER :: iproj       ! map projection number
    REAL :: scale          ! map scale factor
    REAL :: latnot(2)      ! true latitude (degrees N)
    REAL :: orient         ! orientation longitude (degrees E)
    REAL :: x0             ! x coordinate of origin
    REAL :: y0             ! y coordinate of origin
  
    iproj=jproj*jpole
    scale=scmap
    latnot(1)=trulat(1)
    latnot(2)=trulat(2)
    orient=rota
    x0=xorig
    y0=yorig
    RETURN
  END SUBROUTINE getmapr
  
  
  SUBROUTINE setorig(iopt,x0,y0)
  
    IMPLICIT NONE
    INTEGER :: iopt       ! origin setting option
    REAL :: x0(1),y0(1)   ! first coordinate of origin
    REAL :: xo(1),yo(1)
  
    REAL :: xnew(1,1),ynew(1,1),rlat(1,1),rlon(1,1)
  
    IF( iopt == 1 ) THEN
      xorig=x0(1)
      yorig=y0(1)
      xo=0.0;yo=0.0
      CALL xytoll(1,1,xo,yo,rlat,rlon)
  
  
    ELSE IF( iopt == 2 ) THEN
      xorig=0.
      yorig=0.
      CALL lltoxy(1,1,x0,y0,xnew,ynew)
      xorig=xnew(1,1)
      yorig=ynew(1,1)
    ELSE
      xo=0.0;yo=0.0
      CALL xytoll(1,1,xo,yo,rlat,rlon)
      WRITE(6,'(/a,i4,a,/a,f16.2,f16.2,/a,f16.2,f16.2)') &
          ' Setorig option ',iopt,' not supported.',                      &
          '    Coordinate origin unchanged at x,y =',xorig,yorig,         &
          '    Latitude, longitude= ',rlat,rlon
    END IF
    RETURN
  END SUBROUTINE setorig
  
  SUBROUTINE xytoll(idim,jdim,x,y,rlat,rlon)
  
    IMPLICIT NONE
    INTEGER :: idim,jdim
    REAL :: x(idim),y(jdim),rlat(idim,jdim),rlon(idim,jdim)
  
    INTEGER :: i,j
    REAL :: xabs,yabs,yjp
    REAL :: radius,ratio,dlon
  
  
    IF ( jproj == 0 ) THEN
      ratio=r2deg/eradius
      DO j = 1, jdim
        DO i = 1, idim
          rlat(i,j) = ratio*(y(j)+yorig)
          rlon(i,j) = ratio*(x(i)+xorig)
        END DO
      END DO
  
    ELSE IF( jproj == 2 ) THEN
      DO j=1,jdim
        DO i=1,idim
          yabs=y(j)+yorig
          xabs=x(i)+xorig
          radius=SQRT( xabs*xabs + yabs*yabs )/projc3
          rlat(i,j) = jpole*(90. - 2.*r2deg*ATAN(radius))
          rlat(i,j)=AMIN1(rlat(i,j), 90.)
          rlat(i,j)=AMAX1(rlat(i,j),-90.)
  
          IF((jpole*yabs) > 0.) THEN
            dlon=180. + r2deg*ATAN(-xabs/yabs)
          ELSE IF((jpole*yabs) < 0.) THEN
            dlon=r2deg*ATAN(-xabs/yabs)
          ELSE IF (xabs > 0.) THEN     ! y=0.
            dlon=90.
          ELSE
            dlon=-90.
          END IF
          rlon(i,j)= rota + jpole*dlon
          IF(rlon(i,j) > 180) rlon(i,j)=rlon(i,j)-360.
          IF(rlon(i,j) < -180) rlon(i,j)=rlon(i,j)+360.
          rlon(i,j)=AMIN1(rlon(i,j), 180.)
          rlon(i,j)=AMAX1(rlon(i,j),-180.)
  !
        END DO
      END DO
  
    ELSE IF ( jproj == 1 ) THEN
      DO j=1,jdim
        DO i=1,idim
          yabs=y(j)+yorig
          xabs=x(i)+xorig
          radius=SQRT( xabs*xabs+ yabs*yabs )
          ratio=projc3*((radius/(projc1*projc2))**(1./projc4))
          rlat(i,j)=jpole*(90. -2.*r2deg*(ATAN(ratio)))
          rlat(i,j)=AMIN1(rlat(i,j), 90.)
          rlat(i,j)=AMAX1(rlat(i,j),-90.)
  
          yjp=jpole*yabs
          IF(yjp > 0.) THEN
            dlon=180. + r2deg*ATAN(-xabs/yabs)/projc4
          ELSE IF(yjp < 0.) THEN
            dlon=r2deg*ATAN(-xabs/yabs)/projc4
          ELSE IF (xabs > 0.) THEN     ! y=0.
            dlon=90./projc4
          ELSE
            dlon=-90./projc4
          END IF
          rlon(i,j)= rota + jpole*dlon
          IF(rlon(i,j) > 180) rlon(i,j)=rlon(i,j)-360.
          IF(rlon(i,j) < -180) rlon(i,j)=rlon(i,j)+360.
          rlon(i,j)=AMIN1(rlon(i,j), 180.)
          rlon(i,j)=AMAX1(rlon(i,j),-180.)
  
        END DO
      END DO
  
    ELSE IF( jproj == 3 ) THEN
      DO j=1,jdim
        DO i=1,idim
          yabs=y(j)+yorig
          xabs=x(i)+xorig
          rlat(i,j)=(90. - 2.*r2deg*ATAN(EXP(-yabs/projc3)))
          rlat(i,j)=AMIN1(rlat(i,j), 90.)
          rlat(i,j)=AMAX1(rlat(i,j),-90.)
          dlon=r2deg*(xabs/projc3)
          rlon(i,j)=rota + dlon
          IF(rlon(i,j) > 180) rlon(i,j)=rlon(i,j)-360.
          IF(rlon(i,j) < -180) rlon(i,j)=rlon(i,j)+360.
        END DO
      END DO
  
    ELSE IF( jproj == 6 ) THEN
      DO j=1,jdim
        DO i=1,idim
          rlon(i,j)=x(i)+xorig
          rlat(i,j)=y(j)+yorig
        END DO
      END DO
  
    ELSE
      WRITE(6,'(i4,a)') jproj,' projection is not supported'
      stop
    END IF
  
    RETURN
  END SUBROUTINE xytoll
  
  SUBROUTINE lltoxy(idim,jdim,rlat,rlon,xloc,yloc)
  
    IMPLICIT NONE
  
    INTEGER :: idim,jdim
    REAL :: rlat(idim,jdim),rlon(idim,jdim)
    REAL :: xloc(idim,jdim),yloc(idim,jdim)
  
    REAL :: tem, lat
    INTEGER :: i,j
    REAL :: radius,denom,dlon,ratio
  
    IF( jproj == 0 ) THEN
      ratio=d2rad*eradius
      DO j = 1, jdim
        DO i = 1, idim
          xloc(i,j) = ratio*rlon(i,j) - xorig
          yloc(i,j) = ratio*rlat(i,j) - yorig
        END DO
      END DO
  
    ELSE IF( jproj == 2 ) THEN
      DO j=1,jdim
        DO i=1,idim
          denom=(1. + SIN(d2rad*jpole*rlat(i,j)))
          IF(denom == 0.) denom=1.0E-10
          radius=jpole*projc3*COS(d2rad*rlat(i,j))/denom
          dlon=jpole*d2rad*(rlon(i,j)-rota)
          xloc(i,j)= radius*SIN(dlon) - xorig
          yloc(i,j)=-radius*COS(dlon) - yorig
        END DO
      END DO
  
    ELSE IF( jproj == 1 ) THEN
  
      DO j=1,jdim
        DO i=1,idim
  
          IF (jpole*rlat(i,j) < -89.9) THEN
            lat = -89.9 * jpole
          ELSE
            lat = rlat(i,j)
          END IF
  
          radius=projc1*projc2                                            &
                *(TAN(d2rad*(45.-0.5*jpole*lat))/projc3)**projc4
          tem = rlon(i,j)-rota
          IF( tem < -180.0) tem = 360.0+tem
          IF( tem > 180.0) tem = tem-360.0
          dlon=projc4*d2rad*tem
          xloc(i,j)=       radius*SIN(dlon) - xorig
          yloc(i,j)=-jpole*radius*COS(dlon) - yorig
        END DO
      END DO
  
    ELSE IF(jproj == 3) THEN
      DO j=1,jdim
        DO i=1,idim
          dlon=rlon(i,j)-rota
          IF(dlon < -180.) dlon=dlon+360.
          IF(dlon > 180.) dlon=dlon-360.
          xloc(i,j)=projc3*d2rad*dlon - xorig
          denom=TAN(d2rad*(45. - 0.5*rlat(i,j)))
          IF( denom <= 0. ) denom=1.0E-10
          yloc(i,j)=-projc3*ALOG(denom) - yorig
        END DO
      END DO
  
    ELSE IF(jproj == 6) THEN
      DO j=1,jdim
        DO i=1,idim
          xloc(i,j)=rlon(i,j)-xorig
          if (xloc(i,j) < -180.) xloc(i,j)=xloc(i,j)+360.
          if (xloc(i,j) >  180.) xloc(i,j)=xloc(i,j)-360.
          yloc(i,j)=rlat(i,j)-yorig
        END DO
      END DO
  
    ELSE
      WRITE(6,'(i4,a)') jproj,' projection is not supported'
      stop
    END IF
    RETURN
  END SUBROUTINE lltoxy
  
  SUBROUTINE setcornerll( nx,ny, x, y , dx, dy)
  
    IMPLICIT NONE
  
    INTEGER :: nx, ny
    REAL    :: x(nx), y(ny), dx, dy
  
    REAL, DIMENSION(1) :: tema1, tema2, temb1, temb2
  
    tema1 = 0.5*(x(1)+x(2))
    tema2 = 0.5*(x(nx-1)+x(nx)) + dx  ! The nx'th scalar point
  
    temb1 = 0.5*(y(1)+y(2))
    temb2 = 0.5*(y(ny-1)+y(ny)) + dy  ! The ny'th scalar point
  
    CALL xytoll(1,1, tema1,temb1, swlats,swlons) ! for scalar grid
    CALL xytoll(1,1, tema2,temb2, nelats,nelons) ! for scalar grid
    CALL xytoll(1,1, x(1), temb1, swlatu,swlonu) ! for u-wind grid
    CALL xytoll(1,1, x(nx),temb2, nelatu,nelonu) ! for u-wind grid
    CALL xytoll(1,1, tema1,y(1),  swlatv,swlonv) ! for v-wind grid
    CALL xytoll(1,1, tema2,y(ny), nelatv,nelonv) ! for v-wind grid
  
    RETURN
  END SUBROUTINE setcornerll
  
END MODULE module_mapproj
