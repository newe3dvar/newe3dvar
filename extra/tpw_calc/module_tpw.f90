MODULE module_tpw

  IMPLICIT NONE
  !-------------------------------------------------------------------------------
  !
  ! Variable Delaration
  !
  !-------------------------------------------------------------------------------
  CHARACTER(LEN=512)                    :: wrf_input, ob_input, bias_file, rms_file, output_file, mode
  INTEGER                               :: rms_unit, derived_unit
  LOGICAL                               :: debug
  INTEGER                               :: iter, istatus, ncount
  REAL                                  :: rmslpw, rmsmpw, rmshpw, rmstsfc
  REAL                                  :: biaslpw, biasmpw, biashpw, biastsfc

  INTEGER, PARAMETER                    :: namelist_unit=11

  namelist /ctrl_para/ wrf_input, ob_input, rms_file, output_file, mode, iter, debug

  CONTAINS

  SUBROUTINE namelist_read
  
    !------------------------------------------------------------------
    ! Set default parameters
    !------------------------------------------------------------------
  
    wrf_input             = 'wrfinput_d01'
    ob_input              = 'obwrf'
    rms_file              = 'rms_output'
    output_file           = 'derived_tpw'
    mode                  = 'model_space'
    iter  = -1
    debug =.FALSE.
  
    istatus = 0
  
    !------------------------------------------------------------------
    ! Read namelist
    !------------------------------------------------------------------
  
    open (unit = namelist_unit, file = "namelist.tpw", &
            status = 'old', form = 'formatted', iostat = istatus)
  
    if ( istatus /=0 ) then
       write (unit=6, fmt=*) 'Error to open namelist file: namelist.ptb. Stopped.'
       stop
    else
       !----------------------------------------------------------------------------
       ! Read control parameters
       !----------------------------------------------------------------------------
       read (unit=namelist_unit, nml = ctrl_para, iostat = istatus)
  
       if ( istatus /= 0 ) then
          write (unit=6, fmt=*) 'Error to read namelist ctrl_para. Stopped.'
          stop
       end if

       if ( iter <= 0 ) then
          write (unit=6, fmt=*) 'Iteration number is not properly set.'
          stop
       end if
  
       write (unit=6, fmt='(a)') 'Read ctrl_para namelist successfully.'
       write (unit=6, fmt='(2a)') &
              'wrf_input                =', trim(wrf_input)
       write (unit=6, fmt='(2a)') &
              'ob_input                 =', trim(ob_input)
       if ( mode == 'model_space' ) then
         write (unit=6, fmt='(a)') 'Do calculations over the model space.'
       else if ( mode == 'obs_space' ) then
         write (unit=6, fmt='(a)') 'Do calculations over the observation space.'
       else
         write (unit=6, fmt='(a)') 'Unrecognized spacing option. Set to default.'
         mode = 'model_space'
       end if
       write (unit=6, fmt='(a,L13)') &
              'debug                    =', debug
  
    end if
  
    close (unit = namelist_unit)

  END SUBROUTINE namelist_read
  
  SUBROUTINE pw_forward(nx,ny,nz,iptin,jptin,p09,p07,p03,   &
                        ml_pwll,ml_pwlm,ml_pwlh,            &
                        interp_ll,interp_lm,interp_lh,      &
                        wgt,p,qv,vp,hpwll,hpwlm,hpwlh)

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nx,ny,nz,iptin,jptin
    REAL(8), INTENT(IN)  :: p09,p07,p03,wgt(4)
    INTEGER, INTENT(IN)  :: ml_pwll(4),ml_pwlm(4),ml_pwlh(4)
    REAL(8), INTENT(IN)  :: interp_ll(4),interp_lm(4),interp_lh(4)
    REAL,    INTENT(IN)  :: p(nx,ny,nz),qv(nx,ny,nz)
    REAL(8), INTENT(OUT) :: hpwll,hpwlm,hpwlh

    REAL,    INTENT(OUT) :: vp(nx,ny,nz)

  !---------------------------------------------------------------------

    INTEGER :: k,n
    INTEGER :: ipt(4),jpt(4)

    REAL(8) :: tmp1(4),tmp2(4),tmp3(4)
    REAL(8) :: q09(4),q07(4)

    REAL(8), PARAMETER      :: rhowater=1000.0, gravity=9.81

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ipt(1) = iptin
    jpt(1) = jptin
    ipt(2) = iptin+1
    jpt(2) = jptin
    ipt(3) = iptin+1
    jpt(3) = jptin+1
    ipt(4) = iptin
    jpt(4) = jptin+1

    tmp1=0.
    tmp2=0.
    tmp3=0.
    q09=0.
    q07=0.

    vp(:,:,:)=qv(:,:,:)

    DO n=1,4
      DO k=1,ml_pwll(n)
        IF (k==ml_pwll(n)) THEN
          tmp1(n)=tmp1(n)+vp(ipt(n),jpt(n),k)*(p09-p(ipt(n),jpt(n),k))
        ELSE
          tmp1(n)=tmp1(n)+vp(ipt(n),jpt(n),k)*(p(ipt(n),jpt(n),k+1)-p(ipt(n),jpt(n),k))
        END IF
      END DO
      q09(n)=vp(ipt(n),jpt(n),ml_pwll(n))+      &
             (vp(ipt(n),jpt(n),ml_pwll(n)+1)-vp(ipt(n),jpt(n),ml_pwll(n)))*interp_ll(n)
      DO k=ml_pwll(n),ml_pwlm(n)
        IF (k==ml_pwll(n)) THEN
          tmp2(n)=tmp2(n)+q09(n)*(p(ipt(n),jpt(n),k+1)-p09)
        ELSE IF (k==ml_pwlm(n)) THEN
          tmp2(n)=tmp2(n)+vp(ipt(n),jpt(n),k)*(p07-p(ipt(n),jpt(n),k))
        ELSE
          tmp2(n)=tmp2(n)+vp(ipt(n),jpt(n),k)*(p(ipt(n),jpt(n),k+1)-p(ipt(n),jpt(n),k))
        END IF
      END DO
      q07(n)=vp(ipt(n),jpt(n),ml_pwlm(n))+      &
             (vp(ipt(n),jpt(n),ml_pwlm(n)+1)-vp(ipt(n),jpt(n),ml_pwlm(n)))*interp_lm(n)
      DO k=ml_pwlm(n),ml_pwlh(n)
        IF (k==ml_pwlm(n)) THEN
          tmp3(n)=tmp3(n)+q07(n)*(p(ipt(n),jpt(n),k+1)-p07)
        ELSE IF (k==ml_pwlh(n)) THEN
          tmp3(n)=tmp3(n)+vp(ipt(n),jpt(n),k)*(p03-p(ipt(n),jpt(n),k))
        ELSE
          tmp3(n)=tmp3(n)+vp(ipt(n),jpt(n),k)*(p(ipt(n),jpt(n),k+1)-p(ipt(n),jpt(n),k))
        END IF
      END DO
    END DO
    tmp1=-1*tmp1*100/(rhowater*gravity)  !*100 is the convertion from m to cm
    tmp2=-1*tmp2*100/(rhowater*gravity)
    tmp3=-1*tmp3*100/(rhowater*gravity)
    hpwll=wgt(1)*tmp1(1)+wgt(2)*tmp1(2)+      &
          wgt(3)*tmp1(3)+wgt(4)*tmp1(4)
    hpwlm=wgt(1)*tmp2(1)+wgt(2)*tmp2(2)+      &
          wgt(3)*tmp2(3)+wgt(4)*tmp2(4)
    hpwlh=wgt(1)*tmp3(1)+wgt(2)*tmp3(2)+      &
          wgt(3)*tmp3(3)+wgt(4)*tmp3(4)

    RETURN
  END SUBROUTINE pw_forward

END MODULE module_tpw
