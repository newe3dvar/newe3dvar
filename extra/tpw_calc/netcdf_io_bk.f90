  MODULE netcdf_io

  IMPLICIT NONE

  INCLUDE "netcdf.inc"
  REAL, PARAMETER  :: fill_value=nf_fill_double

  CONTAINS

    SUBROUTINE get_dims_wrf(file, var, dims, ndims, debug)

  !----------------------------------------------------------
  ! Purpose:  getting dimension of netcdf 
  !----------------------------------------------------------

  CHARACTER (LEN=*),  INTENT(IN)  :: file
  CHARACTER (LEN=*),  INTENT(IN)  :: var
  LOGICAL,            INTENT(IN)  :: debug
  INTEGER,            INTENT(OUT) :: dims(4)
  INTEGER,            INTENT(OUT) :: ndims

  INTEGER            :: cdfid, rcode, var_id 
  CHARACTER (LEN=80) :: varnam
  INTEGER            :: natts, dimids(10)
  INTEGER            :: i, ivtype

  !Main for getting dimension
  cdfid = ncopn(file, NCNOWRIT, rcode)

  if (rcode == 0) then
     if (debug) write(unit=6,fmt=*) ' open netcdf file ', trim(file)
  else
     write(unit=6,fmt=*) ' error openiing netcdf file ', trim(file)
     stop
  end if

  var_id = ncvid(cdfid, var, rcode)
  rcode = nf_inq_var(cdfid, var_id, varnam, ivtype, ndims, dimids, natts)
  if (debug) then
     write(unit=6,fmt=*) ' number of dims for ',var,' ',ndims
  end if
  do i=1,ndims
     rcode = nf_inq_dimlen(cdfid, dimids(i), dims(i))
     if (debug) write(unit=6,fmt=*) ' dimension ',i,dims(i)
  end do

  call ncclos(cdfid,rcode)

    END SUBROUTINE get_dims_wrf

    SUBROUTINE get_dims_wrf_2d(file, var, dims, ndims, debug)

  !----------------------------------------------------------
  ! Purpose:  getting dimension of netcdf
  !----------------------------------------------------------

  CHARACTER (LEN=*),  INTENT(IN)  :: file
  CHARACTER (LEN=*),  INTENT(IN)  :: var
  LOGICAL,            INTENT(IN)  :: debug
  INTEGER,            INTENT(OUT) :: dims(3)
  INTEGER,            INTENT(OUT) :: ndims

  INTEGER            :: cdfid, rcode, var_id
  CHARACTER (LEN=80) :: varnam
  INTEGER            :: natts, dimids(10)
  INTEGER            :: i, ivtype

  !Main for getting dimension
  cdfid = ncopn(file, NCNOWRIT, rcode)

  if (rcode == 0) then
     if (debug) write(unit=6,fmt=*) ' open netcdf file ', trim(file)
  else
     write(unit=6,fmt=*) ' error openiing netcdf file ', trim(file)
     stop
  end if

  var_id = ncvid(cdfid, var, rcode)
  rcode = nf_inq_var(cdfid, var_id, varnam, ivtype, ndims, dimids, natts)
  if (debug) then
     write(unit=6,fmt=*) ' number of dims for ',var,' ',ndims
  end if
  do i=1,ndims
     rcode = nf_inq_dimlen(cdfid, dimids(i), dims(i))
     if (debug) write(unit=6,fmt=*) ' dimension ',i,dims(i)
  end do

  call ncclos(cdfid,rcode)

    END SUBROUTINE get_dims_wrf_2d

    SUBROUTINE get_dims_wrf_1d(file, var, dims, ndims, debug)

  !----------------------------------------------------------
  ! Purpose:  getting dimension of netcdf
  !----------------------------------------------------------

  CHARACTER (LEN=*),  INTENT(IN)  :: file
  CHARACTER (LEN=*),  INTENT(IN)  :: var
  LOGICAL,            INTENT(IN)  :: debug
  INTEGER,            INTENT(OUT) :: dims(2)
  INTEGER,            INTENT(OUT) :: ndims

  INTEGER            :: cdfid, rcode, var_id
  CHARACTER (LEN=80) :: varnam
  INTEGER            :: natts, dimids(10)
  INTEGER            :: i, ivtype

  !Main for getting dimension
  cdfid = ncopn(file, NCNOWRIT, rcode)

  if (rcode == 0) then
     if (debug) write(unit=6,fmt=*) ' open netcdf file ', trim(file)
  else
     write(unit=6,fmt=*) ' error openiing netcdf file ', trim(file)
     stop
  end if

  var_id = ncvid(cdfid, var, rcode)
  rcode = nf_inq_var(cdfid, var_id, varnam, ivtype, ndims, dimids, natts)
  if (debug) then
     write(unit=6,fmt=*) ' number of dims for ',var,' ',ndims
  end if
  do i=1,ndims
     rcode = nf_inq_dimlen(cdfid, dimids(i), dims(i))
     if (debug) write(unit=6,fmt=*) ' dimension ',i,dims(i)
  end do

  call ncclos(cdfid,rcode)

    END SUBROUTINE get_dims_wrf_1d

    SUBROUTINE get_dims_wrf_scalar(file, var, dims, ndims, debug)

  !----------------------------------------------------------
  ! Purpose:  getting dimension of netcdf
  !----------------------------------------------------------

  CHARACTER (LEN=*),  INTENT(IN)  :: file
  CHARACTER (LEN=*),  INTENT(IN)  :: var
  LOGICAL,            INTENT(IN)  :: debug
  INTEGER,            INTENT(OUT) :: dims
  INTEGER,            INTENT(OUT) :: ndims

  INTEGER            :: cdfid, rcode, var_id
  CHARACTER (LEN=80) :: varnam
  INTEGER            :: natts, dimids(10)
  INTEGER            :: i, ivtype

  !Main for getting dimension
  cdfid = ncopn(file, NCNOWRIT, rcode)

  if (rcode == 0) then
     if (debug) write(unit=6,fmt=*) ' open netcdf file ', trim(file)
  else
     write(unit=6,fmt=*) ' error openiing netcdf file ', trim(file)
     stop
  end if

  var_id = ncvid(cdfid, var, rcode)
  rcode = nf_inq_var(cdfid, var_id, varnam, ivtype, ndims, dimids, natts)
  if (debug) then
     write(unit=6,fmt=*) ' number of dims for ',var,' ',ndims
  end if
  do i=1,ndims
     rcode = nf_inq_dimlen(cdfid, dimids(i), dims)
     if (debug) write(unit=6,fmt=*) ' dimension ',i,dims
  end do

  call ncclos(cdfid,rcode)

    END SUBROUTINE get_dims_wrf_scalar

    SUBROUTINE get_real_var(filen, var, rdata, dims, debug)

  !----------------------------------------------------------
  ! Purpose:  getting variables of wrf
  !----------------------------------------------------------     

  CHARACTER (LEN=*),  INTENT(IN)  :: filen
  CHARACTER (LEN=40), INTENT(IN)  :: var
  INTEGER,            INTENT(IN)  :: dims(4)
  REAL,               INTENT(OUT) :: rdata(dims(1),dims(2),dims(3))
  LOGICAL,            INTENT(IN)  :: debug

  INTEGER                         :: i1, i2, i3, time
  REAL(8) :: tmp(dims(1),dims(2),dims(3))
  REAL(4) :: tmp4(dims(1),dims(2),dims(3))

  CHARACTER (LEN=80) :: varnam

  INTEGER :: cdfid, rcode, var_id
  INTEGER :: ndims, natts, idims(10), istart(10),iend(10), dimids(10)
  INTEGER :: i, ivtype

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2

print *, filen
print *, var
print*,dims
  i1=dims(1); i2=dims(2); i3=dims(3); time=dims(4)
  if ( time /= 0 ) time=1

  cdfid = ncopn(filen, NCNOWRIT, rcode)

  if (rcode /= 0) then
     write(unit=6,fmt=*) ' error opening netcdf file ', trim(filen)
     stop
  end if

  var_id = ncvid(cdfid, var, rcode)

  rcode = nf_inq_var(cdfid, var_id, varnam, ivtype, ndims, dimids, natts)

  if (debug) then
     write(unit=6, fmt='(3a,i6)') ' get_real_var: dims for ',var,' ',ndims
     write(unit=6, fmt='(a,i6)') ' ivtype=', ivtype
     write(unit=6, fmt='(a, a)') ' varnam=', trim(varnam)
     write(unit=6, fmt='(a,i6)') ' kind(data)=', kind(rdata)
  end if

  do i=1,ndims
     rcode = nf_inq_dimlen(cdfid, dimids(i), idims(i))
     if (debug) write(unit=6, fmt='(a,2i6)') ' dimension ',i,idims(i)
  end do

  ! check the dimensions
  if ((i1 /= idims(1)) .or.  &
      (i2 /= idims(2)) .or.  &
      (i3 /= idims(3)) .or.  &
      (time > idims(4))    )  then

     write(unit=6,fmt=*) ' error in real_var read, dimension problem '
     write(unit=6,fmt=*) i1, idims(1)
     write(unit=6,fmt=*) i2, idims(2)
     write(unit=6,fmt=*) i3, idims(3)
     write(unit=6,fmt=*) time, idims(4)
     write(unit=6,fmt=*) ' error stop '
     stop
  end if

  ! get the data
  istart(1) = 1
  iend(1) = i1
  istart(2) = 1
  iend(2) = i2
  istart(3) = 1
  iend(3) = i3
  istart(4) = time
  iend(4) = 1

  if ((ivtype == NF_real) .and. (kind(rdata) == 4)) then
     call ncvgt(cdfid,var_id,istart,iend,rdata,rcode)
  else if ((ivtype == NF_DOUBLE) .and. (kind(rdata) == 4)) then
     call ncvgt(cdfid,var_id,istart,iend,tmp,rcode)
     rdata = tmp
  else if ((ivtype == NF_DOUBLE) .and. (kind(rdata) == 8)) then
     call ncvgt(cdfid,var_id,istart,iend,rdata,rcode)
  else if ((ivtype == NF_REAL) .and. (kind(rdata) == 8)) then
     call ncvgt(cdfid,var_id,istart,iend,tmp4,rcode)
     rdata = tmp4
  else
     write(unit=6, fmt='(a, i6)') &
        'Unrecognizable ivtype:', ivtype
     stop
  end if

  if (debug) then
     write(unit=6, fmt='(a,e24.12)') ' Sample data=', rdata(1,1,1)
  end if

  call ncclos(cdfid,rcode)

    END SUBROUTINE get_real_vart

    SUBROUTINE get_real_var_2d(file, var, data, dims, debug)

  !----------------------------------------------------------
  ! Purpose:  getting variables of wrf
  !----------------------------------------------------------

  INTEGER,            INTENT(IN)  :: dims(3)
  INTEGER                         :: i1, i2, time
  CHARACTER (LEN=*),  INTENT(IN)  :: file
  LOGICAL,            INTENT(IN)  :: debug
  CHARACTER (LEN=*),  INTENT(IN)  :: var
  REAL,               INTENT(OUT) :: data(dims(1),dims(2))

  REAL(8) :: tmp(dims(1),dims(2))
  REAL(4) :: tmp4(dims(1),dims(2))

  CHARACTER (LEN=80) :: varnam

  INTEGER :: cdfid, rcode, var_id
  INTEGER :: ndims, natts, idims(10), istart(10),iend(10), dimids(10)
  INTEGER :: i, ivtype

  i1=dims(1); i2=dims(2); time=dims(3)
  if ( time /= 0 ) time=1

  cdfid = ncopn(file, NCNOWRIT, rcode)

  if (rcode /= 0) then
     write(unit=6,fmt=*) ' error opening netcdf file ', trim(file)
     stop
  end if

  var_id = ncvid(cdfid, var, rcode)

  rcode = nf_inq_var(cdfid, var_id, varnam, ivtype, ndims, dimids, natts)

  if (debug) then
     write(unit=6, fmt='(3a,i6)') ' get_real_var: dims for ',var,' ',ndims
     write(unit=6, fmt='(a,i6)') ' ivtype=', ivtype
     write(unit=6, fmt='(a, a)') ' varnam=', trim(varnam)
     write(unit=6, fmt='(a,i6)') ' kind(data)=', kind(data)
  end if

  do i=1,ndims
     rcode = nf_inq_dimlen(cdfid, dimids(i), idims(i))
     if (debug) write(unit=6, fmt='(a,2i6)') ' dimension ',i,idims(i)
  end do

  ! check the dimensions
  if ((i1 /= idims(1)) .or.  &
      (i2 /= idims(2)) .or.  &
      (time > idims(3))    )  then

     write(unit=6,fmt=*) ' error in real_var read, dimension problem '
     write(unit=6,fmt=*) i1, idims(1)
     write(unit=6,fmt=*) i2, idims(2)
     write(unit=6,fmt=*) time, idims(3)
     write(unit=6,fmt=*) ' error stop '
     stop
  end if

  ! get the data
  istart(1) = 1
  iend(1) = i1
  istart(2) = 1
  iend(2) = i2
  istart(3) = time
  iend(3) = 1

  if ((ivtype == NF_real) .and. (kind(data) == 4)) then
     call ncvgt(cdfid,var_id,istart,iend,data,rcode)
  else if ((ivtype == NF_DOUBLE) .and. (kind(data) == 4)) then
     call ncvgt(cdfid,var_id,istart,iend,tmp,rcode)
     data = tmp
  else if ((ivtype == NF_DOUBLE) .and. (kind(data) == 8)) then
     call ncvgt(cdfid,var_id,istart,iend,data,rcode)
  else if ((ivtype == NF_REAL) .and. (kind(data) == 8)) then
     call ncvgt(cdfid,var_id,istart,iend,tmp4,rcode)
     data = tmp4
  else
     write(unit=6, fmt='(a, i6)') &
        'Unrecognizable ivtype:', ivtype
     stop
  end if

  if (debug) then
     write(unit=6, fmt='(a,e24.12)') ' Sample data=', data(1,1)
  end if

  call ncclos(cdfid,rcode)

    END SUBROUTINE get_real_var_2d

    SUBROUTINE get_int_var_2d(file, var, data, dims, debug)

  !----------------------------------------------------------
  ! Purpose:  getting variables of wrf
  !----------------------------------------------------------

  INTEGER,            INTENT(IN)  :: dims(3)
  INTEGER                         :: i1, i2, time
  CHARACTER (LEN=*),  INTENT(IN)  :: file
  LOGICAL,            INTENT(IN)  :: debug
  CHARACTER (LEN=*),  INTENT(IN)  :: var
  INTEGER,            INTENT(OUT) :: data(dims(1),dims(2))

  REAL(8) :: tmp(dims(1),dims(2))
  REAL(4) :: tmp4(dims(1),dims(2))

  CHARACTER (LEN=80) :: varnam

  INTEGER :: cdfid, rcode, var_id
  INTEGER :: ndims, natts, idims(10), istart(10),iend(10), dimids(10)
  INTEGER :: i, ivtype

  i1=dims(1); i2=dims(2); time=dims(3)
  if ( time /= 0 ) time=1

  cdfid = ncopn(file, NCNOWRIT, rcode)

  if (rcode /= 0) then
     write(unit=6,fmt=*) ' error opening netcdf file ', trim(file)
     stop
  end if

  var_id = ncvid(cdfid, var, rcode)

  rcode = nf_inq_var(cdfid, var_id, varnam, ivtype, ndims, dimids, natts)

  if (debug) then
     write(unit=6, fmt='(3a,i6)') ' get_real_var: dims for ',var,' ',ndims
     write(unit=6, fmt='(a,i6)') ' ivtype=', ivtype
     write(unit=6, fmt='(a, a)') ' varnam=', trim(varnam)
     write(unit=6, fmt='(a,i6)') ' kind(data)=', kind(data)
  end if

  do i=1,ndims
     rcode = nf_inq_dimlen(cdfid, dimids(i), idims(i))
     if (debug) write(unit=6, fmt='(a,2i6)') ' dimension ',i,idims(i)
  end do

  ! check the dimensions
  if ((i1 /= idims(1)) .or.  &
      (i2 /= idims(2)) .or.  &
      (time > idims(3))    )  then

     write(unit=6,fmt=*) ' error in real_var read, dimension problem '
     write(unit=6,fmt=*) i1, idims(1)
     write(unit=6,fmt=*) i2, idims(2)
     write(unit=6,fmt=*) time, idims(3)
     write(unit=6,fmt=*) ' error stop '
     stop
  end if

  ! get the data
  istart(1) = 1
  iend(1) = i1
  istart(2) = 1
  iend(2) = i2
  istart(3) = time
  iend(3) = 1

  if (ivtype == NF_INT) then
     call ncvgt(cdfid,var_id,istart,iend,data,rcode)
  else
     write(unit=6, fmt='(a, i6)') &
        'Unrecognizable ivtype:', ivtype
     stop
  end if

  if (debug) then
     write(unit=6, fmt='(a,e24.12)') ' Sample data=', data(1,1)
  end if

  call ncclos(cdfid,rcode)

    END SUBROUTINE get_int_var_2d

    SUBROUTINE get_real_var_1d(file, var, data, dims, debug)

  !----------------------------------------------------------
  ! Purpose:  getting variables of wrf
  !----------------------------------------------------------

  INTEGER,            INTENT(IN)  :: dims(2)
  INTEGER                         :: i1, time
  CHARACTER (LEN=*),  INTENT(IN)  :: file
  LOGICAL,            INTENT(IN)  :: debug
  CHARACTER (LEN=*),  INTENT(IN)  :: var
  REAL,               INTENT(OUT) :: data(dims(1))

  REAL(8) :: tmp(dims(1))
  REAL(4) :: tmp4(dims(1))

  CHARACTER (LEN=80) :: varnam

  INTEGER :: cdfid, rcode, var_id
  INTEGER :: ndims, natts, idims(10), istart(10),iend(10), dimids(10)
  INTEGER :: i, ivtype

  i1=dims(1); time=dims(2)
  if ( time /= 0 ) time=1

  cdfid = ncopn(file, NCNOWRIT, rcode)

  if (rcode /= 0) then
     write(unit=6,fmt=*) ' error opening netcdf file ', trim(file)
     stop
  end if

  var_id = ncvid(cdfid, var, rcode)

  rcode = nf_inq_var(cdfid, var_id, varnam, ivtype, ndims, dimids, natts)

  if (debug) then
     write(unit=6, fmt='(3a,i6)') ' get_real_var: dims for ',var,' ',ndims
     write(unit=6, fmt='(a,i6)') ' ivtype=', ivtype
     write(unit=6, fmt='(a, a)') ' varnam=', trim(varnam)
     write(unit=6, fmt='(a,i6)') ' kind(data)=', kind(data)
  end if

  do i=1,ndims
     rcode = nf_inq_dimlen(cdfid, dimids(i), idims(i))
     if (debug) write(unit=6, fmt='(a,2i6)') ' dimension ',i,idims(i)
  end do

  ! check the dimensions
  if ((i1 /= idims(1)) .or.  &
      (time > idims(2))    )  then

     write(unit=6,fmt=*) ' error in real_var read, dimension problem '
     write(unit=6,fmt=*) i1, idims(1)
     write(unit=6,fmt=*) time, idims(2)
     write(unit=6,fmt=*) ' error stop '
     stop
  end if

  ! get the data
  istart(1) = 1
  iend(1) = i1
  istart(2) = time
  iend(2) = 1

  if ((ivtype == NF_real) .and. (kind(data) == 4)) then
     call ncvgt(cdfid,var_id,istart,iend,data,rcode)
  else if ((ivtype == NF_DOUBLE) .and. (kind(data) == 4)) then
     call ncvgt(cdfid,var_id,istart,iend,tmp,rcode)
     data = tmp
  else if ((ivtype == NF_DOUBLE) .and. (kind(data) == 8)) then
     call ncvgt(cdfid,var_id,istart,iend,data,rcode)
  else if ((ivtype == NF_REAL) .and. (kind(data) == 8)) then
     call ncvgt(cdfid,var_id,istart,iend,tmp4,rcode)
     data = tmp4
  else
     write(unit=6, fmt='(a, i6)') &
        'Unrecognizable ivtype:', ivtype
     stop
  end if

  if (debug) then
     write(unit=6, fmt='(a,e24.12)') ' Sample data=', data(1)
  end if

  call ncclos(cdfid,rcode)

    END SUBROUTINE get_real_var_1d

    SUBROUTINE get_real_var_scalar(file, var, data, dims, debug)

  !----------------------------------------------------------
  ! Purpose:  getting variables of wrf
  !----------------------------------------------------------

  INTEGER,            INTENT(IN)  :: dims
  INTEGER                         :: time
  CHARACTER (LEN=*),  INTENT(IN)  :: file
  LOGICAL,            INTENT(IN)  :: debug
  CHARACTER (LEN=*),  INTENT(IN)  :: var
  REAL,               INTENT(OUT) :: data

  REAL(8) :: tmp
  REAL(4) :: tmp4

  CHARACTER (LEN=80) :: varnam

  INTEGER :: cdfid, rcode, var_id
  INTEGER :: ndims, natts, idims(10), istart(10),iend(10), dimids(10)
  INTEGER :: i, ivtype

  time=dims
  if ( time /= 0 ) time=1

  cdfid = ncopn(file, NCNOWRIT, rcode)

  if (rcode /= 0) then
     write(unit=6,fmt=*) ' error opening netcdf file ', trim(file)
     stop
  end if

  var_id = ncvid(cdfid, var, rcode)

  rcode = nf_inq_var(cdfid, var_id, varnam, ivtype, ndims, dimids, natts)

  if (debug) then
     write(unit=6, fmt='(3a,i6)') ' get_real_var: dims for ',var,' ',ndims
     write(unit=6, fmt='(a,i6)') ' ivtype=', ivtype
     write(unit=6, fmt='(a, a)') ' varnam=', trim(varnam)
     write(unit=6, fmt='(a,i6)') ' kind(data)=', kind(data)
  end if

  do i=1,ndims
     rcode = nf_inq_dimlen(cdfid, dimids(i), idims(i))
     if (debug) write(unit=6, fmt='(a,2i6)') ' dimension ',i,idims(i)
  end do

  ! check the dimensions
  if (time > idims(1))  then

     write(unit=6,fmt=*) ' error in real_var read, dimension problem '
     write(unit=6,fmt=*) time, idims(1)
     write(unit=6,fmt=*) ' error stop '
     stop
  end if

  ! get the data
  istart(1) = time
  iend(1) = 1

  if ((ivtype == NF_real) .and. (kind(data) == 4)) then
     call ncvgt(cdfid,var_id,istart,iend,data,rcode)
  else if ((ivtype == NF_DOUBLE) .and. (kind(data) == 4)) then
     call ncvgt(cdfid,var_id,istart,iend,tmp,rcode)
     data = tmp
  else if ((ivtype == NF_DOUBLE) .and. (kind(data) == 8)) then
     call ncvgt(cdfid,var_id,istart,iend,data,rcode)
  else if ((ivtype == NF_REAL) .and. (kind(data) == 8)) then
     call ncvgt(cdfid,var_id,istart,iend,tmp4,rcode)
     data = tmp4
  else
     write(unit=6, fmt='(a, i6)') &
        'Unrecognizable ivtype:', ivtype
     stop
  end if

  if (debug) then
     write(unit=6, fmt='(a,e24.12)') ' Sample data=', data
  end if

  call ncclos(cdfid,rcode)

    END SUBROUTINE get_real_var_scalar

    SUBROUTINE nf_handle_error(ierr,sub_name)

  IMPLICIT NONE
  INTEGER,          INTENT(IN) :: ierr
  CHARACTER(LEN=*), INTENT(IN) :: sub_name
  CHARACTER(LEN=80) :: errmsg

  IF(ierr /= NF_NOERR) THEN
    errmsg = NF_STRERROR(ierr)
    WRITE(6,*) 'NetCDF error: ',errmsg
    WRITE(6,*) 'Program stopped while calling ', sub_name
    !STOP
  END IF

  RETURN
    END SUBROUTINE nf_handle_error

  END MODULE netcdf_io
