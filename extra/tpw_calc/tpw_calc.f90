  PROGRAM tpw_calc

  USE module_mapproj
  USE module_tpw
  !USE netcdf_io, only: get_dims_wrf
  USE wrf_io

  IMPLICIT NONE

  !-------------------------------------------------------------------------------
  !
  ! Include Files / Variable Delaration
  !
  !-------------------------------------------------------------------------------

  REAL, ALLOCATABLE, DIMENSION(:)              :: hpw
  REAL, ALLOCATABLE, DIMENSION(:)              :: lat, long, x, y, xloc, yloc
  REAL, ALLOCATABLE, DIMENSION(:)              :: obpw_ll, obpw_lm, obpw_lh, obtsfc
  REAL(8), ALLOCATABLE, DIMENSION(:)           :: hpwll, hpwlm, hpwlh, htsfc
  REAL, ALLOCATABLE, DIMENSION(:,:,:)          :: qv, pres, tem
  REAL, ALLOCATABLE, DIMENSION(:,:)            :: psfc, tsfc
  

  INTEGER                                      :: nx, ny, nz
  REAL(8)                                      :: wgt(4),delx,dely
  INTEGER, DIMENSION(4)                        :: ml_pwll,ml_pwlm,ml_pwlh
  REAL(8), DIMENSION(4)                        :: interp_ll,interp_lm,interp_lh

  INTEGER                                      :: totobs, countobs
  REAL                                         :: PRO(8)
  REAL(8)                                      :: p09, p07, p03, psfctemp

  !-------------------------------------------------------------------------------
  !
  ! Main Program
  !
  !-------------------------------------------------------------------------------

  CALL namelist_read

rmslpw=0.;rmsmpw=0.;rmshpw=0.;rmstsfc=0.
biaslpw=0.;biasmpw=0.;biashpw=0.;biastsfc=0.
print*,'reading pw observation.....'
  totobs=0
  OPEN(1,FILE=ob_input,access='direct',recl=4*8,convert='little_endian')
  DO I=1,1000000000
    READ(1,rec=I,ERR=1001) PRO
    totobs=totobs+1
  END DO
1001 CONTINUE
  ALLOCATE(lat(totobs))
  ALLOCATE(long(totobs))
  ALLOCATE(obpw_ll(totobs))
  ALLOCATE(obpw_lm(totobs))
  ALLOCATE(obpw_lh(totobs))
  ALLOCATE(obtsfc(totobs))
  ALLOCATE(xloc(totobs))
  ALLOCATE(yloc(totobs))
  ALLOCATE(hpwll(totobs))
  ALLOCATE(hpwlm(totobs))
  ALLOCATE(hpwlh(totobs))
  ALLOCATE(htsfc(totobs))
  DO I=1,totobs
    READ(1,rec=I,ERR=1001) PRO
    lat(I)=DBLE(PRO(1))
    long(I)=PRO(2)
    obpw_ll(I)=PRO(4)
    obpw_lm(I)=PRO(5)
    obpw_lh(I)=PRO(6)
    obtsfc(I)=PRO(7)
  END DO

print*,'getting wrf projection.....'
  CALL get_wrf_grid(wrf_input,istatus)
print*,'getting wrf dimensions.....'
  CALL get_dims_wrf(wrf_input,'QVAPOR',dims,ndims,debug)
  nx=dims(1); ny=dims(2); nz=dims(3)
  CALL get_dims_wrf_2d(wrf_input,'T2',dims2d,ndims,debug)
print*,'allocate variables.....'
  ALLOCATE(x(nx))
  ALLOCATE(y(ny))
  ALLOCATE(qv(nx,ny,nz))
  ALLOCATE(pres(nx,ny,nz))
  ALLOCATE(tsfc(nx,ny))
  ALLOCATE(psfc(nx,ny))
  ALLOCATE(tem(nx,ny,nz))
print*,'set grid point location.....'
  CALL setgrd(nx,ny,x,y)
print*,'getting wrf variables.....'
  CALL readvar4pw(wrf_input,nx,ny,nz,qv,pres,psfc,tsfc,debug)

  DO i=1,totobs
    CALL lltoxy(1,1,lat(i),long(i),xloc(i),yloc(i))
    IF ( xloc(i)>=0 .and. xloc(i)<=x(nx) .and.    &
         yloc(i)>=0 .and. yloc(i)<=y(ny) ) THEN

      a=FLOOR(xloc(i)/dx)+1; b=a+1
      c=FLOOR(yloc(i)/dy)+1; d=c+1
      delx=x(b)-x(a); dely=y(d)-y(c)
      wgt(1)=(1-(xloc(i)-x(a))/delx)*(1-(yloc(i)-y(c))/dely)
      wgt(2)=((xloc(i)-x(a))/delx)*(1-(yloc(i)-y(c))/dely)
      wgt(3)=((xloc(i)-x(a))/delx)*((yloc(i)-y(c))/dely)
      wgt(4)=(1-(xloc(i)-x(a))/delx)*((yloc(i)-y(c))/dely)

      psfctemp=wgt(1)*psfc(a,c)+wgt(2)*psfc(b,c)+          &
               wgt(3)*psfc(b,d)+wgt(4)*psfc(a,d)
      htsfc(i)=wgt(1)*tsfc(a,c)+wgt(2)*tsfc(b,c)+          &
               wgt(3)*tsfc(b,d)+wgt(4)*tsfc(a,d)
      IF(psfctemp>97000.0D0) THEN
        p09=90000.0
      ELSE
        p09=0.005+0.9*(psfctemp-0.005)
      END IF
      IF(psfctemp>90000.0D0)THEN
        p07=70000.0
      ELSE
        p07=0.005+0.7*(psfctemp-0.005)
      END IF
      p03=30000.0
      DO k=1,nz-1
        IF (pres(a,c,k)>p09 .and. pres(a,c,k+1)<p09) ml_pwll(1)=k
        IF (pres(b,c,k)>p09 .and. pres(b,c,k+1)<p09) ml_pwll(2)=k
        IF (pres(b,d,k)>p09 .and. pres(b,d,k+1)<p09) ml_pwll(3)=k
        IF (pres(a,d,k)>p09 .and. pres(a,d,k+1)<p09) ml_pwll(4)=k
        IF (pres(a,c,k)>p07 .and. pres(a,c,k+1)<p07) ml_pwlm(1)=k
        IF (pres(b,c,k)>p07 .and. pres(b,c,k+1)<p07) ml_pwlm(2)=k
        IF (pres(b,d,k)>p07 .and. pres(b,d,k+1)<p07) ml_pwlm(3)=k
        IF (pres(a,d,k)>p07 .and. pres(a,d,k+1)<p07) ml_pwlm(4)=k
        IF (pres(a,c,k)>p03 .and. pres(a,c,k+1)<p03) ml_pwlh(1)=k
        IF (pres(b,c,k)>p03 .and. pres(b,c,k+1)<p03) ml_pwlh(2)=k
        IF (pres(b,d,k)>p03 .and. pres(b,d,k+1)<p03) ml_pwlh(3)=k
        IF (pres(a,d,k)>p03 .and. pres(a,d,k+1)<p03) ml_pwlh(4)=k
      END DO
      interp_ll(1)=LOG(p09/pres(a,c,ml_pwll(1)))/LOG(pres(a,c,ml_pwll(1)+1)/pres(a,c,ml_pwll(1)))
      interp_ll(2)=LOG(p09/pres(b,c,ml_pwll(2)))/LOG(pres(b,c,ml_pwll(2)+1)/pres(b,c,ml_pwll(2)))
      interp_ll(3)=LOG(p09/pres(b,d,ml_pwll(3)))/LOG(pres(b,d,ml_pwll(3)+1)/pres(b,d,ml_pwll(3)))
      interp_ll(4)=LOG(p09/pres(a,d,ml_pwll(4)))/LOG(pres(a,d,ml_pwll(4)+1)/pres(a,d,ml_pwll(4)))
      interp_lm(1)=LOG(p07/pres(a,c,ml_pwlm(1)))/LOG(pres(a,c,ml_pwlm(1)+1)/pres(a,c,ml_pwlm(1)))
      interp_lm(2)=LOG(p07/pres(b,c,ml_pwlm(2)))/LOG(pres(b,c,ml_pwlm(2)+1)/pres(b,c,ml_pwlm(2)))
      interp_lm(3)=LOG(p07/pres(b,d,ml_pwlm(3)))/LOG(pres(b,d,ml_pwlm(3)+1)/pres(b,d,ml_pwlm(3)))
      interp_lm(4)=LOG(p07/pres(a,d,ml_pwlm(4)))/LOG(pres(a,d,ml_pwlm(4)+1)/pres(a,d,ml_pwlm(4)))
      interp_lh(1)=LOG(p03/pres(a,c,ml_pwlh(1)))/LOG(pres(a,c,ml_pwlh(1)+1)/pres(a,c,ml_pwlh(1)))
      interp_lh(2)=LOG(p03/pres(b,c,ml_pwlh(2)))/LOG(pres(b,c,ml_pwlh(2)+1)/pres(b,c,ml_pwlh(2)))
      interp_lh(3)=LOG(p03/pres(b,d,ml_pwlh(3)))/LOG(pres(b,d,ml_pwlh(3)+1)/pres(b,d,ml_pwlh(3)))
      interp_lh(4)=LOG(p03/pres(a,d,ml_pwlh(4)))/LOG(pres(a,d,ml_pwlh(4)+1)/pres(a,d,ml_pwlh(4)))

      CALL pw_forward(nx,ny,nz,a,c,p09,p07,p03,ml_pwll,ml_pwlm,ml_pwlh, &
                      interp_ll,interp_lm,interp_lh,wgt,pres,qv,tem,hpwll(i),hpwlm(i),hpwlh(i))

      rmslpw=rmslpw+(hpwll(i)-obpw_ll(i))**2
      rmsmpw=rmsmpw+(hpwlm(i)-obpw_lm(i))**2
      rmshpw=rmshpw+(hpwlh(i)-obpw_lh(i))**2
      rmstsfc=rmstsfc+(htsfc(i)-obtsfc(i))**2
      biaslpw=biaslpw+hpwll(i)-obpw_ll(i)
      biasmpw=biasmpw+hpwlm(i)-obpw_lm(i)
      biashpw=biashpw+hpwlh(i)-obpw_lh(i)
      biastsfc=biastsfc+htsfc(i)-obtsfc(i)
      ncount=ncount+1
    END IF
  END DO

  print*,'total number of observations is ',totobs
  print*,'total number of observations within domain is ',ncount
  rmslpw=sqrt(rmslpw)/(ncount-1)
  rmsmpw=sqrt(rmsmpw)/(ncount-1)
  rmshpw=sqrt(rmshpw)/(ncount-1)
  rmstsfc=sqrt(rmstsfc)/(ncount-1)
  biaslpw=biaslpw/ncount
  biasmpw=biasmpw/ncount
  biashpw=biashpw/ncount
  biastsfc=biastsfc/ncount
  open(rms_unit,file=rms_file,position='APPEND')
  write(rms_unit,'(I4,8(F10.6,1x))') iter,rmslpw,rmsmpw,rmshpw,rmstsfc,biaslpw,biasmpw,biashpw,biastsfc
  close(rms_unit)

  open(derived_unit,file=output_file,status='REPLACE')
  IF ( mode == 'model_space' ) THEN
    countobs=0
    DO i=1,totobs
      IF ( xloc(i)>=0 .and. xloc(i)<=x(nx) .and.    &
           yloc(i)>=0 .and. yloc(i)<=y(ny) ) THEN
        countobs=countobs+1
        write(derived_unit,'(I5,2(F8.3,1x),8(F10.6,1x))') countobs,lat(i),long(i),hpwll(i),hpwlm(i),hpwlh(i),htsfc(i),obpw_ll(i),obpw_lm(i),obpw_lh(i),obtsfc(i)
      END IF
    END DO
  ELSE IF ( mode == 'obs_space' ) THEN
    DO i=1,totobs
      write(derived_unit,'(I5,2(F8.3,1x),8(F10.6,1x))') i,lat(i),long(i),hpwll(i),hpwlm(i),hpwlh(i),htsfc(i),obpw_ll(i),obpw_lm(i),obpw_lh(i),obtsfc(i)
    END DO
  END IF
  close(derived_unit)

  print*,"Model TPW program has been finished successfully!"

  END PROGRAM tpw_calc
