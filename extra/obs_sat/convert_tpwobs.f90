PROGRAM convert_tpwobs

  INTEGER       :: ncwpfil
  CHARACTER(LEN=256)    :: cwpfname, cwpoutfile
  INTEGER       :: istatus, lenstr
  INTEGER       :: i, j
  INTEGER       :: nxcwp,nycwp,valid_num
  INTEGER, ALLOCATABLE             :: cldphase(:)
  REAL, ALLOCATABLE, DIMENSION(:)  :: xloc,yloc,cwp



  CALL GET_COMMAND_ARGUMENT(1, cwpfname,lenstr,istatus)
  OPEN(25,File=cwpfname,Form='Unformatted')
  READ(25) nxcwp,nycwp,valid_num
  print*,'ideal data dimension===============',nxcwp,nycwp,valid_num
  IF (nxcwp*nycwp/=valid_num) THEN
    print*,' ERROR:Dimension of obs mismatch the obs file.'
    stop
  END IF

  ALLOCATE (xloc(valid_num),stat=istatus)

  ALLOCATE (yloc(valid_num),stat=istatus)

  ALLOCATE (cwp(valid_num),stat=istatus)

  ALLOCATE (cldphase(valid_num),stat=istatus)

  xloc=0.0;yloc=0.0;cwp=0.0;cldphase=0

  READ(25) (xloc(i),i=1,valid_num)
  READ(25) (yloc(i),i=1,valid_num)
  READ(25) (cwp(i),i=1,valid_num)
  READ(25) (cldphase(i),i=1,valid_num)
  CLOSE(25)

  write(cwpoutfile,'(2a)')          &
        cwpfname(1:17),'.txt'
  open(124,file=cwpoutfile,status='replace')
  write(124,*) nxcwp,nycwp,valid_num
  DO i=1,valid_num
    IF (xloc(i)>=0 .and. yloc(i)>=0 .and. xloc(i)<=80000 .and. yloc(i)<=80000) THEN
      write(124,'(1x,I1,2(1x,F8.2),2(1x,F12.5),1x,F12.8)') cldphase(i),xloc(i),yloc(i),cwp(i)
    END IF
  END DO
  close(124)

END PROGRAM convert_tpwobs
