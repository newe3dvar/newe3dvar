!
!##################################################################
!##################################################################
!######                                                      ######
!######               SUBROUTINE MKARPS2D                    ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE mkarps2d (nx_ext,ny_ext,nx,ny,                               &
           iorder,iloc,jloc,x_ext,y_ext,                                &
           x2d,y2d,varext,arpsvar,                                      &
           dxfld,dyfld,rdxfld,rdyfld,                                   &
           slopey,alphay,betay,                                         &
           tem_ext)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Take 2D surface data from external file and interpolate in
!  the horizontal to form the ARPS 2D data set
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Fanyou Kong
!  6/10/1997.
!
!  MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx_ext   Number of grid points in the x-direction (east/west)
!             for the external grid
!    ny_ext   Number of grid points in the y-direction (north/south)
!             for the external grid
!
!    nx       Number of grid points in the x-direction (east/west)
!             for the ARPS grid
!    ny       Number of grid points in the y-direction (north/south)
!             for the ARPS grid
!
!    iorder   order of polynomial for interpolation (1, 2 or 3)
!
!    iloc     x-index location of ARPS grid point in the external array
!    jloc     y-index location of ARPS grid point in the external array
!
!    x2d      x coordinate of ARPS grid point in external coordinate
!    y2d      x coordinate of ARPS grid point in external coordinate
!
!    varext   external 2D data
!
!  OUTPUT:
!
!    arpsvar  2D array of variable at ARPS grid locations
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Input variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: nx_ext,ny_ext         ! external grid dimensions
  INTEGER :: nx,ny                 ! ARPS grid dimensions
  INTEGER :: iorder                ! interpolating polynomial order
  INTEGER :: iloc(nx,ny)           ! external x-index of ARPS grid point
  INTEGER :: jloc(nx,ny)           ! external y-index of ARPS grid point
  REAL :: x_ext(nx_ext)            ! external x-coord
  REAL :: y_ext(ny_ext)            ! external y-coord
!                                  interpolated to ARPS grid locs
  REAL :: x2d(nx,ny)
  REAL :: y2d(nx,ny)
  REAL :: varext(nx_ext,ny_ext)    ! 2D variable to convert
!
!-----------------------------------------------------------------------
!
!  Output variables
!
!-----------------------------------------------------------------------
!
  REAL :: arpsvar( nx, ny)         ! 2-D array of ARPS grid data
!
!-----------------------------------------------------------------------
!
!  Temporary work arrays
!
!-----------------------------------------------------------------------
!
  REAL :: dxfld(nx_ext)
  REAL :: dyfld(ny_ext)
  REAL :: rdxfld(nx_ext)
  REAL :: rdyfld(ny_ext)
  REAL :: slopey(nx_ext,ny_ext)
  REAL :: alphay(nx_ext,ny_ext)
  REAL :: betay(nx_ext,ny_ext)
  REAL :: tem_ext(nx_ext,ny_ext)
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: ia,ja
  REAL :: arpsdata
  REAL :: pntint2d
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!-----------------------------------------------------------------------
!
!  Compute derivative terms
!
!-----------------------------------------------------------------------
!
  CALL setdrvy(nx_ext,ny_ext,1,                                         &
               1,nx_ext,1,ny_ext,1,1,                                   &
               dyfld,rdyfld,varext,                                     &
               slopey,alphay,betay)
!
!-----------------------------------------------------------------------
!
!  Loop through all ARPS grid points
!
!-----------------------------------------------------------------------
!
  DO ja=1,ny
    DO ia=1,nx
!
!-----------------------------------------------------------------------
!
!    Horizontal interpolation
!
!-----------------------------------------------------------------------
!
      arpsdata=pntint2d(nx_ext,ny_ext,                                  &
               2,nx_ext-1,2,ny_ext-1,                                   &
               iorder,x_ext,y_ext,x2d(ia,ja),y2d(ia,ja),                &
               iloc(ia,ja),jloc(ia,ja),varext,                          &
               dxfld,dyfld,rdxfld,rdyfld,                               &
               slopey,alphay,betay)

      arpsvar(ia,ja)=arpsdata

    END DO
  END DO

  RETURN
END SUBROUTINE mkarps2d
