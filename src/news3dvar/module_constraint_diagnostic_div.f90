!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Constraint using diagnostic divergence equation
!  to help build some kind of balance
!
!  tmp.debug.gge
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MODULE constraint_diagnostic_div

!-----------------------------------------------------------------------
!
!  AUTHOR:
!
!  05/23/2017 (Y. Wang)
!  Re-organized into a module and use less memory.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------

  USE model_precision

  USE module_modelArray, ONLY : qvbar
  USE module_arpsArray,  ONLY : ubar, vbar, pbar, ptbar, usflx, vsflx, tke, tsfc

  IMPLICIT NONE

  !INCLUDE 'mp.inc'

!-----------------------------------------------------------------------
!
! Control options
!
!-----------------------------------------------------------------------
  SAVE
  PRIVATE

  INTEGER  :: dpec_opt
  REAL(P), ALLOCATABLE :: wgt_dpec(:)
  LOGICAL  :: do_dpec

  INTEGER  :: hBal_opt
  REAL(P), ALLOCATABLE :: wgt_hBal(:)
  LOGICAL  :: do_hBal

  REAL(P)  :: dcwgt, hcwgt
!-----------------------------------------------------------------------
!
! Work arrays
!
!-----------------------------------------------------------------------

  LOGICAL :: wrk_allocated
  REAL(P), ALLOCATABLE, DIMENSION (:,:)   :: sinlat2d

  REAL(P), ALLOCATABLE, DIMENSION (:,:,:) :: wrk01, wrk02, wrk03, wrk04,&
                           wrk05,  wrk06,  wrk07, wrk08,  wrk09, wrk10, &
                           wrk11,  wrk12,  wrk13, wrk14,  wrk15
  REAL(P), ALLOCATABLE, DIMENSION (:,:,:) :: nu, nv, nw
  REAL(P), ALLOCATABLE, DIMENSION (:,:,:) :: nustr, nvstr, nwstr,nwcont
  REAL(P), ALLOCATABLE, DIMENSION (:,:,:) :: pprt, ptprt, qv
  REAL(P), ALLOCATABLE, DIMENSION (:,:,:) :: uforce,  vforce,  wforce

  REAL(P), ALLOCATABLE, DIMENSION (:,:,:) :: dpec3d
  REAL(P), ALLOCATABLE, DIMENSION (:,:,:) :: hBal3D1, hBal3D2,hBal3D3

  REAL,    ALLOCATABLE, DIMENSION (:,:,:) :: buforce
  REAL,    ALLOCATABLE, DIMENSION (:,:,:) :: bvforce
  REAL,    ALLOCATABLE, DIMENSION (:,:,:) :: bwforce
  LOGICAL :: bforce_allocated

!-----------------------------------------------------------------------
!
! Public interface
!
!-----------------------------------------------------------------------
  PUBLIC :: do_hBal, do_dpec, hBal_opt

  PUBLIC :: consdiv_read_nml_options, consdiv_checkflags
  !PUBLIC :: consdiv_allocate_wrk, consdiv_deallocate_wrk
  PUBLIC :: consdiv_deallocate_wrk
  PUBLIC :: consdiv_costf, consdiv_gradt
  PUBLIC :: read_tendency, allocate_timeTendency, deallocate_timeTendency

  CONTAINS

  SUBROUTINE consdiv_read_nml_options(unum,npass,myproc,istatus)

  !#####################################################################

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: unum, myproc
    INTEGER, INTENT(IN)  :: npass
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    NAMELIST /var_consdiv/ dpec_opt,wgt_dpec, hBal_opt, wgt_hBal

  !---------------------------------------------------------------------

    INTEGER :: i

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !IF (npass > maxpass) THEN
    !  WRITE(*,'(1x,a,2(I0,a),/,7x,a)') 'ERROR: Request npass (',npass,  &
    !          ')is larger then this maxpass (',maxpass,') in constraint_diagnostic_div.', &
    !          'Program will abort.'
    !  istatus = -1
    !  RETURN
    !END IF

    ALLOCATE(wgt_dpec(npass), STAT = istatus)
    CALL check_alloc_status(istatus, "constraint_diagnostic_div:wgt_dpec")
    ALLOCATE(wgt_hBal(npass), STAT = istatus)
    CALL check_alloc_status(istatus, "constraint_diagnostic_div:wgt_hBal")

    dpec_opt = 0
    wgt_dpec = 0.0

    hBal_opt = 0
    wgt_hBal = 0.0

    IF (myproc == 0) THEN
      READ(unum, var_consdiv,  END=350)
      WRITE(*,*) 'Namelist block var_consdiv sucessfully read.'

      GOTO 400

      350 CONTINUE
      WRITE(*,*) 'ERROR: reading namelist block var_consdiv.'
      istatus = -2
      RETURN

      400 CONTINUE

      WRITE(*,'(5x,a,I0,a)') 'dpec_opt = ', dpec_opt,','
      IF ( dpec_opt == 1 ) THEN
         WRITE(*,*) 'dpec weighting coefficients: '
         WRITE(*,*) (wgt_dpec(i), i =1 , npass )
      END IF

      WRITE(*,'(5x,a,I0,a)') 'hBal_opt = ', hBal_opt,','
      IF ( hBal_opt >= 1 ) THEN
         WRITE(*,*) (wgt_hBal(i), i =1 , npass )
      END IF

    END IF

    CALL mpupdatei(dpec_opt,       1)
    CALL mpupdater(wgt_dpec,       npass)

    CALL mpupdatei(hBal_opt, 1)
    CALL mpupdater(wgt_hBal, npass)

    wrk_allocated = .FALSE.
    bforce_allocated = .FALSE.

    RETURN
  END SUBROUTINE consdiv_read_nml_options

  !#####################################################################

  SUBROUTINE consdiv_checkflags(ipass,nx,ny,nz,x,y,istatus)
    IMPLICIT NONE
    INTEGER, INTENT(IN)  :: ipass
    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL(P), INTENT(IN)  :: x(nx)
    REAL(P), INTENT(IN)  :: y(ny)
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    do_dpec = .FALSE.
    do_hBal = .FALSE.

    dcwgt = wgt_dpec(ipass)
    hcwgt = wgt_hBal(ipass)

    IF( dpec_opt == 1 .and. wgt_dpec(ipass) > 0.0 ) do_dpec = .TRUE.
    IF( hBal_opt >= 1 .and. wgt_hBal(ipass) > 0.0 ) do_hBal = .TRUE.

    IF (.NOT. wrk_allocated) CALL consdiv_allocate_wrk(nx,ny,nz,istatus)

    IF ( do_dpec .OR. do_hBal ) THEN
      CALL gtsinlat(nx,ny,x,y, sinlat2d, wrk01,wrk02, wrk03)
    END IF

    RETURN
  END SUBROUTINE consdiv_checkflags

  SUBROUTINE consdiv_costf(nx,ny,nz,nscalarq,ibgn,iend,jbgn,jend,       &
                        x,y,z,zp, j1,j2,j3,j3inv,mapfct,                &
                        rhostr,              &
                        dxinv,dyinv,dzinv,             &
                        u_ctr,v_ctr,w_ctr,p_ctr,t_ctr,q_ctr,            &
                        qscalar_ctr,                                &
                        u,v,w,wcont,                                    &
                        f_dpec, f_hBal, istatus)

  !#####################################################################

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nx,ny,nz,nscalarq,ibgn,iend,jbgn,jend

    REAL(P) :: x     (nx)        ! The x-coord. of the physical and
                                 ! computational grid. Defined at u-point.
    REAL(P) :: y     (ny)        ! The y-coord. of the physical and
                                 ! computational grid. Defined at v-point.
    REAL(P) :: z     (nz)        ! The z-coord. of the computational grid.
                                 ! Defined at w-point on the staggered grid.
    REAL(P) :: zp    (nx,ny,nz)  ! The physical height coordinate defined at
                                 ! w-point of the staggered grid.

    REAL(P) :: j1    (nx,ny,nz)  ! Coordinate transformation Jacobian
                                 ! defined as - d( zp )/d( x ).
    REAL(P) :: j2    (nx,ny,nz)  ! Coordinate transformation Jacobian
                                 ! defined as - d( zp )/d( y ).
    REAL(P) :: j3    (nx,ny,nz)  ! Coordinate transformation Jacobian
                                 ! defined as d( zp )/d( z ).
    REAL(P) :: j3inv (nx,ny,nz)  ! Inverse of j3

    REAL(P) :: rhostr(nx,ny,nz)  ! Base state density rhobar times j3

    REAL(P) :: mapfct(nx,ny,8)   ! Map factors at scalar, u and v points

    REAL(P), INTENT(IN)  :: u(nx,ny,nz), v(nx,ny,nz), w(nx,ny,nz)
    REAL(P) :: wcont(nx,ny,nz)

    REAL(P), INTENT(IN)  :: dxinv, dyinv, dzinv

    REAL(P), INTENT(IN)  ::   u_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  ::   v_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  ::   p_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  ::   t_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  ::   q_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  ::   w_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  ::   qscalar_ctr(nx,ny,nz,nscalarq)

    REAL(DP), INTENT(INOUT) :: f_dpec, f_hBal

    INTEGER,  INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i, j, k
    REAL(P) :: aa, bb, cc, hcwgt2

    !REAL(DP) :: f_dpec, f_hBal

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !f_dpec=0.0
    !f_hBal=0.0

    !IF (.NOT. wrk_allocated) CALL consdiv_allocate_wrk(nx,ny,nz,istatus)

    wrk01  = 0.0; wrk02  = 0.0; wrk03 = 0.0; wrk04  = 0.0;
    wrk05  = 0.0; wrk06  = 0.0; wrk07 = 0.0
    wrk08  = 0.0; wrk09  = 0.0; wrk10  = 0.0; wrk11  = 0.0;
    wrk12  = 0.0; wrk13  = 0.0; wrk14  = 0.0; wrk15  = 0.0;

    uforce = 0.0; vforce = 0.0; wforce = 0.0

    !--- BEGIN
    !--- below compute pprt, ptprt, total qv
    !
    DO k= 1,nz
      DO j= 1,ny
        DO i= 1,nx
           pprt(i,j,k)  = p_ctr(i,j,k)- pbar(i,j,k)
           ptprt(i,j,k) = t_ctr(i,j,k)-ptbar(i,j,k)
        END DO
      END DO
    END DO

    !TO BE CHANGED: qc/r/i/s/h need to be nqc/r/i/s/h if dpec_opt and
    !ref_opt are both set to 1 !by GGE 9/6/2013

    !wcont will be computed inside tot_force
    CALL tot_force(nx,ny,nz,                                            &
            u_ctr(:,:,:),v_ctr(:,:,:),w_ctr(:,:,:),wcont(:,:,:),        &
            ptprt(:,:,:),pprt(:,:,:),                                   &
            q_ctr(:,:,:),qscalar_ctr,tke(:,:,:),                        &
            ubar,vbar,ptbar,pbar,rhostr,qvbar,                          &
            usflx,vsflx, x,y,z,zp, j1,j2,j3,sinlat2d,tsfc,              &
            uforce,vforce,wforce,        wrk05,wrk06,wrk07,             &
            u,    v,    w,                                              &
            wrk01,wrk02,wrk03,wrk04,wrk08,wrk09,wrk10,wrk11,wrk12,      &
            wrk13,wrk14,wrk15)

    IF ( do_dpec) THEN
      DO k=2,nz-2
        DO j=jbgn,jend
          DO i=ibgn,iend
            dpec3D(i,j,k) = 1./dcwgt * j3inv(i,j,k)*( mapfct(i,j,7)     &
                          *( (uforce(i+1,j,k)-uforce(i,j,k) )*dxinv     &
                             +(vforce(i,j+1,k)-vforce(i,j,k) )*dyinv )  &
                             +(wforce(i,j,k+1)-wforce(i,j,k) )*dzinv    )
            f_dpec = f_dpec + dpec3D(i,j,k) * dpec3D(i,j,k)
          END DO
        END DO
      END DO
      !cfun_single = cfun_single + f_dpec
      !print*,'f_dpec===',f_dpec/2.0 !to be commented
    END IF

    IF ( do_hBal ) THEN
      !aa=uforce(i,j,k)/rhostru(i,j,k)-buforce(i,j,k)
      IF (.NOT. bforce_allocated) CALL allocate_timeTendency(nx,ny,nz)

      hcwgt2=hcwgt*hcwgt
      Do k = 2, nz-1
        DO j=jbgn,jend
          DO i=ibgn,iend
            aa = uforce(i,j,k)-buforce(i,j,k)
            bb = vforce(i,j,k)-bvforce(i,j,k)
            cc = wforce(i,j,k)-bwforce(i,j,k)
!           hBal3D1(i,j,k) = aa * hcwgt
!           hBal3D2(i,j,k) = bb * hcwgt
!           hBal3D3(i,j,k) = cc * hcwgt
            hBal3D1(i,j,k) = aa / hcwgt2
            hBal3D2(i,j,k) = bb / hcwgt2
            hBal3D3(i,j,k) = cc / hcwgt2

            f_hBal = f_hBal + aa* hBal3D1(i,j,k) + bb * hBal3D2(i,j,k)  &
                            + cc* hBal3D3(i,j,k)
          END DO
        END DO
      END DO
      !cfun_single = cfun_single + f_hBal
      !print *,'f_hBal===', f_hBal/2.0
    END IF

    RETURN
  END SUBROUTINE consdiv_costf

  SUBROUTINE consdiv_gradt(nx,ny,nz,nscalarq,x,y,z,zp,                  &
                        j1,j2,j3,j3inv,mapfct,                          &
                        rhostr,              &
                        dxinv,dyinv,dzinv,             &
                        !u_ctr,v_ctr,w_ctr,p_ctr,t_ctr,q_ctr,qscalar_ctr,&
                        u,v,w,wcont,                                    &
                        u_grd,v_grd,w_grd,p_grd,t_grd,q_grd,            &
                        qscalar_grd,istatus)

  !#####################################################################

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nx,ny,nz,nscalarq

    REAL(P) :: x     (nx)        ! The x-coord. of the physical and
                                 ! computational grid. Defined at u-point.
    REAL(P) :: y     (ny)        ! The y-coord. of the physical and
                                 ! computational grid. Defined at v-point.
    REAL(P) :: z     (nz)        ! The z-coord. of the computational grid.
                                 ! Defined at w-point on the staggered grid.
    REAL(P) :: zp    (nx,ny,nz)  ! The physical height coordinate defined at
                                 ! w-point of the staggered grid.

    REAL(P) :: j1    (nx,ny,nz)  ! Coordinate transformation Jacobian
                                 ! defined as - d( zp )/d( x ).
    REAL(P) :: j2    (nx,ny,nz)  ! Coordinate transformation Jacobian
                                 ! defined as - d( zp )/d( y ).
    REAL(P) :: j3    (nx,ny,nz)  ! Coordinate transformation Jacobian
                                 ! defined as d( zp )/d( z ).
    REAL(P) :: j3inv (nx,ny,nz)  ! Inverse of j3

    REAL(P) :: rhostr(nx,ny,nz)  ! Base state density rhobar times j3

    REAL(P) :: mapfct(nx,ny,8)   ! Map factors at scalar, u and v points

    REAL(P), INTENT(IN) :: u(nx,ny,nz), v(nx,ny,nz), w(nx,ny,nz)
    REAL(P) :: wcont(nx,ny,nz)

    !REAL(P), INTENT(IN)  ::   u_ctr(nx,ny,nz)
    !REAL(P), INTENT(IN)  ::   v_ctr(nx,ny,nz)
    !REAL(P), INTENT(IN)  ::   p_ctr(nx,ny,nz)
    !REAL(P), INTENT(IN)  ::   t_ctr(nx,ny,nz)
    !REAL(P), INTENT(IN)  ::   q_ctr(nx,ny,nz)
    !REAL(P), INTENT(IN)  ::   w_ctr(nx,ny,nz)
    !REAL(P), INTENT(IN)  ::   qscalar_ctr(nx,ny,nz,nscalarq)

    REAL(P), INTENT(IN)  :: dxinv, dyinv, dzinv

    REAL(P), INTENT(INOUT) :: u_grd(nx,ny,nz), v_grd(nx,ny,nz), w_grd(nx,ny,nz)
    REAL(P), INTENT(INOUT) :: p_grd(nx,ny,nz), t_grd(nx,ny,nz), q_grd(nx,ny,nz)
    REAL(P), INTENT(INOUT) :: qscalar_grd(nx,ny,nz,nscalarq)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j,k

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    uforce=0.0; vforce=0.0; wforce=0.0

    IF ( do_dpec ) THEN
      DO k=2,nz-2
        DO j=2,ny-1
          DO i=2,nx-1

            uforce(i+1,j,k)=uforce(i+1,j,k) + 1./dcwgt*j3inv(i,j,k)*        &
                                          mapfct(i,j,7)*dpec3D(i,j,k)*dxinv
            uforce(i  ,j,k)=uforce(i  ,j,k) - 1./dcwgt*j3inv(i,j,k)*        &
                                          mapfct(i,j,7)*dpec3D(i,j,k)*dxinv

            vforce(i,j+1,k)=vforce(i,j+1,k) + 1./dcwgt*j3inv(i,j,k)*        &
                                          mapfct(i,j,7)*dpec3D(i,j,k)*dyinv
            vforce(i  ,j,k)=vforce(i  ,j,k) - 1./dcwgt*j3inv(i,j,k)*        &
                                          mapfct(i,j,7)*dpec3D(i,j,k)*dyinv

            wforce(i,j,k+1)=wforce(i,j,k+1) + 1./dcwgt*j3inv(i,j,k)*        &
                                                        dpec3D(i,j,k)*dzinv
            wforce(i  ,j,k)=wforce(i  ,j,k) - 1./dcwgt*j3inv(i,j,k)*        &
                                                        dpec3D(i,j,k)*dzinv
          END DO
        END DO
      END DO
    END IF !IF ( dpec_opt == 1 .and. wgt_dpec > 0.0)

    IF ( do_hBal  ) THEN
      Do k = 2, nz-1
        DO j = 2, ny-1
          DO i = 2, nx-1
            uforce(i,j,k) = uforce(i,j,k) + hBal3D1(i,j,k)
            vforce(i,j,k) = vforce(i,j,k) + hBal3D2(i,j,k)
            wforce(i,j,k) = wforce(i,j,k) + hBal3D3(i,j,k)
          END DO
        END DO
      END DO
    END IF

    !-------------------------------------------------------------
    ! BEGIN: compute pprt, ptprt, total qv
    !DO k= 1,nz
    !  DO j= 1,ny
    !   DO i= 1,nx
    !      npprt (i,j,k) = p_ctr(i,j,k)-  pbar(i,j,k)
    !      nptprt(i,j,k) = t_ctr(i,j,k)- ptbar(i,j,k)
    !   END DO
    !  END DO
    !END DO


    nustr = 0.0; nvstr = 0.0; nwstr = 0.0; nwcont = 0.0
    ptprt = 0.0; pprt  = 0.0; qv    = 0.0
    nu    = 0.0; nv    = 0.0; nw    = 0.0

    !wcont will be calculated inside adtlm_totforce
    CALL adtlm_totforce(nx,ny,nz,                                       &
             nu(:,:,:),nv(:,:,:),nw(:,:,:),wcont(:,:,:),                &
             ptprt(:,:,:),pprt(:,:,:),qv(:,:,:),                        &
             qscalar_grd(:,:,:,:),tke(:,:,:),                           &
             ubar,vbar,ptbar,pbar,rhostr,qvbar,                         &
             nwcont,                                                    &
             nustr,nvstr,nwstr,                                         &
             usflx,vsflx, x,y,z,zp, j1,j2,j3, sinlat2d,tsfc,            &
             uforce,vforce,wforce, wrk05,wrk06,wrk07,                   &
             u,    v,    w,                                             &
             wrk01,wrk02,wrk03,wrk04,wrk08,wrk09,wrk10,wrk11,wrk12,     &
             wrk13,wrk14,wrk15)

    DO k= 1,nz-1
      DO j= 1,ny-1
        DO i= 1,nx-1
          p_grd(i,j,k) = p_grd(i,j,k) + pprt(i,j,k)
          t_grd(i,j,k) = t_grd(i,j,k) + ptprt(i,j,k)
          q_grd(i,j,k) = q_grd(i,j,k) + qv(i,j,k)
        END DO
      END DO
    END DO

    DO k=2,nz-1
       DO j= 1,ny-1
         DO i= 1,nx-1
           w_grd(i,j,k) = w_grd(i,j,k)+nw(i,j,k)
         END DO
       END DO
    END DO
    DO k= 1,nz-1
       DO j= 2,ny-1
         DO i= 1,nx-1
           v_grd(i,j,k) = v_grd(i,j,k)+nv(i,j,k)
         END DO
       END DO
    END DO
    DO k= 1,nz-1
       DO j= 1,ny-1
         DO i= 2,nx-1
           u_grd(i,j,k) = u_grd(i,j,k)+nu(i,j,k)
         END DO
       END DO
    END DO

    nu=0.0; nv=0.0; nw=0.0; pprt=0.0; ptprt=0.0

    RETURN
  END SUBROUTINE consdiv_gradt

  !#####################################################################

  SUBROUTINE consdiv_allocate_wrk(nx,ny,nz,istatus)
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nx,ny,nz
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (wrk_allocated) THEN
      WRITE(*,'(1x,a)') 'INFO: Should not be here. Maybe multiple calls of "consdiv_allocate_wrk".'
      istatus = 1
      RETURN
    END IF

    istatus = 0

    ALLOCATE(sinlat2d(nx,ny),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_diagnostic_div: sinlat2d")

    ALLOCATE(wrk01(nx,ny,nz), wrk02(nx,ny,nz), STAT = istatus )
    ALLOCATE(wrk03(nx,ny,nz), wrk04(nx,ny,nz), STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_diagnostic_div: wrk04")

    ALLOCATE(wrk05(nx,ny,nz), wrk06(nx,ny,nz), STAT = istatus )
    ALLOCATE(wrk07(nx,ny,nz), wrk08(nx,ny,nz), STAT = istatus )
    ALLOCATE(wrk09(nx,ny,nz), wrk10(nx,ny,nz), STAT = istatus )
    ALLOCATE(wrk11(nx,ny,nz), wrk12(nx,ny,nz), STAT = istatus )
    ALLOCATE(wrk13(nx,ny,nz), wrk14(nx,ny,nz), STAT = istatus )
    ALLOCATE(wrk15(nx,ny,nz),                  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_diagnostic_div: wrk15")

    ALLOCATE(nu(nx,ny,nz),    nv(nx,ny,nz),     nw(nx,ny,nz),  STAT = istatus )
    ALLOCATE(nustr(nx,ny,nz), nvstr(nx,ny,nz),                 STAT = istatus )
    ALLOCATE(nwstr(nx,ny,nz), nwcont(nx,ny,nz),                STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_diagnostic_div: nwcont")

    ALLOCATE(pprt(nx,ny,nz),  ptprt(nx,ny,nz),  qv(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_diagnostic_div: qv")

    ALLOCATE(uforce(nx,ny,nz), vforce(nx,ny,nz), wforce(nx,ny,nz),    STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_diagnostic_div: wforce")

    ALLOCATE(DPEC3D(nx,ny,nz), STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_diagnostic_div: dpec3d")

    ALLOCATE(hBal3D1(nx,ny,nz), hBal3D2(nx,ny,nz), hBal3D3(nx,ny,nz), STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_diagnostic_div: dpec3d")

    wrk_allocated = .TRUE.

    RETURN
  END SUBROUTINE consdiv_allocate_wrk

  !#####################################################################

  SUBROUTINE consdiv_deallocate_wrk(istatus)
    IMPLICIT NONE

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (wrk_allocated) THEN

      DEALLOCATE(sinlat2d,   STAT = istatus )

      DEALLOCATE(wrk01, wrk02, wrk03, wrk04,   STAT = istatus )

      DEALLOCATE(wrk05, wrk06, wrk07, wrk08,   STAT = istatus )
      DEALLOCATE(wrk09, wrk10, wrk11, wrk12,   STAT = istatus )
      DEALLOCATE(wrk13, wrk14, wrk15,          STAT = istatus )

      DEALLOCATE(nu,    nv,     nw,            STAT = istatus )
      DEALLOCATE(nustr, nvstr,  nwstr, nwcont, STAT = istatus )

      DEALLOCATE(pprt,  ptprt,  qv,            STAT = istatus )
      DEALLOCATE(uforce,vforce, wforce,        STAT = istatus )

      DEALLOCATE(DPEC3D, hBal3D1, hBal3D2,     STAT = istatus )

      wrk_allocated = .FALSE.

    END IF

    RETURN
  END SUBROUTINE consdiv_deallocate_wrk

  !#####################################################################

  !
  ! Allocate them
  !
  SUBROUTINE allocate_timeTendency(nx,ny,nz)

    IMPLICIT NONE
    INTEGER       :: nx,ny,nz

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (.NOT. bforce_allocated) THEN
      allocate( buforce(nx,ny,nz) )
      allocate( bvforce(nx,ny,nz) )
      allocate( bwforce(nx,ny,nz) )
    END IF

    bforce_allocated = .TRUE.
    buforce=0.0; bvforce=0.0; bwforce=0.0

  END SUBROUTINE allocate_timeTendency

  !#####################################################################

  SUBROUTINE read_tendency(nx,ny,nz,istatus)

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nx, ny,nz
    INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

    CHARACTER(LEN=256) :: tmpName
    CHARACTER(LEN=2)   :: nfil
    INTEGER            :: ien
    LOGICAL            :: file_exist

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (.NOT. bforce_allocated) THEN
       CALL allocate_timeTendency(nx,ny,nz)
    END IF

    open(255, file='./CARD')
    read(255,*) ien
    write(nfil,'(i2.2)') ien-1
    close(255)

    tmpName = './fcst_anal/tendency.dat'//nfil
    INQUIRE(FILE = TRIM(tmpName), EXIST = file_exist)

    IF (file_exist) THEN
      open(98,file=tmpName,form='unformatted',status='old')
      read(98) buforce,bvforce,bwforce
      close(98)
    END IF

    ! istatus = -1
    RETURN
  END SUBROUTINE read_tendency

  !#####################################################################

  SUBROUTINE deallocate_timeTendency

    IMPLICIT NONE

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (bforce_allocated) THEN
      deallocate( buforce )
      deallocate( bvforce )
      deallocate( bwforce )
    END IF

    RETURN
  END SUBROUTINE deallocate_timeTendency

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

END MODULE constraint_diagnostic_div
