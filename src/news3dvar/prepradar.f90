!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE PREPRADAR                  ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE prepradar(nx,ny,nz,nz_tab,nvar_anx,mx_pass,                  &
           raduvobs,radrhobs,radistride,radkstride,mosaicopt,           &
           npass,refrh,rhradobs,                                        &
           x,y,zp,xs,ys,zps,hterain,latgr,longr,anx,qback,hqback,nlvqback,&
           ref_mos_3d,rhv_mos_3d,zdr_mos_3d,kdp_mos_3d,                 &
           tem1,istat_radar,istatus)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Read radar data stored as columns on a remapped grid
!  and prepare data for use in the analysis.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Keith Brewster
!  August, 1995
!
!  MODIFICATION HISTORY:
!
!  07/15/97 (K. Brewster)
!  Corrected a bug in interpolation weights.
!
!  Nov 1997 (KB)
!  Added rhmax,refsat and rhrad to argument list.
!
!  Feb, 1998 (KB)
!  Added addition option switches to argument list allowing
!  selection of either radar wind obs and/or radar RH pseudo-obs.
!
!  Mar, 1998 (KB)
!  Added uazmlim to protect against division by zero when
!  azimuth is close to perpendicular to u or v component.
!
!  Dec, 1998 (KB)
!  Made changes for RHstar to qv as moisture variable.
!
!  August, 2001 (KB)
!  Corrected call to dhdr which now returns dhdr not local
!  elevation angle.  Modified calculation of odifrad to speed
!  convergence to vr.
!
!  September, 2002 (KB)
!  Added code to pass along trnradc.
!
!  November, 2007 (KWT)
!  Added code to support MPI of radial velocity.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nvar_anx   number of analysis variables
!
!    refsat     Reflectivity threshold for assigning moisture ob
!    rhrad      Value of relative humidity to assign
!
!    xs         x location of scalar pts (m)
!    ys         y location of scalar pts (m)
!    zs         z location of scalar pts (m)
!    hterain    height of terrain (m MSL)
!    anx        analyzed variables (or first guess)
!    qback      background (first guess) error
!
!
!  OUTPUT:
!
!    isrcrad  index of radar source
!    stnrad   radar site name    character*5
!    latrad   latitude of radar  (degrees N)
!    lonrad   longitude of radar (degrees E)
!    elvrad   elevation of feed horn of radar (m MSL)
!    latradc  latitude of radar column   (degrees N)
!    lonradc  longitude of radar column  (degrees E)
!    xradc    x location of radar column
!    yradc    y location of radar column
!    irad     radar number
!    nlevrad  number of levels of radar data in each column
!    distrad  distance of radar column from source radar
!    uazmrad  u-component of radar beam for each column
!    vazmrad  v-component of radar beam for each column
!    hgtradc  height (m MSL) of radar observations
!    obsrad   radar observations
!    oanxrad  analysis (first guess) value at radar data location
!    odifrad  difference between radar observation and analysis
!    qobsrad  normalized observation error
!    QUALRAD  radar data quality indicator
!    ncolrad  number of radar columns read-in, local for MPI
!    ncolrad_mpi  number of radar columns read-in, MPI only
!    indexrad  index of data point processor owners (MPI only)
!    oindexrad 2nd index of data point processor owners (MPI only, iradvel>0)
!    istatus  status indicator
!    istat_radar ref_mos_3d status indicator
!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  USE module_varpara
  USE module_radarobs
  USE module_ensemble, only : iensmbl, icycle

  IMPLICIT NONE
  INTEGER :: nx,ny,nz,nz_tab,nvar_anx
  INTEGER :: mx_pass
  INTEGER :: raduvobs
  INTEGER :: radrhobs
  INTEGER :: radistride
  INTEGER :: radkstride
  INTEGER :: mosaicopt
  INTEGER :: npass
  INTEGER :: np
  REAL    :: refrh
  REAL    :: rhradobs
!
!-----------------------------------------------------------------------
!
!  Grid variables
!
!-----------------------------------------------------------------------
!
  REAL, INTENT(IN) :: x(nx)
  REAL, INTENT(IN) :: y(ny)
  REAL, INTENT(IN) :: zp(nx,ny,nz)
  REAL, INTENT(IN) :: xs(nx)
  REAL, INTENT(IN) :: ys(ny)
  REAL, INTENT(IN) :: zps(nx,ny,nz)
  REAL :: hterain(nx,ny)
  REAL :: latgr(nx,ny)
  REAL :: longr(nx,ny)
  REAL :: anx(nx,ny,nz,nvar_anx)
  REAL :: qback(nvar_anx,nz_tab)
  REAL :: hqback(nz_tab)
  INTEGER :: nlvqback

!
!-----------------------------------------------------------------------
!
!  Radar mosaic variables
!
!-----------------------------------------------------------------------
!
  !REAL    :: refmax_mos_3d(nx,ny,nz)
  REAL,    INTENT(OUT) :: ref_mos_3d(nx,ny,nz)
  REAL,    INTENT(OUT) :: rhv_mos_3d(nx,ny,nz)
  REAL,    INTENT(OUT) :: zdr_mos_3d(nx,ny,nz)
  REAL,    INTENT(OUT) :: kdp_mos_3d(nx,ny,nz)
  !REAL    :: dist_mos_3d(nx,ny,nz)

  INTEGER, INTENT(INOUT) :: istat_radar
  INTEGER, INTENT(OUT)   :: istatus

!
!-----------------------------------------------------------------------
!
  REAL :: tem1(nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: iusechk
  INTEGER :: ierror
  INTEGER :: k,i,j

  INTEGER :: istat_mos
  LOGICAL :: verbose

  REAL :: gasdev,ran3
  INTEGER, PARAMETER :: iseed=-100
  INTEGER            :: iseed1
  REAL,    PARAMETER :: errRf = SQRT(8.0)

  INCLUDE 'mp.inc'
  !INCLUDE 'varpara.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  verbose = .FALSE.

!-----------------------------------------------------------------------
!
!  Read remapped radar data
!
!-----------------------------------------------------------------------
!

  IF(raduvobs > 0 .OR. radrhobs > 0 .OR. mosaicopt > 0 ) THEN
    iusechk=1

    CALL radarobs_get_size(nx,ny,nz,raduvobs,radrhobs,                  &
                           iusechk,npass,mosaicopt,                     &
                           radistride,radkstride,verbose,istatus)
    IF (istatus /= 0) THEN
      IF (nradarfiles <= 0) THEN
        WRITE(*,'(/,1x,a,/)') 'WARNING: no valid radar data file is found.'
        istatus = 9
        RETURN
        !CALL arpsstop('Error in radarobs_get_size',1)
      END IF
    END IF

    CALL radarobs_allocate_arrays(nz,istatus)
    isrcradr(0)=0

    CALL radarobs_read_files(nx,ny,nz,                                  &
                        radistride,radkstride,mosaicopt,                &
                        xs,ys,zps,latgr,longr,                          &
                        ref_mos_3d,rhv_mos_3d,zdr_mos_3d,kdp_mos_3d,    &
                        verbose,istat_mos,istatus)
    IF (istatus /= 0) CALL arpsstop('Error in radarobs_read_files',1)

    CALL mpbarrier
    IF (verbose) WRITE(6,'(1x,a,i4.4,2(a,i8),a,I3,a)')                  &
                'Prepradar(',myproc,'): ncolrad = ',ncolrad,            &
                ', mx_colrad = ',mx_colrad,', nz_radar = ',nz_radar,'.'
    IF (myproc == 0) WRITE(6,'(1x,a,i8,/)')                             &
                'Prepradar: ncolrad_total = ',ncolrad_total

    IF (istat_mos > 0) THEN
      istat_radar = 1
      IF (myproc == 0) WRITE(*,'(1x,a,I0,a)')                           &
          'Radar mosaic was composited from ',istat_mos,                &
          ' radars after <radarobs_read_files>.'
    END IF

    IF(ncolrad > 0 .OR. ncolrad_total > 0) THEN
!
!-----------------------------------------------------------------------
!
!  Remove precipitation terminal velocity from the radial velocity.
!
!-----------------------------------------------------------------------
      IF(idealCase_opt==0) THEN

        CALL rmvterm(nvar_rad,mx_rad,nz_radar,mx_colrad,                &
                 latradar,lonradar,elvradar,                            &
                 latradc,lonradc,irad,nlevrad,hgtradc,obsrad,           &
                 ncolrad,istatus)
      END IF
!
!-----------------------------------------------------------------------
!
!  Unfold and QC radar data
!
!-----------------------------------------------------------------------
!

      IF ( grdtlt_flag == 1 ) THEN
        CALL prcsstlt(nx,ny,nz,nz_tab,nvar_anx,nvar_rad,                &
               nrad_anx,mx_rad,nsrc_rad,nz_radar,mx_colrad,             &
               raduvobs,radrhobs,refrh,rhradobs,                        &
               xs,ys,zps,hterain,anx,qback,hqback,nlvqback,             &
               srcrad,qsrcrad,qcthrradv,                                &
               isrcradr,stnradar,latradar,lonradar,elvradar,elvang,bmwidth, &
               latradc,lonradc,xradc,yradc,irad,nlevrad,                &
               hgtradc,obsrad,oanxrad,odifrad,iradc,jradc,              &
               distrad,uazmrad,vazmrad,                                 &
               theradc,trnradc,qobsrad,qualrad,dsdr,dhdr,eleva,k3L,k3U, &
               ncolrad,indexrad,istatus,tem1)
      ELSE
        CALL prcssrad(nx,ny,nz,1,nz_tab,nvar_anx,nvar_rad,              &
               nrad_anx,mx_rad,nsrc_rad,nz_radar,mx_colrad,             &
               raduvobs,radrhobs,refrh,rhradobs,                        &
               x,y,zp,xs,ys,zps,hterain,anx,qback,hqback,nlvqback,      &
               srcrad,qsrcrad,qcthrradv,                                &
               isrcradr,stnradar,latradar,lonradar,elvradar,            &
               latradc,lonradc,xradc,yradc,irad,nlevrad,                &
               hgtradc,obsrad,oanxrad,odifrad,iradc,jradc,kradc,        &
               mosaicopt,ref_mos_3d,distrad,uazmrad,vazmrad,            &
               theradc,trnradc,qobsrad,qualrad,dsdr,dhdr,               &
               ncolrad,indexrad,istatus,tem1)
      END IF !IF ( grdtiltopt == 1 )

    END IF
  ELSE
    ncolrad=0
  END IF

!
!-----------------------------------------------------------------------
!
!  Modify radar mosaic for reflectivity assimilation
!
!-----------------------------------------------------------------------

  IF (iensmbl /= 0 ) THEN
    iseed1 = (iseed-20*iensmbl + icycle) * (myproc + 1)

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          IF (ref_mos_3d(i,j,k) > -9000.0 ) THEN

            IF(ref_mos_3d(i,j,k) > 0.0) THEN
              ref_mos_3d(i,j,k)  =ref_mos_3d(i,j,k)+errRf*GASDEV(iseed1)
            END IF

            IF (ref_mos_3d(i,j,k) < thresh_ref-errRf ) THEN
              ref_mos_3d(i,j,k) = 0.0
            END IF

          END IF
        END DO
      END DO
    END DO

  ELSE

    IF(idealCase_opt /= 1) THEN  ! real-data case only
      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            IF ( ref_mos_3d(i,j,k) > -9000.0 ) THEN

              IF ( ref_mos_3d(i,j,k) < thresh_ref ) THEN
                ref_mos_3d(i,j,k) = 0.0
              END IF

            END IF
          END DO
        END DO
      END DO
    END IF

  END IF

  RETURN
END SUBROUTINE prepradar
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE PRCSSRAD                   ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE prcssrad(nx,ny,nz,ipass,nz_tab,nvar_anx,nvar_rad,            &
           nrad_anx,mx_rad,nsrc_rad,nz_rdr,mx_colrad,                   &
           raduvobs,radrhobs,refrh,rhradobs,                            &
           x,y,zp,xs,ys,zps,hterain,anx,qback,hqback,nlvqback,          &
           srcrad,qsrcrad,qcthrradv,                                    &
           isrcrad,stnrad,latrad,lonrad,elvrad,                         &
           latradc,lonradc,xradc,yradc,irad,nlevrad,                    &
           hgtradc,obsrad,oanxrad,odifrad,iradc,jradc,kradc,            &
           mosaicopt,ref_mos_3d,distrad,uazmrad,vazmrad,                &
           theradc,trnradc,qobsrad,qualrad,dsdr,dhdr,                   &
           ncolrad,indexrad,istatus,tem1)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Unfolds and QC's radar data stored as columns on a remapped grid.
!  For MPI, data point owners also get assigned.
!
!  AUTHOR: Keith Brewster
!  August, 1995
!
!  MODIFICATION HISTORY:
!
!  September, 2002 (KB)
!  Added code to calculate and store trnradc.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nvar_anx   number of analysis variables
!    nvar_rad   number of variables in the obsrad array
!    nrad_anx   number of variables in the odifrad array
!    mx_rad     maximum number of radars
!    nsrc_rad   number of radar sources
!    nz_rdr     maximum number of levels in a radar column
!    mx_colrad  maximum number of radar columns
!
!    xs         x location of scalar pts (m)
!    ys         y location of scalar pts (m)
!    zs         z location of scalar pts (m)
!    hterain    height of terrain (m MSL)
!    anx        analyzed variables (or first guess)
!    qback      background (first guess) error
!    srcrad   name of radar sources
!    qsrcrad  radar observation error
!    isrcrad  index of radar source
!    stnrad   radar site name    character*5
!    latrad   latitude of radar  (degrees N)
!    lonrad   longitude of radar (degrees E)
!    elvrad   elevation of feed horn of radar (m MSL)
!    latradc  latitude of radar column   (degrees N)
!    lonradc  longitude of radar column  (degrees E)
!    xradc    x location of radar column (m)
!    yradc    y location of radar column (m)
!    irad     radar number
!    nlevrad  number of levels of radar data in each column
!    distrad  distance of radar column from source radar
!    uazmrad  u-component of radar beam for each column
!    vazmrad  v-component of radar beam for each column
!    hgtradc  height (m MSL) of radar observations
!    obsrad   radar observations
!
!    indexrad index of data point processor owners (MPI only)
!
!  OUTPUT:
!
!    oanxrad  analysis (first guess) value at radar data location
!    odifrad  difference between radar observation and analysis
!    theradc  theta (potential temperature K) at each radar ob
!    trnradc  terrain height at each radar column
!    qobsrad  normalized observation error
!    qualrad  radar data quality indicator
!    ncolrad  number of radar columns read-in
!    istatus  status indicator
!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  USE module_varpara
  USE module_ensemble, only : iensmbl, icycle

  IMPLICIT NONE
  INTEGER, INTENT(IN) :: ipass
  INTEGER :: nx,ny,nz,nz_tab,nvar_anx
  INTEGER :: nvar_rad,nrad_anx,mx_rad,nsrc_rad,nz_rdr,mx_colrad
  INTEGER :: raduvobs
  INTEGER :: radrhobs
  REAL    :: refrh
  REAL    :: rhradobs
  INTEGER, INTENT(IN) :: mosaicopt
!
!-----------------------------------------------------------------------
!
!  Grid variables
!
!-----------------------------------------------------------------------
!
  REAL, INTENT(IN) :: x(nx)
  REAL, INTENT(IN) :: y(ny)
  REAL, INTENT(IN) :: zp(nx,ny,nz)
  REAL, INTENT(IN) :: xs(nx)
  REAL, INTENT(IN) :: ys(ny)
  REAL, INTENT(IN) :: zps(nx,ny,nz)
  REAL    :: hterain(nx,ny)
  REAL    :: anx(nx,ny,nz,nvar_anx)
  REAL    :: qback(nvar_anx,nz_tab)
  REAL    :: hqback(nz_tab)
  INTEGER :: nlvqback
!
!-----------------------------------------------------------------------
!
!  Radar site variables
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=8) :: srcrad(nsrc_rad)
  INTEGER :: isrcrad(0:mx_rad)
  REAL    :: qsrcrad(nvar_rad,nsrc_rad)
  REAL    :: qcthrradv(nsrc_rad)
  REAL    :: latrad(mx_rad),lonrad(mx_rad)
  REAL    :: elvrad(mx_rad)
  CHARACTER (LEN=5) :: stnrad(mx_rad)
!
!-----------------------------------------------------------------------
!
!  Radar observation variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: irad(mx_colrad)
  INTEGER :: iradc(mx_colrad), jradc(mx_colrad),kradc(nz_rdr,mx_colrad)
  INTEGER :: nlevrad(mx_colrad)
  REAL :: latradc(mx_colrad),lonradc(mx_colrad)
  REAL :: xradc(mx_colrad),yradc(mx_colrad)
  REAL :: hgtradc(nz_rdr,mx_colrad)
  REAL :: theradc(nz_rdr,mx_colrad)
  REAL :: trnradc(mx_colrad)
  REAL ::    dsdr(nz_rdr,mx_colrad)
  REAL ::    dhdr(nz_rdr,mx_colrad)

  REAL :: obsrad(nvar_rad,nz_rdr,mx_colrad)
  REAL :: oanxrad(nrad_anx,nz_rdr,mx_colrad)
  REAL :: odifrad(nrad_anx,nz_rdr,mx_colrad)
  REAL :: qobsrad(nrad_anx,nz_rdr,mx_colrad)
  REAL :: distrad(mx_colrad),uazmrad(mx_colrad),vazmrad(mx_colrad)
  INTEGER :: qualrad(nrad_anx,nz_rdr,mx_colrad)
  INTEGER :: ncolrad
  INTEGER :: istatus

  REAL, INTENT(IN) :: ref_mos_3d(nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  MPI variables
!
!-----------------------------------------------------------------------
!
  INTEGER, INTENT(IN) :: indexrad(mx_colrad)
!
!-----------------------------------------------------------------------
!
!  Temporary work array
!
!-----------------------------------------------------------------------
!
  REAL :: tem1(nx*ny*nz), tem2(nx*ny*nz)
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: icol,jrad,kr
  INTEGER :: i,imid,iloc, iuloc
  INTEGER :: j,jmid,jloc, jvloc
  INTEGER :: k,kmid,kloc,ktab, kwloc
  INTEGER :: kfold
  REAL :: xmid,ymid,zmid,xumid,yvmid,zwmid
  REAL :: wx,wy,w11,w12,w21,w22,whi,wlo,wqhi,wqlo
  REAL :: ddrot,ugrid,vgrid,prgrid,tgrid,qvgrid,qvsgrid,rhgrid,wgrid
  REAL :: vr_miss,qv_miss
  REAL :: azim,dist,vr,vrdiff,avrdif,dz,eleva,range,dsdrinv
  REAL :: wux,wu11,wu12,wu21,wu22
  REAL :: wvy,wv11,wv12,wv21,wv22
  REAL :: whiw, wlow

  INTEGER,parameter:: iseed=-100
  INTEGER :: iseed1
  REAL,  PARAMETER :: errVr = SQRT(2.0)
! REAL,parameter:: errVr = 1.0

!
!-----------------------------------------------------------------------
!
!  Include file
!
!-----------------------------------------------------------------------
!
  INCLUDE 'phycst.inc'
  INCLUDE 'mp.inc'

!
!-----------------------------------------------------------------------
!
!  External function
!
!-----------------------------------------------------------------------
!
  REAL :: gasdev
!
!-----------------------------------------------------------------------
!
!  QC and conversion parameters
!
!  vrqcthr   maximum radial velocity diff from background to use
!  vrcnvthr  lower limit of the dot product between the radial
!            direction and the analysis wind component to use data
!
!-----------------------------------------------------------------------
!
  REAL :: vrmax,vrqcthr,uazmlim
  PARAMETER (vrmax=70.,                                                 &
             vrqcthr=15.,   & ! maximum vrdiff from background to use
             uazmlim=0.087)   ! sin(5 degrees)
  REAL :: refmax, refmin1, refmin2, refmin
  PARAMETER (refmax = 80.)  ! maximum non-missing reflectivity
  PARAMETER (refmin1= 25.)  ! maximin non-missing reflectivity
  PARAMETER (refmin2= 15.)  ! maximin non-missing reflectivity

  REAL, PARAMETER :: epislon = 1.0E-3
  REAL            :: dxmin, dymin

  INTEGER :: nxlg, nylg, iloclg, jloclg
  INTEGER :: ibdyw, ibdye, jbdys, jbdyn, ijbdy
  REAL    :: qbackloc

  LOGICAL :: hgt_good, ref_valid, vel_valid
!
!-----------------------------------------------------------------------
!
!  Function f_qvsat and inline directive for Cray PVP
!
!-----------------------------------------------------------------------
!
  REAL :: f_qvsat

!fpp$ expand (f_qvsat)
!!dir$ inline always f_qvsat
!*$*  inline routine (f_qvsat)
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  dxmin = (xs(3)-xs(2))*epislon
  dymin = (ys(3)-ys(2))*epislon

  nxlg  = (nx-3)*nproc_x + 3
  nylg  = (ny-3)*nproc_y + 3

!-----------------------------------------------------------------------
!
!  Some initializations
!
!-----------------------------------------------------------------------
!
  iseed1 = (iseed-20*iensmbl+icycle)*(myproc+1)
  vr_miss=-999.
  qv_miss=-999.
!
!-----------------------------------------------------------------------
!
!  Get x and y locations of each radar ob
!
!-----------------------------------------------------------------------
!
  CALL lltoxy(ncolrad,1,latradc,lonradc,xradc,yradc)
!
  imid=nx/2
  xmid=xs(imid);  xumid = x(imid)
  jmid=ny/2
  ymid=ys(jmid);  yvmid = y(jmid)
  kmid=nz/2

  DO icol=1,ncolrad

    IF(mp_opt > 0 .AND. indexrad(icol) .NE. myproc) CYCLE

    IF( irad(icol) > 0 .AND. nlevrad(icol) > 0) THEN
      IF( xradc(icol) > xs(1)-dxmin .AND. xradc(icol) < xs(nx-1)+dxmin .AND.      &
          yradc(icol) > ys(1)-dymin .AND. yradc(icol) < ys(ny-1)+dymin ) THEN
!
!-----------------------------------------------------------------------
!
!  Find column in grid for mass points
!
!-----------------------------------------------------------------------
!
        IF(xradc(icol) < xmid) THEN
          DO i=imid,2,-1
            IF(xs(i) < xradc(icol)) EXIT
          END DO
          iloc=i
        ELSE
          DO i=imid,nx-1
            IF(xs(i) > xradc(icol)) EXIT
          END DO
          iloc=i-1
        END IF
        wx =  (xradc(icol)-xs(iloc))/ (xs(iloc+1)-xs(iloc))

        IF(yradc(icol) < ymid) THEN
          DO j=jmid,2,-1
            IF(ys(j) < yradc(icol)) EXIT
          END DO
          jloc=j
        ELSE
          DO j=jmid,ny-1
            IF(ys(j) > yradc(icol)) EXIT
          END DO
          jloc=j-1
        END IF
        wy = (yradc(icol)-ys(jloc))/(ys(jloc+1)-ys(jloc))

!
!-----------------------------------------------------------------------
!
!  Find column in grid for U points
!
!-----------------------------------------------------------------------
!
        IF(xradc(icol) < xumid) THEN
          DO i=imid,2,-1
            IF(x(i) < xradc(icol)) EXIT
          END DO
          iuloc=i
        ELSE
          DO i=imid,nx-1
            IF(x(i) > xradc(icol)) EXIT
          END DO
          iuloc=i-1
        END IF
        wux =  (xradc(icol)-x(iuloc))/ (x(iuloc+1)-x(iuloc))

!
!-----------------------------------------------------------------------
!
!  Find column in grid for V points
!
!-----------------------------------------------------------------------
!
        IF(yradc(icol) < yvmid) THEN
          DO j=jmid,2,-1
            IF(y(j) < yradc(icol)) EXIT
          END DO
          jvloc=j
        ELSE
          DO j=jmid,ny-1
            IF(y(j) > yradc(icol)) EXIT
          END DO
          jvloc=j-1
        END IF
        wvy = (yradc(icol)-y(jvloc))/(y(jvloc+1)-y(jvloc))
!
!-----------------------------------------------------------------------
!
!  Determine bilinear interpolation weights
!
!-----------------------------------------------------------------------
!
        w22 = wx*wy
        w12 = (1.0 - wx) * wy
        w21 = wx * (1.0 - wy)
        w11 = (1.0 - wx) * (1.0 - wy)

        wu22 = wux*wy
        wu12 = (1.0 - wux) * wy
        wu21 = wux * (1.0 - wy)
        wu11 = (1.0 - wux) * (1.0 - wy)

        wv22 = wx*wvy
        wv12 = (1.0 - wx) * wvy
        wv21 = wx * (1.0 - wvy)
        wv11 = (1.0 - wx) * (1.0 - wvy)

!
!-----------------------------------------------------------------------
!
!  Find heading and distance from radar along ground
!  Store correlation of azimuth with u and v drections
!  in uazmrad and vazmrad, respectively.
!
!-----------------------------------------------------------------------
!
        jrad=irad(icol)
        CALL disthead(latradc(icol),lonradc(icol),                      &
                      latrad(jrad),lonrad(jrad),                        &
                      azim,dist)
        distrad(icol)=dist

        CALL ddrotuv(1,lonradc(icol),azim,1.,ddrot,                     &
                       uazmrad(icol),vazmrad(icol))
!
!    in between added by GAO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!     CALL lltoxy(1,1,latradc(icol),lonradc(icol),xx1,yy1)
!     CALL lltoxy(1,1,latrad(jrad),lonrad(jrad),xx2,yy2)
!      uazmrad(icol)=xx1-xx2
!      vazmrad(icol)=yy1-yy2
!
!    in between added by GAO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!-----------------------------------------------------------------------
!
!  Interpolate grid heights in horizontal for mass grid
!
!-----------------------------------------------------------------------
!
        DO k=1,nz-1
          tem1(k)=w11*zps(  iloc,  jloc,k) +                            &
                  w21*zps(iloc+1,  jloc,k) +                            &
                  w12*zps(  iloc,jloc+1,k) +                            &
                  w22*zps(iloc+1,jloc+1,k)
        END DO
        !trnradc(icol)=0.5*(tem1(1)+tem1(2))

!-----------------------------------------------------------------------
!
!  Interpolate grid heights in horizontal for W grid
!
!-----------------------------------------------------------------------
!
        DO k=1,nz-1
          tem2(k)=w11*zp(  iloc,  jloc,k) +                             &
                  w21*zp(iloc+1,  jloc,k) +                             &
                  w12*zp(  iloc,jloc+1,k) +                             &
                  w22*zp(iloc+1,jloc+1,k)
        END DO
        trnradc(icol)=tem1(2)
!
!-----------------------------------------------------------------------
!
!  Loop in height
!
!-----------------------------------------------------------------------
!
        DO kr=1,nlevrad(icol)

          IF ( hgtradc(kr,icol) >= 1500.0) THEN
            refmin =refmin2
          ELSE
            refmin =refmin1
          END IF

!#ifdef RADIAL
!dr. wang modification for the seprated processing of the vr and mrms ref.
          hgt_good  = .FALSE.
          ref_valid = .FALSE.
          vel_valid = .FALSE.

          IF ( hgtradc(kr,icol) >= tem1(1) .AND. hgtradc(kr,icol) <= tem1(nz-1) ) THEN
            hgt_good = .TRUE.
          END IF

          IF ( ABS(obsrad(2,kr,icol)) < vrmax ) THEN
            vel_valid = .TRUE.
          END IF

          IF (mosaicopt == 0) THEN  ! ref_mos_3d from other sources
            IF (myproc == 0 .AND. icol == 1 .AND. kr == 1) &
                WRITE(*,'(1x,a,I0)') 'Radar mosaic source is MRMS in prcssrad, mosaicopt = ',mosaicopt
            IF (ABS(ref_mos_3d(iradc(icol),jradc(icol),kradc(kr,icol))) < refmax   .AND.   &
                ABS(ref_mos_3d(iradc(icol),jradc(icol),kradc(kr,icol))) > refmin ) THEN
                ref_valid = .TRUE.
            END IF
          ELSE                      ! ref_mos_3d from the same file as radial velocity
            IF (myproc == 0 .AND. icol == 1 .AND. kr == 1) &
                WRITE(*,'(1x,a,I0)') 'Radar mosaic source is NEXRAD in prcssrad, mosaicopt = ',mosaicopt
            IF ( obsrad(1,kr,icol) < refmax .AND. obsrad(1,kr,icol) > refmin ) THEN
              ref_valid = .TRUE.
            END IF
          END IF

          IF( ( vel_valid .AND. ref_valid ) .AND. hgt_good ) THEN

!          IF( ( ABS(obsrad(2,kr,icol)) < vrmax  .AND.                   &
!                ABS(ref_mos_3d(iradc(icol),jradc(icol),kradc(kr,icol))) < refmax   .AND.   &
!                ABS(ref_mos_3d(iradc(icol),jradc(icol),kradc(kr,icol))) > refmin ) .AND.   &
!              ( hgtradc(kr,icol) >= tem1(1)     .AND.                   &
!                hgtradc(kr,icol) <= tem1(nz-1) ) ) THEN
!!!!dr.wang
!#else
!          IF( ( ABS(obsrad(2,kr,icol)) < vrmax  .AND.                   &
!                ABS(obsrad(1,kr,icol)) < refmax .AND.                   &
!                ABS(obsrad(1,kr,icol)) > refmin )      .AND.            &
!              ( hgtradc(kr,icol) >= tem1(1)     .AND.                   &
!                hgtradc(kr,icol) <= tem1(nz-1) ) ) THEN
!#endif
            dz=hgtradc(kr,icol)-elvrad(irad(icol))
            CALL beamelv(dz,dist,eleva,range)
            !write(0,*) 'eleva = ',eleva,dz,dist

!g          CALL dhdrange(eleva,range,dhdr)
!g          dsdr=SQRT(MAX(0.,(1.-dhdr*dhdr)))

            CALL dhdrange( eleva,range,dhdr(kr,icol) )
            dsdr(kr,icol)=SQRT(AMAX1(0.,(1.-dhdr(kr,icol)*dhdr(kr,icol))))

!           IF(dsdr /= 0.) THEN
!             dsdrinv=1./dsdr
!           ELSE
!             dsdrinv=0.
!           END IF

            IF(dsdr(kr,icol) /= 0.) THEN
              dsdrinv=1./dsdr(kr,icol)
            ELSE
              dsdrinv=0.
            END IF
!
!-----------------------------------------------------------------------
!
!  Find z location in mass grid
!
!-----------------------------------------------------------------------
!
            zmid=tem1(kmid)
            IF(hgtradc(kr,icol) < zmid) THEN
              DO k=kmid,2,-1
                IF(tem1(k) < hgtradc(kr,icol)) EXIT
              END DO
              kloc=k
            ELSE
              DO k=kmid,nz-1
                IF(tem1(k) > hgtradc(kr,icol)) EXIT
              END DO
              kloc=k-1
            END IF

!
!-----------------------------------------------------------------------
!
!  Find z location in W grid
!
!-----------------------------------------------------------------------
!
            zwmid=tem2(kmid)
            IF(hgtradc(kr,icol) < zwmid) THEN
              DO k=kmid,2,-1
                IF(tem2(k) < hgtradc(kr,icol)) EXIT
              END DO
              kwloc=k
            ELSE
              DO k=kmid,nz-1
                IF(tem2(k) > hgtradc(kr,icol)) EXIT
              END DO
              kwloc=k-1
            END IF

!
!-----------------------------------------------------------------------
!
!  Set z weights
!
!-----------------------------------------------------------------------
!
            whi = (hgtradc(kr,icol)-tem1(kloc))/                        &
                  (tem1(kloc+1)-tem1(kloc))
            wlo = 1.0 - whi

            whiw = (hgtradc(kr,icol)-tem2(kwloc))/                      &
                  (tem2(kwloc+1)-tem2(kwloc))
            wlow = 1.0 - whiw

!
!-----------------------------------------------------------------------
!
!  Set z weights for error data
!
!-----------------------------------------------------------------------
!
            DO k=2,nlvqback-1
              IF(hqback(k) > hgtradc(kr,icol)) EXIT
            END DO
            ktab=k-1
            wqhi = (hgtradc(kr,icol)-hqback(ktab))/                     &
                   (hqback(ktab+1)-hqback(ktab))
            wqlo = 1.0 - wqhi

            qbackloc = wqhi*qback(1,ktab+1)+wqlo*qback(1,ktab)  ! Use qback for U
!
!-----------------------------------------------------------------------
!
!  Interpolate background field to radar location
!
!  Note this is dependent on indices set so that
!  u=1,v=2,theta=4
!  This can be made more general in a future version.
!
!-----------------------------------------------------------------------
!
            ugrid=                                                      &
                wlo* (wu11*anx(iuloc,  jloc,   kloc,   1) +             &
                      wu21*anx(iuloc+1,jloc,   kloc,   1) +             &
                      wu12*anx(iuloc,  jloc+1, kloc,   1) +             &
                      wu22*anx(iuloc+1,jloc+1, kloc,   1))              &
              + whi* (wu11*anx(iuloc,  jloc,   kloc+1, 1) +             &
                      wu21*anx(iuloc+1,jloc,   kloc+1, 1) +             &
                      wu12*anx(iuloc,  jloc+1, kloc+1, 1) +             &
                      wu22*anx(iuloc+1,jloc+1, kloc+1, 1))
            vgrid=                                                      &
                wlo* (wv11*anx(iloc,   jvloc,  kloc,   2) +             &
                      wv21*anx(iloc+1, jvloc,  kloc,   2) +             &
                      wv12*anx(iloc,   jvloc+1,kloc,   2) +             &
                      wv22*anx(iloc+1, jvloc+1,kloc,   2))              &
              + whi* (wv11*anx(iloc,   jvloc,  kloc+1, 2) +             &
                      wv21*anx(iloc+1, jvloc,  kloc+1, 2) +             &
                      wv12*anx(iloc,   jvloc+1,kloc+1, 2) +             &
                      wv22*anx(iloc+1, jvloc+1,kloc+1, 2))
            prgrid=                                                     &
                wlo* (w11*anx(iloc,    jloc,   kloc,   3) +             &
                      w21*anx(iloc+1,  jloc,   kloc,   3) +             &
                      w12*anx(iloc,    jloc+1, kloc,   3) +             &
                      w22*anx(iloc+1,  jloc+1, kloc,   3))              &
              + whi* (w11*anx(iloc,    jloc,   kloc+1, 3) +             &
                      w21*anx(iloc+1,  jloc,   kloc+1, 3) +             &
                      w12*anx(iloc,    jloc+1, kloc+1, 3) +             &
                      w22*anx(iloc+1,  jloc+1, kloc+1, 3))
            theradc(kr,icol)=                                           &
                wlo* (w11*anx(iloc,    jloc,   kloc,   4) +             &
                      w21*anx(iloc+1,  jloc,   kloc,   4) +             &
                      w12*anx(iloc,    jloc+1, kloc,   4) +             &
                      w22*anx(iloc+1,  jloc+1, kloc,   4))              &
              + whi* (w11*anx(iloc,    jloc,   kloc+1, 4) +             &
                      w21*anx(iloc+1,  jloc,   kloc+1, 4) +             &
                      w12*anx(iloc,    jloc+1, kloc+1, 4) +             &
                      w22*anx(iloc+1,  jloc+1, kloc+1, 4))
            qvgrid=                                                     &
                wlo* (w11*anx(iloc,    jloc,   kloc,   5) +             &
                      w21*anx(iloc+1,  jloc,   kloc,   5) +             &
                      w12*anx(iloc,    jloc+1, kloc,   5) +             &
                      w22*anx(iloc+1,  jloc+1, kloc,   5))              &
              + whi* (w11*anx(iloc,    jloc,   kloc+1, 5) +             &
                      w21*anx(iloc+1,  jloc,   kloc+1, 5) +             &
                      w12*anx(iloc,    jloc+1, kloc+1, 5) +             &
                      w22*anx(iloc+1,  jloc+1, kloc+1, 5))

            wgrid=                                                      &
                wlow* (w11*anx(iloc,   jloc,   kwloc,  6) +             &
                       w21*anx(iloc+1, jloc,   kwloc,  6) +             &
                       w12*anx(iloc,   jloc+1, kwloc,  6) +             &
                       w22*anx(iloc+1, jloc+1, kwloc,  6))              &
              + whiw* (w11*anx(iloc,   jloc,   kwloc+1,6) +             &
                       w21*anx(iloc+1, jloc,   kwloc+1,6) +             &
                       w12*anx(iloc,   jloc+1, kwloc+1,6) +             &
                       w22*anx(iloc+1, jloc+1, kwloc+1,6))


            iloclg = iloc + (nx-3)*(loc_x-1)
            jloclg = jloc + (ny-3)*(loc_y-1)
            ibdyw = qobsrad_bdyzone(ipass)+1-iloclg;        IF (ibdyw < 0) ibdyw = 0
            ibdye = iloclg-(nxlg-qobsrad_bdyzone(ipass)-1); IF (ibdye < 0) ibdye = 0
            jbdys = qobsrad_bdyzone(ipass)+1-jloclg;        IF (jbdys < 0) jbdys = 0
            jbdyn = jloclg-(nylg-qobsrad_bdyzone(ipass)-1); IF (jbdyn < 0) jbdyn = 0

            ijbdy = MAX(ibdyw,ibdye,jbdys,jbdyn)

            IF(ABS(obsrad(2,kr,icol)) < vrmax .AND. raduvobs > 0) THEN

!-----------------------------------------------------------------------
!
!  Find radial velocity
!  This assumes motions are quasi-horizontal.
!
!-----------------------------------------------------------------------
!
              vr=(uazmrad(icol)*ugrid + vazmrad(icol)*vgrid) * dsdr(kr,icol) &
                                                      + wgrid*dhdr(kr,icol)
              oanxrad(2,kr,icol)=vr

!
!-----------------------------------------------------------------------
!
!  Check for folding
!
!-----------------------------------------------------------------------
!
              vrdiff=obsrad(2,kr,icol)-vr
              IF(obsrad(3,kr,icol) > 0.0) THEN  ! Check that Nyquist velocity is sane
!
!---------------for idealized dataset this has to be skipped---gao------
!-------------------one parameter should be added in the future--------

                IF(idealCase_opt==0) THEN
                  kfold=nint(vrdiff/(2.*obsrad(3,kr,icol)))
                ELSE
                  kfold=0.0
                END IF

                obsrad(2,kr,icol)=obsrad(2,kr,icol)-                    &
                                  kfold*2.*obsrad(3,kr,icol)
                vrdiff=obsrad(2,kr,icol)-vr
              END IF
              avrdif=ABS(vrdiff)
!
!-----------------------------------------------------------------------
!
!  QC check and calculation of increments.
!  QC should be more sophisticated at some point.
!
!-----------------------------------------------------------------------
!
              IF( avrdif < qcthrradv(isrcrad(irad(icol))) ) THEN
!
!-----------------------------------------------------------------------
!
!  Find increment in analysis u and v needed to make vrdiff zero
!
!-----------------------------------------------------------------------
!
                IF(iensmbl==0) THEN
                  !obsrad(2,kr,icol)=obsrad(2,kr,icol)
                  odifrad(2,kr,icol)=obsrad(2,kr,icol)-vr
                ELSE
                  obsrad(2,kr,icol)=obsrad(2,kr,icol)+errVr*GASDEV(iseed1)
                  odifrad(2,kr,icol)=obsrad(2,kr,icol)-vr
                END IF
!
!-----------------------------------------------------------------------
!
!  Assign obs error to radial velocity according to observation angle
!
!-----------------------------------------------------------------------
!
                !qobsrad(2,kr,icol)=qsrcrad(2,isrcrad(irad(icol)))
                CALL radarobs_quality(qobsrad_bdyopt,qsrcrad(2,isrcrad(irad(icol))),       &
                          ijbdy,qobsrad_bdyzone(ipass),qbackloc,qback_factor,              &
                          qobsrad(2,kr,icol),qualrad(2,kr,icol),istatus)

                !qobsrad(2,kr,icol)=radarobs_quality(qobsrad_bdyopt,     &
                !                  qsrcrad(2,isrcrad(irad(icol))),       &
                !                  ijbdy,qobsrad_bdyzone,qbackloc,qback_factor)
                !qualrad(2,kr,icol)=10

                !WRITE(myproc+1000+qobsrad_bdyopt*10,'(1x,3I5,4(I4,a),I5,F10.2,I4)') ipass,icol,kr,iloc,'/',iloclg,';',jloc,'/',jloclg,',',ijbdy,qobsrad(2,kr,icol),qualrad(2,kr,icol)

              ELSE !IF NOT ( avrdif < qcthrrad(2,isrcrad(irad(icol))) )

                odifrad(1,kr,icol)=vr_miss
                odifrad(2,kr,icol)=vr_miss
                qobsrad(1,kr,icol)=vr_miss
                qobsrad(2,kr,icol)=vr_miss
                qualrad(1,kr,icol)=-99
                qualrad(2,kr,icol)=-99

              END IF

            ELSE !IF NOT (ABS(obsrad(2,kr,icol)) < vrmax .AND. raduvobs > 0)

              odifrad(1,kr,icol)=vr_miss
              odifrad(2,kr,icol)=vr_miss
              qobsrad(1,kr,icol)=vr_miss
              qobsrad(2,kr,icol)=vr_miss
              qualrad(1,kr,icol)=-99
              qualrad(2,kr,icol)=-99

            END IF
!
!-----------------------------------------------------------------------
!
!    Where reflectivity is greater than that
!    assumed for "saturation" assign an rhs corresponding to
!    rh = rhind, but not if the background rh is greater
!    than rhradobs (background more saturated).
!
!-----------------------------------------------------------------------
!
            IF(radrhobs > 0 .AND. obsrad(1,kr,icol) > refrh .AND.       &
               obsrad(1,kr,icol) < refmax ) THEN
              tgrid=theradc(kr,icol)*(prgrid/p0)**rddcp
              qvsgrid=f_qvsat(prgrid,tgrid)
              rhgrid=qvgrid/qvsgrid
              IF( rhgrid < rhradobs ) THEN
                odifrad(1,kr,icol) = obsrad(1,kr,icol)-qvgrid
                oanxrad(1,kr,icol) = qvgrid
                qualrad(1,kr,icol) = 10
                qobsrad(1,kr,icol) = qsrcrad(1,isrcrad(irad(icol)))*    &
                                     qvsgrid
              ELSE
                odifrad(1,kr,icol) = qv_miss
                oanxrad(1,kr,icol) = qv_miss
                qobsrad(1,kr,icol) = qv_miss
                qualrad(1,kr,icol) =-99
              END IF
            ELSE !IF NOT (radrhobs > 0 .AND. obsrad(1,kr,icol) > refrh .AND.
            	   !obsrad(1,kr,icol) < refmax )
              odifrad(1,kr,icol) = qv_miss
              oanxrad(1,kr,icol) = qv_miss
              qobsrad(1,kr,icol) = qv_miss
              qualrad(1,kr,icol) =-99
            END IF

          ELSE !IF NOT ( ( ABS(obsrad(2,kr,icol)) < vrmax  .AND.
               ! ABS(obsrad(1,kr,icol)) < refmax .AND.
               ! ABS(obsrad(1,kr,icol)) > refmin ).AND.
               ! ( hgtradc(kr,icol) >= tem1(1)    .AND.
               ! hgtradc(kr,icol) <= tem1(nz-1) )

            odifrad(1,kr,icol)=vr_miss
            odifrad(2,kr,icol)=vr_miss
            qobsrad(1,kr,icol)=vr_miss
            qobsrad(2,kr,icol)=vr_miss
            qualrad(1,kr,icol)=-99
            qualrad(2,kr,icol)=-99
            odifrad(1,kr,icol) = qv_miss
            oanxrad(1,kr,icol) = qv_miss
            qobsrad(1,kr,icol) = qv_miss
            qualrad(1,kr,icol) =-99

          END IF

        END DO
      ELSE
        irad(icol)=0
      END IF

    END IF
  END DO

  RETURN
END SUBROUTINE prcssrad
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE PRCSSRADTLT                ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE prcsstlt(nx,ny,nz,nz_tab,nvar_anx,nvar_rad,                  &
           nrad_anx,mx_rad,nsrc_rad,nz_rdr,mx_colrad,                   &
           raduvobs,radrhobs,refrh,rhradobs,                            &
           xs,ys,zs,hterain,anx,qback,hqback,nlvqback,                  &
           srcrad,qsrcrad,qcthrradv,                                    &
           isrcrad,stnrad,latrad,lonrad,elvrad,elvang,bmwidth,          &
           latradc,lonradc,xradc,yradc,irad,nlevrad,                    &
           hgtradc,obsrad,oanxrad,odifrad,iradc,jradc,                  &
           distrad,uazmrad,vazmrad,                                     &
           theradc,trnradc,qobsrad,qualrad,dsdr,dhdr,eleva,k3L,k3U,     &
           ncolrad,indexrad,istatus,tem1)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  QC column-tilt radar data, compute innovation
!  For MPI, data point owners also get assigned.
!
!  AUTHOR: Guoqing Ge
!  May, 2013
!
!  MODIFICATION HISTORY:
!
!   Modified based on prcssrad
!   6/20/2013 MPI version not tested.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nvar_anx   number of analysis variables
!    nvar_rad   number of variables in the obsrad array
!    nrad_anx   number of variables in the odifrad array
!    mx_rad     maximum number of radars
!    nsrc_rad   number of radar sources
!    nz_rdr     maximum number of levels in a radar column
!    mx_colrad  maximum number of radar columns
!
!    xs         x location of scalar pts (m)
!    ys         y location of scalar pts (m)
!    zs         z location of scalar pts (m)
!    hterain    height of terrain (m MSL)
!    anx        analyzed variables (or first guess)
!    qback      background (first guess) error
!    srcrad   name of radar sources
!    qsrcrad  radar observation error
!    isrcrad  index of radar source
!    stnrad   radar site name    character*5
!    latrad   latitude of radar  (degrees N)
!    lonrad   longitude of radar (degrees E)
!    elvrad   elevation of feed horn of radar (m MSL)
!    latradc  latitude of radar column   (degrees N)
!    lonradc  longitude of radar column  (degrees E)
!    xradc    x location of radar column (m)
!    yradc    y location of radar column (m)
!    irad     radar number
!    nlevrad  number of levels of radar data in each column
!    distrad  distance of radar column from source radar
!    uazmrad  u-component of radar beam for each column
!    vazmrad  v-component of radar beam for each column
!    hgtradc  height (m MSL) of radar observations
!    obsrad   radar observations
!
!    indexrad index of data point processor owners (MPI only)
!
!  OUTPUT:
!
!    oanxrad  analysis (first guess) value at radar data location
!    odifrad  difference between radar observation and analysis
!    theradc  theta (potential temperature K) at each radar ob
!    trnradc  terrain height at each radar column
!    qobsrad  normalized observation error
!    qualrad  radar data quality indicator
!    ncolrad  number of radar columns read-in
!    istatus  status indicator
!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
  INTEGER :: nx,ny,nz,nz_tab,nvar_anx
  INTEGER :: nvar_rad,nrad_anx,mx_rad,nsrc_rad,nz_rdr,mx_colrad
  INTEGER :: raduvobs
  INTEGER :: radrhobs
  REAL :: refrh
  REAL :: rhradobs
!
!-----------------------------------------------------------------------
!
!  Grid variables
!
!-----------------------------------------------------------------------
!
  REAL :: xs(nx)
  REAL :: ys(ny)
  REAL :: zs(nx,ny,nz)
  REAL :: hterain(nx,ny)
  REAL :: anx(nx,ny,nz,nvar_anx)
  REAL :: qback(nvar_anx,nz_tab)
  REAL :: hqback(nz_tab)
  INTEGER :: nlvqback
!
!-----------------------------------------------------------------------
!
!  Radar site variables
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=8) :: srcrad(nsrc_rad)
  INTEGER :: isrcrad(0:mx_rad)
  REAL :: qsrcrad(nvar_rad,nsrc_rad)
  REAL :: qcthrradv(nsrc_rad)
  REAL :: latrad(mx_rad),lonrad(mx_rad)
  REAL :: elvrad(mx_rad),bmwidth(mx_rad)
  REAL ::   elvang(nz_rdr,mx_rad) !elevation angles in a column
  CHARACTER (LEN=5) :: stnrad(mx_rad)
!
!-----------------------------------------------------------------------
!
!  Radar observation variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: irad(mx_colrad)
  INTEGER :: nlevrad(mx_colrad)
  REAL    :: latradc(mx_colrad),lonradc(mx_colrad)
  REAL    :: xradc(mx_colrad),yradc(mx_colrad)
  INTEGER :: iradc(mx_colrad),jradc(mx_colrad)
  REAL    :: hgtradc(nz_rdr,mx_colrad)
  REAL    :: theradc(nz_rdr,mx_colrad)
  INTEGER :: k3L(nz_rdr,mx_colrad), k3U(nz_rdr,mx_colrad)
  REAL    :: trnradc(mx_colrad)
  !REAL    :: dsdr(nz_rdr,mx_colrad)
  !REAL    :: dhdr(nz_rdr,mx_colrad)
  REAL    :: dsdr(nz,mx_colrad)
  REAL    :: dhdr(nz,mx_colrad)
  REAL    :: eleva(nz,mx_colrad)
  REAL    :: vrsc(nz)  !Vr at grid points in a column

  REAL    :: obsrad(nvar_rad,nz_rdr,mx_colrad)
  REAL    :: oanxrad(nrad_anx,nz_rdr,mx_colrad)
  REAL    :: odifrad(nrad_anx,nz_rdr,mx_colrad)
  REAL    :: qobsrad(nrad_anx,nz_rdr,mx_colrad)
  REAL    :: distrad(mx_colrad),uazmrad(mx_colrad),vazmrad(mx_colrad)
  INTEGER :: qualrad(nrad_anx,nz_rdr,mx_colrad)
  INTEGER :: ncolrad
  INTEGER :: istatus
!
!-----------------------------------------------------------------------
!
!  MPI variables
!
!-----------------------------------------------------------------------
!
  INTEGER, INTENT(IN) :: indexrad(mx_colrad)
!
!-----------------------------------------------------------------------
!
!  Temporary work array
!
!-----------------------------------------------------------------------
!
  REAL :: tem1(nx*ny*nz), temAng, height
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: icol,jrad,kr
  INTEGER :: kfold, k, kmid
  REAL :: ddrot,ugrid,vgrid,prgrid,tgrid,qvgrid,qvsgrid,rhgrid,wgrid
  REAL :: vr_miss,qv_miss
  REAL :: azim,dist,vr,vrdiff,avrdif,dz,rngsc
!
!-----------------------------------------------------------------------
!
!  Include file
!
!-----------------------------------------------------------------------
!
  INCLUDE 'phycst.inc'
  INCLUDE 'mp.inc'
!
!-----------------------------------------------------------------------
!
!  QC and conversion parameters
!
!  vrqcthr   maximum radial velocity diff from background to use
!
!-----------------------------------------------------------------------
!
  REAL :: vrmax,vrqcthr,uazmlim
  PARAMETER (vrmax=70.,                                                 &
             vrqcthr=15.,   & ! maximum vrdiff from background to use
         uazmlim=0.087) ! sin(5 degrees)
  REAL :: refmax, refmin1, refmin2, refmin
  PARAMETER (refmax = 80.)  ! maximum non-missing reflectivity
  PARAMETER (refmin1= 25.)  ! maximin non-missing reflectivity
  PARAMETER (refmin2= 15.)  ! maximin non-missing reflectivity
!
!-----------------------------------------------------------------------
!
!  Function f_qvsat and inline directive for Cray PVP
!
!-----------------------------------------------------------------------
!
  REAL :: f_qvsat

!fpp$ expand (f_qvsat)
!!dir$ inline always f_qvsat
!*$*  inline routine (f_qvsat)

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!-----------------------------------------------------------------------
!
!  Some initializations
!
!-----------------------------------------------------------------------
!
  vr_miss=-999.
  qv_miss=-999.
!
!  Get x and y locations of each radar ob
!
  CALL lltoxy(ncolrad,1,latradc,lonradc,xradc,yradc)
!
!-----------------------------------------------------------------------
! Loop through all columns
!-----------------------------------------------------------------------
!
  DO icol=1,ncolrad

    IF(mp_opt > 0 .AND. indexrad(icol) .NE. myproc) CYCLE

    IF( irad(icol) > 0 .AND. nlevrad(icol) > 0) THEN
      IF( xradc(icol) >= xs(1) .AND. xradc(icol) <= xs(nx-1) .AND.      &
          yradc(icol) >= ys(1) .AND. yradc(icol) <= ys(ny-1)) THEN
!
!  compute distrad(icol), uazmrad(icol), vazmrad(icol)
!
        jrad=irad(icol)
        CALL disthead(latradc(icol),lonradc(icol),                      &
                      latrad(jrad),lonrad(jrad),                        &
                      azim,dist)
        distrad(icol)=dist

        CALL ddrotuv(1,lonradc(icol),azim,1.,ddrot,                     &
                       uazmrad(icol),vazmrad(icol))

        DO k=1,nz-1
          tem1(k) = zs (iradc(icol), jradc(icol), k)
        END DO
!
!  compute vrsc(k)
!
        DO k = 2, nz-1
          dz=zs(iradc(icol),jradc(icol),k)-elvrad(irad(icol))
          CALL beamelv(dz,dist,eleva(k,icol),rngsc)
          CALL dhdrange( eleva(k,icol),rngsc,dhdr(k,icol) )
          dsdr(k,icol)=SQRT(AMAX1(0.,(1.-dhdr(k,icol)*dhdr(k,icol))))

          ugrid = 0.5*(anx(iradc(icol),jradc(icol),k, 1)+anx(iradc(icol)+1,jradc(icol),  k, 1))
          vgrid = 0.5*(anx(iradc(icol),jradc(icol),k, 2)+anx(iradc(icol),  jradc(icol)+1,k, 2))
          wgrid = 0.5*(anx(iradc(icol),jradc(icol),k, 6)+anx(iradc(icol),  jradc(icol),  k+1, 6))

          vrsc(k)=(uazmrad(icol)*ugrid + vazmrad(icol)*vgrid)           &
                 * dsdr(k,icol) + wgrid*dhdr(k,icol)
        END DO

!
!-----------------------------------------------------------------------
!  Loop in height
!-----------------------------------------------------------------------
!
        DO kr=1,nlevrad(icol)

          IF ( hgtradc(kr,icol) >= 1500.0) THEN
            refmin =refmin2
          ELSE
            refmin =refmin1
          END IF
!
! to determine the k3U(kr,icol), k3L(kr,icol)
!
          kmid = nz/2
          temAng = elvang(kr,irad(icol)) - bmwidth(irad(icol))/2
          CALL bmhgt_elvs(temAng, dist, height)
          height = height + elvrad(irad(icol))
          IF(height < tem1(kmid) ) THEN
            DO k=kmid,2,-1
              IF(height > tem1(k)) EXIT
            END DO
            k3L(kr,icol)=k+1
          ELSE
            DO k=kmid,nz-1
              IF(height < tem1(k)) EXIT
            END DO
            k3L(kr,icol)=k-1
          END IF
          IF (k3L(kr,icol) < 2) k3L(kr,icol) = 2
        !--k3U(kr, icol)
          temAng = elvang(kr,irad(icol)) + bmwidth(irad(icol))/2
          CALL bmhgt_elvs(temAng, dist, height)
          height = height + elvrad(irad(icol))
          IF(height < tem1(kmid) ) THEN
            DO k=kmid,2,-1
              IF(height > tem1(k)) EXIT
            END DO
            k3U(kr,icol)=k+1
          ELSE
            DO k=kmid,nz-1
              IF(height < tem1(k)) EXIT
            END DO
            k3U(kr,icol)=k-1
          END IF
          IF (k3U(kr,icol) > nz-1) k3U(kr,icol) = nz-1

          IF( ( ABS(obsrad(2,kr,icol)) < vrmax  .AND. raduvobs>0 .AND.  &
                ABS(obsrad(1,kr,icol)) < refmax .AND.                   &
                ABS(obsrad(1,kr,icol)) > refmin )      .AND.            &
              ( hgtradc(kr,icol) >= tem1(1)     .AND.                   &
                hgtradc(kr,icol) <= tem1(nz-1) )                        &
            ) THEN

            !to get model conterpart of Vr considering beam broadening
              CALL vrzwtavg1D_3dvar(1,nz,k3L(kr,icol), k3U(kr,icol), tem1,     &
                    eleva(:,icol),elvang(kr,irad(icol)),vrsc,bmwidth(irad(icol)),vr)
              oanxrad(2,kr,icol)=vr
!
!-----------------------------------------------------------------------
!  Check for folding
!-----------------------------------------------------------------------
!
              vrdiff=obsrad(2,kr,icol)-vr
             !! grid-tilted radar data does not contain nyquist information!
              !IF(obsrad(3,kr,icol) > 0.0) THEN ! Check that Nyquist velocity is sane
              !  kfold=nint(vrdiff/(2.*obsrad(3,kr,icol)))
              !  obsrad(2,kr,icol)=obsrad(2,kr,icol)-                        &
              !                    kfold*2.*obsrad(3,kr,icol)
              !  vrdiff=obsrad(2,kr,icol)-vr
              !END IF
              avrdif=ABS(vrdiff)
!
!-----------------------------------------------------------------------
!
!  QC check and calculation of increments.
!  QC should be more sophisticated at some point.
!
!-----------------------------------------------------------------------
!
              IF( avrdif < qcthrradv(isrcrad(irad(icol))) ) THEN
                odifrad(2,kr,icol)=obsrad(2,kr,icol)-vr
                qobsrad(2,kr,icol)=qsrcrad(2,isrcrad(irad(icol)))
                qualrad(2,kr,icol)=10
              ELSE !IF NOT ( avrdif < qcthrrad(2,isrcrad(irad(icol))) )
                odifrad(1,kr,icol)=vr_miss
                odifrad(2,kr,icol)=vr_miss
                qobsrad(1,kr,icol)=vr_miss
                qobsrad(2,kr,icol)=vr_miss
                qualrad(1,kr,icol)=-99
                qualrad(2,kr,icol)=-99
              END IF
          ELSE !IF NOT ( ( ABS(obsrad(2,kr,icol)) < vrmax  .AND.
               ! ABS(obsrad(1,kr,icol)) < refmax .AND.
               ! ABS(obsrad(1,kr,icol)) > refmin ).AND.
               ! ( hgtradc(kr,icol) >= tem1(1)    .AND.
               ! hgtradc(kr,icol) <= tem1(nz-1) )
            odifrad(1,kr,icol)=vr_miss
            odifrad(2,kr,icol)=vr_miss
            qobsrad(1,kr,icol)=vr_miss
            qobsrad(2,kr,icol)=vr_miss
            qualrad(1,kr,icol)=-99
            qualrad(2,kr,icol)=-99
          END IF

        END DO !Loop in height Do kr =1, nelvrad(icol)
      ELSE !IF( xradc(icol) >= xs(1) .AND. xradc(icol) <= xs(nx-1) .AND.      &
           !  yradc(icol) >= ys(1) .AND. yradc(icol) <= ys(ny-1))
        irad(icol)=0
      END IF
    END IF !IF( irad(icol) > 0 .AND. nlevrad(icol) > 0)
  END DO !Loop in all columns

  RETURN
END SUBROUTINE prcsstlt
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE RADMCRO                    ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE radmcro(nx,ny,nz,ref_mos_3d,                                 &
           radqvopt,radqcopt,radqropt,radptopt,                         &
           refsat,rhradobs,                                             &
           refcld,cldrad,ceilopt,ceilmin,dzfill,                        &
           refrain,radsetrat,radreflim,radptgain,                       &
           xs,ys,zs,zp,pr,pt,qv,ptbar,qvbar,rhobar,                     &
           qscalar,ceillim )
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Assign values to microphysical variables based on observed
!  radar data alone.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Keith Brewster
!  November, 1995
!
!  MODIFICATION HISTORY:
!  December, 1997
!  February, 1998
!  September, 1998 Jingsing Zong
!  Corrected logic after DO 150 to continue to next column.
!
!  April, 2010
!  Entirely rewritten to use the ref_mos_3d array instead of the individual
!  radar columns.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    xs         x location of scalar pts (m)
!    ys         y location of scalar pts (m)
!    zs         z location of scalar pts (m)
!
!  OUTPUT:
!
!    qc         cloud water mixing ratio
!
!  WORK ARRAYS
!
!    ceillim    Ceiling limit (m MSL)
!    tem1       Temporary array
!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INCLUDE 'globcst.inc'

  INTEGER :: nx,ny,nz
!
!-----------------------------------------------------------------------
!
!  Control parameters
!
!-----------------------------------------------------------------------
!
  INTEGER :: radqvopt,radqcopt,radqropt,radptopt
  REAL :: refsat,rhradobs,refcld,cldrad
  INTEGER :: ceilopt
  REAL :: ceilmin,dzfill
  REAL :: refrain
  REAL :: radsetrat,radreflim,radptgain
!
!-----------------------------------------------------------------------
!
!  Radar observation variables
!
!-----------------------------------------------------------------------
!
  REAL :: ref_mos_3d(nx,ny,nz)
!
  REAL :: xs(nx)
  REAL :: ys(ny)
  REAL :: zs(nx,ny,nz)
  REAL :: zp(nx,ny,nz)
  REAL :: pr(nx,ny,nz)
  REAL :: pt(nx,ny,nz)
  REAL :: qv(nx,ny,nz)
  REAL :: ptbar(nx,ny,nz)
  REAL :: qvbar(nx,ny,nz)
  REAL :: rhobar(nx,ny,nz)
  REAL :: qscalar(nx,ny,nz,nscalar)
  REAL :: ceillim(nx,ny)
!
  REAL, PARAMETER :: reflim=90.
  REAL, PARAMETER :: refchek=-10.

!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL    :: ref,reflow, qr, qc, qi, qs, qh
  INTEGER :: istatus
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!-----------------------------------------------------------------------
!
!  Set minimum ceiling, stored in ceillim
!
!-----------------------------------------------------------------------
!
  IF(ceilopt == 1) THEN

    WRITE(6,'(a,f6.0,a)')                                               &
        ' Setting minimun ceiling to ',ceilmin,' m AGL'
    DO j=1,ny-1
      DO i=1,nx-1
        ceillim(i,j)=ceilmin+zp(i,j,2)
      END DO
    END DO

  ELSE IF(ceilopt == 2) THEN

    WRITE(6,'(a)')                                                      &
        ' Setting minimun ceiling based on surface LCL'
    CALL adaslcl(nx,ny,nz,zs,pr,pt,qv,ceillim)

  ELSE

    WRITE(6,'(a)')                                                      &
        ' Setting zero AGL minimun ceiling'
    DO j=1,ny-1
      DO i=1,nx-1
        ceillim(i,j)=zp(i,j,2)
      END DO
    END DO

  END IF

  DO j=1,ny-1
    DO i=1,nx-1
      DO k=2,nz-1
        IF(ref_mos_3d(i,j,k) > refchek) THEN
           CALL cldset(pr(i,j,k),pt(i,j,k),                             &
                  ptbar(i,j,k),qvbar(i,j,k),rhobar(i,j,k),              &
                  radqvopt,radqcopt,radqropt,radptopt,                  &
                  refsat,rhradobs,refcld,cldrad,                        &
                  refrain,radsetrat,radreflim,radptgain,                &
                  zs(i,j,k),ceillim(i,j),ref_mos_3d(i,j,k),             &
                  qc,qr,qi,qs,qh)

           IF (P_QC > 0) qscalar(i,j,k,P_QC) = qc
           IF (P_QR > 0) qscalar(i,j,k,P_QR) = qr
           IF (P_QI > 0) qscalar(i,j,k,P_QI) = qi
           IF (P_QS > 0) qscalar(i,j,k,P_QS) = qs
           IF (P_QH > 0) qscalar(i,j,k,P_QH) = qh
        END IF
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE radmcro
!
!##################################################################
!##################################################################
!######                                                      ######
!######                  SUBROUTINE CLDSET                   ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE cldset(pr,pt,ptbar,qvbar,rhobar,                             &
           radqvopt,radqcopt,radqropt,radptopt,                         &
           refsat,rhradobs,refcld,cldrad,                               &
           refrain,radsetrat,radreflim,radptgain,                       &
           hgt,ceillim,ref,                                             &
           qc,qr,qi,qs,qh)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Set RH and microphysical variables at a specific point
!  that is above the minimum ceiling and has a reflectivity
!  greater than the threshold.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Dingchen Hou, CAPS
!  May, 1997
!
!  MODIFICATION HISTORY:
!
!  02/02/98 (K. Brewster)
!  Addition of refcldopt and old cldrad fixed cloud setting.
!  Addition of radsetrat to control amount of cloud added.
!  Addition of radreflim to allow limiting the reflectivity
!     used to derive cloud amounts.
!  Slight streamling of code and addition of documentation.
!
!  04/30/2010 (K. Brewster)
!  Modification for new use of ref_mos_3d here and upstream
!  routines.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!  INPUT:
!
  REAL    :: pr,pt,ptbar,qvbar, rhobar
  INTEGER :: radqvopt,radqcopt,radqropt,radptopt
  REAL    :: refsat,rhradobs,refcld,cldrad
  REAL    :: refrain,radsetrat,radreflim,radptgain
  REAL    :: hgt,ceillim,ref
!
!  OUTPUT:
!
  REAL :: qc,qr,qi,qs,qh
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  REAL :: pratio,t,qv,qvsat,reflect
  REAL :: ref1,ref2,qc1,qc2
  REAL :: dqr,dqc,dpt,dpt1
  REAL :: aaa,bbb,pttem
  INTEGER :: iter
  PARAMETER (ref1=62.96,                                                &
             ref2=7.38,                                                 &
             qc1=2.0E-3,                                                &
             qc2=0.1E-3)
!
!-----------------------------------------------------------------------
!
!  Function f_qvsat and inline directive for Cray PVP
!
!-----------------------------------------------------------------------
!
  REAL :: f_qvsat

!fpp$ expand (f_qvsat)
!!dir$ inline always f_qvsat
!*$*  inline routine (f_qvsat)

!
!-----------------------------------------------------------------------
!
!  Include files
!
!-----------------------------------------------------------------------
!
  INCLUDE 'phycst.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  dqc=0.
  dqr=0.
  dpt=0.
!
!
!-----------------------------------------------------------------------
!
!  Limit reflectivity according to radreflim
!
!-----------------------------------------------------------------------
!
  reflect=AMIN1(ref,radreflim)

  dqc=0.0

  IF( ref > refcld .AND. hgt > ceillim) THEN

    IF( radqcopt == 1 ) THEN

    dqc=AMAX1((cldrad-(qc+qi)),0.0)

    ELSE IF( radqcopt == 2 ) THEN

    dqc=(qc1-qc2)/(LOG(ref1)-LOG(ref2))*                                &
        (LOG(reflect)-LOG(ref2))+qc2
    dqc=AMAX1(((radsetrat*dqc)-(qc+qi)),0.0)

  END IF

  END IF
!
!-----------------------------------------------------------------------
!
!  Adjustment of pt due to qc and qr
!
!-----------------------------------------------------------------------
!
  IF( radqropt > 0 .AND. ref > refrain ) THEN
    dqr=(10.0**(reflect/10.0)/17300.0)**(4.0/7.0)/(rhobar*1000.0)
    dqr=AMAX1(((radsetrat*dqr)-(qr+qs+qh)),0.0)
  END IF
!
  IF(radptopt == 1 .OR. radptopt == 3) dpt=radptgain*(dqr+dqc)*ptbar/(1.0+qvbar)

  pttem=pt+dpt

!
!-----------------------------------------------------------------------
!
!  Adjustment of water vapor
!
!-----------------------------------------------------------------------
!
  IF(radqvopt > 0 .AND. ref > refsat) THEN
!
!-----------------------------------------------------------------------
!
!  Adjustment of pt due to water vapor change
!  bbb is the bouyancy multiplied by ptbar/g, to be conserved.
!
!-----------------------------------------------------------------------
!
    IF (radptopt == 2 .OR. radptopt == 3) THEN
      pratio=(pr/p0) ** rddcp
      t =pt*pratio
      qvsat=f_qvsat( pr, t )
      qv=qvsat*rhradobs
      aaa=ptbar*(1-0.622)/((0.622+qvbar)*(1.0+qvbar))
      bbb=aaa*qv+pt

      DO iter=1,10
        t=pttem*pratio
        qvsat=f_qvsat( pr, t )
        dpt1=bbb-aaa*(qvsat*rhradobs)-pttem
!        print *,iter,pttem,t,p,qvsat,aaa,bbb,dpt1
        IF (ABS(dpt1) < 1.0E-3) EXIT
        pttem=pttem+dpt1
      END DO
!      101     CONTINUE
      t=pttem*pratio
      qvsat=f_qvsat( pr, t )
      qv=qvsat*rhradobs
    END IF
  END IF

  qc=qc+dqc
  qr=qr+dqr
  pt=pttem

  RETURN
END SUBROUTINE cldset
!
!##################################################################
!##################################################################
!######                                                      ######
!######                   SUBROUTINE ADASLCL                 ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
!

SUBROUTINE adaslcl(nx,ny,nz,zs,pr,pt,qv,hgtlcl)

  IMPLICIT NONE
  INTEGER :: nx,ny,nz
  REAL :: zs(nx,ny,nz)
  REAL :: pr(nx,ny,nz)
  REAL :: pt(nx,ny,nz)
  REAL :: qv(nx,ny,nz)
  REAL :: hgtlcl(nx,ny)
!
!-----------------------------------------------------------------------
!
!  Misc local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: imid,jmid

  REAL :: pmb,tc,tk,td,wmr,thepcl,plcl,tlcl
  REAL :: plnhi,plnlo,plnlcl,whi,wlo

  REAL :: oe,wmr2td
  INTEGER :: mprocx, mprocy, nxlg, nylg

!
!-----------------------------------------------------------------------
!
!  Include files
!
!-----------------------------------------------------------------------
!
  INCLUDE 'phycst.inc'
  INCLUDE 'mp.inc'
!
!-----------------------------------------------------------------------
!
!  Find pressure of lcl
!  Using pressure of lcl, find height of lcl
!
!-----------------------------------------------------------------------
!
  !nxlg = (nx-3)*nproc_x+3   ! Added by Y. Wang for proper diagnostic outputs
  !nylg = (ny-3)*nproc_y+3
  !imid = nxlg/2
  !jmid = nylg/2
  !mprocx = (imid-2)/(nx-3)+1
  !mprocy = (jmid-2)/(ny-3)+1
  !
  !IF (loc_x == mprocx) THEN
  !  imid = MOD((imid-2),(nx-3)) + 2   ! Local index for global middle point
  !ELSE
  !  imid = -9999
  !END IF
  !
  !IF (loc_y == mprocy) THEN
  !  jmid = MOD((jmid-2),(ny-3)) + 2   ! Local index for global middle point
  !ELSE
  !  jmid = -9999
  !END IF

  DO j=1,ny-1
    DO i=1,nx-1
      pmb=0.01*pr(i,j,2)
      tk=pt(i,j,2)*((1000./pmb)**rddcp)
      tc=tk-273.15
      wmr=1000.*(qv(i,j,2)/(1.-qv(i,j,2)))
      td=wmr2td(pmb,wmr)
      thepcl=oe(tc,td,pmb)
      CALL ptlcl(pmb,tc,td,plcl,tlcl)
      plcl=plcl*100.
      DO k=3,nz-2
        IF(pr(i,j,k) < plcl) EXIT
      END DO
      plnhi=LOG(pr(i,j,k))
      plnlo=LOG(pr(i,j,k-1))
      plnlcl=LOG(plcl)
      whi=(plnlo-plnlcl)/(plnlo-plnhi)
      wlo=1.-whi
      hgtlcl(i,j)=whi*zs(i,j,k)+wlo*zs(i,j,k-1)
      !IF(i == imid .AND. j == jmid) THEN
      !  WRITE(*,'(1x,a,6F15.5)') 'arpslcl: ',pmb,tc,td,wmr,plcl,hgtlcl(i,j)
      !END IF
    END DO
  END DO
!
  RETURN
END SUBROUTINE adaslcl

!
!##################################################################
!##################################################################
!######                                                      ######
!######               SUBROUTINE RADAROBS_QUALITY            ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
!
SUBROUTINE radarobs_quality(qopt,qsrcrad,ijbdy,bdyzone,qback,qback_factor, &
                            qobsrad,qualrad,istatus)

  IMPLICIT NONE

  INTEGER, INTENT(IN) :: qopt
  REAL,    INTENT(IN) :: qsrcrad
  INTEGER, INTENT(IN) :: ijbdy
  INTEGER, INTENT(IN) :: bdyzone
  REAL,    INTENT(IN) :: qback
  REAL,    INTENT(IN) :: qback_factor

  REAL,    INTENT(OUT) :: qobsrad
  INTEGER, INTENT(OUT) :: qualrad
  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  REAL    :: qbak, qrad
  INTEGER :: iw, ie, js, jn
  INTEGER :: ibdy

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  SELECT CASE (qopt)
  CASE (0)
    qobsrad = qsrcrad
    qualrad = 10
  CASE (1, 11)
    qbak = qback*qback_factor
    qrad = qsrcrad
    qobsrad = qrad + (qbak-qrad)*ijbdy/bdyzone
    qualrad = 10
  CASE (2, 12)
    IF (ijbdy > 0) THEN        ! donot use any obs inside boundary zone
      qobsrad = qback*qback_factor
      qualrad = ijbdy*(-10)
    ELSE
      qobsrad = qsrcrad
      qualrad = 10
    END IF
  CASE DEFAULT
    istatus = -1
    CALL arpsstop('ERROR: wrong qobsradbdy_opt option',1)
  END SELECT

  RETURN
END SUBROUTINE radarobs_quality
