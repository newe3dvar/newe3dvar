
SUBROUTINE recurfilt3d(nx,ny,nz,pgrd,ipass_filt,ipass_loop,stagdim,     &
                       hradius_3d,radius_z,alpha,beta,tem1,tem2)
!-----------------------------------------------------------------------
!
! PURPOSE:
!    Parallel version of the 3D recursive filter.
!
!-----------------------------------------------------------------------
!
!  AUTHOR:
!
!  Yunheng Wang, CAPS, OU, 11/01/2007
!
!-----------------------------------------------------------------------

  IMPLICIT NONE

  INTEGER, INTENT(IN) :: nx,ny,nz
  INTEGER, INTENT(IN) :: ipass_filt, ipass_loop
!  REAL,    INTENT(IN) :: hradius
  REAL,    INTENT(IN) :: hradius_3d(nx,ny,nz)
  REAL,    INTENT(IN) :: radius_z(nx,ny,nz)

  INTEGER, INTENT(IN) :: stagdim
  !INTEGER, INTENT(IN) :: ips,ipe, jps,jpe,kps,kpe


  REAL,    INTENT(INOUT) :: pgrd(nx,ny,nz)

  ! working arrays
  REAL,    INTENT(INOUT) :: alpha(nx,ny,nz)
  REAL,    INTENT(INOUT) :: beta (nx,ny,nz)

  REAL,    INTENT(INOUT) :: tem1(nx,ny,nz)   ! Temporary array for MPI
                                             ! Can be two dimensions as (max(nx,ny),nz)
  REAL,    INTENT(INOUT) :: tem2(nx,ny,nz)   ! Temporary array for MPI
                                             ! Can be two dimensions as (max(nx,ny),nz)

!-----------------------------------------------------------------------
!
! Misc local variables
!
!-----------------------------------------------------------------------

  REAL :: ee
  REAL :: temp1,temp2

  INTEGER :: i,j,k,n

  INCLUDE 'mp.inc'

  INTEGER :: ic, jc

  INTEGER :: ibgn, iend, jbgn, jend, kbgn, kend

!  integer :: nxlg, nylg
!  real, allocatable :: outlg(:,:,:)
!  logical :: outp

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!  IF( hradius == 0 ) return
  IF( hradius_3d(2,2,2) == 0 ) RETURN

!  outp = .false.
!  if (ipass_filt >= 100) then
!    ipass_filt = ipass_filt-100
!    outp = .true.
!
!    nxlg = (nx-3)*nproc_x+3
!    nylg = (ny-3)*nproc_y+3
!    Allocate(outlg(nxlg,nylg,nz),stat=ic)
!    CALL mpimerge3d(pgrd,nx,ny,nz,outlg)
!
!  if (mp_opt >0 .and. myproc == 0) ThEN
!    DO k = 2,2
!     do j = 1,1
!     do i = 1,nxlg
!       write(100,*) i, outlg(i,j,k)
!     end do
!       write(100,*)
!    end do
!    end do
!  else if (mp_opt == 0) then
!    DO k = 2,2
!     do j = 1,1
!     do i = 1,nxlg
!    write(100,*) i, outlg(i,j,k)
!     end do
!    write(100,*)
!    end do
!    end do
!  end if
!  end if
!
!write(0,*) ipass_filt

  ibgn = 1; iend = nx-1
  jbgn = 1; jend = ny-1
  kbgn = 1; kend = nz-1

  IF (stagdim == 1 .AND. loc_x == nproc_x) iend = nx
  IF (stagdim == 2 .AND. loc_y == nproc_y) jend = ny
  IF (stagdim == 3) kend = nz

  DO n = 1, ipass_loop ! ipass_filt/2

!write(0,*) 'ipass = ',n, loc_x, loc_y
!-----------------------------------------------------------------------
!
! X direction - forward
!
!-----------------------------------------------------------------------

    DO k = 1, nz
      DO j = 1, ny
        DO i = 1, nx
          ee = REAL(ipass_filt) / (hradius_3d(i,j,k)*hradius_3d(i,j,k))
          alpha(i,j,k) = 1+ee-SQRT( ee*(ee+2.) )
          beta(i,j,k)  = 1.-alpha(i,j,k)
        END DO
      END DO
    END DO

    CALL inctag

    DO ic = 1, nproc_x

      IF (loc_x == ic) THEN
        IF (loc_x == 1) THEN
          CALL set_bdyxs(pgrd,nx,ny,nz,1,jbgn,jend,kbgn,kend,n,alpha,beta)
        ELSE
          CALL receive_bdyxs(pgrd,nx,ny,nz,ibgn,tem2)
        END IF

        DO k = kbgn,kend
          DO j = jbgn, jend
            DO i = ibgn+1, iend
              pgrd(i,j,k)=alpha(i,j,k)*pgrd(i-1,j,k)+beta(i,j,k)*pgrd(i,j,k)
            END DO
          END DO
        END DO

        IF (loc_x < nproc_x) THEN
          CALL send_next_bdyxs(pgrd,nx,ny,nz,iend-1,tem1)
        END IF
      END IF

      CALL mpbarrier
    END DO

!-----------------------------------------------------------------------
!
! X direction - backward
!
!-----------------------------------------------------------------------

    CALL inctag

    DO ic = nproc_x, 1, -1

      IF (loc_x == ic) THEN
        IF (loc_x == nproc_x) THEN
          CALL set_bdyxe(pgrd,nx,ny,nz,iend,jbgn,jend,kbgn,kend,n,alpha,beta)
        ELSE
          CALL receive_bdyxe(pgrd,nx,ny,nz,iend,tem2)
        END IF

        DO k = kbgn,kend
          DO j = jbgn,jend
            DO i = iend-1, ibgn, -1
              pgrd(i,j,k)=alpha(i,j,k)*pgrd(i+1,j,k)+beta(i,j,k)*pgrd(i,j,k)
            END DO
          END DO
        END DO

        IF (loc_x > 1) THEN
          CALL send_previous_bdyxe(pgrd,nx,ny,nz,ibgn+1,tem1)
        END IF
      END IF

!write(0,*) '=== after backward:', nx/2,ny/2,nz/2,pgrd(nx/2,ny/2,nz/2)
      CALL mpbarrier
    END DO

!-----------------------------------------------------------------------
!
! Y direction - forward
!
!-----------------------------------------------------------------------

    CALL inctag

    DO jc = 1, nproc_y

      IF (jc == loc_y) THEN
        IF (loc_y == 1) THEN
          CALL set_bdyys(pgrd,nx,ny,nz,1,ibgn,iend,kbgn,kend,n,alpha,beta)
        ELSE
          CALL receive_bdyys(pgrd,nx,ny,nz,jbgn,tem2)
        END IF

        DO k = kbgn,kend
          DO j = jbgn+1, jend
            DO i = ibgn,iend
              pgrd(i,j,k)=alpha(i,j,k)*pgrd(i,j-1,k)+beta(i,j,k)*pgrd(i,j,k)
            END DO
          END DO
        END DO

        IF (loc_y < nproc_y) THEN
           CALL send_up_bdyys(pgrd,nx,ny,nz,jend-1,tem1)
        END IF

      END IF

      CALL mpbarrier
    END DO

!write(0,*) '*--- after forward:', n,' --- ',nx/2,ny/2,nz/2,pgrd(nx/2,ny/2,nz/2)

!-----------------------------------------------------------------------
!
! Y direction - backword
!
!-----------------------------------------------------------------------

    CALL inctag

    DO jc = nproc_y, 1, -1

      IF (jc == loc_y) THEN

        IF (loc_y == nproc_y) THEN
          CALL set_bdyye(pgrd,nx,ny,nz,jend,ibgn,iend,kbgn,kend,n,alpha,beta)
        ELSE
          CALL receive_bdyye(pgrd,nx,ny,nz,jend,tem2)
        END IF

        DO k = kbgn,kend
          DO j = jend-1, jbgn, -1
            DO i = ibgn,iend
              pgrd(i,j,k)=alpha(i,j,k)*pgrd(i,j+1,k)+beta(i,j,k)*pgrd(i,j,k)
            END DO
          END DO
        END DO

        IF (loc_y > 1) THEN
          CALL send_down_bdyye(pgrd,nx,ny,nz,jbgn+1,tem1)
        END IF
      END IF

      CALL mpbarrier
    END DO

!write(0,*) '*=== after backward:',n,' --- ', nx/2,ny/2,nz/2,pgrd(nx/2,ny/2,nz/2)
!-----------------------------------------------------------------------
!
! Z direction - forward
!
!-----------------------------------------------------------------------

!   IF( radius_z /= 0 ) THEN

      DO k = 1, nz
        DO j = 1, ny
          DO i = 1, nx
            ee = REAL(ipass_filt)/(radius_z(i,j,k)*radius_z(i,j,k))
            alpha(i,j,k) = 1+ee-SQRT( ee*(ee+2.) )
             beta(i,j,k) = 1.-alpha(i,j,k)
          END DO
        END DO
      END DO

      SELECT CASE (n)
      CASE (1)
        DO j = jbgn, jend
          DO i = ibgn, iend
            pgrd(i,j,kbgn) = beta(i,j,kbgn) * pgrd(i,j,kbgn)
          END DO
        END DO
      CASE (2)
        DO j = jbgn, jend
          DO i = ibgn, iend
            temp1 = 1.-alpha(i,j,kbgn)*alpha(i,j,kbgn)
            pgrd(i,j,kbgn) = beta(i,j,kbgn)/temp1 * pgrd(i,j,kbgn)
          END DO
        END DO
      CASE (3)
        DO j = jbgn, jend
          DO i = ibgn, iend
            temp1 = (1-alpha(i,j,kbgn))/                                            &
                ((1-alpha(i,j,kbgn)*alpha(i,j,kbgn))*(1-alpha(i,j,kbgn)*alpha(i,j,kbgn)))
            temp2 =alpha(i,j,kbgn)*alpha(i,j,kbgn)*alpha(i,j,kbgn)
            pgrd(i,j,kbgn) = temp1 * (pgrd(i,j,kbgn)-temp2*pgrd(i,j,kbgn+1))
          END DO
        END DO
      CASE DEFAULT
        DO j = jbgn, jend
          DO i = ibgn, iend
            temp2 =alpha(i,j,kbgn)*alpha(i,j,kbgn)*alpha(i,j,kbgn)
            temp1 = (1-alpha(i,j,kbgn))/                                            &
               (1-3*alpha(i,j,kbgn)*alpha(i,j,kbgn)+3*temp2*alpha(i,j,kbgn)-temp2*temp2)
            pgrd(i,j,kbgn) = temp1 * (pgrd(i,j,kbgn)-3*temp2*pgrd(i,j,kbgn+1)+      &
               temp2*alpha(i,j,kbgn)*alpha(i,j,kbgn)*pgrd(i,j,kbgn+1)               &
               +temp2*alpha(i,j,kbgn)*pgrd(i,j,kbgn+2))
          END DO
        END DO
      END SELECT

      DO k = 2, kend, 1
        DO j = jbgn, jend
          DO i = ibgn, iend
           pgrd(i,j,k) = alpha(i,j,k)*pgrd(i,j,k-1)+beta(i,j,k)*pgrd(i,j,k)
          END DO
        END DO
      END DO

!write(0,*) '##--- after forward:', nx/2,ny/2,nz/2,pgrd(nx/2,ny/2,nz/2)

!-----------------------------------------------------------------------
!
! Z direction - backward
!
!-----------------------------------------------------------------------

      SELECT CASE (n)
      CASE (0)
        DO j = jbgn, jend
          DO i = ibgn, iend
            pgrd(i,j,kend) = beta(i,j,kend) * pgrd(i,j,kend)
          END DO
        END DO
      CASE (1)
        DO j = jbgn, jend
          DO i = ibgn, iend
            temp1 = (1.-alpha(i,j,kend)*alpha(i,j,kend))
            pgrd(i,j,kend) = beta(i,j,kend)/temp1 * pgrd(i,j,kend)
          END DO
        END DO
      CASE (2)
        DO j = jbgn, jend
          DO i = ibgn, iend
            temp1 = (1-alpha(i,j,kend))/                                               &
             ((1-alpha(i,j,kend)*alpha(i,j,kend))*(1-alpha(i,j,kend)*alpha(i,j,kend)))
            temp2 = alpha(i,j,kend)*alpha(i,j,kend)*alpha(i,j,kend)
            pgrd(i,j,kend) = temp1 * (pgrd(i,j,kend)-temp2*pgrd(i,j,kend-1))
          END DO
        END DO
      CASE DEFAULT
        DO j = jbgn, jend
          DO i = ibgn, iend
            temp2 = alpha(i,j,kend)*alpha(i,j,kend)*alpha(i,j,kend)
            temp1 = (1-alpha(i,j,kend))/(1-3*alpha(i,j,kend)*alpha(i,j,kend)+          &
                              3*temp2*alpha(i,j,kend)-temp2*temp2)
            pgrd(i,j,kend) = temp1 * (pgrd(i,j,kend)-3*temp2*pgrd(i,j,kend-1)+         &
                   temp2*alpha(i,j,kend)*alpha(i,j,kend)*pgrd(i,j,kend-1)+             &
                   temp2*alpha(i,j,kend)*pgrd(i,j,kend-2))
          END DO
        END DO
      END SELECT

      DO k = kend-1, kbgn, -1
        DO j = jbgn, jend
          DO i = ibgn, iend
            pgrd(i,j,k)=alpha(i,j,k)*pgrd(i,j,k+1)+beta(i,j,k)*pgrd(i,j,k)
          END DO
        END DO
      END DO

!   ENDIF

  END DO

  !
  ! to fill the staggered boundaries, nx/ny
  ! all others are valid from 1 to nx-1, 1 to ny-1
  !
  IF (stagdim == 1) THEN
    CALL mpsendrecv2dew(pgrd, nx, ny, nz, 0, 0, 1, tem2)
  ELSE IF (stagdim == 2) THEN
    CALL mpsendrecv2dns(pgrd, nx, ny, nz, 0, 0, 2, tem2)
  END IF

!
!if (myproc == 0) then
!write(0,*) outlg(2,2,30)
!endif
!
!write(0,*) outp
!  outp = .false.
!  if (outp) then
!  CALL mpimerge3d(pgrd,nx,ny,nz,outlg)
!  if (mp_opt >0 .and. myproc == 0) ThEN
!    DO k = 1,nz
!     do j = 1,nylg
!    write(200,*) k,j
!    write(200,*) outlg(:,j,k)
!     end do
!    write(200,*)
!   end do
!  else if (mp_opt == 0) then
!
!    DO k = 1,nz
!     do j = 1,nylg
!       write(200,*) k,j
!       write(200,*) outlg(:,j,k)
!     end do
!       write(200,*)
!     end do
!   end if
!  call arpsstop(' ',0)
!   end if

  RETURN
END SUBROUTINE recurfilt3d

SUBROUTINE set_bdyxs(pgrd,nx,ny,nz,bdyi,jbgn,jend,kbgn,kend,n,alpha,beta)

! Set XS boundary
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: nx, ny, nz, n
  INTEGER, INTENT(IN)  :: bdyi                  ! should be 1
  INTEGER, INTENT(IN)  :: jbgn,jend,kbgn,kend
  REAL,    INTENT(IN)  :: alpha(nx,ny,nz)
  REAL,    INTENT(IN)  ::  beta(nx,ny,nz)
  REAL,    INTENT(INOUT) :: pgrd(nx,ny,nz)

  ! local variables

  REAL    :: temp1, temp2
  INTEGER :: k,j

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!XS

      SELECT CASE (n)
        CASE (1)
          DO k = kbgn,kend
            DO j = jbgn,jend
              pgrd(bdyi,j,k) = beta(bdyi,j,k)*pgrd(bdyi,j,k)
            END DO
          END DO
        CASE (2)
          DO k = kbgn,kend
            DO j = jbgn,jend
              temp1 = (1.-alpha(bdyi,j,k)*alpha(bdyi,j,k))
              pgrd(bdyi,j,k) = beta(bdyi,j,k)/temp1 * pgrd(bdyi,j,k)
            END DO
          END DO
        CASE (3)
          DO k = kbgn,kend
            DO j = jbgn,jend
              temp1 = (1-alpha(bdyi,j,k))/                                            &
                  ((1-alpha(bdyi,j,k)*alpha(bdyi,j,k))*(1-alpha(bdyi,j,k)*alpha(bdyi,j,k)))
              temp2 = alpha(bdyi,j,k)*alpha(bdyi,j,k)*alpha(bdyi,j,k)
              pgrd(bdyi,j,k) = temp1 * (pgrd(bdyi,j,k)-temp2*pgrd(bdyi+1,j,k))
            END DO
          END DO
        CASE DEFAULT
          DO k = kbgn,kend
            DO j = jbgn,jend
              temp2 = alpha(bdyi,j,k)*alpha(bdyi,j,k)*alpha(bdyi,j,k)
              temp1 = (1-alpha(bdyi,j,k))/                                            &
                  (1-3*alpha(bdyi,j,k)*alpha(bdyi,j,k)+3*temp2*alpha(bdyi,j,k)-temp2*temp2)
              pgrd(bdyi,j,k) = temp1 * (pgrd(bdyi,j,k)-3*temp2*pgrd(bdyi+1,j,k)+      &
                            temp2*alpha(bdyi,j,k)*alpha(bdyi,j,k)*pgrd(bdyi+1,j,k)+   &
                            temp2*alpha(bdyi,j,k)*pgrd(bdyi+2,j,k))
            END DO
          END DO
      END SELECT

   RETURN
END SUBROUTINE set_bdyxs

SUBROUTINE set_bdyxe(pgrd,nx,ny,nz,bdyi,jbgn,jend,kbgn,kend,n,alpha,beta)

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: nx, ny, nz, n
  INTEGER, INTENT(IN)  :: bdyi   ! nx-1 for scalar and internal domains
                                 ! nx for east domains and U stagger
  INTEGER, INTENT(IN)  :: jbgn,jend,kbgn,kend
  REAL,    INTENT(IN)  :: alpha(nx,ny,nz)
  REAL,    INTENT(IN)  ::  beta(nx,ny,nz)

  REAL,    INTENT(INOUT)  :: pgrd(nx,ny,nz)

  ! local variables

  REAL    :: temp1, temp2
  INTEGER :: k,j

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!XE
  SELECT CASE (n)
    CASE (0)
      DO k = kbgn,kend
        DO j = jbgn,jend
          pgrd(bdyi,j,k) = beta(bdyi,j,k) * pgrd(bdyi,j,k)
        END DO
      END DO
    CASE (1)
      DO k = kbgn,kend
        DO j = jbgn,jend
          temp1 = 1-alpha(bdyi,j,k)*alpha(bdyi,j,k)
          pgrd(bdyi,j,k) = beta(bdyi,j,k)/temp1 * pgrd(bdyi,j,k)
        END DO
      END DO
    CASE (2)
      DO k = kbgn,kend
        DO j = jbgn,jend
          temp1 = (1-alpha(bdyi,j,k))/                                          &
     ((1-alpha(bdyi,j,k)*alpha(bdyi,j,k))*(1-alpha(bdyi,j,k)*alpha(bdyi,j,k)))
          temp2 = alpha(bdyi,j,k)*alpha(bdyi,j,k)*alpha(bdyi,j,k)
          pgrd(bdyi,j,k) = temp1 * (pgrd(bdyi,j,k)-temp2*pgrd(bdyi-1,j,k))
        END DO
      END DO

  CASE DEFAULT
    DO k = kbgn,kend
      DO j = jbgn,jend
        temp2 = alpha(bdyi,j,k)*alpha(bdyi,j,k)*alpha(bdyi,j,k)
        temp1 = (1-alpha(bdyi,j,k))/                                            &
      (1-3*alpha(bdyi,j,k)*alpha(bdyi,j,k)+3*temp2*alpha(bdyi,j,k)-temp2*temp2)
        pgrd(bdyi,j,k) = temp1 * (pgrd(bdyi,j,k)-3*temp2*pgrd(bdyi-1,j,k)+        &
             temp2*alpha(bdyi,j,k)*alpha(bdyi,j,k)*pgrd(bdyi-1,j,k)+              &
             temp2*alpha(bdyi,j,k)*pgrd(bdyi-2,j,k))
      END DO
    END DO
  END SELECT

  RETURN
END SUBROUTINE set_bdyxe

SUBROUTINE set_bdyys(pgrd,nx,ny,nz,bdyj,ibgn,iend,kbgn,kend,n,alpha,beta)

! Set XS boundary
  IMPLICIT NONE

  INTEGER, INTENT(IN)    :: nx, ny, nz, n
  INTEGER, INTENT(IN)    :: bdyj                  ! should be 1
  INTEGER, INTENT(IN)    :: ibgn,iend,kbgn,kend
  REAL,    INTENT(IN)    :: alpha(nx,ny,nz)
  REAL,    INTENT(IN)    ::  beta(nx,ny,nz)
  REAL,    INTENT(INOUT) :: pgrd(nx,ny,nz)

  ! local variables

  REAL    :: temp1, temp2
  INTEGER :: k,i

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!YS

  SELECT CASE (n)
    CASE (1)
      DO k = kbgn,kend
        DO i = ibgn,iend
          pgrd(i,bdyj,k) = beta(i,bdyj,k) * pgrd(i,bdyj,k)
        END DO
      END DO

    CASE (2)
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp1 = (1.-alpha(i,bdyj,k)*alpha(i,bdyj,k))
          pgrd(i,bdyj,k) = beta(i,bdyj,k)/temp1 * pgrd(i,bdyj,k)
        END DO
      END DO

    CASE (3)
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp1 = (1-alpha(i,bdyj,k))/                                            &
               ((1-alpha(i,bdyj,k)*alpha(i,bdyj,k))*(1-alpha(i,bdyj,k)*alpha(i,bdyj,k)))
          temp2 = alpha(i,bdyj,k)*alpha(i,bdyj,k)*alpha(i,bdyj,k)
          pgrd(i,bdyj,k) = temp1 * (pgrd(i,bdyj,k)-temp2*pgrd(i,bdyj+1,k))
        END DO
      END DO

    CASE DEFAULT
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp2 = alpha(i,bdyj,k)*alpha(i,bdyj,k)*alpha(i,bdyj,k)
          temp1 = (1-alpha(i,bdyj,k))/                                            &
             (1-3*alpha(i,bdyj,k)*alpha(i,bdyj,k)+3*temp2*alpha(i,bdyj,k)-temp2*temp2)
          pgrd(i,bdyj,k) = temp1 * (pgrd(i,bdyj,k)-3*temp2*pgrd(i,bdyj+1,k)+      &
              temp2*alpha(i,bdyj,k)*alpha(i,bdyj,k)*pgrd(i,bdyj+1,k)+             &
              temp2*alpha(i,bdyj,k)*pgrd(i,bdyj+2,k))
        END DO
      END DO

  END SELECT

  RETURN
END SUBROUTINE set_bdyys

SUBROUTINE set_bdyye(pgrd,nx,ny,nz,bdyj,ibgn,iend,kbgn,kend,n,alpha,beta)

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: nx, ny, nz, n
  INTEGER, INTENT(IN)  :: bdyj   ! ny-1 for scalar and internal domains
                                 ! ny for east domains and V stagger
  INTEGER, INTENT(IN)  :: ibgn,iend,kbgn,kend
  REAL,    INTENT(IN)  ::  alpha(nx,ny,nz)
  REAL,    INTENT(IN)  ::   beta(nx,ny,nz)
  REAL,    INTENT(INOUT) :: pgrd(nx,ny,nz)

  ! local variables

  REAL    :: temp1, temp2
  INTEGER :: k,i

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!YE

  SELECT CASE ( n )

    CASE (0)
      DO k = kbgn,kend
        DO i = ibgn,iend
          pgrd(i,bdyj,k) = beta(i,bdyj,k) * pgrd(i,bdyj,k)
        END DO
      END DO

    CASE ( 1)
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp1 = (1.-alpha(i,bdyj,k)*alpha(i,bdyj,k))
          pgrd(i,bdyj,k) = beta(i,bdyj,k)/temp1 * pgrd(i,bdyj,k)
        END DO
      END DO

    CASE ( 2)
      DO k = kbgn,kend
        DO i = ibgn,iend
         temp1 = (1-alpha(i,bdyj,k))/                                                 &
           ((1-alpha(i,bdyj,k)*alpha(i,bdyj,k))*(1-alpha(i,bdyj,k)*alpha(i,bdyj,k)))
         temp2 = alpha(i,bdyj,k)*alpha(i,bdyj,k)*alpha(i,bdyj,k)
         pgrd(i,bdyj,k) = temp1 * (pgrd(i,bdyj,k)-temp2*pgrd(i,bdyj-1,k))
        END DO
      END DO

    CASE DEFAULT
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp2 = alpha(i,bdyj,k)*alpha(i,bdyj,k)*alpha(i,bdyj,k)
          temp1 = (1-alpha(i,bdyj,k))/(1-3*alpha(i,bdyj,k)*alpha(i,bdyj,k)+           &
                   3*temp2*alpha(i,bdyj,k)-temp2*temp2)
          pgrd(i,bdyj,k) = temp1 * (pgrd(i,bdyj,k)-3*temp2*pgrd(i,bdyj-1,k)+          &
               temp2*alpha(i,bdyj,k)*alpha(i,bdyj,k)*pgrd(i,bdyj-1,k)+                &
               temp2*alpha(i,bdyj,k)*pgrd(i,bdyj-2,k))
        END DO
      END DO

  END SELECT

  RETURN
END SUBROUTINE  set_bdyye


SUBROUTINE ad_recurfilt3d(nx,ny,nz,adpgrd,ipass_filt,ipass_loop,stagdim,&
                          hradius_3d,radius_z,alpha,beta,tem1,tem2)
!-----------------------------------------------------------------------
!
! PURPOSE:
!    Parallel version of the adjoint of 3D recursive filter.
!
!-----------------------------------------------------------------------
!
!  AUTHOR:
!
!  Anwei Lai, IHR/CIMMS, 29/01/2018
!
!-----------------------------------------------------------------------
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: nx,ny,nz
  INTEGER, INTENT(IN) :: ipass_filt, ipass_loop
!  REAL,    INTENT(IN) :: hradius
  REAL,    INTENT(IN) :: hradius_3d(nx,ny,nz)
  REAL,    INTENT(IN) :: radius_z(nx,ny,nz)

  INTEGER, INTENT(IN) :: stagdim
  !INTEGER, INTENT(IN) :: ips,ipe, jps,jpe,kps,kpe


  REAL,    INTENT(INOUT) :: adpgrd(nx,ny,nz)

  ! working arrays
  REAL,    INTENT(INOUT) :: alpha(nx,ny,nz)
  REAL,    INTENT(INOUT) :: beta (nx,ny,nz)

  REAL,    INTENT(INOUT) :: tem1(nx,ny,nz)   ! Temporary array for MPI
                                             ! Can be two dimensions as
                                             ! (max(nx,ny),nz)
  REAL,    INTENT(INOUT) :: tem2(nx,ny,nz)   ! Temporary array for MPI
                                             ! Can be two dimensions as
                                             ! (max(nx,ny),nz)

!-----------------------------------------------------------------------
!
! Misc local variables
!
!-----------------------------------------------------------------------

  REAL :: ee
  REAL :: temp1,temp2

  INTEGER :: i,j,k,n

  INCLUDE 'mp.inc'

  INTEGER :: ic, jc

  INTEGER :: ibgn, iend, jbgn, jend, kbgn, kend

  INTEGER :: itmp

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  !itmp = mp_opt*10+100

!!!!begin
  IF( hradius_3d(1,1,1) == 0 ) return
  ibgn = 1; iend = nx-1
  jbgn = 1; jend = ny-1
  kbgn = 1; kend = nz-1

  IF (stagdim == 1 .AND. loc_x == nproc_x) iend = nx
  IF (stagdim == 2 .AND. loc_y == nproc_y) jend = ny
  IF (stagdim == 3) kend = nz

!  IF (stagdim == 1) THEN
!    CALL mpsendrecv2dew(adpgrd, nx, ny, nz, 0, 0, 1, tem2)
!  ELSE IF (stagdim == 2) THEN
!    CALL mpsendrecv2dns(adpgrd, nx, ny, nz, 0, 0, 2, tem2)
!  END IF

  DO n=ipass_loop,1,-1
!-----------------------------------------------------------------------
!
! Z direction - backward(adjoint)
!
!-----------------------------------------------------------------------
    DO k = 1, nz
      DO j = 1, ny
        DO i = 1, nx
          ee = REAL(ipass_filt)/(radius_z(i,j,k)*radius_z(i,j,k))
          alpha(i,j,k) = 1+ee-SQRT( ee*(ee+2.) )
          beta(i,j,k)  = 1.-alpha(i,j,k)
        END DO
      END DO
    END DO

    DO k = kbgn,kend-1, 1
      DO j = jbgn, jend
        DO i = ibgn, iend
           adpgrd(i,j,k+1) = adpgrd(i,j,k+1) + alpha(i,j,k)*adpgrd(i,j,k)
           adpgrd(i,j,k)   = beta(i,j,k)*adpgrd(i,j,k)
        ENDDO
      ENDDO
    ENDDO

    SELECT CASE (n)
    CASE (0)
      DO j = jbgn, jend
        DO i = ibgn, iend
           adpgrd(i,j,kend) = beta(i,j,kend) * adpgrd(i,j,kend)
        END DO
      END DO
    CASE (1)
      DO j = jbgn, jend
        DO i = ibgn, iend
           temp1 = (1.-alpha(i,j,kend)*alpha(i,j,kend))
           adpgrd(i,j,kend) = beta(i,j,kend)/temp1* adpgrd(i,j,kend)
        END DO
      END DO
    CASE (2)
      DO j = jbgn, jend
        DO i = ibgn, iend
           temp1 = (1-alpha(i,j,kend))/                                               &
              ((1-alpha(i,j,kend)*alpha(i,j,kend))*(1-alpha(i,j,kend)*alpha(i,j,kend)))
           temp2 = alpha(i,j,kend)*alpha(i,j,kend)*alpha(i,j,kend)
           adpgrd(i,j,kend-1) = adpgrd(i,j,kend-1) - temp1*temp2*adpgrd(i,j,kend)
           adpgrd(i,j,kend)   = temp1*adpgrd(i,j,kend)
        ENDDO
      ENDDO
    CASE DEFAULT
      DO j = jbgn, jend
        DO i = ibgn, iend
           temp2 = alpha(i,j,kend)*alpha(i,j,kend)*alpha(i,j,kend)
           temp1 = (1-alpha(i,j,kend))/(1-3*alpha(i,j,kend)*alpha(i,j,kend)+          &
                               3*temp2*alpha(i,j,kend)-temp2*temp2)
           adpgrd(i,j,kend-1) = adpgrd(i,j,kend-1)                                    &
             +temp1*(temp2*alpha(i,j,kend)*alpha(i,j,kend)-3*temp2)*adpgrd(i,j,kend)
           adpgrd(i,j,kend-2) = adpgrd(i,j,kend-2)                                    &
             + temp1*temp2*alpha(i,j,kend)*adpgrd(i,j,kend)
           adpgrd(i,j,kend) = temp1*adpgrd(i,j,kend)
        ENDDO
      ENDDO
    END SELECT
!-----------------------------------------------------------------------
!
! Z direction - forward(adjoint)
!
!-----------------------------------------------------------------------
    DO k = kend,2, -1
      DO j = jbgn, jend
        DO i = ibgn, iend
          adpgrd(i,j,k-1) = adpgrd(i,j,k-1) + alpha(i,j,k)*adpgrd(i,j,k)
          adpgrd(i,j,k)   = beta(i,j,k)*adpgrd(i,j,k)
        END DO
      END DO
    END DO

    SELECT CASE (n)
    CASE (1)
      DO j = jbgn, jend
        DO i = ibgn, iend
           adpgrd(i,j,kbgn) = beta(i,j,kbgn) * adpgrd(i,j,kbgn)
        END DO
      END DO
    CASE (2)
      DO j = jbgn, jend
        DO i = ibgn, iend
           temp1 = (1.-alpha(i,j,kend)*alpha(i,j,kend))
           adpgrd(i,j,kbgn) = beta(i,j,kend)/temp1* adpgrd(i,j,kbgn)
        END DO
      END DO
    CASE (3)
      DO j = jbgn, jend
        DO i = ibgn, iend
           temp1 = (1-alpha(i,j,kbgn))/                                     &
              ((1-alpha(i,j,kbgn)*alpha(i,j,kbgn))*(1-alpha(i,j,kend)*alpha(i,j,kbgn)))
           temp2 = alpha(i,j,kbgn)*alpha(i,j,kbgn)*alpha(i,j,kbgn)
           adpgrd(i,j,kbgn+1) = adpgrd(i,j,kbgn+1) - temp1*temp2*adpgrd(i,j,kbgn)
           adpgrd(i,j,kbgn) =  temp1*adpgrd(i,j,kbgn)
        END DO
      END DO
    CASE DEFAULT
      DO j = jbgn, jend
        DO i = ibgn, iend
           temp2 = alpha(i,j,kbgn)*alpha(i,j,kbgn)*alpha(i,j,kbgn)
           temp1 = (1-alpha(i,j,kbgn))/                                    &
                 (1-3*alpha(i,j,kbgn)*alpha(i,j,kbgn)+3*temp2*alpha(i,j,kbgn)-temp2*temp2)
           adpgrd(i,j,kbgn+2) = adpgrd(i,j,kbgn+2) +temp1*temp2*alpha(i,j,kbgn)*adpgrd(i,j,kbgn+2)
           adpgrd(i,j,kbgn+1) = adpgrd(i,j,kbgn+1) +                       &
                        temp1*(-3*temp2+temp2*alpha(i,j,kbgn)*alpha(i,j,kbgn))*adpgrd(i,j,kbgn)
           adpgrd(i,j,kbgn) = temp1*adpgrd(i,j,kbgn)
        END DO
      END DO
    END SELECT

!-----------------------------------------------------------------------
!
! Y direction - backward(adjoint)
!
!-----------------------------------------------------------------------

    DO k = 1, nz
      DO j = 1, ny
        DO i = 1, nx
          ee = REAL(ipass_filt) / (hradius_3d(i,j,k)*hradius_3d(i,j,k))
          alpha(i,j,k) = 1+ee-SQRT( ee*(ee+2.) )
          beta(i,j,k)  = 1.-alpha(i,j,k)
        END DO
      END DO
    END DO

!CALL print3dnc_lg(itmp,'y0_mpi',adpgrd,nx,ny,nz)

    CALL inctag

    DO jc =1,nproc_y
      IF (jc == loc_y) THEN

        IF (loc_y == 1) THEN
          DO k = kbgn,kend
             DO i = ibgn, iend
                adpgrd(i,jbgn+1,k) = adpgrd(i,jbgn+1,k)+alpha(i,jbgn,k)*adpgrd(i,jbgn,k)
                adpgrd(i,jbgn,k)   = beta(i,jbgn,k)*adpgrd(i,jbgn,k)
             END DO
          END DO

        ELSE
          CALL receive_bdyys(adpgrd,nx,ny,nz,jbgn+1,tem2)
        END IF

        DO k = kbgn,kend
          DO j = jbgn+1,jend-1
            DO i = ibgn,iend
              adpgrd(i,j+1,k) = adpgrd(i,j+1,k) + alpha(i,j,k)*adpgrd(i,j,k)
              adpgrd(i,j,k)   = beta(i,j,k)*adpgrd(i,j,k)
            END DO
          END DO
        END DO

        IF (loc_y == nproc_y) THEN
          CALL ad_set_bdyye(adpgrd,nx,ny,nz,jend,ibgn,iend,kbgn,kend,n,alpha,beta)
        ELSE
!           if(myproc==0)print*,"call send_up_bdyys2"
          CALL send_up_bdyys(adpgrd,nx,ny,nz,jend,tem1)
        END IF

      END IF
      CALL mpbarrier
    END DO

!CALL print3dnc_lg(itmp,'y1_mpi',adpgrd,nx,ny,nz)

    CALL mpsendrecv2dns(adpgrd, nx, ny, nz, 0, 0, 0, tem2)

!CALL print3dnc_lg(itmp,'y2_mpi',adpgrd,nx,ny,nz)

!-----------------------------------------------------------------------
!
! Y direction - forward(adjoint)
!
!-----------------------------------------------------------------------
    CALL inctag

    DO jc =nproc_y,1,-1
      IF (jc == loc_y) THEN

        IF (loc_y == nproc_y) THEN
          if (loc_x == 8 .and. loc_y == 24) write(0,*) 'set '
          DO k = kbgn,kend
             DO i = jbgn, iend
                adpgrd(i,jend-1,k) = adpgrd(i,jend-1,k)+ alpha(i,jend,k)*adpgrd(i,jend,k)
                adpgrd(i,jend,k)   = beta(i,jend,k)*adpgrd(i,jend,k)
             END DO
          END DO
        ELSE
          CALL receive_bdyye(adpgrd,nx,ny,nz,jend-1,tem2)
        END IF

        DO k = kbgn,kend
         ! DO j = jend,2,-1
          DO j = jend-1,jbgn+1,-1
            DO i = ibgn,iend
              adpgrd(i,j-1,k) = adpgrd(i,j-1,k) + alpha(i,j,k)*adpgrd(i,j,k)
              adpgrd(i,j,k)   = beta(i,j,k)*adpgrd(i,j,k)
            END DO
          END DO
        END DO

        IF (loc_y == 1) THEN
          CALL ad_set_bdyys(adpgrd,nx,ny,nz,1,ibgn,iend,kbgn,kend,n,alpha,beta)
        ELSE
          CALL send_down_bdyye(adpgrd,nx,ny,nz,jbgn,tem1)
        END IF

      END IF
      CALL mpbarrier
    ENDDO

!CALL print3dnc_lg(itmp+3,'adpgrd_y',adpgrd,nx,ny,nz)

    CALL mpsendrecv2dns(adpgrd, nx, ny, nz, 0, 0, 0, tem2)

!CALL print3dnc_lg(itmp+4,'adpgrd_y',adpgrd,nx,ny,nz)
!CALL arpsstop('',0)

!-----------------------------------------------------------------------
!
! X direction - backward(adjoint)
!
!-----------------------------------------------------------------------

    CALL inctag

    DO ic = 1,nproc_x
      IF(loc_x == ic) THEN

        IF (loc_x == 1) THEN
          DO k = kbgn,kend
             DO j = jbgn, jend
                adpgrd(ibgn+1,j,k) = adpgrd(ibgn+1,j,k)+ alpha(ibgn,j,k)*adpgrd(ibgn,j,k)
                adpgrd(ibgn,j,k)   = beta(ibgn,j,k)*adpgrd(ibgn,j,k)
             END DO
          END DO
        ELSE
          CALL receive_bdyxs(adpgrd,nx,ny,nz,ibgn+1,tem2)
        END IF

        DO k = kbgn,kend
          DO j = jbgn, jend
           ! DO i = 2,iend-1
            DO i = ibgn+1,iend-1
              adpgrd(i+1,j,k)= adpgrd(i+1,j,k) + alpha(i,j,k)*adpgrd(i,j,k)
              adpgrd(i,j,k)  = beta(i,j,k)*adpgrd(i,j,k)
            END DO
          END DO
        END DO

        IF (loc_x == nproc_x) THEN
          CALL ad_set_bdyxe(adpgrd,nx,ny,nz,iend,jbgn,jend,kbgn,kend,n,alpha,beta)
        ELSE
          CALL send_next_bdyxs(adpgrd,nx,ny,nz,iend,tem1)
        END IF

      END IF
      CALL mpbarrier
    END DO

!CALL print3dnc_lg(itmp+1,'adpgrd_x',adpgrd,nx,ny,nz)

    CALL mpsendrecv2dew(adpgrd, nx, ny, nz, 0, 0, 0, tem2)

!CALL print3dnc_lg(itmp+2,'adpgrd_x',adpgrd,nx,ny,nz)

!-----------------------------------------------------------------------
!
! X direction - forward(adjoint)
!
!-----------------------------------------------------------------------

    CALL inctag

    DO ic = nproc_x, 1,-1
      IF (loc_x == ic) THEN

         IF (loc_x == nproc_x) THEN
           DO k = kbgn,kend
              DO j = jbgn, jend
                 adpgrd(iend-1,j,k)= adpgrd(iend-1,j,k) +alpha(iend,j,k)*adpgrd(iend,j,k)
                 adpgrd(iend,j,k) = beta(iend,j,k)*adpgrd(iend,j,k)
              END DO
           END DO

         ELSE
           CALL receive_bdyxe(adpgrd,nx,ny,nz,iend-1,tem2)
         END IF

         DO k = kbgn,kend
           DO j = jbgn, jend
             DO i =  iend-1, ibgn+1, -1
                adpgrd(i-1,j,k) = adpgrd(i-1,j,k) + alpha(i,j,k)*adpgrd(i,j,k)
                adpgrd(i  ,j,k) = beta(i,j,k)*adpgrd(i,j,k)
             END DO
           END DO
         END DO

         IF (loc_x == 1) THEN
           CALL ad_set_bdyxs(adpgrd,nx,ny,nz,1,jbgn,jend,kbgn,kend,n,alpha,beta)
         ELSE
           CALL send_previous_bdyxe(adpgrd,nx,ny,nz,ibgn,tem1)
         END IF
      ENDIF

      CALL mpbarrier
    END DO

!CALL print3dnc_lg(itmp+3,'adpgrd_x',adpgrd,nx,ny,nz)

    CALL mpsendrecv2dew(adpgrd, nx, ny, nz, 0, 0, 0, tem2)       ! make sure 1, nx-1 have valid values

!CALL print3dnc_lg(itmp+4,'adpgrd_x',adpgrd,nx,ny,nz)
!CALL arpsstop('',0)

  END DO   !end ipass_loop

  IF (stagdim == 1) THEN
    CALL mpsendrecv2dew(adpgrd, nx, ny, nz, 0, 0, stagdim, tem2)
  ELSE IF (stagdim == 2) THEN
    CALL mpsendrecv2dns(adpgrd, nx, ny, nz, 0, 0, stagdim, tem2)
  END IF

  RETURN
END SUBROUTINE ad_recurfilt3d

SUBROUTINE ad_set_bdyxs(adpgrd,nx,ny,nz,bdyi,jbgn,jend,kbgn,kend,n,alpha,beta)
! Set XS boundary
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: nx, ny, nz, n
  INTEGER, INTENT(IN)  :: bdyi                  ! should be 1
  INTEGER, INTENT(IN)  :: jbgn,jend,kbgn,kend
  REAL,    INTENT(IN)  :: alpha(nx,ny,nz)
  REAL,    INTENT(IN)  ::  beta(nx,ny,nz)
  REAL,    INTENT(INOUT) :: adpgrd(nx,ny,nz)

  ! local variables

  REAL    :: temp1, temp2
  INTEGER :: k,j

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!XS
      SELECT CASE (n)
        CASE (1)
          DO k = kbgn,kend
            DO j = jbgn,jend
              adpgrd(bdyi,j,k) = beta(bdyi,j,k)*adpgrd(bdyi,j,k)
            END DO
          END DO
        CASE (2)
          DO k = kbgn,kend
            DO j = jbgn,jend
              temp1 = (1.-alpha(bdyi,j,k)*alpha(bdyi,j,k))
              adpgrd(bdyi,j,k) = beta(bdyi,j,k)/temp1 * adpgrd(bdyi,j,k)
            END DO
          END DO
        CASE (3)
          DO k = kbgn,kend
            DO j = jbgn,jend
              temp1 = (1-alpha(bdyi,j,k))/                             &
                  ((1-alpha(bdyi,j,k)*alpha(bdyi,j,k))*(1-alpha(bdyi,j,k)*alpha(bdyi,j,k)))
              temp2 = alpha(bdyi,j,k)*alpha(bdyi,j,k)*alpha(bdyi,j,k)
              adpgrd(bdyi+1,j,k) = adpgrd(bdyi+1,j,k) - temp1*temp2*adpgrd(bdyi,j,k)
              adpgrd(bdyi  ,j,k) = temp1 * adpgrd(bdyi,j,k)
            END DO
          END DO
        CASE DEFAULT
          DO k = kbgn,kend
            DO j = jbgn,jend
              temp2 = alpha(bdyi,j,k)*alpha(bdyi,j,k)*alpha(bdyi,j,k)
              temp1 = (1-alpha(bdyi,j,k))/                                  &
                  (1-3*alpha(bdyi,j,k)*alpha(bdyi,j,k)+3*temp2*alpha(bdyi,j,k)-temp2*temp2)
              adpgrd(bdyi+2,j,k) = adpgrd(bdyi+2,j,k) +temp1*temp2*alpha(bdyi,j,k)*adpgrd(bdyi,j,k)
              adpgrd(bdyi+1,j,k) = adpgrd(bdyi+1,j,k) +                     &
                     temp1*(temp2*alpha(bdyi,j,k)*alpha(bdyi,j,k)-3*temp2)*adpgrd(bdyi,j,k)
              adpgrd(bdyi,j,k) = temp1 * adpgrd(bdyi,j,k)
            END DO
          END DO
      END SELECT

   RETURN
END SUBROUTINE ad_set_bdyxs

SUBROUTINE ad_set_bdyxe(adpgrd,nx,ny,nz,bdyi,jbgn,jend,kbgn,kend,n,alpha,beta)
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: nx, ny, nz, n
  INTEGER, INTENT(IN)  :: bdyi   ! nx-1 for scalar and internal domains
                                 ! nx for east domains and U stagger
  INTEGER, INTENT(IN)  :: jbgn,jend,kbgn,kend
  REAL,    INTENT(IN)  :: alpha(nx,ny,nz)
  REAL,    INTENT(IN)  ::  beta(nx,ny,nz)

  REAL,    INTENT(INOUT)  :: adpgrd(nx,ny,nz)

  ! local variables

  REAL    :: temp1, temp2
  INTEGER :: k,j

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!XE
  SELECT CASE (n)
    CASE (0)
      DO k = kbgn,kend
        DO j = jbgn,jend
          adpgrd(bdyi,j,k) = beta(bdyi,j,k) * adpgrd(bdyi,j,k)
        END DO
      END DO
    CASE (1)
      DO k = kbgn,kend
        DO j = jbgn,jend
          temp1 = 1-alpha(bdyi,j,k)*alpha(bdyi,j,k)
          adpgrd(bdyi,j,k) = beta(bdyi,j,k)/temp1 * adpgrd(bdyi,j,k)
        END DO
      END DO
    CASE (2)
      DO k = kbgn,kend
        DO j = jbgn,jend
          temp1 = (1-alpha(bdyi,j,k))/                             &
     ((1-alpha(bdyi,j,k)*alpha(bdyi,j,k))*(1-alpha(bdyi,j,k)*alpha(bdyi,j,k)))
          temp2 = alpha(bdyi,j,k)*alpha(bdyi,j,k)*alpha(bdyi,j,k)
          adpgrd(bdyi-1,j,k) = adpgrd(bdyi-1,j,k) - temp1*temp2*adpgrd(bdyi,j,k)
          adpgrd(bdyi,j,k) = temp1 * adpgrd(bdyi,j,k)
        END DO
      END DO

  CASE DEFAULT
    DO k = kbgn,kend
      DO j = jbgn,jend
        temp2 = alpha(bdyi,j,k)*alpha(bdyi,j,k)*alpha(bdyi,j,k)
        temp1 = (1-alpha(bdyi,j,k))/                                 &
      (1-3*alpha(bdyi,j,k)*alpha(bdyi,j,k)+3*temp2*alpha(bdyi,j,k)-temp2*temp2)
        adpgrd(bdyi-2,j,k) = adpgrd(bdyi-2,j,k) +                    &
                            temp1*temp2*alpha(bdyi,j,k)*adpgrd(bdyi,j,k)
        adpgrd(bdyi-1,j,k) =  adpgrd(bdyi-1,j,k) +               &
             temp1*(temp2*alpha(bdyi,j,k)*alpha(bdyi,j,k)-3*temp2)*adpgrd(bdyi,j,k)
        adpgrd(bdyi,j,k) = temp1 * adpgrd(bdyi,j,k)
      END DO
    END DO
  END SELECT

  RETURN
END SUBROUTINE ad_set_bdyxe

SUBROUTINE ad_set_bdyys(adpgrd,nx,ny,nz,bdyj,ibgn,iend,kbgn,kend,n,alpha,beta)

! Set XS boundary
  IMPLICIT NONE

  INTEGER, INTENT(IN)    :: nx, ny, nz, n
  INTEGER, INTENT(IN)    :: bdyj                  ! should be 1
  INTEGER, INTENT(IN)    :: ibgn,iend,kbgn,kend
  REAL,    INTENT(IN)    :: alpha(nx,ny,nz)
  REAL,    INTENT(IN)    ::  beta(nx,ny,nz)
  REAL,    INTENT(INOUT) :: adpgrd(nx,ny,nz)

  ! local variables

  REAL    :: temp1, temp2
  INTEGER :: k,i

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!YS

  SELECT CASE (n)
    CASE (1)
      DO k = kbgn,kend
        DO i = ibgn,iend
          adpgrd(i,bdyj,k) = beta(i,bdyj,k) * adpgrd(i,bdyj,k)
        END DO
      END DO

    CASE (2)
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp1 = (1.-alpha(i,bdyj,k)*alpha(i,bdyj,k))
          adpgrd(i,bdyj,k) = beta(i,bdyj,k)/temp1 * adpgrd(i,bdyj,k)
        END DO
      END DO

    CASE (3)
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp1 = (1-alpha(i,bdyj,k))/                                &
               ((1-alpha(i,bdyj,k)*alpha(i,bdyj,k))*(1-alpha(i,bdyj,k)*alpha(i,bdyj,k)))
          temp2 = alpha(i,bdyj,k)*alpha(i,bdyj,k)*alpha(i,bdyj,k)
          adpgrd(i,bdyj+1,k) = adpgrd(i,bdyj+1,k) - temp1*temp2*adpgrd(i,bdyj,k)
          adpgrd(i,bdyj,k)   = temp1*adpgrd(i,bdyj,k)
        END DO
      END DO
    CASE DEFAULT
      DO k = kbgn,kend
        DO i = ibgn,iend
           temp2 = alpha(i,bdyj,k)*alpha(i,bdyj,k)*alpha(i,bdyj,k)
           temp1 = (1-alpha(i,bdyj,k))/                               &
             (1-3*alpha(i,bdyj,k)*alpha(i,bdyj,k)+3*temp2*alpha(i,bdyj,k)-temp2*temp2)
           adpgrd(i,bdyj+2,k) = adpgrd(i,bdyj+2,k) +                  &
                                temp1* temp2*alpha(i,bdyj,k)*adpgrd(i,bdyj,k)
           adpgrd(i,bdyj+1,k) = adpgrd(i,bdyj+1,k)  +                 &
                 temp1*(temp2*alpha(i,bdyj,k)*alpha(i,bdyj,k)-3*temp2)*adpgrd(i,bdyj,k)
           adpgrd(i,bdyj,k)   = temp1*adpgrd(i,bdyj,k)
        END DO
      END DO

  END SELECT

  RETURN

END SUBROUTINE ad_set_bdyys

SUBROUTINE ad_set_bdyye(adpgrd,nx,ny,nz,bdyj,ibgn,iend,kbgn,kend,n,alpha,beta)

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: nx, ny, nz, n
  INTEGER, INTENT(IN)  :: bdyj   ! ny-1 for scalar and internal domains
                                 ! ny for east domains and V stagger
  INTEGER, INTENT(IN)  :: ibgn,iend,kbgn,kend
  REAL,    INTENT(IN)  ::  alpha(nx,ny,nz)
  REAL,    INTENT(IN)  ::   beta(nx,ny,nz)
  REAL,    INTENT(INOUT) :: adpgrd(nx,ny,nz)

  ! local variables

  REAL    :: temp1, temp2
  INTEGER :: k,i

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!YE
  SELECT CASE ( n )

    CASE (0)
      DO k = kbgn,kend
        DO i = ibgn,iend
          adpgrd(i,bdyj,k) = beta(i,bdyj,k) * adpgrd(i,bdyj,k)
        END DO
      END DO

    CASE ( 1)
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp1 = (1.-alpha(i,bdyj,k)*alpha(i,bdyj,k))
          adpgrd(i,bdyj,k) = beta(i,bdyj,k)/temp1 * adpgrd(i,bdyj,k)
        END DO
      END DO

    CASE ( 2)
      DO k = kbgn,kend
        DO i = ibgn,iend
         temp1 = (1-alpha(i,bdyj,k))/                                    &
           ((1-alpha(i,bdyj,k)*alpha(i,bdyj,k))*(1-alpha(i,bdyj,k)*alpha(i,bdyj,k)))
         temp2 = alpha(i,bdyj,k)*alpha(i,bdyj,k)*alpha(i,bdyj,k)
         adpgrd(i,bdyj-1,k) = adpgrd(i,bdyj-1,k) - temp1*temp2*adpgrd(i,bdyj,k)
         adpgrd(i,bdyj,k)   = temp1 * adpgrd(i,bdyj,k)
        END DO
      END DO

    CASE DEFAULT
      DO k = kbgn,kend
        DO i = ibgn,iend
          temp2 = alpha(i,bdyj,k)*alpha(i,bdyj,k)*alpha(i,bdyj,k)
          temp1 = (1-alpha(i,bdyj,k))/(1-3*alpha(i,bdyj,k)*alpha(i,bdyj,k)+ &
                   3*temp2*alpha(i,bdyj,k)-temp2*temp2)
          adpgrd(i,bdyj-2,k) = adpgrd(i,bdyj-2,k) +                         &
                              temp1*temp2*alpha(i,bdyj,k)*adpgrd(i,bdyj,k)
          adpgrd(i,bdyj-1,k) = adpgrd(i,bdyj-1,k) +                         &
                 temp1*(temp2*alpha(i,bdyj,k)*alpha(i,bdyj,k)-3*temp2)*adpgrd(i,bdyj,k)
          adpgrd(i,bdyj,k)   = temp1 * adpgrd(i,bdyj,k)
        END DO
      END DO

  END SELECT
  RETURN
END SUBROUTINE  ad_set_bdyye
