!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%
!%%%% This module handle radar data variational assimilation for radar
!%%%% data that was interpolated into the model grid (on 3D model grid
!%%%% or on horizontal grid only), including
!%%%% assimilation of radial velocity, radar reflectivity etc.
!%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!-----------------------------------------------------------------------
!
!  AUTHOR:
!
!  Yunheng Wang, 06/21/2017
!
!-----------------------------------------------------------------------
!
!  MODIFICATIONS:
!
!-----------------------------------------------------------------------

MODULE module_radar_grid

  USE model_precision
  USE module_radarobs

  IMPLICIT NONE

  REAL(DP), ALLOCATABLE :: xradc_p(:), yradc_p(:)
  REAL(P),  ALLOCATABLE :: zradc_1(:,:), zradc_2(:,:)
  INTEGER,  ALLOCATABLE :: ihgtradc(:,:)

  LOGICAL,  ALLOCATABLE :: ownrad(:)     ! with local index

  REAL,     ALLOCATABLE :: ref_diff(:,:,:)

  CONTAINS

  !#####################################################################

  SUBROUTINE radargrid_init(ref_use,nx,ny,nz,xs,ys,zs,myproc,istatus)

    INTEGER, INTENT(IN)  :: nx,ny,nz
    LOGICAL, INTENT(IN)  :: ref_use
    REAL(P), INTENT(IN)  :: xs(nx)
    REAL(P), INTENT(IN)  :: ys(ny)
    REAL(P), INTENT(IN)  :: zs(nx,ny,nz)

    INTEGER, INTENT(IN)  :: myproc

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF( ALLOCATED(xradc_p) ) DEALLOCATE (xradc_p)
    ALLOCATE ( xradc_p(mx_colrad), STAT=istatus )
    CALL check_alloc_status(istatus, "minimization:xradc_p")

    IF( ALLOCATED(yradc_p) ) DEALLOCATE (yradc_p)
    ALLOCATE ( yradc_p(mx_colrad), STAT=istatus )
    CALL check_alloc_status(istatus, "minimization:yradc_p")

    IF( ALLOCATED(zradc_1) ) DEALLOCATE (zradc_1)
    ALLOCATE ( zradc_1(nz_radar,mx_colrad), STAT=istatus )
    CALL check_alloc_status(istatus, "minimization:zradc_1")

    IF( ALLOCATED(zradc_2) ) DEALLOCATE (zradc_2)
    ALLOCATE ( zradc_2(nz_radar,mx_colrad), STAT=istatus )
    CALL check_alloc_status(istatus, "minimization:zrad_2")

    IF( ALLOCATED(ihgtradc) ) DEALLOCATE (ihgtradc)
    ALLOCATE ( ihgtradc(nz_radar,mx_colrad), STAT=istatus )
    CALL check_alloc_status(istatus, "minimization:ihgtradc")

    !! xradc_p(point) = i + (xradc(icol) - xs(i))/(xs(i+1)-xs(i))
    CALL map_to_mod2(nx,ny,mx_colrad,ncolrad,userad,xs,ys,xradc,yradc,  &
                     xradc_p,yradc_p)
    !
    !! to find k index of hgtradc, here zs is actually zps
    CALL map_to_modz(nz_radar, mx_colrad, nlevrad, ncolrad,nx,ny,nz,zs, &
                     xradc_p,yradc_p,hgtradc,ihgtradc,zradc_1,zradc_2)

    ALLOCATE(ownrad(mx_colrad), STAT = istatus)

    ownrad(:) = .FALSE.
    DO i = 1, ncolrad   ! local number
      IF (indexrad(i) == myproc) ownrad(i)  = .TRUE.
    END DO

    IF (ref_use .AND. grdtlt_flag /= 1) THEN
      !IF ( ALLOCATED(ref_diff   ) ) DEALLOCATE (ref_diff   )
      ALLOCATE ( ref_diff(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:ref_diff")
      ref_diff (:,:,:) = 0.0
    END IF

    RETURN
  END SUBROUTINE radargrid_init

  !#####################################################################

  SUBROUTINE radargrid_costf(nx,ny,nz,zs,u_ctr,v_ctr,w_ctr,             &
                             p_ctr,t_ctr,qscalar_ctr,                   &
                             ref_mos_3d,intvl_T,thresh_ref,rhobar,      &
                             ibgn,iend,jbgn,jend,kbgn,kend,             &
                             ipass,vrob_opt,ref_opt,wgt_ref,            &
                             f_ovrad,f_oref,                            &
                             npt,nqr,nqs,nqh,ref_bkg,istatus)

    INCLUDE 'globcst.inc'
    INCLUDE 'phycst.inc'
    !INCLUDE 'bndry.inc'
    INCLUDE 'mp.inc'

    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL(P), INTENT(IN)  :: zs(nx,ny,nz)
    REAL(P), INTENT(IN)  :: u_ctr(nx,ny,nz),v_ctr(nx,ny,nz),w_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  :: p_ctr(nx,ny,nz),t_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  :: qscalar_ctr(nx,ny,nz,nscalar)

    REAL(P), INTENT(IN)  :: ref_mos_3d(nx,ny,nz)
    REAL(P), INTENT(IN)  :: rhobar(nx,ny,nz)
    REAL(P), INTENT(IN)  :: intvl_T, thresh_ref

    INTEGER, INTENT(IN)  :: ibgn,iend,jbgn,jend,kbgn,kend
    INTEGER, INTENT(IN)  :: vrob_opt,ref_opt,ipass
    REAL(P), INTENT(IN)  :: wgt_ref

    REAL(DP), INTENT(OUT) :: f_ovrad,f_oref

    REAL(P), INTENT(OUT) :: npt(nx,ny,nz)
    REAL(P), INTENT(OUT) :: nqr(nx,ny,nz), nqs(nx,ny,nz), nqh(nx,ny,nz)
    REAL(P), INTENT(OUT) :: ref_bkg(nx,ny,nz)

    INTEGER, INTENT(OUT) :: istatus


  !---------------------------------------------------------------------

    INTEGER :: i,j,k,icol
    REAL(P) :: zs1D(nz),vrsc(nz)
    REAL(P) :: vr, rff
    REAL(P) :: uradc, vradc, wradc
    REAL(DP):: zvv, z_innov, f_innov,b_innov  !   rms , bias
    INTEGER :: i_innov  ! # obs

    INTEGER :: num

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    f_ovrad = 0.0D0
    f_oref  = 0.0D0

    DO k = 1, mx_colrad
      DO j = 1, nz_radar
        DO i = 1, nrad_anx
          oanxrad(i,j,k) = 0.0
        END DO
      END DO
    END DO

    !num = 0
    IF(grdtlt_flag == 1 ) THEN    ! grid-tilted radar format
      DO icol=1, ncolrad
        IF(iuserad(isrcradr(irad(icol)),ipass) > 0 .AND. userad(icol) ) THEN
          zs1D(:) = zs(iradc(icol), jradc(icol),:)
          !!!! compute vrsc(k)
          DO k = 2, nz-1
            uradc = 0.5*(u_ctr(iradc(icol),jradc(icol),k)+u_ctr(iradc(icol)+1,jradc(icol),k))
            vradc = 0.5*(v_ctr(iradc(icol),jradc(icol),k)+v_ctr(iradc(icol),jradc(icol)+1,k))
            wradc = 0.5*(w_ctr(iradc(icol),jradc(icol),k)+w_ctr(iradc(icol),jradc(icol),k+1))
            vrsc(k)=(uazmrad(icol)*uradc + vazmrad(icol)*vradc)         &
                            * dsdr(k,icol) + wradc*dhdr(k,icol)
          END DO !DO k = 2, nz-1
          !!!! loop through height
          DO k=1,nlevrad(icol)
            IF(qualrad(2,k,icol) > 0 .AND.ihgtradc(k,icol)>=0 ) THEN
              !!! to get model conterpart of Vr considering beam broadening
              CALL vrzwtavg1D_3dvar(1,nz,k3L(k,icol), k3U(k,icol), zs1D,     &
                    eleva(:,icol),elvang(k,irad(icol)),vrsc,bmwidth(irad(icol)),vr)

              !zvv        = vr - odifrad(2,k,icol)           ! GAO
              zvv        = vr - obsrad(2,k,icol)
              oanxrad(2,k,icol)=zvv /( qobsrad(2,k,icol) )
              zvv        = zvv * oanxrad(2,k,icol)
              IF (ownrad(icol)) THEN
                f_ovrad = f_ovrad +  zvv
              END IF

            END IF !IF(qualrad(2,k,icol) > 0 .AND.ihgtradc(k,icol)>=0 )
          END DO !DO k=1,nlevrad(icol)
        END IF !IF(iuserad(isrcrad(irad(icol))) > 0 .AND. userad(icol) )
      END DO !DO i=1, ncolrad

    ELSE ! gridded radar format

      IF( vrob_opt == 1) THEN

        !!CALL linearint_3d(nx,ny,nz,u_ctr(1,1,1),xs(1),ys(1),zs(1,1,1),      &
        !!       userad, nzrdr, mxcolrad, nlevrad, ncolrad,                   &
        !!       1,xradc_p,yradc_p,zradc_1,zradc_2,hgtradc,                   &
        !!                                      ihgtradc,corrad(1,1,1) )
        !!
        !!CALL linearint_3d(nx,ny,nz,v_ctr(1,1,1),xs(1),ys(1),zs(1,1,1),      &
        !!       userad, nzrdr, mxcolrad, nlevrad, ncolrad,                   &
        !!       2,xradc_p,yradc_p,zradc_1,zradc_2,hgtradc,                   &
        !!                                        ihgtradc,corrad(1,1,2) )
        !!
        !!CALL linearint_3d(nx,ny,nz,w_ctr(1,1,1),xs(1),ys(1),zs(1,1,1),      &
        !!       userad, nzrdr, mxcolrad, nlevrad, ncolrad,                   &
        !!       6,xradc_p,yradc_p,zradc_1,zradc_2,hgtradc,                   &
        !!                                      ihgtradc,corrad(1,1,3) )

        f_innov = 0.0D0
        b_innov = 0.0D0
        i_innov = 0

        DO i = 1, ncolrad

          IF(iuserad(isrcradr(irad(i)),ipass) > 0 .AND. userad(i) ) THEN
            !IF (ownrad(i)) THEN
            !  num = num +1
            !  WRITE(200+100*mp_opt+myproc,*) num,i,iradc(i),jradc(i)
            !END IF

            DO k=1,nlevrad(i)
              IF(qualrad(2,k,i) > 0 .AND. ihgtradc(k,i)>=0 ) THEN

                !vr = ( uazmrad(i)*corrad(k,i,1)                           &
                !    +  vazmrad(i)*corrad(k,i,2) ) * dsdr(k,i)             &
                !    +             corrad(k,i,3)   * dhdr(k,i)

                !IF(1==0) THEN
                !
                !  uradc = u_ctr(iradc(i),jradc(i),kradc(k,i))               &
                !        - anx(iradc(i),jradc(i),kradc(k,i),1)
                !  vradc = v_ctr(iradc(i),jradc(i),kradc(k,i))               &
                !        - anx(iradc(i),jradc(i),kradc(k,i),2)
                !  wradc = w_ctr(iradc(i),jradc(i),kradc(k,i))               &
                !        - anx(iradc(i),jradc(i),kradc(k,i),6)
                !
                !  vr = ( uazmrad(i)*uradc + vazmrad(i)*vradc ) * dsdr(k,i)  &
                !     + wradc * dhdr(k,i)
                !
                !  zvv = vr - odifrad(2,k,i)
                !
                !ELSE

                  uradc = 0.5*(u_ctr(iradc(i),jradc(i),kradc(k,i))+u_ctr(iradc(i)+1,jradc(i),  kradc(k,i))   )
                  vradc = 0.5*(v_ctr(iradc(i),jradc(i),kradc(k,i))+v_ctr(iradc(i),  jradc(i)+1,kradc(k,i))   )
                  wradc = 0.5*(w_ctr(iradc(i),jradc(i),kradc(k,i))+w_ctr(iradc(i),  jradc(i),  kradc(k,i)+1) )
                  !uradc = u_ctr(iradc(i),jradc(i),kradc(k,i))
                  !vradc = v_ctr(iradc(i),jradc(i),kradc(k,i))
                  !wradc = w_ctr(iradc(i),jradc(i),kradc(k,i))

                  vr = ( uazmrad(i)*uradc + vazmrad(i)*vradc ) * dsdr(k,i)  &
                       + wradc * dhdr(k,i)

                  zvv = vr - obsrad(2,k,i)
                  z_innov    = zvv

                !END IF

                oanxrad(2,k,i)= zvv /( qobsrad(2,k,i) )
                zvv           = zvv * oanxrad(2,k,i)

                IF (ownrad(i)) THEN
                  !num = num +1
                  !WRITE(200+100*mp_opt+myproc,*) num,i,k,zvv
                  f_ovrad = f_ovrad +  zvv
                  f_innov = f_innov + z_innov*z_innov
                  b_innov = b_innov + (obsrad(2,k,i)-vr)
                  i_innov = i_innov+1

                END IF
              END IF  ! (qualrad(2,k,i) > 0 )
            END DO  ! nlevrad(i)
          END IF
        END DO

      END IF ! vrob_opt==1

    END IF  ! IF(grdtlt_flag ==1 )

    !CALL mptotali(num)
    !CALL mpsumdp(f_ovrad,1)
    !IF (myproc == 0) WRITE(0,'(5x,a,I2.2,a,I6,a,F30.15,a)') 'Domain ',myproc,' processed ',num,' RAD data, f_ovrad = ',f_ovrad,'.'
    !CALL arpsstop('',0)

    CALL mptotali(i_innov)
    CALL mpsumdp(f_innov,1)
    CALL mpsumdp(b_innov,1)
    !IF (myproc == 0) WRITE(*,'(5x,a,F30.15,a,F30.15,a)')                &
    !  ' RAD VR DA STATS, RMS_innovation/residual = ',SQRT(f_innov/i_innov), &
    !  ', MEAN_innovation/residual = ', b_innov/i_innov,'.'

!-----------------------------------------------------------------------
!
! To assimiate gridded reflectivity data
!
!-----------------------------------------------------------------------

    IF ( ref_opt == 1 ) THEN

      !ref_bkg=0.0 ; ref_diff=0.0

      DO k = 1, nz
        DO j = 1,ny
          DO i = 1, nx
            !np (i,j,k) =     anx(i,j,k,3) + p_ctr (i,j,k)
            !npt(i,j,k) =     anx(i,j,k,4) + t_ctr (i,j,k)
            !nqr(i,j,k) = max(anx(i,j,k,7) + qr_ctr(i,j,k),0.0)
            !nqs(i,j,k) = max(anx(i,j,k,8) + qs_ctr(i,j,k),0.0)
            !nqh(i,j,k) = max(anx(i,j,k,9) + qh_ctr(i,j,k),0.0)
            !np (i,j,k) = p_ctr (i,j,k)
            npt(i,j,k) = t_ctr (i,j,k)
            nqr(i,j,k) = max(qscalar_ctr(i,j,k,P_QR),0.0)
            nqs(i,j,k) = max(qscalar_ctr(i,j,k,P_QS),0.0)
            IF (P_QG > 0 .AND. P_QH > 0) THEN
              nqh(i,j,k) = max(qscalar_ctr(i,j,k,P_QG),0.0) + max(qscalar_ctr(i,j,k,P_QH),0.0)
            ELSE IF (P_QG > 0) THEN
              nqh(i,j,k) = max(qscalar_ctr(i,j,k,P_QG),0.0)
            ELSE IF (P_QH > 0) THEN
              nqh(i,j,k) = max(qscalar_ctr(i,j,k,P_QH),0.0)
            ELSE
              WRITE(*,'(1x,a)') 'ERROR: Cannot find either QG or QH.'
              RETURN
            END IF
          END DO
        END DO
      END DO

      !convert from pt to T
      DO k= 1,nz-1
        DO j= 1,ny-1
          DO i= 1,nx-1
            npt(i,j,k)=npt(i,j,k) * ( (p_ctr(i,j,k)/p0)**rddcp )
          END DO
        END DO
      END DO

      IF (grdtlt_flag ==1) THEN  ! grid-tilted radar format
        ref_bkg = 1.0     ! mm**6/m**3
        CALL reflec_g(nx,ny,nz,ref_mos_3d,intvl_T,thresh_ref,rhobar,    &
                      nqr, nqs, nqh, npt, ref_bkg)
        DO icol=1, ncolrad
          IF(iuserad(isrcradr(irad(icol)),ipass) > 0 .AND. userad(icol) ) THEN
            zs1D(:) = zs(iradc(icol), jradc(icol),:)
            DO k=1,nlevrad(icol)
              IF(ihgtradc(k,icol)>=0 .and. obsrad(1,k,icol)>= thresh_ref ) THEN
                CALL vrzwtavg1D_3dvar(2,nz,k3L(k,icol), k3U(k,icol), zs1D,     &
                    eleva(:,icol),elvang(k,irad(icol)),                        &
                    ref_bkg(iradc(icol),jradc(icol),:),bmwidth(irad(icol)),rff)

                oanxrad(1,k,icol)= (rff - obsrad(1,k,icol)) * wgt_ref
                f_oref = f_oref + oanxrad(1,k,icol) * (rff - obsrad(1,k,icol))
              END IF
            END DO !DO k=1,nlevrad(icol)
          END IF !IF(iuserad(isrcrad(irad(icol))) > 0 .AND. userad(icol) )
        END DO !DO icol=1, ncolrad

      ELSE ! grid-gridded radar format

        i_innov = 0
        f_innov = 0.0D0
        b_innov = 0.0D0

        CALL reflec_g(nx,ny,nz,ref_mos_3d,intvl_T,thresh_ref,rhobar,    &
                      nqr, nqs, nqh, npt, ref_bkg)

        !CALL DualPolObs(nx,ny,nz,rhobar,npt,qscalar_ctr,ref_bkg,       &
        !               !ref_h,Zdr,kdph,rhv,dualpol)
        !               tem1,tem2,tem3,tem4,6)

        !i_innov = 0
        !f_innov = 0.0D
        DO k = kbgn,kend
          DO j = jbgn, jend
            DO i = ibgn, iend

              !IF (ref_mos_3d(i,j,k) >= thresh_ref ) THEN
              IF (ref_mos_3d(i,j,k) > 0.0 ) THEN
                ref_diff(i,j,k)=(ref_bkg(i,j,k)-ref_mos_3d(i,j,k))* wgt_ref
                !IF ((ref_diff(i,j,k)/wgt_ref)<=(3.0*sqrt(1.0/wgt_ref))) THEN
                f_oref = f_oref + ref_diff(i,j,k)*(ref_bkg(i,j,k)-ref_mos_3d(i,j,k) )
                i_innov = i_innov +1
                z_innov = (ref_bkg(i,j,k)-ref_mos_3d(i,j,k))
                f_innov = f_innov + z_innov * z_innov
                b_innov = b_innov + (ref_mos_3d(i,j,k)-ref_bkg(i,j,k))

              !ELSE IF ( ref_mos_3d(i,j,k) > -9000.0 ) THEN
              !  ref_diff(i,j,k)=(ref_bkg(i,j,k)-0.0)* wgt_ref
              !  f_oref = f_oref + ref_diff(i,j,k)*(ref_bkg(i,j,k)-0.0 )
              !  f_oref = f_oref +                                       &
              !           qscalar_ctr(i,j,k,P_QR)*qscalar_ctr(i,j,k,P_QR)/4.0E-6   +       &
              !           qscalar_ctr(i,j,k,P_QS)*qscalar_ctr(i,j,k,P_QS)/1.0E-6
!             !             qr_ctr(i,j,k)*qr_ctr(i,j,k)/1.0E-6   +               &
!             !             qs_ctr(i,j,k)*qs_ctr(i,j,k)/1.0E-6   +               &
!             !             qh_ctr(i,j,k)*qh_ctr(i,j,k)/1.0E-6
!             !             nqr(i,j,k)*nqr(i,j,k)/1.0E-6   +               &
!             !             nqs(i,j,k)*nqs(i,j,k)/1.0E-6   +               &
!             !             nqh(i,j,k)*nqh(i,j,k)/1.0E-6
              !
              !  IF (P_QG > 0 .AND. P_QH > 0) THEN
              !    f_oref = f_oref + qscalar_ctr(i,j,k,P_QG)*qscalar_ctr(i,j,k,P_QG)/4.0E-6
              !    f_oref = f_oref + qscalar_ctr(i,j,k,P_QH)*qscalar_ctr(i,j,k,P_QH)/4.0E-6
              !  ELSE IF (P_QG > 0) THEN
              !    f_oref = f_oref + qscalar_ctr(i,j,k,P_QG)*qscalar_ctr(i,j,k,P_QG)/4.0E-6
              !  ELSE IF (P_QH > 0) THEN
              !    f_oref = f_oref + qscalar_ctr(i,j,k,P_QH)*qscalar_ctr(i,j,k,P_QH)/4.0E-6
              !  ELSE
              !    WRITE(*,'(1x,a)') 'ERROR: Cannot find either QG or QH.'
              !    RETURN
              !  END IF

              END IF

            END DO
          END DO
        END DO
      END IF !IF (grdtlt_flag ==1)

      CALL mptotali(i_innov)
      CALL mpsumdp(f_innov,1)
      CALL mpsumdp(b_innov,1)
      !IF (myproc == 0) WRITE(*,'(5x,a,F30.15,F30.15,a,I0,a)') ' RAD Ref data, f_innov,b_innov = ',f_innov/i_innov,b_innov/i_innov,', i_innov = ', i_innov,'.'

      !npt=0.0; np=0.0; nqr=0.0; nqs=0.0; nqh=0.0; ref_bkg=0.0
      !NULLIFY (npt,np,nqr,nqs,nqh,ref_bkg)
    END IF ! ref_opt ==1

    RETURN
  END SUBROUTINE radargrid_costf

  !#####################################################################

  SUBROUTINE radargrid_gradt(nx,ny,nz,zs,u_grd,v_grd,w_grd,qscalar_grd, &
                             p_ctr,t_ctr,qscalar_ctr,                   &
                             ref_mos_3d,intvl_T,thresh_ref,rhobar,      &
                             ibgn,iend,jbgn,jend,kbgn,kend,             &
                             vrob_opt,ref_opt,                          &
                             npt,nqr,nqs,nqh,ref_bkg,refz_org,          &
                             tem1,tem2,tem3,istatus)

    INCLUDE 'globcst.inc'
    INCLUDE 'bndry.inc'
    INCLUDE 'phycst.inc'
    INCLUDE 'mp.inc'

    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL(P), INTENT(IN)  :: zs(nx,ny,nz)
    REAL(P), INTENT(INOUT)  :: u_grd(nx,ny,nz),v_grd(nx,ny,nz),w_grd(nx,ny,nz)
    REAL(P), INTENT(INOUT)  :: qscalar_grd(nx,ny,nz,nscalarq)
    REAL(P), INTENT(IN)  :: p_ctr(nx,ny,nz),t_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  :: qscalar_ctr(nx,ny,nz,nscalarq)

    REAL(P), INTENT(IN)  :: ref_mos_3d(nx,ny,nz)
    REAL(P), INTENT(IN)  :: rhobar(nx,ny,nz)
    REAL(P), INTENT(IN)  :: intvl_T, thresh_ref

    INTEGER, INTENT(IN)  :: ibgn,iend,jbgn,jend,kbgn,kend
    INTEGER, INTENT(IN)  :: vrob_opt,ref_opt

    REAL(P), INTENT(OUT) :: npt(nx,ny,nz)
    REAL(P), INTENT(OUT) :: nqr(nx,ny,nz), nqs(nx,ny,nz), nqh(nx,ny,nz)
    REAL(P), INTENT(OUT) :: ref_bkg(nx,ny,nz),refz_org(nx,ny,nz)
    REAL(P), INTENT(OUT) :: tem1(nx,ny,nz), tem2(nx,ny,nz), tem3(nx,ny,nz)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j,k,icol
    REAL :: zs1D(nz), vrsc(nz)
    REAL :: uradc,vradc,wradc
    REAL :: vr,zv

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (grdtlt_flag == 1 ) THEN !grid-tilted radar format

      DO icol = 1, ncolrad
        IF ( ownrad(icol) ) THEN
          vrsc(:) = 0.0
          zs1D(:) = zs(iradc(icol), jradc(icol),:)

          DO k=nlevrad(icol), 1, -1
            IF(qualrad(2,k,icol) > 0 .AND. ihgtradc(k,icol)>=0 ) THEN
              CALL advrzwtavg1D(1,nz,k3L(k,icol), k3U(k,icol), zs1D,    &
                            eleva(:,icol),elvang(k,irad(icol)),vrsc,    &
                            bmwidth(irad(icol)),oanxrad(2,k,icol),      &
                            zs1D) ! this second zs1D is only used to hold places for refz_org parameter.
            END IF
          END DO
          !!! vrsc  ---> u_grd, v_grd, w_grd
          DO k=2, nz-1
            uradc = uazmrad(icol)*vrsc(k) *dsdr(k,icol)
            vradc = vazmrad(icol)*vrsc(k) *dsdr(k,icol)
            wradc =               vrsc(k) *dhdr(k,icol)
            u_grd(iradc(icol),  jradc(icol),  k)   = u_grd(iradc(icol),jradc(icol),k) + 0.5*uradc
            u_grd(iradc(icol)+1,jradc(icol),  k)   = u_grd(iradc(icol),jradc(icol),k) + 0.5*uradc
            v_grd(iradc(icol),  jradc(icol),  k)   = v_grd(iradc(icol),jradc(icol),k) + 0.5*vradc
            v_grd(iradc(icol),  jradc(icol)+1,k)   = v_grd(iradc(icol),jradc(icol),k) + 0.5*vradc
            w_grd(iradc(icol),  jradc(icol),  k)   = w_grd(iradc(icol),jradc(icol),k) + 0.5*wradc
            w_grd(iradc(icol),  jradc(icol),  k+1) = w_grd(iradc(icol),jradc(icol),k) + 0.5*wradc
          END DO
        END IF !IF (userad(icol)) THEN
      END DO !DO i = 1, ncolrad

    ELSE !gridded radar format

      IF( vrob_opt==1) THEN
        !corrad=0.0

        DO i = 1, ncolrad
          IF (userad(i)) THEN
            DO k=1,nlevrad(i)
              IF(qualrad(2,k,i) > 0 .AND. ihgtradc(k,i)>=0 ) THEN
                !!corrad(k,i,1) = uazmrad(i)*oanxrad(2,k,i)*dsdr(k,i)
                !!corrad(k,i,3) =            oanxrad(2,k,i)*dhdr(k,i)
                !!corrad(k,i,2) = vazmrad(i)*oanxrad(2,k,i)*dsdr(k,i)

                uradc = uazmrad(i)*oanxrad(2,k,i)*dsdr(k,i)
                vradc = vazmrad(i)*oanxrad(2,k,i)*dsdr(k,i)
                wradc = oanxrad(2,k,i)*dhdr(k,i)

                !u_grd(iradc(i),jradc(i),kradc(k,i) ) =                  &
                !                uazmrad(i)*oanxrad(2,k,i)*dsdr(k,i)     &
                !              + u_grd(iradc(i),jradc(i),kradc(k,i) )
                !
                !w_grd(iradc(i),jradc(i),kradc(k,i) ) =                  &
                !                           oanxrad(2,k,i)*dhdr(k,i)     &
                !              + w_grd(iradc(i),jradc(i),kradc(k,i) )
                !
                !v_grd(iradc(i),jradc(i),kradc(k,i) ) =                  &
                !                vazmrad(i)*oanxrad(2,k,i)*dsdr(k,i)     &
                !              + v_grd(iradc(i),jradc(i),kradc(k,i) )

                u_grd(iradc(i),  jradc(i),kradc(k,i) ) = 0.5*uradc + u_grd(iradc(i),  jradc(i),kradc(k,i) )
                u_grd(iradc(i)+1,jradc(i),kradc(k,i) ) = 0.5*uradc + u_grd(iradc(i)+1,jradc(i),kradc(k,i) )

                v_grd(iradc(i),jradc(i),  kradc(k,i) ) = 0.5*vradc + v_grd(iradc(i),jradc(i),  kradc(k,i) )
                v_grd(iradc(i),jradc(i)+1,kradc(k,i) ) = 0.5*vradc + v_grd(iradc(i),jradc(i)+1,kradc(k,i) )

                w_grd(iradc(i),jradc(i),kradc(k,i) )   = 0.5*wradc + w_grd(iradc(i),jradc(i),kradc(k,i)   )
                w_grd(iradc(i),jradc(i),kradc(k,i)+1 ) = 0.5*wradc + w_grd(iradc(i),jradc(i),kradc(k,i)+1 )

                !u_grd( iradc(i),jradc(i),kradc(k,i) ) = uradc + u_grd( iradc(i),jradc(i),kradc(k,i) )
                !v_grd( iradc(i),jradc(i),kradc(k,i) ) = vradc + v_grd( iradc(i),jradc(i),kradc(k,i) )
                !w_grd( iradc(i),jradc(i),kradc(k,i) ) = wradc + w_grd( iradc(i),jradc(i),kradc(k,i) )

              END IF
            END DO
          END IF !IF (userad(i)) THEN
        END DO !DO i = 1, ncolrad

        !!CALL adlinearint_3d(nx,ny,nz,u_grd,xs,ys,zs,                      &
        !!       userad,nzrdr, mxcolrad, nlevrad, ncolrad,1,xradc_p,yradc_p,&
        !!       zradc_1,zradc_2,hgtradc,ihgtradc,corrad(:,:,1), tem4 )
        !!
        !!CALL adlinearint_3d(nx,ny,nz,v_grd,xs,ys,zs,                      &
        !!       userad,nzrdr, mxcolrad, nlevrad, ncolrad,2,xradc_p,yradc_p,&
        !!       zradc_1,zradc_2,hgtradc,ihgtradc,corrad(:,:,2), tem4 )
        !!
        !!CALL adlinearint_3d(nx,ny,nz,w_grd,xs,ys,zs,                      &
        !!       userad,nzrdr, mxcolrad, nlevrad, ncolrad,6,xradc_p,yradc_p,&
        !!       zradc_1,zradc_2,hgtradc,ihgtradc,corrad(:,:,3), tem4 )

        IF (mp_opt > 0) THEN
          !CALL acct_interrupt(mp_acct)
          CALL mpsendrecv2dew(u_grd, nx, ny, nz, ebc, wbc, 1, tem1)
          CALL mpsendrecv2dew(u_grd, nx, ny, nz, ebc, wbc, 0, tem1)
          CALL mpsendrecv2dns(u_grd, nx, ny, nz, nbc, sbc, 1, tem1)

          CALL mpsendrecv2dew(v_grd, nx, ny, nz, ebc, wbc, 2, tem1)
          CALL mpsendrecv2dns(v_grd, nx, ny, nz, nbc, sbc, 2, tem1)
          CALL mpsendrecv2dns(v_grd, nx, ny, nz, nbc, sbc, 0, tem1)

          CALL mpsendrecv2dew(w_grd, nx, ny, nz, ebc, wbc, 3, tem1)
          CALL mpsendrecv2dns(w_grd, nx, ny, nz, nbc, sbc, 3, tem1)
          !CALL acct_stop_inter
        END IF
      END IF ! vrob_opt==1

    END IF  !IF (grdtlt_flag == 1 )


    IF ( ref_opt == 1) THEN

      ref_bkg=0.0
      DO k = 1, nz
        DO j = 1,ny
          DO i = 1, nx
            !np (i,j,k) =       p_ctr(i,j,k)
            npt(i,j,k) =       t_ctr(i,j,k)
            nqr(i,j,k) = max( qscalar_ctr(i,j,k,P_QR),0.0 )
            nqs(i,j,k) = max( qscalar_ctr(i,j,k,P_QS),0.0 )

            IF (P_QG > 0 .AND. P_QH > 0) THEN
              nqh(i,j,k) = max(qscalar_ctr(i,j,k,P_QG),0.0) + max(qscalar_ctr(i,j,k,P_QH),0.0)
            ELSE IF (P_QG > 0) THEN
              nqh(i,j,k) = max(qscalar_ctr(i,j,k,P_QG),0.0)
            ELSE IF (P_QH > 0) THEN
              nqh(i,j,k) = max(qscalar_ctr(i,j,k,P_QH),0.0)
            ELSE
              WRITE(*,'(1x,a)') 'ERROR: Cannot find either QG or QH.'
              RETURN
            END IF

          END DO
        END DO
      END DO

      !convert from pt to T
      DO k= 1,nz-1
        DO j= 1,ny-1
          DO i= 1,nx-1
            npt(i,j,k)=npt(i,j,k) * ( (p_ctr(i,j,k)/p0)**rddcp )
          END DO
        END DO
      END DO

      IF (grdtlt_flag ==1) THEN
        refz_org = 1.0 ! mm**6/m**3
        CALL reflec_g(nx,ny,nz,ref_mos_3d,intvl_T,thresh_ref,rhobar,    &
                      nqr, nqs, nqh, npt, refz_org)
        DO icol = 1, ncolrad
          IF ( ownrad(icol) ) THEN
            zs1D(:) = zs(iradc(icol), jradc(icol),:)
            DO k=nlevrad(icol), 1, -1
              IF(ihgtradc(k,icol)>=0 .and. obsrad(1,k,icol)>= thresh_ref ) THEN
                CALL advrzwtavg1D(2,nz,k3L(k,icol), k3U(k,icol), zs1D,  &
                               eleva(:,icol),elvang(k,irad(icol)),      &
                               ref_bkg(iradc(icol),jradc(icol),:),      &
                               bmwidth(irad(icol)),oanxrad(1,k,icol),   &
                               refz_org(iradc(icol),jradc(icol),:) )
              END IF
            END DO
          END IF !IF (userad(icol))
        END DO !DO icol = 1, ncolrad
        tem1=0.0; tem2=0.0; tem3=0.0  !store qr, qs qh respectively
        CALL ad_reflec_g(nx,ny,nz,ref_mos_3d,intvl_T,thresh_ref,rhobar, &
                         tem1, tem2, tem3, npt, ref_bkg, nqr,nqs,nqh)

      ELSE
        ref_bkg=ref_diff
        tem1=0.0; tem2=0.0; tem3=0.0  !store qr, qs qh respectively
        CALL ad_reflec_g(nx,ny,nz,ref_mos_3d,intvl_T,thresh_ref,rhobar, &
                         tem1, tem2, tem3, npt, ref_bkg, nqr,nqs,nqh)
        DO k = kbgn,kend
          DO j = jbgn, jend
            DO i = ibgn, iend
              IF (ref_mos_3d(i,j,k) > 0.0) THEN
                              qscalar_grd(i,j,k,P_QR)=qscalar_grd(i,j,k,P_QR) + tem1(i,j,k)
                              qscalar_grd(i,j,k,P_QS)=qscalar_grd(i,j,k,P_QS) + tem2(i,j,k)
                IF (P_QH > 0) qscalar_grd(i,j,k,P_QH)=qscalar_grd(i,j,k,P_QH) + tem3(i,j,k)
                IF (P_QG > 0) qscalar_grd(i,j,k,P_QG)=qscalar_grd(i,j,k,P_QG) + tem3(i,j,k)

              !ELSE IF (ref_mos_3d(i,j,k) > -9000.0) THEN
              !                qscalar_grd(i,j,k,P_QR)=qscalar_grd(i,j,k,P_QR)+qscalar_ctr(i,j,k,P_QR)/4.0E-6
              !                qscalar_grd(i,j,k,P_QS)=qscalar_grd(i,j,k,P_QS)+qscalar_ctr(i,j,k,P_QS)/1.0E-6
              !  IF (P_QH > 0) qscalar_grd(i,j,k,P_QH)=qscalar_grd(i,j,k,P_QH)+qscalar_ctr(i,j,k,P_QH)/4.0E-6
              !  IF (P_QG > 0) qscalar_grd(i,j,k,P_QG)=qscalar_grd(i,j,k,P_QG)+qscalar_ctr(i,j,k,P_QG)/4.0E-6
              END IF
            END DO
          END DO
        END DO

      END IF        ! IF (grdtlt_flag ==1)

      IF (mp_opt > 0) THEN
        !CALL acct_interrupt(mp_acct)
        CALL mpsendrecv2dew(qscalar_grd(:,:,:,P_QR), nx, ny, nz, ebc, wbc, 0, tem1)
        CALL mpsendrecv2dns(qscalar_grd(:,:,:,P_QR), nx, ny, nz, nbc, sbc, 0, tem1)

        CALL mpsendrecv2dew(qscalar_grd(:,:,:,P_QS), nx, ny, nz, ebc, wbc, 0, tem1)
        CALL mpsendrecv2dns(qscalar_grd(:,:,:,P_QS), nx, ny, nz, nbc, sbc, 0, tem1)

        IF (P_QH > 0) THEN
        CALL mpsendrecv2dew(qscalar_grd(:,:,:,P_QH), nx, ny, nz, ebc, wbc, 0, tem1)
        CALL mpsendrecv2dns(qscalar_grd(:,:,:,P_QH), nx, ny, nz, nbc, sbc, 0, tem1)
        END IF

        IF (P_QG > 0) THEN
        CALL mpsendrecv2dew(qscalar_grd(:,:,:,P_QG), nx, ny, nz, ebc, wbc, 0, tem1)
        CALL mpsendrecv2dns(qscalar_grd(:,:,:,P_QG), nx, ny, nz, nbc, sbc, 0, tem1)
        END IF
        !CALL acct_stop_inter
      END IF

      !npt=0.0; np=0.0; nqr=0.0; nqs=0.0; nqh=0.0; ref_bkg=0.0
      !NULLIFY (npt,np,nqr,nqs,nqh,ref_bkg,refz_org)
    END IF      ! IF (ref_opt==1)

    RETURN
  END SUBROUTINE radargrid_gradt

  !#####################################################################

  SUBROUTINE radargrid_final(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    DEALLOCATE (xradc_p)
    DEALLOCATE (yradc_p)

    DEALLOCATE (zradc_1)
    DEALLOCATE (zradc_2)
    DEALLOCATE (ihgtradc)

    DEALLOCATE (ownrad,   STAT = istatus )
    DEALLOCATE (ref_diff, STAT = istatus )

    RETURN
  END SUBROUTINE radargrid_final

  !
  !---------------------------------------------------------------------
  !
  SUBROUTINE advrzwtavg1D(idvrz,nz,k3ls,k3us,zp,elvsc,elvang,var ,  &
                          bmwidths,rdrWgt,refz_org)
  !-----------------------------------------------------------------------
  !
  !  AUTHOR: Guoqing Ge
  !   Guoqing Ge   08/08/2013
  !-----------------------------------------------------------------------
  !
  !  INPUT :
  !
  !  idvrz          ! =1(Vr), =2(Zhh)
  !  nz             ! The number of grid points in vertical direction
  !  k3l            ! k index of the grid point at the lowest level
  !  k3u            ! k index of the grid point at the highest level
  !                   within the radar beam
  !  zp(nz)         ! vertical coordinate
  !  elvsc(nz)      ! elevation angle of scaler point in the column
  !  elvang         ! elevation angle of obervation point
  !  wesqinv        ! 4*ln4/beamwid
  !  rff(nz)       ! ref at scaler point
  !  rdrWgt         ! weighted average of Vr, Ref
  !
  !  OUTPUT :
  !  var(nz)       ! vr at scaler point
  !-----------------------------------------------------------------------
    IMPLICIT NONE
  !
  ! INPUT
  !
    INTEGER :: idvrz
    INTEGER :: nz
    INTEGER :: k3ls,k3us
    REAL :: zp(nz)
    REAL :: elvsc(nz)
    REAL :: elvang
    REAL :: var(nz)
    REAL :: bmwidths
    REAL :: refz_org(nz)  !z (mm**6/m**3)
  !
  ! OUTPUT
  !
    REAL :: rdrWgt
  !
  ! LOCAL
  !
    INTEGER :: k
    REAL :: wesqinv
    REAL :: delelv
    REAL :: depth      ! integration element
    REAL :: wgtg, wgtr(nz), wgtv(nz)
    REAL :: sumwr, sumr, sumwv, sumv
    REAL :: temz, temz_org
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
    wesqinv= 4.0*alog(4.0)/(bmwidths*bmwidths)

    sumwr=0.
    sumr=0.
    sumwv=0.
    sumv=0.
    temz=0.
    temz_org=0.

    DO k=k3ls,k3us
      IF(elvsc(k) > -99.)THEN
        delelv=elvsc(k)-elvang
        depth=0.5*(zp(k+1)-zp(k))
        wgtg=(delelv*delelv)*wesqinv
        wgtr(k)=depth*exp(-wgtg)
        SELECT CASE (idvrz)
        CASE (1)                    ! vr
          !if(zwopt == 1)then  !zwopt, gge.tmp.debug
          !  wgtv(k)=refz(k)*wgtr(k)
          !else
            wgtv(k)=wgtr(k)
          !endif
          sumwv=sumwv+wgtv(k)
        CASE (2)                      ! ref
          sumwr=sumwr+wgtr(k)
          sumr=sumr+wgtr(k)*refz_org(k)
        END SELECT
      ENDIF
    ENDDO

    SELECT CASE (idvrz)
    CASE (1)                      ! Solve for radial velocity
      IF(sumwv > 0.) THEN
        sumv = rdrWgt/sumwv
      ELSE
        sumv = 0.0
      ENDIF
    CASE (2)  !adjoint code not implemented for the following!
      IF(sumwr > 0.) THEN
        temz_org=sumr/sumwr
        temz = (10.0/LOG(10.0)/temz_org) *rdrwgt
        sumr = temz/sumwr
      ELSE
        sumr = 0.0
      END IF
    END SELECT

    DO k=k3us,k3ls,-1
      IF(elvsc(k) > -99.)THEN
        SELECT CASE (idvrz)
        CASE (1)                    ! vr
          var(k) = var(k) + wgtv(k) * sumv
        CASE (2)                    ! ref
          var(k) = var(k) + wgtr(k) * sumr
        END SELECT
      ENDIF
    ENDDO

    RETURN
  END SUBROUTINE advrzwtavg1D

END MODULE module_radar_grid
