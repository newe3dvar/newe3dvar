MODULE module_modelArray
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! MODULE contains variables for the atmospheric model states,
! i.e. background state vector
!
! HISTORY:
!
!   11/06/2007 (Yunheng Wang)
!   Initial version.
!
!   03/26/2016 (Y. Wang)
!   Reorganized to use less memory when running with the WRF model.
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
  USE module_arpsArray

  IMPLICIT NONE
  SAVE
!
!
!-----------------------------------------------------------------------
!
!  Arrays defining model grid
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: x     (:)      ! The x-coord. of the physical and
                                      ! computational grid. Defined at u-point.
  REAL, ALLOCATABLE :: y     (:)      ! The y-coord. of the physical and
                                      ! computational grid. Defined at v-point.
  REAL, ALLOCATABLE :: z     (:)      ! The z-coord. of the computational grid.
                                      ! Defined at w-point on the staggered grid.
  REAL, ALLOCATABLE :: zp    (:,:,:)  ! The physical height coordinate defined at
                                      ! w-point of the staggered grid.
  REAL, ALLOCATABLE :: zpsoil(:,:,:)  ! Soil level depth.

  REAL, ALLOCATABLE :: hterain(:,:)   ! The height of the terrain.
  REAL, ALLOCATABLE :: mapfct(:,:,:)  ! Map factors at scalar, u and v points

  REAL, ALLOCATABLE :: j1    (:,:,:)  ! Coordinate transformation Jacobian defined
                                      ! as - d( zp )/d( x ).
  REAL, ALLOCATABLE :: j2    (:,:,:)  ! Coordinate transformation Jacobian defined
                                      ! as - d( zp )/d( y ).
  REAL, ALLOCATABLE :: j3    (:,:,:)  ! Coordinate transformation Jacobian defined
                                      ! as d( zp )/d( z ).
  REAL, ALLOCATABLE :: aj3x  (:,:,:)  ! Coordinate transformation Jacobian defined
                                      ! as d( zp )/d( z ) AVERAGED IN THE X-DIR.
  REAL, ALLOCATABLE :: aj3y  (:,:,:)  ! Coordinate transformation Jacobian defined
                                      ! as d( zp )/d( z ) AVERAGED IN THE Y-DIR.
  REAL, ALLOCATABLE :: aj3z  (:,:,:)  ! Coordinate transformation Jacobian defined
                                      ! as d( zp )/d( z ) AVERAGED IN THE Z-DIR.
  REAL, ALLOCATABLE :: j3inv (:,:,:)  ! Inverse of j3

  REAL, ALLOCATABLE :: psfc(:,:), varsfc(:,:,:)
  REAL, ALLOCATABLE :: znw(:), znu(:), dnw(:), dn(:), fnm(:), fnp(:)
  REAL              :: p_top, cf1, cf2, cf3

!
!-----------------------------------------------------------------------
!
!  ARPS Time-dependent variables
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: u     (:,:,:)  ! Total u-velocity (m/s)
  REAL, ALLOCATABLE :: v     (:,:,:)  ! Total v-velocity (m/s)
  REAL, ALLOCATABLE :: w     (:,:,:)  ! Total w-velocity (m/s)
  REAL, ALLOCATABLE :: phprt (:,:,:)  ! Perturbation geopotential (m**2/s**2)

  REAL, ALLOCATABLE :: pt    (:,:,:)  ! Total potential temperature (K)
  REAL, ALLOCATABLE :: pres  (:,:,:)  ! Total pressure (Pascal)
  REAL, ALLOCATABLE :: mu    (:,:)    ! Mass of dry air (Pascal)
  REAL, ALLOCATABLE :: mub   (:,:)

  REAL, ALLOCATABLE :: qv    (:,:,:)  ! Water vapor specific humidity (kg/kg)
  REAL, ALLOCATABLE :: wmixr (:,:,:)  ! Water mixing ratio (kg/kg) - used for WRF model only

  REAL, ALLOCATABLE :: qscalar(:,:,:,:)

  REAL, ALLOCATABLE :: phbar (:,:,:)  ! Base state geopotential Height (m**2/s**2)
  REAL, ALLOCATABLE :: rhobar(:,:,:)  ! Base state density
  REAL, ALLOCATABLE :: qvbar (:,:,:)  ! Base state water vapor specific
                                      ! humidity(kg/kg)

  REAL, ALLOCATABLE :: tk(:,:,:)      ! Temperature in K

  LOGICAL, PRIVATE :: array_allocated  = .FALSE.

  CONTAINS

  SUBROUTINE allocate_modelArray(nx,ny,nz,nzsoil,ntypes,model_opt,istatus)

    INTEGER, INTENT(IN)  :: nx,ny,nz,nzsoil,ntypes,model_opt
    INTEGER, INTENT(OUT) :: istatus

    INCLUDE 'globcst.inc'
!
!-----------------------------------------------------------------------
!
!  Allocate adas arrays
!
!-----------------------------------------------------------------------
!
    ALLOCATE(x(nx),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:x")
    ALLOCATE(y(ny),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:y")
    ALLOCATE(z(nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:z")

    ALLOCATE(hterain(nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:hterain")

    x=0.0
    y=0.0
    z=0.0

    hterain=0.0

    ALLOCATE(zp  (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:zp")
    ALLOCATE(zpsoil(nx,ny,nzsoil),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:zpsoil")
    ALLOCATE(j1  (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:j1")
    ALLOCATE(j2  (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:j2")
    ALLOCATE(j3  (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:j3")
    ALLOCATE(j3inv(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:j3inv")

    zp      = 0.0
    zpsoil  = 0.0
    j1      = 0.0
    j2      = 0.0
    j3      = 0.0
    j3inv   = 0.0

    ALLOCATE(u    (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:u")
    ALLOCATE(v    (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:v")
    ALLOCATE(w    (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:w")

    ALLOCATE(qv   (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:qv")
    ALLOCATE(qscalar(nx,ny,nz,nscalar),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:qscalar")
    ALLOCATE(pt(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:pt")
    ALLOCATE(pres (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:pres")

    u    = 0.0
    v    = 0.0
    w    = 0.0
    pt   = 0.0
    pres = 0.0

    qv      = 0.0
    qscalar = 0.0

    ALLOCATE(mapfct (nx,ny,8),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:mapfct")
    mapfct = 0.0

    ALLOCATE(aj3x(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:aj3x")
    ALLOCATE(aj3y(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:aj3y")
    ALLOCATE(aj3z(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:aj3z")

    aj3x=0.0
    aj3y=0.0
    aj3z=0.0

    IF (model_opt == 2) THEN  ! only WRF model use these

      ALLOCATE(znu(nz),  STAT = istatus)
      ALLOCATE(znw(nz),  STAT = istatus)
      ALLOCATE(dnw(nz),  STAT = istatus)
      ALLOCATE(dn (nz),  STAT = istatus)
      ALLOCATE(fnm(nz),  STAT = istatus)
      ALLOCATE(fnp(nz),  STAT = istatus)

      znu = 0.0
      znw = 0.0
      dnw = 0.0
      dn  = 0.0
      fnm = 0.0
      fnp = 0.0

      ALLOCATE(phprt(nx,ny,nz),STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:phprt")
      ALLOCATE(phbar(nx,ny,nz),STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:phbar")

      ALLOCATE(psfc(nx,ny),      STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:psfc")

      ALLOCATE(varsfc(nx,ny,4),  STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:varsfc(T2,Q2,U10,V10)")

      ALLOCATE(mu(nx,ny),      STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:mu")
      ALLOCATE(mub(nx,ny),     STAT = istatus)
      CALL check_alloc_status(istatus, "news3dvar:mub")

      ALLOCATE(wmixr(nx,ny,nz),STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:wmixr")

      psfc  = 0.0
      varsfc= 0.0
      mub   = 0.0
      phbar = 0.0
      wmixr = 0.0

    ELSE                     ! only ARPS model use this

      ALLOCATE(dnw(1),  STAT = istatus)           ! Those variables appear as arguments in 3dvar
      ALLOCATE(dn (1),  STAT = istatus)           ! Those variables appear as arguments in 3dvar
      ALLOCATE(fnm(1),  STAT = istatus)           ! Those variables appear as arguments in 3dvar
      ALLOCATE(fnp(1),  STAT = istatus)           ! Those variables appear as arguments in 3dvar
                                                  ! Those variables appear as arguments in 3dvar
      dnw = 0.0                                   ! Those variables appear as arguments in 3dvar
      dn  = 0.0                                   ! Those variables appear as arguments in 3dvar
      fnm = 0.0                                   ! Those variables appear as arguments in 3dvar
      fnp = 0.0

      ALLOCATE(psfc(1,1),      STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:psfc")
      psfc = 0.0

      ALLOCATE(varsfc(1,1,1),  STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:varsfc(T2,Q2,U10,V10)")
      varsfc = 0.0

      CALL allocate_arpsArray(nx,ny,nz,nzsoil,ntypes,istatus)

    END IF

    ALLOCATE(rhobar(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:rhobar")
    ALLOCATE(qvbar(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:qvbar")

    rhobar=0.0
    qvbar=0.0

    ALLOCATE(tk   (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:tk")

    array_allocated  = .TRUE.

    RETURN
  END  SUBROUTINE allocate_modelArray

  !#####################################################################

  SUBROUTINE deallocate_modelArray(model_opt,istatus)

    INTEGER, INTENT(IN)  :: model_opt
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    DEALLOCATE(x,y,z )
    DEALLOCATE(hterain,mapfct )

    DEALLOCATE(zp,zpsoil )
    DEALLOCATE(j1,j2,j3,j3inv )
    DEALLOCATE(aj3x,aj3y,aj3z )

    DEALLOCATE(u,v,w )
    DEALLOCATE(pt,pres  )

    DEALLOCATE(qv    )
    DEALLOCATE(qscalar )

    DEALLOCATE(rhobar )

    IF( model_opt == 2) THEN
      DEALLOCATE(znw,znu,dnw )
      DEALLOCATE(phprt, phbar )
      DEALLOCATE(mu, mub  )
      DEALLOCATE(wmixr )
    ELSE
      CALL deallocate_arpsArray(istatus)
    END IF

    DEALLOCATE( tk )
    array_allocated = .FALSE.

    RETURN
  END SUBROUTINE deallocate_modelArray

END module module_modelArray
