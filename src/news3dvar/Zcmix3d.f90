
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE CMIX2UVW0                  ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE cmix2uvw0(nx,ny,nz,                                           &
           u,v,w, ubar,vbar,rhostr,                                     &
           umix,vmix,wmix,                                              &
           ustr,vstr,wstr,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the second order computational mixing terms for the momentum
!  equations. Computational mixing is applied to velocity perturbations
!  only. These terms are placed in the arrays umix, vmix and wmix.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/3/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  4/2/92 (M. Xue and H. Jin)
!  modify for using terrain
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        Verticle component of Cartesian velocity at a
!             given time level (m/s)
!
!    ubar     Base state zonal velocity component (m/s)
!    vbar     Base state meridional velocity component (m/s)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!
!    umix     Array containing turbulent mixing on u
!    vmix     Array containing turbulent mixing on v
!    wmix     Array containing turbulent mixing on w
!
!  OUTPUT:
!
!    umix     Turbulent and computational mixing on u.
!    vmix     Turbulent and computational mixing on v.
!    wmix     Turbulent and computational mixing on w.
!
!  WORK ARRAYS:
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.

  REAL :: umix  (nx,ny,nz)     ! Total mixing in u-eq.
  REAL :: vmix  (nx,ny,nz)     ! Total mixing in v-eq.
  REAL :: wmix  (nx,ny,nz)     ! Total mixing in w-eq.

  REAL :: ustr  (nx,ny,nz)     ! u * rhostr
  REAL :: vstr  (nx,ny,nz)     ! v * rhostr
  REAL :: wstr  (nx,ny,nz)     ! w * rhostr

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k, onvf
  REAL :: dxinv2, dyinv2
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'phycst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'globcst.inc'
!
!-----------------------------------------------------------------------
!

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  IF the coefficients of computational mixing in both horizontal
!  and vertical directions are zero, exit this subroutine.
!
!-----------------------------------------------------------------------
!

  IF( cfcmh2 == 0.0 .AND. cfcmv2 == 0.0 ) RETURN


  dxinv2 = dxinv**2
  dyinv2 = dyinv**2
!
!-----------------------------------------------------------------------
!
!  Calculate ustr=rhostr*u', vstr=rhostr*v', wstr=rhostr*w'
!
!-----------------------------------------------------------------------
!
  CALL rhouvw0(nx,ny,nz,rhostr,tem1,tem2,tem3)

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx
        ustr(i,j,k)=(u(i,j,k)-ubar(i,j,k))*tem1(i,j,k)
      END DO
    END DO
  END DO

  DO k=1,nz-1
    DO j=1,ny
      DO i=1,nx-1
        vstr(i,j,k)=(v(i,j,k)-vbar(i,j,k))*tem2(i,j,k)
      END DO
    END DO
  END DO

  DO k=1,nz
    DO j=1,ny-1
      DO i=1,nx-1
        wstr(i,j,k)=w(i,j,k)*tem3(i,j,k)
      END DO
    END DO
  END DO

  IF( cfcmh2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the second order
!  horizontal computational mixing on u'
!
!-----------------------------------------------------------------------
!
    CALL difxx0(ustr, nx,ny,nz, 2,nx-1,1,ny-1,2,nz-2, dx, tem1)

    CALL difyy0(ustr, nx,ny,nz, 2,nx-1,2,ny-2,2,nz-2, dy, tem2)

    DO k=2,nz-2
      DO j=2,ny-2
        DO i=2,nx-1
          umix(i,j,k)=umix(i,j,k)+cfcmh2*(tem1(i,j,k)+tem2(i,j,k))
        END DO
      END DO
    END DO

    DO k=2,nz-2
      DO i=2,nx-1
        umix(i,  1 ,k)=umix(i,  1 ,k)+cfcmh2*(tem1(i,  1 ,k)            &
                +(-ustr(i,1,k)+ustr(i,2,k))*dyinv2)
        umix(i,ny-1,k)=umix(i,ny-1,k)+cfcmh2*(tem1(i,ny-1,k)            &
                +(-ustr(i,ny-1,k)+ustr(i,ny-2,k))*dyinv2)
      END DO
    END DO

  END IF
!
  IF( cfcmv2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the second order
!  vertical computational mixing on u'
!
!-----------------------------------------------------------------------
!
    CALL difzz0(ustr, nx,ny,nz, 2,nx-1,1,ny-1,2,nz-2, dz, tem1)

    DO k=2,nz-2
      DO j=1,ny-1
        DO i=2,nx-1
          umix(i,j,k)=umix(i,j,k)+cfcmv2*tem1(i,j,k)
        END DO
      END DO
    END DO

  END IF

  IF( cfcmh2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the horizontal
!  computational mixing on v'
!
!-----------------------------------------------------------------------
!
    CALL difxx0(vstr, nx,ny,nz, 2,nx-2,2,ny-1,2,nz-2, dx, tem1)

    CALL difyy0(vstr, nx,ny,nz, 1,nx-1,2,ny-1,2,nz-2, dy, tem2)

    DO k=2,nz-2
      DO j=2,ny-1
        DO i=2,nx-2
          vmix(i,j,k)=vmix(i,j,k)+cfcmh2*(tem1(i,j,k)+tem2(i,j,k))
        END DO
      END DO
    END DO

    DO k=2,nz-2
      DO j=2,ny-1
        vmix(1,j,k)=vmix(1,j,k)+cfcmh2*(tem2(1,j,k)                     &
                +(-vstr(1,j,k)+vstr(2,j,k))*dxinv2)
        vmix(nx-1,j,k)=vmix(nx-1,j,k)+cfcmh2*(tem2(nx-1,j,k)            &
                +(-vstr(nx-1,j,k)+vstr(nx-2,j,k))*dxinv2)
      END DO
    END DO

  END IF

  IF( cfcmv2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the vertical
!  computational mixing on v'
!
!-----------------------------------------------------------------------

    CALL difzz0(vstr, nx,ny,nz, 1,nx-1,2,ny-1,2,nz-2, dz, tem1)

    DO k=2,nz-2
      DO j=2,ny-1
        DO i=1,nx-1
          vmix(i,j,k)=vmix(i,j,k)+cfcmv2*tem1(i,j,k)
        END DO
      END DO
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Second order computational mixing on w
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL avgz0(rhostr, onvf,                                               &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem1)

  DO j=1,ny-1
    DO i=1,nx-1
      tem1(i,j,1) = tem1(i,j,3)
      tem1(i,j,nz)= tem1(i,j,nz-2)
    END DO
  END DO

  IF( cfcmh2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the horizontal
!  computational mixing on w
!
!-----------------------------------------------------------------------
!
    CALL difxx0(wstr, nx,ny,nz, 2,nx-2,1,ny-1,2,nz-1, dx, tem1)

    CALL difyy0(wstr, nx,ny,nz, 1,nx-1,2,ny-2,2,nz-1, dy, tem2)

    DO k=2,nz-1
      DO j=2,ny-2
        DO i=2,nx-2
          wmix(i,j,k)=wmix(i,j,k)+cfcmh2*(tem1(i,j,k)+tem2(i,j,k))
        END DO
      END DO
    END DO

    DO k=2,nz-1
      DO j=2,ny-2
        wmix(1,j,k)=wmix(1,j,k)+cfcmh2*(tem2(1,j,k)                     &
                +(-wstr(1,j,k)+wstr(2,j,k))*dxinv2)
        wmix(nx-1,j,k)=wmix(nx-1,j,k)+cfcmh2*(tem2(nx-1,j,k)            &
                +(-wstr(nx-1,j,k)+wstr(nx-2,j,k))*dxinv2)
      END DO
    END DO

    DO k=2,nz-1
      DO i=2,nx-2
        wmix(i, 1  ,k)=wmix(i, 1  ,k)+cfcmh2*(tem1(i,  1 ,k)            &
                +(-wstr(i,1,k)+wstr(i,2,k))*dyinv2)
        wmix(i,ny-1,k)=wmix(i,ny-1,k)+cfcmh2*(tem1(i,ny-1,k)            &
                +(-wstr(i,ny-1,k)+wstr(i,ny-2,k))*dyinv2)
      END DO
    END DO

    DO k=2,nz-1
      wmix(1, 1  ,k)=wmix(1, 1  ,k)                                     &
              +cfcmh2*((-wstr(1,1,k)+wstr(2,1,k))*dxinv2                &
              +(-wstr(1,1,k)+wstr(1,2,k))*dyinv2)
      wmix(1,ny-1,k)=wmix(1,ny-1,k)                                     &
              +cfcmh2*((-wstr(1,ny-1,k)                                 &
              +wstr(2,ny-1,k))*dxinv2                                   &
              +(-wstr(1,ny-1,k)+wstr(1,ny-2,k))*dyinv2)
      wmix(nx-1,1,k)=wmix(nx-1,1,k)                                     &
              +cfcmh2*((-wstr(nx-1,1,k)                                 &
              +wstr(nx-2,1,k))*dxinv2                                   &
              +(-wstr(nx-1,1,k)+wstr(nx-1,2,k))*dyinv2)
      wmix(nx-1,ny-1,k)=wmix(nx-1,ny-1,k)                               &
              +cfcmh2*((-wstr(nx-1,ny-1,k)                              &
              +wstr(nx-2,ny-1,k))*dxinv2                                &
              +(-wstr(nx-1,ny-1,k)+wstr(nx-1,ny-2,k))*dyinv2)
    END DO

  END IF
!
  IF( cfcmv2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the vertical
!  computational mixing on w
!
!-----------------------------------------------------------------------
!
    CALL difzz0(wstr, nx,ny,nz, 1,nx-1,1,ny-1,2,nz-1, dz, tem1)

    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          wmix(i,j,k)=wmix(i,j,k)+cfcmv2*tem1(i,j,k)
        END DO
      END DO
    END DO

  END IF


  RETURN
END SUBROUTINE cmix2uvw0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE CMIX4UVW0                  ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE cmix4uvw0(nx,ny,nz,                                           &
           u,v,w, ubar,vbar,rhostr,                                     &
           umix,vmix,wmix,                                              &
           ustr,vstr,wstr,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the fourth order computational mixing terms for the momentum
!  equations. These terms are placed in the arrays umix, vmix and wmix
!  and are applied to momentum perturbations only.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/3/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  4/2/92 (M. Xue and H. Jin)
!  modify for using terrain
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        Verticle component of Cartesian velocity at a
!             given time level (m/s)
!
!    ubar     Base state zonal velocity component (m/s)
!    vbar     Base state meridional velocity component (m/s)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!
!    umix     Array containing turbulent mixing on u (kg/(m*s)**2)
!    vmix     Array containing turbulent mixing on v (kg/(m*s)**2)
!    wmix     Array containing turbulent mixing on w (kg/(m*s)**2)
!
!  OUTPUT:
!
!    umix     Turbulent and computational mixing on u (kg/(m*s)**2)
!    vmix     Turbulent and computational mixing on v (kg/(m*s)**2)
!    wmix     Turbulent and computational mixing on w (kg/(m*s)**2)
!
!  WORK ARRAYS:
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.

  REAL :: umix  (nx,ny,nz)     ! Total mixing in u-eq. (kg/(m*s)**2)
  REAL :: vmix  (nx,ny,nz)     ! Total mixing in v-eq. (kg/(m*s)**2)
  REAL :: wmix  (nx,ny,nz)     ! Total mixing in w-eq. (kg/(m*s)**2)

  REAL :: ustr  (nx,ny,nz)     ! u * rhostr
  REAL :: vstr  (nx,ny,nz)     ! v * rhostr
  REAL :: wstr  (nx,ny,nz)     ! w * rhostr

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: onvf
  INTEGER :: i, j, k
  REAL :: dxinv4, dyinv4
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!

  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'
  INCLUDE 'bndry.inc'
!
!-----------------------------------------------------------------------

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  If the coefficients of computational mixing in both the horizontal
!  and vertical are zero, exit this subroutine.
!
!-----------------------------------------------------------------------
!
  IF( cfcmh4 == 0.0 .AND. cfcmv4 == 0.0 ) RETURN

  dxinv4 = dxinv**4
  dyinv4 = dyinv**4
!
!-----------------------------------------------------------------------
!
!  Calculate ustr=rhostr*u', vstr=rhostr*v', wstr=rhostr*w'
!
!-----------------------------------------------------------------------
!
  CALL rhouvw0(nx,ny,nz,rhostr,tem1,tem2,tem3)

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx
        ustr(i,j,k)=(u(i,j,k)-ubar(i,j,k))*tem1(i,j,k)
      END DO
    END DO
  END DO

  DO k=1,nz-1
    DO j=1,ny
      DO i=1,nx-1
        vstr(i,j,k)=(v(i,j,k)-vbar(i,j,k))*tem2(i,j,k)
      END DO
    END DO
  END DO

  DO k=1,nz
    DO j=1,ny-1
      DO i=1,nx-1
        wstr(i,j,k)=w(i,j,k)*tem3(i,j,k)
      END DO
    END DO
  END DO

  IF( cfcmh4 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  Fourth order computational mixing on u'
!  If the coefficient is not zero, calculate the horizontal
!  computational mixing on u'
!
!-----------------------------------------------------------------------
!
    CALL difxx0(ustr, nx,ny,nz, 2,nx-1,1,ny-1,2,nz-2, dx, tem2)

    CALL budifxx0(tem2, nx,ny,nz,1,ny-1,2,nz-2,ebc,wbc)

    CALL difxx0(tem2, nx,ny,nz, 2,nx-1,1,ny-1,2,nz-2, dx, tem3)

    CALL difyy0(ustr, nx,ny,nz, 2,nx-1,2,ny-2,2,nz-2, dy, tem2)

    CALL budifyy0(tem2, nx,ny,nz,2,nx-1,2,nz-2,nbc, sbc)

    CALL difyy0(tem2, nx,ny,nz, 2,nx-1,2,ny-2,2,nz-2, dy, tem1)


    DO k=2,nz-2
      DO j=2,ny-2
        DO i=2,nx-1
          umix(i,j,k)=umix(i,j,k)-cfcmh4*(tem3(i,j,k)+tem1(i,j,k))
        END DO
      END DO
    END DO

    DO k=2,nz-2
      DO i=2,nx-1
        umix(i,1,k)=umix(i,1,k)-cfcmh4*(tem3(i,1,k)                     &
            +(3.0*ustr(i,1,k)-4.0*ustr(i,2,k)+ustr(i,3,k))*dyinv4)
        umix(i,ny-1,k)=umix(i,ny-1,k)-cfcmh4*(tem3(i,ny-1,k)            &
            +(3.0*ustr(i,ny-1,k)-4.0*ustr(i,ny-2,k)                     &
            +ustr(i,ny-3,k))*dyinv4)
      END DO
    END DO

  END IF

  IF( cfcmv4 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the vertical
!  computational mixing on u'
!
!-----------------------------------------------------------------------
!
    CALL difzz0(ustr, nx,ny,nz, 2,nx-1,1,ny-1,2,nz-2, dz, tem2)

    CALL budifzz0(tem2, nx,ny,nz,2,nx-1,1,ny-1, tbc, bbc)

    CALL difzz0(tem2, nx,ny,nz, 2,nx-1,1,ny-1,2,nz-2, dz, tem3)

    DO k=2,nz-2
      DO j=1,ny-1
        DO i=2,nx-1
          umix(i,j,k)=umix(i,j,k)-cfcmv4*tem3(i,j,k)
        END DO
      END DO
    END DO

  END IF

  IF( cfcmh4 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the horizontal
!  computational mixing on v'
!
!-----------------------------------------------------------------------
!
    CALL difxx0(vstr, nx,ny,nz, 2,nx-2,2,ny-1,2,nz-2, dx, tem2)

    CALL bvdifxx0(tem2, nx,ny,nz,2,ny-1,2,nz-2,ebc, wbc)

    CALL difxx0(tem2, nx,ny,nz, 2,nx-2,2,ny-1,2,nz-2, dx, tem3)

    CALL difyy0(vstr, nx,ny,nz, 1,nx-1,2,ny-1,2,nz-2, dy, tem2)

    CALL bvdifyy0(tem2, nx,ny,nz,1,nx-1,2,nz-2,nbc, sbc)

    CALL difyy0(tem2, nx,ny,nz, 1,nx-1,2,ny-1,2,nz-2, dy, tem1)


    DO k=2,nz-2
      DO j=2,ny-1
        DO i=2,nx-2
          vmix(i,j,k)=vmix(i,j,k)-cfcmh4*(tem3(i,j,k)+tem1(i,j,k))
        END DO
      END DO
    END DO

    DO k=2,nz-2
      DO j=2,ny-1
        vmix(1,j,k)=vmix(1,j,k)-cfcmh4*(tem1(1,j,k)                     &
            +(3.0*vstr(1,j,k)-4.0*vstr(2,j,k)+vstr(3,j,k))*dxinv4)
        vmix(nx-1,j,k)=vmix(nx-1,j,k)-cfcmh4*(tem1(nx-1,j,k)            &
            +(3.0*vstr(nx-1,j,k)-4.0*vstr(nx-2,j,k)                     &
            +vstr(nx-3,j,k))*dxinv4)
      END DO
    END DO

  END IF
!
  IF( cfcmv4 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the vertical
!  computational mixing on v'
!
!-----------------------------------------------------------------------
!
    CALL difzz0(vstr, nx,ny,nz, 1,nx-1,2,ny-1,2,nz-2, dz, tem2)

    CALL bvdifzz0(tem2, nx,ny,nz,1,nx-1,2,ny-1,tbc, bbc)

    CALL difzz0(tem2, nx,ny,nz, 1,nx-1,2,ny-1,2,nz-2, dz, tem3)

    DO k=2,nz-2
      DO j=2,ny-1
        DO i=1,nx-1
          vmix(i,j,k)=vmix(i,j,k)-cfcmv4*tem3(i,j,k)
        END DO
      END DO
    END DO

  END IF

  IF( cfcmh4 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the horizontal
!  computational mixing on w
!
!-----------------------------------------------------------------------
!
    CALL difxx0(wstr, nx,ny,nz, 2,nx-2,1,ny-1,2,nz-1, dx, tem2)

    CALL bwdifxx0(tem2, nx,ny,nz,1,ny-1,2,nz-1,ebc, wbc)

    CALL difxx0(tem2, nx,ny,nz, 2,nx-2,1,ny-1,2,nz-1, dx, tem3)

    CALL difyy0(wstr, nx,ny,nz, 1,nx-1,2,ny-2,2,nz-1, dy, tem2)

    CALL bwdifyy0(tem2, nx,ny,nz,1,nx-1,2,nz-1,nbc, sbc)

    CALL difyy0(tem2, nx,ny,nz, 1,nx-1,2,ny-2,2,nz-1, dy, tem1)

    DO k=2,nz-1
      DO j=2,ny-2
        DO i=2,nx-2
          wmix(i,j,k)=wmix(i,j,k)-cfcmh4*(tem3(i,j,k)+tem1(i,j,k))
        END DO
      END DO
    END DO

    DO k=2,nz-1
      DO j=2,ny-2
        wmix(1,j,k)=wmix(1,j,k)-cfcmh4*(tem1(1,j,k)                     &
            +(3.0*wstr(1,j,k)-4.0*wstr(2,j,k)+wstr(3,j,k))*dxinv4)
        wmix(nx-1,j,k)=wmix(nx-1,j,k)-cfcmh4*(tem1(nx-1,j,k)            &
            +(3.0*wstr(nx-1,j,k)-4.0*wstr(nx-2,j,k)                     &
            +wstr(nx-3,j,k))*dxinv4)
      END DO
    END DO

    DO k=2,nz-1
      DO i=2,nx-2
        wmix(i,1,k)=wmix(i,1,k)-cfcmh4*(tem3(i,  1 ,k)                  &
            +(3.0*wstr(i,1,k)-4.0*wstr(i,2,k)+wstr(i,3,k))*dyinv4)
        wmix(i,ny-1,k)=wmix(i,ny-1,k)-cfcmh4*(tem3(i,ny-1,k)            &
            +(3.0*wstr(i,ny-1,k)-4.0*wstr(i,ny-2,k)                     &
            +wstr(i,ny-3,k))*dyinv4)
      END DO
    END DO

    DO k=2,nz-1
      wmix(1,1,k)=wmix(1,1,k)-cfcmh4*(                                  &
          (3.0*wstr(1,1,k)-4.0*wstr(2,1,k)+wstr(3,1,k))*dxinv4          &
          +(3.0*wstr(1,1,k)-4.0*wstr(1,2,k)+wstr(1,3,k))*dyinv4)
      wmix(1,ny-1,k)=wmix(1,ny-1,k)-cfcmh4*(                            &
          (3.0*wstr(1,ny-1,k)-4.0*wstr(2,ny-1,k)                        &
          +wstr(3,ny-1,k))*dxinv4                                       &
          +(3.0*wstr(1,ny-1,k)-4.0*wstr(1,ny-2,k)                       &
          +wstr(1,ny-3,k))*dyinv4)
      wmix(nx-1,1,k)=wmix(nx-1,1,k)-cfcmh4*(                            &
          (3.0*wstr(nx-1,1,k)-4.0*wstr(nx-1,2,k)                        &
          +wstr(nx-1,3,k))*dyinv4                                       &
          +(3.0*wstr(nx-1,1,k)-4.0*wstr(nx-2,1,k)                       &
          +wstr(nx-3,1,k))*dxinv4)
      wmix(nx-1,ny-1,k)=wmix(nx-1,ny-1,k)-cfcmh4*(                      &
          (3.0*wstr(nx-1,ny-1,k)-4.0*wstr(nx-1,ny-2,k)                  &
          +wstr(nx-1,ny-3,k))*dyinv4                                    &
          +(3.0*wstr(nx-1,ny-1,k)-4.0*wstr(nx-2,ny-1,k)                 &
          +wstr(nx-3,ny-1,k))*dxinv4)
    END DO

  END IF

  IF( cfcmv4 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the vertical
!  computational mixing on w
!
!-----------------------------------------------------------------------
!
    CALL difzz0(wstr, nx,ny,nz, 1,nx-1,1,ny-1,2,nz-1, dz, tem2)

    CALL bwdifzz0(tem2, nx,ny,nz,1,nx-1,1,ny-1,tbc, bbc)

    CALL difzz0(tem2, nx,ny,nz, 1,nx-1,1,ny-1,2,nz-1, dz, tem3)

    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          wmix(i,j,k)=wmix(i,j,k)-cfcmv4*tem3(i,j,k)
        END DO
      END DO
    END DO

  END IF

  RETURN
END SUBROUTINE cmix4uvw0
