!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Compute the thermowind constraint term, defined as
!
!  first: du/dz +(g/f/anx(5))*d(anx(5)/dy -u/anx(5)*d(anx(5))/dz
!  first: dv/dz -(g/f/anx(5))*d(anx(5)/dx -v/anx(5)*d(anx(5))/dz
!
!-----------------------------------------------------------------------
! MODIFICATION HISTORY:
!
! 10/28/2016 (Y. Wang)
!   Need more work to consider U,V,W staggering
!
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
MODULE constraint_thermo

  USE model_precision

  IMPLICIT NONE

  !INCLUDE 'mp.inc'

!-----------------------------------------------------------------------
!
! Control options
!
!-----------------------------------------------------------------------

  PRIVATE
  SAVE

  INTEGER  :: thermo_opt
  REAL(SP), ALLOCATABLE :: wgt_thermo(:)

  REAL(SP) :: thermo_wgt

!-----------------------------------------------------------------------
!
! Work arrays
!
!-----------------------------------------------------------------------

  LOGICAL :: wrk_allocated

  REAL(P), ALLOCATABLE, DIMENSION(:,:,:) :: ffu, ffv

  REAL(P), ALLOCATABLE, DIMENSION(:,:,:) :: u, v, t
  REAL(P), ALLOCATABLE, DIMENSION(:,:,:) :: ut,vt,tt
  REAL(P), ALLOCATABLE, DIMENSION(:,:,:) :: tem1, sinlat

  REAL(P), ALLOCATABLE, DIMENSION(:)     :: temx, temy
  REAL(P), ALLOCATABLE, DIMENSION(:,:)   :: temxy, sinlat2d


!-----------------------------------------------------------------------
!
! Public interface
!
!-----------------------------------------------------------------------

  PUBLIC :: thermo_opt

  PUBLIC :: consthermo_read_nml_options, consthermo_checkflags
  PUBLIC :: consthermo_allocate_wrk, consthermo_deallocate_wrk
  PUBLIC :: consthermo_costf, consthermo_gradt

  CONTAINS

  SUBROUTINE consthermo_read_nml_options(unum,npass,myproc,istatus)

  !#####################################################################

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: unum, myproc
    INTEGER, INTENT(IN)  :: npass
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    NAMELIST /var_thermo/ thermo_opt, wgt_thermo

  !---------------------------------------------------------------------

    INTEGER :: i

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !IF (npass > maxpass) THEN
    !  WRITE(*,'(1x,a,2(I0,a),/,7x,a)') 'ERROR: Request npass (',npass,  &
    !          ')is larger then this module maxpass (',maxpass,') in constraint_thermo.', &
    !          'Program will abort.'
    !  istatus = -1
    !  RETURN
    !END IF

    ALLOCATE(wgt_thermo(npass), STAT = istatus)
    CALL check_alloc_status(istatus, "constraint_thermo:wgt_thermo")

    thermo_opt = 0
    wgt_thermo = 0.0

    IF (myproc == 0) THEN
      READ(unum, var_thermo,  END=350)
      WRITE(*,*) 'Namelist block var_thermo sucessfully read.'

      GOTO 400

      350 CONTINUE  ! Error with namelist reading
      WRITE(*,*) 'ERROR: reading namelist block var_thermo.'
      istatus = -2
      RETURN

      400 CONTINUE  ! Successful read namelist

      WRITE(*,'(5x,a,I0,a)') 'thermo_opt = ', thermo_opt,','
      IF ( thermo_opt > 0 ) THEN
         WRITE(*,*) 'User-specified THERMO weights:'
         WRITE(*,*) (wgt_thermo(i), i =1 , npass )
      END IF

    END IF

    CALL mpupdatei(thermo_opt,   1)
    CALL mpupdater(wgt_thermo,   npass)

    wrk_allocated = .FALSE.

    RETURN
  END SUBROUTINE consthermo_read_nml_options

  !#####################################################################

  SUBROUTINE consthermo_checkflags(ipass,nx,ny,nz,x,y,dx,dy,ctrlat,mapfct, &
                                   coriopt,eradius,omega,anxu,anxv,istatus)
    IMPLICIT NONE
    INTEGER, INTENT(IN)  :: ipass, coriopt
    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL(P), INTENT(IN)  :: eradius,omega
    REAL(P), INTENT(IN)  :: dx,dy,ctrlat
    REAL(P), INTENT(IN)  :: mapfct(nx,ny,8)   ! Map factors at scalar, u and v points
    REAL(P), INTENT(IN)  :: x(nx), y(ny)
    REAL(P), INTENT(IN)  :: anxu(nx,ny,nz), anxv(nx,ny,nz)
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i, j, k
    REAL(P) :: omega2, sinclat

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    thermo_wgt = wgt_thermo(ipass)

    IF (thermo_opt > 0) THEN

      IF (.NOT. wrk_allocated) CALL consthermo_allocate_wrk(nx,ny,nz,istatus)

    !------------------------calculate coriolis costant-----------------

      CALL gtsinlat(nx,ny,x,y, sinlat2d, temx,temy, temxy)

      !
      ! Added the effects of spatial gradient of map factor on the coriolis force
      !
      IF( coriopt == 3 .OR. coriopt == 4) THEN
        DO j = 1,ny
          DO i = 1,nx
            temxy(i,j) = sinlat2d(i,j)/SQRT(1-sinlat2d(i,j)**2)  ! tan(lat)
          END DO
        END DO

        DO k = 1,nz
          DO j = 1,ny-1
            DO i = 1,nx-1
              !
              ! fm = U*My - V*Mx + U*TAN(lat)/eradius
              !
              tem1(i,j,k) = 0.5*( (anxu(i,j,k)+anxu(i+1,j,k))*                &
                                  ((mapfct(i,j+1,3)-mapfct(i,j,3))/dy)        &
                                 -(anxv(i,j,k)+anxv(i,j+1,k))*                &
                                  ((mapfct(i+1,j,2)-mapfct(i,j,2))/dx)        &
                                 +((anxu(i,j,k)+anxu(i+1,j,k))*temxy(i,j))/    &
                                  eradius)
            END DO
          END DO
        END DO

      END IF

      omega2 = 2.0* omega
      IF( coriopt == 1 ) THEN
        sinclat = SIN( ATAN(1.0)/45.0 * ctrlat )
        DO k = 1,nz
          DO j=1,ny
            DO i=1,nx
              sinlat(i,j,k) = omega2* sinclat
            END DO
          END DO
        END DO
      ELSE IF( coriopt == 2 ) THEN
        sinclat = SIN( ATAN(1.0)/45.0 * ctrlat )
        DO k = 1,nz
          DO j=1,ny
            DO i=1,nx
              sinlat(i,j,k) = omega2* sinclat
            END DO
          END DO
        END DO

      ELSE IF( coriopt == 3 ) THEN

        DO k = 1,nz
          DO j=1,ny-1
            DO i=1,nx-1
              sinlat(i,j,k) = omega2*sinlat2d(i,j) +tem1(i,j,k)
            END DO
          END DO
        END DO

      ELSE IF( coriopt == 4 ) THEN

        DO k = 1,nz
          DO j=1,ny-1
            DO i=1,nx-1
              sinlat(i,j,k) = omega2*sinlat2d(i,j) +tem1(i,j,k)
            END DO
          END DO
        END DO

      END IF

    END IF

    RETURN
  END SUBROUTINE consthermo_checkflags

  SUBROUTINE consthermo_costf(mp_opt,nx,ny,nz,ibgn,iend,jbgn,jend,      &
                             p0,rddcp,g,dxinv,dyinv,dzinv,j3inv,        &
                             u_ctr,v_ctr,p_ctr,t_ctr,                   &
                             f_thermo,istatus)

  !#####################################################################

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: mp_opt
    INTEGER, INTENT(IN)  :: nx,ny,nz, ibgn,iend,jbgn,jend

    REAL(P), INTENT(IN)  :: p0, rddcp, g
    REAL(P), INTENT(IN)  :: dxinv,dyinv,dzinv

    REAL(P), INTENT(IN)  ::   j3inv(nx,ny,nz)

    REAL(P), INTENT(IN)  ::   p_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  ::   t_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  ::   u_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  ::   v_ctr(nx,ny,nz)

    REAL(DP), INTENT(OUT) :: f_thermo

    INTEGER,  INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i, j, k

    !REAL(DP) :: f_thermo

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (.NOT. wrk_allocated) CALL consthermo_allocate_wrk(nx,ny,nz,istatus)

    f_thermo = 0.0

    !
    ! u, v, tem used as temporary arrays on scalar grid, where tem is used for temperature here
    !
    DO k= 1,nz-1
      DO j= 1,ny-1
        DO i= 1,nx-1
          u(i,j,k)   =    u_ctr(i,j,k)
          v(i,j,k)   =    v_ctr(i,j,k)
          t(i,j,k)   = (  t_ctr(i,j,k) ) * (( p_ctr(i,j,k) )/p0 )**rddcp
        END DO
      END DO
    END DO

    ! Added by WYH for application in MPI mode
    DO j = 1,ny-1          ! Set top boundary (W points) since (k+1) is used below
      DO i = 1,nx-1        ! Assume zero normal gradient condition.
        u(i,j,nz)   = u(i,j,nz-1)
        v(i,j,nz)   = v(i,j,nz-1)
        t(i,j,nz)   = t(i,j,nz-1)
      END DO
    END DO

    DO k = 1,nz            ! Set east boundary (U points) since (i+1) is used below
      DO j = 1,ny-1        ! Assume zero normal gradient condition.
        t(nx,j,k) = t(nx-2,j,k)
      END DO
    END DO

    DO k = 1,nz            ! Set north boundary (V points) since (j+1) is used below
      DO i = 1,nx          ! Assume zero normal gradient condition.
        t(i,ny,k) = t(i,ny-2,k)
     END DO
   END DO
   ! End of addition from WYH

   !IF (mp_opt > 0) THEN
   !  !CALL acct_interrupt(mp_acct)
   !  CALL mpsendrecv2dew(u, nx, ny, nz, 0, 0, 0, tem1)
   !  CALL mpsendrecv2dns(u, nx, ny, nz, 0, 0, 0, tem1)
   !
   !  CALL mpsendrecv2dew(v, nx, ny, nz, 0, 0, 0, tem1)
   !  CALL mpsendrecv2dns(v, nx, ny, nz, 0, 0, 0, tem1)
   !
   !  CALL mpsendrecv2dew(t, nx, ny, nz, 0, 0, 0, tem1)
   !  CALL mpsendrecv2dns(t, nx, ny, nz, 0, 0, 0, tem1)
   !  !CALL acct_stop_inter
   !END IF

   DO k=1,nz-1
     DO j=1,ny-1
       DO i=1,nx-1
         ! Note that t is temperature here
         ffu(i,j,k) = (u(i,j,k+1)-u(i,j,k))*dzinv*j3inv(i,j,k)              &
                     +g/sinlat(i,j,k)/t(i,j,k)*(t(i,j+1,k)-t(i,j,k))*dyinv  &
                     -u(i,j,k)/t(i,j,k)*(t(i,j,k+1)-t(i,j,k))*dzinv*j3inv(i,j,k)

         ffv(i,j,k) = (v(i,j,k+1)-v(i,j,k))*dzinv*j3inv(i,j,k)              &
                     -g/sinlat(i,j,k)/t(i,j,k)*(t(i+1,j,k)-t(i,j,k))*dxinv  &
                     -v(i,j,k)/t(i,j,k)*(t(i,j,k+1)-t(i,j,k))*dzinv*j3inv(i,j,k)
       END DO
     END DO
   END DO

   IF (mp_opt > 0) THEN
     !CALL acct_interrupt(mp_acct)
     !CALL mpsendrecv2dew(ffu, nx, ny, nz, ebc, wbc, 0, tem1)
     CALL mpsendrecv2dns(ffu, nx, ny, nz, 0, 0, 0, tem1)
     CALL mpsendrecv2dew(ffv, nx, ny, nz, 0, 0, 0, tem1)
     !CALL mpsendrecv2dns(ffv, nx, ny, nz, nbc, sbc, 0, tem1)
     !CALL acct_stop_inter
   END IF

   DO k = 1,nz-1
     DO j = jbgn,jend
       DO i = ibgn,iend
         f_thermo = f_thermo + thermo_wgt*                              &
                    (ffu(i,j,k)*ffu(i,j,k)+ffv(i,j,k)*ffv(i,j,k))
       END DO
     END DO
   END DO

   !cfun_single = cfun_single + f_thermo

    RETURN
  END SUBROUTINE consthermo_costf

  SUBROUTINE consthermo_gradt(mp_opt,nx,ny,nz,p0,rddcp,g,               &
                             dxinv,dyinv,dzinv,j3inv,                   &
                             u_ctr,v_ctr,p_ctr,t_ctr,                   &
                             anxu,anxv,anxp,anxpt,                      &
                             u_grd,v_grd,p_grd,t_grd,istatus)

  !#####################################################################

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: mp_opt
    INTEGER, INTENT(IN)  :: nx,ny,nz

    REAL(P), INTENT(IN)  :: p0, rddcp,g
    REAL(P), INTENT(IN)  :: dxinv,dyinv,dzinv

    REAL(P), INTENT(IN)  :: j3inv(nx,ny,nz)

    REAL(P), INTENT(IN)  :: u_ctr(nx,ny,nz),v_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  :: p_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  :: t_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  :: anxu(nx,ny,nz)
    REAL(P), INTENT(IN)  :: anxv(nx,ny,nz)
    REAL(P), INTENT(IN)  :: anxp(nx,ny,nz)
    REAL(P), INTENT(IN)  :: anxpt(nx,ny,nz)

    REAL(P), INTENT(INOUT) :: u_grd(nx,ny,nz)
    REAL(P), INTENT(INOUT) :: v_grd(nx,ny,nz)
    REAL(P), INTENT(INOUT) :: p_grd(nx,ny,nz)
    REAL(P), INTENT(INOUT) :: t_grd(nx,ny,nz)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j,k

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ut = 0.0
    vt = 0.0
    tt = 0.0

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          ut(i,j,k) = anxu(i,j,k)+u_ctr(i,j,k)
          vt(i,j,k) = anxv(i,j,k)+v_ctr(i,j,k)
          ! t used for temperature here temporarily
          tt(i,j,k) = (  anxpt(i,j,k)+t_ctr(i,j,k) )                    &
                     *(( anxp(i,j,k)+p_ctr(i,j,k) )/p0 )**rddcp
          ffu(i,j,k)=thermo_wgt*ffu(i,j,k)
          ffv(i,j,k)=thermo_wgt*ffv(i,j,k)
        END DO
      END DO
    END DO

    u(:,:,:) = 0.0
    v(:,:,:) = 0.0
    t(:,:,:) = 0.0

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          u(i,j,k+1)=u(i,j,k+1) +ffu(i,j,k)*dzinv*j3inv(i,j,k)
          u(i,j,k  )=u(i,j,k  ) -ffu(i,j,k)*dzinv*j3inv(i,j,k)

          t(i,j,k  )=t(i,j,k  ) -g/sinlat(i,j,k)*ffu(i,j,k)        &
                            /tt(i,j,k)**2*(tt(i,j+1,k)-tt(i,j,k))*dyinv
          t(i,j+1,k)=t(i,j+1,k) +g/sinlat(i,j,k)/tt(i,j,k)         &
                                      *ffu(i,j,k)*dyinv
          t(i,j  ,k)=t(i,j  ,k) -g/sinlat(i,j,k)/tt(i,j,k)         &
                                      *ffu(i,j,k)*dyinv

          u(i,j,k  )=u(i,j,k  ) -ffu(i,j,k)/tt(i,j,k)              &
                            *(tt(i,j,k+1)-tt(i,j,k))*dzinv*j3inv(i,j,k)
          t(i,j,k  )=t(i,j,k  ) +ut(i,j,k)*ffu(i,j,k)/tt(i,j,k)**2 &
                            *(tt(i,j,k+1)-tt(i,j,k))*dzinv*j3inv(i,j,k)
          t(i,j,k+1)=t(i,j,k+1) -ut(i,j,k)/tt(i,j,k)*ffu(i,j,k)    &
                            *dzinv*j3inv(i,j,k)
          t(i,j,k  )=t(i,j,k  ) +ut(i,j,k)/tt(i,j,k)*ffu(i,j,k)    &
                            *dzinv*j3inv(i,j,k)
          ffu(i,j,k) = 0.0

          v(i,j,k+1)=v(i,j,k+1) +ffv(i,j,k)*dzinv*j3inv(i,j,k)
          v(i,j,k  )=v(i,j,k  ) -ffv(i,j,k)*dzinv*j3inv(i,j,k)

          t(i,j,k  )=t(i,j,k  ) +g/sinlat(i,j,k)*ffv(i,j,k)        &
                            /tt(i,j,k)**2*(tt(i+1,j,k)-tt(i,j,k))*dxinv
          t(i+1,j,k)=t(i+1,j,k) -g/sinlat(i,j,k)/tt(i,j,k)         &
                                      *ffv(i,j,k)*dxinv
          t(i,j  ,k)=t(i,j  ,k) +g/sinlat(i,j,k)/tt(i,j,k)          &
                                      *ffv(i,j,k)*dxinv

          v(i,j,k  )=v(i,j,k  ) -ffv(i,j,k)/tt(i,j,k)              &
                            *(tt(i,j,k+1)-tt(i,j,k))*dzinv*j3inv(i,j,k)
          t(i,j,k  )=t(i,j,k  ) +vt(i,j,k)*ffv(i,j,k)/tt(i,j,k)**2 &
                            *(tt(i,j,k+1)-tt(i,j,k))*dzinv*j3inv(i,j,k)
          t(i,j,k+1)=t(i,j,k+1) -vt(i,j,k)/tt(i,j,k)*ffv(i,j,k)    &
                            *dzinv*j3inv(i,j,k)
          t(i,j,k  )=t(i,j,k  ) +vt(i,j,k)/tt(i,j,k)*ffv(i,j,k)    &
                            *dzinv*j3inv(i,j,k)
          ffv(i,j,k) = 0.0
        END DO
      END DO
    END DO

    IF (mp_opt > 0) THEN
      !CALL acct_interrupt(mp_acct)
      CALL mpsendrecv2dew(u, nx, ny, nz, 0, 0, 0, tem1)
      CALL mpsendrecv2dns(u, nx, ny, nz, 0, 0, 0, tem1)

      CALL mpsendrecv2dew(v, nx, ny, nz, 0, 0, 0, tem1)
      CALL mpsendrecv2dns(v, nx, ny, nz, 0, 0, 0, tem1)

      CALL mpsendrecv2dew(t, nx, ny, nz, 0, 0, 0, tem1)
      CALL mpsendrecv2dns(t, nx, ny, nz, 0, 0, 0, tem1)
      !CALL acct_stop_inter
    END IF

    DO k= 1,nz-1
      DO j= 1,ny-1
        DO i= 1,nx-1
          u_grd(i,j,k) = u_grd(i,j,k) + u(i,j,k)
          v_grd(i,j,k) = v_grd(i,j,k) + v(i,j,k)
          t_grd(i,j,k) = t_grd(i,j,k) + t(i,j,k)                        &
                          *(( anxp(i,j,k)+p_ctr(i,j,k) )/p0 )**rddcp
          p_grd(i,j,k) = p_grd(i,j,k) + t(i,j,k)                        &
                          *rddcp*( anxpt(i,j,k)+t_ctr(i,j,k) )/p0       &
                          *(( anxp(i,j,k)+p_ctr(i,j,k) )/p0 )**(rddcp-1)
          u(i,j,k) = 0.0
          v(i,j,k) = 0.0
          t(i,j,k) = 0.0
        END DO
      END DO
    END DO

    RETURN
  END SUBROUTINE consthermo_gradt

  !#####################################################################

  SUBROUTINE consthermo_allocate_wrk(nx,ny,nz,istatus)
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nx,ny,nz
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (wrk_allocated) THEN
      WRITE(*,'(1x,a)') 'INFO: Should not be here. Maybe multiple calls of "consthermo_allocate_wrk".'
      istatus = 1
      RETURN
    END IF

    istatus = 0

    ALLOCATE(ffu(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: ffu")
    ALLOCATE(ffv(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: ffv")
    ffu = 0.0
    ffv = 0.0

    ALLOCATE(u(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: u")
    ALLOCATE(v(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: v")
    ALLOCATE(t(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: t")

    ALLOCATE(ut(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: ut")
    ALLOCATE(vt(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: vt")
    ALLOCATE(tt(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: tt")

    ALLOCATE(tem1(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: tem1")

    ALLOCATE(sinlat(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: sinlat")

    ALLOCATE(sinlat2d(nx,ny),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: sinlat2d")

    ALLOCATE(temx(nx),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: temx")

    ALLOCATE(temy(ny),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: temy")

    ALLOCATE(temxy(nx,ny),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: temxy")

    wrk_allocated = .TRUE.

    RETURN
  END SUBROUTINE consthermo_allocate_wrk

  !#####################################################################

  SUBROUTINE consthermo_deallocate_wrk(istatus)
    IMPLICIT NONE

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (wrk_allocated) THEN

      DEALLOCATE(sinlat,sinlat2d,  STAT = istatus )
      DEALLOCATE(temx,temy,temxy,  STAT = istatus )

      DEALLOCATE(ffu,ffv,  STAT = istatus )

      DEALLOCATE(u,v,t,    STAT = istatus )
      DEALLOCATE(ut,vt,tt, STAT = istatus )
      DEALLOCATE(tem1,     STAT = istatus )

      wrk_allocated = .FALSE.

    END IF

    RETURN
  END SUBROUTINE consthermo_deallocate_wrk

END MODULE constraint_thermo
