!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! currently smth_opt is not fully tested and not used
!
!-----------------------------------------------------------------------
! MODIFICATION HISTORY:
!
! 10/28/2016 (Y. Wang)
!   Need more work to consider U,V,W staggering
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MODULE constraint_smooth

  USE model_precision

  IMPLICIT NONE

  !INCLUDE 'mp.inc'

!-----------------------------------------------------------------------
!
! Control options
!
!-----------------------------------------------------------------------

  PRIVATE
  SAVE

  INTEGER  :: smth_opt
  REAL(P), ALLOCATABLE :: wgt_smth(:)

!-----------------------------------------------------------------------
!
! Work arrays
!
!-----------------------------------------------------------------------

  LOGICAL :: wrk_allocated

  REAL(P), ALLOCATABLE, DIMENSION(:,:,:) :: u, v, w
  REAL(P), ALLOCATABLE, DIMENSION(:,:,:) :: smcu,smcv,smcw
  REAL(P), ALLOCATABLE, DIMENSION(:,:,:) :: tem1

!-----------------------------------------------------------------------
!
! Public interface
!
!-----------------------------------------------------------------------

  PUBLIC :: smth_opt

  PUBLIC :: consmth_read_nml_options
  PUBLIC :: consmth_allocate_wrk, consmth_deallocate_wrk
  PUBLIC :: consmth_costf, consmth_gradt

  CONTAINS

  SUBROUTINE consmth_read_nml_options(unum,npass,myproc,istatus)

  !#####################################################################

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: unum, myproc
    INTEGER, INTENT(IN)  :: npass
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    NAMELIST /var_smth/ smth_opt, wgt_smth

  !---------------------------------------------------------------------

    INTEGER :: i

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !IF (npass > maxpass) THEN
    !  WRITE(*,'(1x,a,2(I0,a),/,7x,a)') 'ERROR: Request npass (',npass,  &
    !          ')is larger then this module maxpass (',maxpass,') in constraint_smooth.', &
    !          'Program will abort.'
    !  istatus = -1
    !  RETURN
    !END IF

    ALLOCATE(wgt_smth(npass), STAT = istatus)
    CALL check_alloc_status(istatus, "constraint_smooth:wgt_smth")

    smth_opt = 0
    wgt_smth = 0.0

    IF (myproc == 0) THEN
      READ(unum, var_smth,  END=350)
      WRITE(*,*) 'Namelist block var_smth sucessfully read.'

      GOTO 400

      350 CONTINUE  ! Error with namelist reading
      WRITE(*,*) 'ERROR: reading namelist block var_smth.'
      istatus = -2
      RETURN

      400 CONTINUE  ! Successful read namelist

      WRITE(*,'(5x,a,I0,a)') 'smth_opt = ', smth_opt,','
      IF ( smth_opt > 0 ) THEN
         WRITE(*,'(5x,a)') 'User-specified SMOOTH weights:'
         WRITE(*,'(5x,a,10F9.2)') (wgt_smth(i), i =1 , npass )
      END IF

    END IF

    CALL mpupdatei(smth_opt,   1)
    CALL mpupdater(wgt_smth,   npass)

    wrk_allocated = .FALSE.

    RETURN
  END SUBROUTINE consmth_read_nml_options

  !#####################################################################

  SUBROUTINE consmth_costf(ipass,nx,ny,nz,ibgn,iend,jbgn,jend,          &
                           u_ctr,v_ctr,w_ctr,                           &
                           f_smth_u, f_smth_v, f_smth_w,                &
                           istatus)

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: ipass
    INTEGER, INTENT(IN)  :: nx,ny,nz, ibgn,iend,jbgn,jend

    REAL(P), INTENT(IN)  ::   u_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  ::   v_ctr(nx,ny,nz)
    REAL(P), INTENT(IN)  ::   w_ctr(nx,ny,nz)

    REAL(DP), INTENT(OUT) :: f_smth_u, f_smth_v, f_smth_w

    INTEGER,  INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INCLUDE 'mp.inc'

    INTEGER :: i, j, k

    !REAL(DP) :: f_smth_u, f_smth_v, f_smth_w

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (.NOT. wrk_allocated) CALL consmth_allocate_wrk(nx,ny,nz,istatus)


    f_smth_u = 0.0
    f_smth_v = 0.0
    f_smth_w = 0.0

    ! Added by WYH to define and set boundary for u,v & w, otherwise,
    ! the definitions of u, v & w are ambiguous depending on values of thermo_opt, radsw, div_opt etc.
    DO k= 1,nz-1
      DO j= 1,ny-1
        DO i= 1,nx-1
          u(i,j,k) =  u_ctr(i,j,k)
          v(i,j,k) =  v_ctr(i,j,k)
          w(i,j,k) =  w_ctr(i,j,k)
        END DO
      END DO
    END DO

    DO j = 1,ny-1          ! Set top boundary (W points) since (k+1) is used below
      DO i = 1,nx-1        ! Assume zero normal gradient condition.
        u(i,j,nz) = u(i,j,nz-1)
        v(i,j,nz) = v(i,j,nz-1)
        w(i,j,nz) = w(i,j,nz-1)
      END DO
    END DO

    DO k = 1,nz            ! Set east boundary (U points) since (i+1) is used below
      DO j = 1,ny-1        ! Assume zero normal gradient condition.
        u(nx,j,k) = u(nx-2,j,k)
        v(nx,j,k) = v(nx-2,j,k)
        w(nx,j,k) = w(nx-2,j,k)
      END DO
    END DO

    DO k = 1,nz            ! Set north boundary (V points) since (j+1) is used below
      DO i = 1,nx          ! Assume zero normal gradient condition.
        u(i,ny,k) = u(i,ny-2,k)
        v(i,ny,k) = v(i,ny-2,k)
        w(i,ny,k) = w(i,ny-2,k)
      END DO
    END DO
    ! End of Adding from WYH

    DO k=2,nz-1
      DO j=2,ny-1
        DO i=2,nx-1
          smcu(i,j,k)= (u(i+1,j,k)+u(i-1,j,k)+u(i,j+1,k)+u(i,j-1,k)     &
                       +u(i,j,k+1)+u(i,j,k-1)-6.*u(i,j,k))
          smcv(i,j,k)= (v(i+1,j,k)+v(i-1,j,k)+v(i,j+1,k)+v(i,j-1,k)     &
                       +v(i,j,k+1)+v(i,j,k-1)-6.*v(i,j,k))
          smcw(i,j,k)= (w(i+1,j,k)+w(i-1,j,k)+w(i,j+1,k)+w(i,j-1,k)     &
                       +w(i,j,k+1)+w(i,j,k-1)-6.*w(i,j,k))
        END DO
      END DO
    END DO

    DO j=2,ny-1
      DO i=2,nx-1
        smcu(i,j,1)= (u(i+1,j,1)+u(i-1,j,1)+u(i,j+1,1)+u(i,j-1,1)       &
                     -4.*u(i,j,1))
        smcv(i,j,1)= (v(i+1,j,1)+v(i-1,j,1)+v(i,j+1,1)+v(i,j-1,1)       &
                     -4.*v(i,j,1))
        smcw(i,j,1)= (w(i,j,1)+w(i-1,j,1)+w(i,j+1,1)+w(i,j-1,1)         &
                     -4.*w(i,j,1))
      END DO
    END DO

    IF (mp_opt > 0) THEN
      !CALL acct_interrupt(mp_acct)
      CALL mpsendrecv2dew(smcu, nx, ny, nz, 0, 0, 0, tem1)
      CALL mpsendrecv2dns(smcu, nx, ny, nz, 0, 0, 0, tem1)

      CALL mpsendrecv2dew(smcv, nx, ny, nz, 0, 0, 0, tem1)
      CALL mpsendrecv2dns(smcv, nx, ny, nz, 0, 0, 0, tem1)

      CALL mpsendrecv2dew(smcw, nx, ny, nz, 0, 0, 0, tem1)
      CALL mpsendrecv2dns(smcw, nx, ny, nz, 0, 0, 0, tem1)
      !CALL acct_stop_inter
    END IF

    DO k=1,nz-1
      DO j=2,jend
        DO i=2,iend
          f_smth_u= f_smth_u + wgt_smth(ipass)*smcu(i,j,k)**2
          f_smth_v= f_smth_v + wgt_smth(ipass)*smcv(i,j,k)**2
          f_smth_w= f_smth_w + wgt_smth(ipass)*smcw(i,j,k)**2
        END DO
      END DO
    END DO

    !cfun_singleu = cfun_singleu + f_smth_u
    !cfun_singlev = cfun_singlev + f_smth_v
    !cfun_singlew = cfun_singlew + f_smth_w

    RETURN
  END SUBROUTINE consmth_costf

  SUBROUTINE consmth_gradt(ipass,nx,ny,nz,u_grd,v_grd,w_grd,istatus)

  !#####################################################################

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: ipass
    INTEGER, INTENT(IN)  :: nx,ny,nz

    REAL(P), INTENT(INOUT) :: u_grd(nx,ny,nz)
    REAL(P), INTENT(INOUT) :: v_grd(nx,ny,nz)
    REAL(P), INTENT(INOUT) :: w_grd(nx,ny,nz)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INCLUDE 'mp.inc'

    INTEGER :: i,j,k

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    tem1(:,:,:) = 0.0   !!!! used as temporary array !!!!

    DO i=nx-1,1,-1
      DO j=ny-1,1,-1
        DO k=nz-1,1,-1
          tem1(i,j,k)=smcu(i,j,k)*wgt_smth(ipass)
        END DO
      END DO
    END DO

    DO i=nx-1,2,-1
      DO j=ny-1,2,-1
        u_grd(i,j,1)   =u_grd(i,j,1)-tem1(i,j,1)*4.0
        u_grd(i,j+1,1) =u_grd(i,j+1,1) +tem1(i,j,1)
        u_grd(i,j-1,1) =u_grd(i,j-1,1) +tem1(i,j,1)
        u_grd(i+1,j,1) =u_grd(i+1,j,1) +tem1(i,j,1)
        u_grd(i-1,j,1) =u_grd(i-1,j,1) +tem1(i,j,1)
      END DO
    END DO

    DO i=nx-1,2,-1
      DO j=ny-1,2,-1
        DO k=nz-1,2,-1
          u_grd(i,j,k)=u_grd(i,j,k) -tem1(i,j,k)*6.0
          u_grd(i,j,k+1)=u_grd(i,j,k+1) +tem1(i,j,k)
          u_grd(i,j,k-1)=u_grd(i,j,k-1) +tem1(i,j,k)
          u_grd(i,j+1,k)=u_grd(i,j+1,k) +tem1(i,j,k)
          u_grd(i,j-1,k)=u_grd(i,j-1,k) +tem1(i,j,k)
          u_grd(i+1,j,k)=u_grd(i+1,j,k) +tem1(i,j,k)
          u_grd(i-1,j,k)=u_grd(i-1,j,k) +tem1(i,j,k)
        END DO
      END DO
    END DO

    IF (loc_x > 1) THEN  ! count column 1's affect on column 2
      DO j=ny-1,2,-1
        DO k=nz-1,1,-1
          u_grd(2,j,k)=u_grd(2,j,k) +tem1(1,j,k)
        END DO
      END DO
    END IF

    IF (loc_y > 1) THEN  ! Count row 1's affect on column 2
      DO i=nx-1,2,-1
        DO k=nz-1,1,-1
          u_grd(i,2,k)=u_grd(i,2,k) +tem1(i,1,k)
        END DO
      END DO
    END IF

    DO i=nx-1,2,-1
      DO j=ny-1,2,-1
        DO k=nz-1,1,-1
          tem1(i,j,k)=smcv(i,j,k)*wgt_smth(ipass)
        END DO
      END DO
    END DO

    DO i=nx-1,2,-1
      DO j=ny-1,2,-1
        v_grd(i,j,1)=v_grd(i,j,1)-tem1(i,j,1)*4.0
        v_grd(i,j+1,1)=v_grd(i,j+1,1)+tem1(i,j,1)
        v_grd(i,j-1,1)=v_grd(i,j-1,1)+tem1(i,j,1)
        v_grd(i+1,j,1)=v_grd(i+1,j,1)+tem1(i,j,1)
        v_grd(i-1,j,1)=v_grd(i-1,j,1)+tem1(i,j,1)
      END DO
    END DO

    DO i=nx-1,2,-1
      DO j=ny-1,2,-1
        DO k=nz-1,2,-1
          v_grd(i,j,k)=v_grd(i,j,k)-tem1(i,j,k)*6.0
          v_grd(i,j,k+1)=v_grd(i,j,k+1)+tem1(i,j,k)
          v_grd(i,j,k-1)=v_grd(i,j,k-1)+tem1(i,j,k)
          v_grd(i,j+1,k)=v_grd(i,j+1,k)+tem1(i,j,k)
          v_grd(i,j-1,k)=v_grd(i,j-1,k)+tem1(i,j,k)
          v_grd(i+1,j,k)=v_grd(i+1,j,k)+tem1(i,j,k)
          v_grd(i-1,j,k)=v_grd(i-1,j,k)+tem1(i,j,k)
        END DO
      END DO
    END DO

    IF (loc_x > 1) THEN
      DO j=ny-1,2,-1
        DO k=nz-1,1,-1
          v_grd(2,j,k)=v_grd(2,j,k) +tem1(1,j,k)
        END DO
      END DO
    END IF

    IF (loc_y > 1) THEN
      DO i=nx-1,2,-1
        DO k=nz-1,1,-1
          v_grd(i,2,k)=v_grd(i,2,k) +tem1(i,1,k)
        END DO
      END DO
    END IF

    DO i=nx-1,2,-1
      DO j=ny-1,2,-1
        DO k=nz-1,1,-1
          tem1(i,j,k)=smcw(i,j,k)*wgt_smth(ipass)
        END DO
      END DO
    END DO

    DO i=nx-1,2,-1
      DO j=ny-1,2,-1
        w_grd(i,j,1)=w_grd(i,j,1)-tem1(i,j,1)*4.0
        w_grd(i,j+1,1)=w_grd(i,j+1,1)+tem1(i,j,1)
        w_grd(i,j-1,1)=w_grd(i,j-1,1)+tem1(i,j,1)
        w_grd(i+1,j,1)=w_grd(i+1,j,1)+tem1(i,j,1)
        w_grd(i-1,j,1)=w_grd(i-1,j,1)+tem1(i,j,1)
      END DO
    END DO

    DO i=nx-1,2,-1
      DO j=ny-1,2,-1
        DO k=nz-1,2,-1
          w_grd(i,j,k)=w_grd(i,j,k)-tem1(i,j,k)*6.0
          w_grd(i,j,k+1)=w_grd(i,j,k+1)+tem1(i,j,k)
          w_grd(i,j,k-1)=w_grd(i,j,k-1)+tem1(i,j,k)
          w_grd(i,j+1,k)=w_grd(i,j+1,k)+tem1(i,j,k)
          w_grd(i,j-1,k)=w_grd(i,j-1,k)+tem1(i,j,k)
          w_grd(i+1,j,k)=w_grd(i+1,j,k)+tem1(i,j,k)
          w_grd(i-1,j,k)=w_grd(i-1,j,k)+tem1(i,j,k)
        END DO
      END DO
    END DO

    IF (loc_x > 1) THEN  ! count column 1's affect on column 2
      DO j=ny-1,2,-1
        DO k=nz-1,1,-1
          w_grd(2,j,k)=w_grd(2,j,k) +tem1(1,j,k)
        END DO
      END DO
    END IF

    IF (loc_y > 1) THEN  ! Count row 1's affect on column 2
      DO i=nx-1,2,-1
        DO k=nz-1,1,-1
          w_grd(i,2,k)=w_grd(i,2,k) +tem1(i,1,k)
        END DO
      END DO
    END IF

    IF (mp_opt > 0) THEN
      !CALL acct_interrupt(mp_acct)
      CALL mpsendrecv2dew(u_grd, nx, ny, nz, 0, 0, 0, tem1)
      CALL mpsendrecv2dns(u_grd, nx, ny, nz, 0, 0, 0, tem1)

      CALL mpsendrecv2dew(v_grd, nx, ny, nz, 0, 0, 0, tem1)
      CALL mpsendrecv2dns(v_grd, nx, ny, nz, 0, 0, 0, tem1)

      CALL mpsendrecv2dew(w_grd, nx, ny, nz, 0, 0, 0, tem1)
      CALL mpsendrecv2dns(w_grd, nx, ny, nz, 0, 0, 0, tem1)
      !CALL acct_stop_inter
    END IF

    RETURN
  END SUBROUTINE consmth_gradt

  !#####################################################################

  SUBROUTINE consmth_allocate_wrk(nx,ny,nz,istatus)
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nx,ny,nz
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (wrk_allocated) THEN
      WRITE(*,'(1x,a)') 'INFO: Should not be here. Maybe multiple calls of "consmth_allocate_wrk".'
      istatus = 1
      RETURN
    END IF

    istatus = 0

    ALLOCATE(u(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: u")
    ALLOCATE(v(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: v")
    ALLOCATE(w(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: w")

    ALLOCATE(smcu(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: smcu")
    ALLOCATE(smcv(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: smcv")
    ALLOCATE(smcw(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: smcw")

    ALLOCATE(tem1(nx,ny,nz),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_thermo: tem1")

    wrk_allocated = .TRUE.

    RETURN
  END SUBROUTINE consmth_allocate_wrk

  !#####################################################################

  SUBROUTINE consmth_deallocate_wrk(istatus)
    IMPLICIT NONE

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (wrk_allocated) THEN

      DEALLOCATE(u,v,w,          STAT = istatus )
      DEALLOCATE(smcu,smcv,smcw, STAT = istatus )
      DEALLOCATE(tem1,           STAT = istatus )

      wrk_allocated = .FALSE.

    END IF

    RETURN
  END SUBROUTINE consmth_deallocate_wrk

END MODULE constraint_smooth
