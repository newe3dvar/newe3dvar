!##################################################################
!######                                                      ######
!######                SUBROUTINE MIXUVW                     ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE mixuvw0(nx,ny,nz,                                            &
           u,v,w,ptprt,pprt,qv,qscalar,tke,                             &
           ubar,vbar,ptbar,pbar,rhostr,qvbar,                           &
           usflx,vsflx, x,y,z,zp, j1,j2,j3,stabchk,                     &
           umix,vmix,wmix,km,lendel,defsq,                              &
           tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,                &
           tem10,tem11,tem12)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the total mixing (turbulent mixing and the externally
!  imposed computational mixing) for the momentum equations. This
!  subroutine also calculates the turbulent mixing coefficient ,km,
!  which is used to calculate mixing terms for temperature and
!  water quantities. The mixing coefficient is based on Smagorinsky's
!  formulation. For the computational mixing, there are two options:
!  second order and fourth order mixing. The mixing applies only to
!  the perturbations quantities.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/1/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  7/6/92 (M. Xue and D. Weber)
!  Terrain included.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates at a given time level (m/s)
!    ptprt    Perturbation potential temperature at a given time level (K)
!    pprt     Perturbation pressure at a given time level (Pascal)
!    qv       Water vapor specific humidity at a given time level (kg/kg)
!    qc       Cloud water mixing ratio at a given time level (kg/kg)
!    qr       Rainwater mixing ratio at a given time level (kg/kg)
!    qi       Cloud ice mixing ratio at a given time level (kg/kg)
!    qs       Snow mixing ratio at a given time level (kg/kg)
!    qh       Hail mixing ratio at a given time level (kg/kg)
!    tke      Turbulent Kinetic Energy ((m/s)**2)
!
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!
!    usflx    Surface flux of u-momentum
!    vsflx    Surface flux of v-momentum
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space (m)
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    umix     Total mixing in u equation (kg/(m*s)**2)
!    vmix     Total mixing in v equation (kg/(m*s)**2)
!    wmix     Total mixing in w equation (kg/(m*s)**2)
!    km       Turbulent mixing coefficient for momentum ( m**2/s ).
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3).
!    defsq    Deformation squared (1/s**2)
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!    tem4     Temporary work array.
!    tem5     Temporary work array.
!    tem6     Temporary work array.
!    tem7     Temporary work array.
!    tem8     Temporary work array.
!    tem9     Temporary work array.
!    tem10    Temporary work array.
!    tem11    Temporary work array.
!    tem12    Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)
  REAL :: ptprt (nx,ny,nz)     ! Perturbation potential temperature (K)
  REAL :: pprt  (nx,ny,nz)     ! Perturbation pressure (Pascal)
!
  REAL :: qv    (nx,ny,nz)     ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq)     ! Cloud water mixing ratio (kg/kg)
  REAL :: tke   (nx,ny,nz)     ! Turbulent Kinetic Energy ((m/s)**2)
!
  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific humidity
                               ! (kg/kg)

  REAL :: usflx (nx,ny)        ! surface flux of u-momentum (kg/(m*s**2))
  REAL :: vsflx (nx,ny)        ! surface flux of v-momentum (kg/(m*s**2))
  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.

                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.


  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! d( zp )/d( z ).
  REAL :: stabchk(nx,ny)
!
  REAL :: umix  (nx,ny,nz)     ! Turbulent mixing on u-momentum.
  REAL :: vmix  (nx,ny,nz)     ! Turbulent mixing on v-momentum.
  REAL :: wmix  (nx,ny,nz)     ! Turbulent mixing on w-momentum.

  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient for
                               ! momentum. ( m**2/s )

  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)

  REAL :: defsq (nx,ny,nz)     ! Deformation squared (1/s**2)
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
  REAL :: tem4  (nx,ny,nz)     ! Temporary work array
  REAL :: tem5  (nx,ny,nz)     ! Temporary work array
  REAL :: tem6  (nx,ny,nz)     ! Temporary work array
  REAL :: tem7  (nx,ny,nz)     ! Temporary work array
  REAL :: tem8  (nx,ny,nz)     ! Temporary work array
  REAL :: tem9  (nx,ny,nz)     ! Temporary work array
  REAL :: tem10 (nx,ny,nz)     ! Temporary work array
  REAL :: tem11 (nx,ny,nz)     ! Temporary work array
  REAL :: tem12 (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: nxyz,i,j,k

!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Control parameters for subgrid scalar turbulent mixing are passed
!  in globcst.inc, these include:
!
!  tmixopt: Tubulent mixing option
!           = 0, zero turbulent mixing.
!           = 1, constant mixing coefficient.
!           = 2, Smagorinsky mixing coefficient.
!           = 3, Smagorinsky + constant coefficient mixing.
!  tmixcst: Constant mixing coefficient. (Options 1 and 3)
!
!-----------------------------------------------------------------------
!
! IF( tmixopt == 0) THEN

!   nxyz = nx*ny*nz
!   CALL flzero(umix,nxyz)
!   CALL flzero(vmix,nxyz)
!   CALL flzero(wmix,nxyz)

! ELSE
    CALL tmixuvw0(nx,ny,nz,                                             &
         u,v,w,ptprt,pprt,qv,qscalar,tke,                               &
         ubar,vbar,ptbar,pbar,rhostr,qvbar,                             &
         usflx,vsflx, x,y,z,zp, j1,j2,j3,stabchk,                       &
         umix,vmix,wmix,km, lendel,defsq,                               &
         tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,                  &
         tem10,tem11,tem12)

! END IF
!
  IF( cmix2nd == 1) THEN
!
!-----------------------------------------------------------------------
!
!  Calculate the second order computational mixing terms and add
!  them to the turbulent mixing terms umix, vmix, wmix.
!
!-----------------------------------------------------------------------
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem7(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
        END DO
      END DO
    END DO

    CALL cmix2uvw0(nx,ny,nz,                                             &
                  u,v,w, ubar,vbar,tem7,                                &
                  umix,vmix,wmix,                                       &
                  tem1,tem2,tem3,tem4,tem5,tem6)

  END IF

IF(1==0) THEN
  IF( cmix4th == 1) THEN
!
!-----------------------------------------------------------------------
!
!  Calculate the fourth order computational mixing terms and add
!  them to the turbulent mixing terms umix, vmix, wmix.
!
!-----------------------------------------------------------------------
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem7(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
        END DO
      END DO
    END DO

    CALL cmix4uvw0(nx,ny,nz,                                            &
                  u,v,w, ubar,vbar,tem7,                                &
                  umix,vmix,wmix,                                       &
                  tem1,tem2,tem3,tem4,tem5,tem6)
  END IF

END IF    !(1==0)

  RETURN
END SUBROUTINE mixuvw0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TMIXUVW0                   ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tmixuvw0(nx,ny,nz,                                           &
           u,v,w,ptprt,pprt,qv,qscalar,tke,                             &
           ubar,vbar,ptbar,pbar,rhostr,qvbar,                           &
           usflx,vsflx, x,y,z,zp, j1,j2,j3,stabchk,                     &
           umix,vmix,wmix,km,lendel,defsq,                              &
           nsqed,tau11,tau12,tau13,tau22,tau23,tau33,tem1,tem2,         &
           tem3,tem4,tem5)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the turbulent mixing terms for the momentum equations. These
!  terms are expressed in the form of a stress tensor.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/1/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  6/25/92 (M. Xue and D. Weber)
!  Terrain included.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates at a given time level (m/s)
!    ptprt    Perturbation potential temperature at a given time level (K)
!    pprt     Perturbation pressure at a given time level (Pascal)
!    qv       Water vapor specific humidity at a given time level (kg/kg)
!    qc       Cloud water mixing ratio at a given time level (kg/kg)
!    qr       Rainwater mixing ratio at a given time level (kg/kg)
!    qi       Cloud ice mixing ratio at a given time level (kg/kg)
!    qs       Snow mixing ratio at a given time level (kg/kg)
!    qh       Hail mixing ratio at a given time level (kg/kg)
!    tke      Turbulent Kinetic Energy ((m/s)**2)
!
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!
!    usflx    Surface flux of u-momentum
!    vsflx    Surface flux of v-momentum
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space (m)
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    umix     Total mixing in u equation (kg/(m*s)**2)
!    vmix     Total mixing in v equation (kg/(m*s)**2)
!    wmix     Total mixing in w equation (kg/(m*s)**2)
!    km       Turbulent mixing coefficient for momentum ( m**2/s ).
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3)
!    defsq    Deformation squared (1/s**2)
!
!  WORK ARRAYS:
!
!    real nsqed  Temporary array containing the Brunt Vaisala frquency
!    real tau11  Temporary work array
!    real tau12  Temporary work array
!    real tau13  Temporary work array
!    real tau22  Temporary work array
!    real tau23  Temporary work array
!    real tau33  Temporary work array
!    real tem1   Temporary work array
!    real tem2   Temporary work array
!    real tem3   Temporary work array
!    real tem4   Temporary work array
!    real tem5   Temporary work array
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)
  REAL :: ptprt (nx,ny,nz)     ! Perturbation potential temperature (K)
  REAL :: pprt  (nx,ny,nz)     ! Perturbation pressure (Pascal)
!
  REAL :: qv    (nx,ny,nz)     ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq)     ! Cloud water mixing ratio (kg/kg)
  REAL :: tke   (nx,ny,nz)     ! Turbulent Kinetic Energy ((m/s)**2)
!
  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific humidity
                               ! (kg/kg)

  REAL :: usflx (nx,ny)        ! surface flux of u-momentum (kg/(m*s**2))
  REAL :: vsflx (nx,ny)        ! surface flux of v-momentum (kg/(m*s**2))
  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.


  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! d( zp )/d( z ).
  REAL :: stabchk(nx,ny)
!
  REAL :: umix  (nx,ny,nz)     ! Turbulent mixing on u-momentum.
  REAL :: vmix  (nx,ny,nz)     ! Turbulent mixing on v-momentum.
  REAL :: wmix  (nx,ny,nz)     ! Turbulent mixing on w-momentum.

  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient for
                               ! momentum. ( m**2/s )

  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)

  REAL :: defsq (nx,ny,nz)     ! Deformation squared (1/s**2)
!
  REAL :: nsqed (nx,ny,nz)     ! Temporary work array
  REAL :: tau11 (nx,ny,nz)     ! Temporary work array
  REAL :: tau12 (nx,ny,nz)     ! Temporary work array
  REAL :: tau13 (nx,ny,nz)     ! Temporary work array
  REAL :: tau22 (nx,ny,nz)     ! Temporary work array
  REAL :: tau23 (nx,ny,nz)     ! Temporary work array
  REAL :: tau33 (nx,ny,nz)     ! Temporary work array
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
  REAL :: tem4  (nx,ny,nz)     ! Temporary work array
  REAL :: tem5  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
  REAL :: tema,temb,temc
  REAL :: zpup,zplow

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Calculate the static stability parameter N squared (Brunt-Vaisala
!  frequency). The arrays tem1,tem2,tau11,tau12 are used as work
!  arrays for the following call.
!
!-----------------------------------------------------------------------
!
IF(1==0) THEN

  CALL stabnsq0(nx,ny,nz,                                                &
               ptprt,pprt,qv,qscalar,ptbar,qvbar,pbar,j3,                &
               nsqed,tem1,tem2,tem3,tem4)

END IF
!
!-----------------------------------------------------------------------
!
!  Calculate the deformation tensor components Dij, which are stored
!  in arrays Tauij.
!
!-----------------------------------------------------------------------

  CALL deform0(nx,ny,nz,u,v,w,j1,j2,j3,                                  &
      tau11,tau12,tau13,tau22,tau23,tau33,tem1,tem2,tem3)

!-----------------------------------------------------------------------
!
!  Calculate the turbulent mixing coefficient, km, using
!  Smagorinsky's formulation
!
!-----------------------------------------------------------------------
!

  CALL cftmix0(nx,ny,nz,tau11,tau12,tau13,tau22,tau23,tau33,             &
              nsqed, zp,tke, km,lendel,defsq, tem1)

!
!-----------------------------------------------------------------------
!
!  Calculate the stress tensor Tauij = km * (rhostr/j3) * Dij.
!
!  Please note umix and vmix are used here to temporarily store
!  tau31 and tau32, which are used immediatedly by wmixtrm.
!  umix and vmix are freed afterwards.
!
!-----------------------------------------------------------------------
!

  CALL stress0(nx,ny,nz,j3,zp,                                           &
              km,rhostr,tau11,tau12,tau13,tau22,tau23,tau33,            &
              umix,vmix,                                                &
              tem1,tem2,tem3)
! print*,'before wmixtrm0'
  CALL wmixtrm0(nx,ny,nz, j1,j2,j3, umix,vmix,tau33,                     &
               wmix, tem1, tem2, tem3)
! print*,'after  wmixtrm0'
!
!-----------------------------------------------------------------------
!
!
!  Set the surface momentum fluxes:
!
!  If sfcphy = 1, the surface momentum fluxes are set to the values in
!  usflx and vsflx, which are calculated in the surface physics
!  subroutine sfcflx.
!
!  If sfcphy = 0, the surface momentum fluxes are calculated in the same way
!  as in the the domain interior (i.e., using a subgrid scale turbulence
!  parameterization).
!
!-----------------------------------------------------------------------
!

  IF( sfcphy /= 0 ) THEN
!
    IF ( (sflxdis == 0) .OR. (sflxdis == 1 .AND.                        &
           (sfcphy == 1.OR.sfcphy == 3).AND.cdmlnd == 0.0) .OR.         &
           (sflxdis == 2) ) THEN
!
!-----------------------------------------------------------------------
!
!  tau13 at the surface = usflx.
!
!-----------------------------------------------------------------------
!
      DO j=2,ny-2
        DO i=2,nx-1
          tau13(i,j,2) = usflx(i,j)
        END DO
      END DO
!
!-----------------------------------------------------------------------
!
!  tau23 at the surface = vsflx.
!
!-----------------------------------------------------------------------
!
      DO j=2,ny-1
        DO i=2,nx-2
          tau23(i,j,2) = vsflx(i,j)
        END DO
      END DO

    ELSE IF (sflxdis == 1)  THEN

      DO j=2,ny-2
        DO i=2,nx-1
!         tema = 0.5*(stabchk(i,j)+stabchk(i-1,j))
          temb = stabchk(i  ,j) - ( ptprt(i  ,j,2)+ptbar(i  ,j,2) )
          temc = stabchk(i-1,j) - ( ptprt(i-1,j,2)+ptbar(i-1,j,2) )
          tema = 0.5*(temb + temc)
          DO k=2,nz-1
            zpup =0.5*(zp(i,j,k)+zp(i-1,j,k))
            zplow=0.5*(zp(i,j,2)+zp(i-1,j,2))
            IF ( (tema <= 0.) .OR. ((zpup-zplow) > pbldpth0))  CYCLE

            tau13(i,j,k)=0.5*usflx(i,j)*                                &
                         (1.0-(zpup-zplow)/pbldpth0)**2

          END DO
        END DO
      END DO

      DO j=2,ny-1
        DO i=2,nx-2
!         tema = 0.5*(stabchk(i,j)+stabchk(i,j-1))
          temb = stabchk(i  ,j) - ( ptprt(i  ,j,2)+ptbar(i  ,j,2) )
          temc = stabchk(i,j-1) - ( ptprt(i,j-1,2)+ptbar(i,j-1,2) )
          tema = 0.5*(temb + temc)
          DO k=2,nz-1
            zpup =0.5*(zp(i,j,k)+zp(i,j-1,k))
            zplow=0.5*(zp(i,j,2)+zp(i,j-1,2))
            IF ( (tema <= 0.) .OR. ((zpup-zplow) > pbldpth0))  CYCLE

            tau23(i,j,k)=0.5*vsflx(i,j)*                                &
                         (1.0-(zpup-zplow)/pbldpth0)**2

          END DO
        END DO
      END DO

    END IF

  END IF
!
!-----------------------------------------------------------------------
!
!  Calculate the divergence of the stresses. This is the turbulent
!  mixing on u and v momentum (umix and vmix).
!
!-----------------------------------------------------------------------
!
  CALL umixtrm0(nx,ny,nz, j1,j2,j3, tau11,tau12,tau13,                   &
               umix, tem1, tem2, tem3)

  CALL vmixtrm0(nx,ny,nz, j1,j2,j3, tau12,tau22,tau23,                   &
               vmix, tem1, tem2, tem3)
!
!-----------------------------------------------------------------------
!
!  Set the mixing terms on the lateral boundaries to zero
!
!  The umix and vmix terms are set for completness, they are not
!  used in any calculations.
!
!-----------------------------------------------------------------------

IF(1==0) THEN

!  DO 51 k=2,nz-2
!  DO 51 j=1,ny-1
!    umix(1,j,k)=umix(2,j,k)
!    umix(nx,j,k)=umix(nx-1,j,k)
! 51    CONTINUE

!  DO 52 k=2,nz-2
!  DO 52 i=1,nx-1
!    vmix(i,1,k)=vmix(i,2,k)
!    vmix(i,ny,k)=vmix(i,ny-1,k)
!  52    CONTINUE


!
!-----------------------------------------------------------------------
!
!  Set the mixing terms at the lateral boundaries equal to those
!  at the neighboring interior points.  This is done to essure
!  the boundary points get a similar amount of mixing as the interior
!  points. The boundary values will be used only in the case of
!  radiation boundary condition.
!
!-----------------------------------------------------------------------
!
  DO k=1,nz-1
    DO i=1,nx
      umix(i,1,k)=umix(i,2,k)
      umix(i,ny-1,k)=umix(i,ny-2,k)
    END DO
  END DO

  DO k=1,nz-1
    DO j=1,ny-1
      umix(1,j,k)=umix(2,j,k)
      umix(nx,j,k)=umix(nx-1,j,k)
    END DO
  END DO

  DO k=1,nz-1
    DO i=1,nx-1
      vmix(i,1,k)=vmix(i,2,k)
      vmix(i,ny,k)=vmix(i,ny-1,k)
    END DO
  END DO

  DO k=1,nz-1
    DO j=1,ny
      vmix(1,j,k)=vmix(2,j,k)
      vmix(nx-1,j,k)=vmix(nx-2,j,k)
    END DO
  END DO

  DO k=1,nz-1
    DO i=1,nx-1
      wmix(i,1,k)=wmix(i,2,k)
      wmix(i,ny-1,k)=wmix(i,ny-2,k)
    END DO
  END DO

  DO k=1,nz-1
    DO j=1,ny-1
      wmix(1,j,k)=wmix(2,j,k)
      wmix(nx-1,j,k)=wmix(nx-2,j,k)
    END DO
  END DO

END IF

  RETURN
END SUBROUTINE tmixuvw0

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE STABNSQ0                   ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE stabnsq0(nx,ny,nz,                                            &
           ptprt,pprt,qv,qscalar,ptbar,qvbar,pbar,j3,nsqed,              &
           tmprtr,qvs,qw,pt)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the static stability parameter N-squared (Brunt-Vaisala
!  frequency squared).
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  9/15/1992
!
!  MODIFICATION HISTORY:
!
!  3/25/94 (G. Bassett)
!  The condition for using saturated or nonsaturated formulation for
!  nsqed is changed from qc > 0 to qc >= 1e-06 in order to avoid
!  fluctuations in Km when very small values of qc are present.
!
!  6/7/94 (M. Xue)
!  Correction to the calculation of nsqed for the dry area inside
!  loop 3100. The error was introduced when modifications were made
!  on 3/25/94. Variable pt in that line was mistakenly written as ptbar.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    ptprt    Perturbation potential temperature (K)
!    pprt     Perturbation pressure (Pascal)
!    ptbar    Base state potential temperature (K)
!    qv       Water vapor specific humidity (kg/kg)
!    qc       Cloud water mixing ratio (kg/kg)
!    qr       Rainwater mixing ratio (kg/kg)
!    qi       Cloud ice mixing ratio (kg/kg)
!    qs       Snow mixing ratio (kg/kg)
!    qh       Hail mixing ratio (kg/kg)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!    pbar     Base state pressure (Pascal)
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    nsqed    Brunt-Vaisala frequency squared (1/s**2), a local array
!
!  WORK ARRAYS:
!
!    tmprtr   Work array for temperature
!    qvs      work array for saturation specific humidity
!    qw       Work array for total water mixing ratio
!    pt       Work arrays for total potential temperature
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: ptprt (nx,ny,nz)     ! Perturbation potential temperature (K)
  REAL :: pprt  (nx,ny,nz)     ! Perturbation pressure (Pascal)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: qv    (nx,ny,nz)     ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq)     ! Cloud water mixing ratio (kg/kg)
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific humidity
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal)
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! d( zp )/d( z ).

  REAL :: nsqed (nx,ny,nz)     ! Brunt-Vaisala frequency squared (1/s**2)

  REAL :: tmprtr(nx,ny,nz)     ! Temporary work array
  REAL :: qvs   (nx,ny,nz)     ! Temporary work array
  REAL :: qw    (nx,ny,nz)     ! Temporary work array
  REAL :: pt    (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
!
  REAL :: ppi, dzinvd2
  REAL :: eps   ! A small value of qc above which cloudwater is
                ! regarded as present.

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Calculate the static stability N**2.
!  The moist static stability follows that of Durran and Klemp (1982)
!
!-----------------------------------------------------------------------
!
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1

        ppi   = ((pbar(i,j,k)+pprt(i,j,k))/p0) ** (rddcp)
        tmprtr(i,j,k) = (ptbar(i,j,k)+ptprt(i,j,k)) * ppi
        pt(i,j,k) =  ptbar(i,j,k)+ptprt(i,j,k)

      END DO
    END DO
  END DO

  CALL satmr0(nx,ny,nz,pbar, tmprtr, qvs)

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1

        qw(i,j,k) = qvs(i,j,k)+qscalar(i,j,k,P_QC)+qscalar(i,j,k,P_QR)

      END DO
    END DO
  END DO

  dzinvd2=1.0/(2*dz)
  eps = 1.0E-6

  DO k=2,nz-2
    DO j=1,ny-1
      DO i=1,nx-1

        IF( moist == 0 .OR. qscalar(i,j,k,P_QC) <= eps ) THEN

          nsqed(i,j,k)=g*(pt(i,j,k+1)-pt(i,j,k-1))*dzinvd2              &
                       /(pt(i,j,k)*j3(i,j,k))

        ELSE

          nsqed(i,j,k)= g/j3(i,j,k) *                                   &
              ( (1+lathv*qvs(i,j,k)/(rd*tmprtr(i,j,k)))/                &
              (1+rd/rv*lathv*lathv*qvs(i,j,k)/(cp*rd*tmprtr(i,j,k)**2)) &
              * ((pt(i,j,k+1)-pt(i,j,k-1))/pt(i,j,k)                    &
              +  lathv/(cp*tmprtr(i,j,k))*(qvs(i,j,k+1)-qvs(i,j,k-1)) ) &
              - (qw(i,j,k+1)-qw(i,j,k-1)) ) *dzinvd2

        END IF

      END DO
    END DO
  END DO

  DO j=1,ny-1
    DO i=1,nx-1
      nsqed(i,j,  1 )=nsqed(i,j,2)

      nsqed(i,j,nz-1)=nsqed(i,j,nz-2)
    END DO
  END DO

  RETURN
END SUBROUTINE stabnsq0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE CFTMIX0                    ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE cftmix0(nx,ny,nz,                                             &
           tau11,tau12,tau13,tau22,tau23,tau33,nsqed,zp,                &
           tke, km,lendel,defsq,tem1)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the turbulent mixing coefficient, km, with the modified
!  Smagorinsky formulation.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/1/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  6/1/93 (M. Xue)
!  Calculation of delta from dzp rather than dz.
!
!  6/16/93 (M. Xue)
!  The values of KM at the lateral boundaries are explicitly set.
!  The calculated values are overwritten.
!
!  4/20/1995 (M. Xue)
!  Added an upper limit on KM in ensure numerical stability.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    tau11    Deformation tensor component (1/s)
!    tau12    Deformation tensor component (1/s)
!    tau13    Deformation tensor component (1/s)
!    tau22    Deformation tensor component (1/s)
!    tau23    Deformation tensor component (1/s)
!    tau33    Deformation tensor component (1/s)
!    nsqed    Brunt-Vaisala frequency (1/s**2), a local array
!    zp       Vertical coordinate of grid points in physical space (m)
!
!
!    tke      Turbulent Kinetic Energy ((m/s)**2)
!
!  OUTPUT:
!
!    km       Turbulent mixing coefficient for momentum ( m**2/s )
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3)
!    defsq    Deformation squared (1/s**2)
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: tau11(nx,ny,nz)      ! Deformation tensor component (1/s)
  REAL :: tau12(nx,ny,nz)      ! Deformation tensor component (1/s)
  REAL :: tau13(nx,ny,nz)      ! Deformation tensor component (1/s)
  REAL :: tau22(nx,ny,nz)      ! Deformation tensor component (1/s)
  REAL :: tau23(nx,ny,nz)      ! Deformation tensor component (1/s)
  REAL :: tau33(nx,ny,nz)      ! Deformation tensor component (1/s)

  REAL :: nsqed (nx,ny,nz)     ! Brunt-Vaisala frequency (1/s**2)
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.
!
  REAL :: tke   (nx,ny,nz)     ! Turbulent Kinetic Energy ((m/s)**2)
  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient for
                               ! momentum. ( m**2/s )
  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)
  REAL :: defsq (nx,ny,nz)     ! Deformation squared (1/s**2)

  REAL :: tem1  (nx,ny,nz)     ! Array to store delta
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
!
  REAL :: tema, temb, temc,teman,temap,tembn,tembp
  REAL :: delta, coeff, prinv, one3rd, dzp,k_min,k_max,km_nd_max
  REAL :: eps,checksta
  REAL :: deltah
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'
  INCLUDE 'bndry.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!-----------------------------------------------------------------------
!
!  Set the mixing coefficient, km, to a constant value TMIXCST.
!
!-----------------------------------------------------------------------
!
  one3rd= 1.0/3.0


    DO k=1,nz-1
      DO i=1,nx-1
        DO j=1,ny-1
          km(i,j,k)=tmixcst
        END DO
      END DO
    END DO

    RETURN

END SUBROUTINE cftmix0

!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DEFORM0                    ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!
!

SUBROUTINE deform0(nx,ny,nz,u,v,w,j1,j2,j3,                              &
           d11,d12,d13,d22,d23,d33,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the deformation tensor components Dij.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/1/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  6/25/92 (M. Xue and D. Weber)
!  Terrain included.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates at a given time level (m/s).
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    d11      Deformation tensor component (1/s)
!    d12      Deformation tensor component (1/s)
!    d13      Deformation tensor component (1/s)
!    d21      Deformation tensor component (1/s)
!    d22      Deformation tensor component (1/s)
!    d23      Deformation tensor component (1/s)
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)
!
  REAL :: j1    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! d( zp )/d( z ).
!
  REAL :: d11   (nx,ny,nz)     ! Deformation tensor component (1/s)
  REAL :: d12   (nx,ny,nz)     ! Deformation tensor component (1/s)
  REAL :: d13   (nx,ny,nz)     ! Deformation tensor component (1/s)
  REAL :: d22   (nx,ny,nz)     ! Deformation tensor component (1/s)
  REAL :: d23   (nx,ny,nz)     ! Deformation tensor component (1/s)
  REAL :: d33   (nx,ny,nz)     ! Deformation tensor component (1/s)
!
  REAL :: tem1(nx,ny,nz)       ! Temproary working array
  REAL :: tem2(nx,ny,nz)       ! Temproary working array
  REAL :: tem3(nx,ny,nz)       ! Temproary working array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'
  INCLUDE 'bndry.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!-----------------------------------------------------------------------
!
!  Calculate the deformation tensor components Dij, which are stored
!  in arrays dij.
!
!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!
!  Calculate the deformation tensor component D11:
!
!   d11 = 2./j3 *(difx0(avgsu0(j3) * u)) +
!                (difz0(avgx0(j1 * avgsw0(u)))))
!
!-----------------------------------------------------------------------
!

!-----------------------------------------------------------------------
!
!  Calculate difx0(avgsu0(j3) * u)
!
!-----------------------------------------------------------------------
  tem1 = 0.0; tem2 = 0.0
  CALL avgsu0(j3, nx,ny,nz, 1,ny-1, 1,nz-1, tem1)

  CALL aamult0(tem1, u,nx,ny,nz, 1,nx, 1,ny-1, 1,nz-1, tem2)

  onvf= 0
  CALL difx0(tem2, onvf,                                                 &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, dx, d11)
!
!  At this point, D11 = difx0(avgsu0(j3) * u)
!

  IF( ternopt /= 0 ) THEN

!-------------------------------------------------------------
!
!  Calculate the second term of d11
!  difz(avgx(j1 * avgsw(u)))
!
!-------------------------------------------------------------

  tem1 = 0.0; tem2 = 0.0; tem3 = 0.0
    CALL avgsw0(u,nx,ny,nz, 1,nx, 1,ny-1, tem1)

    CALL aamult0(tem1, j1,nx,ny,nz, 1,nx, 1,ny-1, 1,nz, tem2)

    onvf = 0
    CALL avgx0(tem2, onvf,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz, tem3)

    tem1 = 0.0
    onvf = 0
    CALL difz0(tem3, onvf,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, dz, tem1)

!-----------------------------------------------------------------------
!
!  Combine the terms, ( 2 /j3 * (d11 + TEM1)) to form the
!  final D11 stress tensor
!
!-----------------------------------------------------------------------

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          d11(i,j,k)=(2.0/j3(i,j,k)) * (d11(i,j,k)+tem1(i,j,k))
        END DO
      END DO
    END DO

  ELSE  ! When ternopt=0, tem1=0.0

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          d11(i,j,k)=(2.0/j3(i,j,k)) * d11(i,j,k)
        END DO
      END DO
    END DO

  END IF

!-----------------------------------------------------------------------
!
!  Calculate the deformation tensor component d22:
!
!   d22 = 2./j3 *(dify(avgsv(j3 * v)) +
!                (difz(avgy(j2 * avgsw(v)))))
!
!
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!
!  Calculate the first term in D22
!  dify(avgsv(j3) * v)
!
!-----------------------------------------------------------------------
  tem1 = 0.0; tem2= 0.0
  CALL avgsv0(j3,nx,ny,nz, 1,nx-1, 1,nz-1, tem1)

  CALL aamult0(tem1, v,nx,ny,nz, 1,nx-1, 1,ny, 1,nz-1, tem2)

  onvf = 0
  CALL dify0(tem2, onvf,                                                 &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, dy, d22)

!
!  At this point, D22 = dify(avgsv(j3) * v)
!
  IF( ternopt /= 0 ) THEN


!-------------------------------------------------------------------
!
!  Calculate the second term of d22
!  difz(avgy(j2 * avgsw(v))))
!
!-------------------------------------------------------------------
    tem1 = 0.0; tem2= 0.0; tem3= 0.0
    CALL avgsw0(v,nx,ny,nz, 1,nx-1, 1,ny, tem1)

    CALL aamult0(tem1, j2,                                               &
         nx,ny,nz, 1,nx-1, 1,ny, 1,nz, tem2)

    onvf = 0
    CALL avgy0(tem2, onvf,                                               &
         nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz, tem3)

    tem1 = 0.
    onvf = 0
    CALL difz0(tem3, onvf,                                               &
         nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, dz, tem1)

!-----------------------------------------------------------------------
!
!  Combine terms 2./j3 * (D22 + TEM1) to obtain the final
!  D22 deformation tensor
!
!-----------------------------------------------------------------------

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          d22(i,j,k)=(2.0/j3(i,j,k)) * (d22(i,j,k)+tem1(i,j,k))
        END DO
      END DO
    END DO

  ELSE   ! When ternopt=0, tem1=0.0

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          d22(i,j,k)=(2.0/j3(i,j,k)) * d22(i,j,k)
        END DO
      END DO
    END DO

  END IF

!-----------------------------------------------------------------------
!
!  Calculate the stress tensor component d33
!  d33 = 2./j3 * difz(w)
!
!-----------------------------------------------------------------------

  onvf = 0
  CALL difz0(w, onvf,                                                    &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, dz, d33)

!
!  calculate the final d33 stress tensor
!

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        d33(i,j,k)=(2.0/j3(i,j,k)) * d33(i,j,k)
      END DO
    END DO
  END DO
!-----------------------------------------------------------------------
!
!  Calculate the deformation tensor d12
!
!  d12 = 1/(avgsv0(avgsu0(j3))) * (dify0(avgsu0(j3) * u) + difx0(avgsv0(J3) * v)
!        + difz0(avgsu0(j2) * avgsv0(avgsw0(u)) + avgsv0(j1) * avgsu0(avgsw0(v))))
!
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!
!  Note: In order to reduce memory usage, order dependence has been
!  introduced into the calculation of D12, which reduces the possible
!  term-wise parallellism.  This is due to the constraint of only two
!  temporary arrays being avaliable for passage into this subroutine.
!
!  The third term of the D12 tensor (which includes two sub-terms)
!  will be calculated first.
!
!-----------------------------------------------------------------------

  IF( ternopt /= 0 ) THEN


!-------------------------------------------------------------------
!
!  Calculate the first sub-term of the third term of D12
!  avgsu0(j2) * avgsv0(avgsw0(u)), this term is zero when ternopt=0
!
!-------------------------------------------------------------------
!   tem1 =0.
!   CALL avgsw0(u,nx,ny,nz, 1,nx, 1,ny-1, tem1)
    tem2 =0.
!!  CALL avgsv0(tem1,nx,ny,nz, 1,nx, 1,nz, tem2)
    CALL avgsv0(u   ,nx,ny,nz, 1,nx, 1,nz, tem2)

    tem1 = 0.0
    CALL avgsu0(j2,nx,ny,nz, 1,ny, 1,nz, tem1)

    CALL aamult0(tem1, tem2,                                             &
         nx,ny,nz, 1,nx, 1,ny, 1,nz, d12)

!
!  At this point, D12 = avgsu0(J2) * avgsv0(avgsw0(u))
!

!-------------------------------------------------------------------
!
!  Calculate the second sub-term in the third term of d12
!  avgsv0(j1) * avgsu0(avgsw0(v)), this term is zero when ternopt=0
!
!-------------------------------------------------------------------
!   tem1 = 0.
!   CALL avgsw0(v,nx,ny,nz, 1,nx-1, 1,ny, tem1)
    tem2 = 0.
!!  CALL avgsu0(tem1,nx,ny,nz, 1,ny, 1,nz, tem2)
    CALL avgsu0(v,nx,ny,nz, 1,ny, 1,nz, tem2)

    tem1 = 0.
    CALL avgsv0(j1,nx,ny,nz, 1,nx, 1,nz, tem1)

    tem3 = 0.0
    CALL aamult0(tem1, tem2,                                             &
          nx,ny,nz, 1,nx, 1,ny, 1,nz, tem3)

!-------------------------------------------------------------------
!
!  Combine d12 and tem1 terms to get the third term of d12
!
!-------------------------------------------------------------------

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          tem3(i,j,k)=d12(i,j,k)+tem3(i,j,k)
        END DO
      END DO
    END DO

!
!  At this point, tem1 = avgsu0(j2) * avgsv0(avgsw0(u)) +
!                        avgsv0(j1) * avgsu0(avgsw0(v))
!
    onvf = 0
    CALL difz0(tem3, onvf,                                               &
         nx,ny,nz, 1,nx, 1,ny, 1,nz-1, dz, d12)

  ELSE

    DO k=1,nz-1
      DO j=1,ny
        DO i=1,nx
          d12(i,j,k)=0.0
        END DO
      END DO
    END DO

  END IF
!
!  At this point, d12 = the final third term of d12
!

!-------------------------------------------------------------------
!
!  Calculate the first term in d12
!  dify0(avgsu0(j3) * u)
!
!-------------------------------------------------------------------
  tem1 = 0.0
  CALL avgsu0(j3,nx,ny,nz, 1,ny-1, 1,nz-1, tem1)
  tem2 = 0.0
  CALL aamult0(tem1, u,                                                  &
        nx,ny,nz, 1,nx, 1,ny-1, 1,nz-1, tem2)

  tem3 = 0.
  onvf = 1
  CALL dify0(tem2, onvf,                                                 &
        nx,ny,nz, 1,nx, 2,ny-1, 1,nz-1, dy, tem3)

!  use boundu subroutine here to expand tem2 to the points
!  beyond the physical boundaries.

! CALL boundv0(tem3,nx,ny,nz, 1,nx, 1,nz-1)

!-------------------------------------------------------------------
!
!  Combine the first and third terms of D12 (D12 +TEM2)
!
!-------------------------------------------------------------------

  DO k=1,nz-1
    DO j=1,ny
      DO i=1,nx
        d12(i,j,k)=d12(i,j,k)+tem3(i,j,k)
      END DO
    END DO
  END DO

!
!  At this point, d12 = first + third terms of d12
!

!-------------------------------------------------------------------
!
!  Calculate the second term in D12
!  difx0(avgsv0(j3) * v))
!
!-------------------------------------------------------------------
  tem1 = 0.
  CALL avgsv0(j3,nx,ny,nz, 1,nx-1, 1,nz-1, tem1)
  tem2 = 0.
  CALL aamult0(tem1, v,                                                  &
        nx,ny,nz, 1,nx-1, 1,ny, 1,nz-1, tem2)

  tem3 = 0.
  onvf = 1
  CALL difx0(tem2, onvf,                                                 &
        nx,ny,nz, 2,nx-1, 1,ny, 1,nz-1, dx, tem3)

! CALL boundu0(tem3,nx,ny,nz, 1,ny, 1,nz-1)

!-------------------------------------------------------------------
!
!  Combine D12 and TEM2 terms
!
!-------------------------------------------------------------------

  DO k=1,nz-1
    DO j=1,ny
      DO i=1,nx
        d12(i,j,k)=d12(i,j,k)+tem3(i,j,k)
      END DO
    END DO
  END DO

!
!  At this point, d12 = first + second + third terms of d12
!

!-------------------------------------------------------------------
!
!  Calculate the coefficient of d12
!  avgsv0(avgsu0(j3))
!
!-------------------------------------------------------------------
  tem1 = 0.
  CALL avgsu0(j3,nx,ny,nz, 1,ny-1, 1,nz-1, tem1)

  tem2 = 0.
  CALL avgsv0(tem1,nx,ny,nz, 1,nx, 1,nz-1, tem2)

!
!  Compute the final d12 = d12 / tem2
!

  DO k=1,nz-1
    DO j=1,ny
      DO i=1,nx
        d12(i,j,k)=d12(i,j,k)/tem2(i,j,k)
      END DO
    END DO
  END DO

IF(1==1) THEN

!-----------------------------------------------------------------------
!
!  Calculate the deformation tensor d13
!  d13 = 1./ avgsw(avgsu0(j3)) * (difx0(avgsw0(j3) * w) +
!        difz0(u + avgx0(j1 * (avgsu0(w)))))
!
!-----------------------------------------------------------------------

!-------------------------------------------------------------------
!
!  Compute the first term of d13
!  difx0(avgsw0(j3) * w)
!
!-------------------------------------------------------------------
  tem1 = 0.
  CALL avgsw0(j3,nx,ny,nz, 1,nx-1, 1,ny-1, tem1)
  tem2 = 0.
  CALL aamult0(tem1, w,                                                  &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz, tem2)

  onvf = 1
  CALL difx0(tem2, onvf,                                                 &
        nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz, dx, d13)

! CALL boundu0(d13,nx,ny,nz, 1,ny-1, 1,nz)

!
!  At this point D13 = difx0(avgsw0(j3) * w)
!

!-----------------------------------------------------------------------
!
!  Compute the second term of d13
!  difz(u + avgsw(j1 * (avgz(w))))
!
!-----------------------------------------------------------------------

  IF( ternopt /= 0 ) THEN
    tem1 = 0.
    CALL avgsu0(w,nx,ny,nz, 1,ny-1, 1,nz, tem1)
    tem2 = 0.
    CALL aamult0(tem1, j1,                                               &
         nx,ny,nz, 1,nx, 1,ny-1, 1,nz, tem2)
    tem3 = 0.
    onvf = 0
    CALL avgz0(tem2, onvf,                                               &
         nx,ny,nz, 1,nx, 1,ny-1, 1,nz-1, tem3)

!-----------------------------------------------------------------------
!
!  Compute u + tem2
!
!-----------------------------------------------------------------------

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx
          tem3(i,j,k)=u(i,j,k)+tem3(i,j,k)
        END DO
      END DO
    END DO

  ELSE   ! If ternopt=0, tem2=0

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx
          tem3(i,j,k)=u(i,j,k)
        END DO
      END DO
    END DO

  END IF

  tem1 = 0.
  onvf = 1
  CALL difz0(tem3, onvf,                                                 &
        nx,ny,nz, 1,nx, 1,ny-1, 2,nz-1, dz, tem1)
!
! CALL boundw0(tem1,nx,ny,nz, 1,nx, 1,ny-1)
!-----------------------------------------------------------------------
!
!  Compute d13 + tem1
!
!-----------------------------------------------------------------------

  DO k=1,nz
    DO j=1,ny-1
      DO i=1,nx
        d13(i,j,k)=d13(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO

!
!  At this point, D13 = first and second terms of d13
!

!-----------------------------------------------------------------------
!
!  Compute the coefficient of d13
!  avgsw0(avgsu0(j3))
!
!-----------------------------------------------------------------------
  tem1 = 0.
  CALL avgsu0(j3,nx,ny,nz, 1,ny-1, 1,nz-1, tem1)
  tem2 = 0.
  CALL avgsw0(tem1,nx,ny,nz, 1,nx, 1,ny-1, tem2)

!-----------------------------------------------------------------------
!
!  Compute the final d13 tensor = d13 / tem2
!
!-----------------------------------------------------------------------

  DO k=1,nz
    DO j=1,ny-1
      DO i=1,nx
        d13(i,j,k)=d13(i,j,k)/tem2(i,j,k)
      END DO
    END DO
  END DO

!-----------------------------------------------------------------------
!
!  Calculate the deformation tensor d23
!  d23 = 1./ avgsw(avgsv(j3)) * (dify(avgsw(j3) * w) +
!            difz(v + avgz(j2 * (avgsv(w)))))
!
!-----------------------------------------------------------------------

!-------------------------------------------------------------------
!
!  Compute the first term of d23
!  dify(avgsw(j3) * w)
!
!-------------------------------------------------------------------
  tem1 = 0.
  CALL avgsw0(j3,nx,ny,nz, 1,nx-1, 1,ny-1, tem1)
  tem2 = 0.
  CALL aamult0(tem1, w,                                                  &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz, tem2)

  onvf = 1
  CALL dify0(tem2, onvf,                                                 &
        nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz, dy, d23)

! CALL boundv0(d23,nx,ny,nz, 1,nx-1, 1,nz)

!
!  At this point d23 = dify(avgsw(j3) * w)
!

!-----------------------------------------------------------------------
!
!  Compute the second term of D23
!  difz(v + avgz(j2 * (avgsv(w))))
!
!-----------------------------------------------------------------------

  IF( ternopt /= 0 ) THEN
    tem1 = 0.
    CALL avgsv0(w,nx,ny,nz, 1,nx-1, 1,nz, tem1)
    tem2 = 0.
    CALL aamult0(tem1, j2,                                               &
         nx,ny,nz, 1,nx-1, 1,ny, 1,nz, tem2)

    onvf = 0
    CALL avgz0(tem2, onvf,                                               &
         nx,ny,nz, 1,nx-1, 1,ny, 1,nz-1, tem3)

    DO k=1,nz-1
      DO j=1,ny
        DO i=1,nx-1
          tem3(i,j,k)=v(i,j,k)+tem3(i,j,k)
        END DO
      END DO
    END DO

  ELSE   ! When ternopt.eq.0, tem2=0

    DO k=1,nz-1
      DO j=1,ny
        DO i=1,nx-1
          tem3(i,j,k)=v(i,j,k)
        END DO
      END DO
    END DO

  END IF

  tem1 = 0.
  onvf = 1
  CALL difz0(tem3, onvf,                                                 &
        nx,ny,nz, 1,nx-1, 1,ny, 2,nz-1, dz, tem1)

! CALL boundw0(tem1,nx,ny,nz, 1,nx-1, 1,ny)

!-----------------------------------------------------------------------
!
!    Compute d23 + tem1
!
!-----------------------------------------------------------------------

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx-1
        d23(i,j,k)=d23(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO

!
!  At this point, d23 = first + second terms of d23
!

!-----------------------------------------------------------------------
!
!  Compute the coefficient of d23
!  avgsw(avgsv(j3))
!
!-----------------------------------------------------------------------
  tem1 = 0.
  CALL avgsv0(j3,nx,ny,nz, 1,nx-1, 1,nz-1, tem1)
  tem2 = 0.
  CALL avgsw0(tem1,nx,ny,nz, 1,nx-1, 1,ny, tem2)

!-----------------------------------------------------------------------
!
!  Compute the final d23 deformation tensor d23 = d23 / tem2
!
!-----------------------------------------------------------------------

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx-1
        d23(i,j,k)=d23(i,j,k)/tem2(i,j,k)
      END DO
    END DO
  END DO
END IF
  RETURN
END SUBROUTINE deform0


!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE STRESS0                    ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE stress0(nx,ny,nz,j3,zp,                                       &
           km,rhostr,tau11,tau12,tau13,tau22,tau23,tau33,               &
           tau31,tau32,                                                 &
           tem1, tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the stress tensor tauij from deformation tensor
!  Dij (input as tauij).
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/1/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  6/26/92 (M. Xue and D. Weber)
!  Terrain included.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!    km       Turbulent mixing coefficient for momentum ( m**2/s ).
!    rhostr   Base state air density times j3 (kg/m**3).
!
!    tau11    Deformation tensor component (1/s)
!    tau12    Deformation tensor component (1/s)
!    tau13    Deformation tensor component (1/s)
!    tau22    Deformation tensor component (1/s)
!    tau23    Deformation tensor component (1/s)
!    tau33    Deformation tensor component (1/s)
!
!
!  OUTPUT:
!
!    tau11    Stress tensor component (kg/(m*s**2))
!    tau12    Stress tensor component (kg/(m*s**2))
!    tau13    Stress tensor component (kg/(m*s**2))
!    tau22    Stress tensor component (kg/(m*s**2))
!    tau23    Stress tensor component (kg/(m*s**2))
!    tau33    Stress tensor component (kg/(m*s**2))
!
!  WORKING ARRAYS:
!
!    tem1     Temporary working array
!    tem2     Temporary working array
!
!  (These arrays are defined and used locally (i.e. inside this
!   subroutine), they may also be passed into routines called by
!   this one. Exiting the call to this subroutine, these temporary
!   working arrays may be used for other purposes therefore their
!   contents overwritten. Please examine the usage of working arrays
!   before you alter the code.)
!
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient
  REAL :: rhostr(nx,ny,nz)     ! Base state air density times j3(kg/m**3)
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! d( zp )/d( z ).
!
  REAL :: tau11 (nx,ny,nz)     ! Deformation and stress tensor
                               ! component (kg/(m*s**2))
  REAL :: tau12 (nx,ny,nz)     ! Deformation and stress tensor
                               ! component (kg/(m*s**2))
  REAL :: tau13 (nx,ny,nz)     ! Deformation and stress tensor
                               ! component (kg/(m*s**2))
  REAL :: tau22 (nx,ny,nz)     ! Deformation and stress tensor
                               ! component (kg/(m*s**2))
  REAL :: tau23 (nx,ny,nz)     ! Deformation and stress tensor
                               ! component (kg/(m*s**2))
  REAL :: tau33 (nx,ny,nz)     ! Deformation and stress tensor
                               ! component (kg/(m*s**2))
!
  REAL :: zp    (nx,ny,nz)
  REAL :: tau31 (nx,ny,nz)
  REAL :: tau32 (nx,ny,nz)

  REAL :: tem1  (nx,ny,nz)     ! Temporary working array
  REAL :: tem2  (nx,ny,nz)     ! Temporary working array
  REAL :: tem3  (nx,ny,nz)     ! Temporary working array
!
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL :: tem,deltah2
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  deltah2 = dx*dy
!
!-----------------------------------------------------------------------
!
!  Calculate the stress tensor Tau11.
!  tau11 = rhostr/j3*km * d11
!
!-----------------------------------------------------------------------
  tem1= 0.
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
      END DO
    END DO
  END DO

  tem2 = 0.
  CALL aamult0(tem1,km,                                                  &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem2)


!
!  note:  tem1 = rhostr/j3*km
!
  tem3=0.0
  CALL aamult0(tem2,tau11,                                               &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem3)
  tau11 = tem3
!-----------------------------------------------------------------------
!
!  Calculate the stress tensor Tau22.
!  tau22 = rhostr*km/j3 * d22
!
!-----------------------------------------------------------------------
  tem3 = 0.
  CALL aamult0(tem2,tau22,                                               &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem3)
  tau22 = tem3
!-----------------------------------------------------------------------
!
!  Calculate the stress tensor Tau33.
!  tau33 = rhostr*km/j3 * d33
!
!-----------------------------------------------------------------------
!
  tem3 = 0.
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        IF (trbisotp == 1) THEN
          tem = 1.0
        ELSE
          tem = (zp(i,j,k+1)-zp(i,j,k))**2/deltah2
        END IF
        tem3(i,j,k)=tem2(i,j,k)*tau33(i,j,k)*tem
      END DO
    END DO
  END DO
    tau33 = tem3

!
!-----------------------------------------------------------------------
!
!  Calculate the stress tensor Tau12.
!  tau12 = avgsv0(avgsu0(rhostr/j3*km)) * d12
!
!-----------------------------------------------------------------------
!
  tem1 = 0.
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
      END DO
    END DO
  END DO

  tem2 = 0.
  CALL aamult0(tem1, km,                                                 &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem2)
  tem3 = 0.
  CALL avgsu0(tem2, nx,ny,nz, 1,ny-1, 1,nz-1, tem3)
  tem1 = 0.
  CALL avgsv0(tem3, nx,ny,nz, 1,nx, 1,nz-1, tem1)
  tem3 = 0.
  CALL aamult0(tem1, tau12,                                              &
              nx,ny,nz,1,nx,1,ny,1,nz-1,tem3)
  tau12 = tem3
!
!-----------------------------------------------------------------------
!
!  Calculate the stress tensor Tau31.
!  tau31 = avgsw0(avgsu0(rhostr/j3*km)) * d13
!
!-----------------------------------------------------------------------
!
  tem1 = 0.
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
      END DO
    END DO
  END DO
  tem2 = 0.
  CALL aamult0(tem1, km,                                                 &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem2)
  tem3 = 0.
  CALL avgsu0(tem2,nx,ny,nz, 1,ny-1, 1,nz-1, tem3)
  tem1 = 0.
  CALL avgsw0(tem3,nx,ny,nz, 1,nx, 1,ny-1, tem1)
  tau31 = 0.
  CALL aamult0(tem1, tau13,                                              &
              nx,ny,nz,1,nx,1,ny-1,1,nz,tau31)
!
!-----------------------------------------------------------------------
!
!  Calculate the stress tensor Tau13.
!  tau13 = avgsw0(avgsu0(rhostr/j3*km)) * d13
!
!-----------------------------------------------------------------------
  tem1 = 0.
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        IF (trbisotp == 1) THEN
          tem = 1.0
        ELSE
          tem = (zp(i,j,k+1)-zp(i,j,k))**2/deltah2
        END IF
        tem1(i,j,k)=rhostr(i,j,k)*km(i,j,k)*tem/j3(i,j,k)
      END DO
    END DO
  END DO

  tem2 = 0.
  CALL avgsu0(tem1,nx,ny,nz, 1,ny-1, 1,nz-1, tem2)
  tem1 = 0.
  CALL avgsw0(tem2,nx,ny,nz, 1,nx, 1,ny-1, tem1)
  tem3 = 0.
  CALL aamult0(tem1, tau13,                                              &
              nx,ny,nz,1,nx,1,ny-1,1,nz,tem3 )
  tau13 = tem3
!
!-----------------------------------------------------------------------
!
!  Calculate the stress tensor Tau32.
!  tau32 = avgsw(avgsv(rhostr/j3*km)) * d23
!
!-----------------------------------------------------------------------
  tem1 = 0.
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
      END DO
    END DO
  END DO

  tem2= 0.
  CALL aamult0(tem1, km,                                                 &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem2)
  tem3= 0.
  CALL avgsv0(tem2,nx,ny,nz, 1,nx-1, 1,nz-1, tem3)
  tem1= 0.
  CALL avgsw0(tem3,nx,ny,nz, 1,nx-1, 1,ny, tem1)
  tau32= 0.
  CALL aamult0(tem1, tau23,                                              &
              nx,ny,nz,1,nx-1,1,ny,1,nz,tau32)
!
!-----------------------------------------------------------------------
!
!  Calculate the stress tensor Tau23.
!  tau23 = avgsw(avgsv(rhostr/j3*km * (deltav/deltah)**2 )) * d23
!
!-----------------------------------------------------------------------
!
  tem1 = 0.
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        IF (trbisotp == 1) THEN
          tem = 1.0
        ELSE
          tem = (zp(i,j,k+1)-zp(i,j,k))**2/deltah2
        END IF
        tem1(i,j,k)=rhostr(i,j,k)*km(i,j,k)*tem/j3(i,j,k)
      END DO
    END DO
  END DO

  tem2 = 0.
  CALL avgsv0(tem1,nx,ny,nz, 1,nx-1, 1,nz-1, tem2)
  tem3 = 0.
  CALL avgsw0(tem2,nx,ny,nz, 1,nx-1, 1,ny, tem3)
  tem1 = 0.
  CALL aamult0(tem3, tau23,                                              &
              nx,ny,nz,1,nx-1,1,ny,1,nz,tem1 )
  tau23 = tem1

  RETURN
END SUBROUTINE stress0


!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TMIXPT0                    ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tmixpt0(nx,ny,nz,ptprt,ptbar,rhostr,km,lendel,ptsflx,        &
           x,y,z,zp, j1,j2,j3,stabchk,                                  &
           ptmix,                                                       &
           h1,h2,h3, tem1,tem2,tem3)
!
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the turbulent mixing term for the potential
!  temperature equation. The term is expressed in terms of turbulent
!  heat fluxes.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!
!  Added full documentation.
!
!  6/1/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  7/20/92 (M. Xue and D. Weber)
!  Terrain included.
!
!  6/16/93 (MX)
!  Break down into subroutine calls.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    ptprt    Perturbation potential temperature at a given time level (K)
!    ptbar    Base state potential temperature (K)
!    rhostr   Base state air density times j3 (kg/m**3)
!
!    usflx    Surface flux of u-momentum
!    vsflx    Surface flux of v-momentum
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space (m)
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!    km       Turbulent mixing coefficient for momentum ( m**2/s ).
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3)
!
!    ptsflx   Surface flux of heat (K*kg/(m*s**2))
!
!  OUTPUT:
!
!    ptmix    Total mixing in potential temperature
!             equation (K*kg/(m**3*s)).
!
!  WORK ARRAYS:
!
!    h1       Turbulent heat flux, a local array.
!    h2       Turbulent heat flux, a local array.
!    h3       Turbulent heat flux, a local array.
!    tem1     Temporary work array
!    tem2     Temporary work array
!    tem3     Temporary work array
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes, and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: ptprt (nx,ny,nz)     ! Perturbation potential temperature (K)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: rhostr(nx,ny,nz)     ! Base state air density times j3 (kg/m**3)
  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient for
                               ! momentum. ( m**2/s )
  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)
  REAL :: ptsflx(nx,ny)        ! surface flux of heat (K*kg/(m*s**2))

  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.

  REAL :: j1    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! d( zp )/d( z ).
  REAL :: stabchk(nx,ny)
!
  REAL :: ptmix (nx,ny,nz)     ! Turbulent mixing on potential
                               ! temperature (K*kg/(m**3 *s))
!
  REAL :: h1    (nx,ny,nz)     ! Turbulent heat flux
  REAL :: h2    (nx,ny,nz)     ! Turbulent heat flux
  REAL :: h3    (nx,ny,nz)     ! Turbulent heat flux
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL :: tem,temb
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'
  INCLUDE 'bndry.inc'
!
!-----------------------------------------------------------------------


!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


!-----------------------------------------------------------------------
!
!  tem1 = ptbar+ptprt
!
!-----------------------------------------------------------------------

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=ptbar(i,j,k)+ptprt(i,j,k)
      END DO
    END DO
  END DO


!-----------------------------------------------------------------------
!
!  Compute turbulent heat fluxes H1, H2 and H3.
!
!-----------------------------------------------------------------------

  CALL trbflxs0(nx,ny,nz,tem1,rhostr,km,lendel,x,y,z,zp, j1,j2,j3,       &
               h1,h2,h3, tem2,tem3)

!-----------------------------------------------------------------------
!
!  The surface heat flux depends on the option chosen:
!
!  If sfcphy = 1, the surface heat flux is set to ptsflx,
!  which is calculated in the surface physics subroutine sfcflx.
!
!  If sfcphy = 0, the flux is calculated in the same way as in the
!  interior, i.e., using subgrid scale turbulence parameterization.
!
!-----------------------------------------------------------------------
!
  IF( sfcphy /= 0 ) THEN
!
    IF ( (sflxdis == 0) .OR. (sflxdis == 1 .AND.                        &
           (sfcphy == 1.OR.sfcphy == 3).AND.cdmlnd == 0.0) ) THEN
!
!-----------------------------------------------------------------------
!
!  Heat flux h3 at the surface = ptsflx.
!
!-----------------------------------------------------------------------
!
      DO j=2,ny-2
        DO i=2,nx-2
          h3(i,j,2) = ptsflx(i,j)
        END DO
      END DO

    ELSE IF (sflxdis == 1 .OR. sflxdis == 2)  THEN

      tem=0.833*pbldpth0
      DO j=2,ny-2
        DO i=2,nx-2
          temb = ptsflx(i,j)/tem
          DO k=2,nz-1
            IF ( ( (stabchk(i,j)-ptprt(i,j,2)-ptbar(i,j,2)) <= 0)       &
                 .OR. ((zp(i,j,k)-zp(i,j,2)) > tem ))  CYCLE
            h3(i,j,k)=ptsflx(i,j)-(zp(i,j,k)-zp(i,j,2))*temb
          END DO
        END DO
      END DO

    END IF

  END IF
!
!-----------------------------------------------------------------------
!
!  Calculate the turbulent mixing term for the potential temperature
!
!  ptmix = difx0(avgx0(j3) * h1) + dify0(avgy0(j3) * h2) + difz0(h3 +
!          avgx0(avgz0(h1) * j1) + avgy0(avgz0(h2) * j2))
!
!-----------------------------------------------------------------------

  CALL smixtrm0(nx,ny,nz,h1,h2,h3,rhostr,x,y,z,zp, j1,j2,j3,             &
               ptmix,                                                   &
               tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions for the mixing term
!
!-----------------------------------------------------------------------
!
  DO k=1,nz-1
    DO i=1,nx-1
      ptmix(i,1,k)=ptmix(i,2,k)
      ptmix(i,ny-1,k)=ptmix(i,ny-2,k)
    END DO
  END DO

  DO k=1,nz-1
    DO j=1,ny-1
      ptmix(1,j,k)=ptmix(2,j,k)
      ptmix(nx-1,j,k)=ptmix(nx-2,j,k)
    END DO
  END DO

  RETURN
END SUBROUTINE tmixpt0

!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TMIXQV0                    ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################

SUBROUTINE tmixqv0(nx,ny,nz, qv,rhostr,km,lendel,qvsflx,                 &
           x,y,z,zp, j1,j2,j3,                                          &
           stabchk,ptsfc,ptsflx,qvsfc,ptbar,ptprt,                      &
           qvmix,                                                       &
           h1,h2,h3, tem1,tem2,tem3)

!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the turbulent mixing term for the water vapor specific
!  humidity equation. This term is expressed in the form of a turbulent
!  flux of the quantity in question.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/1/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  7/20/92 (M. Xue and D. Weber)
!  Terrain included.
!
!  6/16/93 (MX)
!  Break down into subroutine calls.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    qv       Water vapor specific humidity at a given time level (kg/kg)
!    rhostr   Base state air density times j3 (kg/m**3)
!
!    km       Turbulent mixing coefficient for momentum ( m**2/s ).
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3)
!
!    qvsflx  Surface flux of moisture (K*kg/(m*s**2))
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space (m)
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    qvmix    Total mixing in water vapor equation (kg/(m**3 * s))
!
!  WORK ARRAYS:
!
!    h1       Turbulent flux of moisture. A local array.
!    h2       Turbulent flux of moisture. A local array.
!    h3       Turbulent flux of moisture. A local array.
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes, and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: qv    (nx,ny,nz)     ! Water vapor specific humidity (kg/kg)
  REAL :: rhostr(nx,ny,nz)     ! Base state air density times j3 (kg/m**3)
  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient for
                               ! momentum. ( m**2/s )
  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)
  REAL :: qvsflx(nx,ny)        ! surface flux of moisture (kg/(m**2*s))

  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.

  REAL :: j1    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! d( zp )/d( z ).
  REAL :: stabchk(nx,ny)

  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: ptprt (nx,ny,nz)     ! Perturbation potential temperature
  REAL :: ptsfc(nx,ny)         ! Temperature at ground (K) (in top 1 cm layer)
  REAL :: qvsfc(nx,ny)         ! Effective qv at the surface (kg/kg)
  REAL :: ptsflx(nx,ny)        ! Surface heat flux (K*kg/(m*s**2))

!
  REAL :: qvmix (nx,ny,nz)     ! Turbulent mixing on water substance
                               ! kg/(m**3 *s)
!
  REAL :: h1    (nx,ny,nz)     ! Turbulent moisture flux.
  REAL :: h2    (nx,ny,nz)     ! Turbulent moisture flux.
  REAL :: h3    (nx,ny,nz)     ! Turbulent moisture flux.
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array


!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL :: tem,temb
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!-----------------------------------------------------------------------
!
!  Compute turbulent moisture fluxes H1, H2 and H3.
!
!-----------------------------------------------------------------------

  CALL trbflxs0(nx,ny,nz,qv,rhostr,km,lendel,x,y,z,zp, j1,j2,j3,         &
               h1,h2,h3, tem1,tem2)

!-----------------------------------------------------------------------
!
!  Set the surface moisture fluxes:
!
!  If sfcphy = 1, the surface moisture fluxes are set to qvsflx,
!  which is calcualted in the surface physics subroutine sfcflx.
!
!  If sfcphy = 0, the fluxes are calculated in the same way as in the
!  interior, i.e., using subgrid scale turbulence parameterization.
!
!-----------------------------------------------------------------------
!

  IF( sfcphy /= 0 ) THEN
!
    IF ( (sflxdis == 0) .OR. (sflxdis == 1 .AND.                        &
           (sfcphy == 1.OR.sfcphy == 3).AND.cdmlnd == 0.0) ) THEN
!
!-----------------------------------------------------------------------
!
!  Moisture flux h3 at the surface = qvsflx.
!
!-----------------------------------------------------------------------
!
      DO j=2,ny-2
        DO i=2,nx-2
          h3(i,j,2) = qvsflx(i,j)
        END DO
      END DO

    ELSE IF (sflxdis == 1 .OR. sflxdis == 2)  THEN

      tem=0.833*pbldpth0
      DO j=2,ny-2
        DO i=2,nx-2
          temb = qvsflx(i,j)/tem
          DO k=2,nz-1
            IF ( ( (stabchk(i,j)-ptprt(i,j,2)-ptbar(i,j,2)) <= 0)       &
                            .OR. ((zp(i,j,k)-zp(i,j,2)) > tem))  CYCLE

            h3(i,j,k)=qvsflx(i,j)-(zp(i,j,k)-zp(i,j,2))*temb

          END DO
        END DO
      END DO

    END IF

  END IF
!
!-----------------------------------------------------------------------
!
!  Calculate the mixing term for the water vapor mixing ratio (qv)
!
!  qvmix = difx0(avgx0(j3) * h1) + dify0(avgy0(j3) * h2) + difz0(h3 +
!          avgx0(avgz0(h1) * j1) + avgy0(avgz0(h2) * j2))
!
!-----------------------------------------------------------------------

  CALL smixtrm0(nx,ny,nz,h1,h2,h3,rhostr,x,y,z,zp, j1,j2,j3,             &
               qvmix,                                                   &
               tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  Set the qvmix boundary values to the inside points.
!
!-----------------------------------------------------------------------
!

  DO k=1,nz-1
    DO i=1,nx-1
      qvmix(i,1,k)=qvmix(i,2,k)
      qvmix(i,ny-1,k)=qvmix(i,ny-2,k)
    END DO
  END DO

  DO k=1,nz-1
    DO j=1,ny-1
      qvmix(1,j,k)=qvmix(2,j,k)
      qvmix(nx-1,j,k)=qvmix(nx-2,j,k)
    END DO
  END DO

  RETURN
END SUBROUTINE tmixqv0

!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TMIXQ0                     ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tmixq0(nx,ny,nz, q,rhostr,km,lendel,                          &
           x,y,z,zp, j1,j2,j3,                                          &
           qmix,                                                        &
           h1,h2,h3, tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the turbulent mixing term for a water substance equation.
!  This term is expressed in the form of a turbulent flux of the water
!  quantity in question.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/1/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  7/20/92 (M. Xue and D. Weber)
!  Terrain included.
!
!  6/16/93 (MX)
!  Break down into subroutine calls.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    q        mixing ratio of one of the water/ice quantities at
!             a given time level (kg/kg)
!
!    rhostr   Base state air density times j3 (kg/m**3)
!
!    km       Turbulent mixing coefficient for momentum ( m**2/s ).
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3)
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space (m)
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    qmix     Total mixing in water/ice substance
!             equation (kg/(m**3 * s))
!
!  WORK ARRAYS:
!
!    h1       Turbulent flux a water quantity, a local array.
!    h2       Turbulent flux a water quantity, a local array.
!    h3       Turbulent flux a water quantity, a local array.
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes, and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: q     (nx,ny,nz)     ! Water/ice mixing ratio (kg/kg)
  REAL :: rhostr(nx,ny,nz)     ! Base state air density times j3 (kg/m**3)
  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient for
                               ! momentum. ( m**2/s )
  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)
  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.
  REAL :: j1    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! d( zp )/d( z ).
!
  REAL :: qmix  (nx,ny,nz)     ! Turbulent mixing on water/ice
                               ! substance (kg/(m**2 *s))
!
  REAL :: h1    (nx,ny,nz)     ! Turbulent flux of a water quantity
  REAL :: h2    (nx,ny,nz)     ! Turbulent flux of a water quantity
  REAL :: h3    (nx,ny,nz)     ! Turbulent flux of a water quantity
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


!-----------------------------------------------------------------------
!
!  Compute turbulent water fluxes H1, H2 and H3.
!
!-----------------------------------------------------------------------

  CALL trbflxs0(nx,ny,nz,q,rhostr,km,lendel,x,y,z,zp, j1,j2,j3,          &
               h1,h2,h3, tem1,tem2)


!-----------------------------------------------------------------------
!
!  Calculate the mixing term for the water mixing ratio (q)
!
!  qmix = difx0(avgx0(j3) * h1) + dify0(avgy0(j3) * h2) + difz0(h3 +
!         avgx0(avgz0(h1) * j1) + avgy0(avgz0(h2) * j2))
!
!-----------------------------------------------------------------------


  CALL smixtrm0(nx,ny,nz,h1,h2,h3,rhostr,x,y,z,zp, j1,j2,j3,             &
               qmix,                                                    &
               tem1,tem2,tem3)

  DO k=1,nz-1
    DO i=1,nx-1
      qmix(i,1,k)=qmix(i,2,k)
      qmix(i,ny-1,k)=qmix(i,ny-2,k)
    END DO
  END DO

  DO k=1,nz-1
    DO j=1,ny-1
      qmix(1,j,k)=qmix(2,j,k)
      qmix(nx-1,j,k)=qmix(nx-2,j,k)
    END DO
  END DO

  RETURN
END SUBROUTINE tmixq0
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TRBFLXS0                   ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE trbflxs0(nx,ny,nz,s,rhostr,km,lendel,x,y,z,zp, j1,j2,j3,      &
           h1,h2,h3, tem1,tem2)
!
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the turbulent fluxes in x, y and z direction for a
!  scalar quantity 's'
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  6/16/1993
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    s        A scalar variable.
!    rhostr   Base state air density times j3 (kg/m**3)
!
!    km       Turbulent mixing coefficient for momentum ( m**2/s ).
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3)
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space (m)
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    h1       Turbulent flux in x direction.
!    h2       Turbulent flux in y direction.
!    h3       Turbulent flux in z direction.
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array
!    tem2     Temporary work array
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes, and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: s     (nx,ny,nz)     ! A scalar variable.
  REAL :: rhostr(nx,ny,nz)     ! Base state air density times j3 (kg/m**3)
  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient for
                               ! momentum. ( m**2/s )
  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)
  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.

  REAL :: j1    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! d( zp )/d( z ).
!
  REAL :: h1    (nx,ny,nz)     ! Turbulent flux in x direction
  REAL :: h2    (nx,ny,nz)     ! Turbulent flux in y direction
  REAL :: h3    (nx,ny,nz)     ! Turbulent flux in z direction

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
  REAL :: tem,deltah2
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!-----------------------------------------------------------------------


!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  deltah2 = dx*dy
!
!-----------------------------------------------------------------------
!
!  Compute turbulent flux in x direction (H1)
!
!  H1 = avgx(rhobar * Km / j3) * (difx0(j3 * s ) +
!       difz(j1 * avgsw(avgx( s ))))
!
!-----------------------------------------------------------------------
!


!-----------------------------------------------------------------------
!
!  Calculate the first term in the turbulent flux (H1)
!  difx0(j3 * s)
!
!-----------------------------------------------------------------------

  CALL aamult0(j3,s,                                                     &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1,tem2)

  onvf = 1
  CALL difx0(tem2,onvf,                                                  &
            nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1,dx,h1)

!-----------------------------------------------------------------------
!
!  Calculate the second term in the turbulent flux (H1)
!  difz(j1 * avgsw(avgx(s))). When ternopt=0, this term is zero.
!
!-----------------------------------------------------------------------

  IF( ternopt /= 0 ) THEN

    onvf = 1
    CALL avgx0(s,onvf,                                                   &
              nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1, tem2)

    CALL avgsw0(tem2,nx,ny,nz, 2,nx-1, 1,ny-1, tem1)

    CALL aamult0(j1,tem1,                                                &
                nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz,tem1)

    onvf = 0
    CALL difz0(tem1,onvf,                                                &
              nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1,dz,tem2)
!
!-----------------------------------------------------------------------
!
!  Combine H1 + TEM2
!
!-----------------------------------------------------------------------
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=2,nx-1
          h1(i,j,k)=h1(i,j,k) + tem2(i,j,k)
        END DO
      END DO
    END DO

  END IF

!-----------------------------------------------------------------------
!
!  Calculate the coefficient for H1
!  avgx(rhobar*kh/j3) = avgx(rhostr*km/prantl/(j3*j3))
!
!  Note: In TKE and anisotropic case, Prandtl number is set 1./3.
!
!-----------------------------------------------------------------------
!

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem=1.0+2.0*lendel(i,j,k)
        tem2(i,j,k) = rhostr(i,j,k)*km(i,j,k)*tem/(j3(i,j,k)*j3(i,j,k))
      END DO
    END DO
  END DO

  onvf = 1
  CALL avgx0(tem2,onvf,                                                  &
            nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1, tem1)

!-----------------------------------------------------------------------
!
!  Multipy tem1 * H1 to obtain the final H1 term.
!
!-----------------------------------------------------------------------

  CALL aamult0(tem1,h1,                                                  &
              nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1,h1)


!
!-----------------------------------------------------------------------
!
!  Calculate turbulent flux in Y direction (H2)
!
!  H2= avgy(rhobar*kh/j3)*(dify(j3 * s) + difz(j2 * avgsw(avgy(s))))
!
!-----------------------------------------------------------------------
!


!-----------------------------------------------------------------------
!
!  Calculate the first term in the turbulent flux (H2)
!  dify(j3 * s)
!
!-----------------------------------------------------------------------

  CALL aamult0(j3,s,                                                     &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1,tem2)

  onvf = 1
  CALL dify0(tem2,onvf,                                                  &
            nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1,dy,h2)

!-----------------------------------------------------------------------
!
!  Calculate the second term in the turbulent flux (H2)
!  difz(j2 * avgsw(avgy(s))). When ternopt=0, this term is zero.
!
!-----------------------------------------------------------------------

  IF( ternopt /= 0 ) THEN

    onvf = 1
    CALL avgy0(s,onvf,                                                   &
              nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1, tem2)

    CALL avgsw0(tem2,nx,ny,nz, 1,nx-1, 2,ny-1, tem1)

    CALL aamult0(j2,tem1,                                                &
                nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz,tem1)

    onvf = 0
    CALL difz0(tem1,onvf,                                                &
              nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1,dz,tem2)
!
!-----------------------------------------------------------------------
!
!  Combine H2 + tem2
!
!-----------------------------------------------------------------------
!
    DO k=1,nz-1
      DO j=2,ny-1
        DO i=1,nx-1
          h2(i,j,k)=h2(i,j,k) + tem2(i,j,k)
        END DO
      END DO
    END DO

  END IF


!-----------------------------------------------------------------------
!
!  Calculate the coefficient for H2
!  avgy(rhobar*kh/j3) = avgy(rhostr*km/prantl/(j3*j3))
!
!  Note: In TKE and anisotropic case, Prandtl number is set 1./3.
!
!-----------------------------------------------------------------------


  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem=1.0+2.0*lendel(i,j,k)
        tem2(i,j,k) = rhostr(i,j,k)*km(i,j,k)*tem/(j3(i,j,k)*j3(i,j,k))
      END DO
    END DO
  END DO

  onvf = 1
  CALL avgy0(tem2,onvf,                                                  &
            nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1,tem1)
!
!-----------------------------------------------------------------------
!
!  Multipy tem1 * H2 and set = H2 for the final H2 term
!
!-----------------------------------------------------------------------
!
  CALL aamult0(tem1,h2,                                                  &
              nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1,h2)

!
!-----------------------------------------------------------------------
!
!  Compute turbulent flux in z direction (H3).
!  H3= avgz (rhobar*kh/j3) * difz(s)
!
!  First calculate H3 = difz(s)
!
!-----------------------------------------------------------------------
!

  onvf = 1
  CALL difz0(s,onvf,                                                     &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1,dz,h3)
!
!-----------------------------------------------------------------------
!
!  Calculate the coefficient for H3
!
!  avgz(rhobar*kh/j3) = avgz(rhostr*km*(deltav/deltah)**2
!                       /prantl/(j3*j3))
!
!-----------------------------------------------------------------------
!
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem=1.0+2.0*lendel(i,j,k)
        IF(trbisotp == 0) tem=tem*(zp(i,j,k+1)-zp(i,j,k))**2/deltah2
        tem2(i,j,k) = rhostr(i,j,k)*km(i,j,k)*tem/(j3(i,j,k)*j3(i,j,k))
      END DO
    END DO
  END DO

  onvf = 1
  CALL avgz0(tem2,onvf,                                                  &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1,tem1)
!
!-----------------------------------------------------------------------
!
!  Multipy tem1 * H3 and save the result in H3.
!
!-----------------------------------------------------------------------
!
  CALL aamult0(tem1,h3,                                                  &
              nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1,h3)

  RETURN
END SUBROUTINE trbflxs0
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE SMIXTRM0                   ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE smixtrm0(nx,ny,nz,h1,h2,h3,rhostr,x,y,z,zp, j1,j2,j3,         &
           smix,                                                        &
           tem1,tem2,tem3)
!
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the turbulent mixing term for a scalar from the
!  turbulent fluxes h1, h2 and h3.
!
!  smix = difx0(avgx0(j3) * h1) + dify0(avgy0(j3) * h2) +
!         difz0(h3 + avgx0(avgz0(h1) * j1) + avgy0(avgz0(h2) * j2))
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue & Dan Weber
!  6/16/93
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    h1       Turbulent heat flux, a local array.
!    h2       Turbulent heat flux, a local array.
!    h3       Turbulent heat flux, a local array.
!
!    rhostr   Base state air density times j3 (kg/m**3)
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space (m)
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    smix     The mixing term for a scalar calculated from its turbulent fluxes
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array
!    tem2     Temporary work array
!    tem3     Temporary work array
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: h1    (nx,ny,nz)     ! Turbulent flux in x direction
  REAL :: h2    (nx,ny,nz)     ! Turbulent flux in y direction
  REAL :: h3    (nx,ny,nz)     ! Turbulent flux in z direction

  REAL :: rhostr(nx,ny,nz)     ! Base state air density times j3 (kg/m**3)
                               ! momentum. ( m**2/s )

  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.

  REAL :: j1    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! d( zp )/d( z ).
!
  REAL :: smix  (nx,ny,nz)     ! Turbulent mixing for a scalar
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!-----------------------------------------------------------------------

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


!-----------------------------------------------------------------------
!
!  Calculate the turbulent mixing term for a scalar as the divergence
!  of its turbluent fluxes:
!
!  smix = difx0(avgx0(j3) * h1) + dify0(avgy0(j3) * h2) + difz0(h3 +
!         avgx0(avgz0(h1) * j1) + avgy0(avgz0(h2) * j2))
!
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
!
!  Calculate term difx0(avgx0(j3) * h1)
!
!-----------------------------------------------------------------------

  tem1 = 0.
  onvf = 1
  CALL avgx0(j3,onvf,                                                    &
            nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1,tem1)

  tem2 = 0.
  CALL aamult0(tem1,h1,                                                  &
              nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1,tem2)

  onvf = 0
  CALL difx0(tem2,onvf,                                                  &
            nx,ny,nz, 2,nx-2, 1,ny-1, 1,nz-1,dx,smix)
!
!  At this point, SMIX = difx0(avgx0(j3) * h1)
!

!-----------------------------------------------------------------------
!
!  Calculate the second term in the smix relation.
!  dify(avgy(j3) * h2)
!
!-----------------------------------------------------------------------

  tem1 = 0.
  onvf = 1
  CALL avgy0(j3,onvf,                                                    &
            nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1,tem1)

  tem2 = 0.
  CALL aamult0(tem1,h2,                                                  &
              nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1,tem2)

  tem3 = 0.
  onvf = 0
  CALL dify0(tem2,onvf,                                                  &
            nx,ny,nz, 1,nx-1, 2,ny-2, 1,nz-1,dy,tem3)

  DO k=1,nz-1
    DO j=2,ny-2
      DO i=2,nx-2
        smix(i,j,k)=tem3(i,j,k) + smix(i,j,k)
      END DO
    END DO
  END DO

!
!  At this point, smix = difx0(avgx0(j3) * h1) + dify0(avgy0(j3) * h2)
!

!-----------------------------------------------------------------------
!
!  Calculate the third term in the smix relation.
!  difz(h3 + avgx(avgz(h1) * j1) + avgy(avgz(h2) * j2))
!
!-----------------------------------------------------------------------


  IF( ternopt /= 0 ) THEN

!-----------------------------------------------------------------------
!
!  Calculate the third part of the third term.
!  avgy(avgz(h2) * j2))
!
!-----------------------------------------------------------------------

    tem1 = 0.
    onvf = 1
    CALL avgz0(h2,onvf,                                                  &
              nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1,tem1)

    tem2 = 0.
    CALL aamult0(tem1,j2,                                                &
                nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1,tem2)

    onvf = 0
    CALL avgy0(tem2,onvf,                                                &
              nx,ny,nz, 1,nx-1, 2,ny-2, 2,nz-1,tem3)

!-----------------------------------------------------------------------
!
!  Calculate the second part of the third term....
!  avgx(avgz(h1) * j1)
!
!-----------------------------------------------------------------------

    tem1 = 0.
    onvf = 1
    CALL avgz0(h1,onvf,                                                  &
              nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1,tem1)

    CALL aamult0(tem1,j1,                                                &
                nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1,tem1)

    tem2 = 0.
    onvf = 0
    CALL avgx0(tem1,onvf,                                                &
              nx,ny,nz, 2,nx-2, 1,ny-1, 2,nz-1,tem2)
!
!-----------------------------------------------------------------------
!
!  Combine tem2 + tem3 + H3 in the third term.
!
!-----------------------------------------------------------------------
!

    DO k=2,nz-1
      DO j=2,ny-2
        DO i=2,nx-2
          tem2(i,j,k)=tem2(i,j,k) + tem3(i,j,k) + h3(i,j,k)
        END DO
      END DO
    END DO

  ELSE  ! No terrain, tem2=tem3=0.0

    DO k=2,nz-1
      DO j=2,ny-2
        DO i=2,nx-2
          tem2(i,j,k)= h3(i,j,k)
        END DO
      END DO
    END DO

  END IF

  tem3 = 0.
  onvf = 0
  CALL difz0(tem2,onvf,                                                  &
            nx,ny,nz, 2,nx-2, 2,ny-2, 2,nz-2,dz,tem3)

!
!-----------------------------------------------------------------------
!
!  Combine TEM3 + SMIX to give the final SMIX.
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-2
    DO j=2,ny-2
      DO i=2,nx-2
        smix(i,j,k)= tem3(i,j,k) + smix(i,j,k)
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE smixtrm0

!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE UMIXTRM0                   ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE umixtrm0(nx,ny,nz,j1,j2,j3,tau11,tau12,tau13,                 &
           umix,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the turbulent mixing term in u equation as the divergence
!  of the turbulent momentum fluxes.
!
!  j3 * DIV U = difx (j3 * tau11) + dify (avgx (avgsv(j3)) * tau12)
!               + difz (tau13 + (j1 * avgz (avgx (tau11))) + (avgy
!               ((avgx (j2)) * (avgz (tau12))))
!
!-----------------------------------------------------------------------
!
!  AUTHOR: D. Weber & M. Xue
!  6/29/92.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!    tau11    Deformation tensor component (1/s)
!    tau12    Deformation tensor component (1/s)
!    tau13    Deformation tensor component (1/s)
!
!
!  OUTPUT:
!
!    umix     The divergence of u mixing term
!
!  WORKING ARRAYS:
!
!    tem1     Temporary working array.
!    tem2     Temporary working array.
!    tem3     Temporary working array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    working arrays may be used for other purposes therefore their
!    contents overwritten. Please examine the usage of working arrays
!    before you alter the code.)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! - d(zp)/dx
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! - d(zp)/dy
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               !   d(zp)/dz
!
  REAL :: tau11 (nx,ny,nz)     ! Deformation stress component.
  REAL :: tau12 (nx,ny,nz)     ! Deformation stress component.
  REAL :: tau13 (nx,ny,nz)     ! Deformation stress component.
!
  REAL :: umix  (nx,ny,nz)     ! Divergence of u mixing term
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary working array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary working array.
  REAL :: tem3  (nx,ny,nz)     ! Temporary working array.

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global constants that control model
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

umix(:,:,:) = 0.0
tem3=0.0
!-----------------------------------------------------------------------
!
!  NOTE: In order to reduce memory usage, order dependance has been
!        introduced in the calculation of the third term in the
!        U mixing divergence term, which reduces the possible term-
!        wise parallelism.  This is due to the constraint that only two
!        temporary arrays being avaliable for passage into this subroutine.
!
!
!-----------------------------------------------------------------------

  IF( ternopt /= 0) THEN

!-----------------------------------------------------------------------
!
!  Calculate the third part of the third term first.
!  avgy (avgx (j2) * avgz (tau12)). This term is zero when ternopt=0.
!
!-----------------------------------------------------------------------
IF(1==0) THEN
    tem1 = 0.0
    onvf = 1
    CALL avgx0(j2,onvf,                                                  &
              nx,ny,nz, 2,nx-1, 1,ny, 1,nz, tem1)

    tem2 = 0.0
    onvf = 1
    CALL avgz0(tau12,onvf,                                               &
              nx,ny,nz, 1,nx, 1,ny, 2,nz-1, tem2)

    tem3 = 0.0
    CALL aamult0(tem1,tem2,                                              &
                nx,ny,nz, 2,nx-1, 1,ny, 2,nz-1, tem3)

    onvf = 0
    CALL avgy0(tem3,onvf,                                                &
              nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1, umix)

!
!  At this point, UMIX =  avgy (avgx (j2) * avgz (tau12)).
!

!-----------------------------------------------------------------------
!
!  Calculate the second part of the third term.
!  j1 * (avgz (avgx (tau11))). This term is zero when ternopt=0.
!
!-----------------------------------------------------------------------
    tem1 = 0.0
    onvf = 1
    CALL avgx0(tau11,onvf,                                               &
              nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1, tem1)

    tem2 = 0.0
    onvf = 1
    CALL avgz0(tem1,onvf,                                                &
              nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1, tem2)

    tem3 = 0.0
    CALL aamult0(tem2,j1,                                                &
                nx,ny,nz,2,nx-1, 1,ny-1, 2,nz-1,tem3)
END IF
!-------------------------------------------------------------------
!
!  Combine the three terms of the third Umix divergence term.
!
!-------------------------------------------------------------------
    tem1 = 0.
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=2,nx-1
          tem1(i,j,k)=tau13(i,j,k) + tem3(i,j,k) + umix(i,j,k)
        END DO
      END DO
    END DO

  ELSE   ! When ternopt=0, tem2=umix=0.0
    tem1 = 0.
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=2,nx-1
          tem1(i,j,k)=tau13(i,j,k)
        END DO
      END DO
    END DO

  END IF

  onvf = 0
  CALL difz0(tem1, onvf,                                                 &
            nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, dz, umix)
!
!  umix = third term of the U mixing divergence relation.

!-----------------------------------------------------------------------
!
!  Calculate the first term of the u mixing divergence term.
!  difx0(j3 * Tau11)
!
!-----------------------------------------------------------------------
  tem1 = 0.0
  CALL aamult0(j3,tau11,                                                 &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem1)

  tem2 = 0.0
  onvf = 1
  CALL difx0(tem1, onvf,                                                 &
            nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1, dx, tem2)

!-----------------------------------------------------------------------
!
!  Combine the first and third terms of u mixing Divergence (tem2 + umix)
!
!-----------------------------------------------------------------------

  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        umix(i,j,k)=tem2(i,j,k) + umix(i,j,k)
      END DO
    END DO
  END DO
!
!  At this point, umix = the first and third terms of the umix relation.
!

!-----------------------------------------------------------------------
!
!  Calculate the second term of the u mixing divergence term.
!  dify(avgx(avgsv(j3)) * Tau12)
!
!-----------------------------------------------------------------------
  tem1 = 0.0
  CALL avgsv0(j3,nx,ny,nz, 1,nx-1, 1,nz-1, tem1)

  tem2 = 0.0
  onvf = 1
  CALL avgx0(tem1,onvf,                                                  &
             nx,ny,nz, 2,nx-1, 1,ny, 1,nz-1, tem2)


  tem3= 0.0
  CALL aamult0(tem2,tau12,                                               &
              nx,ny,nz, 2,nx-1, 1,ny, 1,nz-1, tem3)

  tem1 = 0.0
  onvf = 0
  CALL dify0(tem3, onvf,                                                 &
            nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1, dy, tem1)

!-------------------------------------------------------------------
!
!  Combine the second term tem1 and umix to get the final umix
!  divergence.
!
!-------------------------------------------------------------------

  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        umix(i,j,k)=tem1(i,j,k) + umix(i,j,k)
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE umixtrm0


!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE VMIXTRM0                   ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE vmixtrm0(nx,ny,nz,j1,j2,j3,tau12,tau22,tau23,                 &
           vmix,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the turbulent mixing term in v equation as the
!  divergence of the turbulent momentum fluxes.
!
!  j3 * DIV V = difx0(avgy0(avgsu0(j3)) * tau12) + dify0(j3 * tau22)
!               + difz0(tau23 + avgx0(avgy0(j1) * avgz0(tau12)) +
!               (j2 * (avgz0(avgy0(tau22)))))
!
!-----------------------------------------------------------------------
!
!  AUTHOR: D. Weber & M. Xue
!  6/29/92.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!    tau12    Deformation tensor component (1/s)
!    tau22    Deformation tensor component (1/s)
!    tau23    Deformation tensor component (1/s)
!
!
!  OUTPUT:
!
!    vmix     The divergence of v mixing term
!
!  WORKING ARRAYS:
!
!    tem1     Temporary working array.
!    tem2     Temporary working array.
!    tem3     Temporary working array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    working arrays may be used for other purposes therefore their
!    contents overwritten. Please examine the usage of working arrays
!    before you alter the code.)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! - d(zp)/dx
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! - d(zp)/dy
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               !   d(zp)/dz
!
  REAL :: tau12 (nx,ny,nz)     ! Deformation stress component.
  REAL :: tau22 (nx,ny,nz)     ! Deformation stress component.
  REAL :: tau23 (nx,ny,nz)     ! Deformation stress component.
!
  REAL :: vmix  (nx,ny,nz)     ! Divergence of v mixing term
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary working array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary working array.
  REAL :: tem3  (nx,ny,nz)     ! Temporary working array.

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global constants that control model
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!

!-----------------------------------------------------------------------
!
!  Calculate the third term of the V mixing divergence term first.
!
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!
!  NOTE: In order to reduce memory usage, order dependance has been
!        introduced in the calculation of the third term in the
!        v mixing divergence term, which reduces the possible term-
!        wise parallelism.  This is due to the constraint that only two
!        temporary arrays being avaliable for passage into this subroutine.
!
!
!-----------------------------------------------------------------------
  vmix(:,:,:) = 0.
  tem2=0.0

  IF( ternopt /= 0) THEN

!-----------------------------------------------------------------------
!
!  Calculate the third part of the third term first.
!  j2 * avgz(avgy(tau22)). This term is zero when ternopt=0.
!
!-----------------------------------------------------------------------
IF(1==0) THEN
    tem1 = 0.0
    onvf = 1
    CALL avgy0(tau22,onvf,                                               &
              nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1, tem1)

    tem2 = 0.0
    onvf = 1
    CALL avgz0(tem1,onvf,                                                &
              nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, tem2)

    CALL aamult0(tem2,j2,                                                &
                nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, vmix)

!
!  At this point, VMIX = j2 * avgz(avgy(tau22))
!

!-----------------------------------------------------------------------
!
!  Calculate the second part of the third term.
!  avgx(avgy(j1) * avgz(Tau12)). This term is zero when ternopt=0.
!
!-----------------------------------------------------------------------

    tem1 = 0.0
    onvf = 1
    CALL avgy0(j1,onvf,                                                  &
              nx,ny,nz, 1,nx, 2,ny-1, 1,nz, tem1)

    tem2 = 0.0
    onvf = 1
    CALL avgz0(tau12,onvf,                                               &
              nx,ny,nz, 1,nx, 1,ny, 2,nz-1, tem2)

    tem3 = 0.0
    CALL aamult0(tem1,tem2,                                              &
                nx,ny,nz, 1,nx, 2,ny-1, 2,nz-1, tem3)

    tem2 = 0.0
    onvf = 0
    CALL avgx0(tem3,onvf,                                                &
              nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, tem2)
END IF
!-----------------------------------------------------------------------
!
!  Combine the three terms of the third Vmix divergence term
!
!-----------------------------------------------------------------------

    DO k=2,nz-1
      DO j=2,ny-1
        DO i=1,nx-1
          tem1(i,j,k)=tau23(i,j,k) + tem2(i,j,k) + vmix(i,j,k)
        END DO
      END DO
    END DO

  ELSE    ! When ternopt=0, tem2=vmix=0.0

    DO k=2,nz-1
      DO j=2,ny-1
        DO i=1,nx-1
          tem1(i,j,k)=tau23(i,j,k)
        END DO
      END DO
    END DO

  END IF

  onvf = 0
  CALL difz0(tem1, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, dz, vmix)

!
!  At this point, vmix = difz (tau23 + avgx(avgy(j1) * avgz (tau12))) +
!                             (j2 * (avgz (avgy(tau22))))

!-----------------------------------------------------------------------
!
!  Calculate the first term of the V mixing divergence term.
!  difx0(avgy0(avgsu0(j3)) * Tau12)
!
!-----------------------------------------------------------------------

  tem1 = 0.0
  CALL avgsu0(j3,nx,ny,nz, 1,ny-1, 1,nz-1, tem1)

  tem2= 0.0
  onvf = 1
  CALL avgy0(tem1,onvf,                                                  &
            nx,ny,nz, 1,nx, 2,ny-1, 1,nz-1, tem2)

  tem3 = 0.0
  CALL aamult0(tem2,tau12,                                               &
              nx,ny,nz, 1,nx, 2,ny-1, 1,nz-1, tem3)

  tem1 = 0.0
  onvf = 0
  CALL difx0(tem3, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1, dx, tem1)

!-----------------------------------------------------------------------
!
!  Combine the first and third terms of vmix (tem1 + vmix)
!
!-----------------------------------------------------------------------

  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1

        vmix(i,j,k)=tem1(i,j,k) + vmix(i,j,k)

      END DO
    END DO
  END DO
!
!  At this point, vmix = first and third terms
!

!-----------------------------------------------------------------------
!
!  Calculate the second term of the V mixing divergence term.
!  dify(j3 * Tau22)
!
!-----------------------------------------------------------------------

  tem1 = 0.
  CALL aamult0(j3,tau22,                                                 &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem1)

  tem2 = 0.
  onvf = 1
  CALL dify0(tem1, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1, dy, tem2)

!-------------------------------------------------------------------
!
!  Combine the second term TEM2 and VMIX to get the final v
!  mixing divergence
!
!-------------------------------------------------------------------

  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        vmix(i,j,k)=tem2(i,j,k) + vmix(i,j,k)
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE vmixtrm0


!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE WMIXTRM0                   ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE wmixtrm0(nx,ny,nz,j1,j2,j3,tau13,tau23,tau33,                 &
           wmix,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the turbulent mixing term in w equation as the
!  divergence of the turbulent momentum fluxes.
!
!  j3 * DIV W = difx0(avgz0(avgsu0(j3)) * tau13) + dify(avgz0(avgsv0(j3))
!               * tau23) + difz0(tau33 + avgx0(avgz0(j1) * avgz0(tau13))
!               + (avgy0(avgz0(j2) * avgz0(tau23))))
!
!-----------------------------------------------------------------------
!
!  AUTHOR: D. Weber & M. Xue
!  6/29/92.
!
!  10/17/93  (D. Weber)
!  Vertical loop bounds in loop 300 and 305 corrected.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!    tau13    Deformation tensor component (1/s)
!    tau23    Deformation tensor component (1/s)
!    tau33    Deformation tensor component (1/s)
!
!
!  OUTPUT:
!
!    wmix     The divergence of w mixing term
!
!  WORKING ARRAYS:
!
!    tem1     Temporary working array.
!    tem2     Temporary working array.
!    tem3     Temporary working array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    working arrays may be used for other purposes therefore their
!    contents overwritten. Please examine the usage of working arrays
!    before you alter the code.)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! - d(zp)/dx
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! - d(zp)/dy
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               !   d(zp)/dz
!
  REAL :: tau13 (nx,ny,nz)     ! Deformation stress component.
  REAL :: tau23 (nx,ny,nz)     ! Deformation stress component.
  REAL :: tau33 (nx,ny,nz)     ! Deformation stress component.
!
  REAL :: wmix  (nx,ny,nz)     ! Divergence of w mixing term
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary working array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary working array.
  REAL :: tem3  (nx,ny,nz)     ! Temporary working array.

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global constants that control model
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    wmix(:,:,:)= 0.
    tem2=0.0
!-----------------------------------------------------------------------
!
!  NOTE: In order to reduce memory usage, order dependance has been
!        introduced in the calculation of the third term in the
!        w mixing divergence term, which reduces the possible term-
!        wise parallelism.  This is due to the constraint that only two
!        temporary arrays being avaliable for passage into this subroutine.
!
!
!-----------------------------------------------------------------------
!
  IF( ternopt /= 0 ) THEN

!-----------------------------------------------------------------------
!
!  Calculate the third part of the third term first.
!  avgy(avgz(j2) * avgz(tau23)). This term is zero when ternopt=0.
!
!-----------------------------------------------------------------------
IF(1==0) THEN
    tem1 = 0.
    onvf = 0
    CALL avgz0(j2,onvf,                                                  &
              nx,ny,nz, 1,nx-1, 1,ny, 1,nz-1, tem1)

    tem2 = 0.
    onvf = 0
    CALL avgz0(tau23,onvf,                                               &
              nx,ny,nz, 1,nx-1, 1,ny, 1,nz-1, tem2)

    tem3 = 0.
    CALL aamult0(tem1,tem2,                                              &
                nx,ny,nz, 1,nx-1, 1,ny, 1,nz-1, tem3)
    onvf = 0
    CALL avgy0(tem3,onvf,                                                &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, wmix)
!
!  At this point, wmix = avgy(avgz(j2) * avgz(tau23)).
!
!-----------------------------------------------------------------------
!
!  Calculate the second part of the third term.
!  avgx(avgz(j1) * avgz(tau13)). This term is zero when ternopt=0.
!
!-----------------------------------------------------------------------

    tem1 = 0.
    onvf = 0
    CALL avgz0(j1,onvf,                                                  &
              nx,ny,nz, 1,nx, 1,ny-1, 1,nz-1, tem1)

    tem2 = 0.
    onvf = 0
    CALL avgz0(tau13,onvf,                                               &
              nx,ny,nz, 1,nx, 1,ny-1, 1,nz-1, tem2)

    tem3 = 0.
    CALL aamult0(tem1,tem2,                                              &
                nx,ny,nz, 1,nx, 1,ny-1, 1,nz-1, tem3)

    tem2 = 0.
    onvf = 0
    CALL avgx0(tem3,onvf,                                                &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem2)
 END IF
!-----------------------------------------------------------------------
!
!  Combine the three terms of the third w mix divergence term
!
!-----------------------------------------------------------------------

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem1(i,j,k)=tau33(i,j,k) + tem2(i,j,k) + wmix(i,j,k)
        END DO
      END DO
    END DO

  ELSE    ! When ternopt=0, term2=wmix=0.0

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem1(i,j,k)=tau33(i,j,k)
        END DO
      END DO
    END DO

  END IF

  onvf = 1
  CALL difz0(tem1, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, dz, wmix)

!
!  At this point, wmix = the third term of the wmix divergence relation.
!

!-----------------------------------------------------------------------
!
!  Calculate the first term of the W mixing divergence term.
!  difx0(avgz0(avgsu0(j3)) * Tau13)
!
!-----------------------------------------------------------------------
  tem1 = 0.
  CALL avgsu0(j3,nx,ny,nz,1,ny-1,1,nz-1,tem1)

  tem2 = 0.
  onvf = 1
  CALL avgz0(tem1,onvf,                                                  &
            nx,ny,nz, 1,nx, 1,ny-1, 2,nz-1, tem2)

  tem3 = 0.
  CALL aamult0(tem2,tau13,                                               &
              nx,ny,nz, 1,nx, 1,ny-1, 2,nz-1, tem3)

  tem1 = 0.
  onvf = 0
  CALL difx0(tem3, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, dx, tem1)

!
!  Tem1 = first term of W mixing divergence term
!

!-----------------------------------------------------------------------
!
!  Combine the first and third terms of W mixing Divergence (tem1 + vmix)
!
!-----------------------------------------------------------------------

  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        wmix(i,j,k)=tem1(i,j,k) + wmix(i,j,k)
      END DO
    END DO
  END DO

!
!  At this point, wmix = first and third terms.
!

!-----------------------------------------------------------------------
!
!  Calculate the second term of the W mixing divergence term.
!  dify(avgz(avgsv(j3)) * Tau23)
!
!-----------------------------------------------------------------------

  tem1=0.0; tem2=0.0
  CALL avgsv0(j3,nx,ny,nz,1,nx-1,1,nz-1,tem1)

  onvf = 1
  CALL avgz0(tem1,onvf,                                                  &
            nx,ny,nz, 1,nx-1, 1,ny, 2,nz-1, tem2)

  tem3 = 0.
  CALL aamult0(tem2,tau23,                                               &
            nx,ny,nz, 1,nx-1, 1,ny, 2,nz-1, tem3)

  tem1 = 0.
  onvf = 0
  CALL dify0(tem3, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, dy, tem1)

!-----------------------------------------------------------------------
!
!  Combine the second term tem1 and wmix to get the final w
!  mixing divergence.
!
!-----------------------------------------------------------------------

  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        wmix(i,j,k) = tem1(i,j,k) + wmix(i,j,k)
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE wmixtrm0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE SATMR0                     ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE satmr0(nx,ny,nz, p,t, qvs)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the saturation water vapor specific humidity,
!  qvs, from pressure and temperature using Teten's formula.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  2/29/1992.
!
!  MODIFICATION HISTORY:
!
!  5/02/92 (M. Xue)
!  Added full documentation.
!
!  5/28/92 (K. Brewster)
!  Further facelift.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    p        Full air pressure in Pascal.
!    t        Temperature (K)
!
!  OUTPUT:
!
!    qvs      Saturation water vapor specific humidity (kg/kg)
!
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  REAL:: aw,bw                ! Saturation specific humidity parameters
  PARAMETER( aw = 17.27, bw = 35.86 )
  INTEGER :: nx,ny,nz         ! Number of grid points in 3 directions
!
  REAL :: p    (nx,ny,nz)     ! Pressure (Pascal)
  REAL :: t    (nx,ny,nz)     ! Temperature (Kelvin)
  REAL :: qvs  (nx,ny,nz)     ! Saturation specific humidity (kg/kg)
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!   Calculate qvs, the saturation specific humidity, using
!   Teten's formula:
!
!   qvs = (380./p) * exp(aw * (t-273.15)/(t-bw))
!
!   where parameters aw and bw are given in globcst.inc.
!
!-----------------------------------------------------------------------

  DO k = 1,nz-1
    DO j = 1,ny-1
      DO i = 1,nx-1

        qvs(i,j,k) = (380.0/ p(i,j,k)) *                                &
            EXP( aw  * (t(i,j,k) - 273.15)/(t(i,j,k) - bw) )

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE satmr0
