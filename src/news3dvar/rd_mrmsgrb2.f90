!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE RDNMCGRB2                  ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE rd_mrmsgrb2(nx_ext,ny_ext,nz_ext,gribfile,grbflen, dtstart,  &
                       ix1,ix2,jy1,jy2,latsw,lonsw,dx_grb,dy_grb,       &
                       maxvar,varids, var2dindx, var2dlvl,              &
                       var_grb2d, gribtime, lvldbg, iret)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  This subroutine is to read and select given variables with GRIB
!  grid ID and level type from NMC GRIB files.
!
!  The decoder of GRIB2 is from NCEP.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  08/01/2006
!
!  MODIFICATIONS:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    gribfile      GRIB file name
!    grbflen       Length of GRIB file name
!    gribtime      Time of GRIB data
!
!  OUTPUT:
!
!    iproj_grb     Map projection number of GRIB data
!    trlon_grb     True longitude of GRIB data (degrees E)
!    latnot_grb(2) True latitude(s) of GRIB data (degrees N)
!    swlat_grb     Latitude  of first grid point at southwest conner
!    swlon_grb     Longitude of first grid point at southwest conner
!
!    dx_grb        x-direction grid length
!    dy_grb        y-direction grid length
!
!    iret          Return flag
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  USE GRIB_MOD

  IMPLICIT NONE

  INTEGER, INTENT(IN) :: nx_ext,ny_ext,nz_ext

  INTEGER, INTENT(IN) :: grbflen, dtstart

  CHARACTER(LEN=*), INTENT(IN) :: gribfile

  INTEGER, INTENT(IN)  :: ix1, ix2, jy1, jy2

  REAL,    INTENT(OUT) :: latsw        ! Latitude  of South West corner point
  REAL,    INTENT(OUT) :: lonsw        ! Longitude of South West corner point
  REAL,    INTENT(OUT) :: dx_grb       ! x-direction increment or grid length
  REAL,    INTENT(OUT) :: dy_grb       ! y-direction increment or grid length

  INTEGER, INTENT(IN)  :: maxvar
  INTEGER, INTENT(IN)  :: varids(4,maxvar)
  INTEGER, INTENT(IN)  :: var2dindx(maxvar)
  REAL,    INTENT(IN)  :: var2dlvl(maxvar)
  REAL,    INTENT(OUT) :: var_grb2d(nx_ext,ny_ext,maxvar)

  CHARACTER(LEN=19), INTENT(OUT) :: gribtime

  INTEGER, INTENT(IN)  :: lvldbg

  INTEGER, INTENT(OUT) :: iret         ! Return flag
!
!-----------------------------------------------------------------------
!
!  Temporary arrays to read GRIB file
!
!-----------------------------------------------------------------------
!
  INTEGER :: grbunit

  INTEGER :: iproj_grb    ! Map projection indicator
                          ! Already converted to ARPS map definitions
  INTEGER :: nx_grb       ! Number of points along x-axis
  INTEGER :: ny_grb       ! Number of points along y-axis

  REAL    :: lattru1      ! Latitude (1st) at which projection is true
  REAL    :: lattru2      ! Latitude (2nd) at which projection is true
  REAL    :: lontrue      ! Longitude      at which projection is true
  INTEGER :: uvearth      ! = 0, Resolved u and v components of vector
                          !      quantities relative to easterly and northerly directions
                          ! = 1, Resolved u and v components of vector quantities
                          !      relative to the defined grid in the direction of
                          !      increasing x and y (or i and j) coordinates, respectively

  CHARACTER(LEN=1), ALLOCATABLE :: cgrib(:)

  INTEGER, PARAMETER :: maxlen = 32000

  TYPE(gribfield) :: gfld

!
!-----------------------------------------------------------------------
!
!  Misc internal variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,m,n,ij
  INTEGER :: ii, jj, ioff, joff
  INTEGER :: iyr,  imo,  iday,  ihr,  imn, sec

  LOGICAL             :: verbose = .FALSE.
  CHARACTER(LEN=12), PARAMETER  :: cmd     = 'rd_mrmsgrb2: '

  INTEGER :: iseek, icount, itot
  INTEGER :: lgrib, lgribin, lskip
  INTEGER :: currlen
  INTEGER :: listsec0(3), listsec1(13)
  INTEGER :: numfields, numlocal, maxlocal

  INTEGER :: levelin, levelout

  INTEGER :: iscan, jscan, ijscan
  INTEGER :: ibgn, iinc, iend, jbgn, jinc, jend
  INTEGER :: imin, jmin

  INTEGER :: itemp
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF (lvldbg > 90) verbose = .TRUE.

!-----------------------------------------------------------------------
!
! Date and time expected
!
!-----------------------------------------------------------------------

  sec = 0
  READ (gribfile(dtstart:),'(i4,2i2,1x,2i2)') iyr,imo,iday,ihr,imn
  WRITE(gribtime,'(I4.4,5(a,I2.2))') iyr,'-',imo,'-',iday,'_',ihr,':',imn,':',sec
!
!-----------------------------------------------------------------------
!
!  Open the GRIB file
!
!-----------------------------------------------------------------------
!
  WRITE(6,'(1x,8A)') cmd, ' Opening ', gribfile(1:grbflen)

  CALL getunit( grbunit )

  CALL baopenr(grbunit,gribfile(1:grbflen),iret)
  IF  ( iret /= 0 )  THEN
    WRITE(6,'(a,a,/,a,i4)')                                           &
        'ERROR in opening file ',gribfile(1:grbflen),                 &
        'function baopenr return status: ',iret
    GOTO 9999
  END IF

!-----------------------------------------------------------------------
!
! Loop over all records
!
!-----------------------------------------------------------------------

  itot    = 0     ! total field counter
  icount  = 0     ! total GRIB2 message counter
  iseek   = 0
  currlen = 0

  ! DO n = 1,maxvar
  !   WRITE(0,*) varids(:,n)
  ! END DO
  ! STOP

  iret = 0
  DO WHILE  (iret == 0)

      CALL skgb(grbunit,iseek,maxlen,lskip,lgrib)
      IF (lgrib == 0) EXIT

      IF (lgrib > currlen) THEN      ! Extend buffer size as needed
        IF (ALLOCATED(cgrib)) DEALLOCATE(cgrib)
        ALLOCATE(cgrib(lgrib), STAT = iret)
        currlen = lgrib
      END IF

      CALL baread(grbunit,lskip,lgrib,lgribin,cgrib)   ! read a GRIB message
      IF (lgrib /= lgribin) THEN
        WRITE(6,'(2(a,I6))')                                              &
           'ERROR: GRIB2 message size in error, required size = ',lgrib,  &
           ', read-in size = ',lgribin
        iret = -3
        GOTO 9999
      END IF
      icount = icount + 1
      iseek = lskip+lgrib            ! next GRIB message starting location

      CALL gb_info(cgrib,lgrib,listsec0,listsec1,                         &
                   numfields,numlocal,maxlocal,iret)
      IF (iret /= 0) THEN
        WRITE(6,'(a,I3)') 'ERROR: quering GRIB2 message = ',iret
        GOTO 9999
      END IF
      itot = itot+numfields

      IF (icount == 1) THEN          ! Once per file for date and map projection value
        CALL gf_getfld(cgrib,lgrib,1,.TRUE.,.TRUE.,gfld,iret)
        IF (iret /= 0) THEN
          WRITE(6,'(a,I3)') 'ERROR: Decoding GRIB2 message = ',iret
          GOTO 9999
        END IF

        IF (verbose)  &
          WRITE(6,'(1x,a,I4.4,4(a,I2.2),/)') 'GRIB date-time found is: ', &
              gfld%idsect(6),'-',gfld%idsect(7),'-',gfld%idsect(8),'_',   &
              gfld%idsect(9),'f',gfld%ipdtmpl(9)

        IF (iyr  /= gfld%idsect(6) .OR. imo /= gfld%idsect(7) .OR.      &
            iday /= gfld%idsect(8) .OR. ihr /= gfld%idsect(9)  ) THEN
          WRITE(6,'(a,/,7x,2a,/,7x,a,I4.4,3I2.2)')                      &
              'ERROR: GRIB data is in wrong date and time',             &
              'Expected: ',gribtime, '   Found: ',gfld%idsect(6),       &
              gfld%idsect(7),gfld%idsect(8),gfld%idsect(9)

          iret = -4
          GOTO 9999
        END IF

        itemp = gfld%ipdtmpl(5)         ! indicator of model
        IF (verbose) THEN
          IF (gfld%idsect(1) == 7) THEN
            IF (itemp == 83 .OR. itemp == 84) THEN
              WRITE(6,'(1x,a,/)') 'NCEP NAM model'
            ELSE IF (itemp == 81 .OR. itemp == 96) THEN
              WRITE(6,'(1x,a,/)') 'NCEP GFS model'
            ELSE
              WRITE(6,'(1x,a,I3,/)') 'Unkown model from NCEP, model = ',itemp
            END IF
          ELSE IF (gfld%idsect(1) == 161) THEN
              WRITE(6,'(1x,a,I3,/)') 'NOAA Office of Oceanic and Atmospheric Research, model = ',itemp
          ELSE
            WRITE(6,'(1x,a,I2,a,I2,/)')                                   &
              'Unknown model and orig center, center = ',gfld%idsect(1),  &
              ' model = ',itemp
          END IF
        END IF

        IF (gfld%igdtnum == 0) THEN       ! Lat/Lon grid
          iproj_grb = 4
          nx_grb    = gfld%igdtmpl(8)
          ny_grb    = gfld%igdtmpl(9)
          dx_grb    = gfld%igdtmpl(17)/1.0E6  ! in degree
          dy_grb    = gfld%igdtmpl(18)/1.0E6  ! in degree
          iscan     = IAND(gfld%igdtmpl(19),128)/128  ! get the first bit
                      ! = 0, +x(+i) direction, = 1, -x(-i) direction
          jscan     = IAND(gfld%igdtmpl(19),64) / 64   ! get the second bit
                      ! = 0, -y(-j) direction, = 1, +y(+j) direction
          ijscan    = IAND(gfld%igdtmpl(19),32) / 32   ! get the third bit
                      ! = 0, x direction first, = 1, y direction first

          uvearth   = IAND(gfld%igdtmpl(14),8)  /  8   ! get the fifth bit, big-endian
          IF (jscan == 1) THEN          ! +y(+j) direction
            latsw     = gfld%igdtmpl(12)/1.0E6
            lonsw     = gfld%igdtmpl(13)/1.0E6
          ELSE                          ! -y(-j) direction
            latsw     = gfld%igdtmpl(15)/1.0E6
            lonsw     = gfld%igdtmpl(13)/1.0E6
          END IF
        ELSE IF (gfld%igdtnum == 30) THEN ! Lambert Conformal Grid
          iproj_grb   = 2
          nx_grb      = gfld%igdtmpl(8)
          ny_grb      = gfld%igdtmpl(9)
          lontrue     = gfld%igdtmpl(14)/1.0E6
          lattru1     = gfld%igdtmpl(19)/1.0E6
          lattru2     = gfld%igdtmpl(20)/1.0E6
          dx_grb      = gfld%igdtmpl(15)/1.0E3
          dy_grb      = gfld%igdtmpl(16)/1.0E3
          latsw       = gfld%igdtmpl(10)/1.0E6
          lonsw       = gfld%igdtmpl(11)/1.0E6
          iscan     = IAND(gfld%igdtmpl(18),128)/128  ! get the first bit
                      ! = 0, +x(+i) direction, = 1, -x(-i) direction
          jscan     = IAND(gfld%igdtmpl(18),64) / 64   ! get the second bit
                      ! = 0, -y(-j) direction, = 1, +y(+j) direction
          ijscan    = IAND(gfld%igdtmpl(18),32) / 32   ! get the third bit
                      ! = 0, x direction first, = 1, y direction first

        ELSE IF (gfld%igdtnum == 20) THEN ! Polar-Stereographic Grid
          iproj_grb   = 1
          nx_grb      = gfld%igdtmpl(8)
          ny_grb      = gfld%igdtmpl(9)
          lattru1     = 60.
          lattru2     = 91.
          latsw       = gfld%igdtmpl(10)
          lonsw       = gfld%igdtmpl(11)

        ELSE
           WRITE(6,'(1x,a,I3)') 'Unkown projection: ',gfld%igdtnum
           iret = -4
           GOTO 9999
!           CALL arpsstop('Unknown map project in GRIB2 file.',1)
        END IF

        ibgn = MAX(1,ix1)          ! right order, W - E
        iend = MIN(nx_grb,ix2)     ! S - N
        imin = ibgn-1              ! map to output index starting from 1
        IF (iscan == 0) THEN
          iinc = 1                 ! same order
          ioff = 0
        ELSE
          iinc = -1                ! Reverse from E - W to W - E
          ioff = nx_grb+1
        END IF

        jbgn = MAX(1,jy1)
        jend = MIN(ny_grb,jy2)
        jmin = jbgn-1
        IF (jscan == 1) THEN
          jinc = 1                 ! Same order
          joff = -1
        ELSE
          jinc = -1                ! Reverse from N - S to S - N
          joff = ny_grb
        END IF

        IF (nx_ext /= ABS(iend-ibgn)+1 .OR. ny_ext /= ABS(jend-jbgn)+1 ) THEN
          WRITE(6,'(1x,a,/,8x,a,I5,a,I5,/,8x,a,I5,a,I5)')                 &
             'ERROR: Dimension size inconsistent',                        &
             'Expected: nx_ext = ',nx_ext,', ny_ext = ',ny_ext,           &
             'Found   : nx_grb = ',nx_grb,', ny_grb = ',ny_grb
          iret = -4
          GOTO 9999
        END IF

        IF (verbose) THEN
          WRITE(6,'(1x,2a)') 'M_No  StartBytes F_No. ',                   &
               'Field_No.  (Dis, Cat, Par) LevelType   Levelval  GRB_Array'
          WRITE(6,'(1x,2a)') '===== ========== ===== ',                   &
               '========== =============== ========== ========== =========='
        END IF

        CALL gf_free( gfld )

      END IF        ! First GRIB2 message

      IF (verbose) WRITE(6,FMT='(1x,I4,2x,I10,1x,I3,3x)',ADVANCE='NO')  &
                   icount,lskip+1,numfields

      DO n = 1, numfields   ! unpack GRIB2 fields in each message

        CALL gf_getfld(cgrib,lgrib,n,.TRUE.,.TRUE.,gfld,iret)
        IF (iret /= 0) THEN
          WRITE(6,'(a,I3)') 'ERROR: Decoding GRIB2 message = ',iret
          GOTO 9999
!          CALL arpsstop('ERROR in rdnmcgrb2 when calling gf_getfld.',1)
        END IF

        IF (verbose) THEN
          IF (n == 1) THEN
          WRITE(6,FMT='(I5,6x,3(a,I3),a,I10,4x)',ADVANCE='NO') n,         &
          '(',gfld%discipline,',',gfld%ipdtmpl(1),',',gfld%ipdtmpl(2),')',&
          gfld%ipdtmpl(10)
          ELSE
          WRITE(6,FMT='(24x,I5,6x,3(a,I3),a,I10,4x)',ADVANCE='NO') n,     &
          '(',gfld%discipline,',',gfld%ipdtmpl(1),',',gfld%ipdtmpl(2),')',&
          gfld%ipdtmpl(10)
          END IF
        END IF

        DO m = 1,maxvar    ! match the required variables
          IF (gfld%discipline == varids(1,m) .AND.      &  ! Discipline
              gfld%ipdtmpl(1) == varids(2,m) .AND.      &  ! Category
              gfld%ipdtmpl(2) == varids(3,m) .AND.      &  ! Parameter
              gfld%ipdtmpl(10)== varids(4,m) ) THEN        ! Elevation

            levelin = gfld%ipdtmpl(12)

!-----------------------------------------------------------------------
!
! Check 2D level, Should be at specified 2D slab
!
!-----------------------------------------------------------------------

            IF (var2dindx(m) > 0 .AND.             & ! 2D level
                gfld%ipdtmpl(10) == 102            &  ! Specified height above mean sea level(m)
               ) THEN

              levelout = INT(var2dlvl(var2dindx(m)))

              IF (levelin == levelout ) THEN
                !ij = 0
                DO j = jbgn, jend
                  DO i = ibgn, iend
                    !ij = ij+1
                    ij = ( j*jinc+joff )*nx_grb+( i*iinc+ioff )
                    ii = i-imin
                    jj = j-jmin
! write(0,*) i,j, '=', i*iinc+ioff,j*jinc+joff,'->',ii, jj
                    var_grb2d(ii,jj,var2dindx(m)) = gfld%fld(ij)
                  END DO
                END DO

                IF (verbose) WRITE(6,FMT='(I10,5x,a)',ADVANCE='NO')     &
                                  levelin, '2D'
              ELSE
                IF (verbose) WRITE(6,FMT='(I10,5x,a)',ADVANCE='NO')     &
                                  levelin, '-- Skipped'
              END IF  ! 2D variable at the right layer

            ELSE
              IF (verbose) WRITE(6,FMT='(I10,5x,a)',ADVANCE='NO')       &
                                levelin, '-- Skipped'
            END IF         ! Store if matched levels also

            EXIT           ! already matched, so exit from the required variable loop

          END IF         ! matched var. ids
        END DO         ! matching loop all required variables

        IF (verbose ) THEN
          IF ( m > maxvar) THEN
            WRITE(6,'(10x,a)') ' --- skipped.'
          ELSE
            WRITE(6,*)
          END IF
        END IF

        CALL gf_free( gfld )

      END DO               ! numfields

    END DO   ! number of GRIB2 messages

!-----------------------------------------------------------------------
!
! Close file and release the gribfield before returning
!
!-----------------------------------------------------------------------

  CALL baclose( grbunit,iret )
  CALL retunit( grbunit )

  IF ( .NOT. ALLOCATED(cgrib) ) THEN
    WRITE(6,'(1x,a,/)') 'WARNING:  File is empty!'
    iret = -888
  ELSE
    DEALLOCATE(cgrib)
  END IF

  9999  CONTINUE

  RETURN
END SUBROUTINE rd_mrmsgrb2
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE RDGRB2DIMS                 ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE rdgrb2dims(gribfile,grbflen,nx_grb,ny_grb,iret)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  This subroutine extracts dimension sizes from GRIB2 file
!
!  The decoder of GRIB2 is from NCEP.
!
!  NOTE: It is intended to be called from root processor only.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  08/20/2007
!
!  MODIFICATIONS:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    gribfile      GRIB file name
!    grbflen       Length of GRIB file name
!
!  OUTPUT:
!
!    nx_grb        x-direction grid length
!    ny_grb        y-direction grid length
!
!    iret          Return flag
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  USE GRIB_MOD

  IMPLICIT NONE

  INTEGER, INTENT(IN) :: grbflen

  CHARACTER(LEN=*), INTENT(IN) :: gribfile

  INTEGER, INTENT(OUT) :: nx_grb       ! Number of points along x-axis
  INTEGER, INTENT(OUT) :: ny_grb       ! Number of points along y-axis

  INTEGER, INTENT(OUT) :: iret         ! Return flag

!
!-----------------------------------------------------------------------
!
!  Temporary arrays to read GRIB file
!
!-----------------------------------------------------------------------
!
  INTEGER :: grbunit

  INTEGER, PARAMETER :: maxlen = 32000
  CHARACTER(LEN=1), ALLOCATABLE :: cgrib(:)

  TYPE(gribfield) :: gfld

!
!-----------------------------------------------------------------------
!
!  Misc internal variables
!
!-----------------------------------------------------------------------
!
!  LOGICAL             :: verbose = .FALSE.
  CHARACTER(LEN=12), PARAMETER :: cmd     = 'rdgrib2dims: '

  INTEGER :: iseek
  INTEGER :: lgrib, lgribin, lskip

  INTEGER :: iscan, jscan, ijscan
  INTEGER :: listsec0(3), listsec1(13)
  INTEGER :: numfields, numlocal, maxlocal

  INTEGER :: itemp
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!-----------------------------------------------------------------------
!
!  Open the GRIB file
!
!-----------------------------------------------------------------------
!
  WRITE(6,'(1x,8A)') cmd, ' Opening ', gribfile(1:grbflen)

  CALL getunit( grbunit )

  CALL baopenr(grbunit,gribfile(1:grbflen),iret)

  IF  ( iret /= 0 )  THEN
    WRITE(6,'(a,a,/,a,i4)')                                             &
        'ERROR in opening file ',gribfile(1:grbflen),                   &
        'function baopenr return status: ',iret
    RETURN
  END IF

!-----------------------------------------------------------------------
!
! Get the first record
!
!-----------------------------------------------------------------------

  iseek   = 0
  iret = 0

  CALL skgb(grbunit,iseek,maxlen,lskip,lgrib)
  IF (lgrib == 0) THEN
    iret = -3
    RETURN
  END IF

  ALLOCATE(cgrib(lgrib), STAT = iret)

  CALL baread(grbunit,lskip,lgrib,lgribin,cgrib)   ! read a GRIB message
  IF (lgrib /= lgribin) THEN
    WRITE(6,'(2(a,I6))')                                              &
       'ERROR: GRIB2 message size in error, required size = ',lgrib,  &
       ', read-in size = ',lgribin
    iret = -3
    RETURN
!    CALL arpsstop('ERROR in rdgrb2dims when calling baread.',1)
  END IF

  CALL gb_info(cgrib,lgrib,listsec0,listsec1,                         &
               numfields,numlocal,maxlocal,iret)
  IF (iret /= 0) THEN
    WRITE(6,'(a,I3)') 'ERROR: quering GRIB2 message = ',iret
    RETURN
!    CALL arpsstop('ERROR in rdgrb2dims when calling gb_info.',1)
  END IF

  CALL gf_getfld(cgrib,lgrib,1,.TRUE.,.TRUE.,gfld,iret)
  IF (iret /= 0) THEN
    WRITE(6,'(a,I3)') 'ERROR: Decoding GRIB2 message = ',iret
    RETURN
!    CALL arpsstop('ERROR in rdgrb2dims when calling gf_getfld.',1)
  END IF

  itemp = gfld%ipdtmpl(5)         ! indicator of model
  IF (gfld%idsect(1) == 7) THEN   !US NWS
    IF (itemp == 83 .OR. itemp == 84) THEN
      WRITE(6,'(1x,a,/)') 'NCEP NAM model'
    ELSE IF (itemp == 81 .OR. itemp == 96) THEN
      WRITE(6,'(1x,a,/)') 'NCEP GFS model'
    ELSE IF (itemp == 140) THEN
      WRITE(6,'(1x,a,/)') 'NCEP NARR model'
    ELSE IF (itemp == 150) THEN
      WRITE(6,'(1x,a,/)') 'NCEP RIVER FORECAST SYSTEM'
    ELSE IF (itemp == 183) THEN
      WRITE(6,'(1x,a,/)') 'NDFD products generated by NCEP'
    ELSE IF (itemp == 109) THEN
      WRITE(6,'(1x,a,/)') 'RTMA-Real Time Mesoscale Analysis'
    ELSE IF (itemp == 46) THEN
      WRITE(6,'(1x,a,/)') 'HYCOM- Global'
    ELSE IF (itemp == 47) THEN
      WRITE(6,'(1x,a,/)') 'HYCOM North Pacific Basin'
    ELSE IF (itemp == 48) THEN
      WRITE(6,'(1x,a,/)') 'HYCOM North Atlantic Basin'
    ELSE
      WRITE(6,'(1x,a,I3,/)') 'Unkown model from NCEP, model = ',itemp
    END IF
  ELSE
    WRITE(6,'(1x,a,I2,a,I2,/)')                                   &
          'Unknown model and orig center, center = ',gfld%idsect(1),  &
          ' model = ',itemp
  END IF

  IF (gfld%igdtnum == 0) THEN       ! Lat/Lon grid
!    iproj_grb = 4
    nx_grb    = gfld%igdtmpl(8)
    ny_grb    = gfld%igdtmpl(9)
!    dx_grb    = gfld%igdtmpl(17)/1.0E6  ! in degree
!    dy_grb    = gfld%igdtmpl(18)/1.0E6  ! in degree
!    iscan     = IAND(gfld%igdtmpl(19),128)/128  ! get the first bit
                    ! = 0, +x(+i) direction, = 1, -x(-i) direction
!    jscan     = IAND(gfld%igdtmpl(19),64) / 64   ! get the second bit
                    ! = 0, -y(-j) direction, = 1, +y(+j) direction
!    ijscan    = IAND(gfld%igdtmpl(19),32) / 32   ! get the third bit
                    ! = 0, x direction first, = 1, y direction first

!    uvearth   = IAND(gfld%igdtmpl(14),8)  /  8   ! get the fifth bit, big-endian
!    IF (jscan == 1) THEN          ! +y(+j) direction
!      latsw     = gfld%igdtmpl(12)/1.0E6
!      lonsw     = gfld%igdtmpl(13)/1.0E6
!    ELSE                          ! -y(-j) direction
!      latsw     = gfld%igdtmpl(15)/1.0E6
!      lonsw     = gfld%igdtmpl(13)/1.0E6
!    END IF
  ELSE IF (gfld%igdtnum == 30) THEN ! Lambert Conformal Grid
!    iproj_grb   = 2
    nx_grb      = gfld%igdtmpl(8)
    ny_grb      = gfld%igdtmpl(9)
!    lontrue     = gfld%igdtmpl(14)
!    lattru1     = gfld%igdtmpl(19)
!    lattru2     = gfld%igdtmpl(20)
!    dx_grb      = gfld%igdtmpl(15)
!    dy_grb      = gfld%igdtmpl(16)
!    latsw       = gfld%igdtmpl(10)
!    lonsw       = gfld%igdtmpl(11)

  ELSE IF (gfld%igdtnum == 20) THEN ! Polar-Stereographic Grid
!    iproj_grb   = 1
    nx_grb      = gfld%igdtmpl(8)
    ny_grb      = gfld%igdtmpl(9)
!    lattru1     = 60.
!    lattru2     = 91.
!    latsw       = gfld%igdtmpl(10)
!    lonsw       = gfld%igdtmpl(11)

  ELSE
    WRITE(6,'(1x,a,I3)') 'Unkown projection: ',gfld%igdtnum
    iret = -3
    RETURN
  END IF

!-----------------------------------------------------------------------
!
! Close file and release the gribfield before returning
!
!-----------------------------------------------------------------------

  CALL gf_free( gfld )
  CALL baclose( grbunit,iret )

  CALL retunit( grbunit )

  DEALLOCATE (cgrib)

  RETURN
END SUBROUTINE rdgrb2dims
