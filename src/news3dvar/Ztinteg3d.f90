!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TINTEGCP                   ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tot_force(nx,ny,nz,                                         &
           u,v,w,wcont,ptprt,pprt,qv,qscalar,tke,                      &
           ubar,vbar,ptbar,pbar,rhostr,qvbar,                          &
           usflx,vsflx, x,y,z,zp, j1,j2,j3, sinlat,stabchk,            &
           uforce,vforce,wforce, km,lendel,defsq,                      &
           ubk, vbk, wbk,                                              &
           tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,               &
           tem10,tem11,tem12)
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!  dtbig1     Local large time step size.
!
!    u        x component of velocity (m/s)
!    v        y component of velocity (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates (m/s).
!    wcont    Contravariant vertical velocity (m/s)
!    ptprt    Perturbation potential temperature (K)
!    pprt     Perturbation pressure (Pascal)
!    qv       Water vapor specific humidity (kg/kg)
!    qc       Cloud water mixing ratio (kg/kg)
!    qr       Rainwater mixing ratio (kg/kg)
!    qi       Cloud ice mixing ratio (kg/kg)
!    qs       Snow mixing ratio (kg/kg)
!    qh       Hail mixing ratio (kg/kg)
!    tke      Turbulent Kinetic Energy ((m/s)**2)
!
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!
!    usflx    Surface flux of u-momentum
!    vsflx    Surface flux of v-momentum
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space(m)
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!    sinalt   Sin of latitude at each grid point
!
!  OUTPUT:
!
!    uforce   Acoustically inactive forcing terms in u-momentum
!             equation (kg/(m*s)**2). uforce= -uadv + umix + ucorio
!    vforce   Acoustically inactive forcing terms in v-momentum
!             equation (kg/(m*s)**2). vforce= -vadv + vmix + vcorio
!    wforce   Acoustically inactive forcing terms in w-momentum
!             equation (kg/(m*s)**2).
!             wforce= -wadv + wmix + wbuoy + wcorio
!    km       Turbulent mixing coefficient for momentum.
!             Used to calculate turbulent mixing of scalars.
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3).
!    defsq    Deformation squared.
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!    tem4     Temporary work array.
!    tem5     Temporary work array.
!    tem6     Temporary work array.
!    tem7     Temporary work array.
!    tem8     Temporary work array.
!    tem9     Temporary work array.
!    tem10    Temporary work array.
!    tem11    Temporary work array.
!    tem12    Temporary work array.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'phycst.inc'
  INCLUDE 'globcst.inc'
  INCLUDE 'bndry.inc'

  INTEGER :: nt                ! The no. of t-levels of t-dependent
                               ! arrays.
  INTEGER :: tpast             ! Index of time level for the past time.
  INTEGER :: tpresent          ! Index of time level for the present
                               ! time.
  INTEGER :: tfuture           ! Index of time level for the future
                               ! time.
  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  INTEGER :: nx,ny,nz,i,j,k    ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity (m/s)
  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)
  REAL :: ptprt (nx,ny,nz,nt)  ! Perturbation potential temperature (K)
  REAL :: pprt  (nx,ny,nz,nt)  ! Perturbation pressure (Pascal)
  REAL :: qv    (nx,ny,nz,nt)  ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq,nt)  ! Cloud water mixing ratio (kg/kg)
  REAL :: tke   (nx,ny,nz,nt)  ! Turbulent kinetic energy ((m/s)**2)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific
                               ! humidity(kg/kg)

  REAL,INTENT(IN) :: ubk   (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL,INTENT(IN) :: vbk   (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL,INTENT(IN) :: wbk   (nx,ny,nz)     ! Total w-velocity (m/s)

  REAL :: x     (nx)           ! x-coord. of the physical and compu-
                               ! tational grid. Defined at u-point(m).
  REAL :: y     (ny)           ! y-coord. of the physical and compu-
                               ! tational grid. Defined at v-point(m).
  REAL :: z     (nz)           ! z-coord. of the computational grid.
                               ! Defined at w-point on the staggered
                               ! grid(m).
  REAL :: zp    (nx,ny,nz)     ! Physical height coordinate defined at
                               ! w-point of the staggered grid(m).

  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! -d(zp)/d(x)
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! -d(zp)/d(y)
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! d(zp)/d(z)

  REAL :: sinlat(nx,ny)        ! Sin of latitude at each grid point
  REAL :: stabchk(nx,ny)

  REAL :: usflx (nx,ny)        ! Surface flux of u-momentum
                               ! (kg/(m*s**2))
  REAL :: vsflx (nx,ny)        ! Surface flux of v-momentum
                               ! (kg/(m*s**2))


  REAL :: uforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in u-momentum equation (kg/(m*s)**2)
                               ! uforce= -uadv + umix + ucorio

  REAL :: vforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in v-momentum equation (kg/(m*s)**2)
                               ! vforce= -vadv + vmix + vcorio

  REAL :: wforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in w-momentum equation (kg/(m*s)**2)
                               ! wforce= -wadv + wmix + wbuoy

  REAL :: km    (nx,ny,nz)     ! Turbulent mixing coefficient for
                               ! momentum.
  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)
  REAL :: defsq (nx,ny,nz)     ! Deformation squared (1/s**2)

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem4  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem5  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem6  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem7  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem8  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem9  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem10 (nx,ny,nz)     ! Temporary work array.
  REAL :: tem11 (nx,ny,nz)     ! Temporary work array.
  REAL :: tem12 (nx,ny,nz)     ! Temporary work array.
!
  REAL :: dtbig1               ! Local value of big time step size
  REAL :: dtsml1               ! Local value of small time step size
  REAL :: sum1, sum2, sum3
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
if ( 1==0 ) then
  sum1 = 0.0
  sum2 = 0.0
  sum3 = 0.0
!
  do i=1,nx
  do j=1,ny
  do k=1,nz
    sum1 =sum1 + u(i,j,k,1)*u(i,j,k,1)
    sum2 =sum2 + v(i,j,k,1)*v(i,j,k,1)
    sum3 =sum3 + w(i,j,k,1)*w(i,j,k,1)
  end do
  end do
  end do
!
 print*,'sum1==000000====',sum1
 print*,'sum2==000000====',sum2
 print*,'sum3==000000====',sum3
end if
!
!
!-----------------------------------------------------------------------
!
!  To initialize the three-time level scheme, we assume for
!  the first step that the values of all variables at time
!  past equal the values at time present.  We then perform a
!  forward-in-time integration for the first step only and
!  then, using the same algorithm, we perform a centered-in-time
!  integration for all subsequent steps. The size of the
!  timestep is adjusted accordingly, such that the leapfrog step
!  size is twice that of the first forward-in-time step.
!
!-----------------------------------------------------------------------
!
    dtbig1 = dtbig
    dtsml1 = dtsml
!
!-----------------------------------------------------------------------
!
!  Calculate wcont at time tpresent. wcont will be used in FRCUVW
!  and FRCP. rhostr averaged to u, v, w points. They are stored
!  in tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
  tem1=0.0; tem2=0.0; tem3=0.0

  CALL rhouvw0(nx,ny,nz,rhostr,tem1,tem2,tem3)

! CALL wcontra0(nx,ny,nz,                                               &
!              u(1,1,1,tpresent),v(1,1,1,tpresent),                     &
!              w(1,1,1,tpresent),j1,j2,j3,                              &

  CALL wcontra0(nx,ny,nz,                                               &
               ubk,vbk,wbk,j1,j2,j3,                                    &
               rhostr,tem1,tem2,tem3,wcont,tem4,tem5)

!
!-----------------------------------------------------------------------
!
!  Compute the acoustically inactive terms in the momentum and
!  pressure equations that are held fixed during the small time
!  step computations.  This includes advection, buoyancy, mixing
!  (both physical and computational), and the Coriolis terms.
!  These forcing terms are accumulated into arrays for each
!  of the momentum equations, e.g., uforce for the u-equation,
!  vforce for the v-equation, wforce for the w-equation and
!  pforce for the p-equation.
!
!-----------------------------------------------------------------------
!
!print*,'before calling frcuvw0'
  CALL frcuvw0(nx,ny,nz,dtbig1,                                         &
              u(1,1,1,tpast),v(1,1,1,tpast),w(1,1,1,tpast),             &
              wcont, ptprt(1,1,1,tpast), pprt(1,1,1,tpast),             &
              qv(1,1,1,tpast),qscalar(1,1,1,:,tpast),                   &
              tke(1,1,1,tpast),                                         &
              ubar,vbar,ptbar,pbar,rhostr,qvbar,                        &
              usflx,vsflx, x,y,z,zp, j1,j2,j3, sinlat,stabchk,          &
              uforce,vforce,wforce, km,lendel,defsq,                    &
              ubk, vbk, wbk,                                            &
              tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,             &
              tem10,tem11,tem12)
!
!
          tem1(:,:,:)= 0.0
          tem2(:,:,:)= 0.0
          tem3(:,:,:)= 0.0          ! stand for upgrad
          tem4(:,:,:)= 0.0          ! stand for vpgrad
          tem5(:,:,:)= 0.0          ! stand for wpgrad
!
  CALL pgrad0(nx,ny,nz, pprt(1,1,1,tpast),                               &
             j1,j2,j3,tem3,tem4,tem5,tem1,tem2)
!
  DO k=1,nz
   DO j=1,ny
    DO i=1,nx
     uforce(i,j,k) = uforce(i,j,k) - tem3(i,j,k)
     vforce(i,j,k) = vforce(i,j,k) - tem4(i,j,k)
     wforce(i,j,k) = wforce(i,j,k) - tem5(i,j,k)
     tem1(i,j,k)= 0.0
     tem2(i,j,k)= 0.0
!
    END DO
   END DO
  END DO
!
if (lvldbg>0) then
  sum1 = 0.0
  sum2 = 0.0
  sum3 = 0.0
!
  do i=1,nx
  do j=1,ny
  do k=1,nz
     sum1 =sum1 + tem3(i,j,k)*tem3(i,j,k)
     sum2 =sum2 + tem4(i,j,k)*tem4(i,j,k)
     sum3 =sum3 + tem5(i,j,k)*tem5(i,j,k)

  end do
  end do
  end do
!
 print*,'sum1==prgad0====',sum1
 print*,'sum2==prgad0====',sum2
 print*,'sum3==prgad0====',sum3
end if
!
IF (1==2) THEN
  do j=1, ny
    do i=1, nx
      uforce(i,j,1)= 0.0
      vforce(i,j,1)= 0.0
      wforce(i,j,1)= 0.0
      uforce(i,j,nz)= 0.0
      vforce(i,j,nz)= 0.0
      wforce(i,j,nz)= 0.0
      uforce(i,j,nz-1)= 0.0
      vforce(i,j,nz-1)= 0.0
      wforce(i,j,nz-1)= 0.0
    end do
  end do
!
!
  do k=2, nz-1
    do i=1, nx
      uforce(i,1,k)= 0.0
      vforce(i,1,k)= 0.0
      wforce(i,1,k)= 0.0

      uforce(i,ny,k)= 0.0
      vforce(i,ny,k)= 0.0
      wforce(i,ny,k)= 0.0
    end do
  end do
!
!
  do k=2, nz-1
    do j=1, ny
      uforce(1,j,k)= 0.0
      vforce(1,j,k)= 0.0
      wforce(1,j,k)= 0.0

      uforce(nx,j,k)= 0.0
      vforce(nx,j,k)= 0.0
      wforce(nx,j,k)= 0.0
    end do
  end do
END IF
!
!
!      open(99,file='tendency_p.dat', form='unformatted')
!       write(99) (-tem3),(-tem4),(-tem5)
!      close(99)
!
!      open(99,file='tendency_3dvar.dat', form='unformatted')
!       write(99) uforce,vforce,wforce
!      close(99)
!
  RETURN
END SUBROUTINE TOT_FORCE
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE WCONTRA0                   ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE wcontra0(nx,ny,nz,u,v,w,j1,j2,j3,                             &
           rhostr,rhostru,rhostrv,rhostrw,wcont,ustr,vstr)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate wcont, the contravariant vertical velocity (m/s)
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue & Hao Jin
!  1/4/1993.
!
!  Modification history:
!  8/29/94 (A. Shapiro)
!  Bug fix. Call to vbcwcont moved outside IF block.
!
!  9/9/94 (M. Xue)
!  Optimized.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x component of velocity at all time levels (m/s)
!    v        y component of velocity at all time levels (m/s)
!    w        Vertical component of Cartesian velocity
!             at all time levels (m/s)
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!    rhostr   j3 times base state density rhobar(kg/m**3).
!    rhostru  Average rhostr at u points (kg/m**3).
!    rhostrv  Average rhostr at v points (kg/m**3).
!    rhostrw  Average rhostr at w points (kg/m**3).
!
!  OUTPUT:
!
!    wcont    Vertical component of contravariant velocity in
!             computational coordinates (m/s)
!
!  WORK ARRAYS:
!
!    ustr     Work array
!    vstr     Work array
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! The number of grid points in 3
                               ! directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)

  REAL :: j1    (nx,ny,nz)     ! Coordinate transform Jacobian
                               ! defined as - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transform Jacobian
                               ! defined as - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transform Jacobian
                               ! defined as d( zp )/d( z ).

  REAL :: rhostr(nx,ny,nz)     ! j3 times base state density rhobar
                               ! (kg/m**3).
  REAL :: rhostru(nx,ny,nz)    ! Average rhostr at u points (kg/m**3).
  REAL :: rhostrv(nx,ny,nz)    ! Average rhostr at v points (kg/m**3).
  REAL :: rhostrw(nx,ny,nz)    ! Average rhostr at w points (kg/m**3).

  REAL :: wcont (nx,ny,nz)     ! Vertical velocity in computational
                               ! coordinates (m/s)

  REAL :: ustr  (nx,ny,nz)     ! temporary work array
  REAL :: vstr  (nx,ny,nz)     ! temporary work array

!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
!
!-----------------------------------------------------------------------

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF( crdtrns == 0 ) THEN  ! No coord. transformation case.

    DO k= 2,nz-1
      DO j= 1,ny-1
        DO i= 1,nx-1
          wcont(i,j,k)=w(i,j,k)
        END DO
      END DO
    END DO

  ELSE IF( ternopt == 0) THEN

    DO k= 2,nz-1
      DO j= 1,ny-1
        DO i= 1,nx-1
          wcont(i,j,k)=w(i,j,k)*2.0/(j3(i,j,k)+j3(i,j,k-1))
        END DO
      END DO
    END DO

  ELSE

    DO k= 1,nz-1
      DO j= 1,ny-1
        DO i= 1,nx
          ustr(i,j,k)=u(i,j,k)*rhostru(i,j,k)
        END DO
      END DO
    END DO

    DO k= 1,nz-1
      DO j= 1,ny
        DO i= 1,nx-1
          vstr(i,j,k)=v(i,j,k)*rhostrv(i,j,k)
        END DO
      END DO
    END DO

    DO k= 2,nz-1
      DO j= 1,ny-1
        DO i= 1,nx-1
          wcont(i,j,k)= (                                               &
              ((ustr(i  ,j,k)+ustr(i  ,j,k-1))*j1(i  ,j,k)              &
              +(ustr(i+1,j,k)+ustr(i+1,j,k-1))*j1(i+1,j,k)              &
              +(vstr(i  ,j,k)+vstr(i  ,j,k-1))*j2(i  ,j,k)              &
              +(vstr(i,j+1,k)+vstr(i,j+1,k-1))*j2(i,j+1,k)) * 0.25      &
              / rhostrw(i,j,k) + w(i,j,k)                               &
              ) * 2.0/(j3(i,j,k)+j3(i,j,k-1))
        END DO
      END DO
    END DO

  END IF
!
  CALL vbcwcont0(nx,ny,nz,wcont)  !tmp.debug to be added back
!
  RETURN
END SUBROUTINE wcontra0
