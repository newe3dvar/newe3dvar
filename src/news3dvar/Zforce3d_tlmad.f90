!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TLMFRCUVW                  ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tlmfrcuvw( nx,ny,nz,dtbig1,nstepss,nbasic,                   &
           u,v,w,wcont,ptprt,pprt,qv,qscalar,tke,                       &
           ubar,vbar,ptbar,pbar,rhostr,qvbar,                           &
           nustr,nvstr,nwstr,                                           &
           usflx,vsflx, x,y,z,zp, j1,j2,j3, sinlat,stabchk,             &
           uforce,vforce,wforce, km,lendel,defsq,                       &
           ubk,vbk,wbk,                                                 &
           tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,                &
           tem10,tem11,tem12)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the inactive acoustic forcing terms in the momentum
!  equations. These forcing terms include the advection, mixing, Coriolis
!  force and buoyancy terms, and are accumulated into one array for
!  each equation, i.e.,
!      uforce = - uadv + umix + ucorio.
!      vforce = - vadv + vmix + vcorio.
!      wforce = - wadv + wmix + wbuoy + wcorio.
!  These forcing terms are held fixed during the small acoustic wave
!  integration time steps. The turbulent mixing coefficient for
!  momentum km is an output which will be used to calculate the
!  turbulent mixing of heat and water substances.
!
!  TLMFRCUVW is the tangent linear model of FRCUVW
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  04/19/96
!
!  MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    dtbig1     Local large time step size.
!    nstepss  The number of integrations plus one.
!    nbasic   Index of the current basic state time level.
!
!    u        x component of velocity (m/s)
!    v        y component of velocity (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates (m/s).
!    wcont    Contravariant vertical velocity (m/s)
!    ptprt    Perturbation potential temperature (K)
!    pprt     Perturbation pressure (Pascal)
!    qv       Water vapor specific humidity (kg/kg)
!    qc       Cloud water mixing ratio (kg/kg)
!    qr       Rainwater mixing ratio (kg/kg)
!    qi       Cloud ice mixing ratio (kg/kg)
!    qs       Snow mixing ratio (kg/kg)
!    qh       Hail mixing ratio (kg/kg)
!    tke      Turbulent Kinetic Energy ((m/s)**2)
!
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!
!    usflx    Surface flux of u-momentum
!    vsflx    Surface flux of v-momentum
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space(m)
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!    sinalt   Sin of latitude at each grid point
!
!    Basic state variables:
!    nu,nv,nw,nptprt,npprt,nqv,nqc,nqr,nqi,nqs,nqh,
!    nustr,nvstr,nwstr.
!
!  OUTPUT:
!
!    uforce   Acoustically inactive forcing terms in u-momentum
!             equation (kg/(m*s)**2). uforce= -uadv + umix + ucorio
!    vforce   Acoustically inactive forcing terms in v-momentum
!             equation (kg/(m*s)**2). vforce= -vadv + vmix + vcorio
!    wforce   Acoustically inactive forcing terms in w-momentum
!             equation (kg/(m*s)**2).
!             wforce= -wadv + wmix + wbuoy + wcorio
!    km       Turbulent mixing coefficient for momentum.
!             Used to calculate turbulent mixing of scalars.
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3).
!    defsq    Deformation squared.
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!    tem4     Temporary work array.
!    tem5     Temporary work array.
!    tem6     Temporary work array.
!    tem7     Temporary work array.
!    tem8     Temporary work array.
!    tem9     Temporary work array.
!    tem10    Temporary work array.
!    tem11    Temporary work array.
!    tem12    Temporary work array.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global constants that control model
                            ! execution
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'bndry.inc'
  !INCLUDE 'variable.inc'    ! Basic state variable.
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  INTEGER :: nt                ! The no. of t-levels of t-dependent
                               ! arrays.
  INTEGER :: tpast             ! Index of time level for the past time.
  INTEGER :: tpresent          ! Index of time level for the present
                               ! time.
  INTEGER :: tfuture           ! Index of time level for the future
                               ! time.
  REAL :: dtbig1               ! Local large time step size.
  INTEGER :: nstepss           ! Index of current basic state time levels.
  INTEGER :: nbasic            ! Index of the current basic state time level.
  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity (m/s)
  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)
  REAL :: nwcont (nx,ny,nz)    ! Contravariant vertical velocity (m/s)
  REAL :: nustr  (nx,ny,nz)    ! nu * rhostr
  REAL :: nvstr  (nx,ny,nz)    ! nv * rhostr
  REAL :: nwstr  (nx,ny,nz)    ! nw * rhostr
  REAL :: ptprt (nx,ny,nz,nt)  ! Perturbation potential temperature (K)
  REAL :: pprt  (nx,ny,nz,nt)  ! Perturbation pressure (Pascal)
  REAL :: qv    (nx,ny,nz,nt)  ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq,nt)  ! Cloud water mixing ratio (kg/kg)
  REAL :: tke   (nx,ny,nz,nt)  ! Turbulent kinetic energy ((m/s)**2)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific
                               ! humidity(kg/kg)

  REAL :: ubk  (nx,ny,nz)      ! Base state u-velocity (m/s)
  REAL :: vbk  (nx,ny,nz)      ! Base state v-velocity (m/s)
  REAL :: wbk  (nx,ny,nz)      ! Base state potential temperature (K)

  REAL :: x     (nx)           ! x-coord. of the physical and compu-
                               ! tational grid. Defined at u-point(m).
  REAL :: y     (ny)           ! y-coord. of the physical and compu-
                               ! tational grid. Defined at v-point(m).
  REAL :: z     (nz)           ! z-coord. of the computational grid.
                               ! Defined at w-point on the staggered
                               ! grid(m).
  REAL :: zp    (nx,ny,nz)     ! Physical height coordinate defined at
                               ! w-point of the staggered grid(m).

  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! -d(zp)/d(x)
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! -d(zp)/d(y)
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! d(zp)/d(z)

  REAL :: sinlat(nx,ny)        ! Sin of latitude at each grid point
  REAL :: stabchk(nx,ny)

  REAL :: usflx (nx,ny)        ! Surface flux of u-momentum
                               ! (kg/(m*s**2))
  REAL :: vsflx (nx,ny)        ! Surface flux of v-momentum
                               ! (kg/(m*s**2))


  REAL :: uforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in u-momentum equation (kg/(m*s)**2)
                               ! uforce= -uadv + umix + ucorio

  REAL :: vforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in v-momentum equation (kg/(m*s)**2)
                               ! vforce= -vadv + vmix + vcorio

  REAL :: wforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in w-momentum equation (kg/(m*s)**2)
                               ! wforce= -wadv + wmix + wbuoy

  REAL :: km    (nx,ny,nz)     ! Turbulent mixing coefficient for
                               ! momentum.
  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)
  REAL :: defsq (nx,ny,nz)     ! Deformation squared (1/s**2)

!
!-----------------------------------------------------------------------
!
!  Working arrays
!
!-----------------------------------------------------------------------
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem4  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem5  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem6  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem7  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem8  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem9  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem10 (nx,ny,nz)     ! Temporary work array.
  REAL :: tem11 (nx,ny,nz)     ! Temporary work array.
  REAL :: tem12 (nx,ny,nz)     ! Temporary work array.
!
  REAL :: bem1  (nx,ny,nz)
  REAL :: bem2  (nx,ny,nz)
  REAL :: bem3  (nx,ny,nz)
  REAL :: bem4  (nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  REAL :: sum1,sum2,sum3
!
  INTEGER :: i,j,k,n
  INTEGER :: tlevel
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Calculate the total mixing (which includes subgrid scale
!  turbulent mixing and computational mixing) terms for u, v, w
!  equations.
!
!  These mixing terms are accumulated in the arrays
!  uforce, vforce and wforce.
!
!  The turbulent mixing coefficient KM is saved and passed out of
!  this subroutine and used in the calculation of the turbulent
!  mixing of heat and water substances.
!
!  Since all mixing terms are evaluated at the past time level,
!  we pass only the variable fields at time tpast into the routine.
!
!-----------------------------------------------------------------------
!
!
  tlevel = tpast

IF (1==2) THEN ! turn off mixing term
!
  IF (1==0) THEN
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          tem1(i,j,k)=0.
          tem2(i,j,k)=0.
          tem3(i,j,k)=0.
          tem4(i,j,k)=0.
          tem5(i,j,k)=0.
          tem6(i,j,k)=0.
          tem7(i,j,k)=0.
          tem8(i,j,k)=0.
          tem9(i,j,k)=0.
          tem10(i,j,k)=0.
          tem11(i,j,k)=0.
          tem12(i,j,k)=0.
!
          bem1(i,j,k) =u(i,j,k,tpresent)
          bem2(i,j,k) =v(i,j,k,tpresent)
          bem3(i,j,k) =w(i,j,k,tpresent)
!
        END DO
      END DO
    END DO
  END IF

   CALL tlmmixuvw(nx,ny,nz,nstepss,nbasic,                               &
               u    (1,1,1,tlevel),v   (1,1,1,tlevel),                   &
               w    (1,1,1,tlevel),                                      &
               ptprt(1,1,1,tlevel),pprt(1,1,1,tlevel),                   &
               qv   (1,1,1,tlevel),qscalar(1,1,1,:,tlevel),              &
               tke  (1,1,1,tpresent),ubar,vbar,ptbar,                    &
               pbar,rhostr,qvbar,                                        &
               usflx,vsflx, x,y,z,zp, j1,j2,j3,stabchk,                  &
               uforce,vforce,wforce, km, lendel,defsq,                   &
               tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,             &
               tem10,tem11,tem12)
!
!
  IF (1==0) THEN
    sum1=0.
    CALL product(uforce,uforce,                                          &
                 nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,sum2)
    sum1=sum1+sum2

    CALL product(vforce,vforce,                                          &
                 nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,sum2)
    sum1=sum1+sum2

    CALL product(wforce,wforce,                                          &
                 nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,sum2)
    sum1=sum1+sum2
    PRINT*,'(AQ)*(AQ)= ',sum1,sum2
!
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          u(i,j,k,tpresent)=0.
          w(i,j,k,tpresent)=0.
          v(i,j,k,tpresent)=0.
         tem1(i,j,k)=0.
         tem2(i,j,k)=0.
         tem3(i,j,k)=0.
         tem4(i,j,k)=0.
         tem5(i,j,k)=0.
         tem6(i,j,k)=0.
         tem7(i,j,k)=0.
         tem8(i,j,k)=0.
         tem9(i,j,k)=0.
         tem10(i,j,k)=0.
         tem11(i,j,k)=0.
         tem12(i,j,k)=0.
        END DO
      END DO
    END DO

   CALL adtlmmixuvw(nx,ny,nz,nstepss,nbasic,                             &
               u    (1,1,1,tlevel),v   (1,1,1,tlevel),                   &
               w    (1,1,1,tlevel),                                      &
               ptprt(1,1,1,tlevel),pprt(1,1,1,tlevel),                   &
               qv   (1,1,1,tlevel),qscalar(1,1,1,:,tlevel),              &
               tke  (1,1,1,tpresent),ubar,vbar,ptbar,                    &
               pbar,rhostr,qvbar,                                        &
               usflx,vsflx, x,y,z,zp, j1,j2,j3,stabchk,                  &
               uforce,vforce,wforce, km, lendel,defsq,                   &
               tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,             &
               tem10,tem11,tem12)
!
!
    sum3=0.
    CALL product(u(1,1,1,tlevel),bem1,                                   &
                 nx,ny,nz,1,nx,1,ny-1,1,nz-1,sum2)
    sum3=sum3+sum2
!
    CALL product(v(1,1,1,tlevel),bem2,                                   &
                 nx,ny,nz,1,nx-1,1,ny,1,nz-1,sum2)
    sum3=sum3+sum2
!
    CALL product(w(1,1,1,tlevel),bem3,                                   &
                 nx,ny,nz,1,nx-1,1,ny-1,1,nz,sum2)
    sum3=sum3+sum2

    PRINT*,'Tmix(AQ)*(AQ) sum1 = ',sum1
    PRINT*,'Tmix(AQ)*(AQ) sum3 = ',sum3
    STOP
  END IF

END IF !turn off mixing term
!
!    verification  111111!!!!
!  CALL PRODUCT(wforce,wforce,
!    :             nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
!  print*,'wforce=sum2 is that right=111==',sum2
!
!
!  IF( (tmixopt.ne.0 .or. cmix2nd.ne.0 .or. cmix4th.ne.0 )
!    :    .and. lvldbg.ge.4 ) THEN
!    CALL checkuhx(uforce, nx,ny,nz, 'umixx', tem1)
!    CALL checkuhy(uforce, nx,ny,nz, 'umixy', tem1)
!    CALL checkvhx(vforce, nx,ny,nz, 'vmixx', tem1)
!    CALL checkvhy(vforce, nx,ny,nz, 'vmixy', tem1)
!    CALL checkwhx(wforce, nx,ny,nz, 'wmixx', tem1)
!    CALL checkwhy(wforce, nx,ny,nz, 'wmixy', tem1)
!  ENDIF
!
!
!-----------------------------------------------------------------------
!
!  Calculate the advection terms in the momentum equations, using
!  the equivalent advective formulation.
!
!  On exiting advuvw, the advection terms are stored in temporary
!  arrays tem1, tem2 and tem3, they are then added to uforce, vforce
!  and wforce respectively, which already contain the mixing
!  terms for the u, v and w equations respectively.
!
!-----------------------------------------------------------------------
!
!
  IF ( 1==0 ) THEN !to verify the adjoint of advection term, need 2 modifications
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          tem1(i,j,k)=0.
          tem2(i,j,k)=0.
          tem3(i,j,k)=0.
          tem4(i,j,k)=0.
          tem5(i,j,k)=0.
          tem6(i,j,k)=0.
          tem7(i,j,k)=0.
          tem8(i,j,k)=0.
          tem9(i,j,k)=0.
!
          bem1(i,j,k) = wcont(i,j,k)
          bem2(i,j,k) =u(i,j,k,tpresent)
          bem3(i,j,k) =v(i,j,k,tpresent)
          bem4(i,j,k) =w(i,j,k,tpresent)
!
        END DO
      END DO
    END DO
  END IF
          tem1 =0.
          tem2 =0.
          tem3 =0.
          tem4 =0.
          tem5 =0.
          tem6 =0.
          tem7 =0.
          tem8 =0.
          tem9 =0.
!
  CALL tlmadvuvw(nx,ny,nz,nstepss,nbasic,                               &
              u,v,w,wcont,rhostr,ubar,vbar,nwcont,                      &
                        ubk,vbk,wbk,                                    &
              nustr,nvstr,nwstr,                                        &
              tem1,tem2,tem3,                 & ! uadv,vadv and wadv.
              tem4,tem5,tem6,tem7,tem8,tem9)
!
!
  IF (1==0) THEN !verify the adjoint of advuvw, need two modifications
    sum1=0.
    CALL product(tem1,tem1,                                             &
                 nx,ny,nz,2,nx-1,1,ny-1,2,nz-2,sum2)
    sum1=sum1+sum2

    CALL product(tem2,tem2,                                             &
                 nx,ny,nz,1,nx-1,2,ny-1,2,nz-2,sum2)
    sum1=sum1+sum2

    CALL product(tem3,tem3,                                             &
                 nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
    sum1=sum1+sum2
    PRINT*,'(AQ)*(AQ) sum1 = ',sum1
!
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          u(i,j,k,tpresent)=0.
          w(i,j,k,tpresent)=0.
          v(i,j,k,tpresent)=0.
          wcont(i,j,k)=0.

          tem4(i,j,k)=0.
          tem5(i,j,k)=0.
          tem6(i,j,k)=0.
          tem7(i,j,k)=0.
          tem8(i,j,k)=0.
          tem9(i,j,k)=0.
        END DO
      END DO
    END DO
!
    CALL adtlmadvuvw(nx,ny,nz,nstepss,nbasic,                           &
        u,v,w,wcont,rhostr,ubar,vbar,nwcont,                            &
                  ubk,vbk,wbk,                                          &
        nustr,nvstr,nwstr,                                              &
        tem1,tem2,tem3,                           & ! uadv, vadv and wadv.
        tem4,tem5,tem6,tem7,tem8,tem9)

    sum3=0.
    CALL product(wcont,bem1,                                            &
                 nx,ny,nz,1,nx-1,1,ny-1,1,nz,sum2)
    sum3=sum3+sum2
    CALL product(u(1,1,1,tpresent),bem2,                                &
                 nx,ny,nz,1,nx,1,ny-1,1,nz-1,sum2)
    sum3=sum3+sum2
!
    CALL product(v(1,1,1,tpresent),bem3,                                &
                 nx,ny,nz,1,nx-1,1,ny,1,nz-1,sum2)
    sum3=sum3+sum2
!
    CALL product(w(1,1,1,tpresent),bem4,                                &
                 nx,ny,nz,1,nx-1,1,ny-1,1,nz,sum2)
    sum3=sum3+sum2

    PRINT*,' Q*A*(AQ) sum3 = ',sum3
    STOP
  END IF !to verify ajoint of advection force

  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        uforce(i,j,k)=uforce(i,j,k)-tem1(i,j,k)
      END DO
    END DO
  END DO

  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        vforce(i,j,k)=vforce(i,j,k)-tem2(i,j,k)
      END DO
    END DO
  END DO

  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        wforce(i,j,k)=wforce(i,j,k)-tem3(i,j,k)
      END DO
    END DO
  END DO
!
!
!-----------------------------------------------------------------------
!
!  Calculate the Coriolis terms in u, v and w equations.
!
!  They are temporarily stored in tem1 and tem2, then added
!  to uforce and vforce.
!
!-----------------------------------------------------------------------
!
!
 IF( coriopt /= 0 ) THEN
! IF( 1==2 ) THEN !turn off coriolis term

    tlevel = tpresent

    IF (1 == 0) THEN
!
      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            bem1(i,j,k)=u(i,j,k,tpresent)
            bem2(i,j,k)=v(i,j,k,tpresent)
            bem3(i,j,k)=w(i,j,k,tpresent)
          END DO
        END DO
      END DO
!
    END IF

          tem1 = 0.
          tem2 = 0.
          tem3 = 0.
          tem4 = 0.
          tem5 = 0.
          tem6 = 0.
          tem7 = 0.
!
    CALL tlmcoriol(nx,ny,nz,                                            &
                u(1,1,1,tlevel),v(1,1,1,tlevel),w(1,1,1,tlevel),        &
                ubar,vbar,rhostr, sinlat,                               &
                tem1,tem2,tem3,tem4,tem5,tem6,tem7)

!
!
    IF (1 == 0) THEN

      sum1=0.
      CALL product(tem1,tem1,                                           &
                   nx,ny,nz,2,nx-1,1,ny-1,2,nz-2,sum2)
      sum1=sum1+sum2
      CALL product(tem2,tem2,                                           &
                   nx,ny,nz,1,nx-1,2,ny-1,2,nz-2,sum2)
      sum1=sum1+sum2
      CALL product(tem3,tem3,                                           &
                   nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
      sum1=sum1+sum2

      PRINT*,'w(AQ)*(AQ)= ',sum1

      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            u(i,j,k,tpresent)=0.
            v(i,j,k,tpresent)=0.
            w(i,j,k,tpresent)=0.
            tem4(i,j,k) = 0.
            tem5(i,j,k) = 0.
            tem6(i,j,k) = 0.
            tem7(i,j,k) = 0.
          END DO
        END DO
      END DO

      CALL adtlmcoriol(nx,ny,nz,                                        &
                 u(1,1,1,tlevel),v(1,1,1,tlevel),w(1,1,1,tlevel),       &
                 ubar,vbar,rhostr, sinlat, tem1,tem2, tem3,             &
                 tem4,tem5,tem6,tem7)
!!
!!
      sum3=0.
      CALL product(bem1,u(1,1,1,tlevel),                                &
                   nx,ny,nz,1,nx,1,ny,1,nz,sum2)
      sum3=sum3+sum2

      CALL product(bem2,v(1,1,1,tlevel),                                &
                   nx,ny,nz,1,nx,1,ny,1,nz,sum2)
      sum3=sum3+sum2

      CALL product(bem3,w(1,1,1,tlevel),                                &
                   nx,ny,nz,1,nx,1,ny,1,nz,sum2)
      sum3=sum3+sum2

      PRINT*,'VER1w(AQ)*(AQ)= ',sum1
      PRINT*,'VER2w(AQ)*(AQ)= ',sum3
      STOP
!
    END IF                ! IF (1 == 0) then

    DO k=2,nz-2
      DO j=1,ny-1
        DO i=2,nx-1
          uforce(i,j,k)=uforce(i,j,k)+tem1(i,j,k)
        END DO
      END DO
    END DO

    DO k=2,nz-2
      DO j=2,ny-1
        DO i=1,nx-1
          vforce(i,j,k)=vforce(i,j,k)+tem2(i,j,k)
        END DO
      END DO
    END DO

    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          wforce(i,j,k)=wforce(i,j,k)+tem3(i,j,k)
        END DO
      END DO
    END DO


  END IF !turn off coriolis term
!
!-----------------------------------------------------------------------
!
!  Calculate the buoyancy term for the w-equation.
!  The buoyancy term is stored in tem1 then added to wforce.
!
!  If buoyopt = 0 then buoyancy is turned off.
!
!-----------------------------------------------------------------------
!

  tlevel =  tpresent

! IF ( buoyopt /= 0 ) THEN
    CALL tlmbuoycy(nx,ny,nz,nstepss,nbasic,                             &
                ptprt(1,1,1,tlevel),pprt(1,1,1,tlevel),                 &
                qv(1,1,1,tlevel),qscalar(1,1,1,:,tlevel),               &
                ptbar,pbar,rhostr,qvbar,                                &
                tem1,                                  & ! wbuoy
                tem2)
!-----------------------------------------------------------------------
!
!  Code for verifying the correctness of the adjoint.
!
!-----------------------------------------------------------------------
 IF(1==0) THEN  !verify the adjoint of buoyancy ,need 1 modification
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          tem3(i,j,k)=ptprt(i,j,k,tlevel)
          tem4(i,j,k)=pprt(i,j,k,tlevel)
          tem5(i,j,k)=  qv(i,j,k,tlevel)
        END DO
      END DO
    END DO

    CALL tlmbuoycy(nx,ny,nz,nstepss,nbasic,                             &
                ptprt(1,1,1,tlevel),pprt(1,1,1,tlevel),                 &
                qv(1,1,1,tlevel),qscalar(1,1,1,:,tlevel),               &
                ptbar,pbar,rhostr,qvbar,                                &
                tem1,                                  & ! wbuoy
                tem2)


      sum1=0.
      CALL product(tem1,tem1,                                           &
                   nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
      sum1=sum1+sum2

      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            ptprt(i,j,k,tlevel) = 0.0
             pprt(i,j,k,tlevel) = 0.0
               qv(i,j,k,tlevel) = 0.0
          END DO
        END DO
      END DO

      CALL adtlmbuoycy(nx,ny,nz,nstepss,nbasic,                         &
                  ptprt(1,1,1,tlevel),pprt(1,1,1,tlevel),               &
                  qv(1,1,1,tlevel),qscalar(1,1,1,:,tlevel),             &
                  ptbar,pbar,rhostr,qvbar,                              &
                  tem1,tem2)                                            ! wbuoy
      sum3=0.
      CALL product(ptprt(1,1,1,tlevel),tem3,                            &
                   nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
      sum3=sum3+sum2
         print*,'ptprt sum2==',sum2
      CALL product(pprt(1,1,1,tlevel),tem4,                            &
                   nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
      sum3=sum3+sum2
         print*,'pprt sum2==',sum2
      CALL product(  qv(1,1,1,tlevel),tem5,                            &
                   nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
      sum3=sum3+sum2
         print*,'qvqv sum2==',sum2
!
      PRINT*,'buoyancy, VER3(AQ)*(AQ)= ',sum1
      PRINT*,'buoyancy, VER3(AQ)*(AQ)= ',sum3
      STOP
    END IF ! to verify ajoint of buoyancy

    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          wforce(i,j,k)=wforce(i,j,k) + tem1(i,j,k)
        END DO
      END DO
    END DO
!
!
! END IF !swith for buoyancy term
!
!
!-----------------------------------------------------------------------
!
!  To calculate additional boundary relaxation and mixing terms for
!  the wind fields in the case of externally forced boundary condition.
!
!  Think  that brlxuvw is linear. Please test it.
!
!-----------------------------------------------------------------------
!
! IF ( lbcopt == 2 .AND. mgrid == 1 ) THEN
!-----------------------------------------------------------------------
!
!  Code for verifying the correctness of the adjoint.
!
!-----------------------------------------------------------------------
!   IF(1 == 0) THEN !222
!     DO k=1,nz
!       DO j=1,ny
!         DO i=1,nx
!           tem1(i,j,k)=0.
!           tem2(i,j,k)=0.
!           tem3(i,j,k)=0.
!           tem4(i,j,k)=0.
!           tem5(i,j,k)=0.
!           tem6(i,j,k)=0.
!           tem7(i,j,k)=u(i,j,k,1)
!           tem8(i,j,k)=v(i,j,k,1)
!           tem9(i,j,k)=w(i,j,k,1)
!           bem1(i,j,k)=uforce(i,j,k)
!           bem2(i,j,k)=vforce(i,j,k)
!         END DO
!       END DO
!     END DO
!   END IF ! 222
!
!-----------------------------------------------------------------------
!   CALL tlmbrlxuvw( nx,ny,nz, dtbig1,                                  &
!                 u(1,1,1,1),v(1,1,1,1),w(1,1,1,1),rhostr,              &
!                 uforce,vforce,wforce,                                 &
!                 tem1,tem2,tem3,tem4,tem5,tem6 )
!-----------------------------------------------------------------------
!
!  Code for verifying the correctness of the adjoint.
!
!-----------------------------------------------------------------------
!
!   IF (1 == 0) THEN !
!     sum1=0.
!     CALL product(uforce,uforce,                                       &
!                  nx,ny,nz,2,nx-1,2,ny-2,1,nz-1,sum2)
!     sum1=sum1+sum2
!     CALL product(vforce,vforce,                                       &
!                  nx,ny,nz,2,nx-2,2,ny-1,1,nz-1,sum2)
!     sum1=sum1+sum2
!
!     CALL adtlmbrlxuvw( nx,ny,nz, dtbig1,                              &
!                   u(1,1,1,1),v(1,1,1,1),w(1,1,1,1),rhostr,            &
!                   uforce,vforce,wforce,                               &
!                   tem1,tem2,tem3,tem4,tem5,tem6 )
!     sum3=0.
!     CALL product(u(1,1,1,1),tem7,                                     &
!                  nx,ny,nz,1,nx,1,ny-1,1,nz-1,sum2)
!     sum3=sum3+sum2
!     CALL product(v(1,1,1,1),tem8,                                     &
!                  nx,ny,nz,1,nx-1,1,ny,1,nz-1,sum2)
!     sum3=sum3+sum2
!     CALL product(bem1,uforce,                                         &
!                  nx,ny,nz,2,nx-1,2,ny-2,1,nz-1,sum2)
!     sum3=sum3+sum2
!     CALL product(bem2,vforce,                                         &
!                  nx,ny,nz,2,nx-2,2,ny-1,1,nz-1,sum2)
!     sum3=sum3+sum2
!     PRINT*,'(AQ)*(AQ)= ',sum1,' Q*A*(AQ)= ',sum3
!     STOP
!   END IF
! END IF

  RETURN
END SUBROUTINE tlmfrcuvw
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TLMPGRAD                   ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tlmpgrad(nx,ny,nz, pprt,                                     &
           j1,j2,j3, upgrad,vpgrad,wpgrad,tem1,tem2)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the pressure gradient terms in the momentum equations.
!  These terms are evaluated every small time step.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91.
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  4/10/93 (M. Xue & Hao Jin)
!  Add the terrain.
!
!  5/25/93 (M. Xue & K. Brewster)
!  Fixed and error in the vertical pressure gradient force term.
!  The error is present in the finite differenced w equation
!  of ARPS 3.0 User's guide. The equation in continuous form is
!  correct.
!
!  9/10/94 (D. Weber & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical direction.
!
!    pprt     Perturbation pressure at a given time level (Pascal)
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    upgrad   Pressure gradient force in u-eq. (kg/(m*s)**2)
!    vpgrad   Pressure gradient force in v-eq. (kg/(m*s)**2)
!    wpgrad   Pressure gradient force in w-eq. (kg/(m*s)**2)
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: pprt  (nx,ny,nz)     ! Perturbation pressure at a given time
                               ! level (Pascal).

  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! -d(zp)/dx.
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! -d(zp)/dy.
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! d(zp)/dz.

  REAL :: upgrad(nx,ny,nz)     ! Pressure gradient force in u-eq.
                               ! (kg/(m*s)**2)
  REAL :: vpgrad(nx,ny,nz)     ! Pressure gradient force in v-eq.
                               ! (kg/(m*s)**2)
  REAL :: wpgrad(nx,ny,nz)     ! Pressure gradient force in w-eq.

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: onvf
  INTEGER :: i,j,k
  REAL :: sum1,sum2,sum3
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!-----------------------------------------------------------------------
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  tem1 = 0.
  upgrad=0.
  vpgrad=0.
  wpgrad=0.
!
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=pprt(i,j,k)*j3(i,j,k)
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  d(j3*pprt)/dx at u point - 1st component of p grad. force in x-dir
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL difx0(tem1, onvf,                                                 &
            nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, dx, upgrad)

!   CALL product(upgrad,upgrad,                                         &
!                nx,ny,nz,2,nx-1,1,ny-1,2,nz-2,sum2)
!   print*,'upgrad=sum2==================',sum2
!
!-----------------------------------------------------------------------
!
!  d(j3*pprt)/dy at v point - 1st component of p grad. force in y-dir
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL dify0(tem1, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, dy, vpgrad)

!   CALL product(vpgrad,vpgrad,                                         &
!                nx,ny,nz,1,nx-1,2,ny-1,2,nz-2,sum2)
!   print*,'vpgrad=sum2==================',sum2
!
!-----------------------------------------------------------------------
!
!  d(pprt)/dz at w point - p grad. force in z-dir
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL difz0(pprt, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, dz, wpgrad)

!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,2,2   ,sum2)
!   print*,'wpgrad====sum2====2==========',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,3,3   ,sum2)
!   print*,'wpgrad====sum2====3==========',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,4,4   ,sum2)
!   print*,'wpgrad====sum2====4==========',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,5,5   ,sum2)
!   print*,'wpgrad====sum2====5==========',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,6,6   ,sum2)
!   print*,'wpgrad====sum2====6==========',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,7,7   ,sum2)
!   print*,'wpgrad====sum2====7==========',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,8,8   ,sum2)
!   print*,'wpgrad====sum2====8==========',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,9,9   ,sum2)
!   print*,'wpgrad====sum2====9==========',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,10,10  ,sum2)
!   print*,'wpgrad====sum2===10==========',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,11,11  ,sum2)
!   print*,'wpgrad====sum2===11==========',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,12,12  ,sum2)
!   print*,'wpgrad====sum2===12==========',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,13,13  ,sum2)
!   print*,'wpgrad====sum2===13==========',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,14,14  ,sum2)
!   print*,'wpgrad====sum2===14==========',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,15,15  ,sum2)
!   print*,'wpgrad====sum2===15==========',sum2
!
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,nz-1,nz-1,sum2)
!   print*,'wpgrad====sum2====nz-1=======',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,nz-2,nz-2,sum2)
!   print*,'wpgrad====sum2====nz-2=======',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,nz-3,nz-3,sum2)
!   print*,'wpgrad====sum2====nz-3=======',sum2
!   CALL product(wpgrad,wpgrad,                                         &
!                nx,ny,nz,1,nx-1,1,ny-1,nz-4,nz-4,sum2)
!   print*,'wpgrad====sum2====nz-4=======',sum2
!
!-----------------------------------------------------------------------
!
!  If there is no terrain, i.e. the ground is flat, skip the
!  following calculations.
!
!-----------------------------------------------------------------------
!
  IF( ternopt /= 0 ) THEN
!
!-----------------------------------------------------------------------
!
!  d(j1*pprt)/dz at u point - 2nd component of p grad. force in x-dir
!  due to terrain
!
!-----------------------------------------------------------------------
!
    tem1 = 0.
    tem2 = 0.
!
    onvf = 1
    CALL avgz0(pprt, onvf,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem2)

    onvf = 1
    CALL avgx0(tem2, onvf,                                               &
              nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1, tem1)

    DO k=2,nz-1
      DO j=1,ny-1
        DO i=2,nx-1
          tem1(i,j,k)= tem1(i,j,k)*j1(i,j,k)
        END DO
      END DO
    END DO

    onvf = 0
    CALL difz0(tem1, onvf,                                               &
              nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, dz, tem2)

    DO k=2,nz-2
      DO j=1,ny-1
        DO i=2,nx-1
          upgrad(i,j,k)=upgrad(i,j,k)+tem2(i,j,k)
        END DO
      END DO
    END DO
!
!-----------------------------------------------------------------------
!
!  d(j2*pprt)/dz at v point - 2nd component of p grad. force in y-dir
!  due to terrain
!
!-----------------------------------------------------------------------
!
    onvf = 1
    CALL avgz0(pprt, onvf,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem2)

    onvf = 1
    CALL avgy0(tem2, onvf,                                               &
              nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, tem1)

    DO k=2,nz-1
      DO j=2,ny-1
        DO i=1,nx-1
          tem1(i,j,k)= tem1(i,j,k)*j2(i,j,k)
        END DO
      END DO
    END DO

    onvf = 0
    CALL difz0(tem1, onvf,                                               &
              nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, dz, tem2)

    DO k=2,nz-2
      DO j=2,ny-1
        DO i=1,nx-1
          vpgrad(i,j,k)=vpgrad(i,j,k)+tem2(i,j,k)
        END DO
      END DO
    END DO

  END IF

  RETURN
END SUBROUTINE tlmpgrad
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TLMADVCTS                  ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tlmadvcts(nx,ny,nz,nstepss,nbasic,                           &
           s,u,v,ustr,vstr,wstr,sbar,nustr,nvstr,nwstr,                 &
           ns, sadv,tem1,tem2,tem3)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the TLM scalar equation advection terms. These terms are
!  written in equivalent advection form.
!  For its verification, please see vertlmsolvpt.f in "back".
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  06/03/1994
!
!  MODIFICATION HISTORY:
!  05/05/96 (Jidong Gao)
!  Added full documentation
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nstepss  Index of total current basic state time levels.
!    nbasic   Index of the current basic state time level.
!
!    s        scalar field at a given time level (scalar units)
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!  OUTPUT:
!
!    sadv     Advection term in scalar equation (kg/m**3)*scalar unit/s
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    Subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz

  INTEGER :: nt           ! The no. of t-levels of t-dependent arrays.
  INTEGER :: tpast        ! Index of time level for the past time.
  INTEGER :: tpresent     ! Index of time level for the present time.
  INTEGER :: tfuture      ! Index of time level for the future time.
  INTEGER :: nstepss      ! Index of total current basic state time levels.
  INTEGER :: nbasic       ! Index of the current basic state time level.
  INTEGER :: npast        ! Temporary variable.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  REAL :: s     (nx,ny,nz,nt)  ! a scalar field to be advected.
  REAL :: ns    (nx,ny,nz,nstepss)  ! a basic scalar field to be advected.
  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)

  REAL :: ustr  (nx,ny,nz)     ! u * rhostr
  REAL :: vstr  (nx,ny,nz)     ! v * rhostr
  REAL :: wstr  (nx,ny,nz)     ! w * rhostr
  REAL :: sbar  (nx,ny,nz)     ! A state of 's' towards which 's' is
                               ! relaxed on the inflow boundaries.
!
  REAL :: nustr  (nx,ny,nz)     ! nu * rhostr
  REAL :: nvstr  (nx,ny,nz)     ! nv * rhostr
  REAL :: nwstr  (nx,ny,nz)     ! nw * rhostr
!
!
  REAL :: sadv  (nx,ny,nz)     ! advection term in scalar equation
                             ! (kg/m**3)*(scalar units)/s
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,onvf
  INTEGER :: ibgn,iend,jbgn,jend
  REAL :: ueb,uwb,vsb,vnb,nueb,nuwb,nvsb,nvnb,sum3
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'        ! Physical constants
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'bndry.inc'         ! Parameters for boundary conditions.
!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
  EXTERNAL avgx0
  EXTERNAL avg2x0
  EXTERNAL avgy0
  EXTERNAL avg2y0
  EXTERNAL avgz0
  EXTERNAL dif2x0
  EXTERNAL difx0
  EXTERNAL dif2y0
  EXTERNAL dify0
  EXTERNAL difz0
  EXTERNAL aamult0
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!   sadvopt=1
!   rlxlbc=0.78
!   dx=2500.
!   dy=2500.
!   dz= 500.
!   dxinv=1/dx
!   dyinv=1/dy
!   dzinv=1/dz
!
!-----------------------------------------------------------------------
!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tem1(i,j,k)= 0.
        tem2(i,j,k)= 0.
        tem3(i,j,k)= 0.
        sadv(i,j,k)= 0.
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Calculate rho*u'* ds/dx + rho*u* ds'/dx
!            = avgx0( ustr' * difx0( s ) )
!            + avgx0( ustr * difx0( s' ) )
!
!-----------------------------------------------------------------------
!
!  We first calculate avgx0( ustr' * difx0( s ) ).
!  avgx0 is linear!!!
!
  onvf = 1
  CALL difx0(ns(1,1,1,nbasic), onvf,                                     &
        nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, dx ,tem1)
!
!  tem1 contains ds/dx
!
  CALL aamult0(ustr, tem1,                                               &
        nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, tem1)
!
!  tem1 contains (ustr')*ds/dx
!
  onvf = 0
  CALL avgx0(tem1, onvf,                                                 &
        nx,ny,nz, 2,nx-2, 1,ny-1, 2,nz-2, sadv)

!
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tem1(i,j,k)= 0.
      END DO
    END DO
  END DO
!-----------------------------------------------------------------------
!  Now we calculate avgx0( ustr * difx0( s' ) ).
!
  onvf = 1
  CALL difx0(s(1,1,1,tpresent), onvf,                                    &
        nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, dx ,tem1)
!
!  tem1 contains ds'/dx
!
  CALL aamult0(nustr, tem1,                                              &
        nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, tem1)
!
!  tem1 contains (ustr)*ds/dx
!
  onvf = 0
  CALL avgx0(tem1, onvf,                                                 &
        nx,ny,nz, 2,nx-2, 1,ny-1, 2,nz-2, tem2)

  DO k=2,nz-2
    DO j=1,ny-1
      DO i=1,nx-1
        sadv(i,j,k)=sadv(i,j,k)+tem2(i,j,k)
      END DO
    END DO
  END DO
!
!   sadv=rho*u'* ds/dx + rho*u* ds'/dx.
!
  IF (1 == 0) THEN
    PRINT*,'We are in tlmadvv, Dude, Part I-2'
    sum3=0.
    DO k=2,nz-2
      DO j=1,ny-1
        DO i=1,nx-1
          sum3=sum3+tem2(i,j,k)
        END DO
      END DO
    END DO
    PRINT*,'vadv= ',sum3
    PRINT*,'We are in tlmadvv, Dude, Part I-2'
    sum3=0.
    DO k=2,nz-2
      DO j=1,ny-1
        DO i=1,nx-1
          sum3=sum3+sadv(i,j,k)
        END DO
      END DO
    END DO
    PRINT*,'vadv= ',sum3
  END IF
!-----------------------------------------------------------------------
!
!     For 4th-order scalar advection, calculate the 4th-order
!     adjustment term for ustr*ds/dx:
!          avg2x0( avgx0(ustr) * dif2x0(s) )
!
!-----------------------------------------------------------------------
!
! IF (sadvopt == 2) THEN
  IF (sadvopt == -998) THEN

    onvf = 0
    CALL avgx0(ustr, onvf,                                               &
          nx,ny,nz, 2,nx-2, 1,ny-1, 2,nz-2, tem1)
    CALL dif2x0(s(1,1,1,tpresent),                                       &
          nx,ny,nz, 2,nx-2, 1,ny-1, 2,nz-2, dx ,tem2)
!
    CALL aamult0(tem1, tem2,                                             &
          nx,ny,nz, 2,nx-2, 1,ny-1, 2,nz-2, tem2)
!
!  At this point tem1 contains (ustr)*ds/dx
!
!
!
!-----------------------------------------------------------------------
!
!     In the case of periodic boundary condition, need to calculate
!     the 4th-order term for one additional point near boundary.
!
!-----------------------------------------------------------------------

    ibgn = 3
    iend = nx-3
    jbgn = 1
    jend = ny-1

    IF (ebc == 2) THEN
      iend = nx-2
      DO j=jbgn,jend
        DO k=2,nz-2
          tem2(nx-1,j,k) = tem2(2,j,k)
        END DO
      END DO
    END IF

    IF (wbc == 2) THEN
      ibgn = 2
      DO j=jbgn,jend
        DO k=2,nz-2
          tem2(1,j,k) = tem2(nx-2,j,k)
        END DO
      END DO
    END IF

!-----------------------------------------------------------------------
!
!     In the case of rigid boundary condition, need to calculate
!     the 4th-order term for one additional point near boundary.
!
!-----------------------------------------------------------------------

    IF (ebc == 1) THEN
      iend = nx-2
      DO j=jbgn,jend
        DO k=2,nz-2
          tem2(nx-1,j,k) = tem2(nx-2,j,k)
        END DO
      END DO
    END IF

    IF (wbc == 1) THEN
      ibgn = 2
      DO j=jbgn,jend
        DO k=2,nz-2
          tem2(1,j,k) = tem2(2,j,k)
        END DO
      END DO
    END IF

!-----------------------------------------------------------------------
!
!       Finalize the 4th-order adjustment term by averaging over the
!     specified domain.
!
!-----------------------------------------------------------------------

    CALL avg2x0(tem2,                                                    &
          nx,ny,nz, ibgn,iend, jbgn,jend, 2,nz-2, tem1)

!
!  At this point tem1 contains the 4th-order adjustment term.
!
!-----------------------------------------------------------------------
!
!  Concatenate the 2nd- and 4th- order terms for ustr*ds/dx
!
!-----------------------------------------------------------------------

    DO  k=2,nz-2
      DO  j=jbgn,jend
        DO  i=ibgn,iend
          sadv(i,j,k) = 1.0/3.0*(4.0*sadv(i,j,k) - tem1(i,j,k))
        END DO
      END DO
    END DO

  END IF    !(IF sadvopt=2)

!
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tem1(i,j,k)= 0.
        tem2(i,j,k)= 0.
      END DO
    END DO
  END DO
!-----------------------------------------------------------------------
!
!  Calculate rho*u* ds/dx on the east and west boundaries
!  using one-sided advection.
!
!-----------------------------------------------------------------------
  IF (nbasic == 1) THEN
    npast=nbasic
  ELSE
    npast=nbasic-1
  END IF
!

  DO k=2,nz-2
    DO j=1,ny-1
      uwb=(ustr(1,j,k)+ustr(2,j,k))*0.5
      ueb=(ustr(nx-1,j,k)+ustr(nx,j,k))*0.5
      nuwb=(nustr(1,j,k)+nustr(2,j,k))*0.5
      nueb=(nustr(nx-1,j,k)+nustr(nx,j,k))*0.5
      IF (nuwb < 0.0) THEN
        sadv(1,j,k)=nuwb*(s(2,j,k,tpresent)-s(1,j,k,tpresent))*dxinv    &
            +uwb*(ns(2,j,k,nbasic)-ns(1,j,k,nbasic))*dxinv
      ELSE
        sadv(1,j,k)=rlxlbc*nuwb*(s(1,j,k,tpast))*dxinv                  &
            + rlxlbc*uwb*(ns(1,j,k,npast)-sbar(1,j,k))*dxinv
      END IF

      IF (nueb > 0.0) THEN
        sadv(nx-1,j,k)=nueb*                                            &
            (s(nx-1,j,k,tpresent)-s(nx-2,j,k,tpresent))*dxinv           &
            +ueb*(ns(nx-1,j,k,nbasic)-ns(nx-2,j,k,nbasic))*dxinv
      ELSE
        sadv(nx-1,j,k)=-rlxlbc*nueb*                                    &
            (s(nx-1,j,k,tpast))*dxinv                                   &
                       -rlxlbc*ueb*                                     &
            (ns(nx-1,j,k,npast)-sbar(nx-1,j,k))*dxinv
      END IF
    END DO
  END DO
!
!   print*,'The bcs at 100, Dude, Part I-2'
  sum3=0.
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=1,nx-1
        sum3=sum3+sadv(i,j,k)
      END DO
    END DO
  END DO
!   print*,'vadv,uwb,ueb,nuwb,nueb= ',sum3,uwb,ueb,nuwb,nueb
!   print*,'ustr(1,ny-1,nz-2),ustr(2,ny-1,nz-2)= ',
!  :  ustr(1,ny-1,nz-2),ustr(2,ny-1,nz-2)
!-----------------------------------------------------------------------
!
!  Calculate rho*v'* ds/dy + rho*v* ds'/dy
!          = avgy0( vstr' * dify0( s ) )
!          + avgy0( vstr * dify0( s' ) )
!
!-----------------------------------------------------------------------
!  We first calculate avgy0( vstr' * dify0( s ) ).
!
  onvf = 1
  CALL dify0(ns(1,1,1,nbasic), onvf,                                     &
        nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, dy, tem1)
!
!  tem1 contains ds/dy
!
  CALL aamult0(vstr, tem1,                                               &
        nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, tem1)
!
!  tem1 contains (v'*rho)*ds/dy
!
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tem2(i,j,k)= 0.
      END DO
    END DO
  END DO
  onvf = 0
  CALL avgy0(tem1, onvf,                                                 &
        nx,ny,nz, 1,nx-1, 2,ny-2, 2,nz-2, tem2)

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tem1(i,j,k)= 0.
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!  Now we calculate avgy0( vstr * dify0( s' ) ).
!
  onvf = 1
  CALL dify0(s(1,1,1,tpresent), onvf,                                    &
        nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, dy, tem1)
!
!  tem1 contains ds/dy
!
  CALL aamult0(nvstr, tem1,                                              &
        nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, tem1)
!
!  tem1 contains (v*rho)*ds'/dy
!
  onvf = 0
  CALL avgy0(tem1, onvf,                                                 &
        nx,ny,nz, 1,nx-1, 2,ny-2, 2,nz-2, tem3)
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=1,nx-1
        tem2(i,j,k)=tem2(i,j,k)+tem3(i,j,k)
      END DO
    END DO
  END DO
!
!    Now tem2=rho*v'* ds/dy + rho*v* ds'/dy.
!
  IF (1 == 0) THEN
    PRINT*,'We are in tlmadvv, Dude, Part II-2'
    sum3=0.
    DO k=2,nz-2
      DO j=1,ny-1
        DO i=1,nx-1
          sum3=sum3+tem3(i,j,k)
        END DO
      END DO
    END DO
    PRINT*,'vadv= ',sum3
    PRINT*,'We are in tlmadvv, Dude, Part II-2'
    sum3=0.
    DO k=2,nz-2
      DO j=1,ny-1
        DO i=1,nx-1
          sum3=sum3+tem2(i,j,k)
        END DO
      END DO
    END DO
    PRINT*,'vadv= ',sum3
  END IF
!-----------------------------------------------------------------------
!
!     For 4th-order scalar advection, calculate the 4th-order
!     adjustment term for vstr*ds/dy:
!          avg2y0( avgy0(vstr) * difsy(s) )
!
!-----------------------------------------------------------------------
!
! IF (sadvopt == 2) THEN
  IF (sadvopt == -998) THEN

    onvf = 0
    CALL avgy0(vstr, onvf,                                               &
          nx,ny,nz, 1,nx-1, 2,ny-2, 2,nz-2, tem1)
    CALL dif2y0(s(1,1,1,tpresent),                                       &
          nx,ny,nz, 1,nx-1, 2,ny-2, 2,nz-2, dy, tem3)
!
!  At this point tem3 contains ds/dy
!
    CALL aamult0(tem1, tem3,                                             &
          nx,ny,nz, 1,nx-1, 2,ny-2, 2,nz-2, tem3)
!
!  At this point tem3 contains (vstr)*ds/dy
!
!
!-----------------------------------------------------------------------
!
!     In the case of periodic boundary condition, need to calculate
!     the 4th-order term for one additional point near boundary.
!
!-----------------------------------------------------------------------

    ibgn = 1
    iend = nx-1
    jbgn = 3
    jend = ny-3

    IF (nbc == 2) THEN
      jend = ny-2
      DO i=ibgn,iend
        DO k=2,nz-2
          tem3(i,ny-1,k) = tem3(i,2,k)
        END DO
      END DO
    END IF

    IF (sbc == 2) THEN
      jbgn = 2
      DO i=ibgn,iend
        DO k=2,nz-2
          tem3(i,1,k) = tem3(i,ny-2,k)
        END DO
      END DO
    END IF

!-----------------------------------------------------------------------
!
!     In the case of rigid boundary condition, need to calculate
!     the 4th-order term for one additional point near boundary.
!
!-----------------------------------------------------------------------

    IF (nbc == 1) THEN
      jend = ny-2
      DO i=ibgn,iend
        DO k=2,nz-2
          tem3(i,ny-1,k) = tem3(i,ny-2,k)
        END DO
      END DO
    END IF

    IF (sbc == 1) THEN
      jbgn = 2
      DO i=ibgn,iend
        DO k=2,nz-2
          tem3(i,1,k) = tem3(i,2,k)
        END DO
      END DO
    END IF

!-----------------------------------------------------------------------
!
!       Finalize the 4th-order adjustment term by averaging over the
!     specified domain.
!
!-----------------------------------------------------------------------

    CALL avg2y0(tem3,                                                    &
          nx,ny,nz, ibgn,iend, jbgn,jend, 2,nz-2, tem1)

!
!  At this point tem1 contains the 4th-order adjustment term.
!
!-----------------------------------------------------------------------
!
!  Concatenate the 2nd- and 4th-order terms for vstr*ds/dy.
!
!-----------------------------------------------------------------------

    DO k=2,nz-2
      DO j=jbgn,jend
        DO i=ibgn,iend
          tem2(i,j,k) = 1.0/3.0*(4.0*tem2(i,j,k) - tem1(i,j,k))
        END DO
      END DO
    END DO

  END IF   ! (IF sadvopt=2)
!
!-----------------------------------------------------------------------
!
!  Calculate rho*v* ds/dy on the north and south boundaries
!  using one-sided advection.
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-2
    DO i=1,nx-1
      vsb=(vstr(i,1,k)+vstr(i,2,k))*0.5
      vnb=(vstr(i,ny-1,k)+vstr(i,ny,k))*0.5
      nvsb=(nvstr(i,1,k)+nvstr(i,2,k))*0.5
      nvnb=(nvstr(i,ny-1,k)+nvstr(i,ny,k))*0.5
      IF (nvsb < 0.0) THEN
        tem2(i,1,k)=nvsb*(s(i,2,k,tpresent)-s(i,1,k,tpresent))*dyinv    &
            +  vsb*(ns(i,2,k,nbasic)-ns(i,1,k,nbasic))*dyinv
      ELSE
        tem2(i,1,k)=rlxlbc*nvsb*(s(i,1,k,tpast))*dyinv                  &
            +rlxlbc*vsb*(ns(i,1,k,npast)-sbar(i,1,k))*dyinv
      END IF

      IF (nvnb > 0.0) THEN
        tem2(i,ny-1,k)=nvnb*                                            &
            (s(i,ny-1,k,tpresent)-s(i,ny-2,k,tpresent))*dyinv           &
            +vnb*(ns(i,ny-1,k,nbasic)-ns(i,ny-2,k,nbasic))*dyinv
      ELSE
        tem2(i,ny-1,k)=-rlxlbc*nvnb*                                    &
            (s(i,ny-1,k,tpast))*dyinv                                   &
                       -rlxlbc*vnb*                                     &
            (ns(i,ny-1,k,npast)-sbar(i,ny-1,k))*dyinv
      END IF
    END DO
  END DO
  IF (1 == 0) THEN
    PRINT*,'We are at bcs 200, Dude, Part II-2'
    sum3=0.
    DO k=2,nz-2
      DO j=1,ny-1
        DO i=1,nx-1
          sum3=sum3+tem2(i,j,k)
        END DO
      END DO
    END DO
    PRINT*,'vadv= ',sum3
  END IF
!
!-----------------------------------------------------------------------
!
!  Add the scalar equation horizontal advection terms, rho*u*ds/dx (sadv)
!  and  rho*v*ds/dy (tem2).
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=1,nx-1
        sadv(i,j,k)=sadv(i,j,k)+tem2(i,j,k)
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tem1(i,j,k)= 0.
        tem2(i,j,k)= 0.
        tem3(i,j,k)= 0.
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Calculate rho*w'* ds/dz + rho*w* ds'/dz
!            = avgz0( wstr' * difz0( s ) )
!            + avgz0( wstr * difz0( s' ) )
!
!-----------------------------------------------------------------------
!  We first calculate rho*w'* ds/dz.
!
  onvf = 1
  CALL difz0(ns(1,1,1,nbasic), onvf,                                     &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, dz, tem1)
!
!  tem1 contains ds/dz
!
  CALL aamult0(wstr, tem1,                                               &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem1)
!
!  tem1 contains (w'*rho)*ds/dz
!
  onvf = 0
  CALL avgz0(tem1, onvf,                                                 &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem2)
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tem1(i,j,k)= 0.
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!  Now we calculate rho*w* ds'/dz.
!
  onvf = 1
  CALL difz0(s(1,1,1,tpresent), onvf,                                    &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, dz, tem1)
!
!  tem1 contains ds/dz
!
  CALL aamult0(nwstr, tem1,                                              &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem1)
!
!  tem1 contains (w*rho)*ds/dz
!
  onvf = 0
  CALL avgz0(tem1, onvf,                                                 &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem3)
!
!-----------------------------------------------------------------------
!
!  Add the scalar equation vertical advection term rho*w*ds/dz (tem2)
!  to the horizontal advection terms.
!
!-----------------------------------------------------------------------
  IF (1 == 0) THEN
    PRINT*,'We are in tlmadvv, Dude, Part III-2'
    sum3=0.
    DO k=2,nz-2
      DO j=1,ny-1
        DO i=1,nx-1
          sum3=sum3+tem2(i,j,k)
        END DO
      END DO
    END DO
    PRINT*,'vadv= ',sum3
    PRINT*,'We are in tlmadvv, Dude, Part III-2'
    sum3=0.
    DO k=2,nz-2
      DO j=1,ny-1
        DO i=1,nx-1
          sum3=sum3+tem3(i,j,k)
        END DO
      END DO
    END DO
    PRINT*,'vadv= ',sum3
  END IF
!
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=1,nx-1
        sadv(i,j,k)=sadv(i,j,k)+tem2(i,j,k)+tem3(i,j,k)
      END DO
    END DO
  END DO

  IF (1 == 0) THEN
    PRINT*,'We are in tlmadvv, Dude'
    sum3=0.
    DO k=2,nz-2
      DO j=1,ny-1
        DO i=1,nx-1
          sum3=sum3+sadv(i,j,k)*sadv(i,j,k)
        END DO
      END DO
    END DO
    PRINT*,'vadv= ',sum3
  END IF
  RETURN
END SUBROUTINE tlmadvcts
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TLMCORIOL                  ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tlmcoriol(nx,ny,nz,                                          &
           u,v,w,ubar,vbar,rhostr, sinlat, ucorio,vcorio, wcorio,       &
           fcorio1, fcorio2, tem1,tem2)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the Coriolis force terms in the u, v and w equations.
!
!  TLMCORIOL is the tangent linear model of CORIOL.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  04/22/96
!
!  MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical direction.
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        z component of velocity at a given time level (m/s)
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    sinlat   Sin of latitude at each grid point
!
!  OUTPUT:
!
!    ucorio   Coriolis force term in u-eq. (kg/(m*s)**2)
!    vcorio   Coriolis force term in v-eq. (kg/(m*s)**2)
!    wcorio   Coriolis force term in w-eq. (kg/(m*s)**2)
!
!  WORK ARRAYS:
!
!    fcorio1  Temporary work array.
!    fcorio2  Temporary work array.
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity at a time level (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity at a time level (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity at a time level (m/s)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: sinlat(nx,ny)        ! Sin of latitude at each grid point

  REAL :: ucorio(nx,ny,nz)     ! Coriolis force term in u-eq.
  REAL :: vcorio(nx,ny,nz)     ! Coriolis force term in v-eq.
  REAL :: wcorio(nx,ny,nz)     ! Coriolis force term in w-eq.

  REAL :: fcorio1  (nx,ny)     ! Temporary work array.
  REAL :: fcorio2  (nx,ny)     ! Temporary work array.
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array.

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
  REAL :: omega2,sinclat,cosclat
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  IF( coriopt == 0 ) THEN

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          ucorio(i,j,k) = 0.0
          vcorio(i,j,k) = 0.0
          wcorio(i,j,k) = 0.0
        END DO
      END DO
    END DO

    RETURN

  END IF
!
!-----------------------------------------------------------------------
!
!  Wash up tem1 and tem2.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.
!
!-----------------------------------------------------------------------

  omega2 = 2.0* omega

  IF( coriopt == 1 ) THEN

    sinclat = SIN( ATAN(1.0)/45.0 * ctrlat )
    DO j=1,ny
      DO i=1,nx
        fcorio1(i,j) = omega2* sinclat
        fcorio2(i,j) = 0.0
      END DO
    END DO

  ELSE IF( coriopt == 2 ) THEN

    sinclat = SIN( ATAN(1.0)/45.0 * ctrlat )
    cosclat = SQRT( 1.0 - sinclat*sinclat  )
    DO j=1,ny
      DO i=1,nx
        fcorio1(i,j) = omega2* sinclat
        fcorio2(i,j) = omega2* cosclat
      END DO
    END DO

  ELSE IF( coriopt == 3 ) THEN

    DO j=1,ny
      DO i=1,nx
        fcorio1(i,j) = omega2* sinlat(i,j)
        fcorio2(i,j) = 0.0
      END DO
    END DO

  ELSE IF( coriopt == 4 ) THEN

    DO j=1,ny
      DO i=1,nx
        fcorio1(i,j) = omega2* sinlat(i,j)
        fcorio2(i,j) = omega2* SQRT( 1.0 - sinlat(i,j)**2 )
      END DO
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Coriolis terms in u-eq. if coriotrm=1
!
!    ucorio = avgx0( rhostr * (fcorio1*avgy0(v)-fcorio2*avgz0(w)) )
!
!  or if coriotrm=2,
!
!    ucorio = avgx0( rhostr * (fcorio1*avgy0(v-vbar)-fcorio2*avgz0(w)) )
!
!  where fcorio1 = 2*omega*sinlat.
!  and   fcorio2 = 2*omega* sqrt(1-sinlat**2).
!
!-----------------------------------------------------------------------
!
!
  IF( coriotrm == 1 ) THEN

    DO k=1,nz-1
      DO j=1,ny
        DO i=1,nx-1
          tem2(i,j,k) = v(i,j,k)
        END DO
      END DO
    END DO

  ELSE

    DO k=1,nz-1
      DO j=1,ny
        DO i=1,nx-1
          tem2(i,j,k) = v(i,j,k)
        END DO
      END DO
    END DO

  END IF

  onvf = 0
  CALL avgy0(tem2, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem1)

!-----------------------------------------------------------------------
!
!  Wash up tem2.
!
!-----------------------------------------------------------------------
!
        tem2 = 0.
!
!-----------------------------------------------------------------------
  onvf = 0
  CALL avgz0(w, onvf,                                                    &
            nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem2)

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k) = rhostr(i,j,k)*                                    &
            (fcorio1(i,j)*tem1(i,j,k)-fcorio2(i,j)*tem2(i,j,k))
      END DO
    END DO
  END DO

  onvf = 1
  CALL avgx0(tem1, onvf,                                                 &
            nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, ucorio)

!
!-----------------------------------------------------------------------
!
!  Coriolis terms in v-eq. and w-eq. if coriotrm=1:
!
!    vcorio = - avgy0( rhostr * fcorio1 * avgx0(u) )
!    wcorio = avgz0( rhostr * fcorio2 * avgx0(u) )
!
!  or if coriotrm=2:
!
!    vcorio = - avgy0( rhostr * fcorio1 * avgx0(u-ubar) )
!    wcorio = avgz0( rhostr * fcorio2 * avgx0(u) )
!
!
!-----------------------------------------------------------------------
!
  IF( coriotrm == 1 ) THEN
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx
          tem2(i,j,k) = u(i,j,k)
        END DO
      END DO
    END DO
  ELSE
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx
          tem2(i,j,k) = u(i,j,k)
        END DO
      END DO
    END DO
  END IF

!-----------------------------------------------------------------------
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
!
!-----------------------------------------------------------------------
  onvf = 0
  CALL avgx0(tem2, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem1)

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k) = - rhostr(i,j,k)*fcorio1(i,j)*tem1(i,j,k)
      END DO
    END DO
  END DO

  onvf = 1
  CALL avgy0(tem1, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, vcorio)

  IF( coriopt == 2 .OR. coriopt == 4 ) THEN

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx
          tem2(i,j,k) = u(i,j,k)
        END DO
      END DO
    END DO

    onvf = 0
    CALL avgx0(tem2, onvf,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem1)

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem1(i,j,k) = fcorio2(i,j) * rhostr(i,j,k)* tem1(i,j,k)
        END DO
      END DO
    END DO

    onvf = 1
    CALL avgz0(tem1, onvf,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, wcorio)

  END IF

  RETURN
END SUBROUTINE tlmcoriol
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TLMBUOYCY                  ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tlmbuoycy(nx,ny,nz,nstepss,nbasic,                           &
           ptprt,pprt,qv,qscalar,                                &
           ptbar,pbar,rhostr,qvbar, wbuoy, tem1)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the total buoyancy including liquid and solid water
!  loading.
!
!  TLMBUOYCY is the tangent linear model of BUOYCY.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  05/31/1994.
!
!  MODIFICATION HISTORY:
!
!  04/24/96 (Jidong Gao)
!  Converted to version 4.0.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical direction.
!    nstepss  The number of integrations plus one.
!    nbasic   Index of the current basic state time level.
!
!    ptprt    Perturbation potential temperature at a time level (K)
!    pprt     Perturbation pressure at a given time level (Pascal)
!    qv       Water vapor specific humidity at a given time level
!             (kg/kg)
!    qc       Cloud water mixing ratio at a given time level (kg/kg)
!    qr       Rainwater mixing ratio at a given time level (kg/kg)
!    qi       Cloud ice mixing ratio at a given time level (kg/kg)
!    qs       Snow mixing ratio at a given time level (kg/kg)
!    qh       Hail mixing ratio at a given time level (kg/kg)
!
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!
!    Basic state variables:
!    nu,nv,nw,nptprt,npprt,nqv,nqc,nqr,nqi,nqs,nqh,
!    nustr,nvstr,nwstr.
!
!
!  OUTPUT:
!
!    wbuoy    The total buoyancy force (kg/(m*s)**2)
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global model control parameters
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'      ! Physical constants
!
!
!-----------------------------------------------------------------------
!
!  Variable Declarations
!
!-----------------------------------------------------------------------
!

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  INTEGER :: nstepss      ! Index of total current basic state time levels.
  INTEGER :: nbasic       ! Index of the current basic state time level.
  REAL :: ptprt (nx,ny,nz)     ! Perturbation potential temperature
                               ! at a given time level (K)
  REAL :: pprt  (nx,ny,nz)     ! Perturbation pressure at a given time
                               ! level (Pascal)
  REAL :: qv    (nx,ny,nz)     ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq)     ! Cloud water mixing ratio (kg/kg)

  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific
                               ! humidity(kg/kg)

  REAL :: wbuoy(nx,ny,nz)      ! Total buoyancy in w-eq. (kg/(m*s)**2)

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array.

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,nq
  REAL :: g5,temp1

!-----------------------------------------------------------------------
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Wash up wbuoy.
!
!-----------------------------------------------------------------------
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        wbuoy(i,j,k)= 0.
        tem1(i,j,k)= 0.
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  The total buoyancy
!
!    wbuoy = rhostr*g ( ptprt/ptbar-pprt/(rhobar*csndsq)+
!    qvprt/(rddrv+qvbar)-(qvprt+qc+qr+qs+qi+qh)/(1+qvbar(i,j,k)) )
!
!  and rddrv=rd/rv, cp, cv, rd and rv are defined in phycst.inc.
!
!  Here, the contribution from pprt (i.e., term pprt/(rhobar*csndsq))
!  is evaluated inside the small time steps, therefore wbuoy
!  does not include this part.
!
!  The contribution from ptprt is calculated inside the small time
!  steps if the potential temperature equation is solved inside
!  small time steps, i.e., if ptsmlstp=1.
!
!-----------------------------------------------------------------------
!

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem1(i,j,k) = ptprt(i,j,k)/ptbar(i,j,k)                              &
            - pprt(i,j,k)/(cpdcv*pbar(i,j,k))

!           - pprt(i,j,k)/(cpdcv*pbar(i,j,k))                                  &
!           - 2.*ptprt(i,j,k)*nptprt(i,j,k,1)/ptbar(i,j,k)/ptbar(i,j,k)        &
!           - (1-cpdcv)/cpdcv/cpdcv*pprt(i,j,k)*npprt(i,j,k,1)                 &
!                                    /pbar(i,j,k)/pbar(i,j,k)                  &
!           + 0.5*( ptprt(i,j,k)*npprt(i,j,k,1)+pprt(i,j,k)*nptprt(i,j,k,1) )  &
!                 /ptbar(i,j,k)/pbar(i,j,k)

        END DO
      END DO
    END DO

!
!-----------------------------------------------------------------------
!
!  Add on the contributions to the buoyancy from the water vapor
!  content and the liquid and ice water loading.
!
!-----------------------------------------------------------------------
!

  IF( moist == 1 .AND. ice == 0 ) THEN  ! Moist case, no ice.

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem1(i,j,k) = tem1(i,j,k)                                     &
              + (qv(i,j,k) )/(rddrv + qvbar(i,j,k))                     &
              - (qv(i,j,k)+ qscalar(i,j,k,P_QC) + qscalar(i,j,k,P_QR))  &
              /( 1 + qvbar(i,j,k) )
        END DO
      END DO
    END DO

  ELSE IF(moist == 1 .AND. ice == 1) THEN
          ! Full microphysics case, loading of liquid and ice water.
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          temp1 = 0.0
          DO nq = 1, nscalarq
            temp1 = temp1 + qscalar(i,j,k,nq)
          END DO
          tem1(i,j,k) = tem1(i,j,k)                                     &
              + (qv(i,j,k) )/(rddrv + qvbar(i,j,k))                     &
              - (qv(i,j,k)+temp1 )                        &
              /( 1 + qvbar(i,j,k) )
        END DO
      END DO
    END DO

  END IF
!
!-----------------------------------------------------------------------
!
!  Then the total buoyancy:
!
!    wbuoy = tem1 * rhostr * g
!
!  averged to the w-point on the staggered grid.
!
!-----------------------------------------------------------------------
!
  g5 = g*0.5

  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        wbuoy(i,j,k)= (tem1(i,j, k )*rhostr(i,j, k )                    &
                      +tem1(i,j,k-1)*rhostr(i,j,k-1))*g5
       !wbuoy(i,j,k)= (tem1(i,j, k )*rhostr(i,j, k )) *g5 !tmp.debug to be deleted
      END DO
    END DO
  END DO
!
!
  RETURN
END SUBROUTINE tlmbuoycy




!  Beginnig coding ADJSUBROUTINE
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADPGRAD                    ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adpgrad(nx,ny,nz, pprt,                                      &
           j1,j2,j3, upgrad,vpgrad,wpgrad,tem1,tem2)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform adjoint operations on PGRAD. PGRAD
!  calculates the pressure gradient terms in the momentum equations.
!  These terms are evaluated every small time step of the acoustic wave
!  integration.
!
!  The basic state pressure gradient force term is zero.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  09/08/94.
!    Jidong Gao
!  05/30/96 changed to Arps4.0.22
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    pprt     Perturbation pressure at a given time level (Pascal)
!
!  OUTPUT:
!
!    ugrad    Pressure gradient force in u-eq. (kg/(m*s)**2)
!    vgrad    Pressure gradient force in v-eq. (kg/(m*s)**2)
!    wgrad    Pressure gradient force in w-eq. (kg/(m*s)**2)
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: pprt  (nx,ny,nz)     ! Perturbation pressure at a given time
                               ! level (Pascal).
!
  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian -d(zp)/dx.
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian -d(zp)/dy.
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian  d(zp)/dz.

  REAL :: upgrad(nx,ny,nz)     ! Pressure gradient force in u-eq.
                               ! (kg/(m*s)**2)
  REAL :: vpgrad(nx,ny,nz)     ! Pressure gradient force in v-eq.
                               ! (kg/(m*s)**2)
  REAL :: wpgrad(nx,ny,nz)     ! Pressure gradient force in w-eq.

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: onvf
  INTEGER :: i,j,k
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!-----------------------------------------------------------------------
!
!  If there is no terrain, i.e. the ground is flat, skip the
!  following calculations.
!
!-----------------------------------------------------------------------
!
  tem1 = 0.
  tem2 = 0.
!
!
  IF( ternopt /= 0 ) THEN
!
!-----------------------------------------------------------------------
!
!  d(j2*pprt)/dz at v point - 2nd component of p grad. force in y-dir
!  due to terrain
!
!-----------------------------------------------------------------------
!
    DO k=2,nz-2
      DO j=2,ny-1
        DO i=1,nx-1
          tem2(i,j,k) = vpgrad(i,j,k)
        END DO
      END DO
    END DO

    onvf = 0
    CALL addifz0(tem1, onvf,                                             &
         nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, dz, tem2)

    tem2 = 0.

    DO k=2,nz-1
      DO j=2,ny-1
        DO i=1,nx-1
!         tem1(i,j,k)= tem1(i,j,k)*j2(i,j,k)
          tem1(i,j,k)= tem1(i,j,k)*j2(i,j,k)
        END DO
      END DO
    END DO

    onvf = 1
    CALL adavgy0(tem2, onvf,                                             &
         nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, tem1)
    tem1 = 0.

    onvf = 1
    CALL adavgz0(pprt, onvf,                                             &
         nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem2)
    tem2 = 0.
!
!-----------------------------------------------------------------------
!
!  d(j1*pprt)/dz at u point - 2nd component of p grad. force in x-dir
!  due to terrain
!
!-----------------------------------------------------------------------
!

    DO k=2,nz-2
      DO j=1,ny-1
        DO i=2,nx-1
!         upgrad(i,j,k)=upgrad(i,j,k)+tem2(i,j,k)
          tem2(i,j,k) = upgrad(i,j,k)
        END DO
      END DO
    END DO

    onvf = 0
    CALL addifz0(tem1, onvf,                                             &
              nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, dz, tem2)
    tem2 = 0.

    DO k=2,nz-1
      DO j=1,ny-1
        DO i=2,nx-1
          tem1(i,j,k)= tem1(i,j,k)*j1(i,j,k)
        END DO
      END DO
    END DO

    onvf = 1
    CALL adavgx0(tem2, onvf,                                             &
              nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1, tem1)
    tem1 = 0.
!
    onvf = 1
    CALL adavgz0(pprt, onvf,                                             &
              nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem2)
    tem2 = 0.
!
  END IF

!-----------------------------------------------------------------------
!
!  Adjoint on d(pprt)/dz at w point - p grad. force in z-dir
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL addifz0(pprt, onvf,                                               &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, dz, wpgrad)

        wpgrad =0.0
!
!-----------------------------------------------------------------------
!
!  Adjoint on d(j3*pprt)/dy at v point-1st component of p grad. force in y-dir
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL addify0(tem1, onvf,                                               &
        nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, dy, vpgrad)

        vpgrad =0.0
!
!-----------------------------------------------------------------------
!
!  Adjoint on d(j3*pprt)/dx at u point-1st component of p grad. force in x-dir
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL addifx0(tem1, onvf,                                               &
        nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, dx, upgrad)

        upgrad =0.0
!
!-----------------------------------------------------------------------
!
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        pprt(i,j,k) = pprt(i,j,k) + tem1(i,j,k)*j3(i,j,k)
      END DO
    END DO
  END DO
  tem1 = 0.
!
  RETURN
END SUBROUTINE adpgrad
!----------------------------------------------------------------------
!
!     ADJ for tlmfrcuvw
!
!----------------------------------------------------------------------
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADTLMCORIOL                ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adtlmcoriol(nx,ny,nz,                                        &
           u,v,w,ubar,vbar,rhostr,sinlat,ucorio,vcorio, wcorio,         &
           fcorio1, fcorio2,tem1,tem2)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operations on TLMCORIOL. TLMCORIOL calculates
!  the perturbation Coriolis force terms in the u,v and w equations.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  10/24/1994
!
!  MODIFICATION HISTORY:
!
!  Yidi Liu (6/17/96), update adjoint to 4.0
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical direction.
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        z component of velocity at a given time level (m/s)
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!
!  OUTPUT:
!
!    ucorio   Coriolis force term in u-eq. (kg/(m*s)**2)
!    vcorio   Coriolis force term in v-eq. (kg/(m*s)**2)
!    wcorio   Coriolis force term in w-eq. (kg/(m*s)**2)
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity at a time level (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity at a time level (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity at a time level (m/s)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: sinlat(nx,ny)        ! Sin of latitude at each grid point

  REAL :: ucorio(nx,ny,nz)     ! Coriolis force term in u-eq.
  REAL :: vcorio(nx,ny,nz)     ! Coriolis force term in v-eq.
  REAL :: wcorio(nx,ny,nz)     ! Coriolis force term in w-eq.
!
  REAL :: fcorio1  (nx,ny)     ! Temporary work array.
  REAL :: fcorio2  (nx,ny)     ! Temporary work array.
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array.

  REAL :: omega2, sinclat, cosclat

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global model control parameters
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'
!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!-----------------------------------------------------------------------

  omega2 = 2.0* omega

  IF( coriopt == 1 ) THEN

    sinclat = SIN( ATAN(1.0)/45.0 * ctrlat )
    DO j=1,ny
      DO i=1,nx
        fcorio1(i,j) = omega2* sinclat
        fcorio2(i,j) = 0.0
      END DO
    END DO

  ELSE IF( coriopt == 2 ) THEN

    sinclat = SIN( ATAN(1.0)/45.0 * ctrlat )
    cosclat = SQRT( 1.0 - sinclat*sinclat  )
    DO j=1,ny
      DO i=1,nx
        fcorio1(i,j) = omega2* sinclat
        fcorio2(i,j) = omega2* cosclat
      END DO
    END DO

  ELSE IF( coriopt == 3 ) THEN

    DO j=1,ny
      DO i=1,nx
        fcorio1(i,j) = omega2* sinlat(i,j)
        fcorio2(i,j) = 0.0
      END DO
    END DO

  ELSE IF( coriopt == 4 ) THEN

    DO j=1,ny
      DO i=1,nx
        fcorio1(i,j) = omega2* sinlat(i,j)
        fcorio2(i,j) = omega2* SQRT( 1.0 - sinlat(i,j)**2 )
      END DO
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!
!  Coriolis terms in v-eq. and w-eq. if coriotrm=1:
!
!    vcorio = - avgy0( rhostr * fcorio1 * avgx0(u) )
!    wcorio = avgz0( rhostr * fcorio2 * avgx0(u) )
!
!  or if coriotrm=2:
!
!    vcorio = - avgy0( rhostr * fcorio1 * avgx0(u-ubar) )
!    wcorio = avgz0( rhostr * fcorio2 * avgx0(u) )
!
!     since there is no diffrence between coriotrm=1 and 2 for TLM
!          The TLMs for (u) and (u-ubar) are same
!           there is no diffrence between them in their adjoint
!
!-----------------------------------------------------------------------
!
!----------------------------------------------------------------------
!
!     Wash up u, v, w; Wash up tem1 and tem2 for corio f in w-eq
!
!----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.

  IF( coriopt == 2 .OR. coriopt == 4 ) THEN
    onvf = 1
    CALL adavgz0(tem1, onvf,                                             &
          nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, wcorio)

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem1(i,j,k) = fcorio2(i,j) * rhostr(i,j,k)* tem1(i,j,k)
        END DO
      END DO
    END DO

    onvf = 0
    CALL adavgx0(tem2, onvf,                                             &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem1)
!

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx
          u(i,j,k) = u(i,j,k) + tem2(i,j,k)
        END DO
      END DO
    END DO

  END IF     !( coriopt.eq.2 .or. coriopt.eq.4 )

!----------------------------------------------------------------------
!
!     Wash up tem1 and tem2 for corio f in v-eq
!
!----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.

  onvf = 1
  CALL adavgy0(tem1, onvf,                                               &
        nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, vcorio)

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k) = - rhostr(i,j,k)*fcorio1(i,j)*tem1(i,j,k)
      END DO
    END DO
  END DO

  onvf = 0
  CALL adavgx0(tem2, onvf,                                               &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem1)

  tem1 = 0.0

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx
        u(i,j,k) = u(i,j,k) + tem2(i,j,k)
        tem2(i,j,k) = 0.0
      END DO
    END DO
  END DO

!-----------------------------------------------------------------------
!
!  Coriolis terms in u-eq. if coriotrm=1
!
!    ucorio = avgx0( rhostr * (fcorio1*avgy0(v)-fcorio2*avgz0(w)) )
!
!  or if coriotrm=2,
!
!    ucorio = avgx0( rhostr * (fcorio1*avgy0(v-vbar)-fcorio2*avgz0(w)) )
!
!     Since there is no difference between coriotrm=1 and 2 for TLM
!                TLM of (v) and (v-vbar) are same
!           there is no difference between them for adjoint
!
!  where fcorio1 = 2*omega*sinlat.
!  and   fcorio2 = 2*omega* sqrt(1-sinlat**2).
!
!-----------------------------------------------------------------------
!----------------------------------------------------------------------
!
!     Wash up tem1 and tem2 for corio f in u-eq.
!
!----------------------------------------------------------------------

  tem1 = 0.
  tem2 = 0.

  onvf = 1
  CALL adavgx0(tem1, onvf,                                               &
            nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, ucorio)
!
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k) = rhostr(i,j,k)*fcorio1(i,j)*tem1(i,j,k)
        tem2(i,j,k) =-rhostr(i,j,k)*fcorio2(i,j)*tem1(i,j,k)
      END DO
    END DO
  END DO

  onvf = 0
  CALL adavgz0(w, onvf,                                                  &
            nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem2)
!-----------------------------------------------------------------------
!
!  Wash up tem2.
!
!-----------------------------------------------------------------------
!
        tem2 = 0.
!
!-----------------------------------------------------------------------

  onvf = 0
  CALL adavgy0(tem2, onvf,                                               &
            nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem1)
!
    DO k=1,nz-1
      DO j=1,ny
        DO i=1,nx-1
          v(i,j,k) = v(i,j,k) + tem2(i,j,k)
          tem2(i,j,k) = 0.0
        END DO
      END DO
    END DO
!
  RETURN
END SUBROUTINE adtlmcoriol
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADUVWRHO                   ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE aduvwrho(nx,ny,nz,u,v,wcont,rhostr,                          &
           ustr,vstr,wstr,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operations on UVWRHO. UVWRHO
!  computes ustr=u*rhostr, vstr=v*rhostr, wstr=wcont*rhostr.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  11/07/94.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    wcont    Contravariant vertical velocity (m/s)
!
!  OUTPUT:
!
!    ustr     u * rhostr at u-point
!    vstr     v * rhostr at v-point
!    wstr     wcont * rhostr at w-point
!
!  WORK ARRAYS:
!
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
!
  REAL :: ustr  (nx,ny,nz)     ! u * rhostr
  REAL :: vstr  (nx,ny,nz)     ! v * rhostr
  REAL :: wstr  (nx,ny,nz)     ! wcont * rhostr

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array

  INTEGER :: i,j,k
!
!-----------------------------------------------------------------------
!
!  Routines called: None
!
!-----------------------------------------------------------------------
!

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  CALL rhouvw0(nx,ny,nz,rhostr,tem1,tem2,tem3)

  DO k=1,nz
    DO j=1,ny-1
      DO i=1,nx-1
        wcont(i,j,k)= wcont(i,j,k) + wstr(i,j,k)*tem3(i,j,k)
      END DO
    END DO
  END DO
  DO k=1,nz-1
    DO j=1,ny
      DO i=1,nx-1
        v(i,j,k)= v(i,j,k) + vstr(i,j,k)*tem2(i,j,k)
      END DO
    END DO
  END DO
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx
        u(i,j,k)= u(i,j,k) + ustr(i,j,k)*tem1(i,j,k)
      END DO
    END DO
  END DO
  RETURN
END SUBROUTINE aduvwrho

!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADSTRESS                   ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adstress(nx,ny,nz,j3,zp,                                     &
           km,rhostr,tau11,tau12,tau13,tau22,tau23,tau33,               &
           tau31,tau32,                                                 &
           tem1, tem2, tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operation on STRESS. STRESS calculates
!  the stress tensor tauij from deformation tensor
!  Dij (input as tauij) and the mixing coefficient KM.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  11/08/94
!
!  MODIFICATION HISTORY:
!     yidi Liu (6/24/96) update adjoint to 4.0
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!    km       Turbulent mixing coefficient for momentum ( m**2/s ).
!    rhostr   Base state air density times j3 (kg/m**3).
!
!    tau11    Deformation tensor component (1/s)
!    tau12    Deformation tensor component (1/s)
!    tau13    Deformation tensor component (1/s)
!    tau22    Deformation tensor component (1/s)
!    tau23    Deformation tensor component (1/s)
!    tau33    Deformation tensor component (1/s)
!
!
!  OUTPUT:
!
!    tau11    Stress tensor component (kg/(m*s**2))
!    tau12    Stress tensor component (kg/(m*s**2))
!    tau13    Stress tensor component (kg/(m*s**2))
!    tau22    Stress tensor component (kg/(m*s**2))
!    tau23    Stress tensor component (kg/(m*s**2))
!    tau33    Stress tensor component (kg/(m*s**2))
!
!  WORKING ARRAYS:
!
!    tem1     Temporary working array
!    tem2     Temporary working array
!
!  (These arrays are defined and used locally (i.e. inside this
!   subroutine), they may also be passed into routines called by
!   this one. Exiting the call to this subroutine, these temporary
!   working arrays may be used for other purposes therefore their
!   contents overwritten. Please examine the usage of working arrays
!   before you alter the code.)
!
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient
  REAL :: rhostr(nx,ny,nz)     ! Base state air density times j3(kg/m**3)
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! d( zp )/d( z ).
!
  REAL :: tau11 (nx,ny,nz)     ! Deformation and stress tensor
                               ! component (kg/(m*s**2))
  REAL :: tau12 (nx,ny,nz)     ! Deformation and stress tensor
                               ! component (kg/(m*s**2))
  REAL :: tau13 (nx,ny,nz)     ! Deformation and stress tensor
                               ! component (kg/(m*s**2))
  REAL :: tau22 (nx,ny,nz)     ! Deformation and stress tensor
                               ! component (kg/(m*s**2))
  REAL :: tau23 (nx,ny,nz)     ! Deformation and stress tensor
                               ! component (kg/(m*s**2))
  REAL :: tau33 (nx,ny,nz)     ! Deformation and stress tensor
                               ! component (kg/(m*s**2))

  REAL :: zp    (nx,ny,nz)       !
  REAL :: tau31 (nx,ny,nz)
  REAL :: tau32 (nx,ny,nz)
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary working array
  REAL :: tem2  (nx,ny,nz)     ! Temporary working array
  REAL :: tem3  (nx,ny,nz)     ! Temporary working array
!
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL :: tem,deltah2
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Routines called:
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  deltah2 = dx*dy
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating the stress tensor Tau23.
!  tau23 = avgsw0(avgsv0(rhostr/j3*km * (deltav/deltah)**2 )) * d23
!
!-----------------------------------------------------------------------
!
  tem1 = 0.
  DO  k=1,nz-1
    DO  j=1,ny-1
      DO  i=1,nx-1
        IF (trbisotp == 1) THEN
          tem = 1.0
        ELSE
          tem = (zp(i,j,k+1)-zp(i,j,k))**2/deltah2
        END IF
        tem1(i,j,k)=rhostr(i,j,k)*km(i,j,k)*tem/j3(i,j,k)
      END DO
    END DO
  END DO

  tem2 = 0.
  CALL avgsv0(tem1,nx,ny,nz, 1,nx-1, 1,nz-1, tem2)
  tem3 = 0.
  CALL avgsw0(tem2,nx,ny,nz, 1,nx-1, 1,ny, tem3)
  tem1 = 0.
  CALL adaamult0(tem3, tem1 ,                                            &
              nx,ny,nz,1,nx-1,1,ny,1,nz,tau23)
  tau23 = tem1
!-----------------------------------------------------------------------
!
!  Adjoint on calculating the stress tensor Tau32.
!  tau32 = avgsw0(avgsv0(rhostr/j3*km)) * d23
!
!-----------------------------------------------------------------------
  tem1 = 0.
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
      END DO
    END DO
  END DO

  tem2 = 0.
  CALL aamult0(tem1, km,                                                 &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem2)
  tem3 = 0.
  CALL avgsv0(tem2,nx,ny,nz, 1,nx-1, 1,nz-1, tem3)
  tem1 = 0.
  CALL avgsw0(tem3,nx,ny,nz, 1,nx-1, 1,ny, tem1)
  tem2 = 0.
  CALL adaamult0(tem1, tem2,                                             &
              nx,ny,nz,1,nx-1,1,ny,1,nz,tau32)

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tau23(i,j,k)=tau23(i,j,k)+tem2(i,j,k)
      END DO
    END DO
  END DO

!-----------------------------------------------------------------------
!
!  Calculate the stress tensor Tau13.
!  tau13 = avgsw0(avgsu0(rhostr/j3*km)) * d13
!
!-----------------------------------------------------------------------

  DO  k=1,nz-1
    DO  j=1,ny-1
      DO  i=1,nx-1
        IF (trbisotp == 1) THEN
          tem = 1.0
        ELSE
          tem = (zp(i,j,k+1)-zp(i,j,k))**2/deltah2
        END IF
        tem1(i,j,k)=rhostr(i,j,k)*km(i,j,k)*tem/j3(i,j,k)
      END DO
    END DO
  END DO

  tem2 = 0.
  CALL avgsu0(tem1,nx,ny,nz, 1,ny-1, 1,nz-1, tem2)
  tem1 = 0.
  CALL avgsw0(tem2,nx,ny,nz, 1,nx, 1,ny-1, tem1)
  tem3 = 0.
  CALL adaamult0(tem1, tem3,                                            &
              nx,ny,nz,1,nx,1,ny-1,1,nz,tau13)
  tau13 = tem3
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating the stress tensor Tau31.
!  tau13 = avgsw0(avgsu0(rhostr/j3*km)) * d13
!
!-----------------------------------------------------------------------
  tem1 = 0.
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
      END DO
    END DO
  END DO

  tem2 = 0.
  CALL aamult0(tem1, km,                                                 &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem2)
  tem3 = 0.
  CALL avgsu0(tem2,nx,ny,nz, 1,ny-1, 1,nz-1, tem3)
  tem1 = 0.
  CALL avgsw0(tem3,nx,ny,nz, 1,nx, 1,ny-1, tem1)
  tem2 = 0.
  CALL adaamult0(tem1, tem2,                                             &
              nx,ny,nz,1,nx,1,ny-1,1,nz,tau31)

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tau13(i,j,k)=tau13(i,j,k)+tem2(i,j,k)
      END DO
    END DO
  END DO

!-----------------------------------------------------------------------
!
!  Adjoint on calculating the stress tensor Tau12.
!  tau12 = avgsv0(avgsu0(rhostr/j3*km)) * d12
!
!-----------------------------------------------------------------------
  tem1 = 0.
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
      END DO
    END DO
  END DO

  tem2 = 0.
  CALL aamult0(tem1, km,                                                 &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem2)
  tem3 = 0.
  CALL avgsu0(tem2, nx,ny,nz, 1,ny-1, 1,nz-1, tem3)
  tem1 = 0.
  CALL avgsv0(tem3, nx,ny,nz, 1,nx, 1,nz-1, tem1)
  tem3 = 0.
  CALL adaamult0(tem1, tem3 ,                                            &
              nx,ny,nz,1,nx,1,ny,1,nz-1,tau12)
  tau12 = tem3
!-----------------------------------------------------------------------
!
!  Calculate tem1 = rhostr/j3*km
!
!-----------------------------------------------------------------------
  tem1= 0.
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
      END DO
    END DO
  END DO

  tem2 = 0.
  CALL aamult0(tem1,km,                                                  &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem2)
!-----------------------------------------------------------------------
!
!  Adjoint on calculating the stress tensor Tau33.
!  tau33 = rhostr*km/j3 * d33
!
!-----------------------------------------------------------------------

!   CALL adaamult0(tem1,tau33,
!  :            nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tau33)
  tem3 = 0.
  DO  k=1,nz-1
    DO  j=1,ny-1
      DO  i=1,nx-1
        IF (trbisotp == 1) THEN
          tem = 1.0
        ELSE
          tem = (zp(i,j,k+1)-zp(i,j,k))**2/deltah2
        END IF
        tem3(i,j,k)=tem2(i,j,k)*tau33(i,j,k)*tem
      END DO
    END DO
  END DO
   tau33= tem3
!-----------------------------------------------------------------------
!
!  Adjoint on calculating the stress tensor Tau22.
!  tau22 = rhostr*km/j3 * d22
!
!-----------------------------------------------------------------------
  tem3 = 0.
  CALL adaamult0(tem2,tem3 ,                                             &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tau22)
  tau22= tem3
!

  tem3 = 0.
  CALL adaamult0(tem2,tem3 ,                                             &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tau11)
  tau11= tem3
  RETURN
END SUBROUTINE adstress
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADDEFORM                   ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!
!

SUBROUTINE addeform(nx,ny,nz,u,v,w,j1,j2,j3,                            &
           d11,d12,d13,d22,d23,d33,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operations on deform. deform calculates
!  the deformation tensor components Dij
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  11/10/1994
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates at a given time level (m/s).
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    d11      Deformation tensor component (1/s)
!    d12      Deformation tensor component (1/s)
!    d13      Deformation tensor component (1/s)
!    d21      Deformation tensor component (1/s)
!    d22      Deformation tensor component (1/s)
!    d23      Deformation tensor component (1/s)
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)
!
  REAL :: j1    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! d( zp )/d( z ).
!
  REAL :: d11   (nx,ny,nz)     ! Deformation tensor component (1/s)
  REAL :: d12   (nx,ny,nz)     ! Deformation tensor component (1/s)
  REAL :: d13   (nx,ny,nz)     ! Deformation tensor component (1/s)
  REAL :: d22   (nx,ny,nz)     ! Deformation tensor component (1/s)
  REAL :: d23   (nx,ny,nz)     ! Deformation tensor component (1/s)
  REAL :: d33   (nx,ny,nz)     ! Deformation tensor component (1/s)
!
  REAL :: tem1(nx,ny,nz)       ! Temproary working array
  REAL :: tem2(nx,ny,nz)       ! Temproary working array
  REAL :: tem3(nx,ny,nz)       ! Temproary working array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'
  INCLUDE 'bndry.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Routines called:
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!

IF(1==1) THEN

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!-----------------------------------------------------------------------
!
!  Compute the coefficient of d23
!  avgsw0(avgsv0(j3))
!
!-----------------------------------------------------------------------
  tem1 = 0.
  CALL avgsv0(j3,nx,ny,nz, 1,nx-1, 1,nz-1, tem1)
  tem2 = 0.
  CALL avgsw0(tem1,nx,ny,nz, 1,nx-1, 1,ny, tem2)

!-----------------------------------------------------------------------
!
!  Adjoint on computing the final d23 deformation tensor d23 = d23 / tem2
!
!-----------------------------------------------------------------------

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx-1
        d23(i,j,k)=d23(i,j,k)/tem2(i,j,k)
      END DO
    END DO
  END DO

!-----------------------------------------------------------------------
!
!    Adjoint on computing d23 + tem1
!
!-----------------------------------------------------------------------
  tem1 = 0.
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx-1
        tem1(i,j,k)=d23(i,j,k)
      END DO
    END DO
  END DO
!
! CALL adboundw1(tem1,nx,ny,nz, 1,nx-1, 1,ny)
!
  tem3 = 0.
  onvf = 1
  CALL addifz0(tem3, onvf,                                               &
        nx,ny,nz, 1,nx-1, 1,ny, 2,nz-1, dz, tem1)
!
!-----------------------------------------------------------------------
!
!  Adjoint on computing the second term of D23
!  difz0(v + avgz0(j2 * (avgsv0(w))))
!
!-----------------------------------------------------------------------

  IF( ternopt /= 0 ) THEN

    DO k=1,nz-1
      DO j=1,ny
        DO i=1,nx-1
          v(i,j,k)=v(i,j,k)+tem3(i,j,k)
        END DO
      END DO
    END DO

    tem2 = 0.
    onvf = 0
    CALL adavgz0(tem2, onvf,                                             &
         nx,ny,nz, 1,nx-1, 1,ny, 1,nz-1, tem3)
    tem1 = 0.
    CALL adaamult0(j2, tem1,                                               &
         nx,ny,nz, 1,nx-1, 1,ny, 1,nz, tem2)
    tem3 = 0.0
    CALL adavgsv0(tem3,nx,ny,nz, 1,nx-1, 1,nz, tem1)

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx-1
          w(i,j,k)=w(i,j,k)+tem3(i,j,k)
        END DO
      END DO
    END DO

  ELSE   ! When ternopt.eq.0, tem2=0

    DO k=1,nz-1
      DO j=1,ny
        DO i=1,nx-1
          v(i,j,k)=v(i,j,k) +tem3(i,j,k)
        END DO
      END DO
    END DO

  END IF

!-----------------------------------------------------------------------
!
!  Adjoint on computing the deformation tensor d23
!  d23 = 1./ avgsw0(avgsv0(j3)) * (dify0(avgsw0(j3) * w) +
!            difz0(v + avgz0(j2 * (avgsv0(w)))))
!
!-----------------------------------------------------------------------
!
!  Adjoint on computingthe first term of d23
!  dify0(avgsw0(j3) * w)
!
!-------------------------------------------------------------------
!
! CALL adboundv1(d23,nx,ny,nz, 1,nx-1, 1,nz)
!
  tem2= 0.
  onvf = 1
  CALL addify0(tem2, onvf,                                               &
        nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz, dy, d23)
!
  tem1= 0.
  CALL avgsw0(j3,nx,ny,nz, 1,nx-1, 1,ny-1, tem1)
!
!--------------------question mark need check back--------------------
  tem3 = 0.
  CALL adaamult0(tem1, tem3,                                                &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz, tem2)

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          w(i,j,k)=w(i,j,k) + tem3(i,j,k)
        END DO
      END DO
    END DO
!
!-----------------------------------------------------------------------
!
!  Compute the coefficient of d13
!  avgsw0(avgsu0(j3))
!
!-----------------------------------------------------------------------
  tem1 = 0.
  CALL avgsu0(j3,nx,ny,nz, 1,ny-1, 1,nz-1, tem1)
  tem2 = 0.
  CALL avgsw0(tem1,nx,ny,nz, 1,nx, 1,ny-1, tem2)

!-----------------------------------------------------------------------
!
!  Adjoint on computing the final d13 tensor = d13 / tem2
!
!-----------------------------------------------------------------------

  DO k=1,nz
    DO j=1,ny-1
      DO i=1,nx
        d13(i,j,k)=d13(i,j,k)/tem2(i,j,k)
      END DO
    END DO
  END DO
!-----------------------------------------------------------------------
!
!  Adjoint on computing d13 + tem1
!
!-----------------------------------------------------------------------

  DO k=1,nz
    DO j=1,ny-1
      DO i=1,nx
        tem1(i,j,k)=d13(i,j,k)
      END DO
    END DO
  END DO
!
! CALL adboundw1(tem1,nx,ny,nz, 1,nx, 1,ny-1)
!
  tem3 = 0.
  onvf = 1
  CALL addifz0(tem3, onvf,                                               &
        nx,ny,nz, 1,nx, 1,ny-1, 2,nz-1, dz, tem1)
!-----------------------------------------------------------------------
!
!  Adjoint on computing the second term of d13
!  difz0(u + avgsw0(j1 * (avgz0(w))))
!
!-----------------------------------------------------------------------

  IF( ternopt /= 0 ) THEN

!-----------------------------------------------------------------------
!
!  Compute u + tem2
!
!-----------------------------------------------------------------------

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx
          u(i,j,k)=u(i,j,k)+tem3(i,j,k)
        END DO
      END DO
    END DO

    tem2 = 0.
    onvf = 0
    CALL adavgz0(tem2, onvf,                                               &
         nx,ny,nz, 1,nx, 1,ny-1, 1,nz-1, tem3)
    tem1 = 0.
    CALL adaamult0(j1, tem1,                                               &
         nx,ny,nz, 1,nx, 1,ny-1, 1,nz, tem2)
    tem3 = 0.
    CALL adavgsu0(tem3,nx,ny,nz, 1,ny-1, 1,nz, tem1)

    DO k=1,nz
      DO j=1,ny-1
        DO i=1,nx
          w(i,j,k)=w(i,j,k)+tem3(i,j,k)
        END DO
      END DO
    END DO

  ELSE   ! If ternopt=0, tem2=0

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          u(i,j,k)=u(i,j,k) + tem3(i,j,k)
        END DO
      END DO
    END DO

  END IF
!-----------------------------------------------------------------------
!
!  Adjoint on calculating the deformation tensor d13
!  d13 = 1./ avgsw0(avgsu0(j3)) * (difx0(avgsw0(j3) * w) +
!        difz0(u + avgx0(j1 * (avgsu0(w)))))
!
!-----------------------------------------------------------------------

!-------------------------------------------------------------------
!
!  Adjoint on computing the first term of d13
!  difx0(avgsw0(j3) * w)
!
!-------------------------------------------------------------------
! CALL adboundu1(d13,nx,ny,nz, 1,ny-1, 1,nz)
!
  tem2 = 0.
  onvf = 1
  CALL addifx0(tem2, onvf,                                               &
        nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz, dx, d13)
!
  tem1 = 0.
  CALL avgsw0(j3,nx,ny,nz, 1,nx-1, 1,ny-1, tem1)

!
  tem3 = 0.
  CALL adaamult0(tem1, tem3,                                             &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz, tem2)
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        w(i,j,k)=w(i,j,k)+tem3(i,j,k)
      END DO
    END DO
  END DO

END IF      ! 1==0
!
!-------------------------------------------------------------------
!
!  Adjoint on calculating the coefficient of d12
!  avgsv0(avgsu0(j3))
!
!-------------------------------------------------------------------
  tem1 = 0.
  CALL avgsu0(j3,nx,ny,nz, 1,ny-1, 1,nz-1, tem1)

  tem2 = 0.
  CALL avgsv0(tem1,nx,ny,nz, 1,nx, 1,nz-1, tem2)

!
!  Compute the final d12 = d12 / tem2
!
  DO k=1,nz-1
    DO j=1,ny
      DO i=1,nx
        d12(i,j,k)=d12(i,j,k)/tem2(i,j,k)
      END DO
    END DO
  END DO
!-------------------------------------------------------------------
!
!  Adjoint on combining D12 and TEM2 terms
!
!-------------------------------------------------------------------
  tem3 = 0.
  DO k=1,nz-1
    DO j=1,ny
      DO i=1,nx
        tem3(i,j,k)=d12(i,j,k)
      END DO
    END DO
  END DO
!-------------------------------------------------------------------
!
!  Adjoint on calculating the second term in D12
!  difx0(avgsv0(j3) * v))
!
!-------------------------------------------------------------------
!
! CALL adboundu1(tem3,nx,ny,nz, 1,ny, 1,nz-1)
!
  tem2 = 0.
  onvf = 1
  CALL addifx0(tem2, onvf,                                               &
        nx,ny,nz, 2,nx-1, 1,ny, 1,nz-1, dx, tem3)
!
  tem1 = 0.
  CALL avgsv0(j3,nx,ny,nz, 1,nx-1, 1,nz-1, tem1)
  tem3 = 0.
  CALL adaamult0(tem1, tem3,                                             &
        nx,ny,nz, 1,nx-1, 1,ny, 1,nz-1, tem2)
  DO k=1,nz-1
    DO j=1,ny
      DO i=1,nx-1
        v(i,j,k)=v(i,j,k)+tem3(i,j,k)
      END DO
    END DO
  END DO
!
!-------------------------------------------------------------------
!
!  Adjoint on combining the first and third terms of D12 (D12 +TEM2)
!
!-------------------------------------------------------------------
  tem3 = 0.
  DO k=1,nz-1
    DO j=1,ny
      DO i=1,nx
        tem3(i,j,k)=d12(i,j,k)
      END DO
    END DO
  END DO
!-------------------------------------------------------------------
!
!  Adjoint on calculating the first term in d12
!  dify0(avgsu0(j3) * u)
!
!-------------------------------------------------------------------
!
! CALL adboundv1(tem3,nx,ny,nz, 1,nx, 1,nz-1)
!
  tem2 = 0.
  onvf = 1
  CALL addify0(tem2, onvf,                                               &
        nx,ny,nz, 1,nx, 2,ny-1, 1,nz-1, dy, tem3)

  CALL avgsu0(j3,nx,ny,nz, 1,ny-1, 1,nz-1, tem1)
  tem3 = 0.
  CALL adaamult0(tem1, tem3,                                             &
        nx,ny,nz, 1,nx, 1,ny-1, 1,nz-1, tem2)

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        u(i,j,k)=u(i,j,k)+tem3(i,j,k)
      END DO
    END DO
  END DO

!-----------------------------------------------------------------------
!
!  Calculate the deformation tensor d12
!
!  d12 = 1/(avgsv0(avgsu0(j3))) * (dify0(avgsu0(j3) * u) + difx0(avgsv0(J3) * v)
!        + difz0(avgsu0(j2) * avgsv0(avgsw0(u)) + avgsv0(j1) * avgsu0(avgsw0(v))))
!
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
!
!  Note: In order to reduce memory usage, order dependence has been
!  introduced into the calculation of D12, which reduces the possible
!  term-wise parallellism.  This is due to the constraint of only two
!  temporary arrays being avaliable for passage into this subroutine.
!
!  The third term of the D12 tensor (which includes two sub-terms)
!  will be calculated first.
!
!-----------------------------------------------------------------------

  IF( ternopt /= 0 ) THEN

!
!  At this point, tem3 = avgsu0(j2) * avgsv0(avgsw0(u)) +
!                        avgsv0(j1) * avgsu0(avgsw0(v))
!
    tem3 = 0.
    onvf = 0
    CALL addifz0(tem3, onvf,                                             &
         nx,ny,nz, 1,nx, 1,ny, 1,nz-1, dz, d12)

!-------------------------------------------------------------------
!
!  Combine d12 and tem1 terms to get the third term of d12
!
!-------------------------------------------------------------------
!
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
           d12(i,j,k)= tem3(i,j,k)
        END DO
      END DO
    END DO
!
!  At this point, D12 = avgsu0(J2) * avgsv0(avgsw0(u))
!
!-------------------------------------------------------------------
!
!  Calculate the second sub-term in the third term of d12
!  avgsv0(j1) * avgsu0(avgsw0(v)), this term is zero when ternopt=0
!
!-------------------------------------------------------------------
    CALL avgsv0(j1,nx,ny,nz, 1,nx, 1,nz, tem1)
    tem2 = 0.
    CALL adaamult0(tem1, tem2,                                             &
          nx,ny,nz, 1,nx, 1,ny, 1,nz, tem3)
    tem1 = 0.
    CALL adavgsu0(tem1,nx,ny,nz, 1,ny, 1,nz, tem2)
!   tem3 = 0.
!   CALL adavgsw0(tem3,nx,ny,nz, 1,nx-1, 1,ny, tem1)
    tem3= tem1

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          v(i,j,k)=v(i,j,k)+tem3(i,j,k)
        END DO
      END DO
    END DO

!-------------------------------------------------------------------
!
!  Calculate the first sub-term of the third term of D12
!  avgsu0(j2) * avgsv0(avgsw0(u)), this term is zero when ternopt=0
!
!-------------------------------------------------------------------
    tem1 = 0.
    CALL avgsu0(j2,nx,ny,nz, 1,ny, 1,nz, tem1)
    tem2 = 0.
    CALL adaamult0(tem1, tem2,                                             &
         nx,ny,nz, 1,nx, 1,ny, 1,nz, d12)
    tem1 = 0.
    CALL adavgsv0(tem1,nx,ny,nz, 1,nx, 1,nz, tem2)
!   tem3 = 0.
!   CALL adavgsw0(tem3,nx,ny,nz, 1,nx, 1,ny-1, tem1)
    tem3= tem1

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          u(i,j,k)=u(i,j,k)+tem3(i,j,k)
        END DO
      END DO
    END DO

  ELSE

    DO k=1,nz-1
      DO j=1,ny
        DO i=1,nx
          d12(i,j,k)=0.0
        END DO
      END DO
    END DO

  END IF
!-----------------------------------------------------------------------
!
!  Adjoint on calculating the stress tensor component d33
!  d33 = 2./j3 * difz0(w)
!
!-----------------------------------------------------------------------

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        d33(i,j,k)=(2.0/j3(i,j,k)) * d33(i,j,k)
      END DO
    END DO
  END DO

  tem1 = 0.
  onvf = 0
  CALL addifz0(tem1, onvf,                                               &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, dz, d33)

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        w(i,j,k)=w(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO
!
  IF( ternopt /= 0 ) THEN

!-----------------------------------------------------------------------
!
!  Adjoint on combining terms 2./j3 * (D22 + TEM1) to obtain the final
!  D22 deformation tensor
!
!-----------------------------------------------------------------------

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
!         d22(i,j,k)=(2.0/j3(i,j,k)) * (d22(i,j,k)+tem1(i,j,k))
          tem1(i,j,k) = (2.0/j3(i,j,k)) * d22(i,j,k)
           d22(i,j,k) = (2.0/j3(i,j,k)) * d22(i,j,k)
        END DO
      END DO
    END DO
!-------------------------------------------------------------------
!
!  Calculate the second term of d22
!  difz0(avgy0(j2 * avgsw0(v))))
!
!-------------------------------------------------------------------

    tem3 = 0.
    onvf = 0
    CALL addifz0(tem3, onvf,                                             &
         nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, dz, tem1)

    tem2 = 0.
    onvf = 0
    CALL adavgy0(tem2, onvf,                                             &
         nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz, tem3)
   tem1 = 0.
    CALL adaamult0(j2, tem1,                                             &
         nx,ny,nz, 1,nx-1, 1,ny, 1,nz, tem2)

    tem3 = 0.
    CALL adavgsw0(tem3,nx,ny,nz, 1,nx-1, 1,ny, tem1)

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx-1
          v(i,j,k)=v(i,j,k)+ tem3(i,j,k)
        END DO
      END DO
    END DO

  ELSE   ! When ternopt=0, tem1=0.0

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          d22(i,j,k)=(2.0/j3(i,j,k)) * d22(i,j,k)
        END DO
      END DO
    END DO

  END IF
!-----------------------------------------------------------------------
!
!  Adjoint on Calculating the deformation tensor component d22:
!
!   d22 = 2./j3 *(dify0(avgsv0(j3 * v)) +
!                (difz0(avgy0(j2 * avgsw0(v)))))
!
!
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!
!  Adjoint on calculating the first term in D22
!  dify0(avgsv0(j3) * v)
!
!-----------------------------------------------------------------------
!
  tem2 = 0.
  onvf = 0
  CALL addify0(tem2, onvf,                                               &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, dy, d22)

  CALL avgsv0(j3,nx,ny,nz, 1,nx-1, 1,nz-1, tem1)
  tem3 = 0.
  CALL adaamult0(tem1, tem3,nx,ny,nz, 1,nx-1, 1,ny, 1,nz-1, tem2)

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        v(i,j,k)=v(i,j,k)+tem3(i,j,k)
      END DO
    END DO
  END DO
   print*, 'ternopt===in addeform=',ternopt
  IF( ternopt /= 0 ) THEN

!-----------------------------------------------------------------------
!
!  Combine the terms, ( 2 /j3 * (d11 + TEM1)) to form the
!  final D11 stress tensor
!
!-----------------------------------------------------------------------
    tem1= 0.0
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
!         d11(i,j,k)=(2.0/j3(i,j,k)) * (d11(i,j,k)+tem1(i,j,k))
          tem1(i,j,k) = (2.0/j3(i,j,k)) * d11(i,j,k)
           d11(i,j,k) = (2.0/j3(i,j,k)) * d11(i,j,k)
        END DO
      END DO
    END DO
!-------------------------------------------------------------
!
!  Calculate the second term of d11
!  difz0(avgx0(j1 * avgsw0(u)))
!
!-------------------------------------------------------------

    tem3 = 0.
    onvf = 0
    CALL addifz0(tem3, onvf,                                             &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, dz, tem1)

    tem2 = 0.
    onvf = 0
    CALL adavgx0(tem2, onvf,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz, tem3)

    tem1 = 0.
    CALL adaamult0(j1, tem1,nx,ny,nz, 1,nx, 1,ny-1, 1,nz, tem2)
    tem3 = 0.0
    CALL adavgsw0(tem3,nx,ny,nz, 1,nx, 1,ny-1, tem1)

    DO k=1,nz
      DO j=1,ny-1
        DO i=1,nx
          u(i,j,k)=u(i,j,k) + tem3(i,j,k)
        END DO
      END DO
    END DO


  ELSE  ! When ternopt=0, tem1=0.0

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          d11(i,j,k)=(2.0/j3(i,j,k)) * d11(i,j,k)
        END DO
      END DO
    END DO

  END IF
!-----------------------------------------------------------------------
!
!  Adjoint on calculating the deformation tensor components Dij, which are stored
!  in arrays dij.
!
!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating the deformation tensor component D11:
!
!   d11 = 2./j3 *(difx0(avgsu0(j3) * u)) +
!                (difz0(avgx0(j1 * avgsw0(u)))))
!
!-----------------------------------------------------------------------
  tem2 = 0.
  onvf= 0
  CALL addifx0(tem2, onvf,                                               &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, dx, d11)
!
  CALL avgsu0(j3, nx,ny,nz, 1,ny-1, 1,nz-1, tem1)
  tem3 = 0.
  CALL adaamult0(tem1, tem3,nx,ny,nz, 1,nx, 1,ny-1, 1,nz-1, tem2)
!
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        u(i,j,k)=u(i,j,k)+tem3(i,j,k)
      END DO
    END DO
  END DO
  RETURN
END SUBROUTINE addeform
!##################################################################
!##################################################################
!######                                                      ######
!######                  SUBROUTINE ADBOUNDU                 ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adboundu1(s,nx,ny,nz,jbgn,jend,kbgn,kend)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Adjoint on BOUNDU. BOUNDU sets
!  the boundary conditions for a quantity on the physical boundaries
!  in the u direction to 1 and nx. Please note
!  that the values at the corner points may depend on the order that e-w,
!  n-s and t-b boundary conditions are applied.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR: Jidong Gao
!  11/08/94
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!       jbgn     Index to start the j direction
!       jend     Index to end the j direction
!       kbgn     Index to start the k direction
!       kend     Index to end the k direction
!
!    ebc      Parameter defining east   boundary condition type.
!    wbc      Parameter defining west   boundary condition type.
!
!    s        Input array defined from i=2 to i=nx-1
!
!  OUTPUT:
!
!    s       Output array at u point for i=1 to i=nx
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE             ! Force explicit declarations

  INTEGER :: nx,ny,nz          ! Number of grid points in x, y and z
                               ! directions

  INTEGER :: jbgn,jend            ! Bounds for computations in the y
                                  ! direction

  INTEGER :: kbgn,kend            ! Bounds for computations in the z
                                  ! direction

  REAL :: s(nx,ny,nz)              ! A scalar variable in the x direction
!
!-----------------------------------------------------------------------
!
!  The following integer parameters define the type of condition
!  at each boundary.
!
!   1 for rigid wall (mirror) type boundary condition.
!   2 for periodic boundary condition.
!   3 for zero normal gradient boundary condition.
!   4 for open (radiation) boundary condition.
!   5 for user (externally) specified boundary condition.
!
!-----------------------------------------------------------------------
!
!   integer ebc   ! Parameter defining east   boundary condition type.
!   integer wbc   ! Parameter defining west   boundary condition type.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'bndry.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Set the west boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(wbc == 0) GO TO 5001

  IF(wbc == 1) THEN            ! Wall boundary condition

    DO k=kbgn,kend
      DO j=jbgn,jend
        s(3,j,k)=-s(1,j,k)
      END DO
    END DO

  ELSE IF(wbc == 2) THEN        ! Periodic boundary condition.

    DO k=kbgn,kend
      DO j=jbgn,jend
        s(nx-2,j,k)=s(nx-2,j,k)+s(1,j,k)
      END DO
    END DO

  ELSE IF(wbc == 3 .OR. wbc == 4 .OR. wbc == 5 .OR. wbc == 6)THEN
                                   ! Zero normal gradient condition or
! Radiation or user specified condition.
    DO k=kbgn,kend
      DO j=jbgn,jend
        s(2,j,k)=s(2,j,k)+s(1,j,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BOUNDU', wbc
    STOP

  END IF

  5001  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the east boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(ebc == 0) GO TO 5002

  IF(ebc == 1) THEN             ! Wall boundary condition

    DO k=kbgn,kend
      DO j=jbgn,jend
        s(nx-2,j,k)=-s(nx,j,k)
      END DO
    END DO

  ELSE IF(ebc == 2) THEN         ! Periodic boundary condition.

    DO k=kbgn,kend
      DO j=jbgn,jend
        s(3,j,k)=s(3,j,k)+s(nx,j,k)
      END DO
    END DO

  ELSE IF(ebc == 3 .OR. ebc == 4 .OR. ebc == 5 .OR. ebc == 6)THEN
                                   ! Zero normal gradient condition or
! Radiation or user specified condition.
    DO k=kbgn,kend
      DO j=jbgn,jend
        s(nx-1,j,k)=s(nx-1,j,k)+s(nx,j,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BOUNDU', ebc
    STOP

  END IF

  5002  CONTINUE

  RETURN

  900   FORMAT(1X,'Invalid boundary condition option found in ',a,      &
               /1X,'The option was ',i3,' Job stopped.')
END SUBROUTINE adboundu1
!##################################################################
!##################################################################
!######                                                      ######
!######                  SUBROUTINE ADBOUNDV                 ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adboundv1(s,nx,ny,nz,ibgn,iend,kbgn,kend)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operation on BOUNDV. BOUNDV sets
!  the boundary conditions for a quantity on the physical boundaries
!  in the v direction to 1 and ny. Please note
!  that the values at the corner points may depend on the order that e-w,
!  n-s and t-b boundary conditions are applied.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR: David Zhi Wnag
!  11/08/94
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!       ibgn     Index to start the i direction
!       iend     Index to end the i direction
!       kbgn     Index to start the k direction
!       kend     Index to end the k direction
!
!    nbc      Parameter defining northern boundary condition type.
!    sbc      Parameter defining southern boundary condition type.
!
!    s        Input array defined from j=2 to j=ny-1
!
!  OUTPUT:
!
!    s       Output array at v point for j=1 to j=ny
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE             ! Force explicit declarations

  INTEGER :: nx,ny,nz          ! Number of grid points in x, y and z
                               ! directions

  INTEGER :: ibgn,iend            ! Bounds for computations in the x
                                  ! direction

  INTEGER :: kbgn,kend            ! Bounds for computations in the z
                                  ! direction

  REAL :: s(nx,ny,nz)              ! A scalar variable in the y direction
!
!-----------------------------------------------------------------------
!
!  The following integer parameters define the type of condition
!  at each boundary.
!
!   1 for rigid wall (mirror) type boundary condition.
!   2 for periodic boundary condition.
!   3 for zero normal gradient boundary condition.
!   4 for open (radiation) boundary condition.
!   5 for user (externally) specified boundary condition.
!
!-----------------------------------------------------------------------
!
!  integer nbc   ! Parameter defining north  boundary condition type.
!  integer sbc   ! Parameter defining south  boundary condition type.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'bndry.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Set the north boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(nbc == 0) GO TO 5003

  IF(nbc == 1) THEN             ! Wall boundary condition

    DO k=kbgn,kend
      DO i=ibgn,iend
        s(i,ny-2,k)=-s(i,ny,k)
      END DO
    END DO

  ELSE IF(nbc == 2) THEN         ! Periodic boundary condition.

    DO k=kbgn,kend
      DO i=ibgn,iend
        s(i,3,k)=s(i,3,k)+s(i,ny,k)
      END DO
    END DO

  ELSE IF(nbc == 3 .OR. nbc == 4 .OR. nbc == 5 .OR. nbc == 6)THEN
                                   ! Zero normal gradient condition or
! Radiation or user specified condition.
    DO k=kbgn,kend
      DO i=ibgn,iend
        s(i,ny-1,k)=s(i,ny-1,k)+s(i,ny,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BOUNDV', nbc
    STOP

  END IF

  5003  CONTINUE

!
!-----------------------------------------------------------------------
!
!  Set the south boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(sbc == 0) GO TO 5004

  IF(sbc == 1) THEN             ! Wall boundary condition

    DO k=kbgn,kend
      DO i=ibgn,iend
        s(i,3,k)=-s(i,1,k)
      END DO
    END DO

  ELSE IF(sbc == 2) THEN         ! Periodic boundary condition.

    DO k=kbgn,kend
      DO i=ibgn,iend
        s(i,ny-2,k)=s(i,ny-2,k)+s(i,1,k)
      END DO
    END DO

  ELSE IF(sbc == 3 .OR. sbc == 4 .OR. sbc == 5 .OR. sbc == 6)THEN
                                   ! Zero normal gradient condition or
! Radiation or user specified condition.
    DO k=kbgn,kend
      DO i=ibgn,iend
        s(i,2,k)=s(i,2,k)+s(i,1,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BOUNDV', sbc
    STOP

  END IF

  5004  CONTINUE

  RETURN

  900   FORMAT(1X,'Invalid boundary condition option found in ',a,      &
               /1X,'The option was ',i3,' Job stopped.')
END SUBROUTINE adboundv1

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                  SUBROUTINE ADBOUNDW                 ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adboundw1(s,nx,ny,nz,ibgn,iend,jbgn,jend)

!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operations on BOUNDW. BOUNDW sets
!  the boundary conditions for a quantity on the physical boundaries
!  in the w direction to 1 and nz. Please note
!  that the values at the corner points may depend on the order that e-w,
!  n-s and t-b boundary conditions are applied.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR: David Zhi Wnag
!  11/08/94
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!       ibgn     Index to start the i direction
!       iend     Index to end the i direction
!       jbgn     Index to start the j direction
!       jend     Index to end the j direction
!    tbc      Parameter defining top    boundary condition type.
!    bbc      Parameter defining bottom boundary condition type.
!
!    s        Input array defined from k=2 to k=nz-1
!
!  OUTPUT:
!
!    s       Output array at w point for k=1 to k=nz
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE             ! Force explicit declarations

  INTEGER :: nx,ny,nz          ! Number of grid points in x, y and z
                               ! directions

  INTEGER :: ibgn,iend            ! Bounds for computations in the x
                                  ! direction

  INTEGER :: jbgn,jend            ! Bounds for computations in the y
                                  ! direction

  REAL :: s(nx,ny,nz)              ! A scalar variable in the z direction
!
!-----------------------------------------------------------------------
!
!  The following integer parameters define the type of condition
!  at each boundary.
!
!   1 for rigid wall (mirror) type boundary condition.
!   2 for periodic boundary condition.
!   3 for zero normal gradient boundary condition.
!
!-----------------------------------------------------------------------
!
!     integer tbc                 ! Top boundary condition parameter
!
!     integer bbc                 ! Bottom boundary condition parameter
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'bndry.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Set the top boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(tbc == 0) GO TO 5005

  IF(tbc == 1) THEN             ! Rigid lid boundary condition

    DO j=jbgn,jend
      DO i=ibgn,iend
        s(i,j,nz-2)=s(i,j,nz-2)-s(i,j,nz)
      END DO
    END DO

  ELSE IF(tbc == 2) THEN         ! Periodic boundary condition.

    DO j=jbgn,jend
      DO i=ibgn,iend
        s(i,j,nz)=s(i,j,3)
      END DO
    END DO

  ELSE IF(tbc == 3) THEN         ! Zero normal gradient condition.

    DO j=jbgn,jend
      DO i=ibgn,iend
        s(i,j,nz)=s(i,j,nz-2)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BOUNDW', tbc
    STOP

  END IF

  5005  CONTINUE

!
!-----------------------------------------------------------------------
!
!  Set the bottom boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(bbc == 0) GO TO 5006

  IF(bbc == 1) THEN             ! Non-penetrative ground condition

    DO j=jbgn,jend
      DO i=ibgn,iend
        s(i,j,3)=s(i,j,3)-s(i,j,1)
      END DO
    END DO

  ELSE IF(bbc == 2) THEN         ! Periodic boundary condition.

    DO j=jbgn,jend
      DO i=ibgn,iend
        s(i,j,1)=s(i,j,nz-2)
      END DO
    END DO

  ELSE IF(bbc == 3) THEN         ! Zero normal gradient condition.

    DO j=jbgn,jend
      DO i=ibgn,iend
        s(i,j,1)=s(i,j,3)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BOUNDW', bbc
    STOP

  END IF

  5006  CONTINUE

  RETURN

  900   FORMAT(1X,'Invalid boundary condition option found in ',a,      &
               /1X,'The option was ',i3,' Job stopped.')
END SUBROUTINE adboundw1
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADTLMFRCUVW                ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adtlmfrcuvw( nx,ny,nz,dtbig1,nstepss,nbasic,                 &
           u,v,w,wcont,ptprt,pprt,qv,qscalar,tke,                       &
           ubar,vbar,ptbar,pbar,rhostr,qvbar,                           &
           nwcont,                                                      &
           !nu,nv,nw,nwcont,nptprt,npprt,nqv,nqscalar,                   &
           nustr,nvstr,nwstr,                                           &
           usflx,vsflx, x,y,z,zp, j1,j2,j3, sinlat,stabchk,             &
           uforce,vforce,wforce, km,lendel,defsq,                       &
           ubk, vbk, wbk,                                               &
           tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,                &
           tem10,tem11,tem12)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the inactive acoustic forcing terms in the momentum
!  equations. These forcing terms include the advection, mixing, Coriolis
!  force and buoyancy terms, and are accumulated into one array for
!  each equation, i.e.,
!      uforce = - uadv + umix + ucorio.
!      vforce = - vadv + vmix + vcorio.
!      wforce = - wadv + wmix + wbuoy + wcorio.
!  These forcing terms are held fixed during the small acoustic wave
!  integration time steps. The turbulent mixing coefficient for
!  momentum km is an output which will be used to calculate the
!  turbulent mixing of heat and water substances.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  11/14/1994
!
!  MODIFICATION HISTORY:
!  05/21/96 (Jidong Gao)
!  Converted to version 4.0
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    dtbig1     Local large time step size.
!    nstepss  The number of integrations plus one.
!    nbasic   Index of the current basic state time level.
!
!    u        x component of velocity (m/s)
!    v        y component of velocity (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates (m/s).
!    wcont    Contravariant vertical velocity (m/s)
!    ptprt    Perturbation potential temperature (K)
!    pprt     Perturbation pressure (Pascal)
!    qv       Water vapor specific humidity (kg/kg)
!    qc       Cloud water mixing ratio (kg/kg)
!    qr       Rainwater mixing ratio (kg/kg)
!    qi       Cloud ice mixing ratio (kg/kg)
!    qs       Snow mixing ratio (kg/kg)
!    qh       Hail mixing ratio (kg/kg)
!    tke      Turbulent Kinetic Energy ((m/s)**2)
!
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!
!    usflx    Surface flux of u-momentum
!    vsflx    Surface flux of v-momentum
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space(m)
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!    sinalt   Sin of latitude at each grid point
!
!    Basic state variables:
!    nu,nv,nw,nptprt,npprt,nqv,nqc,nqr,nqi,nqs,nqh,
!    nustr,nvstr,nwstr.
!
!  OUTPUT:
!
!    uforce   Acoustically inactive forcing terms in u-momentum
!             equation (kg/(m*s)**2). uforce= -uadv + umix + ucorio
!    vforce   Acoustically inactive forcing terms in v-momentum
!             equation (kg/(m*s)**2). vforce= -vadv + vmix + vcorio
!    wforce   Acoustically inactive forcing terms in w-momentum
!             equation (kg/(m*s)**2).
!             wforce= -wadv + wmix + wbuoy + wcorio
!    km       Turbulent mixing coefficient for momentum.
!             Used to calculate turbulent mixing of scalars.
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3).
!    defsq    Deformation squared.
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!    tem4     Temporary work array.
!    tem5     Temporary work array.
!    tem6     Temporary work array.
!    tem7     Temporary work array.
!    tem8     Temporary work array.
!    tem9     Temporary work array.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global constants that control model
                            ! execution
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'bndry.inc'
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  INTEGER :: nt                ! The no. of t-levels of t-dependent
                               ! arrays.
  INTEGER :: tpast             ! Index of time level for the past time.
  INTEGER :: tpresent          ! Index of time level for the present
                               ! time.
  INTEGER :: tfuture           ! Index of time level for the future
                               ! time.
  REAL :: dtbig1               ! Local large time step size.
  INTEGER :: nstepss           ! Index of current basic state time levels.
  INTEGER :: nbasic            ! Index of the current basic state time level.
  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity (m/s)
  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)
  REAL :: nwcont (nx,ny,nz)    ! Contravariant vertical velocity (m/s)
  REAL :: nustr  (nx,ny,nz)    ! nu * rhostr
  REAL :: nvstr  (nx,ny,nz)    ! nv * rhostr
  REAL :: nwstr  (nx,ny,nz)    ! nw * rhostr
  REAL :: ptprt (nx,ny,nz,nt)  ! Perturbation potential temperature (K)
  REAL :: pprt  (nx,ny,nz,nt)  ! Perturbation pressure (Pascal)
  REAL :: qv    (nx,ny,nz,nt)  ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq,nt)  ! Cloud water mixing ratio (kg/kg)
  REAL :: tke   (nx,ny,nz,nt)  ! Turbulent kinetic energy ((m/s)**2)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific
                               ! humidity(kg/kg)
  REAL :: ubk   (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbk   (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: wbk   (nx,ny,nz)     ! Base state potential temperature (K)

  REAL :: x     (nx)           ! x-coord. of the physical and compu-
                               ! tational grid. Defined at u-point(m).
  REAL :: y     (ny)           ! y-coord. of the physical and compu-
                               ! tational grid. Defined at v-point(m).
  REAL :: z     (nz)           ! z-coord. of the computational grid.
                               ! Defined at w-point on the staggered
                               ! grid(m).
  REAL :: zp    (nx,ny,nz)     ! Physical height coordinate defined at
                               ! w-point of the staggered grid(m).

  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! -d(zp)/d(x)
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! -d(zp)/d(y)
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! d(zp)/d(z)

  REAL :: sinlat(nx,ny)        ! Sin of latitude at each grid point
  REAL :: stabchk(nx,ny)

  REAL :: usflx (nx,ny)        ! Surface flux of u-momentum
                               ! (kg/(m*s**2))
  REAL :: vsflx (nx,ny)        ! Surface flux of v-momentum
                               ! (kg/(m*s**2))


  REAL :: uforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in u-momentum equation (kg/(m*s)**2)
                               ! uforce= -uadv + umix + ucorio

  REAL :: vforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in v-momentum equation (kg/(m*s)**2)
                               ! vforce= -vadv + vmix + vcorio

  REAL :: wforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in w-momentum equation (kg/(m*s)**2)
                               ! wforce= -wadv + wmix + wbuoy

  REAL :: km    (nx,ny,nz)     ! Turbulent mixing coefficient for
                               ! momentum.
  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)
  REAL :: defsq (nx,ny,nz)     ! Deformation squared (1/s**2)

!-----------------------------------------------------------------------
!
!  Basic State Variable Declarations:
!
!-----------------------------------------------------------------------
!

  !REAL :: nu     (nx,ny,nz)  ! Total u-velocity (m/s)
  !REAL :: nv     (nx,ny,nz)  ! Total v-velocity (m/s)
  !REAL :: nw     (nx,ny,nz)  ! Total w-velocity (m/s)
  !REAL :: nptprt (nx,ny,nz)  ! Perturbation potential temperature
  !REAL :: npprt  (nx,ny,nz)  ! Perturbation pressure
  !REAL :: nqv    (nx,ny,nz)  ! Water vapor specific humidity (kg/kg)
  !REAL :: nqscalar(nx,ny,nz,nscalarq)  ! Cloud water mixing ratio (kg/kg)

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem4  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem5  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem6  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem7  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem8  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem9  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem10 (nx,ny,nz)     ! Temporary work array.
  REAL :: tem11 (nx,ny,nz)     ! Temporary work array.
  REAL :: tem12 (nx,ny,nz)     ! Temporary work array.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: tlevel
  REAL :: sum2

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  To calculate additional boundary relaxation and mixing terms for
!  the wind fields in the case of externally forced boundary condition.
!
!-----------------------------------------------------------------------
! IF ( lbcopt == 2 .AND. mgrid == 1 ) THEN
!
!
!   CALL adtlmbrlxuvw( nx,ny,nz, dtbig1,                                &
!                 u(1,1,1,1),v(1,1,1,1),w(1,1,1,1),rhostr,              &
!                 uforce,vforce,wforce,                                 &
!                 tem1,tem2,tem3,tem4,tem5,tem6 )
!
! END IF
!
!-----------------------------------------------------------------------
!
!  Calculate the buoyancy term for the w-equation.
!  The buoyancy term is stored in tem1 then added to wforce.
!
!  If buoyopt = 0 then buoyancy is turned off.
!
!-----------------------------------------------------------------------
!

  tlevel =  tpresent

        tem1 = 0.0
        tem2 = 0.0
        tem3 = 0.0

! IF ( buoyopt /= 0 ) THEN

  IF (1==0) THEN
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem1(i,j,k)=wforce(i,j,k)
        END DO
      END DO
    END DO
!
!
    CALL adtlmbuoycy(nx,ny,nz,nstepss,nbasic,                           &
                ptprt(1,1,1,tlevel),pprt(1,1,1,tlevel),                 &
                qv(1,1,1,tlevel),qscalar(1,1,1,:,tlevel),               &
                ptbar,pbar,rhostr,qvbar,                                &
                tem1,tem2)                          ! wbuoy

  END IF
! END IF !switch for buoyancy

!-----------------------------------------------------------------------
!
!  Adjoint on calculating the Coriolis terms in u and v equations.
!
!  They are temporarily stored in tem1 and tem2, then added
!  to uforce and vforce.
!
!-----------------------------------------------------------------------
!
!
 IF( coriopt /= 0 ) THEN
! IF( 1==2 ) THEN !turn off coriolis term

    tlevel = tpresent

        tem1 =0.0
        tem2 =0.0
        tem3 =0.0
!
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem3(i,j,k)=wforce(i,j,k)
        END DO
      END DO
    END DO

    DO k=2,nz-2
      DO j=2,ny-1
        DO i=1,nx-1
          tem2(i,j,k)=vforce(i,j,k)
        END DO
      END DO
    END DO

    DO k=2,nz-2
      DO j=1,ny-1
        DO i=2,nx-1
          tem1(i,j,k)=uforce(i,j,k)
        END DO
      END DO
    END DO

    CALL adtlmcoriol(nx,ny,nz,                                          &
                u(1,1,1,tlevel),v(1,1,1,tlevel),w(1,1,1,tlevel),        &
                ubar,vbar,rhostr, sinlat,                               &
                tem1,tem2,tem3, tem4,tem5,tem6,tem7)

  END IF ! turn off coriolis term

!-----------------------------------------------------------------------
!
!  Adjoint on calculating the advection terms in the momentum equations,using
!  the equivalent advective formulation.
!
!  On exiting advuvw, the advection terms are stored in temporary
!  arrays tem1, tem2 and tem3, they are then added to uforce, vforce
!  and wforce respectively, which already contain the mixing
!  terms for the u, v and w equations respectively.
!
!-----------------------------------------------------------------------
!
IF( 1==1 ) THEN
  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem3(i,j,k)= -wforce(i,j,k)
      END DO
    END DO
  END DO

  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        tem2(i,j,k)= -vforce(i,j,k)
      END DO
    END DO
  END DO

  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        tem1(i,j,k)= -uforce(i,j,k)
      END DO
    END DO
  END DO
!
  CALL adtlmadvuvw(nx,ny,nz,nstepss,nbasic,                             &
          u,v,w,wcont,rhostr,ubar,vbar,nwcont,                          &
                    ubk,vbk,wbk,                                        &
          nustr,nvstr,nwstr,                                            &
          tem1,tem2,tem3,                          & ! uadv,vadv and wadv.
          tem4,tem5,tem6,tem7,tem8,tem9)
END IF !close adtlmadvuvw
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating the total mixing (which includes subgrid scale
!  turbulent mixing and computational mixing) terms for u, v, w
!  equations.
!
!  These mixing terms are accumulated in the arrays
!  uforce, vforce and wforce.
!
!  The turbulent mixing coefficient KM is saved and passed out of
!  this subroutine and used in the calculation of the turbulent
!  mixing of heat and water substances.
!
!  Since all mixing terms are evaluated at the past time level,
!  we pass only the variable fields at time tpast into the roc     we pass only the variable fields at time tpast into the routine.
!
!-----------------------------------------------------------------------
!
!
IF (1==1) THEN ! turn off mixing term
  tlevel = tpast

  CALL adtlmmixuvw(nx,ny,nz,nstepss,nbasic,                             &
              u    (1,1,1,tlevel),v   (1,1,1,tlevel),                   &
              w    (1,1,1,tlevel),                                      &
              ptprt(1,1,1,tlevel),pprt(1,1,1,tlevel),                   &
              qv   (1,1,1,tlevel),qscalar(1,1,1,1,tlevel),              &
              tke  (1,1,1,tpresent),ubar,vbar,ptbar,                    &
              pbar,rhostr,qvbar,                                        &
              usflx,vsflx, x,y,z,zp, j1,j2,j3,stabchk,                  &
              uforce,vforce,wforce, km, lendel,defsq,                   &
              tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,             &
              tem10,tem11,tem12)
END IF ! turn off mixing term
!
  RETURN
END SUBROUTINE adtlmfrcuvw

!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADTLMBUOYCY                ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE adtlmbuoycy(nx,ny,nz,nstepss,nbasic,                         &
           ptprt,pprt,qv,qscalar,                                &
           ptbar,pbar,rhostr,qvbar, wbuoy, tem1)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  ADTLMBUOYCY is the adjoint TLMBUOYCY which calculates
!  the total buoyancy including liquid and solid water loading.
!
!  TLMBUOYCY is the tangent linear model of BUOYCY.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  09/27/1994.
!
!  MODIFICATION HISTORY:
!
!  05/30/96 (Jidong Gao)
!  Converted to version 4.0.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical direction.
!    nstepss  The number of integrations plus one.
!    nbasic   Index of the current basic state time level.
!
!    ptprt    Perturbation potential temperature at a time level (K)
!    pprt     Perturbation pressure at a given time level (Pascal)
!    qv       Water vapor specific humidity at a given time level
!             (kg/kg)
!    qc       Cloud water mixing ratio at a given time level (kg/kg)
!    qr       Rainwater mixing ratio at a given time level (kg/kg)
!    qi       Cloud ice mixing ratio at a given time level (kg/kg)
!    qs       Snow mixing ratio at a given time level (kg/kg)
!    qh       Hail mixing ratio at a given time level (kg/kg)
!
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!
!
!  OUTPUT:
!
!    wbuoy    The total buoyancy force (kg/(m*s)**2)
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global model control parameters
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'      ! Physical constants
!
!
!-----------------------------------------------------------------------
!
!  Variable Declarations
!
!-----------------------------------------------------------------------
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  INTEGER :: nstepss      ! Index of total current basic state time levels.
  INTEGER :: nbasic       ! Index of the current basic state time level.
  REAL :: ptprt (nx,ny,nz)     ! Perturbation potential temperature
                               ! at a given time level (K)
  REAL :: pprt  (nx,ny,nz)     ! Perturbation pressure at a given time
                               ! level (Pascal)
  REAL :: qv    (nx,ny,nz)     ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq)     ! Cloud water mixing ratio (kg/kg)

  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific
                               ! humidity(kg/kg)

  REAL :: wbuoy(nx,ny,nz)      ! Total buoyancy in w-eq. (kg/(m*s)**2)

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array.

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,nq
  REAL :: g5,temp1

!-----------------------------------------------------------------------
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
!
  tem1 = 0.
!
!-----------------------------------------------------------------------
!
!  Then the total buoyancy:
!
!    wbuoy = tem1 * rhostr * g
!
!  averged to the w-point on the staggered grid.
!
!-----------------------------------------------------------------------
!
  g5 = g*0.5

  DO k=nz-1,2,-1
    DO j=1,ny-1
      DO i=1,nx-1
       tem1(i,j,k  )=tem1(i,j,k  ) + wbuoy(i,j,k)*rhostr(i,j,k)*g5
       tem1(i,j,k-1)=tem1(i,j,k-1) + wbuoy(i,j,k)*rhostr(i,j,k-1)*g5 !to be added back tmp.debug
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Add on the contributions to the buoyancy from the water vapor content
!  and the liquid and ice water loading.
!  The perturbation buoyancy from the water vapor content etc are not
!  available in the current TLM model.
!
!-----------------------------------------------------------------------
!
  IF( moist == 1 .AND. ice == 0 ) THEN  ! Moist case, no ice.

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
           qv(i,j,k)=tem1(i,j,k)*(1.0/(rddrv + qvbar(i,j,k))-           &
                    1./(1.0 + qvbar(i,j,k))) + qv(i,j,k)
           qscalar(i,j,k,P_QC)=-tem1(i,j,k)/(1.0 + qvbar(i,j,k))+qscalar(i,j,k,P_QC)
           qscalar(i,j,k,P_QR)=-tem1(i,j,k)/(1.0 + qvbar(i,j,k))+qscalar(i,j,k,P_QR)
        END DO
      END DO
    END DO

  ELSE IF(moist == 1 .AND. ice == 1) THEN
                              ! Full microphysics case, loading
! of liquid and ice water.

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          qv(i,j,k)=tem1(i,j,k)*(1./(rddrv + qvbar(i,j,k))-             &
                    1./(1 + qvbar(i,j,k))) + qv(i,j,k)
          DO nq = 1, nscalarq
            qscalar(i,j,k,nq)=-tem1(i,j,k)/(1.0 + qvbar(i,j,k))+qscalar(i,j,k,nq)
          END DO

        END DO
      END DO
    END DO

  END IF

!-----------------------------------------------------------------------
!
!  The total buoyancy
!
!    wbuoy = rhostr*g ( ptprt/ptbar-pprt/(rhobar*csndsq)+
!    qvprt/(rddrv+qvbar)-(qvprt+qc+qr+qs+qi+qh)/(1+qvbar(i,j,k)) )
!
!  and rddrv=rd/rv, cp, cv, rd and rv are defined in phycst.inc.
!
!  Here, the contribution from pprt (i.e., term pprt/(rhobar*csndsq))
!  is evaluated inside the small time steps, therefore wbuoy
!  does not include this part.
!
!  The contribution from ptprt is calculated inside the small time
!  steps if the potential temperature equation is solved inside
!  small time steps, i.e., if ptsmlstp=1.
!
!-----------------------------------------------------------------------
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1

         ptprt(i,j,k) = tem1(i,j,k)/ptbar(i,j,k) + ptprt(i,j,k)
          pprt(i,j,k) =-tem1(i,j,k)/(cpdcv*pbar(i,j,k)) + pprt(i,j,k)

!        ptprt(i,j,k) =-2.*tem1(i,j,k)*nptprt(i,j,k,1)                          &
!                       /ptbar(i,j,k)/ptbar(i,j,k) + ptprt(i,j,k)
!         pprt(i,j,k) =-(1-cpdcv)/cpdcv/cpdcv*tem1(i,j,k)*npprt(i,j,k,1)        &
!                       /pbar(i,j,k)/pbar(i,j,k)   + pprt(i,j,k)
!        ptprt(i,j,k) =0.5*tem1(i,j,k)*npprt(i,j,k,1)/ptbar(i,j,k)/pbar(i,j,k)  &
!                       + ptprt(i,j,k)
!         pprt(i,j,k) =0.5*tem1(i,j,k)*nptprt(i,j,k,1)/ptbar(i,j,k)/pbar(i,j,k) &
!                         + pprt(i,j,k)
!
        END DO
      END DO
    END DO
!
!-----------------------------------------------------------------------
!
!  Wash up wbuoy.
!
!-----------------------------------------------------------------------
!
       wbuoy = 0.
        tem1 = 0.
!
  RETURN
END SUBROUTINE adtlmbuoycy
