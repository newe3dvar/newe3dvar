MODULE module_ensemble
!-----------------------------------------------------------------------
!
! PURPOSE: used for hybrid ensemble input data
!
!-----------------------------------------------------------------------

  USE model_precision
  USE runtime_accts

  IMPLICIT NONE

!
!  Ensemble related parameters
!  ---------------------------
!
  INTEGER            :: enstimeopt
  INTEGER            :: nensmbl, nensvirtl
  INTEGER            :: ntimesample, stimesample
  INTEGER            :: iensmbl
  INTEGER            :: icycle
  INTEGER            :: ensfileopt, ensreadopt, ensreadready
  CHARACTER(LEN=256) :: ensdirname
  CHARACTER(LEN=10)  :: ensintdate
  CHARACTER(LEN=8)   :: ensinttime

  REAL                 :: cov_factor, gain_fac
  INTEGER              :: interpopt
  REAL,    ALLOCATABLE :: hradius_ens(:)            ! radius for hybrid
  REAL,    ALLOCATABLE :: hradius_3d_ens(:,:,:)            ! radius for hybrid
  INTEGER, ALLOCATABLE :: vradius_opt_ens(:)
  REAL,    ALLOCATABLE :: vradius_ens(:)            ! radius for hybrid
  LOGICAL              :: ref_enctrl

  REAL                 :: beta1,  beta2
  INTEGER              :: ibeta1, ibeta2

  REAL,   ALLOCATABLE  :: radius_z_ens(:,:,:)       !
  REAL,   ALLOCATABLE  :: gdscal_ens(:,:,:)         ! scaling factor
  REAL,   ALLOCATABLE  :: en_ctr   (:,:,:,:)
  REAL,   ALLOCATABLE  :: gr_en    (:,:,:,:)

  !
  ! Local working arrays
  !
  LOGICAL :: wrk_allocated

  INTEGER             :: nx_en,ny_en,nz_en
  REAL,   ALLOCATABLE :: zpm(:,:,:)

!-----------------------------------------------------------------------
!  Ensemble arrays:
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE, TARGET :: ue      (:,:,:,:) ! Total u-velocity (m/s)
  REAL, ALLOCATABLE, TARGET :: ve      (:,:,:,:) ! Total v-velocity (m/s)
  REAL, ALLOCATABLE, TARGET :: we      (:,:,:,:) ! Total w-velocity (m/s)
  REAL, ALLOCATABLE, TARGET :: pte     (:,:,:,:) ! Potential temperature (K)
  REAL, ALLOCATABLE, TARGET :: prese   (:,:,:,:) ! Pressure (Pascal)
! REAL, ALLOCATABLE, TARGET :: phe     (:,:,:,:) ! Perturbation Geopotential in WRF
! REAL, ALLOCATABLE, TARGET :: mue     (:,:,:)  ! perturbation dry air mass in column (Pa)
  REAL, ALLOCATABLE, TARGET :: qve     (:,:,:,:) ! Water vapor specific humidity (kg/kg)
  REAL, ALLOCATABLE, TARGET :: qscalare(:,:,:,:,:)

  REAL, ALLOCATABLE :: tkee     (:,:,:,:) ! Turbulent Kinetic Energy ((m/s)**2)

  REAL, ALLOCATABLE :: tsoile   (:,:,:,:,:)! Deep soil temperature (K)
  REAL, ALLOCATABLE :: qsoile   (:,:,:,:,:)! Deep soil temperature (K)
  REAL, ALLOCATABLE :: wetcanpe (:,:,:,:)! Canopy water amount
  REAL, ALLOCATABLE :: snowdpthe(:,:,:) ! Snow depth (m)
  REAL, ALLOCATABLE :: rainge   (:,:,:)   ! Grid supersaturation rain
  REAL, ALLOCATABLE :: raince   (:,:,:)   ! Cumulus convective rain
  REAL, ALLOCATABLE :: prcratee (:,:,:,:)! precipitation rate (kg/(m**2*s))
  REAL, ALLOCATABLE :: radfrce  (:,:,:,:) ! Radiation forcing (K/s)
  REAL, ALLOCATABLE :: radswe   (:,:,:)   ! Solar radiation reaching the surface
  REAL, ALLOCATABLE :: rnflxe   (:,:,:)   ! Net radiation flux absorbed by surface
  REAL, ALLOCATABLE :: radswnete(:,:,:) ! Net solar radiation, SWin - SWout
  REAL, ALLOCATABLE :: radlwine (:,:,:) ! Incoming longwave radiation
  REAL, ALLOCATABLE :: usflxe   (:,:,:) ! Surface flux of u-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE :: vsflxe   (:,:,:) ! Surface flux of v-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE :: ptsflxe  (:,:,:) ! Surface heat flux (K*kg/(m*s**2))
  REAL, ALLOCATABLE :: qvsflxe  (:,:,:) ! Surface moisture flux (kg/(m**2*s))

!-----------------------------------------------------------------------
!  Reflectivity arrays
!-----------------------------------------------------------------------

  REAL,    ALLOCATABLE :: rhobare (:,:,:,:) ! Air density (kg/m^3)
  REAL(P), ALLOCATABLE :: ref_ptbe(:,:,:,:)
  REAL(P), ALLOCATABLE :: ref_bkgavg(:,:,:)
  LOGICAL :: use_rhobare

!-----------------------------------------------------------------------
!  Ensemble to control interpolation arrays
!-----------------------------------------------------------------------
!
  INTEGER              :: mfactor
  INTEGER, ALLOCATABLE :: iim(:,:), jjm(:,:)
  REAL,    ALLOCATABLE :: wm11(:,:), wm21(:,:), wm12(:,:), wm22(:,:)

  CONTAINS

!#######################################################################

  SUBROUTINE ens_read_nml(unum,npass,myproc,dx,dy,istatus)
  !
  !---------------------------------------------------------------------
  !
  !  PURPOSE:
  !  Read in analysis variables in namelist format from standard input.
  !
  !  AUTHOR:
  !  Jidong GAO, add 3dvar input parameter, 2001
  !
  !  MODIFICATION HISTORY:
  !
  !
  !---------------------------------------------------------------------
  !
  !  Variable Declarations:
  !
  !---------------------------------------------------------------------

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: npass
    INTEGER, INTENT(IN)  :: unum, myproc
    REAL,    INTENT(IN)  :: dx, dy

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    NAMELIST /var_ens/ nensmbl,ntimesample, stimesample, iensmbl,icycle,&
                       enstimeopt, interpopt,cov_factor, gain_fac,      &
                       ensdirname,ensfileopt,ensreadopt,ensreadready,   &
                       hradius_ens,vradius_opt_ens, vradius_ens,        &
                       ensintdate,ensinttime,ref_enctrl

  !---------------------------------------------------------------------

    INTEGER :: i
    CHARACTER(LEN=32) :: fmtstr

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !

    ALLOCATE( hradius_ens(npass),    STAT=istatus)
    CALL check_alloc_status(istatus, "hradius_ens")

    ALLOCATE( vradius_opt_ens(npass),STAT=istatus)
    CALL check_alloc_status(istatus, "vradius_opt_ens")

    ALLOCATE( vradius_ens(npass),    STAT=istatus)
    CALL check_alloc_status(istatus, "vradius_ens")
  !
  !---------------------------------------------------------------------
  !
  !  Assign default values to the 3DVAR input variables
  !
  !---------------------------------------------------------------------
  !
    enstimeopt = 1
    nensmbl = 3
    ntimesample = 1
    stimesample = 120
    iensmbl = 1
    icycle  = 1
    ensdirname = './'
    ensfileopt = 3
    ensreadopt = 1
    ensreadready = 0
    interpopt  = 1

    cov_factor         = 0.0
    gain_fac           = 0.0
    hradius_ens(:)     = 3.0
    vradius_opt_ens(:) = 0
    vradius_ens(:)     = 3.0

    ref_enctrl = .FALSE.
  !
  !---------------------------------------------------------------------
  !
  !  read in VAR_ENS namelists
  !
  !---------------------------------------------------------------------
  !
    IF (myproc == 0) THEN
      READ(unum, var_ens,  END=451)
      WRITE(6,*) 'Namelist block 3dvar_ens sucessfully read.'

      GOTO 450

      451 CONTINUE

      WRITE(6,*) 'ERROR: reading namelist block VAR_ENS.'
      CALL arpsstop('ERROR: read namelist block VAR_ENS.',1)

      450 CONTINUE

      IF (enstimeopt == 2) WRITE(6,*) 'ensemble background time = ',ensintdate(1:10),'_',ensinttime(1:8)
      WRITE(*,'(5x,3(a,I4),a)') 'nensmbl         = ', nensmbl,', ntimesample = ', ntimesample,', stimesample = ', stimesample,','
      WRITE(*,'(5x,a,I4,a)')    'iensmbl         = ', iensmbl,','
      WRITE(*,'(5x,a,I4,a)')    'icycle          = ', icycle,','
      WRITE(*,'(5x,a,F8.5,a)')  'cov_factor      = ', cov_factor,','
      WRITE(*,'(5x,a,F8.5,a)')  'gain_fac        = ', gain_fac,','
      WRITE(*,'(5x,a,I4,a)')    'interpopt       = ', interpopt,','

      WRITE(fmtstr,'(a,I0,a)') '(5x,a,',npass,'(I8,a))'
      WRITE(*,FMT=fmtstr)   'vradius_opt_ens = ',(vradius_opt_ens(i),', ',i=1,npass)

      WRITE(fmtstr,'(a,I0,a)') '(5x,a,',npass,'(F8.4,a))'
      WRITE(*,FMT=fmtstr)   'hradius_ens     = ',(hradius_ens(i),', ',i=1,npass)
      WRITE(*,FMT=fmtstr)   'vradius_ens     = ',(vradius_ens(i),', ',i=1,npass)
      WRITE(*,'(5x,3a)')    'ensdirname      = "',TRIM(ensdirname),'",'
      WRITE(*,'(5x,a,i0,a)')'ensfileopt      = ',ensfileopt,','
      WRITE(*,'(5x,2(a,i0),a)') 'ensreadopt      = ',ensreadopt,',  ensreadready = ',ensreadready,','

      WRITE(*,'(5x,a,L ,a)')    'ref_enctrl      = ', ref_enctrl,','

    END IF

    CALL mpupdatei( enstimeopt,    1)
    IF (enstimeopt == 2) THEN
      CALL mpupdatei( ensintdate, 10)
      CALL mpupdatei( ensinttime,  8)
    END IF
    CALL mpupdatei( nensmbl,     1)
    CALL mpupdatei( ntimesample, 1)
    CALL mpupdatei( stimesample, 1)
    CALL mpupdatei( iensmbl,     1)
    CALL mpupdatei( icycle,      1)

    CALL mpupdatec(ensdirname,   256)
    CALL mpupdatei(ensfileopt,   1)
    CALL mpupdatei(ensreadopt,   1)
    CALL mpupdatei(ensreadready, 1)

    CALL mpupdater(cov_factor,      1)
    CALL mpupdatei(interpopt,       1)
    CALL mpupdater(hradius_ens,     npass)
    CALL mpupdatei(vradius_opt_ens, npass)
    CALL mpupdater(vradius_ens,     npass)
    CALL mpupdatel(ref_enctrl,1)

    IF (myproc == 0) THEN
      WRITE(*,'(/,1x,a)') 'Analysis Horizontal Ensmbl      Vertical Ensmbl          '
      WRITE(*,'(1x,a)')   '  pass   influence radius      influence radius          '
      WRITE(*,'(1x,a)')   '-------- ================= ++++++++++++++++++++++++++++++'
    END IF

    DO i=1,npass
      hradius_ens(i)= hradius_ens(i)*1000.0/dx
      IF (myproc == 0) THEN
        WRITE(*,'(1x,I4,F9.2,a13,a10,I2)',ADVANCE='NO')  i,hradius_ens(i),'(grid points)','option - ',vradius_opt_ens(i)
        IF (vradius_opt_ens(i) == 1 ) THEN
          WRITE(*,'(F6.2,a13)') vradius_ens(i), '(grid points)'
        ELSE IF (vradius_opt_ens(i) == 2 ) THEN
          WRITE(*,'(F6.2,a13)') vradius_ens(i), '(km)'
        ELSE
          WRITE(*,*) 'vradius_opt_ens = ', vradius_opt_ens(i), ' is not supported.'
          WRITE(*,*) 'ARPS3DVAR will stop here'
          CALL arpsstop('ERROR: Wrong vradius_opt_ens.',1)
        END IF
      END IF
    END DO

    IF (myproc == 0) THEN
      WRITE(*,'(1x,a)')   '---------------------------------------------------------'
    END IF

    IF (interpopt == 2) THEN
      IF (ensreadopt < 2) THEN
        WRITE(*,*) 'WARNING: interpopt = 2 only works with ensreadopt = 2/3 when memory is not a concern on the platform.'
        WRITE(*,*) '         reset interpopt = 1.'
        interpopt = 1
        !CALL arpsstop('ERROR: Wrong combination of interpopt & ensreadopt.',1)
      END IF
    END IF

    nensvirtl = nensmbl * ntimesample
  !---------------------------------------------------------------------

    beta2 = cov_factor
    beta1 = 1.0-cov_factor

    IF( beta1 < 1.0E-10 ) THEN
       beta1  = 0.0
       ibeta1 = 0
    ELSE
       beta1  = sqrt(beta1)
       ibeta1 = 1
    END IF

    IF( beta2 < 1.0E-10 ) THEN
       beta2  = 0.0
       ibeta2 = 0
    ELSE
       beta2  = sqrt(beta2)
       ibeta2 = 1
    END IF

    IF (myproc == 0) print *,'**** ibeta1 =',ibeta1, ', ibeta2 = ',ibeta2,' ****'

    wrk_allocated = .FALSE.

    RETURN
  END SUBROUTINE ens_read_nml

  !####################################################################

  SUBROUTINE ens_allocate_varwrk(nx,ny,nz,istatus)

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nx,ny,nz
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (ibeta2 == 1) THEN
      ALLOCATE( hradius_3d_ens(nx,ny,nz),    STAT=istatus)
      CALL check_alloc_status(istatus, "hradius_3d_ens")
      hradius_3d_ens = hradius_ens(1)

      ALLOCATE(en_ctr(nx,ny,nz,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "en_ctr")
      en_ctr=0.0

      ALLOCATE ( gr_en(nx,ny,nz,nensvirtl), STAT = istatus )
      CALL check_alloc_status(istatus, "gradt:gr_en")
      gr_en = 0.0

      ALLOCATE( radius_z_ens(nx,ny,nz), stat=istatus)
      CALL check_alloc_status(istatus, "radius_z_ens")
      radius_z_ens(:,:,:) = 0.0

      IF ( ALLOCATED(gdscal_ens) ) DEALLOCATE (gdscal_ens)
      ALLOCATE ( gdscal_ens(nx,ny,nz) , stat=istatus)
      CALL check_alloc_status(istatus, "3dvar:gdscal_ens")
      gdscal_ens(:,:,:) = 1.0

      wrk_allocated = .TRUE.

    END IF

    RETURN
  END SUBROUTINE ens_allocate_varwrk

  !####################################################################

  SUBROUTINE ens_deallocate_varwrk(istatus)

    IMPLICIT NONE

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (wrk_allocated) THEN

      DEALLOCATE ( en_ctr,       STAT = istatus )
      DEALLOCATE ( gr_en,        STAT = istatus )

      DEALLOCATE ( radius_z_ens, STAT = istatus )
      DEALLOCATE ( gdscal_ens ,  STAT = istatus )

      wrk_allocated = .FALSE.
    END IF

    RETURN
  END SUBROUTINE ens_deallocate_varwrk

  !####################################################################

  SUBROUTINE ens_zradius(ipass,nx,ny,nz,zp,myproc,istatus)
  !---------------------------------------------------------------------
  !
  ! Purpose: to specifiy vertical influence radius
  !
  !---------------------------------------------------------------------
     INTEGER, INTENT(IN)  :: ipass
     INTEGER, INTENT(IN)  :: nx,ny,nz
     INTEGER, INTENT(IN)  :: myproc
     REAL,    INTENT(IN)  :: zp(nx,ny,nz)
     INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------
     INTEGER :: i,j,k
     INTEGER :: nL, nU, nV
     REAL    :: rL,rU,rrr

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
    istatus = 0

    IF(vradius_opt_ens(ipass) ==1) THEN  ! vradius is in grid points
      DO j =1, ny
        DO i =1, nx
          DO k =1, nz
            radius_z_ens(i,j,k)=vradius_ens(ipass)
          END DO
        END DO
      END DO
    ELSE IF(vradius_opt_ens(ipass) ==2) THEN  ! vradius in in unit of km
      DO j =1, ny-1
        DO i =1, nx-1
          rrr=vradius_ens(ipass)*1000.0  !user-specified vertical radius in meters
          DO k =2, nz-1
            nL=0; nU=0; rL =0.0; rU=0.0
            ! count upward
            DO nV =k, nz-1
              rU = zp(i,j,nV) - zp(i,j,k)
              IF ( rU >= rrr ) THEN
                nU = nV - k
                exit ! found nU, exit the do loop
              END IF
            END DO
            IF (nU == 0 .and. nV >= nz-1 ) nU = nz -1 - k
            ! count downward
            DO nV = k, 2, -1
              rL = zp(i,j,k) - zp(i,j,nV)
              IF ( rL >= rrr ) THEN
                nL = k - nV
                exit ! found nL, exit the do loop
              END IF
            END DO
            IF (nL == 0 .and. nV <= 2 ) nL = k - 2

            radius_z_ens(i,j,k) = ( nL * max(rrr, rL) + nU * max(rrr, rU) ) / ( rU + rL )
          END DO
          radius_z_ens(i,j,1) = radius_z_ens(i,j,2)
          radius_z_ens(i,j,nz) = radius_z_ens(i,j,nz-1)
        END DO
      END DO

      DO k = 1, nz
        DO j = 1, ny-1
          radius_z_ens(nx,j,k) = radius_z_ens(nx-1,j,k)
        END DO
      END DO

      DO k = 1, nz
        DO i = 1, nx
          radius_z_ens(i,ny,k) = radius_z_ens(i,ny-1,k)
        END DO
      END DO

    END IF ! IF (vradius_opt_ref(ipass) ==1 or 2)
    IF (myproc == 0 ) THEN
      i=3; j =3
      WRITE(*,*) 'The vradius for ensemble is specified in grid points as:'
      WRITE(*,'(1x,4a,2(a,i0))') ' k ','    zp    ','   dzp    ','vradius_ens',' at i=',i,',j=',j
      WRITE(*,'(1x,a)' ) '--- --------- --------- ----------- '
      DO k =1, nz-1
        WRITE(*,'(1x,I3,3F10.2)') k, zp(i,j,k), zp(i,j,k+1)-zp(i,j,k), radius_z_ens(i,j,k)
      END DO
    END IF !IF ( myproc==0 ) THEN

    RETURN
  END SUBROUTINE ens_zradius

  !####################################################################

  SUBROUTINE ens_hydro_perturb(myproc,nx,ny,nz,nscalar,tk,ref_bkg,      &
                               ref_mos_3d,thresh_ref,gdqscalar_err,     &
                               P_QR,P_QS,P_QG,P_QH,tem1,istatus)

    INTEGER, INTENT(IN)  :: myproc
    INTEGER, INTENT(IN)  :: nx,ny,nz,nscalar
    INTEGER, INTENT(IN)  :: P_QR, P_QS, P_QG, P_QH
    REAL,    INTENT(IN)  :: tk(nx,ny,nz),ref_bkg(nx,ny,nz)
    REAL,    INTENT(IN)  :: ref_mos_3d(nx,ny,nz),thresh_ref
    REAL,    INTENT(IN)  :: gdqscalar_err(nx,ny,nz,nscalar)

    REAL,    INTENT(INOUT)  :: tem1(nx,ny,nz)

    INTEGER, INTENT(OUT) :: istatus
  !---------------------------------------------------------------------

    INTEGER :: i,j,k,l,nq
    INTEGER :: ii,jj

    REAL :: errq(nscalar)

    INTEGER              :: iseed1
    INTEGER, PARAMETER   :: iseed=-100

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    iseed1 = (iseed-50*iensmbl - icycle) * (myproc + 1)

    IF (interpopt == 2) THEN

      DO k = 1,nz-1
        DO j = 2,ny-1
          DO i = 2,nx-1

            DO nq = 1,nscalar
              errq(nq) = gdqscalar_err(i,j,k,nq)
            END DO

            IF (ref_mos_3d(i,j,k) >= thresh_ref) THEN

              IF ( icycle <= 3 ) THEN
                CALL ens_mixr_add_noise(i,j,k,tk(i,j,k),5.0,nscalar,errq,  &
                                        P_QR,P_QS,P_QG,P_QH,               &
                                         2.0E-5,2.0E-5,1.0E-5,1.0E-5,       &
                                        iseed1,istatus)
              END IF

            ELSE IF (ref_mos_3d(i,j,k) >= 0) THEN

              IF (ref_bkg(i,j,k) > 0) THEN
                CALL ens_mixr_add_noise(i,j,k,tk(i,j,k),5.0,nscalar,errq,  &
                                        P_QR,P_QS,P_QG,P_QH,               &
                                         1.0E-4,1.0E-2,1.0E-4,1.0E-4,       &
                                        iseed1,istatus)
              END IF

            END IF      !ref>thresh

          END DO
        END DO
      END DO

    ELSE

    !-------------------------------------------------------------------
    !
    ! Note that Ensemble domain is one grid (analysis grid) larger than
    ! the analysis domain on each side when mfactor = 2.
    ! So no valid value from the analysis domain to set the ensemble
    ! domain.
    !
    ! nx_en/ny_en/nz_en is ensemble domain size
    !-------------------------------------------------------------------

      DO k = 1,nz_en
        DO j = 2,ny_en-1
          jj = (j-2)*mfactor+2
          DO i = 2,nx_en-1
            ii = (i-2)*mfactor+2

            DO nq = 1,nscalar
              errq(nq) = gdqscalar_err(ii,jj,k,nq)
            END DO

            IF (ref_mos_3d(ii,jj,k) >= thresh_ref) THEN

              IF ( icycle <= 3 ) THEN
                CALL ens_mixr_add_noise(i,j,k,tk(ii,jj,k),5.0,nscalar,errq, &
                                        P_QR,P_QS,P_QG,P_QH,                &
                                         2.0E-5,2.0E-5,1.0E-5,1.0E-5,       &
                                        iseed1,istatus)
              END IF

            ELSE IF (ref_mos_3d(ii,jj,k) >= 0) THEN

              IF (ref_bkg(ii,jj,k) > 0) THEN
                CALL ens_mixr_add_noise(i,j,k,tk(ii,jj,k),5.0,nscalar,errq, &
                                        P_QR,P_QS,P_QG,P_QH,                &
                                         1.0E-4,1.0E-2,1.0E-4,1.0E-4,       &
                                        iseed1,istatus)
              END IF

            END IF

          END DO
        END DO
      END DO

    END IF

    DO l=1, nensvirtl

      IF (P_QR > 0) THEN
        CALL mpsendrecv2dew(qscalare(:,:,:,P_QR,l),nx_en,ny_en,nz_en,0,0,0,tem1)
        CALL mpsendrecv2dns(qscalare(:,:,:,P_QR,l),nx_en,ny_en,nz_en,0,0,0,tem1)

        CALL smooth3d(nx_en,ny_en,nz_en,1,nx_en-1,1,ny_en-1,1,nz_en-1,0,0.20,zpm, &
                      qscalare(:,:,:,P_QR,l),tem1,qscalare(:,:,:,P_QR,l))
      END IF

      IF (P_QS > 0) THEN
        CALL mpsendrecv2dew(qscalare(:,:,:,P_QS,l),nx_en,ny_en,nz_en,0,0,0,tem1)
        CALL mpsendrecv2dns(qscalare(:,:,:,P_QS,l),nx_en,ny_en,nz_en,0,0,0,tem1)

        CALL smooth3d(nx_en,ny_en,nz_en,1,nx_en-1,1,ny_en-1,1,nz_en-1,0,0.20,zpm, &
                      qscalare(:,:,:,P_QS,l),tem1,qscalare(:,:,:,P_QS,l))
      END IF

      IF (P_QG > 0) THEN
        CALL mpsendrecv2dew(qscalare(:,:,:,P_QG,l),nx_en,ny_en,nz_en,0,0,0,tem1)
        CALL mpsendrecv2dns(qscalare(:,:,:,P_QG,l),nx_en,ny_en,nz_en,0,0,0,tem1)

        CALL smooth3d(nx_en,ny_en,nz_en,1,nx_en-1,1,ny_en-1,1,nz_en-1,0,0.20,zpm, &
                      qscalare(:,:,:,P_QG,l),tem1,qscalare(:,:,:,P_QG,l))
      END IF

      IF (P_QH > 0) THEN
        CALL mpsendrecv2dew(qscalare(:,:,:,P_QH,l),nx_en,ny_en,nz_en,0,0,0,tem1)
        CALL mpsendrecv2dns(qscalare(:,:,:,P_QH,l),nx_en,ny_en,nz_en,0,0,0,tem1)

        CALL smooth3d(nx_en,ny_en,nz_en,1,nx_en-1,1,ny_en-1,1,nz_en-1,0,0.20,zpm, &
                      qscalare(:,:,:,P_QH,l),tem1,qscalare(:,:,:,P_QH,l))
      END IF

    END DO

    RETURN
  END SUBROUTINE ens_hydro_perturb

  !#####################################################################

  SUBROUTINE ens_mixr_add_noise(i,j,k,temperature,temp_adjust,nscalar,err_qscalar,   &
                                P_QR,P_QS,P_QG,P_QH,coef_QR,coef_QS,coef_QG,coef_QH, &
                                seed,istatus)

    INTEGER, INTENT(IN)  :: i,j,k,seed
    INTEGER, INTENT(IN)  :: nscalar,P_QR,P_QS,P_QG,P_QH
    REAL,    INTENT(IN)  :: temperature,temp_adjust,err_qscalar(nscalar)
    REAL,    INTENT(IN)  :: coef_QR,coef_QS,coef_QG,coef_QH

    INTEGER, INTENT(OUT) :: istatus

    REAL, PARAMETER      :: degKtoC=273.15
  !---------------------------------------------------------------------

    REAL                 :: ran3, gasdev

    INTEGER              :: l
    REAL                 :: thresh_liquidtemp, thresh_icetemp

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    thresh_liquidtemp = degKtoC - temp_adjust
    thresh_icetemp    = degKtoC + temp_adjust

    DO l=1, nensvirtl

      IF ( temperature>thresh_liquidtemp .AND. P_QR>0 ) THEN
        qscalare(i,j,k,P_QR,l) = qscalare(i,j,k,P_QR,l) +               &
                                 gasdev(seed)*err_qscalar(P_QR)*coef_QR
      END IF
      IF ( temperature<thresh_icetemp .AND. P_QS>0 ) THEN
        qscalare(i,j,k,P_QS,l) = qscalare(i,j,k,P_QS,l) +               &
                                 gasdev(seed)*err_qscalar(P_QS)*coef_QS
      END IF
      IF ( temperature<thresh_icetemp .AND. P_QG>0 ) THEN
        qscalare(i,j,k,P_QG,l) = qscalare(i,j,k,P_QG,l) +               &
                                 gasdev(seed)*err_qscalar(P_QG)*coef_QG
      END IF
      IF ( temperature<thresh_icetemp .AND. P_QH>0 ) THEN
        qscalare(i,j,k,P_QH,l) = qscalare(i,j,k,P_QH,l) +               &
                                 gasdev(seed)*err_qscalar(P_QH)*coef_QH
      END IF

    END DO

    RETURN
  END SUBROUTINE ens_mixr_add_noise

  !#####################################################################

  SUBROUTINE allocate4DarrayE(model_opt,ref_use,nzsoil,nstyps,nscalar,istatus)

    INTEGER, INTENT(IN)  :: nzsoil,nstyps
    INTEGER, INTENT(IN)  :: nscalar,model_opt
    LOGICAL, INTENT(IN)  :: ref_use
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ALLOCATE(zpm(nx_en,ny_en,nz_en), STAT = istatus)

    ALLOCATE(ue     (nx_en,ny_en,nz_en,nensvirtl),stat=istatus)
    CALL check_alloc_status(istatus, "ue")
    ALLOCATE(ve     (nx_en,ny_en,nz_en,nensvirtl),stat=istatus)
    CALL check_alloc_status(istatus, "ve")
    ALLOCATE(we     (nx_en,ny_en,nz_en,nensvirtl),stat=istatus)
    CALL check_alloc_status(istatus, "we")
    ALLOCATE(pte (nx_en,ny_en,nz_en,nensvirtl),stat=istatus)
    CALL check_alloc_status(istatus, "pte")
    ALLOCATE(prese  (nx_en,ny_en,nz_en,nensvirtl),stat=istatus)
    CALL check_alloc_status(istatus, "prese")

    ue=0.0
    ve=0.0
    we=0.0
    pte=0.0
    prese=0.0

    ALLOCATE(qve  (nx_en,ny_en,nz_en,nensvirtl),stat=istatus)
    CALL check_alloc_status(istatus, "qve")
    ALLOCATE(qscalare(nx_en,ny_en,nz_en,nscalar,nensvirtl),stat=istatus)

    qve=0.0
    qscalare=0.0

    IF (model_opt==1) THEN
      ALLOCATE(tkee (nx_en,ny_en,nz_en,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "tkee")

      ALLOCATE(tsoile   (nx_en,ny_en,nzsoil,0:nstyps,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "tsoile")
      ALLOCATE(qsoile   (nx_en,ny_en,nzsoil,0:nstyps,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "qsoile")
      ALLOCATE(wetcanpe (nx_en,ny_en,0:nstyps,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "wetcanpe")
      ALLOCATE(snowdpthe (nx_en,ny_en,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "snowdpthe")

      tkee=0.0
      tsoile=0.0
      qsoile=0.0
      wetcanpe=0.0
      snowdpthe=0.0

      ALLOCATE(rainge(nx_en,ny_en,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "rainge")
      ALLOCATE(raince(nx_en,ny_en,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "raince")
      ALLOCATE(prcratee(nx_en,ny_en,4,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "prcratee")
      ALLOCATE(radfrce(nx_en,ny_en,nz_en,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "radfrce")
      ALLOCATE(radswe (nx_en,ny_en,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "radswe")
      ALLOCATE(rnflxe (nx_en,ny_en,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "rnflxe")
      ALLOCATE(radswnete (nx_en,ny_en,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "radswnete")
      ALLOCATE(radlwine (nx_en,ny_en,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "radlwine")
      ALLOCATE(usflxe (nx_en,ny_en,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "usflxe")
      ALLOCATE(vsflxe (nx_en,ny_en,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "vsflxe")
      ALLOCATE(ptsflxe(nx_en,ny_en,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "ptsflxe")
      ALLOCATE(qvsflxe(nx_en,ny_en,nensvirtl),stat=istatus)
      CALL check_alloc_status(istatus, "qvsflxe")

      rainge=0.0
      raince=0.0
      prcratee=0.0
      radfrce=0.0
      radswe=0.0
      rnflxe=0.0
      radswnete=0.0
      radlwine=0.0
      usflxe=0.0
      vsflxe=0.0
      ptsflxe=0.0
      qvsflxe=0.0

    END IF

    ALLOCATE(rhobare(nx_en,ny_en,nz_en,nensvirtl),stat=istatus)
    CALL check_alloc_status(istatus, "rhobare")
    rhobare=0.0

    use_rhobare = .FALSE.
    IF (ref_use .AND. ref_enctrl) THEN
      use_rhobare = .TRUE.

      ALLOCATE(ref_ptbe  (nx_en,ny_en,nz_en,nensvirtl),  STAT=istatus)
      ALLOCATE(ref_bkgavg(nx_en,ny_en,nz_en),          STAT=istatus)
    END IF

    RETURN
  END SUBROUTINE allocate4DarrayE

  !#####################################################################

  SUBROUTINE deallocate4DarrayE(model_opt,istatus)

    INTEGER, INTENT(IN)  :: model_opt
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    DEALLOCATE(ue,ve,we,pte,prese,qve,qscalare)

    IF (model_opt==1) THEN
      DEALLOCATE(tkee,tsoile,qsoile,wetcanpe,                           &
                 snowdpthe,rainge,raince,prcratee,                      &
                 radfrce,radswe,rnflxe,radswnete,                       &
                 radlwine,usflxe,vsflxe,ptsflxe,qvsflxe)
    END IF

    DEALLOCATE( rhobare )
    IF (ALLOCATED(ref_ptbe)) DEALLOCATE(ref_ptbe,ref_bkgavg)

    DEALLOCATE(iim,jjm)
    DEALLOCATE(wm11, wm12, wm21, wm22)

    DEALLOCATE(zpm)

  END SUBROUTINE deallocate4DarrayE

  !#####################################################################

  SUBROUTINE enrefcalc4var(nx,ny,nz,ref_mos_3d,                         &
                           nqr,nqs,nqh,npt,rho,tem1,istatus)

    USE module_varpara, only: intvl_T,thresh_ref

    IMPLICIT NONE

    INCLUDE "globcst.inc"

    INTEGER, INTENT(IN) :: nx,ny,nz
    REAL(P), INTENT(IN) :: ref_mos_3d(nx,ny,nz)
    REAL(P), INTENT(INOUT) :: nqr(nx_en,ny_en,nz_en),nqs(nx_en,ny_en,nz_en),nqh(nx_en,ny_en,nz_en)
    REAL(P), INTENT(INOUT) :: npt(nx_en,ny_en,nz_en),rho(nx_en,ny_en,nz_en)
    REAL(P), INTENT(INOUT) :: tem1(nx_en,ny_en,nz_en)

    INTEGER, INTENT(OUT)   :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j,k
    INTEGER :: ii,jj

    INTEGER :: l

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


    ref_ptbe=0.0;ref_bkgavg=0.0

      IF (interpopt == 2) THEN      ! use nx/ny/nz directly
        DO k = 1, nz
          DO j = 1, ny
            DO i = 1, nx
              tem1(i,j,k) = ref_mos_3d(i,j,k)
            END DO
          END DO
        END DO
      ELSE IF (interpopt == 1) THEN  ! extract from nx/ny/nz to nx_m/ny_m/nz_m
        DO k = 1, nz_en
          DO j = 1, ny_en
            jj = (j-1)*mfactor+1
            DO i = 1, nx_en
              ii = (i-1)*mfactor+1
              tem1(i,j,k) = ref_mos_3d(ii,jj,k)
            END DO
          END DO
        END DO

      END IF

    DO l=1,nensvirtl

      nqr=qscalare(:,:,:,P_QR,l)
      nqs=qscalare(:,:,:,P_QS,l)
      nqh=qscalare(:,:,:,P_QH,l)
      npt=pte(:,:,:,l)
      rho=rhobare(:,:,:,l)

      CALL reflec_g(nx_en,ny_en,nz_en,tem1,intvl_T,thresh_ref,rho,      &
                    nqr, nqs, nqh, npt, ref_ptbe(:,:,:,l))

      ref_bkgavg=ref_bkgavg+ref_ptbe(:,:,:,l)

      nqr=0.0;nqs=0.0;nqh=0.0;npt=0.0;rho=0.0

    END DO

    ref_bkgavg=ref_bkgavg/nensvirtl
    DO l=1,nensvirtl
      ref_ptbe(:,:,:,l)=ref_ptbe(:,:,:,l)-ref_bkgavg
    END DO

    !NULLIFY(nqr,nqs,nqh,npt,rho)

    RETURN
  END SUBROUTINE enrefcalc4var

  !#####################################################################

  SUBROUTINE ens2ctrl_init(nx,ny,nz,dx,dy,zp,nx_m,ny_m,nz_m,dx_m,dy_m,istatus)
  !---------------------------------------------------------------------
  !
  ! Initialize interpolation arrays from ensemble members (usually coarse resolution)
  ! to control grid (higher resolution).
  !
  !---------------------------------------------------------------------
    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL,    INTENT(IN)  :: dx, dy
    REAL,    INTENT(IN)  :: zp(nx,ny,nz)
    INTEGER, INTENT(IN)  :: nx_m, ny_m, nz_m
    REAL,    INTENT(IN)  :: dx_m, dy_m
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------
    REAL    :: dxf, dyf
    INTEGER :: i,j,k,ii,jj
    INTEGER :: im11, im22, jm11, jm22, imod, jmod

    REAL    :: denorm

    !INTEGER :: id(1), jd(1)

    REAL, PARAMETER :: epsilon = 1.0E-3

    !include 'mp.inc'
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    dxf = dx_m/dx
    dyf = dy_m/dy

    !-------------------------------------------------------------------
    !
    ! Condition check
    !
    !-------------------------------------------------------------------

    IF (nz /= nz_m) THEN
      istatus = -1
      WRITE(*,'(1x,a,2(I0,a))') 'ERROR: expecting same vertical dimensions. nz = ',nz,', nz_m = ',nz_m,'.'
      RETURN
    END IF

    IF (ABS(dxf-dyf) > epsilon) THEN    ! same factor in X directory and Y directory
      istatus = -2
      WRITE(*,'(1x,2a,2(F8.3,a))') 'ERROR: Unsupported mode.',          &
                          ' dx factor = ',dxf, ', dy factor = ',dyf,'.'
      RETURN
    END IF

    mfactor = FLOOR(dxf)
    IF (ABS(dxf-mfactor) > epsilon) THEN    ! must be exact integer
      istatus = -3
      WRITE(*,'(1x,2a,3(F8.3,a),I3,a)') 'ERROR: Unsupported mode.',     &
                           ' dx = ',dx,', dx_m = ',dx_m,                &
                   ', dx factor = ',dxf, ', mfactor = ',mfactor,'.'
      RETURN
    END IF

    IF (interpopt == 1) THEN
      nx_en = nx_m
      ny_en = ny_m
      nz_en = nz_m
    ELSE IF (interpopt == 2) THEN
      nx_en = nx
      ny_en = ny
      nz_en = nz
    ELSE
      WRITE(*,'(1x,a,I0)') 'ERROR: unsupported interpopt = ',interpopt
      istatus = -1
      RETURN
    END IF

    !-------------------------------------------------------------------
    !
    ! get interpolation arrays
    !
    !-------------------------------------------------------------------

    ALLOCATE(iim(nx,2),   STAT = istatus)
    ALLOCATE(jjm(ny,2),   STAT = istatus)
    ALLOCATE(wm11(nx,ny), STAT = istatus)
    ALLOCATE(wm12(nx,ny), STAT = istatus)
    ALLOCATE(wm21(nx,ny), STAT = istatus)
    ALLOCATE(wm22(nx,ny), STAT = istatus)

    !write(301+myproc,*) nx,ny,nz, nx_m,ny_m,nz_m
    denorm = mfactor*mfactor*1.0
    DO j=1,ny     ! map from _m dimensions to original dimensions
      jm11 = (j-2)/mfactor+2         ! Align at index 2
      jmod = MOD((j-2),mfactor)
      jm22 = jm11+MIN(jmod,1)
      !IF (jm22 > ny_m) jm22 = 1

      jjm(j,1) = jm11
      jjm(j,2) = jm22
      jmod     = ABS(jmod)
      DO i=1,nx

        im11 = (i-2)/mfactor+2
        imod = MOD((i-2),mfactor)
        im22 = im11+MIN(imod,1)
        !IF (im22 > nx_m) im22 = 1

        iim(i,1) = im11
        iim(i,2) = im22
        imod     = ABS(imod)

        wm11(i,j) = (mfactor-imod)*(mfactor-jmod)/denorm
        wm21(i,j) = imod*(mfactor-jmod)/denorm
        wm12(i,j) = (mfactor-imod)*jmod/denorm
        wm22(i,j) = imod*jmod/denorm

        !WRITE(301+myproc,'(1x,2I3,a,4(2I3,a,F5.3,a))') i,j,'-> (',      &
        !                       im11,jm11,';',wm11(i,j),'), (',          &
        !                       im11,jm22,';',wm12(i,j),'), (',          &
        !                       im22,jm11,';',wm21(i,j),'), (',          &
        !                       im22,jm22,';',wm22(i,j),').'
      END DO
    END DO
    !WRITE(0,*) 'dims =', nx,ny,nz, nx_m,ny_m,nz_m
    !WRITE(0,*) 'iim/jjm = ',MAXVAL(iim(:,1)),MAXVAL(iim(:,2)),MAXVAL(jjm(:,1)),MAXVAL(jjm(:,2))
    !WRITE(0,*) 'index   = ',MAXLOC(iim(:,1)),MAXLOC(iim(:,2)),MAXLOC(jjm(:,1)),MAXLOC(jjm(:,2))
    !id = MAXLOC(iim(:,2))
    !jd = MAXLOC(jjm(:,2))

    !i = MAXLOC(iim(:,1));ii = MAXLOC(iim(:,2));j = MAXLOC(jjm(:,1)); jj = MAXLOC(jjm(:,2))

    !WRITE(0,*) 'index = ',i,ii,j,jj
    !WRITE(0,*) 'source = ',iim(53,1), iim(53,2),jjm(2,1),jjm(2,2)
    !WRITE(0,*) 'weight = ',wm11(53,2), wm12(53,2),wm21(53,2),wm22(53,2)


    !-------------------------------------------------------------------
    !
    ! set zpm
    !
    ! Note that Ensemble domain is one grid (analysis grid) larger than
    ! the analysis domain on each side when mfactor = 2.
    ! So no valid value from the analysis domain to set the ensemble
    ! domain.
    !
    !-------------------------------------------------------------------

    !ALLOCATE(zpm(nx_m,ny_m,nz_m), STAT = istatus)
    !DO k = 1, nz_m
    !  DO j = 2, ny_m-1
    !    jj = (j-2)*mfactor+2
    !    DO i = 2, nx_m-1
    !      ii = (i-2)*mfactor+2
    !      zpm(i,j,k) = zp(ii,jj,k)
    !    END DO
    !  END DO
    !END DO
    !
    !! South boundary and North boundary
    !DO k = 1, nz_m
    !  DO j = 2, ny_em-1
    !    jj = (j-2)*mfactor+2
    !    zpm(1,   j,k) = 2.0*zp(1, jj,k)-zp(2,   jj,k)
    !    zpm(nx_m,j,k) = 2.0*zp(nx,jj,k)-zp(nx-1,jj,k)
    !  END DO
    !END DO
    !
    !! South boundary and North boundary
    !DO k = 1, nz_m
    !  DO i = 2, nx_em-1
    !    ii = (i-2)*mfactor+2
    !    zpm(i,1,   k) = 2.0*zp(ii, 1,k)-zp(ii,2,   k)
    !    zpm(i,ny_m,k) = 2.0*zp(ii,ny,k)-zp(ii,ny-1,k)
    !  END DO
    !END DO
    !
    !! 4 corners
    !DO k = 1,nz_m
    !  zpm(1,      1,k) = zpm(2,        1,k)
    !  zpm(1,   ny_m,k) = zpm(2,     ny_m,k)
    !  zpm(nx_m,   1,k) = zpm(nx_m,     2,k)
    !  zpm(nx_m,ny_m,k) = zpm(nx_m,ny_m-1,k)
    !END DO
    !
    !CALL mpsendrecv2dew(zpm,nx_m,ny_m,nz_m,0,0,3,tem1)
    !CALL mpsendrecv2dns(zpm,nx_m,ny_m,nz_m,0,0,3,tem1)

    RETURN
  END SUBROUTINE ens2ctrl_init

  !#####################################################################

  SUBROUTINE ens_cost_ctr(nx,ny,nz,nscalarq,flag_legacyfilter,bdywidth, &
                      npass,ipass_filt,ref_opt, hydro_opt, cwpsw,       &
                      u_ctr,v_ctr,w_ctr,t_ctr,p_ctr,q_ctr,qscalar_ctr,  &
                      tem1,tem2,tem3,tem4,tem5,istatus)

    INTEGER, INTENT(IN)  :: nx,ny,nz,nscalarq
    INTEGER, INTENT(IN)  :: npass, ipass_filt, bdywidth
    INTEGER, INTENT(IN)  :: flag_legacyfilter
    INTEGER, INTENT(IN)  :: ref_opt, hydro_opt, cwpsw

    REAL,    INTENT(INOUT) :: u_ctr(nx,ny,nz),v_ctr(nx,ny,nz),w_ctr(nx,ny,nz)
    REAL,    INTENT(INOUT) :: t_ctr(nx,ny,nz),p_ctr(nx,ny,nz),q_ctr(nx,ny,nz)
    REAL,    INTENT(INOUT) :: qscalar_ctr(nx,ny,nz,nscalarq)

    REAL,    INTENT(INOUT) :: tem1(nx,ny,nz),tem2(nx,ny,nz),tem3(nx,ny,nz)
    REAL,    INTENT(INOUT) :: tem4(nx,ny,nz),tem5(nx,ny,nz)
    INTEGER, INTENT(OUT)   :: istatus

    INCLUDE 'mp.inc'

  !---------------------------------------------------------------------

    INTEGER :: i,j,k,l,nq
    REAL    :: uval, vval, wval, tval, pval, qval, qsval

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    CALL acct_interrupt(recrsub2_acct)

    IF( flag_legacyfilter ==1 ) THEN

      tem5=1.0
      DO l=1, nensvirtl
        tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
        CALL ctr_to_vbl(ipass_filt,hradius_3d_ens,radius_z_ens,nx,ny,nz,0,tem5,  &
                        gdscal_ens,en_ctr(:,:,:,l), tem1,tem2,tem3,tem4)
      END DO

    ELSE

      DO l=1, nensvirtl
        CALL forwardfilter2( nx,ny,nz,en_ctr(:,:,:,l),tem4 )
      END DO

    END IF

    CALL  acct_stop_inter

  !---------------------------------------------------------------------
  !
  ! Introduce weight array to handle boundary relaxation
  !
  !---------------------------------------------------------------------
    tem1(:,:,:) = beta2

    !IF (bdywidth > 0) THEN
    IF (.FALSE.) THEN

      IF (loc_x == 1) THEN             ! West
        DO k = 1,nz
          DO j = 1, ny
            DO i = 1, bdywidth
              tem1(i,j,k) = 0.0
            END DO
          END DO
        END DO
      END IF

      IF (loc_x == nproc_x) THEN       ! East
        DO k = 1,nz
          DO j = 1, ny
            DO i = nx, nx-bdywidth, -1
              tem1(i,j,k) = 0.0
            END DO
          END DO
        END DO
      END IF

      IF (loc_y == 1) THEN             ! South
        DO k = 1,nz
          DO j = 1, bdywidth
            DO i = 1, nx
              tem1(i,j,k) = 0.0
            END DO
          END DO
        END DO

      END IF

      IF (loc_y == nproc_y) THEN       ! North
        DO k = 1,nz
          DO j = ny, ny-bdywidth, -1
            DO i = 1, nx
              tem1(i,j,k) = 0.0
            END DO
          END DO
        END DO
      END IF

    END IF

  !---------------------------------------------------------------------
  !
  ! To get control variables from ensemble weight en_ctr
  !
  !---------------------------------------------------------------------

    IF (interpopt == 2) THEN  ! Ensemble and background are on the same grid
      !  sum1 = 0.

      DO l=1, nensvirtl
        DO k=1,nz     ! map from _m dimensions to original dimensions
          DO j=1,ny
            DO i=1,nx
              u_ctr(i,j,k)= u_ctr(i,j,k) +     ue(i,j,k,l) * en_ctr(i,j,k,l) *tem1(i,j,k)
              v_ctr(i,j,k)= v_ctr(i,j,k) +     ve(i,j,k,l) * en_ctr(i,j,k,l) *tem1(i,j,k)
              w_ctr(i,j,k)= w_ctr(i,j,k) +     we(i,j,k,l) * en_ctr(i,j,k,l) *tem1(i,j,k)
              t_ctr(i,j,k)= t_ctr(i,j,k) +    pte(i,j,k,l) * en_ctr(i,j,k,l) *tem1(i,j,k)
              p_ctr(i,j,k)= p_ctr(i,j,k) +  prese(i,j,k,l) * en_ctr(i,j,k,l) *tem1(i,j,k)
              q_ctr(i,j,k)= q_ctr(i,j,k) +    qve(i,j,k,l) * en_ctr(i,j,k,l) *tem1(i,j,k)
            END DO
          END DO
        END DO
      END DO

      IF( ref_opt==1 .OR. hydro_opt==1 .OR. cwpsw==1 ) THEN

        DO l=1, nensvirtl
          DO k=1,nz
            DO j=1,ny
              DO i=1,nx

                DO nq = 1, nscalarq
                  qscalar_ctr(i,j,k,nq) = qscalar_ctr(i,j,k,nq) +       &
                         qscalare(i,j,k,nq,l) * en_ctr(i,j,k,l) * tem1(i,j,k)
                END DO

              END DO
            END DO
          END DO
        END DO

      END IF

    ELSE   ! Do interpolation on-the-fly

      !  sum1 = 0.
      DO l=1, nensvirtl
        DO k=1,nz     ! map from _m dimensions to original dimensions
          DO j=1,ny
            DO i=1,nx

              uval =     ue(iim(i,1),jjm(j,1),k,l)*wm11(i,j)+           &
                         ue(iim(i,2),jjm(j,1),k,l)*wm21(i,j)+           &
                         ue(iim(i,1),jjm(j,2),k,l)*wm12(i,j)+           &
                         ue(iim(i,2),jjm(j,2),k,l)*wm22(i,j)
              vval =     ve(iim(i,1),jjm(j,1),k,l)*wm11(i,j)+           &
                         ve(iim(i,2),jjm(j,1),k,l)*wm21(i,j)+           &
                         ve(iim(i,1),jjm(j,2),k,l)*wm12(i,j)+           &
                         ve(iim(i,2),jjm(j,2),k,l)*wm22(i,j)
              wval =     we(iim(i,1),jjm(j,1),k,l)*wm11(i,j)+           &
                         we(iim(i,2),jjm(j,1),k,l)*wm21(i,j)+           &
                         we(iim(i,1),jjm(j,2),k,l)*wm12(i,j)+           &
                         we(iim(i,2),jjm(j,2),k,l)*wm22(i,j)
              tval =    pte(iim(i,1),jjm(j,1),k,l)*wm11(i,j)+           &
                        pte(iim(i,2),jjm(j,1),k,l)*wm21(i,j)+           &
                        pte(iim(i,1),jjm(j,2),k,l)*wm12(i,j)+           &
                        pte(iim(i,2),jjm(j,2),k,l)*wm22(i,j)
              pval =  prese(iim(i,1),jjm(j,1),k,l)*wm11(i,j)+           &
                      prese(iim(i,2),jjm(j,1),k,l)*wm21(i,j)+           &
                      prese(iim(i,1),jjm(j,2),k,l)*wm12(i,j)+           &
                      prese(iim(i,2),jjm(j,2),k,l)*wm22(i,j)
              qval =    qve(iim(i,1),jjm(j,1),k,l)*wm11(i,j)+           &
                        qve(iim(i,2),jjm(j,1),k,l)*wm21(i,j)+           &
                        qve(iim(i,1),jjm(j,2),k,l)*wm12(i,j)+           &
                        qve(iim(i,2),jjm(j,2),k,l)*wm22(i,j)

              u_ctr(i,j,k)= u_ctr(i,j,k) + uval* en_ctr(i,j,k,l) *tem1(i,j,k)
              v_ctr(i,j,k)= v_ctr(i,j,k) + vval* en_ctr(i,j,k,l) *tem1(i,j,k)
              w_ctr(i,j,k)= w_ctr(i,j,k) + wval* en_ctr(i,j,k,l) *tem1(i,j,k)
              t_ctr(i,j,k)= t_ctr(i,j,k) + tval* en_ctr(i,j,k,l) *tem1(i,j,k)
              p_ctr(i,j,k)= p_ctr(i,j,k) + pval* en_ctr(i,j,k,l) *tem1(i,j,k)
              q_ctr(i,j,k)= q_ctr(i,j,k) + qval* en_ctr(i,j,k,l) *tem1(i,j,k)
            END DO
          END DO
        END DO
      END DO

      IF( ref_opt==1 .OR. hydro_opt==1 .OR. cwpsw==1 ) THEN

        DO l=1, nensvirtl
          DO k=1,nz
            DO j=1,ny
              DO i=1,nx

                DO nq = 1, nscalarq
                  qsval = qscalare(iim(i,1),jjm(j,1),k,nq,l)*wm11(i,j)  &
                        + qscalare(iim(i,2),jjm(j,1),k,nq,l)*wm21(i,j)  &
                        + qscalare(iim(i,1),jjm(j,2),k,nq,l)*wm12(i,j)  &
                        + qscalare(iim(i,2),jjm(j,2),k,nq,l)*wm22(i,j)
                  qscalar_ctr(i,j,k,nq)=qscalar_ctr(i,j,k,nq)+qsval*en_ctr(i,j,k,l)*tem1(i,j,k)
                END DO

              END DO
            END DO
          END DO
        END DO

      END IF

    END IF

    IF (mp_opt > 0) THEN    ! Ensemble variables are assumed on MASS grid
                            ! but "*_ctr" variables should be on vector grid
                            ! we will use u_ctr(:,:,nx)/v_ctr(:,ny,:) for radial velocity analysis.
                            ! There is approximation here
      CALL mpsendrecv2dew(u_ctr, nx, ny, nz, 0, 0, 1, tem2)
      CALL mpsendrecv2dns(v_ctr, nx, ny, nz, 0, 0, 2, tem2)

    END IF

    RETURN
  END SUBROUTINE ens_cost_ctr

  !#####################################################################

  SUBROUTINE ens_gradc_gr(nx,ny,nz,nscalarq,flag_legacyfilter,bdywidth, &
                          npass,ipass_filt,ref_opt, hydro_opt, cwpsw,   &
                          u_grd,v_grd,w_grd,t_grd,p_grd,q_grd,qscalar_grd, &
                          tem1,tem2,tem3,tem4,tem5,istatus)

    !USE module_varpara, only : ibgn,iend, jbgn,jend, kbgn,kend
    !INCLUDE 'mp.inc'

    INTEGER, INTENT(IN)  :: nx,ny,nz,nscalarq
    INTEGER, INTENT(IN)  :: npass, ipass_filt,bdywidth
    INTEGER, INTENT(IN)  :: flag_legacyfilter
    INTEGER, INTENT(IN)  :: ref_opt, hydro_opt, cwpsw

    REAL,    INTENT(IN)  :: u_grd(nx,ny,nz),v_grd(nx,ny,nz),w_grd(nx,ny,nz)
    REAL,    INTENT(IN)  :: t_grd(nx,ny,nz),p_grd(nx,ny,nz),q_grd(nx,ny,nz)
    REAL,    INTENT(IN)  :: qscalar_grd(nx,ny,nz,nscalarq)

    REAL,    INTENT(INOUT) :: tem1(nx,ny,nz),tem2(nx,ny,nz),tem3(nx,ny,nz)
    REAL,    INTENT(INOUT) :: tem4(nx,ny,nz),tem5(nx,ny,nz)
    INTEGER, INTENT(OUT)   :: istatus

    INCLUDE 'mp.inc'

  !---------------------------------------------------------------------

    INTEGER :: i,j,k,l,nq
    REAL    :: uval, vval, wval, tval, pval, qval, qsval
    !DOUBLE PRECISION :: sum1

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

  !---------------------------------------------------------------------
  !
  ! Introduce weight array to handle boundary relaxation
  !
  !---------------------------------------------------------------------
    tem1(:,:,:) = beta2

    !IF (bdywidth > 0) THEN
    IF (.FALSE.) THEN

      IF (loc_x == 1) THEN             ! West
        DO k = 1,nz
          DO j = 1, ny
            DO i = 1, bdywidth
              tem1(i,j,k) = 0.0
            END DO
          END DO
        END DO
      END IF

      IF (loc_x == nproc_x) THEN       ! East
        DO k = 1,nz
          DO j = 1, ny
            DO i = nx, nx-bdywidth, -1
              tem1(i,j,k) = 0.0
            END DO
          END DO
        END DO
      END IF

      IF (loc_y == 1) THEN             ! South
        DO k = 1,nz
          DO j = 1, bdywidth
            DO i = 1, nx
              tem1(i,j,k) = 0.0
            END DO
          END DO
        END DO

      END IF

      IF (loc_y == nproc_y) THEN       ! North
        DO k = 1,nz
          DO j = ny, ny-bdywidth, -1
            DO i = 1, nx
              tem1(i,j,k) = 0.0
            END DO
          END DO
        END DO
      END IF

    END IF

  !---------------------------------------------------------------------
  !
  ! To get gradient variable
  !
  !---------------------------------------------------------------------

    IF (interpopt == 2) THEN

      DO l=1, nensvirtl
        DO k=1,nz
          DO j=1,ny
            DO i=1,nx

              gr_en(i,j,k,l)=gr_en(i,j,k,l)+     ue(i,j,k,l)*u_grd(i,j,k)*tem1(i,j,k)
              gr_en(i,j,k,l)=gr_en(i,j,k,l)+     ve(i,j,k,l)*v_grd(i,j,k)*tem1(i,j,k)
              gr_en(i,j,k,l)=gr_en(i,j,k,l)+     we(i,j,k,l)*w_grd(i,j,k)*tem1(i,j,k)
              gr_en(i,j,k,l)=gr_en(i,j,k,l)+  prese(i,j,k,l)*p_grd(i,j,k)*tem1(i,j,k)
              gr_en(i,j,k,l)=gr_en(i,j,k,l)+    pte(i,j,k,l)*t_grd(i,j,k)*tem1(i,j,k)
              gr_en(i,j,k,l)=gr_en(i,j,k,l)+    qve(i,j,k,l)*q_grd(i,j,k)*tem1(i,j,k)

            END DO
          END DO
        END DO
      END DO

      IF( ref_opt==1 .OR. hydro_opt==1 .OR. cwpsw==1 ) THEN

        DO l=1, nensvirtl
          DO k=1,nz
            DO j=1,ny
              DO i=1,nx

                DO nq = 1, nscalarq
                  gr_en(i,j,k,l)=gr_en(i,j,k,l) + qscalare(i,j,k,nq,l)*qscalar_grd(i,j,k,nq)*tem1(i,j,k)
                END DO

              END DO
            END DO
          END DO
        END DO

      END IF

    ELSE

      DO l=1, nensvirtl
        DO k=1,nz
          DO j=1,ny
            DO i=1,nx
              uval =     ue(iim(i,1),jjm(j,1),k,l)*wm11(i,j)+           &
                         ue(iim(i,2),jjm(j,1),k,l)*wm21(i,j)+           &
                         ue(iim(i,1),jjm(j,2),k,l)*wm12(i,j)+           &
                         ue(iim(i,2),jjm(j,2),k,l)*wm22(i,j)
              vval =     ve(iim(i,1),jjm(j,1),k,l)*wm11(i,j)+           &
                         ve(iim(i,2),jjm(j,1),k,l)*wm21(i,j)+           &
                         ve(iim(i,1),jjm(j,2),k,l)*wm12(i,j)+           &
                         ve(iim(i,2),jjm(j,2),k,l)*wm22(i,j)
              wval =     we(iim(i,1),jjm(j,1),k,l)*wm11(i,j)+           &
                         we(iim(i,2),jjm(j,1),k,l)*wm21(i,j)+           &
                         we(iim(i,1),jjm(j,2),k,l)*wm12(i,j)+           &
                         we(iim(i,2),jjm(j,2),k,l)*wm22(i,j)
              tval =    pte(iim(i,1),jjm(j,1),k,l)*wm11(i,j)+           &
                        pte(iim(i,2),jjm(j,1),k,l)*wm21(i,j)+           &
                        pte(iim(i,1),jjm(j,2),k,l)*wm12(i,j)+           &
                        pte(iim(i,2),jjm(j,2),k,l)*wm22(i,j)
              pval =  prese(iim(i,1),jjm(j,1),k,l)*wm11(i,j)+           &
                      prese(iim(i,2),jjm(j,1),k,l)*wm21(i,j)+           &
                      prese(iim(i,1),jjm(j,2),k,l)*wm12(i,j)+           &
                      prese(iim(i,2),jjm(j,2),k,l)*wm22(i,j)
              qval =    qve(iim(i,1),jjm(j,1),k,l)*wm11(i,j)+           &
                        qve(iim(i,2),jjm(j,1),k,l)*wm21(i,j)+           &
                        qve(iim(i,1),jjm(j,2),k,l)*wm12(i,j)+           &
                        qve(iim(i,2),jjm(j,2),k,l)*wm22(i,j)

              gr_en(i,j,k,l)=gr_en(i,j,k,l)+ uval*u_grd(i,j,k)*tem1(i,j,k)
              gr_en(i,j,k,l)=gr_en(i,j,k,l)+ vval*v_grd(i,j,k)*tem1(i,j,k)
              gr_en(i,j,k,l)=gr_en(i,j,k,l)+ wval*w_grd(i,j,k)*tem1(i,j,k)
              gr_en(i,j,k,l)=gr_en(i,j,k,l)+ pval*p_grd(i,j,k)*tem1(i,j,k)
              gr_en(i,j,k,l)=gr_en(i,j,k,l)+ tval*t_grd(i,j,k)*tem1(i,j,k)
              gr_en(i,j,k,l)=gr_en(i,j,k,l)+ qval*q_grd(i,j,k)*tem1(i,j,k)
            END DO
          END DO
        END DO
      END DO

      IF( ref_opt == 1 .OR. hydro_opt == 1 .OR. cwpsw == 1 ) THEN

        DO l=1, nensvirtl
          DO k=1,nz
            DO j=1,ny
              DO i=1,nx

                DO nq = 1, nscalarq
                  qsval = qscalare(iim(i,1),jjm(j,1),k,nq,l)*wm11(i,j)  &
                        + qscalare(iim(i,2),jjm(j,1),k,nq,l)*wm21(i,j)  &
                        + qscalare(iim(i,1),jjm(j,2),k,nq,l)*wm12(i,j)  &
                        + qscalare(iim(i,2),jjm(j,2),k,nq,l)*wm22(i,j)

                  gr_en(i,j,k,l)=gr_en(i,j,k,l)+qsval*qscalar_grd(i,j,k,nq)*tem1(i,j,k)
                END DO

              END DO
            END DO
          END DO
        END DO

      END IF

    END IF

!    sum1 = 0.0D0
!    DO l=1, nensvirtl
!      DO k = kbgn, kend
!        DO j = jbgn, jend
!          DO i = ibgn, iend
!            sum1 =sum1 +  gr_en(i,j,k,l)**2
!          END DO
!        END DO
!      END DO
!    END DO
!
!    DO l = 1, nprocs
!      IF (myproc == l) WRITE(0,*) 's2m_en=',myproc,sum1
!      CALL mpbarrier
!    END DO
!    CALL arpsstop('',0)

    CALL acct_interrupt(recrsub3_acct)
    IF( flag_legacyfilter ==1 ) THEN
      tem5=1.0
      DO l=1, nensvirtl
        tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
        CALL vbl_to_ctr(ipass_filt,hradius_3d_ens,radius_z_ens,nx,ny,nz,0,tem5,&
                        gdscal_ens, gr_en(:,:,:,l), tem1,tem2,tem3,tem4)
        !sum1 = 0.0
        !DO k = 1, nz
        !  DO j = 1, ny
        !    DO i = 1, nx
        !      sum1 =sum1 +  gr_en(i,j,k,l)**2
        !    END DO
        !  END DO
        !END DO
        !print*,'s3m_en=',sqrt( sum1/nx/ny/nz )

      END DO
    ELSE
      DO l=1, nensvirtl
        CALL adjointfilter2(nx,ny,nz,gr_en(:,:,:,l),tem4)
      END DO
    END IF
    CALL  acct_stop_inter

    RETURN
  END SUBROUTINE ens_gradc_gr

  !
  !##################################################################
  !##################################################################
  !######                                                      ######
  !######                SUBROUTINE WRFREAD_PROCESS            ######
  !######                                                      ######
  !######                     Developed by                     ######
  !######       National Severe Storm Laboratory, NOAA         ######
  !######                                                      ######
  !##################################################################
  !##################################################################
  !
  SUBROUTINE ens_wrfread(nx_m,ny_m,nz_m,iens,ntimesample,stimesample,nmem,istatus)

  !
  !-----------------------------------------------------------------------
  !
  !  PURPOSE:
  !
  !  Coordinate the reading of WRF history data in one file and then
  !  broadcast and process in the memory.
  !
  !  It combines the capability of dtaread4wrf & dtareadPH for speedy
  !  processing when node has plenty of memory.
  !
  !  It can only handles One WRF file in no-mpi mode  or mpi mode.
  !
  !-----------------------------------------------------------------------
  !
  !  AUTHOR: Yunheng Wang
  !    5/05/2017.
  !
  !  MODIFICATION HISTORY:
  !
  !-----------------------------------------------------------------------

    USE model_precision

    IMPLICIT NONE

    INCLUDE 'mp.inc'
    INCLUDE 'globcst.inc'

  !-----------------------------------------------------------------------

    !INTEGER, INTENT (IN) :: nx,   ny,   nz
    INTEGER, INTENT (IN) :: nx_m, ny_m, nz_m
    INTEGER, INTENT (IN) :: iens, nmem
    INTEGER, INTENT (IN) :: ntimesample, stimesample

    INTEGER, INTENT(OUT) :: istatus

  !-----------------------------------------------------------------------

    REAL, PARAMETER :: gravity = 9.81

  !-----------------------------------------------------------------------
  !
  ! Local working variables
  !
  !-----------------------------------------------------------------------

    INTEGER :: numsvar, num1dvar, num2dvar
    CHARACTER(LEN=40) :: namessvar(1), names1dvar(2),names2dvar(2)
    INTEGER           :: size1dvar(2), maxsize1d

    INTEGER :: num3dvar, numqbase
    CHARACTER(LEN=40) :: names3dvar(20)
    CHARACTER(LEN=1)  :: stag3dvar(20)

    INTEGER :: imem,ldest

    INTEGER :: PTR_U, PTR_V, PTR_W, PTR_T, PTR_P, PTR_PB, PTR_PH, PTR_PHB
    INTEGER :: PTR_QV

    REAL(P)  :: p_top
    REAL(P),  ALLOCATABLE :: qv_wrf(:,:,:), pt_wrf(:,:,:), ph_wrf(:,:,:)
    REAL(P),  ALLOCATABLE :: mub_wrf(:,:), mu_wrf(:,:)
    REAL(P),  ALLOCATABLE :: znu_wrf(:), rdnw_wrf(:)
    REAL(P),  ALLOCATABLE :: rb_wrf(:,:,:)

    REAL(P),  ALLOCATABLE :: vars_wrf(:,:), var1d_wrf(:,:,:), var2d_wrf(:,:,:,:)
    REAL(P),  ALLOCATABLE :: var3d_wrf(:,:,:,:,:)

    REAL(P),  ALLOCATABLE :: rhotmp(:,:,:)

  !-----------------------------------------------------------------------
  !
  ! Misc. Local variables
  !
  !-----------------------------------------------------------------------

    CHARACTER (LEN=19)  :: timestr

    INTEGER :: nx_wrf, ny_wrf, nz_wrf
    INTEGER :: nxlg_wrf, nylg_wrf

    INTEGER :: i,j,k,nq,l
    INTEGER :: kk,ii1,jj1,ii2,jj2

    REAL :: ptval, ppval, pbval, phval,qvval

    INTEGER :: dim0dvar, dim1dvar, dim2dvar, dim3dvar

    INTEGER :: iyr,imon,idy,ihr,imin,isec
    INTEGER :: imod, iabs0, iabs

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (nproc_x_in /= 1 .OR. nproc_y_in /= 1) THEN
      WRITE(*,'(1x,a)')       'ERROR: unsupported process configuration.'
      WRITE(*,'(7x,2(a,I4))') ' nproc_x_in = ',nproc_x_in, ', nproc_x = ', nproc_x
      WRITE(*,'(7x,2(a,I4))') ' nproc_y_in = ',nproc_y_in, ', nproc_y = ', nproc_y
      istatus = -1
      !RETURN
      CALL arpsstop('ERROR mpi parameters',1)
    END IF

  !-----------------------------------------------------------------------
  !
  ! Dimension sizes
  !
  !-----------------------------------------------------------------------
    nx_wrf = nx_m-2       ! staggered dimensions
    ny_wrf = ny_m-2
    nz_wrf = nz_m-2

    nxlg_wrf = (nx_wrf-1)*nproc_x + 1     ! WRF domain indices
    nylg_wrf = (ny_wrf-1)*nproc_y + 1

  !-----------------------------------------------------------------------
  !
  ! Variable names
  !
  !-----------------------------------------------------------------------

    numsvar = 1
    namessvar(1) = 'P_TOP'

    num1dvar = 2
    names1dvar(1) = 'RDNW'; size1dvar(1) = nz_wrf-1
    names1dvar(2) = 'ZNU';  size1dvar(2) = nz_wrf-1

    num2dvar = 2
    names2dvar(1) = 'MUB'
    names2dvar(2) = 'MU'

    IF (.NOT. use_rhobare) THEN   ! Do not read scalar/1d/2d variables
      numsvar  = 0
      num1dvar = 0
      num2dvar = 0
    END IF

    names3dvar(1) = 'U';      stag3dvar(1) = 'X'; PTR_U   = 1
    names3dvar(2) = 'V';      stag3dvar(2) = 'Y'; PTR_V   = 2
    names3dvar(3) = 'W';      stag3dvar(3) = 'Z'; PTR_W   = 3
    names3dvar(4) = 'T';      stag3dvar(4) = '0'; PTR_T   = 4
    names3dvar(5) = 'P';      stag3dvar(5) = '0'; PTR_P   = 5
    names3dvar(6) = 'PB';     stag3dvar(6) = '0'; PTR_PB  = 6
    names3dvar(7) = 'PH';     stag3dvar(7) = 'V'; PTR_PH  = 7
    names3dvar(8) = 'PHB';    stag3dvar(8) = 'V'; PTR_PHB = 8
    names3dvar(9) = 'QVAPOR'; stag3dvar(9) = '0'; PTR_QV  = 9
    numqbase = 9
    num3dvar = numqbase+nscalarq

    DO nq = 1, nscalarq

      SELECT CASE ( TRIM(qnames(nq)) )
      CASE ('qc')
        names3dvar(numqbase+nq) = 'QCLOUD'
      CASE ('qr')
        names3dvar(numqbase+nq) = 'QRAIN'
      CASE ('qi')
        names3dvar(numqbase+nq) ='QICE'
      CASE ('qs')
        names3dvar(numqbase+nq) = 'QSNOW'
      CASE ('qg')
        names3dvar(numqbase+nq) = 'QGRAUP'
      CASE ('qh')
        names3dvar(numqbase+nq) ='QHAIL'
      CASE DEFAULT
        WRITE(*,'(1x,3a)') 'ERROR: support of ',TRIM(qnames(nq)),' is still not added.'
        CONTINUE
      END SELECT
      stag3dvar(numqbase+nq) = '0'

    END DO

    dim0dvar = MAX(numsvar,1)   ! To avoid 0 dimension arrays
    dim1dvar = MAX(num1dvar,1)
    dim2dvar = MAX(num2dvar,1)
    dim3dvar = num3dvar

  !-----------------------------------------------------------------------
  !
  ! Allocate data arrays and read
  !
  !-----------------------------------------------------------------------

    maxsize1d = MAXVAL(size1dvar)
    ALLOCATE(var3d_wrf(0:nx_wrf+1,0:ny_wrf+1,0:nz_wrf+1,dim3dvar,nmem), STAT = istatus)
    ALLOCATE(var2d_wrf(0:nx_wrf+1,0:ny_wrf+1,           dim2dvar,nmem), STAT = istatus)
    ALLOCATE(var1d_wrf(maxsize1d,                       dim1dvar,nmem), STAT = istatus)
    ALLOCATE(vars_wrf (                                 dim0dvar,nmem),  STAT = istatus)

    var3d_wrf = 0.0
    var2d_wrf = 0.0
    var1d_wrf = 0.0
    vars_wrf  = 0.0

    imod = mod(myproc+1,ntimesample)
    IF (imod == 0) imod = ntimesample
    imem = (iens-1)*ntimesample + imod - 1    ! this member (and its time samples) to be skipped

    !---- suppose ntimesample = 3
    !member  Process    imod   imod-(ntimesample/2+1)) relative time
    !------  -------    ----   -------------------------------------
    !          0          1     -1
    ! 1        1          2      0
    !          2          3      1
    !          3          1     -1
    ! 2        4          2      0
    !          5          3      1
    !          6          1     -1
    ! 3        7          2      0
    !          8          3      1
    !
    ! if iens = 0, then imem are negative, so skip nothing
    ! if iens > 0, then the correspoing process(es) will be skipped
    !

    !print *, 'imem = ',myproc, imod, ntimesample, imem, stimesample

    CALL ctim2abss(year, month, day, hour, minute, second, iabs0)
    iabs = iabs0 + (imod - (ntimesample/2+1))*stimesample
    CALL abss2ctim( iabs, iyr, imon, idy, ihr, imin, isec )

    IF (enstimeopt == 1) THEN
      WRITE(timestr,'(I4.4,5(a,I2.2))')                                 &
                 iyr,'-',imon,'-',idy,'_',ihr,':',imin,':',isec
    ELSE IF (enstimeopt == 2) THEN
      WRITE(timestr,'(3a)') ensintdate,'_',ensinttime
    END IF

    !print *, myproc, timestr


    CALL readwrfens(ensdirname,ensfileopt,ensreadready,nmem,timestr,imem,&
                    nx_wrf,ny_wrf,nz_wrf,nxlg_wrf,nylg_wrf,             &
                    dim0dvar, dim1dvar, dim2dvar, dim3dvar,             &
                    numsvar,namessvar,vars_wrf,                         &
                    num1dvar,names1dvar,size1dvar,maxsize1d,var1d_wrf,  &
                    num2dvar,names2dvar,var2d_wrf,                      &
                    num3dvar,names3dvar,stag3dvar,var3d_wrf,            &
                    istatus)

  !-----------------------------------------------------------------------
  !
  !  Convert to output arrays
  !
  !-----------------------------------------------------------------------

    ldest = 1
    IF (imem == 1) ldest = 2

    IF (interpopt == 2) THEN    ! interpolate to nx,ny,nz, nx_en == nx

      DO l = 1, nmem

        IF (l == imem) CYCLE

        DO k = 1,nz_en
          kk = k-1
          DO j = 1, ny_en
            jj1 = jjm(j,1)-1; jj2 = jjm(j,2)-1
            DO i = 1, nx_en
              ii1 = iim(i,1)-1; ii2 = iim(i,2)-1

              ue(i,j,k,l) = var3d_wrf(ii1,jj1,kk,PTR_U,l)*wm11(i,j)+    &
                            var3d_wrf(ii2,jj1,kk,PTR_U,l)*wm21(i,j)+    &
                            var3d_wrf(ii1,jj2,kk,PTR_U,l)*wm12(i,j)+    &
                            var3d_wrf(ii2,jj2,kk,PTR_U,l)*wm22(i,j)
              ve(i,j,k,l) = var3d_wrf(ii1,jj1,kk,PTR_V,l)*wm11(i,j)+    &
                            var3d_wrf(ii2,jj1,kk,PTR_V,l)*wm21(i,j)+    &
                            var3d_wrf(ii1,jj2,kk,PTR_V,l)*wm12(i,j)+    &
                            var3d_wrf(ii2,jj2,kk,PTR_V,l)*wm22(i,j)
              we(i,j,k,l) = var3d_wrf(ii1,jj1,kk,PTR_W,l)*wm11(i,j)+    &
                            var3d_wrf(ii2,jj1,kk,PTR_W,l)*wm21(i,j)+    &
                            var3d_wrf(ii1,jj2,kk,PTR_W,l)*wm12(i,j)+    &
                            var3d_wrf(ii2,jj2,kk,PTR_W,l)*wm22(i,j)

              ptval = var3d_wrf(ii1,jj1,kk,PTR_T,l)*wm11(i,j)+          &
                      var3d_wrf(ii2,jj1,kk,PTR_T,l)*wm21(i,j)+          &
                      var3d_wrf(ii1,jj2,kk,PTR_T,l)*wm12(i,j)+          &
                      var3d_wrf(ii2,jj2,kk,PTR_T,l)*wm22(i,j)
              pte(i,j,k,l) = ptval + 300.0

              ppval = var3d_wrf(ii1,jj1,kk,PTR_P,l)*wm11(i,j)+          &
                      var3d_wrf(ii2,jj1,kk,PTR_P,l)*wm21(i,j)+          &
                      var3d_wrf(ii1,jj2,kk,PTR_P,l)*wm12(i,j)+          &
                      var3d_wrf(ii2,jj2,kk,PTR_P,l)*wm22(i,j)
              pbval = var3d_wrf(ii1,jj1,kk,PTR_PB,l)*wm11(i,j)+         &
                      var3d_wrf(ii2,jj1,kk,PTR_PB,l)*wm21(i,j)+         &
                      var3d_wrf(ii1,jj2,kk,PTR_PB,l)*wm12(i,j)+         &
                      var3d_wrf(ii2,jj2,kk,PTR_PB,l)*wm22(i,j)
              prese(i,j,k,l) = ppval + pbval

              qvval = var3d_wrf(ii1,jj1,kk,PTR_QV,l)*wm11(i,j)+         &
                      var3d_wrf(ii2,jj1,kk,PTR_QV,l)*wm21(i,j)+         &
                      var3d_wrf(ii1,jj2,kk,PTR_QV,l)*wm12(i,j)+         &
                      var3d_wrf(ii2,jj2,kk,PTR_QV,l)*wm22(i,j)
              qve(i,j,k,l) =  qvval/(1.0+qvval)

            END DO
          END DO
        END DO
      END DO

      WHERE(qve < 0.0) qve = 0.0
      WHERE(qve > 1.0) qve = 1.0

      !
      ! hydrometeors
      !

      DO l = 1, nmem

        IF (l == imem) CYCLE

        DO nq = 1, nscalarq
          DO k = 1,nz_en
            kk = k-1
            DO j = 1, ny_en
              jj1 = jjm(j,1)-1; jj2 = jjm(j,2)-1
              DO i = 1, nx_en
                ii1 = iim(i,1)-1; ii2 = iim(i,2)-1

                qscalare(i,j,k,nq,l) = var3d_wrf(ii1,jj1,kk,numqbase+nq,l)*wm11(i,j)+ &
                                       var3d_wrf(ii2,jj1,kk,numqbase+nq,l)*wm21(i,j)+ &
                                       var3d_wrf(ii1,jj2,kk,numqbase+nq,l)*wm12(i,j)+ &
                                       var3d_wrf(ii2,jj2,kk,numqbase+nq,l)*wm22(i,j)

              END DO
            END DO
          END DO
        END DO
      END DO

      !WHERE(qscalare < 0.0) qscalare = 0.0
      !WHERE(qscalare > 1.0) qscalare = 1.0

      !
      ! Vertical coordinates variables
      !

      DO k = 1,nz_en
        kk = k-1
        DO j = 1, ny_en
          jj1 = jjm(j,1)-1; jj2 = jjm(j,2)-1
          DO i = 1, nx_en
            ii1 = iim(i,1)-1; ii2 = iim(i,2)-1

            phval = var3d_wrf(ii1,jj1,kk,PTR_PH, ldest)*wm11(i,j)+      &
                    var3d_wrf(ii2,jj1,kk,PTR_PH, ldest)*wm21(i,j)+      &
                    var3d_wrf(ii1,jj2,kk,PTR_PH, ldest)*wm12(i,j)+      &
                    var3d_wrf(ii2,jj2,kk,PTR_PH, ldest)*wm22(i,j)
            pbval = var3d_wrf(ii1,jj1,kk,PTR_PHB,ldest)*wm11(i,j)+      &
                    var3d_wrf(ii2,jj1,kk,PTR_PHB,ldest)*wm21(i,j)+      &
                    var3d_wrf(ii1,jj2,kk,PTR_PHB,ldest)*wm12(i,j)+      &
                    var3d_wrf(ii2,jj2,kk,PTR_PHB,ldest)*wm22(i,j)

            zpm(i,j,k) = (phval+pbval)/gravity

          END DO
        END DO
      END DO

    ELSE             ! nx_m == nx_en

      DO l = 1, nmem

        IF (l == imem) CYCLE

        DO k = 1,nz_en
          DO j = 1, ny_en
            DO i = 1, nx_en
              ue(i,j,k,l)     = var3d_wrf(i-1,j-1,k-1,PTR_U,l)
              ve(i,j,k,l)     = var3d_wrf(i-1,j-1,k-1,PTR_V,l)
              we(i,j,k,l)     = var3d_wrf(i-1,j-1,k-1,PTR_W,l)
              pte(i,j,k,l)    = var3d_wrf(i-1,j-1,k-1,PTR_T,l)+ 300.0
              prese(i,j,k,l)  = var3d_wrf(i-1,j-1,k-1,PTR_P,l)+ var3d_wrf(i-1,j-1,k-1,PTR_PB,l)
              qve(i,j,k,l)    = var3d_wrf(i-1,j-1,k-1,PTR_QV,l)/(1.0+var3d_wrf(i-1,j-1,k-1,PTR_QV,l))
            END DO
          END DO
        END DO
      END DO

      WHERE(qve < 0.0) qve = 0.0
      WHERE(qve > 1.0) qve = 1.0

      !
      ! hydrometeors
      !

      DO l = 1, nmem

        IF (l == imem) CYCLE

        DO nq = 1, nscalarq
          DO k = 1,nz_en
            DO j = 1, ny_en
              DO i = 1, nx_en
                qscalare(i,j,k,nq,l) = var3d_wrf(i-1,j-1,k-1,numqbase+nq,l)
              END DO
            END DO
          END DO
        END DO
      END DO

      !WHERE(qscalare < 0.0) qscalare = 0.0
      !WHERE(qscalare > 1.0) qscalare = 1.0

      !
      ! Vertical coordinates variables
      !
      DO k = 1,nz_en
        DO j = 1, ny_en
          DO i = 1, nx_en
            zpm(i,j,k) = (var3d_wrf(i-1,j-1,k-1,PTR_PH,ldest)+var3d_wrf(i-1,j-1,k-1,PTR_PHB,ldest))/gravity
          END DO
        END DO
      END DO

    END IF

  !-----------------------------------------------------------------------
  !
  ! Calcultate rhobar
  !
  !-----------------------------------------------------------------------

    IF (use_rhobare) THEN

      ALLOCATE(rhotmp(nx_m,ny_m,nz_m), STAT = istatus)

      ALLOCATE(qv_wrf(nx_wrf,ny_wrf,nz_wrf),     STAT = istatus)
      ALLOCATE(pt_wrf(nx_wrf,ny_wrf,nz_wrf),     STAT = istatus)
      ALLOCATE(ph_wrf(nx_wrf,ny_wrf,nz_wrf),     STAT = istatus)
      ALLOCATE(rb_wrf(nx_wrf,ny_wrf,nz_wrf),     STAT = istatus)
      qv_wrf  = 0.0
      pt_wrf  = 0.0
      ph_wrf  = 0.0
      rb_wrf  = 0.0

      ALLOCATE(mub_wrf (nx_wrf,ny_wrf),          STAT = istatus)
      ALLOCATE(mu_wrf  (nx_wrf,ny_wrf),          STAT = istatus)
      ALLOCATE(znu_wrf (nz_wrf-1),               STAT = istatus)
      ALLOCATE(rdnw_wrf(nz_wrf-1),               STAT = istatus)
      mub_wrf  = 0.0
      mu_wrf   = 0.0
      znu_wrf  = 0.0
      rdnw_wrf = 0.0

      DO l = 1, nmem

        IF (l == imem) CYCLE

        DO j = 1,ny_wrf
          DO i = 1, nx_wrf
            mub_wrf(i,j) = var2d_wrf(i,j,1,l)
          END DO
        END DO

        DO j = 1,ny_wrf
          DO i = 1, nx_wrf
            mu_wrf(i,j) = var2d_wrf(i,j,2,l)
          END DO
        END DO

        DO k =1,nz_wrf-1
          rdnw_wrf(k) = var1d_wrf(k,1,l)
        END DO

        DO k = 1, nz_wrf-1
          znu_wrf(k) = var1d_wrf(k,2,l)
        END DO

        p_top = vars_wrf(1,l)

        DO k = 1,nz_wrf
          DO j = 1, ny_wrf
            DO i = 1, nx_wrf
              qv_wrf(i,j,k) = var3d_wrf(i,j,k,PTR_QV,l)
            END DO
          END DO
        END DO

        DO k = 1,nz_wrf
          DO j = 1, ny_wrf
            DO i = 1, nx_wrf
              ph_wrf(i,j,k) = var3d_wrf(i,j,k,PTR_PH,l)
            END DO
          END DO
        END DO

        DO k = 1,nz_wrf
          DO j = 1, ny_wrf
            DO i = 1, nx_wrf
              pt_wrf(i,j,k) = var3d_wrf(i,j,k,PTR_T,l)
            END DO
          END DO
        END DO

        CALL cal_rho_p(nx_wrf,ny_wrf,nz_wrf,mub_wrf,mu_wrf,rdnw_wrf,        &
                       p_top,znu_wrf,qv_wrf,ph_wrf,pt_wrf,                  &
                       rb_wrf,istatus)

        CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',rb_wrf,                &
                            nx_m,ny_m,nz_m,rhotmp,qv_wrf,istatus) !qv_wrf used as a working array

        IF (interpopt == 2) THEN

            DO k = 1,nz_en
              DO j = 1, ny_en
                jj1 = jjm(j,1); jj2 = jjm(j,2)
                DO i = 1, nx_en
                  ii1 = iim(i,1); ii2 = iim(i,2)

                  rhobare(i,j,k,l) = rhotmp(ii1,jj1,k)*wm11(i,j)+       &
                                     rhotmp(ii2,jj1,k)*wm21(i,j)+       &
                                     rhotmp(ii1,jj2,k)*wm12(i,j)+       &
                                     rhotmp(ii2,jj2,k)*wm22(i,j)

                END DO
              END DO
            END DO

        ELSE

          rhobare(:,:,:,l) = rhotmp(:,:,:)

        END IF

      END DO

      DEALLOCATE ( qv_wrf, pt_wrf , ph_wrf, rb_wrf )
      DEALLOCATE ( mub_wrf, mu_wrf, znu_wrf, rdnw_wrf)
      DEALLOCATE ( rhotmp )

    ELSE
      rhobare(:,:,:,:) = 0.0
    END IF

  !-----------------------------------------------------------------------
  ! Finishing
  !-----------------------------------------------------------------------

    DEALLOCATE ( vars_wrf, var1d_wrf, var2d_wrf, var3d_wrf)

    RETURN
  END SUBROUTINE ens_wrfread

  !#####################################################################

  SUBROUTINE ens_perturbNorm(nx,ny,nz,nscalar,myproc,istatus)
  !
  !-----------------------------------------------------------------------
  !
  !  PURPOSE:
  !
  !  Calculate ensemble perturbation
  !
  !-----------------------------------------------------------------------

    USE module_modelArray

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nx,ny,nz
    INTEGER, INTENT(IN)  :: nscalar
    INTEGER, INTENT(IN)  :: myproc
    INTEGER, INTENT(OUT) :: istatus

    REAL,   ALLOCATABLE  :: mean_tm(:,:,:)
  !
  !
  ! input:
  !
    !INTEGER :: nx,ny,nz,nensvirtl,nscalar
    !REAL :: uen(nx,ny,nz,nensvirtl)
    !REAL :: ven(nx,ny,nz,nensvirtl)
    !REAL :: wen(nx,ny,nz,nensvirtl)
    !REAL :: ptprten(nx,ny,nz,nensvirtl)
    !REAL :: pprten(nx,ny,nz,nensvirtl)
    !REAL :: qven(nx,ny,nz,nensvirtl)
    !REAL :: qscalaren(nx,ny,nz,nscalar,nensvirtl)
    REAL :: cfun1
  !
  ! test
  !
    !REAL :: sumu, sumv, sumw, sump, sumt, sumqv, sumqc, sumqr, sumqi, sumqs, sumqh
  !
  ! local
  !
  INTEGER :: i, j, k, l, nq

  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    !gain_fac=0.7
    if (myproc==0) print*,'gain_fac=',gain_fac,nx_en,ny_en,nz_en,interpopt
    if (myproc==0) print*, 'u, p,pt=',u(10,10,10), pres(10,10,10),pt(10,10,10)

    IF (interpopt == 2) THEN   ! nx = nx_en

      DO k=1,nz_en
        DO j=1,ny_en
          DO i=1,nx_en

            cfun1 = 0.
            DO l=1,nensvirtl
              cfun1 = cfun1  + ue(i,j,k,l)
            END DO
            cfun1 = cfun1/nensvirtl
            u(i,j,k) = (1-gain_fac)*u(i,j,k)+gain_fac*cfun1

            DO l=1,nensvirtl
              ue(i,j,k,l)=( ue(i,j,k,l)-cfun1 )/sqrt(nensvirtl-1.0)
            END DO

            cfun1 = 0.
            DO l=1,nensvirtl
              cfun1 = cfun1  + ve(i,j,k,l)
            END DO
            cfun1 = cfun1/nensvirtl
            v(i,j,k) = (1-gain_fac)*v(i,j,k)+gain_fac*cfun1

            DO l=1,nensvirtl
              ve(i,j,k,l)=( ve(i,j,k,l)-cfun1 )/sqrt(nensvirtl-1.0)
            END DO

            cfun1 = 0.
            DO l=1,nensvirtl
              cfun1 = cfun1  + we(i,j,k,l)
            END DO
            cfun1 = cfun1/nensvirtl
            w(i,j,k) = (1-gain_fac)*w(i,j,k)+gain_fac*cfun1

            DO l=1,nensvirtl
              we(i,j,k,l)=( we(i,j,k,l)-cfun1 )/sqrt(nensvirtl-1.0)
            END DO

            cfun1 = 0.
            DO l=1,nensvirtl
              cfun1 = cfun1  + prese(i,j,k,l)
            END DO
            cfun1 = cfun1/nensvirtl
            pres(i,j,k) = (1-gain_fac)*pres(i,j,k)+gain_fac*cfun1

            DO l=1,nensvirtl
              prese(i,j,k,l)=( prese(i,j,k,l)-cfun1 )/sqrt(nensvirtl-1.0)
            END DO

            cfun1 = 0.
            DO l=1,nensvirtl
              cfun1 = cfun1  + pte(i,j,k,l)
            END DO
            cfun1 = cfun1/nensvirtl
            pt(i,j,k) = (1-gain_fac)*pt(i,j,k)+gain_fac*cfun1

            DO l=1,nensvirtl
              pte(i,j,k,l)=( pte(i,j,k,l)-cfun1 )/sqrt(nensvirtl-1.0)
            END DO

            cfun1 = 0.
            DO l=1,nensvirtl
              cfun1 = cfun1  + qve(i,j,k,l)
            END DO
            cfun1 = cfun1/nensvirtl
            qv(i,j,k) = (1-gain_fac)*qv(i,j,k)+gain_fac*cfun1

            DO l=1,nensvirtl
              qve(i,j,k,l)=( qve(i,j,k,l)-cfun1 )/sqrt(nensvirtl-1.0)
            END DO

          END DO
        END DO
      END DO

      DO k=1,nz_en
        DO j=1,ny_en
          DO i=1,nx_en
            DO nq=1, nscalar

              cfun1 = 0.
              DO l=1,nensvirtl
                cfun1 = cfun1  + qscalare(i,j,k,nq,l)
              END DO
              cfun1 = cfun1/nensvirtl
              qscalar(i,j,k,nq) = (1-gain_fac)*qscalar(i,j,k,nq)+gain_fac*cfun1

              DO l=1,nensvirtl
                qscalare(i,j,k,nq,l)=( qscalare(i,j,k,nq,l)-cfun1 )/sqrt(nensvirtl-1.0)
              END DO

            END DO
          END DO
        END DO
      END DO


    ELSE      ! interpopt == 1

      ALLOCATE ( mean_tm(nx_en,ny_en,nz_en) )

!!  !u
      DO k=1,nz_en
        DO j=1,ny_en
          DO i=1,nx_en
            cfun1 = 0.
            DO l=1,nensvirtl
              cfun1 = cfun1  + ue(i,j,k,l)
            END DO
            mean_tm(i,j,k) = cfun1/nensvirtl

            DO l=1,nensvirtl
              ue(i,j,k,l)=( ue(i,j,k,l)-cfun1 )/sqrt(nensvirtl-1.0)
            END DO

          END DO
        END DO
      END DO

      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            cfun1 =    mean_tm(iim(i,1),jjm(j,1),k)*wm11(i,j)+          &
                       mean_tm(iim(i,2),jjm(j,1),k)*wm21(i,j)+          &
                       mean_tm(iim(i,1),jjm(j,2),k)*wm12(i,j)+          &
                       mean_tm(iim(i,2),jjm(j,2),k)*wm22(i,j)
            u(i,j,k) = (1-gain_fac)*u(i,j,k)+gain_fac*cfun1
          END DO
        END DO
      END DO

!!  !v
      DO k=1,nz_en
        DO j=1,ny_en
          DO i=1,nx_en
            cfun1 = 0.
            DO l=1,nensvirtl
              cfun1 = cfun1  + ve(i,j,k,l)
            END DO
            mean_tm(i,j,k) = cfun1/nensvirtl

            DO l=1,nensvirtl
              ve(i,j,k,l)=( ve(i,j,k,l)-cfun1 )/sqrt(nensvirtl-1.0)
            END DO

          END DO
        END DO
      END DO

      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            cfun1 =    mean_tm(iim(i,1),jjm(j,1),k)*wm11(i,j)+          &
                       mean_tm(iim(i,2),jjm(j,1),k)*wm21(i,j)+          &
                       mean_tm(iim(i,1),jjm(j,2),k)*wm12(i,j)+          &
                       mean_tm(iim(i,2),jjm(j,2),k)*wm22(i,j)
            v(i,j,k) = (1-gain_fac)*v(i,j,k)+gain_fac*cfun1
          END DO
        END DO
      END DO

!!  !w
      DO k=1,nz_en
        DO j=1,ny_en
          DO i=1,nx_en
            cfun1 = 0.
            DO l=1,nensvirtl
              cfun1 = cfun1  + we(i,j,k,l)
            END DO
            mean_tm(i,j,k) = cfun1/nensvirtl

            DO l=1,nensvirtl
              we(i,j,k,l)=( we(i,j,k,l)-cfun1 )/sqrt(nensvirtl-1.0)
            END DO

          END DO
        END DO
      END DO

      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            cfun1 =    mean_tm(iim(i,1),jjm(j,1),k)*wm11(i,j)+          &
                       mean_tm(iim(i,2),jjm(j,1),k)*wm21(i,j)+          &
                       mean_tm(iim(i,1),jjm(j,2),k)*wm12(i,j)+          &
                       mean_tm(iim(i,2),jjm(j,2),k)*wm22(i,j)
            w(i,j,k) = (1-gain_fac)*w(i,j,k)+gain_fac*cfun1
          END DO
        END DO
      END DO

!!  !pres
      DO k=1,nz_en
        DO j=1,ny_en
          DO i=1,nx_en
            cfun1 = 0.
            DO l=1,nensvirtl
              cfun1 = cfun1  + prese(i,j,k,l)
            END DO
            mean_tm(i,j,k) = cfun1/nensvirtl

            DO l=1,nensvirtl
              prese(i,j,k,l)=( prese(i,j,k,l)-cfun1 )/sqrt(nensvirtl-1.0)
            END DO

          END DO
        END DO
      END DO

      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            cfun1 =    mean_tm(iim(i,1),jjm(j,1),k)*wm11(i,j)+           &
                       mean_tm(iim(i,2),jjm(j,1),k)*wm21(i,j)+           &
                       mean_tm(iim(i,1),jjm(j,2),k)*wm12(i,j)+           &
                       mean_tm(iim(i,2),jjm(j,2),k)*wm22(i,j)
            pres(i,j,k) = (1-gain_fac)*pres(i,j,k)+gain_fac*cfun1
          END DO
        END DO
      END DO

!!  !ptprt
      DO k=1,nz_en
        DO j=1,ny_en
          DO i=1,nx_en
            cfun1 = 0.
            DO l=1,nensvirtl
              cfun1 = cfun1  + pte(i,j,k,l)
            END DO
            mean_tm(i,j,k) = cfun1/nensvirtl

            DO l=1,nensvirtl
              pte(i,j,k,l)=( pte(i,j,k,l)-cfun1 )/sqrt(nensvirtl-1.0)
            END DO

          END DO
        END DO
      END DO

      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            cfun1 =    mean_tm(iim(i,1),jjm(j,1),k)*wm11(i,j)+           &
                       mean_tm(iim(i,2),jjm(j,1),k)*wm21(i,j)+           &
                       mean_tm(iim(i,1),jjm(j,2),k)*wm12(i,j)+           &
                       mean_tm(iim(i,2),jjm(j,2),k)*wm22(i,j)
            pt(i,j,k) = (1-gain_fac)*pt(i,j,k)+gain_fac*cfun1
          END DO
        END DO
      END DO

!!  !qv
      DO k=1,nz_en
        DO j=1,ny_en
          DO i=1,nx_en
            cfun1 = 0.
            DO l=1,nensvirtl
              cfun1 = cfun1  + qve(i,j,k,l)
            END DO
            mean_tm(i,j,k) = cfun1/nensvirtl

            DO l=1,nensvirtl
              qve(i,j,k,l)=( qve(i,j,k,l)-cfun1 )/sqrt(nensvirtl-1.0)
            END DO

          END DO
        END DO
      END DO

      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            cfun1 =    mean_tm(iim(i,1),jjm(j,1),k)*wm11(i,j)+           &
                       mean_tm(iim(i,2),jjm(j,1),k)*wm21(i,j)+           &
                       mean_tm(iim(i,1),jjm(j,2),k)*wm12(i,j)+           &
                       mean_tm(iim(i,2),jjm(j,2),k)*wm22(i,j)
            qv(i,j,k) = (1-gain_fac)*qv(i,j,k)+gain_fac*cfun1
          END DO
        END DO
      END DO

!!  !qscalar
      DO nq=1, nscalar

      DO k=1,nz_en
        DO j=1,ny_en
          DO i=1,nx_en
            cfun1 = 0.
            DO l=1,nensvirtl
              cfun1 = cfun1  + qscalare(i,j,k,nq,l)
            END DO
            mean_tm(i,j,k) = cfun1/nensvirtl

            DO l=1,nensvirtl
              qscalare(i,j,k,nq,l)=( qscalare(i,j,k,nq,l)-cfun1 )/sqrt(nensvirtl-1.0)
            END DO

          END DO
        END DO
      END DO

      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            cfun1 =    mean_tm(iim(i,1),jjm(j,1),k)*wm11(i,j)+           &
                       mean_tm(iim(i,2),jjm(j,1),k)*wm21(i,j)+           &
                       mean_tm(iim(i,1),jjm(j,2),k)*wm12(i,j)+           &
                       mean_tm(iim(i,2),jjm(j,2),k)*wm22(i,j)
            qscalar(i,j,k,nq) = (1-gain_fac)*qscalar(i,j,k,nq)+gain_fac*cfun1
          END DO
        END DO
      END DO

      END DO


      DEALLOCATE ( mean_tm )
    END IF

  !
  !   sumu = 0.0
  !   sumv = 0.0
  !   sumw = 0.0
  !   sump = 0.0
  !   sumt = 0.0
  !   sumqv = 0.0
  !   sumqc = 0.0
  !   sumqr = 0.0
  !   sumqi = 0.0
  !   sumqs = 0.0
  !   sumqh = 0.0
  !
  !   DO k = 1, nz
  !     DO j = 1, ny
  !       DO i = 1, nx
  !        DO l=1,nensvirtl
  !         sumu =sumu +  uen(i,j,k,l)**2
  !         sumv =sumv +  ven(i,j,k,l)**2
  !         sumw =sumw +  wen(i,j,k,l)**2
  !         sump =sump +pprten(i,j,k,l)**2
  !         sumt =sumt +ptprten(i,j,k,l)**2
  !         sumqv=sumqv+ qven(i,j,k,l)**2
  !         sumqc=sumqc+ qscalaren(i,j,k,1,l)**2
  !         sumqr=sumqr+ qscalaren(i,j,k,2,l)**2
  !         sumqi=sumqi+ qscalaren(i,j,k,3,l)**2
  !         sumqs=sumqs+ qscalaren(i,j,k,4,l)**2
  !         sumqh=sumqh+ qscalaren(i,j,k,5,l)**2
  !
  !        END DO
  !       END DO
  !     END DO
  !   END DO
  !     print*,'sumu===',sqrt( sumu/nx/ny/nz/nensvirtl )
  !     print*,'sumv===',sqrt( sumv/nx/ny/nz/nensvirtl )
  !     print*,'sumw===',sqrt( sumw/nx/ny/nz/nensvirtl )
  !     print*,'sump===',sqrt( sump/nx/ny/nz/nensvirtl )
  !     print*,'sumt===',sqrt( sumt/nx/ny/nz/nensvirtl )
  !     print*,'sumqv==',sqrt( sumqv/nx/ny/nz/nensvirtl )
  !     print*,'sumqc==',sqrt( sumqc/nx/ny/nz/nensvirtl )
  !     print*,'sumqr==',sqrt( sumqr/nx/ny/nz/nensvirtl )
  !     print*,'sumqi==',sqrt( sumqi/nx/ny/nz/nensvirtl )
  !     print*,'sumqs==',sqrt( sumqs/nx/ny/nz/nensvirtl )
  !     print*,'sumqh==',sqrt( sumqh/nx/ny/nz/nensvirtl )

    RETURN
  END SUBROUTINE ens_perturbNorm

END MODULE module_ensemble
