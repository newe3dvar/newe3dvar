!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE FRCUVW0                    ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE frcuvw0( nx,ny,nz,dtbig1,                                    &
           u,v,w,wcont,ptprt,pprt,qv,qscalar,tke,                       &
           ubar,vbar,ptbar,pbar,rhostr,qvbar,                           &
           usflx,vsflx, x,y,z,zp, j1,j2,j3, sinlat,stabchk,             &
           uforce,vforce,wforce, km,lendel,defsq,                       &
           ubk, vbk, wbk,                                               &
           tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,                &
           tem10,tem11,tem12)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the inactive acoustic forcing terms in the momentum
!  equations. These forcing terms include the advection, mixing, Coriolis
!  force and buoyancy terms, and are accumulated into one array for
!  each equation, i.e.,
!      uforce = - uadv + umix + ucorio.
!      vforce = - vadv + vmix + vcorio.
!      wforce = - wadv + wmix + wbuoy + wcorio.
!  These forcing terms are held fixed during the small acoustic wave
!  integration time steps. The turbulent mixing coefficient for
!  momentum km is an output which will be used to calculate the
!  turbulent mixing of heat and water substances.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  3/21/92.
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  4/10/93 (M. Xue & Hao Jin)
!  Add the terrain.
!
!  9/10/94 (D. Weber & Y. Lu)
!  Cleaned up documentation.
!
!  01/28/95 (G. Bassett)
!  Option added to turn off buoyancy terms (for buoyopt=0).
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!  dtbig1     Local large time step size.
!
!    u        x component of velocity (m/s)
!    v        y component of velocity (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates (m/s).
!    wcont    Contravariant vertical velocity (m/s)
!    ptprt    Perturbation potential temperature (K)
!    pprt     Perturbation pressure (Pascal)
!    qv       Water vapor specific humidity (kg/kg)
!    qc       Cloud water mixing ratio (kg/kg)
!    qr       Rainwater mixing ratio (kg/kg)
!    qi       Cloud ice mixing ratio (kg/kg)
!    qs       Snow mixing ratio (kg/kg)
!    qh       Hail mixing ratio (kg/kg)
!    tke      Turbulent Kinetic Energy ((m/s)**2)
!
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!
!    usflx    Surface flux of u-momentum
!    vsflx    Surface flux of v-momentum
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space(m)
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!    sinalt   Sin of latitude at each grid point
!
!  OUTPUT:
!
!    uforce   Acoustically inactive forcing terms in u-momentum
!             equation (kg/(m*s)**2). uforce= -uadv + umix + ucorio
!    vforce   Acoustically inactive forcing terms in v-momentum
!             equation (kg/(m*s)**2). vforce= -vadv + vmix + vcorio
!    wforce   Acoustically inactive forcing terms in w-momentum
!             equation (kg/(m*s)**2).
!             wforce= -wadv + wmix + wbuoy + wcorio
!    km       Turbulent mixing coefficient for momentum.
!             Used to calculate turbulent mixing of scalars.
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3).
!    defsq    Deformation squared.
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!    tem4     Temporary work array.
!    tem5     Temporary work array.
!    tem6     Temporary work array.
!    tem7     Temporary work array.
!    tem8     Temporary work array.
!    tem9     Temporary work array.
!    tem10    Temporary work array.
!    tem11    Temporary work array.
!    tem12    Temporary work array.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global constants that control model
                            ! execution
  INCLUDE 'grid.inc'        !Added by Ying 10/15/2001
  INCLUDE 'bndry.inc'
!
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  INTEGER :: nt                ! The no. of t-levels of t-dependent
                               ! arrays.
  INTEGER :: tpast             ! Index of time level for the past time.
  INTEGER :: tpresent          ! Index of time level for the present
                               ! time.
  INTEGER :: tfuture           ! Index of time level for the future
                               ! time.
  REAL :: dtbig1               ! Local large time step size.
  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity (m/s)
  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)
  REAL :: ptprt (nx,ny,nz,nt)  ! Perturbation potential temperature (K)
  REAL :: pprt  (nx,ny,nz,nt)  ! Perturbation pressure (Pascal)
  REAL :: qv    (nx,ny,nz,nt)  ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq,nt)  ! Cloud water mixing ratio (kg/kg)
  REAL :: tke   (nx,ny,nz,nt)  ! Turbulent kinetic energy ((m/s)**2)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific
                               ! humidity(kg/kg)

  REAL :: ubk   (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: vbk   (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: wbk   (nx,ny,nz)     ! Total w-velocity (m/s)

  REAL :: x     (nx)           ! x-coord. of the physical and compu-
                               ! tational grid. Defined at u-point(m).
  REAL :: y     (ny)           ! y-coord. of the physical and compu-
                               ! tational grid. Defined at v-point(m).
  REAL :: z     (nz)           ! z-coord. of the computational grid.
                               ! Defined at w-point on the staggered
                               ! grid(m).
  REAL :: zp    (nx,ny,nz)     ! Physical height coordinate defined at
                               ! w-point of the staggered grid(m).

  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! -d(zp)/d(x)
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! -d(zp)/d(y)
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! d(zp)/d(z)

  REAL :: sinlat(nx,ny)        ! Sin of latitude at each grid point
  REAL :: stabchk(nx,ny)

  REAL :: usflx (nx,ny)        ! Surface flux of u-momentum
                               ! (kg/(m*s**2))
  REAL :: vsflx (nx,ny)        ! Surface flux of v-momentum
                               ! (kg/(m*s**2))


  REAL :: uforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in u-momentum equation (kg/(m*s)**2)
                               ! uforce= -uadv + umix + ucorio

  REAL :: vforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in v-momentum equation (kg/(m*s)**2)
                               ! vforce= -vadv + vmix + vcorio

  REAL :: wforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in w-momentum equation (kg/(m*s)**2)
                               ! wforce= -wadv + wmix + wbuoy

  REAL :: km    (nx,ny,nz)     ! Turbulent mixing coefficient for
                               ! momentum.
  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)
  REAL :: defsq (nx,ny,nz)     ! Deformation squared (1/s**2)

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem4  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem5  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem6  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem7  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem8  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem9  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem10 (nx,ny,nz)     ! Temporary work array.
  REAL :: tem11 (nx,ny,nz)     ! Temporary work array.
  REAL :: tem12 (nx,ny,nz)     ! Temporary work array.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: tlevel
  real :: sum1,sum2,sum3

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Calculate the total mixing (which includes subgrid scale
!  turbulent mixing and computational mixing) terms for u, v, w
!  equations.
!
!  These mixing terms are accumulated in the arrays
!  uforce, vforce and wforce.
!
!  The turbulent mixing coefficient KM is saved and passed out of
!  this subroutine and used in the calculation of the turbulent
!  mixing of heat and water substances.
!
!  Since all mixing terms are evaluated at the past time level,
!  we pass only the variable fields at time tpast into the routine.
!
!-----------------------------------------------------------------------
!
!
  uforce=0.0;vforce=0.0;wforce=0.0
  tlevel = tpast
  tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0;tem5=0.0;tem6=0.0
  tem7=0.0;tem8=0.0;tem9=0.0;tem10=0.0;tem11=0.0;tem12=0.0

IF (1==1) THEN ! turn off mixing term
  CALL mixuvw0(nx,ny,nz,                                                 &
              u    (1,1,1,tlevel),v   (1,1,1,tlevel),                   &
              w    (1,1,1,tlevel),                                      &
              ptprt(1,1,1,tlevel),pprt(1,1,1,tlevel),                   &
              qv   (1,1,1,tlevel),qscalar(1,1,1,1,tlevel),              &
              tke  (1,1,1,tpresent),ubar,vbar,ptbar,                    &
              pbar,rhostr,qvbar,                                        &
              usflx,vsflx, x,y,z,zp, j1,j2,j3,stabchk,                  &
              uforce,vforce,wforce, km, lendel,defsq,                   &
              tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,             &
              tem10,tem11,tem12)
  IF (lvldbg>0) then
    sum1 = 0.0
    sum2 = 0.0
    sum3 = 0.0
     do i=1,nx
     do j=1,ny
     do k=1,nz
     sum1 =sum1 + uforce(i,j,k)*uforce(i,j,k)
     sum2 =sum2 + vforce(i,j,k)*vforce(i,j,k)
     sum3 =sum3 + wforce(i,j,k)*wforce(i,j,k)
     end do
     end do
     end do
    print*,'sum1==uforce==',sum1
    print*,'sum2==vforce==',sum2
    print*,'sum3==wforce==',sum3
  end if
END IF  ! turn off mixing term
!
!
!-----------------------------------------------------------------------
!
!  Calculate the advection terms in the momentum equations, using
!  the equivalent advective formulation.
!
!  On exiting advuvw, the advection terms are stored in temporary
!  arrays tem1, tem2 and tem3, they are then added to uforce, vforce
!  and wforce respectively, which already contain the mixing
!  terms for the u, v and w equations respectively.
!
!-----------------------------------------------------------------------
!
!
IF ( 1==1 ) THEN
  tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0;tem5=0.0;tem6=0.0
  tem7=0.0;tem8=0.0;tem9=0.0

  CALL advuvw0(nx,ny,nz,u,v,w,wcont,rhostr,ubar,vbar,                    &
          ubk,vbk,wbk,tem1,tem2,tem3,                 &     ! uadv,vadv and wadv.
          tem4,tem5,tem6,tem7,tem8,tem9)
END IF

if (lvldbg>0) then !output debug information
  sum1 = 0.0
  sum2 = 0.0
  sum3 = 0.0
     do i=1,nx
     do j=1,ny
     do k=1,nz
     sum1 =sum1 + tem1(i,j,k)*tem1(i,j,k)
     sum2 =sum2 + tem2(i,j,k)*tem2(i,j,k)
     sum3 =sum3 + tem3(i,j,k)*tem3(i,j,k)
     end do
     end do
     end do
  print*,'sum1==advuvw0====',sum1
  print*,'sum2==advuvw0====',sum2
  print*,'sum3==advuvw0====',sum3
end if  !output debug information
!

  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        uforce(i,j,k)=uforce(i,j,k) - tem1(i,j,k)
      END DO
    END DO
  END DO

  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        vforce(i,j,k)=vforce(i,j,k) - tem2(i,j,k)
      END DO
    END DO
  END DO

  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        wforce(i,j,k)=wforce(i,j,k) - tem3(i,j,k)
      END DO
    END DO
  END DO
!
if (lvldbg>0) then
   open(99,file='tendency_adv.dat', form='unformatted')
        write(99) uforce,vforce,wforce
   close(99)
end if

!
!-----------------------------------------------------------------------
!
!  Calculate the Coriolis terms in u, v and w equations.
!
!  They are temporarily stored in tem1 and tem2, then added
!  to uforce and vforce.
!
!-----------------------------------------------------------------------
!
!
 IF( coriopt /= 0 ) THEN
! IF( 1==2 ) THEN  ! turn off coriolis tern

    tlevel = tpresent

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          tem1(i,j,k)=0.
          tem2(i,j,k)=0.
          tem3(i,j,k)=0.
          tem4(i,j,k)=0.
          tem5(i,j,k)=0.
          tem6(i,j,k)=0.
          tem7(i,j,k)=0.
        END DO
      END DO
    END DO
    CALL coriol0(nx,ny,nz,                                               &
                u(1,1,1,tlevel),v(1,1,1,tlevel),w(1,1,1,tlevel),        &
                ubar,vbar,rhostr, sinlat,                               &
                tem1,tem2,tem3, tem4,tem5,tem6,tem7)


  if (lvldbg>0) then
    sum1 = 0.0
    sum2 = 0.0
    sum3 = 0.0
       do i=1,nx
       do j=1,ny
       do k=1,nz
         sum1 =sum1 + tem1(i,j,k)*tem1(i,j,k)
         sum2 =sum2 + tem2(i,j,k)*tem2(i,j,k)
         sum3 =sum3 + tem3(i,j,k)*tem3(i,j,k)
       end do
       end do
       end do
    print*,'sum1==coriol0====',sum1
    print*,'sum2==coriol0====',sum2
    print*,'sum3==coriol0====',sum3
  end if
!

    DO k=2,nz-2
      DO j=1,ny-1
        DO i=2,nx-1
          uforce(i,j,k)=uforce(i,j,k)+tem1(i,j,k)
        END DO
      END DO
    END DO

    DO k=2,nz-2
      DO j=2,ny-1
        DO i=1,nx-1
          vforce(i,j,k)=vforce(i,j,k)+tem2(i,j,k)
        END DO
      END DO
    END DO

    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          wforce(i,j,k)=wforce(i,j,k)+tem3(i,j,k)
        END DO
      END DO
    END DO

!   IF( lvldbg >= 4 ) THEN
!     CALL checkuhx(tem1, nx,ny,nz, 'ucorx', tem9)
!     CALL checkuhy(tem1, nx,ny,nz, 'ucory', tem9)
!     CALL checkvhx(tem2, nx,ny,nz, 'vcorx', tem9)
!     CALL checkvhy(tem2, nx,ny,nz, 'vcory', tem9)
!     CALL checkwhx(tem3, nx,ny,nz, 'wcorx', tem9)
!     CALL checkwhy(tem3, nx,ny,nz, 'wcory', tem9)
!   END IF
  END IF ! turn on/off coriolis term
!
!-----------------------------------------------------------------------
!
!  Calculate the buoyancy term for the w-equation.
!  The buoyancy term is stored in tem1 then added to wforce.
!
!  If buoyopt = 0 then buoyancy is turned off.
!
!-----------------------------------------------------------------------
!

  tlevel =  tpresent
!
! IF ( buoyopt /= 0 ) THEN
!
  IF( 1==0 ) THEN
    tem1=0.0; tem2=0.0
    CALL buoycy0(nx,ny,nz, ptprt(1,1,1,tlevel),pprt(1,1,1,tlevel),      &
                qv(1,1,1,tlevel),qscalar(1,1,1,:,tlevel),               &
                ptbar,pbar,rhostr,qvbar,                                &
                tem1,tem2)                              ! wbuoy

    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          wforce(i,j,k)=wforce(i,j,k)+tem1(i,j,k)
        END DO
      END DO
    END DO

   IF (lvldbg>0) then
     sum1 = 0.0
     do i=1,nx
     do j=1,ny
     do k=1,nz
     sum1 =sum1 + tem1(i,j,k)*tem1(i,j,k)
     end do
     end do
     end do
!    print*,'sum1==bouy000====',sum1
!
!    open(99,file='tendency_buoy.dat', form='unformatted')
!       write(99) uforce,vforce,tem1
!    close(99)
   END IF

!
!
  END IF  !switch for buoyancy term
!
!-----------------------------------------------------------------------
!
!  To calculate additional boundary relaxation and mixing terms for
!  the wind fields in the case of externally forced boundary condition.
!
!-----------------------------------------------------------------------
!
! IF ( lbcopt == 2 .AND. mgrid == 1 ) THEN
!   CALL brlxuvw( nx,ny,nz, dtbig1,                                     &
!                 u(1,1,1,1),v(1,1,1,1),w(1,1,1,1),rhostr,              &
!                 uforce,vforce,wforce,                                 &
!                 tem1,tem2,tem3,tem4,tem5,tem6 )
! END IF
!
!
  RETURN
END SUBROUTINE frcuvw0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE CORIOL0                    ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE coriol0(nx,ny,nz,                                             &
           u,v,w,ubar,vbar,rhostr, sinlat, ucorio,vcorio, wcorio,       &
           fcorio1, fcorio2, tem1,tem2)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the Coriolis force terms in the u, v and w equations.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  4/20/92.
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  8/28/92 (M. Xue)
!  Included the domain translation effect into the Coriolis force
!  terms.
!
!  9/10/94 (D. Weber & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical direction.
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        z component of velocity at a given time level (m/s)
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    sinlat   Sin of latitude at each grid point
!
!  OUTPUT:
!
!    ucorio   Coriolis force term in u-eq. (kg/(m*s)**2)
!    vcorio   Coriolis force term in v-eq. (kg/(m*s)**2)
!    wcorio   Coriolis force term in w-eq. (kg/(m*s)**2)
!
!  WORK ARRAYS:
!
!    fcorio1  Temporary work array.
!    fcorio2  Temporary work array.
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity at a time level (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity at a time level (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity at a time level (m/s)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: sinlat(nx,ny)        ! Sin of latitude at each grid point

  REAL :: ucorio(nx,ny,nz)     ! Coriolis force term in u-eq.
  REAL :: vcorio(nx,ny,nz)     ! Coriolis force term in v-eq.
  REAL :: wcorio(nx,ny,nz)     ! Coriolis force term in w-eq.

  REAL :: fcorio1  (nx,ny)     ! Temporary work array.
  REAL :: fcorio2  (nx,ny)     ! Temporary work array.
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array.

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
  REAL :: omega2,sinclat,cosclat
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  IF( coriopt == 0 ) THEN

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          ucorio(i,j,k) = 0.0
          vcorio(i,j,k) = 0.0
          wcorio(i,j,k) = 0.0
        END DO
      END DO
    END DO

    RETURN

  END IF

  omega2 = 2.0* omega

  IF( coriopt == 1 ) THEN

    sinclat = SIN( ATAN(1.0)/45.0 * ctrlat )
    DO j=1,ny
      DO i=1,nx
        fcorio1(i,j) = omega2* sinclat
        fcorio2(i,j) = 0.0
      END DO
    END DO

  ELSE IF( coriopt == 2 ) THEN

    sinclat = SIN( ATAN(1.0)/45.0 * ctrlat )
    cosclat = SQRT( 1.0 - sinclat*sinclat  )
    DO j=1,ny
      DO i=1,nx
        fcorio1(i,j) = omega2* sinclat
        fcorio2(i,j) = omega2* cosclat
      END DO
    END DO

  ELSE IF( coriopt == 3 ) THEN

    DO j=1,ny
      DO i=1,nx
        fcorio1(i,j) = omega2* sinlat(i,j)
        fcorio2(i,j) = 0.0
      END DO
    END DO

  ELSE IF( coriopt == 4 ) THEN

    DO j=1,ny
      DO i=1,nx
        fcorio1(i,j) = omega2* sinlat(i,j)
        fcorio2(i,j) = omega2* SQRT( 1.0 - sinlat(i,j)**2 )
      END DO
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Coriolis terms in u-eq. if coriotrm=1
!
!    ucorio = avgx( rhostr * (fcorio1*avgy(v)-fcorio2*avgz(w)) )
!
!  or if coriotrm=2,
!
!    ucorio = avgx( rhostr * (fcorio1*avgy(v-vbar)-fcorio2*avgz(w)) )
!
!  where fcorio1 = 2*omega*sinlat.
!  and   fcorio2 = 2*omega* sqrt(1-sinlat**2).
!
!-----------------------------------------------------------------------
!
!
  IF( coriotrm == 1 ) THEN
    DO k=1,nz-1
      DO j=1,ny
        DO i=1,nx-1
          tem2(i,j,k) = v(i,j,k) + vmove
        END DO
      END DO
    END DO
  ELSE
    DO k=1,nz-1
      DO j=1,ny
        DO i=1,nx-1
          tem2(i,j,k) = v(i,j,k) + vmove - vbar(i,j,k)
        END DO
      END DO
    END DO
  END IF

  onvf = 0
  CALL avgy0(tem2, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem1)

  onvf = 0
  CALL avgz0(w, onvf,                                                    &
            nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem2)

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k) = rhostr(i,j,k)*                                    &
            (fcorio1(i,j)*tem1(i,j,k)-fcorio2(i,j)*tem2(i,j,k))
      END DO
    END DO
  END DO

  onvf = 1
  CALL avgx0(tem1, onvf,                                                 &
            nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, ucorio)

!
!-----------------------------------------------------------------------
!
!  Coriolis terms in v-eq. and w-eq. if coriotrm=1:
!
!    vcorio = - avgy( rhostr * fcorio1 * avgx(u) )
!    wcorio = avgz( rhostr * fcorio2 * avgx(u) )
!
!  or if coriotrm=2:
!
!    vcorio = - avgy( rhostr * fcorio1 * avgx(u-ubar) )
!    wcorio = avgz( rhostr * fcorio2 * avgx(u) )
!
!
!-----------------------------------------------------------------------
!
  IF( coriotrm == 1 ) THEN
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx
          tem2(i,j,k) = u(i,j,k) + umove
        END DO
      END DO
    END DO
  ELSE
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx
          tem2(i,j,k) = u(i,j,k) + umove -ubar(i,j,k)
        END DO
      END DO
    END DO
  END IF

  onvf = 0
  CALL avgx0(tem2, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem1)

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k) = - rhostr(i,j,k)*fcorio1(i,j)*tem1(i,j,k)
      END DO
    END DO
  END DO

  onvf = 1
  CALL avgy0(tem1, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, vcorio)

  IF( coriopt == 2 .OR. coriopt == 4 ) THEN

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx
          tem2(i,j,k) = u(i,j,k) + umove
        END DO
      END DO
    END DO

    onvf = 0
    CALL avgx0(tem2, onvf,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem1)

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem1(i,j,k) = fcorio2(i,j) * rhostr(i,j,k)* tem1(i,j,k)
        END DO
      END DO
    END DO

    onvf = 1
    CALL avgz0(tem1, onvf,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, wcorio)

  END IF

  RETURN
END SUBROUTINE coriol0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE BUOYCY0                    ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE buoycy0(nx,ny,nz,ptprt,pprt,qv,qscalar,                &
                   ptbar,pbar,rhostr,qvbar, wbuoy, tem1)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the total buoyancy including liquid and solid water
!  loading.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91.
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  3/10/93 (M. Xue)
!  The buoyancy term is reformulated. The previous formula was
!  in error. The water loading was calculated wrong, resulting in
!  a value of the water loading that is typically an order of
!  magnitude too small.
!
!  3/25/94 (G. Bassett & M. Xue)
!  The buoyancy terms are reformulated for better numerical accuracy.
!  Instead of storing numbers which had the form (1+eps)*(1+eps1)
!  (eps << 1 and eps1 <<1), terms were expanded out, and most of the
!  high order terms neglected, except for the second order terms
!  in ptprt, pprt and qvbar.
!
!  9/10/94 (D. Weber & Y. Lu)
!  Cleaned up documentation.
!
!  6/21/95 (Alan Shapiro)
!  Fixed bug involving missing qvpert term in buoyancy formulation.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical direction.
!
!    ptprt    Perturbation potential temperature at a time level (K)
!    pprt     Perturbation pressure at a given time level (Pascal)
!    qv       Water vapor specific humidity at a given time level
!             (kg/kg)
!    qc       Cloud water mixing ratio at a given time level (kg/kg)
!    qr       Rainwater mixing ratio at a given time level (kg/kg)
!    qi       Cloud ice mixing ratio at a given time level (kg/kg)
!    qs       Snow mixing ratio at a given time level (kg/kg)
!    qh       Hail mixing ratio at a given time level (kg/kg)
!
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!
!  OUTPUT:
!
!    wbuoy    The total buoyancy force (kg/(m*s)**2)
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global model control parameters
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'      ! Physical constants
!
!-----------------------------------------------------------------------
!
!  Variable Declarations
!
!-----------------------------------------------------------------------
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: ptprt (nx,ny,nz)     ! Perturbation potential temperature
                               ! at a given time level (K)
  REAL :: pprt  (nx,ny,nz)     ! Perturbation pressure at a given time
                               ! level (Pascal)
  REAL :: qv    (nx,ny,nz)     ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar (nx,ny,nz,nscalarq)     ! Cloud water mixing ratio (kg/kg)

  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific
                               ! humidity(kg/kg)

  REAL :: wbuoy(nx,ny,nz)      ! Total buoyancy in w-eq. (kg/(m*s)**2)

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array.

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,nq
  REAL :: g5,temp1

!
!-----------------------------------------------------------------------
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!-----------------------------------------------------------------------
!
!  The total buoyancy
!
!    wbuoy = rhostr*g ( ptprt/ptbar-pprt/(rhobar*csndsq)+
!    qvprt/(rddrv+qvbar)-(qvprt+qc+qr+qs+qi+qh)/(1+qvbar(i,j,k)) )
!
!  and rddrv=rd/rv, cp, cv, rd and rv are defined in phycst.inc.
!
!  Here, the contribution from pprt (i.e., term pprt/(rhobar*csndsq))
!  is evaluated inside the small time steps, therefore wbuoy
!  does not include this part.
!
!  The contribution from ptprt is calculated inside the small time
!  steps if the potential temperature equation is solved inside
!  small time steps, i.e., if ptsmlstp=1.
!
!-----------------------------------------------------------------------
!

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1

          tem1(i,j,k) = ptprt(i,j,k)/ptbar(i,j,k)                         &
            - pprt(i,j,k)/(cpdcv*pbar(i,j,k))

!           - pprt(i,j,k)/(cpdcv*pbar(i,j,k))                             &
!           - ptprt(i,j,k)*ptprt(i,j,k)/ptbar(i,j,k)/ptbar(i,j,k)         &
!           - 0.5*(1-cpdcv)/cpdcv/cpdcv*pprt(i,j,k)*pprt(i,j,k)           &
!                                      /pbar(i,j,k)/pbar(i,j,k)           &
!           + 0.5*ptprt(i,j,k)*pprt(i,j,k)/ptbar(i,j,k)/pbar(i,j,k)
!
        END DO
      END DO
    END DO

!
!-----------------------------------------------------------------------
!
!  Add on the contributions to the buoyancy from the water vapor
!  content and the liquid and ice water loading.
!
!-----------------------------------------------------------------------
!
!
  IF( moist == 1 .AND. ice == 0 ) THEN  ! Moist case, no ice.
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem1(i,j,k) = tem1(i,j,k)                                     &
              + (qv(i,j,k) - qvbar(i,j,k))/(rddrv + qvbar(i,j,k))       &
              - (qv(i,j,k) - qvbar(i,j,k) + qscalar(i,j,k,P_QC) + qscalar(i,j,k,P_QR))      &
              /(1 + qvbar(i,j,k))
        END DO
      END DO
    END DO
!
  ELSE IF(moist == 1 .AND. ice == 1) THEN
        ! Full microphysics case, loading of liquid and ice water.
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          temp1 = 0.0
          DO nq = 1, nscalarq
            temp1 = temp1 + qscalar(i,j,k,nq)
          END DO

          tem1(i,j,k) = tem1(i,j,k)                                     &
              + (qv(i,j,k) - qvbar(i,j,k))/(rddrv + qvbar(i,j,k))       &
              - (qv(i,j,k) - qvbar(i,j,k) + temp1)/(1 + qvbar(i,j,k))
        END DO
      END DO
    END DO

  END IF
!
!
!-----------------------------------------------------------------------
!
!  Then the total buoyancy:
!
!    wbuoy = tem1 * rhostr * g
!
!  averged to the w-point on the staggered grid.
!
!-----------------------------------------------------------------------
!
  g5 = g*0.5

  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        wbuoy(i,j,k)= (tem1(i,j, k )*rhostr(i,j, k )                    &
                      +tem1(i,j,k-1)*rhostr(i,j,k-1))*g5
      END DO
    END DO
  END DO
!
!
  RETURN
END SUBROUTINE buoycy0
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE PGRAD0                     ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE pgrad0(nx,ny,nz, pprt,                                        &
           j1,j2,j3, upgrad,vpgrad,wpgrad,tem1,tem2)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the pressure gradient terms in the momentum equations.
!  These terms are evaluated every small time step.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91.
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  4/10/93 (M. Xue & Hao Jin)
!  Add the terrain.
!
!  5/25/93 (M. Xue & K. Brewster)
!  Fixed and error in the vertical pressure gradient force term.
!  The error is present in the finite differenced w equation
!  of ARPS 3.0 User's guide. The equation in continuous form is
!  correct.
!
!  9/10/94 (D. Weber & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical direction.
!
!    pprt     Perturbation pressure at a given time level (Pascal)
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    upgrad   Pressure gradient force in u-eq. (kg/(m*s)**2)
!    vpgrad   Pressure gradient force in v-eq. (kg/(m*s)**2)
!    wpgrad   Pressure gradient force in w-eq. (kg/(m*s)**2)
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: pprt  (nx,ny,nz)     ! Perturbation pressure at a given time
                               ! level (Pascal).

  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! -d(zp)/dx.
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! -d(zp)/dy.
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! d(zp)/dz.

  REAL :: upgrad(nx,ny,nz)     ! Pressure gradient force in u-eq.
                               ! (kg/(m*s)**2)
  REAL :: vpgrad(nx,ny,nz)     ! Pressure gradient force in v-eq.
                               ! (kg/(m*s)**2)
  REAL :: wpgrad(nx,ny,nz)     ! Pressure gradient force in w-eq.

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array.
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: onvf
  INTEGER :: i,j,k
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!-----------------------------------------------------------------------
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  upgrad = 0.0
  vpgrad = 0.0
  wpgrad = 0.0
!
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tem1(i,j,k)=pprt(i,j,k)*j3(i,j,k)
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  d(j3*pprt)/dx at u point - 1st component of p grad. force in x-dir
!
!-----------------------------------------------------------------------
!
!
! write(49,'(3i6)') nx,ny,nz
! write(49,'(10f19.5)') (((tem1(i,j,k),i=1,nx),j=1,ny),k=1,nz)
!
! do j=1,ny
!   print*,'tem1=',tem1(1,j,2),tem1(2,j,2),tem1(3,j,2)
! end do
!
  onvf = 1
  CALL difx0(tem1, onvf,                                                 &
            nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, dx, upgrad)
!
! do j=1,ny
!   print*,'tem1=',upgrad(2,j,2),upgrad(3,j,2),upgrad(4,j,2)
! end do
!
! write(50,'(3i6)') nx-2,ny-1,nz-3
! write(50,'(10f19.5)') (((upgrad(i,j,k),i=2,nx-1),j=1,ny-1),k=2,nz-2)
!
! stop
!
!-----------------------------------------------------------------------
!
!  d(j3*pprt)/dy at v point - 1st component of p grad. force in y-dir
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL dify0(tem1, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, dy, vpgrad)
!
!-----------------------------------------------------------------------
!
!  d(pprt)/dz at w point - p grad. force in z-dir
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL difz0(pprt, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, dz, wpgrad)

!
!-----------------------------------------------------------------------
!
!  If there is no terrain, i.e. the ground is flat, skip the
!  following calculations.
!
!-----------------------------------------------------------------------
!
  IF( ternopt /= 0 ) THEN
!
!-----------------------------------------------------------------------
!
!  d(j1*pprt)/dz at u point - 2nd component of p grad. force in x-dir
!  due to terrain
!
!-----------------------------------------------------------------------
!
    onvf = 1
    CALL avgz0(pprt, onvf,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem2)

    onvf = 1
    CALL avgx0(tem2, onvf,                                               &
              nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1, tem1)

    DO k=2,nz-1
      DO j=1,ny-1
        DO i=2,nx-1
          tem1(i,j,k)= tem1(i,j,k)*j1(i,j,k)
        END DO
      END DO
    END DO

    onvf = 0
    CALL difz0(tem1, onvf,                                               &
              nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, dz, tem2)

    DO k=2,nz-2
      DO j=1,ny-1
        DO i=2,nx-1
          upgrad(i,j,k)=upgrad(i,j,k)+tem2(i,j,k)
        END DO
      END DO
    END DO
!
!-----------------------------------------------------------------------
!
!  d(j2*pprt)/dz at v point - 2nd component of p grad. force in y-dir
!  due to terrain
!
!-----------------------------------------------------------------------
!
    onvf = 1
    CALL avgz0(pprt, onvf,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem2)

    onvf = 1
    CALL avgy0(tem2, onvf,                                               &
              nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, tem1)

    DO k=2,nz-1
      DO j=2,ny-1
        DO i=1,nx-1
          tem1(i,j,k)= tem1(i,j,k)*j2(i,j,k)
        END DO
      END DO
    END DO

    onvf = 0
    CALL difz0(tem1, onvf,                                               &
              nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, dz, tem2)

    DO k=2,nz-2
      DO j=2,ny-1
        DO i=1,nx-1
          vpgrad(i,j,k)=vpgrad(i,j,k)+tem2(i,j,k)
        END DO
      END DO
    END DO

  END IF

  IF( lvldbg >= 5) THEN
!   CALL checkuhx(upgrad, nx,ny,nz, 'upgrdx', tem2)
!   CALL checkuhy(upgrad, nx,ny,nz, 'upgrdy', tem2)
!   CALL checkvhx(vpgrad, nx,ny,nz, 'vpgrdx', tem2)
!   CALL checkvhy(vpgrad, nx,ny,nz, 'vpgrdy', tem2)
  END IF

  RETURN
END SUBROUTINE pgrad0
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE UVWRHO0                    ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE uvwrho0(nx,ny,nz,u,v,wcont,rhostr,                            &
           ustr,vstr,wstr)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Compute ustr=u*rhostr, vstr=v*rhostr, wstr=wcont*rhostr.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91.
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  9/10/94 (D. Weber & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    wcont    Contravariant vertical velocity (m/s)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!
!  OUTPUT:
!
!    ustr     u * rhostr at u-point
!    vstr     v * rhostr at v-point
!    wstr     wcont * rhostr at w-point
!
!  WORK ARRAYS:
!
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.

  REAL :: ustr  (nx,ny,nz)     ! u * rhostr
  REAL :: vstr  (nx,ny,nz)     ! v * rhostr
  REAL :: wstr  (nx,ny,nz)     ! wcont * rhostr

  INTEGER :: i,j,k
!
!-----------------------------------------------------------------------
!

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Calculate ustr=rhostr*u, vstr=rhostr*v, wstr=rhostr*wcont
!
!-----------------------------------------------------------------------
!
  CALL rhouvw0(nx,ny,nz,rhostr,ustr,vstr,wstr)

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx
        ustr(i,j,k)=u(i,j,k)*ustr(i,j,k)
      END DO
    END DO
  END DO

  DO k=1,nz-1
    DO j=1,ny
      DO i=1,nx-1
        vstr(i,j,k)=v(i,j,k)*vstr(i,j,k)
      END DO
    END DO
  END DO

  DO k=1,nz
    DO j=1,ny-1
      DO i=1,nx-1
        wstr(i,j,k)=wcont(i,j,k)*wstr(i,j,k)
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE uvwrho0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE RHOUVW0                    ######
!######                                                      ######
!######                Copyright (c) 1993                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE rhouvw0(nx,ny,nz,rhostr,rhostru,rhostrv,rhostrw)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate rhostr averaged to u, v, and w points.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue & Hao Jin
!  3/8/1993.
!
!  MODIFICATION HISTORY:
!
!  9/10/94 (D. Weber & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    rhostr   j3 times base state density rhobar(kg/m**3).
!
!  OUTPUT:
!
!    rhostru  Average rhostr at u points (kg/m**3).
!    rhostrv  Average rhostr at v points (kg/m**3).
!    rhostrw  Average rhostr at w points (kg/m**3).
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! The number of grid points in 3
                               ! directions

  REAL :: rhostr(nx,ny,nz)     ! j3 times base state density rhobar
                               ! (kg/m**3).

  REAL :: rhostru(nx,ny,nz)    ! Average rhostr at u points (kg/m**3).
  REAL :: rhostrv(nx,ny,nz)    ! Average rhostr at v points (kg/m**3).
  REAL :: rhostrw(nx,ny,nz)    ! Average rhostr at w points (kg/m**3).

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  CALL avgsu0(rhostr,nx,ny,nz, 1,ny-1, 1,nz-1, rhostru)
  CALL avgsv0(rhostr,nx,ny,nz, 1,nx-1, 1,nz-1, rhostrv)
  CALL avgsw0(rhostr,nx,ny,nz, 1,nx-1, 1,ny-1, rhostrw)

  RETURN
END SUBROUTINE rhouvw0
