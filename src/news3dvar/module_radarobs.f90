!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%                                                      %%%%%%
!%%%%%%                MODULE RADAROBS                       %%%%%%
!%%%%%%                                                      %%%%%%
!%%%%%%                     Developed by                     %%%%%%
!%%%%%%       National Severe Storm Laboratory, NOAA         %%%%%%
!%%%%%%                                                      %%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
MODULE module_radarobs

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! PURPOSE:
!
!   This module contains all radar obseravation-related arrays that are
!   to be analyzed.
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!
!  03/10/2016. Written based on rdradcol.f90.
!
!  MODIFICATION HISTORY:
!
!  Y. Wang (06/28/2017)
!  Moved namelist reading to this module for integrity.
!
!-----------------------------------------------------------------------

  USE model_precision

  IMPLICIT NONE

  PRIVATE

  !
  ! Constants
  !
  INTEGER, PARAMETER :: nsrc_rad = 5   ! number of sources of radar data
  INTEGER, PARAMETER :: mx_rad   = 160   ! max number of radar sites
  INTEGER, PARAMETER :: nvar_rad = 4
  REAL,    PARAMETER :: dpmiss   = -9999.

  !
  ! Namelist variables
  !
  INTEGER             :: nradfil
  CHARACTER (LEN=256) :: radfname(mx_rad)
  CHARACTER (LEN=256) :: raderrfil(nsrc_rad)
  CHARACTER (LEN=8)   :: srcrad(nsrc_rad)

  INTEGER, ALLOCATABLE :: iuserad(:,:)   ! (0:nsrc_rad,mx_pass)
  REAL    :: bmwidth(mx_rad)
  INTEGER :: grdtlt_flag

  !
  !---------------------------------------------------------------------
  !
  !  Radar site variables
  !
  !---------------------------------------------------------------------
  !
  REAL, ALLOCATABLE :: qsrcrad(:,:)
  REAL, ALLOCATABLE :: qcthrradv(:)

  INCLUDE 'mp.inc'

  INTEGER :: nradarfiles, nrad_anx
  CHARACTER(LEN=256), ALLOCATABLE :: radarfiles(:)       ! only the first nradarfiles are valid
  INTEGER,            ALLOCATABLE :: radarfile_size(:,:) ! dimensions (nradarfiles,4)
             ! (:,1)  - numradcol
             ! (:,2)  - nummaxlev
             ! (:,3)  - icolstr         ! process-local
             ! (:,4)  - icoluse         ! process-local
  INTEGER,            ALLOCATABLE :: radarfile_dualpol(:)

  INTEGER :: ncolrad           ! local number of radar columns within domain
  INTEGER :: mx_colrad         ! local dimension to do allocation
  INTEGER :: ncolrad_total     ! total number of radar columns, MPISUMI(ncolrad) may be greater than ncolrad_total
  INTEGER :: nz_radar          ! Maximum number of vertical levels
  INTEGER :: drdsdh1st

  INTEGER :: istart, iend, jstart, jend
  !
  !---------------------------------------------------------------------
  !
  !  Radar Constants
  !
  !---------------------------------------------------------------------
  !

  INTEGER,          PARAMETER  :: nsrcrad_par=5
  CHARACTER(LEN=8), PARAMETER  :: srcrad_par(nsrcrad_par) = (/          &
          '88D-AII ','88D-NIDS','CASA-IP1','TDWR    ','88D-POL ' /)

  !
  !---------------------------------------------------------------------
  !
  !  Radar site variables
  !
  !---------------------------------------------------------------------
  !
  CHARACTER(LEN=5),   ALLOCATABLE :: stnradar(:)       ! only the first nradarfiles are valid
  INTEGER,            ALLOCATABLE :: isrcradr(:)

  REAL,               ALLOCATABLE :: latradar(:)   ! latitude of radar  (degrees N)
  REAL,               ALLOCATABLE :: lonradar(:)   ! longitude of radar (degrees E)
  REAL,               ALLOCATABLE :: elvradar(:)   ! elevation of feed horn of radar (m MSL)

  !REAL    :: elvang(nz_rdr,mx_rad)
  REAL,               ALLOCATABLE :: elvang(:,:)

  LOGICAL :: radar_size_initialized = .FALSE.    ! radarfiles and size are initialized
  !
  !---------------------------------------------------------------------
  !
  !  Radar observation arrays
  !
  !---------------------------------------------------------------------
  !
  LOGICAL :: radar_obs_array_allocated  = .FALSE.    ! radar data arrays are allocated and filled

  INTEGER, ALLOCATABLE :: indexrad(:)
  INTEGER, ALLOCATABLE :: oindexrad(:)
  LOGICAL, ALLOCATABlE :: userad(:)

  INTEGER, ALLOCATABLE :: irad(:), nlevrad(:)
  INTEGER, ALLOCATABLE :: iradc(:),jradc(:),kradc(:,:)
  REAL,    ALLOCATABLE :: latradc(:),lonradc(:)
  REAL,    ALLOCATABLE :: hgtradc(:,:)   ! height (m MSL) of radar observations
  REAL,    ALLOCATABLE :: obsrad(:,:,:)

  !
  !---------------------------------------------------------------------
  !
  !  Radar observation derived arrays
  !
  !---------------------------------------------------------------------
  !

  LOGICAL :: radar_ext_array_allocated  = .FALSE.    ! radar data arrays are allocated

  REAL, ALLOCATABLE :: xradc(:), yradc(:), trnradc(:)
  REAL, ALLOCATABLE :: distrad(:) ! distance of radar column from source radar
  REAL, ALLOCATABLE :: uazmrad(:) ! u-component of radar beam for each column
  REAL, ALLOCATABLE :: vazmrad(:) ! v-component of radar beam for each column

  REAL, ALLOCATABLE :: theradc(:,:)
  REAL, ALLOCATABLE :: dsdr(:,:)
  REAL, ALLOCATABLE :: dhdr(:,:)

  !REAL    :: distrad(mx_colrad),uazmrad(mx_colrad),vazmrad(mx_colrad)
  !REAL    :: xradc(mx_colrad),yradc(mx_colrad)
  !REAL    :: trnradc(mx_colrad)

  !REAL    :: theradc(nz_rdr,mx_colrad)
  !REAL    :: dsdr(nz_rdr,mx_colrad)
  !REAL    :: dhdr(nz_rdr,mx_colrad)
  !

  !
  !---------------------------------------------------------------------
  !
  !  Radar observation grid-tile format specific arrays
  !
  !---------------------------------------------------------------------
  !

  INTEGER, ALLOCATABLE :: k3L(:,:), k3U(:,:)
  REAL,    ALLOCATABLE :: eleva(:,:)

  !INTEGER :: k3L(nz_rdr,mx_colrad), k3U(nz_rdr,mx_colrad)
  !REAL    :: eleva  (nz,mx_colrad)

  !
  !---------------------------------------------------------------------
  !
  !  Radar analysis arrays
  !
  !---------------------------------------------------------------------
  !

  LOGICAL :: radar_anx_array_allocated  = .FALSE.    ! radar data arrays are allocated

  REAL,    ALLOCATABLE :: oanxrad(:,:,:)
  REAL,    ALLOCATABLE :: odifrad(:,:,:)
  REAL,    ALLOCATABLE :: qobsrad(:,:,:)
  INTEGER, ALLOCATABLE :: qualrad(:,:,:)
  REAL,    ALLOCATABLE :: corrad(:,:,:)

  !REAL    :: oanxrad(nvar_rad,nz_rdr,mx_colrad)
  !REAL    :: odifrad(nvar_rad,nz_rdr,mx_colrad)
  !REAL    :: qobsrad(nvar_rad,nz_rdr,mx_colrad)
  !INTEGER :: qualrad(nvar_rad,nz_rdr,mx_colrad)

  !---------------------------------------------------------------------
  !
  !  PUBLIC members
  !
  !---------------------------------------------------------------------
  !
  PUBLIC :: mx_rad, nsrc_rad, nradarfiles
  PUBLIC :: nradfil, radfname, grdtlt_flag
  PUBLIC :: srcrad, bmwidth, iuserad
  PUBLIC :: qsrcrad, qcthrradv

  PUBLIC :: ncolrad, mx_colrad, ncolrad_total, nz_radar
  PUBLIC :: nvar_rad, nrad_anx, drdsdh1st
  PUBLIC :: stnradar, isrcradr
  PUBLIC :: latradar,lonradar, elvradar
  PUBLIC :: elvang

  PUBLIC :: indexrad, oindexrad, userad

  PUBLIC :: irad, nlevrad
  PUBLIC :: iradc,jradc,kradc,latradc,lonradc
  PUBLIC :: hgtradc,obsrad

  PUBLIC :: xradc, yradc, trnradc
  PUBLIC :: distrad, uazmrad, vazmrad
  PUBLIC :: theradc
  PUBLIC :: dsdr, dhdr
  PUBLIC :: k3L, k3U, eleva
  PUBLIC :: oanxrad, odifrad, qobsrad, qualrad, corrad

  PUBLIC :: radarobs_get_size,radarobs_read_files
  PUBLIC :: radarobs_allocate_arrays
  PUBLIC :: radarobs_read_nml, radarobs_set_usage, radarobs_init_site

  CONTAINS

  !#####################################################################

  SUBROUTINE radarobs_read_nml(unum,myproc,maxpass,istatus)

    INTEGER, INTENT(IN)  :: unum
    INTEGER, INTENT(IN)  :: myproc, maxpass
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------
    NAMELIST /adas_radar/ grdtlt_flag,nradfil,radfname,bmwidth,srcrad,raderrfil,iuserad

  !---------------------------------------------------------------------

    INTEGER :: i,j

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ALLOCATE(iuserad(0:nsrc_rad,maxpass), STAT = istatus)
    CALL check_alloc_status(istatus, "radarobs:iuserad")

    grdtlt_flag = 0
    nradfil=0
    DO i=1,mx_rad
      radfname(i)='NULL'
      bmwidth(i) = 1.0
    END DO
    DO i=1,nsrc_rad
      srcrad(i)='NULL'
      raderrfil(i)='NULL'
    END DO
    DO i=0,nsrc_rad
      DO j=1,maxpass
        iuserad(i,j)=0
      END DO
    END DO

    IF(myproc == 0) THEN
      READ(unum, adas_radar,  END=330)
      WRITE(6,*) 'Namelist block adas_radar sucessfully read.'
      GOTO 331

      330 CONTINUE
      WRITE(*,*) 'ERROR: reading namelist <adas_radar>.'
      istatus = -1
      RETURN
      331 CONTINUE

    END IF

    CALL mpupdatei(grdtlt_flag,1)
    CALL mpupdatei(nradfil,1)
    CALL mpupdatec(radfname,256*mx_rad)
    CALL mpupdatec(bmwidth,mx_rad)
    CALL mpupdatec(srcrad,8*nsrc_rad)
    CALL mpupdatec(raderrfil,256*nsrc_rad)
    CALL mpupdatei(iuserad,(nsrc_rad+1)*maxpass)

    RETURN
  END SUBROUTINE radarobs_read_nml

  !#####################################################################

  SUBROUTINE radarobs_init_site(myproc,maxpass,qbackv,istatus)

    INTEGER, INTENT(IN)  :: myproc, maxpass
    REAL,    INTENT(IN)  :: qbackv    ! velocity part from qback
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j
    INTEGER            :: jsrc, ivar
    CHARACTER (LEN=8 ) :: rdsource
    CHARACTER (LEN=80) :: header

    CHARACTER (LEN=80) :: srcrad_full(nsrc_rad)
    REAL               :: qcmulrad(nsrc_rad)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    ALLOCATE( qsrcrad  (nvar_rad,nsrc_rad) )
    ALLOCATE( qcthrradv(nsrc_rad) )
    qsrcrad   = 0.0
    qcthrradv = 0.0

    IF (myproc == 0) THEN

      DO jsrc=1,nsrc_rad

        IF ( ANY(iuserad(jsrc,:) > 0) ) THEN
          IF(srcrad(jsrc) /= 'NULL') THEN
            WRITE(6,'(/,a,a)') ' Reading ', TRIM(raderrfil(jsrc))
            OPEN(4,FILE=trim(raderrfil(jsrc)),STATUS='old')
            READ(4,'(a8)') rdsource
            IF(rdsource /= srcrad(jsrc)) THEN
              WRITE(6,'(a,i4,a,a,a,a,a)')                               &
                 ' Mismatch of source names',jsrc,                      &
                 ' read ',rdsource,' expected ',srcrad(jsrc)
              CALL arpsstop("mismatch",1)
            END IF
            READ(4,'(a80)') srcrad_full(jsrc)
            READ(4,*)       qcmulrad(jsrc)
            READ(4,'(a80)') header
            READ(4,*) qsrcrad(1,jsrc), qsrcrad(2,jsrc), qsrcrad(3,jsrc)
            WRITE(6,'(//,a,a,/a)') 'Radar data std error for ',         &
                            srcrad(jsrc),srcrad_full(jsrc)
            WRITE(6,'(a,f5.1)') ' QC multiplier: ',qcmulrad(jsrc)
            WRITE(6,'(1x,a)')                                           &
               '    ref(dBz) Vrad(m/s) SpWid(m/s)'
            WRITE(6,'(1x,4f8.2)') (qsrcrad(ivar,jsrc),ivar=1,nvar_rad)
            CLOSE(4)
          ELSE
            DO j=1,maxpass
              iuserad(jsrc,j) = 0
            END DO
          END IF
        END IF
      END DO

      DO jsrc=1,nsrc_rad
        DO ivar=1,nvar_rad
          qsrcrad (ivar,jsrc) = qsrcrad(ivar,jsrc)*qsrcrad(ivar,jsrc)
          !qcthrrad(ivar,jsrc) = qcmulrad(jsrc)*                         &
          !                      SQRT(qback(ivar,1)+qsrcrad(ivar,jsrc))

          !IF (ibeta2==0 .OR. iensmbl==0) THEN
          !  qcthrrad(ivar,isrc)=1.2*qcthrrad(ivar,isrc)
          !ELSE
          !  qcthrrad(ivar,isrc)=1.5*qcthrrad(ivar,isrc)
          !END IF
        END DO
        qcthrradv(jsrc) = qcmulrad(jsrc)*SQRT(qbackv+qsrcrad(2,jsrc))
        ! should only qcthrrad(2,jsrc) be used for velocity
      END DO
    END IF

    CALL mpupdater(qsrcrad,   nvar_rad*nsrc_rad   )
    CALL mpupdater(qcthrradv, nsrc_rad            )
    CALL mpupdatei(iuserad,  (nsrc_rad+1)*maxpass )

    RETURN
  END SUBROUTINE radarobs_init_site

  !#####################################################################

  SUBROUTINE radarobs_set_usage(myproc,ipass,raduvobs,ref_use,radsw,istatus)

    INTEGER, INTENT(IN)  :: myproc,ipass
    INTEGER, INTENT(IN)  :: raduvobs
    LOGICAL, INTENT(IN)  :: ref_use
    INTEGER, INTENT(OUT) :: radsw

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: isrc, icnt
    CHARACTER(LEN=80) :: radsrc(nsrc_rad)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    radsw = 0

    IF (ipass > 0) THEN
      icnt = 0
      IF( nradfil /= 0 .AND. raduvobs /= 0 ) THEN
        DO isrc=1,nsrc_rad
          IF(iuserad(isrc,ipass) > 0) THEN
            radsw=1
            icnt = icnt+1
            radsrc(icnt) = srcrad(isrc)
          END IF
        END DO
      END IF
      !IF( ref_opt  /= 0 .AND. nradfil /= 0 ) radsw = 1
      !IF( raduvobs /= 0 .AND. nradfil /= 0 ) radsw = 1
    ELSE                        ! chk_opt == 1, ipass == 0
      IF( nradfil /= 0 .AND. raduvobs /= 0) radsw = 1
      IF( nradfil /= 0 .AND. ref_use      ) radsw = 1

      IF (radsw == 1) radsrc = srcrad
      icnt  = 1
    END IF

    CALL mpmaxi(radsw)

    IF (myproc == 0) THEN
      IF (radsw == 0) THEN
        WRITE(6,'(3x,a,26x,a)') 'Raw radar data',' none'
      ELSE
        WRITE(6,'(3x,a,26x,a)') 'Raw radar data',' Using'
        DO isrc= 1,icnt
          WRITE(6,'(9x,I0,2a)') isrc,' - ',radsrc(isrc)
        END DO
      END IF
      WRITE(6,*) ' '
    END IF

    RETURN
  END SUBROUTINE radarobs_set_usage

  !#####################################################################

  SUBROUTINE radarobs_get_size(nx,ny,nz,raduvobs,radrhobs,              &
                               iusechk,npass,mosaicopt,                 &
                               radistride,radkstride,verbose,istatus)

    INTEGER, INTENT(IN)  :: nx,ny,nz

    INTEGER, INTENT(IN)  :: raduvobs,radrhobs
    INTEGER, INTENT(IN)  :: mosaicopt

    INTEGER, INTENT(IN)  :: iusechk, npass

    INTEGER, INTENT(IN)  :: radistride,radkstride

    LOGICAL, INTENT(IN)  :: verbose

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: nfile

    INTEGER :: k, icol, ipass

    INTEGER :: isource, typelev, numradcol,nummaxlev, dualpol

    LOGICAL :: proccols, fndsrcrad
    INTEGER :: kradf, lens, filefmt

    CHARACTER (LEN=4) :: stn
    INTEGER :: isrc, knt, kcol, icstrt   ! radar file - local

    REAL    :: latr, lonr, elvr

    INTEGER, ALLOCATABLE :: coli(:),colj(:)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ncolrad_total = 0
    ncolrad       = 0
    nz_radar      = 0

    nfile = nradfil
    IF(nradfil == 0 ) THEN
      RETURN
    ELSE IF (nradfil > mx_rad) THEN
      WRITE(*,'(1x,a,I0,a,I0,a,/,8x,a,I0,a)')                           &
        'ERROR: Pass in number of radar file (',nradfil,                &
        ') is greater than the maximum number of allocated (',mx_rad,').', &
        'Only ',mx_rad,' radar files will be read.'
        !'Please modify file include/adas.inc and rebuild again.'
      !istatus = -1
      !RETURN
      nfile = mx_rad
    END IF
    !proccols=(raduvobs > 0 .OR. radrhobs > 0)
    proccols = .TRUE.   ! Always TRUE for 3dvar

  !---------------------------------------------------------------------
  !
  ! Allocate radar file related arrays
  !
  !---------------------------------------------------------------------

    ALLOCATE(radarfiles(nradfil),        STAT = istatus )
    ALLOCATE(radarfile_size(nradfil,4),  STAT = istatus )
    radarfile_size(:,:) = 0
    ALLOCATE(radarfile_dualpol(nradfil), STAT = istatus )

    ALLOCATE(stnradar(mx_rad),   STAT = istatus )
    ALLOCATE(isrcradr(0:mx_rad), STAT = istatus )

    ALLOCATE(latradar(mx_rad),   STAT = istatus )
    ALLOCATE(lonradar(mx_rad),   STAT = istatus )
    ALLOCATE(elvradar(mx_rad),   STAT = istatus )

  !---------------------------------------------------------------------
  !
  ! Determine process-local limits
  !
  !---------------------------------------------------------------------

    IF (mp_opt > 0) THEN
      istart = (loc_x-1) * (nx-3) + 1  ! this is the global index
      IF (istart < 1 ) istart = 1      ! so the radar grid and the analysis grid
      iend = loc_x * (nx-3) + 3        ! must be the same, and it also cannot
      k = (nx-3) * nproc_x + 3         ! read split files.
      IF (iend > k) iend = k

      jstart = (loc_y-1) * (ny-3) + 1
      IF (jstart < 1 ) jstart = 1
      jend = loc_y * (ny-3) + 3
      k = (ny-3) * nproc_y + 3
      IF (jend > k) jend = k

    ELSE
      istart = 1
      iend   = nx
      jstart = 1
      jend   = ny
    END IF
    !
    !-----------------------------------------------------------------------
    !
    !  Loop through all radars
    !
    !-----------------------------------------------------------------------
    !
    nradarfiles = 0
    icol = 0
    DO kradf=1,nfile

      lens=LEN(trim(radfname(kradf)))
      IF (radfname(kradf)(lens-3:lens) == 'hdf4') THEN
        filefmt=3
      ELSE IF (radfname(kradf)(lens-2:lens) == '.nc') THEN
        filefmt=7
      ELSE
        filefmt=1
      ENDIF

      IF (myproc == 0) THEN
        IF (filefmt == 1) THEN
           CALL radarobs_bin_read_size(radfname(kradf),stn,             &
                              isource,typelev,latr,lonr,elvr,dualpol,   &
                              numradcol,nummaxlev,coli,colj,istatus)
        ELSE IF (filefmt == 7) THEN
           CALL radarobs_net_read_size(radfname(kradf),stn,             &
                              isource,typelev,latr,lonr,elvr,dualpol,   &
                              numradcol,nummaxlev,coli,colj,istatus)
        ELSE
           CALL radarobs_hdf_read_size(radfname(kradf),stn,             &
                              isource,typelev,latr,lonr,elvr,dualpol,   &
                              numradcol,nummaxlev,coli,colj,istatus)
        END IF
      END IF
      CALL mpbarrier
      CALL mpupdatei(istatus,1)
      !write(0,*) 'RADAROBS GET_SIZE: ',kradf, TRIM(radfname(kradf)),istatus
      IF (istatus /= 0) CYCLE  ! GOTO 700

      CALL mpupdatec(stn,4)

      CALL mpupdatei(isource,1)
      CALL mpupdatei(typelev,1)
      CALL mpupdatei(dualpol,1)

      CALL mpupdater(latr,1)
      CALL mpupdater(lonr,1)
      CALL mpupdater(elvr,1)

      CALL mpupdatei(numradcol,1)
      CALL mpupdatei(nummaxlev,1)

      IF (myproc > 0) THEN
        ALLOCATE(coli(numradcol), STAT = istatus)
        CALL check_alloc_status(istatus, "radarobs_get_size:coli")

        ALLOCATE(colj(numradcol), STAT = istatus)
        CALL check_alloc_status(istatus, "radarobs_get_size:colj")
      END IF

      CALL mpupdatei(coli,numradcol)
      CALL mpupdatei(colj,numradcol)

      !
      !--------------------------------------------------------------
      !
      !    Connect source number to source name.
      !
      !--------------------------------------------------------------
      !
      IF(isource < 1) THEN
        WRITE(6,'(1x,a,i5,/10x,a/)')                                    &
          'WARNING: in rdradcol, read isource as ',isource,             &
                   'Setting isource to default, 1'
        isource=1
      END IF

      IF(isource > nsrcrad_par) THEN
        WRITE(6,'(1x,a,i3,a,i3,a,/10x,a/)')                             &
          'WARNING: in rdradcol, read isource as ',isource,             &
          ' only ',nsrcrad_par,' sources known.',                       &
          ' If a new radar source has been added, update rdradcol.f90'
        istatus = -999
        GO TO 700
      END IF

      fndsrcrad = .FALSE.
      DO isrc=1,nsrc_rad
        IF(srcrad(isrc) == srcrad_par(isource)) THEN
          IF (myproc == 0) WRITE(6,'(3x,2a,i0,a,i0,3a)') 'Radar file source', &
               ': isrc = ',isrc,', srcrad(',isrc,') = "',srcrad(isrc),'"'
          fndsrcrad = .TRUE.
          EXIT
        END IF
      END DO

      IF (.NOT. fndsrcrad) THEN
        WRITE(6,'(/1x,a,i4,a,a)') 'ERROR: rdradcol read isource: ',     &
               isource, ' could not find srcrad= ',srcrad_par(isource)
        WRITE(6,'(1x,a//)') 'Please check radar source names'

        istatus = -999
        GO TO 700
      END IF
!
!-----------------------------------------------------------------------
!
!   If this radar source is not going to be used in the successive
!   corrections analysis step and cloud analysis processing is off,
!   then don't bother reading columns.
!
!-----------------------------------------------------------------------
!
      IF(iusechk > 0 ) THEN
        knt=0
        DO ipass=1,npass
          knt=knt+iuserad(isource,ipass)
        END DO
      ELSE
        knt=1
      END IF

      IF (typelev /= 1 .AND. typelev /= 2) THEN
        IF (myproc == 0) WRITE(6,'(1x,a,I0)') 'Currently, this program only support new data format, typelev = ', typelev
        GOTO 700
      END IF

      IF( knt > 0 .OR. mosaicopt > 0 ) THEN

        IF(proccols) THEN
          DO kcol = 1, numradcol
            IF (MOD(coli(kcol)+colj(kcol),radistride) == 0) THEN
              ncolrad_total = ncolrad_total + 1           ! absolute column number
              IF (coli(kcol) < istart .OR. coli(kcol) > iend) CYCLE
              IF (colj(kcol) < jstart .OR. colj(kcol) > jend) CYCLE
              ncolrad = ncolrad + 1
            END IF  ! horizontal stride
          END DO

          IF ( verbose .AND. (icol-icstrt) > 0 ) THEN
            WRITE(6,'(1x,a,I5,a,i6,a)') 'Processor: ',myproc,' accepts ', &
            (icol-icstrt),' non-missing columns.'  ! from file - ',trim(fname),'.'
          END IF

          nradarfiles = nradarfiles + 1
          radarfiles(nradarfiles) = radfname(kradf)
          stnradar(nradarfiles)   = stn
          isrcradr(nradarfiles)   = isrc
          latradar(nradarfiles)   = latr
          lonradar(nradarfiles)   = lonr
          elvradar(nradarfiles)   = elvr
          radarfile_dualpol(nradarfiles) = dualpol
          radarfile_size(nradarfiles,1) = numradcol
          radarfile_size(nradarfiles,2) = nummaxlev
          radarfile_size(nradarfiles,3) = icstrt
          radarfile_size(nradarfiles,4) = ncolrad-icstrt

          icstrt = ncolrad
          nz_radar = MAX(nz_radar,nummaxlev)

        END IF   !  proccols


      ELSE  ! iuserad check failed
        IF (myproc == 0) WRITE(6,'(a,a,/a/)')                           &
              '   Data for source ',srcrad_par(isource),                &
              ' are not used in successive correction step, skipping'
      END IF  ! use source ?

      700 CONTINUE

      IF (ALLOCATED(coli)) DEALLOCATE(coli,colj)

    END DO  ! file loop

    IF (grdtlt_flag == 1) THEN
      IF (typelev /= 2) THEN
        CALL arpsstop('It is not in grid-tilted radar dat format as requested.',1)
      ELSE
        ALLOCATE(elvang(nz_radar,mx_rad), STAT = istatus)
      END IF
    END IF

    IF (verbose ) WRITE(*,'(1x,a,I3.3,a,I8,a,I3,a,I8,a)')               &
      'Radar data dimensions for myproc = ',myproc,', ncolrad = ',ncolrad, &
      ', nz_radar = ',nz_radar,', ncolrad_total = ',ncolrad_total,'.'

    CALL mpmaxi(nz_radar)
    mx_colrad = MAX(ncolrad,1)

    nrad_anx = nvar_rad       ! Analyze all variable in the file

    IF (nradarfiles > 0) THEN
      radar_size_initialized = .TRUE.
      istatus = 0          ! in case last file in radfname not exist
    END IF

    RETURN
  END SUBROUTINE radarobs_get_size

  !#####################################################################

  SUBROUTINE radarobs_read_files(nx,ny,nz,                              &
                    radistride,radkstride,mosaicopt,                    &
                    xs,ys,zs,latgrd,longrd,                             &
                    ref_mos_3d,rhv_mos_3d,zdr_mos_3d,kdp_mos_3d,        &
                    verbose,mos_count,istatus)

    IMPLICIT NONE
    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL,    INTENT(IN)  :: xs(nx), ys(ny), zs(nx,ny,nz)
    REAL,    INTENT(IN)  :: latgrd(nx,ny), longrd(nx,ny)

    INTEGER, INTENT(IN)  :: radistride,radkstride,mosaicopt

    INTEGER, INTENT(OUT) :: mos_count
    REAL,    INTENT(OUT) :: ref_mos_3d(nx,ny,nz)
    REAL,    INTENT(OUT) :: rhv_mos_3d(nx,ny,nz)
    REAL,    INTENT(OUT) :: zdr_mos_3d(nx,ny,nz)
    REAL,    INTENT(OUT) :: kdp_mos_3d(nx,ny,nz)

    LOGICAL, INTENT(IN)  :: verbose
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER          :: mapprin, strhoptin
    REAL             :: dxin,dyin,dzin,dzminin
    REAL             :: ctrlatin,ctrlonin,tlat1in,tlat2in,tlonin,scalin
    CHARACTER(LEN=4) :: stn
    REAL             :: refelvmin,refelvmax
    INTEGER          :: irngmin,irngmax

    REAL             :: latrad, lonrad, elvrad

    REAL,    ALLOCATABLE :: inelvang(:)

    INTEGER, ALLOCATABLE :: coli(:)
    INTEGER, ALLOCATABLE :: colj(:)
    INTEGER, ALLOCATABLE :: colk(:,:)
    INTEGER, ALLOCATABLE :: numlev(:)
    REAL,    ALLOCATABLE :: collat(:)
    REAL,    ALLOCATABLE :: collon(:)
    REAL,    ALLOCATABLE :: radcolhgt(:,:)
    REAL,    ALLOCATABLE :: radcolref(:,:)
    REAL,    ALLOCATABLE :: radcolrhv(:,:)
    REAL,    ALLOCATABLE :: radcolzdr(:,:)
    REAL,    ALLOCATABLE :: radcolkdp(:,:)
    REAL,    ALLOCATABLE :: radcolvel(:,:)
    REAL,    ALLOCATABLE :: radcolnyq(:,:)
    REAL,    ALLOCATABLE :: radcoltim(:,:)

    INCLUDE 'grid.inc'


  !---------------------------------------------------------------------

    CHARACTER(LEN=256) :: fname
    INTEGER :: lens, filefmt

    INTEGER :: numradcol, nummaxlev
    INTEGER :: kcol, kradf, tcol, icol, icstrt, numcol
    INTEGER :: i,j,k,kk, irange
    INTEGER :: iproc, jproc
    REAL    :: elev

    REAL    :: xradc,yradc
    REAL    :: head,sfcrng,srange

    LOGICAL :: matched
    REAL, PARAMETER :: epislon = 1.0E-3

    REAL, PARAMETER :: epsdx = 0.1
    REAL, PARAMETER :: epsdz = 0.01
    REAL, PARAMETER :: epslat = 0.001

    LOGICAL :: dualdata, grdtlt
    REAL    :: dxmin, dymin

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    dxmin = (xs(3)-xs(2))*epislon
    dymin = (ys(3)-ys(2))*epislon

    mos_count  = 0

    IF (.NOT. radar_size_initialized ) THEN
      WRITE(*,'(1x,a)') 'ERROR: radar column size and file arrays are still not initialized.'
      istatus = -1
      RETURN
    END IF

    IF(nradarfiles == 0 ) RETURN

    !IF (.NOT. radar_obs_array_allocated ) CALL radarobs_allocate_arrays(0,grdtlt_flag,nvar_rad,nz,istatus)

    grdtlt = (grdtlt_flag == 1)
    !
    !-------------------------------------------------------------------
    !
    !  Loop through all radar files
    !
    !-------------------------------------------------------------------
    !
    tcol=0
    icol=0

    DO kradf=1,nradarfiles

      fname=radarfiles(kradf)
      dualdata=.FALSE.

      lens=LEN(trim(fname))
      IF (fname(lens-3:lens) == 'hdf4') THEN
        filefmt=3
      ELSE IF (fname(lens-2:lens) == '.nc') THEN
        filefmt=7
      ELSE
        filefmt=1
      ENDIF

      numradcol = radarfile_size(kradf,1)
      nummaxlev = radarfile_size(kradf,2)
      icstrt    = radarfile_size(kradf,3)
      numcol    = radarfile_size(kradf,4)
      dualdata  = (radarfile_dualpol(kradf) > 0)

      latrad    = latradar(kradf)
      lonrad    = lonradar(kradf)
      elvrad    = elvradar(kradf)

      ALLOCATE(coli(numradcol), STAT = istatus)
      CALL check_alloc_status(istatus, "radarobs_read_files:coli")

      ALLOCATE(colj(numradcol), STAT = istatus)
      CALL check_alloc_status(istatus, "radarobs_read_files:colj")

      ALLOCATE(colk(nummaxlev,numradcol), STAT = istatus)
      CALL check_alloc_status(istatus, "radarobs_read_files:colk")

      ALLOCATE(numlev(numradcol), STAT = istatus)
      CALL check_alloc_status(istatus, "radarobs_read_files:numlev")

      coli(:)   = 0
      colj(:)   = 0
      colk(:,:) = 0
      numlev(:) = 0

      ALLOCATE(collat(numradcol), STAT = istatus)
      CALL check_alloc_status(istatus, "radarobs_read_files:collat")

      ALLOCATE(collon(numradcol), STAT = istatus)
      CALL check_alloc_status(istatus, "radarobs_read_files:collon")

      collat(:) = 0.
      collon(:) = 0.

      ALLOCATE(radcolhgt(nummaxlev,numradcol), STAT = istatus)
      CALL check_alloc_status(istatus, "radarobs_read_files:radcolhgt")

      ALLOCATE(radcolref(nummaxlev,numradcol), STAT = istatus)
      CALL check_alloc_status(istatus, "radarobs_read_files:radcolref")

      ALLOCATE(radcolvel(nummaxlev,numradcol), STAT = istatus)
      CALL check_alloc_status(istatus, "radarobs_read_files:radcolvel")

      ALLOCATE(radcolnyq(nummaxlev,numradcol), STAT = istatus)
      CALL check_alloc_status(istatus, "radarobs_read_files:radcolnyq")

      ALLOCATE(radcoltim(nummaxlev,numradcol), STAT = istatus)
      CALL check_alloc_status(istatus, "radarobs_read_files:radcoltim")

      radcolhgt(:,:) = dpmiss
      radcolref(:,:) = dpmiss
      radcolvel(:,:) = dpmiss
      radcolnyq(:,:) = dpmiss
      radcoltim(:,:) = dpmiss

      IF(dualdata) THEN
        ALLOCATE(radcolrhv(nummaxlev,numradcol), STAT = istatus)
        CALL check_alloc_status(istatus, "radarobs_read_files:radcolrhv")

        ALLOCATE(radcolzdr(nummaxlev,numradcol), STAT = istatus)
        CALL check_alloc_status(istatus, "radarobs_read_files:radcolzdr")

        ALLOCATE(radcolkdp(nummaxlev,numradcol), STAT = istatus)
        CALL check_alloc_status(istatus, "radarobs_read_files:radcolkdp")
      ELSE
        ALLOCATE(radcolrhv(1,1), STAT = istatus)
        ALLOCATE(radcolzdr(1,1), STAT = istatus)
        ALLOCATE(radcolkdp(1,1), STAT = istatus)
      END IF
      radcolrhv(:,:) = dpmiss
      radcolzdr(:,:) = dpmiss
      radcolkdp(:,:) = dpmiss

      IF (grdtlt) THEN
        ALLOCATE(inelvang(nummaxlev), STAT = istatus)
        CALL check_alloc_status(istatus, "radarobs_read_files:inelvang")
      ELSE
        ALLOCATE(inelvang(1), STAT = istatus)
        CALL check_alloc_status(istatus, "radarobs_read_files:inelvang")
      END IF

      IF (myproc == 0) THEN
        IF (filefmt == 1) THEN
          CALL radarobs_bin_read_file(fname,numradcol,nummaxlev,verbose,&
                strhoptin,dxin,dyin,dzin,dzminin,                       &
                mapprin,ctrlatin,ctrlonin,tlat1in,tlat2in,tlonin,scalin,&
                stn,irngmin,irngmax,refelvmin,refelvmax,                &
                grdtlt,inelvang,                                        &
                coli,colj,colk,numlev,collat,collon,                    &
                radcolhgt,radcolref,radcolvel,radcolnyq,radcoltim,      &
                radcolrhv,radcolzdr,radcolkdp,                          &
                istatus)
        ELSE IF (filefmt == 7) THEN
          CALL radarobs_net_read_file(fname,numradcol,nummaxlev,verbose,&
                strhoptin,dxin,dyin,dzin,dzminin,                       &
                mapprin,ctrlatin,ctrlonin,tlat1in,tlat2in,tlonin,scalin,&
                stn,irngmin,irngmax,refelvmin,refelvmax,                &
                grdtlt,inelvang,                                        &
                coli,colj,colk,numlev,collat,collon,                    &
                radcolhgt,radcolref,radcolvel,radcolnyq,radcoltim,      &
                radcolrhv,radcolzdr,radcolkdp,                          &
                istatus)
        ELSE
          CALL radarobs_hdf_read_file(fname,numradcol,nummaxlev,verbose,&
                strhoptin,dxin,dyin,dzin,dzminin,                       &
                mapprin,ctrlatin,ctrlonin,tlat1in,tlat2in,tlonin,scalin,&
                stn,irngmin,irngmax,refelvmin,refelvmax,                &
                grdtlt,inelvang,                                        &
                coli,colj,colk,numlev,collat,collon,                    &
                radcolhgt,radcolref,radcolvel,radcolnyq,radcoltim,      &
                radcolrhv,radcolzdr,radcolkdp,                          &
                istatus)
        END IF
      END IF

      CALL mpupdatei(istatus,1)
      IF (istatus /= 0) THEN
        WRITE(*,'(1x,3a)') 'ERROR: reading file "',TRIM(fname),'".'
        CYCLE
      END IF

      CALL mpupdatec(stn,4)

      CALL mpupdatei(mapprin,1)
      CALL mpupdatei(strhoptin,1)
      CALL mpupdater(dxin,1)
      CALL mpupdater(dyin,1)
      CALL mpupdater(dzin,1)
      CALL mpupdater(dzminin,1)

      CALL mpupdater(ctrlatin,1)
      CALL mpupdater(ctrlonin,1)

      CALL mpupdater(tlat1in,1)
      CALL mpupdater(tlat2in,1)
      CALL mpupdater(tlonin,1)

      CALL mpupdatei(irngmin,1)
      CALL mpupdatei(irngmax,1)
      CALL mpupdater(refelvmin,1)
      CALL mpupdater(refelvmax,1)

      CALL mpupdatei(coli,numradcol)
      CALL mpupdatei(colj,numradcol)
      CALL mpupdatei(colk,numradcol*nummaxlev)
      CALL mpupdatei(numlev,numradcol)
      CALL mpupdater(collat,numradcol)
      CALL mpupdater(collon,numradcol)
      CALL mpupdater(radcolhgt,nummaxlev*numradcol)
      CALL mpupdater(radcolref,nummaxlev*numradcol)
      CALL mpupdater(radcolvel,nummaxlev*numradcol)
      CALL mpupdater(radcolnyq,nummaxlev*numradcol)
      CALL mpupdater(radcoltim,nummaxlev*numradcol)
      IF (dualdata) THEN
        CALL mpupdater(radcolzdr,nummaxlev*numradcol)
        CALL mpupdater(radcolrhv,nummaxlev*numradcol)
        CALL mpupdater(radcolkdp,nummaxlev*numradcol)
      END IF

      IF (grdtlt) THEN
        CALL mpupdater(inelvang, nummaxlev)
        DO k = 1, nummaxlev
          elvang(k,kradf) = inelvang(k)
        END DO
      END IF

      DO kcol = 1, numradcol
        IF (MOD(coli(kcol)+colj(kcol),radistride) == 0) THEN
          tcol = tcol + 1           ! absolute column number
          IF (coli(kcol) < istart .OR. coli(kcol) > iend) CYCLE
          IF (colj(kcol) < jstart .OR. colj(kcol) > jend) CYCLE
          icol = icol + 1

          IF (mp_opt > 0) THEN
            CALL lltoxy(1,1,collat(kcol),collon(kcol),xradc,yradc)
            !indexrad(icol)  = -1
            oindexrad(icol) = tcol
            IF( xradc > xs(1)-dxmin .AND. xradc < xs(nx-1)+dxmin  .AND. &
                yradc > ys(1)-dymin .AND. yradc < ys(ny-1)+dymin) THEN
                 indexrad(icol) = myproc   ! temporary setting for using
                                           ! with in the call of prepradar
            ENDIF
          ENDIF
          irad(icol)    = kradf
          iradc(icol)   = coli(kcol) - (loc_x-1)*(nx-3)  ! change to process-local
          jradc(icol)   = colj(kcol) - (loc_y-1)*(ny-3)  ! change to process-local
          latradc(icol) = collat(kcol)
          lonradc(icol) = collon(kcol)
          nlevrad(icol) = numlev(kcol)
          DO kk = 1,numlev(kcol)
            IF ( MOD( (colk(kk,kcol)-1), radkstride ) == 0  .OR. kk == numlev(kcol) ) THEN
              hgtradc (kk,icol) = radcolhgt(kk,kcol)
              obsrad(1,kk,icol) = radcolref(kk,kcol)
              obsrad(2,kk,icol) = radcolvel(kk,kcol)
              IF (.NOT. grdtlt) THEN
                obsrad(3,kk,icol) = radcolnyq(kk,kcol)
                obsrad(4,kk,icol) = radcoltim(kk,kcol)
                   kradc(kk,icol) =      colk(kk,kcol)
              END IF
            END IF
          END DO
        END IF  ! horizontal stride
      END DO

      IF ( (icol-icstrt) /= numcol ) THEN
        WRITE(*,'(1x,a,/,8x,2(a,I0))')                                  &
            'ERROR: It should not be here. Accepted col is not consitent with expected.', &
            'Expected: ',numcol,', Accepted: ', (icol-icstrt),', myproc = ',myproc
        istatus = -9
        RETURN
      END IF

      IF ( verbose .AND. (icol-icstrt) > 0 ) THEN
        WRITE(6,'(1x,a,I5,a,i6,3a)') 'Processor: ',myproc,' accepts ',    &
        (icol-icstrt),' non-missing columns from file - ',trim(fname),'.'
      END IF

!-----------------------------------------------------------------------
!
! Process for radar mosaic
!
!-----------------------------------------------------------------------

      IF( mosaicopt > 0 .AND. .NOT. grdtlt) THEN

        !
        !  First check for match of remapped grid and analysis grid
        !
        matched=.TRUE.
        !IF( mapprin /= mapproj .OR. strhoptin /= strhopt ) THEN
        !  matched=.FALSE.
        !  WRITE(6,'(1x,2a,/2(2(a,i10),/))')                                 &
        !    'WARNING: Grid mis-match for remapped radar ',stn,              &
        !    '  Radar mapproj:',mapprin,'  strhopt:',strhoptin,              &
        !    '  Anlys mapproj:',mapproj,'  strhopt:',strhopt
        !
        !END IF
        IF( mapprin /= mapproj ) THEN
          matched=.FALSE.
          WRITE(6,'(1x,2a,/2(a,i10,/))')                                 &
            'WARNING: Grid mis-match for remapped radar ',stn,           &
            '  Radar mapproj:',mapprin,                                  &
            '  Anlys mapproj:',mapproj

        END IF
        IF(abs(dxin-dx) > epsdx .OR. abs(dyin-dy) > epsdx ) THEN !.OR.              &
          !abs(dzin-dz) > epsdz .OR. abs(dzminin-dzmin) > epsdz ) THEN
          matched=.FALSE.
          WRITE(6,'(1x,2a,/2(4(a,f12.3),/))')                           &
            'WARNING: Grid mis-match for remapped radar ',stn,          &
            '  Radar dx:',dxin,', dy:',dyin, ', dz:',dzin,', dzmin:',dzminin, &
            '  Anlys dx:',dx,  ', dy:',dy,   ', dz:',dz,  ', dzmin:',dzmin
        END IF
        IF(abs(ctrlatin-ctrlat) > epslat .OR.                           &
           abs(ctrlonin-ctrlon) > epslat ) THEN
          matched=.FALSE.
          WRITE(6,'(1x,2a,/2(a,f12.4))')                                &
            'WARNING: Grid mis-match for remapped radar ',stn,          &
            '  Radar ctrlat:',ctrlatin,'  ctrlonin:',ctrlonin
        END IF
        IF(abs(tlat1in-trulat1) > epslat .OR.                           &
           abs(tlat2in-trulat2) > epslat .OR.                           &
           abs(tlonin  -trulon) > epslat ) THEN
          matched=.FALSE.
          WRITE(6,'(1x,2a,/3(a,f12.4))')                                &
            'WARNING: Grid mis-match for remapped radar ',stn,          &
            '  Radar trulat1:',tlat1in,'  trulat2:',tlat2in,            &
            '    trulon:',tlonin
        END IF
        !
        !  Since clear columns are not written, set reflectivity mosaic
        !  to zero within the active radar scan area.  The zero value replaces
        !  the default missing value and carries a different meaning in the
        !  cloud analysis.  Like the mosaicking done for observed reflectivity
        !  values this is done with a max function to avoid overwriting data
        !  from previous radars.
        !
        IF(matched) THEN
          IF(myproc == 0) THEN
            WRITE(6,'(1x,3a)') 'Adding radar ',stn,' to mosaic.'
            WRITE(6,'(1x,2(a,f10.2),a)') 'Reflectivity coverage ',      &
              (irngmin*0.001),' km  to ',(irngmax*0.001),' km.'
            WRITE(6,'(1x,2(a,f10.2),a)') 'Reflectivity elevs    ',      &
              refelvmin,' deg to ',refelvmax,' deg.'
          END IF
          !
          ! NOTE: since radremap does not distinguish missing observation
          !       and observation that observes nothing, we have to
          !       use radar range to distinguish the radar coverage area.
          !
          DO j=1,ny
            DO i=1,nx
              CALL disthead(latrad,lonrad,latgrd(i,j),longrd(i,j),head,sfcrng)
              DO k=2,nz-1
                CALL beamelv(zs(i,j,k)-elvrad,sfcrng,elev,srange)
                irange = NINT(srange)

                IF(irange < irngmax  .AND. irange > irngmin .AND.       &
                   elev >= refelvmin .AND. elev <= refelvmax ) THEN
                  ref_mos_3d(i,j,k) = max(ref_mos_3d(i,j,k),0.)
                END IF

              END DO
            END DO
          END DO

          !
          !  Now process the radar columns that are in this processor's domain.
          !  ref_max mosaic is formed using the max value of all radar observations.
          !  Mosaic of ref and dual pol variables is formed using the nearest radar observations.

          IF(dualdata) THEN

            DO kcol = 1, numradcol
              iproc = (coli(kcol)-2)/(nx-3) + 1
              jproc = (colj(kcol)-2)/(ny-3) + 1

              IF (iproc == loc_x .AND. jproc == loc_y) THEN
                i =  MOD((coli(kcol)-2),(nx-3)) + 2
                j =  MOD((colj(kcol)-2),(ny-3)) + 2

                IF( i < (nx-1) .AND. j < (ny-1) .AND. i >= 2 .AND. j >= 2 ) THEN
                  !CALL disthead(latrad,lonrad,latgrd(i,j),longrd(i,j),head,sfcrng)

                  DO kk=1,numlev(kcol)
                    k=colk(kk,kcol)
                    IF(k <= nz .AND. k > 0) THEN
                      IF(radcolref(kk,kcol) > ref_mos_3d(i,j,k)) THEN
                        ref_mos_3d(i,j,k)  = radcolref(kk,kcol)
                        rhv_mos_3d(i,j,k)  = radcolrhv(kk,kcol)
                        zdr_mos_3d(i,j,k)  = radcolzdr(kk,kcol)
                        kdp_mos_3d(i,j,k)  = radcolkdp(kk,kcol)
                      END IF
                    END IF  ! 1 < k < nz
                  END DO  ! kk = 1, klev
                END IF  ! col index within limits
              END IF  ! this processor
            END DO  ! kcol

          ELSE  ! no dual-pol data

            rhv_mos_3d(:,:,:)  = dpmiss
            zdr_mos_3d(:,:,:)  = dpmiss
            kdp_mos_3d(:,:,:)  = dpmiss

            DO kcol = 1, numradcol
              iproc = (coli(kcol)-2)/(nx-3) + 1
              jproc = (colj(kcol)-2)/(ny-3) + 1

              IF (iproc == loc_x .AND. jproc == loc_y) THEN
                i =  MOD((coli(kcol)-2),(nx-3)) + 2
                j =  MOD((colj(kcol)-2),(ny-3)) + 2

                IF( i < (nx-1) .AND. j < (ny-1) .AND. i >= 2 .AND. j >= 2 ) THEN

                  !write(100+myproc, *) ' coli,colj: ',coli(kcol),colj(kcol), iproc, jproc, i, j

                  !CALL disthead(latrad,lonrad,latgrd(i,j),longrd(i,j),head,sfcrng)

                  DO kk=1,numlev(kcol)
                    k=colk(kk,kcol)
                    IF(k <= nz .AND. k > 0) THEN
                      IF(radcolref(kk,kcol) > ref_mos_3d(i,j,k)) THEN
                        ref_mos_3d(i,j,k)  = radcolref(kk,kcol)
                      END IF
                    END IF  ! 1 < k < nz
                  END DO  ! kk = 1, klev
                END IF  ! col index within limits
              END IF  ! this processor
            END DO  ! kcol

          END IF  ! whether it is dualdata
          mos_count = mos_count+1
        END IF ! grid matched

      END IF ! mosaicopt ref_mos_3d_block

      DEALLOCATE(inelvang)

      DEALLOCATE(coli, colj, colk, numlev )
      DEALLOCATE(collat, collon)

      DEALLOCATE(radcolhgt, radcolref, radcolvel, radcolnyq, radcoltim)

      DEALLOCATE( radcolrhv, radcolzdr, radcolkdp )

    END DO  ! file loop

    RETURN
  END SUBROUTINE radarobs_read_files

  !#####################################################################

  SUBROUTINE radarobs_allocate_arrays(nz,istatus)

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nz
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (.NOT. radar_obs_array_allocated ) THEN
      ALLOCATE(indexrad(mx_colrad),                   STAT = istatus )
      ALLOCATE(oindexrad(mx_colrad),                  STAT = istatus )
      indexrad(:)  = -1
      oindexrad(:) = -1
      ALLOCATE(userad(mx_colrad),                     STAT = istatus )

      ALLOCATE(irad(mx_colrad),                       STAT = istatus )
      ALLOCATE(nlevrad(mx_colrad),                    STAT = istatus )
      ALLOCATE(iradc(mx_colrad),                      STAT = istatus )
      ALLOCATE(jradc(mx_colrad),                      STAT = istatus )
      ALLOCATE(kradc(nz_radar,mx_colrad),             STAT = istatus )
      ALLOCATE(latradc(mx_colrad),                    STAT = istatus )
      ALLOCATE(lonradc(mx_colrad),                    STAT = istatus )
      ALLOCATE(hgtradc(nz_radar,mx_colrad),           STAT = istatus )
      ALLOCATE(obsrad(nvar_rad,nz_radar,mx_colrad),   STAT = istatus )
      hgtradc(:,:)  = dpmiss
      obsrad(:,:,:) = dpmiss

      radar_obs_array_allocated = .TRUE.
    END IF

    IF (.NOT. radar_ext_array_allocated ) THEN


      ALLOCATE(xradc(mx_colrad),   STAT = istatus )
      ALLOCATE(yradc(mx_colrad),   STAT = istatus )
      ALLOCATE(trnradc(mx_colrad), STAT = istatus )
      ALLOCATE(distrad(mx_colrad), STAT = istatus )
      ALLOCATE(uazmrad(mx_colrad), STAT = istatus )
      ALLOCATE(vazmrad(mx_colrad), STAT = istatus )

      ALLOCATE(theradc(nz_radar,mx_colrad), STAT = istatus )

      IF (grdtlt_flag == 1) THEN
        ALLOCATE(k3L(nz_radar,mx_colrad),    STAT = istatus )
        ALLOCATE(k3U(nz_radar,mx_colrad),    STAT = istatus )
        ALLOCATE(eleva(nz,mx_colrad),    STAT = istatus )
        ALLOCATE(dsdr (nz,mx_colrad),    STAT = istatus )
        ALLOCATE(dhdr (nz,mx_colrad),    STAT = istatus )
        drdsdh1st = nz
      ELSE
        ALLOCATE(dsdr(nz_radar,mx_colrad),    STAT = istatus )
        ALLOCATE(dhdr(nz_radar,mx_colrad),    STAT = istatus )
        drdsdh1st = nz_radar
      END IF

      radar_ext_array_allocated  = .TRUE.    ! radar data arrays are allocated
    END IF

    IF (.NOT. radar_anx_array_allocated ) THEN
      ! grdtlt == 1?????
      ALLOCATE(oanxrad(nrad_anx,nz_radar,mx_colrad), STAT = istatus )
      ALLOCATE(odifrad(nrad_anx,nz_radar,mx_colrad), STAT = istatus )
      ALLOCATE(qobsrad(nrad_anx,nz_radar,mx_colrad), STAT = istatus )
      ALLOCATE(qualrad(nrad_anx,nz_radar,mx_colrad), STAT = istatus )
      qualrad = -99
      ALLOCATE(corrad (nz_radar,mx_colrad,nrad_anx), STAT = istatus )

      radar_anx_array_allocated  = .TRUE.    ! radar data arrays are allocated
    END IF

    RETURN
  END SUBROUTINE radarobs_allocate_arrays

  !#####################################################################

  SUBROUTINE radarobs_deallocate_arrays(istatus)

    IMPLICIT NONE

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (radar_obs_array_allocated) THEN
      DEALLOCATE(indexrad,oindexrad,userad,   STAT = istatus )

      DEALLOCATE(irad,nlevrad,         STAT = istatus )
      DEALLOCATE(iradc,jradc,kradc,    STAT = istatus )
      DEALLOCATE(latradc,lonradc,      STAT = istatus )
      DEALLOCATE(hgtradc,obsrad,       STAT = istatus )

      radar_obs_array_allocated = .FALSE.
    END IF

    IF (radar_ext_array_allocated ) THEN


      DEALLOCATE(xradc,   STAT = istatus )
      DEALLOCATE(yradc,   STAT = istatus )
      DEALLOCATE(trnradc, STAT = istatus )
      DEALLOCATE(distrad, STAT = istatus )
      DEALLOCATE(uazmrad, STAT = istatus )
      DEALLOCATE(vazmrad, STAT = istatus )

      DEALLOCATE(theradc, STAT = istatus )

      DEALLOCATE(dsdr,    STAT = istatus )
      DEALLOCATE(dhdr,    STAT = istatus )

      IF (ALLOCATED(k3L)) THEN
        DEALLOCATE(k3L,    STAT = istatus )
        DEALLOCATE(k3U,    STAT = istatus )
        DEALLOCATE(eleva,  STAT = istatus )
      END IF

      radar_ext_array_allocated  = .FALSE.    ! radar data arrays are allocated
    END IF

    IF (radar_anx_array_allocated ) THEN
      ! grdtlt == 1?????
      DEALLOCATE(oanxrad, STAT = istatus )
      DEALLOCATE(odifrad, STAT = istatus )
      DEALLOCATE(qobsrad, STAT = istatus )
      DEALLOCATE(qualrad, STAT = istatus )
      DEALLOCATE(corrad, STAT = istatus )

      radar_anx_array_allocated  = .FALSE.    ! radar data arrays are allocated
    END IF

    RETURN
  END SUBROUTINE radarobs_deallocate_arrays

  !#####################################################################

  SUBROUTINE radarobs_hdf_read_size(filename,stn,isource,typelev,       &
                                    latrad,lonrad,elvrad,dualpol,       &
                                    numradcol,nummaxlev,                &
                                    outi,outj,istatus)
    IMPLICIT NONE

    CHARACTER(LEN=256), INTENT(IN)  :: filename
    CHARACTER(LEN=4),   INTENT(OUT) :: stn
    INTEGER,            INTENT(OUT) :: isource, typelev,dualpol
    REAL,               INTENT(OUT) :: latrad,lonrad,elvrad
    INTEGER,            INTENT(OUT) :: numradcol,nummaxlev
    INTEGER,ALLOCATABLE,INTENT(OUT) :: outi(:),outj(:)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: sd_id

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    WRITE(6,'(/1x,a,a)') 'get_radarobs_size: Opening radar file - ',trim(filename)

    CALL hdfopen(trim(filename), 1, sd_id)
    IF (sd_id < 0) THEN
      WRITE (6,'(1x,3a)') 'get_radarobs_size: ERROR opening hdf file:',      &
                trim(filename),' for reading.'
      istatus = sd_id
      RETURN
    END IF

    CALL hdfrdc(sd_id, 4, 'radid', stn,    istatus)
    CALL hdfrdi(sd_id, 'isource', isource, istatus)
    CALL hdfrdi(sd_id, 'dualpol', dualpol, istatus)

    CALL hdfrdr(sd_id, 'latrad', latrad, istatus)
    CALL hdfrdr(sd_id, 'lonrad', lonrad, istatus)
    CALL hdfrdr(sd_id, 'elvrad', elvrad, istatus)

    CALL hdfrdi(sd_id, 'numradcol', numradcol, istatus)
    CALL hdfrdi(sd_id, 'nummaxelv', nummaxlev, istatus)

    CALL hdfrdi(sd_id, 'typelev', typelev, istatus)
    IF (istatus /= 0) THEN
      typelev = 0
      istatus = 0
    END IF

    ALLOCATE(outi(numradcol), STAT = istatus)
    CALL check_alloc_status(istatus, "radarobs_hdf_read_size:outi")

    ALLOCATE(outj(numradcol), STAT = istatus)
    CALL check_alloc_status(istatus, "radarobs_hdf_read_size:outj")

    CALL hdfrd1di(sd_id,'radcoli',  numradcol,outi,istatus)
    CALL hdfrd1di(sd_id,'radcolj',  numradcol,outj,istatus)

    CALL hdfclose(sd_id,istatus)

    RETURN
  END SUBROUTINE radarobs_hdf_read_size

  !#####################################################################

  SUBROUTINE radarobs_hdf_read_file(filename,numradcol,nummaxlev,verbose, &
                strhopt,dx,dy,dz,dzmin,                                 &
                mapproj,ctrlat,ctrlon,trulat1,trulat2,trulon,scale,     &
                stn,irngmin,irngmax,refelvmin,refelvmax,                &
                grdtlt,inelvang,                                        &
                coli,colj,colk,nlevls,collat,collon,                    &
                radcolhgt,radcolref,radcolvel,radcolnyq,radcoltim,      &
                radcolrhv,radcolzdr,radcolkdp,                          &
                istatus)

    USE model_precision

    IMPLICIT NONE

    CHARACTER(LEN=256), INTENT(IN)  :: filename
    INTEGER,            INTENT(IN)  :: numradcol, nummaxlev
    LOGICAL,            INTENT(IN)  :: verbose

    CHARACTER(LEN=4),   INTENT(OUT) :: stn
    INTEGER,            INTENT(OUT) :: strhopt,mapproj
    REAL,               INTENT(OUT) :: dx,dy,dz,dzmin
    REAL,               INTENT(OUT) :: ctrlat,ctrlon,trulat1,trulat2,trulon
    REAL,               INTENT(OUT) :: scale
    INTEGER,            INTENT(OUT) :: irngmin,irngmax
    REAL,               INTENT(OUT) :: refelvmin,refelvmax

    LOGICAL, INTENT(IN)  :: grdtlt
    REAL,    INTENT(OUT) :: inelvang(:)

    REAL,    INTENT(OUT) :: collat(:)
    REAL,    INTENT(OUT) :: collon(:)
    INTEGER, INTENT(OUT) :: nlevls(:)

    INTEGER, INTENT(OUT) :: coli(:)
    INTEGER, INTENT(OUT) :: colj(:)

    INTEGER, INTENT(OUT) :: colk(:,:)

    REAL,    INTENT(OUT) :: radcolhgt(:,:)
    REAL,    INTENT(OUT) :: radcolref(:,:)
    REAL,    INTENT(OUT) :: radcolvel(:,:)
    REAL,    INTENT(OUT) :: radcolnyq(:,:)
    REAL,    INTENT(OUT) :: radcoltim(:,:)

    REAL,    INTENT(OUT) :: radcolrhv(:,:)
    REAL,    INTENT(OUT) :: radcolzdr(:,:)
    REAL,    INTENT(OUT) :: radcolkdp(:,:)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: sd_id
    INTEGER (KIND=INT16), ALLOCATABLE :: itmp2(:,:)

  !---------------------------------------------------------------------

    INTEGER, PARAMETER  :: mxradvr=10
    INTEGER :: iradvr(mxradvr)

    INTEGER :: ireftim, itime, vcpnum, dualpol,isource,idummy
    CHARACTER (LEN=80)  :: runname
    INTEGER :: iradfmt,nradvr,typelev,numcol,numlev
    REAL    :: latrad,lonrad,elvrad

    INTEGER :: iyr, imon, idy, ihr, imin, isec

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (grdtlt) THEN
      WRITE(*,'(1x,a)') 'ERROR: HDF format is still not added for grid and tilt format.'
      istatus = -1
      RETURN
    END IF

    WRITE(6,'(/1x,a,a)') 'radarobs_hdf_read_file: Opening radar file - ',trim(filename)

    CALL hdfopen(trim(filename), 1, sd_id)
    IF (sd_id < 0) THEN
      WRITE (6,'(1x,3a)') 'radarobs_hdf_read_file: ERROR opening hdf file:',      &
                trim(filename),' for reading.'
      istatus = sd_id
      RETURN
    END IF
    CALL hdfrdc(sd_id, 4, 'radid', stn, istatus)

    CALL hdfrdi(sd_id, 'ireftim', ireftim, istatus)  !
    CALL hdfrdi(sd_id, 'itime',   itime,   istatus)  !
    CALL hdfrdi(sd_id, 'vcpnum',  vcpnum,  istatus)  !
    CALL hdfrdi(sd_id, 'isource', isource, istatus)  !
    CALL hdfrdi(sd_id, 'dualpol', dualpol, istatus)
!
!-----------------------------------------------------------------------
!
!    Read more header data
!
!-----------------------------------------------------------------------
!
    CALL hdfrdc(sd_id, 40, 'runname', runname, istatus)  !
    CALL hdfrdi(sd_id, 'iradfmt', iradfmt, istatus)      !
    CALL hdfrdi(sd_id, 'strhopt', strhopt, istatus)
    CALL hdfrdi(sd_id, 'mapproj', mapproj, istatus)
    CALL hdfrdi(sd_id, 'irngmin', irngmin, istatus)
    CALL hdfrdi(sd_id, 'irngmax', irngmax, istatus)

    CALL hdfrdr(sd_id, 'dx', dx, istatus)
    CALL hdfrdr(sd_id, 'dy', dy, istatus)
    CALL hdfrdr(sd_id, 'dz', dz, istatus)
    CALL hdfrdr(sd_id, 'dzmin', dzmin, istatus)
    CALL hdfrdr(sd_id, 'ctrlat', ctrlat, istatus)
    CALL hdfrdr(sd_id, 'ctrlon', ctrlon, istatus)
    CALL hdfrdr(sd_id, 'trulat1', trulat1, istatus)
    CALL hdfrdr(sd_id, 'trulat2', trulat2, istatus)
    CALL hdfrdr(sd_id, 'trulon', trulon, istatus)
    CALL hdfrdr(sd_id, 'sclfct', scale, istatus)
    CALL hdfrdr(sd_id, 'latrad', latrad, istatus)
    CALL hdfrdr(sd_id, 'lonrad', lonrad, istatus)
    CALL hdfrdr(sd_id, 'elvrad', elvrad, istatus)
    CALL hdfrdr(sd_id, 'refelvmin', refelvmin, istatus)
    CALL hdfrdr(sd_id, 'refelvmax', refelvmax, istatus)

    CALL hdfrdi(sd_id, 'nradvr', nradvr, istatus)
    CALL hdfrd1di(sd_id,'iradvr', mxradvr,iradvr,istatus)

    CALL hdfrdi(sd_id, 'numradcol', numcol, istatus)
    CALL hdfrdi(sd_id, 'nummaxelv', numlev, istatus)

    typelev = 0
    CALL hdfrdi(sd_id, 'typelev', typelev, istatus)

    IF (numcol /= numradcol .OR. numlev /= nummaxlev) THEN
      WRITE(*,'(1x,3a,/,2(8x,2(a,I5),/))')                              &
         'ERROR: Numbers in the file "',TRIM(filename),                 &
         '" are not consistent with expectation:',                      &
         'Expected: numradcol = ',numradcol,', nummaxlev = ',nummaxlev, &
         'In file:  numradcol = ',numcol,   ', nummaxlev = ',numlev
      istatus = -2
      RETURN
    END IF

    IF (verbose ) THEN
      WRITE(6,'(1x,2a)')      'runname: ',runname
      WRITE(6,'(1x,3(a,I2))') 'iradfmt: ',iradfmt,                      &
                    ', strhoptin: ',strhopt,', mapprin: ',mapproj
      WRITE(6,'(1x,a,3F8.0)') 'dxin,dyin,dzin: ',dx,dy,dz
      WRITE(6,'(1x,a,2F8.2)') 'ctrlatin,ctrlonin: ',ctrlat,ctrlon
      WRITE(6,'(1x,a,3F8.2)') 'tlat1in,tlat2in,tlonin: ',               &
                               trulat1,trulat2,trulon
      WRITE(6,'(1x,a,F8.0)')  'scalin: ',scale
      WRITE(6,'(1x,a,3F8.2)') 'latrad,lonrad,elvrad: ',                 &
                               latrad,lonrad,elvrad
      WRITE(6,'(1x,a,20I4)')  'Got nradvr,iradvr: ',nradvr,iradvr
      WRITE(6,'(1x,a,i6)') ' Got dualpol: ',dualpol

      CALL abss2ctim(itime, iyr, imon, idy, ihr, imin, isec )
      iyr=MOD(iyr,100)
      WRITE(6,'(/a,i2.2,a,i2.2,a,i2.2,1X,i2.2,a,i2.2,a)')               &
          ' Reading remapped raw radar data for: ',                     &
          imon,'-',idy,'-',iyr,ihr,':',imin,' UTC'
    END IF

    ALLOCATE (itmp2(nummaxlev,numradcol), STAT=istatus)
    CALL check_alloc_status(istatus, "read_process_radar_column_format:itmp2")

    CALL hdfrd1d (sd_id,'radcollat',numradcol,collat,istatus)
    CALL hdfrd1d (sd_id,'radcollon',numradcol,collon,istatus)
    CALL hdfrd1di(sd_id,'numelev',  numradcol,nlevls,istatus)

    CALL hdfrd1di(sd_id,'radcoli',  numradcol,coli,istatus)
    CALL hdfrd1di(sd_id,'radcolj',  numradcol,colj,istatus)

    CALL hdfrd2di(sd_id,'radcolk',  nummaxlev,numradcol,colk,istatus)

    CALL hdfrd2d (sd_id,'radcolhgt',nummaxlev,numradcol,radcolhgt,istatus,itmp2)
    CALL hdfrd2d (sd_id,'radcolref',nummaxlev,numradcol,radcolref,istatus,itmp2)
    CALL hdfrd2d (sd_id,'radcolvel',nummaxlev,numradcol,radcolvel,istatus,itmp2)
    CALL hdfrd2d (sd_id,'radcolnyq',nummaxlev,numradcol,radcolnyq,istatus,itmp2)
    CALL hdfrd2d (sd_id,'radcoltim',nummaxlev,numradcol,radcoltim,istatus,itmp2)

    IF(dualpol > 0) THEN
      CALL hdfrd2d (sd_id,'radcolrhv',nummaxlev,numradcol,radcolrhv,istatus,itmp2)
      CALL hdfrd2d (sd_id,'radcolzdr',nummaxlev,numradcol,radcolzdr,istatus,itmp2)
      CALL hdfrd2d (sd_id,'radcolkdp',nummaxlev,numradcol,radcolkdp,istatus,itmp2)
    END IF

    CALL hdfclose(sd_id,istatus)

    DEALLOCATE(itmp2)

    RETURN
  END SUBROUTINE radarobs_hdf_read_file

  !#####################################################################

  SUBROUTINE radarobs_net_read_size(filename,stn,isource,typelev,       &
                                    latrad,lonrad,elvrad,dualpol,       &
                                    numradcol,nummaxlev,                &
                                    outi,outj,istatus)
    IMPLICIT NONE

    CHARACTER(LEN=256), INTENT(IN)  :: filename
    CHARACTER(LEN=4),   INTENT(OUT) :: stn
    INTEGER,            INTENT(OUT) :: isource, typelev,dualpol
    REAL,               INTENT(OUT) :: latrad,lonrad,elvrad
    INTEGER,            INTENT(OUT) :: numradcol,nummaxlev
    INTEGER,ALLOCATABLE,INTENT(OUT) :: outi(:),outj(:)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER  :: ncid
    REAL(SP) :: latrad_in,lonrad_in,elvrad_in

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    WRITE(6,'(/1x,a,a)') 'get_radarobs_size: Opening radar file - ',trim(filename)

    CALL netopen(trim(filename), 'R', ncid)
    IF (ncid < 0) THEN
      WRITE (6,'(1x,3a)') 'get_radarobs_size: ERROR opening net file:', &
                trim(filename),' for reading.'
      istatus = ncid
      RETURN
    END IF


    CALL netreadattstr(ncid, 'radid',   stn,     istatus)
    CALL netreadatti(  ncid, 'isource', isource, istatus)
    CALL netreadatti(  ncid, 'dualpol', dualpol, istatus)

    CALL netreadattr(ncid, 'latrad', latrad_in, istatus)
    CALL netreadattr(ncid, 'lonrad', lonrad_in, istatus)
    CALL netreadattr(ncid, 'elvrad', elvrad_in, istatus)
    latrad = latrad_in
    lonrad = lonrad_in
    elvrad = elvrad_in

    CALL netreaddim(ncid, 'numradcol', numradcol, istatus)
    CALL netreaddim(ncid, 'nummaxelv', nummaxlev, istatus)

    CALL netreadatti( ncid, 'typelev', typelev, istatus)
    IF (istatus /= 0) THEN
      typelev = 0
      istatus = 0
    END IF

    ALLOCATE(outi(numradcol), STAT = istatus)
    CALL check_alloc_status(istatus, "radarobs_hdf_read_size:outi")

    ALLOCATE(outj(numradcol), STAT = istatus)
    CALL check_alloc_status(istatus, "radarobs_hdf_read_size:outj")

    CALL netread1di(ncid,0,0,'radcoli',numradcol,outi)
    CALL netread1di(ncid,0,0,'radcolj',numradcol,outj)

    CALL netclose(ncid)

    RETURN
  END SUBROUTINE radarobs_net_read_size

  !#####################################################################

  SUBROUTINE radarobs_net_read_file(filename,numradcol,nummaxlev,verbose, &
                strhopt,dx,dy,dz,dzmin,                                 &
                mapproj,ctrlat,ctrlon,trulat1,trulat2,trulon,scale,     &
                stn,irngmin,irngmax,refelvmin,refelvmax,                &
                grdtlt,inelvang,                                        &
                coli,colj,colk,nlevls,collat,collon,                    &
                radcolhgt,radcolref,radcolvel,radcolnyq,radcoltim,      &
                radcolrhv,radcolzdr,radcolkdp,                          &
                istatus)

    USE model_precision

    IMPLICIT NONE

    CHARACTER(LEN=256), INTENT(IN)  :: filename
    INTEGER,            INTENT(IN)  :: numradcol, nummaxlev
    LOGICAL,            INTENT(IN)  :: verbose

    CHARACTER(LEN=4),   INTENT(OUT) :: stn
    INTEGER,            INTENT(OUT) :: strhopt,mapproj
    REAL,               INTENT(OUT) :: dx,dy,dz,dzmin
    REAL,               INTENT(OUT) :: ctrlat,ctrlon,trulat1,trulat2,trulon
    REAL,               INTENT(OUT) :: scale
    INTEGER,            INTENT(OUT) :: irngmin,irngmax
    REAL,               INTENT(OUT) :: refelvmin,refelvmax

    LOGICAL, INTENT(IN)  :: grdtlt
    REAL,    INTENT(OUT) :: inelvang(:)

    REAL,    INTENT(OUT) :: collat(:)
    REAL,    INTENT(OUT) :: collon(:)
    INTEGER, INTENT(OUT) :: nlevls(:)

    INTEGER, INTENT(OUT) :: coli(:)
    INTEGER, INTENT(OUT) :: colj(:)

    INTEGER, INTENT(OUT) :: colk(:,:)

    REAL,    INTENT(OUT) :: radcolhgt(:,:)
    REAL,    INTENT(OUT) :: radcolref(:,:)
    REAL,    INTENT(OUT) :: radcolvel(:,:)
    REAL,    INTENT(OUT) :: radcolnyq(:,:)
    REAL,    INTENT(OUT) :: radcoltim(:,:)

    REAL,    INTENT(OUT) :: radcolrhv(:,:)
    REAL,    INTENT(OUT) :: radcolzdr(:,:)
    REAL,    INTENT(OUT) :: radcolkdp(:,:)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: ncid

    REAL(SP) :: dx_in
    REAL(SP) :: dy_in
    REAL(SP) :: ctrlat_in
    REAL(SP) :: ctrlon_in
    REAL(SP) :: trulat1_in
    REAL(SP) :: trulat2_in
    REAL(SP) :: trulon_in
    REAL(SP) :: scale_in
    REAL(SP) :: latrad_in
    REAL(SP) :: lonrad_in
    REAL(SP) :: elvrad_in
    REAL(SP) :: refelvmin_in
    REAL(SP) :: refelvmax_in

  !---------------------------------------------------------------------

    INTEGER, PARAMETER  :: mxradvr=10
    INTEGER :: iradvr(mxradvr)

    INTEGER :: ireftim, itime, vcpnum, dualpol,isource,idummy
    CHARACTER (LEN=80)  :: runname
    INTEGER :: iradfmt,nradvr,typelev,numcol,numlev
    REAL    :: latrad,lonrad,elvrad

    INTEGER :: iyr, imon, idy, ihr, imin, isec

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (grdtlt) THEN
      WRITE(*,'(1x,a)') 'ERROR: netCDF format is still not added for grid and tilt format.'
      istatus = -1
      RETURN
    END IF

    WRITE(6,'(/1x,a,a)') 'radarobs_net_read_file: Opening radar file - ',trim(filename)

    CALL netopen(trim(filename), 'R', ncid)
    IF (ncid < 0) THEN
      WRITE (6,'(1x,3a)') 'radarobs_net_read_file: ERROR opening net file:',      &
                trim(filename),' for reading.'
      istatus = ncid
      RETURN
    END IF

    CALL netreadattstr(ncid, 'radid',   stn,     istatus)
    CALL netreadatti(  ncid, 'itime',   itime,   istatus)
    CALL netreadatti(  ncid, 'vcpnum',  vcpnum,  istatus)
    CALL netreadatti(  ncid, 'isource', isource, istatus)
    CALL netreadatti(  ncid, 'dualpol', dualpol, istatus)
!
!-----------------------------------------------------------------------
!
!    Read more header data
!
!-----------------------------------------------------------------------
!
    CALL netreadattstr(ncid, 'runname', runname,  istatus)

    CALL netreadatti(  ncid, 'iradfmt', iradfmt,  istatus)
    CALL netreadatti(  ncid, 'mapproj', mapproj,  istatus)
    CALL netreadatti(  ncid, 'irngmin', irngmin,  istatus)
    CALL netreadatti(  ncid, 'irngmax', irngmax,  istatus)

    !CALL hdfrdi(sd_id, 'strhopt', strhopt, istatus)

    CALL netreadattr(ncid, 'dx',        dx_in,        istatus)
    CALL netreadattr(ncid, 'dy',        dy_in,        istatus)
    CALL netreadattr(ncid, 'ctrlat',    ctrlat_in,    istatus)
    CALL netreadattr(ncid, 'ctrlon',    ctrlon_in,    istatus)
    CALL netreadattr(ncid, 'trulat1',   trulat1_in,   istatus)
    CALL netreadattr(ncid, 'trulat2',   trulat2_in,   istatus)
    CALL netreadattr(ncid, 'trulon',    trulon_in,    istatus)
    CALL netreadattr(ncid, 'sclfct',    scale_in,     istatus)
    CALL netreadattr(ncid, 'latrad',    latrad_in,    istatus)
    CALL netreadattr(ncid, 'lonrad',    lonrad_in,    istatus)
    CALL netreadattr(ncid, 'elvrad',    elvrad_in,    istatus)
    CALL netreadattr(ncid, 'refelvmin', refelvmin_in, istatus)
    CALL netreadattr(ncid, 'refelvmax', refelvmax_in, istatus)

    dx         = dx_in
    dy         = dy_in
    ctrlat     = ctrlat_in
    ctrlon     = ctrlon_in
    trulat1    = trulat1_in
    trulat2    = trulat2_in
    trulon     = trulon_in
    scale      = scale_in
    latrad     = latrad_in
    lonrad     = lonrad_in
    elvrad     = elvrad_in
    refelvmin  = refelvmin_in
    refelvmax  = refelvmax_in

    !CALL hdfrdr(sd_id, 'dz', dz, istatus)
    !CALL hdfrdr(sd_id, 'dzmin', dzmin, istatus)

    !CALL hdfrdi(sd_id, 'nradvr', nradvr, istatus)
    !CALL hdfrd1di(sd_id,'iradvr', mxradvr,iradvr,istatus)

    !CALL hdfrdi(sd_id, 'numradcol', numcol, istatus)
    !CALL hdfrdi(sd_id, 'nummaxelv', numlev, istatus)

    typelev = 0
    CALL netreadatti(  ncid, 'typelev', typelev,  istatus)

    CALL netreaddim(ncid, 'numradcol', numcol, istatus)
    CALL netreaddim(ncid, 'nummaxelv', numlev, istatus)

    IF (numcol /= numradcol .OR. numlev /= nummaxlev) THEN
      WRITE(*,'(1x,3a,/,2(8x,2(a,I5),/))')                              &
         'ERROR: Numbers in the file "',TRIM(filename),                 &
         '" are not consistent with expectation:',                      &
         'Expected: numradcol = ',numradcol,', nummaxlev = ',nummaxlev, &
         'In file:  numradcol = ',numcol,   ', nummaxlev = ',numlev
      istatus = -2
      RETURN
    END IF

    IF (verbose ) THEN
      WRITE(6,'(1x,2a)')      'runname: ',runname
      WRITE(6,'(1x,2(a,I2))') 'iradfmt: ',iradfmt,', mapprin: ',mapproj
      WRITE(6,'(1x,a,3F8.0)') 'dxin,dyin: ',dx,dy
      WRITE(6,'(1x,a,2F8.2)') 'ctrlatin,ctrlonin: ',ctrlat,ctrlon
      WRITE(6,'(1x,a,3F8.2)') 'tlat1in,tlat2in,tlonin: ',               &
                               trulat1,trulat2,trulon
      WRITE(6,'(1x,a,F8.0)')  'scalin: ',scale
      WRITE(6,'(1x,a,3F8.2)') 'latrad,lonrad,elvrad: ',                 &
                               latrad,lonrad,elvrad
      !WRITE(6,'(1x,a,20I4)')  'Got nradvr,iradvr: ',nradvr,iradvr
      WRITE(6,'(1x,a,i6)') ' Got dualpol: ',dualpol

      CALL abss2ctim(itime, iyr, imon, idy, ihr, imin, isec )
      iyr=MOD(iyr,100)
      WRITE(6,'(/a,i2.2,a,i2.2,a,i2.2,1X,i2.2,a,i2.2,a)')               &
          ' Reading remapped raw radar data for: ',                     &
          imon,'-',idy,'-',iyr,ihr,':',imin,' UTC'
    END IF

    CALL netread1d (ncid,0,0,'radcollat',numradcol,collat)
    CALL netread1d (ncid,0,0,'radcollon',numradcol,collon)
    CALL netread1di(ncid,0,0,'numelev',  numradcol,nlevls)

    CALL netread1di(ncid,0,0,'radcoli',  numradcol,coli)
    CALL netread1di(ncid,0,0,'radcolj',  numradcol,colj)

    CALL netread2di(ncid,0,0,'radcolk',  nummaxlev,numradcol,colk)

    CALL netread2d(ncid,0,0,'radcolhgt', nummaxlev,numradcol,radcolhgt)
    CALL netread2d(ncid,0,0,'radcolref', nummaxlev,numradcol,radcolref)
    CALL netread2d(ncid,0,0,'radcolvel', nummaxlev,numradcol,radcolvel)
    CALL netread2d(ncid,0,0,'radcolnyq', nummaxlev,numradcol,radcolnyq)
    CALL netread2d(ncid,0,0,'radcoltim', nummaxlev,numradcol,radcoltim)

    IF(dualpol > 0) THEN
      CALL netread2d(ncid,0,0,'radcolrhv',nummaxlev,numradcol,radcolrhv)
      CALL netread2d(ncid,0,0,'radcolzdr',nummaxlev,numradcol,radcolzdr)
      CALL netread2d(ncid,0,0,'radcolkdp',nummaxlev,numradcol,radcolkdp)
    END IF

    CALL netclose(ncid)

    RETURN
  END SUBROUTINE radarobs_net_read_file

  !#####################################################################

  SUBROUTINE radarobs_bin_read_size(filename,stn,isource,typelev,       &
                                    latrad,lonrad,elvrad,dualpol,       &
                                    numradcol,nummaxlev,                &
                                    outi,outj,istatus)
    IMPLICIT NONE

    CHARACTER(LEN=256), INTENT(IN)  :: filename
    CHARACTER(LEN=4),   INTENT(OUT) :: stn
    INTEGER,            INTENT(OUT) :: isource, typelev,dualpol
    REAL,               INTENT(OUT) :: latrad,lonrad,elvrad
    INTEGER,            INTENT(OUT) :: numradcol,nummaxlev
    INTEGER,ALLOCATABLE,INTENT(OUT) :: outi(:),outj(:)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER, PARAMETER  :: mxradvr=10
    INTEGER :: iradvr(mxradvr)

    INTEGER :: nchanl, jcol, kcol
    INTEGER :: ireftim, itime, vcpnum, idummy
    INTEGER :: i,j,klev
    REAL    :: xrd,yrd, elev,latcin,loncin

    CHARACTER (LEN=80)  :: runname
    INTEGER :: iradfmt, nradvr
    INTEGER :: strhoptin,mapprin,irngmin,irngmax,refelvmin,refelvmax
    REAL    :: dxin,dyin,dzin,dzminin
    REAL    :: ctrlatin,ctrlonin,tlat1in,tlat2in,tlonin,scalin

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    WRITE(6,'(/1x,a,a)') 'radarobs_bin_read_size: Opening radar file - ',trim(filename)

    CALL getunit( nchanl )
    OPEN(UNIT=nchanl,FILE=trim(filename),ERR=400,                       &
                     FORM='unformatted',STATUS='old')

    READ(nchanl) stn
    READ(nchanl) ireftim, itime,vcpnum,isource,dualpol,                 &
                 idummy, idummy,idummy, idummy,idummy

    READ(nchanl) runname
    READ(nchanl) iradfmt,strhoptin,mapprin,irngmin,irngmax,             &
                 typelev,numradcol,nummaxlev,idummy,idummy
    READ(nchanl) dxin,dyin,dzin,dzminin,ctrlatin,                       &
                 ctrlonin,tlat1in,tlat2in,tlonin,scalin,                &
                 latrad,lonrad,elvrad,refelvmin,refelvmax
    READ(nchanl) nradvr,iradvr

    ALLOCATE(outi(numradcol), STAT = istatus)
    CALL check_alloc_status(istatus, "radarobs_bin_read_size:outi")

    ALLOCATE(outj(numradcol), STAT = istatus)
    CALL check_alloc_status(istatus, "radarobs_bin_read_size:outj")

    kcol  = 0
    DO jcol=1,numradcol
      READ(nchanl,END=201) i,j,xrd,yrd,latcin,loncin,elev,klev

      kcol = kcol + 1
      outi(kcol) = i
      outj(kcol) = j

      READ(nchanl,END=202)
      READ(nchanl,END=202)
      READ(nchanl,END=202)
      READ(nchanl,END=202)
      READ(nchanl,END=202)
      READ(nchanl,END=202)
      IF(dualpol > 0) THEN
        READ(nchanl,END=201)
        READ(nchanl,END=201)
        READ(nchanl,END=201)
      END IF
    END DO

    201   CONTINUE
    WRITE(6,'(a,i6,a)') ' End of file reached after reading',kcol,' columns'
    GO TO 200

    202   CONTINUE
    WRITE(6,'(a,i6,a)') ' End of file reached while reading', kcol,' column'

    200   CONTINUE

    CLOSE(nchanl)
    CALL retunit( nchanl )

    RETURN

    400 CONTINUE
    WRITE(6,'(a,a,/a)') '   Error opening radar file ',trim(filename),  &
                        ' Stopping in rdradcol.'
    !CALL arpsstop("Error opening file",1)
    istatus = -999

    RETURN
  END SUBROUTINE radarobs_bin_read_size

  !#####################################################################

  SUBROUTINE radarobs_bin_read_file(filename,numradcol,nummaxlev,verbose, &
                strhopt,dx,dy,dz,dzmin,                                 &
                mapproj,ctrlat,ctrlon,trulat1,trulat2,trulon,scale,     &
                stn,irngmin,irngmax,refelvmin,refelvmax,                &
                grdtlt,inelvang,                                        &
                coli,colj,colk,nlevls,collat,collon,                    &
                radcolhgt,radcolref,radcolvel,radcolnyq,radcoltim,      &
                radcolrhv,radcolzdr,radcolkdp,                          &
                istatus)

    IMPLICIT NONE

    CHARACTER(LEN=256), INTENT(IN)  :: filename
    INTEGER,            INTENT(IN)  :: numradcol, nummaxlev
    LOGICAL,            INTENT(IN)  :: verbose

    CHARACTER(LEN=4),   INTENT(OUT) :: stn
    INTEGER,            INTENT(OUT) :: strhopt,mapproj
    REAL,               INTENT(OUT) :: dx,dy,dz,dzmin
    REAL,               INTENT(OUT) :: ctrlat,ctrlon,trulat1,trulat2,trulon
    REAL,               INTENT(OUT) :: scale
    INTEGER,            INTENT(OUT) :: irngmin,irngmax
    REAL,               INTENT(OUT) :: refelvmin,refelvmax

    LOGICAL, INTENT(IN)  :: grdtlt
    REAL,    INTENT(OUT) :: inelvang(:)

    REAL,    INTENT(OUT) :: collat(:)
    REAL,    INTENT(OUT) :: collon(:)
    INTEGER, INTENT(OUT) :: nlevls(:)

    INTEGER, INTENT(OUT) :: coli(:)
    INTEGER, INTENT(OUT) :: colj(:)

    INTEGER, INTENT(OUT) :: colk(:,:)

    REAL,    INTENT(OUT) :: radcolhgt(:,:)
    REAL,    INTENT(OUT) :: radcolref(:,:)
    REAL,    INTENT(OUT) :: radcolvel(:,:)
    REAL,    INTENT(OUT) :: radcolnyq(:,:)
    REAL,    INTENT(OUT) :: radcoltim(:,:)

    REAL,    INTENT(OUT) :: radcolrhv(:,:)
    REAL,    INTENT(OUT) :: radcolzdr(:,:)
    REAL,    INTENT(OUT) :: radcolkdp(:,:)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: nchanl, jcol, kcol
    INTEGER :: i,j,klev,kk
    REAL    :: xrd,yrd, elev,latcin,loncin

  !---------------------------------------------------------------------

    INTEGER, PARAMETER  :: mxradvr=10
    INTEGER :: iradvr(mxradvr)

    INTEGER :: ireftim, itime, vcpnum, dualpol,isource,idummy
    CHARACTER (LEN=80)  :: runname
    INTEGER :: iradfmt,nradvr,typelev,numcol,numlev
    REAL    :: latrad,lonrad,elvrad

    INTEGER :: iyr, imon, idy, ihr, imin, isec

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    WRITE(6,'(/1x,a,a)') 'radarobs_bin_read_file: Opening radar file - ',trim(filename)

    CALL getunit( nchanl )
    OPEN(UNIT=nchanl,FILE=trim(filename),ERR=400,                       &
                     FORM='unformatted',STATUS='old')

    READ(nchanl) stn
    READ(nchanl) ireftim, itime,vcpnum,isource,dualpol,                 &
                 idummy, idummy,idummy, idummy,idummy

    READ(nchanl) runname
    READ(nchanl) iradfmt,strhopt,mapproj,irngmin,irngmax,               &
                 typelev,numcol,numlev,idummy,idummy
    READ(nchanl) dx,dy,dz,dzmin,ctrlat,                                 &
                 ctrlon,trulat1,trulat2,trulon,scale,                   &
                 latrad,lonrad,elvrad,refelvmin,refelvmax
    READ(nchanl) nradvr,iradvr

    IF (grdtlt .AND. typelev == 2) THEN  ! grid-tilted radar data format
      READ(nchanl) !xmin, xmax, ymin, ymax
      READ(nchanl) !iyr,imon,idy,ihr,imin,isec
      READ(nchanl) !radarx, radary, dazim
      READ(nchanl) inelvang
      READ(nchanl) !tltTime
    END IF

    kcol  = 0
    DO jcol=1,numradcol
      READ(nchanl,END=201) i,j,xrd,yrd,latcin,loncin,elev,klev

      kcol = kcol + 1
      coli(kcol) = i
      colj(kcol) = j

      collat(kcol)  = latcin
      collon(kcol)  = loncin
      nlevls(kcol)  = klev

      IF (grdtlt) THEN
         READ(nchanl,END=202) (radcolhgt(kk,kcol),kk=1,klev)
         READ(nchanl,END=202) !(radcolrng(kk,kcol),kk=1,klev)
         READ(nchanl,END=202) (radcolvel(kk,kcol),kk=1,klev)
         READ(nchanl,END=202) (radcolref(kk,kcol),kk=1,klev)
      ELSE
         READ(nchanl,END=202)      (colk(kk,kcol),kk=1,klev)
         READ(nchanl,END=202) (radcolhgt(kk,kcol),kk=1,klev)
         READ(nchanl,END=202) (radcolref(kk,kcol),kk=1,klev)
         READ(nchanl,END=202) (radcolvel(kk,kcol),kk=1,klev)
         READ(nchanl,END=202) (radcolnyq(kk,kcol),kk=1,klev)
         READ(nchanl,END=202) (radcoltim(kk,kcol),kk=1,klev)
         IF(dualpol > 0) THEN
           READ(nchanl,END=201) (radcolrhv(kk,kcol),kk=1,klev)
           READ(nchanl,END=201) (radcolzdr(kk,kcol),kk=1,klev)
           READ(nchanl,END=201) (radcolkdp(kk,kcol),kk=1,klev)
         END IF
      END IF
    END DO

    IF (numcol /= numradcol .OR. numlev /= nummaxlev) THEN
      WRITE(*,'(1x,3a,/,2(8x,2(a,I5),/))')                              &
         'ERROR: Numbers in the file "',TRIM(filename),                 &
         '" are not consistent with expectation:',                      &
         'Expected: numradcol = ',numradcol,', nummaxlev = ',nummaxlev, &
         'In file:  numradcol = ',numcol,   ', nummaxlev = ',numlev
      istatus = -2
      RETURN
    END IF

    IF (verbose ) THEN
      WRITE(6,'(1x,2a)')      'runname: ',runname
      WRITE(6,'(1x,3(a,I2))') 'iradfmt: ',iradfmt,                      &
                    ', strhoptin: ',strhopt,', mapprin: ',mapproj
      WRITE(6,'(1x,a,3F8.0)') 'dxin,dyin,dzin: ',dx,dy,dz
      WRITE(6,'(1x,a,2F8.2)') 'ctrlatin,ctrlonin: ',ctrlat,ctrlon
      WRITE(6,'(1x,a,3F8.2)') 'tlat1in,tlat2in,tlonin: ',               &
                               trulat1,trulat2,trulon
      WRITE(6,'(1x,a,F8.0)')  'scalin: ',scale
      WRITE(6,'(1x,a,3F8.2)') 'latrad,lonrad,elvrad: ',                 &
                               latrad,lonrad,elvrad
      WRITE(6,'(1x,a,20I4)')  'Got nradvr,iradvr: ',nradvr,iradvr
      WRITE(6,'(1x,a,i6)') ' Got dualpol: ',dualpol

      CALL abss2ctim(itime, iyr, imon, idy, ihr, imin, isec )
      iyr=MOD(iyr,100)
      WRITE(6,'(/a,i2.2,a,i2.2,a,i2.2,1X,i2.2,a,i2.2,a)')               &
          ' Reading remapped raw radar data for: ',                     &
          imon,'-',idy,'-',iyr,ihr,':',imin,' UTC'
    END IF

    201   CONTINUE
    WRITE(6,'(a,i6,a)') ' End of file reached after reading ',kcol,' columns'

    GO TO 200

    202   CONTINUE
    WRITE(6,'(a,i6,a)') ' End of file reached while reading ',kcol,' column'

    200   CONTINUE

    CLOSE(nchanl)
    CALL retunit( nchanl )

    RETURN

    400 CONTINUE
    WRITE(6,'(a,a,/a)') '   Error opening radar file ',trim(filename),  &
                        ' Stopping in rdradcol.'
    !CALL arpsstop("Error opening file",1)
    istatus = -999

    RETURN
  END SUBROUTINE radarobs_bin_read_file

END MODULE module_radarobs
