!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE tlm_totforce               ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!
 SUBROUTINE tlm_totforce(nx,ny,nz,                                      &
           u,v,w,wcont,ptprt,pprt,qv,qscalar,tke,                       &
           ubar,vbar,ptbar,pbar,rhostr,qvbar,                           &
           nu,nv,nw,nwcont,nptprt,npprt,                                &
           nqv,nqscalar,nustr,nvstr,nwstr,                              &
           usflx,vsflx, x,y,z,zp, j1,j2,j3, sinlat,stabchk,             &
           uforce,vforce,wforce,km,lendel,defsq,                        &
           ubk, vbk, wbk,                                               &
           tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,                &
           tem10,tem11,tem12)
  IMPLICIT NONE        ! Force explicit declarations

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'phycst.inc'
  INCLUDE 'globcst.inc'
  INCLUDE 'bndry.inc'
  INCLUDE 'grid.inc'
!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!

  INTEGER :: nt           ! The no. of t-levels of t-dependent arrays.
  INTEGER :: tpast        ! Index of time level for the past time.
  INTEGER :: tpresent     ! Index of time level for the present time.
  INTEGER :: tfuture      ! Index of time level for the future time.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity (m/s)
  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)
  REAL :: ptprt (nx,ny,nz,nt)  ! Perturbation potential temperature (K)
  REAL :: pprt  (nx,ny,nz,nt)  ! Perturbation pressure (Pascal)

  REAL :: qv    (nx,ny,nz,nt)  ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq,nt)  ! Cloud water mixing ratio (kg/kg)
  REAL :: tke   (nx,ny,nz,nt)  ! Turbulent Kinetic Energy ((m/s)**2)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific humidity
                               ! (kg/kg)
  REAL :: nwcont(nx,ny,nz)    ! Contravariant vertical
                               ! velocity (m/s) of the basic states
  REAL :: nustr (nx,ny,nz)    ! nu * rhostr
  REAL :: nvstr (nx,ny,nz)    ! nv * rhostr
  REAL :: nwstr (nx,ny,nz)    ! nw * rhostr

  REAL :: ubk   (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbk   (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: wbk   (nx,ny,nz)     ! Base state potential temperature (K)

  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.

  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! d( zp )/d( z ).

  REAL :: sinlat(nx,ny)        ! Sin of latitude at each grid point

  REAL :: stabchk(nx,ny)
  REAL :: usflx (nx,ny)        ! Surface flux of u-momentum (kg/(m*s**2))
  REAL :: vsflx (nx,ny)        ! Surface flux of v-momentum (kg/(m*s**2))

  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient for
                               ! momentum. ( m**2/s )
  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)
  REAL :: defsq (nx,ny,nz)     ! Deformation squared (1/s**2)

  REAL :: uforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in u-momentum equation (kg/(m*s)**2)
  REAL :: vforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in v-momentum equation (kg/(m*s)**2)
  REAL :: wforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in w-momentum equation (kg/(m*s)**2)

!-----------------------------------------------------------------------
!
!  Basic State Variable Declarations:
!
!-----------------------------------------------------------------------
!

  REAL :: nu     (nx,ny,nz)  ! Total u-velocity (m/s)
  REAL :: nv     (nx,ny,nz)  ! Total v-velocity (m/s)
  REAL :: nw     (nx,ny,nz)  ! Total w-velocity (m/s)
  REAL :: nptprt (nx,ny,nz)  ! Perturbation potential temperature
  REAL :: npprt  (nx,ny,nz)  ! Perturbation pressure
  REAL :: nqv    (nx,ny,nz)  ! Water vapor specific humidity (kg/kg)

  REAL :: nqscalar(nx,ny,nz,nscalarq)

!
!-----------------------------------------------------------------------
!
!  Working arrays
!
!-----------------------------------------------------------------------
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
  REAL :: tem4  (nx,ny,nz)     ! Temporary work array
  REAL :: tem5  (nx,ny,nz)     ! Temporary work array
  REAL :: tem6  (nx,ny,nz)     ! Temporary work array
  REAL :: tem7  (nx,ny,nz)     ! Temporary work array
  REAL :: tem8  (nx,ny,nz)     ! Temporary work array
  REAL :: tem9  (nx,ny,nz)     ! Temporary work array
  REAL :: tem10 (nx,ny,nz)     ! Temporary work array
  REAL :: tem11 (nx,ny,nz)     ! Temporary work array
  REAL :: tem12 (nx,ny,nz)     ! Temporary work array
!
!
!-----------------------------------------------------------------------
!
!  Temporary arrays
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: bem1  (:,:,:)     ! Temporary work array.
  REAL, ALLOCATABLE :: bem2  (:,:,:)     ! Temporary work array.
  REAL, ALLOCATABLE :: bem3  (:,:,:)     ! Temporary work array.
  REAL, ALLOCATABLE :: bem4  (:,:,:)     ! Temporary work array.
  REAL, ALLOCATABLE :: bem5  (:,:,:)     ! Temporary work array.
  REAL, ALLOCATABLE :: bem6  (:,:,:)     ! Temporary work array.
  REAL, ALLOCATABLE :: bem7  (:,:,:)     ! Temporary work array.
  REAL, ALLOCATABLE :: bem8  (:,:,:)     ! Temporary work array.
  REAL, ALLOCATABLE :: bem9  (:,:,:)     ! Temporary work array.

  REAL :: sum1,sum2,sum3,delet
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  REAL :: dtbig1               ! Local value of big time step size
  REAL :: dtsml1               ! Local value of small time step size
  INTEGER :: i,j,k,n
  INTEGER :: qflag             ! Indicator for the water/ice type
                               ! when calling SOLVQ.
  INTEGER :: nstepss,nbasic

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!-----------------------------------------------------------------------
!
!  To initialize the 3-time level scheme, we assume for
!  the first step that the values of all variables at time
!  past equal the values at time present.  We then perform a
!  forward-in-time integration for the first step only and
!  then, using the same algorithm, we perform a centered-in-time
!  integration for all subsequent steps. The size of the
!  timestep is adjusted accordingly, such that the leapfrog step
!  size is twice that of the first forward-in-time step.
!
!-----------------------------------------------------------------------
!
  allocate ( bem1(nx,ny,nz) )   ! Temporary work array.
  allocate ( bem2(nx,ny,nz) )   ! Temporary work array.
  allocate ( bem3(nx,ny,nz) )   ! Temporary work array.
  allocate ( bem4(nx,ny,nz) )   ! Temporary work array.
  allocate ( bem5(nx,ny,nz) )   ! Temporary work array.
  allocate ( bem6(nx,ny,nz) )   ! Temporary work array.
  allocate ( bem7(nx,ny,nz) )   ! Temporary work array.
  allocate ( bem8(nx,ny,nz) )   ! Temporary work array.
  allocate ( bem9(nx,ny,nz) )   ! Temporary work array.
!
!
    nstepss= 1
    nbasic = 1
    dtbig1 = dtbig
    dtsml1 = dtsml
!
!-----------------------------------------------------------------------
!
!  Calculate wcont at time tpresent. wcont will be used in FRCUVW
!  and FRCP. rhostr averaged to u, v, w points. They are stored
!  in tem1, tem2 and tem3.
!
  CALL rhouvw0(nx,ny,nz,rhostr,tem1,tem2,tem3)
    tem4 = 0.
    tem5 = 0.
    wcont=0.
   nwcont=0.
! CALL wcontra0(nx,ny,nz,                                                &
!            nu(1,1,1,1),nv(1,1,1,1),                                    &
!            nw(1,1,1,1),j1,j2,j3,                                       &
!            rhostr,tem1,tem2,tem3,nwcont,tem4,tem5)
  CALL wcontra0(nx,ny,nz,ubk,vbk,wbk,j1,j2,j3,                           &
             rhostr,tem1,tem2,tem3,nwcont,tem4,tem5)
!
!-----------------------------------------------------------------------
!
!  rhouvw0 calculate the averaged rhostr at the u, v, and w points.
!  It is linear!!!
!  Inputs: nx,ny,nz,rhostr; Outputs: tem1,tem2,tem3.
!
  IF (1 == 0) THEN !  to verify the adjoint of wcont ,need two modifications
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          bem1(i,j,k)=    u(i,j,k,tpresent)
          bem2(i,j,k)=    v(i,j,k,tpresent)
          bem3(i,j,k)=    w(i,j,k,tpresent)
        END DO
      END DO
    END DO
  END IF
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!-----turn off----Nov 9th 2016-----
! tem1=0.; tem2=0; tem3=0
! CALL rhouvw0(nx,ny,nz,rhostr,tem1,tem2,tem3)
!   tem4 = 0.
!   tem5 = 0.
! CALL   wcontra0(nx,ny,nz,                                              &
!              u(1,1,1,tpresent),v(1,1,1,tpresent),                      &
!              w(1,1,1,tpresent),j1,j2,j3,                               &
!              rhostr,tem1,tem2,tem3,wcont,tem4,tem5)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!   CALL product(wcont,wcont,                                           &
!                nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
!   sum1=sum1+sum2
!   print*,'wcont in totforce===',sum2
!
!-----------------------------------------------------------------------
!
!  Code for verification of the adjoint purposes
!
!-----------------------------------------------------------------------
  IF (1 == 0) THEN ! to verify the adjoint fo wcont, need two modifications

    sum1=0.
    CALL product(wcont,wcont,                                           &
                 nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
    sum1=sum1+sum2

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
              u(i,j,k,tpresent) = 0.0
              v(i,j,k,tpresent) = 0.0
              w(i,j,k,tpresent) = 0.0
        END DO
      END DO
    END DO

   tem1=0.; tem2=0; tem3=0
   CALL rhouvw0(nx,ny,nz,rhostr,tem1,tem2,tem3)
      tem4 = 0.
      tem5 = 0.
   CALL   adwcontra0(nx,ny,nz,                                            &
               u(1,1,1,tpresent),v(1,1,1,tpresent),                      &
               w(1,1,1,tpresent),j1,j2,j3,                               &
               rhostr,tem1,tem2,tem3,wcont,tem4,tem5)

    sum3=0.

    sum2=0.
    CALL product(u(1,1,1,tpresent),bem1,                                &
                 nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
    PRINT*,"u",sum2
    sum3=sum3+sum2

    sum2=0.
    CALL product(v(1,1,1,tpresent),bem2,                                &
                 nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
    PRINT*,"v",sum2
    sum3=sum3+sum2

    sum2=0.
    CALL product(w(1,1,1,tpresent),bem3,                                &
                 nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
    PRINT*,"w",sum2
    sum3=sum3+sum2

    PRINT*, "adwcontra0 here"
    PRINT*,'wcontra0 (AQ)*(AQ)= ',sum1
    PRINT*,'wcontra0  Q*A*(AQ)= ',sum3
    STOP
  END IF
!
!
!-----------------------------------------------------------------------
!
!  Compute the acoustically inactive terms in the momentum and
!  pressure equations that are held fixed during the small time
!  step computations.  This includes advection, buoyancy, mixing
!  (both physical and computational), and the Coriolis terms.
!  These forcing terms are accumulated into arrays for each
!  of the momentum equations, e.g., uforce for the u-equation,
!  vforce for the v-equation, wforce for the w-equation and
!  pforce for the p-equation.
!
!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!
!  Code for verification of the adjoint purposes
!
!-----------------------------------------------------------------------
!
  IF (1==0) THEN !  to verify the adjoint of the uforce,vforce,wforce,need  2 modifications
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          bem1(i,j,k)=    u(i,j,k,tpresent)
          bem2(i,j,k)=    v(i,j,k,tpresent)
          bem3(i,j,k)=    w(i,j,k,tpresent)
          bem4(i,j,k)=ptprt(i,j,k,tpresent)
          bem5(i,j,k)= pprt(i,j,k,tpresent)
          bem6(i,j,k)=wcont(i,j,k)
          bem7(i,j,k)=   qv(i,j,k,tpresent)
!
          uforce(i,j,k)=0.
          vforce(i,j,k)=0.
          wforce(i,j,k)=0.
        END DO
      END DO
    END DO
  END IF !1==0
!
  tem1 = 0.
  tem2 = 0.
  tem3 = 0.
  tem4 = 0.
  tem5 = 0.
  tem6 = 0.
  tem7 = 0.
  tem8 = 0.
  tem9 = 0.
!
  CALL tlmfrcuvw(nx,ny,nz,dtbig1,nstepss,nbasic,                        &
              u(1,1,1,tpast),v(1,1,1,tpast),w(1,1,1,tpast),wcont,       &
              ptprt(1,1,1,tpast),pprt(1,1,1,tpast),qv(1,1,1,tpast),     &
              qscalar(1,1,1,:,tpast),tke(1,1,1,tpast),                  &
              ubar,vbar,ptbar,pbar,rhostr,qvbar,                        &
              nustr,nvstr,nwstr,                                        &
              usflx,vsflx, x,y,z,zp, j1,j2,j3, sinlat,stabchk,          &
              uforce,vforce,wforce, km,lendel,defsq,                    &
              ubk, vbk, wbk,                                            &
              tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,             &
              tem10,tem11,tem12)
!
   tem1 = 0.0
   tem2 = 0.0
   tem3 = 0.0
   tem4 = 0.0
   tem5 = 0.0
!
  CALL tlmpgrad(nx,ny,nz, pprt(1,1,1,tpast),                            &
         j1,j2,j3, tem3,tem4,tem5,tem1,tem2)
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          uforce(i,j,k) = uforce(i,j,k) - tem3(i,j,k)
          vforce(i,j,k) = vforce(i,j,k) - tem4(i,j,k)
          wforce(i,j,k) = wforce(i,j,k) - tem5(i,j,k)
          tem1(i,j,k)= 0.0
          tem2(i,j,k)= 0.0
        END DO
      END DO
    END DO
!
!
!
!-----------------------------------------------------------------------
!
!  Code for verification of the adjoint purposes
!
!-----------------------------------------------------------------------
  IF (1==0) THEN !verify ajoint code, need two modifications

    sum1=0.
    CALL product(uforce,uforce,                                         &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
                 !nx,ny,nz,2,nx-1,1,ny-1,2,nz-2,sum2)
    sum1=sum1+sum2

    CALL product(vforce,vforce,                                         &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
                 !nx,ny,nz,1,nx-1,2,ny-1,2,nz-2,sum2)
    sum1=sum1+sum2

    CALL product(wforce,wforce,                                         &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
                 !nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
    sum1=sum1+sum2

!
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          tem3(i,j,k) = -uforce(i,j,k)
          tem4(i,j,k) = -vforce(i,j,k)
          tem5(i,j,k) = -wforce(i,j,k)

              u(i,j,k,tpast) = 0.0
              v(i,j,k,tpast) = 0.0
              w(i,j,k,tpast) = 0.0
          ptprt(i,j,k,tpast) = 0.0
           pprt(i,j,k,tpast) = 0.0
             qv(i,j,k,tpast) = 0.0
                wcont(i,j,k) = 0.0
        END DO
      END DO
    END DO
!
    tem1 = 0.0
    tem2 = 0.0
!
    CALL adpgrad(nx,ny,nz, pprt(1,1,1,tpast),                           &
                       j1,j2,j3,tem3,tem4,tem5,tem1,tem2)
!
          tem1 = 0.0
          tem2 = 0.0
          tem3 = 0.0
          tem4 = 0.0
          tem5 = 0.0
          tem6 = 0.0
          tem7 = 0.0
          tem8 = 0.0
          tem9 = 0.0
!
    CALL adtlmfrcuvw(nx,ny,nz,dtbig1,nstepss,nbasic,                    &
                u(1,1,1,tpast),v(1,1,1,tpast),w(1,1,1,tpast),wcont,     &
                ptprt(1,1,1,tpast),pprt(1,1,1,tpast),qv(1,1,1,tpast),   &
                qscalar(1,1,1,:,tpast),tke(1,1,1,tpast),                &
                ubar,vbar,ptbar,pbar,rhostr,qvbar,                      &
                nu,nv,nw,nwcont,nptprt,npprt,nqv,nqscalar,              &
                nustr,nvstr,nwstr,                                      &
                usflx,vsflx, x,y,z,zp, j1,j2,j3, sinlat,stabchk,        &
                uforce,vforce,wforce, km,lendel,defsq,                  &
                ubk, vbk, wbk,                                          &
                tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,           &
                tem10,tem11,tem12)
!
!
    sum3=0.

    CALL product(u(1,1,1,tpresent),bem1,                                &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    PRINT*,"upresent",sum2
    sum3=sum3+sum2

    CALL product(v(1,1,1,tpresent),bem2,                                &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    PRINT*,"vpresent",sum2
    sum3=sum3+sum2

    CALL product(w(1,1,1,tpresent),bem3,                                &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    PRINT*,"wpresent",sum2
    sum3=sum3+sum2

    CALL product(wcont,bem6,                                            &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    PRINT*,"wcont=",sum2
    sum3=sum3+sum2
!
!
    CALL product(pprt(1,1,1,tpast),bem5,                                &
               nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    PRINT*,"pprt=",sum2
    sum3=sum3+sum2

    CALL product(qv(1,1,1,tpresent),bem7,                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    PRINT*,"qv===",sum2
    sum3=sum3+sum2

    CALL product(ptprt(1,1,1,tpresent),bem4,                            &
           nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    PRINT*,"ptprt=",sum2
    sum3=sum3+sum2
!
    PRINT*, "adtlmfrcuvw"
    PRINT*,'(AQ)*(AQ)= ',sum1
    PRINT*,' Q*A*(AQ)= ',sum3
    STOP
  END IF
!
!-----------------------------------------------------------------------
!
  IF(1 == 0) THEN ! to verify the adjoint of pgrad, need only 1 modification
    DO i=1,nx
      DO j=1,ny
        DO k=1,nz
          tem9(i,j,k)=pprt(i,j,k,tpast)
          tem3(i,j,k) = 0.0
          tem4(i,j,k) = 0.0
          tem5(i,j,k) = 0.0
          tem1(i,j,k) = 0.0
          tem2(i,j,k) = 0.0
        END DO
      END DO
    END DO

  CALL tlmpgrad(nx,ny,nz,pprt(1,1,1,tpast),j1,j2,j3,                    &
                                     tem3,tem4,tem5,tem1,tem2)
!
    sum1=0.
!
    CALL product(tem3,tem3,nx,ny,nz,2,nx-1,1,ny-1,2,nz-2,sum2)
    sum1=sum1+sum2
!
    CALL product(tem4,tem4,nx,ny,nz,1,nx-1,2,ny-1,2,nz-2,sum2)
    sum1=sum1+sum2
!
    CALL product(tem5,tem5,nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
    sum1=sum1+sum2
!
    DO i=1,nx
      DO j=1,ny
        DO k=1,nz
          pprt(i,j,k,tpast) = 0.0
          tem1(i,j,k) = 0.0
          tem2(i,j,k) = 0.0
        END DO
      END DO
    END DO
!
    CALL adpgrad(nx,ny,nz, pprt(1,1,1,tpast),                           &
                       j1,j2,j3,tem3,tem4,tem5,tem1,tem2)
!
    sum3=0.
!
    CALL product(pprt(1,1,1,tpast),tem9,                                &
                 nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,sum2)
    sum3=sum3+sum2
    PRINT*,'pgrad: sum1, sum3= ',sum1,sum3
    STOP
  END IF ! to verify the adjoint of pgrad
!
!
  deallocate ( bem1 )   ! Temporary work array.
  deallocate ( bem2 )   ! Temporary work array.
  deallocate ( bem3 )   ! Temporary work array.
  deallocate ( bem4 )   ! Temporary work array.
  deallocate ( bem5 )   ! Temporary work array.
  deallocate ( bem6 )   ! Temporary work array.
  deallocate ( bem7 )   ! Temporary work array.
  deallocate ( bem8 )   ! Temporary work array.
  deallocate ( bem9 )   ! Temporary work array.
!
  RETURN
END SUBROUTINE TLM_TOTFORCE
!
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE adtlm_totforce             ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!
 SUBROUTINE adtlm_totforce(nx,ny,nz,                                    &
           u,v,w,wcont,ptprt,pprt,qv,qscalar,tke,                       &
           ubar,vbar,ptbar,pbar,rhostr,qvbar,                           &
           nwcont,                                                      &
           !nu,nv,nw,nwcont,nptprt,npprt,nqv,nqscalar,                  &
           nustr,nvstr,nwstr,                                           &
           usflx,vsflx, x,y,z,zp, j1,j2,j3, sinlat,stabchk,             &
           uforce,vforce,wforce,km,lendel,defsq,                        &
           ubk, vbk, wbk,                                               &
           tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,                &
           tem10,tem11,tem12)

  IMPLICIT NONE        ! Force explicit declarations
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'phycst.inc'
  INCLUDE 'globcst.inc'
  INCLUDE 'bndry.inc'
  INCLUDE 'grid.inc'	    !Added by Ying 10/15/2001
!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  INTEGER :: nt           ! The no. of t-levels of t-dependent arrays.
  INTEGER :: tpast        ! Index of time level for the past time.
  INTEGER :: tpresent     ! Index of time level for the present time.
  INTEGER :: tfuture      ! Index of time level for the future time.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity (m/s)
  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)
  REAL :: ptprt (nx,ny,nz,nt)  ! Perturbation potential temperature (K)
  REAL :: pprt  (nx,ny,nz,nt)  ! Perturbation pressure (Pascal)

  REAL :: qv    (nx,ny,nz,nt)  ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq,nt)  ! Cloud water mixing ratio (kg/kg)
  REAL :: tke   (nx,ny,nz,nt)  ! Turbulent Kinetic Energy ((m/s)**2)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific humidity
                               ! (kg/kg)
  REAL :: nwcont(nx,ny,nz)    ! Contravariant vertical
                               ! velocity (m/s) of the basic states
  REAL :: nustr (nx,ny,nz)    ! nu * rhostr
  REAL :: nvstr (nx,ny,nz)    ! nv * rhostr
  REAL :: nwstr (nx,ny,nz)    ! nw * rhostr

!-----------------------------------------------------------------------
!
!  Basic State Variable Declarations:
!
!-----------------------------------------------------------------------
!

  !REAL :: nu     (nx,ny,nz)  ! Total u-velocity (m/s)
  !REAL :: nv     (nx,ny,nz)  ! Total v-velocity (m/s)
  !REAL :: nw     (nx,ny,nz)  ! Total w-velocity (m/s)
  !REAL :: nptprt (nx,ny,nz)  ! Perturbation potential temperature
  !REAL :: npprt  (nx,ny,nz)  ! Perturbation pressure
  !REAL :: nqv    (nx,ny,nz)  ! Water vapor specific humidity (kg/kg)
  !
  !REAL :: nqscalar(nx,ny,nz,nscalarq)

!-----------------------------------------------------------------------


  REAL,INTENT(IN) :: ubk   (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL,INTENT(IN) :: vbk   (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL,INTENT(IN) :: wbk   (nx,ny,nz)     ! Total w-velocity (m/s)

  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.

  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! d( zp )/d( z ).

  REAL :: sinlat(nx,ny)        ! Sin of latitude at each grid point

  REAL :: stabchk(nx,ny)
  REAL :: usflx (nx,ny)        ! Surface flux of u-momentum (kg/(m*s**2))
  REAL :: vsflx (nx,ny)        ! Surface flux of v-momentum (kg/(m*s**2))

  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient for
                               ! momentum. ( m**2/s )
  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)
  REAL :: defsq (nx,ny,nz)     ! Deformation squared (1/s**2)

  REAL :: uforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in u-momentum equation (kg/(m*s)**2)
  REAL :: vforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in v-momentum equation (kg/(m*s)**2)
  REAL :: wforce(nx,ny,nz)     ! Acoustically inactive forcing terms
                               ! in w-momentum equation (kg/(m*s)**2)

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
  REAL :: tem4  (nx,ny,nz)     ! Temporary work array
  REAL :: tem5  (nx,ny,nz)     ! Temporary work array
  REAL :: tem6  (nx,ny,nz)     ! Temporary work array
  REAL :: tem7  (nx,ny,nz)     ! Temporary work array
  REAL :: tem8  (nx,ny,nz)     ! Temporary work array
  REAL :: tem9  (nx,ny,nz)     ! Temporary work array
  REAL :: tem10 (nx,ny,nz)     ! Temporary work array
  REAL :: tem11 (nx,ny,nz)     ! Temporary work array
  REAL :: tem12 (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  REAL    :: dtbig1               ! Local value of big time step size
  REAL    :: dtsml1               ! Local value of small time step size
  INTEGER :: i,j,k,n
  INTEGER :: qflag             ! Indicator for the water/ice type
                               ! when calling SOLVQ.
  INTEGER :: nstepss,nbasic

  REAL    :: sum1, sum2, sum3

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
    nstepss= 1
    nbasic = 1
    dtbig1 = dtbig
    dtsml1 = dtsml
!
!
!if ( 1==0 ) then
!  sum1 = 0.0
!  sum2 = 0.0
!  sum3 = 0.0
!!
!  do i=1,nx
!  do j=1,ny
!  do k=1,nz
!    sum1 =sum1 + nu(i,j,k,1)*nu(i,j,k,1)
!    sum2 =sum2 + nv(i,j,k,1)*nv(i,j,k,1)
!    sum3 =sum3 + nw(i,j,k,1)*nw(i,j,k,1)
!  end do
!  end do
!  end do
!!
! print*,'sum1==dddddd====',sum1
! print*,'sum2==dddddd====',sum2
! print*,'sum3==dddddd====',sum3
! stop
!end if
!
!-----------------------------------------------------------------------
!
!  Calculate wcont at time tpresent. wcont will be used in FRCUVW
!  and FRCP. rhostr averaged to u, v, w points. They are stored
!  in tem1, tem2 and tem3.
!
!  rhouvw0 calculate the averaged rhostr at the u, v, and w points.
!  It is linear!!!
!  Inputs: nx,ny,nz,rhostr; Outputs: tem1,tem2,tem3.
!
!-----------------------------------------------------------------------
!
          tem1 = 0.
          tem2 = 0.
          tem3 = 0.
          tem4 = 0.
          tem5 = 0.
          wcont = 0.0
         nwcont = 0.0
!
  CALL rhouvw0(nx,ny,nz,rhostr,tem1,tem2,tem3)
  CALL wcontra0(nx,ny,nz,ubk,vbk,wbk,j1,j2,j3,                           &
             rhostr,tem1,tem2,tem3,nwcont,tem4,tem5)
! CALL wcontra0(nx,ny,nz,                                                &
!            nu(1,1,1,1),nv(1,1,1,1),                                    &
!            nw(1,1,1,1),j1,j2,j3,                                       &
!            rhostr,tem1,tem2,tem3,nwcont,tem4,tem5)
!
!
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tem1(i,j,k) = 0.0
        tem2(i,j,k) = 0.0
        tem3(i,j,k) = -uforce(i,j,k)
        tem4(i,j,k) = -vforce(i,j,k)
        tem5(i,j,k) = -wforce(i,j,k)
      END DO
    END DO
  END DO

  CALL adpgrad(nx,ny,nz, pprt(1,1,1,tpast),                             &
                       j1,j2,j3,tem3,tem4,tem5,tem1,tem2)

  tem1 = 0.
  tem2 = 0.
  tem3 = 0.
  tem4 = 0.
  tem5 = 0.
  tem6 = 0.
  tem7 = 0.
  tem8 = 0.
  tem9 = 0.

  CALL adtlmfrcuvw(nx,ny,nz,dtbig1,nstepss,nbasic,                      &
                u(1,1,1,tpast),v(1,1,1,tpast),w(1,1,1,tpast),wcont,     &
                ptprt(1,1,1,tpast),pprt(1,1,1,tpast),qv(1,1,1,tpast),   &
                qscalar(1,1,1,:,tpast),tke(1,1,1,tpast),                &
                ubar,vbar,ptbar,pbar,rhostr,qvbar,                      &
                nwcont,                                                 &
                !nu,nv,nw,nwcont,nptprt,npprt,nqv,nqscalar,              &
                nustr,nvstr,nwstr,                                      &
                usflx,vsflx, x,y,z,zp, j1,j2,j3, sinlat,stabchk,        &
                uforce,vforce,wforce, km,lendel,defsq,                  &
                ubk, vbk, wbk,                                          &
                tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,           &
                tem10,tem11,tem12)
!
!
          tem1 = 0.
          tem2 = 0.
          tem3 = 0.
          tem4 = 0.
          tem5 = 0.
!
! CALL rhouvw0(nx,ny,nz,rhostr,tem1,tem2,tem3)
!
! CALL adwcontra0(nx,ny,nz,                                              &
!              u(1,1,1,tpresent),v(1,1,1,tpresent),                      &
!              w(1,1,1,tpresent),j1,j2,j3,                               &
!              rhostr,tem1,tem2,tem3,wcont,tem4,tem5)
!
!
  RETURN
END SUBROUTINE ADTLM_TOTFORCE
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADWCONTRA0                 ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adwcontra0(nx,ny,nz,u,v,w,j1,j2,j3,                          &
           rhostr,rhostru,rhostrv,rhostrw,wcont,ustr,vstr)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operations on WCONTRA. WCONTRA
!  calculates wcont, the contravariant vertical velocity (m/s)
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!   Jidong Gao
!  5/20/96 converted to ARPS4.0.22
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x component of velocity at all time levels (m/s)
!    v        y component of velocity at all time levels (m/s)
!    w        Vertical component of Cartesian velocity
!             at all time levels (m/s)
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!    rhostr   j3 times base state density rhobar(kg/m**3).
!    rhostru  Average rhostr at u points (kg/m**3).
!    rhostrv  Average rhostr at v points (kg/m**3).
!    rhostrw  Average rhostr at w points (kg/m**3).
!
!  OUTPUT:
!
!    wcont    Vertical component of contravariant velocity in
!             computational coordinates (m/s)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! The number of grid points in 3 directions
!
  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)

  REAL :: j1    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! d( zp )/d( z ).
!
  REAL :: rhostr(nx,ny,nz)     ! j3 times base state density rhobar(kg/m**3).
  REAL :: rhostru(nx,ny,nz)    ! Average rhostr at u points (kg/m**3).
  REAL :: rhostrv(nx,ny,nz)    ! Average rhostr at v points (kg/m**3).
  REAL :: rhostrw(nx,ny,nz)    ! Average rhostr at w points (kg/m**3).

  REAL :: wcont (nx,ny,nz)     ! Vertical velocity in computational
                               ! coordinates (m/s)

  REAL :: tem1  (nx,ny,nz)     ! temporary work array
  REAL :: tem2  (nx,ny,nz)     ! temporary work array
  REAL :: ustr  (nx,ny,nz)     ! temporary work array
  REAL :: vstr  (nx,ny,nz)     ! temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global constants that control model execution
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  CALL advbcwcont0(nx,ny,nz,wcont) !by gge 2012/07/10, no need to do this in minimization process
!
  IF( crdtrns == 0 ) THEN  ! No coord. transformation case.

    DO k= 2,nz-1
      DO j= 1,ny-1
        DO i= 1,nx-1
          w(i,j,k) = w(i,j,k) + wcont(i,j,k)
          wcont(i,j,k)=0.0
        END DO
      END DO
    END DO

  ELSE IF( ternopt == 0) THEN

    DO k= 2,nz-1
      DO j= 1,ny-1
        DO i= 1,nx-1
          w(i,j,k)=w(i,j,k) + wcont(i,j,k)*2.0/(j3(i,j,k)+j3(i,j,k-1))
          wcont(i,j,k) = 0.0
        END DO
      END DO
    END DO

  ELSE

  ustr = 0.
  vstr = 0.

    DO k= 2,nz-1
      DO j= 1,ny-1
        DO i= 1,nx-1

          ustr(i  ,j,k  )=ustr(i  ,j,k  ) + wcont(i,j,k)*j1(i,j,k)*0.25    &
                        /rhostrw(i,j,k)*2.0/(j3(i,j,k)+j3(i,j,k-1))

          ustr(i  ,j,k-1)=ustr(i  ,j,k-1) + wcont(i,j,k)*j1(i,j,k)*0.25    &
                        /rhostrw(i,j,k)*2.0/(j3(i,j,k)+j3(i,j,k-1))

          ustr(i+1,j,k  )=ustr(i+1,j,k) + wcont(i,j,k)*j1(i+1,j,k)*0.25    &
                        /rhostrw(i,j,k)*2.0/(j3(i,j,k)+j3(i,j,k-1))

          ustr(i+1,j,k-1)=ustr(i+1,j,k-1)+wcont(i,j,k)*j1(i+1,j,k)*0.25    &
                        /rhostrw(i,j,k)*2.0/(j3(i,j,k)+j3(i,j,k-1))
!------------------------------------------------------------------------
          vstr(i  ,j,k  )=vstr(i  ,j,k  )+wcont(i,j,k)*j2(i,j  ,k)*0.25    &
                        /rhostrw(i,j,k)*2.0/(j3(i,j,k)+j3(i,j,k-1))

          vstr(i  ,j,k-1)=vstr(i  ,j,k-1)+wcont(i,j,k)*j2(i,j  ,k)*0.25    &
                        /rhostrw(i,j,k)*2.0/(j3(i,j,k)+j3(i,j,k-1))

          vstr(i,j+1,k  )=vstr(i,j+1,k  )+wcont(i,j,k)*j2(i,j+1,k)*0.25    &
                        /rhostrw(i,j,k)*2.0/(j3(i,j,k)+j3(i,j,k-1))

          vstr(i,j+1,k-1)=vstr(i,j+1,k-1)+wcont(i,j,k)*j2(i,j+1,k)*0.25    &
                        /rhostrw(i,j,k)*2.0/(j3(i,j,k)+j3(i,j,k-1))

          w(i,j,k)=w(i,j,k) + wcont(i,j,k)* 2.0/(j3(i,j,k)+j3(i,j,k-1))
          wcont(i,j,k) = 0.

        END DO
      END DO
    END DO

    DO k= 1,nz-1
      DO j= 1,ny
        DO i= 1,nx-1
          v(i,j,k) = v(i,j,k) + vstr(i,j,k)*rhostrv(i,j,k)
          vstr(i,j,k) = 0.0
        END DO
      END DO
    END DO
!
    DO k= 1,nz-1
      DO j= 1,ny-1
        DO i= 1,nx
          u(i,j,k) = u(i,j,k) + ustr(i,j,k)*rhostru(i,j,k)
          ustr(i,j,k) = 0.0
        END DO
      END DO
    END DO
!
  END IF

  RETURN
END SUBROUTINE adwcontra0
