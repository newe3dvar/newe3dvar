!
!##################################################################
!##################################################################
!######                                                      ######
!######                 SUBROUTINE  ENBGREAD                 ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE enbgread4var(nx,ny,nz,nx_m,ny_m,nz_m,tem1,tem2,tem3,tem4,istatus)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Read in model state of ensemble members
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Mingjing Tong
!    07/24/2006.
!
!  MODIFIED HISTORY:
!    Youngsun Jung      08/04/2007
!         Add modules and polarimetric variables
!    Youngsun Jung   05/10/2008
!         Adopt dynamic array qscalar
!    Youngsun Jung   09/20/2010
!         Add a block of code to deal with split files
!         in each member's own directory.
!
!  03/30/2012 (Y. Wang)
!  Upgraded the HDF I/O with module arps_dtaread for efficiency purpose.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx,ny,nz The dimension of data arrays
!
!-----------------------------------------------------------------------
!
  USE module_varpara
  USE module_ensemble

  USE arps_dtaread
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'          ! Grid & map parameters.

  INCLUDE 'mp.inc'
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  INTEGER, INTENT(IN) :: nx,ny,nz          ! Number of grid points in 3 directions
  INTEGER, INTENT(IN) :: nx_m,ny_m,nz_m
  REAL, DIMENSION(nx,ny,nz), INTENT(INOUT) :: tem1,tem2,tem3,tem4
  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------
  INTEGER :: i,j,k,l,nq
  INTEGER :: nscalarloop
  REAL    :: time
  CHARACTER (LEN=256) :: grdbasfn, filename
  CHARACTER (LEN=256) :: dirfn

  REAL :: n0rain1, n0snow1, n0hail1, rhosnow1,  rhohail1
  REAL :: rdummy

  REAL, ALLOCATABLE :: kmh(:,:,:), kmv(:,:,:)
  REAL, ALLOCATABLE :: tem3d(:,:,:)

  INTEGER :: nensmbl0, imemid, ymemid
  INTEGER :: iabs0, iabs, imem, ld
  INTEGER :: iyr, imon, idy, ihr, isec

  INTEGER :: imax,jmax,kmax, imin,jmin,kmin
  REAL    :: tem3d_max, tem3d_min

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0
  nensmbl0 = nensmbl

  IF (modelopt==1) THEN
    ALLOCATE(kmh(nx,ny,nz), STAT = istatus)
    ALLOCATE(kmv(nx,ny,nz), STAT = istatus)
  END IF

!-----------------------------------------------------------------------

  IF (ensreadopt == 3 ) THEN

    IF (nprocs > nensvirtl .AND. modelopt == 2) THEN

      IF (myproc == 0) WRITE(*,'(1x,2(a,I0),a)') 'Parallel reading of ',nensvirtl,' ensemble members by procs: 1 - ', nensvirtl,'.'

      CALL ens_wrfread(nx_m,ny_m,nz_m,iensmbl,ntimesample,stimesample,nensvirtl,istatus)

      GOTO 7777        ! skip reading below
    ELSE
      IF (myproc == 0) WRITE(*,'(1x,4(a,I0))') 'Parallel reading is skipping because nprocs = ',      &
                 nprocs,', nensmbl0 = ',nensmbl0, ', modelopt = ',modelopt, ', iensmbl = ',iensmbl
      !IF (interpopt == 2) THEN
      !  WRITE(*,*) 'ERROR: interpopt = 2 only works with ensreadopt = 3 when memory is not a concern on the platform.'
      !  CALL arpsstop('ERROR: Wrong combination of interpopt & ensreadopt.',1)
      !ELSE
      !  ensreadopt = 2
      !END IF
      ensreadopt = 2
    END IF
  END IF

  IF (ntimesample /= 1 .AND. modelopt == 1) THEN
    WRITE(*,'(1x,a)') 'WARNING!!!! I do not know how to handle the file name for time samples. Please implement this feature yourself in ensemblio3d.f90.'
    CALL arpsstop('ERROR: not supported feature')
  END IF

  IF (ntimesample /= 1 .AND. ensfileopt /= 2) THEN
    WRITE(*,'(1x,a)') 'WARNING!!!! I do not know how to handle the file name for time samples. Please implement this feature yourself in ensemblio3d.f90.'
    CALL arpsstop('ERROR: not supported feature')
  END IF

  DO l = nensmbl0, 1, -1

    CALL ctim2abss(year, month, day, hour, minute, second, iabs0)

    IF( l /= iensmbl ) THEN

      IF(myproc == 0) WRITE(*,'(1x,a,I0)') 'Reading Background from Member: ',l
      CALL get_input_dirname(ensdirname,l,dirfn)

      DO imem = 1, ntimesample
        ld = ntimesample*(l-1) + imem
        iabs = iabs0 + (imem - (ntimesample/2+1))*stimesample

        CALL abss2ctim( iabs, iyr, imon, idy, ihr, imin, isec )

!-----------------------------------------------------------------------
!
!     Read ARPS history datasets
!
!-----------------------------------------------------------------------
!
      IF(modelopt == 1) THEN

        WRITE(grdbasfn,'(a,a,I3.3,a)')  TRIM(dirfn),'enf',ld,'.hdfgrdbas'
        WRITE(filename,'(a,a,I3.3,a)')  TRIM(dirfn),'enf',ld,'.hdfxxxxxx'

        ! Initialize ARPS DTAREAD module
        CALL dtaread_init(grdbasfn,filename,3,lvldbg,istatus)

        !
        !  Read the data of the ensemble forecast.
        !
        CALL dtaread_get_3d(time, ue(:,:,:,ld), ve(:,:,:,ld), we(:,:,:,ld),     &
          pte(:,:,:,ld), prese(:,:,:,ld),qve(:,:,:,ld), qscalare(:,:,:,:,ld),   &
          tkee(:,:,:,ld), kmh,kmv,                                              &
          tsoile(:,:,:,:,ld),qsoile(:,:,:,:,ld),wetcanpe(:,:,:,ld),             &
          snowdpthe(:,:,ld), rainge(:,:,ld),    raince(:,:,ld),                 &
          prcratee(:,:,:,ld), istatus )
  !     print*,'ue ve we==',ue(50,20,9,l), ve(50,20,9,l), we(50,20,9,l),    &
  !                ptprte(50,20,9,l),pprte(50,20,9,l),qve(50,20,9,l),       &
  !                qscalare(50,20,9,1,l),qscalare(50,20,9,2,l),             &
  !                qscalare(50,20,9,3,l),qscalare(50,20,9,4,l),             &
  !                qscalare(50,20,9,5,l)
  !     stop

        CALL dtaread_get_optional(radfrce(:,:,:,ld),radswe(:,:,ld),           &
                   rnflxe(:,:,ld),radswnete(:,:,ld),radlwine(:,:,ld),         &
                   usflxe(:,:,ld),vsflxe(:,:,ld),ptsflxe(:,:,ld),             &
                   qvsflxe(:,:,ld), istatus)

        !
        !  Finalize ARPS DTAREAD module
        !
        CALL dtaread_final( istatus )

      ELSE IF (modelopt == 2) THEN

  !-----------------------------------------------------------------------
  !
  !     Read WRF history files
  !
  !-----------------------------------------------------------------------
  !

          IF (ensfileopt == 1) THEN      ! WRF INPUT
            WRITE(filename,'(2a)') TRIM(dirfn),'wrfinput_d01'
          ELSE IF (ensfileopt == 2) THEN      ! WRF OUT
            WRITE(filename,'(2a,I4.4,5(a,I2.2))') TRIM(dirfn),          &
                'wrfout_d01_',iyr,'-',imon,'-',idy,'_',ihr,':',imin,':',isec
            IF (ensreadready > 0) THEN
              WRITE(grdbasfn,'(2a,I4.4,5(a,I2.2))') TRIM(dirfn),        &
                'wrfoutReady_d01_',iyr,'-',imon,'-',idy,'_',ihr,':',imin,':',isec
              IF (myproc == 0) CALL wait_for_readyfile(grdbasfn,istatus)
              CALL mpupdatei(istatus,1)
              IF (istatus /= 0) CALL arpsstop('ERROR: readyfile not ready -'//TRIM(grdbasfn))
            END IF
          ELSE IF (ensfileopt == 3) THEN      ! WRF INPUT
            WRITE(filename,'(2a)') TRIM(dirfn),'wrfinput_d01_ic'
          ELSE IF (ensfileopt == 4) THEN ! WRF Forecast from NEWS-e
            WRITE(filename,'(2a,I4.4,5(a,I2.2),a,I0)') TRIM(dirfn),     &
                'wrffcst_d01_',iyr,'-',imon,'-',idy,'_',ihr,':',imin,':',isec,'_',l
          ELSE  ! WRF OUTPUT
            IF (enstimeopt == 1) THEN
              WRITE(filename,'(2a,I4.4,5(a,I2.2),a,I0)') TRIM(dirfn),   &
                'wrfout_d01_',iyr,'-',imon,'-',idy,'_',ihr,':',imin,':',isec,'_',l
            ELSE IF (enstimeopt == 2) THEN
              WRITE(filename,'(2a,I2.2,a,4a)') TRIM(dirfn),             &
                'wrfout_en',l,'_', ensintdate,'_',ensinttime
            END IF
          END IF

          IF(myproc == 0) WRITE(*,'(1x,3a)') 'member file <',TRIM(filename),'>'

          IF (ensreadopt == 2) THEN
            CALL dtareadwrfinone(nx_en,ny_en,nz_en,nx_m,ny_m,nz_m,      &
               filename,use_rhobare,                                    &
               interpopt, iim,jjm,wm11,wm12,wm21,wm22,                  &
               ue(:,:,:,ld), ve(:,:,:,ld),we(:,:,:,ld),pte(:,:,:,ld),   &
               prese(:,:,:,ld),qve(:,:,:,ld),qscalare(:,:,:,:,ld),      &
               rhobare(:,:,:,ld),zpm(:,:,:),istatus)
          ELSE
            CALL dtaread4wrf(nx_en,ny_en,nz_en,nscalar,qnames,          &
               filename,4,use_rhobare,                                  &
               ue(:,:,:,ld), ve(:,:,:,ld),we(:,:,:,ld),pte(:,:,:,ld),   &
               prese(:,:,:,ld),qve(:,:,:,ld),qscalare(:,:,:,:,ld),      &
               rhobare(:,:,:,ld),zpm(:,:,:),istatus)
          END IF
          !CALL print3dnc_lg(100+ld,'ubkg1',ue(:,:,:,ld),nx,ny,nz)
          !CALL print3dnc_lg(100+ld,'vbkg2',ve(:,:,:,ld),nx,ny,nz)
          !CALL print3dnc_lg(100+ld,'ubkg2',ue(:,:,:,ld),nx,ny,nz)
          !CALL print3dnc_lg(100+ld,'pbkg2',pprte(:,:,:,ld),nx,ny,nz)
          !CALL arpsstop(' ',0)

        END IF  !  (modelopt == 1/2) THEN
      END DO

   END IF   ! end of IF ( l /= iensmbl ) THEN

   IF(l == 1) curtim = time

  END DO

  IF (modelopt==1) DEALLOCATE(kmh,kmv)

  7777 CONTINUE

  IF(iensmbl /= 0) THEN
    nensvirtl = nensvirtl-ntimesample
  ENDIF

  IF (myproc == 0) WRITE(*,'(1x,3(a,I0))') 'Number of ensemble members = ',nensmbl,', iensmbl =',iensmbl,', total time sample members = ', nensvirtl

  IF( iensmbl /= 0 .and. iensmbl /= nensmbl0 ) THEN
    DO l = 1, ntimesample
      imemid = (iensmbl-1)*ntimesample+l
      ymemid = (nensmbl0-1)*ntimesample+l
      !WRITE(*,'(1x,2(a,I3),a,I3)') 'Moving ',l,',', imemid, ' <- ', ymemid

              ue(:,:,:,imemid)   =         ue(:,:,:,ymemid)
              ve(:,:,:,imemid)   =         ve(:,:,:,ymemid)
              we(:,:,:,imemid)   =         we(:,:,:,ymemid)
             pte(:,:,:,imemid)   =        pte(:,:,:,ymemid)
           prese(:,:,:,imemid)   =      prese(:,:,:,ymemid)
             qve(:,:,:,imemid)   =        qve(:,:,:,ymemid)
      qscalare(:,:,:,:,imemid)   = qscalare(:,:,:,:,ymemid)
         !rhobare(:,:,:,imemid)    =   rhobare(:,:,:,ymemid)

      IF (modelopt == 1) THEN
           tkee(:,:,:,imemid)   =      tkee(:,:,:,ymemid)
       tsoile(:,:,:,:,imemid)   =  tsoile(:,:,:,:,ymemid)
       qsoile(:,:,:,:,imemid)   =  qsoile(:,:,:,:,ymemid)
       wetcanpe(:,:,:,imemid)   =  wetcanpe(:,:,:,ymemid)
        snowdpthe(:,:,imemid)   =   snowdpthe(:,:,ymemid)
           rainge(:,:,imemid)   =      rainge(:,:,ymemid)
           raince(:,:,imemid)   =      raince(:,:,ymemid)
       prcratee(:,:,:,imemid)   =  prcratee(:,:,:,ymemid)
        radfrce(:,:,:,imemid)   =   radfrce(:,:,:,ymemid)
           radswe(:,:,imemid)   =      radswe(:,:,ymemid)
           rnflxe(:,:,imemid)   =      rnflxe(:,:,ymemid)
        radswnete(:,:,imemid)   =   radswnete(:,:,ymemid)
         radlwine(:,:,imemid)   =    radlwine(:,:,ymemid)
           usflxe(:,:,imemid)   =      usflxe(:,:,ymemid)
           vsflxe(:,:,imemid)   =      vsflxe(:,:,ymemid)
          ptsflxe(:,:,imemid)   =     ptsflxe(:,:,ymemid)
          qvsflxe(:,:,imemid)   =     qvsflxe(:,:,ymemid)
      END IF
    END DO
  END IF

  IF (0==1) THEN
    tem1=0.;tem2=0.;tem3=0.;tem4=0.
    radius_z_ens=vradius_ens(1)
    DO l = 1, nensmbl0
      CALL recurfilt3d(nx_en,ny_en,nz_en,ue(:,:,:,l),4,2,0,             &
                       hradius_3d_ens,radius_z_ens,tem1,tem2,tem3,tem4)
      CALL recurfilt3d(nx_en,ny_en,nz_en,ve(:,:,:,l),4,2,0,             &
                       hradius_3d_ens,radius_z_ens,tem1,tem2,tem3,tem4)
      CALL recurfilt3d(nx_en,ny_en,nz_en,we(:,:,:,l),4,2,0,             &
                       hradius_3d_ens,radius_z_ens,tem1,tem2,tem3,tem4)
      CALL recurfilt3d(nx_en,ny_en,nz_en,pte(:,:,:,l),4,2,0,            &
                       hradius_3d_ens,radius_z_ens,tem1,tem2,tem3,tem4)
      CALL recurfilt3d(nx_en,ny_en,nz_en,prese(:,:,:,l),4,2,0,          &
                       hradius_3d_ens,radius_z_ens,tem1,tem2,tem3,tem4)
      CALL recurfilt3d(nx_en,ny_en,nz_en,qve(:,:,:,l),4,2,0,            &
                       hradius_3d_ens,radius_z_ens,tem1,tem2,tem3,tem4)
      DO nq = 1, nscalar
        CALL recurfilt3d(nx_en,ny_en,nz_en,qscalare(:,:,:,nq,l),4,2,0,  &
                       hradius_3d_ens,radius_z_ens,tem1,tem2,tem3,tem4)
      END DO
    END DO
  END IF

  CALL ens_perturbNorm(nx,ny,nz, nscalar,myproc, istatus )

  IF(lvldbg > 100) THEN

    ALLOCATE(tem3d(nx_en,ny_en,nz_en), STAT = istatus)

    DO l = nensvirtl, 1, -1

      !print*, 'l==========',l

      tem3d(:,:,:) = ue(:,:,:,l)
      CALL a3dmax(tem3d,1,nx_en,1,nx_en  ,1,ny_en,1,ny_en  ,1,nz_en,1,nz_en  ,  &
                tem3d_max,tem3d_min, imax,jmax,kmax, imin,jmin,kmin)

      print*, '           '
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'ue_min=',tem3d_min,' at i=',imin,', j=',jmin,', k=',kmin
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'ue_max=',tem3d_max,' at i=',imax,', j=',jmax,', k=',kmax

        tem3d(:,:,:) = ve(:,:,:,l)
        CALL a3dmax(tem3d,1,nx_en,1,nx_en  ,1,ny_en,1,ny_en  ,1,nz_en,1,nz_en,  &
                  tem3d_max,tem3d_min, imax,jmax,kmax, imin,jmin,kmin)

        print*, '           '
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            've_min=',tem3d_min,' at i=',imin,', j=',jmin,', k=',kmin
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            've_max=',tem3d_max,' at i=',imax,', j=',jmax,', k=',kmax


        tem3d(:,:,:) = we(:,:,:,l)
        CALL a3dmax(tem3d,1,nx_en,1,nx_en  ,1,ny_en,1,ny_en  ,1,nz_en,1,nz_en,  &
                  tem3d_max,tem3d_min, imax,jmax,kmax, imin,jmin,kmin)

        print*, '           '
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'we_min=',tem3d_min,' at i=',imin,', j=',jmin,', k=',kmin
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'we_max=',tem3d_max,' at i=',imax,', j=',jmax,', k=',kmax


        tem3d(:,:,:) = prese(:,:,:,l)
        CALL a3dmax(tem3d,1,nx_en,1,nx_en  ,1,ny_en,1,ny_en  ,1,nz_en,1,nz_en,  &
                  tem3d_max,tem3d_min, imax,jmax,kmax, imin,jmin,kmin)

        print*, '           '
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'prese_min=',tem3d_min,' at i=',imin,', j=',jmin,', k=',kmin
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'prese_max=',tem3d_max,' at i=',imax,', j=',jmax,', k=',kmax


        tem3d(:,:,:) = pte(:,:,:,l)
        CALL a3dmax(tem3d,1,nx_en,1,nx_en  ,1,ny_en,1,ny_en  ,1,nz_en,1,nz_en,  &
                  tem3d_max,tem3d_min, imax,jmax,kmax, imin,jmin,kmin)

        print*, '           '
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'pte_min=',tem3d_min,' at i=',imin,', j=',jmin,', k=',kmin
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'pte_max=',tem3d_max,' at i=',imax,', j=',jmax,', k=',kmax


        tem3d(:,:,:) =    qve(:,:,:,l)
        CALL a3dmax(tem3d,1,nx_en,1,nx_en  ,1,ny_en,1,ny_en  ,1,nz_en,1,nz_en,  &
                  tem3d_max,tem3d_min, imax,jmax,kmax, imin,jmin,kmin)

        print*, '           '
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'qve_min=',tem3d_min,' at i=',imin,', j=',jmin,', k=',kmin
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'qve_max=',tem3d_max,' at i=',imax,', j=',jmax,', k=',kmax


        tem3d(:,:,:) =   qscalare(:,:,:,1,l)
        CALL a3dmax(tem3d,1,nx_en,1,nx_en  ,1,ny_en,1,ny_en  ,1,nz_en,1,nz_en,  &
                  tem3d_max,tem3d_min, imax,jmax,kmax, imin,jmin,kmin)

        print*, '           '
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'qc_min=',tem3d_min,' at i=',imin,', j=',jmin,', k=',kmin
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'qc_max=',tem3d_max,' at i=',imax,', j=',jmax,', k=',kmax


        tem3d(:,:,:) =   qscalare(:,:,:,2,l)
        CALL a3dmax(tem3d,1,nx_en,1,nx_en  ,1,ny_en,1,ny_en  ,1,nz_en,1,nz_en,  &
                  tem3d_max,tem3d_min, imax,jmax,kmax, imin,jmin,kmin)

        print*, '           '
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'qr_min=',tem3d_min,' at i=',imin,', j=',jmin,', k=',kmin
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'qr_max=',tem3d_max,' at i=',imax,', j=',jmax,', k=',kmax


        tem3d(:,:,:) =   qscalare(:,:,:,3,l)
        CALL a3dmax(tem3d,1,nx_en,1,nx_en  ,1,ny_en,1,ny_en  ,1,nz_en,1,nz_en,  &
                  tem3d_max,tem3d_min, imax,jmax,kmax, imin,jmin,kmin)

        print*, '           '
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'qi_min=',tem3d_min,' at i=',imin,', j=',jmin,', k=',kmin
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'qi_max=',tem3d_max,' at i=',imax,', j=',jmax,', k=',kmax


        tem3d(:,:,:) =    qscalare(:,:,:,4,l)
        CALL a3dmax(tem3d,1,nx_en,1,nx_en  ,1,ny_en,1,ny_en  ,1,nz_en,1,nz_en,  &
                  tem3d_max,tem3d_min, imax,jmax,kmax, imin,jmin,kmin)

        print*, '           '
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'qs_min=',tem3d_min,' at i=',imin,', j=',jmin,', k=',kmin
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'qs_max=',tem3d_max,' at i=',imax,', j=',jmax,', k=',kmax


        tem3d(:,:,:) =    qscalare(:,:,:,5,l)
        CALL a3dmax(tem3d,1,nx_en,1,nx_en  ,1,ny_en,1,ny_en  ,1,nz_en,1,nz_en,  &
                  tem3d_max,tem3d_min, imax,jmax,kmax, imin,jmin,kmin)

        print*, '           '
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'qh_min=',tem3d_min,' at i=',imin,', j=',jmin,', k=',kmin
      IF (myproc == 0) WRITE(6,'((1x,a,g25.12,3(a,i4)))')               &
            'qh_max=',tem3d_max,' at i=',imax,', j=',jmax,', k=',kmax

      qscalare(:,:,:,:,l) = 0.0

    END DO

    DEALLOCATE(tem3d)

  END IF

!-----------------------------------------------------------------------
!
! Get vertical grid heights
!
!-----------------------------------------------------------------------
!
  l = 1
  IF(modelopt == 1) THEN

    IF(myproc == 0) print*,'Reading ensemble grid physical heights from Member ',l

    WRITE(grdbasfn,'(a,a,I3.3,a)')  TRIM(dirfn),'enf',l,'.hdfgrdbas'

    WRITE(filename,'(a,a,I3.3,a)')  TRIM(dirfn),'enf',l,'.hdf000000'

    ! Initialize ARPS DTAREAD module
    CALL dtaread_init(grdbasfn,filename,3,lvldbg,istatus)

    !  Read the data of the ensemble forecast.
    CALL dtaread_get_var('zp',3,(/nx,ny,nz/),3,zpm,istatus)

    !  Finalize ARPS DTAREAD module
    CALL dtaread_final( istatus )

  !ELSE IF (modelopt == 2) THEN

    !IF (ensfileopt == 1) THEN      ! WRF INPUT
    !  WRITE(filename,'(2a)') TRIM(dirfn),'wrfinput_d01'
    !ELSE IF (ensfileopt == 3) THEN      ! WRF INPUT
    !  WRITE(filename,'(2a)') TRIM(dirfn),'wrfinput_d01_ic'
    !ELSE IF (ensfileopt == 4) THEN ! WRF Forecast from NEWS-e
    !  WRITE(filename,'(2a,I4.4,5(a,I2.2),a,I0)') TRIM(dirfn),'wrffcst_d01_',   &
    !               year,'-',month,'-',day,'_',hour,':',minute,':',second,'_',l
    !ELSE  ! WRF OUTPUT
    !
    !  IF (enstimeopt == 1) THEN
    !    WRITE(filename,'(2a,I4.4,5(a,I2.2),a,I0)') TRIM(dirfn),'wrfout_d01_',   &
    !               year,'-',month,'-',day,'_',hour,':',minute,':',second,'_',l
    !  ELSE IF (enstimeopt == 2) THEN
    !    WRITE(filename,'(2a,I2.2,a,4a)') TRIM(dirfn),'wrfout_en',l,'_',   &
    !               ensintdate,'_',ensinttime
    !  END IF
    !END IF
    !
    !CALL dtareadPH(nx,ny,nz,filename,4,zpm,istatus)

  END IF  !  (modelopt == 1/2) THEN

  RETURN
END SUBROUTINE enbgread4var

!
!##################################################################
!#               FUNCTION GASDEV                                  #
!##################################################################

REAL FUNCTION GASDEV(IDUM)

      IMPLICIT NONE

      INTEGER,SAVE::ISET
      INTEGER :: IDUM
      REAL,SAVE::GSET
      REAL :: V1, V2, R
      REAL :: RAN1
      REAL :: FAC
      DATA ISET/0/
      IF (ISET.EQ.0) THEN
      1 V1=2.*RAN1(IDUM)-1.
        V2=2.*RAN1(IDUM)-1.
        R=V1**2+V2**2
        IF(R.GE.1.)GO TO 1
        FAC=SQRT(-2.*LOG(R)/R)
        GSET=V1*FAC
        GASDEV=V2*FAC
        ISET=1
      ELSE
        GASDEV=GSET
        ISET=0
      ENDIF
      RETURN
END FUNCTION GASDEV
!
!##############################################################
!#                FUNCTION RAN1                               #
!##############################################################

REAL FUNCTION RAN1(IDUM)
      implicit none
      REAL,SAVE :: R(97)
      INTEGER IX1,IX2,IX3,IDUM,J,IFF
      INTEGER M1,M2,M3,IA1,IA2,IA3,IC1,IC2,IC3
      REAL RM1,RM2
      SAVE IX1,IX2,IX3

      PARAMETER (M1=259200,IA1=7141,IC1=54773,RM1=3.8580247E-6)
      PARAMETER (M2=134456,IA2=8121,IC2=28411,RM2=7.4373773E-6)
      PARAMETER (M3=243000,IA3=4561,IC3=51349)
      DATA IFF /0/
      IF (IDUM.LT.0.OR.IFF.EQ.0) THEN
        IFF=1
        IX1=MOD(IC1-IDUM,M1)
        IX1=MOD(IA1*IX1+IC1,M1)
        IX2=MOD(IX1,M2)
        IX1=MOD(IA1*IX1+IC1,M1)
        IX3=MOD(IX1,M3)
        DO J=1,97
          IX1=MOD(IA1*IX1+IC1,M1)
          IX2=MOD(IA2*IX2+IC2,M2)
          R(J)=(FLOAT(IX1)+FLOAT(IX2)*RM2)*RM1
        ENDDO
        IDUM=1
      ENDIF
      IX1=MOD(IA1*IX1+IC1,M1)
      IX2=MOD(IA2*IX2+IC2,M2)
      IX3=MOD(IA3*IX3+IC3,M3)
      J=1+(97*IX3)/M3
      !IF(J.GT.97.OR.J.LT.1)PAUSE
      RAN1=R(J)
      R(J)=(FLOAT(IX1)+FLOAT(IX2)*RM2)*RM1
      RETURN
END FUNCTION RAN1
