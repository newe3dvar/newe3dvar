!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TLMADVUVW                  ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!
!
SUBROUTINE tlmadvuvw(nx,ny,nz,nstepss,nbasic,                           &
           u,v,w,wcont,rhostr,ubar,vbar,nwcont,                         &
!          nu,nv,nw, 
           ubk,vbk,wbk,                                                 &
           nustr,nvstr,nwstr,                                           &
           uadv,vadv,wadv,                                              &
           ustr,vstr,wstr,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Coordinates the calculation of the advection terms uadv, vadv and
!  wadv of the u, v and w equations. These terms are written in
!  equivalent advection form.
!
!  TLMADVUVW is the tangent linear model of ADVUVW.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  05/21/1994
!
!  MODIFICATION HISTORY:
!
!  05/21/1994 (Jidong Gao)
!  Convered to version 4.0
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nstepss  The number of integrations plus one.
!    nbasic   Index of the current basic state time level.
!
!    u        x component of velocity at all time levels (m/s)
!    v        y component of velocity at all time levels (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates at all time levels (m/s).
!    wcont    Contravariant vertical velocity (m/s)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    ubar     Base state x component of velocity (m/s)
!    vbar     Base state y component of velocity (m/s)
!
!    Basic state variables:
!    nu,nv,nw,
!    nustr,nvstr,nwstr.
!
!  OUTPUT:
!
!    uadv     u-equation advection terms  (kg/m**3)*(m/s)/s
!    vadv     v-equation advection terms  (kg/m**3)*(m/s)/s
!    wadv     w-equation advection terms  (kg/m**3)*(m/s)/s
!
!  WORK ARRAYS:
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!-----------------------------------------------------------------------
!
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nt                ! no. of t-levels of t-dependent arrays.
  INTEGER :: tpast             ! Index of time level for the past time.
  INTEGER :: tpresent          ! Index of time level for the present
                               ! time.
  INTEGER :: tfuture           ! Index of time level for the future
                               ! time.
  INTEGER :: nstepss           ! Index of total current basic state time levels.
  INTEGER :: nbasic            ! Index of the current basic state time level.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity (m/s)
  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)

  REAL :: uadv  (nx,ny,nz)     ! u-eqn advection terms
                               ! (kg/m**3)*(m/s)/s
  REAL :: vadv  (nx,ny,nz)     ! v-eqn advection terms
                               ! (kg/m**3)*(m/s)/s
  REAL :: wadv  (nx,ny,nz)     ! w-eqn advection terms
                               ! (kg/m**3)*(m/s)/s

  REAL :: ustr  (nx,ny,nz)     ! u * rhostr
  REAL :: vstr  (nx,ny,nz)     ! v * rhostr
  REAL :: wstr  (nx,ny,nz)     ! w * rhostr

  REAL :: ubk   (nx,ny,nz)     ! u * rhostr
  REAL :: vbk   (nx,ny,nz)     ! v * rhostr
  REAL :: wbk   (nx,ny,nz)     ! w * rhostr

  REAL :: nwcont (nx,ny,nz)    ! basic states for wcont
  REAL :: nustr  (nx,ny,nz)    ! nu * rhostr
  REAL :: nvstr  (nx,ny,nz)    ! nv * rhostr
  REAL :: nwstr  (nx,ny,nz)    ! nw * rhostr

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array

  REAL :: bem1  (nx,ny,nz)     ! Temporary work array
  REAL :: bem2  (nx,ny,nz)     ! Temporary work array
  REAL :: bem3  (nx,ny,nz)     ! Temporary work array
  REAL :: bem4  (nx,ny,nz)     ! Temporary work array
! 
  REAL :: sum1, sum2, sum3
  INTEGER i,j,k
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: tlevel
! INCLUDE 'variable.inc'    ! Basic state variable.
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code.
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  tlevel=tpresent
!
!-----------------------------------------------------------------------
!
!  Calculate nustr=rhostr*nu, nvstr=rhostr*nv, nwstr=rhostr*nw
!
!-----------------------------------------------------------------------
!
   tem1 = 0.
   tem2 = 0.
   tem3 = 0.
  nustr = 0.
  nvstr = 0.
  nwstr = 0.
   ustr = 0.
   vstr = 0.
   wstr = 0.
!
! CALL uvwrho0(nx,ny,nz,nu(1,1,1,nbasic),nv(1,1,1,nbasic),              &
  CALL uvwrho0(nx,ny,nz,ubk,vbk,nwcont,rhostr,nustr,nvstr,nwstr)
!
!
  IF (1 == 0) THEN
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          bem1(i,j,k)=u(i,j,k,tpresent)
          bem2(i,j,k)=v(i,j,k,tpresent)
          bem3(i,j,k)=wcont(i,j,k)
        END DO
      END DO
    END DO
  END IF
!
!
!-----------------------------------------------------------------------
!
!  Calculate ustr=rhostr*u, vstr=rhostr*v, wstr=rhostr*w
!  uvwrho0 is linear!!!
!
!-----------------------------------------------------------------------
!
! CALL uvwrho0(nx,ny,nz,u(1,1,1,tlevel),v(1,1,1,tlevel),                &
!                           wcont,rhostr,ustr,vstr,wstr)
    wcont = 0.0
    ustr =0.0; vstr =0.0; wstr =0.0
!  
  IF (1 == 0) THEN
!
   sum1=0.
    CALL product(ustr,ustr,                                             &
                 nx,ny,nz,1,nx  ,1,ny-1,1,nz-1,sum2)
    sum1=sum1+sum2
  
    CALL product(vstr,vstr,                                             &
                 nx,ny,nz,1,nx-1,1,ny  ,1,nz-1,sum2)
    sum1=sum1+sum2

    CALL product(wstr,wstr,                                             &
                 nx,ny,nz,1,nx-1,1,ny-1,1,nz  ,sum2)
    sum1=sum1+sum2
    PRINT*,'uv_sum1 (AQ)*(AQ)= ',sum1

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          u(i,j,k,tpresent)=0.0
          v(i,j,k,tpresent)=0.0
          wcont(i,j,k)=0.0
        END DO
      END DO
    END DO

   CALL aduvwrho(nx,ny,nz,u(1,1,1,tlevel),v(1,1,1,tlevel),              &
               wcont,rhostr,ustr,vstr,wstr,tem1,tem2,tem3)


 sum3=0.
    CALL product(bem1,u(1,1,1,tlevel),                                  &
                 nx,ny,nz,1,nx,1,ny-1,1,nz-1,sum2)
    sum3=sum3+sum2

    CALL product(bem2,v(1,1,1,tlevel),                                  &
                 nx,ny,nz,1,nx-1,1,ny,1,nz-1,sum2)
    sum3=sum3+sum2

    CALL product(bem3,wcont,                                            &
                 nx,ny,nz,1,nx-1,1,ny-1,1,nz,sum2)
    sum3=sum3+sum2
    PRINT*,'uv_sum3 (AQ)*(AQ)= ',sum3
    stop

  END IF
!-----------------------------------------------------------------------
!
!  Calculate uadv:
!
!-----------------------------------------------------------------------
!
  IF (1 == 0) THEN
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          bem1(i,j,k)=u(i,j,k,tpresent)
          bem2(i,j,k)=ustr(i,j,k)
          bem3(i,j,k)=vstr(i,j,k)
          bem4(i,j,k)=wstr(i,j,k)
        END DO
      END DO
    END DO
  END IF
!
!
  ustr=0.0; vstr=0.0; wstr=0.0
  CALL tlmadvu(nx,ny,nz, nstepss,nbasic,u,v,w,                          &
            ubar, uadv,nustr,nvstr,nwstr,                               &
            tem1,tem2,tem3)
! CALL tlmadvu(nx,ny,nz, nstepss,nbasic,u,v,w,                          &
!           ustr,vstr,wstr, ubar, uadv,nustr,nvstr,nwstr,               &
!           nu,nv,nw,                                                   &
!           tem1,tem2,tem3)
!
  IF (1 == 0) THEN
!
   sum1=0.
    CALL product(uadv,uadv,                                             &
!                nx,ny,nz,2,nx-1,1,ny-1,2,nz-2,sum2)
                 nx,ny,nz,1,nx-0,1,ny-0,1,nz-0,sum2)
    sum1=sum1+sum2
  
    PRINT*,'tlmadvu (AQ)*(AQ)= ',sum1

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          u(i,j,k,tpresent)=0.0
          ustr(i,j,k) = 0.0
          vstr(i,j,k) = 0.0
          wstr(i,j,k) = 0.0
        END DO
      END DO
    END DO

!
! CALL adtlmadvu(nx,ny,nz, nstepss,nbasic,u,v,w,                        &
!           ustr,vstr,wstr, ubar, uadv,nustr,nvstr,nwstr,               &
!           nu,nv,nw,                                                   &
!           tem1,tem2,tem3)
!
  CALL adtlmadvu(nx,ny,nz, nstepss,nbasic,u,v,w,                        &
            ubar, uadv,nustr,nvstr,nwstr,                               &
            tem1,tem2,tem3)

     sum3=0.
    CALL product(bem1,u(1,1,1,tlevel),                                  &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2

    CALL product(bem2,ustr,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
 
    CALL product(bem3,vstr,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2

    CALL product(bem4,wstr,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2

    PRINT*,'adtlmadvuQ)*(AQ) = ',sum3
    stop

  END IF
!
!
!-----------------------------------------------------------------------
!
!  Calculate vadv:
!
!-----------------------------------------------------------------------
!
  IF (1 == 0) THEN
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          bem1(i,j,k)=v(i,j,k,tpresent)
          bem2(i,j,k)=ustr(i,j,k)
          bem3(i,j,k)=vstr(i,j,k)
          bem4(i,j,k)=wstr(i,j,k)
        END DO
      END DO
    END DO
  END IF
!
!
! CALL tlmadvv(nx,ny,nz, nstepss,nbasic,u,v,w,                          &
!           ustr,vstr,wstr, vbar, vadv,nustr,nvstr,nwstr,               &
!           nu,nv,nw,                                                   &
!           tem1,tem2,tem3)
!
  CALL tlmadvv(nx,ny,nz, nstepss,nbasic,u,v,w,                          &
            vbar, vadv,nustr,nvstr,nwstr,                               &
            tem1,tem2,tem3)

  IF (1 == 0) THEN
!
   sum1=0.
    CALL product(vadv,vadv,                                             &
                 nx,ny,nz,1,nx-1,2,ny-1,2,nz-2,sum2)
    sum1=sum1+sum2

    PRINT*,'tlmadvv (AQ)*(AQ)= ',sum1

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          v(i,j,k,tpresent)=0.0
          ustr(i,j,k) = 0.0
          vstr(i,j,k) = 0.0
          wstr(i,j,k) = 0.0
        END DO
      END DO
    END DO

!
! CALL adtlmadvv(nx,ny,nz, nstepss,nbasic,u,v,w,                        &
!           ustr,vstr,wstr, vbar, vadv,nustr,nvstr,nwstr,               &
!           nu,nv,nw,                                                   &
!           tem1,tem2,tem3)
!
  CALL adtlmadvv(nx,ny,nz,nstepss,nbasic,u,v,w,                         &
           vbar,vadv, nustr,nvstr,nwstr,                                &
           tem1,tem2,tem3)


     sum3=0.
    CALL product(bem1,v(1,1,1,tlevel),                                  &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2

    CALL product(bem2,ustr,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2

    CALL product(bem3,vstr,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2

    CALL product(bem4,wstr,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2

    PRINT*,'adtlmadvvQ)*(AQ) = ',sum3
    stop

  END IF
!

!
!-----------------------------------------------------------------------
!
!  Calculate wadv:
!
!-----------------------------------------------------------------------
!
  IF (1 == 0) THEN
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          bem1(i,j,k)=w(i,j,k,tpresent)
          bem2(i,j,k)=ustr(i,j,k)
          bem3(i,j,k)=vstr(i,j,k)
          bem4(i,j,k)=wstr(i,j,k)
        END DO
      END DO
    END DO
  END IF
!   
!
! CALL     tlmadvw(nx,ny,nz,nstepss,nbasic,u,v,w,                       &
!          ustr,vstr,wstr, wadv,  nustr,nvstr,nwstr,                    &
!          nu,nv,nw,                                                    &
!          tem1,tem2,tem3)
  CALL tlmadvw(nx,ny,nz,nstepss,nbasic,u,v,w,                           &
            wadv,nustr,nvstr,nwstr,                                     &
            tem1,tem2,tem3)


  IF (1 == 0) THEN
!
   sum1=0.
    CALL product(wadv,wadv,                                             &
                 nx,ny,nz,1,nx-1,1,ny-1,2,nz-1,sum2)
    sum1=sum1+sum2

    PRINT*,'tlmadvw (AQ)*(AQ)= ',sum1

    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          w(i,j,k,tpresent)=0.0
          ustr(i,j,k) = 0.0
          vstr(i,j,k) = 0.0
          wstr(i,j,k) = 0.0
        END DO
      END DO
    END DO

!
! CALL     adtlmadvw(nx,ny,nz,nstepss,nbasic,u,v,w,                     &
!          ustr,vstr,wstr, wadv,  nustr,nvstr,nwstr,                    &
!          nu,nv,nw,                                                    &
!          tem1,tem2,tem3)
!
  CALL     adtlmadvw(nx,ny,nz,nstepss,nbasic,u,v,w,                     &
           wadv,  nustr,nvstr,nwstr,                                    &
           tem1,tem2,tem3)

     sum3=0.
    CALL product(bem1,w(1,1,1,tlevel),                                  &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2

    CALL product(bem2,ustr,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2

    CALL product(bem3,vstr,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2

    CALL product(bem4,wstr,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2

    PRINT*,'adtlmadvwQ)*(AQ) = ',sum3
    stop

  END IF
!
RETURN
END SUBROUTINE tlmadvuvw
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                 SUBROUTINE TLMADVU                   ######
!######                                                      ######
!######               Copyright (c) 1992-1994                ######
!######     Center for Analysis and Prediction of Storms     ######
!######     University of Oklahoma.  All rights reserved.    ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tlmadvu(nx,ny,nz,nstepss,nbasic,                             &
           u,v,w,                ubar,uadv,                             &
!          u,v,w,ustr,vstr,wstr, ubar,uadv,                             &
           nustr,nvstr,nwstr,                                           &
!          nu,nv,nw,                                                    &
           tem1,tem2,tem3)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the advection terms of the u-equation. These terms are
!  written in equivalent advection form.
!
!  TLMADVU is the tangent linear model of ADVU.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  05/21/1994
!
!  MODIFICATION HISTORY:
!
!  04/23/96 (Jidong Gao)
!  Converted into version 4.0.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nstepss  The number of integrations plus one.
!    nbasic   Index of the current basic state time level.
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at all time levels (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates at all time levels (m/s).
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!    ubar     Base state x component of velocity (m/s)
!
!    Basic state variables:
!    nu,nv,nw,                                     
!    nustr,nvstr,nwstr.
!
!  OUTPUT:
!
!    uadv     u-equation advection terms (kg/m**3)*(m/s)/s
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  INTEGER :: nt                ! The no. of t-levels of t-dependent
                               ! arrays.
  INTEGER :: tpast             ! Index of time level for the past time.
  INTEGER :: tpresent          ! Index of time level for the present
  INTEGER :: nstepss           ! Index of total current basic state time levels.
  INTEGER :: nbasic            ! Index of the current basic state time level.
                               ! time.
  INTEGER :: tfuture           ! Index of time level for the future
                               ! time.

  INTEGER :: npast             ! Temporary variable.
  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity at a given time level
                               ! (m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity at a given time level
                               ! (m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity at a given time level
                               ! (m/s)
! REAL :: ustr  (nx,ny,nz)     ! u*rhostr
! REAL :: vstr  (nx,ny,nz)     ! v*rhostr
! REAL :: wstr  (nx,ny,nz)     ! w*rhostr
  REAL :: nustr  (nx,ny,nz)    ! nu * rhostr
  REAL :: nvstr  (nx,ny,nz)    ! nv * rhostr
  REAL :: nwstr  (nx,ny,nz)    ! nw * rhostr
  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)

  REAL :: uadv  (nx,ny,nz)     ! Advection term of the u-eq.
                               ! (kg/m**3)*(m/s)/s

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,onvf
  INTEGER :: ibgn,iend,jbgn,jend
  REAL :: vsb, vnb,nvsb,nvnb
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'       ! Global model control parameters
  INCLUDE 'grid.inc'
  INCLUDE 'bndry.inc'         ! Parameters for boundary conditions.
! INCLUDE 'variable.inc'      ! Basic state variable.
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
        uadv = 0.
!
!-----------------------------------------------------------------------
!
!  Calculate rho*u'* du/dx + rho*u* du'/dx
!               avgx0( avgx0( ustr )*difx0( u' ) )
!   where u'=u and u=nu.
!
  onvf = 0
  CALL avgx0(nustr, onvf,                                                &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem1)
!
!
  onvf = 0
  CALL difx0(u(1,1,1,tpresent), onvf,                                    &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, dx, tem2)
!
!
  CALL aamult0(tem1, tem2,                                               &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem3)
!
!
!   tem2 contains (avgx0(nu*rho))*du'/dx
!
!-----------------------------------------------------------------------
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
!
  onvf = 1
  CALL avgx0(tem3, onvf,                                                 &
        nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, tem1)
!
!  tem1=avgx0( avgx0( ustr )*difx0( u' ) )
!
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        uadv(i,j,k)= uadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO
!
!
!-----------------------------------------------------------------------
!
!  Calculate  = rho*v*du'/dy + rho*v'* du/dy
!             = avgx0( avgx0( vstr' )*dify0( u ) )+
!               avgx0( avgx0( vstr )*dify0( u' ) )
!   where v'=v, v=nv, u'=u and u=nu.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
!
  onvf = 1
  CALL avgx0(nvstr, onvf,                                                &
        nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, tem1)
!
  onvf = 1
  CALL dify0(u(1,1,1,tpresent), onvf,                                    &
        nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, dy, tem2)
!
!   tem1 contains avgx0(nv*rho)
!   tem2 contains du/dy
!
  CALL aamult0(tem1, tem2,                                               &
        nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, tem3)
!
!  tem2 contains (avgx0(nv*rho)) * du/dy
!
!-----------------------------------------------------------------------
!
!  Wash up and tem3. Hi, dude, never remove this wash up sentence.
!
!-----------------------------------------------------------------------
!
  tem1 = 0.

  onvf = 0
  CALL avgy0(tem3, onvf,                                                 &
        nx,ny,nz, 2,nx-1, 2,ny-2, 2,nz-2, tem1)
 
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        uadv(i,j,k)= uadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO
 
!
!-----------------------------------------------------------------------
!
!  Calculate rho*v* du/dy on the north and south boundaries using
!  one sided advection. rlxlbc is a constant coefficient.
!
!-----------------------------------------------------------------------
!
  IF (nbasic == 1) THEN
    npast=nbasic
  ELSE
    npast=nbasic-1
  END IF
  DO k=2,nz-2
    DO i=2,nx-1
      nvsb=(nvstr(i-1,1,k)+nvstr(i-1,2,k)                               &
          +nvstr(i,1,k)+nvstr(i,2,k))*0.25
      nvnb=(nvstr(i-1,ny-1,k)+nvstr(i-1,ny,k)                           &
          +nvstr(i,ny-1,k)+nvstr(i,ny,k))*0.25
      IF (nvsb < 0.0) THEN
        tem1(i,1,k)=nvsb*(u(i,2,k,tpresent)-u(i,1,k,tpresent))*dyinv    !&
      ELSE
        tem1(i,1,k)= rlxlbc*nvsb*(u(i,1,k,tpast))*dyinv                 !&
      END IF

      IF (nvnb > 0.0) THEN
        tem1(i,ny-1,k)=nvnb*                                            &
               (u(i,ny-1,k,tpresent)-u(i,ny-2,k,tpresent))*dyinv        !&
      ELSE
        tem1(i,ny-1,k)=-rlxlbc*nvnb*                                    &
               (u(i,ny-1,k,tpast))*dyinv                                !&
      END IF
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Add the u-equation horizontal advection terms, rho*u*du/dx (uadv) and
!  rho*v*du/dy (tem1).
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        uadv(i,j,k)=uadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO

!
!-----------------------------------------------------------------------
!
!  Calculate rho*w* du'/dz + rho*w'* du/dz=
!                  avgz0( avgx0( wstr ) * difz0 ( u' ) )
!                  avgz0( avgx0(nwstr ) * difz0 ( u ) )
!
!-----------------------------------------------------------------------
!
!  Wash up tem1 and tem2.
!
!-----------------------------------------------------------------------
!
   tem1 = 0.
   tem2 = 0.
   tem3 = 0.
!-----------------------------------------------------------------------
!  We first calculate avgz0( avgx0( wstr ) * difz0 ( u' ) ).
  onvf = 1
  CALL avgx0(nwstr, onvf,                                                &
        nx,ny,nz,2,nx-1,1,ny-1, 2,nz-1, tem1)
!
  onvf = 1
  CALL difz0(u(1,1,1,tpresent), onvf,                                    &
        nx,ny,nz,2,nx-1,1,ny-1, 2,nz-1, dz, tem2)
!
!  tem1 contains avgx0(nw*rho)
!  tem2 contains du/dz
!
  CALL aamult0(tem1, tem2,                                               &
        nx,ny,nz,2,nx-1,1,ny-1, 2,nz-1, tem3)
!
!  tem2 contains avgx0(nw*rho) * du/dz
!
  tem1 = 0.0
  onvf = 0
  CALL avgz0(tem3, onvf,                                                 &
        nx,ny,nz,2,nx-1,1,ny-1, 2,nz-2, tem1)
!
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        uadv(i,j,k)= uadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO
!
  RETURN
END SUBROUTINE tlmadvu
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TLMADVV                    ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tlmadvv(nx,ny,nz,nstepss,nbasic,u,v,w,                       &
!          ustr,vstr,wstr, vbar,vadv, nustr,nvstr,nwstr,                &
                           vbar,vadv, nustr,nvstr,nwstr,                &
!          nu,nv,nw,                                                    &
           tem1,tem2,tem3)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the v-equation advection terms of the TLM.
!  These terms are written in equivalent advection form.
!
!  This is the tangent linear model of ADVV.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  05/29/1994
!
!  MODIFICATION HISTORY:
!
!  04/23/96 (Jidong Gao)
!  Converted into version 4.0.
!
!-----------------------------------------------------------------------
!
!  INPUTS:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nstepss  Index of total current basic state time levels.
!    nbasic   Index of the current basic state time level.
!
!    v        y component of velocity at a given time level (m/s)
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!    Basic state variables:
!    nu,nv,nw,                                       
!    nustr,nvstr,nwstr.
!
!  OUTPUT:
!
!    vadv     v equation advection terms (kg/m**3)*(m/s)/s
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
  INTEGER :: nx,ny,nz

  INTEGER :: nt           ! The no. of t-levels of t-dependent arrays.
  INTEGER :: tpast        ! Index of time level for the past time.
  INTEGER :: tpresent     ! Index of time level for the present time.
  INTEGER :: tfuture      ! Index of time level for the future time.
  INTEGER :: nstepss      ! Index of total current basic state time levels.
  INTEGER :: nbasic       ! Index of the current basic state time level.
  INTEGER :: npast        ! Temporary variable.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity at a given time level(m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity at a given time level(m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity at a given time level(m/s)
! REAL :: ustr  (nx,ny,nz)     ! u*rhostr
! REAL :: vstr  (nx,ny,nz)     ! v*rhostr
! REAL :: wstr  (nx,ny,nz)     ! w*rhostr
!
  REAL :: nustr  (nx,ny,nz)     ! nu * rhostr
  REAL :: nvstr  (nx,ny,nz)     ! nv * rhostr
  REAL :: nwstr  (nx,ny,nz)     ! nw * rhostr
!
  REAL :: vbar  (nx,ny,nz)
!
  REAL :: vadv  (nx,ny,nz)     ! Advection term of the v-eq.
                               ! (kg/m**3)*(m/s)/s
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,onvf
  INTEGER :: ibgn,iend,jbgn,jend
  REAL :: ueb, uwb, sum3, nuwb,nueb

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'       ! Global model control parameters
  INCLUDE 'grid.inc'
  INCLUDE 'bndry.inc'         ! Parameters for boundary conditions.
! INCLUDE 'variable.inc'      ! Basic state variables.
!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
  EXTERNAL avgx0
  EXTERNAL avg2x0
  EXTERNAL avgy0
  EXTERNAL avg2y0
  EXTERNAL avgz0
  EXTERNAL difx0
  EXTERNAL dif2x0
  EXTERNAL dify0
  EXTERNAL dif2y0
  EXTERNAL difz0
  EXTERNAL aamult0
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
        vadv = 0.
!
!-----------------------------------------------------------------------
!  Now we calculate avgx0( avgy0( ustr ) * difx0( v' ) )
!  avgy0 is linear!!!
  onvf = 1
  CALL avgy0(nustr, onvf,                                                &
        nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, tem1)
!
  onvf = 1
  CALL difx0(v(1,1,1,tpresent), onvf,                                    &
        nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, dx, tem2)
!
!  tem1 contains avgy0(u'*rho)
!  tem2 contains dv/dx
!
  CALL aamult0(tem1, tem2,                                               &
        nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, tem3)
!
!-----------------------------------------------------------------------
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
!
!-----------------------------------------------------------------------
  onvf = 0
  CALL avgx0(tem3, onvf,                                                 &
        nx,ny,nz, 2,nx-2, 2,ny-1, 2,nz-2, tem1)
!
  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        vadv(i,j,k)=vadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Calculate rho*u* dv/dx at the east and west boundaries
!  using one-sided advection.
!
!-----------------------------------------------------------------------
!
  IF (nbasic == 1) THEN
    npast=nbasic
  ELSE
    npast=nbasic-1
  END IF
  DO k=2,nz-2
    DO j=2,ny-1
      nuwb=(nustr(1,j,k)+nustr(2,j,k)+nustr(1,j-1,k)+                   &
           nustr(2,j-1,k))*0.25
      nueb=(nustr(nx-1,j,k)+nustr(nx,j,k)+nustr(nx-1,j-1,k)             &
          +nustr(nx,j-1,k))*0.25
      IF (nuwb < 0.0) THEN
        vadv(1,j,k)=nuwb*(v(2,j,k,tpresent)-v(1,j,k,tpresent))*dxinv    !& 
      ELSE
        vadv(1,j,k)=rlxlbc*nuwb*(v(1,j,k,tpast))*dxinv                   
      END IF

      IF (nueb > 0.0) THEN
        vadv(nx-1,j,k)=nueb*                                            &
            (v(nx-1,j,k,tpresent)-v(nx-2,j,k,tpresent))*dxinv           !& 
      ELSE
        vadv(nx-1,j,k)=-rlxlbc*nueb*                                    &
            (v(nx-1,j,k,tpast))*dxinv                                   !& 
      END IF
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Calculate rho*v'* dv/dy +rho*v* dv'/dy=
!                          avgy0( avgy0( vstr ) * dify0( v' ) )
!
!-----------------------------------------------------------------------
!
!  Wash up tem1, and tem2.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
!
!-----------------------------------------------------------------------
!  Now we calculate rho*v* dv'/dy.
!
  onvf = 0
  CALL avgy0(nvstr, onvf,                                                &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem1)
!
  onvf = 0
  CALL dify0(v(1,1,1,tpresent), onvf,                                    &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, dy, tem2)
!
!  tem1 contains avgy0(v*rho)
!  tem2 contains dv'/dy
!
  CALL aamult0(tem1, tem2,                                               &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem3)
!
!  tem2 contains avgy0(v'*rho) * dv/dy
!-----------------------------------------------------------------------
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
!
!-----------------------------------------------------------------------
  onvf=1
  CALL avgy0(tem3, onvf,                                                 &
        nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, tem1)
!
!-----------------------------------------------------------------------
!
!  Sum the x and y contributions to v advection
!  At this point tem1 contains the y contribution to the v advection
!
!-----------------------------------------------------------------------
!
!  Add the v-equation horizontal advection terms, rho*u'*dv/dx +
!  rho*u*dv'/dx (vadv) and rho*v'*dv/dy + rho*v*dv'/dy (tem1).
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        vadv(i,j,k)=vadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO
!
!
!-----------------------------------------------------------------------
!
!  Calculate rho*w'* dv/dz + rho*w* dv'/dz =
!                  avgz0( avgy0( wstr' ) * difz0( v ) )
!                  avgz0( avgy0( wstr ) * difz0( v' ) )
!
!-----------------------------------------------------------------------
!
!  Wash up tem1, and tem2.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
!
!-----------------------------------------------------------------------
!  We first calculate  rho*w* dv'/dz
!

  onvf = 1
  CALL avgy0(nwstr, onvf,                                                &
        nx,ny,nz, 1,nx-1,2,ny-1,2,nz-1, tem1)
!
  onvf = 1
  CALL difz0(v(1,1,1,tpresent), onvf,                                    &
        nx,ny,nz, 1,nx-1,2,ny-1,2,nz-1, dz, tem2)
!
!  tem1 contains avgy0(w*rho)
!  tem2 contains dv'/dz
!
  CALL aamult0(tem1, tem2,                                               &
        nx,ny,nz, 1,nx-1,2,ny-1,2,nz-1, tem3)
!
!  tem2 contains avgy0(w*rho)*dv/dz
!
!-----------------------------------------------------------------------
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
!
   tem1 = 0.
!
!-----------------------------------------------------------------------
  onvf = 0
  CALL avgz0(tem3, onvf,                                                 &
        nx,ny,nz, 1,nx-1,2,ny-1,2,nz-2, tem1)
!
  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        vadv(i,j,k)=vadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO
!
  RETURN
END SUBROUTINE tlmadvv
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TLMADVW                    ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tlmadvw(nx,ny,nz,nstepss,nbasic,u,v,w,                       &
!          ustr,vstr,wstr, wadv,  nustr,nvstr,nwstr,                    &
                           wadv,  nustr,nvstr,nwstr,                    &
!          nu,nv,nw,                                                    &
           tem1,tem2,tem3)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!
!  Calculate the advection terms of the w-equation. These terms are
!  written in equivalent advection form.
!
!  This is the tangent linear model of ADVW.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  05/31/1994
!
!  MODIFICATION HISTORY:
!
!  04/23/96 (Jidong Gao)
!  Converted into version 4.0.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nstepss  Index of total current basic state time levels.
!    nbasic   Index of the current basic state time level.
!
!    w        z component of velocity at a given time level (m/s)
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!    Basic state variables:
!    nu,nv,nw,                                     
!    nustr,nvstr,nwstr.
!
!  OUTPUT:
!
!    wadv     Advection term in w equation (kg/m**3)*(m/s)/s
!
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  INTEGER :: nt           ! The no. of t-levels of t-dependent arrays.
  INTEGER :: tpast        ! Index of time level for the past time.
  INTEGER :: tpresent     ! Index of time level for the present time.
  INTEGER :: tfuture      ! Index of time level for the future time.
  INTEGER :: nstepss      ! Index of total current basic state time levels.
  INTEGER :: nbasic       ! Index of the current basic state time level.
  INTEGER :: npast        ! Temporary variable.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)
!
  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity at a given time level(m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity at a given time level(m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity at a given time level(m/s)
!
  REAL :: nustr  (nx,ny,nz)     ! nu * rhostr
  REAL :: nvstr  (nx,ny,nz)     ! nv * rhostr
  REAL :: nwstr  (nx,ny,nz)     ! nw * rhostr
!
!
  REAL :: wadv  (nx,ny,nz)     ! w-eqn advection term (kg/m**3)*(m/s)/s
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,onvf
  INTEGER :: ibgn,iend,jbgn,jend
  REAL :: ueb, uwb, vsb, vnb, nueb,nuwb,nvsb,nvnb

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'       ! Global model control parameters
  INCLUDE 'grid.inc'
  INCLUDE 'bndry.inc'         ! Parameters for boundary conditions.
! INCLUDE 'variable.inc'      ! Basic state variables.
!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
  EXTERNAL avgx0
  EXTERNAL avg2x0
  EXTERNAL avgy0
  EXTERNAL avg2y0
  EXTERNAL avgz0
  EXTERNAL dif2x0
  EXTERNAL difx0
  EXTERNAL dif2y0
  EXTERNAL dify0
  EXTERNAL difz0
  EXTERNAL aamult0
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
        wadv = 0.
!
!-----------------------------------------------------------------------
!
!  Calculate rho*u'* dw/dx + rho*u* dw'/dx =
!            +avgx0( avgz0( ustr ) * difx0( w' ) )
!
!-----------------------------------------------------------------------
!  Now we calculate avgx0( avgz0( ustr ) * difx0( w' ) ).
!
  onvf = 1
  CALL avgz0(nustr, onvf,                                                &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem1)
!
  onvf = 1
  CALL difx0(w(1,1,1,tpresent), onvf,                                    &
        nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1, dx, tem2)
!
!  tem1 contains avgz0(u*rho)
!  tem2 contains dw'/dx
!
  CALL aamult0(tem1, tem2,                                               &
        nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1, tem3)
!
!  At this point tem2 contains avgz0(u*rho)*dw/dx
!
!-----------------------------------------------------------------------
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
!
  tem1 = 0.
  onvf = 0
  CALL avgx0(tem3, onvf,                                                 &
        nx,ny,nz, 2,nx-2, 1,ny-1, 2,nz-1, tem1)
  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        wadv(i,j,k)=wadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO
!
!
!-----------------------------------------------------------------------
!
!  Calculate rho*u* dw/dx on the east and west boundaries
!  using one-sided advection.
!
!-----------------------------------------------------------------------
!
  IF (nbasic == 1) THEN
    npast=nbasic
  ELSE
    npast=nbasic-1
  END IF

  DO k=2,nz-1
    DO j=1,ny-1
      nuwb=(nustr(1,j,k)+nustr(2,j,k)+nustr(1,j,k-1)+                   &
            nustr(2,j,k-1))*0.25
      nueb=(nustr(nx-1,j,k)+nustr(nx,j,k)+nustr(nx-1,j,k-1)             &
          +nustr(nx,j,k-1))*0.25
      IF (nuwb < 0.0) THEN
        wadv(1,j,k)=nuwb*(w(2,j,k,tpresent)-w(1,j,k,tpresent))*dxinv    !&
      ELSE
        wadv(1,j,k)=rlxlbc*nuwb*w(1,j,k,tpast)*dxinv                    !&
      END IF

      IF (nueb > 0.0) THEN
        wadv(nx-1,j,k)=nueb*                                            &
            (w(nx-1,j,k,tpresent)-w(nx-2,j,k,tpresent))*dxinv           !&
      ELSE
        wadv(nx-1,j,k)=-rlxlbc*nueb*w(nx-1,j,k,tpast)*dxinv             !&
      END IF
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Wash up tem1 and tem2.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
!
!-----------------------------------------------------------------------
!  Now we calculate rho*v* dw'/dy.
!
  onvf = 1
  CALL avgz0(nvstr, onvf,                                                &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem1)
!
  onvf = 1
  CALL dify0(w(1,1,1,tpresent), onvf,                                    &
        nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, dy, tem2)
!
!  tem1 contains avgz0(v*rho)
!  tem2 contains dw'/dy
!
  CALL aamult0(tem1, tem2,                                               &
        nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, tem3)
!
!  At this point tem2 contains avgz0(v*rho)*dw'/dy
!
!-----------------------------------------------------------------------
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
!
   tem1 = 0.
!
!-----------------------------------------------------------------------
  onvf = 0
  CALL avgy0(tem3, onvf,                                                 &
        nx,ny,nz, 1,nx-1, 2,ny-2, 2,nz-1, tem1)
!
!-----------------------------------------------------------------------
!
!  Calculate rho*v* dw/dy on the north and south boundaries
!  using one-sided advection.
!
!-----------------------------------------------------------------------
!
  IF (nbasic == 1) THEN
    npast=nbasic
  ELSE
    npast=nbasic-1
  END IF
  DO k=2,nz-1
    DO i=1,nx-1
      nvsb=(nvstr(i,1,k)+nvstr(i,2,k)+nvstr(i,1,k-1)                    &
          +nvstr(i,2,k-1))*0.25
      nvnb=(nvstr(i,ny-1,k)+nvstr(i,ny,k)+nvstr(i,ny-1,k-1)             &
          +nvstr(i,ny,k-1))*0.25
      IF (nvsb < 0.0) THEN
        tem1(i,1,k)=nvsb*(w(i,2,k,tpresent)-w(i,1,k,tpresent))*dyinv    !&
      ELSE
        tem1(i,1,k)=rlxlbc*nvsb*w(i,1,k,tpast)*dyinv                    !&
      END IF

      IF (nvnb > 0.0) THEN
        tem1(i,ny-1,k)=nvnb*                                            &
            (w(i,ny-1,k,tpresent)-w(i,ny-2,k,tpresent))*dyinv           !&
      ELSE
        tem1(i,ny-1,k)=-rlxlbc*nvnb*w(i,ny-1,k,tpast)*dyinv             !&
      END IF
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Add the w-equation horizontal advection terms, rho*u'*dw/dx
!  rho*u*dw'/dx (wadv) and
!  rho*v'*dw/dy  rho*v*dw'/dy (tem1).
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        wadv(i,j,k)=wadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO
!
!
!-----------------------------------------------------------------------
!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
!
!-----------------------------------------------------------------------
!  Now we calculate  rho*w* dw'/dz.
!
  onvf = 0
  CALL avgz0(nwstr, onvf,                                                &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem1)
!
  onvf = 0
  CALL difz0(w(1,1,1,tpresent), onvf,                                    &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, dz, tem2)
!
!  tem1 contains avgz0(w*rho)
!  tem2 contains dw'/dz
!
  CALL aamult0(tem1, tem2,                                               &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem3)
!
!  tem2 contains avgz0(w*rho)*dw'/dz
!
!-----------------------------------------------------------------------
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
!
      tem1 = 0.
!
!-----------------------------------------------------------------------
  onvf = 1
  CALL avgz0(tem3, onvf,                                                 &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem1)

!------------------------------------------------------------------------
!
!  Add the w-equation vertical advection term rho*w*dw/dz (tem1) to the
!  horizontal advection terms.
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        wadv(i,j,k)=wadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE tlmadvw

!----------------------------------------------------------------------
!
!   ADJ for advuvw
!
!----------------------------------------------------------------------
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADTLMADVUVW                ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!
!

SUBROUTINE adtlmadvuvw(nx,ny,nz,nstepss,nbasic,                         &
           u,v,w,wcont,rhostr,ubar,vbar,nwcont,                         &
!          nu,nv,nw, ubk,vbk,wbk,                                       &
                     ubk,vbk,wbk,                                       &
           nustr,nvstr,nwstr,                                           &
           uadv,vadv,wadv,                                              &
           ustr,vstr,wstr,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operations on TLMADVUVW. TLMADVUVW
!  coordinates the calculation of the advection terms uadv, vadv and wadv
!  of the u, v and w equations of TLM. These terms are written in
!  equivalent advection form.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  11/07/1994
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUTS:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nstepss  Index of total current basic state time levels.
!    nbasic   Index of the current basic state time level.
!
!    u        x component of velocity at all time levels (m/s)
!    v        y component of velocity at all time levels (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates at all time levels (m/s).
!    wcont    Contravariant vertical velocity (m/s)
!    nwcont   Contravariant vertical velocity (m/s) of the basic states.
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    Basic states.
!
!  OUTPUTS:
!
!    uadv     u-equation advection terms  (kg/m**3)*(m/s)/s
!    vadv     v-equation advection terms  (kg/m**3)*(m/s)/s
!    wadv     w-equation advection terms  (kg/m**3)*(m/s)/s
!    nustr,nvstr,nwstr: nustr=rhostr*nu, nvstr=rhostr*nv, nwstr=rhostr*nw
!
!  WORK ARRAYS:
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nt           ! The no. of t-levels of t-dependent arrays.
  INTEGER :: tpast        ! Index of time level for the past time.
  INTEGER :: tpresent     ! Index of time level for the present time.
  INTEGER :: tfuture      ! Index of time level for the future time.
  INTEGER :: nstepss      ! Index of total current basic state time levels.
  INTEGER :: nbasic       ! Index of the current basic state time level.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity (m/s)
  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)
  REAL :: nwcont (nx,ny,nz)    ! Contravariant vertical velocity (m/s) of the basic states
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: ubar  (nx,ny,nz)
  REAL :: vbar  (nx,ny,nz)
!
  REAL :: uadv  (nx,ny,nz)     ! u-eqn advection terms (kg/m**3)*(m/s)/s
  REAL :: vadv  (nx,ny,nz)     ! v-eqn advection terms (kg/m**3)*(m/s)/s
  REAL :: wadv  (nx,ny,nz)     ! w-eqn advection terms (kg/m**3)*(m/s)/s
!
  REAL :: ubk   (nx,ny,nz)     ! u-eqn advection terms (kg/m**3)*(m/s)/s
  REAL :: vbk   (nx,ny,nz)     ! v-eqn advection terms (kg/m**3)*(m/s)/s
  REAL :: wbk   (nx,ny,nz)     ! w-eqn advection terms (kg/m**3)*(m/s)/s
!
  REAL :: ustr  (nx,ny,nz)     ! u * rhostr
  REAL :: vstr  (nx,ny,nz)     ! v * rhostr
  REAL :: wstr  (nx,ny,nz)     ! w * rhostr
!
  REAL :: nustr  (nx,ny,nz)     ! nu * rhostr
  REAL :: nvstr  (nx,ny,nz)     ! nv * rhostr
  REAL :: nwstr  (nx,ny,nz)     ! nw * rhostr
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!-----------------------------------------------------------------------
!
! INCLUDE 'variable.inc'
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,n
  INTEGER :: tlevel
  REAL :: sum1, sum2, sum3
!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  tlevel=tpresent
  nustr = 0.0; nvstr = 0.0; nwstr = 0.0
   ustr = 0.0;  vstr = 0.0;  wstr = 0.0
!
!-----------------------------------------------------------------------
!
!  Calculate nustr=rhostr*nu, nvstr=rhostr*nv, nwstr=rhostr*nw
!  It is linear!!!
!
!-----------------------------------------------------------------------
!
if (1==0    ) then
     sum1  = 0.0; sum2  = 0.0; sum3  = 0.
   do i=1, nx
   do j=1, ny
   do k=1, nz
!    sum1 = sum1 +nu(i,j,k,1)*nu(i,j,k,1)
!    sum2 = sum2 +nv(i,j,k,1)*nv(i,j,k,1)
!    sum3 = sum3 +nw(i,j,k,1)*nw(i,j,k,1)
     sum1 = sum1 +ubar(i,j,k)*ubar(i,j,k)
     sum2 = sum2 +vbar(i,j,k)*vbar(i,j,k)
   end do
   end do
   end do
     print*,'sum1==u===========',sum1
     print*,'sum2==v===========',sum2
     print*,'sum3==w===========',sum3
   stop
end if

! CALL uvwrho0(nx,ny,nz,nu(1,1,1,nbasic),nv(1,1,1,nbasic),              &
!             nwcont,rhostr,nustr,nvstr,nwstr)

  CALL uvwrho0(nx,ny,nz,ubk,vbk,                                        &
              nwcont,rhostr,nustr,nvstr,nwstr)
!
!-----------------------------------------------------------------------
   tem1 = 0.0;  tem2 = 0.0;  tem3 = 0.0
! CALL adtlmadvw(nx,ny,nz,nstepss,nbasic,u,v,w,                         &
!     ustr,vstr,wstr, wadv,  nustr,nvstr,nwstr,                         &
!     nu,nv,nw, tem1,tem2,tem3)
  CALL adtlmadvw(nx,ny,nz,nstepss,nbasic,u,v,w,                         &
                      wadv,  nustr,nvstr,nwstr,                         &
                tem1,tem2,tem3)
 
   tem1 = 0.0; tem2 = 0.0;tem3 = 0.
! CALL adtlmadvv(nx,ny,nz,nstepss,nbasic,u,v,w,                         &
!     ustr,vstr,wstr, vbar,vadv, nustr,nvstr,nwstr,                     &
!     nu,nv,nw, tem1,tem2,tem3)
  CALL adtlmadvv(nx,ny,nz,nstepss,nbasic,u,v,w,                         &
                      vbar,vadv, nustr,nvstr,nwstr,                     &
                tem1,tem2,tem3)
 
   tem1 = 0.0; tem2 = 0.0;tem3 = 0.
! CALL adtlmadvu(nx,ny,nz, nstepss,nbasic,u,v,w,                        &
!     ustr,vstr,wstr, ubar, uadv,nustr,nvstr,nwstr,                     &
!     nu,nv,nw, tem1,tem2,tem3)
  CALL adtlmadvu(nx,ny,nz, nstepss,nbasic,u,v,w,                        &
                      ubar, uadv,nustr,nvstr,nwstr,                     &
                tem1,tem2,tem3)

!  tem1 = 0.0; tem2 = 0.0;tem3 = 0.
! CALL aduvwrho(nx,ny,nz,u(1,1,1,tlevel),v(1,1,1,tlevel),               &
!             wcont,rhostr,ustr,vstr,wstr,tem1,tem2,tem3)
  RETURN
END SUBROUTINE adtlmadvuvw

!##################################################################
!##################################################################
!######                                                      ######
!######                 SUBROUTINE ADTLMADVU                 ######
!######                                                      ######
!######                 Copyright (c) 1994                   ######
!######     Center for Analysis and Prediction of Storms     ######
!######     University of Oklahoma.  All rights reserved.    ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adtlmadvu(nx,ny,nz,nstepss,nbasic,                           &
!          u,v,w,ustr,vstr,wstr, ubar,uadv,nustr,nvstr,nwstr,           &
           u,v,w,                ubar,uadv,nustr,nvstr,nwstr,           &
!          nu,nv,nw,                                                    &
           tem1,tem2,tem3)
!
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate u-equation advection terms of the TLM. These terms are written
!  in equivalent advection form.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  05/25/1994
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nstepss  Index of total current basic state time levels.
!    nbasic   Index of the current basic state time level.
!
!    u        x component of velocity at a given time level (m/s)
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!    ustr     nu * rhostr
!    vstr     nv * rhostr
!    wstr     nw * rhostr
!
!  OUTPUT:
!
!    uadv     u-equation advection terms (kg/m**3)*(m/s)/s
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
  INTEGER :: nx,ny,nz

  INTEGER :: nt           ! The no. of t-levels of t-dependent arrays.
  INTEGER :: tpast        ! Index of time level for the past time.
  INTEGER :: tpresent     ! Index of time level for the present time.
  INTEGER :: tfuture      ! Index of time level for the future time.
  INTEGER :: nstepss      ! Index of total current basic state time levels.
  INTEGER :: nbasic       ! Index of the current basic state time level.
  INTEGER :: npast        ! Temporary variable.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity at a given time level(m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity at a given time level(m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity at a given time level(m/s)
! REAL :: ustr  (nx,ny,nz)     ! u*rhostr
! REAL :: vstr  (nx,ny,nz)     ! v*rhostr
! REAL :: wstr  (nx,ny,nz)     ! w*rhostr
!
  REAL :: nustr  (nx,ny,nz)     ! nu * rhostr
  REAL :: nvstr  (nx,ny,nz)     ! nv * rhostr
  REAL :: nwstr  (nx,ny,nz)     ! nw * rhostr
!
  REAL :: ubar  (nx,ny,nz)
!
  REAL :: uadv  (nx,ny,nz)     ! Advection term of the u-eq.
                               ! (kg/m**3)*(m/s)/s
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,onvf
  INTEGER :: ibgn,iend,jbgn,jend
  REAL :: vsb, vnb,nvsb,nvnb
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'       ! Global model control parameters
  INCLUDE 'grid.inc' 
  INCLUDE 'bndry.inc'         ! Parameters for boundary conditions.
! INCLUDE 'variable.inc'      ! Basic state variables.
!   real bem1(nx,ny,nz),bem2(nx,ny,nz),bem3(nx,ny,nz)
!   real bem4(nx,ny,nz),bem5(nx,ny,nz)

  REAL :: sum1,sum2,sum3
!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
     tem1= 0.
     tem2= 0.
     tem3= 0.
!
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        tem1(i,j,k) = uadv(i,j,k)
      END DO
    END DO
  END DO

!
!-----------------------------------------------------------------------
!  Adjoint on avgz0( avgx0( wstr ) * difz0 ( u' ) ).
!
!  tem2 contains avgx0(nw*rho) * du/dz
!
  onvf = 0
  CALL adavgz0(tem2, onvf,                                               &
        nx,ny,nz,2,nx-1,1,ny-1, 2,nz-2, tem1)
!
        tem1 = 0.
!
  onvf = 1
  CALL avgx0(nwstr, onvf,                                                &
        nx,ny,nz,2,nx-1,1,ny-1, 2,nz-1, tem1)
!
!  tem1 contains avgx0(nw*rho)
!  tem2 contains du/dz
!
  CALL adaamult0(tem1, tem3,                                             &
        nx,ny,nz,2,nx-1,1,ny-1, 2,nz-1, tem2)
!
!
  onvf = 1
  CALL addifz0(u(1,1,1,tpresent), onvf,                                  &
        nx,ny,nz,2,nx-1,1,ny-1, 2,nz-1, dz, tem3)
!
!-----------------------------------------------------------------------
!
!  Wash up tem1 and tem2.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
!
!-----------------------------------------------------------------------
!
!  Adjoint on adding the u-equation horizontal advection terms, rho*u*du/dx (uadv) and
!  rho*v*du/dy (tem1).
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        tem1(i,j,k)=uadv(i,j,k)
      END DO
    END DO
  END DO
!-----------------------------------------------------------------------
!
!  Adjoint on calculating rho*v* du/dy on the north and south boundaries using
!  one sided advection. rlxlbc is a constant coefficient.
!
!-----------------------------------------------------------------------
!
  IF (nbasic == 1) THEN
    npast=nbasic
  ELSE
    npast=nbasic-1
  END IF

  DO k=2,nz-2
    DO i=2,nx-1

      nvsb=(nvstr(i-1,1,k)+nvstr(i-1,2,k)                               &
          +nvstr(i,1,k)+nvstr(i,2,k))*0.25
      nvnb=(nvstr(i-1,ny-1,k)+nvstr(i-1,ny,k)                           &
          +nvstr(i,ny-1,k)+nvstr(i,ny,k))*0.25

      IF (nvnb > 0.0) THEN
        u(i,ny-1,k,tpresent) = u(i,ny-1,k,tpresent)                     &
                        + nvnb*tem1(i,ny-1,k)*dyinv
        u(i,ny-2,k,tpresent) = u(i,ny-2,k,tpresent)                     &
                        - nvnb*tem1(i,ny-1,k)*dyinv 
        tem1(i,ny-1,k) = 0.0
      ELSE
        u(i,ny-1,k,tpast) = u(i,ny-1,k,tpast)                           &
                        -rlxlbc*nvnb*tem1(i,ny-1,k)*dyinv
        tem1(i,ny-1,k) = 0.0
        
      END IF

      IF (nvsb < 0.0) THEN
        u(i,2,k,tpresent) = u(i,2,k,tpresent)                           &
                         + nvsb*tem1(i,1,k)*dyinv
        u(i,1,k,tpresent) = u(i,1,k,tpresent)                           &
                         - nvsb*tem1(i,1,k)*dyinv
        tem1(i,1,k) = 0.0
      ELSE
        u(i,1,k,tpast) = u(i,1,k,tpast)                                 &
                   + rlxlbc*nvsb*tem1(i,1,k)*dyinv
        tem1(i,1,k) = 0.0
      END IF

    END DO
  END DO
!
  tem2 = 0.0
  tem3 = 0.0
  onvf = 0
  CALL adavgy0(tem2, onvf,                                               &
        nx,ny,nz, 2,nx-1, 2,ny-2, 2,nz-2, tem1)
!
!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating
!             = rho*v*du'/dy + rho*v'* du/dy
!             = avgx0( avgx0( vstr' )*dify0( u ) )+
!               avgx0( avgx0( vstr )*dify0( u' ) )
!   where v'=v, v=nv, u'=u and u=nu.
!
!-----------------------------------------------------------------------
!  Adjoint on rho*v*du'/dy.
!
  onvf = 1
  tem1 = 0.0
  CALL avgx0(nvstr, onvf,                                                &
        nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, tem1)
!
  CALL adaamult0(tem1, tem3,                                             &
        nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, tem2)
!
  onvf = 1
  CALL addify0(u(1,1,1,tpresent), onvf,                                  &
        nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, dy, tem3)
!
!
!-----------------------------------------------------------------------
!  Adjoint on rho*v'* du/dy.
!
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating
!                               rho*u* du'/dx
!               avgx0( avgx0( ustr )*difx0( u' ) )
!   where u'=u and u=nu.
!
!-----------------------------------------------------------------------
!  Adjoint on avgx0( avgx0( ustr )*difx0( u' ) ).
!
  tem1 = 0.0
  tem2 = 0.0
  tem3 = 0.0
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        tem1(i,j,k)=uadv(i,j,k)
      END DO
    END DO
  END DO
!
  onvf = 1
  CALL adavgx0(tem2, onvf,                                               &
        nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, tem1)
!
!-----------------------------------------------------------------------
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
        tem1 = 0.
!
  onvf = 0
  CALL avgx0(nustr, onvf,                                                &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem3)
!
!
  CALL adaamult0(tem3, tem1,                                             &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem2)
!
  onvf = 0
  CALL addifx0(u(1,1,1,tpresent), onvf,                                  &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, dx, tem1)
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
        uadv = 0.

  RETURN
END SUBROUTINE adtlmadvu
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADTLMADVV                  ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE adtlmadvv(nx,ny,nz,nstepss,nbasic,u,v,w,                     &
!          ustr,vstr,wstr, vbar,vadv, nustr,nvstr,nwstr,                &
                           vbar,vadv, nustr,nvstr,nwstr,                &
!          nu,nv,nw,                                                    &
           tem1,tem2,tem3)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the v-equation advection terms of the TLM.  These terms are written
!  in equivalent advection form.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  11/03/1994
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUTS:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nstepss  Index of total current basic state time levels.
!    nbasic   Index of the current basic state time level.
!
!    v        y component of velocity at a given time level (m/s)
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!  OUTPUT:
!
!    vadv     v equation advection terms (kg/m**3)*(m/s)/s
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
  INTEGER :: nx,ny,nz

  INTEGER :: nt           ! The no. of t-levels of t-dependent arrays.
  INTEGER :: tpast        ! Index of time level for the past time.
  INTEGER :: tpresent     ! Index of time level for the present time.
  INTEGER :: tfuture      ! Index of time level for the future time.
  INTEGER :: nstepss      ! Index of total current basic state time levels.
  INTEGER :: nbasic       ! Index of the current basic state time level.
  INTEGER :: npast        ! Temporary variable.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity at a given time level(m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity at a given time level(m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity at a given time level(m/s)
! REAL :: ustr  (nx,ny,nz)     ! u*rhostr
! REAL :: vstr  (nx,ny,nz)     ! v*rhostr
! REAL :: wstr  (nx,ny,nz)     ! w*rhostr
!
  REAL :: nustr  (nx,ny,nz)     ! nu * rhostr
  REAL :: nvstr  (nx,ny,nz)     ! nv * rhostr
  REAL :: nwstr  (nx,ny,nz)     ! nw * rhostr
!
  REAL :: vbar  (nx,ny,nz)
!
  REAL :: vadv  (nx,ny,nz)     ! Advection term of the v-eq.
                               ! (kg/m**3)*(m/s)/s
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,onvf
  INTEGER :: ibgn,iend,jbgn,jend
  REAL :: ueb, uwb,  nuwb,nueb

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'       ! Global model control parameters
  INCLUDE 'grid.inc'
  INCLUDE 'bndry.inc'         ! Parameters for boundary conditions.
! INCLUDE 'variable.inc'      ! Basic state variables.
!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!-----------------------------------------------------------------------
!
!  Adjoint on adding v-equation vertical advection term rho*w*dv'/dz (tem1) +
!  rho*w'*dv/dz (tem3) to the horizontal advection terms.
!
!-----------------------------------------------------------------------
!
!  Wash up tem2, and tem3.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
!
  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        tem1(i,j,k) = vadv(i,j,k)
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!  Adjoint on callculating rho*w* dv'/dz.
!-----------------------------------------------------------------------
  onvf = 0
  CALL adavgz0(tem3, onvf,                                               &
        nx,ny,nz, 1,nx-1,2,ny-1,2,nz-2, tem1)
!-----------------------------------------------------------------------
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
!
!
  onvf = 1
  CALL avgy0(nwstr, onvf,                                                &
        nx,ny,nz, 1,nx-1,2,ny-1,2,nz-1, tem1)
!
!
  CALL adaamult0(tem1, tem2,                                             &
        nx,ny,nz, 1,nx-1,2,ny-1,2,nz-1, tem3)
!
  onvf = 1
  CALL addifz0(v(1,1,1,tpresent), onvf,                                  &
        nx,ny,nz, 1,nx-1,2,ny-1,2,nz-1, dz, tem2)
!
!-----------------------------------------------------------------------
!
!  Wash up tem1, and tem2.
!
!-----------------------------------------------------------------------
!
      tem1 = 0.
!
!
!-----------------------------------------------------------------------
!
!  Adjoint on adding v-equation horizontal advection terms, rho*u'*dv/dx +
!  rho*u*dv'/dx (vadv) and rho*v'*dv/dy + rho*v*dv'/dy (tem1).
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=vadv(i,j,k)
      END DO
    END DO
  END DO
!
!
!-----------------------------------------------------------------------
!
  tem3 = 0.0
  onvf = 1
  CALL adavgy0(tem3, onvf,                                               &
        nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, tem1)
!
!-----------------------------------------------------------------------
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating
!  rho*v'* dv/dy +rho*v* dv'/dy=
!                          avgy0( avgy0( vstr' ) * dify0( v ) )
!                          avgy0( avgy0( vstr ) * dify0( v' ) )
!
!-----------------------------------------------------------------------
!  Adjoint on calculating rho*v* dv'/dy.
  onvf = 0
  CALL avgy0(nvstr, onvf,                                                &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem1)
!
  CALL adaamult0(tem1, tem2,                                             &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem3)
!
        tem3 = 0.
!
  onvf = 0
  CALL addify0(v(1,1,1,tpresent), onvf,                                               &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, dy, tem2)
!
If (1==1) THEN
!
!-----------------------------------------------------------------------
!
!  Wash up tem1, and tem2.
!
!-----------------------------------------------------------------------
!
   tem1 = 0.
   tem2 = 0.
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating rho*u* dv/dx at the east and west boundaries
!  using one-sided advection.
!
!-----------------------------------------------------------------------
  IF (nbasic == 1) THEN
    npast=nbasic
  ELSE
    npast=nbasic-1
  END IF
!
  DO k=2,nz-2
    DO j=2,ny-1
      nuwb=(nustr(1,j,k)+nustr(2,j,k)+nustr(1,j-1,k)+                   &
           nustr(2,j-1,k))*0.25
      nueb=(nustr(nx-1,j,k)+nustr(nx,j,k)+nustr(nx-1,j-1,k)             &
          +nustr(nx,j-1,k))*0.25

      IF (nueb > 0.0) THEN

        v(nx-1,j,k,tpresent) = v(nx-1,j,k,tpresent)                     &
                                     + nueb*vadv(nx-1,j,k)*dxinv
        v(nx-2,j,k,tpresent) = v(nx-2,j,k,tpresent)                     &
                                     - nueb*vadv(nx-1,j,k)*dxinv
      ELSE
 
        v(nx-1,j,k,tpast) = v(nx-1,j,k,tpast)                           &
                                -rlxlbc*nueb*vadv(nx-1,j,k)*dxinv
      END IF

      IF (nuwb < 0.0) THEN
 
        v(2,j,k,tpresent) = v(2,j,k,tpresent)                           &
                                     + nuwb*vadv(1,j,k)*dxinv
        v(1,j,k,tpresent) = v(1,j,k,tpresent)                           &
                                     - nuwb*vadv(1,j,k)*dxinv
      ELSE
 
        v(1,j,k,tpast) = v(1,j,k,tpast)                                 &
                                 + rlxlbc*nuwb*vadv(1,j,k)*dxinv 
      END IF
    END DO
  END DO
!-----------------------------------------------------------------------
!
!  Adjoint on calculating rho*u'* dv/dx  +rho*u * dv'/dx
!         = avgx0( avgy0( ustr' ) * difx0( v ) )
!         + avgx0( avgy0( ustr ) * difx0( v' ) )
!
!-----------------------------------------------------------------------
!  Adjoint on avgx0( avgy0( ustr ) * difx0( v' ) ).
!

        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
!
  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=vadv(i,j,k)
      END DO
    END DO
  END DO

  onvf = 0
  CALL adavgx0(tem3, onvf,                                               &
        nx,ny,nz, 2,nx-2, 2,ny-1, 2,nz-2, tem1)
!
!-----------------------------------------------------------------------
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.
!
!-----------------------------------------------------------------------
  onvf = 1
  CALL avgy0(nustr, onvf,                                                &
        nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, tem1)
!
  CALL adaamult0(tem1, tem2,                                             &
        nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, tem3)
!
  onvf = 1
  CALL addifx0(v(1,1,1,tpresent), onvf,                                  &
        nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, dx, tem2)
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
        vadv = 0.
END IF !!(1==0)
!
  RETURN
END SUBROUTINE adtlmadvv
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADTLMADVW                  ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!
!
SUBROUTINE adtlmadvw(nx,ny,nz,nstepss,nbasic,u,v,w,                     &
!          ustr,vstr,wstr, wadv,  nustr,nvstr,nwstr,                    &
!          nu,nv,nw,                                                    &
                           wadv,  nustr,nvstr,nwstr,                    &
           tem1,tem2,tem3)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!
!  Perform the adjoint operations on TLMADVW. TLMADVW calculates
!  the advection terms of the w-equation. These terms are
!  written in equivalent advection form.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  11/04/1996
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nstepss  Index of total current basic state time levels.
!    nbasic   Index of the current basic state time level.
!
!    w        z component of velocity at a given time level (m/s)
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!  OUTPUT:
!
!    wadv     Advection term in w equation (kg/m**3)*(m/s)/s
!
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  INTEGER :: nt           ! The no. of t-levels of t-dependent arrays.
  INTEGER :: tpast        ! Index of time level for the past time.
  INTEGER :: tpresent     ! Index of time level for the present time.
  INTEGER :: tfuture      ! Index of time level for the future time.
  INTEGER :: nstepss      ! Index of total current basic state time levels.
  INTEGER :: nbasic       ! Index of the current basic state time level.
  INTEGER :: npast        ! Temporary variable.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)
!
  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity at a given time level(m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity at a given time level(m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity at a given time level(m/s)
! REAL :: ustr  (nx,ny,nz)     ! u*rhostr
! REAL :: vstr  (nx,ny,nz)     ! v*rhostr
! REAL :: wstr  (nx,ny,nz)     ! w*rhostr
!
  REAL :: nustr  (nx,ny,nz)     ! nu * rhostr
  REAL :: nvstr  (nx,ny,nz)     ! nv * rhostr
  REAL :: nwstr  (nx,ny,nz)     ! nw * rhostr
!
!
  REAL :: wadv  (nx,ny,nz)     ! w-eqn advection term (kg/m**3)*(m/s)/s
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,onvf
  INTEGER :: ibgn,iend,jbgn,jend
  REAL :: ueb, uwb, vsb, vnb, nueb,nuwb,nvsb,nvnb

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'       ! Global model control parameters
  INCLUDE 'grid.inc'
  INCLUDE 'bndry.inc'         ! Parameters for boundary conditions.
! INCLUDE 'variable.inc'      ! Basic state variables.
!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!------------------------------------------------------------------------
!
!  Adjoint on the w-equation vertical advection term
!  rho*w*dw/dz (tem1) to the
!  horizontal advection terms.
!
!-----------------------------------------------------------------------
!
!
  tem1 = 0.
  tem2 = 0.
  tem3 = 0.

  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=wadv(i,j,k)
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating rho*w'* dw/dz + rho*w* dw'/dz =
!                    avgz0( avgz0( wstr' ) * difz0( w ) )
!                  + avgz0( avgz0( wstr ) * difz0( w' ) )
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating rho*w* dw'/dz.
!
!C#######################################################################
  onvf = 1
  CALL adavgz0(tem3, onvf,                                               &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem1)
!
!-----------------------------------------------------------------------
!
!  Wash up tem3.
!
!-----------------------------------------------------------------------
!
  tem1 = 0.
!
  onvf = 0
  CALL avgz0(nwstr, onvf,                                                &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem1)
!
  CALL adaamult0(tem1, tem2,                                             &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem3)
!
  onvf = 0
  CALL addifz0(w(1,1,1,tpresent), onvf,                                  &
        nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, dz, tem2)
!
!-----------------------------------------------------------------------
!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
!
  tem1 = 0.
  tem2 = 0.
  tem3 = 0.
!
!-----------------------------------------------------------------------
!
!  Adjoint on adding the w-equation horizontal advection terms, rho*u'*dw/dx
!  rho*u*dw'/dx (wadv) and
!  rho*v'*dw/dy  rho*v*dw'/dy (tem1).
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=wadv(i,j,k)
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating rho*v* dw/dy on the north and south boundaries
!  using one-sided advection.
!
!-----------------------------------------------------------------------
!
  IF (nbasic == 1) THEN
    npast=nbasic
  ELSE
    npast=nbasic-1
  END IF

  DO k=2,nz-1
    DO i=1,nx-1
      nvsb=(nvstr(i,1,k)+nvstr(i,2,k)+nvstr(i,1,k-1)                    &
          +nvstr(i,2,k-1))*0.25
      nvnb=(nvstr(i,ny-1,k)+nvstr(i,ny,k)+nvstr(i,ny-1,k-1)             &
          +nvstr(i,ny,k-1))*0.25

      IF (nvnb > 0.0) THEN
!
        w(i,ny-1,k,tpresent)=w(i,ny-1,k,tpresent)                       &
                                      +nvnb*tem1(i,ny-1,k)*dyinv
        w(i,ny-2,k,tpresent)=w(i,ny-2,k,tpresent)                       &
                                      -nvnb*tem1(i,ny-1,k)*dyinv
      ELSE
!
        w(i,ny-1,k,tpast) = w(i,ny-1,k,tpast)                           &
                          - rlxlbc*nvnb*tem1(i,ny-1,k)*dyinv 
      END IF

      IF (nvsb < 0.0) THEN
!
        w(i,2,k,tpresent)=w(i,2,k,tpresent)                             &
                                 + nvsb*tem1(i,1,k)*dyinv
        w(i,1,k,tpresent)=w(i,1,k,tpresent)                             &
                                 - nvsb*tem1(i,1,k)*dyinv
      ELSE
!
        w(i,1,k,tpast)=w(i,1,k,tpast)                                   &
                     + rlxlbc*nvsb*tem1(i,1,k)*dyinv
      END IF
    END DO
  END DO

!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating rho*v'* dw/dy + rho*v* dw'/dy
!        = avgy0( avgz0( vstr' ) * dify0 ( w ) )
!          avgy0( avgz0( vstr ) * dify0 ( w' ) )
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating rho*v* dw'/dy.
!
!-----------------------------------------------------------------------
  tem3 = 0.0
  onvf = 0
  CALL adavgy0(tem3, onvf,                                               &
        nx,ny,nz, 1,nx-1, 2,ny-2, 2,nz-1, tem1)
!
!-----------------------------------------------------------------------
!
!  Wash up tem3.
!
!-----------------------------------------------------------------------
!
  tem1 = 0.
  onvf = 1
  CALL avgz0(nvstr, onvf,                                                &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem1)
!
  CALL adaamult0(tem1, tem2,                                             &
        nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, tem3)
!
  tem3 = 0.
!
  onvf = 1
  CALL addify0(w(1,1,1,tpresent), onvf,                                  &
        nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, dy, tem2)
!
!-----------------------------------------------------------------------
!
!  Wash up tem1 and tem2.
!
!-----------------------------------------------------------------------
!
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating rho*u* dw/dx on the east and west boundaries
!  using one-sided advection.
!
!-----------------------------------------------------------------------
!
!
  IF (nbasic == 1) THEN
    npast=nbasic
  ELSE
    npast=nbasic-1
  END IF

  DO k=2,nz-2
    DO j=1,ny-1
      nuwb=(nustr(1,j,k)+nustr(2,j,k)+nustr(1,j,k-1)+                   &
            nustr(2,j,k-1))*0.25
      nueb=(nustr(nx-1,j,k)+nustr(nx,j,k)+nustr(nx-1,j,k-1)             &
          +nustr(nx,j,k-1))*0.25

      IF (nueb > 0.0) THEN
!
        w(nx-1,j,k,tpresent)=w(nx-1,j,k,tpresent)                       &
                                  + nueb*wadv(nx-1,j,k)*dxinv
        w(nx-2,j,k,tpresent)=w(nx-2,j,k,tpresent)                       &
                                  - nueb*wadv(nx-1,j,k)*dxinv
      ELSE
!
        w(nx-1,j,k,tpast) = w(nx-1,j,k,tpast)                           &
                         - rlxlbc*nueb*wadv(nx-1,j,k)*dxinv
      END IF

      IF (nuwb < 0.0) THEN
!
        w(2,j,k,tpresent)=w(2,j,k,tpresent)                             &
                                    + nuwb*wadv(1,j,k)*dxinv
        w(1,j,k,tpresent)=w(1,j,k,tpresent)                             &
                                    - nuwb*wadv(1,j,k)*dxinv

      ELSE
!
        w(1,j,k,tpast) = w(1,j,k,tpast)                                 &
                         + rlxlbc*nuwb*wadv(1,j,k)*dxinv

      END IF

    END DO
  END DO

!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating rho*u'* dw/dx + rho*u* dw'/dx =
!             avgx0( avgz0( ustr' ) * difx0( w ) )
!            +avgx0( avgz0( ustr ) * difx0( w' ) )
!
!-----------------------------------------------------------------------
!  Adjoint on calculating avgx0( avgz0( ustr ) * difx0( w' ) ).
!
!
!
  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=wadv(i,j,k)
      END DO
    END DO
  END DO
!
  tem3 =0.0
  onvf = 0
  CALL adavgx0(tem3, onvf,                                               &
        nx,ny,nz, 2,nx-2, 1,ny-1, 2,nz-1, tem1)
!
!-----------------------------------------------------------------------
!
!  Wash up tem1.
!
!-----------------------------------------------------------------------
!
  tem1 = 0.
  onvf = 1
  CALL avgz0(nustr, onvf,                                                &
        nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem1)
!
  tem2 = 0.0
  CALL adaamult0(tem1, tem2,                                             &
        nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1, tem3)
!
!
  onvf = 1
  CALL addifx0(w(1,1,1,tpresent), onvf,                                  &
        nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1, dx, tem2)
!       
        tem1 = 0.
        tem2 = 0.
        tem3 = 0.
        wadv = 0.
!
  RETURN
END SUBROUTINE adtlmadvw
