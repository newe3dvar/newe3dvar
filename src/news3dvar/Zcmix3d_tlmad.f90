
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADTLMCMIX2UVW              ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adtlmcmix2uvw(nx,ny,nz,u,v,w, ubar,vbar,rhostr,              &
           umix,vmix,wmix,                                              &
           ustr,vstr,wstr,tem1,tem2,tem3,tem4,tem5,tem6)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the second order computational mixing terms for the momentum
!  equations. Computational mixing is applied to velocity perturbations
!  only. These terms are placed in the arrays umix, vmix and wmix.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/3/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  4/2/92 (M. Xue and H. Jin)
!  modify for using terrain
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        Verticle component of Cartesian velocity at a
!             given time level (m/s)
!
!    ubar     Base state zonal velocity component (m/s)
!    vbar     Base state meridional velocity component (m/s)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!
!    umix     Array containing turbulent mixing on u
!    vmix     Array containing turbulent mixing on v
!    wmix     Array containing turbulent mixing on w
!
!  OUTPUT:
!
!    umix     Turbulent and computational mixing on u.
!    vmix     Turbulent and computational mixing on v.
!    wmix     Turbulent and computational mixing on w.
!
!  WORK ARRAYS:
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.

  REAL :: umix  (nx,ny,nz)     ! Total mixing in u-eq.
  REAL :: vmix  (nx,ny,nz)     ! Total mixing in v-eq.
  REAL :: wmix  (nx,ny,nz)     ! Total mixing in w-eq.

  REAL :: ustr  (nx,ny,nz)     ! u * rhostr
  REAL :: vstr  (nx,ny,nz)     ! v * rhostr
  REAL :: wstr  (nx,ny,nz)     ! w * rhostr

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
  REAL :: tem4  (nx,ny,nz)     ! Temporary work array
  REAL :: tem5  (nx,ny,nz)     ! Temporary work array
  REAL :: tem6  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k, onvf
  REAL :: dxinv2, dyinv2
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'phycst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'globcst.inc'
!
!-----------------------------------------------------------------------
!

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  IF the coefficients of computational mixing in both horizontal
!  and vertical directions are zero, exit this subroutine.
!
!-----------------------------------------------------------------------
!

  IF( cfcmh2 == 0.0 .AND. cfcmv2 == 0.0 ) RETURN


  dxinv2 = dxinv**2
  dyinv2 = dyinv**2
!
!-----------------------------------------------------------------------
!
!  Calculate ustr=rhostr*u', vstr=rhostr*v', wstr=rhostr*w'
!
!-----------------------------------------------------------------------
!
     tem1=0.0;tem2=0.0;tem3=0.0
     ustr=0.0;vstr=0.0;wstr=0.0
!
  CALL rhouvw0(nx,ny,nz,rhostr,tem4,tem5,tem6)
  IF( cfcmh2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the second order
!  horizontal computational mixing on u'
!
!-----------------------------------------------------------------------
!
    DO k=2,nz-2
      DO j=2,ny-2
        DO i=2,nx-1
!         umix(i,j,k)=umix(i,j,k)+cfcmh2*(tem1(i,j,k)+tem2(i,j,k))
          tem1(i,j,k)=tem1(i,j,k)+cfcmh2*umix(i,j,k)
          tem2(i,j,k)=tem2(i,j,k)+cfcmh2*umix(i,j,k)
        END DO
      END DO
    END DO

    DO k=2,nz-2
      DO i=2,nx-1
!       umix(i,  1 ,k)=umix(i,  1 ,k)+cfcmh2*(tem1(i,  1 ,k)            &
!               +(-ustr(i,1,k)+ustr(i,2,k))*dyinv2)
!
        tem1(i,  1 ,k)=tem1(i,  1 ,k)+cfcmh2*umix(i,  1 ,k)
        ustr(i,1,k)=ustr(i,1,k)-cfcmh2*umix(i,  1 ,k)*dyinv2
        ustr(i,2,k)=ustr(i,2,k)+cfcmh2*umix(i,  1 ,k)*dyinv2

!       umix(i,ny-1,k)=umix(i,ny-1,k)+cfcmh2*(tem1(i,ny-1,k)            &
!               +(-ustr(i,ny-1,k)+ustr(i,ny-2,k))*dyinv2)
!
        tem1(i,ny-1,k)=tem1(i,ny-1,k)+cfcmh2*umix(i,ny-1,k)
        ustr(i,ny-1,k)=ustr(i,ny-1,k)-cfcmh2*umix(i,ny-1,k)*dyinv2
        ustr(i,ny-2,k)=ustr(i,ny-2,k)+cfcmh2*umix(i,ny-1,k)*dyinv2
      END DO
    END DO

    CALL addifxx0(ustr, nx,ny,nz, 2,nx-1,1,ny-1,2,nz-2, dx, tem1)
    CALL addifyy0(ustr, nx,ny,nz, 2,nx-1,2,ny-2,2,nz-2, dy, tem2)
    tem1=0.0; tem2=0.0

  END IF
!
  IF( cfcmv2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the second order
!  vertical computational mixing on u'
!
!-----------------------------------------------------------------------
     tem1=0.0
    DO k=2,nz-2
      DO j=1,ny-1
        DO i=2,nx-1
!         umix(i,j,k)=umix(i,j,k)+cfcmv2*tem1(i,j,k)
          tem1(i,j,k)=cfcmv2*umix(i,j,k)
        END DO
      END DO
    END DO
!
    CALL addifzz0(ustr, nx,ny,nz, 2,nx-1,1,ny-1,2,nz-2, dz, tem1)
    tem1 = 0.0
  END IF

  IF( cfcmh2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the horizontal
!  computational mixing on v'
!
!-----------------------------------------------------------------------
!
      tem1=0.0;tem2=0.0
    DO k=2,nz-2
      DO j=2,ny-1
        DO i=2,nx-2
!         vmix(i,j,k)=vmix(i,j,k)+cfcmh2*(tem1(i,j,k)+tem2(i,j,k))
          tem1(i,j,k)=tem1(i,j,k)+ cfcmh2*vmix(i,j,k)
          tem2(i,j,k)=tem2(i,j,k)+ cfcmh2*vmix(i,j,k)
        END DO
      END DO
    END DO

    DO k=2,nz-2
      DO j=2,ny-1
!       vmix(1,j,k)=vmix(1,j,k)+cfcmh2*(tem2(1,j,k)                     &
!               +(-vstr(1,j,k)+vstr(2,j,k))*dxinv2)
       tem2(1,j,k)=tem2(1,j,k)+cfcmh2*vmix(1,j,k) 
       vstr(1,j,k)=vstr(1,j,k)-cfcmh2*vmix(1,j,k)*dxinv2
       vstr(2,j,k)=vstr(2,j,k)+cfcmh2*vmix(1,j,k)*dxinv2

!       vmix(nx-1,j,k)=vmix(nx-1,j,k)+cfcmh2*(tem2(nx-1,j,k)            &
!               +(-vstr(nx-1,j,k)+vstr(nx-2,j,k))*dxinv2)
      tem2(nx-1,j,k)=tem2(nx-1,j,k)+cfcmh2*vmix(nx-1,j,k)
      vstr(nx-1,j,k)=vstr(nx-1,j,k)-cfcmh2*vmix(nx-1,j,k)*dxinv2
      vstr(nx-2,j,k)=vstr(nx-2,j,k)+cfcmh2*vmix(nx-1,j,k)*dxinv2
      END DO
    END DO

    CALL addifxx0(vstr, nx,ny,nz, 2,nx-2,2,ny-1,2,nz-2, dx, tem1)

    CALL addifyy0(vstr, nx,ny,nz, 1,nx-1,2,ny-1,2,nz-2, dy, tem2)
      tem1= 0.0; tem2=0.0
  END IF

  IF( cfcmv2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the vertical
!  computational mixing on v'
!
!-----------------------------------------------------------------------
      tem1=0.0
    DO k=2,nz-2
      DO j=2,ny-1
        DO i=1,nx-1
!         vmix(i,j,k)=vmix(i,j,k)+cfcmv2*tem1(i,j,k)
          tem1(i,j,k)=cfcmv2*vmix(i,j,k)
        END DO
      END DO
    END DO

    CALL addifzz0(vstr, nx,ny,nz, 1,nx-1,2,ny-1,2,nz-2, dz, tem1)
    tem1=0.0
  END IF


  IF( cfcmh2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the horizontal
!  computational mixing on w'
!
!-----------------------------------------------------------------------
!
    tem1=0.0; tem2=0.0
    DO k=2,nz-1
      DO j=2,ny-2
        DO i=2,nx-2
!         wmix(i,j,k)=wmix(i,j,k)+cfcmh2*(tem1(i,j,k)+tem2(i,j,k))
          tem1(i,j,k)=tem1(i,j,k)+cfcmh2*wmix(i,j,k)
          tem2(i,j,k)=tem2(i,j,k)+cfcmh2*wmix(i,j,k)
        END DO
      END DO
    END DO

    DO k=2,nz-1
      DO j=2,ny-2
!       wmix(1,j,k)=wmix(1,j,k)+cfcmh2*(tem2(1,j,k)                     &
!               +(-wstr(1,j,k)+wstr(2,j,k))*dxinv2)
        tem2(1,j,k)=tem2(1,j,k)+cfcmh2*wmix(1,j,k)
        wstr(1,j,k)=wstr(1,j,k)-cfcmh2*wmix(1,j,k)*dxinv2
        wstr(2,j,k)=wstr(2,j,k)+cfcmh2*wmix(1,j,k)*dxinv2

!       wmix(nx-1,j,k)=wmix(nx-1,j,k)+cfcmh2*(tem2(nx-1,j,k)            &
!               +(-wstr(nx-1,j,k)+wstr(nx-2,j,k))*dxinv2)
        tem2(nx-1,j,k)=tem2(nx-1,j,k)+cfcmh2*wmix(nx-1,j,k)
        wstr(nx-1,j,k)=wstr(nx-1,j,k)-cfcmh2*wmix(nx-1,j,k)*dxinv2
        wstr(nx-2,j,k)=wstr(nx-2,j,k)+cfcmh2*wmix(nx-1,j,k)*dxinv2
     
      END DO
    END DO

    DO k=2,nz-1
      DO i=2,nx-2
!       wmix(i, 1  ,k)=wmix(i, 1  ,k)+cfcmh2*(tem1(i,  1 ,k)            &
!               +(-wstr(i,1,k)+wstr(i,2,k))*dyinv2)
        tem1(i, 1  ,k)=tem1(i, 1  ,k)+cfcmh2*wmix(i, 1  ,k)
        wstr(i,1,k)=wstr(i,1,k)- cfcmh2*wmix(i, 1  ,k)*dyinv2
        wstr(i,2,k)=wstr(i,2,k)+ cfcmh2*wmix(i, 1  ,k)*dyinv2

!       wmix(i,ny-1,k)=wmix(i,ny-1,k)+cfcmh2*(tem1(i,ny-1,k)            &
!               +(-wstr(i,ny-1,k)+wstr(i,ny-2,k))*dyinv2)
        tem1(i,ny-1,k)=tem1(i,ny-1,k)+cfcmh2*wmix(i,ny-1,k)
        wstr(i,ny-1,k)=wstr(i,ny-1,k)-cfcmh2*wmix(i,ny-1,k)*dyinv2
        wstr(i,ny-2,k)=wstr(i,ny-2,k)+cfcmh2*wmix(i,ny-1,k)*dyinv2
      END DO
    END DO

    DO k=2,nz-1
!     wmix(1, 1  ,k)=wmix(1, 1  ,k)                                     &
!             +cfcmh2*((-wstr(1,1,k)+wstr(2,1,k))*dxinv2                &
!             +(-wstr(1,1,k)+wstr(1,2,k))*dyinv2)
      wstr(1,1,k)=wstr(1,1,k)-cfcmh2*wmix(1, 1  ,k)*dxinv2
      wstr(2,1,k)=wstr(2,1,k)+cfcmh2*wmix(1, 1  ,k)*dxinv2
      wstr(1,1,k)=wstr(1,1,k)-cfcmh2*wmix(1, 1  ,k)*dyinv2
      wstr(1,2,k)=wstr(1,2,k)+cfcmh2*wmix(1, 1  ,k)*dyinv2

!     wmix(1,ny-1,k)=wmix(1,ny-1,k)                                     &
!             +cfcmh2*((-wstr(1,ny-1,k)                                 &
!             +wstr(2,ny-1,k))*dxinv2                                   &
!             +(-wstr(1,ny-1,k)+wstr(1,ny-2,k))*dyinv2)
      wstr(1,ny-1,k)=wstr(1,ny-1,k)-cfcmh2*wmix(1,ny-1,k)*dxinv2
      wstr(2,ny-1,k)=wstr(2,ny-1,k)+cfcmh2*wmix(1,ny-1,k)*dxinv2
      wstr(1,ny-1,k)=wstr(1,ny-1,k)-cfcmh2*wmix(1,ny-1,k)*dyinv2
      wstr(1,ny-2,k)=wstr(1,ny-2,k)+cfcmh2*wmix(1,ny-1,k)*dyinv2

!     wmix(nx-1,1,k)=wmix(nx-1,1,k)                                     &
!             +cfcmh2*((-wstr(nx-1,1,k)                                 &
!             +wstr(nx-2,1,k))*dxinv2                                   &
!             +(-wstr(nx-1,1,k)+wstr(nx-1,2,k))*dyinv2)
      wstr(nx-1,1,k)=wstr(nx-1,1,k)-cfcmh2*wmix(nx-1,1,k)*dxinv2
      wstr(nx-2,1,k)=wstr(nx-2,1,k)+cfcmh2*wmix(nx-1,1,k)*dxinv2
      wstr(nx-1,1,k)=wstr(nx-1,1,k)-cfcmh2*wmix(nx-1,1,k)*dyinv2
      wstr(nx-1,2,k)=wstr(nx-1,2,k)+cfcmh2*wmix(nx-1,1,k)*dyinv2

!     wmix(nx-1,ny-1,k)=wmix(nx-1,ny-1,k)                               &
!             +cfcmh2*((-wstr(nx-1,ny-1,k)                              &
!             +wstr(nx-2,ny-1,k))*dxinv2                                &
!             +(-wstr(nx-1,ny-1,k)+wstr(nx-1,ny-2,k))*dyinv2)
      wstr(nx-1,ny-1,k)=wstr(nx-1,ny-1,k)-cfcmh2*                       &
                                     wmix(nx-1,ny-1,k)*dxinv2
      wstr(nx-2,ny-1,k)=wstr(nx-2,ny-1,k)+cfcmh2*                       &
                                     wmix(nx-1,ny-1,k)*dxinv2
      wstr(nx-1,ny-1,k)=wstr(nx-1,ny-1,k)-cfcmh2*                       &
                                     wmix(nx-1,ny-1,k)*dyinv2
      wstr(nx-1,ny-2,k)=wstr(nx-1,ny-2,k)+cfcmh2*                       &
                                     wmix(nx-1,ny-1,k)*dyinv2
    END DO

    CALL addifxx0(wstr, nx,ny,nz, 2,nx-2,1,ny-1,2,nz-1, dx, tem1)

    CALL addifyy0(wstr, nx,ny,nz, 1,nx-1,2,ny-2,2,nz-1, dy, tem2)

  END IF
!
  IF( cfcmv2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the vertical
!  computational mixing on w
!
!-----------------------------------------------------------------------
!
      tem1 = 0.0
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
!         wmix(i,j,k)=wmix(i,j,k)+cfcmv2*tem1(i,j,k)
          tem1(i,j,k)=tem1(i,j,k)+cfcmv2*wmix(i,j,k)
        END DO
      END DO
    END DO

    CALL addifzz0(wstr, nx,ny,nz, 1,nx-1,1,ny-1,2,nz-1, dz, tem1)
    tem1 = 0.0
  END IF

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx
        u(i,j,k)=u(i,j,k)+ ustr(i,j,k)*tem4(i,j,k)
      END DO
    END DO
  END DO

  DO k=1,nz-1
    DO j=1,ny
      DO i=1,nx-1
        v(i,j,k)=v(i,j,k)+ vstr(i,j,k)*tem5(i,j,k)
      END DO
    END DO
  END DO

  DO k=1,nz
    DO j=1,ny-1
      DO i=1,nx-1
        w(i,j,k)=w(i,j,k)+ wstr(i,j,k)*tem6(i,j,k)
      END DO
    END DO
  END DO
!
RETURN
END SUBROUTINE adtlmcmix2uvw


!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TLMCMIX2UVW                ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tlmcmix2uvw(nx,ny,nz,                                        &
           u,v,w, ubar,vbar,rhostr,                                     &
           umix,vmix,wmix,                                              &
           ustr,vstr,wstr,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the second order computational mixing terms for the momentum
!  equations. Computational mixing is applied to velocity perturbations
!  only. These terms are placed in the arrays umix, vmix and wmix.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/3/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  4/2/92 (M. Xue and H. Jin)
!  modify for using terrain
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        Verticle component of Cartesian velocity at a
!             given time level (m/s)
!
!    ubar     Base state zonal velocity component (m/s)
!    vbar     Base state meridional velocity component (m/s)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!
!    umix     Array containing turbulent mixing on u
!    vmix     Array containing turbulent mixing on v
!    wmix     Array containing turbulent mixing on w
!
!  OUTPUT:
!
!    umix     Turbulent and computational mixing on u.
!    vmix     Turbulent and computational mixing on v.
!    wmix     Turbulent and computational mixing on w.
!
!  WORK ARRAYS:
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.

  REAL :: umix  (nx,ny,nz)     ! Total mixing in u-eq.
  REAL :: vmix  (nx,ny,nz)     ! Total mixing in v-eq.
  REAL :: wmix  (nx,ny,nz)     ! Total mixing in w-eq.

  REAL :: ustr  (nx,ny,nz)     ! u * rhostr
  REAL :: vstr  (nx,ny,nz)     ! v * rhostr
  REAL :: wstr  (nx,ny,nz)     ! w * rhostr

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k, onvf
  REAL :: dxinv2, dyinv2
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'phycst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'globcst.inc'
!
!-----------------------------------------------------------------------
!

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  IF the coefficients of computational mixing in both horizontal
!  and vertical directions are zero, exit this subroutine.
!
!-----------------------------------------------------------------------
!

  IF( cfcmh2 == 0.0 .AND. cfcmv2 == 0.0 ) RETURN


  dxinv2 = dxinv**2
  dyinv2 = dyinv**2
!
!-----------------------------------------------------------------------
!
!  Calculate ustr=rhostr*u', vstr=rhostr*v', wstr=rhostr*w'
!
!-----------------------------------------------------------------------
!
  CALL rhouvw0(nx,ny,nz,rhostr,tem1,tem2,tem3)

  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx
        ustr(i,j,k)=u(i,j,k)*tem1(i,j,k)
      END DO
    END DO
  END DO

  DO k=1,nz-1
    DO j=1,ny
      DO i=1,nx-1
        vstr(i,j,k)=v(i,j,k)*tem2(i,j,k)
      END DO
    END DO
  END DO

  DO k=1,nz
    DO j=1,ny-1
      DO i=1,nx-1
        wstr(i,j,k)=w(i,j,k)*tem3(i,j,k)
      END DO
    END DO
  END DO

  IF( cfcmh2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the second order
!  horizontal computational mixing on u'
!
!-----------------------------------------------------------------------
!
    CALL difxx0(ustr, nx,ny,nz, 2,nx-1,1,ny-1,2,nz-2, dx, tem1)

    CALL difyy0(ustr, nx,ny,nz, 2,nx-1,2,ny-2,2,nz-2, dy, tem2)

    DO k=2,nz-2
      DO j=2,ny-2
        DO i=2,nx-1
          umix(i,j,k)=umix(i,j,k)+cfcmh2*(tem1(i,j,k)+tem2(i,j,k))
        END DO
      END DO
    END DO

    DO k=2,nz-2
      DO i=2,nx-1
        umix(i,  1 ,k)=umix(i,  1 ,k)+cfcmh2*(tem1(i,  1 ,k)            &
                +(-ustr(i,1,k)+ustr(i,2,k))*dyinv2)
        umix(i,ny-1,k)=umix(i,ny-1,k)+cfcmh2*(tem1(i,ny-1,k)            &
                +(-ustr(i,ny-1,k)+ustr(i,ny-2,k))*dyinv2)
      END DO
    END DO

  END IF
!
  IF( cfcmv2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the second order
!  vertical computational mixing on u'
!
!-----------------------------------------------------------------------
!
    CALL difzz0(ustr, nx,ny,nz, 2,nx-1,1,ny-1,2,nz-2, dz, tem1)

    DO k=2,nz-2
      DO j=1,ny-1
        DO i=2,nx-1
          umix(i,j,k)=umix(i,j,k)+cfcmv2*tem1(i,j,k)
        END DO
      END DO
    END DO

  END IF

  IF( cfcmh2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the horizontal
!  computational mixing on v'
!
!-----------------------------------------------------------------------
!
    CALL difxx0(vstr, nx,ny,nz, 2,nx-2,2,ny-1,2,nz-2, dx, tem1)

    CALL difyy0(vstr, nx,ny,nz, 1,nx-1,2,ny-1,2,nz-2, dy, tem2)

    DO k=2,nz-2
      DO j=2,ny-1
        DO i=2,nx-2
          vmix(i,j,k)=vmix(i,j,k)+cfcmh2*(tem1(i,j,k)+tem2(i,j,k))
        END DO
      END DO
    END DO

    DO k=2,nz-2
      DO j=2,ny-1
        vmix(1,j,k)=vmix(1,j,k)+cfcmh2*(tem2(1,j,k)                     &
                +(-vstr(1,j,k)+vstr(2,j,k))*dxinv2)
        vmix(nx-1,j,k)=vmix(nx-1,j,k)+cfcmh2*(tem2(nx-1,j,k)            &
                +(-vstr(nx-1,j,k)+vstr(nx-2,j,k))*dxinv2)
      END DO
    END DO

  END IF

  IF( cfcmv2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the vertical
!  computational mixing on v'
!
!-----------------------------------------------------------------------

    CALL difzz0(vstr, nx,ny,nz, 1,nx-1,2,ny-1,2,nz-2, dz, tem1)

    DO k=2,nz-2
      DO j=2,ny-1
        DO i=1,nx-1
          vmix(i,j,k)=vmix(i,j,k)+cfcmv2*tem1(i,j,k)
        END DO
      END DO
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Second order computational mixing on w
!
!-----------------------------------------------------------------------
!

  IF( cfcmh2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the horizontal
!  computational mixing on w
!
!-----------------------------------------------------------------------
!
    CALL difxx0(wstr, nx,ny,nz, 2,nx-2,1,ny-1,2,nz-1, dx, tem1)

    CALL difyy0(wstr, nx,ny,nz, 1,nx-1,2,ny-2,2,nz-1, dy, tem2)

    DO k=2,nz-1
      DO j=2,ny-2
        DO i=2,nx-2
          wmix(i,j,k)=wmix(i,j,k)+cfcmh2*(tem1(i,j,k)+tem2(i,j,k))
        END DO
      END DO
    END DO

    DO k=2,nz-1
      DO j=2,ny-2
        wmix(1,j,k)=wmix(1,j,k)+cfcmh2*(tem2(1,j,k)                     &
                +(-wstr(1,j,k)+wstr(2,j,k))*dxinv2)
        wmix(nx-1,j,k)=wmix(nx-1,j,k)+cfcmh2*(tem2(nx-1,j,k)            &
                +(-wstr(nx-1,j,k)+wstr(nx-2,j,k))*dxinv2)
      END DO
    END DO

    DO k=2,nz-1
      DO i=2,nx-2
        wmix(i, 1  ,k)=wmix(i, 1  ,k)+cfcmh2*(tem1(i,  1 ,k)            &
                +(-wstr(i,1,k)+wstr(i,2,k))*dyinv2)
        wmix(i,ny-1,k)=wmix(i,ny-1,k)+cfcmh2*(tem1(i,ny-1,k)            &
                +(-wstr(i,ny-1,k)+wstr(i,ny-2,k))*dyinv2)
      END DO
    END DO

    DO k=2,nz-1
      wmix(1, 1  ,k)=wmix(1, 1  ,k)                                     &
              +cfcmh2*((-wstr(1,1,k)+wstr(2,1,k))*dxinv2                &
              +(-wstr(1,1,k)+wstr(1,2,k))*dyinv2)
      wmix(1,ny-1,k)=wmix(1,ny-1,k)                                     &
              +cfcmh2*((-wstr(1,ny-1,k)                                 &
              +wstr(2,ny-1,k))*dxinv2                                   &
              +(-wstr(1,ny-1,k)+wstr(1,ny-2,k))*dyinv2)
      wmix(nx-1,1,k)=wmix(nx-1,1,k)                                     &
              +cfcmh2*((-wstr(nx-1,1,k)                                 &
              +wstr(nx-2,1,k))*dxinv2                                   &
              +(-wstr(nx-1,1,k)+wstr(nx-1,2,k))*dyinv2)
      wmix(nx-1,ny-1,k)=wmix(nx-1,ny-1,k)                               &
              +cfcmh2*((-wstr(nx-1,ny-1,k)                              &
              +wstr(nx-2,ny-1,k))*dxinv2                                &
              +(-wstr(nx-1,ny-1,k)+wstr(nx-1,ny-2,k))*dyinv2)
    END DO

  END IF
!
  IF( cfcmv2 /= 0.0) THEN
!
!-----------------------------------------------------------------------
!
!  If the coefficient is not zero, calculate the vertical
!  computational mixing on w
!
!-----------------------------------------------------------------------
!
    CALL difzz0(wstr, nx,ny,nz, 1,nx-1,1,ny-1,2,nz-1, dz, tem1)

    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          wmix(i,j,k)=wmix(i,j,k)+cfcmv2*tem1(i,j,k)
        END DO
      END DO
    END DO

  END IF


  RETURN
END SUBROUTINE tlmcmix2uvw
!
!
