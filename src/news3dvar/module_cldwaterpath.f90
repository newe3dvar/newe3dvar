MODULE module_cwp

  USE module_cwpobs, ONLY : pseudo_opt, qsrcpsd

  USE model_precision

  IMPLICIT NONE

  REAL, PARAMETER, PRIVATE :: g=9.81, zerok=273.15

  INTEGER         :: nobscwp,totcwp,nobspsd

  REAL, ALLOCATABLE :: cldbase(:),cldtop(:),obcwp(:),xcwp(:),ycwp(:),   &
                       hcwp(:),odifcwp(:),cwperr(:)
  !REAL, ALLOCATABLE, DIMENSION(:)       :: cwpgridlat,cwpgridlon

  INTEGER, ALLOCATABLE  :: cldphase(:)
  INTEGER, ALLOCATABLE  :: cwpflag(:),cwpusetype(:)
  INTEGER, ALLOCATABLE  :: iptcwp(:),jptcwp(:),cldbslv(:),cldtplv(:)
  REAL,    ALLOCATABLE  :: wgtcwp(:,:), cwp_diff(:)
  REAL,    ALLOCATABLE  :: xpsd(:),ypsd(:)
  !REAL, ALLOCATABLE, DIMENSION(:)  :: psdgridlat,psdgridlon
  INTEGER, ALLOCATABLE  :: psdflag(:),iptpsd(:),jptpsd(:)
  REAL,    ALLOCATABLE  :: wgtpsd(:,:)

  REAL,    ALLOCATABLE  :: odifhydro(:,:,:),hhydro(:,:,:),hydro_diff(:,:,:)
  !REAL, ALLOCATABLE                     :: qobscwp(:,:)

  REAL(P),  ALLOCATABLE :: cwp_bkg(:)

  CONTAINS

  !#####################################################################

  SUBROUTINE cwp_alloc_within_dom(total_num,valid_num,psd_num,nz,nscalarq,istatus)

    INTEGER, INTENT(IN)  :: total_num,valid_num,psd_num
    INTEGER, INTENT(IN)  :: nz,nscalarq
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    totcwp  = total_num
    nobscwp = valid_num
    nobspsd = psd_num

    ALLOCATE (xcwp(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "xcwp")

    ALLOCATE (ycwp(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "ycwp")

!     ALLOCATE (cwpgridlat(valid_num),stat=istatus)
!     CALL check_alloc_status(istatus, "cwpgridlat")

!     ALLOCATE (cwpgridlon(valid_num),stat=istatus)
!     CALL check_alloc_status(istatus, "cwpgridlon")

    ALLOCATE (cldbase(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cldbase")

    ALLOCATE (cldtop(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cldtop")

    ALLOCATE (obcwp(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "obcwp")

    ALLOCATE (cwperr(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cwperr")

    ALLOCATE (hcwp(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "hcwp")

    ALLOCATE (cldphase(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cldphase")

    ALLOCATE (odifcwp(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "odifcwp")
    odifcwp=0.0

    ALLOCATE (cwpflag(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cwpflag")
    cwpflag=0

    ALLOCATE (cwpusetype(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cwpusetype")
    cwpusetype=2

    ALLOCATE (iptcwp(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "iptcwp")

    ALLOCATE (jptcwp(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "jptcwp")

    ALLOCATE (wgtcwp(valid_num,4),stat=istatus)
    CALL check_alloc_status(istatus, "wgtcwp")

    ALLOCATE (cldbslv(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cldbslv")
    ALLOCATE (cldtplv(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cldtplv")

    ALLOCATE (cwp_diff(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cwp_diff")

    IF (psd_num /= 0) THEN
      ALLOCATE (xpsd(psd_num),stat=istatus)
      CALL check_alloc_status(istatus, "xpsd")

      ALLOCATE (ypsd(psd_num),stat=istatus)
      CALL check_alloc_status(istatus, "ypsd")

!     ALLOCATE (psdgridlat(psd_num),stat=istatus)
!     CALL check_alloc_status(istatus, "psdgridlat")

!     ALLOCATE (psdgridlon(psd_num),stat=istatus)
!     CALL check_alloc_status(istatus, "psdgridlon")

!     ALLOCATE (obpsd(psd_num),stat=istatus)
!     CALL check_alloc_status(istatus,"obpsd")

      ALLOCATE (psdflag(psd_num),stat=istatus)
      CALL check_alloc_status(istatus,"psdflag")
      psdflag=0

      ALLOCATE (hhydro(psd_num,nz,nscalarq),stat=istatus)
      CALL check_alloc_status(istatus,"hhydro")
      hhydro=0

!     ALLOCATE (odifhydro(psd_num,nz,2),stat=istatus)
!     CALL check_alloc_status(istatus,"odifhydro")
!     odifhydro=0

      ALLOCATE (hydro_diff(psd_num,nz,nscalarq),stat=istatus)
      CALL check_alloc_status(istatus, "hydro_diff")
      hydro_diff=0

      ALLOCATE (iptpsd(psd_num),stat=istatus)
      CALL check_alloc_status(istatus, "iptpsd")

      ALLOCATE (jptpsd(psd_num),stat=istatus)
      CALL check_alloc_status(istatus, "jptpsd")

      ALLOCATE (wgtpsd(psd_num,4),stat=istatus)
      CALL check_alloc_status(istatus, "wgtpsd")

!     ALLOCATE (qobscwp(nvar,nsrc,valid_num),stat=istatus)
!     CALL check_alloc_status(istatus, "qobscwp")
    END IF

    ALLOCATE (cwp_bkg(valid_num), stat=istatus)
    CALL check_alloc_status(istatus, "cwp_bkg")

    RETURN
  END SUBROUTINE cwp_alloc_within_dom

  !#####################################################################

  SUBROUTINE cwp_dealloc_within_dom(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !DEALLOCATE(cwpgridlat)
    !DEALLOCATE(cwpgridlon)
    DEALLOCATE(xcwp)
    DEALLOCATE(ycwp)
    DEALLOCATE(cldbase)
    DEALLOCATE(cldtop)
    DEALLOCATE(obcwp)
    DEALLOCATE(cwperr)
    DEALLOCATE(hcwp)
    DEALLOCATE(cldphase)

    DEALLOCATE (odifcwp, stat=istatus)
    DEALLOCATE (cwpflag, stat=istatus)
    DEALLOCATE (iptcwp,  stat=istatus)
    DEALLOCATE (jptcwp,  stat=istatus)
    DEALLOCATE (wgtcwp,  stat=istatus)
    DEALLOCATE (cldbslv, stat=istatus)
    DEALLOCATE (cldtplv, stat=istatus)
    DEALLOCATE (cwp_diff,stat=istatus)

    IF (ALLOCATED(xpsd)) THEN

      DEALLOCATE (xpsd,stat=istatus)
      DEALLOCATE (ypsd,stat=istatus)

!     DEALLOCATE (psdgridlat,stat=istatus)
!     DEALLOCATE (psdgridlon,stat=istatus)
!     DEALLOCATE (obpsd,     stat=istatus)

      DEALLOCATE (psdflag,   stat=istatus)
      DEALLOCATE (hhydro,    stat=istatus)
!     DEALLOCATE (odifhydro, stat=istatus)
      DEALLOCATE (hydro_diff,stat=istatus)
      DEALLOCATE (iptpsd,    stat=istatus)
      DEALLOCATE (jptpsd,    stat=istatus)
      DEALLOCATE (wgtpsd,    stat=istatus)
!     DEALLOCATE (qobscwp,   stat=istatus)
    END IF

    RETURN
  END SUBROUTINE cwp_dealloc_within_dom

  !#####################################################################

  SUBROUTINE cwp_costf(nx,ny,nz,ptotal,tk,qscalar_ctr,f_ocwp,f_opsd,    &
                        nqc,nqi,nqr,nqs,nqg,nqh,hydro_bkg,istatus)

    INCLUDE 'globcst.inc'

    INTEGER,  INTENT(IN)  :: nx,ny,nz
    REAL(P),  INTENT(IN)  :: ptotal(nx,ny,nz),tk(nx,ny,nz)
    REAL(P),  INTENT(IN)  :: qscalar_ctr(nx,ny,nz,nscalarq)

    REAL(DP), INTENT(OUT) :: f_ocwp, f_opsd

    REAL(P),  INTENT(OUT) :: nqc(nx,ny,nz),nqi(nx,ny,nz)
    REAL(P),  INTENT(OUT) :: nqr(nx,ny,nz),nqs(nx,ny,nz)
    REAL(P),  INTENT(OUT) :: nqg(nx,ny,nz),nqh(nx,ny,nz)
    REAL(P),  INTENT(OUT) :: hydro_bkg(nz,nscalarq)

    INTEGER,  INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER  :: i,j,k,n
    REAL(DP) :: sum1, sum2
    REAL     :: cwpbkg

    REAL(P),  ALLOCATABLE :: qc_ctr1(:,:,:),qi_ctr1(:,:,:)
    REAL(P),  ALLOCATABLE :: qr_ctr1(:,:,:),qs_ctr1(:,:,:)
    REAL(P),  ALLOCATABLE :: qg_ctr1(:,:,:),qh_ctr1(:,:,:)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    f_ocwp = 0.0D0
    f_opsd = 0.0D0

    cwp_bkg = 0.0; cwp_diff = 0.0

    nqc = 0.0; nqi = 0.0;
    nqr = 0.0; nqs = 0.0;
    IF (P_QG>0) nqg = 0.0;
    IF (P_QH>0) nqh = 0.0; 
    hydro_bkg = 0.0

    DO k = 1, nz
      DO j = 1,ny
        DO i = 1, nx
          nqc(i,j,k) = qscalar_ctr(i,j,k,P_QC)
          nqi(i,j,k) = qscalar_ctr(i,j,k,P_QI)
          nqr(i,j,k) = qscalar_ctr(i,j,k,P_QR)
          nqs(i,j,k) = qscalar_ctr(i,j,k,P_QS)
          IF (P_QG>0) nqg(i,j,k) = qscalar_ctr(i,j,k,P_QG)
          IF (P_QH>0) nqh(i,j,k) = qscalar_ctr(i,j,k,P_QH)
        END DO
      END DO
    END DO

    DO i=1,nobscwp
      IF (cldtplv(i)==-1 .OR. cldbslv(i)==-1) THEN
        cwpflag(i)=-1
      END IF
    END DO

    DO i=1,nobscwp
      IF ( (pseudo_opt == 0 .AND. cwpflag(i)>0) .OR.                        &
           (pseudo_opt == 1 .AND. cwpflag(i)==1) ) THEN
      !IF ( cwpflag(i)>0 ) THEN   ! Should be checked - WYH

        CALL cwp_forward(nx,ny,nz,ptotal,tk,nqc,nqi,nqr,nqs,nqg,nqh,i,cwpbkg,istatus)
        
        cwp_bkg(i) = cwpbkg
        cwp_diff(i)=(cwp_bkg(i)-obcwp(i))/cwperr(i)
        IF (cwpusetype(i)==2) THEN
          f_ocwp=f_ocwp+cwp_diff(i)*(cwp_bkg(i)-obcwp(i))
        END IF
      ELSE
        cwp_diff(i)=0.0
      END IF
    END DO

    IF (pseudo_opt==1) THEN

      DO i=1,nobspsd
        IF (psdflag(i)>0) THEN
          CALL hydro_forward(nx,ny,nz,qscalar_ctr,i,hydro_bkg,istatus)  ! compute hydro_bkg
          DO n=1,nscalarq
            DO k=2,nz-1
              hydro_diff(i,k,n)=hydro_bkg(k,n)/qsrcpsd(n)
              IF (psdflag(i)==2) THEN
                f_opsd=f_opsd+hydro_diff(i,k,n)*hydro_bkg(k,n)
              END IF
            END DO
          END DO
        ELSE
          hydro_diff(i,:,:) = 0
        END IF
      END DO

    END IF

    IF(0==1) then   ! for verification of adjoint

      sum1 =0.0
      DO i=1,nobscwp
        !print*,cwp_bkg(i),wgtcwp(i,:)
        sum1=sum1+cwp_bkg(i)*cwp_bkg(i)
      END DO

      ALLOCATE(qc_ctr1(nx,ny,nz),stat=istatus)
      ALLOCATE(qi_ctr1(nx,ny,nz),stat=istatus)
      ALLOCATE(qr_ctr1(nx,ny,nz),stat=istatus)
      ALLOCATE(qs_ctr1(nx,ny,nz),stat=istatus)
      IF (P_QG>0) ALLOCATE(qg_ctr1(nx,ny,nz),stat=istatus)
      IF (P_QH>0) ALLOCATE(qh_ctr1(nx,ny,nz),stat=istatus)
      qc_ctr1=0.0; qi_ctr1=0.0; qr_ctr1=0.0; qs_ctr1=0.0;
      IF (P_QG>0) qg_ctr1=0.0
      IF (P_QH>0) qh_ctr1=0.0
      DO i=1,nobscwp
        IF ( (pseudo_opt==0 .and. cwpflag(i)>0) .or.      &
             (pseudo_opt==1 .and. cwpflag(i)==1) ) THEN
          CALL ad_cwp_forward(nx,ny,nz,ptotal,tk,qc_ctr1,qi_ctr1,qr_ctr1,   &
                              qs_ctr1,qg_ctr1,qh_ctr1,i,cwp_bkg(i),istatus)
        END IF
      END DO
      sum2 =0.0
      DO k = 1, nz
        DO j = 1,ny
          DO i = 1, nx
            sum2=sum2+ nqc(i,j,k)*qc_ctr1(i,j,k)
            sum2=sum2+ nqi(i,j,k)*qi_ctr1(i,j,k)
            sum2=sum2+ nqr(i,j,k)*qr_ctr1(i,j,k)
            sum2=sum2+ nqs(i,j,k)*qs_ctr1(i,j,k)
            IF (P_QG>0) sum2=sum2+ nqg(i,j,k)*qg_ctr1(i,j,k)
            IF (P_QH>0) sum2=sum2+ nqh(i,j,k)*qh_ctr1(i,j,k)
          END DO
        END DO
      END DO
      print*,'cwp,sum1,sum2====',sum1,sum2
      DEALLOCATE( qc_ctr1, qi_ctr1, qr_ctr1, qs_ctr1 )
      IF (P_QG>0) DEALLOCATE( qg_ctr1 )
      IF (P_QH>0) DEALLOCATE( qh_ctr1 )
      stop
    END IF

    IF(0==1 .and. pseudo_opt==1) then   ! for verification of adjoint
      !ALLOCATE(hydro_bkg(nz,2), STAT = istatus)
      !
      ! hydro_bkg need values - WYH ????
      !

      sum1 =0.0
      DO i=1,nobspsd
        IF (psdflag(i)>0) THEN
          CALL hydro_forward(nx,ny,nz,qscalar_ctr,i,hydro_bkg,istatus)
          DO n=1,nscalarq
            DO k=2,nz-1
              sum1=sum1+hydro_bkg(k,n)*hydro_bkg(k,n)
            END DO
          END DO
        END IF
      END DO

      ALLOCATE(qr_ctr1(nx,ny,nz),stat=istatus)
      ALLOCATE(qs_ctr1(nx,ny,nz),stat=istatus)
      IF (P_QG>0) ALLOCATE(qg_ctr1(nx,ny,nz),stat=istatus)
      IF (P_QH>0) ALLOCATE(qh_ctr1(nx,ny,nz),stat=istatus)
      qr_ctr1=0.0;qs_ctr1=0.0;qr_ctr1=0.0;qi_ctr1=0.0
      IF (P_QG>0) qg_ctr1=0.0
      IF (P_QH>0) qh_ctr1=0.0
      DO i=1,nobspsd
        IF (psdflag(i)>0) THEN
          CALL hydro_forward(nx,ny,nz,qscalar_ctr,i,hydro_bkg,istatus)
          CALL ad_hydro_forward(nx,ny,nz,qc_ctr1,qi_ctr1,qr_ctr1,qs_ctr1,   &
                                qg_ctr1,qh_ctr1,i,hydro_bkg(:,:),istatus)
        END IF
      END DO
      sum2 =0.0
      DO k = 1, nz-1
        DO j = 1,ny-1
          DO i = 1, nx-1
            sum2=sum2+ nqc(i,j,k)*qc_ctr1(i,j,k)
            sum2=sum2+ nqi(i,j,k)*qi_ctr1(i,j,k)
            sum2=sum2+ qscalar_ctr(i,j,k,P_QR)*qr_ctr1(i,j,k)
            sum2=sum2+ qscalar_ctr(i,j,k,P_QS)*qs_ctr1(i,j,k)
            IF (P_QG>0) sum2=sum2+ qscalar_ctr(i,j,k,P_QG)*qg_ctr1(i,j,k)
            IF (P_QH>0) sum2=sum2+ qscalar_ctr(i,j,k,P_QH)*qh_ctr1(i,j,k)
          END DO
        END DO
      END DO
      print*,'hydro,sum1,sum2=',sum1,sum2

      DEALLOCATE(qr_ctr1,qs_ctr1)
      IF (P_QG>0) DEALLOCATE(qg_ctr1)
      IF (P_QH>0) DEALLOCATE(qh_ctr1)

    END IF

    RETURN
  END SUBROUTINE cwp_costf

  !#####################################################################

  SUBROUTINE cwp_gradt(nx,ny,nz,ptotal,tk,qscalar_grd,          &
                       nqc,nqi,nqr,nqs,nqg,nqh,hydro2d,tem1,istatus)

    INCLUDE 'globcst.inc'
    INCLUDE 'bndry.inc'
    INCLUDE 'mp.inc'

    INTEGER, INTENT(IN)    :: nx,ny,nz
    REAL(P), INTENT(IN)    :: ptotal(nx,ny,nz),tk(nx,ny,nz)
    REAL(P), INTENT(INOUT) :: qscalar_grd(nx,ny,nz,nscalarq)

    REAL(P), INTENT(OUT)   :: nqc(nx,ny,nz), nqi(nx,ny,nz)
    REAL(P), INTENT(OUT)   :: nqr(nx,ny,nz), nqs(nx,ny,nz)
    REAL(P), INTENT(OUT)   :: nqg(nx,ny,nz), nqh(nx,ny,nz)
    REAL(P), INTENT(OUT)   :: hydro2d(nz,nscalarq), tem1(nx,ny,nz)

    INTEGER, INTENT(OUT)   :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,k,n

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    nqc=0.0;nqi=0.0;
    nqr=0.0;nqs=0.0;
    IF (P_QG>0) nqg=0.0
    IF (P_QH>0) nqh=0.0

    DO i=1,nobscwp
      IF ( (pseudo_opt == 0 .AND. cwpflag(i)>0 ) .OR.                       &
           (pseudo_opt == 1 .AND. cwpflag(i)==1) ) THEN
        CALL ad_cwp_forward(nx,ny,nz,ptotal,tk,nqc,nqi,nqr,nqs,nqg,nqh,i,cwp_diff(i),istatus)
      END IF
    END DO

    !   qc_grd=qc_grd+tem1
    !   qi_grd=qi_grd+tem3

    IF (pseudo_opt==1) THEN

      DO i=1,nobspsd
        IF (psdflag(i)>0) THEN

          DO n = 1,nscalarq
            DO k = 1,nz
              hydro2d(k,n) = hydro_diff(i,k,n)
            END DO
          END DO

          CALL ad_hydro_forward(nx,ny,nz,nqc,nqi,nqr,nqs,nqg,nqh,i,hydro2d,istatus)

        END IF
      END DO

    END IF

    IF (mp_opt>0) THEN
      CALL mpsendrecv2dew(nqc,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(nqc,nx,ny,nz,nbc,sbc,0,tem1)
 
      CALL mpsendrecv2dew(nqi,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(nqi,nx,ny,nz,nbc,sbc,0,tem1)
 
      CALL mpsendrecv2dew(nqr,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(nqr,nx,ny,nz,nbc,sbc,0,tem1)
 
      CALL mpsendrecv2dew(nqs,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(nqs,nx,ny,nz,nbc,sbc,0,tem1)
 
      IF (P_QG>0) THEN
        CALL mpsendrecv2dew(nqg,nx,ny,nz,ebc,wbc,0,tem1)
        CALL mpsendrecv2dns(nqg,nx,ny,nz,nbc,sbc,0,tem1)
      END IF
 
      IF (P_QH>0) THEN
        CALL mpsendrecv2dew(nqh,nx,ny,nz,ebc,wbc,0,tem1)
        CALL mpsendrecv2dns(nqh,nx,ny,nz,nbc,sbc,0,tem1)
      END IF
    END IF

    qscalar_grd(:,:,:,P_QC)=qscalar_grd(:,:,:,P_QC)+nqc
    qscalar_grd(:,:,:,P_QI)=qscalar_grd(:,:,:,P_QI)+nqi
    qscalar_grd(:,:,:,P_QR)=qscalar_grd(:,:,:,P_QR)+nqr
    qscalar_grd(:,:,:,P_QS)=qscalar_grd(:,:,:,P_QS)+nqs
    IF (P_QG>0) qscalar_grd(:,:,:,P_QG)=qscalar_grd(:,:,:,P_QG)+nqg
    IF (P_QH>0) qscalar_grd(:,:,:,P_QH)=qscalar_grd(:,:,:,P_QH)+nqh

    RETURN
  END SUBROUTINE cwp_gradt

  !#####################################################################

  SUBROUTINE cwp_forward(nx,ny,nz,pp,tk,qc,qi,qr,qs,qg,qh,ii,cwpbkg,istatus)

    IMPLICIT NONE

    INCLUDE 'globcst.inc'

    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL,    INTENT(IN)  :: qc(nx,ny,nz),qi(nx,ny,nz)
    REAL,    INTENT(IN)  :: qr(nx,ny,nz),qs(nx,ny,nz)
    REAL,    INTENT(IN)  :: qg(nx,ny,nz),qh(nx,ny,nz)
    REAL ,   INTENT(IN)  :: pp(nx,ny,nz),tk(nx,ny,nz)
    INTEGER, INTENT(IN)  :: ii
    REAL ,   INTENT(OUT) :: cwpbkg

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER  :: k

    INTEGER  :: ipt,jpt,phase,cbslv, ctplv
    REAL     :: wgt(4)

    REAL     :: tmp1(4),tmp2(4)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ipt     = iptcwp(ii)
    jpt     = jptcwp(ii)
    wgt(:)  = wgtcwp(ii,:)
    phase   = cldphase(ii)
    cbslv   = cldbslv(ii)
    ctplv   = cldtplv(ii)

    tmp2=0.

    IF ( ipt/=nx .and. jpt/=ny ) THEN

      DO k=cbslv,ctplv-1
        tmp1=0.
        tmp1(1)=tmp1(1)+(qc(ipt,jpt,k)+qc(ipt,jpt,k+1))*0.5
        tmp1(2)=tmp1(2)+(qc(ipt+1,jpt,k)+qc(ipt+1,jpt,k+1))*0.5
        tmp1(3)=tmp1(3)+(qc(ipt+1,jpt+1,k)+qc(ipt+1,jpt+1,k+1))*0.5
        tmp1(4)=tmp1(4)+(qc(ipt,jpt+1,k)+qc(ipt,jpt+1,k+1))*0.5
        tmp1(1)=tmp1(1)+(qr(ipt,jpt,k)+qr(ipt,jpt,k+1))*0.5
        tmp1(2)=tmp1(2)+(qr(ipt+1,jpt,k)+qr(ipt+1,jpt,k+1))*0.5
        tmp1(3)=tmp1(3)+(qr(ipt+1,jpt+1,k)+qr(ipt+1,jpt+1,k+1))*0.5
        tmp1(4)=tmp1(4)+(qr(ipt,jpt+1,k)+qr(ipt,jpt+1,k+1))*0.5
        IF ( phase == 2 ) THEN
          IF (tk(ipt,jpt,k)    <=zerok) tmp1(1)=tmp1(1)+(qi(ipt,jpt,k)+qi(ipt,jpt,k+1))*0.5
          IF (tk(ipt+1,jpt,k)  <=zerok) tmp1(2)=tmp1(2)+(qi(ipt+1,jpt,k)+qi(ipt+1,jpt,k+1))*0.5
          IF (tk(ipt+1,jpt+1,k)<=zerok) tmp1(3)=tmp1(3)+(qi(ipt+1,jpt+1,k)+qi(ipt+1,jpt+1,k+1))*0.5
          IF (tk(ipt,jpt+1,k)  <=zerok) tmp1(4)=tmp1(4)+(qi(ipt,jpt+1,k)+qi(ipt,jpt+1,k+1))*0.5
          IF (tk(ipt,jpt,k)    <=zerok) tmp1(1)=tmp1(1)+(qs(ipt,jpt,k)+qs(ipt,jpt,k+1))*0.5
          IF (tk(ipt+1,jpt,k)  <=zerok) tmp1(2)=tmp1(2)+(qs(ipt+1,jpt,k)+qs(ipt+1,jpt,k+1))*0.5
          IF (tk(ipt+1,jpt+1,k)<=zerok) tmp1(3)=tmp1(3)+(qs(ipt+1,jpt+1,k)+qs(ipt+1,jpt+1,k+1))*0.5
          IF (tk(ipt,jpt+1,k)  <=zerok) tmp1(4)=tmp1(4)+(qs(ipt,jpt+1,k)+qs(ipt,jpt+1,k+1))*0.5
          IF (P_QG>0) THEN
            IF (tk(ipt,jpt,k)    <=zerok) tmp1(1)=tmp1(1)+(qg(ipt,jpt,k)+qg(ipt,jpt,k+1))*0.5
            IF (tk(ipt+1,jpt,k)  <=zerok) tmp1(2)=tmp1(2)+(qg(ipt+1,jpt,k)+qg(ipt+1,jpt,k+1))*0.5
            IF (tk(ipt+1,jpt+1,k)<=zerok) tmp1(3)=tmp1(3)+(qg(ipt+1,jpt+1,k)+qg(ipt+1,jpt+1,k+1))*0.5
            IF (tk(ipt,jpt+1,k)  <=zerok) tmp1(4)=tmp1(4)+(qg(ipt,jpt+1,k)+qg(ipt,jpt+1,k+1))*0.5
          END IF
          IF (P_QH>0) THEN
            IF (tk(ipt,jpt,k)    <=zerok) tmp1(1)=tmp1(1)+(qh(ipt,jpt,k)+qh(ipt,jpt,k+1))*0.5
            IF (tk(ipt+1,jpt,k)  <=zerok) tmp1(2)=tmp1(2)+(qh(ipt+1,jpt,k)+qh(ipt+1,jpt,k+1))*0.5
            IF (tk(ipt+1,jpt+1,k)<=zerok) tmp1(3)=tmp1(3)+(qh(ipt+1,jpt+1,k)+qh(ipt+1,jpt+1,k+1))*0.5
            IF (tk(ipt,jpt+1,k)  <=zerok) tmp1(4)=tmp1(4)+(qh(ipt,jpt+1,k)+qh(ipt,jpt+1,k+1))*0.5
          END IF
        END IF
        tmp2(1)=tmp2(1)+tmp1(1)*(pp(ipt,jpt,k)-pp(ipt,jpt,k+1))
        tmp2(2)=tmp2(2)+tmp1(2)*(pp(ipt+1,jpt,k)-pp(ipt+1,jpt,k+1))
        tmp2(3)=tmp2(3)+tmp1(3)*(pp(ipt+1,jpt+1,k)-pp(ipt+1,jpt+1,k+1))
        tmp2(4)=tmp2(4)+tmp1(4)*(pp(ipt,jpt+1,k)-pp(ipt,jpt+1,k+1))
      END DO
      cwpbkg=(wgt(1)*tmp2(1)+wgt(2)*tmp2(2)+      &
              wgt(3)*tmp2(3)+wgt(4)*tmp2(4))/g

    ELSE IF ( ipt==nx .and. jpt/=nx ) THEN
      DO k=cbslv,ctplv-1
        tmp1=0.
        tmp1(1)=tmp1(1)+(qc(ipt,jpt,k)+qc(ipt,jpt,k+1))*0.5
        tmp1(4)=tmp1(4)+(qc(ipt,jpt+1,k)+qc(ipt,jpt+1,k+1))*0.5
        tmp1(1)=tmp1(1)+(qr(ipt,jpt,k)+qr(ipt,jpt,k+1))*0.5
        tmp1(4)=tmp1(4)+(qr(ipt,jpt+1,k)+qr(ipt,jpt+1,k+1))*0.5
        IF ( phase == 2 ) THEN
          IF (tk(ipt,jpt,k)  <=zerok) tmp1(1)=tmp1(1)+(qi(ipt,jpt,k)+qi(ipt,jpt,k+1))*0.5
          IF (tk(ipt,jpt+1,k)<=zerok) tmp1(4)=tmp1(4)+(qi(ipt,jpt+1,k)+qi(ipt,jpt+1,k+1))*0.5
          IF (tk(ipt,jpt,k)  <=zerok) tmp1(1)=tmp1(1)+(qs(ipt,jpt,k)+qs(ipt,jpt,k+1))*0.5
          IF (tk(ipt,jpt+1,k)<=zerok) tmp1(4)=tmp1(4)+(qs(ipt,jpt+1,k)+qs(ipt,jpt+1,k+1))*0.5
          IF (P_QG>0) THEN
            IF (tk(ipt,jpt,k)  <=zerok) tmp1(1)=tmp1(1)+(qg(ipt,jpt,k)+qg(ipt,jpt,k+1))*0.5
            IF (tk(ipt,jpt+1,k)<=zerok) tmp1(4)=tmp1(4)+(qg(ipt,jpt+1,k)+qg(ipt,jpt+1,k+1))*0.5
          END IF
          IF (P_QH>0) THEN
            IF (tk(ipt,jpt,k)  <=zerok) tmp1(1)=tmp1(1)+(qh(ipt,jpt,k)+qh(ipt,jpt,k+1))*0.5
            IF (tk(ipt,jpt+1,k)<=zerok) tmp1(4)=tmp1(4)+(qh(ipt,jpt+1,k)+qh(ipt,jpt+1,k+1))*0.5
          END IF
        END IF
        tmp2(1)=tmp2(1)+tmp1(1)*(pp(ipt,jpt,k)-pp(ipt,jpt,k+1))
        tmp2(4)=tmp2(4)+tmp1(4)*(pp(ipt,jpt+1,k)-pp(ipt,jpt+1,k+1))
      END DO
      cwpbkg=(wgt(1)*tmp2(1)+wgt(4)*tmp2(4))/g
    ELSE IF ( ipt/=nx .and. jpt==nx ) THEN
      DO k=cbslv,ctplv-1
        tmp1=0.
        tmp1(1)=tmp1(1)+(qc(ipt,jpt,k)+qc(ipt,jpt,k+1))*0.5
        tmp1(2)=tmp1(2)+(qc(ipt+1,jpt,k)+qc(ipt+1,jpt,k+1))*0.5
        tmp1(1)=tmp1(1)+(qr(ipt,jpt,k)+qr(ipt,jpt,k+1))*0.5
        tmp1(2)=tmp1(2)+(qr(ipt+1,jpt,k)+qr(ipt+1,jpt,k+1))*0.5
        IF ( phase == 2 ) THEN
          IF (tk(ipt,jpt,k)  <=zerok) tmp1(1)=tmp1(1)+(qi(ipt,jpt,k)+qi(ipt,jpt,k+1))*0.5
          IF (tk(ipt+1,jpt,k)<=zerok) tmp1(2)=tmp1(2)+(qi(ipt+1,jpt,k)+qi(ipt+1,jpt,k+1))*0.5
          IF (tk(ipt,jpt,k)  <=zerok) tmp1(1)=tmp1(1)+(qs(ipt,jpt,k)+qs(ipt,jpt,k+1))*0.5
          IF (tk(ipt+1,jpt,k)<=zerok) tmp1(2)=tmp1(2)+(qs(ipt+1,jpt,k)+qs(ipt+1,jpt,k+1))*0.5
          IF (P_QG>0) THEN
            IF (tk(ipt,jpt,k)  <=zerok) tmp1(1)=tmp1(1)+(qg(ipt,jpt,k)+qg(ipt,jpt,k+1))*0.5
            IF (tk(ipt+1,jpt,k)<=zerok) tmp1(2)=tmp1(2)+(qg(ipt+1,jpt,k)+qg(ipt+1,jpt,k+1))*0.5
          END IF
          IF (P_QH>0) THEN
            IF (tk(ipt,jpt,k)  <=zerok) tmp1(1)=tmp1(1)+(qh(ipt,jpt,k)+qh(ipt,jpt,k+1))*0.5
            IF (tk(ipt+1,jpt,k)<=zerok) tmp1(2)=tmp1(2)+(qh(ipt+1,jpt,k)+qh(ipt+1,jpt,k+1))*0.5
          END IF
        END IF
        tmp2(1)=tmp2(1)+tmp1(1)*(pp(ipt,jpt,k)-pp(ipt,jpt,k+1))
        tmp2(2)=tmp2(2)+tmp1(2)*(pp(ipt+1,jpt,k)-pp(ipt+1,jpt,k+1))
      END DO
      cwpbkg=(wgt(1)*tmp2(1)+wgt(2)*tmp2(2))/g
    ELSE
      DO k=cbslv,ctplv-1
        tmp1=0.
        tmp1(1)=tmp1(1)+(qc(ipt,jpt,k)+qc(ipt,jpt,k+1))*0.5
        tmp1(1)=tmp1(1)+(qr(ipt,jpt,k)+qr(ipt,jpt,k+1))*0.5
        IF ( phase == 2 ) THEN
          IF (tk(ipt,jpt,k)<=zerok) tmp1(1)=tmp1(1)+(qi(ipt,jpt,k)+qi(ipt,jpt,k+1))*0.5
          IF (tk(ipt,jpt,k)<=zerok) tmp1(1)=tmp1(1)+(qs(ipt,jpt,k)+qs(ipt,jpt,k+1))*0.5
          IF (P_QG>0) tmp1(1)=tmp1(1)+(qg(ipt,jpt,k)+qg(ipt,jpt,k+1))*0.5
          IF (P_QH>0) tmp1(1)=tmp1(1)+(qh(ipt,jpt,k)+qh(ipt,jpt,k+1))*0.5
        END IF
        tmp2(1)=tmp2(1)+tmp1(1)*(pp(ipt,jpt,k)-pp(ipt,jpt,k+1))
      END DO
      cwpbkg=wgt(1)*tmp2(1)/g
    END IF

    RETURN
  END SUBROUTINE cwp_forward

  !#####################################################################

  SUBROUTINE ad_cwp_forward(nx,ny,nz,pp,tk,qc,qi,qr,qs,qg,qh,ii,h_cwp,istatus)

    IMPLICIT NONE

    INCLUDE 'globcst.inc'

    INTEGER, INTENT(IN)    :: nx,ny,nz
    REAL,    INTENT(IN)    :: pp(nx,ny,nz),tk(nx,ny,nz)
    REAL,    INTENT(INOUT) :: qc(nx,ny,nz),qi(nx,ny,nz)
    REAL,    INTENT(INOUT) :: qr(nx,ny,nz),qs(nx,ny,nz)
    REAL,    INTENT(INOUT) :: qg(nx,ny,nz),qh(nx,ny,nz)
    INTEGER, INTENT(IN)    :: ii
    REAL,    INTENT(IN)    :: h_cwp

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j,k,n

    INTEGER :: ipt,jpt,phase,cbslv,ctplv
    REAL    :: wgt(4)
    REAL    :: tmp2(4),tmp1(4)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ipt = iptcwp(ii)
    jpt = jptcwp(ii)
    phase  = cldphase(ii)
    cbslv  = cldbslv(ii)
    ctplv  = cldtplv(ii)
    !hcwp   = cwp_bkg(ii)
    wgt(:) = wgtcwp(ii,:)

    tmp1=0.
    tmp2=0.

    IF ( ipt/=nx .and. jpt/=ny ) THEN
      tmp2(1)=wgt(1)*h_cwp/g
      tmp2(2)=wgt(2)*h_cwp/g
      tmp2(3)=wgt(3)*h_cwp/g
      tmp2(4)=wgt(4)*h_cwp/g
      DO k=ctplv-1,cbslv,-1
        tmp1(4)=tmp2(4)*(pp(ipt,jpt+1,k)-pp(ipt,jpt+1,k+1))
        tmp1(3)=tmp2(3)*(pp(ipt+1,jpt+1,k)-pp(ipt+1,jpt+1,k+1))
        tmp1(2)=tmp2(2)*(pp(ipt+1,jpt,k)-pp(ipt+1,jpt,k+1))
        tmp1(1)=tmp2(1)*(pp(ipt,jpt,k)-pp(ipt,jpt,k+1))

        qc(ipt,jpt+1,k)=qc(ipt,jpt+1,k)+tmp1(4)*0.5
        qc(ipt+1,jpt+1,k)=qc(ipt+1,jpt+1,k)+tmp1(3)*0.5
        qc(ipt+1,jpt,k)=qc(ipt+1,jpt,k)+tmp1(2)*0.5
        qc(ipt,jpt,k)=qc(ipt,jpt,k)+tmp1(1)*0.5
        qc(ipt,jpt+1,k+1)=qc(ipt,jpt+1,k+1)+tmp1(4)*0.5
        qc(ipt+1,jpt+1,k+1)=qc(ipt+1,jpt+1,k+1)+tmp1(3)*0.5
        qc(ipt+1,jpt,k+1)=qc(ipt+1,jpt,k+1)+tmp1(2)*0.5
        qc(ipt,jpt,k+1)=qc(ipt,jpt,k+1)+tmp1(1)*0.5
        
        qr(ipt,jpt+1,k)=qr(ipt,jpt+1,k)+tmp1(4)*0.5
        qr(ipt+1,jpt+1,k)=qr(ipt+1,jpt+1,k)+tmp1(3)*0.5
        qr(ipt+1,jpt,k)=qr(ipt+1,jpt,k)+tmp1(2)*0.5
        qr(ipt,jpt,k)=qr(ipt,jpt,k)+tmp1(1)*0.5
        qr(ipt,jpt+1,k+1)=qr(ipt,jpt+1,k+1)+tmp1(4)*0.5
        qr(ipt+1,jpt+1,k+1)=qr(ipt+1,jpt+1,k+1)+tmp1(3)*0.5
        qr(ipt+1,jpt,k+1)=qr(ipt+1,jpt,k+1)+tmp1(2)*0.5
        qr(ipt,jpt,k+1)=qr(ipt,jpt,k+1)+tmp1(1)*0.5

        IF ( phase == 2 ) THEN
          IF (tk(ipt,jpt+1,k)<=zerok) THEN
            qi(ipt,jpt+1,k)=qi(ipt,jpt+1,k)+tmp1(4)*0.5
            qi(ipt,jpt+1,k+1)=qi(ipt,jpt+1,k+1)+tmp1(4)*0.5
          END IF
          IF (tk(ipt+1,jpt+1,k)<=zerok) THEN
            qi(ipt+1,jpt+1,k)=qi(ipt+1,jpt+1,k)+tmp1(3)*0.5
            qi(ipt+1,jpt+1,k+1)=qi(ipt+1,jpt+1,k+1)+tmp1(3)*0.5
          END IF
          IF (tk(ipt+1,jpt,k)<=zerok) THEN
            qi(ipt+1,jpt,k)=qi(ipt+1,jpt,k)+tmp1(2)*0.5
            qi(ipt+1,jpt,k+1)=qi(ipt+1,jpt,k+1)+tmp1(2)*0.5
          END IF
          IF (tk(ipt,jpt,k)<=zerok) THEN
            qi(ipt,jpt,k)=qi(ipt,jpt,k)+tmp1(1)*0.5
            qi(ipt,jpt,k+1)=qi(ipt,jpt,k+1)+tmp1(1)*0.5
          END IF
          
          IF (tk(ipt,jpt+1,k)<=zerok) THEN
            qs(ipt,jpt+1,k)=qs(ipt,jpt+1,k)+tmp1(4)*0.5
            qs(ipt,jpt+1,k+1)=qs(ipt,jpt+1,k+1)+tmp1(4)*0.5
          END IF
          IF (tk(ipt+1,jpt+1,k)<=zerok) THEN
            qs(ipt+1,jpt+1,k)=qs(ipt+1,jpt+1,k)+tmp1(3)*0.5
            qs(ipt+1,jpt+1,k+1)=qs(ipt+1,jpt+1,k+1)+tmp1(3)*0.5
          END IF
          IF (tk(ipt+1,jpt,k)<=zerok) THEN
            qs(ipt+1,jpt,k)=qs(ipt+1,jpt,k)+tmp1(2)*0.5
            qs(ipt+1,jpt,k+1)=qs(ipt+1,jpt,k+1)+tmp1(2)*0.5
          END IF
          IF (tk(ipt,jpt,k)<=zerok) THEN
            qs(ipt,jpt,k)=qs(ipt,jpt,k)+tmp1(1)*0.5
            qs(ipt,jpt,k+1)=qs(ipt,jpt,k+1)+tmp1(1)*0.5
          END IF
          
          IF (P_QG>0) THEN
            IF (tk(ipt,jpt+1,k)<=zerok) THEN
              qg(ipt,jpt+1,k)=qg(ipt,jpt+1,k)+tmp1(4)*0.5
              qg(ipt,jpt+1,k+1)=qg(ipt,jpt+1,k+1)+tmp1(4)*0.5
            END IF
            IF (tk(ipt+1,jpt+1,k)<=zerok) THEN
              qg(ipt+1,jpt+1,k)=qg(ipt+1,jpt+1,k)+tmp1(3)*0.5
              qg(ipt+1,jpt+1,k+1)=qg(ipt+1,jpt+1,k+1)+tmp1(3)*0.5
            END IF
            IF (tk(ipt+1,jpt,k)<=zerok) THEN
              qg(ipt+1,jpt,k)=qg(ipt+1,jpt,k)+tmp1(2)*0.5
              qg(ipt+1,jpt,k+1)=qg(ipt+1,jpt,k+1)+tmp1(2)*0.5
            END IF
            IF (tk(ipt,jpt,k)<=zerok) THEN
              qg(ipt,jpt,k)=qg(ipt,jpt,k)+tmp1(1)*0.5
              qg(ipt,jpt,k+1)=qg(ipt,jpt,k+1)+tmp1(1)*0.5
            END IF
          END IF
          
          IF (P_QH>0) THEN
            IF (tk(ipt,jpt+1,k)<=zerok) THEN
              qh(ipt,jpt+1,k)=qh(ipt,jpt+1,k)+tmp1(4)*0.5
              qh(ipt,jpt+1,k+1)=qh(ipt,jpt+1,k+1)+tmp1(4)*0.5
            END IF
            IF (tk(ipt+1,jpt+1,k)<=zerok) THEN
              qh(ipt+1,jpt+1,k)=qh(ipt+1,jpt+1,k)+tmp1(3)*0.5
              qh(ipt+1,jpt+1,k+1)=qh(ipt+1,jpt+1,k+1)+tmp1(3)*0.5
            END IF
            IF (tk(ipt+1,jpt,k)<=zerok) THEN
              qh(ipt+1,jpt,k)=qh(ipt+1,jpt,k)+tmp1(2)*0.5
              qh(ipt+1,jpt,k+1)=qh(ipt+1,jpt,k+1)+tmp1(2)*0.5
            END IF
            IF (tk(ipt,jpt,k)<=zerok) THEN
              qh(ipt,jpt,k)=qh(ipt,jpt,k)+tmp1(1)*0.5
              qh(ipt,jpt,k+1)=qh(ipt,jpt,k+1)+tmp1(1)*0.5
            END IF
          END IF
        END IF
      END DO
    ELSE IF ( ipt==nx .and. jpt/=ny ) THEN
      tmp2(1)=wgt(1)*h_cwp/g
      tmp2(4)=wgt(4)*h_cwp/g
      DO k=ctplv-1,cbslv,-1
        tmp1(4)=tmp2(4)*(pp(ipt,jpt+1,k)-pp(ipt,jpt+1,k+1))
        tmp1(1)=tmp2(1)*(pp(ipt,jpt,k)-pp(ipt,jpt,k+1))

        qc(ipt,jpt+1,k)=qc(ipt,jpt+1,k)+tmp1(4)*0.5
        qc(ipt,jpt,k)=qc(ipt,jpt,k)+tmp1(1)*0.5
        qc(ipt,jpt+1,k+1)=qc(ipt,jpt+1,k+1)+tmp1(4)*0.5
        qc(ipt,jpt,k+1)=qc(ipt,jpt,k+1)+tmp1(1)*0.5
        
        qr(ipt,jpt+1,k)=qr(ipt,jpt+1,k)+tmp1(4)*0.5
        qr(ipt,jpt,k)=qr(ipt,jpt,k)+tmp1(1)*0.5
        qr(ipt,jpt+1,k+1)=qr(ipt,jpt+1,k+1)+tmp1(4)*0.5
        qr(ipt,jpt,k+1)=qr(ipt,jpt,k+1)+tmp1(1)*0.5

        IF ( phase == 2 ) THEN
          IF (tk(ipt,jpt+1,k)<=zerok) THEN
            qi(ipt,jpt+1,k)=qi(ipt,jpt+1,k)+tmp1(4)*0.5
            qi(ipt,jpt+1,k+1)=qi(ipt,jpt+1,k+1)+tmp1(4)*0.5
          END IF
          IF (tk(ipt,jpt,k)<=zerok) THEN
            qi(ipt,jpt,k)=qi(ipt,jpt,k)+tmp1(1)*0.5
            qi(ipt,jpt,k+1)=qi(ipt,jpt,k+1)+tmp1(1)*0.5
          END IF
          
          IF (tk(ipt,jpt+1,k)<=zerok) THEN
            qs(ipt,jpt+1,k)=qs(ipt,jpt+1,k)+tmp1(4)*0.5
            qs(ipt,jpt+1,k+1)=qs(ipt,jpt+1,k+1)+tmp1(4)*0.5
          END IF
          IF (tk(ipt,jpt,k)<=zerok) THEN
            qs(ipt,jpt,k)=qs(ipt,jpt,k)+tmp1(1)*0.5
            qs(ipt,jpt,k+1)=qs(ipt,jpt,k+1)+tmp1(1)*0.5
          END IF
          
          IF (P_QG>0) THEN
            IF (tk(ipt,jpt+1,k)<=zerok) THEN
              qg(ipt,jpt+1,k)=qg(ipt,jpt+1,k)+tmp1(4)*0.5
              qg(ipt,jpt+1,k+1)=qg(ipt,jpt+1,k+1)+tmp1(4)*0.5
            END IF
            IF (tk(ipt,jpt,k)<=zerok) THEN
              qg(ipt,jpt,k)=qg(ipt,jpt,k)+tmp1(1)*0.5
              qg(ipt,jpt,k+1)=qg(ipt,jpt,k+1)+tmp1(1)*0.5
            END IF
          END IF
          
          IF (P_QH>0) THEN
            IF (tk(ipt,jpt+1,k)<=zerok) THEN
              qh(ipt,jpt+1,k)=qh(ipt,jpt+1,k)+tmp1(4)*0.5
              qh(ipt,jpt+1,k+1)=qh(ipt,jpt+1,k+1)+tmp1(4)*0.5
            END IF
            IF (tk(ipt,jpt,k)<=zerok) THEN
              qh(ipt,jpt,k)=qh(ipt,jpt,k)+tmp1(1)*0.5
              qh(ipt,jpt,k+1)=qh(ipt,jpt,k+1)+tmp1(1)*0.5
            END IF
          END IF
        END IF
      END DO
    ELSE IF ( ipt/=nx .and. jpt==ny ) THEN
      tmp2(1)=wgt(1)*h_cwp/g
      tmp2(2)=wgt(2)*h_cwp/g
      DO k=ctplv-1,cbslv,-1
        tmp1(2)=tmp2(2)*(pp(ipt+1,jpt,k)-pp(ipt+1,jpt,k+1))
        tmp1(1)=tmp2(1)*(pp(ipt,jpt,k)-pp(ipt,jpt,k+1))

        qc(ipt+1,jpt,k)=qc(ipt+1,jpt,k)+tmp1(2)*0.5
        qc(ipt,jpt,k)=qc(ipt,jpt,k)+tmp1(1)*0.5
        qc(ipt+1,jpt,k+1)=qc(ipt+1,jpt,k+1)+tmp1(2)*0.5
        qc(ipt,jpt,k+1)=qc(ipt,jpt,k+1)+tmp1(1)*0.5
        
        qr(ipt+1,jpt,k)=qr(ipt+1,jpt,k)+tmp1(2)*0.5
        qr(ipt,jpt,k)=qr(ipt,jpt,k)+tmp1(1)*0.5
        qr(ipt+1,jpt,k+1)=qr(ipt+1,jpt,k+1)+tmp1(2)*0.5
        qr(ipt,jpt,k+1)=qr(ipt,jpt,k+1)+tmp1(1)*0.5

        IF ( phase == 2 ) THEN
          IF (tk(ipt+1,jpt,k)<=zerok) THEN
            qi(ipt+1,jpt,k)=qi(ipt+1,jpt,k)+tmp1(2)*0.5
            qi(ipt+1,jpt,k+1)=qi(ipt+1,jpt,k+1)+tmp1(2)*0.5
          END IF
          IF (tk(ipt,jpt,k)<=zerok) THEN
            qi(ipt,jpt,k)=qi(ipt,jpt,k)+tmp1(1)*0.5
            qi(ipt,jpt,k+1)=qi(ipt,jpt,k+1)+tmp1(1)*0.5
          END IF
          
          IF (tk(ipt+1,jpt,k)<=zerok) THEN
            qs(ipt+1,jpt,k)=qs(ipt+1,jpt,k)+tmp1(2)*0.5
            qs(ipt+1,jpt,k+1)=qs(ipt+1,jpt,k+1)+tmp1(2)*0.5
          END IF
          IF (tk(ipt,jpt,k)<=zerok) THEN
            qs(ipt,jpt,k)=qs(ipt,jpt,k)+tmp1(1)*0.5
            qs(ipt,jpt,k+1)=qs(ipt,jpt,k+1)+tmp1(1)*0.5
          END IF
          
          IF (P_QG>0) THEN
            IF (tk(ipt+1,jpt,k)<=zerok) THEN
              qg(ipt+1,jpt,k)=qg(ipt+1,jpt,k)+tmp1(2)*0.5
              qg(ipt+1,jpt,k+1)=qg(ipt+1,jpt,k+1)+tmp1(2)*0.5
            END IF
            IF (tk(ipt,jpt,k)<=zerok) THEN
              qg(ipt,jpt,k)=qg(ipt,jpt,k)+tmp1(1)*0.5
              qg(ipt,jpt,k+1)=qg(ipt,jpt,k+1)+tmp1(1)*0.5
            END IF
          END IF
          
          IF (P_QH>0) THEN
            IF (tk(ipt+1,jpt,k)<=zerok) THEN
              qh(ipt+1,jpt,k)=qh(ipt+1,jpt,k)+tmp1(2)*0.5
              qh(ipt+1,jpt,k+1)=qh(ipt+1,jpt,k+1)+tmp1(2)*0.5
            END IF
            IF (tk(ipt,jpt,k)<=zerok) THEN
              qh(ipt,jpt,k)=qh(ipt,jpt,k)+tmp1(1)*0.5
              qh(ipt,jpt,k+1)=qh(ipt,jpt,k+1)+tmp1(1)*0.5
            END IF
          END IF
        END IF
      END DO
    ELSE
      tmp2(1)=wgt(1)*h_cwp/g
      DO k=ctplv-1,cbslv,-1
        tmp1(1)=tmp2(1)*(pp(ipt,jpt,k)-pp(ipt,jpt,k+1))

        qc(ipt,jpt,k)=qc(ipt,jpt,k)+tmp1(1)*0.5
        qc(ipt,jpt,k+1)=qc(ipt,jpt,k+1)+tmp1(1)*0.5
        
        qr(ipt,jpt,k)=qr(ipt,jpt,k)+tmp1(1)*0.5
        qr(ipt,jpt,k+1)=qr(ipt,jpt,k+1)+tmp1(1)*0.5

        IF ( phase == 2 ) THEN
          IF (tk(ipt,jpt,k)<=zerok) THEN
            qi(ipt,jpt,k)=qi(ipt,jpt,k)+tmp1(1)*0.5
            qi(ipt,jpt,k+1)=qi(ipt,jpt,k+1)+tmp1(1)*0.5
          END IF
          
          IF (tk(ipt,jpt,k)<=zerok) THEN
            qs(ipt,jpt,k)=qs(ipt,jpt,k)+tmp1(1)*0.5
            qs(ipt,jpt,k+1)=qs(ipt,jpt,k+1)+tmp1(1)*0.5
          END IF
          
          IF (P_QG>0) THEN
            IF (tk(ipt,jpt,k)<=zerok) THEN
              qg(ipt,jpt,k)=qg(ipt,jpt,k)+tmp1(1)*0.5
              qg(ipt,jpt,k+1)=qg(ipt,jpt,k+1)+tmp1(1)*0.5
            END IF
          END IF
          
          IF (P_QH>0) THEN
            IF (tk(ipt,jpt,k)<=zerok) THEN
              qh(ipt,jpt,k)=qh(ipt,jpt,k)+tmp1(1)*0.5
              qh(ipt,jpt,k+1)=qh(ipt,jpt,k+1)+tmp1(1)*0.5
            END IF
          END IF
        END IF
      END DO
    END IF

    RETURN
  END SUBROUTINE ad_cwp_forward

  !#####################################################################

  SUBROUTINE hydro_forward(nx,ny,nz,qscalar_ctr,ii,hhydro2d,istatus)

    IMPLICIT NONE

    INCLUDE 'globcst.inc'

    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL,    INTENT(IN)  :: qscalar_ctr(nx,ny,nz,nscalarq)
    REAL,    INTENT(OUT) :: hhydro2d(nz,nscalarq)
    INTEGER, INTENT(IN)  :: ii

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------
    INTEGER :: k,n
    INTEGER :: ipt,jpt
    REAL    :: wgt(4)
    !REAL   :: tmp1(4),tmp2(4)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ipt = iptpsd(ii)
    jpt = jptpsd(ii)
    wgt(:) = wgtpsd(ii,:)

    DO k=2,nz-1
      IF ( ipt/=nx .and. jpt/=ny ) THEN
        DO n=1,nscalarq
          hhydro2d(k,n)= wgt(1)*qscalar_ctr(ipt,jpt,k,n)+       &
                         wgt(2)*qscalar_ctr(ipt+1,jpt,k,n)+     &
                         wgt(3)*qscalar_ctr(ipt+1,jpt+1,k,n)+   &
                         wgt(4)*qscalar_ctr(ipt,jpt+1,k,n)
        END DO
      ELSE IF ( ipt==nx .and. jpt/=ny ) THEN
        DO n=1,nscalarq
          hhydro2d(k,n)= wgt(1)*qscalar_ctr(ipt,jpt,k,n)+       &
                         wgt(4)*qscalar_ctr(ipt,jpt+1,k,n)
        END DO
      ELSE IF ( ipt/=nx .and. jpt==ny ) THEN
        DO n=1,nscalarq
          hhydro2d(k,n)= wgt(1)*qscalar_ctr(ipt,jpt,k,n)+       &
                         wgt(2)*qscalar_ctr(ipt+1,jpt,k,n)
        END DO
      ELSE
        DO n=1,nscalarq
          hhydro2d(k,n)= wgt(1)*qscalar_ctr(ipt,jpt,k,n)
        END DO
      END IF
    END DO

    RETURN
  END SUBROUTINE hydro_forward

  !#####################################################################

  SUBROUTINE ad_hydro_forward(nx,ny,nz,qc,qi,qr,qs,qg,qh,ii,hhydro2d,istatus)

    IMPLICIT NONE

    INCLUDE 'globcst.inc'

    INTEGER, INTENT(IN)    :: nx,ny,nz
    REAL,    INTENT(INOUT) :: qc(nx,ny,nz), qi(nx,ny,nz)
    REAL,    INTENT(INOUT) :: qr(nx,ny,nz), qs(nx,ny,nz)
    REAL,    INTENT(INOUT) :: qg(nx,ny,nz), qh(nx,ny,nz)
    INTEGER, INTENT(IN)    :: ii

    REAL,    INTENT(IN)    :: hhydro2d(nz,nscalarq)

    INTEGER, INTENT(OUT)   :: istatus

  !---------------------------------------------------------------------

    INTEGER :: k,n
    INTEGER :: ipt,jpt
    REAL    :: wgt(4)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ipt = iptpsd(ii)
    jpt = jptpsd(ii)
    wgt(:) = wgtpsd(ii,:)


    DO k=nz-1,2,-1
      IF ( ipt/=nx .and. jpt/=ny ) THEN
        qc(ipt,jpt+1,k)=qc(ipt,jpt+1,k)+wgt(4)*hhydro2d(k,P_QC)
        qc(ipt+1,jpt+1,k)=qc(ipt+1,jpt+1,k)+wgt(3)*hhydro2d(k,P_QC)
        qc(ipt+1,jpt,k)=qc(ipt+1,jpt,k)+wgt(2)*hhydro2d(k,P_QC)
        qc(ipt,jpt,k)=qc(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QC)

        qi(ipt,jpt+1,k)=qi(ipt,jpt+1,k)+wgt(4)*hhydro2d(k,P_QI)
        qi(ipt+1,jpt+1,k)=qi(ipt+1,jpt+1,k)+wgt(3)*hhydro2d(k,P_QI)
        qi(ipt+1,jpt,k)=qi(ipt+1,jpt,k)+wgt(2)*hhydro2d(k,P_QI)
        qi(ipt,jpt,k)=qi(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QI)

        qr(ipt,jpt+1,k)=qr(ipt,jpt+1,k)+wgt(4)*hhydro2d(k,P_QR)
        qr(ipt+1,jpt+1,k)=qr(ipt+1,jpt+1,k)+wgt(3)*hhydro2d(k,P_QR)
        qr(ipt+1,jpt,k)=qr(ipt+1,jpt,k)+wgt(2)*hhydro2d(k,P_QR)
        qr(ipt,jpt,k)=qr(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QR)

        qs(ipt,jpt+1,k)=qs(ipt,jpt+1,k)+wgt(4)*hhydro2d(k,P_QS)
        qs(ipt+1,jpt+1,k)=qs(ipt+1,jpt+1,k)+wgt(3)*hhydro2d(k,P_QS)
        qs(ipt+1,jpt,k)=qs(ipt+1,jpt,k)+wgt(2)*hhydro2d(k,P_QS)
        qs(ipt,jpt,k)=qs(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QS)

        IF (P_QG>0) THEN
          qg(ipt,jpt+1,k)=qg(ipt,jpt+1,k)+wgt(4)*hhydro2d(k,P_QG)
          qg(ipt+1,jpt+1,k)=qg(ipt+1,jpt+1,k)+wgt(3)*hhydro2d(k,P_QG)
          qg(ipt+1,jpt,k)=qg(ipt+1,jpt,k)+wgt(2)*hhydro2d(k,P_QG)
          qg(ipt,jpt,k)=qg(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QG)
        END IF

        IF (P_QH>0) THEN
          qh(ipt,jpt+1,k)=qh(ipt,jpt+1,k)+wgt(4)*hhydro2d(k,P_QH)
          qh(ipt+1,jpt+1,k)=qh(ipt+1,jpt+1,k)+wgt(3)*hhydro2d(k,P_QH)
          qh(ipt+1,jpt,k)=qh(ipt+1,jpt,k)+wgt(2)*hhydro2d(k,P_QH)
          qh(ipt,jpt,k)=qh(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QH)
        END IF
      ELSE IF ( ipt==nx .and. jpt/=ny ) THEN
        qc(ipt,jpt+1,k)=qc(ipt,jpt+1,k)+wgt(4)*hhydro2d(k,P_QC)
        qc(ipt,jpt,k)=qc(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QC)

        qi(ipt,jpt+1,k)=qi(ipt,jpt+1,k)+wgt(4)*hhydro2d(k,P_QI)
        qi(ipt,jpt,k)=qi(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QI)

        qr(ipt,jpt+1,k)=qr(ipt,jpt+1,k)+wgt(4)*hhydro2d(k,P_QR)
        qr(ipt,jpt,k)=qr(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QR)

        qs(ipt,jpt+1,k)=qs(ipt,jpt+1,k)+wgt(4)*hhydro2d(k,P_QS)
        qs(ipt,jpt,k)=qs(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QS)

        IF (P_QG>0) THEN
          qg(ipt,jpt+1,k)=qg(ipt,jpt+1,k)+wgt(4)*hhydro2d(k,P_QG)
          qg(ipt,jpt,k)=qg(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QG)
        END IF

        IF (P_QH>0) THEN
          qh(ipt,jpt+1,k)=qh(ipt,jpt+1,k)+wgt(4)*hhydro2d(k,P_QH)
          qh(ipt,jpt,k)=qh(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QH)
        END IF
      ELSE IF ( ipt/=nx .and. jpt==ny ) THEN
        qc(ipt+1,jpt,k)=qc(ipt+1,jpt,k)+wgt(2)*hhydro2d(k,P_QC)
        qc(ipt,jpt,k)=qc(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QC)

        qi(ipt+1,jpt,k)=qi(ipt+1,jpt,k)+wgt(2)*hhydro2d(k,P_QI)
        qi(ipt,jpt,k)=qi(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QI)

        qr(ipt+1,jpt,k)=qr(ipt+1,jpt,k)+wgt(2)*hhydro2d(k,P_QR)
        qr(ipt,jpt,k)=qr(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QR)

        qs(ipt+1,jpt,k)=qs(ipt+1,jpt,k)+wgt(2)*hhydro2d(k,P_QS)
        qs(ipt,jpt,k)=qs(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QS)

        IF (P_QG>0) THEN
          qg(ipt+1,jpt,k)=qg(ipt+1,jpt,k)+wgt(2)*hhydro2d(k,P_QG)
          qg(ipt,jpt,k)=qg(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QG)
        END IF

        IF (P_QH>0) THEN
          qh(ipt+1,jpt,k)=qh(ipt+1,jpt,k)+wgt(2)*hhydro2d(k,P_QH)
          qh(ipt,jpt,k)=qh(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QH)
        END IF
      ELSE
        qc(ipt,jpt,k)=qc(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QC)

        qi(ipt,jpt,k)=qi(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QI)

        qr(ipt,jpt,k)=qr(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QR)

        qs(ipt,jpt,k)=qs(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QS)

        IF (P_QG>0) qg(ipt,jpt,k)=qg(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QG)

        IF (P_QH>0) qh(ipt,jpt,k)=qh(ipt,jpt,k)+wgt(1)*hhydro2d(k,P_QH)
      END IF
    END DO

    RETURN
  END SUBROUTINE ad_hydro_forward

  !#####################################################################

  !SUBROUTINE find_zerolvl(nx,ny,nz,nobs,ipt,jpt,flag,t,p,zerolvl, &
  !                        p0,rddcp,tem)
  !  INTEGER :: nx, ny, nz, nobs
  !  INTEGER, DIMENSION(nobs) :: ipt, jpt, flag
  !  REAL, DIMENSION(nx,ny,nz) :: t, p, tem
  !  INTEGER, DIMENSION(nx,ny) :: zerolvl
  !  REAL :: p0, rddcp
  !  REAL, PARAMETER :: degKtoC=273.15
  !
  !  INTEGER :: iobs, k
  !
  !  tem=0.0
  !  zerolvl=0
  !  DO k= 1,nz-1
  !    DO j= 1,ny-1
  !      DO i= 1,nx-1
  !        tem(i,j,k)=t(i,j,k) * ( (p(i,j,k)/p0)**rddcp )
  !      END DO
  !    END DO
  !  END DO
  !
  !  DO iobs=1,nobs
  !    IF (flag(iobs)>0) THEN
  !      IF (zerolvl(ipt(iobs),jpt(iobs))==0) THEN
  !        DO k=1,nz
  !          IF (tem(ipt(iobs),jpt(iobs),k)<=degKtoC) THEN
  !            zerolvl(ipt(iobs),jpt(iobs))=k
  !            EXIT
  !          END IF
  !        END DO
  !      END IF
  !      IF (zerolvl(ipt(iobs)+1,jpt(iobs))==0) THEN
  !        DO k=1,nz
  !          IF (tem(ipt(iobs)+1,jpt(iobs),k)<=degKtoC) THEN
  !            zerolvl(ipt(iobs)+1,jpt(iobs))=k
  !            EXIT
  !          END IF
  !        END DO
  !      END IF
  !      IF (zerolvl(ipt(iobs)+1,jpt(iobs)+1)==0) THEN
  !        DO k=1,nz
  !          IF (tem(ipt(iobs)+1,jpt(iobs)+1,k)<=degKtoC) THEN
  !            zerolvl(ipt(iobs)+1,jpt(iobs)+1)=k
  !            EXIT
  !          END IF
  !        END DO
  !      END IF
  !      IF (zerolvl(ipt(iobs),jpt(iobs)+1)==0) THEN
  !        DO k=1,nz
  !          IF (tem(ipt(iobs),jpt(iobs)+1,k)<=degKtoC) THEN
  !            zerolvl(ipt(iobs),jpt(iobs)+1)=k
  !            EXIT
  !          END IF
  !        END DO
  !      END IF
  !    END IF
  !  END DO
  !
  !  tem=0.0
  !
  !END SUBROUTINE find_zerolvl

END MODULE module_cwp
