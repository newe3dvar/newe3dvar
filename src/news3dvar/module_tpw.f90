MODULE module_tpw

  USE model_precision
  USE module_tpwobs, ONLY : qsrctpw

  IMPLICIT NONE

  REAL, PARAMETER, PRIVATE :: rhowater=1000.0, gravity=9.81

  INTEGER               :: nobstpw,tottpw

  REAL,    ALLOCATABLE  :: xtpw(:),ytpw(:),obtpw(:),htpw(:),odiftpw(:)
  REAL,    ALLOCATABLE  :: obpwll(:),obpwlm(:),obpwlh(:),   &
                           hpwll(:),hpwlm(:),hpwlh(:),      &
                           odifpwll(:),odifpwlm(:),odifpwlh(:)
  REAL,    ALLOCATABLE  :: p09(:),p07(:),p03(:)
  INTEGER, ALLOCATABLE  :: tpwflag(:),ipttpw(:),jpttpw(:),lvl300(:)
  REAL,    ALLOCATABLE  :: wgttpw(:,:),tpw_diff(:)
  REAL,    ALLOCATABLE  :: pwll_diff(:),pwlm_diff(:),pwlh_diff(:)
  REAL,    ALLOCATABLE  :: interp_ll(:,:),interp_lm(:,:),interp_lh(:,:)
  INTEGER, ALLOCATABLE  :: ml_pwll(:,:),ml_pwlm(:,:),ml_pwlh(:,:)

  REAL(P),  ALLOCATABLE :: tpw_bkg(:)

  CONTAINS

  !#####################################################################

  SUBROUTINE pw_alloc_within_dom(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ALLOCATE (xtpw(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "xtpw")

    ALLOCATE (ytpw(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "ytpw")

    ALLOCATE (tpwflag(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "tpwflag")
    tpwflag=-1

    ALLOCATE (obtpw(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "nobs: Total Precipitable Water")
    obtpw=0.
    ALLOCATE (obpwll(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "PW: Boundary Layer")
    obpwll=0.
    ALLOCATE (obpwlm(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "PW: Middle Layer")
    obpwlm=0.
    ALLOCATE (obpwlh(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "PW: High Layer")
    obpwlh=0.

    ALLOCATE (ipttpw(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "ipttpw")
    ALLOCATE (jpttpw(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "jpttpw")
    ALLOCATE (wgttpw(nobstpw,4),stat=istatus)
    CALL check_alloc_status(istatus, "wgttpw")
    wgttpw=0.
    ALLOCATE (interp_ll(nobstpw,4),stat=istatus)
    CALL check_alloc_status(istatus, "interp_operator_pwlow")
    interp_ll=0.
    ALLOCATE (interp_lm(nobstpw,4),stat=istatus)
    CALL check_alloc_status(istatus, "interp_operator_pwmid")
    interp_lm=0.
    ALLOCATE (interp_lh(nobstpw,4),stat=istatus)
    CALL check_alloc_status(istatus, "interp_operator_pwhigh")
    interp_lh=0.
    ALLOCATE (ml_pwll(nobstpw,4),stat=istatus)
    CALL check_alloc_status(istatus, "model_levels_lowlayer")
    ml_pwll=0
    ALLOCATE (ml_pwlm(nobstpw,4),stat=istatus)
    CALL check_alloc_status(istatus, "model_levels_midlayer")
    ml_pwlm=0
    ALLOCATE (ml_pwlh(nobstpw,4),stat=istatus)
    CALL check_alloc_status(istatus, "model_levels_highlayer")
    ml_pwlh=0

    ALLOCATE (p09(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "Pressure: Low Level")
    p09=-999.
    ALLOCATE (p07(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "Pressure: Mid Level")
    p09=-999.
    ALLOCATE (p03(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "Pressure: High Level")
    p09=-999.

    ALLOCATE (hpwll(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "PW Forward: Low Level")
    hpwll=0.
    ALLOCATE (hpwlm(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "PW Forward: Mid Level")
    hpwlm=0.
    ALLOCATE (hpwlh(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "PW Forward: High Level")
    hpwlh=0.

    ALLOCATE (pwll_diff(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "PW Increment:Low Level")
    pwll_diff=0.
    ALLOCATE (pwlm_diff(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "PW Increment: Mid Level")
    pwlm_diff=0.
    ALLOCATE (pwlh_diff(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "PW Increment: High Level")
    pwlh_diff=0.

  END SUBROUTINE pw_alloc_within_dom

  !#####################################################################

  SUBROUTINE tpw_alloc_within_dom(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    tottpw  = nobstpw

    ALLOCATE (xtpw(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "xtpw")

    ALLOCATE (ytpw(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "ytpw")

    ALLOCATE (obtpw(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "obtpw")

    ALLOCATE (htpw(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "htpw")

    ALLOCATE (odiftpw(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "odiftpw")
    odiftpw=0

    ALLOCATE (tpwflag(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "tpwflag")
    tpwflag=0

    ALLOCATE (ipttpw(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "ipttpw")

    ALLOCATE (jpttpw(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "jpttpw")

    ALLOCATE (wgttpw(nobstpw,4),stat=istatus)
    CALL check_alloc_status(istatus, "wgttpw")

    ALLOCATE (tpw_diff(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "tpw_diff")

    ALLOCATE (lvl300(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "lvl300")
    lvl300=0

    ALLOCATE (tpw_bkg(nobstpw),stat=istatus)
    CALL check_alloc_status(istatus, "tpw_bkg")

    RETURN
  END SUBROUTINE tpw_alloc_within_dom

  !#####################################################################

  SUBROUTINE tpw_dealloc_within_dom(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    DEALLOCATE (xtpw,      stat=istatus)
    DEALLOCATE (ytpw,      stat=istatus)
    DEALLOCATE (obtpw,     stat=istatus)
    DEALLOCATE (htpw,      stat=istatus)
    DEALLOCATE (odiftpw,   stat=istatus)
    DEALLOCATE (tpwflag,   stat=istatus)
    DEALLOCATE (ipttpw,    stat=istatus)
    DEALLOCATE (jpttpw,    stat=istatus)
    DEALLOCATE (wgttpw,    stat=istatus)
    DEALLOCATE (tpw_diff,  stat=istatus)
    DEALLOCATE (lvl300,    stat=istatus)

    DEALLOCATE (tpw_bkg,    stat=istatus)

  END SUBROUTINE tpw_dealloc_within_dom

  !#####################################################################

  SUBROUTINE tpw_costf(nx,ny,nz,zp,rhobar,q_ctr,f_otpw,                 &
                       q_ctr1,tem1,istatus)

    INTEGER,  INTENT(IN)  :: nx,ny,nz
    REAL,     INTENT(IN)  :: zp(nx,ny,nz), rhobar(nx,ny,nz)
    REAL,     INTENT(IN)  :: q_ctr(nx,ny,nz)
    REAL(DP), INTENT(OUT) :: f_otpw

    REAL,     INTENT(OUT) :: q_ctr1(nx,ny,nz)
    REAL,     INTENT(OUT) :: tem1(nx,ny,nz)

    INTEGER,  INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER  :: i,j,k
    REAL(DP) :: sum1, sum2

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    f_otpw = 0.0D0

    tpw_bkg = 0.0; tpw_diff = 0.0

    DO i=1,nobstpw
      IF (tpwflag(i)>0) THEN

        CALL tpw_forward(nx,ny,nz,zp,rhobar,q_ctr,i,tpw_bkg(i),tem1,istatus)

        tpw_diff(i)=(tpw_bkg(i)-obtpw(i))/qsrctpw(1,1)
        f_otpw=f_otpw+tpw_diff(i)*(tpw_bkg(i)-obtpw(i))
      ELSE
        tpw_diff(i)=0
      END IF
    END DO

    IF(0==1) then   ! for verification of adjoint

      sum1 =0.0
      DO i=1,nobstpw
        sum1=sum1+tpw_bkg(i)*tpw_bkg(i)
      END DO
      q_ctr1 = 0.0
      DO i=1,nobstpw
        CALL ad_tpw_forward(nx,ny,nz,zp,rhobar,q_ctr,i,tpw_bkg(i),      &
                            q_ctr1,tem1,istatus)
      END DO


      sum2 =0.0
      DO k = MAXVAL(lvl300)+1, nz
        DO j = 1, ny
          DO i = 1, nx
            sum2=sum2+ q_ctr(i,j,k)*q_ctr1(i,j,k)
          END DO
        END DO
      END DO
      print*,'tpw,sum1,sum2=',sum1,sum2
      
    END IF

    RETURN
  END SUBROUTINE tpw_costf

  !#####################################################################

  SUBROUTINE tpw_gradt(nx,ny,nz,zp,rhobar,q_ctr,q_grd,dqv,tem1,istatus)

    INTEGER,  INTENT(IN)    :: nx,ny,nz
    REAL,     INTENT(IN)    :: zp(nx,ny,nz), rhobar(nx,ny,nz)
    REAL,     INTENT(IN)    :: q_ctr(nx,ny,nz)
    REAL,     INTENT(INOUT) :: q_grd(nx,ny,nz)

    REAL,     INTENT(OUT)   :: dqv(nx,ny,nz), tem1(nx,ny,nz)   ! work arrays

    INTEGER,  INTENT(OUT)   :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j,k

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    dqv=0.0

    DO i=1,nobstpw
      IF (tpwflag(i)>0) THEN

        CALL ad_tpw_forward(nx,ny,nz,zp,rhobar,q_ctr,i,tpw_diff(i),     &
                            dqv,tem1,istatus)
      END IF
    END DO

    q_grd=q_grd+dqv

    RETURN
  END SUBROUTINE tpw_gradt

  !#####################################################################

  SUBROUTINE tpw_forward(nx,ny,nz,zp,rho,qv,ii,dtpw,vp,istatus)

    IMPLICIT NONE

    INCLUDE 'globcst.inc'

    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL,    INTENT(IN)  :: zp(nx,ny,nz),rho(nx,ny,nz),qv(nx,ny,nz)
    INTEGER, INTENT(IN)  :: ii

    REAL,    INTENT(OUT) :: dtpw
    REAL,    INTENT(OUT) :: vp(nx,ny,nz)

    INTEGER, INTENT(OUT)  :: istatus
  !---------------------------------------------------------------------

    INTEGER :: i,j,k,n
    INTEGER :: ipt,jpt,lvl
    REAL    :: wgt(4)

    REAL    :: tmp1(4)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ipt = ipttpw(ii)
    jpt = jpttpw(ii)
    lvl = lvl300(ii)
    wgt(:) = wgttpw(ii,:)

    tmp1=0.

  ! DO k=2,lvl
  !   vp(ipt,jpt,k)=qv(ipt,jpt,k)/(1-qv(ipt,jpt,k))
  ! END DO
    vp(:,:,:)=qv(:,:,:)

    IF ( ipt/=nx .and. jpt/=ny ) THEN
      DO k=2,lvl

        tmp1(1)=tmp1(1)+vp(ipt,jpt,k)*rho(ipt,jpt,k)*          &
                (zp(ipt,jpt,k+1)-zp(ipt,jpt,k))
        tmp1(2)=tmp1(2)+vp(ipt+1,jpt,k)*rho(ipt+1,jpt,k)*      &
                (zp(ipt+1,jpt,k+1)-zp(ipt+1,jpt,k))
        tmp1(3)=tmp1(3)+vp(ipt+1,jpt+1,k)*rho(ipt+1,jpt+1,k)*  &
                (zp(ipt+1,jpt+1,k+1)-zp(ipt+1,jpt+1,k))
        tmp1(4)=tmp1(4)+vp(ipt,jpt+1,k)*rho(ipt,jpt+1,k)*      &
                (zp(ipt,jpt+1,k+1)-zp(ipt,jpt+1,k))

      END DO
      tmp1=tmp1*100/rhowater  !*100 is the convertion from m to cm
      dtpw=wgt(1)*tmp1(1)+wgt(2)*tmp1(2)+      &
           wgt(3)*tmp1(3)+wgt(4)*tmp1(4)
    ELSE IF ( ipt==nx .and. jpt/=nx ) THEN
      DO k=2,lvl

        tmp1(1)=tmp1(1)+vp(ipt,jpt,k)*rho(ipt,jpt,k)*      &
                (zp(ipt,jpt,k+1)-zp(ipt,jpt,k))
        tmp1(4)=tmp1(4)+vp(ipt,jpt+1,k)*rho(ipt,jpt+1,k)*    &
                (zp(ipt,jpt+1,k+1)-zp(ipt,jpt+1,k))

      END DO
      tmp1=tmp1*100/rhowater
      dtpw=wgt(1)*tmp1(1)+wgt(4)*tmp1(4)
    ELSE IF ( ipt/=nx .and. jpt==nx ) THEN
      DO k=2,lvl

        tmp1(1)=tmp1(1)+vp(ipt,jpt,k)*rho(ipt,jpt,k)*      &
                (zp(ipt,jpt,k+1)-zp(ipt,jpt,k))
        tmp1(2)=tmp1(2)+vp(ipt+1,jpt,k)*rho(ipt+1,jpt,k)*    &
                (zp(ipt+1,jpt,k+1)-zp(ipt+1,jpt,k))

      END DO
      tmp1=tmp1*100/rhowater
      dtpw=wgt(1)*tmp1(1)+wgt(2)*tmp1(2)
    ELSE
      DO k=2,lvl
        tmp1(1)=tmp1(1)+vp(ipt,jpt,k)*rho(ipt,jpt,k)*      &
                (zp(ipt,jpt,k+1)-zp(ipt,jpt,k))
      END DO
      tmp1=tmp1*100/rhowater
      dtpw=wgt(1)*tmp1(1)
    END IF

    RETURN
  END SUBROUTINE tpw_forward

  !#####################################################################

  SUBROUTINE ad_tpw_forward(nx,ny,nz,zp,rho,qv,ii,dtpw,dqv,vp,istatus)

    IMPLICIT NONE

    INCLUDE 'globcst.inc'

    INTEGER, INTENT(IN)    :: nx,ny,nz
    REAL,    INTENT(IN)    :: zp(nx,ny,nz),rho(nx,ny,nz),qv(nx,ny,nz)
    REAL,    INTENT(INOUT) :: dqv(nx,ny,nz)
    INTEGER, INTENT(IN)    :: ii
    REAL,    INTENT(IN)    :: dtpw

    REAL,    INTENT(OUT)   :: vp(nx,ny,nz)

    INTEGER, INTENT(OUT)   :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j,k
    INTEGER :: ipt,jpt,lvl
    REAL    :: wgt(4)
    REAL    :: tmp1(4)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ipt = ipttpw(ii)
    jpt = jpttpw(ii)
    lvl = lvl300(ii)
    wgt(:) = wgttpw(ii,:)

    tmp1=0.
    vp(:,:,:)=0.

    IF ( ipt/=nx .and. jpt/=ny ) THEN
      tmp1(1)=wgt(1)*dtpw
      tmp1(2)=wgt(2)*dtpw
      tmp1(3)=wgt(3)*dtpw
      tmp1(4)=wgt(4)*dtpw
      tmp1=tmp1*100/rhowater
      DO k=lvl,2,-1
        vp(ipt,jpt+1,k)=vp(ipt,jpt+1,k)+tmp1(4)*(rho(ipt,jpt+1,k)*      &
                        (zp(ipt,jpt+1,k+1)-zp(ipt,jpt+1,k)))
        vp(ipt+1,jpt+1,k)=vp(ipt+1,jpt+1,k)+tmp1(3)*(rho(ipt+1,jpt+1,k)*      &
                          (zp(ipt+1,jpt+1,k+1)-zp(ipt+1,jpt+1,k)))
        vp(ipt+1,jpt,k)=vp(ipt+1,jpt,k)+tmp1(2)*(rho(ipt+1,jpt,k)*      &
                        (zp(ipt+1,jpt,k+1)-zp(ipt+1,jpt,k)))
        vp(ipt,jpt,k)=vp(ipt,jpt,k)+tmp1(1)*(rho(ipt,jpt,k)*      &
                      (zp(ipt,jpt,k+1)-zp(ipt,jpt,k)))
      END DO
    ELSE IF ( ipt==nx .and. jpt/=ny ) THEN
      tmp1(1)=wgt(1)*dtpw
      tmp1(4)=wgt(4)*dtpw
      tmp1=tmp1*100/rhowater
      DO k=lvl,2,-1
        vp(ipt,jpt+1,k)=vp(ipt,jpt+1,k)+tmp1(4)*(rho(ipt,jpt+1,k)*      &
                        (zp(ipt,jpt+1,k+1)-zp(ipt,jpt+1,k)))
        vp(ipt,jpt,k)=vp(ipt,jpt,k)+tmp1(1)*(rho(ipt,jpt,k)*      &
                      (zp(ipt,jpt,k+1)-zp(ipt,jpt,k)))
      END DO
    ELSE IF ( ipt/=nx .and. jpt==ny ) THEN
      tmp1(1)=wgt(1)*dtpw
      tmp1(2)=wgt(2)*dtpw
      tmp1=tmp1*100/rhowater
      DO k=lvl,2,-1
        vp(ipt+1,jpt,k)=vp(ipt+1,jpt,k)+tmp1(2)*(rho(ipt+1,jpt,k)*      &
                        (zp(ipt+1,jpt,k+1)-zp(ipt+1,jpt,k)))
        vp(ipt,jpt,k)=vp(ipt,jpt,k)+tmp1(1)*(rho(ipt,jpt,k)*      &
                      (zp(ipt,jpt,k+1)-zp(ipt,jpt,k)))
      END DO
    ELSE
      tmp1(1)=wgt(1)*dtpw
      tmp1=tmp1*100/rhowater
      DO k=lvl,2,-1
        vp(ipt,jpt,k)=vp(ipt,jpt,k)+tmp1(1)*(rho(ipt,jpt,k)*      &
                      (zp(ipt,jpt,k+1)-zp(ipt,jpt,k)))
      END DO
    END IF

  ! DO k=lvl,2,-1
  !   dqv(ipt,jpt,k)=dqv(ipt,jpt,k)+vp(ipt,jpt,k)/((1-qv(ipt,jpt,k))**2)
  ! END DO
    dqv(:,:,:)=dqv(:,:,:)+vp(:,:,:)

    RETURN
  END SUBROUTINE ad_tpw_forward

  !#####################################################################

  SUBROUTINE pw_costf(nx,ny,nz,p,q_ctr,f_opwll,f_opwlm,f_opwlh,    &
                      tem1,tem2,istatus)

    INTEGER,  INTENT(IN)  :: nx,ny,nz
    REAL,     INTENT(IN)  :: p(nx,ny,nz)
    REAL,     INTENT(IN)  :: q_ctr(nx,ny,nz)
    REAL(DP), INTENT(OUT) :: f_opwll,f_opwlm,f_opwlh

    REAL,     INTENT(OUT) :: tem1(nx,ny,nz),tem2(nx,ny,nz)

    INTEGER,  INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER  :: i,j,k
    REAL(DP) :: sum1, sum2

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    f_opwll = 0.0D0; f_opwlm = 0.0D0; f_opwlh = 0.0D0

    hpwll = 0.0; hpwlm = 0.0; hpwlh = 0.0
    pwll_diff = 0.0; pwlm_diff = 0.0; pwlh_diff = 0.0

    DO i=1,nobstpw
      IF (tpwflag(i)>0) THEN

        CALL pw_forward(nx,ny,nz,p,q_ctr,i,tem1,istatus)

        pwll_diff(i)=(hpwll(i)-obpwll(i))/qsrctpw(2,1)
        pwlm_diff(i)=(hpwlm(i)-obpwlm(i))/qsrctpw(3,1)
!       pwlh_diff(i)=(hpwlh(i)-obpwlh(i))/qsrctpw(4,1)
        IF (tpwflag(i)==2) THEN
          f_opwll=f_opwll+pwll_diff(i)*(hpwll(i)-obpwll(i))
          f_opwlm=f_opwlm+pwlm_diff(i)*(hpwlm(i)-obpwlm(i))
!         f_opwlh=f_opwlh+pwlh_diff(i)*(hpwlh(i)-obpwlh(i))
        END IF
      ELSE
        pwll_diff(i)=0
        pwlm_diff(i)=0
!       pwlh_diff(i)=0
      END IF
    END DO

    IF(0==1) then   ! for verification of adjoint

      sum1=0.0
      DO i=1,nobstpw
          sum1=sum1+hpwll(i)**2+hpwlm(i)**2+hpwlh(i)**2
      END DO
      tem1=0.0
      DO i=1,nobstpw
        IF (tpwflag(i)>0) THEN
          CALL ad_pw_forward(nx,ny,nz,p,q_ctr,i,tem1,tem2,istatus)
        END IF
      END DO

      sum2=0.0
      DO k = 1, nz
        DO j = 1, ny
          DO i = 1, nx
            sum2=sum2+ q_ctr(i,j,k)*tem1(i,j,k)
          END DO
        END DO
      END DO
      print*,'pw_ll,sum1,sum2=',sum1,sum2
      STOP
      
    END IF

    RETURN
  END SUBROUTINE pw_costf
  
  !#####################################################################

  SUBROUTINE pw_gradt(nx,ny,nz,p,q_ctr,q_grd,dqv,tem1,istatus)

    INCLUDE 'bndry.inc'
    INCLUDE 'mp.inc'

    INTEGER,  INTENT(IN)    :: nx,ny,nz
    REAL,     INTENT(IN)    :: p(nx,ny,nz), q_ctr(nx,ny,nz)
    REAL,     INTENT(INOUT) :: q_grd(nx,ny,nz)

    REAL,     INTENT(OUT)   :: dqv(nx,ny,nz), tem1(nx,ny,nz)   ! work arrays

    INTEGER,  INTENT(OUT)   :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j,k

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    dqv=0.0

    DO i=1,nobstpw
      IF (tpwflag(i)>0) THEN

        CALL ad_pw_forward(nx,ny,nz,p,q_ctr,i,dqv,tem1,istatus)
                            
      END IF
    END DO

    IF (mp_opt>0) THEN
      CALL mpsendrecv2dew(dqv,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(dqv,nx,ny,nz,nbc,sbc,0,tem1)
    END IF

    q_grd=q_grd+dqv

    RETURN
  END SUBROUTINE pw_gradt

  !#####################################################################

  SUBROUTINE pw_forward(nx,ny,nz,p,qv,ii,vp,istatus)

    IMPLICIT NONE

    INCLUDE 'globcst.inc'

    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL,    INTENT(IN)  :: p(nx,ny,nz),qv(nx,ny,nz)
    INTEGER, INTENT(IN)  :: ii

    REAL,    INTENT(OUT) :: vp(nx,ny,nz)

    INTEGER, INTENT(OUT)  :: istatus
  !---------------------------------------------------------------------

    INTEGER :: k,n
    INTEGER :: ipt(4),jpt(4)
    REAL    :: wgt(4)

    REAL    :: tmp1(4),tmp2(4),tmp3(4)
    REAL    :: q09(4),q07(4)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ipt(1) = ipttpw(ii)
    jpt(1) = jpttpw(ii)
    ipt(2) = ipttpw(ii)+1
    jpt(2) = jpttpw(ii)
    ipt(3) = ipttpw(ii)+1
    jpt(3) = jpttpw(ii)+1
    ipt(4) = ipttpw(ii)
    jpt(4) = jpttpw(ii)+1

    wgt(:) = wgttpw(ii,:)

    tmp1=0.
    tmp2=0.
    tmp3=0.
    q09=0.
    q07=0.

    vp(:,:,:)=qv(:,:,:)

    DO n=1,4
      DO k=2,ml_pwll(ii,n)
        IF (k==ml_pwll(ii,n)) THEN
          tmp1(n)=tmp1(n)+vp(ipt(n),jpt(n),k)*(p09(ii)-p(ipt(n),jpt(n),k))
        ELSE
          tmp1(n)=tmp1(n)+vp(ipt(n),jpt(n),k)*(p(ipt(n),jpt(n),k+1)-p(ipt(n),jpt(n),k))
        END IF
      END DO
      q09(n)=vp(ipt(n),jpt(n),ml_pwll(ii,n))+      &
             (vp(ipt(n),jpt(n),ml_pwll(ii,n)+1)-vp(ipt(n),jpt(n),ml_pwll(ii,n)))*interp_ll(ii,n)
      DO k=ml_pwll(ii,n),ml_pwlm(ii,n)
        IF (k==ml_pwll(ii,n)) THEN
          tmp2(n)=tmp2(n)+q09(n)*(p(ipt(n),jpt(n),k+1)-p09(ii))
        ELSE IF (k==ml_pwlm(ii,n)) THEN
          tmp2(n)=tmp2(n)+vp(ipt(n),jpt(n),k)*(p07(ii)-p(ipt(n),jpt(n),k))
        ELSE
          tmp2(n)=tmp2(n)+vp(ipt(n),jpt(n),k)*(p(ipt(n),jpt(n),k+1)-p(ipt(n),jpt(n),k))
        END IF
      END DO
!     q07(n)=vp(ipt(n),jpt(n),ml_pwlm(ii,n))+      &
!            (vp(ipt(n),jpt(n),ml_pwlm(ii,n)+1)-vp(ipt(n),jpt(n),ml_pwlm(ii,n)))*interp_lm(ii,n)
!     DO k=ml_pwlm(ii,n),ml_pwlh(ii,n)
!       IF (k==ml_pwlm(ii,n)) THEN
!         tmp3(n)=tmp3(n)+q07(n)*(p(ipt(n),jpt(n),k+1)-p07(ii))
!       ELSE IF (k==ml_pwlh(ii,n)) THEN
!         tmp3(n)=tmp3(n)+vp(ipt(n),jpt(n),k)*(p03(ii)-p(ipt(n),jpt(n),k))
!       ELSE
!         tmp3(n)=tmp3(n)+vp(ipt(n),jpt(n),k)*(p(ipt(n),jpt(n),k+1)-p(ipt(n),jpt(n),k))
!       END IF
!     END DO
    END DO
    tmp1=-1*tmp1*100/(rhowater*gravity)  !*100 is the convertion from m to cm
    tmp2=-1*tmp2*100/(rhowater*gravity)
!   tmp3=-1*tmp3*100/(rhowater*gravity)
    hpwll(ii)=wgt(1)*tmp1(1)+wgt(2)*tmp1(2)+      &
              wgt(3)*tmp1(3)+wgt(4)*tmp1(4)
    hpwlm(ii)=wgt(1)*tmp2(1)+wgt(2)*tmp2(2)+      &
              wgt(3)*tmp2(3)+wgt(4)*tmp2(4)
!   hpwlh(ii)=wgt(1)*tmp3(1)+wgt(2)*tmp3(2)+      &
!             wgt(3)*tmp3(3)+wgt(4)*tmp3(4)

    RETURN
  END SUBROUTINE pw_forward

  !#####################################################################

  SUBROUTINE ad_pw_forward(nx,ny,nz,p,qv,ii,dqv,vp,istatus)

    IMPLICIT NONE

    INCLUDE 'globcst.inc'

    INTEGER, INTENT(IN)    :: nx,ny,nz
    REAL,    INTENT(IN)    :: p(nx,ny,nz),qv(nx,ny,nz)
    REAL,    INTENT(INOUT) :: dqv(nx,ny,nz)
    INTEGER, INTENT(IN)    :: ii

    REAL,    INTENT(OUT)   :: vp(nx,ny,nz)

    INTEGER, INTENT(OUT)   :: istatus
  !---------------------------------------------------------------------

    INTEGER :: k,n
    INTEGER :: ipt(4),jpt(4)
    REAL    :: wgt(4)

    REAL    :: tmp1(4),tmp2(4),tmp3(4)
    REAL    :: q09(4),q07(4)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ipt(1) = ipttpw(ii)
    jpt(1) = jpttpw(ii)
    ipt(2) = ipttpw(ii)+1
    jpt(2) = jpttpw(ii)
    ipt(3) = ipttpw(ii)+1
    jpt(3) = jpttpw(ii)+1
    ipt(4) = ipttpw(ii)
    jpt(4) = jpttpw(ii)+1

    wgt(:) = wgttpw(ii,:)

    tmp1=0.
    tmp2=0.
    tmp3=0.
    q09=0.
    q07=0.

    vp(:,:,:)=0.
!   !%part for only testing adjoint
!   pwll_diff(ii)=hpwll(ii)
!   pwlm_diff(ii)=hpwlm(ii)
!   pwlh_diff(ii)=hpwlh(ii)
!   !%-----------------------------
    DO n=4,1,-1
      tmp1(n)=wgt(n)*pwll_diff(ii)
      tmp2(n)=wgt(n)*pwlm_diff(ii)
!     tmp3(n)=wgt(n)*pwlh_diff(ii)
    END DO
    tmp1=-1*tmp1*100/(rhowater*gravity)
    tmp2=-1*tmp2*100/(rhowater*gravity)
!   tmp3=-1*tmp3*100/(rhowater*gravity)
    
    DO n=4,1,-1
!     DO k=ml_pwlh(ii,n),ml_pwlm(ii,n),-1
!       IF (k==ml_pwlm(ii,n)) THEN
!         q07(n)=q07(n)+tmp3(n)*(p(ipt(n),jpt(n),k+1)-p07(ii))
!       ELSE IF (k==ml_pwlh(ii,n)) THEN
!         vp(ipt(n),jpt(n),k)=vp(ipt(n),jpt(n),k)+tmp3(n)*(p03(ii)-p(ipt(n),jpt(n),k))
!       ELSE
!         vp(ipt(n),jpt(n),k)=vp(ipt(n),jpt(n),k)+tmp3(n)*(p(ipt(n),jpt(n),k+1)-p(ipt(n),jpt(n),k))
!       END IF
!     END DO
!     vp(ipt(n),jpt(n),ml_pwlm(ii,n))=vp(ipt(n),jpt(n),ml_pwlm(ii,n))+q07(n)*(1-interp_lm(ii,n))
!     vp(ipt(n),jpt(n),ml_pwlm(ii,n)+1)=vp(ipt(n),jpt(n),ml_pwlm(ii,n)+1)+q07(n)*interp_lm(ii,n)
      DO k=ml_pwlm(ii,n),ml_pwll(ii,n),-1
        IF (k==ml_pwll(ii,n)) THEN
          q09(n)=q09(n)+tmp2(n)*(p(ipt(n),jpt(n),k+1)-p09(ii))
        ELSE IF (k==ml_pwlm(ii,n)) THEN
          vp(ipt(n),jpt(n),k)=vp(ipt(n),jpt(n),k)+tmp2(n)*(p07(ii)-p(ipt(n),jpt(n),k))
        ELSE
          vp(ipt(n),jpt(n),k)=vp(ipt(n),jpt(n),k)+tmp2(n)*(p(ipt(n),jpt(n),k+1)-p(ipt(n),jpt(n),k))
        END IF
      END DO
      vp(ipt(n),jpt(n),ml_pwll(ii,n))=vp(ipt(n),jpt(n),ml_pwll(ii,n))+q09(n)*(1-interp_ll(ii,n))
      vp(ipt(n),jpt(n),ml_pwll(ii,n)+1)=vp(ipt(n),jpt(n),ml_pwll(ii,n)+1)+q09(n)*interp_ll(ii,n)
      DO k=ml_pwll(ii,n),2,-1
        IF (k==ml_pwll(ii,n)) THEN
          vp(ipt(n),jpt(n),k)=vp(ipt(n),jpt(n),k)+tmp1(n)*(p09(ii)-p(ipt(n),jpt(n),k))
        ELSE
          vp(ipt(n),jpt(n),k)=vp(ipt(n),jpt(n),k)+tmp1(n)*(p(ipt(n),jpt(n),k+1)-p(ipt(n),jpt(n),k))
        END IF
      END DO
    END DO
    
    dqv=dqv+vp

    RETURN
  END SUBROUTINE ad_pw_forward

END MODULE module_tpw
