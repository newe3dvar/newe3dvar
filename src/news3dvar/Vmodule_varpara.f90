
MODULE module_varpara

  IMPLICIT NONE
  SAVE

!
!-----------------------------------------------------------------------
!
!  3DVAR control parameters
!  Input via namelist in INIT3DVAR
!
!-----------------------------------------------------------------------
!
  !INTEGER, PARAMETER :: maxpass = 8
  INTEGER, ALLOCATABLE :: maxin(:)
  INTEGER              :: qobsrad_bdyopt
  INTEGER, ALLOCATABLE :: qobsrad_bdyzone(:)
  REAL                 :: qback_factor

  INTEGER, ALLOCATABLE :: cldfirst_opt(:)
  INTEGER, ALLOCATABLE :: pseudo_pt(:), pseudo_qv(:), pseudo_qs(:)
  INTEGER, ALLOCATABLE :: vrob_opt(:)
  INTEGER :: idealCase_opt
  INTEGER :: sglob_opt
  INTEGER :: hydro_opt

  INTEGER, ALLOCATABLE :: ipass_filt(:), vradius_opt(:)
  REAL,    ALLOCATABLE :: hradius(:),vradius(:)
  INTEGER :: smooth_opt,filt_type, flag_legacyfilter, flag_iso

  INTEGER :: chk_opt,assim_opt
  INTEGER, PARAMETER :: cntl_var = 0
  INTEGER :: cntl_var_rh

  REAL    :: intvl_T, thresh_ref, bkgerr_qr, bkgerr_qs, bkgerr_qh
  INTEGER, ALLOCATABLE :: ref_opt(:), vradius_opt_ref(:)
  REAL    :: pse_qr_err, pse_qs_err, pse_qg_err,  pse_qh_err
  REAL,    ALLOCATABLE :: wgt_ref(:), hradius_ref(:), vradius_ref(:)

  INTEGER :: refout
  !
  ! Analysis variable indices in the big array "anx"
  !
  INTEGER, PARAMETER :: PTR_U  = 1, PTR_V = 2, PTR_P = 3, PTR_PT = 4,   &
                        PTR_QV = 5, PTR_W = 6, PTR_LN = 6
                ! QSCALAR variable order depends on case (no fixed assumption)
                        !PTR_QC = 7, PTR_QR = 8,   &
                        !PTR_QI = 9, PTR_QS=10,   &
                        !PTR_QH = 9, PTR_QG = 12

  !
  ! var analysis indices
  !
  INTEGER :: ibgn, iend, jbgn, jend, kbgn,kend
  INTEGER :: iendu, jendv, kendw
  INTEGER :: icall
  !INTEGER :: ibgn_m, iend_m, jbgn_m, jend_m, kbgn_m,kend_m

  CONTAINS
  !
  !
  !##################################################################
  !##################################################################
  !######                                                      ######
  !######                SUBROUTINE INITMODEL                  ######
  !######                                                      ######
  !##################################################################
  !##################################################################
  !

  SUBROUTINE init_model_para(namelist_filename)
  !
  !-----------------------------------------------------------------------
  !
  !  PURPOSE:
  !
  !  Start the program formally and initialize the model control parameters.
  !
  !-----------------------------------------------------------------------
  !
  !  AUTHOR: Yunheng Wang (03/10/2017)
  !  Modified based on initpara in the ARPS package.
  !
  !  MODIFICATION HISTORY:
  !
  !-----------------------------------------------------------------------
  !
  !  Variable Declarations. (Local Variables)
  !
  !-----------------------------------------------------------------------
  !
    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(OUT) :: namelist_filename
  !
  !-----------------------------------------------------------------------
  !
  !  Misc. local variables:
  !
  !-----------------------------------------------------------------------
  !
    REAL    :: wrmax            ! Maximun value of canopy moisture
    INTEGER :: i

    INTEGER :: lenstr      ! Length of a string
    LOGICAL :: iexist      ! Flag set by inquire statement for file
                           ! existence
    REAL    :: temr
    REAL    :: dtsml0,dtsfc0        ! Temporary variable

    CHARACTER (LEN=19)  :: initime  ! Real time in form of 'year-mo-dy:hr:mn:ss'

    INTEGER :: unum         ! unit number for reading in namelist
    CHARACTER(LEN=256)   :: nlfile, outdirname

    INTEGER :: inisplited, dmp_out_joined
    INTEGER :: idummy

  !
  !-----------------------------------------------------------------------
  !
  !  Include files:
  !
  !-----------------------------------------------------------------------
  !
  !
  !-----------------------------------------------------------------------
  !
  !  Global constants and parameters, most of them specify the
  !  model run options.
  !
  !-----------------------------------------------------------------------
  !
    INCLUDE 'globcst.inc'
  !
  !-----------------------------------------------------------------------
  !
  !  Grid and map parameters.
  !
  !-----------------------------------------------------------------------
  !
    INCLUDE 'grid.inc'
  !
  !-----------------------------------------------------------------------
  !
  !  Control parameters defining the boundary condition types.
  !
  !-----------------------------------------------------------------------
  !
    INCLUDE 'bndry.inc'
  !
  !-----------------------------------------------------------------------
  !
  !  Universal physical constants such as gas constants.
  !
  !-----------------------------------------------------------------------
  !
    INCLUDE 'phycst.inc'
  !
  !-----------------------------------------------------------------------
  !
  !  External boundary parameters and variables.
  !
  !-----------------------------------------------------------------------
  !
    INCLUDE 'exbc.inc'
  !
  !-----------------------------------------------------------------------
  !
  !  Message passing parameters.
  !
  !-----------------------------------------------------------------------
  !
    INCLUDE 'mp.inc'
  !
  !-----------------------------------------------------------------------
  ! Flags for fields readin from history format initial condition file
  ! e.g., tkein is used to determine whether tke exists in history IC file.
  !-----------------------------------------------------------------------

    INCLUDE 'indtflg.inc'
  !
  !-----------------------------------------------------------------------
  !
  !  namelist Declarations:
  !
  !-----------------------------------------------------------------------
  !
    NAMELIST /message_passing/ nproc_x,nproc_y,max_fopen,nproc_x_in,nproc_y_in

    NAMELIST /comment_lines/ nocmnt, cmnt

    NAMELIST /jobname/ runname

    NAMELIST /initialization/ modelopt,initime,inifmt,inisplited,inifile,inigbf

    NAMELIST /grid/ dz,strhopt,dzmin,zrefsfc,dlayer1,dlayer2,           &
              strhtune,zflat

    NAMELIST /soil_ebm/ soilmodel_option,soilstrhopt,                   &
              dzsoil,zrefsoil,                                          &
              soildzmin,soildlayer1,soildlayer2,soilstrhtune

    NAMELIST /history_dump/ dmp_out_joined,hdmpfmt,hdfcompr

    NAMELIST /output/ dirname,readyfl,refout,                           &
              grdout,basout,varout,mstout,rainout,prcout,               &
              iceout,tkeout, trbout,sfcout,landout,totout,              &
              radout,flxout

    NAMELIST /debug/ lvldbg

    REAL    :: dh
    INTEGER :: err_no
    DATA err_no /0/

    INTEGER :: ip
    INTEGER :: ebcsv,wbcsv,nbcsv,sbcsv

    INTEGER :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

     istatus = 0
  !-----------------------------------------------------------------------
  !
  !  Set the ARPS version number, which will be printed in the log
  !  file in the comment line. The string can be up to 20 character long.
  !
  !-----------------------------------------------------------------------

    arpsversion = 'ARPS 5.4'
  !
  !-----------------------------------------------------------------------
  !
  !  Now we begin to read in the values of parameters:
  !
  !-----------------------------------------------------------------------
  !

  !
  !-----------------------------------------------------------------------
  !
  !  Set up the default values for all the variables to be read in
  !  using the namelist method. In case the user does not specify a
  !  particular value, this value will be used.
  !
  !-----------------------------------------------------------------------
  !
    nproc_x = 1
    nproc_y = 1
    max_fopen = 1
    nprocs  = 1

    nocmnt = 10

    cmnt(1) = ' '
    cmnt(2) = 'A zero perturbation run                    '
    cmnt(3) = ' '
    cmnt(4) = ' '
    cmnt(5) = ' '
    cmnt(6) = ' '
    cmnt(7) = ' '
    cmnt(8) = ' '
    cmnt(9) = ' '
    cmnt(10) =' '

    runname = 'may20'

    runmod = 1
    hxopt = 0
    memid = 1
    hx_interval = 60

    initime = '1977-05-20.21:00:00'
    timeopt = 0
    modelopt = 1
    initopt = 3
    inibasopt  = 1
    inisplited = 0
    viniopt = 1
    pt0opt  = 1

    ubar0   = 0.0
    vbar0   = 0.0

    zshear = 3000.0
    pttrop = 343.0           ! Tropopause pot.temp.
    ttrop  = 213.0           ! Tropopause temp.
    ptground = 300.0         ! Groud urface pot.temp.
    htrop  = 12000.0         ! Tropopause height
    qvmixed= 0.015           ! Mixed layer mixing ratio
    rhmixed= 0.95
    mixtop = 1200.0          ! Mixed layer height

    ptpert0 = 0.0
    pt0radx = 0.0
    pt0rady = 0.0
    pt0radz = 0.0
    pt0ctrx = 0.0
    pt0ctry = 0.0
    pt0ctrz = 0.0

    ptpert0(1) = 4.0
    pt0radx(1) = 10000.0
    pt0rady(1) = 10000.0
    pt0radz(1) =  1500.0
    pt0ctrx(1) = 32000.0
    pt0ctry(1) = 32000.0
    pt0ctrz(1) =  1500.0

    rstinf  = 'may20.rst003600'
    inifmt  = 1
    inifile = 'may20.bin003600'
    inigbf  = 'may20.bingrdbas'
    sndfile = 'may20.snd'

    soilinitopt = 0
    soiltintv   = 0.0
    tsfcopt = 0

    !nudgopt  = 0
    !ndstart  = 0.
    !ndstop   = 0.
    !ndintvl  = 600.
    !ndgain   = 1.9
    !incrfnam = 'nudge.spam'
    !incrfmt  = 1
    !nudgu    = 1
    !nudgv    = 1
    !nudgw    = 1
    !nudgp    = 1
    !nudgpt   = 1
    !nudgqv   = 1
    !nudgqc   = 0
    !nudgqr   = 0
    !nudgqi   = 0
    !nudgqs   = 0
    !nudgqh   = 0

    !dfilter_opt = 0
    !dfsoil_opt = 0
    !df_tstart   = 0
    !df_tinv  = 10
    !df_nstps = 10
    !df_wght  = 1.0/df_nstps

    ternopt = 0
    mntopt =  1
    hmount =  0.0
    mntwidx = 1.0E4
    mntwidy = 1.0E4
    mntctrx = 1.0E4
    mntctry = 1.0E4
    terndta ='arpstern.data'
    ternfmt = 1

    dx = 1000.0
    dy = 1000.0
    dz =  500.0

    strhopt  = 0
    dzmin    = 500.0
    zrefsfc  =   0.0
    dlayer1  =   0.0
    dlayer2  =   1.0E5
    strhtune =   1.0
    zflat    =   1.0E5

    ctrlat  =  35.0
    ctrlon  = -100.0
    crdorgnopt = 0

    mapproj  = 0
    trulat1  =   30.0
    trulat2  =   60.0
    trulon   = -100.0
    sclfct   =    1.0
    mpfctopt = 1
    mptrmopt = 1
    maptest  = 0

    dtbig = 6.0
    tstart= 0.0
    tstop = 3600.0

    vimplct  = 1
    ptsmlstp = 0
    csopt    = 1
    csfactr  =   0.5
    csound   = 150.0
    tacoef   =   0.6
    dtsml    =   1.0

    buoyopt   = 1
    buoy2nd   = 1
    rhofctopt = 1
    bsnesq    = 0
    peqopt    = 1

    tintegopt = 1
    madvopt  = 1
    sadvopt  = 1
    fctorderopt=1
    fctadvptprt=1

    lbcopt = 1
    wbc = 4
    ebc = 4
    sbc = 4
    nbc = 4
    tbc = 1
    fftopt = 2
    bbc = 1
    rbcopt  = 1
    rbc_plbc = 1
    c_phase = 30.0
    rlxlbc  =  0.0
    pdetrnd = 0

    radopt  = 0
    radstgr = 1
    rlwopt  = 1
    radshade = 0
    dtrad   = 600.0
    raddiag = 1

    moist    = 0
    mphyopt  = 0
    nscalar  = 0
    nscalarq = 0
    P_QC = -1; P_QR = -1; P_QI = -1; P_QS = -1; P_QH = -1; P_QG = -1
    P_NC = -1; P_NR = -1; P_NI = -1; P_NS = -1; P_NG = -1; P_NH = -1
               P_ZR = -1; P_ZI = -1; P_ZS = -1; P_ZG = -1; P_ZH = -1;
    P_CC = -1
    qnames(:)= '    '; qdescp(:)= ' '
    nmphystp = 1
    cnvctopt = 0
    subsatopt = 0
    kffbfct  = 0.0
    kfsubsattrig = 0
    ice      = 0
    wcldbs   = 0.005
    confrq   = 600.0
    qpfgfrq  = 120.0
    idownd   = 1
    dsdpref  = 0
    ntcloud  = 1.0e8 ! Default for MY scheme (not used for LIN scheme)
    n0rain   = 8.0e6 ! Default for LIN scheme
    n0snow   = 3.0e6 ! Default for LIN scheme
    n0grpl   = 4.0e5 ! Default for MY scheme (not used for LIN scheme)
    n0hail   = 4.0e4 ! Default for LIN scheme
    rhoice   = 500.0 ! Default for MY scheme (not used for LIN scheme)
    rhosnow  = 100.0 ! Default for LIN and MY scheme
    rhogrpl  = 400.0 ! Default for MY scheme (not used for LIN scheme)
    rhohail  = 913.0 ! Default for LIN scheme

    alpharain = 0.0  ! Default for MY scheme (not used for LIN scheme)
    alphaice = 0.0   ! Default for MY scheme (not used for LIN scheme)
    alphasnow = 0.0  ! Default for MY scheme (not used for LIN scheme)
    alphagrpl = 0.0  ! Default for MY scheme (not used for LIN scheme)
    alphahail = 0.0  ! Default for MY scheme (not used for LIN scheme)

    graupel_ON = 1
    hail_ON = 1      !BJP OCT 2010 hail switch

    mpthermdiag = 0

    impfallopt = 0
    fallopt = 1

    rhsat    = 0.80
    rhsatmin = 0.80
    dx_rhsatmin = 50000.
    dx_rhsat100 = 5000.

    sfcphy   = 0
    landwtr  = 1
    cdhwtropt= 0
    cdmlnd   = 3.0E-3
    cdmwtr   = 1.0E-3
    cdhlnd   = 3.0E-3
    cdhwtr   = 1.0E-3
    cdqlnd   = 2.1E-3
    cdqwtr   = 0.7E-3
    pbldopt  = 0
    pbldpth0 = 1400.0
    lsclpbl0 = 0.15
    sflxdis  = 0
    tqflxdis = 0
    dtqflxdis= 200.0
    smthflx  = 0
    numsmth  = 1
    sfcdiag  = 0

    sfcdat  = 1
    nstyp  = 4
    styp    = 3
    vtyp    = 10
    lai0    = 0.31
    roufns0 = 0.1
    veg0    = 0.0
    sfcdtfl = 'arpssfc.data'
    sfcfmt  = 1

    soilmodel_forced = 0
    sitemeso = '../../arpsdata.dir/mtsnorm.dir/Meso'
    siteflux = '../../arpsdata.dir/mtsnorm.dir/Flux'
    siternet = '../../arpsdata.dir/mtsnorm.dir/Radd'
    sitesoil = '../../arpsdata.dir/mtsnorm.dir/Soil'
    siteveg =  '../../arpsdata.dir/mtsnorm.dir/Veg'

    soilmodel_option = 1
    dzsoil     = 1.00
    zrefsoil   = 0.0
    tsoilint(:) = 273.15
    qsoilint(:) = 0.50

    soilstrhopt = 0
    soildzmin   = 0.025
    soildlayer1 = 0.0
    soildlayer2 = 1.0
    soilstrhtune = 1.0

    soilinit = 1
    ptslnd0  = 300.16
    ptswtr0  = 288.16
    wetcanp0 = 0.0
    snowdpth0 = 0
    soilinfl = 'may20.soilinit'
    soilfmt  = 1

    dtsfc = 10.0

    prtsoilflx = 0

    coriopt = 0
    earth_curvature = 0
    coriotrm= 0

    tmixopt  = 2
    trbisotp = 1
    tkeopt   = 1
    trbvimp  = 0
    tmixvert = 1
    prantl   = 1.0
    tmixcst  = 0.0
    kmlimit  = 0.5

    cmix2nd = 1
    cfcm2h  = 0.0
    cfcm2v  = 1.0E-3
    cmix4th = 1
    cfcm4h  = 1.0E-3
    cfcm4v  = 0.0
    scmixfctr = 1.0
    cmix_opt = 0

    divdmp    = 1
    divdmpndh = 0.05
    divdmpndv = 0.05

    tmaxmin  = 60.0
    tenergy  = 360000.0
    imgopt   = 0
    timgdmp  = 60.0
    pltopt   = 0
    tplots   = 1800.0
    filcmprs = 0
    readyfl = 0

    raydmp = 0
    cfrdmp = 1./300.
    zbrdmp = 10000.0

    flteps = 0.10

    cltkopt = 0
    grdtrns = 0
    umove = 0.0
    vmove = 0.0
    chkdpth = 2500.0
    twindow = 33333
    tceltrk  = 120.0
    tcrestr  = 1800.0

    lvldbg = 0

    hdmpopt  = 1
    dmp_out_joined = 0
    hdmpfmt  = 1
    grbpkbit = 16
    hdfcompr = 0
    thisdmp  = 3600.0
    tstrtdmp = 0.0
    numhdmp  = 1
    DO i=1,numhdmp
      hdmptim(i) = 0.
    END DO
    istager = 0

    dirname  = ' '
    tfmtprt  = 3600.0
    exbcdmp  = 0
    exbchdfcompr = 0
    extdadmp = 0
    grdout   = 0
    basout   = 0
    varout   = 1
    mstout   = 1
    rainout  = 0
    prcout   = 0
    iceout   = 0
    totout   = 1
    tkeout   = 0
    trbout   = 0
    sfcout   = 0
    snowout  = 0
    landout  = 0
    radout   = 0
    flxout   = 0
    refout   = 1

    grdin = 0
    basin = 0
    varin = 0
    mstin = 0
    rainin= 0
    prcin = 0
    icein = 0
    tkein = 0
    trbin = 0
    sfcin = 0
    landin= 0
    totin = 0
    radin = 0
    flxin = 0
    snowcin=0
    snowin= 0

    qcexout = 0
    qrexout = 0
    qiexout = 0
    qsexout = 0
    qhexout = 0
    qgexout = 0
    nqexout = 0
    zqexout = 0

    sfcdmp   = 0
    soildmp  = 0
    terndmp  = 0

    trstout  = 3600.0

    exbcname = 'arpsexbc'
    tinitebd = '1977-05-20.15:00:00'
    tintvebd = 10800
    ngbrz    = 5
    brlxhw   = 2.3
    cbcdmp   = 0.0033333333
    cbcmix   = 0.0                 ! dropped from namelist since ARPS5.2.4
    exbcfmt  = 1

    mgrid = 1
  !
  !-----------------------------------------------------------------------
  !
  !  Initialize message passing processors.
  !
  !-----------------------------------------------------------------------
  !
    ! Non-MPI defaults:
    mp_opt = 0
    myproc = 0
    loc_x  = 1
    loc_y  = 1

    nproc_x_in  = 1
    nproc_y_in  = 1
    nproc_x_out = 1
    nproc_y_out = 1

    readsplit(:) = 0
    joindmp(:)   = 0
  !
  !-----------------------------------------------------------------------
  !
  !      Initialize the processors for an MPI job.
  !
  !-----------------------------------------------------------------------
  !

    CALL mpinit_proc(0)

    IF (myproc == 0)THEN

      WRITE(6,'(/ 16(/5x,a)//)')                                            &
         '###############################################################', &
         '###############################################################', &
         '#####                                                     #####', &
         '#####                      Welcome to                     #####', &
         '#####                                                     #####', &
         '#####  The Variational Data Assimilation System (NEWSVAR) #####', &
         '#####                                                     #####', &
         '#####                     Version 2.0                     #####', &
         '#####                                                     #####', &
         '###############################################################', &
         '###############################################################'

      WRITE(6,'(4(/5x,a),/)')                                               &
          'The model begins by reading a number of control parameters,',    &
          'which are specified in namelist format through the standard',    &
          'input stream (unit 5).  See the sample input file, ',            &
          'news3dvar.input, for guidance on specifying these parameters.'

      unum = 0
      !CALL getarg(1,nlfile)
      CALL GET_COMMAND_ARGUMENT(1, nlfile, lenstr, istatus )
      !print*,'nlfile==', nlfile
      IF ( nlfile(1:1) == ' ' .OR. istatus /= 0 ) THEN  ! Use standard input to be backward-compatible
          unum = 5
      ELSE
          INQUIRE(FILE=nlfile,EXIST=iexist)
          IF (.NOT. iexist) THEN
            WRITE(6,'(1x,3a)') 'WARNING: namelist file - ',               &
                  TRIM(nlfile),' does not exist. Falling back to standard input.'
            unum = 5
          END IF
      END IF

      IF (unum /= 5) THEN
        namelist_filename = nlfile
        CALL getunit( unum )
        OPEN(unum,FILE=TRIM(nlfile),STATUS='OLD',FORM='FORMATTED')
        WRITE(6,'(1x,3a,/,1x,a,/)') 'Reading NEWSVAR namelist from file - ',   &
                TRIM(nlfile),' ... ','========================================'
      ELSE
        WRITE(6,'(2(1x,a,/))') 'Waiting namelist from standard input ... ', &
                               '========================================'
      END IF

    END IF
  !
  !-----------------------------------------------------------------------
  !
  !  Read in message passing options.
  !
  !-----------------------------------------------------------------------
  !
    IF (myproc == 0) READ (unum,message_passing, END=100)

    IF (myproc == 0) &
      WRITE(6,'(a)')'Namelist block message_passing sucessfully read.'

    CALL mpupdatei(nproc_x,1)
    CALL mpupdatei(nproc_y,1)
    CALL mpupdatei(max_fopen,1)
    CALL mpupdatei(nproc_x_in,1)
    CALL mpupdatei(nproc_y_in,1)

    IF (myproc == 0)THEN
      WRITE(6,'(5x,a,i4)')                                                &
           "Number of processors in the x-direction is:",nproc_x
      WRITE(6,'(5x,a,i4)')                                                &
          "Number of processors in the y-direction is:",nproc_y
      WRITE(6,'(5x,a,i4)')                                                &
          "Maximum number of files open:",max_fopen
    END IF

  !
  !-----------------------------------------------------------------------
  !
  !  Initialize message passing variables.
  !
  !-----------------------------------------------------------------------
  !
    CALL mpinit_var

  !-----------------------------------------------------------------------
  !
  !  Read in some comment lines on this job and the name of
  !  this run designated by a string at least 6 character long.
  !
  !-----------------------------------------------------------------------
  !
    IF (myproc == 0) READ (unum,comment_lines, END=100)
    IF (myproc == 0) &
      WRITE(6,'(a)')'Namelist block comment_lines sucessfully read.'
    CALL mpupdatei(nocmnt,1)
    CALL mpupdatec(cmnt,80*nocmnt)

    IF (myproc == 0) READ (unum,jobname,END=100)
    IF (myproc == 0) &
      WRITE(6,'(a)')'Namelist block jobname sucessfully read.'
    CALL mpupdatec(runname,80)

    IF (myproc == 0) &
      WRITE(6,'(/5x,a,a)') 'The name of this run is: ', runname
  !
  !-----------------------------------------------------------------------
  !
  !  Find out the number of characters to be used to construct file
  !  names.
  !
  !-----------------------------------------------------------------------
  !
    CALL gtlfnkey( runname, lfnkey )
  !
  !-----------------------------------------------------------------------
  !
  !  Read in control parameter INITOPT for model initialization
  !
  !  INITOPT = 1, Self initialization (e.g. specify perturbation using
  !                 analytical functions),
  !            = 2, Restart run, initialize the model using previous
  !                 model output,
  !            = 3, Initialize the model using external input data file.
  !            = 4, Read in restart file then overwrite current time level
  !                 with those in history file.
  !
  !  For options 2-4, the names of input files need to be provided.
  !
  !-----------------------------------------------------------------------
  !
    IF (myproc == 0) THEN
      READ (unum,initialization,END=100)
      WRITE(6,'(a)')'Namelist block initialization sucessfully read.'
    END IF

    DO i = FINDX_NUM, 1, -1
      idummy = inisplited / 10**(i-1)     ! ith digit is odd (1,3,5..), no readsplit
      readsplit(i) = MOD(idummy+1, 2)     ! ith digit is even(0,2,4..), readsplit
    END DO

    CALL mpupdatec(initime,19)
    CALL mpupdatei(initopt,1)
    CALL mpupdatei(inifmt,1)
    CALL mpupdatei(readsplit,FINDX_NUM)
    CALL mpupdatec(inifile,256)
    CALL mpupdatec(inigbf, 256)
    CALL mpupdatei(modelopt,1)

    READ (initime, '(i4.4,1x,i2.2,1x,i2.2,1x,i2.2,1x,i2.2,1x,i2.2)' )     &
         year,month,day,hour,minute,second

    IF (myproc == 0)THEN
      WRITE(6,'(a,a,i4.4,a,i2.2,a,i2.2,a,i2.2,a,i2.2,a,i2.2)')            &
          '     The initial local time for this run is ',                 &
          '     year-mo-dy:hr:mn:ss = ',                                  &
          year,'-',month,'-',day,'.',hour,':',minute,':',second

      WRITE(6,'(5x,a,i4/)') 'Perturbation option was ', pt0opt

    END IF
  !
  !-----------------------------------------------------------------------
  !
  !  Input data files for initialization:
  !
  !-----------------------------------------------------------------------
  !
    lenstr = 256
    CALL strlnth( inifile, lenstr)
    IF (myproc == 0)THEN
      WRITE(6,'(5x,a,a)')                                                 &
        'The background history file to be read is ',inifile(1:lenstr)
    END IF

    lenstr = 256
    CALL strlnth( inigbf, lenstr)
    IF (myproc == 0)THEN
      WRITE(6,'(5x,a,a,a)')                                               &
          'The base state/grid history to be read is ', inigbf(1:lenstr)
    END IF

  !
  !-----------------------------------------------------------------------
  !
  !  Input horizontal grid size
  !
  !-----------------------------------------------------------------------
  !
    IF (myproc == 0) THEN
      READ (unum,grid,END=100)
      WRITE(6,'(a)')'Namelist block grid sucessfully read.'
    END IF

    CALL mpupdater(dz,1)
    CALL mpupdatei(strhopt,1)
    CALL mpupdater(dzmin,1)
    CALL mpupdater(zrefsfc,1)
    CALL mpupdater(dlayer1,1)
    CALL mpupdater(dlayer2,1)
    CALL mpupdater(strhtune,1)
    CALL mpupdater(zflat,1)

    IF( strhopt == 0.AND.dzmin /= dz ) THEN
      dzmin = dz
      IF (myproc == 0)  WRITE(6,'(5x,a)')                               &
           'For non-stretched case, dzmin was reset to dz.'
    END IF

    IF (myproc == 0)THEN

      WRITE(6,'(5x,a,i4)') 'The stretch option was ', strhopt

      WRITE(6,'(5x,a,f10.3,a)')                                         &
         'Input dz was ',dz,' meters'

      WRITE(6,'(5x,a,f10.3,a)')                                         &
         'dzmin is ',dzmin,' meters'

      WRITE(6,'(5x,a,f10.3,a)')                                         &
         'zrefsfc is ',zrefsfc ,' meters'

      WRITE(6,'(5x,a,f10.3,a)')                                         &
         'dlayer1 is ',dlayer1 ,' meters'

      WRITE(6,'(5x,a,f10.3,a)')                                         &
         'dlayer2 is ',dlayer2 ,' meters'

      WRITE(6,'(5x,a,f10.3,a)')                                         &
         'zflat   is ',zflat   ,' meters'
    END IF

  !
  !-----------------------------------------------------------------------
  !
  !  Initialize hydrometeor indices
  !
  !-----------------------------------------------------------------------
  !

    !IF ( mphyopt < 0 .OR. mphyopt > 12 ) THEN
    !  IF (myproc == 0)THEN
    !    WRITE (6,'(5x,a/5x,a/5x,a)')                                      &
    !      'No option for mphyopt > 11.',                                  &
    !      'Program will try to complete reading in input parameters, ',   &
    !      'but will stop at the end of subroutine INITPARA.'
    !  END IF
    !  err_no = err_no + 1
    !ELSE IF ( mphyopt == 2 .OR. mphyopt == 3 .OR. mphyopt == 4 ) THEN
    !  nscalar = 5; nscalarq =  nscalar
    !  P_QC = 1; P_QR = 2; P_QI = 3; P_QS = 4; P_QH = 5
    !  qnames(P_QC) = 'qc'; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
    !  qnames(P_QR) = 'qr'; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
    !  qnames(P_QI) = 'qi'; qdescp(P_QI) = 'Cloud ice   mixing ratio (kg/kg)'
    !  qnames(P_QS) = 'qs'; qdescp(P_QS) = 'Snow mixing ratio (kg/kg)'
    !  qnames(P_QH) = 'qh'; qdescp(P_QH) = 'Hail mixing ratio (kg/kg)'
    !  ice = 1
    !ELSE IF ( mphyopt == 5 .OR. mphyopt == 6 .OR. mphyopt == 7 ) THEN
    !
    !  nscalar = 5; nscalarq =  nscalar
    !  P_QC = 1; P_QR = 2; P_QI = 3; P_QS = 4; P_QG = 5
    !  qnames(P_QC) = 'qc'; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
    !  qnames(P_QR) = 'qr'; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
    !  qnames(P_QI) = 'qi'; qdescp(P_QI) = 'Cloud ice   mixing ratio (kg/kg)'
    !  qnames(P_QS) = 'qs'; qdescp(P_QS) = 'Snow mixing ratio (kg/kg)'
    !  qnames(P_QG) = 'qg'; qdescp(P_QG) = 'Graupel mixing ratio (kg/kg)'
    !  ice = 1
    !
    !ELSE IF ( mphyopt == 8 ) THEN
    !  nscalar = 6; nscalarq = 6
    !  P_QC = 1; P_QR = 2; P_QI = 3; P_QS = 4; P_QG = 5; P_QH = 6;
    !  qnames(P_QC) = 'qc'; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
    !  qnames(P_QR) = 'qr'; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
    !  qnames(P_QI) = 'qi'; qdescp(P_QI) = 'Cloud ice   mixing ratio (kg/kg)'
    !  qnames(P_QS) = 'qs'; qdescp(P_QS) = 'Snow mixing ratio (kg/kg)'
    !  qnames(P_QG) = 'qg'; qdescp(P_QG) = 'Graupel mixing ratio (kg/kg)'
    !  qnames(P_QH) = 'qh'; qdescp(P_QH) = 'Hail mixing ratio (kg/kg)'
    !  ice = 1
    !ELSE IF ( mphyopt == 9 .OR. mphyopt == 10 .OR. mphyopt == 12) THEN
    !  nscalar = 12; nscalarq = 6
    !  P_QC = 1; P_QR = 2; P_QI = 3; P_QS =  4; P_QG =  5; P_QH =  6;
    !  P_NC = 7; P_NR = 8; P_NI = 9; P_NS = 10; P_NG = 11; P_NH = 12;
    !  qnames(P_QC) = 'qc'; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
    !  qnames(P_QR) = 'qr'; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
    !  qnames(P_QI) = 'qi'; qdescp(P_QI) = 'Cloud ice   mixing ratio (kg/kg)'
    !  qnames(P_QS) = 'qs'; qdescp(P_QS) = 'Snow mixing ratio (kg/kg)'
    !  qnames(P_QG) = 'qg'; qdescp(P_QG) = 'Graupel mixing ratio (kg/kg)'
    !  qnames(P_QH) = 'qh'; qdescp(P_QH) = 'Hail mixing ratio (kg/kg)'
    !  qnames(P_NC) = 'nc'; qdescp(P_NC) = 'Cloud water concentrations (#/m3)'
    !  qnames(P_NR) = 'nr'; qdescp(P_NR) = 'Rain water concentrations (#/m3)'
    !  qnames(P_NI) = 'ni'; qdescp(P_NI) = 'Cloud ice concentrations (#/m3)'
    !  qnames(P_NS) = 'ns'; qdescp(P_NS) = 'Snow concentrations (#/m3)'
    !  qnames(P_NG) = 'ng'; qdescp(P_NG) = 'Graupel concentrations (#/m3)'
    !  qnames(P_NH) = 'nh'; qdescp(P_NH) = 'Hail concentrations (#/m3)'
    !  ice = 1
    !ELSE IF ( mphyopt == 11) THEN
    !  nscalar = 17; nscalarq = 6
    !  P_QC = 1; P_QR =  2; P_QI =  3; P_QS =  4; P_QG =  5; P_QH =  6;
    !  P_NC = 7; P_NR =  8; P_NI =  9; P_NS = 10; P_NG = 11; P_NH = 12;
    !            P_ZR = 13; P_ZI = 14; P_ZS = 15; P_ZG = 16; P_ZH = 17;
    !  qnames(P_QC) = 'qc  '; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
    !  qnames(P_QR) = 'qr  '; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
    !  qnames(P_QI) = 'qi  '; qdescp(P_QI) = 'Cloud ice   mixing ratio (kg/kg)'
    !  qnames(P_QS) = 'qs  '; qdescp(P_QS) = 'Snow mixing ratio (kg/kg)'
    !  qnames(P_QG) = 'qg  '; qdescp(P_QG) = 'Graupel mixing ratio (kg/kg)'
    !  qnames(P_QH) = 'qh  '; qdescp(P_QH) = 'Hail mixing ratio (kg/kg)'
    !  qnames(P_NC) = 'nc  '; qdescp(P_NC) = 'Cloud water concentrations (#/kg)'
    !  qnames(P_NR) = 'nr  '; qdescp(P_NR) = 'Reain water concentrations (#/kg)'
    !  qnames(P_NI) = 'ni  '; qdescp(P_NI) = 'Cloud ice concentrations (#/kg)'
    !  qnames(P_NS) = 'ns  '; qdescp(P_NS) = 'Snow concentrations (#/kg)'
    !  qnames(P_NG) = 'ng  '; qdescp(P_NG) = 'Graupel concentrations (#/kg)'
    !  qnames(P_NH) = 'nh  '; qdescp(P_NH) = 'Hail concentrations (#/kg)'
    !  qnames(P_ZR) = 'zr  '; qdescp(P_ZR) = 'Rain reflectivity (m6/kg)'
    !  qnames(P_ZI) = 'zi  '; qdescp(P_ZI) = 'Ice reflectivity (m6/kg)'
    !  qnames(P_ZS) = 'zs  '; qdescp(P_ZS) = 'Snow reflectivity (m6/kg)'
    !  qnames(P_ZG) = 'zg  '; qdescp(P_ZG) = 'Graupel reflectivity (m6/kg)'
    !  qnames(P_ZH) = 'zh  '; qdescp(P_ZH) = 'Hail reflectivity (m6/kg)'
    !  ice = 1
    !ELSE
    !  nscalar = 2; nscalarq = 2
    !  P_QC = 1; P_QR = 2
    !  qnames(P_QC) = 'qc'; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
    !  qnames(P_QR) = 'qr'; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
    !  ice = 0
    !END IF
    !
    !IF (kfsubsattrig < 0 .OR. kfsubsattrig > 1) THEN
    !  WRITE (6,'(5x,a/5x,a/5x,a)')                                        &
    !    'ERROR: No option for kfsubsattrig < 0 or > 1. ',                 &
    !    'Program will try to complete reading in input parameters, ',     &
    !    'but will stop at the end of subroutine INITPARA.'
    !  err_no = err_no + 1
    !END IF
    !
  !
  !-----------------------------------------------------------------------
  !
  !  Input soil and vegetation parameters
  !
  !-----------------------------------------------------------------------
  !
    IF (myproc == 0) THEN
      READ (unum,soil_ebm,END=100)
      WRITE(6,'(a)')'Namelist block soil_ebm sucessfully read.'
    END IF

    CALL mpupdatei(soilmodel_option,1)
    CALL mpupdater(dzsoil,1)
    CALL mpupdater(zrefsoil,1)
    CALL mpupdater(soildzmin,1)
    CALL mpupdater(soildlayer1,1)
    CALL mpupdater(soildlayer2,1)
    CALL mpupdater(soilstrhtune,1)
    CALL mpupdatei(soilstrhopt,1)

    IF( soilstrhopt == 0 .AND. soildzmin /= dzsoil ) THEN
      IF (myproc == 0)THEN
        WRITE(6,'(5x,a)')                                                   &
           'For non-stretched case, dzmin was reset to dz.'
      END IF
      soildzmin = dzsoil
    END IF

    IF (myproc == 0)THEN

      WRITE(6,'(5x,a,i4)') 'The stretch option was ', soilstrhopt

      WRITE(6,'(5x,a,f10.3,a)')                                             &
           'Input dzsoil was ',dzsoil,' meters'

      WRITE(6,'(5x,a,f10.3,a)')                                             &
           'Input zrefsoil was ',zrefsoil,' meters'

      WRITE(6,'(5x,a,f10.3,a)')                                             &
           'soildzmin is ',soildzmin,' meters'

      WRITE(6,'(5x,a,f10.3,a)')                                             &
           'soildlayer1 is ',soildlayer1 ,' meters'

      WRITE(6,'(5x,a,f10.3,a)')                                             &
           'soildlayer2 is ',soildlayer2 ,' meters'
    END IF

    IF (myproc == 0)THEN

      WRITE (6,'(5x,a,i4)')                                                 &
           'The soil scheme input option was ',soilmodel_option

    END IF

    IF (soilmodel_option == 1) THEN
      dzsoil = 1.0
      IF (myproc == 0)  WRITE(6,'(5x,a)')                               &
          'Since soilmodel_option = 1, nzsoil is set to 2 and dzsoil is set to 1.0'
    END IF

    IF (myproc == 0)THEN

      WRITE (6,'(5x,a,i4)') 'The soil scheme is ',soilmodel_option

      WRITE (6,'(5x,a,f10.3)') 'The average soil layer depth is ',dzsoil

      WRITE (6,'(5x,a,f10.3)') 'The reference height of the soil depth is ',zrefsoil

    END IF

  !
  !-----------------------------------------------------------------------
  !
  !  Read in namelist &history_dump
  !
  !-----------------------------------------------------------------------
  !
    IF (myproc == 0) THEN
      READ (unum,history_dump,END=100)
      WRITE(6,'(a)')'Namelist block history_dump sucessfully read.'
    END IF

    DO i = FINDX_NUM,1,-1
      idummy = dmp_out_joined/10**(i-1)
      joindmp(i) = MOD(idummy,2)
    END DO

    CALL mpupdatei(hdmpfmt,1)
    CALL mpupdatei(joindmp,FINDX_NUM)
    CALL mpupdatei(hdfcompr,1)

    splitdmp = 0
    IF (modelopt == 1) THEN
      splitdmp = hdmpfmt/100
      hdmpfmt  = MOD(hdmpfmt,100)
      IF (hdmpfmt /= 3 .AND. hdmpfmt /= 7) splitdmp = 0

      IF( hdmpfmt < 0 .OR. hdmpfmt > 11) THEN
        IF (myproc == 0) WRITE(6,'(5x,a,i4,a,2(/5x,a))')                  &
            'The option hdmpfmt=', hdmpfmt, ' not valid.',                &
            'Program will try to complete reading in input parameters, ', &
            'but will stop at the end of subroutine INITPARA.'
        err_no = err_no + 1
      END IF
    END IF

    IF (myproc == 0)THEN
      WRITE(6,'(5x,a,i4)') 'The history dump option was ',hdmpopt
      WRITE(6,'(5x,a,i4)') 'The history dump format was ',hdmpfmt
    END IF


    IF (myproc == 0)THEN
      WRITE(6,'(5x,a,i4)')                                             &
           'HDF4 compression option was ',hdfcompr
    END IF

  !
  !-----------------------------------------------------------------------
  !
  !  Read in namelist &output
  !
  !-----------------------------------------------------------------------
  !
    IF (myproc == 0) THEN
      READ (unum,output,END=100)
      WRITE(6,'(a)')'Namelist block output sucessfully read.'
    END IF
    CALL mpupdatec(dirname,256)
    CALL mpupdatei(readyfl,1)
    CALL mpupdatei(refout,1)
    CALL mpupdatei(grdout,1)
    CALL mpupdatei(basout,1)
    CALL mpupdatei(varout,1)
    CALL mpupdatei(mstout,1)
    CALL mpupdatei(rainout,1)
    CALL mpupdatei(prcout,1)
    CALL mpupdatei(iceout,1)
    CALL mpupdatei(tkeout,1)
    CALL mpupdatei(trbout,1)
    CALL mpupdatei(sfcout,1)
    CALL mpupdatei(landout,1)
    CALL mpupdatei(totout,1)
    CALL mpupdatei(radout,1)
    CALL mpupdatei(flxout,1)

    grdbasfout = MOD ( basout/100+1, 2 )
    basout = MOD(basout,100)
    IF (myproc == 0) WRITE(6,'(5x,a,I10,a)') 'Grid and Base file output is ',grdbasfout,'.'
    IF (myproc == 0) WRITE(6,'(5x,a,f10.3,a)')                       &
        'Formatted printout time interval was ',tfmtprt,' seconds.'
  !
  !-----------------------------------------------------------------------
  !
  !  Input model output parameters:
  !
  !  First, give the name of the directory into which output files
  !  will be written:
  !
  !-----------------------------------------------------------------------
  !
    ldirnam = LEN_TRIM(dirname)

    IF( ldirnam == 0 ) THEN
      dirname = './'
      ldirnam=2

      IF (myproc == 0) WRITE(6,'(5x,a)')                                  &
          'Output files will be in the current work directory.'
    END IF
  !
  !-----------------------------------------------------------------------
  !
  !  Set the control parameters for the output of selected fields.
  !
  !-----------------------------------------------------------------------
  !
    IF (myproc == 0)THEN
      WRITE(6,'(5x,a,i4)')                                             &
           'The input grid coordinate dump option was ', grdout

      WRITE(6,'(5x,a,i4)')                                             &
          'The input base state array dump option was ', basout

      WRITE(6,'(5x,a,i4)')                                             &
          'The input mass-velocity array dump option was ', varout

      WRITE(6,'(5x,a,i4)')                                             &
          'The input non-ice water array dump option was ',mstout

      WRITE(6,'(5x,a,i4)')                                             &
          'The input rain array dump option was ', rainout
    END IF
    rainout = rainout * mstout

    IF (myproc == 0)  WRITE(6,'(5x,a,i4)')                             &
        'The input precipitation rates array dump option was ',prcout
    prcout = prcout * mstout

    IF (ice == 1) THEN
      IF (iceout == 0) THEN
        IF (myproc == 0) WRITE(6,'(1x,a,I4,a,/10x,a)')                 &
        'WARNING: Since option mphyopt = ',mphyopt,                    &
        ', ice array dump should be truned on.',                       &
        'iceout is reset to 1.'
      END IF
      iceout = 1
    END IF

    IF (myproc == 0)THEN
      WRITE(6,'(5x,a,i4)')                                             &
          'The input ice array dump option was ', iceout

      WRITE(6,'(5x,a,i4)')                                             &
          'The input TKE dump option was ', tkeout

      WRITE(6,'(5x,a,i4)')                                             &
          'The input eddy mixing coeff dump option was ', trbout

      WRITE(6,'(5x,a,i4)')                                             &
          'The soil variable dump option was ', sfcout

      WRITE(6,'(5x,a,i4)')                                             &
          'The surface property array dump option was ', landout

      WRITE(6,'(5x,a,i4)')                                             &
          'The radiation arrays dump option was ', radout

      WRITE(6,'(5x,a,i4)')                                             &
          'The surface fluxes dump option was ', flxout

    END IF
  !
  !-----------------------------------------------------------------------
  !
  !  Input debug information print controls:
  !
  !-----------------------------------------------------------------------
  !
    IF (myproc == 0) THEN
      READ (unum,debug)
      WRITE(6,'(a)')'Namelist block debug sucessfully read.'
    END IF
    CALL mpupdatei(lvldbg,1)

    IF (myproc == 0) WRITE(6,'(5x,a,i4)') 'The debug printing level was ', lvldbg

    GO TO 102
  !
  !-----------------------------------------------------------------------
  !
  !  Print out the input parameters.
  !  Write out a log file of model parameters which can be used as
  !  the input file to re-run the model.
  !
  !-----------------------------------------------------------------------
  !

    100  CONTINUE
    IF (myproc == 0) CALL wrtcomment('Error reading NAMELIST file. Default values used',1)
    CALL arpsstop('ERROR: please check namelist file.',1)

    102  CONTINUE

    IF (unum /= 5 .AND. myproc == 0) THEN
      CLOSE( unum )
      CALL retunit( unum )
    END IF

    IF(myproc ==0) WRITE(6,'(1x,a)') '======================================='

  !-----------------------------------------------------------------------
  !
  ! Hardcode some parameters
  ! (they has been removed from the namelist)
  !
  !----------------------------------------------------------------------

    fallvalpha = 0.5
    alfcoef    = 0.5
    IF ( trbvimp == 0 ) alfcoef = 1.0
  !
  !-----------------------------------------------------------------------
  !
  !  Adjust max_fopen if readsplit > 0 and/or joindmp > 0
  !
  !-----------------------------------------------------------------------

    IF (mp_opt > 0 ) THEN

      readstride = max_fopen
      IF (ANY(readsplit > 0) ) readstride = nprocs

      dumpstride = max_fopen
      IF (ANY(joindmp > 0) )   dumpstride = nprocs

      IF (nproc_x_out > nproc_x .OR. nproc_y_out > nproc_y) THEN

        IF (splitdmp  > 0) joindmp(FINDX_H) = 0    ! We should not join history file
        IF (splitexbc > 0) joindmp(FINDX_B) = 0    ! boundary file
        IF (splitsoil > 0) joindmp(FINDX_S) = 0    ! soil file even
      ELSE
        !WRITE(6,'(/,1x,3a,/)') 'WARNING: ',                               &
        !        'Output multiple patches is only supported for HDF4 outputs', &
        !        'nproc_x_out/nproc_y_out was reset.'
        !IF (joindmp > 0) THEN
        !  nproc_x_out = 1
        !  nproc_y_out = 1
        !ELSE
        !  nproc_x_out = nproc_x
        !  nproc_y_out = nproc_y
        !END IF
      END IF

      !IF (readsplit > 0) THEN
      !  nproc_x_in = 1
      !  nproc_y_in = 1
      !ELSE
      !  nproc_x_in = nproc_x
      !  nproc_y_in = nproc_y
      !END IF

    ELSE
      readsplit(:)  = 0
      joindmp(:)    = 0
      readstride = max_fopen
      dumpstride = max_fopen
      nproc_x_out = 1
      nproc_y_out = 1
    END IF
  !
  !-----------------------------------------------------------------------
  !
  !  Compute derived variables.
  !
  !-----------------------------------------------------------------------
  !
    ebc_global = ebc
    wbc_global = wbc
    nbc_global = nbc
    sbc_global = sbc

    IF (mp_opt > 0) THEN  ! Convert from global to processor specific values.
      IF (loc_x /= 1) wbc = 0
      IF (loc_x /= nproc_x) ebc = 0
      IF (loc_y /= 1) sbc = 0
      IF (loc_y /= nproc_y) nbc = 0
    END IF

    IF (lbcopt == 2) THEN
      IF (myproc == 0) WRITE (*,*) "INIT3DVAR: resetting lbcopt to 1 & lateral bc's to 4"
      lbcopt = 1
      ebc = 4
      wbc = 4
      nbc = 4
      sbc = 4
    END IF

    IF( err_no /= 0 ) THEN
      IF (myproc == 0) WRITE(6,'(5x,i4,a,/5x,a,/5x,a,a)')              &
          err_no, ' fatal errors found with the input parameters.',    &
          'Please check the ARPS input parameters carefully.',         &
          'The values of parameters you have used can be found',       &
          ' in the log file.'
      CALL arpsstop('arpsstop called from INIT_MODEL_PARA with an option',1)
    END IF

    RETURN
  END SUBROUTINE init_model_para

  !#######################################################################

  SUBROUTINE init_scalar_array(istatus)
    IMPLICIT NONE
    INTEGER, INTENT(OUT) :: istatus

    INCLUDE 'globcst.inc'

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    nscalar  = 0
    nscalarq = 0
    P_QC = -1; P_QR = -1; P_QI = -1; P_QS = -1; P_QH = -1; P_QG = -1
    P_NC = -1; P_NR = -1; P_NI = -1; P_NS = -1; P_NG = -1; P_NH = -1
               P_ZR = -1; P_ZI = -1; P_ZS = -1; P_ZG = -1; P_ZH = -1;
    P_CC = -1
    qnames(:)= '    '; qdescp(:)= ' '

    IF (modelopt == 1) THEN

      IF ( mphyopt < 0 .OR. mphyopt > 12 ) THEN
        WRITE (6,'(5x,a/5x,a/5x,a)')                                      &
          'No option for mphyopt > 11.',                                  &
          'Program will try to complete reading in input parameters, ',   &
          'but will stop at the end of subroutine INITPARA.'
      ELSE IF ( mphyopt == 2 .OR. mphyopt == 3 .OR. mphyopt == 4 ) THEN
        nscalar = 5; nscalarq =  nscalar
        P_QC = 1; P_QR = 2; P_QI = 3; P_QS = 4; P_QH = 5
        qnames(P_QC) = 'qc'; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
        qnames(P_QR) = 'qr'; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
        qnames(P_QI) = 'qi'; qdescp(P_QI) = 'Cloud ice   mixing ratio (kg/kg)'
        qnames(P_QS) = 'qs'; qdescp(P_QS) = 'Snow mixing ratio (kg/kg)'
        qnames(P_QH) = 'qh'; qdescp(P_QH) = 'Hail mixing ratio (kg/kg)'
        ice = 1
      ELSE IF ( mphyopt == 5 .OR. mphyopt == 6 .OR. mphyopt == 7 ) THEN

        nscalar = 5; nscalarq =  nscalar
        P_QC = 1; P_QR = 2; P_QI = 3; P_QS = 4; P_QG = 5
        qnames(P_QC) = 'qc'; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
        qnames(P_QR) = 'qr'; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
        qnames(P_QI) = 'qi'; qdescp(P_QI) = 'Cloud ice   mixing ratio (kg/kg)'
        qnames(P_QS) = 'qs'; qdescp(P_QS) = 'Snow mixing ratio (kg/kg)'
        qnames(P_QG) = 'qg'; qdescp(P_QG) = 'Graupel mixing ratio (kg/kg)'
        ice = 1

      ELSE IF ( mphyopt == 8 ) THEN
        nscalar = 6; nscalarq = 6
        P_QC = 1; P_QR = 2; P_QI = 3; P_QS = 4; P_QG = 5; P_QH = 6;
        qnames(P_QC) = 'qc'; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
        qnames(P_QR) = 'qr'; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
        qnames(P_QI) = 'qi'; qdescp(P_QI) = 'Cloud ice   mixing ratio (kg/kg)'
        qnames(P_QS) = 'qs'; qdescp(P_QS) = 'Snow mixing ratio (kg/kg)'
        qnames(P_QG) = 'qg'; qdescp(P_QG) = 'Graupel mixing ratio (kg/kg)'
        qnames(P_QH) = 'qh'; qdescp(P_QH) = 'Hail mixing ratio (kg/kg)'
        ice = 1
      ELSE IF ( mphyopt == 9 .OR. mphyopt == 10 .OR. mphyopt == 12) THEN
        nscalar = 12; nscalarq = 6
        P_QC = 1; P_QR = 2; P_QI = 3; P_QS =  4; P_QG =  5; P_QH =  6;
        P_NC = 7; P_NR = 8; P_NI = 9; P_NS = 10; P_NG = 11; P_NH = 12;
        qnames(P_QC) = 'qc'; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
        qnames(P_QR) = 'qr'; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
        qnames(P_QI) = 'qi'; qdescp(P_QI) = 'Cloud ice   mixing ratio (kg/kg)'
        qnames(P_QS) = 'qs'; qdescp(P_QS) = 'Snow mixing ratio (kg/kg)'
        qnames(P_QG) = 'qg'; qdescp(P_QG) = 'Graupel mixing ratio (kg/kg)'
        qnames(P_QH) = 'qh'; qdescp(P_QH) = 'Hail mixing ratio (kg/kg)'
        qnames(P_NC) = 'nc'; qdescp(P_NC) = 'Cloud water concentrations (#/m3)'
        qnames(P_NR) = 'nr'; qdescp(P_NR) = 'Rain water concentrations (#/m3)'
        qnames(P_NI) = 'ni'; qdescp(P_NI) = 'Cloud ice concentrations (#/m3)'
        qnames(P_NS) = 'ns'; qdescp(P_NS) = 'Snow concentrations (#/m3)'
        qnames(P_NG) = 'ng'; qdescp(P_NG) = 'Graupel concentrations (#/m3)'
        qnames(P_NH) = 'nh'; qdescp(P_NH) = 'Hail concentrations (#/m3)'
        ice = 1
      ELSE IF ( mphyopt == 11) THEN
        nscalar = 17; nscalarq = 6
        P_QC = 1; P_QR =  2; P_QI =  3; P_QS =  4; P_QG =  5; P_QH =  6;
        P_NC = 7; P_NR =  8; P_NI =  9; P_NS = 10; P_NG = 11; P_NH = 12;
                  P_ZR = 13; P_ZI = 14; P_ZS = 15; P_ZG = 16; P_ZH = 17;
        qnames(P_QC) = 'qc  '; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
        qnames(P_QR) = 'qr  '; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
        qnames(P_QI) = 'qi  '; qdescp(P_QI) = 'Cloud ice   mixing ratio (kg/kg)'
        qnames(P_QS) = 'qs  '; qdescp(P_QS) = 'Snow mixing ratio (kg/kg)'
        qnames(P_QG) = 'qg  '; qdescp(P_QG) = 'Graupel mixing ratio (kg/kg)'
        qnames(P_QH) = 'qh  '; qdescp(P_QH) = 'Hail mixing ratio (kg/kg)'
        qnames(P_NC) = 'nc  '; qdescp(P_NC) = 'Cloud water concentrations (#/kg)'
        qnames(P_NR) = 'nr  '; qdescp(P_NR) = 'Reain water concentrations (#/kg)'
        qnames(P_NI) = 'ni  '; qdescp(P_NI) = 'Cloud ice concentrations (#/kg)'
        qnames(P_NS) = 'ns  '; qdescp(P_NS) = 'Snow concentrations (#/kg)'
        qnames(P_NG) = 'ng  '; qdescp(P_NG) = 'Graupel concentrations (#/kg)'
        qnames(P_NH) = 'nh  '; qdescp(P_NH) = 'Hail concentrations (#/kg)'
        qnames(P_ZR) = 'zr  '; qdescp(P_ZR) = 'Rain reflectivity (m6/kg)'
        qnames(P_ZI) = 'zi  '; qdescp(P_ZI) = 'Ice reflectivity (m6/kg)'
        qnames(P_ZS) = 'zs  '; qdescp(P_ZS) = 'Snow reflectivity (m6/kg)'
        qnames(P_ZG) = 'zg  '; qdescp(P_ZG) = 'Graupel reflectivity (m6/kg)'
        qnames(P_ZH) = 'zh  '; qdescp(P_ZH) = 'Hail reflectivity (m6/kg)'
        ice = 1
      ELSE
        !nscalar = 2; nscalarq = 2
        !P_QC = 1; P_QR = 2
        !qnames(P_QC) = 'qc'; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
        !qnames(P_QR) = 'qr'; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
        !ice = 0
        nscalar = 5; nscalarq =  nscalar
        P_QC = 1; P_QR = 2; P_QI = 3; P_QS = 4; P_QH = 5
        qnames(P_QC) = 'qc'; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
        qnames(P_QR) = 'qr'; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
        qnames(P_QI) = 'qi'; qdescp(P_QI) = 'Cloud ice   mixing ratio (kg/kg)'
        qnames(P_QS) = 'qs'; qdescp(P_QS) = 'Snow mixing ratio (kg/kg)'
        qnames(P_QH) = 'qh'; qdescp(P_QH) = 'Hail mixing ratio (kg/kg)'
        ice = 1

      END IF

    ELSE IF (modelopt == 2) THEN

      SELECT CASE (mphyopt)
      CASE (2,6,7,8,10,16)
        nscalar = 5; nscalarq =  nscalar
        P_QC = 1; P_QR = 2; P_QI = 3; P_QS = 4; P_QG = 5
        qnames(P_QC) = 'qc'; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
        qnames(P_QR) = 'qr'; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
        qnames(P_QI) = 'qi'; qdescp(P_QI) = 'Cloud ice   mixing ratio (kg/kg)'
        qnames(P_QS) = 'qs'; qdescp(P_QS) = 'Snow mixing ratio (kg/kg)'
        qnames(P_QG) = 'qg'; qdescp(P_QG) = 'Graupel mixing ratio (kg/kg)'
        ice = 1
      CASE (9,17,18)
        nscalar = 6; nscalarq = 6
        P_QC = 1; P_QR = 2; P_QI = 3; P_QS = 4; P_QG = 5; P_QH = 6;
        qnames(P_QC) = 'qc'; qdescp(P_QC) = 'Cloud water mixing ratio (kg/kg)'
        qnames(P_QR) = 'qr'; qdescp(P_QR) = 'Rain  water mixing ratio (kg/kg)'
        qnames(P_QI) = 'qi'; qdescp(P_QI) = 'Cloud ice   mixing ratio (kg/kg)'
        qnames(P_QS) = 'qs'; qdescp(P_QS) = 'Snow mixing ratio (kg/kg)'
        qnames(P_QG) = 'qg'; qdescp(P_QG) = 'Graupel mixing ratio (kg/kg)'
        qnames(P_QH) = 'qh'; qdescp(P_QH) = 'Hail mixing ratio (kg/kg)'
        ice = 1
      CASE DEFAULT
        WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported mphyopt = ',mphyopt,'.'
        istatus = -2
      END SELECT

      IF (mphyopt == 8  ) mphyopt = 5   ! Map to a similar ARPS option
      IF (mphyopt == 18 ) mphyopt = 8

    ELSE
      istatus = -1
      WRITE(0,*) 'ERROR: unsupported model option ',modelopt
    END IF

!   SELECT CASE (mphyopt)
!      CASE (1:9,12:14)
!        rfopt = 1
!      CASE (10:11)
!        rfopt = 2
!    END SELECT
!
!    IF (myproc == 0) THEN
!      WRITE(6, '(5x,a,i2,a,i2)') 'rfopt = ', rfopt,                     &
!        ' was chosen based on mphyopt = ', mphyopt
!    END IF

    RETURN
  END SUBROUTINE init_scalar_array

  !#######################################################################

  SUBROUTINE varpara_read_nml_var(unum,npass,myproc,nx,ny,dx,dy,istatus)
  !
  !---------------------------------------------------------------------
  !
  !  PURPOSE:
  !  Read in analysis variables in namelist format from opened units.
  !
  !  AUTHOR:
  !
  !  Y. Wang (11/30/2016)
  !  Based on early code - init3dvar - in Vinit3dvar.f90 by J. Gao in 2001.
  !
  !  MODIFICATION HISTORY:
  !
  !
  !---------------------------------------------------------------------
  !
  !  Variable Declarations:
  !
  !---------------------------------------------------------------------

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: npass
    INTEGER, INTENT(IN)  :: unum, myproc
    INTEGER, INTENT(IN)  :: nx, ny
    REAL,    INTENT(IN)  :: dx, dy

    INTEGER, INTENT(OUT) :: istatus

    CHARACTER(LEN=32) :: fmtstr
  !
  !-----------------------------------------------------------------------
  !
  !  3DVAR namelists
  !
  !-----------------------------------------------------------------------
  !
     NAMELIST /var_const/ maxin, vrob_opt,sglob_opt,hydro_opt,          &
                          cldfirst_opt,pseudo_qv,pseudo_pt,pseudo_qs,   &
                          idealCase_opt,                                &
                          qobsrad_bdyopt, qobsrad_bdyzone, qback_factor

     NAMELIST /var_refil/ipass_filt,hradius,vradius_opt,vradius,        &
                         smooth_opt,filt_type

     !NAMELIST /var_exprt/chk_opt,assim_opt,cntl_var,cntl_var_rh
     NAMELIST /var_exprt/chk_opt,assim_opt,cntl_var_rh

     NAMELIST /var_reflec/ref_opt,intvl_T, thresh_ref, bkgerr_qr, bkgerr_qs,  &
               bkgerr_qh, wgt_ref, hradius_ref, vradius_opt_ref, vradius_ref, &
               pse_qr_err, pse_qs_err, pse_qg_err, pse_qh_err

  !---------------------------------------------------------------------

    INTEGER :: i

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ALLOCATE(maxin(npass),            STAT = istatus )
    ALLOCATE(qobsrad_bdyzone(npass),  STAT = istatus )
    ALLOCATE(cldfirst_opt(npass),     STAT = istatus )
    ALLOCATE(pseudo_qv(npass),        STAT = istatus )
    ALLOCATE(pseudo_qs(npass),        STAT = istatus )
    ALLOCATE(pseudo_pt(npass),        STAT = istatus )
    ALLOCATE(vrob_opt(npass),         STAT = istatus )
    ALLOCATE(ipass_filt(npass),       STAT = istatus )
    ALLOCATE(vradius_opt(npass),      STAT = istatus )
    ALLOCATE(hradius(npass),          STAT = istatus )
    ALLOCATE(vradius(npass),          STAT = istatus )
    ALLOCATE(ref_opt(npass),          STAT = istatus )
    ALLOCATE(vradius_opt_ref(npass),  STAT = istatus )
    ALLOCATE(wgt_ref(npass),          STAT = istatus )
    ALLOCATE(hradius_ref(npass),      STAT = istatus )
    ALLOCATE(vradius_ref(npass),      STAT = istatus )

  !-----------------------------------------------------------------------
  !
  !  Assign default values to the ADAS input variables
  !
  !-----------------------------------------------------------------------
  !
    cldfirst_opt = 0
    pseudo_qv = 0
    pseudo_pt = 0
    pseudo_qs = 0
    vrob_opt     = 0
    sglob_opt    = 0
    hydro_opt    = 0
    idealCase_opt= 0

    maxin(:)        = 20
    qobsrad_bdyopt  = 0
    qobsrad_bdyzone = 7
    qback_factor    = 2.0
    ipass_filt(:)   = 1
    hradius(:)      = 25.0
    vradius_opt     = 1
    vradius(:)      = 2
    chk_opt         = 0
    assim_opt       = 1
    !cntl_var = 0

    ref_opt         = 0
    intvl_T         = 5.0
    thresh_ref      = 15.0
    wgt_ref         = 0.0
    hradius_ref     = 3.0
    vradius_opt_ref = 1
    vradius_ref     = 2
    bkgerr_qr       = 1.0E-4
    bkgerr_qs       = 1.0E-4
    bkgerr_qh       = 1.0E-4
    pse_qr_err      = 1.0E-4
    pse_qs_err      = 1.0E-4
    pse_qg_err      = 1.0E-4
    pse_qh_err      = 1.0E-4
    filt_type       = 1
  !
  !-----------------------------------------------------------------------
  !
  !  read in 3DVAR namelists
  !
  !-----------------------------------------------------------------------
  !
    IF (myproc == 0) THEN
      READ(unum, var_const,  END=350)
      WRITE(6,*) 'Namelist block 3dvar_const sucessfully read.'

      !WRITE(*,'(1x,a,I4,a)') 'cldfirst_opt = ', cldfirst_opt,','
      !WRITE(*,'(1x,a,I4,a)') 'vrob_opt     = ', vrob_opt,','
      !WRITE(*,'(1x,a,I4,a)') 'sglob_opt    = ', sglob_opt,','
      !WRITE(fmtstr,'(a,I0,a)') '(1x,a,',npass,'(I8,a))'
      !WRITE(*,FMT=fmtstr) 'vrob_opt = ',(vrob_opt(i),', ',i=1,npass)

      READ(unum, var_refil,  END=350)
      WRITE(6,*) 'Namelist block 3dvar_refil sucessfully read.'

      READ(unum, var_exprt,  END=350)
      WRITE(6,*) 'Namelist block 3dvar_exprt sucessfully read.'

      READ(unum, var_reflec,  END=350)
      WRITE(6,*) 'Namelist block 3dvar_reflec sucessfully read.'

      350 CONTINUE
    END IF
    CALL mpupdatei(maxin,           npass)
    CALL mpupdatei(qobsrad_bdyopt,  1)
    CALL mpupdatei(qobsrad_bdyzone, npass)
    CALL mpupdater(qback_factor,    1)

    CALL mpupdatei(cldfirst_opt,    npass)
    CALL mpupdatei(vrob_opt,        npass)
    !CALL mpupdatei(sglob_opt,       1)
    CALL mpupdatei(hydro_opt,       1)
    CALL mpupdatei(pseudo_qv,       npass)
    CALL mpupdatei(pseudo_pt,       npass)
    CALL mpupdatei(pseudo_qs,       npass)
    CALL mpupdatei(idealCase_opt,   1)

    CALL mpupdatei(ipass_filt,      npass)
    CALL mpupdater(hradius,         npass)
    CALL mpupdatei(vradius,         npass)
    CALL mpupdatei(vradius_opt,     npass)
    CALL mpupdatei(filt_type,       1)

    CALL mpupdatei(chk_opt,     1)
    CALL mpupdatei(assim_opt,   1)
    !CALL mpupdatei(cntl_var,    1)
    CALL mpupdatei(cntl_var_rh, 1)

    CALL mpupdatei(ref_opt,         npass)
    CALL mpupdatei(vradius_opt_ref, npass)
    CALL mpupdater(intvl_T,         1)
    CALL mpupdater(thresh_ref,      1)
    CALL mpupdater(wgt_ref,         npass)
    CALL mpupdater(hradius_ref,     npass)
    CALL mpupdater(vradius_ref,     npass)
    CALL mpupdater(bkgerr_qr,       1)
    CALL mpupdater(bkgerr_qs,       1)
    CALL mpupdater(bkgerr_qh,       1)
    CALL mpupdater(pse_qr_err, 1)
    CALL mpupdater(pse_qs_err, 1)
    CALL mpupdater(pse_qg_err, 1)
    CALL mpupdater(pse_qh_err, 1)
  !
  !-------preparing parameters for recursive filter---new option added by GAO
  !
    IF (filt_type==1) THEN
       flag_legacyfilter=1  !use old low-order recursive filter
    ELSE IF (filt_type==2) THEN
       flag_legacyfilter=2  !use new high-order istropic recursive filter
       flag_iso=1           !isotropic
    ELSE
       PRINT *, 'Wrong option for parameter filt_type =',filt_type
       CALL arpsstop('ERROR: Wrong filt_type.',1)
    END IF

    IF (myproc == 0) THEN
      WRITE(*,'(/,1x,a)') 'Analysis     Horizontal                                                   '
      WRITE(*,'(1x,a)')   '  pass:  influence radius    Vertical influence radius     Boundary width '
      WRITE(*,'(1x,a)')   '-------- ================= ++++++++++++++++++++++++++++++ %%%%%%%%%%%%%%%%'
    END IF

    DO i=1,npass
      hradius(i)= hradius(i)*1000.0/dx
      IF (myproc == 0) THEN
        WRITE(*,'(1x,I4,F9.2,a13,a10,I2)',ADVANCE='NO')  i,hradius(i),'(grid points)','option - ',vradius_opt(i)
        IF (vradius_opt(i) == 1 ) THEN
          WRITE(*,'(F6.2,a13)',ADVANCE='NO') vradius(i), '(grid points)'
        ELSE IF (vradius_opt(i) == 2 ) THEN
          WRITE(*,'(F6.2,a13)',ADVANCE='NO') vradius(i), '(km)'
        ELSE
          WRITE(*,*) 'vradius_opt = ', vradius_opt(i), ' is not supported.'
          WRITE(*,*) 'NEWS3DVAR will stop here'
          CALL arpsstop('ERROR: Wrong vradius_opt.',1)
        END IF
      END IF

      IF (qobsrad_bdyopt > 10) THEN
        qobsrad_bdyzone(i) = MIN(qobsrad_bdyzone(i)+NINT(hradius(i)), MIN(20, MIN(nx, ny)))
      ELSE IF (qobsrad_bdyopt == 0) THEN
        qobsrad_bdyzone(i) = 0
      END IF
      IF (myproc == 0) THEN
!       IF ( qobsrad_bdyopt > 0) THEN
          WRITE(*,'(I4,a13)') qobsrad_bdyzone(i),'(grid points)'
!       ELSE
!         WRITE(*,'(I4,a13)') 0,'(grid points)'
!       END IF
      END IF
    END DO

    IF (myproc == 0) THEN
      WRITE(*,'(1x,a)')   '--------------------------------------------------------------------------'
    END IF

    IF (myproc == 0) THEN
      WRITE(*,'(5x,a)') 'Reflectivity options:'
!     WRITE(*,'(5x,a,I0,a)') 'ref_opt   = ', (ref_opt(i),', ',i=1,npass)
      WRITE(fmtstr,'(a,I0,a)') '(5x,a,',npass,'(I8,a))'
      WRITE(*,FMT=fmtstr) 'ref_opt   = ', (ref_opt(i),', ',i=1,npass)
      WRITE(*,'(5x,a,F9.2,a)') 'intvl_T    = ', intvl_T, ','
      WRITE(*,'(5x,a,F9.2,a)') 'thresh_ref = ', thresh_ref,','
      WRITE(*,'(5x,a,G9.2,a)') 'bkgerr_qr  = ', bkgerr_qr,','
      WRITE(*,'(5x,a,G9.2,a)') 'bkgerr_qs  = ', bkgerr_qs,','
      WRITE(*,'(5x,a,G9.2,a)') 'bkgerr_qh  = ', bkgerr_qh,','
    END IF

    IF (myproc == 0) THEN
      WRITE(*,'(/,1x,a)') 'Analysis Horizontal ref    Vertical reflectivity                          '
      WRITE(*,'(1x,a)')   '  pass:  influence radius  influence radius                    wgt_ref    '
      WRITE(*,'(1x,a)')   '-------- ================= ++++++++++++++++++++++++++++++ %%%%%%%%%%%%%%%%'
    END IF

    DO i=1,npass
      hradius_ref(i)= hradius_ref(i)*1000.0/dx
      IF (myproc == 0 .AND. ref_opt(i) == 1) THEN
        WRITE(*,'(1x,I4,F9.2,a13,a10,I2)',ADVANCE='NO')  i,hradius_ref(i),'(grid points)','option - ',vradius_opt_ref(i)
        IF (vradius_opt_ref(i) == 1 ) THEN
          WRITE(*,'(F6.2,a13)',ADVANCE='NO') vradius_ref(i), '(grid points)'
        ELSE IF (vradius_opt_ref(i) == 2 ) THEN
          WRITE(*,'(F6.2,a13)',ADVANCE='NO') vradius_ref(i), '(km)'
        ELSE
          WRITE(*,*) 'vradius_opt_ref = ', vradius_opt_ref(i), ' is not supported.'
          WRITE(*,*) 'ARPS3DVAR will stop here'
          CALL arpsstop('ERROR: Wrong vradius_opt_ref.',1)
        END IF

        WRITE(*,'(F17.10)') wgt_ref(i)
      END IF
    END DO

    IF (myproc == 0) THEN
      WRITE(*,'(1x,a)')   '--------------------------------------------------------------------------'
    END IF

    RETURN
  END SUBROUTINE varpara_read_nml_var

  !####################################################################

  SUBROUTINE varpara_set_varindex(nx,ny,nz,istatus)

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nx,ny,nz
    !INTEGER, INTENT(IN)  :: nx_m,ny_m,nz_m
    INTEGER, INTENT(OUT) :: istatus

    INCLUDE 'mp.inc'

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ibgn = 1;  iend = nx-1
    jbgn = 1;  jend = ny-1
    kbgn = 1;  kend = nz-1

    !ibgn_m = 1;  iend_m = nx_m-1
    !jbgn_m = 1;  jend_m = ny_m-1
    !kbgn_m = 1;  kend_m = nz_m-1

    iendu = nx; jendv = ny; kendw = nz

    IF (loc_x > 1)       THEN
      ibgn   = 2
      !ibgn_m = 2
    END IF

    IF (loc_x < nproc_x) THEN
      iend   = nx-2
      iendu  = nx-2
      !iend_m = nx_m-2
    END IF

    IF (loc_y > 1)       THEN
      jbgn   = 2
      !jbgn_m = 2
    END IF

    IF (loc_y < nproc_y) THEN
      jend   = ny-2
      jendv  = ny-2
      !jend_m = ny_m-2
    END IF

    RETURN
  END SUBROUTINE varpara_set_varindex

END MODULE module_varpara
