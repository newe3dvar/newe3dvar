!
!##################################################################
!##################################################################
!######                                                      ######
!######                 SUBROUTINE  COSTF                    ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Define the total costfunction for analysis
!
!
!-----------------------------------------------------------------------
!
!  AUTHOR:
!
!  Jidong Gao, CAPS, July, 2000
!
!-----------------------------------------------------------------------

SUBROUTINE costf(ount,numctr,ctrv, cfun_single,                         &
           gdu_err,gdv_err,gdp_err,gdt_err,gdq_err,gdw_err,             &
           gdqscalar_err,qscalar_ctr,                                   &
           u_ctr,v_ctr,p_ctr,t_ctr,q_ctr,w_ctr, psi, phi,               &
           gdscal, nx,ny,nz,gdscal_ref,                                 &
           nvar,nzua,nzret,                                             &
           mapfct,j1,j2,j3,aj3x,aj3y,aj3z,j3inv,rhobar,                 &
           dnw,dn,fnm,fnp,cf1,cf2,cf3,     & ! Added for WRF divergence constraint
           rhostr,rhostru, rhostrv, rhostrw, div3,                      &
           mxsng,mxua,mxret,mxcolret,                                   &
           nsrcsng,nsrcua,nsrcret,ncat,                                 &
           npass,xs,ys,zs,x,y,z,zp,hterain,                             &
           icatg,xcor,nam_var,                                          &
           ownsng,usesng,xsng,ysng,hgtsng,thesng,                       &
           obsng,odifsng,qobsng2,qualsng,isrcsng,nlevsng,nobsng,        &
           ownua,useua,xua,yua,hgtua,theua,                             &
           obsua,odifua,qobsua2,qualua,isrcua,nlevsua,nobsua,           &
           xretc,yretc,hgtretc,theretc,                                 &
           obsret,odifret,qobsret,qualret,                              &
           iret,isrcret,nlevret,ncolret,                                &
           srcsng,srcua,srcret,                                         &
           iusesng,iuseua,iuseret,                                      &
           corsng,corua,corret,                                         &
           xsng_p,ysng_p,ihgtsng,xua_p,yua_p,ihgtua,                    &
           zsng_1,zsng_2,zua_1,zua_2,                                   &
           oanxsng,oanxua,oanxret,                                      &
           sngsw, uasw, radsw, lgtsw, cwpsw, tpwsw, retsw,aerisw,       &
           hradius_3d,hradius_3d_ref,                                   &
           radius_z,ref_mos_3d,radius_z_ref,                            &
           anx,tk,tem1,tem2,tem3,tem4,tem5,tem6,tem7,istatus)

!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  USE model_precision
  USE module_varpara

  USE module_radar_grid
  USE module_lightning
  USE module_aeri !JJ
  USE module_ensemble
  USE module_convobs
  USE module_cwp
  USE module_tpw
  USE module_PseudoQobs

  USE constraint_divergence
  USE constraint_diagnostic_div
  USE constraint_mslp
  USE constraint_thermo
  USE constraint_smooth

  IMPLICIT NONE
!
!  INCLUDE 'varpara.inc'
!
  INCLUDE 'phycst.inc'
  INCLUDE 'grid.inc'
  INCLUDE 'bndry.inc'     ! to include ebc, nbc, sbc, wbc etc.
  INCLUDE 'globcst.inc'   ! to include mp_acct etc.
  INCLUDE 'mp.inc'

!
!-----------------------------------------------------------------------
!
!  Input Sizing Arguments
!
!-----------------------------------------------------------------------
!
  INTEGER, INTENT(IN) :: ount    ! output unit
  INTEGER, INTENT(IN) :: nx,ny,nz
  INTEGER, INTENT(IN) :: sngsw, uasw, radsw, lgtsw, retsw, aerisw
  INTEGER, INTENT(IN) :: cwpsw, tpwsw
  INTEGER, INTENT(IN) :: nvar
  INTEGER, INTENT(IN) :: nzua,nzret
  INTEGER, INTENT(IN) :: mxsng,mxua,mxret,mxcolret
  INTEGER, INTENT(IN) :: nsrcsng,nsrcua,nsrcret,ncat
  INTEGER, INTENT(IN) :: npass
  REAL,    INTENT(IN) :: radius_z(nx,ny,nz),radius_z_ref(nx,ny,nz)
  REAL,    INTENT(IN) :: hradius_3d(nx,ny,nz),hradius_3d_ref(nx,ny,nz)
  REAL,    INTENT(IN) :: ref_mos_3d(nx,ny,nz)

  LOGICAL, INTENT(IN) :: ownsng(mxsng), ownua(mxua)
  LOGICAL, INTENT(IN) :: usesng(mxsng), useua(mxua)
!
!-----------------------------------------------------------------------
!
!  input grid arguments
!
!-----------------------------------------------------------------------
!
!
  REAL(P) :: x     (nx)        ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL(P) :: y     (ny)        ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL(P) :: z     (nz)        ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL(P) :: zp    (nx,ny,nz)  ! The physical height coordinate defined at
                               ! w-point of the staggered grid.
  REAL(P) :: hterain(nx,ny)    ! The height of the terrain.

  REAL(P) :: mapfct(nx,ny,8)   ! Map factors at scalar, u and v points

  REAL(P) :: j1    (nx,ny,nz)  ! Coordinate transformation Jacobian
                               ! defined as - d( zp )/d( x ).
  REAL(P) :: j2    (nx,ny,nz)  ! Coordinate transformation Jacobian
                               ! defined as - d( zp )/d( y ).
  REAL(P) :: j3    (nx,ny,nz)  ! Coordinate transformation Jacobian
                               ! defined as d( zp )/d( z ).
  REAL(P) :: aj3x  (nx,ny,nz)  ! Coordinate transformation Jacobian defined
                               ! as d( zp )/d( z ) AVERAGED IN THE X-DIR.
  REAL(P) :: aj3y  (nx,ny,nz)  ! Coordinate transformation Jacobian defined
                               ! as d( zp )/d( z ) AVERAGED IN THE Y-DIR.
  REAL(P) :: aj3z  (nx,ny,nz)  ! Coordinate transformation Jacobian defined
                               ! as d( zp )/d( z ) AVERAGED IN THE Z-DIR.
  REAL(P) :: j3inv (nx,ny,nz)     ! Inverse of j3
  REAL(P), INTENT(IN) :: rhobar(nx,ny,nz)     ! Base state density

  REAL(P), INTENT(IN) :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3
  REAL(P), INTENT(IN) :: rhostru(nx,ny,nz)    ! Averaged rhostr at u points (kg/m**3).
  REAL(P), INTENT(IN) :: rhostrv(nx,ny,nz)    ! Averaged rhostr at v points (kg/m**3).
  REAL(P), INTENT(IN) :: rhostrw(nx,ny,nz)    ! Averaged rhostr at w points (kg/m**3).
!
  REAL(P) :: xs(nx)
  REAL(P) :: ys(ny)
  REAL(P) :: zs(nx,ny,nz)

  REAL(P), INTENT(IN) :: dnw(nz), dn(nz), fnm(nz), fnp(nz)
  REAL(P), INTENT(IN) :: cf1, cf2, cf3

  INTEGER :: icatg(nx,ny)
  REAL(P) :: xcor(ncat,ncat)
!
!-----------------------------------------------------------------------
!
!  Input Observation Arguments
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=6) :: nam_var(nvar)
  REAL(P) :: xsng(mxsng)
  REAL(P) :: ysng(mxsng)
  REAL(P) :: hgtsng(mxsng)
  REAL(P) :: thesng(mxsng)
  REAL(P) :: obsng(nvar,mxsng)
  REAL(P) :: odifsng(nvar,mxsng)
  REAL(P) :: qobsng2(nvar,mxsng)
  INTEGER :: qualsng(nvar,mxsng)
  INTEGER :: isrcsng(mxsng)
  INTEGER :: nlevsng(mxsng)
  INTEGER :: nobsng

  REAL(DP):: xsng_p(mxsng),ysng_p(mxsng)
  REAL(P) :: zsng_1(mxsng),zsng_2(mxsng)
  INTEGER :: ihgtsng(mxsng)

  REAL(P) :: xua(mxua)
  REAL(P) :: yua(mxua)
  REAL(P) :: hgtua(nzua,mxua)
  REAL(P) :: theua(nzua,mxua)
  REAL(P) :: obsua(nvar,nzua,mxua)
  REAL(P) :: odifua(nvar,nzua,mxua)
  REAL(P) :: qobsua2(nvar,nzua,mxua)
  INTEGER :: qualua(nvar,nzua,mxua)
  INTEGER :: nlevsua(mxua)
  INTEGER :: isrcua(mxua)
  INTEGER :: nobsua
!
  REAL(DP) :: xua_p(mxua),yua_p(mxua)
  REAL(P) :: zua_1(nzua,mxua),zua_2(nzua,mxua)
  INTEGER :: ihgtua(nzua,mxua)
!
  REAL(P) :: xretc(mxcolret)
  REAL(P) :: yretc(mxcolret)
  REAL(P) :: hgtretc(nzret,mxcolret)
  REAL(P) :: theretc(nzret,mxcolret)
  REAL(P) :: obsret(nvar,nzret,mxcolret)
  REAL(P) :: odifret(nvar,nzret,mxcolret)
  REAL(P) :: qobsret(nvar,nzret,mxcolret)
  INTEGER :: qualret(nvar,nzret,mxcolret)
  INTEGER :: nlevret(mxcolret)
  INTEGER :: iret(mxcolret)
  INTEGER :: isrcret(0:mxret)
  INTEGER :: ncolret

!
!-----------------------------------------------------------------------
!
!  Input Analysis Control Variables
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=8) :: srcsng(nsrcsng)
  CHARACTER (LEN=8) :: srcua (nsrcua )
  CHARACTER (LEN=8) :: srcret(nsrcret)

  INTEGER, INTENT(IN) :: iusesng(0:nsrcsng)
  INTEGER, INTENT(IN) :: iuseua (0:nsrcua)
  INTEGER, INTENT(IN) :: iuseret(0:nsrcret)
!
!-----------------------------------------------------------------------
!
!  Output Variables at Observation Locations
!
!-----------------------------------------------------------------------
!
  REAL(P) :: corsng(mxsng,nvar)
  REAL(P) :: corua(nzua,mxua,nvar)
! REAL(P) :: corret(nzret,mxcolret,nvar)
  REAL(P) :: corret(1,1,1)

  REAL(P) :: oanxsng(nvar,mxsng)
  REAL(P) :: oanxua(nvar,nzua,mxua)
  REAL(P) :: oanxret(nvar,nzret,mxcolret)
!
!-----------------------------------------------------------------------
!
!  Model grid state
!
!-----------------------------------------------------------------------
!
  REAL(P), INTENT(IN) :: anx(nx,ny,nz,nvar)
  REAL(P), INTENT(IN) :: tk(nx,ny,nz)

!
!-----------------------------------------------------------------------
!
!  Work arrays
!
!-----------------------------------------------------------------------
!
  REAL(P), INTENT(OUT) :: tem1(nx,ny,nz)
  REAL(P), INTENT(OUT) :: tem2(nx,ny,nz)
  REAL(P), INTENT(OUT) :: tem3(nx,ny,nz)
  REAL(P), INTENT(OUT) :: tem4(nx,ny,nz)
  REAL(P), INTENT(OUT) :: tem5(nx,ny,nz)
  REAL(P), INTENT(OUT) :: tem6(nx,ny,nz)
  REAL(P), INTENT(OUT) :: tem7(nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  Return status
!
!-----------------------------------------------------------------------
!
  INTEGER :: istatus
  INTEGER, SAVE :: itera = 0
  CHARACTER(LEN=20) :: varname

!
!   output argument
! --------
  INTEGER :: numctr
  REAL(P) :: ctrv(numctr)
  REAL(P) :: gdu_err(nx,ny,nz)
  REAL(P) :: gdv_err(nx,ny,nz)
  REAL(P) :: gdp_err(nx,ny,nz)
  REAL(P) :: gdt_err(nx,ny,nz)
  REAL(P) :: gdq_err(nx,ny,nz)
  REAL(P) :: gdw_err(nx,ny,nz)
  REAL(P) :: gdscal(nx,ny,nz)
  REAL(P) :: gdscal_ref(nx,ny,nz)
  REAL(P) :: gdqscalar_err(nx,ny,nz,nscalarq)

  REAL(P) ::   u_ctr(nx,ny,nz)
  REAL(P) ::   v_ctr(nx,ny,nz)
  REAL(P) ::   p_ctr(nx,ny,nz)
  REAL(P) ::   t_ctr(nx,ny,nz)
  REAL(P) ::   q_ctr(nx,ny,nz)
  REAL(P) ::   w_ctr(nx,ny,nz)
  REAL(P) ::   qscalar_ctr(nx,ny,nz,nscalarq)

  REAL(P) ::     psi(nx,ny,nz)
  REAL(P) ::     phi(nx,ny,nz)

!
!-----------------------------------------------------------------------
!
!  Misc.local variables
!
!    cfun:  value of cost function
!
!-----------------------------------------------------------------------
!
  INTEGER :: iflag,num

  REAL(P) :: ftabinv,setexp
  INTEGER :: i,j,k,l, nq, n,isrc
  REAL(P) :: rpass,zrngsq,thrngsq

  REAL(DP) :: zuu,zvv,zpp,ztt,zqq,zww
  REAL(DP) :: zqs

  REAL(P)  :: cfun
  REAL(DP) :: f_b,f_bu,f_bv,f_bp,f_bt,f_bq,f_bw
  REAL(DP) :: f_bqscalar(nscalarq)
  REAL(DP) :: f_ens, f_light
  REAL(DP) :: f_osng,f_ousng,f_ovsng,f_opsng,f_otsng,f_oqsng,f_owsng
  REAL(DP) :: f_oua,f_ouua,f_ovua,f_opua,f_otua,f_oqua,f_owua
  REAL(DP) :: f_oconv,f_ouconv,f_ovconv,f_opconv,f_otconv,f_oqconv,f_owconv
  REAL(DP) :: f_orad,f_ourad,f_ovrad,f_oprad,f_otrad,f_owrad
  REAL(DP) :: f_dpec,f_hBal, f_mslp, f_thermo, f_smth_u, f_smth_v, f_smth_w
  REAL(DP) :: f_cld_tot, f_cld_pt, f_cld_qv
  REAL(DP) :: f_cld_qs(nscalarq)
  REAL(DP) :: f_aeri_qv,f_aeri_pt !JJ

  REAL(DP) :: f_div, f_oref, f_ocwp, f_opsd
  REAL(DP) :: f_otpw, f_opwll, f_opwlm, f_opwlh
  REAL(P)  :: div3(nx,ny,nz)
  REAL(P)  :: maxdiv3, idiv,jdiv,kdiv
  REAL(P)  :: tdiv1, tdiv2,tdiv3,tdiv4

! array save coast function of each variable, the nvar+1 is divergence and radar
  REAL(DP) :: cfun_single(nvar+1), cfun_total


!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  !!! Use module module_varpara instead
  !ibgn = 2
  !iend = nx-2
  !iendu= nx-1
  !jbgn = 2
  !jend = ny-2
  !jendv= ny-1
  !
  !IF (loc_x > 1)       ibgn = 2
  !IF (loc_x < nproc_x) THEN
  !  iend  = nx-2
  !  iendu = nx-2
  !END IF
  !
  !IF (loc_y > 1)       jbgn = 2
  !IF (loc_y < nproc_y) THEN
  !  jend  = ny-2
  !  jendv = ny-2
  !END IF
  !
  !kbgn = 2
  !kend = nz-2
  !kendw= nz-1

  !!! Use module module_varpara instead
!
!
!  initialize control variable arrays
!  -------------------------------------------------------
!

  DO i = 1, nvar+1
    cfun_single(i) = 0.0
  END DO

  !
  ! sngsw,uasw,radsw should not be changed from now on
  !

  u_ctr (:,:,:) = 0.
  v_ctr (:,:,:) = 0.
  psi   (:,:,:) = 0.
  phi   (:,:,:) = 0.
  p_ctr (:,:,:) = 0.
  t_ctr (:,:,:) = 0.
  q_ctr (:,:,:) = 0.
  w_ctr (:,:,:) = 0.
  qscalar_ctr(:,:,:,:) = 0.

  IF( ibeta2 == 1 ) THEN
    en_ctr(:,:,:,:) = 0.
  END IF
!
! From ctrv to U, V, W, p_ctr, t_ctr, q_ctr, en_ctr etc.
!----------ctrv-----control variables--------one dimensional------------
!

  CALL adtrans(numctr,nx,ny,nz,nscalarq,psi,phi,p_ctr,t_ctr,q_ctr,w_ctr, &
               qscalar_ctr,ctrv,tem4,npass)
!
!----from here until div_opt all operations are on the scalar points----
!
  !ALLOCATE(f_bqscalar(nscalarq), STAT = istatus)

  f_b = 0.D0

  f_bu = 0.D0
  f_bv = 0.D0
  f_bp = 0.D0
  f_bt = 0.D0
  f_bq = 0.D0
  f_bw = 0.D0
  f_bqscalar = 0.D0

  IF ( ibeta1 == 1 ) THEN
    DO k = kbgn,kend
      DO j = jbgn,jend             ! local sum
        DO i = ibgn,iendu
          zuu =   psi(i,j,k) *   psi(i,j,k)
          f_bu = f_bu + zuu
        END DO
      END DO
    END DO

    DO k = kbgn,kend
      DO j = jbgn,jendv             ! local sum
        DO i = ibgn,iend
          zvv =   phi(i,j,k) *   phi(i,j,k)
          f_bv = f_bv + zvv
        END DO
      END DO
    END DO

    DO k = kbgn,kendw
      DO j = jbgn,jend             ! local sum
        DO i = ibgn,iendu
          zww = w_ctr(i,j,k) * w_ctr(i,j,k)
          f_bw = f_bw + zww
        END DO
      END DO
    END DO

    DO k = kbgn,kend
      DO j = jbgn,jend             ! local sum
        DO i = ibgn,iend
          zpp = p_ctr(i,j,k) * p_ctr(i,j,k)
          ztt = t_ctr(i,j,k) * t_ctr(i,j,k)
          zqq = q_ctr(i,j,k) * q_ctr(i,j,k)

          f_bp = f_bp + zpp
          f_bt = f_bt + ztt
          f_bq = f_bq + zqq
        END DO
      END DO
    END DO

    IF (ref_opt(npass) ==1 .OR. hydro_opt==1 .OR. cwpsw==1) THEN
      DO k = kbgn,kend
        DO j = jbgn,jend             ! local sum
          DO i = ibgn,iend

            DO nq = 1, nscalarq
              zqs   = qscalar_ctr(i,j,k,nq) * qscalar_ctr(i,j,k,nq)
              f_bqscalar(nq) = f_bqscalar(nq) + zqs
            END DO

          END DO
        END DO
      END DO
    END IF !IF (ref_opt ==1 .OR. hydro_opt==1)
  END IF

  f_ens=0.0
  IF( ibeta2 == 1 ) THEN
    DO l=1, nensvirtl
      DO k=kbgn,kend
        DO j = jbgn,jend             ! local sum
          DO i = ibgn,iend
            f_ens = f_ens+ en_ctr(i,j,k,l)*en_ctr(i,j,k,l)
          END DO
        END DO
      END DO
    END DO
    !DO l = 1, nensvirtl
    !  WRITE(varname,'(a,i1,a,i2.2)') 'en_ctr_',l,'_',itera
    !  CALL print3dnc_lg(200+l+itera,varname,en_ctr(:,:,:,l),nx,ny,nz)
    !END DO
  END IF   ! end of option ibeta2==1

  !if (itera > 4) stop

  f_b  = f_bu+f_bv+f_bp+f_bt+f_bq+f_bw
  DO nq = 1, nscalarq
    f_b = f_b+f_bqscalar(nq)
  END DO

  !IF (myproc == 0) THEN
  !  PRINT *,'f_b(u,v,p,t,q,w) = ',f_bu,f_bv,f_bp,f_bt,f_bq,f_bw
  !  PRINT *,'f_bq(r,s,h,i,c)  = ',f_bqr,f_bqs,f_bqh,f_bqi,f_bqc
  !  PRINT *,'f_ens            = ',f_ens
  !END IF

  IF( ibeta1 == 1 ) THEN
    cfun_single(1)=f_bu
    cfun_single(2)=f_bv
    cfun_single(3)=f_bp
    cfun_single(4)=f_bt
    cfun_single(5)=f_bq
    cfun_single(6)=f_bw

    IF (ref_opt(npass) ==1 .OR. hydro_opt==1 .OR. cwpsw==1) THEN
      DO nq = 1,nscalarq
        cfun_single(6+nq)  = f_bqscalar(nq)
      END DO
    END IF
  ENDIF

  IF( ibeta2 == 1 ) THEN
    cfun_single(nvar+1) = f_ens
  END IF
!
! --------------------------------------------------------
!
  IF( ibeta1 == 1 ) THEN

    CALL acct_interrupt(recrsub2_acct)

    IF( flag_legacyfilter ==1 ) THEN

    tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0      ! working array
    CALL ctr_to_vbl(ipass_filt(npass),hradius_3d,radius_z,nx,ny,nz,1,gdu_err, &
                    gdscal,  psi, tem1,tem2,tem3,tem4)

    tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
    CALL ctr_to_vbl(ipass_filt(npass),hradius_3d,radius_z,nx,ny,nz,2,gdv_err, &
                    gdscal,  phi, tem1,tem2,tem3,tem4)

    tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
    CALL ctr_to_vbl(ipass_filt(npass),hradius_3d,radius_z,nx,ny,nz,3,gdw_err, &
                    gdscal,w_ctr, tem1,tem2,tem3,tem4)

    tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
    CALL ctr_to_vbl(ipass_filt(npass),hradius_3d,radius_z,nx,ny,nz,0,gdp_err, &
                    gdscal,p_ctr, tem1,tem2,tem3,tem4)

    tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
    CALL ctr_to_vbl(ipass_filt(npass),hradius_3d,radius_z,nx,ny,nz,0,gdt_err, &
                    gdscal,t_ctr, tem1,tem2,tem3,tem4)

    tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
    CALL ctr_to_vbl(ipass_filt(npass),hradius_3d,radius_z,nx,ny,nz,0,gdq_err, &
                    gdscal,q_ctr, tem1,tem2,tem3,tem4)

!   Anwei Lai uses for Pseudo-qv obs. Should consider a better way to handle this
!
!    CALL ctr_to_vbl(ipass_filt(npass),0.5*hradius_ref(npass),radius_z_ref,nx,ny,nz,0,gdq_err,     &
!                    gdscal,q_ctr, tem1,tem2,tem3,tem4)

    IF ( ref_opt(npass) == 1 .OR. hydro_opt == 1 .OR. cwpsw==1 ) THEN

      DO nq = 1, nscalarq

        tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
        CALL ctr_to_vbl(ipass_filt(npass),hradius_3d_ref,radius_z_ref,  &
                        nx,ny,nz,0,gdqscalar_err(:,:,:,nq),             &
                        gdscal_ref,qscalar_ctr(:,:,:,nq),               &
                        tem1,tem2,tem3,tem4)

      END DO

    END IF !IF (ref_opt==1.OR. hydro_opt==1)

    ELSE    !(flag_legacyfilter ==2 ) then

      CALL forwardfilter(nx,ny,nz,nscalarq,psi,phi,p_ctr,t_ctr,q_ctr,w_ctr,   &
                         qscalar_ctr,                                         &
                         gdu_err,gdv_err,gdp_err,gdt_err,gdq_err,gdw_err,     &
                         gdqscalar_err, tem4)
      !print*,'inside forwardfilter'
    END IF

    CALL  acct_stop_inter

    tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0

    psi(:,:,:)   = psi(:,:,:)   * beta1
    phi(:,:,:)   = phi(:,:,:)   * beta1
    p_ctr(:,:,:) = p_ctr(:,:,:) * beta1
    t_ctr(:,:,:) = t_ctr(:,:,:) * beta1
    q_ctr(:,:,:) = q_ctr(:,:,:) * beta1
    w_ctr(:,:,:) = w_ctr(:,:,:) * beta1

    IF ( ref_opt(npass) == 1 .OR. hydro_opt == 1 .OR. cwpsw==1 ) THEN
        qscalar_ctr(:,:,:,:)= qscalar_ctr(:,:,:,:)*beta1
    END IF

  END IF

  IF( ibeta2 == 1 ) THEN

    CALL acct_interrupt(ensemble_acct)

    CALL ens_cost_ctr(nx,ny,nz,nscalarq,flag_legacyfilter,qobsrad_bdyzone(npass),&
                      npass,ipass_filt(npass),ref_opt(npass), hydro_opt,&
                      cwpsw,psi,phi,w_ctr,t_ctr,p_ctr,q_ctr,qscalar_ctr,&
                      tem1,tem2,tem3,tem4,tem5,istatus)

    CALL acct_stop_inter

  END IF    !end of ibeta2==1

!
!  Option (cntl_var ==0),  u, v as control variables
!
!  u_ctr, v_ctr are scalars
!
  !IF(cntl_var == 0) THEN

    DO k = 1, nz
      DO i = 1, nx
        DO j = 1, ny
          u_ctr(i,j,k) = psi(i,j,k)
          v_ctr(i,j,k) = phi(i,j,k)
        END DO
      END DO
    END DO

  !ELSE
  !
  !  DO k = 1, nz
  !    DO i = 2, nx-1
  !      DO j = 2, ny-1
  !        u_ctr(i,j,k) = ( psi(i-1,j+1,k)+psi(i,j+1,k)                  &
  !                        -psi(i-1,j-1,k)-psi(i,j-1,k) )/dy/4.          &
  !                      +( phi(i,  j,  k)-phi(i-1,j,k) )/dx
  !        u_ctr(i,j,k) = u_ctr(i,j,k)*mapfct(i,j,2)
  !
  !        v_ctr(i,j,k) = ( psi(i+1,j-1,k)+psi(i+1,j,k)                  &
  !                        -psi(i-1,j-1,k)-psi(i-1,j,k) )/dx/4.          &
  !                      +( phi(i,  j,  k)-phi(i,j-1,k) )/dy
  !        v_ctr(i,j,k) = v_ctr(i,j,k)*mapfct(i,j,3)
  !      END DO
  !    END DO
  !
  !    DO j=2,ny-1
  !      u_ctr( 1,j,k)=u_ctr( 2,j,k)+u_ctr( 2,j,k)-u_ctr( 3,j,k)
  !      v_ctr( 1,j,k)=v_ctr( 2,j,k)+v_ctr( 2,j,k)-v_ctr( 3,j,k)
  !      u_ctr(nx,j,k)=u_ctr(nx-1,j,k)+u_ctr(nx-1,j,k)-u_ctr(nx-2,j,k)
  !      v_ctr(nx,j,k)=v_ctr(nx-1,j,k)+v_ctr(nx-1,j,k)-v_ctr(nx-2,j,k)
  !    END DO
  !
  !    DO i=2,nx-1
  !      u_ctr(i, 1,k)=u_ctr(i, 2,k)+u_ctr(i, 2,k)-u_ctr(i, 3,k)
  !      v_ctr(i, 1,k)=v_ctr(i, 2,k)+v_ctr(i, 2,k)-v_ctr(i, 3,k)
  !      u_ctr(i,ny,k)=u_ctr(i,ny-1,k)+u_ctr(i,ny-1,k)-u_ctr(i,ny-2,k)
  !      v_ctr(i,ny,k)=v_ctr(i,ny-1,k)+v_ctr(i,ny-1,k)-v_ctr(i,ny-2,k)
  !    END DO
  !
  !    u_ctr(1,1 ,k)=0.5*( u_ctr(2,1,k)+u_ctr(1,2,k) )
  !    v_ctr(1,1 ,k)=0.5*( v_ctr(2,1,k)+v_ctr(1,2,k) )
  !    u_ctr(1,ny,k)=0.5*( u_ctr(1,ny-1,k)+u_ctr(2,ny,k) )
  !    v_ctr(1,ny,k)=0.5*( v_ctr(1,ny-1,k)+v_ctr(2,ny,k) )
  !
  !    u_ctr(nx,1,k)=0.5*( u_ctr(nx-1,1,k)+u_ctr(nx,2,k) )
  !    v_ctr(nx,1,k)=0.5*( v_ctr(nx-1,1,k)+v_ctr(nx,2,k) )
  !    u_ctr(nx,ny,k)=0.5*( u_ctr(nx,ny-1,k)+u_ctr(nx-1,ny,k) )
  !    v_ctr(nx,ny,k)=0.5*( v_ctr(nx,ny-1,k)+v_ctr(nx-1,ny,k) )
  !
  !  END DO
  !
  !  IF (mp_opt > 0) THEN
  !    CALL acct_interrupt(mp_acct)
  !    CALL mpsendrecv2dew(u_ctr, nx, ny, nz, ebc, wbc, 0, tem4)
  !    CALL mpsendrecv2dns(u_ctr, nx, ny, nz, nbc, sbc, 0, tem4)
  !
  !    CALL mpsendrecv2dew(v_ctr, nx, ny, nz, ebc, wbc, 0, tem4)
  !    CALL mpsendrecv2dns(v_ctr, nx, ny, nz, nbc, sbc, 0, tem4)
  !    CALL acct_stop_inter
  !  END IF
  !
  !END IF
!
!  loading single level data
! --------------------------------------------------------
!
  f_osng  = 0.
  f_ousng = 0.
  f_ovsng = 0.
  f_opsng = 0.
  f_otsng = 0.
  f_oqsng = 0.
  f_owsng = 0.

!  IF (myproc == 0) WRITE(ount,'(1x,a,i2)') '==== sngsw ===== ',sngsw

  IF(sngsw > 0) THEN

!   CALL linearint_3d(nx,ny,nz,u_ctr(1,1,1),x(1),ys(1),zs(1,1,1),       &
!          usesng,1, mxsng, nlevsng, nobsng,                            &
!          1,xsng_p,ysng_p,zsng_1,zsng_2,hgtsng,ihgtsng,corsng(1,1) )

!   CALL linearint_3d(nx,ny,nz,v_ctr(1,1,1),xs(1), y(1),zs(1,1,1),      &
!          usesng,1, mxsng, nlevsng, nobsng,                            &
!          2,xsng_p,ysng_p,zsng_1,zsng_2,hgtsng,ihgtsng,corsng(1,2) )

!   CALL linearint_3d(nx,ny,nz,p_ctr(1,1,1),xs(1),ys(1),zs(1,1,1),      &
!          usesng,1, mxsng, nlevsng, nobsng,                            &
!          3,xsng_p,ysng_p,zsng_1,zsng_2,hgtsng,ihgtsng,corsng(1,3) )

!   CALL linearint_3d(nx,ny,nz,t_ctr(1,1,1),xs(1),ys(1),zs(1,1,1),      &
!          usesng,1, mxsng, nlevsng, nobsng,                            &
!          4,xsng_p,ysng_p,zsng_1,zsng_2,hgtsng,ihgtsng,corsng(1,4) )

!   CALL linearint_3d(nx,ny,nz,q_ctr(1,1,1),xs(1),ys(1),zs(1,1,1),      &
!          usesng,1, mxsng, nlevsng, nobsng,                            &
!          5,xsng_p,ysng_p,zsng_1,zsng_2,hgtsng,ihgtsng,corsng(1,5) )

    !03/12/2020 Pan
    DO i = 1, nobsng
      IF ( ownsng(i) ) THEN
        CALL linearint_2df(nx, ny, u_ctr(:,:,2), xsng_p(i), ysng_p(i), corsng(i,PTR_U))
        CALL linearint_2df(nx, ny, v_ctr(:,:,2), xsng_p(i), ysng_p(i), corsng(i,PTR_V))
        CALL linearint_2df(nx, ny, p_ctr(:,:,2), xsng_p(i), ysng_p(i), corsng(i,PTR_P))
        CALL linearint_2df(nx, ny, t_ctr(:,:,2), xsng_p(i), ysng_p(i), corsng(i,PTR_PT))
        CALL linearint_2df(nx, ny, q_ctr(:,:,2), xsng_p(i), ysng_p(i), corsng(i,PTR_QV))
      END IF
    END DO

    !call linearint_3d(nx,ny,nz,w_ctr(1,1,1),xs(1),ys(1),zs(1,1,1),
    !   1, mxsng, nlevsng, nobsng,                                   &
    !   6,xsng_p,ysng_p,zsng_1,zsng_2,hgtsng,ihgtsng,corsng(1,6) )

    !num=0
    DO i = 1, nobsng
      IF ( usesng(i) ) THEN
        iflag = 1
        IF(qualsng(PTR_U,i) <= 0 .or. ihgtsng(i)<0 ) iflag = 0
        zuu  = corsng(i,PTR_U) - odifsng(PTR_U,i)
        IF(iflag /= 0) THEN
          corsng(i,PTR_U) = iflag*zuu / ( qobsng2(PTR_U,i) )
        ELSE
          corsng(i,PTR_U) = 0.0
        END IF

        zuu     = zuu * corsng(i,PTR_U)
        IF (ownsng(i)) f_ousng = f_ousng +  zuu
!
!
!    if(iflag.eq.1) num=num+1
!    print*,' num==',num,nobsng
!    print*,'qobsng2==', qobsng2(1,i),odifsng(1,i),iflag
!
!
        iflag = 1
        IF(qualsng(PTR_V,i) <= 0 .or. ihgtsng(i)<0 ) iflag = 0
        zvv = corsng(i,PTR_V) - odifsng(PTR_V,i)
        IF(iflag /= 0) THEN
          corsng(i,PTR_V) = iflag*zvv / ( qobsng2(PTR_V,i) )
        ELSE
          corsng(i,PTR_V) = 0.0
        END IF
        zvv     = zvv * corsng(i,PTR_V)
        IF (ownsng(i)) f_ovsng = f_ovsng +  zvv
!
!    if(iflag.eq.1) num=num+1
!    print*,' num==',num,nobsng
!    print*,'corsng==', corsng(i,2),odifsng(2,i)
!
!
        iflag = 1
        IF(qualsng(PTR_P,i) <= 0 .or. ihgtsng(i)<0) iflag = 0
        zpp        = corsng(i,PTR_P) - odifsng(PTR_P,i)
        IF(iflag /= 0) THEN
          corsng(i,PTR_P) = iflag*zpp / ( qobsng2(PTR_P,i) )
        ELSE
          corsng(i,PTR_P) = 0.0
        END IF
        zpp        = zpp * corsng(i,PTR_P)
        IF (ownsng(i)) f_opsng    = f_opsng +  zpp
!
!    if(iflag.eq.1) num=num+1
!    print*,' num==',num,nobsng
!    print*,'corsng==', corsng(i,3),odifsng(3,i)
!    print*,'qobsng2==', qobsng2(3,i),odifsng(3,i),iflag
!    print*,'f_opsng====',qobsng2(1,i),qobsng2(2,i),
!    :                       qobsng2(3,i),
!    :                       qobsng2(4,i),qobsng2(5,i)
!
        iflag = 1
        IF(qualsng(PTR_PT,i) <= 0 .or. ihgtsng(i)<0) iflag = 0
        ztt        = corsng(i,PTR_PT) - odifsng(PTR_PT,i)
        IF(iflag /= 0) THEN
          corsng(i,PTR_PT) = iflag*ztt / ( qobsng2(PTR_PT,i) )
        ELSE
          corsng(i,PTR_PT) = 0.0
        END IF
        ztt        = ztt * corsng(i,PTR_PT)
        IF (ownsng(i)) f_otsng    = f_otsng +  ztt
!
!    if(iflag.eq.1) num=num+1
!    print*,' num==',num,nobsng
!    print*,'corsng==', corsng(i,4),odifsng(4,i)
!
        iflag = 1
        IF(qualsng(PTR_QV,i) <= 0 .or. ihgtsng(i)<0) iflag = 0
        zqq        = corsng(i,PTR_QV) - odifsng(PTR_QV,i)
        IF(iflag /= 0) THEN
          corsng(i,PTR_QV) = iflag*zqq / ( qobsng2(PTR_QV,i) )
        ELSE
          corsng(i,PTR_QV) = 0.0
        END IF
        zqq        = zqq * corsng(i,PTR_QV)
        IF (ownsng(i)) f_oqsng    = f_oqsng +  zqq
!
!
!!   if(iflag.eq.1) num=num+1
!!   print*,'corsng==', corsng(i,5),odifsng(5,i)
!!   print*,' qobsng2 odifsng=',qobsng2(5,i),odifsng(5,i)
!    :            ,qualsng(5,i),iflag
!
!
!
!    if(1.eq.0) THEN
!
!       iflag = 1
!    if(qualsng(6,i).le.0 .and. ihgtsng<0) iflag = 0
!    zww        = corsng(i,6) - odifsng(6,i)
!    if(iflag.eq.1) num=num+1
!    print*,' num==',num,nobsng
!    if(iflag.ne.0) then
!      corsng(i,6) = iflag*zww / ( qobsng2(6,i) )
!    else
!      corsng(i,6) = 0.0
!    end if
!    zww        = zww * corsng(i,6)
!    f_owsng    = f_owsng +  zww
!    print*,'obsng==', corsng(i,6),odifsng(6,i)
!      end if
!
!  observation cost function for single level data
!  --------------------------------------------------------------
       !if (ownsng(i)) num = num + 1

      END IF  ! usesng(i)

    END DO

    f_osng=f_ousng+f_ovsng+f_opsng+f_otsng+f_oqsng+f_owsng
    cfun_single(PTR_U) =cfun_single(PTR_U )+f_ousng
    cfun_single(PTR_V) =cfun_single(PTR_V )+f_ovsng
    cfun_single(PTR_P) =cfun_single(PTR_P )+f_opsng
    cfun_single(PTR_PT)=cfun_single(PTR_PT)+f_otsng
    cfun_single(PTR_QV)=cfun_single(PTR_QV)+f_oqsng
    cfun_single(PTR_W) =cfun_single(PTR_W )+f_owsng

    !call mpsumdp(f_ousng,1)
    !call mpsumdp(f_ovsng,1)
    !call mpsumdp(f_opsng,1)
    !call mpsumdp(f_otsng,1)
    !call mpsumdp(f_oqsng,1)
    !call mpsumdp(f_owsng,1)
    !call mpsumdp( f_osng,1)
    !
    !IF (myproc == 0) THEN
    !  write(ount,*) '======================='
    !  write(ount,*) '  npass=',npass,' f_ousng=', f_ousng
    !  write(ount,*) '  npass=',npass,' f_ovsng=', f_ovsng
    !  write(ount,*) '  npass=',npass,' f_opsng=', f_opsng
    !  write(ount,*) '  npass=',npass,' f_otsng=', f_otsng
    !  write(ount,*) '  npass=',npass,' f_oqsng=', f_oqsng
    !  write(ount,*) '  npass=',npass,' f_owsng=', f_owsng
    !
    !  write(ount,*) '  npass=',npass,' f_osng =', f_osng
    !  write(ount,*) '======================='
    !  call flush(ount)
    !END IF
    !
    !WRITE(ount,*) myproc,':  npass=',npass,' f_ousng=', f_ousng
    !WRITE(ount,'(5x,a,I2.2,a,I6,a)') 'Domain ',myproc,' processed ',num,' SNG data.'
    !CALL mpbarrier

  END IF  ! sngsw > 0
!
!  loading conventional observation
! --------------------------------------------------------
!
  f_oconv = 0.0

  IF(convsw > 0) THEN

    CALL conv_costf(nx, ny, nz, p_ctr, t_ctr, q_ctr, u_ctr, v_ctr,  &
                    f_ouconv,f_ovconv,f_opconv,f_oqconv,f_otconv)

    f_oconv=f_ouconv+f_ovconv+f_opconv+f_otconv+f_oqconv
    cfun_single(PTR_U)  = cfun_single(PTR_U)  + f_ouconv
    cfun_single(PTR_V)  = cfun_single(PTR_V)  + f_ovconv
    cfun_single(PTR_P)  = cfun_single(PTR_P)  + f_opconv
    cfun_single(PTR_PT) = cfun_single(PTR_PT) + f_otconv
    cfun_single(PTR_QV) = cfun_single(PTR_QV) + f_oqconv

  END IF  ! convsw > 0

!
!  loading upper level data
! --------------------------------------------------------
!
  !num = 0
  f_oua  = 0.
  f_ouua = 0.
  f_ovua = 0.
  f_opua = 0.
  f_otua = 0.
  f_oqua = 0.
  f_owua = 0.

!  IF (myproc == 0) WRITE(ount,'(1x,a,i2)') '==== uasw ===== ',uasw

  IF(uasw > 0) THEN

    CALL linearint_3d(nx,ny,nz,u_ctr(1,1,1),x(1),ys(1),zs(1,1,1),       &
           useua, nzua, mxua, nlevsua, nobsua,                          &
           PTR_U,xua_p,yua_p,zua_1,zua_2,hgtua,ihgtua,corua(1,1,PTR_U) )

    CALL linearint_3d(nx,ny,nz,v_ctr(1,1,1),xs(1),y(1),zs(1,1,1),       &
           useua, nzua, mxua, nlevsua, nobsua,                          &
           PTR_V,xua_p,yua_p,zua_1,zua_2,hgtua,ihgtua,corua(1,1,PTR_V) )

    CALL linearint_3d(nx,ny,nz,p_ctr(1,1,1),xs(1),ys(1),zs(1,1,1),      &
           useua, nzua, mxua, nlevsua, nobsua,                          &
           PTR_P,xua_p,yua_p,zua_1,zua_2,hgtua,ihgtua,corua(1,1,PTR_P) )

    CALL linearint_3d(nx,ny,nz,t_ctr(1,1,1),xs(1),ys(1),zs(1,1,1),      &
           useua, nzua, mxua, nlevsua, nobsua,                          &
           PTR_PT,xua_p,yua_p,zua_1,zua_2,hgtua,ihgtua,corua(1,1,PTR_PT) )

    CALL linearint_3d(nx,ny,nz,q_ctr(1,1,1),xs(1),ys(1),zs(1,1,1),      &
           useua, nzua, mxua, nlevsua, nobsua,                          &
           PTR_QV,xua_p,yua_p,zua_1,zua_2,hgtua,ihgtua,corua(1,1,PTR_QV) )
!
!   CALL linearint_3d(nx,ny,nz,w_ctr(1,1,1),xs(1),ys(1),zs(1,1,1),      &
!          nzua, mxua, nlevsua, nobsua,                                 &
!          6,xua_p,yua_p,zua_1,zua_2,hgtua,ihgtua,corua(1,1,6) )
!
!
    DO i = 1, nobsua
      IF ( useua(i) ) THEN
        DO j = 1, nlevsua(i)

          iflag = 1
          IF(qualua(PTR_U,j,i) <= 0 .OR. ihgtua(j,i)<0) iflag = 0
          zuu          = corua(j,i,PTR_U) - odifua(PTR_U,j,i)
          IF(iflag /= 0) THEN
            corua(j,i,PTR_U) = iflag*zuu /( qobsua2(PTR_U,j,i) )
          ELSE
            corua(j,i,PTR_U) = 0.0
          END IF
          zuu          = zuu * corua(j,i,PTR_U)
          IF (ownua(i)) f_ouua = f_ouua +  zuu

          iflag = 1
          IF(qualua(PTR_V,j,i) <= 0 .or. ihgtua(j,i)<0) iflag = 0
          zvv          = corua(j,i,PTR_V) - odifua(PTR_V,j,i)
          IF(iflag /= 0) THEN
            corua(j,i,PTR_V) = iflag*zvv/( qobsua2(PTR_V,j,i) )
          ELSE
            corua(j,i,PTR_V) = 0.0
          END IF
          zvv          = zvv * corua(j,i,PTR_V)
          IF (ownua(i)) f_ovua = f_ovua +  zvv

          iflag = 1
          IF(qualua(PTR_P,j,i) <= 0 .or. ihgtua(j,i)<0) iflag = 0
          zpp          = corua(j,i,PTR_P) - odifua(PTR_P,j,i)
          IF(iflag /= 0) THEN
            corua(j,i,PTR_P) = iflag*zpp /( qobsua2(PTR_P,j,i) )
          ELSE
            corua(j,i,PTR_P) = 0.0
          END IF
          zpp          = zpp * corua(j,i,PTR_P)
          IF (ownua(i)) f_opua = f_opua +  zpp

          iflag = 1
          IF(qualua(PTR_PT,j,i) <= 0 .or. ihgtua(j,i)<0) iflag = 0
          ztt          = corua(j,i,PTR_PT) - odifua(PTR_PT,j,i)
          IF(iflag /= 0) THEN
            corua(j,i,PTR_PT) = iflag*ztt /( qobsua2(PTR_PT,j,i) )
          ELSE
            corua(j,i,PTR_PT) = 0.0
          END IF
          ztt          = ztt * corua(j,i,PTR_PT)
          IF (ownua(i)) f_otua = f_otua +  ztt


          iflag = 1
          IF(qualua(PTR_QV,j,i) <= 0 .or. ihgtua(j,i)<0) iflag = 0
          zqq          = corua(j,i,PTR_QV) - odifua(PTR_QV,j,i)
          IF(iflag /= 0) THEN
            corua(j,i,PTR_QV) = iflag*zqq / ( qobsua2(PTR_QV,j,i) )
          ELSE
            corua(j,i,PTR_QV) = 0.0
          END IF
          zqq          = zqq * corua(j,i,PTR_QV)
          IF (ownua(i)) f_oqua  = f_oqua +  zqq

!
!    print*,' qobsua2 odifua=',qobsua2(5,k,i),odifua(5,k,i)
!   :            ,qualua(5,k,i),iflag
!
!         iflag = 1
!      if(qualua(6,k,i).le.0 .or. ihgtua(j,i)<0) iflag = 0
!      zww          = corua(j,i,6) - odifua(6,j,i)
!      if(iflag.ne.0) then
!        corua(j,i,6) = iflag*zww / ( qobsua2(6,j,i) )
!      else
!        corua(j,i,6) = 0.0
!      end if
!      zww          = zww * corua(j,i,6)
!      f_owua       = f_owua +  zww
!

        END DO
        !IF (ownua(i)) num = num + 1
      END IF
    END DO

    f_oua = f_ouua+f_ovua+f_opua+f_otua+f_oqua+f_owua
    cfun_single(PTR_U )=cfun_single(PTR_U )+f_ouua
    cfun_single(PTR_V )=cfun_single(PTR_V )+f_ovua
    cfun_single(PTR_P )=cfun_single(PTR_P )+f_opua
    cfun_single(PTR_PT)=cfun_single(PTR_PT)+f_otua
    cfun_single(PTR_QV)=cfun_single(PTR_QV)+f_oqua
    cfun_single(PTR_W )=cfun_single(PTR_W )+f_owua

!    IF (myproc == 0) THEN
!    WRITE(ount,*) myproc, 'f_oua = ',f_oua
!    print*,'f_ouua = ',f_ouua
!    print*,'f_ovua = ',f_ovua
!    print*,'f_opua = ',f_opua
!    print*,'f_otua = ',f_otua
!    print*,'f_oqua = ',f_oqua
!    print*,'f_owua = ',f_owua
!      call flush(ount)
!    END IF
!    WRITE(ount,'(5x,a,I2.2,a,I6,a)') 'Domain ',myproc,' processed ',num,' UA data.'


  END IF

!
!=======================================================================
!=======================================================================
!
! NOTE: from now on until the end of the procedure, all control
!       variables are total values instead of innovation.
!
!=======================================================================
!=======================================================================

   u_ctr(:,:,:) = u_ctr(:,:,:)  + anx(:,:,:,PTR_U )
   v_ctr(:,:,:) = v_ctr(:,:,:)  + anx(:,:,:,PTR_V )
   w_ctr(:,:,:) = w_ctr(:,:,:)  + anx(:,:,:,PTR_W )
   t_ctr(:,:,:) = t_ctr(:,:,:)  + anx(:,:,:,PTR_PT)
   p_ctr(:,:,:) = p_ctr(:,:,:)  + anx(:,:,:,PTR_P )
   q_ctr(:,:,:) = q_ctr(:,:,:)  + anx(:,:,:,PTR_QV)

   IF( ref_opt(npass) == 1 .OR. hydro_opt == 1 .OR. cwpsw==1 ) THEN
     DO nq = 1,nscalarq
       qscalar_ctr(:,:,:,nq)=qscalar_ctr(:,:,:,nq)+ anx(:,:,:,PTR_LN+nq)
     END DO
   END IF

!-----------------------------------------------------------------------
!
!  Assimilate lightning data
!
!-----------------------------------------------------------------------
!
  IF (lgtsw > 0) THEN
    ! Qv assimilation via lightning:

    CALL lightning_costf(nx,ny,nz,q_ctr,                                &
                         ibgn,iend,jbgn,jend,kbgn,kend,                 &
                         f_light,istatus)

    cfun_single(PTR_QV)=cfun_single(PTR_QV)+f_light

  END IF ! lightning_opt=1

!-----------------------------------------------------------------------
!
!  Assimilate aeri t and qv data -JJ
!
!-----------------------------------------------------------------------
!
  IF (aerisw > 0) THEN

    CALL aeri_costf(nx,ny,nz,q_ctr,t_ctr,xs,ys,zs,                      &
                    ibgn,iend,jbgn,jend,kbgn,kend,                      &
                    f_aeri_qv,f_aeri_pt,istatus)

    cfun_single(PTR_QV)=cfun_single(PTR_QV)+f_aeri_qv
    cfun_single(PTR_PT)=cfun_single(PTR_PT)+f_aeri_pt


  END IF ! aeri_opt=1

!
!------------------------------------------------------------
! incorporate pseudo observations from cloud analysis
! in increment form (dqv, dpt )
!------------------------------------------------------------
!
  f_cld_tot = 0.0D0
  IF (cldfirst_opt(npass) ==1) THEN

    !CALL print3dnc_lg(200,'q_ctr',q_ctr,nx,ny,nz)
    CALL pseudoQ_cost(nx,ny,nz,nscalarq,ref_opt(npass),hydro_opt,       &
                      t_ctr,q_ctr,qscalar_ctr,                          &
                      f_cld_pt,f_cld_qv,f_cld_qs,f_cld_tot,             &
                      pseudo_pt(npass),pseudo_qv(npass),pseudo_qs(npass), & !add haiqin
                      istatus)

    cfun_single(PTR_PT) =cfun_single(PTR_PT) +f_cld_pt
    cfun_single(PTR_QV) =cfun_single(PTR_QV) +f_cld_qv
    DO nq = 1,nscalarq
      cfun_single(PTR_LN+nq) =cfun_single(PTR_LN+nq) +f_cld_qs(nq)
    END DO

   !IF (myproc == 0) print *, 'f_cld_pt = ', f_cld_pt/2.0
   !IF (myproc == 0) print *, 'f_cld_qv = ', f_cld_qv/2.0
   !IF (myproc == 0) print *, 'f_cld_qs = ', f_cld_qs/2.0
   !IF (myproc == 0) print *, 'f_cld_tot= ', f_cld_tot/2.0

  END IF !end of cldfirst_opt ==1

!==================================================================
!
!  loading MSLP  (mslp)
!
! --------------------------------------------------------
!
!
  IF ( do_mslp ) THEN
    CALL consmslp_costf(nx,ny,p0,rddcp,zs(:,:,2),p_ctr(:,:,2),t_ctr(:,:,2), &
                        f_mslp,istatus )
    cfun_single(3) = cfun_single(3) + f_mslp
  END IF
!
!-----------------------------------------------------------------------
!
! To assimiate cloud water path data
!
!-----------------------------------------------------------------------
!
  f_ocwp = 0.0D0
  f_opsd = 0.0D0
  IF ( cwpsw > 0 ) THEN
    CALL cwp_costf(nx,ny,nz,anx(:,:,:,PTR_P),tk,qscalar_ctr,f_ocwp,f_opsd,  &
                   tem1,tem2,tem3,tem4,tem5,tem6,tem7,istatus)

    cfun_single(nvar+1) = cfun_single(nvar+1) + f_ocwp + f_opsd

  END IF
!
!-----------------------------------------------------------------------
!
! loading total precipitable water data
!
!-----------------------------------------------------------------------
!
  f_otpw = 0.0D0; f_opwll = 0.0D0; f_opwlm = 0.0D0; f_opwlh = 0.0D0
  IF ( tpwsw > 0 ) THEN

    IF (idealCase_opt==1) THEN
      CALL tpw_costf(nx,ny,nz,zp,rhobar,q_ctr,f_otpw,                     &
                     tem1,tem2,istatus)
      cfun_single(nvar+1) = cfun_single(nvar+1) + f_otpw
    ELSE IF (idealCase_opt==0) THEN
      CALL pw_costf(nx,ny,nz,anx(:,:,:,PTR_P),q_ctr,f_opwll,f_opwlm,f_opwlh,            &
                    tem1,tem2,istatus)
      cfun_single(nvar+1) = cfun_single(nvar+1) + f_opwll + f_opwlm + f_opwlh
      f_otpw= f_otpw + f_opwll + f_opwlm + f_opwlh
    END IF

  END IF
!
!=======================================================================
!
!  loading radar data
!
! ----------------------------------------------------------------------
!
  f_orad  = 0.0D0
  f_ovrad = 0.0D0
  f_oref  = 0.0D0

!  IF (myproc == 0) WRITE(ount,'(1x,a,i2)') '==== radsw ===== ',radsw

  IF(radsw > 0) THEN

    CALL radargrid_costf(nx,ny,nz,zs,u_ctr,v_ctr,w_ctr,p_ctr,t_ctr,     &
                    qscalar_ctr,ref_mos_3d,intvl_T,thresh_ref,rhobar,   &
                    ibgn,iend,jbgn,jend,kbgn,kend,                      &
                    npass,vrob_opt(npass),ref_opt(npass),wgt_ref(npass),&
                    f_ovrad,f_oref,                                     &
                    tem1,tem2,tem3,tem4,tem5,istatus)

    f_orad = f_ovrad+f_oref
    cfun_single(nvar+1) = cfun_single(nvar+1) + f_orad

  END IF  ! If (radsw > 0 ) THEN


!
!-----------------------------------------------------------------------
!
!  Compute the momentum divergence term, defined as
!
!  div = d(u*rhostr)/dx + d(v*rhostr)/dy + d(wcont*rhostr)/dz.
!
!  NOTE: u, v & w should be on vector points when using in divergence constraint
!        and all other arps model constraints in the future.
!
!-----------------------------------------------------------------------
!
  f_div = 0.0D0
  IF( div_opt== 1 ) THEN

    IF (modelopt == 1 .OR. use_arps_metrics) THEN  ! for ARPS model
      CALL compute_div3_arps(npass,nx,ny,nz,dx,dy,dz,dxinv,dyinv,dzinv, &
                             u_ctr,v_ctr,w_ctr,div3,                    &
                             mapfct,j1,j2,j3,aj3z,j3inv,                &
                             rhostr,rhostru,rhostrv,rhostrw,            &
                             tem1,tem2,tem3,tem4,tem5,istatus)
    ELSE
       CALL cal_div3_wrf( npass,nx,ny,nz, dxinv, dyinv,                 &
                          u_ctr, v_ctr, w_ctr, div3,                    &
                          !msfuy, msfvx, msftx, msfty,                  &
                          mapfct(:,:,6),mapfct(:,:,7),mapfct(:,:,4),mapfct(:,:,5), &
                          dn, dnw, j3inv,                               &
                          fnm, fnp, cf1, cf2, cf3, j1, j2,              &
                          tem1,tem2,tem3, istatus )

    END IF

    DO k=kbgn,kend
      DO j=jbgn,jend
        DO i=ibgn,iend
          f_div = f_div + div3(i,j,k) * div3(i,j,k)
        END DO
      END DO
    END DO

    !call mpsumdp(f_div,1)
    !if (myproc == 0) write(100+myproc,*) 'f_div = ',f_div
    !CALL arpsstop(" ",1)
    !
  END IF   !IF( div_opt== 1 ) THEN

  !f_orad = f_orad+f_div
  cfun_single(nvar+1) = cfun_single(nvar+1) + f_div
!
!
!=======================================================================
! to use diagnostic divergence equation constraint
!    to help build some kind of balance tmp.debug.gge
!=======================================================================
!
  f_dpec = 0.0D0
  f_hBal = 0.0D0

  IF( do_dpec .OR. do_hBal ) THEN

     !u(:,:,:)=anx(:,:,:,1); v(:,:,:)=anx(:,:,:,2); w(:,:,:)=anx(:,:,:,6)

     CALL consdiv_costf(nx,ny,nz,nscalarq,ibgn,iend,jbgn,jend,          &
                        x,y,z,zp, j1,j2,j3,j3inv,mapfct,                &
                        rhostr,dxinv,dyinv,dzinv,                       &
                        u_ctr,v_ctr,w_ctr,p_ctr,t_ctr,q_ctr,            &
                        qscalar_ctr,                                    &
!                       u,v,w,wcont,                                    &
                        anx(:,:,:,PTR_U),anx(:,:,:,PTR_V),anx(:,:,:,PTR_W),tem1,    &
                        f_dpec,f_hBal,istatus)

    IF (do_dpec) cfun_single(nvar+1) = cfun_single(nvar+1) + f_dpec
    IF (do_hBal) cfun_single(nvar+1) = cfun_single(nvar+1) + f_hBal
    !u=0.0; v=0.0; w=0.0
  END IF !IF ( do_dpec .OR. do_hBal ) THEN

!
!
!-----------------------------------------------------------------------
!
!  Compute the thermowind constraint term, defined as
!
!  first: du/dz +(g/f/anx(5))*d(anx(5)/dy -u/anx(5)*d(anx(5))/dz
!  first: dv/dz -(g/f/anx(5))*d(anx(5)/dx -v/anx(5)*d(anx(5))/dz
!
!
!-----------------------------------------------------------------------
!
!
!  IF ( thermo_opt==1 ) then
!    CALL consthermo_costf(mp_opt,nx,ny,nz,ibgn,iend,jbgn,jend,          &
!                          p0,rddcp,g,dxinv,dyinv,dzinv,j3inv,           &
!                          u_ctr,v_ctr,p_ctr,t_ctr,                      &
!                          f_thermo,istatus)
!    cfun_single(nvar+1) = cfun_single(nvar+1) + f_thermo
!  END IF
!
!  IF(smth_opt == 1) THEN
!    CALL consmth_costf(npass,nx,ny,nz,ibgn,iend,jbgn,jend,              &
!                       u_ctr,v_ctr,w_ctr,                               &
!                       f_smth_u, f_smth_v, f_smth_w,istatus)
!    cfun_single(1) = cfun_single(1) + f_smth_u
!    cfun_single(2) = cfun_single(2) + f_smth_v
!    cfun_single(6) = cfun_single(6) + f_smth_w
!  END IF

  DO i=1,nvar+1
    cfun_single(i)=cfun_single(i)/2.0
  END DO

!-----------------------------------------------------------------------
!
! For diagnostic outputs
!
!-----------------------------------------------------------------------

  !call mpsumdp(cfun_single,nvar+1)
  IF (sngsw > 0)  CALL mpsumdp(f_osng,1)
  IF (convsw > 0) CALL mpsumdp(f_oconv,1)
  IF (uasw > 0)   CALL mpsumdp(f_oua,1)
  IF (radsw > 0) THEN
    CALL mpsumdp(f_ovrad,1)
    CALL mpsumdp(f_orad,1)
    CALL mpsumdp(f_oref,1)
  END IF
  IF (lgtsw > 0) THEN
    CALL mpsumdp(f_light,1)
  END IF

  IF (ibeta2 == 1) THEN
    CALL mpsumdp(f_ens,1)
  END IF

  IF (div_opt== 1 ) CALL mpsumdp(f_div,1)
  IF (cwpsw > 0)    CALL mpsumdp(f_ocwp,1)
  IF (tpwsw > 0) THEN
    !CALL mpsumdp(f_opsd,1)
    CALL mpsumdp(f_otpw,1)
  END IF
  IF (cldfirst_opt(npass) ==1) CALL mpsumdp(f_cld_tot,1)
  CALL mpsumdp(f_b,1)

  IF (myproc == 0) THEN
    IF (sngsw > 0) WRITE(6,'(1x,a,F25.10)') 'f_osng    = ',f_osng/2.0
    IF (convsw > 0) WRITE(6,'(1x,a,F25.10)') 'f_oconv   = ',f_oconv/2.0

    IF (radsw > 0) THEN
      WRITE(6,'(1x,3(a,F25.10))') 'f_orad    = ',f_orad/2.0,' = f_ovrad: ',f_ovrad/2.0,' + f_oref: ',f_oref/2.0
    END IF

    IF (uasw > 0)                WRITE(6,'(1x,a,F25.10)') 'f_oua     = ',f_oua/2.0
    IF (lgtsw > 0)               WRITE(6,'(1x,a,F25.10)') 'f_lgt     = ',f_light/2.0
    IF( ibeta2 == 1 )            WRITE(6,'(1x,a,F25.10)') 'f_ens     = ',f_ens/2.0

    IF (div_opt== 1 )            WRITE(6,'(1x,a,F25.10)') 'f_div     = ',f_div/2.0
    IF (cwpsw > 0)               WRITE(6,'(1x,a,F25.10)') 'f_ocwp    = ',(f_ocwp+f_opsd)/2.0
    IF (tpwsw > 0)               WRITE(6,'(1x,a,F25.10)') 'f_otpw    = ',f_otpw/2.0
    IF (do_dpec)                 WRITE(6,'(1x,a,F25.10)') 'f_dpec    = ',f_dpec/2.0
    IF (do_hBal)                 WRITE(6,'(1x,a,F25.10)') 'f_hBal    = ',f_hBal/2.0
    IF (cldfirst_opt(npass) ==1) WRITE(6,'(1x,a,F25.10)') 'f_cld_tot = ',f_cld_tot/2.0
    WRITE(6,'(1x,a,F25.10)') 'f_b       = ',f_b/2.0
    CALL flush(ount)
  END IF

!  cfun = (f_b+f_osng+f_oua+f_orad)/2.0
!
!  cfun_total=0
!  DO i=1,nvar+1
!    cfun_total=cfun_total+cfun_single(i)
!  ENDDO
!  write(*,*) 'check cost function==',cfun_total, cfun
!
!
!  PRINT*,' cfun b o===',f_b,f_osng,f_oua,f_orad,f_div
!  PRINT*,' cfun b o===',f_b,2*cfun-f_b, f_div
!
!  print *, ' cfun===',cfun, ' fb=',f_b,' f_osng=',f_osng     &
!          ,' f_oua==',f_oua,' f_orad=',f_orad
!
!
  itera = itera + 1

  RETURN
END SUBROUTINE costf
