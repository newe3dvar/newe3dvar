!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADVUVW0                    ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!
!

SUBROUTINE advuvw0(nx,ny,nz,u,v,w,wcont,rhostr,ubar,vbar,               &
           ubk,vbk,wbk, uadv,vadv,wadv,                                 &
           ustr,vstr,wstr,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Coordinates the calculation of the advection terms uadv, vadv and
!  wadv of the u, v and w equations. These terms are written in
!  equivalent advection form.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  11/20/91.
!
!  MODIFICATION HISTORY:
!
!  5/20/92 (M. Xue)
!  Added full documentation.
!
!  5/29/92 (K. Brewster)
!  Further facelift.
!
!  4/9/93 (M. Xue & K. Brewster)
!  Some index bounds for operators and loop bounds were corrected.
!  Redundant calculations were done before at the boundaries that
!  caused out-of-bound calculations, which should not have
!  affected the results in normal situations though.
!
!  4/10/93 (M. Xue & Hao Jin)
!  Add the terrain.
!
!  2/9/94 (D. Jahn)
!  Add tem3 work array in conjunction w/ changes to advu,advv,
!  advw, and advcts.
!
!  9/1/94 (D. Weber & Y. Lu)
!  Cleaned up documentation
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x component of velocity at all time levels (m/s)
!    v        y component of velocity at all time levels (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates at all time levels (m/s).
!    wcont    Contravariant vertical velocity (m/s)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    ubar     Base state x component of velocity (m/s)
!    vbar     Base state y component of velocity (m/s)
!
!  OUTPUT:
!
!    uadv     u-equation advection terms  (kg/m**3)*(m/s)/s
!    vadv     v-equation advection terms  (kg/m**3)*(m/s)/s
!    wadv     w-equation advection terms  (kg/m**3)*(m/s)/s
!
!  WORK ARRAYS:
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!-----------------------------------------------------------------------
!
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
  INCLUDE 'globcst.inc'

  INTEGER :: nt                ! no. of t-levels of t-dependent arrays.
  INTEGER :: tpast             ! Index of time level for the past time.
  INTEGER :: tpresent          ! Index of time level for the present
                               ! time.
  INTEGER :: tfuture           ! Index of time level for the future
                               ! time.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity (m/s)
  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)

  REAL :: uadv  (nx,ny,nz)     ! u-eqn advection terms
                               ! (kg/m**3)*(m/s)/s
  REAL :: vadv  (nx,ny,nz)     ! v-eqn advection terms
                               ! (kg/m**3)*(m/s)/s
  REAL :: wadv  (nx,ny,nz)     ! w-eqn advection terms
                               ! (kg/m**3)*(m/s)/s

  REAL :: ustr  (nx,ny,nz)     ! u * rhostr
  REAL :: vstr  (nx,ny,nz)     ! v * rhostr
  REAL :: wstr  (nx,ny,nz)     ! w * rhostr

  REAL :: ubk   (nx,ny,nz)     ! u * rhostr
  REAL :: vbk   (nx,ny,nz)     ! v * rhostr
  REAL :: wbk   (nx,ny,nz)     ! w * rhostr

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
  REAL :: umax,umin,uver,urms
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: tlevel,i,j,k
  REAL :: sum1, sum2, sum3
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  tlevel=tpresent
! print*,'tpresent=====================',tpresent
!
!-----------------------------------------------------------------------
!
!  Calculate ustr=rhostr*u, vstr=rhostr*v, wstr=rhostr*w
!
!-----------------------------------------------------------------------
!
!if (lvldbg>0) then
 if (1==0    ) then
     sum1  = 0.
     sum2  = 0.
     sum3  = 0.
   do i=1, nx
   do j=1, ny
   do k=1, nz
!    sum1 = sum1 +u(i,j,k,1)*u(i,j,k,1)
!    sum2 = sum2 +v(i,j,k,1)*v(i,j,k,1)
!    sum3 = sum3 +w(i,j,k,1)*w(i,j,k,1)
     sum1 = sum1 +ubar(i,j,k)*ubar(i,j,k)
     sum2 = sum2 +vbar(i,j,k)*vbar(i,j,k)
   end do
   end do
   end do
     print*,'sum1==u===========',sum1
     print*,'sum2==v===========',sum2
     print*,'sum3==w===========',sum3
end if
!
  ustr=0.0; vstr=0.0; wstr=0.0
!
! CALL uvwrho0(nx,ny,nz,u(1,1,1,tlevel),v(1,1,1,tlevel),                 &
!             wcont,rhostr,                                              &
!             ustr,vstr,wstr)
!
  CALL uvwrho0(nx,ny,nz,ubk,vbk,wcont,rhostr,                            &
              ustr,vstr,wstr)
!
!-----------------------------------------------------------------------
!
!  Calculate uadv:
!
!-----------------------------------------------------------------------
!
  CALL advu0(nx,ny,nz, u,v,w,                                            &
            ustr,vstr,wstr, ubar, uadv,                                  &
            tem1,tem2,tem3)

if(lvldbg>0) then
     sum1  = 0.
   do i=2, nx-1
   do j=2, ny-1
   do k=2, nz-2
     sum1 = sum1 +uadv(i,j,k)*uadv(i,j,k)
!   IF(uadv(i,j,k)>1.0) print*,'ijk===',i,j,k,uadv(i,j,k)
!   if(uadv(i,j,k)<(-0.1)) print*,'ijk===',i,j,k,uadv(i,j,k)
   end do
   end do
   end do
 CALL a3dmax1(uadv,1,nx,1,nx,1,ny,1,ny,1,nz,1,nz,umax,umin,uver,urms)
  print*,'sum1==uadv========',sum1
  print*,'umax=',umax,umin,uver,urms
end if
!
!-----------------------------------------------------------------------
!
!  Calculate vadv:
!
!-----------------------------------------------------------------------
!
  CALL advv0(nx,ny,nz, u,v,w,                                            &
            ustr,vstr,wstr, vbar, vadv,                                 &
            tem1,tem2,tem3)
! 
!-----------------------------------------------------------------------
!
!  Calculate wadv:
!
!-----------------------------------------------------------------------
!
  CALL advw0(nx,ny,nz, u,v,w,                                            &
            ustr,vstr,wstr, wadv,                                       &
            tem1,tem2,tem3)
  tem1=0.0; tem2=0.0; tem3=0.0
!
  RETURN
END SUBROUTINE advuvw0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                 SUBROUTINE ADVU0                     ######
!######                                                      ######
!######               Copyright (c) 1992-1994                ######
!######     Center for Analysis and Prediction of Storms     ######
!######     University of Oklahoma.  All rights reserved.    ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE advu0(nx,ny,nz,u,v,w,ustr,vstr,wstr, ubar,                   &
           uadv,                                                        &
           tem1,tem2,tem3)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the advection terms of the u-equation. These terms are
!  written in equivalent advection form.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  11/20/91.
!
!  MODIFICATION HISTORY:
!
!  5/20/92 (M. Xue)
!  Added full documentation.
!
!  5/29/92 (K. Brewster)
!  Further facelift.
!
!  4/10/93 (M. Xue & Hao Jin)
!  Add the terrain.
!
!  2/9/94 (D. Jahn)
!  Add 4th-order momentum advection.
!
!  9/1/94 (D. Weber & Y. Lu)
!  Cleaned up documentation
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at all time levels (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates at all time levels (m/s).
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!    ubar     Base state x component of velocity (m/s)
!
!  OUTPUT:
!
!    uadv     u-equation advection terms (kg/m**3)*(m/s)/s
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  INTEGER :: nt                ! The no. of t-levels of t-dependent
                               ! arrays.
  INTEGER :: tpast             ! Index of time level for the past time.
  INTEGER :: tpresent          ! Index of time level for the present
                               ! time.
  INTEGER :: tfuture           ! Index of time level for the future
                               ! time.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity at a given time level
                               ! (m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity at a given time level
                               ! (m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity at a given time level
                               ! (m/s)
  REAL :: ustr  (nx,ny,nz)     ! u*rhostr
  REAL :: vstr  (nx,ny,nz)     ! v*rhostr
  REAL :: wstr  (nx,ny,nz)     ! w*rhostr
  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)

  REAL :: uadv  (nx,ny,nz)     ! Advection term of the u-eq.
                               ! (kg/m**3)*(m/s)/s

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,onvf
  INTEGER :: ibgn,iend,jbgn,jend
  REAL :: vsb, vnb, sum1
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'       ! Global model control parameters
  INCLUDE 'grid.inc'          ! Added by Ying 10/15/2001
  INCLUDE 'bndry.inc'         ! Parameters for boundary conditions.
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!         tem1 = 0.
!         tem2 = 0.
!         tem3 = 0.
!
!-----------------------------------------------------------------------
!
!  Calculate rho*u* du/dx = avgx( avgx( ustr )*difx0( u ) ):
!
!-----------------------------------------------------------------------
!
!    sum1  = 0.
!  do i=1, nx
!  do j=1, ny
!  do k=1, nz
!    sum1 = sum1 +ustr(i,j,k)*ustr(i,j,k)
!  end do
!  end do
!  end do
!    print*,'0000=00000========',sum1
!
  onvf = 0
  CALL avgx0(ustr, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem1)

  onvf = 0
  CALL difx0(u(1,1,1,tpresent), onvf,                                    &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, dx, tem2)
!
!-----------------------------------------------------------------------
!
!   tem1 contains avgx(u*rho)
!   tem2 contains du/dx
!
!-----------------------------------------------------------------------

  CALL aamult0(tem1, tem2,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem3)

!-----------------------------------------------------------------------
!
!   tem3 contains (avgx(u*rho))*du/dx
!
!-----------------------------------------------------------------------

  onvf = 1
  CALL avgx0(tem3, onvf,                                                 &
            nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, uadv)

!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
          tem1 = 0.
          tem2 = 0.
          tem3 = 0.
!
!-----------------------------------------------------------------------
!
!  Calculate rho*v* du/dy = avgy( avgx( vstr )* dify( u ) )
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL avgx0(vstr, onvf,                                                 &
            nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, tem1)

  onvf = 1
  CALL dify0(u(1,1,1,tpresent), onvf,                                    &
            nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, dy, tem2)

!-----------------------------------------------------------------------
!
!   tem1 contains avgx(v*rho)
!   tem2 contains du/dy
!
!-----------------------------------------------------------------------

  CALL aamult0(tem1, tem2,                                               &
              nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, tem3)

!-----------------------------------------------------------------------
!
!  tem3 contains (avgx(v*rho)) * du/dy
!
!-----------------------------------------------------------------------

  tem1 = 0.0
  onvf = 0
  CALL avgy0(tem3, onvf,                                                 &
            nx,ny,nz, 2,nx-1, 2,ny-2, 2,nz-2, tem1)

!
!-----------------------------------------------------------------------
!
!  Calculate rho*v* du/dy on the north and south boundaries using
!  one sided advection.
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-2
    DO i=2,nx-1
      vsb=(vstr(i-1,1,k)+vstr(i-1,2,k)                                  &
          +vstr(i,1,k)+vstr(i,2,k))*0.25
      vnb=(vstr(i-1,ny-1,k)+vstr(i-1,ny,k)                              &
          +vstr(i,ny-1,k)+vstr(i,ny,k))*0.25
      IF (vsb < 0.0) THEN
        tem1(i,1,k)=vsb*(u(i,2,k,tpresent)-u(i,1,k,tpresent))*dyinv
      ELSE
        tem1(i,1,k)= rlxlbc* vsb*(u(i,1,k,tpast)-ubar(i,1,k))*dyinv
      END IF

      IF (vnb > 0.0) THEN
        tem1(i,ny-1,k)=vnb*                                             &
               (u(i,ny-1,k,tpresent)-u(i,ny-2,k,tpresent))*dyinv
      ELSE
        tem1(i,ny-1,k)=-rlxlbc*vnb*                                     &
               (u(i,ny-1,k,tpast)-ubar(i,ny-1,k))*dyinv
      END IF
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Add the u-equation horizontal advection terms, rho*u*du/dx (uadv) and
!  rho*v*du/dy (tem1).
!
!-----------------------------------------------------------------------
!
!    sum1  = 0.
!  do i=1, nx
!  do j=1, ny
!  do k=1, nz
!    sum1 = sum1 +tem1(i,j,k)*tem1(i,j,k)
!  end do
!  end do
!  end do
!    print*,'2222==uadv========',sum1
!
!
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        uadv(i,j,k)=uadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO
!
!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
          tem1 = 0.
          tem2 = 0.
          tem3 = 0.
!
!-----------------------------------------------------------------------
!
!  Calculate rho*w* du/dz = avgz0( avgx0( wstr ) * difz0( u ) )
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL avgx0(wstr, onvf,                                                 &
            nx,ny,nz,2,nx-1,1,ny-1, 2,nz-1, tem1)

  onvf = 1
  CALL difz0(u(1,1,1,tpresent), onvf,                                    &
            nx,ny,nz,2,nx-1,1,ny-1, 2,nz-1, dz, tem2)

!-----------------------------------------------------------------------
!
!  tem1 contains avgx(w*rho)
!  tem2 contains du/dz
!
!-----------------------------------------------------------------------

  CALL aamult0(tem1, tem2,                                               &
              nx,ny,nz,2,nx-1,1,ny-1, 2,nz-1, tem3)

!-----------------------------------------------------------------------
!
!  tem3 contains avgx(w*rho) * du/dz
!
!-----------------------------------------------------------------------

  onvf = 0
  tem1 = 0.0
  CALL avgz0(tem3, onvf,                                                 &
            nx,ny,nz,2,nx-1,1,ny-1, 2,nz-2, tem1)
!
!-----------------------------------------------------------------------
!
!  Add the u-equation vertical advection term rho*w*du/dz (tem1) to the
!  horizontal advection terms.
!
!-----------------------------------------------------------------------
!
!    sum1  = 0.
!  do i=1, nx
!  do j=1, ny
!  do k=1, nz
!    sum1 = sum1 +tem1(i,j,k)*tem1(i,j,k)
!  end do
!  end do
!  end do
!    print*,'3333==uadv========',sum1
!
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        uadv(i,j,k)=uadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO
!
  RETURN
END SUBROUTINE advu0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADVV0                      ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE advv0(nx,ny,nz,u,v,w,ustr,vstr,wstr, vbar,                    &
           vadv,                                                        &
           tem1,tem2,tem3)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the advection terms of the v-equation. These terms are
!  written in equivalent advection form.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  11/20/91.
!
!  MODIFICATION HISTORY:
!
!  5/20/92 (M. Xue)
!  Added full documentation.
!
!  5/29/92 (K. Brewster)
!  Further facelift.
!
!  4/9/93 (M. Xue & K. Brewster)
!  Some index bounds for operators and loop bounds were corrected.
!  Redundant calculations were done before at the boundaries that
!  caused out-of-bound calculations, which should not have
!  affected the results in normal situations though.
!
!  4/10/93 (M. Xue & Hao Jin)
!  Add the terrain.
!
!  2/9/94 (D. Jahn)
!  Add 4th-order momentum advection.
!
!  9/1/94 (D. Weber & Y. Lu)
!  Cleaned up documentation
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    v        y component of velocity at a given time level (m/s)
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!    vbar     Base state y component of velocity  (m/s)
!
!  OUTPUT:
!
!    vadv     v equation advection terms (kg/m**3)*(m/s)/s
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  INTEGER :: nt                ! no. of t-levels of t-dependent arrays.
  INTEGER :: tpast             ! Index of time level for the past time.
  INTEGER :: tpresent          ! Index of time level for the present
                               ! time.
  INTEGER :: tfuture           ! Index of time level for the future
                               ! time.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity at a given time level
                               ! (m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity at a given time level
                               ! (m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity at a given time level
                               ! (m/s)
  REAL :: ustr  (nx,ny,nz)     ! u*rhostr
  REAL :: vstr  (nx,ny,nz)     ! v*rhostr
  REAL :: wstr  (nx,ny,nz)     ! w*rhostr

  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)

  REAL :: vadv  (nx,ny,nz)     ! Advection term of the v-eq.
                               ! (kg/m**3)*(m/s)/s

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,onvf
  INTEGER :: ibgn,iend,jbgn,jend
  REAL :: ueb, uwb
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'    ! Global model control parameters
  INCLUDE 'grid.inc'       !Added by Ying 10/15/2001 
  INCLUDE 'bndry.inc'      ! Parameters for boundary conditions.
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
          tem1 = 0.
          tem2 = 0.
          tem3 = 0.
IF (1==1) THEN
!
!-----------------------------------------------------------------------
!
!  Calculate rho*u* dv/dx  = avgx( avgy( ustr ) * difx0( v ) )
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL avgy0(ustr, onvf,                                                 &
            nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, tem1)

  onvf = 1
  CALL difx0(v(1,1,1,tpresent), onvf,                                    &
            nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, dx, tem2)

!-----------------------------------------------------------------------
!
!  tem1 contains avgy(u*rho)
!  tem2 contains dv/dx
!
!-----------------------------------------------------------------------
!
  CALL aamult0(tem1, tem2,                                               &
              nx,ny,nz, 2,nx-1, 2,ny-1, 2,nz-2, tem3)

  onvf = 0
  CALL avgx0(tem3, onvf,                                                 &
            nx,ny,nz, 2,nx-2, 2,ny-1, 2,nz-2, vadv)

!
!-----------------------------------------------------------------------
!
!  Calculate rho*u* dv/dx at the east and west boundaries
!  using one-sided advection.
!
!-----------------------------------------------------------------------
!

  DO k=2,nz-2
    DO j=2,ny-1
      uwb=(ustr(1,j,k)+ustr(2,j,k)+ustr(1,j-1,k)+ustr(2,j-1,k))*0.25
      ueb=(ustr(nx-1,j,k)+ustr(nx,j,k)+ustr(nx-1,j-1,k)                 &
          +ustr(nx,j-1,k))*0.25
      IF (uwb < 0.0) THEN
        vadv(1,j,k)=uwb*(v(2,j,k,tpresent)-v(1,j,k,tpresent))*dxinv
      ELSE
        vadv(1,j,k)=rlxlbc*uwb*(v(1,j,k,tpast)-vbar(1,j,k))*dxinv
      END IF

      IF (ueb > 0.0) THEN
        vadv(nx-1,j,k)=ueb*                                             &
            (v(nx-1,j,k,tpresent)-v(nx-2,j,k,tpresent))*dxinv
      ELSE
        vadv(nx-1,j,k)=-rlxlbc*ueb*                                     &
            (v(nx-1,j,k,tpast)-vbar(nx-1,j,k))*dxinv
      END IF
    END DO
  END DO

 END IF !(1==0)
!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
          tem1 = 0.
          tem2 = 0.
          tem3 = 0.
!
!-----------------------------------------------------------------------
!
!  Calculate rho*v* dv/dy = avgy( avgy( vstr ) * dify( v ) )
!
!-----------------------------------------------------------------------
!
  onvf = 0
  CALL avgy0(vstr, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem1)

  onvf = 0
  CALL dify0(v(1,1,1,tpresent), onvf,                                    &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, dy, tem2)
!
!-----------------------------------------------------------------------
!
!  tem1 contains avgy(v*rho)
!  tem2 contains dv/dy
!
!-----------------------------------------------------------------------
!
  CALL aamult0(tem1, tem2,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-2, tem3)
!
!-----------------------------------------------------------------------
!
!  tem2 contains avgy(v*rho) * dv/dy
!
!-----------------------------------------------------------------------
!
  onvf = 1
  tem1 = 0.0
  CALL avgy0(tem3, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, tem1)

!
!-----------------------------------------------------------------------
!
!  Sum the x and y contributions to v advection
!  At this point tem1 contains the y contribution to the v advection
!
!-----------------------------------------------------------------------
!
!  Add the v-equation horizontal advection terms, rho*u*dv/dx (vadv) and
!  rho*v*dv/dy (tem1).
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        vadv(i,j,k)=vadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO
!
!
!
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
          tem1 = 0.
          tem2 = 0.
          tem3 = 0.
!
!-----------------------------------------------------------------------
!
!  Calculate rho*w* dv/dz = avgz0( avgy0( wstr ) * difz0( v ) )
!
!-----------------------------------------------------------------------
!

  onvf = 1
  CALL avgy0(wstr, onvf,                                                 &
            nx,ny,nz, 1,nx-1,2,ny-1,2,nz-1, tem1)

  onvf = 1
  CALL difz0(v(1,1,1,tpresent), onvf,                                    &
            nx,ny,nz, 1,nx-1,2,ny-1,2,nz-1, dz, tem2)
!
!-----------------------------------------------------------------------
!
!  tem1 contains avgy(w*rho)
!  tem2 contains dv/dz
!
!-----------------------------------------------------------------------
!
  CALL aamult0(tem1, tem2,                                               &
              nx,ny,nz, 1,nx-1,2,ny-1,2,nz-1, tem3)
!
!-----------------------------------------------------------------------
!
!  tem2 contains avgy(w*rho)*dv/dz
!
!-----------------------------------------------------------------------
!
  onvf = 0
  tem1 = 0.0
  CALL avgz0(tem3, onvf,                                                 &
            nx,ny,nz, 1,nx-1,2,ny-1,2,nz-2, tem1)
!
!-----------------------------------------------------------------------
!
!  Add the v-equation vertical advection term rho*w*dv/dz (tem1) to the
!  horizontal advection terms.
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        vadv(i,j,k)=vadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE advv0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADVW0                      ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE advw0(nx,ny,nz,u,v,w,ustr,vstr,wstr,                         &
           wadv,                                                        &
           tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!
!  Calculate the advection terms of the w-equation. These terms are
!  written in equivalent advection form.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  11/20/91.
!
!  MODIFICATION HISTORY:
!
!  5/20/92 (M. Xue)
!  Added full documentation.
!
!  5/29/92 (K. Brewster)
!  Further facelift.
!
!  4/9/93 (M. Xue & K. Brewster)
!  Some index bounds for operators and loop bounds were corrected.
!  Redundant calculations were done before at the boundaries that
!  caused out-of-bound calculations, which should not have
!  affected the results in normal situations though.
!
!  4/10/93 (M. Xue & Hao Jin)
!  Add the terrain.
!
!  2/9/94 (D. Jahn)
!  Add 4th-order momentum advection.
!
!  9/1/94 (D. Weber & Y. Lu)
!  Cleaned up documentation
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    w        z component of velocity at a given time level (m/s)
!
!    ustr     u * rhostr
!    vstr     v * rhostr
!    wstr     w * rhostr
!
!  OUTPUT:
!
!    wadv     Advection term in w equation (kg/m**3)*(m/s)/s
!
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  INTEGER :: nt                ! no. of t-levels of t-dependent arrays.
  INTEGER :: tpast             ! Index of time level for the past time.
  INTEGER :: tpresent          ! Index of time level for the present
                               ! time.
  INTEGER :: tfuture           ! Index of time level for the future
                               ! time.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  REAL :: u     (nx,ny,nz,nt)  ! Total u-velocity at a given time level
                               ! (m/s)
  REAL :: v     (nx,ny,nz,nt)  ! Total v-velocity at a given time level
                               ! (m/s)
  REAL :: w     (nx,ny,nz,nt)  ! Total w-velocity at a given time level
                               ! (m/s)
  REAL :: ustr  (nx,ny,nz)     ! u*rhostr
  REAL :: vstr  (nx,ny,nz)     ! v*rhostr
  REAL :: wstr  (nx,ny,nz)     ! w*rhostr

  REAL :: wadv  (nx,ny,nz)     ! w-eqn advection term (kg/m**3)*(m/s)/s

  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,onvf
  INTEGER :: ibgn,iend,jbgn,jend
  REAL :: ueb, uwb, vsb, vnb

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'       ! Global model control parameters
  INCLUDE 'grid.inc'          !Added by Ying 10/15/2001
  INCLUDE 'bndry.inc'         ! Parameters for boundary conditions.
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! 
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
          tem1 = 0.
          tem2 = 0.
          tem3 = 0.
!
!-----------------------------------------------------------------------
!
!  Calculate rho*u* dw/dx = avgx( avgz( ustr ) * difx0( w ) )
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL avgz0(ustr, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem1)

  onvf = 1
  CALL difx0(w(1,1,1,tpresent), onvf,                                    &
            nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1, dx, tem2)
!
!-----------------------------------------------------------------------
!
!  tem1 contains avgz(u*rho)
!  tem2 contains dw/dx
!
!-----------------------------------------------------------------------
!
  CALL aamult0(tem1, tem2,                                               &
              nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1, tem3)
!
!-----------------------------------------------------------------------
!
!  At this point tem2 contains avgz(u*rho)*dw/dx
!
!-----------------------------------------------------------------------
!
  onvf = 0
  CALL avgx0(tem3, onvf,                                                 &
            nx,ny,nz, 2,nx-2, 1,ny-1, 2,nz-1, wadv)

!
!-----------------------------------------------------------------------
!
!  Calculate rho*u* dw/dx on the east and west boundaries
!  using one-sided advection.
!
!-----------------------------------------------------------------------
!

  DO k=2,nz-1
    DO j=1,ny-1
      uwb=(ustr(1,j,k)+ustr(2,j,k)+ustr(1,j,k-1)+ustr(2,j,k-1))*0.25
      ueb=(ustr(nx-1,j,k)+ustr(nx,j,k)+ustr(nx-1,j,k-1)                 &
          +ustr(nx,j,k-1))*0.25
      IF (uwb < 0.0) THEN
        wadv(1,j,k)=uwb*(w(2,j,k,tpresent)-w(1,j,k,tpresent))*dxinv
      ELSE
        wadv(1,j,k)=rlxlbc*uwb*w(1,j,k,tpast)*dxinv
      END IF

      IF (ueb > 0.0) THEN
        wadv(nx-1,j,k)=ueb*                                             &
            (w(nx-1,j,k,tpresent)-w(nx-2,j,k,tpresent))*dxinv
      ELSE
        wadv(nx-1,j,k)=-rlxlbc*ueb*w(nx-1,j,k,tpast)*dxinv
      END IF
    END DO
  END DO
!
! 
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
          tem1 = 0.
          tem2 = 0.
          tem3 = 0.
!
!-----------------------------------------------------------------------
!
!  Calculate rho*v* dw/dy = avgy( avgz( vstr ) * dify ( w ) )
!
!-----------------------------------------------------------------------
!
  onvf = 1
  CALL avgz0(vstr, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem1)

  onvf = 1
  CALL dify0(w(1,1,1,tpresent), onvf,                                    &
            nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, dy, tem2)
!
!-----------------------------------------------------------------------
!
!  tem1 contains avgz(v*rho)
!  tem2 contains dw/dy
!
!-----------------------------------------------------------------------
!
  CALL aamult0(tem1, tem2,                                               &
              nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, tem3)
!
!-----------------------------------------------------------------------
!
!  At this point tem2 contains avgz(v*rho)*dw/dy
!
!-----------------------------------------------------------------------
!
  tem1 = 0.0
  onvf = 0
  CALL avgy0(tem3, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 2,ny-2, 2,nz-1, tem1)

!
!-----------------------------------------------------------------------
!
!  Calculate rho*v* dw/dy on the north and south boundaries
!  using one-sided advection.
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-1
    DO i=1,nx-1
      vsb=(vstr(i,1,k)+vstr(i,2,k)+vstr(i,1,k-1)+vstr(i,2,k-1))*0.25
      vnb=(vstr(i,ny-1,k)+vstr(i,ny,k)+vstr(i,ny-1,k-1)                 &
          +vstr(i,ny,k-1))*0.25
      IF (vsb < 0.0) THEN
        tem1(i,1,k)=vsb*(w(i,2,k,tpresent)-w(i,1,k,tpresent))*dyinv
      ELSE
        tem1(i,1,k)=rlxlbc*vsb*w(i,1,k,tpast)*dyinv
      END IF

      IF (vnb > 0.0) THEN
        tem1(i,ny-1,k)=vnb*                                             &
            (w(i,ny-1,k,tpresent)-w(i,ny-2,k,tpresent))*dyinv
      ELSE
        tem1(i,ny-1,k)=-rlxlbc*vnb*w(i,ny-1,k,tpast)*dyinv
      END IF
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Add the w-equation horizontal advection terms, rho*u*dw/dx (wadv) and
!  rho*v*dw/dy (tem1).
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        wadv(i,j,k)=wadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO
!
! 
!  Wash up tem1, tem2 and tem3.
!
!-----------------------------------------------------------------------
!
          tem1 = 0.
          tem2 = 0.
          tem3 = 0.
!
!-----------------------------------------------------------------------
!
!  Calculate rho*w* dw/dz = avgz0( avgz0( wstr ) * difz0( w ) )
!
!-----------------------------------------------------------------------
!
  onvf = 0
  CALL avgz0(wstr, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem1)

  onvf = 0
  CALL difz0(w(1,1,1,tpresent), onvf,                                    &
            nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, dz, tem2)
!-----------------------------------------------------------------------
!
!  tem1 contains avgz(w*rho)
!  tem2 contains dw/dz
!
!-----------------------------------------------------------------------

  CALL aamult0(tem1, tem2,                                               &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem3)

!-----------------------------------------------------------------------
!
!  tem2 contains avgz(w*rho)*dw/dz
!
!-----------------------------------------------------------------------
  tem1 = 0.0
  onvf = 1
  CALL avgz0(tem3, onvf,                                                 &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, tem1)
!
!-----------------------------------------------------------------------
!
!  Add the w-equation vertical advection term rho*w*dw/dz (tem1) to the
!  horizontal advection terms.
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        wadv(i,j,k)=wadv(i,j,k)+tem1(i,j,k)
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE advw0
