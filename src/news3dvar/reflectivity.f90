!
!  modified reflec_ferrier to compute reflectivity
!
SUBROUTINE reflec_g(nx,ny,nz,ref_obs,intvl,thresh_ref,rho, qr, qs, qh, t, rff)
  IMPLICIT NONE
!-----------------------------------------------------------------------
! Declare arguments.
!-----------------------------------------------------------------------

  INTEGER, INTENT(IN) :: nx,ny,nz   ! Dimensions of grid

  REAL, INTENT(IN) :: rho(nx,ny,nz)     ! Air density (kg m**-3)
  REAL, INTENT(IN) :: qr(nx,ny,nz)      ! Rain mixing ratio (kg kg**-1)
  REAL, INTENT(IN) :: qs(nx,ny,nz)      ! Snow mixing ratio (kg kg**-1)
  REAL, INTENT(IN) :: qh(nx,ny,nz)      ! Hail mixing ratio (kg kg**-1)
  REAL, INTENT(IN) :: t(nx,ny,nz)       ! Temperature (K)
  REAL, INTENT(IN) :: ref_obs(nx,ny,nz) ! observed gridded reflectivity
  REAL, INTENT(IN) :: intvl, thresh_ref

  REAL, INTENT(OUT) :: rff(nx,ny,nz)! Reflectivity (dBZ)
  REAL:: rtem

!-----------------------------------------------------------------------
! Declare local parameters.
!-----------------------------------------------------------------------

  REAL,PARAMETER :: ki2 = 0.176 ! Dielectric factor for ice if other
                                !   than melted drop diameters are used.
  REAL,PARAMETER :: kw2=0.93 ! Dielectric factor for water.

  REAL,PARAMETER :: degKtoC=273.15 ! Conversion factor from degrees K to
                                   !   degrees C

  REAL,PARAMETER :: m3todBZ=1.0E+18 ! Conversion factor from m**3 to
                                    !   mm**6 m**-3.

  REAL,PARAMETER :: pi=3.1415926 ! Pi.

  REAL,PARAMETER :: pipowf=7.0/4.0 ! Power to which pi is raised.

  REAL,PARAMETER :: N0r=8.0E+06 ! Intercept parameter in 1/(m^4) for rain.
  REAL,PARAMETER :: N0s=3.0E+06 ! Intercept parameter in 1/(m^4) for snow.
  REAL,PARAMETER :: N0h=4.0E+04 ! Intercept parameter in 1/(m^4) for hail.

  REAL,PARAMETER :: N0xpowf=3.0/4.0 ! Power to which N0r,N0s & N0h are
                                    !   raised.

  REAL,PARAMETER :: approxpow=0.95 ! Approximation power for hail
                                   !   integral.

  REAL,PARAMETER :: rqrpowf=7.0/4.0 ! Power to which product rho * qr
                                    !   is raised.
  REAL,PARAMETER :: rqsnpowf=7.0/4.0 ! Power to which product rho * qs
                                     !   is raised (dry snow).
  REAL,PARAMETER :: rqsppowf=7.0/4.0 ! Power to which product rho * qs
                                     !   is raised (wet snow).

  REAL,PARAMETER :: rqhpowf=(7.0/4.0)*approxpow ! Power to which product
                                                !   rho * qh is raised.

  REAL,PARAMETER :: rhoi=917.  ! Density of ice (kg m**-3)
  REAL,PARAMETER :: rhor=1000. ! Density of rain (kg m**-3)
  REAL,PARAMETER :: rhos=100.  ! Density of snow (kg m**-3)
  REAL,PARAMETER :: rhoh=913.  ! Density of hail (kg m**-3)

  REAL,PARAMETER :: rhoipowf=2.0     ! Power to which rhoi is raised.
  REAL,PARAMETER :: rhospowf=1.0/4.0 ! Power to which rhos is raised.
  REAL,PARAMETER :: rhoxpowf=7.0/4.0 ! Power to which rhoh is raised.

  REAL,PARAMETER :: Zefact=720.0 ! Multiplier for Ze components.

  REAL,PARAMETER :: lg10mul=10.0 ! Log10 multiplier

!-----------------------------------------------------------------------
! Declare local variables.
!-----------------------------------------------------------------------

  REAL    :: rcomp,scomp,hcomp,sumcomp
  INTEGER :: i,j,k
! REAL :: Zerf,Zesnegf,Zesposf,Zehf
  REAL(8),PARAMETER :: Zerf=3630803470.863780
  REAL(8),PARAMETER :: Zesnegf=958893421.2925436
  REAL(8),PARAMETER :: Zesposf=426068366492.9870
  REAL(8),PARAMETER :: Zehf   =61263782772.99985


!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!-----------------------------------------------------------------------
! First gather all the constants together.  (These are treated as
! variables because Fortran 90 does not allow real exponents when
! calculating parameters).
!-----------------------------------------------------------------------

! Zerf = (m3todBZ * Zefact) /  &
!                 ((pi ** pipowf) * (N0r ** N0xpowf) *  &
!                  (rhor ** rhoxpowf))
! Zesnegf = ((m3todBZ * Zefact   * Ki2 * (rhos ** rhospowf)) /  &
!                  ((pi ** pipowf) * Kw2 * (N0s ** N0xpowf) *  &
!                   (rhoi ** rhoipowf)))
! Zesposf = ((m3todBZ * Zefact) /  &
!                  ((pi ** pipowf) * (N0s ** N0xpowf) *  &
!                   (rhos ** rhoxpowf)))
! Zehf = (((m3todBZ * Zefact) /  &
!                   ((pi ** pipowf) * (N0h ** N0xpowf) *  &
!                    (rhoh ** rhoxpowf))) ** approxpow)

!-----------------------------------------------------------------------
! Now loop through the scalar grid points.
!-----------------------------------------------------------------------
  rcomp = 0.0
  scomp = 0.0
  hcomp = 0.0
  DO k = 1,nz
    DO j = 1,ny
      DO i = 1,nx
!-----------------------------------------------------------------------
! Check for bad air density value.
!-----------------------------------------------------------------------
!     IF (rho(i,j,k) <= 0.0 .or. ref_obs(i,j,k) <= thresh_ref ) THEN
      IF (rho(i,j,k) <= 0.0) THEN
          rff(i,j,k) = 0.0
      ELSE
          IF (qr(i,j,k) <= 0.0) THEN
            rcomp = 0.0
          ELSE
            rcomp = Zerf*((qr(i,j,k)*rho(i,j,k)) ** rqrpowf)
          END IF

          IF (qs(i,j,k) <= 0.0) THEN
             scomp = 0.0
          ELSE
            IF (t(i,j,k) <= degKtoC) THEN
              scomp = Zesnegf*((qs(i,j,k)*rho(i,j,k)) ** rqsnpowf)
            ELSE IF (t(i,j,k) <= (degKtoC + intvl)) THEN
              scomp = Zesposf*((qs(i,j,k)*rho(i,j,k)) ** rqsppowf)
            END IF
          END IF

          IF (qh(i,j,k) <= 0.0) THEN
            hcomp = 0.0
          ELSE
            hcomp = Zehf*((qh(i,j,k)*rho(i,j,k)) ** rqhpowf)
          END IF

!-----------------------------------------------------------------------
! Now add the contributions and convert to logarithmic reflectivity
! factor dBZ.
!-----------------------------------------------------------------------
!         if (t(i,j,k) >= (degKtoC + intvl ) ) then
!            sumcomp = rcomp
!         elseif (t(i,j,k) <= (degKtoC - intvl )) then
!            sumcomp = scomp + hcomp
!         else
!            rtem=(t(i,j,k)-degKtoC+intvl)/(2*intvl)
!            sumcomp=rtem*rcomp+(1-rtem)*(scomp+hcomp)
!         end if
          sumcomp = rcomp + scomp + hcomp

          IF( sumcomp>1.0 ) THEN
            rff(i,j,k) = lg10mul * LOG10(sumcomp)
          ELSE
            rff(i,j,k) = 0.0
          END IF
      END IF !  IF (rho(i,j,k) <= 0.0) ... ELSE ...
      END DO ! DO i
    END DO ! DO j
  END DO ! DO k

  RETURN
END SUBROUTINE reflec_g


SUBROUTINE ad_reflec_g(nx,ny,nz,ref_obs,intvl,thresh_ref, rho,        &
                       qr, qs, qh, t0, rff,qr0, qs0, qh0)

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: nx,ny,nz ! Dimensions of grid
  REAL,    INTENT(IN)  :: rho(nx,ny,nz) ! Air density (kg m**-3)
  REAL,    INTENT(OUT) :: qr(nx,ny,nz) ! Rain mixing ratio (kg kg**-1)
  REAL,    INTENT(OUT) :: qs(nx,ny,nz) ! Snow mixing ratio (kg kg**-1)
  REAL,    INTENT(OUT) :: qh(nx,ny,nz) ! Hail mixing ratio (kg kg**-1)
  REAL,    INTENT(IN)  :: ref_obs(nx,ny,nz)
  REAL,    INTENT(IN)  :: t0(nx,ny,nz) ! Temperature (K)
  REAL,    INTENT(IN)  :: qr0(nx,ny,nz)
  REAL,    INTENT(IN)  :: qs0(nx,ny,nz)
  REAL,    INTENT(IN)  :: qh0(nx,ny,nz)
  REAL,    INTENT(IN)  :: intvl, thresh_ref

  REAL, INTENT(INOUT) :: rff(nx,ny,nz) ! Reflectivity (dBZ)

!-----------------------------------------------------------------------
! Declare local parameters.
!-----------------------------------------------------------------------


  REAL :: rtem
  REAL,PARAMETER :: ki2 = 0.176 ! Dielectric factor for ice if other
                                !   than melted drop diameters are used.
  REAL,PARAMETER :: kw2=0.93 ! Dielectric factor for water.

  REAL,PARAMETER :: degKtoC=273.15 ! Conversion factor from degrees K to
                                   !   degrees C

  REAL,PARAMETER :: m3todBZ=1.0E+18 ! Conversion factor from m**3 to
                                    !   mm**6 m**-3.

  REAL,PARAMETER :: pi=3.1415926 ! Pi.

  REAL,PARAMETER :: pipowf=7.0/4.0 ! Power to which pi is raised.

  REAL,PARAMETER :: N0r=8.0E+06 ! Intercept parameter in 1/(m^4) for rain.
  REAL,PARAMETER :: N0s=3.0E+06 ! Intercept parameter in 1/(m^4) for snow.
  REAL,PARAMETER :: N0h=4.0E+04 ! Intercept parameter in 1/(m^4) for hail.

  REAL,PARAMETER :: N0xpowf=3.0/4.0 ! Power to which N0r,N0s & N0h are
                                    !   raised.

  REAL,PARAMETER :: approxpow=0.95 ! Approximation power for hail
                                   !   integral.

  REAL,PARAMETER :: rqrpowf=7.0/4.0 ! Power to which product rho * qr
                                    !   is raised.
  REAL,PARAMETER :: rqsnpowf=7.0/4.0 ! Power to which product rho * qs
                                     !   is raised (dry snow).
  REAL,PARAMETER :: rqsppowf=7.0/4.0 ! Power to which product rho * qs
                                     !   is raised (wet snow).

  REAL,PARAMETER :: rqhpowf=(7.0/4.0)*approxpow ! Power to which product
                                                !   rho * qh is raised.

  REAL,PARAMETER :: rhoi=917.  ! Density of ice (kg m**-3)
  REAL,PARAMETER :: rhor=1000. ! Density of rain (kg m**-3)
  REAL,PARAMETER :: rhos=100.  ! Density of snow (kg m**-3)
  REAL,PARAMETER :: rhoh=913.  ! Density of hail (kg m**-3)

  REAL,PARAMETER :: rhoipowf=2.0     ! Power to which rhoi is raised.
  REAL,PARAMETER :: rhospowf=1.0/4.0 ! Power to which rhos is raised.
  REAL,PARAMETER :: rhoxpowf=7.0/4.0 ! Power to which rhoh is raised.

  REAL,PARAMETER :: Zefact=720.0 ! Multiplier for Ze components.

  REAL,PARAMETER :: lg10mul=10.0 ! Log10 multiplier

!-----------------------------------------------------------------------
! Declare local variables.
!-----------------------------------------------------------------------

  REAL :: rcomp0,scomp0,hcomp0,sumcomp0
  REAL    :: rcomp,scomp,hcomp,sumcomp
  INTEGER :: i,j,k
! REAL    :: Zerf,Zesnegf,Zesposf,Zehf
  REAL(8),PARAMETER :: Zerf=3630803470.863780
  REAL(8),PARAMETER :: Zesnegf=958893421.2925436
  REAL(8),PARAMETER :: Zesposf=426068366492.9870
  REAL(8),PARAMETER :: Zehf   =61263782772.99985

  INCLUDE 'mp.inc'      ! to include mp_opt, myproc

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!-----------------------------------------------------------------------
! First gather all the constants together.  (These are treated as
! variables because Fortran 90 does not allow real exponents when
! calculating parameters).
!-----------------------------------------------------------------------

! Zerf = (m3todBZ * Zefact) / ((pi ** pipowf) * (N0r ** N0xpowf) *      &
!                              (rhor ** rhoxpowf))
! Zesnegf = ((m3todBZ * Zefact * Ki2 * (rhos ** rhospowf)) /            &
!                  ((pi ** pipowf) * Kw2 * (N0s ** N0xpowf) * (rhoi ** rhoipowf)))
! Zesposf = ((m3todBZ * Zefact) / ((pi ** pipowf) * (N0s ** N0xpowf) *  &
!                                  (rhos ** rhoxpowf)))
! Zehf    = ((m3todBZ * Zefact) / ((pi ** pipowf) * (N0h ** N0xpowf) *  &
!                                  (rhoh ** rhoxpowf)) ) ** approxpow

!-----------------------------------------------------------------------
! Now loop through the scalar grid points.
!-----------------------------------------------------------------------
  qr(:,:,:)=0.
  qs(:,:,:)=0.
  qh(:,:,:)=0.
  DO k = 1,nz
    DO j = 1,ny
      DO i = 1,nx
        rcomp0= 0.0
        scomp0= 0.0
        hcomp0= 0.0
!-----------------------------------------------------------------------
! Check for bad air density value.
!-----------------------------------------------------------------------

!       IF (rho(i,j,k) <= 0.0 .or. ref_obs(i,j,k) <= thresh_ref) THEN
        IF (rho(i,j,k) <= 0.0) THEN
          qs(i,j,k)=0.
          qr(i,j,k)=0.
          qh(i,j,k)=0.
        ELSE

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from rain.
!-----------------------------------------------------------------------

          IF (qr0(i,j,k) <= 0.0) THEN
            rcomp0= 0.0
          ELSE
            rcomp0= Zerf*((qr0(i,j,k)*rho(i,j,k)) ** rqrpowf)
          END IF

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from snow (dry or wet).
!-----------------------------------------------------------------------
IF(1==1) THEN
          IF (qs0(i,j,k) <= 0.0) THEN
            scomp0= 0.0
          ELSE IF (t0(i,j,k) <= degKtoC) THEN
            scomp0= Zesnegf*((qs0(i,j,k)*rho(i,j,k)) ** rqsnpowf)
          ELSE IF (t0(i,j,k) <= (degKtoC + intvl)) THEN
            scomp0= Zesposf*((qs0(i,j,k)*rho(i,j,k)) ** rqsppowf)
          END IF

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from hail.
!-----------------------------------------------------------------------

          IF (qh0(i,j,k) <= 0.0) THEN
            hcomp0= 0.0
          ELSE
            hcomp0= Zehf*((qh0(i,j,k)*rho(i,j,k)) ** rqhpowf)
          END IF
END IF
!
!-----------------------------------------------------------------------
! Now add the contributions and convert to logarithmic reflectivity
! factor dBZ.
!-----------------------------------------------------------------------
         sumcomp0= rcomp0 + scomp0 + hcomp0

!          if (t0(i,j,k) >= (degKtoC + intvl ) ) then
!            sumcomp0 = rcomp0
!          elseif (t0(i,j,k) <= (degKtoC - intvl )) then
!             sumcomp0 = scomp0 + hcomp0
!          else
!             rtem=(t0(i,j,k)-degKtoC+intvl)/(2*intvl)
!             sumcomp0=rtem*rcomp0+(1-rtem)*(scomp0+hcomp0)
!          end if

         IF( sumcomp0>1.0 ) THEN
           sumcomp = lg10mul/ALOG(10.0)/sumcomp0 *rff(i,j,k)
           rff(i,j,k) = 0.0
         ELSE
           sumcomp = 0.0
           rff(i,j,k) = 0.0
         END IF

         rcomp = sumcomp
         scomp = sumcomp
         hcomp = sumcomp
!         rcomp = 0.0
!         scomp = 0.0
!         hcomp = 0.0
!
!         if (t0(i,j,k) >= (degKtoC + intvl ) ) then
!           rcomp=sumcomp
!           scomp = 0.0
!           hcomp = 0.0
!           sumcomp = 0.0
!         elseif (t0(i,j,k) <= (degKtoC - intvl )) then
!           rcomp = 0.0
!           hcomp = sumcomp
!           scomp = sumcomp
!           sumcomp = 0.0
!         else
!           rtem=(t0(i,j,k)-degKtoC+intvl)/(2*intvl)
!           rcomp= rtem * sumcomp
!           scomp= (1-rtem) * sumcomp
!           hcomp= (1-rtem) * sumcomp
!           sumcomp = 0.0
!         end if
!
!-----------------------------------------------------------------------
!-------------------------adjoint---------------------------------------
!-----------------------------------------------------------------------
! Calculate reflectivity contribution from rain.
!-----------------------------------------------------------------------
          IF (qr0(i,j,k) <= 0.0) THEN
            qr(i,j,k) = 0.0
            rcomp = 0.0
          ELSE
            qr(i,j,k) = qr(i,j,k)+ Zerf*rqrpowf*rho(i,j,k)*              &
              ( (qr0(i,j,k)*rho(i,j,k))**(rqrpowf-1) )*rcomp
            rcomp = 0.0
          END IF

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from snow (dry or wet).
!-----------------------------------------------------------------------
IF(1==1) THEN
          IF (qs0(i,j,k) <= 0.0) THEN
            qs(i,j,k) = 0.0
            scomp = 0.0
          ELSE IF (t0(i,j,k) <= degKtoC) THEN
            qs(i,j,k) = qs(i,j,k)+ Zesnegf*rqsnpowf*rho(i,j,k)*          &
              ((qs0(i,j,k)*rho(i,j,k))**(rqsnpowf-1))*scomp
            scomp = 0.0
          ELSE IF (t0(i,j,k) <= (degKtoC + intvl)) THEN 
            qs(i,j,k) = qs(i,j,k)+ Zesposf*rqsppowf*rho(i,j,k)*          &
              ((qs0(i,j,k)*rho(i,j,k))**(rqsppowf-1))*scomp
            scomp = 0.0
          END IF

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from hail.
!-----------------------------------------------------------------------

          IF (qh0(i,j,k) <= 0.0) THEN
            qh(i,j,k) = 0.0
            hcomp = 0.0
          ELSE
            qh(i,j,k) = qh(i,j,k)+ Zehf*rqhpowf*rho(i,j,k)*              &
              ((qh0(i,j,k)*rho(i,j,k))**(rqhpowf-1))*hcomp
            hcomp = 0.0
          END IF
END IF
        END IF !  IF (rho(i,j,k) <= 0.0) ... ELSE ...

      END DO ! DO i
    END DO ! DO j
  END DO ! DO k
  RETURN
END SUBROUTINE ad_reflec_g
