!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Constraint using MSLP (mean sea level pressure)
!  to help build some kind of balance
!
!  tmp.debug.gge
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
MODULE constraint_mslp

  USE model_precision

  IMPLICIT NONE

  !INCLUDE 'mp.inc'

!-----------------------------------------------------------------------
!
! Control options
!
!-----------------------------------------------------------------------

  PRIVATE
  SAVE

  INTEGER  :: mslp_opt
  REAL(P)  :: mslp_lat, mslp_lon, mslp_obs
  REAL(P), ALLOCATABLE :: mslp_err(:)

  LOGICAL  :: do_mslp
  REAL(P)  :: err_mslp

  REAL(P), PARAMETER :: gamma=6.5,              & ! 6.5 K/km
                         ex2=5.2558774             ! g/R/gamma

  REAL(P)   :: mslp_i, mslp_j

  REAL(P)  :: mslp_diff

!-----------------------------------------------------------------------
!
! Work arrays
!
!-----------------------------------------------------------------------

  LOGICAL :: wrk_allocated

  REAL(P), ALLOCATABLE, DIMENSION(:,:) :: mslpwrk

!-----------------------------------------------------------------------
!
! Public interface
!
!-----------------------------------------------------------------------
  PUBLIC :: mslp_opt, do_mslp

  PUBLIC :: consmslp_read_nml_options, consmslp_checkflags
  PUBLIC :: consmslp_allocate_wrk, consmslp_deallocate_wrk
  PUBLIC :: consmslp_costf, consmslp_gradt

  CONTAINS

  SUBROUTINE consmslp_read_nml_options(unum,npass,myproc,istatus)

  !#####################################################################

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: unum, myproc
    INTEGER, INTENT(IN)  :: npass
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    NAMELIST /var_mslp/ mslp_opt, mslp_lat, mslp_lon, mslp_obs, mslp_err

  !---------------------------------------------------------------------

    INTEGER :: i

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !IF (npass > maxpass) THEN
    !  WRITE(*,'(1x,a,2(I0,a),/,7x,a)') 'ERROR: Request npass (',npass,  &
    !          ')is larger then this module maxpass (',maxpass,') in constraint_mslp.', &
    !          'Program will abort.'
    !  istatus = -1
    !  RETURN
    !END IF

    ALLOCATE(mslp_err(npass), STAT = istatus)
    CALL check_alloc_status(istatus, "constraint_mslp:mslp_err")

    mslp_opt = 0
    mslp_lat = 0.0
    mslp_lon = 0.0
    mslp_obs = 0.0
    mslp_err = 0.0

    IF (myproc == 0) THEN
      READ(unum, var_mslp,  END=350)
      WRITE(*,*) 'Namelist block var_mslp sucessfully read.'

      GOTO 400

      350 CONTINUE  ! Error with namelist reading
      WRITE(*,*) 'ERROR: reading namelist block var_mslp.'
      istatus = -2
      RETURN

      400 CONTINUE  ! Successful read namelist

      WRITE(*,'(5x,a,I0,a)') 'mslp_opt = ', mslp_opt,','
      IF ( mslp_opt >= 1 ) THEN
         mslp_obs = mslp_obs * 100 !hPa -> Pa
         mslp_err = mslp_err * 100 !hPa -> Pa
         WRITE(*,*) 'mslp_lat=', mslp_lat, ' mslp_lon=', mslp_lon
         WRITE(*,*) 'observed MSLP(Pa):', mslp_obs
         WRITE(*,*) 'User-specified MSLP obs error(Pa):'
         WRITE(*,*) (mslp_err(i), i =1 , npass )
      END IF

    END IF

    CALL mpupdatei(mslp_opt,   1)
    CALL mpupdater(mslp_lat,   1)
    CALL mpupdater(mslp_lon,   1)
    CALL mpupdater(mslp_obs,   1)
    CALL mpupdater(mslp_err,   npass)

    wrk_allocated = .FALSE.

    RETURN
  END SUBROUTINE consmslp_read_nml_options

  !#####################################################################

  SUBROUTINE consmslp_checkflags(ipass,nx,ny,xs,ys,istatus)
    IMPLICIT NONE
    INTEGER, INTENT(IN)  :: ipass
    INTEGER, INTENT(IN)  :: nx,ny
    REAL(P), INTENT(IN)  :: xs(nx), ys(ny)
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    REAL(P) :: mslp_x, mslp_y
    INTEGER :: i, j

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    do_mslp = .FALSE.

    err_mslp = mslp_err(ipass)

    IF( mslp_opt >= 1 .AND. mslp_err(ipass) > 0.0 ) do_mslp = .TRUE.

    mslp_i = -99999.9
    mslp_j = -99999.9

    IF (do_mslp) THEN

      CALL lltoxy(1,1,mslp_lat,mslp_lon,mslp_x,mslp_y)
      DO i = 1, nx-1
        IF ( mslp_x >= xs(i) .AND. mslp_x < xs(i+1) ) THEN
          mslp_i = i + ( mslp_x - xs(i) ) / ( xs(i+1)- xs(i))
          EXIT
        END IF
      END DO
      DO j = 1, ny-1
        IF ( mslp_y >= ys(j) .AND. mslp_y < ys(j+1) ) THEN
          mslp_j = j + ( mslp_y - ys(j) ) / ( ys(j+1)- ys(j))
          EXIT
        END IF
      END DO

    END IF

    RETURN
  END SUBROUTINE consmslp_checkflags

  SUBROUTINE consmslp_costf(nx,ny,p0,rddcp,zsfc,p_ctr,t_ctr,            &
                            f_mslp,istatus)

  !#####################################################################

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nx,ny

    REAL(P), INTENT(IN)  :: p0, rddcp
    REAL(P), INTENT(IN)  :: zsfc(nx,ny)

    REAL(P), INTENT(IN)  ::   p_ctr(nx,ny)
    REAL(P), INTENT(IN)  ::   t_ctr(nx,ny)

    REAL(DP), INTENT(OUT) :: f_mslp

    INTEGER,  INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i, j
    REAL(P) :: p00, t00, psl

    REAL(P) :: deltadx,deltady,deltadxm,deltadym

    !REAL(DP) :: f_mslp

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (.NOT. wrk_allocated) CALL consmslp_allocate_wrk(nx,ny,istatus)

    !! to compute SLP (tem18)
    DO j = 1, ny
      Do i =1, nx
        p00 = p_ctr(i,j)
        t00 = t_ctr(i,j)*(p00/p0)**rddcp  !temperature

        mslpwrk(i,j) = p00 * ( (t00 + gamma * zsfc(i,j)*0.001 ) / t00 ) **ex2
      END DO
    END DO

    i = FLOOR(mslp_i); j=FLOOR(mslp_j)
    IF((i > 0) .AND. (i < nx-1)  .AND. (j > 0) .AND. (j < ny-1)) THEN
      deltadx = mslp_i - i; deltady = mslp_j - j
      deltadxm= 1. - deltadx; deltadym= 1. - deltady

      psl  =    deltadxm*deltadym * mslpwrk(i,  j  )                    &
              + deltadx *deltadym * mslpwrk(i+1,j  )                    &
              + deltadxm*deltady  * mslpwrk(i,  j+1)                    &
              + deltadx *deltady  * mslpwrk(i+1,j+1)

      print *, 'mslp_obs=', mslp_obs, 'modelPart=', psl
      mslp_diff = ( psl - mslp_obs ) / err_mslp
      f_mslp = mslp_diff * mslp_diff

      print *, 'f_mslp=', f_mslp
      !cfun_single = cfun_single + f_mslp

    ELSE
      WRITE (6,'(a)')          ' WARNING: '
      WRITE (6,'(2(a,f10.2))') ' mslp_i = ',mslp_i,' mslp_j = ',mslp_j
      WRITE (6,'(a,/)')        ' no interpolation was performed'
    END IF

    RETURN
  END SUBROUTINE consmslp_costf

  SUBROUTINE consmslp_gradt(nx,ny,p0,rddcp,zsfc,p_ctr,t_ctr,            &
                            p_grd,istatus)

  !#####################################################################

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nx,ny

    REAL(P), INTENT(IN)  :: p0, rddcp
    REAL(P), INTENT(IN)  :: zsfc(nx,ny)
    REAL(P), INTENT(IN)  :: p_ctr(nx,ny)
    REAL(P), INTENT(IN)  :: t_ctr(nx,ny)

    REAL(P), INTENT(INOUT) :: p_grd(nx,ny)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j

    REAL(P) :: p00, t00, psl

    REAL(P) :: deltadx,deltady,deltadxm,deltadym

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    mslp_diff = mslp_diff / err_mslp
    mslpwrk = 0.0
    i = FLOOR(mslp_i); j=FLOOR(mslp_j)
    IF((i > 0) .AND. (i < nx-1)  .AND. (j > 0) .AND. (j < ny-1)) THEN
      deltadx  = mslp_i - i;   deltady  = mslp_j - j
      deltadxm = 1. - deltadx; deltadym = 1. - deltady

      mslpwrk(i+1,j+1)=mslpwrk(i+1,j+1) + deltadx*deltady *mslp_diff
      mslpwrk(i  ,j+1)=mslpwrk(i  ,j+1) + deltadxm*deltady*mslp_diff
      mslpwrk(i+1,j  )=mslpwrk(i+1,j  ) + deltadx*deltadym*mslp_diff
      mslpwrk(i  ,j  )=mslpwrk(i  ,j  ) + deltadxm*deltadym*mslp_diff

      mslp_diff = 0.0
    ELSE
      WRITE (0,*) 'WARNING: no interpolation was performed'
    END IF

    Do j = ny, 1, -1
      DO i = nx, 1, -1
        !p00 = anx(i,j,2,3) + p_ctr(i,j,2)
        !t00 = ( anx(i,j,2,4) + t_ctr(i,j,2))*(p00/p0)**rddcp  !temperature
        p00 =  p_ctr(i,j)
        t00 =  t_ctr(i,j)*(p00/p0)**rddcp  ! temperature

        p_grd(i,j) = p_grd(i,j) + mslpwrk(i,j) *                        &
                     ( (t00 + gamma * zsfc(i,j)*0.001 ) / t00 ) **ex2
!       grdTem =  tem18(i,j,2) *p00 * ex2 *                        &
!          ( (t00 + gamma * zs(i,j,2)*0.001 ) / t00 ) ** (ex2 -1)* &
!          gamma * zs(i,j,2)*0.001 *(-1.0) / t00 /t00

!       t_grd(i,j,2) = t_grd(i,j,2) + grdTem * (p00/p0)**rddcp
!       p_grd(i,j,2) = p_grd(i,j,2) + grdTem * ( anx(i,j,2,4) + t_ctr(i,j,2)) &
!                  * rddcp * (p00/p0)** (rddcp-1)

      END DO
    END DO

    RETURN
  END SUBROUTINE consmslp_gradt

  !#####################################################################

  SUBROUTINE consmslp_allocate_wrk(nx,ny,istatus)
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nx,ny
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (wrk_allocated) THEN
      WRITE(*,'(1x,a)') 'INFO: Should not be here. Maybe multiple calls of "consmslp_allocate_wrk".'
      istatus = 1
      RETURN
    END IF

    istatus = 0

    ALLOCATE(mslpwrk(nx,ny),  STAT = istatus )
    CALL check_alloc_status(istatus, "constraint_mslp: mslpwrk")

    wrk_allocated = .TRUE.

    RETURN
  END SUBROUTINE consmslp_allocate_wrk

  !#####################################################################

  SUBROUTINE consmslp_deallocate_wrk(istatus)
    IMPLICIT NONE

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (wrk_allocated) THEN

      DEALLOCATE(mslpwrk,    STAT = istatus )

      wrk_allocated = .FALSE.

    END IF

    RETURN
  END SUBROUTINE consmslp_deallocate_wrk

END MODULE constraint_mslp
