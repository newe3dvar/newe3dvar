MODULE module_convobs

  USE model_precision
  USE module_physics
  USE module_read_prepbufr, ONLY: nvar_conv

  IMPLICIT NONE

  PRIVATE

  CHARACTER(LEN=256)            :: convfname, convinfofil, converrfil

  INTEGER, PARAMETER            :: E_PS = 1, E_T = 2, E_QV= 3,  &
                                   E_UV = 4, E_PW = 5
  REAL(P),ALLOCATABLE           :: convinfo(:,:,:), errtable(:,:,:)

  REAL(P), PARAMETER            :: rmiss_conv = -9999.0
  INTEGER                       :: totalconvobs
  INTEGER, ALLOCATABLE          :: ownuseconv(:)               ! 1 for use, 2 for own
  REAL(P), ALLOCATABLE          :: convobs(:, :, :)

  INTEGER, PUBLIC                         :: nconvfil, nconvobs, convsw
  INTEGER, ALLOCATABLE, PUBLIC            :: iuseconv(:)
  DOUBLE PRECISION, ALLOCATABLE, PUBLIC   :: x_grid(:), y_grid(:), z_grid(:)
  REAL(P), ALLOCATABLE, PUBLIC            :: conv_innov(:, :, :), bkg_incr(:, :), ana_incr(:, :)
  LOGICAL, ALLOCATABLE, PUBLIC            :: sfctype(:)
  PUBLIC  :: convobs_read_nml, prep_convtbl, prepconvobs, grdtoconv, convobs_set_usage,     &
             dealloc_convobs, conv_costf, conv_gradt

  CONTAINS

  !#####################################################################

  SUBROUTINE convobs_read_nml(unum, maxpass, istatus)

    IMPLICIT NONE

    INCLUDE "globcst.inc"
    INCLUDE 'mp.inc'

    INTEGER, INTENT(IN)  :: unum
    INTEGER, INTENT(IN)  :: maxpass
    INTEGER, INTENT(OUT) :: istatus

    !---------------------------------------------------------------------

      NAMELIST /adas_conv/ nconvfil,convfname,convinfofil,converrfil,iuseconv

    !---------------------------------------------------------------------

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ALLOCATE(iuseconv(maxpass), STAT = istatus)
    CALL check_alloc_status(istatus, "convobs:iuseconv")
    nconvfil = 0
    iuseconv = 0
    convfname = 'NULL'
    convinfofil = 'NULL'
    converrfil = 'NULL'

    IF (myproc == 0) THEN
      READ(unum, adas_conv, END=324)
      WRITE(6,*) 'Namelist block adas_conv sucessfully read.'
      GOTO 325

      324 CONTINUE
      WRITE(*,*) 'ERROR: reading namelist <adas_conv>.'
      istatus = -1
      RETURN
      325 CONTINUE

      IF (nconvfil > 0 .AND. (TRIM(convfname) == 'NULL' .OR. TRIM(convinfofil) == 'NULL' .OR.  &
          TRIM(converrfil) == 'NULL')) THEN
        WRITE(*,'(1x,a)') 'WARNING: Incorrect filepath for convfname/convinfofil/converrfil, nconvfil is set to be 0.'
        nconvfil = 0
        iuseconv = 0
      END IF

      IF (ANY(iuseconv(2:maxpass)==1)) THEN
        iuseconv(2:maxpass) = 0
        WRITE(*,'(1x,a)') 'WARNING: Currently, prepbufr conventional observations can only be used in the first pass.'
        WRITE(*,'(1x,a)') 'iuseconv for other passes are set to 0.'
      END IF
    END IF

    IF (mp_opt > 0) THEN
      CALL mpupdatei(nconvfil, 1)
      CALL mpupdatei(iuseconv, maxpass)
    END IF

    RETURN
  END SUBROUTINE convobs_read_nml

  !#####################################################################

  SUBROUTINE prep_convtbl(ptop, istatus)

    USE module_read_prepbufr, ONLY: nrpttyp

    IMPLICIT NONE

    INCLUDE "globcst.inc"
    INCLUDE 'mp.inc'
    INCLUDE 'adas.inc'

    REAL, INTENT(IN)      :: ptop
    INTEGER               :: istatus

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    ! Read convential observation infos
    IF (myproc == 0) CALL read_convinfo(TRIM(convinfofil), istatus)

    IF (mp_opt > 0) THEN
      CALL mpupdatei(nrpttyp, 1)
      CALL mpupdatei(istatus, 1)
    END IF

    IF (nrpttyp == 0 .OR. istatus /= 0) THEN
      nconvfil = 0
      iuseconv = 0
      CALL dealloc_convobs()
      RETURN
    ELSE
      !! Read error table
      IF (myproc == 0) CALL read_errtable(TRIM(converrfil), ptop, istatus)
      IF (mp_opt > 0) CALL mpupdatei(istatus, 1)

      IF (istatus/=0) THEN
        nconvfil = 0
        iuseconv = 0
        CALL dealloc_convobs()
        RETURN
      END IF
    END IF

    IF (mp_opt > 0) THEN
      CALL mpupdatei(nconvfil, 1)
      CALL mpupdatei(iuseconv, mx_pass)

      IF (myproc > 0) ALLOCATE(convinfo(200, nvar_conv-5,7))
      IF (myproc > 0) ALLOCATE(errtable(7, 33, 200))
      CALL mpupdater(convinfo, 200*(nvar_conv-5)*7)
      CALL mpupdater(errtable, 7*33*200)
    END IF

    RETURN

  END SUBROUTINE prep_convtbl

  !#####################################################################
  !
  ! Read conventional observations from prepbufr.
  !
  ! Observations are saved in convobs(istn, ivar, flag)
  !
  ! istn    = ith station of observation
  ! ivar    = state variables if available
  !           including (x, y, elev, height, p, pt, qv, u, v, pw, t in C)
  !                     (1, 2, 3,    4,      5, 6,  7,  8, 9, 10, 11)
  ! flag    = flag if it is observation or observation type
  !         = 1 - observation
  !         = 2 - prepbufr observation type (if available)
  !
  !#######################################################################

  SUBROUTINE prepconvobs(datime, nx, ny, xs, ys, nxlg, nylg, xslg, yslg, istatus)

    USE module_read_prepbufr, ONLY: read_prepbufr_obs, dealloc_convraw, &
                                    nstn, nobs, obs, obsdim

    IMPLICIT NONE

    INCLUDE 'mp.inc'

    CHARACTER(LEN=19), INTENT(IN)   :: datime ! In format YYYY-MM-DD_hh:mm:ss
    INTEGER, INTENT(IN)             :: nx, ny, nxlg, nylg
    REAL(P), INTENT(IN)             :: xs(nx), ys(ny), xslg(nxlg), yslg(nylg)

    INTEGER, INTENT(OUT)            :: istatus

    INTEGER                         :: iobs
    REAL(P), ALLOCATABLE            :: xloc_all(:), yloc_all(:)

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (myproc == 0) THEN

      WRITE(6,'(1x,2a)') 'Getting conventional observations from PrepBUFR file: ', TRIM(convfname)

      !observations contained by variable(iobs, nvar_conv) obs are:
      !1. lat  2. lon  3. elev  4. height  5. pressure  6. temperature
      !7. specific humidity  8. u_earth  9. v_earth  10. precipitable water
      !11. BUFR type
      CALL read_prepbufr_obs(TRIM(convfname), rmiss_conv, datime, istatus)

    END IF
    CALL mpupdatei(istatus, 1)
    IF (istatus == -1) THEN
      IF (myproc == 0) WRITE(6,'(1x,a)') 'PrepBUFR file does not exist!'
      RETURN
    END IF

    IF (mp_opt > 0) THEN
      CALL mpupdatei(nobs, 1)
    END IF

    IF (myproc == 0) THEN
      !-----------------------------------------------------------------------
      !Find the x,y location of each station.
      !Note the use of lltoxy requires that map projection
      !be initialized with map parameters. It is assumed here
      !that this has been done already.
      !-----------------------------------------------------------------------
      totalconvobs = 0
      ALLOCATE(xloc_all(nobs))
      ALLOCATE(yloc_all(nobs))
      CALL lltoxy(nobs, 1, obs(:,1,1), obs(:,2,1),xloc_all,yloc_all)
      DO iobs = 1, nobs

        IF (xloc_all(iobs) > xslg(2) .AND. xloc_all(iobs) < xslg(nxlg-2) .AND.    &
            yloc_all(iobs) > yslg(2) .AND. yloc_all(iobs) < yslg(nylg-2)) THEN
          IF ( .NOT. (ANY(obs(iobs, 1:2, 1) == rmiss_conv)) ) THEN
            totalconvobs = totalconvobs + 1
            obs(totalconvobs, 3:nvar_conv, :) = obs(iobs, 3:nvar_conv, :)
            ! Converting earth wind to U- and V-component on particular map projection
            ! before observations are distributed into different processors. Latitude
            ! and longitude are replaced by xs and ys after wind conversion.
            IF ( .NOT. (ANY(obs(iobs, 8:9, 1) == rmiss_conv)) ) THEN
              CALL uvetomp(1, 1, obs(iobs, 8, 1), obs(iobs, 9, 1), obs(iobs, 2, 1),        &
                           obs(totalconvobs, 8, 1), obs(totalconvobs, 9, 1))
              obs(totalconvobs, 8:9, 2) = obs(iobs, 8:9, 2)
            END IF
            obs(totalconvobs, 1, 1) = xloc_all(iobs)
            obs(totalconvobs, 2, 1) = yloc_all(iobs)
            obs(totalconvobs, 1:2, 2) = obs(iobs, 1:2, 2)
          END IF
        END IF

      END DO

      DEALLOCATE(xloc_all, yloc_all)

      WRITE(*,'(1x,(a,I0))') 'INFO: total conventional observations within the domain = ',totalconvobs

    ELSE

      ALLOCATE(obs(nobs,nvar_conv,obsdim))
      obs = rmiss_conv

    END IF

    IF (mp_opt > 0) THEN
      CALL mpupdatei(totalconvobs, 1)
      CALL mpupdater(obs, nobs * nvar_conv * obsdim)
    END IF

    nconvobs = 0
    DO iobs = 1, totalconvobs
      IF (obs(iobs, 1, 1) > xs(1) .AND. obs(iobs, 1, 1) < xs(nx-1) .AND.  &
          obs(iobs, 2, 1) > ys(1) .AND. obs(iobs, 2, 1) < ys(ny-1)) THEN
        nconvobs = nconvobs + 1
      END IF
    END DO

    ALLOCATE(ownuseconv(nconvobs))
    ALLOCATE(convobs(nconvobs, nvar_conv, obsdim))
    ownuseconv = 0
    convobs = rmiss_conv

    !convobs format(iobs, 1:10, 1):
    !1. xs   2. ys   3. elev   4. height   5. pressure   6. temperature
    !7. specific humidity   8. u_map   9. v_map   10. precipitable water
    !(iobs, 1:10, 2):
    !Corresponding bufr type
    !NEED TO MAKE SURE THE UNIT OF PRECIPITABLE WATER(VARIABLE 10)
    nconvobs = 0
    DO iobs = 1, totalconvobs
      IF (obs(iobs, 1, 1) > xs(1) .AND. obs(iobs, 1, 1) < xs(nx-1) .AND.  &
          obs(iobs, 2, 1) > ys(1) .AND. obs(iobs, 2, 1) < ys(ny-1)) THEN

        nconvobs = nconvobs + 1
        IF (obs(iobs, 1, 1) >= xs(2) .AND. obs(iobs, 2, 1) >= ys(2)) THEN
          ownuseconv(nconvobs) = 2
        ELSE
          ownuseconv(nconvobs) = 1
        END IF

        convobs(nconvobs, 1:4, :) = obs(iobs, 1:4, :)
        convobs(nconvobs, 8:nvar_conv, :) = obs(iobs, 8:nvar_conv, :)

        IF (obs(iobs, 5, 1) /= rmiss_conv) THEN
          convobs(nconvobs, 5, 1) = obs(iobs, 5, 1) * 100 ! Convert hPa to Pa
          convobs(nconvobs, 5, 2) = obs(iobs, 5, 2)
        ELSE
          convobs(nconvobs, 5, :) = rmiss_conv
        END IF

        IF (obs(iobs, 6, 1) /= rmiss_conv) THEN
          convobs(nconvobs, 6, 1) = obs(iobs, 6, 1) + 273.15 ! Convert C to K
          convobs(nconvobs, 6, 2) = obs(iobs, 6, 2)
        ELSE
          convobs(nconvobs, 6, :) = rmiss_conv
        END IF

        IF (obs(iobs, 7, 1) /= rmiss_conv) THEN
          convobs(nconvobs, 7, 1) = obs(iobs, 7, 1) * (10. ** (-6)) ! Convert mg/kg to kg/kg
          convobs(nconvobs, 7, 2) = obs(iobs, 7, 2)
        ELSE
          convobs(nconvobs, 7, :) = rmiss_conv
        END IF

      END IF
    END DO

    CALL dealloc_convraw()

    RETURN

  END SUBROUTINE prepconvobs

  !#####################################################################
  !
  ! Innovation of conventional observation conv_innov(iobs, ivar, ichar)
  ! iobs    = index of observation
  ! ivar    = index of observation type
  !         = 1, pressure
  !         = 2, temperature
  !         = 3, specific humidity
  !         = 4, U-wind
  !         = 5, V-wind
  ! ichar   = index of observation characteristics
  !         = 1, observation innovation
  !         = 2, observation error
  !         = 3, use(=1)/own(=2)/not use(=-1)
  !         = 4, error covariance
  !
  !#######################################################################

  SUBROUTINE grdtoconv(nx, ny, nz, xs, ys, nvar, var3d, ps, varsfc)

    USE module_varpara, only: PTR_U, PTR_V, PTR_PT, PTR_QV

    IMPLICIT NONE

    INCLUDE 'mp.inc'

    INTEGER, INTENT(IN)               :: nx, ny, nz, nvar
    REAL(P), INTENT(IN)               :: xs(nx), ys(ny)
    REAL(P), INTENT(IN)               :: var3d(nx, ny, nz, nvar)
    REAL(P), INTENT(IN)               :: ps(nx, ny), varsfc(nx, ny,4)

    INTEGER                           :: iobs, ivar,i ,j, ityp, istatus, invalid, grossfail
    LOGICAL                           :: validobs, qcgross
    REAL(P)                           :: obserr(7, 33), saturated_q, obspress
    REAL(P)                           :: innov, model2obs, error(3)
    REAL(P)                           :: pt2m(nx, ny)

    LOGICAL, ALLOCATABLE              :: useobs(:)

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    grossfail = 0
    invalid = 0
    CALL alloc_convinnov()

    ALLOCATE(useobs(nconvobs))
    ! For now, assuming all obs within the processor domain are used.
    ! Otherwise, useobs need to be set based on ownuseconv(:) if it is necessary.
    useobs = .TRUE.
    CALL map_to_mod2(nx, ny, nconvobs, nconvobs, useobs, xs, ys, convobs(:, 1, 1), convobs(:, 2, 1),      &
                     x_grid, y_grid)
    DEALLOCATE(useobs)

    DO iobs = 1, nconvobs
      DO ivar = 1, 5

        qcgross = .FALSE.
        ityp = convobs(iobs, ivar+4, 2)
        sfctype(iobs) = (ityp>=180 .and. ityp<190) .or. (ityp>=280 .and. ityp<=290) .or.    &
                        (ityp>=192 .and. ityp<=199) .or. (ityp>=292 .and. ityp<=299) .or.   &
                        (ityp==151)
        validobs = (convobs(iobs, ivar+4, 1) /= rmiss_conv)

        IF (validobs) THEN
          ! Interpolation for getting observation error
          ityp = ityp - 99
          obserr = errtable(:, :, ityp)

          IF (sfctype(iobs) .AND. ownuseconv(iobs) > 0) THEN

            CALL interp_error(nx, ny, 1, obserr, iobs, ivar, ityp, ps, error)

            IF (ivar == 1 .AND. convinfo(ityp, E_PS, 3) == 1) THEN
              ! Gross check
              CALL gross_check(nx, ny, 1, ps, iobs, ivar, ityp, error, innov, qcgross)
            END IF

            IF (ivar == 2 .AND. convinfo(ityp, E_T, 3) == 1) THEN
              CALL gross_check(nx, ny, 1, varsfc(:,:,1), iobs, ivar, ityp, error, innov, qcgross)
              IF (qcgross) THEN
                ! If qcgross check has passed, convert temperature innovation
                ! to potential temperature innovation.
                IF (convobs(iobs, 5, 1) /= rmiss_conv) THEN
                  obspress = convobs(iobs, 5, 1)
                ELSE
                  CALL linearint_2df(nx, ny, ps, x_grid(iobs), y_grid(iobs), obspress)
                END IF
                innov = innov * (p0 / obspress) ** rddcp
                error(1) = error(1) * (p0 / obspress) ** rddcp
              END IF
            END IF

            IF (ivar == 3 .AND. convinfo(ityp, E_QV, 3) == 1) THEN
              CALL gross_check(nx, ny, 1, varsfc(:,:,2), iobs, ivar, ityp, error, innov, qcgross)
            END IF

            IF (ivar == 4 .AND. convinfo(ityp, E_UV, 3) == 1) THEN
              CALL gross_check(nx, ny, 1, varsfc(:,:,3), iobs, ivar, ityp, error, innov, qcgross)
            END IF

            IF (ivar == 5 .AND. convinfo(ityp, E_UV, 3) == 1) THEN
              CALL gross_check(nx, ny, 1, varsfc(:,:,4), iobs, ivar, ityp, error, innov, qcgross)
            END IF

          ELSE IF (ownuseconv(iobs) > 0) THEN ! for upper air observation, to be implemented

            !pressure and height = missing, ownuseconv(nconvobs) = -1, convobs(nconvobs, :, :) = rmiss_conv
            !IF (convobs(nconvobs, 4, 1) == rmiss_conv .AND. convobs(nconvobs, 5, 1) == rmiss_conv
            CYCLE

          END IF

          IF (qcgross) THEN
            conv_innov(iobs, ivar, 1) = innov
            conv_innov(iobs, ivar, 2) = error(1)
            conv_innov(iobs, ivar, 3) = ownuseconv(iobs)
            conv_innov(iobs, ivar, 4) = error(1) ** 2
          ELSE
            conv_innov(iobs, ivar, 3) = -1
            grossfail = grossfail + 1
          END IF
        ELSE
          conv_innov(iobs, ivar, 3) = -1
          invalid = invalid + 1
        END IF

      END DO
    END DO
!   stop

    IF (mp_opt > 0) CALL mpsumi(invalid, 1)
    IF (mp_opt > 0) CALL mpsumi(grossfail, 1)
    IF (myproc == 0) WRITE(6,'(1x,(a,I0,a))') 'INFO: ', invalid, ' individual observations do not have a valid value.'
    IF (myproc == 0) WRITE(6,'(1x,(a,I0))') 'INFO: Individual conventional observations abandoned by gross check = ',grossfail

    RETURN

  END SUBROUTINE grdtoconv

  !#####################################################################

  SUBROUTINE convobs_set_usage(myproc,ipass,istatus)

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: myproc,ipass

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    convsw = 0

    IF (ipass > 0) THEN
      IF( nconvfil /= 0 .AND. iuseconv(ipass) > 0 ) THEN
        convsw = 1
      END IF
    ELSE             ! chk_opt == 1, ipass == 0
      IF( nconvfil /= 0 ) THEN
        convsw = 1
      END IF
    END IF

    IF (myproc == 0) THEN
      IF (convsw == 0) THEN
        WRITE(6,'(3x,a,16x,a)') 'Conventional observation',' none'
      ELSE
        WRITE(6,'(3x,a,16x,a)') 'Conventional observation',' Using'
        WRITE(6,'(9x,3a)') '1',' - ','PrepBUFR'
      END IF
      WRITE(6,*) ' '
    END IF

    RETURN
  END SUBROUTINE convobs_set_usage

  !#####################################################################

  SUBROUTINE conv_costf(nx, ny, nz, p_ctr, t_ctr, q_ctr, u_ctr, v_ctr,  &
                        f_ouconv,f_ovconv,f_opconv,f_oqconv,f_otconv)

    IMPLICIT NONE

    INCLUDE 'mp.inc'

    INTEGER, INTENT(IN)       :: nx, ny, nz
    REAL(P), INTENT(IN), DIMENSION(nx, ny, nz)  :: p_ctr, t_ctr, q_ctr, &
                                                   u_ctr, v_ctr

    REAL(DP), INTENT(OUT)     :: f_ouconv,f_ovconv,f_opconv,f_oqconv,f_otconv

    INTEGER                   :: iobs, ivar

    REAL  :: sum11, sum12, sum13, sum14, sum15
    REAL  :: sum21, sum22, sum23, sum24, sum25
    REAL, DIMENSION(:,:,:), ALLOCATABLE  :: p_grd, t_grd, q_grd, u_grd, v_grd, tem
    INTEGER :: i, j, k

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    f_ouconv = 0.0D0
    f_ovconv = 0.0D0
    f_opconv = 0.0D0
    f_otconv = 0.0D0
    f_oqconv = 0.0D0

    bkg_incr = 0.0
    ana_incr = 0.0

    DO iobs = 1, nconvobs

      IF (sfctype(iobs)) THEN

        IF ( conv_innov(iobs, 1, 3) > 0 ) THEN
          CALL linearint_2df(nx, ny, p_ctr(:,:,2), x_grid(iobs), y_grid(iobs), bkg_incr(iobs, 1))
        END IF

        IF ( conv_innov(iobs, 2, 3) > 0 ) THEN
          CALL linearint_2df(nx, ny, t_ctr(:,:,2), x_grid(iobs), y_grid(iobs), bkg_incr(iobs, 2))
        END IF

        IF ( conv_innov(iobs, 3, 3) > 0 ) THEN
          CALL linearint_2df(nx, ny, q_ctr(:,:,2), x_grid(iobs), y_grid(iobs), bkg_incr(iobs, 3))
        END IF

        IF ( conv_innov(iobs, 4, 3) > 0 ) THEN
          CALL linearint_2df(nx, ny, u_ctr(:,:,2), x_grid(iobs), y_grid(iobs), bkg_incr(iobs, 4))
        END IF

        IF ( conv_innov(iobs, 5, 3) > 0 ) THEN
          CALL linearint_2df(nx, ny, v_ctr(:,:,2), x_grid(iobs), y_grid(iobs), bkg_incr(iobs, 5))
        END IF

      END IF

      DO ivar = 1, 5

        IF ( conv_innov(iobs, ivar, 3) > 0 ) THEN
          ana_incr(iobs, ivar) = (bkg_incr(iobs, ivar) - conv_innov(iobs, ivar, 1)) / conv_innov(iobs, ivar, 4)
        END IF

        IF ( conv_innov(iobs, ivar, 3) > 1 ) THEN
          IF (ivar == 1) f_opconv = f_opconv + ana_incr(iobs, ivar) * (bkg_incr(iobs, ivar) - conv_innov(iobs, ivar, 1))
          IF (ivar == 2) f_otconv = f_otconv + ana_incr(iobs, ivar) * (bkg_incr(iobs, ivar) - conv_innov(iobs, ivar, 1))
          IF (ivar == 3) f_oqconv = f_oqconv + ana_incr(iobs, ivar) * (bkg_incr(iobs, ivar) - conv_innov(iobs, ivar, 1))
          IF (ivar == 4) f_ouconv = f_ouconv + ana_incr(iobs, ivar) * (bkg_incr(iobs, ivar) - conv_innov(iobs, ivar, 1))
          IF (ivar == 5) f_ovconv = f_ovconv + ana_incr(iobs, ivar) * (bkg_incr(iobs, ivar) - conv_innov(iobs, ivar, 1))
        END IF

      END DO

    END DO

  END SUBROUTINE conv_costf

  !#####################################################################

  SUBROUTINE conv_gradt(nx, ny, nz, p_grd, t_grd, q_grd, u_grd, v_grd, tem1)

    IMPLICIT NONE

    INCLUDE 'bndry.inc'
    INCLUDE 'mp.inc'

    INTEGER, INTENT(IN)       :: nx, ny, nz
    REAL(P), INTENT(OUT), DIMENSION(nx, ny, nz) :: p_grd, t_grd, q_grd, &
                                                   u_grd, v_grd

    REAL(P), INTENT(OUT)      :: tem1(nx, ny, nz)

    INTEGER                   :: iobs, ivar

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    DO iobs = 1, nconvobs

      IF (sfctype(iobs)) THEN

        IF ( conv_innov(iobs, 1, 3) > 0 ) THEN
          CALL alinearint_2df(nx, ny, p_grd(:,:,2), x_grid(iobs), y_grid(iobs), ana_incr(iobs, 1))
        END IF

        IF ( conv_innov(iobs, 2, 3) > 0 ) THEN
          CALL alinearint_2df(nx, ny, t_grd(:,:,2), x_grid(iobs), y_grid(iobs), ana_incr(iobs, 2))
        END IF

        IF ( conv_innov(iobs, 3, 3) > 0 ) THEN
          CALL alinearint_2df(nx, ny, q_grd(:,:,2), x_grid(iobs), y_grid(iobs), ana_incr(iobs, 3))
        END IF

        IF ( conv_innov(iobs, 4, 3) > 0 ) THEN
          CALL alinearint_2df(nx, ny, u_grd(:,:,2), x_grid(iobs), y_grid(iobs), ana_incr(iobs, 4))
        END IF

        IF ( conv_innov(iobs, 5, 3) > 0 ) THEN
          CALL alinearint_2df(nx, ny, v_grd(:,:,2), x_grid(iobs), y_grid(iobs), ana_incr(iobs, 5))
        END IF

      END IF

    END DO

    IF (mp_opt>0) THEN
      CALL mpsendrecv2dew(p_grd,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(p_grd,nx,ny,nz,nbc,sbc,0,tem1)

      CALL mpsendrecv2dew(t_grd,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(t_grd,nx,ny,nz,nbc,sbc,0,tem1)

      CALL mpsendrecv2dew(q_grd,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(q_grd,nx,ny,nz,nbc,sbc,0,tem1)

      CALL mpsendrecv2dew(u_grd,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(u_grd,nx,ny,nz,nbc,sbc,0,tem1)

      CALL mpsendrecv2dew(v_grd,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(v_grd,nx,ny,nz,nbc,sbc,0,tem1)
    END IF

  END SUBROUTINE conv_gradt

  !#####################################################################
  !
  ! Read GSI conventional info table.
  !
  ! Tested table: nam_regional_convinfo.txt
  !
  ! Table : Conventional observation information file
  !
  ! Only part of columns described below are read and saved in convinfo(itype,ivar,7)
  ! Column #: 1          2          3          4          5          6          7
  ! Content:  type       sub        iuse       twindow    gross      ermax      ermin
  ! Unit:                                      hours
  !
  ! Array pattern for rpttyp(:,6)
  ! where rpttyp only save used type defined in convinfo
  ! Column #: 1          2          3          4          5          6
  ! Content : ps_window  t_window   q_window   uv_window  pw_window  type
  ! Unit    : hours      hours      hours      hours      hours
  !
  ! ivar    = index of observation type (pw, t, uv, q, etc.), for now nvar = 5
  ! itype   = index of prepbufr observation type (if available)
  ! type    = prepbufr observation type (if available)
  ! sub     = prepbufr subtype (not yet available)
  ! iuse    = flag if to use/not use / monitor data
  !         = 1  use data
  !         = 0  do not use data
  !         = -1 monitor data
  ! twindow = time window (+/- hours)
  ! gross   = gross error parameter - gross error
  ! ermax   = gross error parameter - max error
  ! ermin   = gross error parameter - min error
  !
  !#######################################################################

  SUBROUTINE read_convinfo(infile, istatus)

    USE module_read_prepbufr, ONLY: nrpttyp, rpttyp, nvar_conv

    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN)    :: infile
    CHARACTER(LEN=256)              :: infostring, variable
    INTEGER                         :: unit_file, istatus, locsta, i, j
    REAL(P)                         :: varin(18)

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    !Allocate space for convinfo (need to be deallocated after gross check)
    ALLOCATE(convinfo(200,nvar_conv-5,7))
    DO i = 1, nvar_conv-5
      convinfo(:,i,1) = (/(j, j=100, 299, 1)/)
    END DO
    convinfo(:,:,2:7) = rmiss_conv

    CALL getunit(unit_file)
    OPEN(unit_file, FILE = infile, ACTION='READ', FORM='FORMATTED', STATUS='old',IOSTAT=istatus)
    IF (istatus /= 0) THEN
      WRITE(6,'(a)') 'Error in opening convinfo file. No convetional observation will be used.'
      RETURN
    END IF

    istatus = 0
    DO WHILE (istatus == 0)
      READ(unit_file,'(a)',IOSTAT=istatus) infostring
      infostring = TRIM(ADJUSTL(infostring))

      IF (infostring(1:1)/='!') THEN
        locsta = INDEX(infostring, ' ') + 1

        IF (TRIM(infostring(1:2)) == 'ps') THEN
          READ(infostring(locsta:LEN(infostring)), *) varin
          IF (varin(1) >= 100 .AND. varin(1) <= 299) THEN
            IF (varin(3) == 1.0) THEN
              convinfo(INT(varin(1))-99, E_PS, 1:4) = varin(1:4)
              convinfo(INT(varin(1))-99, E_PS, 5:7) = varin(8:10)
            END IF
          ELSE
            WRITE(6,'(A,I0,A)') 'Wrong observation type ',varin(1),' for reading ',TRIM(infostring(1:2)),' from prepbufr.'
            CALL arpsstop('Please check the configuration in convinfofil file.',1)
          END IF
        END IF

        IF (TRIM(infostring(1:2)) == 't') THEN
          READ(infostring(locsta:LEN(infostring)), *) varin
          IF (varin(1) >= 100 .AND. varin(1) <= 299) THEN
            IF (varin(3) == 1.0) THEN
              convinfo(INT(varin(1))-99, E_T, 1:4) = varin(1:4)
              convinfo(INT(varin(1))-99, E_T, 5:7) = varin(8:10)
            END IF
          ELSE
            WRITE(6,'(A,I0,A)') 'Wrong observation type ',varin(1),' for reading ',TRIM(infostring(1:2)),' from prepbufr.'
            CALL arpsstop('Please check the configuration in convinfofil file.',1)
          END IF
        END IF

        IF (TRIM(infostring(1:2)) == 'q') THEN
          READ(infostring(locsta:LEN(infostring)), *) varin
          IF (varin(1) >= 100 .AND. varin(1) <= 299) THEN
            IF (varin(3) == 1.0) THEN
              convinfo(INT(varin(1))-99, E_QV, 1:4) = varin(1:4)
              convinfo(INT(varin(1))-99, E_QV, 5:7) = varin(8:10)
            END IF
          ELSE
            WRITE(6,'(A,I0,A)') 'Wrong observation type ',varin(1),' for reading ',TRIM(infostring(1:2)),' from prepbufr.'
            CALL arpsstop('Please check the configuration in convinfofil file.',1)
          END IF
        END IF

        IF (TRIM(infostring(1:2)) == 'uv') THEN
          READ(infostring(locsta:LEN(infostring)), *) varin
          IF (varin(1) >= 100 .AND. varin(1) <= 299) THEN
            IF (varin(3) == 1.0) THEN
              convinfo(INT(varin(1))-99, E_UV, 1:4) = varin(1:4)
              convinfo(INT(varin(1))-99, E_UV, 5:7) = varin(8:10)
            END IF
          ELSE
            WRITE(6,'(A,I0,A)') 'Wrong observation type ',varin(1),' for reading ',TRIM(infostring(1:2)),' from prepbufr.'
            CALL arpsstop('Please check the configuration in convinfofil file.',1)
          END IF
        END IF

        IF (TRIM(infostring(1:2)) == 'pw') THEN
          READ(infostring(locsta:LEN(infostring)), *) varin
          IF (varin(1) >= 100 .AND. varin(1) <= 299) THEN
            IF (varin(3) == 1.0) THEN
              convinfo(INT(varin(1))-99, E_PW, 1:4) = varin(1:4)
              convinfo(INT(varin(1))-99, E_PW, 5:7) = varin(8:10)
            END IF
          ELSE
            WRITE(6,'(A,I0,A)') 'Wrong observation type ',varin(1),' for reading ',TRIM(infostring(1:2)),' from prepbufr.'
            CALL arpsstop('Please check the configuration in convinfofil file.',1)
          END IF
        END IF
      END IF !Not comment
    END DO ! End of file or Error

    nrpttyp = 0
    DO i = 1, 200
      IF (MAXVAL(convinfo(i, :, 3)) == 1) nrpttyp = nrpttyp + 1
    END DO
    IF (nrpttyp > 0) THEN
      ALLOCATE(rpttyp(nrpttyp, nvar_conv-4))
      nrpttyp = 0
      DO i = 1, 200
        IF (MAXVAL(convinfo(i, :, 3)) == 1) THEN
           nrpttyp = nrpttyp + 1
           rpttyp(nrpttyp, nvar_conv-4) = i + 99.
           rpttyp(nrpttyp, 1:nvar_conv-5) = MAXVAL(convinfo(i, :, 4))
        END IF
      END DO
      istatus = 0
    ELSE
      WRITE(6,'(a)') '0 observation types are selected in convfile. No convetional observation will be used.'
    END IF

    CALL retunit(unit_file)
    RETURN

  END SUBROUTINE read_convinfo

  !#####################################################################
  !
  ! Read GSI conventional observation error table.
  !
  ! Tested table: nam_errtable.r3dv
  !
  ! save error table in errtable(numcol, numrow, numtype)
  ! numtype: observation type (nrpttyp)
  ! numrow:  vertical levels (33)
  ! numcol:  variables (7)
  !
  ! Table 4.10: Description of each column in the observation error table file
  !
  ! Column #: 1          2          3            4     5    6            7
  ! Content:  Pressure   T          RH           UV    Ps   Pw           Log(Pressure)
  ! Unit:     hPa        degree C   percent/10   m/s   mb   kg/m2(or mm) Log(Pa)
  !
  ! For each observation type, the error table has six columns and 33 rows (levels).
  ! The 1st column prescribes 33 pressure levels, covering 1100 hPa to 0 hPa.
  ! Columns 2-6 prescribe observation errors for temperature (T),
  ! relative humidity (RH), horizontal wind component (UV),
  ! surface pressure (Ps), and the total column precipitable water (Pw).
  ! The missing value is 0.10000E+10.
  !
  !#######################################################################

  SUBROUTINE read_errtable(infile, ptop, istatus)

    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN)    :: infile
    REAL, INTENT(IN)                :: ptop
    CHARACTER(LEN=80)               :: mydummy
    INTEGER                         :: unit_file, istatus
    INTEGER                         :: ityp, mytype, ivar, ilev

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0
    !Allocate space for storing
    ALLOCATE(errtable(7,33,200))
    errtable = 0.10000E+10

    CALL getunit(unit_file)
    OPEN(unit_file,FILE=trim(infile), ACTION='READ', FORM='FORMATTED', STATUS='old',IOSTAT=istatus)
    IF (istatus /= 0) THEN
      WRITE(6,'(a)') 'Error in opening error table file. No convetional observation will be used.'
      RETURN
    END IF

    DO ityp = 1, 200
      READ(unit_file,'(I4,a)', END=700, ERR=800, IOSTAT= istatus) mytype, mydummy
      READ(unit_file, *, END=700, ERR=800, IOSTAT= istatus) errtable(1:6, :, mytype-99)
      DO ivar = 1, 6
        DO ilev = 1, 33
          IF (errtable(ivar, ilev, mytype-99) > 9.0E08) THEN
            errtable(ivar, ilev, mytype-99) = rmiss_conv
          END IF
        END DO
      END DO
      800 CONTINUE
    END DO

    700 CONTINUE

    CLOSE(unit_file)
    CALL retunit(unit_file)

    ! Create log(pressure) column for linear interpolation
    errtable(1, :, :) = errtable(1, :, :) * 100
    errtable(7, 1:32, :) = -1.0 * LOG(errtable(1, 1:32, :))
    errtable(7, 33, :) = -1.0 * LOG(ptop)

    RETURN

  END SUBROUTINE read_errtable

  !#######################################################################
  !
  ! Interpolate observation errors from prescribed pressure level
  ! to observed pressure.
  !
  ! error(:) = (1) interpolated observation error
  !          = (2) Max error read from convinfo file for gross check
  !          = (3) Min error read from convinfo file for gross check
  !
  ! ityp is prepbufr observation type - 99.
  !
  !#######################################################################

  SUBROUTINE interp_error(nx, ny, nz, errtable, iobs, ivar, ityp, pressure, error)

    IMPLICIT NONE

    INTEGER, INTENT(IN)           :: nx, ny, nz
    REAL(P), INTENT(IN)           :: pressure(nx, ny, nz)
    REAL(P), INTENT(IN)           :: errtable(7, 33)
    INTEGER, INTENT(IN)           :: iobs, ivar, ityp

    REAL(P), INTENT(OUT)          :: error(3)

    INTEGER                       :: ivar_tbl, ivar_info, ilev, nlev
    REAL(P)                       :: obspress, obsheight, saturated_q

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    error = rmiss_conv
    obspress = convobs(iobs, 5, 1)
    obsheight = convobs(iobs, 4, 1)

    IF (obspress == rmiss_conv) THEN
      IF (nz == 1) THEN
        CALL linearint_2df(nx, ny, pressure, x_grid(iobs), y_grid(iobs), obspress)
      ELSE
        RETURN
      END IF
    END IF

    IF (ivar == 1) THEN
      ivar_tbl = 5
    ELSE IF ((ivar >= 2 .AND. ivar <= 4) .OR. ivar == 6) THEN
      ivar_tbl = ivar
    ELSE IF (ivar == 5) THEN
      ivar_tbl = 4
    END IF

    IF (ivar <= 3) ivar_info = ivar
    IF (ivar == 4 .OR. ivar == 5) ivar_info = E_UV
    IF (ivar == 6) ivar_info = E_PW

    nlev = 33
    DO ilev = 1, 33
      IF (errtable(ivar_tbl, ilev) == rmiss_conv) THEN
        nlev = ilev
      END IF
    END DO

    IF (-1 * LOG(obspress) <= errtable(7, 1)) THEN
      error(1) = errtable(ivar_tbl, 1)
    ELSE IF (-1 * LOG(obspress) >= errtable(7, nlev)) THEN
      error(1) = errtable(ivar_tbl, nlev)
    ELSE IF (-1 * LOG(obspress) >= errtable(7, nlev-1)) THEN
      error(1) = errtable(ivar_tbl, nlev-1)
    ELSE
      ! Interpolate observation error from specific height in pressure to the pressure of observation (-log(pressure))
      CALL linear(nlev, errtable(7, 1:nlev), errtable(ivar_tbl, 1:nlev), -1 * LOG(obspress), error(1))
    END IF

    ! Convert unit of errors to that we used in analysis
    IF (ivar_tbl == 3) THEN   ! Need to convert error for relative humidity to that for specific humidity
      IF (error(1) /= rmiss_conv .AND. convobs(iobs, 6, 1) /= rmiss_conv) THEN
        ! Calculate saturation specific humidity based on observed temperature and pressure.
        CALL calc_satq_cc(obspress, convobs(iobs, 6, 1) - 273.15, saturated_q)
        ! The unit of observation error for q is percent/10
        ! So, we have to convert it to value for specific humidity
        ! Err_sq = saturated_q * conv_innov(i, j, 2) * 0.1
        error(1) = saturated_q * error(1) * 0.1
        error(2) = saturated_q * convinfo(ityp,ivar_info,6) * 0.01       ! Unit of q error in convinfo is percent instead of percent/10
        error(3) = saturated_q * convinfo(ityp,ivar_info,7) * 0.01
      ELSE
        error(1) = rmiss_conv
      END IF
    ELSE IF (ivar_tbl == 5) THEN
      ! Convert error for surface pressure in unit of hPa to that in unit of Pa
      IF (error(1) /= rmiss_conv) THEN
        error(1) = error(1) * 100
        error(2) = convinfo(ityp,ivar_info,6) * 100
        error(3) = convinfo(ityp,ivar_info,7) * 100
      END IF
    ELSE IF (ivar_tbl == 6) THEN
      ! Convert error for PW in unit of mm to that in unit of cm
      IF (error(1) /= rmiss_conv) THEN
        error(1) = error(1) * 0.1
        error(2) = convinfo(ityp,ivar_info,6) * 0.1
        error(3) = convinfo(ityp,ivar_info,7) * 0.1
      END IF
    ELSE
      IF (error(1) /= rmiss_conv) THEN
        error(2) = convinfo(ityp,ivar_info,6)
        error(3) = convinfo(ityp,ivar_info,7)
      END IF
    END IF

    RETURN

  END SUBROUTINE interp_error

  !#######################################################################

  SUBROUTINE gross_check(nx, ny, nz, modelstate, iobs, ivar, ityp, error, innov, qcgross)

    IMPLICIT NONE

    INTEGER, INTENT(IN)           :: nx, ny, nz, iobs, ivar, ityp
    REAL(P), INTENT(IN)           :: modelstate(nx, ny, nz)

    REAL(P), INTENT(INOUT)        :: error(3)
    REAL(P), INTENT(OUT)          :: innov
    LOGICAL, INTENT(OUT)          :: qcgross

    REAL(P)                       :: model2obs, ratio, errgross
    INTEGER                       :: ivar_info

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    innov = rmiss_conv
    qcgross = .FALSE.
    IF (error(1) == rmiss_conv) RETURN

    IF (ivar <= 3) ivar_info = ivar
    IF (ivar == 4 .OR. ivar == 5) ivar_info = E_UV
    IF (ivar == 6) ivar_info = E_PW

    ! Interpolation for getting simulated observation at observation space
    IF (nz == 1) THEN
      CALL linearint_2df(nx, ny, modelstate, x_grid(iobs), y_grid(iobs), model2obs)
    ELSE IF (nz > 1) THEN
      !CALL linearint_3d()
      RETURN
    END IF

    ! Calculate innovation
    innov = convobs(iobs, ivar+4, 1) - model2obs
    ! Calculate error for gross check
    errgross = MAX(error(3), MIN(error(2), error(1)))
    ! Calculate ratio for gross check
    ratio = ABS(innov) / errgross

    ! Compare ratio with gross value
    IF (ratio <= convinfo(ityp,ivar_info,5)) qcgross = .TRUE.

    RETURN

  END SUBROUTINE gross_check

  !#######################################################################

  SUBROUTINE alloc_convinnov()

    IMPLICIT NONE

    INTEGER                         :: istatus

    ALLOCATE(x_grid(nconvobs),   STAT = istatus)
    CALL check_alloc_status(istatus, "x_grid_convinnov")

    ALLOCATE(y_grid(nconvobs),   STAT = istatus)
    CALL check_alloc_status(istatus, "y_grid_convinnov")

    ALLOCATE(sfctype(nconvobs),  STAT = istatus)
    CALL check_alloc_status(istatus, "sfctype_convinnov")

    ALLOCATE(bkg_incr(nconvobs, 5), STAT = istatus)
    CALL check_alloc_status(istatus, "bkg_incr_convinnov")

    ALLOCATE(ana_incr(nconvobs, 5), STAT = istatus)
    CALL check_alloc_status(istatus, "ana_incr_convinnov")

    ALLOCATE(conv_innov(nconvobs, 5, 4), STAT = istatus)
    CALL check_alloc_status(istatus, "conv_innov_convinnov")

    x_grid = 0.0
    y_grid = 0.0
    bkg_incr = 0.0
    ana_incr = 0.0
    conv_innov(:, :, 1:2) = rmiss_conv
    conv_innov(:, :, 3) = -1
    conv_innov(:, :, 4) = rmiss_conv
    sfctype = .FALSE.

    RETURN

  END SUBROUTINE alloc_convinnov

  !#######################################################################

  SUBROUTINE dealloc_convinnov()

    IMPLICIT NONE

    IF (ALLOCATED(x_grid))     DEALLOCATE(x_grid)
    IF (ALLOCATED(y_grid))     DEALLOCATE(y_grid)
    IF (ALLOCATED(sfctype))    DEALLOCATE(sfctype)
    IF (ALLOCATED(bkg_incr))   DEALLOCATE(bkg_incr)
    IF (ALLOCATED(ana_incr))   DEALLOCATE(ana_incr)
    IF (ALLOCATED(conv_innov)) DEALLOCATE(conv_innov)

    RETURN

  END SUBROUTINE dealloc_convinnov

  !#######################################################################

  SUBROUTINE dealloc_convobs()

    USE module_read_prepbufr, ONLY : dealloc_convraw

    IMPLICIT NONE

    IF (ALLOCATED(ownuseconv)) DEALLOCATE(ownuseconv)
    IF (ALLOCATED(convobs)) DEALLOCATE(convobs)
    IF (ALLOCATED(convinfo)) DEALLOCATE(convinfo)
    IF (ALLOCATED(errtable)) DEALLOCATE(errtable)

    CALL dealloc_convinnov()

    CALL dealloc_convraw()

    RETURN

  END SUBROUTINE dealloc_convobs

END MODULE module_convobs
