!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE RDMPUVW0                   ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE rdmpuvw0(nx,ny,nz,                                       &
           u,v,w, ubar,vbar,rhostr, zp,                             &
           umix,vmix,wmix, rdmp)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Apply Rayleigh sponge to w, and to perturbations of u and v in the
!  momentum equations. The Rayleigh damping terms are then added to
!  arrays umix, vmix and wmix.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  4/21/1992.
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/2/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  2/10/93 (K. Droegemeier)
!  Cleaned up documentation.
!
!  9/23/93 (M. Xue)
!  Rayleigh damping was extended to include the points on the lateral
!  boundaries. These values will be used in the case of radiation
!  lateral boundary conditions, where prognostic equations are
!  integrated on the y boundaries for u, x boundaries for v and
!  all four lateral boundaries for w and scalar (except for pprt)
!  variables.
!
!  9/10/94 (D. Weber & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical direction.
!
!    u        x component of velocity (m/s)
!    v        y component of velocity (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates (m/s).
!
!    ubar     Base state zonal velocity component (m/s)
!    vbar     Base state meridional velocity component (m/s)
!    rhostr   Base state density times j3 (kg/m**3)
!
!    zp       Vertical coordinate of grid points in physical space(m)
!
!    umix     Array containing turbulent and computational
!             mixing on u
!    vmix     Array containing turbulent and computational
!             mixing on v
!    wmix     Array containing turbulent and computational
!             mixing on w
!
!  OUTPUT:
!
!    umix     Total mixing including, Rayleigh damping on u
!    vmix     Total mixing including, Rayleigh damping on v
!    wmix     Total mixing including, Rayleigh damping on w
!
!  WORK ARRAYS:
!
!    rdmp     Rayleigh damping coefficient.
!             A temporary array defined locally.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)

  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: rhostr(nx,ny,nz)     ! Base state air density times j3
                               ! (kg/m**3)

  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined
                               ! at w-point of the staggered grid.

  REAL :: umix  (nx,ny,nz)     ! Total mixing in u-eq.
  REAL :: vmix  (nx,ny,nz)     ! Total mixing in v-eq.
  REAL :: wmix  (nx,ny,nz)     ! Total mixing in w-eq.

  REAL :: rdmp  (nx,ny,nz)     ! Rayleigh damping coefficient.
                               ! Temporary array defined locally.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
  REAL :: pi, zpmax
  INTEGER :: klowest
  SAVE klowest

  DATA klowest/-1/
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF( raydmp == 0 .OR. cfrdmp == 0.0 ) RETURN

  IF( zbrdmp > zp(2,2,nz-1) ) RETURN

  IF (klowest <= 0) THEN
!
!-----------------------------------------------------------------------
!
!  Find the lowest model layer (index klowest) that is entirely or
!  partially contained in the specified Rayleigh damping (sponge)
!  layers.
!
!  The Rayleigh damping is then applied only to layers with
!  k greater than or equal to klowest.
!
!-----------------------------------------------------------------------
!
    klowest = nz-1

    DO k=nz-1,2,-1

      zpmax = zp(1,1,k)

      DO j=1,ny-1
        DO i=1,nx-1
          zpmax = MAX( zp(i,j,k), zpmax )
        END DO
      END DO

      IF( zpmax < zbrdmp ) THEN
        klowest = MAX(2, k+1)
        EXIT
      END IF

    END DO
    120     CONTINUE

  END IF

!
!-----------------------------------------------------------------------
!
!  Calculate the Rayleigh damping coefficient defined at a w-point.
!
!-----------------------------------------------------------------------
!
  pi = 4.0*ATAN( 1.0 )

  DO k= klowest,nz
    DO j=1,ny-1
      DO i=1,nx-1

        rdmp(i,j,k) = cfrdmp*                                           &
            (1.0-COS(pi*MIN(1.0,(zp(i,j,k)-zbrdmp)/(zp(i,j,nz-1)-zbrdmp)) &
            ))*0.5

      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Calculate the Rayleigh damping term in the u-momentum equation and
!  add to array umix.
!
!-----------------------------------------------------------------------
!
  DO k=klowest,nz-2
    DO j=1,ny-1
      DO i=2,nx-1

        umix(i,j,k)=umix(i,j,k) -                                       &
            (rdmp(i,j,k)+rdmp(i-1,j,k)+rdmp(i,j,k+1)+rdmp(i-1,j,k+1))*0.25* &
            (rhostr(i,j,k)+rhostr(i-1,j,k))*0.5*                        &
            (u(i,j,k)-ubar(i,j,k))

      END DO
    END DO
  END DO

!
!-----------------------------------------------------------------------
!
!  Calculate the Rayleigh damping term in the v-momentum equation and
!  add to array vmix.
!
!-----------------------------------------------------------------------
!
  DO k=klowest,nz-2
    DO j=2,ny-1
      DO i=1,nx-1

        vmix(i,j,k)=vmix(i,j,k) -                                       &
            (rdmp(i,j,k)+rdmp(i,j-1,k)+rdmp(i,j,k+1)+rdmp(i,j-1,k+1))*0.25* &
            (rhostr(i,j,k)+rhostr(i,j-1,k))*0.5*                        &
            (v(i,j,k)-vbar(i,j,k))

      END DO
    END DO
  END DO

!
!-----------------------------------------------------------------------
!
!  Calculate the Rayleigh damping term in the w-momentum equation and
!  add to array wmix.
!
!-----------------------------------------------------------------------
!
  DO k=klowest,nz-1
    DO j=1,ny-1
      DO i=1,nx-1

        wmix(i,j,k)=wmix(i,j,k) - rdmp(i,j,k)*                          &
                    (rhostr(i,j,k)+rhostr(i,j,k-1))*0.5*                &
                    w(i,j,k)

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE rdmpuvw0

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE RDMPPT0                    ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE rdmppt0(nx,ny,nz,                                            &
           ptprt ,rhostr, zp,                                           &
           ptmix, rdmp)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Apply Rayleigh sponge to the perturbation potential temperature,
!  and accumulate the results in array ptmix.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  4/21/1992.
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/2/92 (M. Xue and H. Jin)
!  Further facelift.
!
!  2/10/93 (K. Droegemeier)
!  Cleaned up documentation.
!
!  9/23/93 (MX)
!  Rayleigh damping was extended to include the points on the lateral
!  boundaries. These values will be used in the case of radiation
!  lateral boundary conditions, where prognostic equations are
!  integrated on the y boundaries for u, x boundaries for v and
!  all four lateral boundaries for w and scalar (except for pprt)
!  variables.
!
!  9/1/94 (D. Weber & Y. Lu)
!  Cleaned up documentation
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical direction.
!
!    ptprt    Perturbation potential temperature (K)
!
!    rhostr   Base state air density times j3 (kg/m**3)
!
!    zp       Vertical coordinate of grid points in physical space
!             (m)
!
!  OUTPUT:
!
!    ptmix    Total mixing in potential temperature equation.
!
!
!  WORK ARRAYS:
!
!    rdmp     Rayleigh damping coefficient.
!             A temporary array defined locally.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: ptprt (nx,ny,nz)     ! Perturbation potential temperature (K)
  REAL :: rhostr(nx,ny,nz)     ! Base state air density times j3
                               ! (kg/m**3)
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined
                               ! at w-point of the staggered grid.

  REAL :: ptmix (nx,ny,nz)     ! Total mixing on potential temperature

  REAL :: rdmp  (nx,ny,nz)     ! Rayleigh damping coefficient.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
  INTEGER :: klowest
  REAL :: pi, zpmax
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF( raydmp == 0 .OR. cfrdmp == 0.0 ) RETURN
!
!-----------------------------------------------------------------------
!
!  Find the lowest model layer (index klowest) that is entirely or
!  partially contained in the Rayleigh damping (sponge) layers.
!
!  The Rayleigh damping is then only applied to layers with
!  k greater than or equal to klowest.
!
!-----------------------------------------------------------------------
!
  klowest = nz-1

  DO k=nz-1,2,-1

    zpmax = zp(1,1,k)

    DO j=1,ny-1
      DO i=1,nx-1
        zpmax = MAX( zp(i,j,k), zpmax )
      END DO
    END DO

    IF( zpmax < zbrdmp ) THEN

      klowest = MAX(2, k+1)
      EXIT

    END IF

  END DO

  120   CONTINUE

!
!-----------------------------------------------------------------------
!
!  Calculate the Rayleigh damping coefficient defined at a w-point.
!
!-----------------------------------------------------------------------
!
  pi = 4.0*ATAN( 1.0 )

  DO k= klowest,nz
    DO j=1,ny-1
      DO i=1,nx-1

        rdmp(i,j,k) = cfrdmp*                                           &
            (1.0-COS(pi*MIN(1.0,(zp(i,j,k)-zbrdmp)/(zp(i,j,nz-1)-zbrdmp)) &
            ))*0.5


      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Calculate the Rayleigh damping term for the perturbation potential
!  temperature equation and add to array ptmix.
!
!-----------------------------------------------------------------------
!
  DO k=klowest,nz-2
    DO j=1,ny-1
      DO i=1,nx-1

        ptmix(i,j,k)=ptmix(i,j,k) -                                     &
                    (rdmp(i,j,k)+rdmp(i,j,k+1))*0.5*                    &
                     rhostr(i,j,k)*ptprt(i,j,k)

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE rdmppt0
