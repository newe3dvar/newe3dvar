!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE LINEARINT_2DF              ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  linear interpolation in 2D.
!
!-----------------------------------------------------------------------
!
!  AUTHOR:
!
!  Jidong Gao, CAPS, July, 2000
!
!-----------------------------------------------------------------------
!
!  MODIFICATION HISTORY:
!
!  02/18/2014 (Y. Wang)
!  Changed condition checking indices to include boundary row (ny-1) and
!  column (nx-1). Code modified to follow ARPS conventions.
!
!-----------------------------------------------------------------------
!
SUBROUTINE  linearint_2df(nx,ny,vbl2,pxx,pyy,pval)

  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: nx, ny
  REAL,             INTENT(IN)  :: vbl2(nx,ny)
  !REAL    :: pxx, pyy
  DOUBLE PRECISION, INTENT(IN)  :: pxx, pyy
  REAL,             INTENT(OUT) :: pval

!-----------------------------------------------------------------------

  INTEGER :: i, j
  REAL    :: deltadx,deltady,deltadxm,deltadym

  INCLUDE 'mp.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  IF ( ABS(pxx-1.0) < 1.0E-3 ) THEN
    i = 1
    deltadx = 0.0
  ELSE
    i = FLOOR(pxx)
    deltadx = pxx - i
  END IF

  IF ( ABS(pyy-1.0) < 1.0E-3 ) THEN
    j = 1
    deltady = 0.0
  ELSE
    j = FLOOR(pyy)
    deltady = pyy - j
  END IF
  !
  !print*,'ij=',i,j,' pxx=',pxx,pyy
  !
  IF((i > 0) .AND. (i < nx-1)  .AND. (j > 0) .AND. (j < ny-1)) THEN

    !deltadx = pxx - i
    !deltady = pyy - j

    deltadxm= 1. - deltadx
    deltadym= 1. - deltady

    pval =    deltadxm*deltadym * vbl2(i,  j  )                         &
            + deltadx *deltadym * vbl2(i+1,j  )                         &
            + deltadxm*deltady  * vbl2(i,  j+1)                         &
            + deltadx *deltady  * vbl2(i+1,j+1)
  ELSE IF ( i == nx-1 .AND. loc_x < nproc_x) THEN
    ! Next process should pick up the calculation
    ! do nothing
  ELSE IF ( j == ny-1 .AND. loc_y < nproc_y) THEN
    ! Next process should pick up the calculation
    ! do nothing
  ELSE

    WRITE (6,'(a)')          ' WARNING: '
    WRITE (6,'(2(a,f10.2))') ' pxx = ',pxx,' pyy = ',pyy
    WRITE (6,'(a,/)')        ' no interpolation was performed'

  END IF

  RETURN
END SUBROUTINE  linearint_2df
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ALINEARINT_2DF             ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE alinearint_2df(nx, ny, vbl2, pxx, pyy, pval)
!
  IMPLICIT NONE

  INTEGER,          INTENT(IN)    :: nx, ny
  REAL,             INTENT(INOUT) :: vbl2 (nx,ny)
  !REAL    :: pxx, pyy
  DOUBLE PRECISION, INTENT(IN)    :: pxx, pyy
  REAL,             INTENT(INOUT) :: pval

!-----------------------------------------------------------------------

  INTEGER :: i, j
  REAL    :: deltadx,deltady,deltadxm,deltadym

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  i = FLOOR (pxx)
  j = FLOOR (pyy)

  IF ((i > 0) .AND. (i < nx-1)  .AND. (j > 0) .AND. (j < ny-1) ) THEN

    deltadx = pxx - i
    deltady = pyy - j

    deltadxm= 1.  - deltadx
    deltadym= 1.  - deltady

    vbl2(i+1,j+1)=vbl2(i+1,j+1) + deltadx*deltady *pval
    vbl2(i  ,j+1)=vbl2(i  ,j+1) + deltadxm*deltady*pval
    vbl2(i+1,j  )=vbl2(i+1,j  ) + deltadx*deltadym*pval
    vbl2(i  ,j  )=vbl2(i  ,j  ) + deltadxm*deltadym*pval
    pval = 0.

  ELSE
    !WRITE (0,*) 'WARNING: no interpolation was performed'
  END IF

  RETURN
END SUBROUTINE alinearint_2df
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE MAP_TO_MOD2                ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE map_to_mod2(nx,ny,mxobs,nobs,useobs,pgx,pgy,px,py,pxx,pyy)

  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: nx, ny, nobs, mxobs
  LOGICAL,          INTENT(IN)  :: useobs(mxobs)
  REAL,             INTENT(IN)  :: pgx(nx), pgy(ny)
  REAL,             INTENT(IN)  :: px(mxobs), py(mxobs)
  !REAL,             INTENT(OUT) :: pxx(mxobs),pyy(mxobs)
  DOUBLE PRECISION, INTENT(OUT) :: pxx(mxobs),pyy(mxobs)

!-----------------------------------------------------------------------

  REAL, PARAMETER :: epislon = 1.0E-3
  REAL :: dxmin, dymin

  INTEGER :: i, j, n
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  pxx = -99999.
  pyy = -99999.

  dxmin = (pgx(3)-pgx(2))*epislon
  dymin = (pgy(3)-pgy(2))*epislon

  obs_loop: DO n = 1, nobs

    IF ( useobs(n) ) THEN

      DO  j=1,ny-1
        DO  i=1,nx-1
          IF( (px(n) > pgx(i)-dxmin) .AND. (px(n) < pgx(i+1)) .AND.          &
              (py(n) > pgy(j)-dymin) .AND. (py(n) < pgy(j+1)) ) THEN

             !pxx(n) = FLOAT(i)+ ( px(n)-pgx(i) )/( pgx(i+1)-pgx(i) )
             !pyy(n) = FLOAT(j)+ ( py(n)-pgy(j) )/( pgy(j+1)-pgy(j) )
             !pxx(n) = i + ( px(n)-pgx(i) )/( pgx(i+1)-pgx(i) )
             !pyy(n) = j + ( py(n)-pgy(j) )/( pgy(j+1)-pgy(j) )
             pxx(n) = DBLE(i) + ( DBLE(px(n))-DBLE(pgx(i)) )/( DBLE(pgx(i+1))-DBLE(pgx(i)) )
             pyy(n) = DBLE(j) + ( DBLE(py(n))-DBLE(pgy(j)) )/( DBLE(pgy(j+1))-DBLE(pgy(j)) )

             CYCLE obs_loop
          END IF
        END DO
      END DO

    END IF
  END DO obs_loop

  RETURN
END SUBROUTINE map_to_mod2
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE MAP_TO_MODZ                ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE map_to_modz(nzk,mxobs,nlev,nobs,nx,ny,nz,pgz,                &
                       pxx, pyy, hgt, ihgt, pz1, pz2)

  IMPLICIT NONE

  INTEGER,          INTENT(IN) :: nzk,mxobs,nobs
  INTEGER,          INTENT(IN) :: nx,ny,nz
  INTEGER,          INTENT(IN) :: nlev(mxobs)
  REAL,             INTENT(IN) :: pgz(nx,ny,nz)
  !REAL    :: pxx(mxobs), pyy(mxobs)
  DOUBLE PRECISION, INTENT(IN) :: pxx(mxobs), pyy(mxobs)
  REAL,             INTENT(IN) :: hgt(nzk,mxobs)
  INTEGER,          INTENT(OUT)   :: ihgt(nzk,mxobs)
  REAL,             INTENT(INOUT) :: pz1(nzk,mxobs),pz2(nzk,mxobs)

!-----------------------------------------------------------------------

  INTEGER :: k,ii,kk

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  ihgt = -1
  obs_loop: DO ii = 1,nobs
  obslvl_loop:  DO kk = 1, nlev(ii)

      IF( pxx(ii) < 0.0 .OR. pyy(ii) < 0.0) CYCLE obs_loop

      DO k = 1, nz-1
        CALL linearint_2df(nx,ny,pgz(1,1,k  ),                          &
                           pxx(ii),pyy(ii),pz1(kk,ii) )
        CALL linearint_2df(nx,ny,pgz(1,1,k+1),                          &
                           pxx(ii),pyy(ii),pz2(kk,ii) )

        IF(  hgt(kk,ii) <= pz1(kk,ii) ) THEN
          ihgt(kk,ii) = 0
          CYCLE obslvl_loop
        ELSE IF( (hgt(kk,ii) > pz1(kk,ii) ) .AND.                       &
                 (hgt(kk,ii)<= pz2(kk,ii))) THEN
          ihgt(kk,ii) = k
          CYCLE obslvl_loop
        END IF
      END DO

    END DO obslvl_loop
  END DO obs_loop

  RETURN
END SUBROUTINE map_to_modz
