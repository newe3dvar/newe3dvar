!
!##################################################################
!##################################################################
!######                                                      ######
!######                 SUBROUTINE LINEAR                    ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!
!  Linear interplation in one direction.
!
!
!-----------------------------------------------------------------------
!
!  AUTHOR:
!
!  Jidong Gao, CAPS, July, 2000
!
!-----------------------------------------------------------------------
!
SUBROUTINE linear(n,xx,yy,u,f)

  USE model_precision

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: n
  REAL(P), INTENT(IN)  :: xx(n),yy(n),u
  REAL(P), INTENT(OUT) :: f
!-----------------------------------------------------------------------

  REAL(P) :: a1, a2
  INTEGER :: i

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  IF( u <= xx(1)) THEN

    a1=( u-xx(2))/(xx(1)-xx(2))
    a2=( u-xx(1))/(xx(2)-xx(1))
    f=a1*yy(1)+a2*yy(2)

  ELSE

    DO i=2,n
      IF( u <= xx(i) )  EXIT
    END DO
    i = MIN(i,n)

    a1=( u-xx(i)  )/(xx(i-1)-xx(i))
    a2=( u-xx(i-1))/(xx(i)-xx(i-1))
    f=a1*yy(i-1)+a2*yy(i)

  END IF

  RETURN
END SUBROUTINE linear

SUBROUTINE TbasedInterp(nx, ny, nz, alpha, T, TlowThresh, ThighThresh,  &
                        ElowThresh, EhighThresh, ErrOut)
    
  USE model_precision
  
  IMPLICIT NONE
  
  INTEGER, INTENT(IN)   :: nx, ny, nz
  REAL(P), INTENT(IN)   :: alpha, T(nx,ny,nz)
  REAL(P), INTENT(IN)   :: TlowThresh, ThighThresh, ElowThresh, EhighThresh
  REAL(P), INTENT(OUT)  :: ErrOut(nx,ny,nz)
!-----------------------------------------------------------------------

  INTEGER               :: i, j, k
  REAL(P)               :: Tdiff, beta
  
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  Tdiff = ThighThresh - TlowThresh
  DO j = 1, ny
    DO i = 1, nx
      DO k = 1, nz
        IF (T(i,j,k) >= TlowThresh) THEN
          ErrOut(i,j,k) = ElowThresh
        ELSE IF(T(i,j,k) <= ThighThresh) THEN
          ErrOut(i,j,k) = EhighThresh
        ELSE
          !beta = TANH(2 * alpha * (2 * T(i,j,k) - Tdiff - 1) / (1 - Tdiff))  &
          !                / TANH(2 * alpha)
          !ErrOut(i,j,k) = (1 - beta) * EhighThresh / 2 + (1 + beta) * ElowThresh / 2
          ErrOut(i,j,k) = ElowThresh + (EhighThresh - ElowThresh) *     &
                          (T(i,j,k) - TlowThresh) / (ThighThresh - TlowThresh)
          IF (ErrOut(i,j,k) < 1E-6) ErrOut(i,j,k) = 0.0
        END IF
      END DO
    END DO
  END DO
  
  RETURN
END SUBROUTINE TbasedInterp