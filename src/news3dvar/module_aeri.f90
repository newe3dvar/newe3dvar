MODULE module_aeri
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%
!%%%% This module handle assimilation of the thermodynamic profiles from
!%%%% Atmospheric Emitted Radiance Interferometer (AERI) downwelling
!%%%% spectral radiance observations.
!%%%%
!%%%% AERI is a ground-based instrument that measures the downwelling
!%%%% infrared radiance from the Earths atmosphere. The observations
!%%%% have broad spectral content and sufficient spectral resolution to
!%%%% discriminate among gaseous emitters (e.g., carbon dioxide and
!%%%% water vapor) and suspended matter (e.g., aerosols, water droplets,
!%%%% and ice crystals). These upward-looking surface observations can
!%%%% be used to obtain vertical profiles of tropospheric temperature
!%%%% and water vapor, as well as measurements of trace gases (e.g.,
!%%%% ozone, carbon monoxide, and methane) and downwelling infrared
!%%%% spectral signatures of clouds and aerosols.
!%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!-----------------------------------------------------------------------
!
!  AUTHOR:
!
!  Junjun Hu, 07/17/2017
!
!-----------------------------------------------------------------------
!
!  MODIFICATIONS:
!
!  Y. Wang (07/17/2017)
!  Modified to follow NEWS3DVAR coding standards and made the code
!  MPI parallel valid.
!
!-----------------------------------------------------------------------

  USE model_precision

  IMPLICIT NONE
  PRIVATE

  REAL,    PARAMETER   :: t0c = 273.15
  INTEGER, PARAMETER   :: mxaeri = 6 ! the maximum number of obs stations
  INTEGER, PARAMETER   :: nzk = 55 ! the maximum number of zlevs at each station

  INTEGER             :: aeri_opt
  CHARACTER(LEN=256)  :: aeri_file
  INTEGER, DIMENSION (:), ALLOCATABLE :: iuseaeri

  INTEGER :: No4t  ! number of available temperature observations
  INTEGER, DIMENSION (:),     ALLOCATABLE :: nlev4t
  REAL,    DIMENSION (:),     ALLOCATABLE :: lon_ob4t
  REAL,    DIMENSION (:),     ALLOCATABLE :: lat_ob4t
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: z_ob4t
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: p_ob4t
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: ta_ob4t
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: pt_ob4t
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: toe_ob4t
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: elev_ob4t
  REAL,    DIMENSION (:),     ALLOCATABLE :: pxx_ob4t
  REAL,    DIMENSION (:),     ALLOCATABLE :: pyy_ob4t
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: hgt_ob4t, ihgt_ob4t, pz1_ob4t, pz2_ob4t
  LOGICAL, DIMENSION (:),     ALLOCATABLE :: useobs4t
  LOGICAL, DIMENSION (:),     ALLOCATABLE :: useaeri4t   ! dynamic with ipass


  INTEGER :: No4qv  ! number of available qv observations
  INTEGER, DIMENSION (:),     ALLOCATABLE :: nlev4qv
  REAL,    DIMENSION (:),     ALLOCATABLE :: lon_ob4qv
  REAL,    DIMENSION (:),     ALLOCATABLE :: lat_ob4qv
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: z_ob4qv
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: p_ob4qv
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: q_ob4qv
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: qv_ob4qv
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: qoe_ob4qv
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: elev_ob4qv
  REAL,    DIMENSION (:),     ALLOCATABLE :: pxx_ob4qv
  REAL,    DIMENSION (:),     ALLOCATABLE :: pyy_ob4qv
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: hgt_ob4qv, ihgt_ob4qv, pz1_ob4qv, pz2_ob4qv

  LOGICAL, DIMENSION (:),     ALLOCATABLE :: useobs4qv   ! valid obs
  LOGICAL, DIMENSION (:),     ALLOCATABLE :: useaeri4qv  ! dynamic with ipass

  REAL,    DIMENSION (:,:),   ALLOCATABLE :: pt_pQJc
  REAL,    DIMENSION (:,:),   ALLOCATABLE :: qv_pQJc

  LOGICAL, ALLOCATABLE :: ownobs4t(:), ownobs4qv(:)

  LOGICAL :: aeriobj_initialized

!-----------------------------------------------------------------------
!
! Public interfaces
!
!-----------------------------------------------------------------------

  PUBLIC :: aeri_opt, readaeri
  PUBLIC :: aeriobs_init, aeriobs_set_usage
  PUBLIC :: aeri_read_nml_options, aeri_costf, aeri_gradt
  PUBLIC :: allocateAERIobs, deallocateAERIobs

  CONTAINS

  !#####################################################################

  SUBROUTINE aeri_read_nml_options(unum, npass, myproc,istatus)

    IMPLICIT NONE
    INTEGER, INTENT(IN)  :: unum, myproc
    INTEGER, INTENT(IN)  :: npass
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    NAMELIST /var_aeri/ aeri_opt, aeri_file, iuseaeri

  !---------------------------------------------------------------------

    INTEGER :: i

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    aeri_opt = 0
    aeri_file= '/scratch/junjun.hu/AERIBUFR/aeribufr'

    IF ( ALLOCATED(iuseaeri)) DEALLOCATE (iuseaeri)
    ALLOCATE ( iuseaeri(0:npass), stat=istatus )

    iuseaeri(:) = 0
    DO i=1,npass
       iuseaeri(i) = 1
    END DO

    IF (myproc == 0) THEN
       READ(unum, var_aeri,  END=350)
       WRITE(*,*) 'Namelist block var_aeri sucessfully read.'

       GOTO 400

       350 CONTINUE  ! Error with namelist reading
       WRITE(*,*) 'ERROR: reading namelist block var_aeri.'
       istatus = -2
       RETURN

       400 CONTINUE  ! Successful read namelist

       WRITE(*,'(5x,a,I0,a)') 'aeri_opt      = ', aeri_opt,','
       WRITE(*,'(5x,a,a,a)')  'aeri_file     = "', TRIM(aeri_file),'",'
       WRITE(*,'(5x,a,5(I0,a))') 'iuseaeri      = ', (iuseaeri(i),',',i=0,npass)

    END IF

    CALL mpupdatei(aeri_opt, 1)
    CALL mpupdatei(iuseaeri, npass)

    aeriobj_initialized = .FALSE.

    RETURN
  END SUBROUTINE aeri_read_nml_options

  !#####################################################################

  SUBROUTINE allocateAERIobs(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF ( ALLOCATED(lon_ob4t)) DEALLOCATE (lon_ob4t)
    ALLOCATE ( lon_ob4t(mxaeri), stat=istatus )

    IF ( ALLOCATED(lat_ob4t)) DEALLOCATE (lat_ob4t)
    ALLOCATE ( lat_ob4t(mxaeri), stat=istatus )

    IF ( ALLOCATED(z_ob4t)) DEALLOCATE (z_ob4t)
    ALLOCATE ( z_ob4t(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(p_ob4t)) DEALLOCATE (p_ob4t)
    ALLOCATE ( p_ob4t(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(ta_ob4t)) DEALLOCATE (ta_ob4t)
    ALLOCATE ( ta_ob4t(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(pt_ob4t)) DEALLOCATE (pt_ob4t)
    ALLOCATE ( pt_ob4t(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(toe_ob4t)) DEALLOCATE (toe_ob4t)
    ALLOCATE ( toe_ob4t(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(elev_ob4t)) DEALLOCATE (elev_ob4t)
    ALLOCATE ( elev_ob4t(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(pxx_ob4t)) DEALLOCATE (pxx_ob4t)
    ALLOCATE ( pxx_ob4t(mxaeri), stat=istatus )

    IF ( ALLOCATED(pyy_ob4t)) DEALLOCATE (pyy_ob4t)
    ALLOCATE ( pyy_ob4t(mxaeri), stat=istatus )

    IF ( ALLOCATED(hgt_ob4t)) DEALLOCATE (hgt_ob4t)
    ALLOCATE ( hgt_ob4t(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(ihgt_ob4t)) DEALLOCATE (ihgt_ob4t)
    ALLOCATE ( ihgt_ob4t(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(pz1_ob4t)) DEALLOCATE (pz1_ob4t)
    ALLOCATE ( pz1_ob4t(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(pz2_ob4t)) DEALLOCATE (pz2_ob4t)
    ALLOCATE ( pz2_ob4t(nzk,mxaeri), stat=istatus )

    lon_ob4t = 0
    lat_ob4t = 0
    hgt_ob4t = -99999.
    ta_ob4t = -99999.

    IF ( ALLOCATED(lon_ob4qv)) DEALLOCATE (lon_ob4qv)
    ALLOCATE ( lon_ob4qv(mxaeri), stat=istatus )

    IF ( ALLOCATED(lat_ob4qv)) DEALLOCATE (lat_ob4qv)
    ALLOCATE ( lat_ob4qv(mxaeri), stat=istatus )

    IF ( ALLOCATED(z_ob4qv)) DEALLOCATE (z_ob4qv)
    ALLOCATE ( z_ob4qv(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(p_ob4qv)) DEALLOCATE (p_ob4qv)
    ALLOCATE ( p_ob4qv(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(qv_ob4qv)) DEALLOCATE (qv_ob4qv)
    ALLOCATE ( qv_ob4qv(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(qoe_ob4qv)) DEALLOCATE (qoe_ob4qv)
    ALLOCATE ( qoe_ob4qv(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(elev_ob4qv)) DEALLOCATE (elev_ob4qv)
    ALLOCATE ( elev_ob4qv(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(pxx_ob4qv)) DEALLOCATE (pxx_ob4qv)
    ALLOCATE ( pxx_ob4qv(mxaeri), stat=istatus )

    IF ( ALLOCATED(pyy_ob4qv)) DEALLOCATE (pyy_ob4qv)
    ALLOCATE ( pyy_ob4qv(mxaeri), stat=istatus )

    IF ( ALLOCATED(hgt_ob4qv)) DEALLOCATE (hgt_ob4qv)
    ALLOCATE ( hgt_ob4qv(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(ihgt_ob4qv)) DEALLOCATE (ihgt_ob4qv)
    ALLOCATE ( ihgt_ob4qv(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(pz1_ob4qv)) DEALLOCATE (pz1_ob4qv)
    ALLOCATE ( pz1_ob4qv(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(pz2_ob4qv)) DEALLOCATE (pz2_ob4qv)
    ALLOCATE ( pz2_ob4qv(nzk,mxaeri), stat=istatus )

    IF ( ALLOCATED(useobs4t)) DEALLOCATE (useobs4t)
    ALLOCATE ( useobs4t(mxaeri), stat=istatus )

    IF ( ALLOCATED(useobs4qv)) DEALLOCATE (useobs4qv)
    ALLOCATE ( useobs4qv(mxaeri), stat=istatus )

    IF ( ALLOCATED(nlev4t)) DEALLOCATE (nlev4t)
    ALLOCATE ( nlev4t(mxaeri), stat=istatus )

    IF ( ALLOCATED(nlev4qv)) DEALLOCATE (nlev4qv)
    ALLOCATE ( nlev4qv(mxaeri), stat=istatus )

    IF ( ALLOCATED(useaeri4t)) DEALLOCATE (useaeri4t)
    ALLOCATE ( useaeri4t(mxaeri), stat=istatus )

    IF ( ALLOCATED(useaeri4qv)) DEALLOCATE (useaeri4qv)
    ALLOCATE ( useaeri4qv(mxaeri), stat=istatus )

    lon_ob4qv = 0
    lat_ob4qv = 0
    hgt_ob4qv = -99999.0
    qv_ob4qv = -99999.0
    useobs4qv  = .FALSE.
    useobs4t   = .FALSE.
    useaeri4qv = .FALSE.
    useaeri4t  = .FALSE.
    nlev4t = 0
    nlev4qv = 0

    IF ( ALLOCATED(ownobs4qv)) DEALLOCATE (ownobs4qv)
    ALLOCATE ( ownobs4qv(mxaeri), stat=istatus )
    IF ( ALLOCATED(ownobs4t)) DEALLOCATE (ownobs4t)
    ALLOCATE ( ownobs4t(mxaeri), stat=istatus )
    ownobs4t (:) = .FALSE.
    ownobs4qv(:) = .FALSE.

    IF ( ALLOCATED(   pt_pQJc) ) DEALLOCATE (   pt_pQJc)
    ALLOCATE (    pt_pQJc(nzk,mxaeri), stat=istatus )
    pt_pQJc = 0.0
    IF ( ALLOCATED(   qv_pQJc) ) DEALLOCATE (   qv_pQJc)
    ALLOCATE (    qv_pQJc(nzk,mxaeri), stat=istatus )
    qv_pQJc = 0.0

    RETURN
  END SUBROUTINE allocateAERIobs

  !#####################################################################

  SUBROUTINE deallocateAERIobs(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    DEALLOCATE (lon_ob4t,  STAT = istatus)
    DEALLOCATE (lat_ob4t,  STAT = istatus)
    DEALLOCATE (z_ob4t,    STAT = istatus)
    DEALLOCATE (p_ob4t,    STAT = istatus)
    DEALLOCATE (ta_ob4t,   STAT = istatus)
    DEALLOCATE (pt_ob4t,   STAT = istatus)
    DEALLOCATE (toe_ob4t,  STAT = istatus)
    DEALLOCATE (elev_ob4t, STAT = istatus)
    DEALLOCATE (pxx_ob4t,  STAT = istatus)
    DEALLOCATE (pyy_ob4t,  STAT = istatus)
    DEALLOCATE (hgt_ob4t,  STAT = istatus)
    DEALLOCATE (ihgt_ob4t, STAT = istatus)
    DEALLOCATE (pz1_ob4t,  STAT = istatus)
    DEALLOCATE (pz2_ob4t,  STAT = istatus)

    DEALLOCATE (lon_ob4qv, STAT = istatus)
    DEALLOCATE (lat_ob4qv, STAT = istatus)
    DEALLOCATE (z_ob4qv,   STAT = istatus)
    DEALLOCATE (p_ob4qv,   STAT = istatus)
    DEALLOCATE (qv_ob4qv,  STAT = istatus)
    DEALLOCATE (qoe_ob4qv, STAT = istatus)
    DEALLOCATE (elev_ob4qv,STAT = istatus)
    DEALLOCATE (pxx_ob4qv, STAT = istatus)
    DEALLOCATE (pyy_ob4qv, STAT = istatus)
    DEALLOCATE (hgt_ob4qv, STAT = istatus)
    DEALLOCATE (ihgt_ob4qv,STAT = istatus)
    DEALLOCATE (pz1_ob4qv, STAT = istatus)
    DEALLOCATE (pz2_ob4qv, STAT = istatus)

    DEALLOCATE ( pt_pQJc,      STAT = istatus )
    DEALLOCATE ( qv_pQJc,      STAT = istatus )
    DEALLOCATE ( useobs4t,     STAT = istatus )
    DEALLOCATE ( useobs4qv,    STAT = istatus )
    DEALLOCATE ( nlev4t,       STAT = istatus )
    DEALLOCATE ( nlev4qv,      STAT = istatus )
    IF ( ALLOCATED(iuseaeri)) DEALLOCATE (iuseaeri,STAT = istatus)
    DEALLOCATE ( useaeri4t,     STAT = istatus )
    DEALLOCATE ( useaeri4qv,    STAT = istatus )
    DEALLOCATE ( ownobs4qv, STAT = istatus )
    DEALLOCATE ( ownobs4t,  STAT = istatus )

    RETURN
  END SUBROUTINE

  !#####################################################################

  SUBROUTINE readAERI(myproc,nx,ny,nz,xs,ys,zs,istatus)

    INCLUDE 'phycst.inc'
    INTEGER, INTENT(IN) :: myproc
    INTEGER, INTENT(IN) :: nx,ny,nz
    REAL(P), INTENT(IN) :: xs(nx)
    REAL(P), INTENT(IN) :: ys(ny)
    REAL(P), INTENT(IN) :: zs(nx,ny,nz)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER, PARAMETER :: mxlv=255 ! max pressure levels

    CHARACTER(40) :: hdstr,qcstr,oestr,obstr,levstr
    CHARACTER(8)  :: subset

    INTEGER       :: ireadmg,ireadsb
    INTEGER       :: lunin,levs
    INTEGER       :: idate,iret,k,n,j
    INTEGER       :: cat
    INTEGER       :: tqm,qqm
    INTEGER       :: iob4t,ilev4t,iob4qv,ilev4qv

    REAL          :: toe,qoe,elev,zob,tob,qob,pob,lon,lat
    REAL          :: rhb,rhe,rhp,rh_err,rep_t,rep_rh

    REAL(DP), DIMENSION(6)      :: hdr
    REAL(DP), DIMENSION(4,mxlv) :: qcmark,obserr
    REAL(DP), DIMENSION(5,mxlv) :: obsdat
    REAL(DP), DIMENSION(1,mxlv) :: levdat

    DATA hdstr /'SID XOB YOB DHR TYP ELV'/
    DATA levstr /'ZOB'/
    DATA obstr /'CAT TOB QOB ZOB POB'/
    DATA qcstr /'TQM QQM ZQM PQM'/
    DATA oestr /'TOE QOE ZOE POE'/

    DATA lunin / 61 /

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !start reading the data
    IF (myproc == 0) THEN
      open(lunin,file=trim(aeri_file),form='unformatted')
      call openbf(lunin,'IN',lunin)
      call datelen(10)

      iob4t = 1
      iob4qv  = 1
      msg_loop:  do while(ireadmg(lunin,subset,idate) == 0)
        if(subset/='AERITR') then
          cycle msg_loop
        end if
        subset_loop: do while(ireadsb(lunin) == 0)
          ilev4t = 0
          ilev4qv = 0
          levs = 0

          call ufbint(lunin,hdr,6,1,iret,hdstr)
          call ufbint(lunin,obsdat,5,mxlv,levs,obstr) !obs
          call ufbint(lunin,qcmark,4,mxlv,levs,qcstr) !qm
          call ufbint(lunin,obserr,4,mxlv,levs,oestr) !oerr

          if(levs<=0) then
             cycle subset_loop
          endif

          if(140/=hdr(5)) then !the type is 140 for aeribufr
             write(6,*) 'READAERI: ERROR - TYPE IN BUFR FILE is ', hdr(5),', NOT 140'
             cycle subset_loop
          endif

          if(abs(hdr(3))>90.0 .or. abs(hdr(2))>180.0) then
             write(6,*) 'READAERI: ERROR - LAT AND LONG INVALID, LONG = ', hdr(2),', LAT = ', hdr(3)
             cycle subset_loop
          endif
          lon = hdr(2) ! degree N: -90 -> 90
          lat = hdr(3) ! degree E: -180 -> 180

          loop_obs:do k=1,levs
             tqm = qcmark(1,k)
             qqm = qcmark(2,k)
             cat = obsdat(1,k)
             tob = obsdat(2,k) ! C
             qob = obsdat(3,k) ! g/kg
             zob = obsdat(4,k) ! m
             pob = obsdat(5,k) ! mb

             toe = obserr(1,k) ! C (or K, error does not matter)

             qoe = obserr(2,k)! in g/kg

             write(6,*) tqm,qqm,cat,tob,qob,zob,pob,toe,qoe

             ! convert temperature from C to K
             tob = tob + t0c  ! convert C to K

             ! convert observation error from q to relative humidity
             !call qv_to_relh(qob,tob,pob,rhb) !g/kg, K, mb
             !call qv_to_relh(qob+qoe,tob,pob,rhe)
             !call qv_to_relh(qob,tob+toe,pob,rhp)
             !rh_err = sqrt((rhe - rhb)*(rhe - rhb)+(rhp - rhb)*(rhp - rhb)) ! 0.0 - 1.0, not percent

             ! pressure mb
             pob = obsdat(5,k)
             ! add the representation error
             call rep_err_t(pob,rep_t)
             ! call rep_err_rh(pob,rep_rh) ! TODO qv error or RH error, how to deal with the representation error
             toe = toe+rep_t
             !rh_err = rh_err+rep_rh/10.0

             ! convert water vapor from g/kg to kg.kg
             qob = qob*1.e-03 ! convert g/kg to kg/kg

             elev = hdr(6) ! m
             ! check missing value and invalid value
             if(tob>0.0 .and. pob>0.0 .and. toe>0.0 .and. abs(tob)<9999.0 .and. abs(pob)<9999.0 .and. abs(toe)<9999.0) then
                ilev4t = ilev4t+1
                z_ob4t(ilev4t,iob4t)=zob                  ! obseration height (m)
                p_ob4t(ilev4t,iob4t)=pob                  ! pressure (mb)
                ta_ob4t(ilev4t,iob4t)=tob                     ! temperature ob, K
                toe_ob4t(ilev4t,iob4t)=toe                    ! temperature obs error (K)
                elev_ob4t(ilev4t,iob4t)=elev                   ! station elevation (m)
             endif


             ! check missing value and invalid value
             if(qob>0.0 .and. pob>0.0 .and. qoe>0.0 .and. abs(qob)<9999.0 .and. abs(pob)<9999.0 .and. abs(qoe)<9999.0) then
                ilev4qv = ilev4qv+1
                z_ob4qv(ilev4qv,iob4qv)=zob                  ! obseration height (m)
                p_ob4qv(ilev4qv,iob4qv)=pob                  ! pressure(mb)
                qv_ob4qv(ilev4qv,iob4qv)=qob                     ! water vapor observation (kg/kg)
                qoe_ob4qv(ilev4qv,iob4qv)=qoe*1.e-03                      ! rh observation error (0-1) !TODO rh OR qv?
                elev_ob4qv(ilev4qv,iob4qv)=elev                   ! station elevation (m)
             endif

          end do loop_obs
          if(ilev4t>0) then
              lon_ob4t(iob4t)=lon                    ! grid relative longitude
              lat_ob4t(iob4t)=lat                    ! grid relative latitude
              nlev4t(iob4t) = ilev4t
              useobs4t(iob4t) = .TRUE.
              iob4t = iob4t+1
          endif

          if(ilev4qv>0) then
              lon_ob4qv(iob4qv)=lon                    ! grid relative longitude
              lat_ob4qv(iob4qv)=lat                    ! grid relative latitude
              nlev4qv(iob4qv) = ilev4qv
              useobs4qv(iob4qv) = .TRUE.
              iob4qv = iob4qv+1
          endif
        end do subset_loop
      end do msg_loop

      No4t = iob4t-1
      No4qv = iob4qv-1
      call closbf(lunin)
      close(lunin)

      ! calculate potential temp
      DO k=1,No4t
         DO j=1,nlev4t(k)
             ! calculate potential temp from observed air temperature
             pt_ob4t(j,k) = ta_ob4t(j,k)*(p0/(p_ob4t(j,k)*100.0))**(rd/cp)
         END DO
      END DO
    ENDIF

    CALL mpupdatei(No4qv,1)
    CALL mpupdatei(No4t, 1)
    CALL mpupdater(lat_ob4qv,mxaeri)
    CALL mpupdater(lat_ob4t,mxaeri)
    CALL mpupdater(lon_ob4qv,mxaeri)
    CALL mpupdater(lon_ob4t,mxaeri)
    CALL mpupdatei(nlev4qv,  mxaeri)
    CALL mpupdatei(nlev4t,   mxaeri)
    CALL mpupdatel(useobs4qv,  mxaeri)
    CALL mpupdatel(useobs4t,   mxaeri)
    CALL mpupdater(z_ob4qv,  nzk*mxaeri)
    CALL mpupdater(z_ob4t,   nzk*mxaeri)
    !CALL mpupdater(p_ob4qv,  nzk*mxaeri)
    !CALL mpupdater(p_ob4t,   nzk*mxaeri)
    CALL mpupdater(pt_ob4t,   nzk*mxaeri)
    CALL mpupdater(qv_ob4qv,  nzk*mxaeri)
    !CALL mpupdater(ta_ob4t,   nzk*mxaeri)
    CALL mpupdater(qoe_ob4qv,  nzk*mxaeri)
    CALL mpupdater(toe_ob4t,   nzk*mxaeri)
    !CALL mpupdater(elev_ob4qv,  nzk*mxaeri)
    !CALL mpupdater(elev_ob4t,   nzk*mxaeri)

    RETURN
  END SUBROUTINE readAERI

  !#####################################################################

  SUBROUTINE aeriobs_init(nx,ny,nz,xs,ys,zs,myproc,nprocs,istatus)

    INTEGER, INTENT(IN) :: myproc,nprocs
    INTEGER, INTENT(IN) :: nx,ny,nz
    REAL(P), INTENT(IN) :: xs(nx)
    REAL(P), INTENT(IN) :: ys(ny)
    REAL(P), INTENT(IN) :: zs(nx,ny,nz)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i
    REAL,    ALLOCATABLE :: x_ob(:),y_ob(:)
    INTEGER, ALLOCATABLE :: isrcobs(:),item1(:)
    INTEGER, ALLOCATABLE :: indexobs4t(:),indexobs4qv(:)
    INTEGER :: kprocobs(nprocs)
    INTEGER :: kobsmax

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (aeri_opt <= 0 .OR. aeriobj_initialized) RETURN

  !---------------------------------------------------------------------
  !
  ! qv observations
  !
  !---------------------------------------------------------------------

    ALLOCATE(isrcobs(No4qv),     STAT = istatus)
    ALLOCATE(indexobs4qv(No4qv), STAT = istatus)
    ALLOCATE(item1(No4qv),       STAT = istatus)

    ALLOCATE(x_ob(No4qv), STAT = istatus)
    ALLOCATE(y_ob(No4qv), STAT = istatus)

    isrcobs(:) = 1
    ownobs4qv(:) = .FALSE.

    CALL lltoxy(No4qv,1,lat_ob4qv,lon_ob4qv,x_ob,y_ob)

    CALL mpiprocess(no4qv,indexobs4qv,useobs4qv,nprocs,kprocobs,kobsmax, &
                    isrcobs,item1,nx,ny,x_ob,y_ob,xs,ys,0)

    DO i = 1, no4qv
      IF (indexobs4qv(i) == myproc) ownobs4qv(i) = .TRUE.
    END DO

    !
    ! perform some coordinate transformation
    !
    CALL map_to_mod2(nx,ny,mxaeri,No4qv,useobs4qv,xs,ys,x_ob,y_ob,pxx_ob4qv,pyy_ob4qv)

    hgt_ob4qv = z_ob4qv! MSL, m
    CALL map_to_modz(nzk,mxaeri,nlev4qv,No4qv,nx,ny,nz,zs,              &
                      pxx_ob4qv,pyy_ob4qv, hgt_ob4qv, ihgt_ob4qv, pz1_ob4qv, pz2_ob4qv)

    DEALLOCATE(isrcobs, indexobs4qv, item1)
    DEALLOCATE(x_ob, y_ob)

  !---------------------------------------------------------------------
  !
  ! t observations
  !
  !---------------------------------------------------------------------

    ALLOCATE(isrcobs(No4t),     STAT = istatus)
    ALLOCATE(indexobs4t(No4t),  STAT = istatus)
    ALLOCATE(item1(No4t),       STAT = istatus)

    ALLOCATE(x_ob(No4t), STAT = istatus)
    ALLOCATE(y_ob(No4t), STAT = istatus)

    isrcobs(:) = 1
    ownobs4t(:) = .FALSE.

    CALL lltoxy(No4t,1,lat_ob4t,lon_ob4t,x_ob,y_ob)

    CALL mpiprocess(No4t,indexobs4t,useobs4t,nprocs,kprocobs,kobsmax,   &
                    isrcobs,item1,nx,ny,x_ob,y_ob,xs,ys,0)

    DO i = 1, no4t
      IF (indexobs4t(i) == myproc) ownobs4t(i) = .TRUE.
    END DO

    !
    ! perform some coordinate transformation
    !
    CALL map_to_mod2(nx,ny,mxaeri,No4t,useobs4t,xs,ys,x_ob,y_ob,pxx_ob4t,pyy_ob4t)

    hgt_ob4t = z_ob4t! MSL, m
    CALL map_to_modz(nzk,mxaeri,nlev4t,No4t,nx,ny,nz,zs,                &
                     pxx_ob4t,pyy_ob4t, hgt_ob4t, ihgt_ob4t, pz1_ob4t, pz2_ob4t)

    DEALLOCATE(isrcobs, indexobs4t, item1)
    DEALLOCATE(x_ob, y_ob)

  !---------------------------------------------------------------------
  !
  ! Before return
  !
  !---------------------------------------------------------------------

    aeriobj_initialized = .TRUE.

    RETURN
  END SUBROUTINE aeriobs_init

  !#####################################################################

  SUBROUTINE aeriobs_set_usage(myproc,ipass,aerisw,istatus)
  !
  ! To determine whether AERI data will be used in this "ipass" specific pass
  !
    INTEGER, INTENT(IN)  :: myproc,ipass
    INTEGER, INTENT(OUT) :: aerisw

    INTEGER, INTENT(OUT) :: istatus
    INTEGER ::i

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0
    aerisw = 0

    IF( aeri_opt == 0 ) RETURN

    useaeri4t  = .FALSE.
    useaeri4qv = .FALSE.

    !IF (ipass > 0) THEN   ! chk_opt == 1, ipass == 0
      IF (myproc == 0) WRITE(6,'(/a)') '   Thermodynamic profiles from AERI'
      aerisw=0
      IF( aeri_opt /= 0 ) THEN
        IF(iuseaeri(ipass) > 0 .AND. (No4t > 0 .OR. No4qv > 0)) THEN
          aerisw=1
          DO i=1,No4t
             IF (useobs4t(i)) useaeri4t(i) = .TRUE.
          END DO
          DO i=1,No4qv
             IF (useobs4qv(i)) useaeri4qv(i) = .TRUE.
          END DO
          IF (myproc == 0) WRITE(6,'(a)') '        Using          AERI'
        END IF
      END IF
      IF(aerisw == 0 .AND. myproc == 0) WRITE(6,'(a)') '        none'
    !END IF

    !CALL mpmaxi(aerisw)
    !CALL mpupdatel(useaeri4t,  mxaeri)
    !CALL mpupdatel(useaeri4qv,  mxaeri)
    !write(6,*) 'useaeri4t=',useaeri4t
    !write(6,*) 'useaeri4qv=',useaeri4qv
    RETURN
  END SUBROUTINE aeriobs_set_usage

  !#####################################################################

  SUBROUTINE aeri_costf(nx,ny,nz,q_ctr,t_ctr,xs,ys,zs,                  &
                        ibgn,iend,jbgn,jend,kbgn,kend,                  &
                        f_aeri_qv,f_aeri_pt,istatus)

    !INCLUDE 'phycst.inc'
    INTEGER,  INTENT(IN)  :: nx,ny,nz
    REAL(P),  INTENT(IN)  :: xs(nx)
    REAL(P),  INTENT(IN)  :: ys(ny)
    REAL(P),  INTENT(IN)  :: zs(nx,ny,nz)

    REAL(P),  INTENT(IN)  :: q_ctr(nx,ny,nz)
    REAL(P),  INTENT(IN)  :: t_ctr(nx,ny,nz)

    INTEGER,  INTENT(IN)  :: ibgn,iend,jbgn,jend,kbgn,kend

    REAL(DP), INTENT(OUT) :: f_aeri_qv,f_aeri_pt

    INTEGER,  INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER  :: k, ihgt_ob,j
    REAL(DP) :: zqq,ztt
    REAL(DP) :: qv_intrp(nzk,mxaeri), t_intrp(nzk,mxaeri)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    f_aeri_qv = 0.0D0
    f_aeri_pt = 0.0D0

    ! interpolate model qv into obs locations
    CALL linearint_3d(nx,ny,nz,q_ctr(1,1,1),xs(1),ys(1),zs(1,1,1),      &
                    useaeri4qv,nzk, mxaeri, nlev4qv, No4qv, 5,           &
                    pxx_ob4qv,pyy_ob4qv,pz1_ob4qv, pz2_ob4qv,hgt_ob4qv, &
                    ihgt_ob4qv,qv_intrp)

    DO k=1,No4qv
      IF (useaeri4qv(k)) THEN
         DO j=1,nlev4qv(k)
            zqq = qv_intrp(j,k) - qv_ob4qv(j,k) !
            qv_pQJc(j,k) = zqq/(qoe_ob4qv(j,k)*qoe_ob4qv(j,k))
            zqq = zqq*qv_pQJc(j,k)
            IF (ownobs4qv(k)) f_aeri_qv  = f_aeri_qv +  zqq
         END DO
      END IF
    END DO

    CALL linearint_3d(nx,ny,nz,t_ctr(1,1,1),xs(1),ys(1),zs(1,1,1),      &
                    useaeri4t,nzk, mxaeri, nlev4t, No4t, 4,              &
                    pxx_ob4t,pyy_ob4t,pz1_ob4t, pz2_ob4t,hgt_ob4t,      &
                    ihgt_ob4t,t_intrp)

    DO k=1,No4t
      IF (useaeri4t(k)) THEN
         DO j=1,nlev4t(k)
            ztt = t_intrp(j,k) - pt_ob4t(j,k)
            pt_pQJc(j,k) = ztt/(toe_ob4t(j,k)*toe_ob4t(j,k))
            ztt = ztt*pt_pQJc(j,k)
            IF (ownobs4t(k)) f_aeri_pt    = f_aeri_pt +  ztt
         END DO
      END IF
    END DO

    RETURN
  END SUBROUTINE aeri_costf

  !#####################################################################

  SUBROUTINE aeri_gradt(nx,ny,nz,q_grd,t_grd,xs,ys,zs,tem1,istatus)

    REAL(P),  INTENT(IN)     :: xs(nx)
    REAL(P),  INTENT(IN)     :: ys(ny)
    REAL(P),  INTENT(IN)     :: zs(nx,ny,nz)
    INTEGER,  INTENT(IN)     :: nx,ny,nz
    REAL(P),  INTENT(INOUT)  :: t_grd(nx,ny,nz)
    REAL(P),  INTENT(INOUT)  :: q_grd(nx,ny,nz)
    REAL(P),  INTENT(OUT)    :: tem1(nx,ny,nz)

    INTEGER,  INTENT(OUT)    :: istatus

  !---------------------------------------------------------------------

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    CALL adlinearint_3d(nx,ny,nz,q_grd(1,1,1),xs(1),ys(1),zs(1,1,1),    &
                      useaeri4qv, nzk, mxaeri, nlev4qv, No4qv, 5,        &
                      pxx_ob4qv,pyy_ob4qv,pz1_ob4qv,pz2_ob4qv,hgt_ob4qv,&
                      ihgt_ob4qv,qv_pQJc,tem1)

    CALL adlinearint_3d(nx,ny,nz,t_grd(1,1,1),xs(1),ys(1),zs(1,1,1),    &
                      useaeri4t, nzk, mxaeri, nlev4t, No4t, 4,           &
                      pxx_ob4t,pyy_ob4t,pz1_ob4t, pz2_ob4t,hgt_ob4t,    &
                      ihgt_ob4t,pt_pQJc,tem1)

    RETURN
  END SUBROUTINE aeri_gradt

  !#####################################################################

  ! define the representation error for t, according to pressure levels
  SUBROUTINE rep_err_t(presw,rep_err)
    !use kinds, only: r_kind

    REAL(P), INTENT(IN)  :: presw  ! mb
    REAL(P), INTENT(OUT) :: rep_err ! C/K

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    if(presw>=1075.0)  then ! 1100 mb select the closest level
       rep_err = 1.27 - 0.3
    elseif(presw>=1025.0 .and. presw<1075.0) then ! 1050mb
       rep_err = 1.33 - 0.3
    elseif(presw>=975.0 .and. presw<1025.0) then ! 1000mb
       rep_err = 1.39 - 0.3
    elseif(presw>=925.0 .and. presw<975.0) then ! 950mb
       rep_err = 1.44 - 0.3
    elseif(presw>=875.0 .and. presw<925.0) then ! 900mb
       rep_err = 1.44 - 0.3
    elseif(presw>=825.0 .and. presw<875.0) then ! 850mb
       rep_err = 1.37 - 0.3
    elseif(presw>=775.0 .and. presw<825.0) then ! 800mb
       rep_err = 1.26 - 0.3
    elseif(presw>=725.0 .and. presw<775.0) then ! 750mb
       rep_err = 1.14 - 0.3
    elseif(presw>=675.0 .and. presw<725.0) then ! 700mb
       rep_err = 1.04 - 0.3
    elseif(presw>=625.0 .and. presw<675.0) then ! 650mb
       rep_err = 0.98 - 0.3
    elseif(presw>=575.0 .and. presw<625.0) then ! 600mb
       rep_err = 0.95 - 0.3
    elseif(presw>=525.0 .and. presw<575.0) then ! 550mb
       rep_err = 0.93 - 0.3
    elseif(presw>=475.0 .and. presw<525.0) then ! 500mb
       rep_err = 0.92 - 0.3
    elseif(presw>=425.0 .and. presw<475.0) then ! 450mb
       rep_err = 0.92 - 0.3
    elseif(presw>=375.0 .and. presw<425.0) then ! 400mb
       rep_err = 0.94 - 0.3
    elseif(presw>=325.0 .and. presw<375.0) then ! 350mb
       rep_err = 0.98 - 0.3
    elseif(presw>=275.0 .and. presw<325.0) then ! 300mb
       rep_err = 1.07 - 0.3
    elseif(presw>=225.0 .and. presw<275.0) then ! 250mb
       rep_err = 1.18 - 0.3
    elseif(presw>=175.0 .and. presw<225.0) then ! 200mb
       rep_err = 1.29 - 0.3
    elseif(presw>=125.0 .and. presw<175.0) then ! 150mb
       rep_err = 1.35 - 0.3
    elseif(presw>=87.5 .and. presw<125.0) then ! 100mb
       rep_err = 1.38 - 0.3
    elseif(presw>=62.5 .and. presw<87.5) then ! 75mb
       rep_err = 1.38 - 0.3
    elseif(presw>=45.0 .and. presw<62.5) then ! 50mb
       rep_err = 1.35 - 0.3
    elseif(presw>=35.0 .and. presw<45.0) then ! 40mb
       rep_err = 1.31 - 0.3
    elseif(presw>=25.0 .and. presw<35.0) then ! 30mb
       rep_err = 1.30 - 0.3
    elseif(presw>=15.0 .and. presw<25.0) then ! 20mb
       rep_err = 1.34 - 0.3
    elseif(presw>=7.5 .and. presw<15.0) then ! 10mb
       rep_err = 1.40 - 0.3
    elseif(presw>=4.5 .and. presw<7.5) then ! 5mb
       rep_err = 1.45 - 0.3
    elseif(presw>=3.5 .and. presw<4.5) then ! 4mb
       rep_err = 1.47 - 0.3
    elseif(presw>=2.5 .and. presw<3.5) then ! 3mb
       rep_err = 1.49 - 0.3
    elseif(presw>=1.5 .and. presw<2.5) then ! 2mb
       rep_err = 1.50 - 0.3
    elseif(presw<1.5) then ! 0-1mb
       rep_err = 1.50 - 0.3
    endif

    RETURN
  END SUBROUTINE rep_err_t

END MODULE module_aeri
