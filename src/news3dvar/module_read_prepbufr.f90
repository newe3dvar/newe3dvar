MODULE module_read_prepbufr

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! Based on decode_prepbufr_example from NCEP.
! https://www.emc.ncep.noaa.gov/mmb/data_processing/prepbufr.doc/decode_prepbufr_example.htm
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  IMPLICIT NONE

  PRIVATE
!-----------------------------------------------------------------------
!
! CONSTANTS
!
!-----------------------------------------------------------------------
  REAL*8,  PARAMETER, PUBLIC :: R8BFMS = 9.0E08 ! Missing value threshhold for BUFR

  INTEGER, PARAMETER, PUBLIC :: NHR8PM = 14     ! Actual number of BUFR parameters
                                                !  in header
  INTEGER, PARAMETER, PUBLIC :: MXR8PM =  8     ! Maximum number of BUFR parameters
                                        !  in level data (non-radiance) or
                                        !  in channel data (radiance)
  INTEGER, PARAMETER, PUBLIC :: MXR8LV = 255    ! Maximum number of BUFR levels/channels
  INTEGER, PARAMETER, PUBLIC :: MXR8VN = 10     ! Max. number of BUFR event sequences
                                                !  (non-radiance reports)
  INTEGER, PARAMETER, PUBLIC :: MXR8VT = 17     ! Max. number of BUFR variable types
                                                !  (non-radiance reports)
  INTEGER, PARAMETER, PUBLIC :: MXSTRL = 80     ! Maximum size of a string

  CHARACTER(LEN=MXSTRL), PARAMETER :: ostr ( MXR8VT ) = (/              &
    'POB PQM PPC PRC PFC PAN POE CAT                         ',         & !  1-P
    'QOB QQM QPC QRC QFC QAN QOE CAT                         ',         & !  2-Q
    'TOB TQM TPC TRC TFC TAN TOE CAT                         ',         & !  3-T
    'ZOB ZQM ZPC ZRC ZFC ZAN ZOE CAT                         ',         & !  4-Z
    'UOB WQM WPC WRC UFC UAN WOE CAT                         ',         & !  5-U
    'VOB WQM WPC WRC VFC VAN WOE CAT                         ',         & !  6-V
    'DDO DFQ DFP DFR NULL NULL NULL CAT                      ',         & !  7
    'FFO DFQ DFP DFR NULL NULL NULL CAT                      ',         & !  8
    'PWO PWQ PWP PWR PWF PWA PWE CAT                         ',         & !
    'REQ6 REQ6_QM REQ6_PC REQ6_RC REQ6_FC REQ6_AN REQ6_OE CAT',         & !
    'PW1O PW1Q PW1P PW1R PW1F PW1A PW1E CAT                  ',         & !
    'PW2O PW2Q PW2P PW2R PW2F PW2A PW2E CAT                  ',         & !
    'PW3O PW3Q PW3P PW3R PW3F PW3A PW3E CAT                  ',         & !
    'PW4O PW4Q PW4P PW4R PW4F PW4A PW4E CAT                  ',         & !
    'CDTP CDTP_QM CDTP_PC CDTP_RC CDTP_TC CDTP_AN CDTP_OE CAT',         & !
    'GCDTT NULL NULL NULL NULL NULL NULL CAT                 ' ,        & !
    'TOCC  NULL NULL NULL NULL NULL NULL CAT                 ' /)         !

  INTEGER :: i
  INTEGER, PARAMETER, PUBLIC :: indxvr1 ( 100:299 ) = (/                &
        (1,i=1,20), (1,i=1,30), 10,  15,   9,    1,1,1,   11,11,11,11,  (1,i=1,40),       &
       !  100-119     120-149  150  151  152   153-155        156-159     160-199
        (1,i=1,20), (1,i=1,40),    (1,i=1,25),    1,   1,   (1,i=1,13)    /)
       !  200-219     220-259        260-284    285  286      287-299

  INTEGER, PARAMETER, PUBLIC :: indxvr2 ( 100:299 ) = (/                &
        (4,i=1,20), (8,i=1,30),   10,  17,   9,    6,6,6,   14,14,14,14,(6,i=1,40),       &
        ! 100-119     120-149    150  151  152   153-155        156-159   160-299
        (6,i=1,20), (8,i=1,40),    (6,i=1,25),    8,   8,   (6,i=1,13)    /)
        ! 200-219     220-259        260-284    285  286      287-299
  INTEGER, PARAMETER, PUBLIC :: KPOB = 1, KQOB = 2, KTOB = 3, KZOB = 4, KUOB = 5,       &
                                KVOB = 6, PWOB = 9

!-----------------------------------------------------------------------
!
! REMARKS:
!   The header array HDR contains the following list of mnemonics:
!         HDR(1)  Station identification (SID)
!         HDR(2)  Latitude  (YOB)
!         HDR(3)  Longitude (XOB)
!         HDR(4)  Elevation (ELV)
!         HDR(5)  Observation time minus cycle time (DHR)
!         HDR(6)  Reported observation time (RPT)
!         HDR(7)  Indicator whether observation time used to generate
!                  "DHR" was corrected (TCOR)
!         HDR(8)  PREPBUFR report type (TYP)
!         HDR(9) PREPBUFR report subtype (TSB)
!         HDR(10) Input report type (T29)
!         HDR(11) Instrument type (ITP)
!         HDR(12) Report sequence number (SQN)
!         HDR(13) Process number for this MPI run (obtained from
!                  script (PROCN)
!         HDR(14) Satellite identifier (SAID)
!
!-----------------------------------------------------------------------
  REAL*8, PUBLIC  :: hdr ( NHR8PM ), hdr2 ( NHR8PM )
  REAL*8, PUBLIC  :: qkswnd_hdr(3), aircar_hdr(6), aircft_hdr(4),       &
                     adpupa_hdr(1), goesnd1_hdr(6),goesnd2_hdr(MXR8LV), &
                     adpsfc_hdr(5), sfcshp_hdr(2), satwnd_hdr(1)
  REAL*8, PUBLIC  :: drft (3, MXR8LV ), toth ( 2, MXR8LV )

!-----------------------------------------------------------------------
!
!   The 4-D array of non-radiance data, EVNS (ii, lv, jj, kk), is
!    indexed as follows:
!     "ii" indexes the event data types; these consist of:
!         1) Observation       (e.g., POB, ZOB, UOB, VOB, TOB, QOB, PWO)
!         2) Quality mark      (e.g., PQM, ZRM, WQM, TQM, QQM, PWQ)
!         3) Program code      (e.g., PPC, ZPC, WPC, TPC, QPC, PWP)
!         4) Reason code       (e.g., PRC, ZRC, WRC, TRC, QRC, PWR)
!         5) Forecast value    (e.g., PFC, ZFC, UFC, VFC, TFC, QFC, PWF)
!         6) Analyzed value    (e.g., PAN, ZAN, UAN, VAN, TAN, QAN, PWA)
!         7) Observation error (e.g., POE, ZOE, WOE, TOE, QOE, PWO)
!         8) PREPBUFR data level category (CAT)
!     "lv" indexes the levels of the report
!         1) Lowest level
!     "jj" indexes the event stacks
!         1) N'th event
!         2) (N-1)'th event (if present)
!         3) (N-2)'th event (if present)
!                ...
!        10) (N-9)'th event (if present)
!     "kk" indexes the variable types
!         1) Pressure
!         2) Specific humidity
!         3) Temperature
!         4) Height
!         5) U-component wind
!         6) V-component wind
!         7) Wind direction
!         8) Wind speed
!         9) Total precipitable water
!        10) Rain rate
!        11) 1.0 to 0.9 sigma layer precipitable water
!        12) 0.9 to 0.7 sigma layer precipitable water
!        13) 0.7 to 0.3 sigma layer precipitable water
!        14) 0.3 to 0.0 sigma layer precipitable water
!        15) Cloud-top pressure
!        16) Cloud-top temperature
!        17) Total cloud cover
!
!     Note that the structure of this array is identical to one
!      returned from BUFRLIB routine UFBEVN, with an additional (4'th)
!      dimension to include the 14 variable types into the same
!      array.
!
!-----------------------------------------------------------------------

  REAL*8, PUBLIC  :: evns ( MXR8PM, MXR8LV, MXR8VN, MXR8VT )

!-----------------------------------------------------------------------
!
!   The 2-D array of radiance data, BRTS (ii, lv), is indexed
!    as follows:
!     "ii" indexes the event data types; these consist of:
!         1) Observation       (CHNM, TMBR)
!     "lv" indexes the channels (CHNM gives channel number)
!
!     Note that this array is directly returned from BUFRLIB
!      routine UFBINT.
!
!-----------------------------------------------------------------------
   REAL*8, PUBLIC  :: brts (MXR8PM, MXR8LV )

!-----------------------------------------------------------------------
   INTEGER, PUBLIC :: nlev = 0, nchn = 0, nstn = 0, nobs = 0
   INTEGER, PARAMETER, PUBLIC   :: nvar_conv = 10, obsdim = 2

   INTEGER       :: sondetypes(5)  = (/120, 132, 220, 221, 232/)

   REAL, PUBLIC, ALLOCATABLE, DIMENSION(:,:,:)  :: obs
   CHARACTER(24), PUBLIC, ALLOCATABLE           :: obsdt(:)
   INTEGER, PARAMETER   :: assimquality = 2
   INTEGER              :: bufrtype(2)
   REAL                 :: timediff, maxwindow
   LOGICAL              :: replace, sondeobs
   INTEGER              :: btypemin, btypew
   INTEGER              :: irpttypmin, irpttypw

   INTEGER, PUBLIC      :: nrpttyp
   REAL, ALLOCATABLE, PUBLIC    :: rpttyp(:,:)

   PUBLIC :: read_prepbufr_obs, dealloc_convraw

!-----------------------------------------------------------------------

CONTAINS
!
!#######################################################################
!
! SUBPROGRAM:    READPB
!   PRGMMR: KEYSER           ORG: NP12        DATE: 2002-01-28
!
! ABSTRACT: THIS SUBROUTINE READS IN SUBSETS FROM THE PREPBUFR
!   FILE.  SINCE ACTUAL OBSERVATIONS ARE SPLIT INTO MASS AND
!   WIND SUBSETS, THIS ROUTINE COMBINES THEM INTO A SINGLE
!   REPORT FOR OUTPUT.  IT IS STYLED AFTER BUFRLIB ENTRY POINT
!   READNS, AND IT ONLY REQUIRES THE PREPBUFR FILE TO BE OPENED
!   FOR READING WITH OPENBF.  THE COMBINED REPORT IS RETURNED TO
!   THE CALLER IN COMMON /PREPBC/.  THIS COMMON AREA CONTAINS
!   THE NUMBER OF LEVELS (NON-RADIANCES) IN THE REPORT, THE
!   NUMBER OF CHANNELS (RADIANCES) IN THE REPORT, A ONE
!   DIMENSIONAL ARRAY WITH THE HEADER INFORMATION, A FOUR
!   DIMENSIONAL ARRAY CONTAINING ALL EVENTS FROM ALL
!   NON-RADIANCE OBSERVATIONAL VARIABLE TYPES, AND A TWO
!   DIMENSIONAL ARRAY CONTAINING ALL CHANNELS OF BRIGHTNESS
!   TEMPERATURE FOR REPORTS CONTAINING RADIANCE DATA.
!
!
! USAGE:    CALL READPB  (LUNIT, MSGTYP, IDATE, IRET)
!   INPUT ARGUMENT LIST:
!     LUNIT    - UNIT NUMBER OF INPUT PREPBUFR FILE
!
!   OUTPUT ARGUMENT LIST:
!     MSGTYP   - BUFR MESSAGE TYPE (CHARACTER)
!     IDATE    - BUFR MESSAGE DATE IN FORM YYYYMMDDHH
!     IRET     - ERROR RETURN CODE (0 - normal return; 1 - the report
!                within COMMON /PREPBC/ contains the last available
!                report from within the prepbufr file; -1 - there are
!                no more reports available from within the PREPBUFR
!                file, all subsets have been processed)
!
!#######################################################################

  SUBROUTINE READPB  ( lunit, msgtyp_set,in_msgtyps, in_msgsize, msgtyp, idate, iret )

    INTEGER,          INTENT(IN)  :: lunit
    LOGICAL,          INTENT(IN)  :: msgtyp_set
                                  ! if msgtyp_set=T, then only
      !                           ! reports in this in_msgtyps will be
      !                           ! processed
    INTEGER,          INTENT(IN)  :: in_msgsize
    CHARACTER(LEN=8), INTENT(IN)  :: in_msgtyps(in_msgsize)
    CHARACTER(LEN=8), INTENT(OUT) :: msgtyp
    INTEGER,          INTENT(OUT) :: idate
    INTEGER,          INTENT(OUT) :: iret

!-----------------------------------------------------------------------

    CHARACTER(LEN=MXSTRL) :: head1 = 'SID YOB XOB ELV DHR RPT TCOR TYP TSB T29 ITP SQN'
    CHARACTER(LEN=MXSTRL) :: head2 = 'PROCN SAID'

    REAL*8 :: qkswnd_hdr2(3), aircar_hdr2(6),                           &
              aircft_hdr2(4),  adpupa_hdr2(1), goesnd1_hdr2(6),         &
              goesnd2_hdr2(MXR8LV), adpsfc_hdr2(5), sfcshp_hdr2(2),     &
              satwnd_hdr2(1), evns2 ( MXR8PM, MXR8LV, MXR8VN, MXR8VT),  &
              drft2 ( 3, MXR8LV ), toth2 ( 2, MXR8LV )
    REAL*8 :: hdr_save  ( NHR8PM ), adpsfc_hdr_save(5),                 &
              sfcshp_hdr_save(2),                                       &
              evns_save ( MXR8PM, MXR8LV, MXR8VN, MXR8VT ),             &
              drft_save ( 3, MXR8LV ), toth_save ( 2, MXR8LV )

    REAL*8      :: r8sid, r8sid2
    CHARACTER*8 :: csid, csid2
    EQUIVALENCE    ( r8sid, csid ), ( r8sid2, csid2 )

    REAL*8      :: pob1, pob2
    REAL        :: p ( MXR8LV )

    INTEGER     :: indr ( MXR8LV )

    LOGICAL     ::  seq_match = .FALSE.
    LOGICAL     ::  single_msgtyp, radiance, not_radiance

    INTEGER,     SAVE :: idate2
    CHARACTER*8, SAVE :: msgtp2
    LOGICAL,     SAVE :: match = .true.
!-----------------------------------------------------------------------

    !CHARACTER*8 :: msgtyp_process
    REAL*8      :: seq, seq2
    INTEGER     :: nlevd, jret, kk, nlevt, nlev2, nlev2d, nlev2t
    INTEGER     :: lv2, lv1, ii, jj, j, iset

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    single_msgtyp = .false.    ! set to true if you want to process
                              !  reports from only ONE message type

    !IF (inset > 0) THEN
    !  !msgtyp_process = in_msgtyp ! if single_msgtyp=T, then only
    !  !                           !  reports in this msgtyp will be
    !  !                           !  processed
    !  single_msgtyp = .TRUE.
    !END IF

    1000 CONTINUE

    iret = 0

    !  If the previous call to this subroutine did not yield matching
    !   mass and wind subsets, then BUFRLIB routine READNS is already
    !   pointing at an unmatched subset.  Otherwise, call READNS to
    !   advance the subset pointer to the next subset.
    !  --------------------------------------------------------------

    IF  ( match )  THEN
      CALL READNS(lunit,msgtyp,idate,jret) ! BUFRLIB routine to
                                           !  read in next subset
      IF  ( jret .ne. 0 )  THEN
         iret = -1 ! there are no more subsets in the PREPBUFR file
                   !  and all subsets have been processed
         RETURN
      END IF

      IF(msgtyp_set) THEN
        iset = in_subsets(msgtyp,in_msgtyps,in_msgsize)
        IF (iset <= 0 ) GO TO 1000
      END IF
    ELSE
      msgtyp = msgtp2
      idate = idate2
    END IF

    !  Read the report header based on mnemonic string "head" and
    !   transfer 1 for 1 into array HDR for the subset that is
    !   currently being pointed to.
    !  -----------------------------------------------------------

                       !BUFRLIB routine to read specific info from
                       ! report (no event info though)
    CALL UFBINT(lunit,hdr,           12,1,jret,head1)
    CALL UFBINT(lunit,hdr(13),NHR8PM-12,1,jret,head2)

    !  Read header information that is specific to the various
    !   message types
    !  -------------------------------------------------------

    qkswnd_hdr=10E10
    aircar_hdr=10E10
    aircft_hdr=10E10
    adpupa_hdr=10E10
    goesnd1_hdr=10E10
    goesnd2_hdr=10E10
    adpsfc_hdr=10E10
    sfcshp_hdr=10E10
    satwnd_hdr=10E10

    IF( msgtyp .eq. 'QKSWND')  THEN
       CALL UFBINT(lunit,qkswnd_hdr,3,1,jret,'CTCN ATRN SPRR ')
    ELSE IF( msgtyp .eq. 'AIRCAR')  THEN
       CALL UFBINT(lunit,aircar_hdr,6,1,jret,                           &
         'PCAT POAF TRBX10 TRBX21 TRBX32 TRBX43 ')
    ELSE IF( msgtyp .eq. 'AIRCFT')  THEN
       !CALL UFBINT(lunit,aircft_hdr,4,255,jret,'RCT PCAT POAF DGOT ')
       !print *, 'jret = ',jret
    ELSE IF( msgtyp .eq. 'ADPUPA')  THEN
       CALL UFBINT(lunit,adpupa_hdr,1,1,jret,'SIRC ')
    ELSE IF( msgtyp .eq. 'GOESND' .or.  msgtyp .eq. 'SATEMP' )  THEN
       CALL UFBINT(lunit,goesnd1_hdr,6,1,jret,                          &
                    'ACAV ELEV SOEL OZON TMSK CLAM')
       IF( msgtyp .eq. 'GOESND' ) CALL UFBINT(lunit,goesnd2_hdr,1,MXR8LV,jret,'PRSS ')
    ELSE IF( msgtyp .eq. 'ADPSFC')  THEN
       CALL UFBINT(lunit,adpsfc_hdr,5,1,jret,'PMO PMQ ALSE SOB SQM ')
    ELSE IF( msgtyp .eq. 'SFCSHP')  THEN
       CALL UFBINT(lunit,sfcshp_hdr,2,1,jret,'PMO PMQ ')
    ELSE IF( msgtyp .eq. 'SATWND')  THEN
       CALL UFBINT(lunit,satwnd_hdr,1,1,jret,'RFFL ')
    END IF

    !  From PREPBUFR report type, determine if this report contains
    !   only non-radiance data, only radiance data, or both
    !  ------------------------------------------------------------

    radiance = (  nint(hdr(8)) .eq. 102 .or.                            &
                 ( nint(hdr(8)) .ge. 160 .and. nint(hdr(8)) .le. 179 ))
    not_radiance = ( .not.radiance .or.                                 &
               ( nint(hdr(8)) .ge. 160 .and. nint(hdr(8)) .le. 163 )    &
          .or. ( nint(hdr(8)) .ge. 170 .and. nint(hdr(8)) .le. 173 ))

    EVNS = 10E10
    BRTS = 10E10
    nlev = 0
    nchn = 0

    IF ( not_radiance )  THEN

      !  For reports with non-radiance data, read the report level data
      !   for the variable types based on the variable-specific mnemonic
      !   string "ostr(kk)" and transfer all levels, all events 1 for 1
      !   into array EVNS for the subset that is currently being pointed
      !   to.
      !  ---------------------------------------------------------------

      DO kk = indxvr1(nint(hdr(8))),indxvr2(nint(hdr(8)))
                                 ! loop through the variables
        CALL UFBEVN(lunit,evns(1,1,1,kk),MXR8PM,MXR8LV,MXR8VN,nlev,ostr(kk))
                        ! BUFRLIB routine to read specific info from
                        ! report, accounting for all possible events
      END DO

      drft=10E10
      toth=10E10
      IF( msgtyp .eq. 'ADPUPA')  THEN

        !  Read balloon drift level data for message type ADPUPA
        !  -----------------------------------------------------

        CALL UFBINT(lunit,drft,3,MXR8LV,nlevd,'HRDR YDR XDR ')
        IF(nlevd .ne. nlev) THEN
          iret = -22
          RETURN
        END IF

        !  Read virtual temperature and dewpoint temperature (through
        !   "PREPRO" step only) data for message type ADPUPA
        !  ----------------------------------------------------------

        CALL UFBINT(lunit,toth,2,MXR8LV,nlevt,'TVO TDO ')
        if(nlevt .ne. nlev) THEN
          iret = -33
          RETURN
        END IF

      END IF
    END IF

    IF ( radiance)  THEN

    !  For reports with radiance data, read the report channel data
    !   for the variable types based on the mnemonic string
    !   "CHNM TMBR" and transfer all channels 1 for 1 into array BRTS
    !   for the subset that is currently being pointed to.
    !  --------------------------------------------------------------

        CALL UFBINT(lunit,brts,MXR8PM,MXR8LV,nchn,'CHNM TMBR')

    END IF

    2000 CONTINUE

    !  Now, advance the subset pointer to the next subset and read
    !   its report header.
    !  -----------------------------------------------------------

    CALL READNS(lunit,msgtp2,idate2,jret)
    IF  ( jret .ne. 0 )  THEN
       iret = 1 ! there are no more subsets in the PREPBUFR file
                !  but we must still return and process the
                !  previous report
       RETURN
    END IF
    IF(msgtyp_set) THEN
      iset = in_subsets(msgtp2,in_msgtyps,in_msgsize)
      IF (iset <= 0 ) GO TO 2000
    END IF

    !IF(single_msgtyp .AND. msgtp2 .NE. msgtyp_process) GO TO 2000
    CALL UFBINT(lunit,hdr2,           12,1,jret,head1)
    CALL UFBINT(lunit,hdr2(13),NHR8PM-12,1,jret,head2)

    !  Next, check whether this subset and the previous one are
    !   matching mass and wind components for a single "true" report.
    !  --------------------------------------------------------------

    match = .true.
    seq_match = .false.

    IF ( max( hdr (12), hdr2 (12) ) .le. R8BFMS )  THEN
       IF ( hdr (13)  .gt. R8BFMS )  hdr (13) = 0.
       IF ( hdr2(13)  .gt. R8BFMS )  hdr2(13) = 0.
       seq  = (hdr (13) * 1000000.) + hdr (12) ! combine seq. nunmber
       seq2 = (hdr2(13) * 1000000.) + hdr2(12) !  and MPI proc. number
       IF  ( seq .ne. seq2 )  THEN
          match = .false.
          RETURN  ! the two process/sequence numbers do not match,
                  !  return and process information in common /PREPBC/
       ELSE
          seq_match = .true.  ! the two process/sequence numbers match
       END IF
    END IF

    IF  ( msgtyp .ne. msgtp2 )  THEN
        match = .false.
        RETURN   ! the two message types do not match, return and
                 !  process information in common /PREPBC/
    END IF

    IF ( .not. seq_match )  THEN

    !  The two process/sequence numbers are missing, so as a last
    !   resort must check whether this subset and the previous one
    !   have identical values for SID, YOB, XOB, ELV, and DHR in
    !   which case they match.
    !  -----------------------------------------------------------

       r8sid  = hdr (1)
       r8sid2 = hdr2(1)
       IF  ( csid .ne. csid2 )  THEN
           match = .false.
           RETURN   ! the two report id's do not match, return and
                    !  process information in common /PREPBC/
       END IF

       DO ii = 5, 2, -1
          IF  ( hdr (ii) .ne. hdr2 (ii) )  THEN
             match = .false.
             RETURN   ! the two headers do not match, return and
                      !  process information in common /PREPBC/
          END IF
       END DO
    END IF

    !  The two headers match, read level data for the second subset.
    !   (Note: This can only happen for non-radiance reports)
    !  -------------------------------------------------------------

    DO kk = indxvr1(nint(hdr(8))),indxvr2(nint(hdr(8)))
                               ! loop through the variables
      CALL UFBEVN(lunit,evns2 (1,1,1,kk),MXR8PM,MXR8LV,MXR8VN,nlev2,ostr(kk))
    END DO

    drft2=10E10
    toth2=10E10
    IF( msgtp2 .eq. 'ADPUPA')  THEN

      !  Read balloon drift level data for message type ADPUPA
      !   for the second subset
      !  -----------------------------------------------------

       CALL UFBINT(lunit,drft2,3,MXR8LV,nlev2d,'HRDR YDR XDR ')
       if(nlev2d .ne. nlev2)  THEN
         iret = -22
         RETURN
       END IF

       !  Read virtual temperature and dewpoint temperature (through
       !   "PREPRO" step only) data for message type ADPUPA for the
       !   second subset
       !  ----------------------------------------------------------

       CALL UFBINT(lunit,toth2,2,MXR8LV,nlev2t,'TVO TDO ')
       if(nlev2t .ne. nlev2) THEN
         iret = -33
         RETURN
       END IF

    END IF

    !  Read header information that is specific to the various
    !   message types for the second supset
    !  -------------------------------------------------------

    qkswnd_hdr2=10E10
    aircar_hdr2=10E10
    aircft_hdr2=10E10
    adpupa_hdr2=10E10
    goesnd1_hdr2=10E10
    goesnd2_hdr2=10E10
    adpsfc_hdr2=10E10
    sfcshp_hdr2=10E10
    satwnd_hdr2=10E10
    IF( msgtp2 .eq. 'QKSWND')  THEN
       CALL UFBINT(lunit,qkswnd_hdr2,3,1,jret,'CTCN ATRN SPRR ')
    ELSE IF( msgtp2 .eq. 'AIRCAR')  THEN
       CALL UFBINT(lunit,aircar_hdr2,6,1,jret,                        &
        'PCAT POAF TRBX10 TRBX21 TRBX32 TRBX43 ')
    ELSE IF( msgtp2 .eq. 'AIRCFT')  THEN
       !CALL UFBINT(lunit,aircft_hdr2,4,255,jret,'RCT PCAT POAF DGOT ')
       !print *, 'jret = ',jret
    ELSE IF( msgtp2 .eq. 'ADPUPA')  THEN
       CALL UFBINT(lunit,adpupa_hdr2,1,1,jret,'SIRC ')
    ELSE IF( msgtp2 .eq. 'GOESND' .or.  msgtp2 .eq. 'SATEMP' )  THEN
       CALL UFBINT(lunit,goesnd1_hdr2,6,1,jret,                       &
                   'ACAV ELEV SOEL OZON TMSK CLAM')
       IF( msgtp2 .eq. 'GOESND' ) CALL UFBINT(lunit,goesnd2_hdr2,1,   &
                                              MXR8LV,jret,'PRSS ')
    ELSE IF( msgtp2 .eq. 'ADPSFC')  THEN
       CALL UFBINT(lunit,adpsfc_hdr2,5,1,jret,'PMO PMQ ALSE SOB SQM ')
    ELSE IF( msgtp2 .eq. 'SFCSHP')  THEN
       CALL UFBINT(lunit,sfcshp_hdr2,2,1,jret,'PMO PMQ ')
    ELSE IF( msgtp2 .eq. 'SATWND')  THEN
       CALL UFBINT(lunit,satwnd_hdr2,1,1,jret,'RFFL ')
    END IF

    IF (nint(hdr(8)) .ge. 280)  THEN

       !  If this is a surface report, the wind subset precedes the
       !   mass subset - switch the subsets around in order to combine
       !   the surface pressure properly
       !  ------------------------------------------------------------

       evns_save = evns2
       evns2 = evns
       evns = evns_save
       hdr_save = hdr2
       hdr2 = hdr
       hdr = hdr_save
       adpsfc_hdr_save = adpsfc_hdr2
       adpsfc_hdr2 = adpsfc_hdr
       adpsfc_hdr = adpsfc_hdr_save
       sfcshp_hdr_save = sfcshp_hdr2
       sfcshp_hdr2 = sfcshp_hdr
       sfcshp_hdr = sfcshp_hdr_save
    END IF

    !  Combine the message type-specific header data for the two
    !   matching subsets into a single array.
    !  ---------------------------------------------------------

    qkswnd_hdr(:) = min(qkswnd_hdr(:),qkswnd_hdr2(:))
    aircar_hdr(:) = min(aircar_hdr(:),aircar_hdr2(:))
    aircft_hdr(:) = min(aircft_hdr(:),aircft_hdr2(:))
    adpupa_hdr(:) = min(adpupa_hdr(:),adpupa_hdr2(:))
    goesnd1_hdr(:) = min(goesnd1_hdr(:),goesnd1_hdr2(:))
    goesnd2_hdr(:) = min(goesnd2_hdr(:),goesnd2_hdr2(:))
    adpsfc_hdr(:) = min(adpsfc_hdr(:),adpsfc_hdr2(:))
    sfcshp_hdr(:) = min(sfcshp_hdr(:),sfcshp_hdr2(:))
    satwnd_hdr(:) = min(satwnd_hdr(:),satwnd_hdr2(:))

    !  Combine the data for the two matching subsets into a single 4-D
    !   array.  Do this by merging the EVNS2 array into the EVNS array.
    !  ----------------------------------------------------------------

    LOOP1: DO lv2 = 1, nlev2  ! loop through the levels of subset 2
       DO lv1 = 1, nlev  ! loop through the levels of subset 1
          pob1 = evns  ( 1, lv1, 1, 1 )
          pob2 = evns2 ( 1, lv2, 1, 1 )
          IF  ( pob1 .eq. pob2 )  THEN

             !  This pressure level from the second subset also exists in the
             !   first subset, so overwrite any "missing" piece of data for
             !   this pressure level in the first subset with the corresponding
             !   piece of data from the second subset (since this results in no
             !   net loss of data!).
             !  ---------------------------------------------------------------

             DO kk = indxvr1(nint(hdr(8))),indxvr2(nint(hdr(8)))
                               ! loop through the variables
                DO jj = 1,MXR8VN  ! loop through the events
                   DO ii = 1,MXR8PM-1 ! loop through BUFR parameters
                                      ! skip the CAT parameter
                      IF ( evns (ii,lv1,jj,kk ) .gt. R8BFMS ) THEN
                         evns  ( ii, lv1, jj, kk ) = evns2 ( ii, lv2, jj, kk )
                         IF ( evns2 (ii,lv2,jj,kk).le.R8BFMS) THEN

             !  If any piece of data in the first subset is missing and the
             !   corresponding piece of data is NOT missing in the second
             !   subset, also overwrite the PREPBUFR category parameter in the
             !   first subset with that from the second subset regardless of
             !   its value in either subset
             !  --------------------------------------------------------------

                            evns  (  8, lv1, jj, kk ) = evns2 (  8, lv2, jj, kk )
                         END IF
                      END IF
                   END DO
                END DO
             END DO
             CYCLE LOOP1
          ELSE IF  (  ( pob2 .gt. pob1 )  .or. ( lv1  .eq. nlev )  )  THEN

            !  Either all remaining pressure levels within the first subset
            !   are less than this pressure level from the second subset
            !   (since levels within each subset are guaranteed to be in
            !   descending order wrt pressure!) *OR* there are more total
            !   levels within the second subset than in the first subset.
            !   In either case, we should now add this second subset level
            !   to the end of the EVNS array.
            !  ------------------------------------------------------------

             nlev = nlev + 1
             evns  ( :, nlev, :, : ) = evns2 ( :, lv2,  :, : )
             drft  ( :, nlev ) = drft2 ( :, lv2 )
             toth  ( :, nlev ) = toth2 ( :, lv2 )
             CYCLE LOOP1
          END IF
       END DO
    END DO  LOOP1

    !  Sort combined report according to decreasing pressure.
    !  ------------------------------------------------------

    p(:) = evns  ( 1, :, 1, 1)
    if(nlev .gt. 0)  call indexf(nlev,p,indr)
    evns_save = evns
    drft_save = drft
    toth_save = toth
    do i = 1,nlev
       j = indr(nlev-i+1)
       evns ( :, i, :, :) = evns_save ( :, j, :, :)
       drft ( :, i) = drft_save ( :, j)
       toth ( :, i) = toth_save ( :, j)
    end do

    !  Return to calling program with a complete report
    !
    !  ----------------------------------------------------------

    RETURN

  END SUBROUTINE READPB

!#######################################################################
!
!$$$  SUBPROGRAM DOCUMENTATION BLOCK
!                .      .    .                                       .
! SUBPROGRAM:    INDEXF      GENERAL SORT ROUTINE FOR REAL ARRAY
!   PRGMMR: D. A. KEYSER     ORG: NP22       DATE: 1995-05-30
!
! ABSTRACT: USES EFFICIENT SORT ALGORITHM TO PRODUCE INDEX SORT LIST
!   FOR AN REAL ARRAY.  DOES NOT REARRANGE THE FILE.
!
! PROGRAM HISTORY LOG:
! 1993-06-05  R  KISTLER -- FORTRAN VERSION OF C-PROGRAM
! 1995-05-30  D. A. KEYSER - TESTS FOR < 2 ELEMENTS IN SORT LIST,
!             IF SO RETURNS WITHOUT SORTING (BUT FILLS INDX ARRAY)
!
! USAGE:    CALL INDEXF(N,ARRIN,INDX)
!   INPUT ARGUMENT LIST:
!     N        - SIZE OF ARRAY TO BE SORTED
!     ARRIN    - REAL ARRAY TO BE SORTED
!
!   OUTPUT ARGUMENT LIST:
!     INDX     - ARRAY OF POINTERS GIVING SORT ORDER OF ARRIN IN
!              - ASCENDING ORDER {E.G., ARRIN(INDX(I)) IS SORTED IN
!              - ASCENDING ORDER FOR ORIGINAL I = 1, ... ,N}
!
! REMARKS: NONE.
!
! ATTRIBUTES:
!   LANGUAGE: FORTRAN 90
!   MACHINE:  IBM-SP
!
!$$$
!
!#######################################################################

  SUBROUTINE INDEXF(N,ARRIN,INDX)
    INTEGER, INTENT(IN)  :: N
    INTEGER, INTENT(OUT) :: INDX(N)
    REAL,    INTENT(IN)  :: ARRIN(N)
  !---------------------------------------------------------------------

    INTEGER :: j, L, IR, INDXT
    REAL    :: AA

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

      DO J = 1,N
         INDX(J) = J
      ENDDO
  ! MUST BE > 1 ELEMENT IN SORT LIST, ELSE RETURN
      IF(N.LE.1)  RETURN

      L = N/2 + 1
      IR = N

      33 CONTINUE
      IF(L.GT.1)  THEN
         L = L - 1
         INDXT = INDX(L)
         AA = ARRIN(INDXT)
      ELSE
         INDXT = INDX(IR)
         AA = ARRIN(INDXT)
         INDX(IR) = INDX(1)
         IR = IR - 1
         IF(IR.EQ.1)  THEN
            INDX(1) = INDXT
            RETURN
         END IF
      END IF
      I = L
      J = L * 2
      30 CONTINUE
      IF(J.LE.IR)  THEN
         IF(J.LT.IR)  THEN
            IF(ARRIN(INDX(J)).LT.ARRIN(INDX(J+1)))  J = J + 1
         END IF
         IF(AA.LT.ARRIN(INDX(J)))  THEN
            INDX(I) = INDX(J)
            I = J
            J = J + I
         ELSE
            J = IR + 1
         END IF
      END IF
      IF(J.LE.IR)  GO TO 30
      INDX(I) = INDXT
      GO TO 33
  END SUBROUTINE INDEXF

!#######################################################################

  INTEGER FUNCTION in_subsets(sname, snames, msize)

    INTEGER, INTENT(IN) :: msize
    CHARACTER(8)        :: snames(msize), sname

    INTEGER :: i

    IF (msize < 1) THEN
      in_subsets = 0
      RETURN
    END IF

    DO i = 1, msize
      IF (sname == snames(i)) THEN
        in_subsets = i
        RETURN
      END IF
    END DO

    in_subsets = 0
    RETURN
  END FUNCTION in_subsets

!
!##################################################################
!##################################################################
!######                                                      ######
!######             SUBROUTINE READ_PREPBUFR_OBS             ######
!######                                                      ######
!######                     Developed by                     ######
!######        National Severe Storm Laboratory, NOAA        ######
!######                                                      ######
!##################################################################
!##################################################################
!
  SUBROUTINE read_prepbufr_obs(infile, rmiss, intime, istatus)
  !
  !-----------------------------------------------------------------------
  !
  !  PURPOSE:
  !
  !    Routine to read conventional observations from prepbufr file.
  !
  !     Variable        Var type   I/O   Description
  !    ----------      ---------- ----- -------------
  !     infile            A*256     I    file name including directory
  !     rmiss             RA        I    Default missing value used by newsvar
  !     intime            A*24      O    Data time of background in dd-mmm-yyyy hh:mm
  !
  !     nstn              I         O    Number observations records
  !
  !     obsdt   - time    A*24      O    reported observation time (h)
  !     obs(1)  - lat     RA        O    Station latitude (deg)
  !     obs(2)  - lon     RA        O    Station longitude (deg)
  !     obs(3)  - elev    RA        O    Station elevation (m)
  !     obs(4)  - height  RA        O    Observation height (m)
  !     obs(5)  - press   RA        O    Station pressure (Pascal)
  !     obs(6)  - T       RA        O    Temperature (K)
  !     obs(7)  - qv      RA        O    Specific humidity (kg/kg)
  !     obs(8)  - u       RA        O    U-comp wind (m/s)
  !     obs(9)  - v       RA        O    V-comp wind (m/s)
  !     obs(10) - pw      RA        O    GPS integrated precipitable water
  !     obs(11) - btype   RA        O    Observation bufr type
  !     istatus           I         O    Status flag: 0 = normal
  !                                                  -1 = file not found
  !                                                  -2 = Arrays too small
  !
  !-----------------------------------------------------------------------
  !
  !  AUTHOR: Yunheng Wang (02/20/2019)
  !
  !  MODIFICATION HISTORY:
  !
  !          Sijie Pan(03/13/2019)
  !
  !-----------------------------------------------------------------------
  !
  !
    USE model_precision

    IMPLICIT NONE
    CHARACTER (LEN=*),  INTENT(IN)  :: infile
    REAL,               INTENT(IN)  :: rmiss
    CHARACTER (LEN=19), INTENT(IN)  :: intime

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    !
    ! Hard coded prepbufr types used by newsvar system.
    ! Only observation types used by NEWS-E system (monitored observations are
    ! excluded) are adopted here.
    ! For details about PREPBUFR report types, please visit:
    ! https://www.emc.ncep.noaa.gov/mmb/data_processing/prepbufr.doc/table_2.htm
    !
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    CHARACTER(8)  :: set_msgtyp(13) = (/'ADPSFC', 'SFCSHP', 'AIRCAR',   &
                                        'AIRCFT', 'ADPUPA', 'RASSDA',   &
                                        'PROFLR', 'VADWND', 'SATWND',   &
                                        'WDSATR', 'GPSIPW', 'SYNDAT',   &
                                        'ASCATW'/)

    !-----------------------------------------------------------------------
    !
    ! Misc Local Variables
    !
    !-----------------------------------------------------------------------

    CHARACTER(10), POINTER              :: sngstnid(:), mlvstnid(:)
    REAL, POINTER, DIMENSION(:,:,:,:)   :: snglv, mlv
    INTEGER, POINTER                    :: nlev_sng(:), nlev_mlv(:)

    CHARACTER(10)       :: sid
    CHARACTER(8)        :: msgtyp
    INTEGER             :: nx_sng = 4000, nx_mlv = 400, nz_obs = 155
    INTEGER             :: k, istn, iobs, nmlvstn = 0, i
    INTEGER             :: unit_in, idate
    INTEGER             :: obsdate, bkgdate
    REAL                :: obstime, bkgtime
    INTEGER             :: duplicate, iret, istatus
    LOGICAL             :: fexist, var4d
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0
    var4d = .FALSE.

    INQUIRE(FILE=TRIM(infile),EXIST=fexist)
    IF (.NOT. fexist) THEN
      istatus = -1
      RETURN
    END IF

    CALL getunit(unit_in)
    OPEN(unit_in,FILE=TRIM(infile),ACTION='READ',FORM='UNFORMATTED')
    CALL openbf(unit_in,'IN',unit_in)
    CALL datelen(10)     ! set data format as YYYYMMDDHH

    !Allocate temporary memory for observations
    !Beside 10 stored parameter defined above, we add 2 more parameter
    !in temporary array for selecting the nearest observation at the
    !same station.
    !snglv(:,nvar_conv+1,1,obs/type) is Observation time minus analysis time (hours)
    !sngstnid(:) is station ID
    ALLOCATE(snglv(nx_sng,nvar_conv+1,1,obsdim))
    ALLOCATE(sngstnid(nx_sng))
    snglv      = rmiss
    sngstnid   = ''
    !For multi-level observations, the data array has three dimensions
    !E.g, mlv(istn,jvar,klv,obs/type) where istn -> ith station,
    !jvar -> jth variable, klv -> kth level
    ALLOCATE(mlv(nx_mlv,nvar_conv+1,nz_obs,obsdim))
    ALLOCATE(mlvstnid(nx_mlv))
    ALLOCATE(nlev_mlv(nx_mlv))
    mlv        = rmiss
    mlvstnid   = ''
    nlev_mlv   = 0

    !.....  Read the prepbufr messages
    CALL READPB(unit_in,.TRUE.,set_msgtyp,13,msgtyp,idate,iret)
    DO WHILE (iret >= 0)

      duplicate = 0  ! .FALSE.
      bufrtype(1) = nint(hdr(8))
      btypemin = bufrtype(1)
      btypew = bufrtype(1)
      IF (nint(hdr2(8)) >= 100) THEN
        bufrtype(2) = nint(hdr2(8))
        btypemin = MINVAL(bufrtype(:))
        btypew = MAXVAL(bufrtype(:))
        IF (btypemin >= 200 .OR. btypew < 200 ) btypew = btypemin
      END IF

      ! Looking for specific report type that is defined by rpttyp
      IF (ANY(btypemin == INT(rpttyp(:, 6))) .OR. ANY(btypew == INT(rpttyp(:, 6)))) THEN

        IF (.NOT. ANY(btypemin == INT(rpttyp(:, 6)))) btypemin =  btypew
        IF (.NOT. ANY(btypew == INT(rpttyp(:, 6)))) btypew =  btypemin

        sondeobs = .FALSE.
        replace  = .FALSE.
        WRITE(sid, '(a10)') hdr(1)

        ! Calculate Observation time minus analysis time.
        ! Assume that the time difference between analysis time
        ! and observation time is less than 1 month (whatever it
        ! is 28, 29, 30, 31 days).
        ! A positive value means that the observation time is ahead of
        ! analysis time, or vice versa.
        obsdate = idate/100
        obstime = hdr(6)
        CALL decode_datetime(intime, bkgdate, bkgtime)
        timediff = converttime(obsdate, obstime, bkgdate, bkgtime)
        irpttypmin = find_value(btypemin, nrpttyp, rpttyp(:, 6))
        irpttypw = find_value(btypew, nrpttyp, rpttyp(:, 6))
        maxwindow = MAXVAL(ABS(rpttyp(irpttypmin, 1:5)))
        IF (btypew >= 200) maxwindow = MAX(maxwindow, MAXVAL(ABS(rpttyp(irpttypw, 1:5))))

        ! for single level observation
        IF (nlev == 1 .AND. ABS(timediff) <= maxwindow .AND.     &
            MINVAL(evns(2,1,:,:)) <= assimquality) THEN

          IF ( .NOT. var4d ) THEN

            duplicate = check_duplicate(sid, sngstnid, nstn)
            IF ( duplicate > 0 ) duplicate = check_location(snglv, duplicate)

            IF ( duplicate == 0 ) THEN

              IF ( nstn == nx_sng ) CALL incsize(snglv, sngstnid, nlev_sng,     &
                                       nx_sng, nvar_conv, 1, obsdim, 1, rmiss)
              nstn    = nstn + 1   ! total number of stations
              nobs    = nobs + 1   ! total number of observations
              sngstnid(nstn) = sid ! Station ID

              CALL obsreserve(snglv, nstn, nlev, nvar_conv, rmiss)

            ELSE   ! has duplicated record in temporary array

              IF (ABS(timediff) <= ABS(snglv(duplicate, nvar_conv+1, 1, 1))) replace = .TRUE.

              CALL obsreplace(snglv, duplicate, nvar_conv, rmiss)

            END IF

          END IF ! 4DVAR OPTION (not implemented yet)

        ! for multi-level observation
        ELSE IF (nlev > 1 .AND. ABS(timediff) <= maxwindow .AND. &
            MINVAL(evns(2, :, :, :)) <= assimquality) THEN

          IF ( .NOT. var4d ) THEN

            IF (ANY(btypemin == sondetypes(:)) .OR. ANY(btypew == sondetypes(:))) sondeobs = .TRUE.
            duplicate = check_duplicate(sid, mlvstnid, nmlvstn)

            IF ( duplicate == 0 ) THEN

              IF ( nmlvstn == nx_mlv ) CALL incsize(mlv, mlvstnid, nlev_mlv,    &
                                          nx_mlv, nvar_conv, nz_obs, obsdim, 1, rmiss)
              nmlvstn           = nmlvstn + 1   ! total number of stations with multi-level observations
              mlvstnid(nmlvstn) = sid           ! Station ID for multi-level observations
              nlev_mlv(nmlvstn) = nlev          ! Number of observational levels at a particular station

              DO k = 1, nlev
                IF ( k == nz_obs ) CALL incsize(mlv, mlvstnid, nlev_mlv,        &
                                      nx_mlv, nvar_conv, nz_obs, obsdim, 3, rmiss)

                CALL obsreserve(mlv, nmlvstn, k, nvar_conv, rmiss)
              END DO

            ELSE

              IF (ABS(timediff) <= ABS(mlv(duplicate, nvar_conv+1, 1, 1))) replace = .TRUE.

              IF ( replace ) THEN
                nlev_mlv(duplicate)     = nlev
                mlv(duplicate, :, :, :)    = rmiss

                DO k = 1, nlev
                  IF ( k == nz_obs ) CALL incsize(mlv, mlvstnid, nlev_mlv,      &
                                        nx_mlv, nvar_conv, nz_obs, obsdim, 3, rmiss)

                  CALL obsreserve(mlv, duplicate, k, nvar_conv, rmiss)
                END DO
              END IF

            END IF

          END IF

        END IF

      END IF ! check report type

      IF (iret /= 0) EXIT
      CALL READPB(unit_in,.TRUE.,set_msgtyp,13,msgtyp,idate,iret)

    END DO

    CALL closbf(unit_in)
    CALL retunit(unit_in)

    ! Store observations in a sequential array
    iobs = nobs
    nobs = nobs + SUM(nlev_mlv(1:nmlvstn))
    IF (nobs > 0) THEN
      ALLOCATE(obs(nobs,nvar_conv,2))
      obs(1:nstn, :, :) = snglv(1:nstn,1:nvar_conv,1,:)
      nstn = nstn + nmlvstn
      DO istn = 1, nmlvstn
        DO k = 1, nlev_mlv(istn)
          iobs = iobs + 1
          obs(iobs, :, :) = mlv(istn, 1:nvar_conv, k, :)
        END DO
      END DO
    END IF

    ! Finalization
    DEALLOCATE(sngstnid, snglv)
    DEALLOCATE(mlvstnid, mlv, nlev_mlv)
    NULLIFY(sngstnid, snglv)
    NULLIFY(mlvstnid, mlv, nlev_mlv)
  !
  !-----------------------------------------------------------------------
  !
  !..... End of data gathering. Let's go home...
  !
  !-----------------------------------------------------------------------

    WRITE(*, '(1x,a)') 'Normal completion of READ_PREPBUFR_OBS'

    RETURN

  END SUBROUTINE read_prepbufr_obs

!#######################################################################

  INTEGER FUNCTION find_value(value, length, vector)

    INTEGER, INTENT(IN)         :: value, length
    REAL, INTENT(IN)            :: vector(length)

    INTEGER                     :: i

    find_value = -1

    DO i = 1, length
      IF (value == INT(vector(i))) THEN
        find_value = I
        RETURN
      END IF
    END DO

    RETURN
  END FUNCTION find_value

!#######################################################################

  INTEGER FUNCTION check_duplicate(stnid, stnset, setsize)

    INTEGER, INTENT(IN)         :: setsize
    CHARACTER(10), POINTER      :: stnset(:)
    CHARACTER(10), INTENT(IN)   :: stnid

    INTEGER :: i

    check_duplicate = 0
    IF (setsize < 1) THEN
      RETURN
    ELSE
      DO i = 1, setsize
        IF (stnid == stnset(i)) THEN
          check_duplicate = i
          RETURN
        END IF
      END DO
    END IF

    RETURN
  END FUNCTION check_duplicate

!#######################################################################

  INTEGER FUNCTION check_location(tem, istn)

    REAL, POINTER               :: tem(:, :, :, :)
    INTEGER, INTENT(IN)         :: istn

    REAL                        :: newlat, newlon, newelev

    check_location = istn
    newlat    = hdr(2)
    newlon    = hdr(3)
    newelev   = hdr(4)
    IF (newlat /= tem(istn,1,1,1) .OR. newlon /= tem(istn,2,1,1) .OR. &
        newelev /= tem(istn,3,1,1)) check_location = 0

    RETURN
  END FUNCTION check_location

!#######################################################################

  REAL FUNCTION converttime(obsd, obst, bkgd, bkgt)

    INTEGER, INTENT(IN)         :: obsd, bkgd
    REAL, INTENT(IN)            :: obst, bkgt

    IF (obsd == bkgd) THEN
        converttime = obst - bkgt
    ELSE IF (obsd > bkgd) THEN
        converttime = (obst + (obsd - bkgd) * 24.0) - bkgt
    ELSE
        converttime = obst - (bkgt + (bkgd - obsd) * 24.0)
    END IF

    RETURN
  END FUNCTION converttime

!#######################################################################

  SUBROUTINE decode_datetime(timestr, date, time)

    IMPLICIT NONE
    CHARACTER (LEN=19), INTENT(IN)  :: timestr
    INTEGER, INTENT(OUT)            :: date
    REAL, INTENT(OUT)               :: time

    CHARACTER(8)                    :: dtchar
    REAL                            :: hour, minute

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    WRITE(dtchar,'(3a)') TRIM(timestr(1:4)), TRIM(timestr(6:7)), TRIM(timestr(9:10))
    READ(dtchar, '(I8)') date
    READ(timestr(15:16), *) minute
    minute = minute / 60.0
    READ(timestr(12:13), *) hour
    time = hour + minute

    RETURN

  END SUBROUTINE decode_datetime

!#######################################################################

  SUBROUTINE incsize(array1, array2, arr_nlev, nx, ny, nz, ndim, dbldim, rmiss)

    IMPLICIT NONE
    INTEGER                 :: nx, ny, nz, ndim, dbldim
    REAL, POINTER           :: array1(:,:,:,:), array3(:,:,:,:)
    CHARACTER(10), POINTER  :: array2(:), array4(:)
    INTEGER, POINTER        :: arr_nlev(:), array5(:)
    REAL, INTENT(IN)        :: rmiss

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF ( dbldim == 1 ) THEN

      ALLOCATE( array3(nx+1000, ny+1, nz, ndim) )
      array3 = rmiss
      array3(1:nx, :, :, :) = array1(1:nx, :, :, :)
      DEALLOCATE(array1)
      array1 => array3
      NULLIFY(array3)

      ALLOCATE( array4(nx+1000) )
      array4 = ''
      array4(1:nx) = array2(1:nx)
      DEALLOCATE(array2)
      array2 => array4
      NULLIFY(array4)

      IF (nz > 1) THEN
        ALLOCATE( array5(nx+1000) )
        array5 = 0
        array5(1:nx) = arr_nlev(1:nx)
        DEALLOCATE(arr_nlev)
        arr_nlev => array5
        NULLIFY(array5)
      END IF

      nx = nx + 1000

    ELSE IF ( dbldim == 3 ) THEN

      ALLOCATE( array3(nx, ny+1, nz+100, ndim) )
      array3 = rmiss
      array3(:, :, 1:nz, :) = array1(:, :, 1:nz, :)
      DEALLOCATE(array1)
      array1 => array3
      NULLIFY(array3)

      nz = nz + 100

    END IF

    RETURN

  END SUBROUTINE incsize

!#######################################################################

  SUBROUTINE latesttimereplace(tem, stnind, ilev, nvar, rmiss)

    IMPLICIT NONE
    INTEGER, INTENT(IN)         :: stnind, ilev, nvar
    REAL, INTENT(IN)            :: rmiss
    REAL, POINTER               :: tem(:, :, :, :)

    INTEGER                     :: ievent

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    ! Check quality marker before storing the observation
    ! indxvr1 ad indxvr2 indicate the types of observation that
    ! that is contained by PREPBUFR report type (bufrtype).
    ! evns(2,:,:,:) is the quality marker.
    ! For Height.
    IF ( indxvr1(btypew) <= KZOB .AND. indxvr2(btypew) >= KZOB .AND.  &
         evns(2,ilev,1,KZOB) <= assimquality ) THEN
      IF ( evns(1,ilev,1,KZOB) < R8BFMS ) THEN
        tem(stnind,4,ilev,1) = evns(1,ilev,1,KZOB)
      END IF
    END IF
    ! For Pressure.
    IF ( indxvr1(btypemin) <= KPOB .AND. indxvr2(btypemin) >= KPOB .AND.  &
         evns(2,ilev,1,KPOB) <= assimquality ) THEN
      IF ( sondeobs .OR. ABS(timediff) <= ABS(rpttyp(irpttypmin, 1)) ) THEN
        IF ( evns(1,ilev,1,KPOB) < R8BFMS ) THEN
          tem(stnind,5,ilev,1) = evns(1,ilev,1,KPOB)
          tem(stnind,5,ilev,2) = btypemin
        END IF
      END IF
    END IF
    ! For Temperature.
    IF ( indxvr1(btypemin) <= KTOB .AND. indxvr2(btypemin) >= KTOB .AND.  &
         evns(2,ilev,1,KTOB) <= assimquality .AND. ABS(timediff) <= ABS(rpttyp(irpttypmin, 2)) ) THEN
      DO ievent = 1, MXR8VN
        IF ( evns(3,ilev,ievent,KTOB) < 8.0 .AND. evns(1,ilev,ievent,KTOB) < R8BFMS ) THEN
          tem(stnind,6,ilev,1) = evns(1,ilev,ievent,KTOB)
          tem(stnind,6,ilev,2) = btypemin
          EXIT
        END IF
      END DO
    END IF
    ! For Specific Humidity.
    IF ( indxvr1(btypemin) <= KQOB .AND. indxvr2(btypemin) >= KQOB .AND.  &
         evns(2,ilev,1,KQOB) <= assimquality .AND. ABS(timediff) <= ABS(rpttyp(irpttypmin, 3)) ) THEN
      DO ievent = 1, MXR8VN
        IF ( evns(3,ilev,ievent,KQOB) < 8.0 .AND. evns(1,ilev,ievent,KQOB) < R8BFMS ) THEN
          tem(stnind,7,ilev,1) = evns(1,ilev,ievent,KQOB)
          tem(stnind,7,ilev,2) = btypemin
        END IF
      END DO
    END IF
    ! For U-component Wind.
    IF ( indxvr1(btypew) <= KUOB .AND. indxvr2(btypew) >= KUOB .AND.  &
         evns(2,ilev,1,KUOB) <= assimquality .AND. ABS(timediff) <= ABS(rpttyp(irpttypw, 4)) ) THEN
      IF ( evns(1,ilev,1,KUOB) < R8BFMS ) THEN
        tem(stnind,8,ilev,1) = evns(1,ilev,1,KUOB)
        tem(stnind,8,ilev,2) = btypew
      END IF
    END IF
    ! For V-component Wind.
    IF ( indxvr1(btypew) <= KVOB .AND. indxvr2(btypew) >= KVOB .AND.  &
         evns(2,ilev,1,KVOB) <= assimquality .AND. ABS(timediff) <= ABS(rpttyp(irpttypw, 4)) ) THEN
      IF ( evns(1,ilev,1,KVOB) < R8BFMS ) THEN
        tem(stnind,9,ilev,1) = evns(1,ilev,1,KVOB)
        tem(stnind,9,ilev,2) = btypew
      END IF
    END IF
    ! For Precipitable Water.
    IF ( indxvr1(btypemin) <= PWOB .AND. indxvr2(btypemin) >= PWOB .AND.  &
         evns(2,ilev,1,PWOB) <= assimquality .AND. ABS(timediff) <= ABS(rpttyp(irpttypmin, 5)) ) THEN
      IF ( evns(1,ilev,1,PWOB) < R8BFMS ) THEN
        tem(stnind,10,ilev,1) = evns(1,ilev,1,PWOB)
        tem(stnind,10,ilev,2) = btypemin
      END IF
    END IF

    tem(stnind,nvar+1,ilev,1) = timediff

    RETURN

  END SUBROUTINE latesttimereplace

!#######################################################################

  SUBROUTINE currenttimereplace(tem, stnind, ilev, nvar, rmiss)

    IMPLICIT NONE
    INTEGER, INTENT(IN)         :: stnind, ilev, nvar
    REAL, INTENT(IN)            :: rmiss
    REAL, POINTER               :: tem(:, :, :, :)

    INTEGER                     :: ievent

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF ( tem(stnind,4,ilev,1) == rmiss ) THEN
      IF ( indxvr1(btypew) <= KZOB .AND. indxvr2(btypew) >= KZOB .AND.  &
           evns(2,ilev,1,KZOB) <= assimquality ) THEN
        IF ( evns(1,ilev,1,KZOB) < R8BFMS ) THEN
          tem(stnind,4,ilev,1) = evns(1,ilev,1,KZOB)
        END IF
      END IF
    END IF

    IF ( tem(stnind,5,ilev,1) == rmiss ) THEN
      IF ( indxvr1(btypemin) <= KPOB .AND. indxvr2(btypemin) >= KPOB .AND.  &
           evns(2,ilev,1,KPOB) <= assimquality ) THEN
        IF ( sondeobs .OR. ABS(timediff) <= ABS(rpttyp(irpttypmin, 1)) ) THEN
          IF ( evns(1,ilev,1,KPOB) < R8BFMS ) THEN
            tem(stnind,5,ilev,1) = evns(1,ilev,1,KPOB)
            tem(stnind,5,ilev,2) = btypemin
          END IF
        END IF
      END IF
    END IF

    IF ( tem(stnind,6,ilev,1) == rmiss ) THEN
      IF ( indxvr1(btypemin) <= KTOB .AND. indxvr2(btypemin) >= KTOB .AND.  &
           evns(2,ilev,1,KTOB) <= assimquality .AND. ABS(timediff) <= ABS(rpttyp(irpttypmin, 2)) ) THEN
        DO ievent = 1, MXR8VN
          IF ( evns(3,ilev,ievent,KTOB) < 8.0 .AND. evns(1,ilev,ievent,KTOB) < R8BFMS ) THEN
            tem(stnind,6,ilev,1) = evns(1,ilev,ievent,KTOB)
            tem(stnind,6,ilev,2) = btypemin
            EXIT
          END IF
        END DO
      END IF
    END IF

    IF ( tem(stnind,7,ilev,1) == rmiss ) THEN
      IF ( indxvr1(btypemin) <= KQOB .AND. indxvr2(btypemin) >= KQOB .AND.  &
           evns(2,ilev,1,KQOB) <= assimquality .AND. ABS(timediff) <= ABS(rpttyp(irpttypmin, 3)) ) THEN
        DO ievent = 1, MXR8VN
          IF ( evns(3,ilev,ievent,KQOB) < 8.0 .AND. evns(1,ilev,ievent,KQOB) < R8BFMS ) THEN
            tem(stnind,7,ilev,1) = evns(1,ilev,ievent,KQOB)
            tem(stnind,7,ilev,2) = btypemin
            EXIT
          END IF
        END DO
      END IF
    END IF

    IF ( tem(stnind,8,ilev,1) == rmiss ) THEN
      IF ( indxvr1(btypew) <= KUOB .AND. indxvr2(btypew) >= KUOB .AND.  &
           evns(2,ilev,1,KUOB) <= assimquality .AND. ABS(timediff) <= ABS(rpttyp(irpttypw, 4)) ) THEN
        IF ( evns(1,ilev,1,KUOB) < R8BFMS ) THEN
          tem(stnind,8,ilev,1) = evns(1,ilev,1,KUOB)
          tem(stnind,8,ilev,2) = btypew
        END IF
      END IF
    END IF

    IF ( tem(stnind,9,ilev,1) == rmiss ) THEN
      IF ( indxvr1(btypew) <= KVOB .AND. indxvr2(btypew) >= KVOB .AND.  &
           evns(2,ilev,1,KVOB) <= assimquality .AND. ABS(timediff) <= ABS(rpttyp(irpttypw, 4)) ) THEN
        IF ( evns(1,ilev,1,KVOB) < R8BFMS ) THEN
          tem(stnind,9,ilev,1) = evns(1,ilev,1,KVOB)
          tem(stnind,9,ilev,2) = btypew
        END IF
      END IF
    END IF

    IF ( tem(stnind,10,ilev,1) == rmiss ) THEN
      IF ( indxvr1(btypemin) <= PWOB .AND. indxvr2(btypemin) >= PWOB .AND.  &
           evns(2,ilev,1,PWOB) <= assimquality .AND. ABS(timediff) <= ABS(rpttyp(irpttypmin, 5)) ) THEN
        IF ( evns(1,ilev,1,PWOB) < R8BFMS ) THEN
          tem(stnind,10,ilev,1) = evns(1,ilev,1,PWOB)
          tem(stnind,10,ilev,2) = btypemin
        END IF
      END IF
    END IF

    RETURN

  END SUBROUTINE currenttimereplace

!#######################################################################

  SUBROUTINE obsreserve(tem, stnind, ilev, nvar, rmiss)

    IMPLICIT NONE
    REAL, POINTER               :: tem(:, :, :, :)
    REAL, INTENT(IN)            :: rmiss
    INTEGER, INTENT(IN)         :: stnind, ilev, nvar

    INTEGER                     :: jvar

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (sondeobs) THEN
      DO jvar = 1, 2
        tem(stnind, jvar, ilev, 1) = drft(jvar+1, ilev)
      END DO
    ELSE
      DO jvar = 1, 2
        tem(stnind, jvar, ilev, 1) = hdr(jvar+1)
      END DO
    END IF
    tem(stnind, 3, ilev, 1)     = hdr(4)

    CALL latesttimereplace(tem, stnind, ilev, nvar, rmiss)

    RETURN

  END SUBROUTINE obsreserve

!#######################################################################

  SUBROUTINE obsreplace(tem, stnind, nvar, rmiss)

    IMPLICIT NONE
    INTEGER, INTENT(IN)         :: stnind, nvar
    REAL, INTENT(IN)            :: rmiss

    REAL, POINTER               :: tem(:, :, :, :)

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (replace) THEN

      CALL latesttimereplace(tem, stnind, 1, nvar, rmiss)

    ELSE

      CALL currenttimereplace(tem, stnind, 1, nvar, rmiss)

    END IF

    RETURN

  END SUBROUTINE obsreplace

!#######################################################################

  SUBROUTINE dealloc_convraw()

    IMPLICIT NONE

    IF (ALLOCATED(obs)) DEALLOCATE(obs)
    IF (ALLOCATED(obsdt)) DEALLOCATE(obsdt)
    IF (ALLOCATED(rpttyp)) DEALLOCATE(rpttyp)

    RETURN

  END SUBROUTINE dealloc_convraw

END MODULE module_read_prepbufr
