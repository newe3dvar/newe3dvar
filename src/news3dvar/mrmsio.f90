!
!##################################################################
!##################################################################
!######                                                      ######
!######               SUBROUTINE READ_REFLECTIVITY           ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE read_reflectivity(refsrc,refile,reffmt,iboxs,iboxe,jboxs,jboxe, &
                  iorder,nx,ny,nz,xs,ys,zps,ref_mos_3d,timestr,istatus)

  IMPLICIT NONE

  INTEGER,            INTENT(IN)    :: refsrc
  INTEGER,            INTENT(IN)    :: reffmt
  INTEGER,            INTENT(INOUT) :: iboxs, iboxe,jboxs,jboxe
  INTEGER,            INTENT(IN)    :: iorder
  CHARACTER(LEN=256), INTENT(IN)    :: refile
  INTEGER,            INTENT(IN)    :: nx, ny, nz
  REAL,               INTENT(IN)    :: xs(nx),ys(ny)
  REAL,               INTENT(IN)    :: zps(nx,ny,nz)
  REAL,               INTENT(OUT)   :: ref_mos_3d(nx,ny,nz)
  CHARACTER(LEN=19),  INTENT(OUT)   :: timestr

  INTEGER,            INTENT(OUT)   :: istatus

  INCLUDE 'mp.inc'

!-----------------------------------------------------------------------

  REAL, PARAMETER :: rmissing = -9999.

  INTEGER :: i,j,k,jj

  INTEGER :: nlon, nlat, nhgt

  REAL,    ALLOCATABLE :: lat2d(:,:),  lon2d(:,:)
  !REAL,    ALLOCATABLE :: x2d(:,:),  y2d(:,:)
  INTEGER, ALLOCATABLE :: iloc(:,:), jloc(:,:)

  REAL,    ALLOCATABLE :: lonmrms(:), latmrms(:), hgtmrms(:)
  REAL,    ALLOCATABLE :: dxfld(:), dyfld(:), rdxfld(:), rdyfld(:)
  REAL,    ALLOCATABLE :: slopey(:,:,:)

  REAL,    ALLOCATABLE :: hgtxym(:,:,:)
  REAL,    ALLOCATABLE :: refmrms(:,:,:)

  REAL :: tema, latmin, latmax, lonmin, lonmax
  !
  ! MRMS specific values below
  !
  REAL,    PARAMETER :: lat1 = 20.005, lon1 = -129.995, dx1 = 0.01, dy1 = 0.01
  INTEGER, PARAMETER :: rnx  = 7000,   rny  = 3500,     rwidth = 20

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  ALLOCATE(lat2d(nx,ny), STAT = istatus)
  ALLOCATE(lon2d(nx,ny), STAT = istatus)

  CALL xytoll(nx,ny,xs,ys,lat2d,lon2d)

  !CALL print3dnc_lg(200,'lat2d',lat2d,nx,ny,1)
  !CALL print3dnc_lg(200,'lon2d',lon2d,nx,ny,1)

  ALLOCATE(iloc(nx,ny),  STAT = istatus)
  ALLOCATE(jloc(nx,ny),  STAT = istatus)

  CALL a3dmax0(lat2d,1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,latmax,latmin)
  CALL a3dmax0(lon2d,1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,lonmax,lonmin)

  iboxs = FLOOR((lonmin-lon1)/dx1); iboxe = CEILING((lonmax-lon1)/dx1)
  jboxs = FLOOR((latmin-lat1)/dy1); jboxe = CEILING((latmax-lat1)/dy1)

  iboxs = MAX(1,iboxs-rwidth); iboxe = MIN(rnx,iboxe+rwidth)
  jboxs = MAX(1,jboxs-rwidth); jboxe = MIN(rny,jboxe+rwidth)
!
!-----------------------------------------------------------------------
! Read data file
!-----------------------------------------------------------------------
!

  IF (myproc == 0) THEN
    CALL read_mrms_dims(refsrc,refile,reffmt,iboxs,iboxe,jboxs,jboxe,   &
                        nlon,nlat,nhgt,istatus)
    !WRITE(0,*) 'nlon = ',nlon,', nlat = ',nlat,', nhgt = ',nhgt
  END IF
  CALL mpupdatei(nlon,1)
  CALL mpupdatei(nlat,1)
  CALL mpupdatei(nhgt,1)

  ALLOCATE(refmrms(nlon,nlat,nhgt), STAT = istatus)
  ALLOCATE(hgtmrms(nhgt), STAT = istatus)
  ALLOCATE(latmrms(nlat), STAT = istatus)
  ALLOCATE(lonmrms(nlon), STAT = istatus)

  IF (myproc == 0) THEN
    CALL read_mrms(refsrc,refile,reffmt,nlon,nlat,nhgt,iboxs,iboxe,jboxs,jboxe, &
                   rmissing,lonmrms,latmrms,hgtmrms,refmrms,timestr,istatus)
  END IF
  CALL mpupdater(lonmrms,nlon)
  CALL mpupdater(latmrms,nlat)
  CALL mpupdater(hgtmrms,nhgt)
  CALL mpupdater(refmrms,nlon*nlat*nhgt)
  CALL mpupdatec(timestr,19)

  !IF (myproc == 0) THEN
  !  WRITE(0,*) 'max lat ',latmin, latmax
  !  WRITE(0,*) 'max lon ',lonmin, lonmax
  !  WRITE(0,*) latmrms(1:2),latmrms(nlat-1:nlat), 'nlat = ',nlat
  !  WRITE(0,*) lonmrms(1:2),lonmrms(nlon-1:nlon), 'nlon = ',nlon
  !END IF

  !WHERE (refmrms < rmissing+100.) refmrms = 0.0

  !print *, 'refmrms = ',MAXVAL(refmrms), MINVAL(refmrms)
  !IF (myproc == 0) CALL print3dnc_lc(200,'mrms_org',refmrms,nlon,nlat,nhgt,1,1)

!-----------------------------------------------------------------------
! Interpolate to the model grid
!-----------------------------------------------------------------------

  ALLOCATE(dxfld(nlon),  STAT = istatus)
  ALLOCATE(dyfld(nlat),  STAT = istatus)
  ALLOCATE(rdxfld(nlon), STAT = istatus)
  ALLOCATE(rdyfld(nlat), STAT = istatus)

  CALL setijloc(nx,ny,nlon,nlat,lon2d,lat2d,lonmrms,latmrms,iloc,jloc)
  CALL setdxdy(nlon,nlat,1,nlon,1,nlat,lonmrms,latmrms,                 &
               dxfld,dyfld,rdxfld,rdyfld)

  ALLOCATE(hgtxym(nx,ny,nhgt), STAT = istatus)

  DO k = 1, nhgt
    DO j = 1,ny
      DO i = 1, nx
        hgtxym(i,j,k) = hgtmrms(k)
      END DO
    END DO
  END DO

  !
  ! Allocate working arrays
  !
  ALLOCATE(slopey(nlon,nlat,nhgt), STAT = istatus)

  CALL fldint3dm(nx,ny,nz,nlon,nlat,nhgt,                               &
             1,nx,1,ny,1,nz,1,nlon,1,nlat,1,nhgt,                       &
             iorder,lon2d,lat2d,zps,refmrms,                            &
             lonmrms,latmrms,hgtxym,iloc,jloc,                          &
             dxfld,dyfld,rdxfld,rdyfld,slopey,rmissing,                 &
             ref_mos_3d,istatus)

  DEALLOCATE( slopey,  STAT = istatus )
  DEALLOCATE( hgtxym,  STAT = istatus )

  DEALLOCATE( dxfld, dyfld, rdxfld, rdyfld, STAT = istatus )
  DEALLOCATE( iloc, jloc,                   STAT = istatus)

!-----------------------------------------------------------------------
! Just before return
!-----------------------------------------------------------------------

  DEALLOCATE( lat2d, lon2d )
  DEALLOCATE( lonmrms, latmrms, hgtmrms )
  DEALLOCATE( refmrms )

  RETURN
END SUBROUTINE read_reflectivity
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######               SUBROUTINE WRITE_REFLECTIVITY          ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE write_reflectivity(refout,outdir,refsrc,crefopt,timestring,  &
                              nx,ny,nz,ref_mos_3d,xlat,xlon,zps,istatus)

  IMPLICIT NONE

  INTEGER,            INTENT(IN)  :: refout,refsrc
  CHARACTER(LEN=256), INTENT(IN)  :: outdir
  CHARACTER(LEN=19),  INTENT(IN)  :: timestring
  LOGICAL,            INTENT(IN)  :: crefopt
  INTEGER,            INTENT(IN)  :: nx, ny, nz
  REAL,               INTENT(IN)  :: xlat(nx,ny),xlon(nx,ny)
  REAL,               INTENT(IN)  :: zps(nx,ny,nz)
  REAL,               INTENT(OUT) :: ref_mos_3d(nx,ny,nz)
  !REAL,               INTENT(IN)  :: dx,dy

  INTEGER,            INTENT(OUT) :: istatus

  INCLUDE 'mp.inc'
  INCLUDE 'grid.inc'

!-----------------------------------------------------------------------

  REAL, PARAMETER :: rmissing = -9999.

  INTEGER :: i,j,k

  CHARACTER(LEN=256) :: refile
  CHARACTER(LEN=40)  :: srcstr, mapstr

  INTEGER :: ncid

  INTEGER, PARAMETER :: ndims = 4
  CHARACTER(LEN=40) :: dimnames(ndims) = (/'west_east  ',               &
                                           'south_north',               &
                                           'bottom_top ',               &
                                           'Time       '        /)
  CHARACTER(LEN=40) :: outvarname, outvarunit, outvarlong
  INTEGER :: dimvalues(ndims)
  INTEGER :: dimids(ndims)

  CHARACTER(LEN=40) :: iattnames (4),rattnames(8),cattnames(4)
  INTEGER           :: iattvalues(4)
  REAL              :: rattvalues(8)
  CHARACTER(LEN=80) :: cattvalues(4)
  INTEGER           :: cattlens  (4)

  CHARACTER(LEN=40) :: varnames(2)
  CHARACTER(LEN=80) :: coordstr, title

  !CHARACTER(LEN=80) :: timestr

  CHARACTER(LEN=40) :: i1dnames(1),i2dnames(1),r1dnames(1),r2dnames(4),r3dnames(2)
  CHARACTER(LEN=80) :: i1dlongs(1),i2dlongs(1),r1dlongs(1),r2dlongs(4),r3dlongs(2)
  CHARACTER(LEN=20) :: i1dunits(1),i2dunits(1),r1dunits(2),r2dunits(4),r3dunits(2)
  INTEGER           :: i1dimids(1),r1dimids(1),i2dimids(2,1),r2dimids(2,4),r3dimids(3,2)
  INTEGER           :: nr2d

  INTEGER :: wrfmapproj
  INTEGER :: nxout, nyout, nzout
  INTEGER :: nxlg, nylg, nzlg
  REAL    :: outvalidmin, outvalidmax

  REAL,    ALLOCATABLE :: lat2d(:,:),  lon2d(:,:)
  REAL,    ALLOCATABLE :: ref3d(:,:,:), hgt3d(:,:,:)
  REAL,    ALLOCATABLE :: latlg(:,:),  lonlg(:,:)
  REAL,    ALLOCATABLE :: reflg(:,:,:), hgtlg(:,:,:)
  REAL,    ALLOCATABLE :: outtem(:,:,:)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  nxlg = (nx-3)*nproc_x+1   ! use WRF dimension size
  nylg = (ny-3)*nproc_y+1
  nzlg = nz-2

  nxout = nx-2
  nyout = ny-2
  nzout = nz-2

  IF (refsrc < 3) THEN
    outvarname  = 'REFMOSAIC3D     '
    outvarunit  = 'DBZ             '
    outvalidmin = -35.0
    outvalidmax = 80.

    IF (refsrc == 0) THEN
      srcstr = 'NEXRAD'
    ELSE IF (refsrc == 1) THEN
      srcstr = 'MRMS'
    END IF

    IF (crefopt) THEN
      nzlg  = 1
      nzout = 1
      srcstr = TRIM(srcstr)//'_CREF'
      outvarlong  = 'Composite reflectivity        '
    ELSE
      srcstr = TRIM(srcstr)//'_REF3D'
      outvarlong  = '3D Reflectivity on model grid '
    END IF
  ELSE IF (refsrc == 3) THEN
    nzlg  = 1
    nzout = 1
    outvarname  = 'HOURLYPRCP      '
    outvarlong  = 'Hourly precipitation         '
    outvarunit  = 'mm              '
    outvalidmin = 0.0
    outvalidmax = 1000.
    srcstr      = 'MRMS_HPRCP'
  ELSE IF (refsrc == 4) THEN
    nzlg  = 1
    nzout = 1
    outvarname  = 'HOURLYPRCP      '
    outvarlong  = 'Hourly precipitation         '
    outvarunit  = 'mm              '
    outvalidmin = 0.0
    outvalidmax = 1000.
    srcstr      = 'ST4_HPRCP'
  END IF

  WRITE(refile,'(6a)') TRIM(outdir),TRIM(srcstr),'_',timestring,'.nc'

  IF(mapproj == 0) THEN        ! NO MAP PROJECTION
    wrfmapproj = 0
    mapstr = 'UNKNOWN'
  ELSE IF(mapproj == 1) THEN   ! POLAR STEREOGRAPHIC
    wrfmapproj = 2
    mapstr = 'POLAR STEREOGRAPHIC'
  ELSE IF(mapproj == 2) THEN   ! LAMBERT CONFORMAL
    wrfmapproj = 1
    mapstr = 'LAMBERT CONFORMAL'
  ELSE IF(mapproj == 3) THEN   ! MERCATOR
    wrfmapproj = 3
    mapstr = 'Mercator'
  ELSE
    WRITE(6,*) 'Unknown map projection: ', mapproj
    istatus = -555
    RETURN
  END IF

  IF (refout == 7) THEN

    ALLOCATE(lat2d(nxout,nyout),       STAT = istatus)
    ALLOCATE(lon2d(nxout,nyout),       STAT = istatus)
    ALLOCATE(ref3d(nxout,nyout,nzout), STAT = istatus)
    ALLOCATE(hgt3d(nxout,nyout,nzout), STAT = istatus)

    CALL array_arps2wrf_2d(nx,ny,xlat,nxout,nyout,lat2d,istatus)
    CALL array_arps2wrf_2d(nx,ny,xlon,nxout,nyout,lon2d,istatus)

    ALLOCATE(latlg(nxlg,nylg),       STAT = istatus)
    ALLOCATE(lonlg(nxlg,nylg),       STAT = istatus)
    ALLOCATE(reflg(nxlg,nylg,nzlg),  STAT = istatus)
    ALLOCATE(hgtlg(nxlg,nylg,nzlg),  STAT = istatus)

    CALL wrfmerge2d(lat2d,nxout,nyout,latlg,istatus)
    CALL wrfmerge2d(lon2d,nxout,nyout,lonlg,istatus)

    IF (nzout == 1) THEN
      CALL array_arps2wrf_2d(nx,ny,zps(:,:,2),nxout,nyout,hgt3d,istatus)
      CALL array_arps2wrf_2d(nx,ny,ref_mos_3d,nxout,nyout,ref3d,istatus)

      CALL wrfmerge2d(hgt3d,nxout,nyout,hgtlg,istatus)
      CALL wrfmerge2d(ref3d,nxout,nyout,reflg,istatus)

      ALLOCATE(outtem(nxlg-1,nylg-1,1),  STAT = istatus)
    ELSE
      CALL array_arps2wrf(nx,ny,nz,zps,nxout,nyout,nzout,hgt3d,istatus)
      CALL array_arps2wrf(nx,ny,nz,ref_mos_3d,nxout,nyout,nzout,ref3d,istatus)

      CALL wrfmerge3d(hgt3d,nxout,nyout,nzout,hgtlg,istatus)
      CALL wrfmerge3d(ref3d,nxout,nyout,nzout,reflg,istatus)

      ALLOCATE(outtem(nxlg-1,nylg-1,nzlg-1),  STAT = istatus)
    END IF

    IF (myproc == 0) THEN

      CALL netopen(refile,'C',ncid)

      dimvalues(1) = nxlg-1
      dimvalues(2) = nylg-1
      dimvalues(3) = nzlg-1
      dimvalues(4) = 0
      IF (crefopt) dimvalues(3) = 1

      CALL net_def_raddims(ncid,ndims,dimnames,dimvalues,dimids,istatus)

      title = 'OUTPUT FROM NEWS3DVAR INTERPOLATION'
      cattnames(1) = 'TITLE';           cattvalues(1) = title;      cattlens(1) = LEN_TRIM(title)
      cattnames(2) = 'time';            cattvalues(2) = timestring; cattlens(2) = 19
      cattnames(3) = 'source';          cattvalues(3) = srcstr;     cattlens(3) = LEN_TRIM(srcstr)
      cattnames(4) = 'MAP_PROJ_CHAR';   cattvalues(4) = mapstr;     cattlens(4) = LEN_TRIM(mapstr)

      iattnames(1) = 'MAP_PROJ';                     iattvalues(1) = wrfmapproj
      iattnames(2) = 'BOTTOM-TOP_GRID_DIMENSION';    iattvalues(2) = dimvalues(3)
      iattnames(3) = 'SOUTH-NORTH_GRID_DIMENSION';   iattvalues(3) = dimvalues(2)
      iattnames(4) = 'WEST-EAST_GRID_DIMENSION';     iattvalues(4) = dimvalues(1)

      rattnames(1) = 'DX';           rattvalues(1) = dx
      rattnames(2) = 'DY';           rattvalues(2) = dy
      rattnames(3) = 'CEN_LAT';      rattvalues(3) = ctrlat
      rattnames(4) = 'CEN_LON';      rattvalues(4) = ctrlon
      rattnames(5) = 'TRUELAT1';     rattvalues(5) = trulat1
      rattnames(6) = 'TRUELAT2';     rattvalues(6) = trulat2
      rattnames(7) = 'MOAD_CEN_LAT'; rattvalues(7) = ctrlat
      rattnames(8) = 'STAND_LON';    rattvalues(8) = trulon

      CALL net_wrt_radatts(ncid,4,8,4,iattnames,rattnames,cattnames,    &
                     iattvalues,rattvalues,cattvalues,cattlens,istatus)

      r2dnames(1) = 'XLAT_M';  r2dimids(1:2,1) = dimids(1:2); r2dlongs(1) = 'latitude' ; r2dunits(1) = 'Degree N'
                               !r2dimids(3,1)   = dimids(4)
      r2dnames(2) = 'XLONG_M'; r2dimids(1:2,2) = dimids(1:2); r2dlongs(2) = 'longitude'; r2dunits(2) = 'Degree E'
                               !r2dimids(3,2)   = dimids(4)

      IF (crefopt) THEN
        nr2d = 4
        r2dnames(3) = 'height';   r2dimids(1:2,3) = dimids(1:2); r2dlongs(3) = 'physical heights'; r2dunits(3) = 'm'
                                  !r2dimids(3,3)   = dimids(4)
        r2dnames(4) = outvarname; r2dimids(1:2,4) = dimids(1:2); r2dlongs(4) = TRIM(outvarlong);   r2dunits(4) = TRIM(outvarunit)
                                  !r2dimids(3,4)   = dimids(4)
      ELSE
        nr2d = 2
      END IF

      istatus = 101
      CALL net_def_radvars(ncid,0,i1dnames,i1dimids,i1dlongs,i1dunits,  &
                                0,i2dnames,i2dimids,i2dlongs,i2dunits,  &
                                0,r1dnames,r1dimids,r1dlongs,r1dunits,  &
                             nr2d,r2dnames,r2dimids,r2dlongs,r2dunits,  &
                             istatus)


      IF (.NOT. crefopt) THEN
        r3dnames(1) = 'height';   r3dimids(:,1) = dimids(1:3); r3dlongs(1) = 'physical heights'; r3dunits(1) = 'm'
        r3dnames(2) = outvarname; r3dimids(:,2) = dimids(1:3); r3dlongs(2) = TRIM(outvarlong);   r3dunits(2) = TRIM(outvarunit)
        istatus = 101
        CALL net_def_radvar3d(ncid,2,r3dnames,r3dimids,r3dlongs,r3dunits,istatus)
      END IF

      CALL net_add_atts(ncid,'height',  1,(/'coordinates'/), (/"XLONG_M XLAT_M"/), istatus)

      CALL net_add_atts(ncid,outvarname,1,(/'coordinates'/), (/"XLONG_M XLAT_M"/), istatus)
      CALL net_add_attr(ncid,outvarname,1,(/'valid_min'/),   (/outvalidmin/),      istatus)
      CALL net_add_attr(ncid,outvarname,1,(/'valid_max'/),   (/outvalidmax/),      istatus)
      !CALL net_add_attr(ncid,'REFMOSAIC3D',1,(/'missing_value'/), (/-99900.0/),   istatus)
      !varnames = (/'height', 'REFMOSAIC3D'/)
      !coordstr = "lon lat"
      !CALL net_var_addcoords(ncid,2,varnames,coordstr,istatus)

      DO j = 1, nylg-1
        DO i = 1, nxlg-1
          outtem(i,j,1) = latlg(i,j)
        END DO
      END DO
      CALL netwrt2d(ncid,0,1,'XLAT_M', outtem(:,:,1), nxlg-1,nylg-1)

      DO j = 1, nylg-1
        DO i = 1, nxlg-1
          outtem(i,j,1) = lonlg(i,j)
        END DO
      END DO
      CALL netwrt2d(ncid,0,1,'XLONG_M', outtem(:,:,1), nxlg-1,nylg-1)

      IF (crefopt) THEN

        DO j = 1, nylg-1
          DO i = 1, nxlg-1
            outtem(i,j,1) = hgtlg(i,j,1)
          END DO
        END DO
        CALL netwrt2d(ncid,0,1,'height',     outtem,nxlg-1,nylg-1)

        DO j = 1, nylg-1
          DO i = 1, nxlg-1
            outtem(i,j,1) = reflg(i,j,1)
          END DO
        END DO
        CALL netwrt2d(ncid,0,1,outvarname,outtem,nxlg-1,nylg-1)

      ELSE

        DO k = 1, nzlg-1
          DO j = 1, nylg-1
            DO i = 1, nxlg-1
              outtem(i,j,k) = hgtlg(i,j,k)
            END DO
          END DO
        END DO
        CALL netwrt3d(ncid,0,1,'height',     outtem,nxlg-1,nylg-1,nzlg-1)

        DO k = 1, nzlg-1
          DO j = 1, nylg-1
            DO i = 1, nxlg-1
              outtem(i,j,k) = reflg(i,j,k)
            END DO
          END DO
        END DO
        CALL netwrt3d(ncid,0,1,outvarname,outtem,nxlg-1,nylg-1,nzlg-1)
      END IF

      CALL netclose(ncid)

    END IF
!-----------------------------------------------------------------------
! Just before return
!-----------------------------------------------------------------------

    DEALLOCATE(lat2d, lon2d)
    DEALLOCATE(ref3d, hgt3d)

    DEALLOCATE(latlg, lonlg)
    DEALLOCATE(reflg, hgtlg)

    DEALLOCATE(outtem)
  ELSE
    WRITE(0,'(1x,a,I0)') 'ERROR: Unsupported reflectivity output format, refout = ',refout
    istatus = -1
    RETURN
  END IF

  RETURN
END SUBROUTINE write_reflectivity
!
!##################################################################
!##################################################################
!######                                                      ######
!######               SUBROUTINE READ_MRMS_DIMS              ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE read_mrms_dims(refsrc,refile,reffmt,iboxs,iboxe,jboxs,jboxe, &
                          nlon,nlat,nhgt,istatus)

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: refsrc
  INTEGER, INTENT(IN)  :: reffmt
  INTEGER, INTENT(IN)  :: iboxs,iboxe,jboxs,jboxe
  CHARACTER(LEN=256), INTENT(IN) :: refile
  INTEGER, INTENT(OUT)  :: nlon, nlat, nhgt

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER :: filefmt
  LOGICAL :: crefopt
  LOGICAL :: fexists
  INTEGER :: ncid, dimid

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  IF (refsrc == 3) THEN
    nlon = 7001
    nlat = 3501
    nhgt = 1
    RETURN
  END IF

  crefopt = (reffmt/200 > 0)
  filefmt = MOD(reffmt,200)

  IF (filefmt == 1) THEN
    IF (iboxe > iboxs .AND. jboxe > jboxs) THEN
      nlon = iboxe-iboxs+1
      nlat = jboxe-jboxs+1
    ELSE
      nlon = 7000
      nlat = 3500
    END IF
    nhgt = 33
    RETURN
  ELSE IF (filefmt == 2) THEN
    nlon = 7000
    nlat = 3500
    IF (crefopt) THEN
      nhgt = 1
    ELSE
      nhgt = 33
    END IF

  !ELSE IF (filefmt == 3) THEN   ! Stage IV precipitation data in grib format
  !                              ! It is NOT MRMS dataset
  !  INQUIRE(FILE = refile, EXIST = fexists)
  !  IF (.NOT. fexists) THEN
  !    WRITE(6,'(2a)') 'File not found: ', refile
  !    istatus = -1
  !    RETURN
  !    !STOP 'read_mrms_dims'
  !  ENDIF
  !
  !  dimid = LEN_TRIM(refile)
  !  CALL rdgrbdims (refile,dimid,nlon,nlat,istatus)
  !  nhgt = 1

  ELSE IF (filefmt == 7) THEN

    INQUIRE(FILE = refile, EXIST = fexists)
    IF (fexists) THEN
      istatus = NF_OPEN(TRIM(refile),NF_NOWRITE,ncid)
      CALL handle_ncd_error(istatus,'NF_OPEN in read_mrms_dims',.TRUE.)
    ELSE
      WRITE(6,'(2a)') 'File not found: ', refile
      istatus = -1
      RETURN
      !STOP 'read_mrms_dims'
    ENDIF

!-----------------------------------------------------------------------
!
! Get file dimensions
!
!-----------------------------------------------------------------------

    istatus = NF_INQ_DIMID(ncid,'Lat',dimid)
    CALL handle_ncd_error(istatus,'NF_INQ_DIMID in read_mrms_dims',.TRUE.)
    istatus = NF_INQ_DIMLEN(ncid,dimid,nlat)
    CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in read_mrms_dims',.TRUE.)

    istatus = NF_INQ_DIMID(ncid,'Lon',dimid)
    CALL handle_ncd_error(istatus,'NF_INQ_DIMID in read_mrms_dims',.TRUE.)
    istatus = NF_INQ_DIMLEN(ncid,dimid,nlon)
    CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in read_mrms_dims',.TRUE.)

    istatus = NF_INQ_DIMID(ncid,'Ht',dimid)
    CALL handle_ncd_error(istatus,'NF_INQ_DIMID in read_mrms_dims',.TRUE.)
    istatus = NF_INQ_DIMLEN(ncid,dimid,nhgt)
    CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in read_mrms_dims',.TRUE.)

    !istatus = NF_INQ_DIMID(ncid,'time',dimid)
    !CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_wrf_dimension',.FALSE.)
    !istatus = NF_INQ_DIMLEN(ncid,dimid,ntime)
    !CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_dimension',.FALSE.)

    istatus = NF_CLOSE(ncid)
    CALL handle_ncd_error(istatus,'NF_CLOSE in read_mrms_dims',.TRUE.)
  ELSE
    WRITE(*,'(1x,2(a,I0))') 'ERROR: unsupported file format: ',filefmt,', reffmt = ',reffmt
    istatus = -1
    RETURN
  END IF

  RETURN
END SUBROUTINE read_mrms_dims
!
!##################################################################
!##################################################################
!######                                                      ######
!######               SUBROUTINE READ_MRMS                   ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE read_mrms(refsrc,refile,reffmt,nlon,nlat,nhgt,iboxs,iboxe,jboxs,jboxe,  &
                     rmissing,lonmrms,latmrms,hgtmrms,refmrms,timestr,istatus)

  IMPLICIT NONE

  INTEGER,            INTENT(IN)  :: refsrc
  INTEGER,            INTENT(IN)  :: reffmt
  CHARACTER(LEN=256), INTENT(IN)  :: refile
  INTEGER,            INTENT(IN)  :: nlon, nlat, nhgt
  INTEGER,            INTENT(IN)  :: iboxs,iboxe,jboxs,jboxe

  REAL,               INTENT(IN)  :: rmissing
  REAL,               INTENT(OUT) :: latmrms(nlat)
  REAL,               INTENT(OUT) :: lonmrms(nlon)
  REAL,               INTENT(OUT) :: hgtmrms(nhgt)

  REAL,               INTENT(OUT) :: refmrms(nlon,nlat,nhgt)
  CHARACTER(LEN=19),  INTENT(OUT) :: timestr

  INTEGER,            INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER, PARAMETER   :: VAR_NOTEXIST = -1

  INTEGER :: filefmt
  LOGICAL :: crefopt
  INTEGER :: i,j,k,jj
  INTEGER :: ix1,ix2, jy1,jy2
  INTEGER :: lvldbg = 0

  LOGICAL :: fexists, tmpfl
  INTEGER :: ncid, varid
  CHARACTER(LEN=NF_MAX_NAME) :: invarname

  REAL :: nceplvls(33) = (/ 00.50, 00.75, 01.00, 01.25, 01.50,          &
                     01.75, 02.00, 02.25, 02.50, 02.75, 03.00, 03.50,   &
                     04.00, 04.50, 05.00, 05.50, 06.00, 06.50, 07.00,   &
                     07.50, 08.00, 08.50, 09.00, 10.00, 11.00, 12.00,   &
                     13.00, 14.00, 15.00, 16.00, 17.00, 18.00, 19.00 /)
  INTEGER :: ia, ib
  INTEGER :: indx1, indx2, flen1, flen2
  CHARACTER(LEN=256) :: filename, tmpfile

  INTEGER :: nx_ext, ny_ext, nz_ext
  REAL    :: latsw, lonsw, dx_grb, dy_grb, latnw, lonnw
  REAL, ALLOCATABLE :: vargrb2d(:,:,:)

  INTEGER :: GETPID    ! function to get process ID
  INTEGER, SAVE :: iserial = 0

  REAL    :: tema
  DOUBLE PRECISION :: time
  INTEGER, PARAMETER :: abstimoffset = 315619200    ! this system starts from 1960 01 01
  INTEGER :: iyr, imn, iday, ihr, imin,isec

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  crefopt = (reffmt/200 > 0)
  filefmt = MOD(reffmt,200)

!
!-----------------------------------------------------------------------
!
!  Retrieve MRMS in GRIB2 format
!
!-----------------------------------------------------------------------
!
  IF (filefmt == 1) THEN   ! Read GRIB2 file

    flen1 = LEN_TRIM(refile)

    indx1 = INDEX(refile,'00.50',.TRUE.)
    IF (indx1 <= 0) THEN
      WRITE(*,'(1x,3a)') 'ERROR: filename ',TRIM(refile),' does not contain "00.50".'
      istatus = -1
      RETURN
    END IF
    indx2 = indx1+5
    indx1 = indx1-1

    nx_ext = 7000
    ny_ext = 3500
    nz_ext = 1

    IF (iboxe > iboxs .AND. jboxe > jboxs) THEN
      ix1 = iboxs
      ix2 = iboxe
      jy1 = jboxs
      jy2 = jboxe
    ELSE
      ix1 = 1
      ix2 = nx_ext
      jy1 = 1
      jy2 = ny_ext
    END IF

    ALLOCATE(vargrb2d(nlon,nlat,1), STAT = istatus )

    DO k = 1, nhgt
      ia = FLOOR(nceplvls(k))
      ib = FLOOR((nceplvls(k)-ia)*100)
      WRITE(filename,'(a,I2.2,a,I2.2,a)') refile(1:indx1),ia,'.',ib,refile(indx2:)

      INQUIRE(FILE = filename, EXIST = fexists)
      IF (fexists) THEN
        tmpfile = filename
        flen2   = flen1
        tmpfl   = .FALSE.
        GOTO 200
      ENDIF

      INQUIRE(FILE=filename(1:flen1)//'.gz', EXIST = fexists )
      IF( fexists ) THEN
        iserial = iserial + 1
        WRITE(tmpfile,'(2(a,I0))') refile(1:flen1),'_',GETPID(),'-',iserial
        flen2 = LEN_TRIM(tmpfile)

        CALL unixcmd( 'cp '//filename(1:flen1)//'.gz '//TRIM(tmpfile)//'.gz' )
        INQUIRE(FILE=TRIM(tmpfile), EXIST = fexists )
        IF( fexists ) CALL unixcmd( '/bin/rm '//TRIM(tmpfile) )
        CALL uncmprs( TRIM(tmpfile)//'.gz' )
        tmpfl = .TRUE.

        GO TO 200
      END IF

      WRITE(6,'(/1x,a,/1x,a/)')
      WRITE(6,'(a)') 'GRIB file '//filename(1:flen1)//                      &
          ' or its compressed version not found. Program stopped in read_mrms.'
      istatus = -5
      RETURN

      200 CONTINUE

      hgtmrms(k) = nceplvls(k)*1000.0

      CALL rd_mrmsgrb2(nlon,nlat,1,tmpfile,flen2,indx2+1,               &
                       ix1,ix2,jy1,jy2,latsw,lonsw,dx_grb,dy_grb,       &
                       1,(/209,9,0,102/),(/1/),(/hgtmrms(k)/),          &
                       vargrb2d, timestr,lvldbg, istatus)
      IF (istatus /= 0) RETURN

      !WRITE(0,'(1x,2(a,F10.5))') 'Get latsw = ',latsw, ', lonsw = ',lonsw

      IF (lonsw > 180.) lonsw = lonsw - 360.0
      latsw = latsw + (jy1-1)*dy_grb
      lonsw = lonsw + (ix1-1)*dx_grb

      IF (k == 1) WRITE(*,'(1x,2(a,F10.5),4(a,I0))')                    &
            'Use latsw = ',latsw, ', lonsw = ',lonsw,                   &
          ', based on ibox = ',ix1,'-',ix2,'; jbox = ',jy1,'-',jy2

      ! in GRIB file,
      ! = -99, clear
      ! = -999, no coverage
      !WHERE (vargrb2d == -99)  vargrb2d = rmissing
      WHERE (vargrb2d < -900) vargrb2d = rmissing

      DO j = 1, nlat
        DO i = 1, nlon
          refmrms(i,j,k) = vargrb2d(i,j,1)
        END DO
      END DO

      IF (tmpfl) CALL unixcmd( 'rm '//TRIM(tmpfile) )

    END DO

    DO j = 1, nlat
      latmrms(j) = latsw+(j-1)*dy_grb
    END DO

    DO i = 1, nlon
      lonmrms(i) = lonsw+(i-1)*dx_grb
    END DO

    DEALLOCATE(vargrb2d)

!
!-----------------------------------------------------------------------
!
!  Retrieve MRMS binary files
!
!-----------------------------------------------------------------------
!
  ELSE IF (filefmt == 2) THEN   ! Read MRMS binary file

    flen1 = LEN_TRIM(refile)

    IF (.NOT. crefopt) THEN
      indx1 = INDEX(refile,'00.50',.TRUE.)
      IF (indx1 <= 0) THEN
        WRITE(*,'(1x,3a)') 'ERROR: filename ',TRIM(refile),' does not contain "00.50".'
        istatus = -1
        RETURN
      END IF
      indx2 = indx1+5
      indx1 = indx1-1
    END IF

    ALLOCATE(vargrb2d(nlon,nlat,1), STAT = istatus )

    DO k = 1, nhgt
      IF (.NOT. crefopt) THEN
        ia = FLOOR(nceplvls(k))
        ib = FLOOR((nceplvls(k)-ia)*100)
        WRITE(filename,'(a,I2.2,a,I2.2,a)') refile(1:indx1),ia,'.',ib,refile(indx2:)
      ELSE
        WRITE(filename,'(a)') TRIM(refile)
      END IF

      INQUIRE(FILE = filename, EXIST = fexists)
      IF (fexists) THEN
        tmpfile = filename
        flen2   = flen1
        tmpfl   = .FALSE.
        GOTO 300
      ENDIF

      INQUIRE(FILE=filename(1:flen1)//'.gz', EXIST = fexists )
      IF( fexists ) THEN
        iserial = iserial + 1
        WRITE(tmpfile,'(a,2(a,I0))') refile(1:flen1),'_',GETPID(),'-',iserial
        flen2 = LEN_TRIM(tmpfile)

        CALL unixcmd( 'cp '//filename(1:flen1)//'.gz '//TRIM(tmpfile)//'.gz' )
        INQUIRE(FILE=TRIM(tmpfile), EXIST = fexists )
        IF( fexists ) CALL unixcmd( '/bin/rm '//TRIM(tmpfile) )
        CALL uncmprs( TRIM(tmpfile)//'.gz' )
        tmpfl = .TRUE.

        GO TO 300
      END IF

      WRITE(6,'(/1x,a,/1x,a/)')
      WRITE(6,'(a)') 'GRIB file '//filename(1:flen1)//                      &
          ' or its compressed version not found. Program stopped in read_mrms.'
      istatus = -5
      RETURN

      300 CONTINUE

      CALL rd_mrmsbin(nlon,nlat,1,tmpfile,flen2,latnw,lonnw,dx_grb,dy_grb,       &
                      hgtmrms(k),vargrb2d, timestr,lvldbg, istatus)
      IF (istatus /= 0) RETURN

      !WRITE(0,'(1x,2(a,F10.5))') 'Get latsw = ',latsw, ', lonsw = ',lonsw

      IF (lonnw > 180.) lonnw = lonnw - 360.0

      IF (k == 1) WRITE(*,'(1x,2(a,F10.5))')                            &
            'Use latnw = ',latnw, ', lonnw = ',lonnw

      ! in GRIB file,
      ! = -99, missing
      ! = -999, no coverage
      !WHERE (vargrb2d == -99)  vargrb2d = rmissing
      WHERE (vargrb2d < -900) vargrb2d = rmissing

      DO j = 1, nlat
        DO i = 1, nlon
          refmrms(i,j,k) = vargrb2d(i,j,1)
        END DO
      END DO

      IF (tmpfl) CALL unixcmd( 'rm '//TRIM(tmpfile) )

    END DO

    DO j = 1, nlat
      latmrms(j) = latnw-(nlat-j)*dy_grb
    END DO

    DO i = 1, nlon
      lonmrms(i) = lonnw+(i-1)*dx_grb
    END DO

    DEALLOCATE(vargrb2d)
!
!-----------------------------------------------------------------------
!
!  Retrieve netCDF file
!
!-----------------------------------------------------------------------
!

  ELSE IF (filefmt == 7) THEN   ! Read netCDF file

    INQUIRE(FILE = refile, EXIST = fexists)
    IF (fexists) THEN
      istatus = NF_OPEN(TRIM(refile),NF_NOWRITE,ncid)
      CALL handle_ncd_error(istatus,'NF_OPEN in read_mrms_dims',.TRUE.)
    ELSE
      WRITE(6,'(2a)') 'File not found: ', refile
      istatus = -1
      RETURN
      !STOP 'read_mrms_dims'
    ENDIF

    !
    ! Read Lat2D
    !
    invarname = 'Lat'
    istatus = NF_INQ_VARID(ncid,invarname,varid)
    IF(istatus == NF_ENOTVAR) THEN
       WRITE(6,'(3a)') ' WARNING: variable ',TRIM(invarname),' does not exist.'
       istatus = VAR_NOTEXIST
       RETURN
    END IF
    CALL handle_ncd_error(istatus,'NF_INQ_VARID in read_mrms with '//TRIM(invarname),.TRUE.)

    istatus = NF_GET_VARA_REAL(ncid,varid,(/1/),(/nlat/),latmrms)
    CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in read_mrms',.TRUE.)

    DO j = 1,nlat/2
      jj = nlat-j+1
      tema = latmrms(jj)
      latmrms(jj) = latmrms(j)
      latmrms(j)  = tema
    END DO

    !
    ! Read Lon2D
    !
    invarname = 'Lon'
    istatus = NF_INQ_VARID(ncid,invarname,varid)
    IF(istatus == NF_ENOTVAR) THEN
       WRITE(6,'(3a)') ' WARNING: variable ',TRIM(invarname),' does not exist.'
       istatus = VAR_NOTEXIST
       RETURN
    END IF
    CALL handle_ncd_error(istatus,'NF_INQ_VARID in read_mrms with '//TRIM(invarname),.TRUE.)

    istatus = NF_GET_VARA_REAL(ncid,varid,(/1/),(/nlon/),lonmrms)
    CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in read_mrms',.TRUE.)

    !
    ! Read Height
    !
    !ALLOCATE(hgt1d(nhgt), STAT = istatus)

    invarname = 'Height'
    istatus = NF_INQ_VARID(ncid,invarname,varid)
    IF(istatus == NF_ENOTVAR) THEN
       WRITE(6,'(3a)') ' WARNING: variable ',TRIM(invarname),' does not exist.'
       istatus = VAR_NOTEXIST
       RETURN
    END IF
    CALL handle_ncd_error(istatus,'NF_INQ_VARID in read_mrms with '//TRIM(invarname),.TRUE.)

    istatus = NF_GET_VARA_REAL(ncid,varid,(/1/),(/nhgt/),hgtmrms)
    CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in read_mrms',.TRUE.)

    !DO k = 1, nhgt
    !  DO j = 1, nlat
    !    DO i = 1, nlon
    !      hgtmrms(i,j,k) = hgt1d(k)
    !    END DO
    !  END DO
    !END DO
    !
    !DEALLOCATE(hgt1d)

    !
    ! Read Height
    !
    invarname = 'MergedReflectivityQC'
    istatus = NF_INQ_VARID(ncid,invarname,varid)
    IF(istatus == NF_ENOTVAR) THEN
       WRITE(6,'(3a)') ' WARNING: variable ',TRIM(invarname),' does not exist.'
       istatus = VAR_NOTEXIST
       RETURN
    END IF
    CALL handle_ncd_error(istatus,'NF_INQ_VARID in read_mrms with '//TRIM(invarname),.TRUE.)

    istatus = NF_GET_VARA_REAL(ncid,varid,(/1,1,1/),(/nlon,nlat,nhgt/),refmrms)
    CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in read_mrms',.TRUE.)

    DO k = 1,nhgt
      DO j = 1, nlat/2
        jj = nlat-j+1
        DO i = 1, nlon
          tema = refmrms(i,jj,k)
          refmrms(i,jj,k) = refmrms(i,j,k)
          refmrms(i,j,k)  = tema
        END DO
      END DO
    END DO

    invarname = 'time'
    istatus = NF_INQ_VARID(ncid,invarname,varid)
    istatus = NF_GET_VARA_DOUBLE(ncid,varid,(/1/),(/1/),time)

    time = time + abstimoffset
    CALL abss2ctim(NINT(time), iyr, imn, iday, ihr, imin, isec )
    WRITE(timestr,'(I4.4,5(a,I2.2))') iyr,'-', imn, '-',iday, '_',ihr,':', imin,':', isec

!-----------------------------------------------------------------------
!
! Close file
!
!-----------------------------------------------------------------------

    istatus = NF_CLOSE(ncid)
    CALL handle_ncd_error(istatus,'NF_CLOSE in read_mrms_dims',.TRUE.)

  ELSE
    WRITE(*,'(1x,2(a,I0))') 'ERROR: unsupported file format: ',filefmt,', reffmt = ',reffmt
    istatus = -1
    RETURN

  END IF

  WRITE(*,'(1x,a,4F8.2)') 'latmrms, lonmrms = ',latmrms(1),latmrms(nlat),lonmrms(1),lonmrms(nlon)

  RETURN
END SUBROUTINE read_mrms
!
!##################################################################
!##################################################################
!######                                                      ######
!######                 FUNCTION PNTINT2D                    ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE rd_mrmsbin(nlon,nlat,nhgt,filename,flen,nwlat,nwlon,dx,dy,   &
                      zhgt,mrms,timestr,lvldbg, istatus)
  IMPLICIT NONE

  INTEGER,            INTENT(IN)  :: nlon, nlat, nhgt
  CHARACTER(LEN=256), INTENT(IN)  :: filename
  INTEGER,            INTENT(IN)  :: flen
  REAL,               INTENT(OUT) :: nwlat, nwlon
  REAL,               INTENT(OUT) :: dx, dy
  REAL,               INTENT(OUT) :: zhgt(nhgt)
  REAL,               INTENT(OUT) :: mrms(nlon,nlat,nhgt)
  CHARACTER(LEN=19),  INTENT(OUT) :: timestr
  INTEGER,            INTENT(IN)  :: lvldbg
  INTEGER,            INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INTEGER :: yr, mo, day, hr, min,sec
  INTEGER :: nx, ny, nz

  CHARACTER(LEN=4)  :: maptype
  CHARACTER(LEN=20) :: varname
  CHARACTER(LEN=6)  :: varunit
  CHARACTER(LEN=4)  :: radarnam(200)

  INTEGER :: map_scale, trulat1, turlat2, turlon, nw_lon, nw_lat, xy_scale
  INTEGER :: dxy_scale,z_scale,var_scale
  INTEGER :: missing_val,nradars
  INTEGER :: idummyx, idummyy, idummy(33)

  INTEGER(KIND=2), ALLOCATABLE :: binary_data(:)

  INTEGER :: unt

!-----------------------------------------------------------------------

  INTEGER :: i,j,k
  INTEGER :: num, sw_origin_index, k_size


!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  unt = 38
  WRITE(*,'(1x,2a)') 'Open file: ',TRIM(filename)
  OPEN(unt,FILE=filename,FORM='UNFORMATTED',ACCESS='STREAM')

  READ(unt) yr, mo, day, hr, min, sec
  READ(unt) nx, ny, nz

  IF (nx /= nlon .OR. ny /= nlat .OR. nz /= nhgt) THEN
    WRITE(*,'(1x,a,3I6)') 'ERROR: expect size:    ',nlon,nlat,nhgt
    WRITE(*,'(1x,a,3I6)') '       size from file: ',nx,ny,nz
    istatus = -1
    RETURN
  END IF

  READ(unt) maptype, map_scale
  READ(unt) trulat1, turlat2, turlon, nw_lon, nw_lat, xy_scale
  !print *, 'nwlon,nwlat ',nw_lon,nw_lat, map_scale
  nwlon = REAL(nw_lon)/map_scale
  nwlat = REAL(nw_lat)/map_scale
  READ(unt) idummyx, idummyy
  READ(unt) dxy_scale
  dx = REAL(idummyx)/dxy_scale
  dy = REAL(idummyy)/dxy_scale

  ! read heights
  READ(unt) (idummy(i),i=1,nz)
  DO k = 1, nz
    zhgt(k) = REAL(idummy(k))
  END DO

  READ(unt) z_scale
  IF (z_scale /= 1 .AND. z_scale /= 0) THEN
    DO k = 1, nz
      zhgt(k) = zhgt(k)/z_scale
    END DO
  END IF

  !read junk (place holders for future use)
  READ(unt) (idummy(i),i=1,10)   ! [X+5] - [X+44]

  !read variable name
  READ(unt) varname              ! [X+45] - [X+64]

  !read variable unit
  READ(unt) varunit              ! [X+65] - [X+70]

  !read variable scaling factor
  READ(unt) var_scale            ! [X+71] - [X+74]

  !read variable missing flag
  READ(unt) missing_val          ! [X+75] - [X+78]

  !read number of radars affecting this product
  READ(unt) nradars              ! [X+79] - [X+82]

  !read in names of radars
  READ(unt) (radarnam(i),i=1,nradars )    ! [X+83] - [X+82+nradars*4]

  !-------------------------*/
  !** 3. Read binary data ***/
  !-------------------------*/

  num = nx*ny*nz;
  ALLOCATE(binary_data(num), STAT = istatus)

  !read data array    [X+83+nradars*4] - [X+82+nradars*4+num*2]
  !i = 1
  !DO WHILE (.TRUE.)
    READ(unt) (binary_data(i), i =1,num)
    !print *, i
    !i = i+1
  !END DO

  !------------------------------*/
  !** 4. Close file and return ***/
  !------------------------------*/

  !close file
  CLOSE(UNT)

  ! unscale
  DO k = 1,nz
    k_size = (k-1)*nx*ny;

    DO j = 1, ny

      sw_origin_index = k_size + (j-1)*nx;
      !nw_origin_index = k_size + (ny-j-1)*nx;

      DO i = 1, nx
        sw_origin_index = sw_origin_index + 1
        !nw_origin_index++;

        mrms(i,j,k) = REAL(binary_data(sw_origin_index)) / REAL(var_scale);

      END DO ! end i-loop
    END DO ! end j-loop
  END DO ! end k-loop

  WRITE(timestr,'(I4.4,5(a,I2.2))') yr,'-', mo,'-', day,'_', hr,':', min,':', sec

  !PRINT *, timestr
  !PRINT *, nx, ny, nz
  !PRINT *, maptype, map_scale
  !PRINT *, trulat1, turlat2, turlon, nw_lon, nw_lat, xy_scale
  !PRINT *, nwlon, nwlat, dx, dy
  !
  !PRINT *, zhgt(1),varname,varunit,missing_val, var_scale
  !print *, nradars,(radarnam(i),i=1,nradars )
  !print *, (binary_data(i),i=1,10)
  !print *, (mrms(i,1,1),i=1,10)
  !stop

  RETURN
END SUBROUTINE rd_mrmsbin
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%
!%%%% Interpolation subs that handle missing values
!%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!######################################################################
!######################################################################
!######                                                          ######
!######                   FUNCTION PNTINT2D                      ######
!######                                                          ######
!######                       Developed by                       ######
!######         National Severe Storm Laboratory, NOAA           ######
!######                                                          ######
!######################################################################
!######################################################################
!
FUNCTION pntint2dm(vnx,vny,                                             &
             ivbeg,ivend,jvbeg,jvend,                                   &
             iorder,vx,vy,xpnt,ypnt,iloc,jloc,var,                      &
             dxfld,dyfld,rdxfld,rdyfld,                                 &
             slopey,rmissing)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!    Interpolate a 2-d field for a single point on that plane.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang (03/15/2017)
!  Based on PNTINT2D in the ADAS package by Keith Brewster, CAPS, November, 1996
!  Added code to handle missing values.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!    vnx       Number of model grid points in the x-direction (east/west)
!    vny       Number of model grid points in the y-direction (north/south)
!
!    ivbeg,ivend   Range of x index to use in verification array
!    jvbeg,jvend   Range of y index to use in verification array
!
!    iorder   Interpolation parameter.
!             iorder specifies the order of interpolation
!             1 = bi-linear
!             2 = bi-quadratic
!
!    vx       x coordinate of verif scalar grid points in physical space (m)
!    vy       y coordinate of verif scalar grid points in physical space (m)
!
!    xpnt     x coordinate (m) of interpolation point
!    ypnt     y coordinate (m) of interpolation point
!
!    iloc     I-index of interpolation point in field to be interpolated
!    jloc     J-index of interpolation point in field to be interpolated
!    dxfld    Vector of delta-x (m) of field to be interpolated
!    dyfld    Vector of delta-y (m) of field to be interpolated
!    rdxfld   Vector of 1./delta-x (1/m) of field to be interpolated
!    rdyfld   Vector of 1./delta-y (1/m) of field to be interpolated
!
!    slopey   Piecewise linear df/dy
!    alphay   Coefficient of y-squared term in y quadratic interpolator
!    betay    Coefficient of y term in y quadratic interpolator
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: vnx,vny
  INTEGER, INTENT(IN) :: ivbeg,ivend,jvbeg,jvend
  INTEGER, INTENT(IN) :: iorder
  REAL   , INTENT(IN) :: vx(vnx)
  REAL   , INTENT(IN) :: vy(vny)
  REAL   , INTENT(IN) :: xpnt
  REAL   , INTENT(IN) :: ypnt
  INTEGER, INTENT(IN) :: iloc
  INTEGER, INTENT(IN) :: jloc
  REAL   , INTENT(IN) :: var(vnx,vny)
  REAL   , INTENT(IN) :: dxfld(vnx)
  REAL   , INTENT(IN) :: dyfld(vny)
  REAL   , INTENT(IN) :: rdxfld(vnx)
  REAL   , INTENT(IN) :: rdyfld(vny)
  REAL   , INTENT(IN) :: slopey(vnx,vny)
  REAL   , INTENT(IN) :: rmissing      ! Assume a small negative number

  REAL    :: pntint2dm

!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: ii,jj, count, icount, jcount
  INTEGER :: ik(4),jk(4)
  REAL    :: varmiss, varclear
  REAL    :: delx,dely,r1,r2, w1,w2
  REAL    :: alpha,beta,rtwodx
  REAL    :: varm1,var00,varp1
  REAL    :: varint
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  varmiss  = rmissing
  varclear = -99.0

  count = 0
  ik(:) = 0
  jk(:) = 0
!-----------------------------------------------------------------------
!
!  Compute bilinear interpolated value
!
!-----------------------------------------------------------------------
!
  IF(iorder == 1) THEN
    ii=MIN(MAX(iloc,ivbeg),(ivend-1))
    jj=MIN(MAX(jloc,jvbeg),(jvend-1))
    delx=(xpnt-vx(ii))
    dely=(ypnt-vy(jj))

    IF (var(ii,jj) > varmiss) THEN
      count  = count + 1
      ik(count) = ii
      jk(count) = jj
    END IF

    IF (var(ii+1,jj) > varmiss) THEN
      count  = count + 1
      ik(count) = ii+1
      jk(count) = jj
    END IF

    IF (var(ii,jj+1) > varmiss) THEN
      count  = count + 1
      ik(count) = ii
      jk(count) = jj+1
    END IF

    IF (var(ii+1,jj+1) > varmiss) THEN
      count  = count + 1
      ik(count) = ii+1
      jk(count) = jj+1
    END IF

    SELECT CASE (count)
    CASE (0)
      varint = rmissing
    CASE (1)   ! Only one no-misisng value, use near-neighbor interpolation
      delx = ABS(xpnt-vx(ik(1)))
      dely = ABS(ypnt-vy(jk(1)))
      IF (delx < 0.5*dxfld(ii) .AND. dely < 0.5*dyfld(jj) ) THEN
        varint = var(ik(1),jk(1))
      ELSE
        varint = rmissing
      END IF
    CASE (2)
      icount = ik(1)+ik(2)
      jcount = jk(1)+jk(2)
      IF (icount == 2*ii+1 .AND. jcount /= 2*jj+1) THEN
        dely = ABS(ypnt-vy(jk(1)))
        IF (dely >= 0.5*dyfld(jj)) THEN  ! too far from the valid points
          varint = rmissing
          GOTO 200
        END IF
      ELSE IF (jcount == 2*jj+1 .AND. icount /= 2*ii+1) THEN
        delx = ABS(xpnt-vx(ik(1)))
        IF (delx >= 0.5*dxfld(ii)) THEN
          varint = rmissing
          GOTO 200
        END IF
      !ELSE  ! not possible
      !  write(0,*) icount, jcount, ii, jj
      !  CALL arpsstop('ERROR in pntint2dm.',1)
      END IF
      !
      ! Do two point interpolation
      !
      delx = (xpnt-vx(ik(1)))
      dely = (ypnt-vy(jk(1)))
      r1 = SQRT(delx*delx+dely*dely)
      delx = (xpnt-vx(ik(2)))
      dely = (ypnt-vy(jk(2)))
      r2 = SQRT(delx*delx+dely*dely)
      w1 = r2/(r1+r2)
      w2 = r1/(r1+r2)
      !IF (var(ik(1),jk(1)) > varclear .AND. var(ik(2),jk(2)) > varclear ) THEN
        varint = var(ik(1),jk(1))*w1+var(ik(2),jk(2))*w2
      !ELSE IF (var(ik(1),jk(1)) > varclear .AND. w1 > 0.5) THEN
      !  varint = var(ik(1),jk(1))
      !ELSE IF (var(ik(2),jk(2)) > varclear .AND. w2 > 0.5) THEN
      !  varint = var(ik(2),jk(2))
      !ELSE
      !  varint = varclear
      !END IF


    CASE (3)   ! use delx & dely computed above
      r1 = 0.25*dxfld(ii)*dxfld(ii)+0.25*dyfld(jj)*dyfld(jj)
      IF (var(ii,jj) < varmiss+10) THEN
        r2 = delx*delx+dely*dely
        IF (r2 < r1) THEN    ! more closer to missing point
          varint = rmissing
          GOTO 200
        END IF
        varint=(1.-delx*rdxfld(ii))*(var(ii  ,jj+1))                    &
              +   (delx*rdxfld(ii))*(var(ii+1,jj)+slopey(ii+1,jj)*dely)
      ELSE IF (var(ii,  jj+1) < varmiss+10) THEN
        r2 = delx*delx+(dyfld(jj)-dely)*(dyfld(jj)-dely)
        IF (r2 < r1) THEN    ! more closer to missing point
          varint = rmissing
          GOTO 200
        END IF
        varint=(1.-delx*rdxfld(ii))*(var(ii  ,jj))                      &
              +  (delx*rdxfld(ii))*(var(ii+1,jj)+slopey(ii+1,jj)*dely)
      ELSE IF (var(ii+1,jj  ) < varmiss+10) THEN
        r2 = (dxfld(ii)-delx)*(dxfld(ii)-delx)+dely*dely
        IF (r2 < r1) THEN    ! more closer to missing point
          varint = rmissing
          GOTO 200
        END IF
        varint=(1.-delx*rdxfld(ii))*(var(ii  ,jj)+slopey(ii  ,jj)*dely) &
              +   (delx*rdxfld(ii))*(var(ii+1,jj+1))
      ELSE IF (var(ii+1,jj+1) < varmiss+10) THEN
        r2 = (dxfld(ii)-delx)*(dxfld(ii)-delx)+(dyfld(jj)-dely)*(dyfld(jj)-dely)
        IF (r2 < r1) THEN    ! more closer to missing point
          varint = rmissing
          GOTO 200
        END IF
        varint=(1.-delx*rdxfld(ii))*(var(ii  ,jj)+slopey(ii  ,jj)*dely) &
              +   (delx*rdxfld(ii))*(var(ii+1,jj))
      END IF
    CASE (4)       ! no missing, normal 4-point interpolation
      varint=(1.-delx*rdxfld(ii))*(var(ii  ,jj)+slopey(ii  ,jj)*dely)   &
            +   (delx*rdxfld(ii))*(var(ii+1,jj)+slopey(ii+1,jj)*dely)
    CASE DEFAULT  ! not possible
      CALL arpsstop('ERROR in pntint2dm.',1)
    END SELECT

    200 CONTINUE
!
!-----------------------------------------------------------------------
!
!  Compute biquadratic
!
!-----------------------------------------------------------------------
!
!  ELSE
!    ii=MIN(MAX(iloc,(ivbeg+1)),(ivend-1))
!    jj=MIN(MAX(jloc,(jvbeg+1)),(jvend-1))
!    delx=(xpnt-vx(ii))
!    dely=(ypnt-vy(jj))
!!
!!-----------------------------------------------------------------------
!!
!!    Stencil is ii-1 to ii+1 and jj-1 to jj + 1
!!
!!    Interpolate in y.
!!
!!-----------------------------------------------------------------------
!!
!    varm1=(alphay(ii-1,jj)*dely+betay(ii-1,jj))*dely+var(ii-1,jj)
!    var00=(alphay(ii  ,jj)*dely+betay(ii  ,jj))*dely+var(ii  ,jj)
!    varp1=(alphay(ii+1,jj)*dely+betay(ii+1,jj))*dely+var(ii+1,jj)
!!
!!-----------------------------------------------------------------------
!!
!!    Interpolate intermediate results in x.
!!
!!-----------------------------------------------------------------------
!!
!    rtwodx=1./(dxfld(ii-1)+dxfld(ii))
!    alpha=((varp1-var00)*rdxfld(ii  ) +                                 &
!           (varm1-var00)*rdxfld(ii-1))*rtwodx
!    beta=(varp1-var00)*rdxfld(ii) -                                     &
!             dxfld(ii)*alpha
!    varint=(alpha*delx+beta)*delx+var00
  END IF
  pntint2dm=varint

  RETURN
END FUNCTION pntint2dm

!
!##################################################################
!##################################################################
!######                                                      ######
!######               SUBROUTINE FLDINT3D                    ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE fldint3dm(nx,ny,nz,vnx,vny,vnz,                              &
           ibeg,iend,jbeg,jend,kstart,kend,                             &
           ivstart,ivend,jvstart,jvend,kvstart,kvend,                   &
           iorder,x2d,y2d,z,var,vx,vy,zpver,iloc,jloc,                  &
           dxfld,dyfld,rdxfld,rdyfld,                                   &
           slopey,rmissing,                                             &
           varint,istatus)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Intfield interpolates scalars from a set of fields (the verification
!    fields, "verif") having Cartesian coordinates described by vx,vy,vzp
!    to a second set of fields described by cartesion coordinates x,y,zp.
!    It is assumed that x,y,zp and vx,vy,vzp are monotonically increasing
!    with increasing index.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang (03/15/2017)
!  Based on fldint3d in the ARPS package by Keith Brewster  OU School of Meteorology.  Feb 1992
!  Added handling for missing values.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: nx,ny,nz
  INTEGER, INTENT(IN)  :: vnx,vny,vnz
  INTEGER, INTENT(IN)  :: ibeg,iend,jbeg,jend,kstart,kend
  INTEGER, INTENT(IN)  :: ivstart,ivend,jvstart,jvend,kvstart,kvend
  INTEGER, INTENT(IN)  :: iorder
  REAL,    INTENT(IN)  :: x2d(nx,ny)
  REAL,    INTENT(IN)  :: y2d(nx,ny)
  REAL,    INTENT(IN)  :: z(nx,ny,nz)
  REAL,    INTENT(IN)  :: var(vnx,vny,vnz)
  REAL,    INTENT(IN)  :: vx(vnx)
  REAL,    INTENT(IN)  :: vy(vny)
  REAL,    INTENT(IN)  :: zpver(nx,ny,vnz)
  INTEGER, INTENT(IN)  :: iloc(nx,ny)
  INTEGER, INTENT(IN)  :: jloc(nx,ny)
  REAL,    INTENT(IN)  :: dxfld(vnx)
  REAL,    INTENT(IN)  :: dyfld(vny)
  REAL,    INTENT(IN)  :: rdxfld(vnx)
  REAL,    INTENT(IN)  :: rdyfld(vny)

  REAL,    INTENT(OUT) :: slopey(vnx,vny,vnz)
  REAL,    INTENT(IN)  :: rmissing

  REAL,    INTENT(OUT) :: varint(nx,ny,nz)
  INTEGER, INTENT(OUT) :: istatus
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,kv,kbot,ktop
  REAL    :: wtop,varbot,vartop, varmiss, varclear
  REAL    :: pntint2dm
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  istatus = 0

  varmiss  = rmissing
  varclear = -99.0

!-----------------------------------------------------------------------
!
!  Compute derivative terms
!
!-----------------------------------------------------------------------
!
  CALL setdrvym(vnx,vny,vnz,                                            &
               ivstart,ivend,jvstart,jvend,kvstart,kvend,               &
               dyfld,rdyfld,var,                                        &
               slopey)

!
!-----------------------------------------------------------------------
!
!  Compute 2D interpolated values
!
!-----------------------------------------------------------------------
!
  IF (kvstart == kvend .AND. kvstart == 1) THEN

    DO j=jbeg,jend
      DO i=ibeg,iend

        ktop=1
        vartop=pntint2dm(vnx,vny,                                       &
                 ivstart,ivend,jvstart,jvend,                           &
                 iorder,vx,vy,x2d(i,j),y2d(i,j),                        &
                 iloc(i,j),jloc(i,j),var(:,:,ktop),                     &
                 dxfld,dyfld,rdxfld,rdyfld,                             &
                 slopey(:,:,ktop),rmissing)

        IF (vartop > varmiss) THEN
          varint(i,j,ktop) = vartop
        ELSE
          varint(i,j,ktop) = rmissing
        END IF
      END DO
    END DO

  ELSE
!
!-----------------------------------------------------------------------
!
!  Compute 3D interpolated values
!
!-----------------------------------------------------------------------
!
    DO k=kstart,kend
      DO j=jbeg,jend
        DO i=ibeg,iend

        IF (z(i,j,k) > zpver(i,j,kvend) .OR. z(i,j,k) < zpver(i,j,kvstart)) THEN
          varint(i,j,k) = rmissing
        ELSE
          DO kv=kvstart+1,kvend-1
            IF(zpver(i,j,kv) > z(i,j,k)) EXIT
          END DO

          ktop=kv
          kbot=kv-1
          wtop=(z(i,j,k)-zpver(i,j,kbot))/                                &
               (zpver(i,j,ktop)-zpver(i,j,kbot))
          varbot=pntint2dm(vnx,vny,                                       &
                   ivstart,ivend,jvstart,jvend,                           &
                   iorder,vx,vy,x2d(i,j),y2d(i,j),                        &
                   iloc(i,j),jloc(i,j),var(:,:,kbot),                     &
                   dxfld,dyfld,rdxfld,rdyfld,                             &
                   slopey(:,:,kbot),rmissing)
          vartop=pntint2dm(vnx,vny,                                       &
                   ivstart,ivend,jvstart,jvend,                           &
                   iorder,vx,vy,x2d(i,j),y2d(i,j),                        &
                   iloc(i,j),jloc(i,j),var(:,:,ktop),                     &
                   dxfld,dyfld,rdxfld,rdyfld,                             &
                   slopey(:,:,ktop),rmissing)

          IF (varbot > varmiss .AND. vartop > varmiss) THEN

            !IF (varbot > varclear .AND. vartop > varclear) THEN
              varint(i,j,k)=((1.-wtop)*varbot) + (wtop*vartop)
            !ELSE IF (varbot > varclear) THEN
            !  IF (wtop < 0.5) THEN
            !    varint(i,j,k) = varbot
            !  ELSE
            !    varint(i,j,k) = varclear
            !  END IF
            !ELSE IF (vartop > varclear) THEN
            !  IF (wtop > 0.5) THEN
            !    varint(i,j,k) = vartop
            !  ELSE
            !    varint(i,j,k) = varclear
            !  END IF
            !ELSE
            !  varint(i,j,k) = varclear
            !END IF

          ELSE IF (varbot > varmiss) THEN
            IF (wtop < 0.5) THEN
              varint(i,j,k) = varbot
            ELSE
              varint(i,j,k) = rmissing
            END IF
          ELSE IF (vartop > varmiss) THEN
            IF (wtop > 0.5) THEN
              varint(i,j,k) = vartop
            ELSE
              varint(i,j,k) = rmissing
            END IF
          ELSE
            varint(i,j,k) = rmissing
          END IF
        END IF
      END DO
    END DO
    END DO
  END IF

  RETURN
END SUBROUTINE fldint3dm

!
!##################################################################
!##################################################################
!######                                                      ######
!######               SUBROUTINE SETDRVY                     ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE setdrvym(nx,ny,nz,                                           &
           ibeg,iend,jbeg,jend,kbeg,kend,                               &
           dyfld,rdyfld,var,                                            &
           slopey)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Calculate the coefficients of interpolating polynomials
!    in the y-direction.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Keith Brewster, CAPS, November, 1996
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!    nx       Number of model grid points in the x-direction (east/west)
!    ny       Number of model grid points in the y-direction (north/south)
!    nz       Number of model grid points in the vertical
!
!    ibeg,iend   Range of x index to do interpolation
!    jbeg,jend   Range of y index to do interpolation
!    kbeg,kend   Range of z index to do interpolation
!
!    dyfld    Vector of delta-y (m) of field to be interpolated
!    rdyfld   Vector of 1./delta-y (1/m) of field to be interpolated
!
!    var      variable to be interpolated
!
!    slopey   Piecewise linear df/dy
!    alphay   Coefficient of y-squared term in y quadratic interpolator
!    betay    Coefficient of y term in y quadratic interpolator
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
  INTEGER :: nx,ny,nz
  INTEGER :: ibeg,iend,jbeg,jend,kbeg,kend
  REAL :: dyfld(ny)
  REAL :: rdyfld(ny)
  REAL :: var(nx,ny,nz)
  REAL :: slopey(nx,ny,nz)
  REAL :: alphay(nx,ny,nz)
  REAL :: betay(nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: jstart,jstop
  REAL    :: rtwody

  include 'mp.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  jstart=MAX(jbeg,1)
  jstop=MIN((jend-1),(ny-1))
  !write(0,*) myproc,nx,ny,nz
  !write(0,*) ibeg,iend,jstart,jstop,kbeg,kend
  DO k=kbeg,kend
    DO j=jstart,jstop
      !write(0,*) j, dyfld(j),rdyfld(j)
      DO i=ibeg,iend
        slopey(i,j,k)=(var(i,j+1,k)-var(i,j,k))*rdyfld(j)
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE setdrvym
!
!##################################################################
!##################################################################
!######                                                      ######
!######               SUBROUTINE READ_STAGEIV                ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE read_intrp_stage4(st4file,st4fmt,iboxs,iboxe,jboxs,jboxe,    &
                       iorder,nx,ny,nz,xs,ys,zps,st4p,timestr,istatus)

  IMPLICIT NONE

  INTEGER,            INTENT(IN)    :: st4fmt
  INTEGER,            INTENT(INOUT) :: iboxs, iboxe,jboxs,jboxe
  INTEGER,            INTENT(IN)    :: iorder
  CHARACTER(LEN=256), INTENT(IN)    :: st4file
  INTEGER,            INTENT(IN)    :: nx, ny, nz
  REAL,               INTENT(IN)    :: xs(nx),ys(ny)
  REAL,               INTENT(IN)    :: zps(nx,ny,nz)
  REAL,               INTENT(OUT)   :: st4p(nx,ny,1)
  CHARACTER(LEN=19),  INTENT(OUT)   :: timestr

  INTEGER,            INTENT(OUT)   :: istatus

  INCLUDE 'mp.inc'

!-----------------------------------------------------------------------

  REAL, PARAMETER :: rmissing = -9999.

  INTEGER :: i,j,k,jj

  INTEGER :: nlon, nlat, nhgt

  REAL,    ALLOCATABLE :: lat2d(:,:),  lon2d(:,:)
  REAL,    ALLOCATABLE :: xs2d(:,:),    ys2d(:,:)
  INTEGER, ALLOCATABLE :: iloc(:,:), jloc(:,:)

  REAL,    ALLOCATABLE :: dxfld(:), dyfld(:), rdxfld(:), rdyfld(:)
  REAL,    ALLOCATABLE :: slopey(:,:), alphay(:,:), betay(:,:), tem1_ext(:,:)

  REAL,    ALLOCATABLE :: lon_ext(:,:), lat_ext(:,:), hgt_ext(:)
  REAL,    ALLOCATABLE :: st4p_ext(:,:,:)
  REAL,    ALLOCATABLE :: x_ext(:), y_ext(:)

  REAL :: tema, latmin, latmax, lonmin, lonmax
  !
  ! MRMS specific values below
  !
  REAL,    PARAMETER :: lat1 = 20.005, lon1 = -129.995, dx1 = 0.01, dy1 = 0.01
  INTEGER, PARAMETER :: rnx  = 7000,   rny  = 3500,     rwidth = 20

  REAL    :: x0_ext, y0_ext     , xorig_arps, yorig_arps
  INTEGER :: iproj_ext          , iproj_arps
  REAL    :: scale_ext,trlon_ext, scale_arps,trlon_arps
  REAL    :: latnot_ext(2)      , latnot_arps(2)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  ALLOCATE(lat2d(nx,ny), STAT = istatus)
  ALLOCATE(lon2d(nx,ny), STAT = istatus)

  CALL xytoll(nx,ny,xs,ys,lat2d,lon2d)

  !CALL print3dnc_lg(200,'lat2d',lat2d,nx,ny,1)
  !CALL print3dnc_lg(200,'lon2d',lon2d,nx,ny,1)

  !CALL a3dmax0(lat2d,1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,latmax,latmin)
  !CALL a3dmax0(lon2d,1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,lonmax,lonmin)
  !
  !iboxs = FLOOR((lonmin-lon1)/dx1); iboxe = CEILING((lonmax-lon1)/dx1)
  !jboxs = FLOOR((latmin-lat1)/dy1); jboxe = CEILING((latmax-lat1)/dy1)
  !
  !iboxs = MAX(1,iboxs-rwidth); iboxe = MIN(rnx,iboxe+rwidth)
  !jboxs = MAX(1,jboxs-rwidth); jboxe = MIN(rny,jboxe+rwidth)
!
!-----------------------------------------------------------------------
! Read data file
!-----------------------------------------------------------------------
!

  IF (myproc == 0) THEN
    CALL read_st4_dims(st4file,st4fmt,iboxs,iboxe,jboxs,jboxe,   &
                        nlon,nlat,nhgt,istatus)
    !WRITE(0,*) 'nlon = ',nlon,', nlat = ',nlat,', nhgt = ',nhgt
  END IF
  CALL mpupdatei(nlon,1)
  CALL mpupdatei(nlat,1)
  CALL mpupdatei(nhgt,1)

  ALLOCATE(st4p_ext(nlon,nlat,nhgt), STAT = istatus)
  ALLOCATE(hgt_ext(nhgt),            STAT = istatus)
  ALLOCATE(lat_ext(nlon,nlat),       STAT = istatus)
  ALLOCATE(lon_ext(nlon,nlat),       STAT = istatus)
  ALLOCATE(x_ext(nlon),              STAT = istatus)
  ALLOCATE(y_ext(nlat),              STAT = istatus)

  IF (myproc == 0) THEN
    hgt_ext = 0.0
    CALL read_st4(st4file,st4fmt,nlon,nlat,nhgt,iboxs,iboxe,jboxs,jboxe, &
                   rmissing,lon_ext,lat_ext,hgt_ext,st4p_ext,timestr,   &
                   iproj_ext,scale_ext,latnot_ext,trlon_ext,            &
                   x0_ext,y0_ext,x_ext,y_ext,istatus)
  END IF
  CALL mpupdater(lon_ext,nlon*nlat)
  CALL mpupdater(lat_ext,nlat*nlat)
  CALL mpupdater(hgt_ext,nhgt)
  CALL mpupdater(st4p_ext,nlon*nlat*nhgt)
  CALL mpupdatec(timestr,19)
  CALL mpupdatei(iproj_ext,1)
  CALL mpupdater(scale_ext,1)
  CALL mpupdater(latnot_ext,2)
  CALL mpupdater(trlon_ext,1)
  CALL mpupdater(x0_ext,1)
  CALL mpupdater(y0_ext,1)
  CALL mpupdater(x_ext,nlon)
  CALL mpupdater(y_ext,nlat)

  !IF (myproc == 0) THEN
  !  WRITE(0,*) 'max lat ',latmin, latmax
  !  WRITE(0,*) 'max lon ',lonmin, lonmax
  !  WRITE(0,*) latmrms(1:2),latmrms(nlat-1:nlat), 'nlat = ',nlat
  !  WRITE(0,*) lonmrms(1:2),lonmrms(nlon-1:nlon), 'nlon = ',nlon
  !END IF

  !WHERE (refmrms < rmissing+100.) refmrms = 0.0

  !print *, 'refmrms = ',MAXVAL(refmrms), MINVAL(refmrms)
  !IF (myproc == 0) CALL print3dnc_lc(200,'mrms_org',refmrms,nlon,nlat,nhgt,1,1)

  CALL getmapr(iproj_arps,scale_arps,latnot_arps,                       &
               trlon_arps,xorig_arps,yorig_arps)

!-----------------------------------------------------------------------
! Interpolate to the model grid
!-----------------------------------------------------------------------

  ALLOCATE(xs2d(nx,ny), STAT = istatus)
  ALLOCATE(ys2d(nx,ny), STAT = istatus)


  CALL setmapr(iproj_ext,scale_ext,latnot_ext,trlon_ext)
  CALL setorig(1,x0_ext,y0_ext)

  ALLOCATE(dxfld(nlon),  STAT = istatus)
  ALLOCATE(dyfld(nlat),  STAT = istatus)
  ALLOCATE(rdxfld(nlon), STAT = istatus)
  ALLOCATE(rdyfld(nlat), STAT = istatus)

  ALLOCATE(iloc(nx,ny),  STAT = istatus)
  ALLOCATE(jloc(nx,ny),  STAT = istatus)

  CALL lltoxy(nx,ny,lat2d,lon2d,xs2d,ys2d)

  CALL setijloc(nx,ny,nlon,nlat,xs2d,ys2d,x_ext,y_ext,iloc,jloc)

  CALL setdxdy(nlon,nlat,1,nlon,1,nlat,x_ext,y_ext,                     &
                 dxfld,dyfld,rdxfld,rdyfld)

  ALLOCATE(slopey  (nlon,nlat), STAT = istatus)
  ALLOCATE(alphay  (nlon,nlat), STAT = istatus)
  ALLOCATE(betay   (nlon,nlat), STAT = istatus)
  ALLOCATE(tem1_ext(nlon,nlat), STAT = istatus)

  CALL mkarps2d(nlon,nlat,nx,ny,iorder,iloc,jloc,x_ext,y_ext,xs2d,ys2d, &
                st4p_ext,st4p,dxfld,dyfld,rdxfld,rdyfld,                &
                slopey,alphay,betay,tem1_ext)

  DEALLOCATE(slopey,alphay,betay,tem1_ext)

  DEALLOCATE( dxfld, dyfld, rdxfld, rdyfld, STAT = istatus )
  DEALLOCATE( iloc, jloc,                   STAT = istatus)

!-----------------------------------------------------------------------
! Just before return
!-----------------------------------------------------------------------

  DEALLOCATE( lat2d, lon2d, xs2d, ys2d )
  DEALLOCATE( lon_ext, lat_ext, hgt_ext )
  DEALLOCATE( x_ext, y_ext )
  DEALLOCATE( st4p_ext )

!
!-----------------------------------------------------------------------
!
!  Restore map projection to ARPS grid.
!
!-----------------------------------------------------------------------
!
  CALL setmapr(iproj_arps,scale_arps,latnot_arps,trlon_arps)

  CALL setorig(1,xorig_arps,yorig_arps)

  RETURN
END SUBROUTINE read_intrp_stage4
!
!##################################################################
!##################################################################
!######                                                      ######
!######               SUBROUTINE READ_ST4_DIMS               ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE read_st4_dims(st4file,st4fmt,iboxs,iboxe,jboxs,jboxe,        &
                         nlon,nlat,nhgt,istatus)

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: st4fmt
  INTEGER, INTENT(IN)  :: iboxs,iboxe,jboxs,jboxe
  CHARACTER(LEN=256), INTENT(IN) :: st4file
  INTEGER, INTENT(OUT)  :: nlon, nlat, nhgt

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER :: filefmt
  LOGICAL :: crefopt
  LOGICAL :: fexists
  INTEGER :: flen

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  crefopt = (st4fmt/200 > 0)
  filefmt = MOD(st4fmt,200)

  IF (filefmt == 3) THEN   ! Stage IV precipitation data in grib format
                           ! It is NOT MRMS dataset
    IF (iboxe > iboxs .AND. jboxe > jboxs) THEN
      nlon = iboxe-iboxs+1
      nlat = jboxe-jboxs+1
    ELSE

      INQUIRE(FILE = st4file, EXIST = fexists)
      IF (.NOT. fexists) THEN
        WRITE(6,'(2a)') 'File not found: ', st4file
        istatus = -1
        RETURN
        !STOP 'read_mrms_dims'
      ENDIF

      flen = LEN_TRIM(st4file)
      CALL rdgrbdims (st4file,flen,nlon,nlat,istatus)

    END IF

    nhgt = 1

  ELSE
    WRITE(*,'(1x,2(a,I0))') 'ERROR: unsupported file format: ',filefmt,', st4fmt = ',st4fmt
    istatus = -1
    RETURN
  END IF

  RETURN
END SUBROUTINE read_st4_dims
!
!##################################################################
!##################################################################
!######                                                      ######
!######               SUBROUTINE READ_ST4                    ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE read_st4(st4file,st4fmt,nlon,nlat,nhgt,iboxs,iboxe,jboxs,jboxe,  &
                    rmissing,lon_ext,lat_ext,hgt_ext,st4p_ext,timestr,  &
                    iproj_ext,scale_ext,latnot_ext,trlon_ext,           &
                    x0_ext,y0_ext,x_ext,y_ext,istatus)

  IMPLICIT NONE

  INTEGER,            INTENT(IN)  :: st4fmt
  CHARACTER(LEN=256), INTENT(IN)  :: st4file
  INTEGER,            INTENT(IN)  :: nlon, nlat, nhgt
  INTEGER,            INTENT(IN)  :: iboxs,iboxe,jboxs,jboxe

  REAL,               INTENT(IN)  :: rmissing
  REAL,               INTENT(OUT) :: lat_ext(nlon,nlat)
  REAL,               INTENT(OUT) :: lon_ext(nlon,nlat)
  REAL,               INTENT(OUT) :: hgt_ext(nhgt)

  REAL,               INTENT(OUT) :: st4p_ext(nlon,nlat,nhgt)
  CHARACTER(LEN=19),  INTENT(OUT) :: timestr

  REAL,               INTENT(OUT) :: x_ext(nlon), y_ext(nlat)
  REAL,               INTENT(OUT) :: x0_ext, y0_ext
  INTEGER,            INTENT(OUT) :: iproj_ext
  REAL,               INTENT(OUT) :: scale_ext,trlon_ext
  REAL,               INTENT(OUT) :: latnot_ext(2)


  INTEGER,            INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INTEGER :: filefmt
  LOGICAL :: crefopt
  INTEGER :: i,j,k,ii, jj
  INTEGER :: ix1,ix2, jy1,jy2
  INTEGER :: lvldbg = 0

  INTEGER :: nx_ext, ny_ext, nz_ext
  REAL    :: latsw, lonsw, latnw, lonnw

  INTEGER :: flen

  INTEGER :: iyr, imn, iday, ihr, imin,isec

  !
  ! variables for grib format
  !
  INCLUDE 'gribcst.inc'

  CHARACTER (LEN=42) :: gridesc ! Grid description

  INTEGER :: iproj_grb    ! Map projection indicator
  INTEGER :: gthin        ! Indicator of whether the grid is "thinned"

  INTEGER :: ni_grb       ! Number of points along x-axis
  INTEGER :: nj_grb       ! Number of points along y-axis
  INTEGER :: np_grb       ! Total number of horizontal grid points

  INTEGER :: nk_grb       ! Number of vertical parameters
  REAL    :: zk_grb(1)  ! Vertical coordinate parameters

  INTEGER :: npeq         ! Number of lat circles from pole to equator
  INTEGER :: nit(1)  ! Number of x-points for thinned grid

  REAL :: pi_grb          ! x-coordinate of pole point
  REAL :: pj_grb          ! y-coordinate of pole point
  INTEGER :: ipole        ! Projection center flag

  REAL :: di_grb          ! x-direction increment or grid length
  REAL :: dj_grb          ! y-direction increment or grid length

  REAL :: latne           ! Latitude  of North East corner point
  REAL :: lonne           ! Longitude of North East corner point

  REAL :: lattru1         ! Latitude (1st) at which projection is true
  REAL :: lattru2         ! Latitude (2nd) at which projection is true
  REAL :: lontrue         ! Longitude      at which projection is true

  REAL :: latrot          ! Latitude  of southern pole of rotation
  REAL :: lonrot          ! Longitude of southern pole of rotation
  REAL :: angrot          ! Angle of rotation

  REAL :: latstr          ! Latitude  of the pole of stretching
  REAL :: lonstr          ! Longitude of the pole of stretching
  REAL :: facstr          ! Stretching factor

  INTEGER :: scanmode     ! Scanning indicator
  INTEGER :: iscan        ! x-direction   scanning indicator
  INTEGER :: jscan        ! y-direction   scanning indicator
  INTEGER :: kscan        ! FORTRAN index scanning indicator

  INTEGER :: ires         ! Resolution direction increments indicator
  INTEGER :: iearth       ! Earth shape indicator: spherical or oblate?
  INTEGER :: icomp        ! (u,v) components decomposition indicator

  INTEGER :: jpenta       ! J-Pentagonal resolution parameter
  INTEGER :: kpenta       ! K-Pentagonal resolution parameter
  INTEGER :: mpenta       ! M-Pentagonal resolution parameter
  INTEGER :: ispect       ! Spectral representation type
  INTEGER :: icoeff       ! Spectral coefficient storage mode

  REAL :: xp_grb          ! X coordinate of sub-satellite point
  REAL :: yp_grb          ! Y coordinate of sub-satellite point
  REAL :: xo_grb          ! X coordinate of image sector origin
  REAL :: yo_grb          ! Y coordinate of image sector origin
  REAL :: zo_grb          ! Camera altitude from center of Earth

  REAL     :: var_grb3d(1,1,1,1,1) ! GRIB 3-D variables
  INTEGER  :: var_lev3d(1,1,1)     ! Levels (hybrid) for
                                               ! each 3-D variable

  CHARACTER (LEN=14)  :: gribtime
  INTEGER  :: n

  INTEGER :: iproj
  REAL    :: scale, trlon
  REAL    :: latnot(2)
  REAL    :: x0, y0

  REAL,    ALLOCATABLE :: var_grb2d(:,:)       ! GRIB 2D variables

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  crefopt = (st4fmt/200 > 0)
  filefmt = MOD(st4fmt,200)

  nx_ext = 1121
  ny_ext = 881
  nz_ext = 1

  IF (iboxe > iboxs .AND. jboxe > jboxs) THEN
    ix1 = iboxs
    ix2 = iboxe
    jy1 = jboxs
    jy2 = jboxe
  ELSE
    ix1 = 1
    ix2 = nx_ext
    jy1 = 1
    jy2 = ny_ext
  END IF

!
!-----------------------------------------------------------------------
!
!  Retrieve stage IV in grib format
!
!-----------------------------------------------------------------------
!
  IF (filefmt == 3) THEN   ! Stage IV precipitation data in grib format
                                ! It is NOT MRMS dataset
    flen = LEN_TRIM(st4file)

    ! Expecting file ST4.2017051004.01h
    i = INDEX(st4file,'ST4')
    j = INDEX(st4file,'.01h')

    gribtime = st4file(i+4:j-1)//'f001'

    ALLOCATE(var_grb2d(nlon,nlat), STAT = istatus)

    gridtyp   = st4grid
    mproj_grb = st4proj

    n2dvars  = st4nvs2d
    n2dlvtps = st4nlvt2d

    DO k=1,n2dlvtps
      DO n=1,n2dvars
        var_id2d(n,k) = st4var_id2d(n,k)
      END DO
      levtyp2d(k) = st4levs2d(k)
    END DO

    n3dvars  = 1
    n3dlvtps = 1

    CALL rdnmcgrb(nlon,nlat,nhgt,st4file,flen, gribtime,                &
                  gridesc, ix1,ix2,jy1,jy2, iproj_grb, gthin,           &
                  ni_grb,nj_grb,np_grb, nk_grb,zk_grb, npeq,nit,        &
                  pi_grb,pj_grb,ipole, di_grb,dj_grb,                   &
                  latsw,lonsw, latne,lonne,                             &
                  latrot,lonrot,angrot,                                 &
                  latstr,lonstr,facstr,                                 &
                  lattru1,lattru2,lontrue,                              &
                  scanmode, iscan,jscan,kscan,                          &
                  ires,iearth,icomp,                                    &
                  jpenta,kpenta,mpenta,ispect,icoeff,                   &
                  xp_grb,yp_grb, xo_grb,yo_grb,zo_grb,                  &
                  var_grb2d,var_grb3d,var_lev3d,lvldbg,istatus)

    IF (iproj_grb /= 5) THEN
      WRITE(*,'(1x, a, I0)') 'ERROR: unsupported map projection: ', iproj_grb
      istatus = -1
      RETURN
    END IF

    !print *, nlat, nlon, lontrue, lattru1, lattru2
    !print *, di_grb, dj_grb

!-----------------------------------------------------------------------
!
!  Save orignal grid map projection information
!
!-----------------------------------------------------------------------

    CALL getmapr(iproj,scale,latnot,trlon,x0,y0)
    !print *, iproj, scale, latnot, trlon

!-----------------------------------------------------------------------
!
!  Set map projection and check for lat_ext, lon_ext etc.
!
!-----------------------------------------------------------------------

    IF ( iproj_grb == 5 .AND. ipole == 0 ) THEN       ! Center in NP
      iproj_ext = 1
    ELSE IF ( iproj_grb == 5 .AND. ipole == 1 ) THEN  ! Center in SP
      iproj_ext = -1
    ELSE IF ( iproj_grb == 3 .AND. ipole == 0 ) THEN  ! Center in NP
      iproj_ext = 2
    ELSE IF ( iproj_grb == 3 .AND. ipole == 1 ) THEN  ! Center in SP
      iproj_ext = -2
    ELSE IF ( iproj_grb == 1 ) THEN
      iproj_ext = 3
    ELSE IF ( iproj_grb == 0 ) THEN
      iproj_ext = 4
    ELSE
      WRITE (6,'(a)') 'Unknown map projection. Set to non-projection.'
      iproj_ext = 0
    END IF

    scale_ext = 1.0
    latnot_ext(1) = lattru1
    latnot_ext(2) = lattru2
    trlon_ext     = lontrue

    CALL setmapr(iproj_ext,scale_ext,latnot_ext,trlon_ext)
    CALL lltoxy(1,1,latsw,lonsw,x0_ext,y0_ext)
    CALL setorig( 1, x0_ext, y0_ext)

    DO i=ix1,ix2
      x_ext(i-ix1+1)=(i-1)*di_grb
    END DO

    DO j=jy1,jy2
      y_ext(j-jy1+1)=(j-1)*dj_grb
    END DO

    CALL xytoll(nlon,nlat,x_ext,y_ext,lat_ext,lon_ext)

!
!  Retrieve 2-D variables
!
    WRITE(6,*) 'Filling 2D arrays from GRIB data.'
    DO j=jy1,jy2
      DO i=ix1,ix2
        ii = i-ix1+1
        jj = j-jy1+1
        !IF (var_nr2d(1,1) == 0) THEN
        !  st4p(ii,jj,1) = -999.0
        !ELSE
          st4p_ext(ii,jj,1) = var_grb2d(ii,jj)
        !END IF
      END DO
    END DO

    999  CONTINUE

    READ(gribtime,'(I4.4,3I2.2)') iyr, imn, iday, ihr
    imin = 0
    isec = 0
    WRITE(timestr,'(I4.4,5(a,I2.2))') iyr,'-', imn, '-',iday, '_',ihr,':', imin,':', isec

!
!-----------------------------------------------------------------------
!
!  Restore map projection to previous values
!
!-----------------------------------------------------------------------
!

    CALL setmapr(iproj,scale,latnot,trlon)
    CALL setorig(1,x0,y0)

    DEALLOCATE(var_grb2d)

  ELSE

    WRITE(*,'(1x,2(a,I0))') 'ERROR: unsupported file format: ',filefmt,', st4fmt = ',st4fmt
    istatus = -1
    RETURN

  END IF

  WRITE(*,'(1x,a,4F8.2)') 'lat_ext, lon_ext = ',lat_ext(1,1),lat_ext(nlon,nlat),lon_ext(1,1),lon_ext(nlon,nlat)

  RETURN
END SUBROUTINE read_st4
