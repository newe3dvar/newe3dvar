MODULE module_arpsArray
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! MODULE contains variables specifically for the ARPS model only,
!
! HISTORY:
!
!   10/13/2020 (Yunheng Wang)
!   Initial version.
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
  IMPLICIT NONE
  SAVE

  REAL, ALLOCATABLE :: ptprt (:,:,:)  ! Perturbation potential temperature (K)
  REAL, ALLOCATABLE :: pprt  (:,:,:)  ! Perturbation pressure (Pascal)

  REAL, ALLOCATABLE :: ubar  (:,:,:)  ! Base state u-velocity (m/s)
  REAL, ALLOCATABLE :: vbar  (:,:,:)  ! Base state v-velocity (m/s)
  REAL, ALLOCATABLE :: ptbar (:,:,:)  ! Base state potential temperature (K)
  REAL, ALLOCATABLE :: pbar  (:,:,:)  ! Base state pressure (Pascal).

  REAL, ALLOCATABLE :: wcont (:,:,:)  ! Contravariant vertical velocity (m/s)
  REAL, ALLOCATABLE :: j3soil(:,:,:)  ! Coordinate transformation Jacobian defined
                                      ! as d( zpsoil )/d( z ).
  REAL, ALLOCATABLE :: j3soilinv (:,:,:)! Inverse of j3soil

  REAL, ALLOCATABLE :: tke   (:,:,:)  ! Turbulent Kinetic Energy ((m/s)**2)


  REAL, ALLOCATABLE :: udteb (:,:)    ! T-tendency of u at e-boundary (m/s**2)
  REAL, ALLOCATABLE :: udtwb (:,:)    ! T-tendency of u at w-boundary (m/s**2)
  REAL, ALLOCATABLE :: vdtnb (:,:)    ! T-tendency of v at n-boundary (m/s**2)
  REAL, ALLOCATABLE :: vdtsb (:,:)    ! T-tendency of v at s-boundary (m/s**2)

  REAL, ALLOCATABLE :: pdteb (:,:)    ! T-tendency of pprt at e-boundary (PASCAL/s)
  REAL, ALLOCATABLE :: pdtwb (:,:)    ! T-tendency of pprt at w-boundary (PASCAL/s)
  REAL, ALLOCATABLE :: pdtnb (:,:)    ! T-tendency of pprt at n-boundary (PASCAL/s)
  REAL, ALLOCATABLE :: pdtsb (:,:)    ! T-tendency of pprt at s-boundary (PASCAL/s)

  REAL, ALLOCATABLE :: ppi(:,:,:)     ! Exner function.
  REAL, ALLOCATABLE :: csndsq(:,:,:)  ! Speed of sound squared

!
!-----------------------------------------------------------------------
!
!  ARPS Surface variables:
!
!-----------------------------------------------------------------------
!
  REAL,    ALLOCATABLE :: radfrc(:,:,:)  ! Radiation forcing (K/s)
  REAL,    ALLOCATABLE :: radsw(:,:)     ! Solar radiation reaching the surface
  REAL,    ALLOCATABLE :: rnflx(:,:)     ! Net absorbed radiation by the surface
  REAL,    ALLOCATABLE :: radswnet(:,:)  ! Net shortwave radiation
  REAL,    ALLOCATABLE :: radlwin(:,:)   ! Incominging longwave radiation

  INTEGER, ALLOCATABLE :: soiltyp (:,:,:) ! Soil type
  REAL,    ALLOCATABLE :: stypfrct(:,:,:) ! Soil type fraction
  INTEGER, ALLOCATABLE :: vegtyp (:,:)    ! Vegetation type
  REAL,    ALLOCATABLE :: lai    (:,:)    ! Leaf Area Index
  REAL,    ALLOCATABLE :: roufns (:,:)    ! Surface roughness
  REAL,    ALLOCATABLE :: veg    (:,:)    ! Vegetation fraction

  REAL,    ALLOCATABLE, TARGET :: tsoil(:,:,:,:) ! Soil temperature (K)
  REAL,    ALLOCATABLE :: qsoil(:,:,:,:) ! Soil moisture
  REAL,    ALLOCATABLE :: qvsfc(:,:,:)   ! Effective qv at sfc.

  REAL,    ALLOCATABLE :: wetcanp(:,:,:) ! Canopy water amount
  REAL,    ALLOCATABLE :: snowdpth(:,:)  ! Snow depth (:)

  REAL,    ALLOCATABLE :: ptcumsrc(:,:,:)! Source term in pt-equation due
                                         ! to cumulus parameterization
  REAL,    ALLOCATABLE :: qcumsrc(:,:,:,:) ! Source term in Qv equation due
                                         ! to cumulus parameterization

  REAL,    ALLOCATABLE :: w0avg(:,:,:)   ! a closing running average vertical
                                         ! velocity in 10min for K-F scheme
  REAL,    ALLOCATABLE :: kfraincv(:,:)  ! K-F convective rainfall (:)
  INTEGER, ALLOCATABLE :: nca(:,:)    ! K-F counter for CAPE release
  REAL,    ALLOCATABLE :: cldefi(:,:)    ! BMJ cloud efficiency
  REAL,    ALLOCATABLE :: xland(:,:)     ! BMJ land mask
                                      !   (1.0 = land, 2.0 = sea)
  REAL,    ALLOCATABLE :: bmjraincv(:,:) ! BMJ convective rainfall (cm)


  REAL,    ALLOCATABLE :: raing(:,:)     ! Grid supersaturation rain
  REAL,    ALLOCATABLE :: rainc(:,:)     ! Cumulus convective rain
  REAL,    ALLOCATABLE :: prcrate(:,:,:) ! precipitation rate (kg/(m**2*s))
                                         ! prcrate(:,:,:) = total precipitation rate
                                         ! prcrate(:,:,:) = grid scale precip. rate
                                         ! prcrate(:,:,:) = cumulus precip. rate
                                         ! prcrate(:,:,:) = microphysics precip. rate

  REAL,    ALLOCATABLE :: usflx (:,:)    ! Surface flux of u-momentum (kg/(m*s**2))
  REAL,    ALLOCATABLE :: vsflx (:,:)    ! Surface flux of v-momentum (kg/(m*s**2))
  REAL,    ALLOCATABLE :: ptsflx(:,:)    ! Surface heat flux (K*kg/(m*s**2))
  REAL,    ALLOCATABLE :: qvsflx(:,:)    ! Surface moisture flux (kg/(m**2*s))

  REAL,    ALLOCATABLE :: trigs1(:)   ! Array containing pre-computed trig
                                      ! function for use in fft.
  REAL,    ALLOCATABLE :: trigs2(:)   ! Array containing pre-computed trig
                                      ! function for use in fft.
  INTEGER, ALLOCATABLE :: ifax1(:)    ! Array containing the factors of nx.
  INTEGER, ALLOCATABLE :: ifax2(:)    ! Array containing the factors of ny.

  REAL, ALLOCATABLE :: vwork1 (:,:)   ! 2-D work array for fftopt=2.
  REAL, ALLOCATABLE :: vwork2 (:,:)   ! 2-D work array for fftopt=2.
  REAL, ALLOCATABLE :: wsave1 (:)     ! Work array for fftopt=2.
  REAL, ALLOCATABLE :: wsave2 (:)     ! Work array for fftopt=2.

  REAL :: exbcbuf( 1 ) ! EXBC buffer array (unused)

  REAL,    POINTER     :: tsfc(:,:)

  LOGICAL, PRIVATE :: array_allocated  = .FALSE.
  LOGICAL, PRIVATE :: array_initgrdvar = .FALSE.

  CONTAINS

  SUBROUTINE allocate_arpsArray(nx,ny,nz,nzsoil,nstyps,istatus)

    INTEGER, INTENT(IN)  :: nx,ny,nz,nzsoil, nstyps
    INTEGER, INTENT(OUT) :: istatus

    INCLUDE 'globcst.inc'

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ALLOCATE(ptprt(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:ptprt")
    ALLOCATE(pprt (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:pprt")

    ptprt= 0.0
    pprt = 0.0

    ALLOCATE(tke  (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:tke")
    tke     = 0.0

    ALLOCATE(ubar (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:ubar")
    ALLOCATE(vbar (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:vbar")
    ALLOCATE(ptbar(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:ptbar")
    ALLOCATE(pbar (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:pbar")

    ubar =0.0
    vbar =0.0
    ptbar=0.0
    pbar =0.0

    ALLOCATE(j3soil(nx,ny,nzsoil),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:j3soil")

    ALLOCATE(j3soilinv(nx,ny,nzsoil),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:j3soilinv")

    j3soil   =0.0
    j3soilinv=0.0

    ALLOCATE(ppi   (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:ppi")
    ppi   =0.0

    !----------------------------------------------------------------------
    !
    !  Allocate soil arrays
    !
    !-----------------------------------------------------------------------
    !
    ALLOCATE(soiltyp(nx,ny,nstyps),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:soiltyp")
    ALLOCATE(stypfrct(nx,ny,nstyps),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:stypfrct")
    ALLOCATE(vegtyp (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:vegtyp")
    ALLOCATE(lai    (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:lai")
    ALLOCATE(roufns (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:roufns")
    ALLOCATE(veg    (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:veg")

    soiltyp=0.0
    stypfrct=0.0
    vegtyp =0.0
    lai    =0.0
    roufns =0.0
    veg    =0.0

    ALLOCATE(qvsfc  (nx,ny,0:nstyps),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:qvsfc")
    ALLOCATE(tsoil  (nx,ny,nzsoil,0:nstyps),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:tsoil")
    ALLOCATE(qsoil  (nx,ny,nzsoil,0:nstyps),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:qsoil")
    ALLOCATE(wetcanp(nx,ny,0:nstyps),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:wetcanp")
    ALLOCATE(snowdpth(nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:snowdpth")

    qvsfc  =0.0
    tsoil  =0.0
    qsoil  =0.0
    wetcanp=0.0
    snowdpth=0.0

    ALLOCATE(ptcumsrc(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:ptcumsrc")
    ALLOCATE(qcumsrc (nx,ny,nz,5),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:qcumsrc")
    ALLOCATE(w0avg   (nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:w0avg")
    ALLOCATE(nca    (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:nca")
    ALLOCATE(kfraincv (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:kfraincv")
    ALLOCATE(cldefi (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:cldefi")
    ALLOCATE(xland  (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:xland")
    ALLOCATE(bmjraincv (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:bmjraincv")
    ALLOCATE(raing  (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:raing")
    ALLOCATE(rainc  (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:rainc")

    ptcumsrc=0.0
    qcumsrc =0.0
    w0avg   =0.0
    nca    =0.0
    kfraincv =0.0
    cldefi = 0.0
    xland = 0.0
    bmjraincv = 0.0
    raing  =0.0
    rainc  =0.0

    ALLOCATE(ptsflx(nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:ptsflx")
    ALLOCATE(qvsflx(nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:qvsflx")

    ptsflx=0.0
    qvsflx=0.0

    ALLOCATE(radfrc(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:radfrc")
    ALLOCATE(radsw (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:radsw")
    ALLOCATE(rnflx (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:rnflx")
    ALLOCATE(radswnet (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:radswnet")
    ALLOCATE(radlwin (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:radlwin")

    radfrc=0.0
    radsw =0.0
    rnflx =0.0
    radswnet =0.0
    radlwin =0.0

    ALLOCATE(trigs1(3*(nx-1)/2+1),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:trigs1")
    ALLOCATE(trigs2(3*(ny-1)/2+1),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:trigs2")
    ALLOCATE(ifax1(13),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:ifax1")
    ALLOCATE(ifax2(13),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:ifax2")

    trigs1=0.0
    trigs2=0.0
    ifax1=0.0
    ifax2=0.0

    ALLOCATE(vwork1(nx+1,ny+1),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:vwork1")
    ALLOCATE(vwork2(ny,nx+1),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:vwork2")

    ALLOCATE(wsave1(3*(ny-1)+15),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:wsave1")
    ALLOCATE(wsave2(3*(nx-1)+15),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:wsave2")

    vwork1=0.0
    vwork2=0.0
    wsave1=0.0
    wsave2=0.0

    array_initgrdvar = .TRUE.

    ALLOCATE(prcrate(nx,ny,4),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:prcrate")
    prcrate=0.0

    ALLOCATE(usflx (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:usflx")
    ALLOCATE(vsflx (nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "adas:vsflx")
    usflx = 0.0
    vsflx = 0.0

    tsfc => tsoil(:,:,1,0)

    array_allocated  = .TRUE.

    RETURN
  END  SUBROUTINE allocate_arpsArray

  !#####################################################################

  SUBROUTINE deallocate_arpsArray(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (array_allocated) THEN

      DEALLOCATE(ubar,vbar,ptbar,pbar)

      DEALLOCATE(ppi, tke )

      DEALLOCATE( soiltyp, stypfrct, vegtyp, lai, roufns, veg )
      DEALLOCATE( qvsfc, tsoil, qsoil, wetcanp, snowdpth )

      IF (array_initgrdvar) THEN
        DEALLOCATE( ptcumsrc, qcumsrc, w0avg, nca, kfraincv, cldefi, xland  )
        DEALLOCATE( bmjraincv )
        DEALLOCATE( vwork1, vwork2, wsave1, wsave2 )
        DEALLOCATE( trigs1,trigs2,ifax1,ifax2 )

        DEALLOCATE(j3soil,j3soilinv)

        array_initgrdvar = .FALSE.
      END IF

      DEALLOCATE( raing, rainc, prcrate )

      DEALLOCATE( usflx, vsflx, ptsflx, qvsflx )

      DEALLOCATE(radfrc,radsw,rnflx,radswnet,radlwin )

      DEALLOCATE( usflx, vsflx )
    END IF

    array_allocated = .FALSE.

    RETURN
  END SUBROUTINE deallocate_arpsArray

  !#####################################################################

  SUBROUTINE deallocate_arpsArray_after_initgrdvar(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (array_initgrdvar ) THEN
      DEALLOCATE( ptcumsrc, qcumsrc, w0avg, nca, kfraincv, cldefi, xland  )
      DEALLOCATE( bmjraincv )
      DEALLOCATE( vwork1, vwork2, wsave1, wsave2 )
      DEALLOCATE( trigs1,trigs2,ifax1,ifax2 )

      DEALLOCATE(j3soil,j3soilinv)
    END IF

    array_initgrdvar = .FALSE.

    RETURN
  END SUBROUTINE deallocate_arpsArray_after_initgrdvar

  !#####################################################################

  SUBROUTINE allocate_arpsArray_for_hydr(nx,ny,nz,istatus)

    INTEGER, INTENT(IN)  :: nx,ny,nz
    INTEGER, INTENT(OUT) :: istatus

    ALLOCATE(csndsq(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:csndsq")
    csndsq=0.0

    RETURN
  END  SUBROUTINE allocate_arpsArray_for_hydr

  !#####################################################################

  SUBROUTINE deallocate_arpsArray_for_hydr(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    DEALLOCATE(csndsq )

    RETURN
  END SUBROUTINE deallocate_arpsArray_for_hydr

  !#####################################################################

  SUBROUTINE allocate_arpsArray_for_lbc(nx,ny,nz,istatus)

    INTEGER, INTENT(IN)  :: nx,ny,nz
    INTEGER, INTENT(OUT) :: istatus

    ALLOCATE(wcont(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:wcont")
    wcont= 0.0

    ALLOCATE(udteb(ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:udteb")
    ALLOCATE(udtwb(ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:udtbw")
    ALLOCATE(vdtnb(nx,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:vdtnb")
    ALLOCATE(vdtsb(nx,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:vdtsb")
    ALLOCATE(pdteb(ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:pdteb")
    ALLOCATE(pdtwb(ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:pdtwb")
    ALLOCATE(pdtnb(nx,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:pdtnb")
    ALLOCATE(pdtsb(nx,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:pdtsb")

    udteb=0.0
    udtwb=0.0
    vdtnb=0.0
    vdtsb=0.0

    pdteb=0.0
    pdtwb=0.0
    pdtnb=0.0
    pdtsb=0.0

    RETURN
  END  SUBROUTINE allocate_arpsArray_for_lbc

  !#####################################################################

  SUBROUTINE deallocate_arpsArray_for_lbc(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    DEALLOCATE(udteb,udtwb,vdtnb,vdtsb )
    DEALLOCATE(pdteb,pdtwb,pdtnb,pdtsb )

    DEALLOCATE( wcont)

    RETURN
  END SUBROUTINE deallocate_arpsArray_for_lbc

END module module_arpsArray


