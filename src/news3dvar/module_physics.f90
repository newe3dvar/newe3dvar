MODULE module_physics
  
  USE model_precision
  
  IMPLICIT NONE
  
  REAL(P), PARAMETER, PUBLIC    :: p0 = 100000.0,                         &
                                   rd = 287.05,                           & ! gas constant for dry air (J kg-1 K-1)
                                   rv = 461.0,                            & ! gas constant for water vapor (J kg-1 K-1)
                                   cp = 1004.,                            & ! specific heat at constant pressure (J kg-1 K-1)
                                   mweightwat = 18.016 * 0.001,           & ! Molecular weight of water (kg mol-1)
                                   mweightdry = 28.97 * 0.001,            & ! Molecular weight of dry air (kg mol-1)
                                   epsilon    = mweightwat / mweightdry,  &
                                   rddcp = rd / cp,                       &
                                   rddrv = rd / rv
                                   
  CONTAINS

  !#######################################################################
  !
  ! Calculate saturated specific humidity based on Clausius-Clapeyron Equation
  ! that account variations with temperature of lv and ls (latent heat),
  ! respectively.
  ! Bohren and Albrecht (Atmospheric Thermodynamics, Oxford University Press, 
  ! New York, 1998, ISBN 0-19-509904-4)
  !
  !#######################################################################

  SUBROUTINE calc_satq_cc(pressure, temperature, saturated_q)
    
    IMPLICIT NONE
    
    REAL(P), INTENT(IN)           :: pressure, temperature              ! Pressure in Pa, temperature in C
    REAL(P), INTENT(OUT)          :: saturated_q
    
    REAL(P)                       :: tkelvin, satvppres
    REAL(P)                       :: intmedvar1, intmedvar2

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    
    ![Step. 1] Coner Celsius to Kelvin
    tkelvin = temperature + 273.15
    
    ![Step. 2] Calculate saturation vapor pressure in Pa
    !          based on Clausius-Clapeyron Equation
    intmedvar1 = 1. / 273. - 1. / tkelvin
    intmedvar2 = tkelvin / 273.
    IF (temperature >= 0) THEN
      satvppres = 611 * EXP(6808 * intmedvar1) / (intmedvar2 ** 5.09)
    ELSE
      satvppres = 611 * EXP(6293 * intmedvar1) / (intmedvar2 ** 0.555)
    END IF
    
    ![Step. 3] Calculate saturation specific humidity by using
    !          atmospheric pressure and saturation vapor pressure
    saturated_q = epsilon * satvppres / (pressure - satvppres)
    
    RETURN
    
  END SUBROUTINE calc_satq_cc

  !#######################################################################
  !
  ! Calculate saturated specific humidity based on Tetens's Equation
  !
  !#######################################################################

  SUBROUTINE calc_satq_tetens(pressure, temperature, saturated_q)

    IMPLICIT NONE

    REAL(P), INTENT(IN)           :: pressure, temperature              ! Pressure in Pa, temperature in C
    REAL(P), INTENT(OUT)          :: saturated_q

    REAL(P)                       :: satvppres, consta, constb, constc, &
                                     constd, conste
    REAL(P)                       :: intmedvar

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    ![Step. 1] Setup constants based on temperature (Tetens's Equation)
    IF (temperature >= 0) THEN
      consta = 611.21
      constb = 17.502
      constc = 240.97
      constd = 1.0007
      conste = 3.46E-8
    ELSE
      consta = 611.15
      constb = 22.452
      constc = 272.55
      constd = 1.0003
      conste = 4.18E-8
    END IF

    ![Step. 2] Calculate saturation vapor pressure in Pa
    intmedvar = constd + conste * pressure
    satvppres = intmedvar * consta * EXP(constb * temperature / (temperature + constc))

    ![Step. 3] Calculate saturation specific humidity by using
    !          atmospheric pressure and saturation vapor pressure
    saturated_q = rddrv * satvppres / (pressure - (1 - rddrv) * satvppres)

    RETURN

  END SUBROUTINE calc_satq_tetens
  
END MODULE module_physics
