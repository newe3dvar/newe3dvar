!
!##################################################################
!##################################################################
!######                                                      ######
!######                 SUBROUTINE VBL_TO_CTR                ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
!-----------------------------------------------------------------------
!
!  PURPOSE: Transfer from analysis variables to control variables.
!
!-----------------------------------------------------------------------
!
!  AUTHOR:  Jidong GAO, CAPS, June, 2000
!
!-----------------------------------------------------------------------
!
SUBROUTINE vbl_to_ctr(ipass_filt,hradius_3d,radius_z,nx,ny,nz,stagdim,     &
                      pbkg, pscal,pgrd,tem1,tem2,tem3,tem4)


  IMPLICIT NONE

  INTEGER, INTENT(IN) :: ipass_filt
  INTEGER, INTENT(IN) :: nx,ny,nz
  INTEGER, INTENT(IN) :: stagdim
!    stagdim  Dimension of grid staggering:
!              =0, no staggering;
!              =1, staggered in the x-direction (e.g. u);
!              =2, staggered in the y-direction (e.g. v);
!              =3, staggered in the z-direction (e.g. w).

  REAL,    INTENT(IN) :: hradius_3d(nx,ny,nz)
  REAL, INTENT(IN)    :: radius_z(nx,ny,nz)
  REAL, INTENT(IN)    :: pbkg (nx,ny,nz)
  REAL, INTENT(IN)    :: pscal (nx,ny,nz)
  REAL, INTENT(INOUT) :: pgrd (nx,ny,nz)
!
  REAL    :: tem1 (nx,ny,nz)
  REAL    :: tem2 (nx,ny,nz)
  REAL    :: tem3 (nx,ny,nz)
  REAL    :: tem4 (nx,ny,nz)
!

  INTEGER :: i, j, k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  DO k = 1, nz
    DO j = 1, ny
      DO i = 1, nx
        pgrd(i,j,k) =  pgrd(i,j,k)*pbkg(i,j,k)
      END DO
    END DO
  END DO

  DO k = 1, nz
    DO j = 1, ny
      DO i = 1, nx
        pgrd(i,j,k) = pgrd(i,j,k)*pscal(i,j,k)
      END DO
    END DO
  END DO

!  CALL recurfilt3d(nx,ny,nz,pgrd,ipass_filt,ipass_filt/2,stagdim,       &
!                   hradius,radius_z,tem1,tem2,tem3,tem4)

  CALL ad_recurfilt3d(nx,ny,nz,pgrd,ipass_filt,ipass_filt/2,stagdim,    &
                          hradius_3d,radius_z,tem1,tem2,tem3,tem4)

  RETURN
END SUBROUTINE vbl_to_ctr
