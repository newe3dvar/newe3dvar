SUBROUTINE prepcwpobs(nx,ny,nz,nxlg,nylg,xs,ys,idealopt,istatus)

  USE module_cwpobs
  USE module_cwp

  IMPLICIT NONE

  INCLUDE 'grid.inc'
  INCLUDE 'mp.inc'
  INCLUDE 'globcst.inc'
  INTEGER       :: idealopt,nxlg,nylg
  INTEGER       :: file_status
  INTEGER       :: i, j, ii, jj
  INTEGER       :: nx,ny,nz
  REAL          :: xs(nx),ys(ny)
  REAL          :: xctr,yctr,tdx,tdy,xoset,yoset
  REAL          :: xtdom,ytdom,xbdom,ybdom

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INTEGER :: nobs_cwp, nobs_psd, nobs_cwp_sum, nobs_psd_sum

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0
  IF (ncwpfil==0 .or. satcwpobs==0) RETURN

  CALL cwpobs_read(idealopt,istatus)

  IF (idealopt==0) THEN

    nobs_cwp=0
    nobs_psd=0
    nobs_cwp_sum=0
    nobs_psd_sum=0
    DO i=1,valid_num
      IF ( xloc(i) >= xs(1) .and. xloc(i) < xs(nx-1) .and.              &
           yloc(i) >= ys(1) .and. yloc(i) < ys(ny-1) ) THEN
        IF (spctopt==0 .and. (cldphase1(i)<=2) ) THEN
          nobs_cwp=nobs_cwp+1
        ELSE IF (spctopt==1 .and. (cldphase1(i)<=2 .or. cldphase1(i)>=6) ) THEN
          nobs_cwp=nobs_cwp+1
        ELSE IF (cldphase1(i)==4) THEN
          IF (pseudo_opt==1) THEN
            nobs_psd=nobs_psd+1
          ELSE
            nobs_cwp=nobs_cwp+1
          END IF
        END IF
      END IF
    END DO

    CALL cwp_alloc_within_dom(valid_num,nobs_cwp,nobs_psd,nz,nscalarq,istatus)

    ii=0
    jj=0
    DO i=1,valid_num
      IF ( xloc(i) >= xs(1) .AND. xloc(i) < xs(nx-1) .AND.              &
           yloc(i) >= ys(1) .AND. yloc(i) < ys(ny-1) ) THEN
        IF (spctopt==0 .AND. cldphase1(i)<=2) THEN
          ii=ii+1
          xcwp(ii)=xloc(i)
          ycwp(ii)=yloc(i)

          IF (cwp1(i) >= 0.03) THEN
            cldbase(ii)=cldbase1(i)
            cldtop(ii)=cldtop1(i)
            obcwp(ii)=cwp1(i)
            cwperr(ii)=cwperr1(i)
            cldphase(ii)=cldphase1(i)
            cwpflag(ii)=1
          ELSE IF (cwp1(i)< 0.03) THEN
            !obcwp(ii)=0.
            !cldphase(ii)=2
            !cwpflag(ii)=-1
            cldbase(ii)=101300.
            cldtop(ii)=5000.
            obcwp(ii)=0.
            cwperr(ii)=cwperr1(i)
            cldphase(ii)=2
            cwpflag(ii)=1
          END IF
          IF ( mp_opt>0 ) THEN
            IF (cwpflag(ii)>0) THEN
              IF (loc_x>1 .and. xloc(i)<xs(2)) cwpusetype(ii)=1
              IF (loc_y>1 .and. yloc(i)<ys(2)) cwpusetype(ii)=1
              IF (cwpusetype(ii) == 2) nobs_cwp_sum = nobs_cwp_sum + 1
            ELSE
              cwpusetype(ii)=cwpflag(ii)
            END IF
          END IF
        ELSE IF ( spctopt==1 .AND. (cldphase1(i)<=2 .OR. cldphase1(i)>=6) ) THEN
          ii=ii+1
          xcwp(ii)=xloc(i)
          ycwp(ii)=yloc(i)

          IF (cwp1(i)>=0.03) THEN
            cldbase(ii)=cldbase1(i)
            cldtop(ii)=cldtop1(i)
            obcwp(ii)=cwp1(i)
            cwperr(ii)=cwperr1(i)
            IF (cldphase1(i)==6) THEN
              cldphase(ii)=1
            ELSE IF (cldphase1(i)==7) THEN
              cldphase(ii)=2
            ELSE
              cldphase(ii)=cldphase1(i)
            END IF
            cwpflag(ii)=1
          ELSE IF (cwp1(i)<0.03) THEN
            cldbase(ii)=101300.
            cldtop(ii)=5000.
            obcwp(ii)=0.
            cwperr(ii)=cwperr1(i)
            cldphase(ii)=2
            cwpflag(ii)=1
          END IF
          IF ( mp_opt>0 ) THEN
            IF (cwpflag(ii)>0) THEN
              IF (loc_x>1 .and. xloc(i)<xs(2)) cwpusetype(ii)=1
              IF (loc_y>1 .and. yloc(i)<ys(2)) cwpusetype(ii)=1
              IF (cwpusetype(ii) == 2) nobs_cwp_sum = nobs_cwp_sum + 1
            ELSE
              cwpusetype(ii)=cwpflag(ii)
            END IF
          END IF
        ELSE IF (cldphase1(i)==4) THEN
          IF (pseudo_opt==1) THEN
            jj=jj+1
            xpsd(jj)=xloc(i)
            ypsd(jj)=yloc(i)
            psdflag(jj)=2
            IF ( mp_opt>0 .and. psdflag(jj)>0) THEN
              IF (loc_x>1 .and. xloc(i)<xs(2)) psdflag(jj)=1
              IF (loc_y>1 .and. yloc(i)<ys(2)) psdflag(jj)=1
              IF (psdflag(ii) == 2) nobs_psd_sum = nobs_psd_sum + 1
            END IF
          ELSE
            ii=ii+1
            xcwp(ii)=xloc(i)
            ycwp(ii)=yloc(i)
            cldbase(ii)=101300.
            cldtop(ii)=5000.
            obcwp(ii)=0.
            cwperr(ii)=cwperr1(i)
            cldphase(ii)=2
            cwpflag(ii)=2
            IF ( mp_opt>0 ) THEN
              IF (cwpflag(ii)>0) THEN
                IF (loc_x>1 .and. xloc(i)<xs(2)) cwpusetype(ii)=1
                IF (loc_y>1 .and. yloc(i)<ys(2)) cwpusetype(ii)=1
                IF (cwpusetype(ii) == 2) nobs_cwp_sum = nobs_cwp_sum + 1
              ELSE
                cwpusetype(ii)=cwpflag(ii)
              END IF
            END IF
          END IF
        END IF
      END IF

    END DO

  ELSE

    IF (myproc==0) THEN
      nobs_psd=0
      IF (pseudo_opt==1) THEN
        DO i=1,valid_num
          IF (cldphase1(i)==4) THEN
            nobs_psd=nobs_psd+1
          END IF
        END DO
        nobs_cwp=valid_num-nobs_psd
      ELSE
        nobs_cwp=valid_num
      END IF
    END IF
    IF (mp_opt>0) THEN
      CALL mpupdatei(nobs_cwp,1)
      CALL mpupdatei(nobs_psd,1)
    END IF

    CALL cwp_alloc_within_dom(valid_num,nobs_cwp,nobs_psd,nz,nscalarq,istatus)

    ii=0
    jj=0
    DO i=1,valid_num
      IF ( xloc(i) >= xs(2) .and. xloc(i) < xs(nx-1) .and. &
           yloc(i) >= ys(2) .and. yloc(i) < ys(ny-1) ) THEN
        IF (spctopt==0 .and. cldphase1(i)<=2) THEN
          ii=ii+1
          cwpflag(ii)=1
          xcwp(ii)=xloc(i)
          ycwp(ii)=yloc(i)
          obcwp(ii)=cwp1(i)
          cwperr(ii)=cwperr1(i)
          cldbase(ii)=cldbase1(i)
          cldtop(ii)=cldtop1(i)
          cldphase(ii)=cldphase1(i)
        ELSE IF (spctopt==1 .and. (cldphase1(i)<=2 .or. &
                  cldphase1(i)>=6) ) THEN
          ii=ii+1
          cwpflag(ii)=1
          xcwp(ii)=xloc(i)
          ycwp(ii)=yloc(i)
          obcwp(ii)=cwp1(i)
          cwperr(ii)=cwperr1(i)
          cldbase(ii)=cldbase1(i)
          cldtop(ii)=cldtop1(i)
          IF (cldphase1(i)==6) THEN
            cldphase(ii)=1
          ELSE IF (cldphase1(i)==7) THEN
            cldphase(ii)=2
          ELSE
            cldphase(ii)=cldphase1(i)
          END IF
        ELSE IF (cldphase1(i)==4) THEN
          IF (pseudo_opt==1) THEN
            jj=jj+1
            psdflag(jj)=2
            xpsd(jj)=xloc(i)
            ypsd(jj)=yloc(i)
          ELSE
            ii=ii+1
            cwpflag(ii)=2
            xcwp(ii)=xloc(i)
            ycwp(ii)=yloc(i)
            obcwp(ii)=0.
            cwperr(ii)=cwperr1(i)
            cldbase(ii)=cldbase1(i)
            cldtop(ii)=cldtop1(i)
            cldphase(ii)=2
          END IF
        END IF
        nobs_cwp_sum = nobs_cwp_sum + 1
      END IF
    END DO

  END IF

  CALL mpsumi(nobs_cwp_sum,1)
  CALL mpsumi(nobs_psd_sum,1)
  IF (myproc == 0) WRITE(*,'(1x,2(a,I0))') 'INFO: nobs_cwp = ',nobs_cwp_sum,', nobs_psd = ',nobs_psd_sum

  CALL cwpobs_dealloc_array(istatus)

END SUBROUTINE prepcwpobs

SUBROUTINE preptpwobs(nx,ny,nz,nxlg,nylg,xs,ys,idealopt,istatus)

  USE module_tpwobs
  USE module_tpw

  IMPLICIT NONE

  INCLUDE 'grid.inc'
  INCLUDE 'mp.inc'
  INCLUDE 'globcst.inc'
  INTEGER       :: idealopt,nxlg,nylg
  INTEGER       :: file_status
  INTEGER       :: i, ii
  INTEGER       :: nx,ny,nz
  INTEGER       :: ibgn,iend,jbgn,jend
  REAL          :: xs(nx),ys(ny)
  REAL          :: xctr,yctr,tdx,tdy,xoset,yoset
  REAL          :: xtdom,ytdom,xbdom,ybdom
  REAL          :: tpwlat,tpwlon

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INTEGER :: nx1,ny1

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  IF (ntpwfil==0 .or. sattpwobs==0) RETURN

  IF (idealopt==0) THEN

    IF (myproc==0) THEN
       ALLOCATE(PRO(1000000,8))
       OPEN(25,File=tpwfname(1),access='direct',recl=4*8,convert='little_endian')
       DO i=1,1000000
          READ(25,rec=i,ERR=1001) PRO(i,:)
       END DO
  1001 CONTINUE
       tottpw=i-1
       ALLOCATE(tpw_lat(tottpw))
       ALLOCATE(tpw_lon(tottpw))
    END IF

    IF (mp_opt>0) CALL mpupdatei(tottpw,1)
    CALL pwobs_alloc_valid_array(tottpw,istatus)

    IF (myproc==0) THEN
       DO i=1,tottpw
          tpw_lat(i)=PRO(i,1)
          tpw_lon(i)=PRO(i,2)
          tpworig(i)=PRO(i,3)
          pwlowlv(i)=PRO(i,4)
          pwmidlv(i)=PRO(i,5)
          pwhighlv(i)=PRO(i,6)
       END DO
       CALL lltoxy(tottpw,1,tpw_lat,tpw_lon,xloc,yloc)

!      DEALLOCATE(PRO,tpw_lat,tpw_lon)
    END IF

    IF (mp_opt>0 .and. tottpw>0) THEN
       CALL mpupdater(xloc,tottpw)
       CALL mpupdater(yloc,tottpw)
       CALL mpupdater(tpworig,tottpw)
       CALL mpupdater(pwlowlv,tottpw)
       CALL mpupdater(pwmidlv,tottpw)
       CALL mpupdater(pwhighlv,tottpw)
    END IF

    ibgn=1;iend=nx-1;jbgn=1;jend=ny-1

    nobstpw=0
    DO i=1,tottpw
      IF ( xloc(i) > xs(ibgn) .and. xloc(i) < xs(iend) .and. &
           yloc(i) > ys(jbgn) .and. yloc(i) < ys(jend) ) THEN
         nobstpw=nobstpw+1
      END IF
    END DO
    CALL pw_alloc_within_dom(istatus)
    ii=0
    DO i=1,tottpw
      IF ( xloc(i) > xs(ibgn) .and. xloc(i) < xs(iend) .and. &
           yloc(i) > ys(jbgn) .and. yloc(i) < ys(jend) ) THEN
         ii=ii+1
         xtpw(ii)=xloc(i)
         ytpw(ii)=yloc(i)
         obtpw(ii)=tpworig(i)
         obpwll(ii)=pwlowlv(i)
         obpwlm(ii)=pwmidlv(i)
         obpwlh(ii)=pwhighlv(i)
         tpwflag(ii)=2
         IF ( mp_opt>0 ) THEN
           IF (loc_x>1 .and. xloc(i)<xs(ibgn+1)) tpwflag(ii)=1
           IF (loc_y>1 .and. yloc(i)<ys(jbgn+1)) tpwflag(ii)=1
         END IF
      END IF
    END DO

    CALL pwobs_dealloc_valid_array(istatus)

  ELSE

    IF (myproc==0) THEN
      OPEN(25,File=tpwfname(1),Form='Unformatted')
      READ(25) nx1,ny1,nobstpw
      print*,'ideal data dimension===============',nx1,ny1,nobstpw
      IF (nx1*ny1/=nobstpw) THEN
        CALL arpsstop('ERROR:Dimension of obs mismatch the obs file.',1)
      END IF
      !tottpw=nobstpw
    END IF

    IF (mp_opt>0) CALL mpupdatei(nobstpw,1)
    CALL tpw_alloc_within_dom(istatus)

    IF (myproc==0) THEN
      READ(25) (xtpw(i),i=1,nobstpw)
      READ(25) (ytpw(i),i=1,nobstpw)
      READ(25) (obtpw(i),i=1,nobstpw)
      READ(25) (tpwflag(i),i=1,nobstpw)
      CLOSE(25)
    END IF

    IF (mp_opt>0) THEN
      CALL mpupdater(xtpw,nobstpw)
      CALL mpupdater(ytpw,nobstpw)
      CALL mpupdater(obtpw,nobstpw)
      CALL mpupdatei(tpwflag,nobstpw)
    END IF

    DO i=1,nobstpw
      IF (tpwflag(i)==1) THEN
        IF ( xtpw(i) >= xs(2) .and. xtpw(i) < xs(nx-1) .and. &
             ytpw(i) >= ys(2) .and. ytpw(i) < ys(ny-1) ) THEN
          CONTINUE
  !     tpwflag(i)=1
        ELSE
          tpwflag(i)=-1
        END IF
      END IF
    END DO

  END IF

  RETURN
END SUBROUTINE preptpwobs

