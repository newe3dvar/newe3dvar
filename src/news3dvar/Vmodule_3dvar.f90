MODULE Vmodule_3dvar

  USE model_precision
  USE module_varpara

  IMPLICIT NONE
  SAVE

!
! 0. 3DVAR analysis constants
!    ------------------------
!

  INTEGER :: mgra, nxm, nwork
  INTEGER :: numctr
!
! 1. work arrays depend on model grid only
!    -------------------------------------
!
  REAL(P), DIMENSION (:,:,:),   ALLOCATABLE :: gdscal, gdscal_ref
  REAL(P), DIMENSION (:,:,:),   ALLOCATABLE :: gdu_err,gdv_err,gdp_err, &
                                               gdt_err,gdq_err,gdw_err
  REAL(P), DIMENSION (:,:,:,:), ALLOCATABLE :: gdqscalar_err
  REAL(P), DIMENSION (:,:,:),   ALLOCATABLE :: u_ctr,v_ctr,p_ctr,t_ctr,q_ctr,w_ctr
  REAL(P), DIMENSION (:,:,:,:), ALLOCATABLE :: qscalar_ctr
  REAL(P), DIMENSION (:,:,:),   ALLOCATABLE :: psi,phi
  REAL(P), DIMENSION (:,:,:),   ALLOCATABLE :: rhostru, rhostrv,rhostrw,div3

!
! 2. work arrays depend on 3DVAR constants declared in 1.
!    ----------------------------------------------------
!
  REAL(P), DIMENSION (:),       ALLOCATABLE :: ctrv,grad,xgus
  REAL(P), DIMENSION (:),       ALLOCATABLE :: swork,ywork,diag,work

!
! 3. work arrays for surface observations
!    ----------------------------------------------------
!
  REAL(DP), DIMENSION(:), ALLOCATABLE :: xsng_p, ysng_p
  REAL(P),  DIMENSION(:), ALLOCATABLE :: zsng_1, zsng_2
  INTEGER,  DIMENSION(:), ALLOCATABLE :: ihgtsng

!
! 4. work arrays for multiple level soundings
!    ----------------------------------------------------
!
  REAL(DP), DIMENSION(:),   ALLOCATABLE:: xua_p, yua_p
  REAL(P),  DIMENSION(:,:), ALLOCATABLE:: zua_1, zua_2
  INTEGER,  DIMENSION(:,:), ALLOCATABLE:: ihgtua

!
! 5. work arrays for influence radius
!    ----------------------------------------------------
!
!
!-----------------------------------------------------------------------
!
!   Define the weather dependent horirontal radius
!   added by Anwei Lai
!
!----------------------------------------------------------------------
!
  REAL(P), ALLOCATABLE :: radius_z    (:,:,:), hradius_3d    (:,:,:)
  REAL(P), ALLOCATABLE :: radius_z_ref(:,:,:), hradius_3d_ref(:,:,:)

!    ----------------------------------------------------

  LOGICAL :: allocated_varwrk1    = .FALSE., allocated_varwrk2   = .FALSE.
  LOGICAL :: allocated_varwrk_sng = .FALSE., allocated_varwrk_ua = .FALSE.

  CONTAINS

  !#####################################################################

  SUBROUTINE allocate_3dvar_workarray1(nx,ny,nz,nscalarq,initval,istatus)
  !---------------------------------------------------------------------
  !
  ! PURPOSE: Allocate work array for 3DVAR that depends on model grid only
  !
  !---------------------------------------------------------------------

    INTEGER, INTENT(IN)  :: nx,ny,nz,nscalarq
    REAL(P), INTENT(IN)  :: initval
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (.NOT. allocated_varwrk1) THEN
      ALLOCATE ( gdu_err(nx,ny,nz), STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:gdu_err")

      ALLOCATE ( gdv_err(nx,ny,nz), STAT = istatus )
      CALL check_alloc_status(istatus, "3dvar:gdv_err")

      ALLOCATE ( gdp_err(nx,ny,nz), STAT = istatus )
      CALL check_alloc_status(istatus, "3dvar:gdp_err")

      ALLOCATE ( gdt_err(nx,ny,nz), STAT = istatus )
      CALL check_alloc_status(istatus, "3dvar:gdt_err")

      ALLOCATE ( gdq_err(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:gdq_err")

      ALLOCATE ( gdw_err(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:gdw_err")

      ALLOCATE ( gdqscalar_err(nx,ny,nz,nscalarq) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:gdqscalar_err")

      !-----------------------------------------------------------------------

      ALLOCATE ( gdscal(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:gdscal")

      ALLOCATE ( gdscal_ref(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:gdscal_ref")

      !-----------------------------------------------------------------------

      ALLOCATE (   u_ctr(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:u_ctr")

      ALLOCATE (   v_ctr(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:v_ctr")

      ALLOCATE (     psi(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:psi")

      ALLOCATE (     phi(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:phi")

      ALLOCATE (   p_ctr(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:p_ctr")

      ALLOCATE (   t_ctr(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:t_ctr")

      ALLOCATE (   q_ctr(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:q_ctr")

      ALLOCATE (   w_ctr(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:w_ctr")

      ALLOCATE (   qscalar_ctr(nx,ny,nz,nscalarq) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:qscalar_ctr")

      !-----------------------------------------------------------------------

      ALLOCATE ( rhostru(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:rhostru")

      ALLOCATE ( rhostrv(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:rhostrv")

      ALLOCATE ( rhostrw(nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:rhostrw")

      ALLOCATE ( div3   (nx,ny,nz) , STAT = istatus)
      CALL check_alloc_status(istatus, "3dvar:div3")

      allocated_varwrk1 = .TRUE.
    END IF

    gdu_err(:,:,:) = initval
    gdv_err(:,:,:) = initval
    gdp_err(:,:,:) = initval
    gdt_err(:,:,:) = initval
    gdq_err(:,:,:) = initval
    gdw_err(:,:,:) = initval
    gdqscalar_err(:,:,:,:) = initval
    gdscal(:,:,:)     = 1.0
    gdscal_ref(:,:,:) = 1.0
    u_ctr(:,:,:) = initval
    v_ctr(:,:,:) = initval
    psi(:,:,:)   = initval
    phi(:,:,:)   = initval
    p_ctr(:,:,:) = initval
    t_ctr(:,:,:) = initval
    q_ctr(:,:,:) = initval
    w_ctr(:,:,:) = initval
    qscalar_ctr(:,:,:,:) = initval
    rhostru(:,:,:) = initval
    rhostrv(:,:,:) = initval
    rhostrw(:,:,:) = initval
    div3 (:,:,:)   = initval


    RETURN
  END  SUBROUTINE allocate_3dvar_workarray1

  !#####################################################################

  SUBROUTINE allocate_3dvar_workarray2(nx,ny,nz,nscalarq,ref_use,hydro_opt,  &
                                       nensvirtl,ibeta1,ibeta2,initval,istatus)
  !---------------------------------------------------------------------
  !
  ! PURPOSE: Allocate work array for 3DVAR that depends on both the
  !          model grid and the 3dvar contstant decalared in this module.
  !
  !          The 3DVAR constants will also be determined here.
  !
  ! Note: it will depends on nensvirtl - the number of ensembles.
  !       So it should be called after the number of ensembles is adjusted
  !       in enbgread4var.
  !
  !---------------------------------------------------------------------

    INTEGER, INTENT(IN)  :: nx,ny,nz,nscalarq
    LOGICAL, INTENT(IN)  :: ref_use
    INTEGER, INTENT(IN)  :: hydro_opt
    INTEGER, INTENT(IN)  :: nensvirtl,ibeta1,ibeta2
    REAL(P), INTENT(IN)  :: initval
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: ierr
    INTEGER :: ctrsize

    INTEGER :: iscale, ivectr, jscale, jvectr, kscale, kvectr
    !INTEGER :: iscale_m, jscale_m, kscale_m

    INCLUDE 'mp.inc'

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF( ibeta1==1 .and. ibeta2==0 ) THEN
      IF (ref_use .OR. hydro_opt==1) THEN
        ctrsize = (6+nscalarq)*nx*ny*nz
      ELSE
        ctrsize = 6*nx*ny*nz
      END IF
    ELSE IF( ibeta1==0 .and. ibeta2==1 ) THEN
      ctrsize = nensvirtl*nx*ny*nz
    ELSE IF( ibeta1==1 .and. ibeta2==1 ) THEN
      IF (ref_use .OR. hydro_opt==1) THEN
        ctrsize = (6+nscalarq)*nx*ny*nz+ nensvirtl*nx*ny*nz
      ELSE
        ctrsize = 6*nx*ny*nz+ nensvirtl*nx*ny*nz
      END IF
    ELSE
      print*,' no! should not come here program will exit!!'
      stop
    END IF

    iscale = iend-ibgn+1
    ivectr = iendu-ibgn+1

    jscale = jend-jbgn+1
    jvectr = jendv-jbgn+1

    kscale = kend-kbgn+1
    kvectr = kendw-kbgn+1

    !iscale_m = iend_m-ibgn_m+1
    !jscale_m = jend_m-jbgn_m+1
    !kscale_m = kend_m-kbgn_m+1

    IF( ibeta1==1 .and. ibeta2==0 ) THEN
      IF (ref_use .OR. hydro_opt==1) THEN
        numctr = (3+nscalarq)*kscale*iscale *jscale     & ! 8 scalar arrays
             +   kscale*ivectr *jscale       & ! U
             +   kscale*iscale *jvectr       & ! V
             +   kvectr*iscale *jscale         ! W
      ELSE
        numctr = 3*kscale*iscale *jscale      & ! 3 scalar arrays
             +   kscale*ivectr *jscale        & ! U
             +   kscale*iscale *jvectr        & ! V
             +   kvectr*iscale *jscale          ! W
      END IF
    ELSE IF( ibeta1==0 .and. ibeta2==1 ) THEN
        numctr = kscale*iscale*jscale*nensvirtl
    ELSE IF( ibeta1==1 .and. ibeta2==1 ) THEN
      IF (ref_use .OR. hydro_opt==1) THEN
        numctr = (3+nscalarq)*kscale*iscale*jscale    & ! 8 scalar arrays
             +   kscale*ivectr *jscale     & ! U
             +   kscale*iscale *jvectr     & ! V
             +   kvectr*iscale *jscale     & ! W
             +   kscale*iscale *jscale*nensvirtl
      ELSE
        numctr = 3*kscale*iscale *jscale    & ! 3 scalar arrays
             +   kscale*ivectr *jscale      & ! U
             +   kscale*iscale *jvectr      & ! V
             +   kvectr*iscale *jscale      & ! W
             +   kscale*iscale *jscale*nensvirtl
      END IF
    ELSE
      PRINT *, ' should not come here program will exit!!'
      CALL arpsstop('ERROR: ibeta1/ibeta2',1)
    END IF

    mgra    = 2

    nxm     = ctrsize*mgra
    nwork   = ctrsize+2*mgra

    IF (.NOT. allocated_varwrk2) THEN

      ALLOCATE ( swork(nxm),    stat=ierr )
      ALLOCATE ( ywork(nxm),    stat=ierr )
      ALLOCATE ( diag(ctrsize), stat=ierr )
      ALLOCATE ( work(nwork),   stat=ierr )
      CALL check_alloc_status(ierr, "3dvar:work")

      ALLOCATE ( xgus(ctrsize), stat=ierr )
      CALL check_alloc_status(ierr, "3dvar:xgus")

      ALLOCATE ( ctrv(ctrsize), stat=ierr )
      CALL check_alloc_status(ierr, "3dvar:ctrv")

      ALLOCATE ( grad(ctrsize), stat=ierr)
      CALL check_alloc_status(ierr, "3dvar:grad")

      allocated_varwrk2 = .TRUE.
    END IF

    xgus(:) = initval
    ctrv(:) = initval
    grad(:) = initval

    ALLOCATE(hradius_3d(nx,ny,nz),     STAT = istatus)
    ALLOCATE(radius_z  (nx,ny,nz),     STAT = istatus)

    IF (ref_use .OR. hydro_opt ==1) THEN
      ALLOCATE(radius_z_ref  (nx,ny,nz), STAT = istatus)
      ALLOCATE(hradius_3d_ref(nx,ny,nz), STAT = istatus)
    END IF

    RETURN
  END  SUBROUTINE allocate_3dvar_workarray2

  !#####################################################################

  SUBROUTINE deallocate_3dvar_workarray1(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !-------------------------------------------------------------------

    IF (allocated_varwrk1) THEN
      DEALLOCATE (u_ctr ,      STAT = istatus )
      DEALLOCATE (v_ctr ,      STAT = istatus )
      DEALLOCATE (p_ctr ,      STAT = istatus )
      DEALLOCATE (t_ctr ,      STAT = istatus )
      DEALLOCATE (q_ctr ,      STAT = istatus )
      DEALLOCATE (w_ctr ,      STAT = istatus )
      DEALLOCATE (qscalar_ctr, STAT = istatus )

      DEALLOCATE ( xgus,       STAT = istatus )
      DEALLOCATE ( ctrv,       STAT = istatus )
      DEALLOCATE ( grad,       STAT = istatus )

      DEALLOCATE ( gdu_err ,      STAT = istatus )
      DEALLOCATE ( gdv_err ,      STAT = istatus )
      DEALLOCATE ( gdp_err ,      STAT = istatus )
      DEALLOCATE ( gdt_err ,      STAT = istatus )
      DEALLOCATE ( gdq_err ,      STAT = istatus )
      DEALLOCATE ( gdw_err ,      STAT = istatus )
      DEALLOCATE ( gdqscalar_err, STAT = istatus )
      DEALLOCATE ( gdscal  ,      STAT = istatus )
      DEALLOCATE ( gdscal_ref,    STAT = istatus )

      DEALLOCATE ( rhostru ,      STAT = istatus )
      DEALLOCATE ( rhostrv ,      STAT = istatus )
      DEALLOCATE ( rhostrw ,      STAT = istatus )
      DEALLOCATE ( div3    ,      STAT = istatus )

      allocated_varwrk1 = .FALSE.
    END IF

  END SUBROUTINE deallocate_3dvar_workarray1

  !#####################################################################

  SUBROUTINE deallocate_3dvar_workarray2(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !-------------------------------------------------------------------

    IF (allocated_varwrk2) THEN
      DEALLOCATE ( swork, STAT = istatus  )
      DEALLOCATE ( ywork, STAT = istatus  )
      DEALLOCATE (  diag, STAT = istatus  )
      DEALLOCATE (  work, STAT = istatus  )

      DEALLOCATE ( hradius_3d, radius_z )
      IF (ALLOCATED(radius_z_ref)) DEALLOCATE( radius_z_ref, hradius_3d_ref )

      allocated_varwrk2 = .FALSE.
    END IF

  END SUBROUTINE deallocate_3dvar_workarray2

  !#####################################################################

  SUBROUTINE allocate_3dvar_workarray_sng(mxsng,initval,istatus)
  !---------------------------------------------------------------------
  !
  ! PURPOSE: Allocate work array for sng observations
  !
  !---------------------------------------------------------------------

    INTEGER, INTENT(IN)  :: mxsng
    REAL(P), INTENT(IN)  :: initval
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (.NOT. allocated_varwrk_sng) THEN

      ALLOCATE ( xsng_p(mxsng), STAT=istatus )
      CALL check_alloc_status(istatus, "minimization:xsng_p")

      ALLOCATE ( ysng_p(mxsng), STAT=istatus )
      CALL check_alloc_status(istatus, "minimization:ysng_p")

      ALLOCATE ( zsng_1(mxsng), STAT=istatus )
      CALL check_alloc_status(istatus, "minimization:zsng_l")

      ALLOCATE ( zsng_2(mxsng), STAT=istatus )
      CALL check_alloc_status(istatus, "minimization:zsng_2")

      ALLOCATE ( ihgtsng(mxsng), STAT=istatus )
      CALL check_alloc_status(istatus, "minimization:ihgtsng")

      allocated_varwrk_sng = .TRUE.
    END IF

    xsng_p(:) = initval
    ysng_p(:) = initval
    zsng_1(:) = initval
    zsng_2(:) = initval
    ihgtsng(:) = 0

    RETURN
  END  SUBROUTINE allocate_3dvar_workarray_sng

  !#####################################################################

  SUBROUTINE allocate_3dvar_workarray_ua(mxua,nzua,initval,istatus)
  !---------------------------------------------------------------------
  !
  ! PURPOSE: Allocate work array for 3DVAR that depends on model grid only
  !
  !---------------------------------------------------------------------

    INTEGER, INTENT(IN)  :: mxua,nzua
    REAL(P), INTENT(IN)  :: initval
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (.NOT. allocated_varwrk_ua) THEN

      ALLOCATE ( xua_p(mxua), STAT=istatus )
      CALL check_alloc_status(istatus, "minimization:xua_p")

      ALLOCATE ( yua_p(mxua), STAT=istatus )
      CALL check_alloc_status(istatus, "minimization:yua_pua_p")

      ALLOCATE ( zua_1(nzua,mxua), STAT=istatus )
      CALL check_alloc_status(istatus, "minimization:zua_1")

      ALLOCATE ( zua_2(nzua,mxua), STAT=istatus )
      CALL check_alloc_status(istatus, "minimization:zua_2")

      ALLOCATE ( ihgtua(nzua,mxua), STAT=istatus )
      CALL check_alloc_status(istatus, "minimization:ihgtua")

      allocated_varwrk_ua = .TRUE.
    END IF

    xua_p(:)     = initval
    yua_p(:)     = initval
    zua_1(:,:)   = initval
    zua_2(:,:)   = initval
    ihgtua(:,:) = 0

    RETURN
  END  SUBROUTINE allocate_3dvar_workarray_ua

  !#####################################################################

  SUBROUTINE deallocate_3dvar_workarray_sng(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !-------------------------------------------------------------------

    IF (allocated_varwrk_sng) THEN
      DEALLOCATE (xsng_p ,      STAT = istatus )
      DEALLOCATE (ysng_p ,      STAT = istatus )
      DEALLOCATE (zsng_1 ,      STAT = istatus )
      DEALLOCATE (zsng_2 ,      STAT = istatus )
      DEALLOCATE (ihgtsng ,     STAT = istatus )

      allocated_varwrk_sng = .FALSE.
    END IF

    RETURN
  END SUBROUTINE deallocate_3dvar_workarray_sng

  !#####################################################################

  SUBROUTINE deallocate_3dvar_workarray_ua(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !-------------------------------------------------------------------

    IF (allocated_varwrk_ua) THEN
      DEALLOCATE (xua_p ,      STAT = istatus )
      DEALLOCATE (yua_p ,      STAT = istatus )
      DEALLOCATE (zua_1 ,      STAT = istatus )
      DEALLOCATE (zua_2 ,      STAT = istatus )
      DEALLOCATE (ihgtua ,     STAT = istatus )

      allocated_varwrk_ua = .FALSE.
    END IF

    RETURN
  END SUBROUTINE deallocate_3dvar_workarray_ua


  !#####################################################################

  SUBROUTINE deallocate_3dvar_workarray(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !-------------------------------------------------------------------

    CALL deallocate_3dvar_workarray1(istatus)
    CALL deallocate_3dvar_workarray2(istatus)

    CALL deallocate_3dvar_workarray_sng(istatus)
    CALL deallocate_3dvar_workarray_ua (istatus)

    RETURN
  END SUBROUTINE deallocate_3dvar_workarray

  !#####################################################################

  SUBROUTINE set_ipass_hradius(opt,nx,ny,nz,ref_mos_3d,                 &
                               inithradius,hradius3d,istatus)

   !--------------------------------------------------------------------
   !
   !   Define the weather dependent horirontal radius
   !   added by Anwei Lai
   !
   !--------------------------------------------------------------------

    INTEGER, INTENT(IN)  :: opt
    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL(P), INTENT(IN)  :: ref_mos_3d(nx,ny,nz)
    REAL(P), INTENT(IN)  :: inithradius
    REAL(P), INTENT(OUT) :: hradius3d(nx,ny,nz)
    INTEGER, INTENT(OUT) :: istatus

  !-------------------------------------------------------------------

    INTEGER :: i,j,k
    REAL(P) :: w_coef

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

!!!!!    DO j=1,ny
!!!!!      DO i =1, nx
!!!!!!        IF(maxval(ref_mos_3d(i,j,:))>20)THEN
!!!!!      !    w_coef = 1./(1. + 2.*(maxval(ref_mos_3d(i,j,:))/80.))
!!!!!!          w_coef = 1./(1. + 2.*(maxval(ref_mos_3d(i,j,:))/80.))
!!!!!!        ELSE
!!!!!          w_coef = 1.
!!!!!!        END IF
!!!!!        DO k = 1,nz
!!!!!!       IF(ref_mos_3d(i,j,k)>0)THEN
!!!!!!         w_coef = 1./(1. + 4.*(ref_mos_3d(i,j,k)/80.))
!!!!!!         w_coef = 1. + 1.*(ref_mos_3d(i,j,k)/80.)
!!!!!!       ELSE
!!!!!!        w_coef = 1.
!!!!!!       ENDIF
!!!!!          hradius_3d_ref(i,j,k) = w_coef*inithradius
!!!!!        END DO
!!!!!      END DO
!!!!!    END DO

    w_coef = 1.

    DO k = 1,nz
      DO j=1,ny
        DO i =1, nx
          hradius3d(i,j,k) = w_coef*inithradius
        END DO
      END DO
    END DO

    RETURN
  END SUBROUTINE set_ipass_hradius

  !#####################################################################

  SUBROUTINE set_ipass_vradius(vopt,nx,ny,nz,zp,initvradius,vradius3d,istatus)

  !-------------------------------------------------------------------
  !
  ! Set vertical influence radius
  !
  !-------------------------------------------------------------------

    INTEGER, INTENT(IN)  :: vopt
    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL(P), INTENT(IN)  :: zp(nx,ny,nz)
    REAL(P), INTENT(IN)  :: initvradius
    REAL(P), INTENT(OUT) :: vradius3d(nx,ny,nz)
    INTEGER, INTENT(OUT) :: istatus

  !-------------------------------------------------------------------

    INTEGER :: i,j,k

    INTEGER :: nL,nU,nV
    REAL(P) :: rL,rU,rrr

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF(vopt ==1) THEN  ! vradius is in grid points

      DO j =1, ny
        DO i =1, nx
          DO k =1, nz
            vradius3d(i,j,k)=initvradius
          END DO
        END DO
      END DO

    ELSE IF(vopt ==2) THEN  ! vradius in in unit of km

      DO j = 1, ny-1
        DO i = 1, nx-1
          rrr=initvradius*1000.0  !user-specified vertical radius in meters
          DO k = 2, nz-1
            nL=0; nU=0; rL =0.0; rU=0.0
            ! count upward
            DO nV = k, nz-1
              rU = zp(i,j,nV) - zp(i,j,k)
              IF ( rU >= rrr ) THEN
                nU = nV - k
                exit ! found nU, exit the do loop
              END IF
            END DO
            IF (nU == 0 .and. nV >= nz-1 ) nU = nz -1 - k
            ! count downward
            DO nV = k, 2, -1
              rL = zp(i,j,k) - zp(i,j,nV)
              IF ( rL >= rrr ) THEN
                nL = k - nV
                exit ! found nL, exit the do loop
              END IF
            END DO
            IF (nL == 0 .and. nV <= 2 ) nL = k - 2

            vradius3d(i,j,k) = ( nL * max(rrr, rL) + nU * max(rrr, rU) ) / ( rU + rL )
          END DO
          vradius3d(i,j,1)  = vradius3d(i,j,2)
          vradius3d(i,j,nz) = vradius3d(i,j,nz-1)
        END DO
      END DO

      DO k = 1, nz
        DO j = 1, ny-1
          vradius3d(nx,j,k) = vradius3d(nx-1,j,k)
        END DO
      END DO

      DO k = 1, nz
        DO i = 1, nx
          vradius3d(i,ny,k) = vradius3d(i,ny-1,k)
        END DO
      END DO

      !IF (mp_opt > 0) THEN
      !  CALL acct_interrupt(mp_acct)
      !  CALL mpsendrecv2dew(vradius3d, nx, ny, nz, ebc, wbc, 0, tem4)
      !  CALL mpsendrecv2dns(vradius3d, nx, ny, nz, nbc, sbc, 0, tem4)
      !  CALL acct_stop_inter
      !END IF

    END IF ! IF (vradius_opt(ipass) == 1 or 2)
    !END  : to specifiy vertical influence radius

    RETURN
  END SUBROUTINE set_ipass_vradius

END module Vmodule_3dvar
