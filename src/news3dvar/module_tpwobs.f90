!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%                                                      %%%%%%
!%%%%%%                MODULE TPWOBS                         %%%%%%
!%%%%%%                                                      %%%%%%
!%%%%%%                     Developed by                     %%%%%%
!%%%%%%       National Severe Storm Laboratory, NOAA         %%%%%%
!%%%%%%                                                      %%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
MODULE module_tpwobs

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! PURPOSE:
!
!   This module contains all Total Precipitable Water obseravation-related
!   arrays that are to be read in and initialize module_tpw.
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!-----------------------------------------------------------------------
!
!  AUTHOR: Sijie Pan & Yunheng Wang
!
!  06/28/2017. Written based on Sijie's work on TPW.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------

  IMPLICIT NONE

  !
  ! Constants
  !
  INTEGER, PARAMETER :: nsrc_tpw=5       ! number of sources of total precipitable water
  INTEGER, PARAMETER :: nvar_tpw=4       ! number of variables for tpw data
  INTEGER, PARAMETER :: mx_tpw_file=50   ! max number of total precipitable water files
  INTEGER, PARAMETER :: mx_tpw=200000    ! max number of total precipitable water data

  !
  ! Namelist variables
  !
  INTEGER :: ntpwfil
  INTEGER :: sattpwobs
  CHARACTER (LEN=256)  :: tpwfname(mx_tpw_file)
  CHARACTER (LEN=256)  :: tpwerrfil(nsrc_tpw)
  INTEGER, ALLOCATABLE :: iusetpw(:,:)   ! (0:nsrc_tpw,mx_pass)
  CHARACTER (LEN=8)    :: srctpw(nsrc_tpw)

  !
  ! Analysis arrays
  !
  REAL   , ALLOCATABLE :: qsrctpw(:,:)
  REAL   , ALLOCATABLE :: qcthrtpw(:,:)


  !
  ! working arrays
  !
  INTEGER              :: nxtpw,nytpw
  REAL(4), ALLOCATABLE, DIMENSION(:,:)  :: PRO
  REAL,    ALLOCATABLE, DIMENSION(:)    :: tpw_lat,tpw_lon,tpworig,   &
                                           pwlowlv,pwmidlv,pwhighlv

  REAL,    ALLOCATABLE, DIMENSION(:)    :: xloc,yloc
  REAL,    ALLOCATABLE, DIMENSION(:)    :: gridlat1,gridlon1,         &
                                           cldbase1,cldtop1,cwp1

  CONTAINS

  !#####################################################################

  SUBROUTINE tpwobs_read_nml(unum,myproc,maxpass,istatus)

    INTEGER, INTENT(IN)  :: unum
    INTEGER, INTENT(IN)  :: myproc, maxpass
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------
    NAMELIST /adas_tpw/ ntpwfil,tpwfname,sattpwobs,srctpw,tpwerrfil,iusetpw

  !---------------------------------------------------------------------

    INTEGER :: i,j
    INTEGER            :: jsrc, ivar
    CHARACTER (LEN=8 ) :: rdsource
    CHARACTER (LEN=80) :: srctpw_full(nsrc_tpw)
    REAL               :: qcmultpw(nsrc_tpw)
    CHARACTER (LEN=80) :: header

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ALLOCATE(iusetpw(0:nsrc_tpw,maxpass), STAT = istatus)
    CALL check_alloc_status(istatus, "tpwobs:iusetpw")

    ntpwfil=0
    sattpwobs=0
    DO i=1,mx_tpw_file
      tpwfname(i)='NULL'
    END DO
    DO i=1,nsrc_tpw
      DO j=1,maxpass
        iusetpw(i,j)=0
      END DO
      srctpw(i)='NULL'
      tpwerrfil(i)='NULL'
    END DO

    IF(myproc == 0) THEN
       READ(unum, adas_tpw, END=326)
       WRITE(6,*) 'Namelist block adas_tpw sucessfully read.'

       GOTO 327
       326 CONTINUE
       WRITE(*,*) 'ERROR: reading namelist <adas_tpw>.'
       istatus = -1
       RETURN
       327 CONTINUE

    END IF

    CALL mpupdatei(ntpwfil,1)
    CALL mpupdatec(tpwfname,256*mx_tpw_file)
    CALL mpupdatei(sattpwobs,1)
    CALL mpupdatec(srctpw,8*nsrc_tpw)
    CALL mpupdatec(tpwerrfil,256*nsrc_tpw)
    !CALL mpupdatei(iusetpw,(nsrc_tpw+1)*maxpass)

    IF (ntpwfil == 0) sattpwobs = 0

    ALLOCATE( qcthrtpw(nvar_tpw,nsrc_tpw) )
    ALLOCATE( qsrctpw(nvar_tpw,nsrc_tpw) )
    qsrctpw  = 0.0

    IF (myproc == 0) THEN
      DO jsrc=1,nsrc_tpw
         IF(srctpw(jsrc) /= 'NULL') THEN
            WRITE(6,'(/,a,a)') ' Reading ', TRIM(tpwerrfil(jsrc))
            OPEN(4,FILE=trim(tpwerrfil(jsrc)),STATUS='old')
            READ(4,'(a8)') rdsource
            IF(rdsource /= srctpw(jsrc)) THEN
               WRITE(6,'(a,i4,a,a,a,a,a)')                              &
               ' Mismatch of source names',jsrc,                        &
               ' read ',rdsource,' expected ',srctpw(jsrc)
               CALL arpsstop("mismatch",1)
            END IF
            READ(4,'(a80)') srctpw_full(jsrc)
            READ(4,*) qcmultpw(jsrc)
            READ(4,'(a80)') header
            READ(4,*) (qsrctpw(ivar,jsrc),ivar=1,nvar_tpw)
            WRITE(6,'(//,a,a,/a)') 'Satellite std error for ',          &
                             srctpw(jsrc),srctpw_full(jsrc)
            WRITE(6,'(a,f5.1)') ' QC multiplier: ',qcmultpw(jsrc)
            WRITE(6,'(1x,a)')                                           &
                '   tpw (cm)'
            WRITE(6,'(1x,1f8.2)') (qsrctpw(ivar,jsrc),ivar=1,nvar_tpw)
            CLOSE(4)
         ELSE
            DO j=1,maxpass
              iusetpw(jsrc,j) = 0
            END DO
         END IF
      END DO

      DO jsrc=1,nsrc_tpw
        DO ivar=1,nvar_tpw
          qsrctpw(ivar,jsrc) = qsrctpw(ivar,jsrc)*qsrctpw(ivar,jsrc)
          qcthrtpw(ivar,jsrc)= qcmultpw(jsrc)*                          &
                               SQRT(qsrctpw(ivar,jsrc))
        END DO
      END DO
    END IF

    CALL mpupdater(qsrctpw,  (nvar_tpw*nsrc_tpw)  )
    CALL mpupdater(qcthrtpw, (nvar_tpw*nsrc_tpw)  )
    CALL mpupdatei(iusetpw,  (nsrc_tpw+1)*maxpass )

    RETURN
  END SUBROUTINE tpwobs_read_nml

  !#####################################################################

  SUBROUTINE tpwobs_set_usage(myproc,ipass,tpwsw,istatus)

    INTEGER, INTENT(IN)  :: myproc,ipass
    INTEGER, INTENT(OUT) :: tpwsw

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: isrc, icnt
    CHARACTER(LEN=80) :: tpwsrc(nsrc_tpw)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    tpwsw = 0
    IF (ipass > 0) THEN
      icnt=0
      IF( ntpwfil /= 0 .AND. sattpwobs /= 0 ) THEN
        DO isrc=1,nsrc_tpw
          IF(iusetpw(isrc,ipass) > 0) THEN
            tpwsw=1
            icnt = icnt + 1
            tpwsrc(icnt) = srctpw(isrc)
          END IF
        END DO
      END IF
    ELSE  ! chk_opt == 1, ipass == 0
      IF( ntpwfil /= 0 ) tpwsw = 1
      tpwsrc = srctpw
    END IF

    CALL mpmaxi(tpwsw)

    IF (myproc == 0) THEN
      IF (tpwsw == 0) THEN
        WRITE(6,'(3x,a,11x,a)') 'Total precipitable water data',' none'
      ELSE
        WRITE(6,'(3x,a,11x,a)') 'Total precipitable water data',' Using'
        DO isrc= 1,icnt
          WRITE(6,'(9x,I0,2a)') isrc,' - ',TRIM(tpwsrc(isrc))
        END DO
      END IF
      WRITE(6,*) ' '
    END IF

    RETURN
  END SUBROUTINE tpwobs_set_usage

  !#####################################################################

  SUBROUTINE pwobs_alloc_valid_array(valid_num,istatus)

    INTEGER, INTENT(IN)  :: valid_num
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    ALLOCATE( xloc(valid_num),stat=istatus )
    CALL check_alloc_status(istatus, "xloc")

    ALLOCATE( yloc(valid_num),stat=istatus )
    CALL check_alloc_status(istatus, "yloc")

    ALLOCATE( tpworig(valid_num),stat=istatus )
    CALL check_alloc_status(istatus, "tpworig")

    ALLOCATE( pwlowlv(valid_num),stat=istatus )
    CALL check_alloc_status(istatus, "pwlowlv")

    ALLOCATE( pwmidlv(valid_num),stat=istatus )
    CALL check_alloc_status(istatus, "pwmidlv")

    ALLOCATE( pwhighlv(valid_num),stat=istatus )
    CALL check_alloc_status(istatus, "pwhighlv")

    RETURN
  END SUBROUTINE pwobs_alloc_valid_array

  !#####################################################################

  SUBROUTINE pwobs_dealloc_valid_array(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    DEALLOCATE(xloc,yloc,tpworig,pwlowlv,pwmidlv,pwhighlv)

    RETURN
  END SUBROUTINE pwobs_dealloc_valid_array

END MODULE module_tpwobs
