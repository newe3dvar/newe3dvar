!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TRANS                      ######
!######                                                      ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.                           ######
!######                                                      ######
!##################################################################
!##################################################################
!
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Convert (u,v,pres,pt,qv,w...) to x
!
!-----------------------------------------------------------------------
!
!  AUTHOR: JIDONG GAO
!  01/17/00
!
!
!-----------------------------------------------------------------------
!
!

SUBROUTINE trans(numctr,nx,ny,nz,nscalarq,u,v,pres,pt,qv,w,qscalar,x,ipass)

!-----------------------------------------------------------------------
!
!  INPUT:
!
!    numctr     The number of components of the control variables.
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x-component of velocity at all time levels (m/s).
!    v        y-component of velocity at all time levels (m/s).
!    pres     Pressure at all time levels (Pascal)
!    pt       Potential temperature at all time levels (K)
!    qv       Water vapor specific humidity at all time levels (kg/kg)
!    w        z-component of velocity at all time levels (m/s).
!
!    Output:
!    x        The control variable.
!
!    Temporary Variables:
!    itemp    Temporary variable indicating the position of the index.
!    i,j,k
!
!-----------------------------------------------------------------------
!
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  USE module_varpara
  USE module_ensemble

  IMPLICIT NONE

  INTEGER :: numctr         ! The no. of components of the control var.

  INTEGER :: nx,ny,nz,nscalarq          ! The number of grid points in 3 directions
  INTEGER :: ipass

  REAL :: u     (nx,ny,nz)  ! Total u-velocity (m/s).
  REAL :: v     (nx,ny,nz)  ! Total v-velocity (m/s).
  REAL :: pres  (nx,ny,nz)  ! Pressure from that
  REAL :: pt    (nx,ny,nz)  ! Potential temperature
  REAL :: qv    (nx,ny,nz)  ! Water vapor specific humidity (kg/kg).
  REAL :: w     (nx,ny,nz)  ! Total w-velocity (m/s).
  REAL :: x     (numctr)       ! Control variable.
  REAL :: qscalar(nx,ny,nz,nscalarq)

  INCLUDE 'mp.inc'

  INTEGER :: itemp, i,j,k,l,nq ! Temporary variables.

  !INTEGER :: ibgn, iend, jbgn, jend, kbgn,kend
  !INTEGER :: iuend,jvend
!
!    only initial conditions are controled.
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  !ibgn = 2
  !iend = nx-2
  !jbgn = 2
  !jend = ny-2
  !
  !iuend = nx-1
  !jvend = ny-1
  !
  !IF (loc_x > 1) ibgn = 2
  !IF (loc_y > 1) jbgn = 2
  !
  !IF (loc_x < nproc_x) THEN
  !  iend  = nx-2
  !  iuend = nx-2
  !END IF
  !
  !IF (loc_y < nproc_y) THEN
  !  jend  = ny-2
  !  jvend = ny-2
  !END IF
  !
  !kbgn = 2
  !kend = nz-2

!-----------------------------------------------------------------------
!
!  We first place the initial conditions for (u,v,w,pt,pres,qv)
!  in x in the order as they appear.
!
!-----------------------------------------------------------------------
!
  itemp = 0

  IF( ibeta1==1 ) THEN

    DO k= kbgn,kend
      DO j=jbgn,jend
        DO i=ibgn,iendu
          itemp=itemp+1
          x(itemp) = u(i,j,k)
        END DO
      END DO
    END DO

    DO k=kbgn,kend
      DO j=jbgn,jendv
        DO i=ibgn,iend
          itemp=itemp+1
          x(itemp) = v(i,j,k)
        END DO
      END DO
    END DO

    DO k=kbgn,kend
      DO j=jbgn,jend
        DO i=ibgn,iend
          itemp=itemp+1
          x(itemp)=pres (i,j,k)
        END DO
      END DO
    END DO

    DO k=kbgn,kend
      DO j=jbgn,jend
        DO i=ibgn,iend
          itemp=itemp+1
          x(itemp)=pt(i,j,k)
        END DO
      END DO
    END DO

    DO k=kbgn,kend
      DO j=jbgn,jend
        DO i=ibgn,iend
          itemp=itemp+1
          x(itemp)=qv   (i,j,k)
        END DO
      END DO
    END DO

    DO k=kbgn,kendw
      DO j=jbgn,jend
        DO i=ibgn,iend
          itemp=itemp+1
          x(itemp)=w    (i,j,k)
        END DO
      END DO
    END DO

    IF (ref_opt(ipass) ==1 .OR. hydro_opt==1 ) THEN
      DO nq = 1, nscalarq
        DO k=kbgn,kend
          DO j=jbgn,jend
            DO i=ibgn,iend
              itemp=itemp+1
              x(itemp)=qscalar(i,j,k,nq)
            END DO
          END DO
        END DO
      END DO

    END IF !IF (ref_opt ==1 .or. hydro_opt==1)

  END IF

  IF( ibeta2==1 ) THEN
    !print*,'dims check before ', itemp, nensvirtl
    DO l=1,nensvirtl
      DO k=kbgn,kend
        DO j=jbgn,jend
          DO i=ibgn,iend
            itemp=itemp+1
            x(itemp)= gr_en(i,j,k,l)
          END DO
        END DO
      END DO
    END DO
    !   print*,'dims check after ', itemp
  END IF

!
!-----------------------------------------------------------------------
!
!  End of processing initial conditions.
!
!-----------------------------------------------------------------------
!
!
  RETURN
END SUBROUTINE trans
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADTRANS                    ######
!######                                                      ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adtrans(numctr,nx,ny,nz,nscalarq,u,v,pres,pt,qv,w,        &
                   qscalar,x,tem1,ipass)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Convert x to (u,v,w,pres,pt,qv...)
!
!-----------------------------------------------------------------------
!
!  AUTHOR: JIDONG GAO
!  01/17/00
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    numctr     The number of components of the control variables.
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    u        x-component of velocity at all time levels (m/s).
!    v        y-component of velocity at all time levels (m/s).
!    pres     Pressure at all time levels (Pascal)
!    pt       Potential temperature at all time levels (K)
!    qv       Water vapor specific humidity at all time levels (kg/kg)
!    w        z-component of velocity at all time levels (m/s).
!
!    Output:
!    x        The control variable.
!
!    Temporary Variables:
!    itemp    Temporary variable indicating the position of the index.
!    i,j,k
!
!-----------------------------------------------------------------------
!
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  USE module_varpara
  USE module_ensemble

  IMPLICIT NONE

  INTEGER :: numctr         ! The no. of components of the control var.

  INTEGER :: nx,ny,nz,nscalarq          ! The number of grid points in 3 directions
  INTEGER :: ipass

  REAL :: u     (nx,ny,nz)  ! Total u-velocity (m/s).
  REAL :: v     (nx,ny,nz)  ! Total v-velocity (m/s).
  REAL :: pres  (nx,ny,nz)  ! Pressure from that
  REAL :: pt    (nx,ny,nz)  ! Potential temperature
  REAL :: qv    (nx,ny,nz)  ! Water vapor specific humidity (kg/kg).
  REAL :: w     (nx,ny,nz)  ! Total w-velocity (m/s).
  REAL :: x     (numctr)       ! Control variable.
  REAL :: qscalar(nx,ny,nz,nscalarq)

  REAL :: tem1  (nx,ny,nz)  ! working array

  INCLUDE 'mp.inc'
  INCLUDE 'bndry.inc'

  !INTEGER :: ibgn, iend, jbgn, jend, kbgn,kend
  !INTEGER :: iuend,jvend

  INTEGER :: itemp, i,j,k,l,nq ! Temporary variables.

!
!    only initial conditions are controled.
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  !ibgn = 2;  iend = nx-2
  !jbgn = 2;  jend = ny-2
  !
  !iuend = nx-1
  !jvend = ny-1
  !
  !IF (loc_x > 1)       ibgn = 2
  !IF (loc_y > 1)       jbgn = 2
  !
  !IF (loc_x < nproc_x) THEN
  !  iend  = nx-2
  !  iuend = nx-2
  !END IF
  !
  !IF (loc_y < nproc_y) THEN
  !  jend  = ny-2
  !  jvend = ny-2
  !END IF
  !
  !kbgn = 2; kend = nz-2

!-----------------------------------------------------------------------
!
!  We first place the initial conditions for (u,v,w,pres,pt,qv)
!  in x in the order as they appear.
!
!-----------------------------------------------------------------------
!
  itemp = 0

  IF ( ibeta1==1 ) THEN

    DO k=kbgn,kend
      DO j=jbgn,jend
        DO i=ibgn,iendu
          itemp=itemp+1
          u    (i,j,k) = x(itemp)
        END DO
      END DO
    END DO

    DO k=kbgn,kend
      DO j=jbgn,jendv
        DO i=ibgn,iend
          itemp=itemp+1
          v    (i,j,k) = x(itemp)
        END DO
      END DO
    END DO

    DO k=kbgn,kend
      DO j=jbgn,jend
        DO i=ibgn,iend
          itemp=itemp+1
          pres (i,j,k) = x(itemp)
        END DO
      END DO
    END DO

    DO k=kbgn,kend
      DO j=jbgn,jend
        DO i=ibgn,iend
          itemp=itemp+1
          pt(i,j,k) = x(itemp)
        END DO
      END DO
    END DO

    DO k=kbgn,kend
      DO j=jbgn,jend
        DO i=ibgn,iend
          itemp=itemp+1
          qv   (i,j,k) = x(itemp)
        END DO
      END DO
    END DO

    DO k=kbgn,kendw
      DO j=jbgn,jend
        DO i=ibgn,iend
          itemp=itemp+1
          w    (i,j,k) = x(itemp)
        END DO
      END DO
    END DO

    IF (ref_opt(ipass) ==1 .OR. hydro_opt==1) THEN

      DO nq = 1, nscalarq
        DO k=kbgn,kend
          DO j=jbgn,jend
            DO i=ibgn,iend
              itemp=itemp+1
              qscalar(i,j,k,nq) = x(itemp)
            END DO
          END DO
        END DO
      END DO

    END IF !IF (ref_opt ==1 .or. hydro_opt==1 )

  END IF  ! ibeta1

  IF( ibeta2==1 ) THEN
    !   print*,'dims check before ', itemp
    DO l=1,nensvirtl
      DO k=kbgn,kend
        DO j=jbgn,jend
          DO i=ibgn,iend
            itemp=itemp+1
            en_ctr(i,j,k,l) = x(itemp)
          END DO
        END DO
      END DO
    END DO
    !   print*,'dims check after ', itemp
  END IF
!
!-----------------------------------------------------------------------
!
!  End of processing initial conditions.
!
!-----------------------------------------------------------------------
!
  IF (mp_opt > 0) THEN
    !CALL acct_interrupt(mp_acct)
    CALL mpsendrecv2dew(u, nx, ny, nz, ebc, wbc, 0, tem1)  ! 2- nx-2 is good , Fill nx-1, 1
    CALL mpsendrecv2dew(u, nx, ny, nz, ebc, wbc, 1, tem1)  ! 1- nx-1 is good, fill nx, 1
    CALL mpsendrecv2dns(u, nx, ny, nz, nbc, sbc, 1, tem1)

    CALL mpsendrecv2dns(v, nx, ny, nz, nbc, sbc, 0, tem1)  ! 2 - ny-2 is good, fill ny-1, 1
    CALL mpsendrecv2dew(v, nx, ny, nz, ebc, wbc, 2, tem1)
    CALL mpsendrecv2dns(v, nx, ny, nz, nbc, sbc, 2, tem1)  ! 1 - ny-1 is good, fill ny, 1

    CALL mpsendrecv2dew(w, nx, ny, nz, ebc, wbc, 3, tem1)
    CALL mpsendrecv2dns(w, nx, ny, nz, nbc, sbc, 3, tem1)

    CALL mpsendrecv2dew(pres, nx, ny, nz, ebc, wbc, 0, tem1)
    CALL mpsendrecv2dns(pres, nx, ny, nz, nbc, sbc, 0, tem1)

    CALL mpsendrecv2dew(pt, nx, ny, nz, ebc, wbc, 0, tem1)
    CALL mpsendrecv2dns(pt, nx, ny, nz, nbc, sbc, 0, tem1)

    CALL mpsendrecv2dew(qv, nx, ny, nz, ebc, wbc, 0, tem1)
    CALL mpsendrecv2dns(qv, nx, ny, nz, nbc, sbc, 0, tem1)

    !CALL mpsendrecv2dew(u, nx, ny, nz, ebc, wbc, 1, tem1)  ! make sure nx contains good value
    !CALL mpsendrecv2dns(v, nx, ny, nz, nbc, sbc, 2, tem1)  ! make sure ny contains good value

    IF (ref_opt(ipass) ==1 .OR. hydro_opt==1) THEN

      DO nq = 1, nscalarq
        CALL mpsendrecv2dew(qscalar(:,:,:,nq), nx, ny, nz, ebc, wbc, 0, tem1)
        CALL mpsendrecv2dns(qscalar(:,:,:,nq), nx, ny, nz, nbc, sbc, 0, tem1)
      END DO

    END IF !IF (ref_opt ==1 .or. hydro_opt==1)
    !CALL acct_stop_inter
  END IF

  RETURN
END SUBROUTINE adtrans
