MODULE module_cwpobs

  IMPLICIT NONE

    !
    ! Constants
    !
    INTEGER, PARAMETER :: nsrc_cwp=5         ! number of sources of cloud water path data
    INTEGER, PARAMETER :: nvar_cwp=1         ! number of variables for cwp data

    INTEGER, PARAMETER :: mx_cwp_file=50     ! max number of cloud water path files
    INTEGER, PARAMETER :: mx_cwp=200000      ! max number of cloud water path data

    !
    ! namliest variables
    !
    INTEGER :: ncwpfil
    INTEGER :: satcwpobs,spctopt,pseudo_opt

    CHARACTER (LEN=256) :: cwpfname(mx_cwp_file)
    INTEGER             :: cwpffmt
    CHARACTER (LEN=256) :: cwperrfil(nsrc_cwp)
    INTEGER, ALLOCATABLE :: iusecwp(:,:)   ! 0:nsrc_cwp,mx_pass)
    CHARACTER (LEN=8) :: srccwp(nsrc_cwp)

    !
    ! Analysis Arrays
    !
    REAL   , ALLOCATABLE :: qsrccwp(:,:)
    REAL   , ALLOCATABLE :: qsrcpsd(:)

    REAL   , ALLOCATABLE :: qcthrcwp(:,:)

    !
    ! Working variables
    !
    !INTEGER       :: totcwp
    INTEGER       :: valid_num

    REAL,    ALLOCATABLE, DIMENSION(:)   :: xloc,yloc
    REAL,    ALLOCATABLE, DIMENSION(:)   :: gridlat1,gridlon1,          &
                                            cldbase1,cldtop1,cwp1,cwperr1
    INTEGER, ALLOCATABLE, DIMENSION(:,:) :: cldphase1(:)

    PRIVATE :: read_cwpfile, get_dimensions, get_2dr, get_2di, get_1di, get_1dd

  CONTAINS

  !#####################################################################

  SUBROUTINE cwpobs_read_nml(unum,myproc,maxpass,istatus)

    IMPLICIT NONE

    INCLUDE "globcst.inc"

    INTEGER, INTENT(IN)  :: unum
    INTEGER, INTENT(IN)  :: myproc, maxpass

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    NAMELIST /adas_cwp/ ncwpfil,cwpfname,cwpffmt,satcwpobs,spctopt,pseudo_opt,  &
                        srccwp,cwperrfil,iusecwp

  !---------------------------------------------------------------------

    INTEGER :: i, j
    INTEGER            :: ivar, jsrc
    CHARACTER (LEN=8 ) :: rdsource
    CHARACTER (LEN=80) :: srccwp_full(nsrc_cwp)
    REAL               :: qcmulcwp(nsrc_cwp)
    CHARACTER (LEN=80) :: header


  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ALLOCATE(iusecwp(0:nsrc_cwp,maxpass), STAT = istatus)
    CALL check_alloc_status(istatus, "cwpobs:iusecwp")

    ncwpfil=0
    satcwpobs=0
    spctopt=0
    pseudo_opt=1
    DO i=1,mx_cwp_file
      cwpfname(i)='NULL'
    END DO
    DO i=1,nsrc_cwp
      DO j=1,maxpass
        iusecwp(i,j)=0
      END DO
      srccwp(i)='NULL'
      cwperrfil(i)='NULL'
    END DO

    cwpffmt = 1

    IF(myproc == 0) THEN
       READ(unum, adas_cwp, END=325)
       WRITE(6,*) 'Namelist block adas_cwp sucessfully read.'
       GOTO 326
       325 CONTINUE
       WRITE(*,*) 'ERROR: reading namelist <adas_cwp>.'
       istatus = -1
       RETURN
       326 CONTINUE
    END IF

    CALL mpupdatei(ncwpfil,1)
    CALL mpupdatec(cwpfname,256*mx_cwp_file)
    CALL mpupdatec(cwpffmt,1)
    CALL mpupdatei(satcwpobs,1)
    CALL mpupdatei(spctopt,1)
    CALL mpupdatei(pseudo_opt,1)
    CALL mpupdatec(srccwp,8*nsrc_cwp)
    CALL mpupdatec(cwperrfil,256*nsrc_cwp)
    !CALL mpupdatei(iusecwp,(nsrc_cwp+1)*maxpass)

    IF (ncwpfil == 0) satcwpobs = 0

    ALLOCATE( qcthrcwp(nvar_cwp,nsrc_cwp), STAT = istatus )

    ALLOCATE( qsrccwp (nvar_cwp,nsrc_cwp), STAT = istatus )
    qsrccwp  = 0.0
    ALLOCATE( qsrcpsd (nscalarq),          STAT = istatus )

    IF (myproc == 0) THEN
      DO jsrc=1,nsrc_cwp
         IF(srccwp(jsrc) /= 'NULL') THEN
            WRITE(6,'(/,a,a)') ' Reading ', TRIM(cwperrfil(jsrc))
            OPEN(4,FILE=trim(cwperrfil(jsrc)),STATUS='old')
            READ(4,'(a8)') rdsource
            IF(rdsource /= srccwp(jsrc)) THEN
               WRITE(6,'(a,i4,a,a,a,a,a)')                              &
               ' Mismatch of source names',jsrc,                        &
               ' read ',rdsource,' expected ',srccwp(jsrc)
               CALL arpsstop("mismatch",1)
            END IF
            READ(4,'(a80)') srccwp_full(jsrc)
            READ(4,*) qcmulcwp(jsrc)
            READ(4,'(a80)') header
            READ(4,*) qsrccwp(1,jsrc)
            WRITE(6,'(//,a,a,/a)') 'Satellite std error for ',          &
                             srccwp(jsrc),srccwp_full(jsrc)
            WRITE(6,'(a,f5.1)') ' QC multiplier: ',qcmulcwp(jsrc)
            WRITE(6,'(1x,a)')   '   cwp (kg/m^2)'
            WRITE(6,'(1x,1f8.2)') (qsrccwp(ivar,jsrc),ivar=1,nvar_cwp)
            CLOSE(4)
         ELSE
            DO j=1,maxpass
              iusecwp(jsrc,j) = 0
            END DO
         END IF
      END DO

      DO jsrc=1,nsrc_cwp
        DO ivar=1,nvar_cwp
          qsrccwp(ivar,jsrc) = qsrccwp(ivar,jsrc)*qsrccwp(ivar,jsrc)
          qcthrcwp(ivar,jsrc)= qcmulcwp(jsrc)*SQRT(qsrccwp(ivar,jsrc))
        END DO
      END DO
    END IF

    CALL mpupdater(qsrccwp,  (nvar_cwp*nsrc_cwp)  )
    CALL mpupdater(qcthrcwp, (nvar_cwp*nsrc_cwp)  )
    CALL mpupdatei(iusecwp,  (nsrc_cwp+1)*maxpass )
    CALL mpupdater(qsrcpsd, nscalarq)

    qsrcpsd=2.0E-3
    DO ivar=1,nscalarq
      IF (ivar==P_QS) qsrcpsd=qsrcpsd/2
      qsrcpsd(ivar)= qsrcpsd(ivar)**2
    END DO

    RETURN
  END SUBROUTINE cwpobs_read_nml

  !#####################################################################

  SUBROUTINE cwpobs_set_usage(myproc,ipass,cwpsw,istatus)

    INTEGER, INTENT(IN)  :: myproc,ipass
    INTEGER, INTENT(OUT) :: cwpsw

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: isrc, icnt
    CHARACTER(LEN=80) :: cwpsrc(nsrc_cwp)


  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    cwpsw = 0

    IF (ipass > 0) THEN
      icnt=0
      IF( ncwpfil /= 0 .AND. satcwpobs /= 0 ) THEN
        DO isrc=1,nsrc_cwp
          IF(iusecwp(isrc,ipass) > 0) THEN
            cwpsw=1
            icnt = icnt+1
            cwpsrc(icnt) = srccwp(isrc)
          END IF
        END DO
      END IF
    ELSE             ! chk_opt == 1, ipass == 0
      icnt = 1
      IF( ncwpfil /= 0 ) THEN
        cwpsw = 1
        cwpsrc = srccwp
      END IF
    END IF

    CALL mpmaxi(cwpsw)

    IF (myproc == 0) THEN
      IF (cwpsw == 0) THEN
        WRITE(6,'(3x,a,19x,a)') 'Cloud water path data',' none'
      ELSE
        WRITE(6,'(3x,a,19x,a)') 'Cloud water path data',' Using'
        DO isrc= 1,icnt
          WRITE(6,'(9x,I0,2a)') isrc,' - ',TRIM(cwpsrc(isrc))
        END DO
      END IF
      WRITE(6,*) ' '
    END IF

    RETURN
  END SUBROUTINE cwpobs_set_usage

  !#####################################################################

  SUBROUTINE cwpobs_alloc_array(istatus)

    !INTEGER, INTENT(IN)  :: valid_num
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ALLOCATE (xloc(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "xloc")

    ALLOCATE (yloc(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "yloc")

    ALLOCATE (gridlat1(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "gridlat1")

    ALLOCATE (gridlon1(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "gridlon1")

    ALLOCATE (cldbase1(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cldbase1")

    ALLOCATE (cldtop1(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cldtop1")

    ALLOCATE (cwp1(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cwp1")

    ALLOCATE (cwperr1(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cwperr1")

    ALLOCATE (cldphase1(valid_num),stat=istatus)
    CALL check_alloc_status(istatus, "cldphase1")

    RETURN
  END SUBROUTINE cwpobs_alloc_array

  !#####################################################################

  SUBROUTINE cwpobs_dealloc_array(istatus)

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    DEALLOCATE(xloc,yloc,gridlat1,gridlon1,cldbase1,cldtop1,cwp1,cwperr1,cldphase1)

    RETURN
  END SUBROUTINE cwpobs_dealloc_array

  !#####################################################################

   SUBROUTINE cwpobs_read(idealopt,istatus)

    INTEGER, INTENT(IN)  :: idealopt
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: nx1, ny1
    INTEGER :: i, j, ii

    REAL,    ALLOCATABLE, DIMENSION(:,:)  :: cwp_grid_lat,cwp_grid_lon, &
                                             cld_base,cld_top,lwp_iwp,cwperrraw
    INTEGER, ALLOCATABLE, DIMENSION(:,:)  :: cld_phase

    INCLUDE 'mp.inc'

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (idealopt == 0 ) THEN

      IF (myproc==0) THEN
        CALL get_dimensions (cwpfname(1),cwpffmt, nx1,ny1,istatus)
        IF (istatus /= 0) GOTO 700

        ALLOCATE( cwp_grid_lat(nx1,ny1),stat=istatus)
        CALL check_alloc_status(istatus, "cwp_grid_lat")

        ALLOCATE( cwp_grid_lon(nx1,ny1),stat=istatus)
        CALL check_alloc_status(istatus, "cwp_grid_lon")

        ALLOCATE( cld_base(nx1,ny1),stat=istatus)
        CALL check_alloc_status(istatus, "cld_base")

        ALLOCATE( cld_top(nx1,ny1),stat=istatus)
        CALL check_alloc_status(istatus, "cld_top")

        ALLOCATE( lwp_iwp(nx1,ny1),stat=istatus)
        CALL check_alloc_status(istatus, "lwp_iwp")

        ALLOCATE( cwperrraw(nx1,ny1),stat=istatus)
        CALL check_alloc_status(istatus, "cwperrraw")

        ALLOCATE( cld_phase(nx1,ny1),stat=istatus)
        CALL check_alloc_status(istatus, "cld_phase")

        CALL read_cwpfile(cwpfname(1),cwpffmt,nx1,ny1,                  &
                          cwp_grid_lat,cwp_grid_lon,                    &
                          cld_base,cld_top,lwp_iwp,cwperrraw,cld_phase)

        valid_num=0
        DO j=1,ny1
          DO i=1,nx1
            IF ( (lwp_iwp(i,j)>=0   .AND. lwp_iwp(i,j) < 10. .AND.      &
                  cld_phase(i,j)>0 ) .OR. cld_phase(i,j)==4 ) THEN
              valid_num=valid_num+1
            END IF
          END DO
        END DO

        700 CONTINUE
      END IF  ! myproc == 0
      CALL mpupdatei(istatus,1)
      IF (istatus /= 0) RETURN

      IF (mp_opt>0) CALL mpupdatei(valid_num,1)
      CALL cwpobs_alloc_array(istatus)

      IF (myproc==0) THEN
        ii=0
        DO i=1,nx1
          DO j=1,ny1
            IF ( (lwp_iwp(i,j)>=0   .AND. lwp_iwp(i,j) < 10. .AND.      &
                  cld_phase(i,j)>0 ) .OR. cld_phase(i,j)==4 ) THEN
              ii=ii+1
              gridlat1(ii) = cwp_grid_lat(i,j)
              gridlon1(ii) = cwp_grid_lon(i,j)
              cldbase1(ii) = cld_base(i,j)
              cldtop1(ii)  = cld_top(i,j)
              cwp1(ii)     = lwp_iwp(i,j)
              cwperr1(ii)  = cwperrraw(i,j) ** 2
              cldphase1(ii)= cld_phase(i,j)
            END IF
          END DO
        END DO

        DEALLOCATE(cwp_grid_lat)
        DEALLOCATE(cwp_grid_lon)
        DEALLOCATE(cld_base)
        DEALLOCATE(cld_top)
        DEALLOCATE(lwp_iwp)
        DEALLOCATE(cld_phase)

        CALL lltoxy(valid_num,1,gridlat1,gridlon1,xloc,yloc)

      END IF

    ELSE  ! idealopt == 1

      IF (myproc==0) THEN
        OPEN(25,File=cwpfname(1),Form='Unformatted')
        READ(25) nx1,ny1,valid_num
        print*,'ideal data dimension===============',nx1,ny1,valid_num
        IF (nx1*ny1/=valid_num) THEN
          CALL arpsstop('ERROR:Dimension of obs mismatch the obs file.',1)
        END IF
        !totcwp=valid_num
      END IF

      IF (mp_opt>0) CALL mpupdatei(valid_num,1)
      CALL cwpobs_alloc_array(istatus)

      IF (myproc==0) THEN
        READ(25) (cldphase1(i),i=1,valid_num)
        READ(25) (xloc(i),i=1,valid_num)
        READ(25) (yloc(i),i=1,valid_num)
        READ(25) (cldbase1(i),i=1,valid_num)
        READ(25) (cldtop1(i),i=1,valid_num)
        READ(25) (cwp1(i),i=1,valid_num)
        CLOSE(25)
        cwperr1 = qsrccwp(1,1)
      END IF
    END IF  ! idealopt == 0

    IF (myproc == 0) WRITE(*,'(1x,a,I0,a)') 'INFO: There are ',valid_num,' CWP observations.'

    IF (mp_opt > 0 .AND. valid_num > 0) THEN
      !CALL mpupdater(gridlat1,valid_num)
      !CALL mpupdater(gridlon1,valid_num)
      CALL mpupdater(xloc,     valid_num)
      CALL mpupdater(yloc,     valid_num)
      CALL mpupdater(cldbase1, valid_num)
      CALL mpupdater(cldtop1,  valid_num)
      CALL mpupdater(cwp1,     valid_num)
      CALL mpupdater(cwperr1,  valid_num)
      CALL mpupdatei(cldphase1,valid_num)
    END IF

    RETURN
  END SUBROUTINE cwpobs_read

  !#####################################################################

  SUBROUTINE get_dimensions( filein,filefmt, nxsat,nysat,istatus)

    IMPLICIT NONE

    CHARACTER(LEN=256),INTENT(IN)  :: filein
    INTEGER,           INTENT(IN)  :: filefmt
    INTEGER,           INTENT(OUT) :: nxsat,nysat
    INTEGER                        :: ncid,dimid
    INTEGER                        :: istatus

    INCLUDE 'netcdf.inc'

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    CALL open_ncd_wrf_file ( filein, 'r', ncid, istatus )
    IF (istatus /= NF_NOERR) RETURN

    IF (filefmt == 1) THEN

      istatus = nf_inq_dimid(ncid,'image_x',dimid)
      IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_dimensions:nf_inq_dimid(image_x)')
      istatus = nf_inq_dimlen(ncid,dimid,nxsat)
      IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_dimensions:nf_inq_dimlen(nxsat))')

      istatus = nf_inq_dimid(ncid,'image_y',dimid)
      IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_dimensions:nf_inq_dimid(image_y)')
      istatus = nf_inq_dimlen(ncid,dimid,nysat)
      IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_dimensions:nf_inq_dimlen(nysat))')

    ELSE IF (filefmt == 2) THEN
      istatus = nf_inq_dimid(ncid,'nobs',dimid)
      IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_dimensions:nf_inq_dimid(nobs)')
      istatus = nf_inq_dimlen(ncid,dimid,nxsat)
      IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_dimensions:nf_inq_dimlen(nobs))')

      nysat = 1
    ELSE
      istatus = -1
      WRITE(*,'(1x,a,I0)') 'ERROR: unsupported file formt - ', filefmt
    END IF

    CALL close_ncd_wrf_file ( ncid, istatus )

    RETURN

  END SUBROUTINE get_dimensions

  !#####################################################################

  SUBROUTINE read_cwpfile(file_name,filefmt,nxcwp,nycwp,grid_lat,grid_lon, &
                          cld_base,cld_top,lwp_iwp,cwperrraw,cld_phase)

    USE model_precision

    IMPLICIT NONE

    CHARACTER(LEN=*)      :: file_name
    INTEGER, INTENT(IN)   :: filefmt
    INTEGER               :: nxcwp, nycwp
    REAL, DIMENSION(nxcwp,nycwp), INTENT(OUT) :: grid_lat,grid_lon,     &
                                                 cld_base,cld_top,lwp_iwp,cwperrraw
    INTEGER, INTENT(OUT)  :: cld_phase(nxcwp,nycwp)

  !-----------------------------------------------------------------------

    INTEGER :: istatus
    INTEGER :: nidout

    REAL(SP), ALLOCATABLE :: dat2d(:,:)
    REAL(DP), ALLOCATABLE :: dat1d(:)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  ! io_form = 7   ! hard-coded for 7 at present
  ! numdigit = 4  ! hard-coded for 4 at present

    CALL open_ncd_wrf_file ( file_name, 'r', nidout, istatus )
    IF (istatus /= 0) RETURN

  !-----------------------------------------------------------------------
  ! Initialize data array
  !-----------------------------------------------------------------------

    ! variables used for analysis
    grid_lat  = 0.0
    grid_lon  = 0.0
    cld_base  = 0.0
    cld_top = 0.0
    lwp_iwp = 0.0

    IF (filefmt == 1) THEN
      ALLOCATE(dat2d(nxcwp,nycwp), STAT = istatus)

      CALL get_2dr(nidout,'latitude',nxcwp,nycwp,grid_lat,dat2d,istatus)
      IF (istatus==-1) CALL arpsstop('arpsstop called from reading cloud water path data',1)

      CALL get_2dr(nidout,'longitude',nxcwp,nycwp,grid_lon,dat2d,istatus)
      IF (istatus==-1) CALL arpsstop('arpsstop called from reading cloud water path data',1)

      CALL get_2dr(nidout,'cloud_top_pressure',nxcwp,nycwp,cld_top,dat2d,istatus)
      IF (istatus==-1) CALL arpsstop('arpsstop called from reading cloud water path data',1)

      CALL get_2dr(nidout,'cloud_bottom_pressure',nxcwp,nycwp,cld_base,dat2d,istatus)
      IF (istatus==-1) CALL arpsstop('arpsstop called from reading cloud water path data',1)

      CALL get_2di(nidout,'cloud_phase',nxcwp,nycwp,cld_phase,istatus)
      IF (istatus==-1) CALL arpsstop('arpsstop called from reading cloud water path data',1)

      CALL get_2dr(nidout,'cloud_lwp_iwp',nxcwp,nycwp,lwp_iwp,dat2d,istatus)
      IF (istatus==-1) CALL get_2dr(nidout,'liquid_water_path',nxcwp,nycwp,lwp_iwp,dat2d,istatus)
      IF (istatus==-1) CALL arpsstop('arpsstop called from reading cloud water path data',1)

      DEALLOCATE(dat2d)

      lwp_iwp(:,:)  = lwp_iwp(:,:) / 1000.    ! Convert g/m^2 to kg/m^2
      WHERE (cld_base(:,:) < 9999.9) cld_base(:,:) = cld_base(:,:) * 100.0   ! Convert hpa to pa
      WHERE (cld_top(:,:)  < 9999.9) cld_top(:,:)  = cld_top(:,:)  * 100.0

      cwperrraw = qsrccwp(1,1)

    ELSE IF (filefmt == 2) THEN
      ALLOCATE(dat1d(nxcwp), STAT = istatus)

      CALL get_1dd(nidout,'lat',nxcwp,grid_lat(:,1),dat1d,istatus)
      IF (istatus /= 0) CALL arpsstop('arpsstop called from reading cwp data - lat',1)

      CALL get_1dd(nidout,'lon',nxcwp,grid_lon(:,1),dat1d,istatus)
      IF (istatus /= 0) CALL arpsstop('arpsstop called from reading cwp data - lon',1)

      CALL get_1dd(nidout,'ctp',nxcwp,cld_top(:,1),dat1d,istatus)
      IF (istatus /= 0) CALL arpsstop('arpsstop called from reading cwp data - ctp',1)

      CALL get_1dd(nidout,'cbp',nxcwp,cld_base(:,1),dat1d,istatus)
      IF (istatus /= 0) CALL arpsstop('arpsstop called from reading cwp data - cbp',1)

      CALL get_1di(nidout,'phase',nxcwp,cld_phase(:,1),istatus)
      IF (istatus /= 0) CALL arpsstop('arpsstop called from reading cwp data - phase',1)

      CALL get_1dd(nidout,'cwp',nxcwp,lwp_iwp(:,1),dat1d,istatus)
      IF (istatus /= 0) CALL arpsstop('arpsstop called from reading cwp data - cwp',1)

      CALL get_1dd(nidout,'cwp_err',nxcwp,cwperrraw(:,1),dat1d,istatus)
      IF (istatus /= 0) CALL arpsstop('arpsstop called from reading cwp data - cwperrraw',1)

      DEALLOCATE(dat1d)

      WHERE (cld_base(:,:) < 9999.9) cld_base(:,:) = cld_base(:,:) * 100.0   ! Convert hpa to pa
      WHERE (cld_top(:,:)  > -999.9) cld_top(:,:)  = cld_top(:,:)  * 100.0

    END IF

    CALL close_ncd_wrf_file ( nidout, istatus )

  END SUBROUTINE read_cwpfile

  !#####################################################################

  SUBROUTINE get_2dr(ncid,varname,nxsat,nysat,var2d,dat2d,istatus)

    USE model_precision
    IMPLICIT NONE

    INTEGER,       INTENT(IN)      :: ncid, nxsat, nysat
    CHARACTER(LEN=*)               :: varname
    REAL,          INTENT(OUT)     :: var2d(nxsat,nysat)
    REAL(SP),      INTENT(INOUT)   :: dat2d(nxsat,nysat)
    INTEGER,       INTENT(OUT)     :: istatus

  !-----------------------------------------------------------------------

    INCLUDE 'netcdf.inc'

    INTEGER                        :: varid,vartype,ndims,natts,dimlen
    CHARACTER(LEN=NF_MAX_NAME)     :: namein
    INTEGER                        :: dimids(NF_MAX_VAR_DIMS)
    INTEGER, PARAMETER             :: VAR_NOTEXIST = -1

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = NF_INQ_VARID(ncid,varname,varid)
    IF(istatus == NF_ENOTVAR) THEN
       WRITE(6,'(3a)') ' WARNING: variable ',TRIM(varname),' does not exist.'
       var2d(:,:) = -9999.0
       istatus = VAR_NOTEXIST
       RETURN
    END IF
    CALL handle_ncd_error(istatus,'NF_INQ_VARID in get_2dr with '//TRIM(varname),.TRUE.)

    istatus = NF_INQ_VAR(ncid,varid,namein,vartype,ndims,dimids,natts)
    CALL handle_ncd_error(istatus,'NF_INQ_VAR in get_2dr',.TRUE.)

    IF(vartype /= NF_FLOAT) THEN
      WRITE(6,'(3a)') 'Variable ',varname, ' is not REAL.'
      STOP 'WRONG_VAR_TYPE'
    END IF

    IF(ndims /= 2) THEN
      WRITE(6,'(3a)') 'Variable ', varname, ' is not a 2D array.'
      STOP 'WRONG_VAR_DIMENSIONS'
    END IF

    istatus=NF_INQ_DIMLEN(ncid,dimids(1),dimlen)
    CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_2dr',.TRUE.)
    IF(dimlen /= nxsat) THEN
      WRITE(6,'(3a,I3,a,I0)') 'First dimension of variable ', varname,   &
                      ' is ',dimlen, ' and it should be ',nxsat
      STOP 'WRONG_DIM_length'
    END IF

    istatus=NF_INQ_DIMLEN(ncid,dimids(2),dimlen)
    CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_2dr',.TRUE.)
    IF(dimlen /= nysat) THEN
      WRITE(6,'(3a,I3,a,I0)') 'Second dimension of variable ', varname,   &
                      ' is ',dimlen, ' and it should be ',nysat
      STOP 'WRONG_DIM_length'
    END IF

    istatus = NF_GET_VARA_REAL(ncid,varid,(/1,1/),(/nxsat,nysat/),dat2d)
    CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in get_2dr',.TRUE.)

    var2d(:,:) = dat2d(:,:)

    RETURN
  END SUBROUTINE get_2dr

  !#####################################################################

  SUBROUTINE get_2di(ncid,varname,nxsat,nysat,var2d,istatus)

   IMPLICIT NONE

   INTEGER,       INTENT(IN)      :: ncid, nxsat, nysat
   CHARACTER(LEN=*)               :: varname
   INTEGER,       INTENT(OUT)     :: var2d(nxsat,nysat)
   INTEGER,       INTENT(OUT)     :: istatus

   INCLUDE 'netcdf.inc'

   INTEGER                        :: varid,vartype,ndims,natts,dimlen
   CHARACTER(LEN=NF_MAX_NAME)     :: namein
   INTEGER                        :: dimids(NF_MAX_VAR_DIMS)
   INTEGER, PARAMETER             :: VAR_NOTEXIST = -1


  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = NF_INQ_VARID(ncid,varname,varid)
    IF(istatus == NF_ENOTVAR) THEN
       WRITE(6,'(3a)') ' WARNING: variable ',TRIM(varname),' does not exist.'
       var2d(:,:) = -9999.0
       istatus = VAR_NOTEXIST
       RETURN
    END IF
    CALL handle_ncd_error(istatus,'NF_INQ_VARID in get_2di with '//TRIM(varname),.TRUE.)

    istatus = NF_INQ_VAR(ncid,varid,namein,vartype,ndims,dimids,natts)
    CALL handle_ncd_error(istatus,'NF_INQ_VAR in get_2di',.TRUE.)

    IF(vartype /= NF_INT) THEN
      WRITE(6,'(3a)') 'Variable ',varname, ' is not INTEGER.'
      STOP 'WRONG_VAR_TYPE'
    END IF

    IF(ndims /= 2) THEN
      WRITE(6,'(3a)') 'Variable ', varname, ' is not a 2D array.'
      STOP 'WRONG_VAR_DIMENSIONS'
    END IF

    istatus=NF_INQ_DIMLEN(ncid,dimids(1),dimlen)
    CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_2di',.TRUE.)
    IF(dimlen /= nxsat) THEN
      WRITE(6,'(3a,I3,a,I0)') 'First dimension of variable ', varname,   &
                      ' is ',dimlen, ' and it should be ',nxsat
      STOP 'WRONG_DIM_length'
    END IF

    istatus=NF_INQ_DIMLEN(ncid,dimids(2),dimlen)
    CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_2di',.TRUE.)
    IF(dimlen /= nysat) THEN
      WRITE(6,'(3a,I3,a,I0)') 'Second dimension of variable ', varname,   &
                      ' is ',dimlen, ' and it should be ',nysat
      STOP 'WRONG_DIM_length'
    END IF

    istatus = NF_GET_VARA_INT(ncid,varid,(/1,1/),(/nxsat,nysat/),var2d)
    CALL handle_ncd_error(istatus,'NF_GET_VARA_INT in get_2di',.TRUE.)

    RETURN
  END SUBROUTINE get_2di

  !#####################################################################

  SUBROUTINE get_1dd(ncid,varname,nxsat,var1d,dat1d,istatus)

    USE model_precision
    IMPLICIT NONE

    INTEGER,       INTENT(IN)      :: ncid, nxsat
    CHARACTER(LEN=*)               :: varname
    REAL,          INTENT(OUT)     :: var1d(nxsat)
    REAL(DP),      INTENT(INOUT)   :: dat1d(nxsat)
    INTEGER,       INTENT(OUT)     :: istatus

  !-----------------------------------------------------------------------

    INCLUDE 'netcdf.inc'

    INTEGER                        :: varid,vartype,ndims,natts,dimlen
    CHARACTER(LEN=NF_MAX_NAME)     :: namein
    INTEGER                        :: dimids(NF_MAX_VAR_DIMS)
    INTEGER, PARAMETER             :: VAR_NOTEXIST = -1

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = NF_INQ_VARID(ncid,varname,varid)
    IF(istatus == NF_ENOTVAR) THEN
       WRITE(6,'(3a)') ' WARNING: variable ',TRIM(varname),' does not exist.'
       var1d(:) = -9999.0
       istatus = VAR_NOTEXIST
       RETURN
    END IF
    CALL handle_ncd_error(istatus,'NF_INQ_VARID in get_1dd with '//TRIM(varname),.TRUE.)

    istatus = NF_INQ_VAR(ncid,varid,namein,vartype,ndims,dimids,natts)
    CALL handle_ncd_error(istatus,'NF_INQ_VAR in get_1dd',.TRUE.)

    IF(vartype /= NF_DOUBLE) THEN
      WRITE(6,'(3a)') 'Variable ',varname, ' is not DOUBLE.'
      STOP 'WRONG_VAR_TYPE'
    END IF

    IF(ndims /= 1) THEN
      WRITE(6,'(3a)') 'Variable ', varname, ' is not a 1D array.'
      STOP 'WRONG_VAR_DIMENSIONS'
    END IF

    istatus=NF_INQ_DIMLEN(ncid,dimids(1),dimlen)
    CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_1dd',.TRUE.)
    IF(dimlen /= nxsat) THEN
      WRITE(6,'(3a,I3,a,I0)') 'First dimension of variable ', varname,   &
                      ' is ',dimlen, ' and it should be ',nxsat
      STOP 'WRONG_DIM_length'
    END IF

    istatus = NF_GET_VARA_DOUBLE(ncid,varid,(/1/),(/nxsat/),dat1d)
    CALL handle_ncd_error(istatus,'NF_GET_VARA_DOUBLE in get_1dd',.TRUE.)

    var1d(:) = dat1d(:)

    RETURN
  END SUBROUTINE get_1dd

  !#####################################################################

  SUBROUTINE get_1di(ncid,varname,nxsat,var1d,istatus)

   IMPLICIT NONE

   INTEGER,       INTENT(IN)      :: ncid, nxsat
   CHARACTER(LEN=*)               :: varname
   INTEGER,       INTENT(OUT)     :: var1d(nxsat)
   INTEGER,       INTENT(OUT)     :: istatus

   INCLUDE 'netcdf.inc'

   INTEGER                        :: varid,vartype,ndims,natts,dimlen
   CHARACTER(LEN=NF_MAX_NAME)     :: namein
   INTEGER                        :: dimids(NF_MAX_VAR_DIMS)
   INTEGER, PARAMETER             :: VAR_NOTEXIST = -1


  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = NF_INQ_VARID(ncid,varname,varid)
    IF(istatus == NF_ENOTVAR) THEN
       WRITE(6,'(3a)') ' WARNING: variable ',TRIM(varname),' does not exist.'
       var1d(:) = -9999.0
       istatus = VAR_NOTEXIST
       RETURN
    END IF
    CALL handle_ncd_error(istatus,'NF_INQ_VARID in get_1di with '//TRIM(varname),.TRUE.)

    istatus = NF_INQ_VAR(ncid,varid,namein,vartype,ndims,dimids,natts)
    CALL handle_ncd_error(istatus,'NF_INQ_VAR in get_1di',.TRUE.)

    IF(vartype /= NF_INT) THEN
      WRITE(6,'(3a)') 'Variable ',varname, ' is not INTEGER.'
      STOP 'WRONG_VAR_TYPE'
    END IF

    IF(ndims /= 1) THEN
      WRITE(6,'(3a)') 'Variable ', varname, ' is not a 1D array.'
      STOP 'WRONG_VAR_DIMENSIONS'
    END IF

    istatus=NF_INQ_DIMLEN(ncid,dimids(1),dimlen)
    CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_1di',.TRUE.)
    IF(dimlen /= nxsat) THEN
      WRITE(6,'(3a,I3,a,I0)') 'First dimension of variable ', varname,   &
                      ' is ',dimlen, ' and it should be ',nxsat
      STOP 'WRONG_DIM_length'
    END IF

    istatus = NF_GET_VARA_INT(ncid,varid,(/1/),(/nxsat/),var1d)
    CALL handle_ncd_error(istatus,'NF_GET_VARA_INT in get_1di',.TRUE.)

    RETURN
  END SUBROUTINE get_1di

END MODULE module_cwpobs
