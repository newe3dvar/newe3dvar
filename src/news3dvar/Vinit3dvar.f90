!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE INIT3DVAR                  ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE init3dvar(cntl_var_rh_out,npass,nx,ny,namelist_filename)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!  Read in analysis variables in namelist format from standard input.
!
!  AUTHOR:
!  Jidong GAO, add 3dvar input parameter, 2001
!
!  MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  USE module_varpara
  USE constraint_divergence
  USE constraint_diagnostic_div
  USE constraint_mslp
  USE constraint_thermo
  USE constraint_smooth
  USE module_lightning
  USE module_ensemble
  USE module_aeri ! JJ

  IMPLICIT NONE

  !INCLUDE 'varpara.inc'
  INCLUDE 'grid.inc'
  INCLUDE 'mp.inc'
!
  INTEGER :: nx,ny,nz          ! ARPS grid size
  INTEGER :: nstyps            ! Maximum number of soil types per grid point.
  INTEGER :: nt                ! Number of time levels of data
  INTEGER :: i
  INTEGER, INTENT(IN) :: npass
!
!
!-----------------------------------------------------------------------
!
!  3DVAR namelists
!
!-----------------------------------------------------------------------
!
  !NAMELIST /var_const/ maxin

  !NAMELIST /var_refil/ipass_filt,hradius,vradius_opt,vradius,smooth_opt

  !NAMELIST /var_exprt/chk_opt,assim_opt,cntl_var,cntl_var_rh

  !NAMELIST /var_reflec/ref_opt, intvl_T, thresh_ref, bkgerr_qr, bkgerr_qs, &
  !           bkgerr_qh, wgt_ref, hradius_ref, vradius_opt_ref, vradius_ref
!
  INTEGER,          INTENT(OUT) :: cntl_var_rh_out
  CHARACTER(LEN=*), INTENT(IN)  :: namelist_filename

  INTEGER :: unum, istatus
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  unum = 0
  IF (myproc == 0) THEN
    IF (LEN_TRIM(namelist_filename) <= 0 .OR. namelist_filename == ' ') THEN
      unum = 5
      WRITE(6,'(2(1x,a,/))') 'Waiting NEWS3DVAR namelist from standard input ... ', &
                             '========================================'
    ELSE
      CALL getunit( unum )
      OPEN(unum,FILE=TRIM(namelist_filename),STATUS='OLD',FORM='FORMATTED')
      WRITE(6,'(1x,3a,/,1x,a,/)') 'Reading NEWS3DVAR namelist from file - ',   &
              TRIM(namelist_filename),' ... ','========================================'
    END IF
  END IF
!
!-----------------------------------------------------------------------
!
!  Assign default values to the ADAS input variables
!
!-----------------------------------------------------------------------
!
!   maxin(1) = 50
!   maxin(2) = 20
!   maxin(3) = 30
!   ipass_filt(1) = 1
!   ipass_filt(2) = 1
!   ipass_filt(3) = 1
!   hradius(1) = 25.0
!   hradius(2) = 25.0
!   hradius(3) = 15.0
!   vradius_opt= 1
!   vradius(1) = 2
!   vradius(2) = 2
!   vradius(3) = 2
!   chk_opt = 0
!   assim_opt= 1
!   cntl_var = 0
!   !div_opt = 0
!   !DO i=1,maxpass
!   !  wgt_div_h(i) = -1.0
!   !  wgt_div_v(i) = -1.0
!   !ENDDO
!
!   ref_opt = 0
!   intvl_T = 5.0
!   thresh_ref = 15.0
!   wgt_ref = 0.0
!   hradius_ref = 3.0
!   vradius_opt_ref = 1
!   vradius_ref = 2
!   bkgerr_qr = 1.0E-4
!   bkgerr_qs = 1.0E-4
!   bkgerr_qh = 1.0E-4
!
!!
!!-----------------------------------------------------------------------
!!
!!  read in 3DVAR namelists
!!
!!-----------------------------------------------------------------------
!!
!  IF (myproc == 0) THEN
!    READ(unum, var_const,  END=350)
!    WRITE(6,*) 'Namelist block 3dvar_const sucessfully read.'
!
!    READ(unum, var_refil,  END=350)
!    WRITE(6,*) 'Namelist block 3dvar_refil sucessfully read.'
!
!    READ(unum, var_exprt,  END=350)
!    WRITE(6,*) 'Namelist block 3dvar_exprt sucessfully read.'
!
!    !READ(unum, var_diverge,  END=350)
!    !WRITE(6,*) 'Namelist block 3dvar_divergence sucessfully read.'
!
!    !READ(unum, var_mslp,  END=350)
!    !WRITE(6,*) 'Namelist block 3dvar_mslp sucessfully read.'
!
!    READ(unum, var_reflec,  END=350)
!    WRITE(6,*) 'Namelist block 3dvar_reflec sucessfully read.'
!
!  ! READ(unum, var_smth,  END=350)
!  ! WRITE(6,*) 'Namelist block 3dvar_smoothness sucessfully read.'
!
!    350 CONTINUE
!  END IF
!  CALL mpupdatei(maxin,      maxpass)
!  CALL mpupdatei(ipass_filt, maxpass)
!  CALL mpupdater(hradius,    maxpass)
!  CALL mpupdatei(vradius,    maxpass)
!  CALL mpupdatei(vradius_opt,maxpass)
!
!  CALL mpupdatei(chk_opt,     1)
!  CALL mpupdatei(assim_opt,   1)
!  CALL mpupdatei(cntl_var,    1)
!  CALL mpupdatei(cntl_var_rh, 1)
!
!  !CALL mpupdatei(div_opt,   1)
!  !CALL mpupdater(wgt_div_h, maxpass)
!  !CALL mpupdater(wgt_div_v, maxpass)
!
!  !CALL mpupdatei(mslp_opt,   1)
!  !CALL mpupdater(mslp_lat,   1)
!  !CALL mpupdater(mslp_lon,   1)
!  !CALL mpupdater(mslp_obs,   1)
!  !CALL mpupdater(mslp_err, maxpass)
!
!  !CALL mpupdatei(smth_flag, 1)
!  !CALL mpupdater(wgt_smth,  maxpass)
!
!  !CALL mpupdatei(thermo_opt, 1)
!  !CALL mpupdater(wgt_thermo, maxpass)
!
!  CALL mpupdatei(ref_opt, 1)
!  CALL mpupdatei(vradius_opt_ref, 1)
!  CALL mpupdater(intvl_T, 1)
!  CALL mpupdater(thresh_ref, 1)
!  CALL mpupdater(wgt_ref, maxpass)
!  CALL mpupdater(hradius_ref, maxpass)
!  CALL mpupdater(vradius_ref, maxpass)
!  CALL mpupdater(bkgerr_qr, maxpass)
!  CALL mpupdater(bkgerr_qs, maxpass)
!  CALL mpupdater(bkgerr_qh, maxpass)
!
!  !IF ( myproc == 0 ) THEN
!  !
!  !  WRITE(*,*) 'mslp_opt = ', mslp_opt
!  !  IF ( mslp_opt >= 1 ) THEN
!  !     mslp_obs = mslp_obs * 100 !hPa -> Pa
!  !     mslp_err = mslp_err * 100 !hPa -> Pa
!  !     WRITE(*,*) 'mslp_lat=', mslp_lat, ' mslp_lon=', mslp_lon
!  !     WRITE(*,*) 'observed MSLP(Pa):', mslp_obs
!  !     WRITE(*,*) 'User-specified MSLP obs error(Pa):'
!  !     WRITE(*,*) (mslp_err(i), i =1 , npass )
!  !  END IF
!  !
!  !END IF
!
!  DO i=1,npass
!    hradius(i)= hradius(i)*1000.0/dx
!    IF (myproc == 0) THEN
!      WRITE(*,*) 'Analysis pass:', i
!      WRITE(*,*) 'The horizontal influence radius:',hradius(i),'grid points'
!      WRITE(*,*) 'The vertical influence radius option vradius_opt=',vradius_opt(i)
!      IF (vradius_opt(i) == 1 ) THEN
!        WRITE(*,*) 'The vertical influence radius:',vradius(i), 'grid points'
!      ELSE IF (vradius_opt(i) == 2 ) THEN
!        WRITE(*,*) 'The vertical influence radius:',vradius(i), 'km'
!      ELSE
!        WRITE(*,*) 'vradius_opt = ', vradius_opt(i), ' is not supported.'
!        WRITE(*,*) 'ARPS3DVAR will stop here'
!        CALL arpsstop('ERROR: Wrong vradius_opt.',1)
!      END IF
!    END IF
!  END DO
!
!  IF (myproc == 0) THEN
!    WRITE(*,*)
!    WRITE(*,*) 'ref_opt = ', ref_opt
!    WRITE(*,*) 'intvl_T=', intvl_T, 'thresh_ref= ', thresh_ref
!    WRITE(*,*) 'bkgerr_qr/qs/qh=', bkgerr_qr, bkgerr_qs, bkgerr_qh
!  END IF
!  DO i=1,npass
!    hradius_ref(i)= hradius_ref(i)*1000.0/dx
!    IF (myproc == 0) THEN
!      WRITE(*,*) 'Analysis pass:', i, ' wgt_ref =', wgt_ref(i)
!      WRITE(*,*) 'The horizontal influence radius:',hradius_ref(i),'grid points'
!      WRITE(*,*) 'The vertical influence radius option vradius_opt_ref=',vradius_opt_ref(i)
!      IF (vradius_opt_ref(i) == 1 ) THEN
!        WRITE(*,*) 'The vertical influence radius:',vradius_ref(i), 'grid points'
!      ELSE IF (vradius_opt_ref(i) == 2 ) THEN
!        WRITE(*,*) 'The vertical influence radius:',vradius_ref(i), 'km'
!      ELSE
!        WRITE(*,*) 'vradius_opt_ref = ', vradius_opt_ref(i), ' is not supported.'
!        WRITE(*,*) 'ARPS3DVAR will stop here'
!        CALL arpsstop('ERROR: Wrong vradius_opt_ref.',1)
!      END IF
!    END IF
!  END DO

  CALL varpara_read_nml_var(unum,npass,myproc,nx,ny,dx,dy,istatus)
!
!-----------------------------------------------------------------------
!

  CALL consdiv3_read_nml_options(unum,npass,myproc,istatus)

  CALL consthermo_read_nml_options(unum,npass,myproc,istatus)

  CALL consmth_read_nml_options(unum,npass,myproc,istatus)

  CALL consmslp_read_nml_options(unum,npass,myproc,istatus)

  CALL consdiv_read_nml_options(unum,npass,myproc,istatus)

  CALL lightning_read_nml_options(unum,npass,myproc,istatus)

  CALL aeri_read_nml_options(unum,npass,myproc,istatus) !JJ

  cntl_var_rh_out=cntl_var_rh
!
!-----------------------------------------------------------------------
!
  CALL ens_read_nml(unum,npass,myproc,dx,dy,istatus)

  IF (unum /= 5 .AND. myproc == 0) THEN
    CLOSE(unum)
    CALL retunit(unum)
  END IF

  RETURN
END SUBROUTINE init3dvar


!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE PRODUCT                    ######
!######                                                      ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE product(a,b,nx,ny,nz,                                        &
           ibgn,iend,jbgn,jend,kbgn,kend,pr)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the product SIGMA a(i,j,k)*b(i,j,k).
!
!-----------------------------------------------------------------------
!
!  AUTHOR: JIDONG GAO
!  01/23/00
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       First dimension of arrays a, b and ab
!    ny       Second dimension of arrays a, b and ab
!    nz       Third dimension of arrays a, b and ab
!
!    a        First multiplier array
!    b        Second multiplier array
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!  OUTPUT:
!
!    pr     The sum of the product a(i,j,k)*b(i,j,k).
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions
!
  REAL :: a (nx,ny,nz)         ! Input array 1
  REAL :: b (nx,ny,nz)         ! Input array 2
!
  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
!
  REAL :: pr                   ! The sum of the product a(i,j,k)*b(i,j,k).
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  pr=0.
  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        pr=pr+a(i,j,k)*b(i,j,k)

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE product
