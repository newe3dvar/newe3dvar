MODULE module_pseudoQobs

  IMPLICIT NONE
  SAVE

! REAL,PARAMETER   :: pQob_pt_err= 0.8*0.8 !2.6*2.6 !0.5 * 0.5
! REAL,PARAMETER   :: pQob_qv_err= 1.0E-3*1.0E-3 !1.0E-3 *1.0E-3

!  REAL,PARAMETER   :: pQob_pt_err= 2.0*2.0 !2.6*2.6 !0.5 * 0.5
  REAL,PARAMETER   :: pQob_pt_err= 4.5*4.5 !2.6*2.6 !0.5 * 0.5
  REAL,PARAMETER   :: pQob_qv_err= 1.0E-3*1.0E-3 !0.05*0.05 !1.0E-3 *1.0E-3
!  REAL,PARAMETER   :: pQob_qv_err= 5.0E-3*5.0E-3 !0.05*0.05 !1.0E-3 *1.0E-3

  REAL,ALLOCATABLE :: pQob_qs_err(:)
!
  INTEGER :: No4pt
  INTEGER, DIMENSION (:),   ALLOCATABLE :: i_pt, j_pt, k_pt

  INTEGER :: No4qv
  INTEGER, DIMENSION (:),   ALLOCATABLE :: i_qv, j_qv, k_qv

  INTEGER, DIMENSION (:),   ALLOCATABLE :: No4qs
  INTEGER, DIMENSION (:,:), ALLOCATABLE :: i_qs, j_qs, k_qs

  REAL,    DIMENSION (:),   ALLOCATABLE ::        pt_pQob
  REAL,    DIMENSION (:),   ALLOCATABLE ::        qv_pQob
  REAL,    DIMENSION (:,:), ALLOCATABLE :: qscalar_pQob
!
  REAL,    DIMENSION (:),   ALLOCATABLE ::        pt_pQJc
  REAL,    DIMENSION (:),   ALLOCATABLE ::        qv_pQJc
  REAL,    DIMENSION (:,:), ALLOCATABLE :: qscalar_pQJc
!
!  LOGICAL :: test_pt, test_qv, test_qs
  INTEGER :: P_QIi,P_QCc    ! Exclude from var analysis

  CONTAINS

    SUBROUTINE allocatePseudoQobs(nx,ny,nz,ref_use,hydro_opt)
      USE module_varpara, ONLY: pse_qr_err,pse_qs_err,pse_qg_err,pse_qh_err
      INTEGER, INTENT(IN)  :: nx,ny,nz
      LOGICAL, INTENT(IN)  :: ref_use
      INTEGER, INTENT(IN)  :: hydro_opt

    !-------------------------------------------------------------------

      INTEGER :: ierr
      INTEGER :: ctrsize
      INTEGER :: nxyz

      INCLUDE 'mp.inc'
      INCLUDE 'globcst.inc'

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


      nxyz = nx*ny*nz
      IF ( ALLOCATED(i_pt) ) DEALLOCATE (i_pt)
      ALLOCATE ( i_pt(nxyz), stat=ierr )
      IF ( ALLOCATED(j_pt) ) DEALLOCATE (j_pt)
      ALLOCATE ( j_pt(nxyz), stat=ierr )
      IF ( ALLOCATED(k_pt) ) DEALLOCATE (k_pt)
      ALLOCATE ( k_pt(nxyz), stat=ierr )

      IF ( ALLOCATED(pt_pQob) ) DEALLOCATE (pt_pQob)
      ALLOCATE ( pt_pQob(nxyz), stat=ierr )
      CALL check_alloc_status(ierr, "PseudoQob:pt_pQob")
      pt_pQob = 0.0
      IF ( ALLOCATED(pt_pQJc) ) DEALLOCATE (pt_pQJc)
      ALLOCATE ( pt_pQJc(nxyz), stat=ierr )
      CALL check_alloc_status(ierr, "PseudoQob:pt_pQJc")
      pt_pQJc= 0.0

      IF ( ALLOCATED(i_qv) ) DEALLOCATE (i_qv)
      ALLOCATE ( i_qv(nxyz), stat=ierr )
      IF ( ALLOCATED(j_qv) ) DEALLOCATE (j_qv)
      ALLOCATE ( j_qv(nxyz), stat=ierr )
      IF ( ALLOCATED(k_qv) ) DEALLOCATE (k_qv)
      ALLOCATE ( k_qv(nxyz), stat=ierr )

      IF ( ALLOCATED(   qv_pQob) ) DEALLOCATE (   qv_pQob)
      ALLOCATE (    qv_pQob(nxyz), stat=ierr )
      CALL check_alloc_status(ierr, "PseudoQob:qv_pQob")
      qv_pQob = 0.0
      IF ( ALLOCATED(   qv_pQJc) ) DEALLOCATE (   qv_pQJc)
      ALLOCATE (    qv_pQJc(nxyz), stat=ierr )
      CALL check_alloc_status(ierr, "PseudoQJc:qv_pQJc")
      qv_pQJc = 0.0

      IF ( ref_use .or. hydro_opt==1 ) THEN

        IF ( ALLOCATED(No4qs) ) DEALLOCATE (No4qs)
        ALLOCATE ( No4qs(nscalarq), stat=ierr )

        IF ( ALLOCATED(i_qs) ) DEALLOCATE (i_qs)
        ALLOCATE ( i_qs(nxyz,nscalarq), stat=ierr )

        IF ( ALLOCATED(j_qs) ) DEALLOCATE (j_qs)
        ALLOCATE ( j_qs(nxyz,nscalarq), stat=ierr )

        IF ( ALLOCATED(k_qs) ) DEALLOCATE (k_qs)
        ALLOCATE ( k_qs(nxyz,nscalarq), stat=ierr )

        IF ( ALLOCATED(qscalar_pQob) ) DEALLOCATE (qscalar_pQob)
        ALLOCATE ( qscalar_pQob(nxyz,nscalarq), stat=ierr )
        CALL check_alloc_status(ierr, "PseudoQob:qscalar_pQob")
        qscalar_pQob = 0.0

        IF ( ALLOCATED(qscalar_pQJc) ) DEALLOCATE (qscalar_pQJc)
        ALLOCATE ( qscalar_pQJc(nxyz,nscalarq), stat=ierr )
        CALL check_alloc_status(ierr, "PseudoQJc:qscalar_pQJc")
        qscalar_pQJc = 0.0

      END IF

      ALLOCATE(pQob_qs_err(nscalarq), STAT = ierr)

      IF (P_QC > 0) pQob_qs_err(P_QC) = 2.5E-4*2.5E-4 !1.0E-3 *1.0E-3(0.5E-3)**2
      IF (P_QR > 0) pQob_qs_err(P_QR) = pse_qr_err**2 !1.0E-3 *1.0E-3(0.5E-3)**2
      IF (P_QI > 0) pQob_qs_err(P_QI) = 2.5E-4*2.5E-4 !1.0E-3 *1.0E-3(0.9E-3)**2
      IF (P_QS > 0) pQob_qs_err(P_QS) = pse_qs_err**2 !1.0E-3 *1.0E-3(0.6E-3)**2
      IF (P_QG > 0) pQob_qs_err(P_QG) = pse_qg_err**2 !1.0E-3 *1.0E-3(0.8E-3)**2
      IF (P_QH > 0) pQob_qs_err(P_QH) = pse_qh_err**2 !1.0E-3 *1.0E-3(0.8E-3)**2
      P_QCc = P_QC
      P_QIi = P_QI

    END  SUBROUTINE allocatePseudoQobs

!#######################################################################

    SUBROUTINE deallocatePseudoQobs(istatus)

      INTEGER, INTENT(OUT) :: istatus

    !-------------------------------------------------------------------

      DEALLOCATE ( i_pt, j_pt, k_pt )
      DEALLOCATE ( i_qv, j_qv, k_qv )
      DEALLOCATE ( i_qs, j_qs, k_qs )

      DEALLOCATE ( pt_pQob,      STAT = istatus )
      DEALLOCATE ( qv_pQob,      STAT = istatus )
      DEALLOCATE ( qscalar_pQob, STAT = istatus )

      DEALLOCATE ( pt_pQJc,      STAT = istatus )
      DEALLOCATE ( qv_pQJc,      STAT = istatus )
      DEALLOCATE ( qscalar_pQJc, STAT = istatus )

    END SUBROUTINE deallocatePseudoQobs

  !#####################################################################

  SUBROUTINE pseudoQ_init(nx,ny,nz,nscalarq,zs,ref_mos_3d,ptbar,          &
                          pt_ptem,qv_ptem,qscalar_ptem,istatus)

    INTEGER, INTENT(IN)  :: nx,ny,nz,nscalarq
    REAL,    INTENT(IN)  :: zs(nx,ny,nz)
    REAL,    INTENT(IN)  :: ref_mos_3d(nx,ny,nz),ptbar(nx,ny,nz)
    REAL,    INTENT(IN)  :: pt_ptem(nx,ny,nz)
    REAL,    INTENT(IN)  :: qv_ptem(nx,ny,nz)
    REAL,    INTENT(IN)  :: qscalar_ptem(nx,ny,nz,nscalarq)

    INTEGER, INTENT(OUT) :: istatus

    REAL :: refmax, refmin1, refmin2, refmin
    PARAMETER (refmax = 85.)  ! maximum non-missing reflectivity
!    PARAMETER (refmin1= 30.)  ! maximin non-missing reflectivity
!    PARAMETER (refmin2= 25.)  ! maximin non-missing reflectivity

    PARAMETER (refmin1= 30.)  ! maximin non-missing reflectivity for pt
    PARAMETER (refmin2= 25.)  ! maximin non-missing reflectivity for pt

  !---------------------------------------------------------------------

    INTEGER :: i,j,k,nq

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !test_qv = .TRUE.
    !test_pt = .FALSE.
    !test_qs = .FALSE.

    No4pt = 0
    DO k=2,nz-1
      DO j=2,ny-2
        DO i=2,nx-2

          IF ( zs(i,j,k) >= 1500.0) THEN
            refmin =refmin2
          ELSE
            refmin =refmin1
          END IF

          IF(ref_mos_3d(i,j,k) > refmin .AND. ref_mos_3d(i,j,k) < refmax) THEN

            No4pt = No4pt+1
            i_pt( No4pt ) = i
            j_pt( No4pt ) = j
            k_pt( No4pt ) = k

            IF(pt_ptem(i,j,k) > -900.0 ) THEN
              pt_pQob(No4pt) = pt_ptem(i,j,k) + ptbar(i,j,k)
            END IF

          END IF

        END DO
      END DO
    END DO

    No4qv = 0
    DO k=2,nz-1
      DO j=2,ny-2
        DO i=2,nx-2

           IF ( zs(i,j,k) >= 1500.0) THEN
             refmin =refmin2
           ELSE
             refmin =refmin1
           END IF

           !IF(ref_mos_3d(i,j,k) > refmin .AND. ref_mos_3d(i,j,k) < refmax) THEN

             No4qv = No4qv+1
             i_qv( No4qv ) = i
             j_qv( No4qv ) = j
             k_qv( No4qv ) = k
             IF(qv_ptem(i,j,k) > -900.0 ) THEN
               qv_pQob(No4qv) = qv_ptem(i,j,k)
             END IF

           !END IF

        END DO
      END DO
    END DO

    DO nq = 1, nscalarq

      No4qs(nq) = 0

      DO k=2,nz-1
        DO j=2,ny-2
          DO i=2,nx-2

         IF ( zs(i,j,k) >= 1500.0) THEN
            refmin =refmin2
         ELSE
           refmin =refmin1
         END IF

          IF(ref_mos_3d(i,j,k) < refmax) THEN !ref_mos_3d(i,j,k) > refmin .and.

              No4qs(nq) = No4qs(nq)+1
              i_qs(No4qs(nq),nq) = i
              j_qs(No4qs(nq),nq) = j
              k_qs(No4qs(nq),nq) = k
              IF(qscalar_ptem(i,j,k,nq) > -900.0 ) THEN
              qscalar_pQob(No4qs(nq),nq) = qscalar_ptem(i,j,k,nq)
            END IF
            END IF

          END DO
        END DO
      END DO
    END DO

    RETURN
  END SUBROUTINE pseudoQ_init

  !#####################################################################

  SUBROUTINE pseudoQ_cost(nx,ny,nz,nscalarq,ref_opt,hydro_opt,          &
                          pt_ctr,q_ctr,qscalar_ctr,                     &
                          f_cld_pt,f_cld_qv,f_cld_qs,f_cld_tot,         &
                          test_pt,test_qv,test_qs,istatus)

    INTEGER,          INTENT(IN)  :: nx,ny,nz,nscalarq
    INTEGER,          INTENT(IN)  :: ref_opt,hydro_opt
    REAL,             INTENT(IN)  :: pt_ctr(nx,ny,nz)
    REAL,             INTENT(IN)  :: q_ctr(nx,ny,nz)
    REAL,             INTENT(IN)  :: qscalar_ctr(nx,ny,nz,nscalarq)
    DOUBLE PRECISION, INTENT(OUT) :: f_cld_pt, f_cld_qv
    DOUBLE PRECISION, INTENT(OUT) :: f_cld_qs(nscalarq)
    DOUBLE PRECISION, INTENT(OUT) :: f_cld_tot
    INTEGER,          INTENT(IN)  :: test_pt,test_qv,test_qs

    INTEGER, INTENT(OUT) :: istatus
  !---------------------------------------------------------------------

    INTEGER :: i,j,k,n,nq

    DOUBLE PRECISION :: ztt, zqq

    !INCLUDE 'mp.inc'

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    f_cld_tot   = 0.0D0

    f_cld_pt    = 0.0D0
    f_cld_qv    = 0.0D0
    f_cld_qs(:) = 0.0D0

    IF (test_pt == 1) THEN
      DO n=1, No4pt
        i = i_pt(n)
        j = j_pt(n)
        k = k_pt(n)

        ztt = pt_ctr(i,j,k) - pt_pQob(n)
        pt_pQJc(n)=ztt/pQob_pt_err
        f_cld_pt=f_cld_pt+pt_pQJc(n)*ztt

      END DO
    END IF

    IF (test_qv == 1) THEN
      DO n=1, No4qv
        i = i_qv(n)
        j = j_qv(n)
        k = k_qv(n)


        zqq = q_ctr(i,j,k) - qv_pQob(n)
        qv_pQJc(n)=zqq/pQob_qv_err
        f_cld_qv=f_cld_qv+qv_pQJc(n)*zqq
        !write(200,*) n,i,j,k,qv_pQob(n),qv_pQJc(n)

      END DO
    END IF

    IF (test_qs == 1) THEN

      IF ( ref_opt==1 .OR. hydro_opt==1 ) THEN
        DO nq = 1, nscalarq

          IF (nq .EQ. P_QIi .OR. nq .EQ. P_QCc) CYCLE

          DO n=1, No4qs(nq)
            i = i_qs(n,nq)
            j = j_qs(n,nq)
            k = k_qs(n,nq)
            zqq = qscalar_ctr(i,j,k,nq) - qscalar_pQob(n,nq)
            qscalar_pQJc(n,nq)=zqq/pQob_qs_err(nq)
            f_cld_qs(nq)=f_cld_qs(nq)+qscalar_pQJc(n,nq)*zqq
          END DO

        END DO
      END IF

    END IF

    f_cld_tot=f_cld_pt+f_cld_qv
    DO nq = 1, nscalarq
      f_cld_tot = f_cld_tot + f_cld_qs(nq)
    END DO

    RETURN
  END SUBROUTINE pseudoQ_cost

  !#####################################################################

  SUBROUTINE pseudoQ_gradt(nx,ny,nz,nscalarq,ref_opt,hydro_opt,         &
                           pt_grd,q_grd,qscalar_grd,tem1,               &
                           test_pt,test_qv,test_qs,istatus)

    INTEGER, INTENT(IN)     :: nx,ny,nz,nscalarq
    INTEGER, INTENT(IN)     :: ref_opt,hydro_opt
    REAL,    INTENT(INOUT)  :: pt_grd(nx,ny,nz)
    REAL,    INTENT(INOUT)  :: q_grd(nx,ny,nz)
    REAL,    INTENT(INOUT)  :: qscalar_grd(nx,ny,nz,nscalarq)
    INTEGER, INTENT(IN)     :: test_pt,test_qv,test_qs

    REAL,    INTENT(OUT) :: tem1(nx,ny,nz)
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j,k,n,nq
    INTEGER :: ebc, wbc, sbc, nbc

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ebc = 0; wbc = 0; sbc = 0; nbc = 0

    IF (test_pt == 1) THEN

      DO n=1, No4pt
        i = i_pt(n)
        j = j_pt(n)
        k = k_pt(n)
        pt_grd(i,j,k) = pt_grd(i,j,k) + pt_pQJc(n)
      END DO

      !IF (mp_opt > 0) THEN
        CALL mpsendrecv2dew(pt_grd,nx,ny,nz,ebc,wbc,0,tem1)
        CALL mpsendrecv2dns(pt_grd,nx,ny,nz,nbc,sbc,0,tem1)
      !END IF

    END IF

    IF (test_qv == 1) THEN
      DO n=1, No4qv
        i = i_qv(n)
        j = j_qv(n)
        k = k_qv(n)
        q_grd(i,j,k) = q_grd(i,j,k) + qv_pQJc(n)
        !write(300,*) n,i,j,k,qv_pQJc(n)

      END DO

      !IF (mp_opt > 0) THEN
        CALL mpsendrecv2dew(q_grd,nx,ny,nz,ebc,wbc,0,tem1)
        CALL mpsendrecv2dns(q_grd,nx,ny,nz,nbc,sbc,0,tem1)
      !END IF

    END IF

    IF (test_qs == 1) THEN
      IF ( ref_opt==1 .OR. hydro_opt==1 ) THEN

        DO nq = 1,nscalarq

          IF (nq .EQ. P_QIi .OR. nq .EQ. P_QCc) CYCLE
          DO n=1, No4qs(nq)
            i = i_qs(n,nq)
            j = j_qs(n,nq)
            k = k_qs(n,nq)
            qscalar_grd(i,j,k,nq) = qscalar_grd(i,j,k,nq) + qscalar_pQJc(n,nq)
          END DO
        END DO

        DO nq = 1, nscalarq
          !IF (mp_opt > 0) THEN
            CALL mpsendrecv2dew(qscalar_grd(:,:,:,nq),nx,ny,nz,ebc,wbc,0,tem1)
            CALL mpsendrecv2dns(qscalar_grd(:,:,:,nq),nx,ny,nz,nbc,sbc,0,tem1)
          !END IF

        END DO

      END IF ! ref_opt==1 .or. hydro_opt==1
    END IF

    RETURN
  END SUBROUTINE pseudoQ_gradt

END MODULE module_pseudoQobs

