!
!####################### Extracted from GSI ############################
!
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Use Anisotropic filter to model the background covariance
!  Adapted from GSI codes (kinds.f90,plib8.f90,raflib.f90)
!
!-----------------------------------------------------------------------
!
!  AUTHOR:
!
!-----------------------------------------------------------------------
MODULE filtpara
  USE raflib, only : filter_cons,raf4_ad,init_raf4,raf4,raf_sm4_ad,raf_sm4

  TYPE(filter_cons), SAVE :: ufilter(7), vfilter(7), pfilter(7), tfilter(7),   &
                             qvfilter(7),wfilter(7)
  TYPE(filter_cons), ALLOCATABLE, SAVE :: qscalarfilter(:,:)
  TYPE(filter_cons), SAVE :: enfilter(7)

  LOGICAL, SAVE :: filter_allocated = .FALSE.

  CONTAINS

  !#####################################################################

  SUBROUTINE filtpara_init(nscalarq,istatus)
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nscalarq
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    ALLOCATE(qscalarfilter(7,nscalarq), STAT = istatus)
    filter_allocated = .TRUE.

    RETURN
  END SUBROUTINE filtpara_init

END MODULE filtpara

!#######################################################################
!
! SUBROUTINE INITFILTER
!
!#######################################################################
!

SUBROUTINE initfilter(modelopt,stat_iso,hradius_3d,vradius,             &
                     hradius_3d_ref,  vradius_ref,                      &
                     ref_opt,hydro_opt,                                 &
                     nx,ny,nz,nscalarq,gdu_err,gdv_err,                 &
                     gdp_err,gdt_err,gdq_err,gdw_err,                   &
                     gdqscalar_err )
  USE filtpara
  USE mpimod

  IMPLICIT NONE
  INTEGER, INTENT(IN) :: modelopt
  INTEGER, INTENT(IN) :: stat_iso,nx,ny,nz,nscalarq
  REAL    :: vradius(nx,ny,nz)
  REAL    :: vradius_ref(nx,ny,nz)
  REAL    :: hradius_3d(nx,ny,nz),hradius_3d_ref(nx,ny,nz)
  INTEGER :: ref_opt,hydro_opt

  REAL    :: gdu_err(nx,ny,nz),gdv_err(nx,ny,nz),gdw_err(nx,ny,nz)
  REAL    :: gdp_err(nx,ny,nz),gdt_err(nx,ny,nz),gdq_err(nx,ny,nz)
  REAL    :: gdqscalar_err(nx,ny,nz,nscalarq)
!
  REAL    :: fradius_u, fradius_v, fradius_w, fradius_p, fradius_pt,fradius_qv
  REAL    :: fradius_qscalar(nscalarq)
!
! NameList /fradius/ fradius_u, fradius_v, fradius_w, fradius_p, fradius_pt,  &
!    fradius_qv, fradius_qc, fradius_qr, fradius_qi, fradius_qs, fradius_qh
!   read(5,fradius,end=390)
!     write(6,*) "fradius(u,v,w,p,pt,qv) read: ", fradius_u, fradius_v,       &
!                fradius_w, fradius_p, fradius_pt,fradius_qv,fradius_qc,      &
!                fradius_qr, fradius_qi, fradius_qs, fradius_qh
!   390 continue
!
!-----------------------------------------------------------------------

  INTEGER :: nq
  INTEGER :: istatus

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  IF (.NOT. filter_allocated) CALL filtpara_init(nscalarq,istatus)

  CALL setmpindices(modelopt,nx,ny,nz,istatus)

  fradius_u=1.0; fradius_v=1.0;  fradius_w=1.0;  fradius_p=1.0;
  fradius_pt=1.0;fradius_qv=1.0;

  fradius_qscalar(:)=1.0;

  CALL initfilt_sub(stat_iso,1,hradius_3d,vradius,fradius_u,nx,ny,nz,nscalarq,gdu_err)

  CALL initfilt_sub(stat_iso,2,hradius_3d,vradius,fradius_v,nx,ny,nz,nscalarq,gdv_err)

  CALL initfilt_sub(stat_iso,3,hradius_3d,vradius,fradius_p,nx,ny,nz,nscalarq,gdp_err)

  tfilter = pfilter
  qvfilter= pfilter

  CALL initfilt_sub(stat_iso,6,hradius_3d,vradius,fradius_w,nx,ny,nz,nscalarq,gdw_err)

  !call initfilt_sub(stat_iso,2,hradius,vradius,fradius_v,nx,ny,nz,gdv_err)
  !call initfilt_sub(stat_iso,3,hradius,vradius,fradius_p,nx,ny,nz,gdp_err)
  !call initfilt_sub(stat_iso,4,hradius,vradius,fradius_pt,nx,ny,nz,gdt_err)
  !call initfilt_sub(stat_iso,5,hradius,vradius,fradius_qv,nx,ny,nz,gdq_err)
  !call initfilt_sub(stat_iso,6,hradius,vradius,fradius_w,nx,ny,nz,gdw_err)

  IF (ref_opt == 1 .OR. hydro_opt==1) THEN
    !qcfilter=ufilter
    CALL initfilt_sub(stat_iso,7,hradius_3d_ref,vradius_ref,            &
                      fradius_qscalar(1),nx,ny,nz,nscalarq,             &
                      gdqscalar_err(:,:,:,1))
    DO nq = 1, nscalarq
      qscalarfilter(:,nq)=qscalarfilter(:,1)
    END DO
    !call initfilt_sub(stat_iso,8,hradius,vradius,fradius_qr,nx,ny,nz,gdqr_err)
    !call initfilt_sub(stat_iso,9,hradius,vradius,fradius_qi,nx,ny,nz,gdqi_err)
    !call initfilt_sub(stat_iso,10,hradius,vradius,fradius_qs,nx,ny,nz,gdqs_err)
    !call initfilt_sub(stat_iso,11,hradius,vradius,fradius_qh,nx,ny,nz,gdqh_err)
  END IF

  RETURN
END SUBROUTINE initfilter

!#######################################################################

SUBROUTINE initfilter2(modelopt,stat_iso, hradius_3d_ens, vradius_ens, nx,ny,nz,nscalarq)
  USE filtpara
  USE mpimod
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: modelopt
  INTEGER :: stat_iso,nx,ny,nz,nscalarq
  REAL    :: vradius_ens(nx,ny,nz)
  REAL    :: hradius_3d_ens(nx,ny,nz)
  INTEGER :: iVar  !iVar=1 for u, 2 for v, 3 for p, 4 for pt, 5 for qv, 6 for w

!-----------------------------------------------------------------------

  INTEGER :: istatus
  REAL, ALLOCATABLE :: bkg_err(:,:,:)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  IF (.NOT. filter_allocated) CALL filtpara_init(nscalarq,istatus)

  ALLOCATE(bkg_err(nx,ny,nz), STAT = istatus)
  bkg_err = 1.0

  CALL setmpindices(modelopt,nx,ny,nz,istatus)

  iVar=7+nscalarq
  CALL initfilt_sub(stat_iso,iVar,hradius_3d_ens,vradius_ens,           &
                    1.0,nx,ny,nz,nscalarq,bkg_err  )

  DEALLOCATE(bkg_err)

  RETURN
END SUBROUTINE initfilter2
!
!
!#######################################################################
!
! SUBROUTINE initfilt_sub
!
!#######################################################################

SUBROUTINE initfilt_sub(stat_iso,iVar,rlenxy,rlenz,rlenf,nx,ny,nz,nscalarq,bkg_err)
!
  USE FILTPARA
  USE mpimod
  USE KINDS
  USE raflib, only: filter_cons,raf4_ad,init_raf4,raf4,raf_sm4_ad,raf_sm4

  IMPLICIT NONE

  INTEGER :: nx,ny,nz,nscalarq
  INTEGER :: iVar  !iVar=1 for u, 2 for v, 3 for p, 4 for pt, 5 for qv, 6 for w
  REAL    :: bkg_err(nx,ny,nz)
  REAL    :: rlenxy(nx,ny,nz),rlenf
  REAL    :: rlenz(nx,ny,nz)

  INTEGER :: stat_iso,stat_update

  LOGICAL :: triad4
  INTEGER :: nvars
  INTEGER :: npass,ifilt_ord,ngauss
  INTEGER :: normal
  LOGICAL :: binom
  INTEGER :: nsmooth, nsmooth_shapiro
  INTEGER, ALLOCATABLE :: idvar(:),kvar_start(:),kvar_end(:)
  CHARACTER(len=80),      ALLOCATABLE :: var_names(:)
  REAL(R_SINGLE),         ALLOCATABLE :: aspect(:,:,:,:)
  REAL(R_SINGLE),         ALLOCATABLE :: work0(:,:,:,:)
  REAL,                   ALLOCATABLE :: flow(:,:,:)
  REAL(r_double)                      :: rgauss(10)

  INTEGER        :: i,ierror,igauss,j,k,nq
  INTEGER        :: il, jl, kl     ! local index for i,j,k
  INTEGER        :: itest,jtest,npts,isave,niter
  REAL(r_single) :: testpoint
  REAL(r_single) :: a1,a2,a3,a4,a5,a6,biga1,biga2,biga3,biga4,biga5,biga6,det,gradfmax
  REAL(r_single) :: dxi,dyi,fx,fy,fz,dzi
  INTEGER        :: im,ip,jm,jp,kp,km
  INTEGER        :: iend, jend, kend
  INTEGER        :: istatus

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  triad4=.true.                     !  if true, use 4-color smoothing for 2 dimensional filters
                                    !  if false, revert to older 3-color smoothing in 2 dimensions
  npass=1
  ifilt_ord=4
  normal=-200                        !  number of stocastic samples used for normalization
  binom=.true.                      !  if true, and npass >1, produces nicer results in anisotropic case
  ngauss=3                          !  extra feature needed in GSI--does nothing here !pause
  rgauss=0._r_double                !  extra feature
  rgauss(1)=0.5_r_double
  rgauss(2)=1._r_double
  rgauss(3)=2._r_double
  nsmooth=0; nsmooth_shapiro=0
  !!rlenxy=03._r_single             !  isotropic correlation length for example 1

! for now just run with one pe, so no subdomains
!ids=1   ; ide=nx  ; jds=1   ; jde=ny  ; kds=1   ; kde=nz
!ips=ids ; ipe=ide ; jps=jds ; jpe=jde ; kps=kds ; kpe=kde
!mype=0  ; npes=1
!       extra feature for GSI--can do multiple 2 and/or 3d variables simultaneously
!         not used here
  nvars=7+nscalarq
  ALLOCATE(kvar_start(nvars),kvar_end(nvars),var_names(nvars))
  kvar_start(1) =kds ; kvar_end(1) =kde  ; var_names(1) ='u'
  kvar_start(2) =kds ; kvar_end(2) =kde  ; var_names(2) ='v'
  kvar_start(3) =kds ; kvar_end(3) =kde  ; var_names(3) ='p'
  kvar_start(4) =kds ; kvar_end(4) =kde  ; var_names(4) ='pt'
  kvar_start(5) =kds ; kvar_end(5) =kde  ; var_names(5) ='qv'
  kvar_start(6) =kds ; kvar_end(6) =kde+1; var_names(6) ='w'
  DO nq = 1, nscalarq
    kvar_start(6+nq) =kds
    kvar_end(6+nq)   =kde
    WRITE(var_names(6+nq),'(a,I0)') 'qhymeter-',nq
  END DO
  kvar_start(6+nscalarq+1) =kds ; kvar_end(6+nscalarq+1) =kde  ; var_names(6+nscalarq+1) ='ens_member'

  select case (iVar)
  case(1)  !u
    iend = iupe ;  jend = jpe  ;  kend = kpe    ! U stagger
  case(2)  !v
    iend = ipe  ;  jend = jvpe ;  kend = kpe    ! V stagger
  case(6)  !w
    iend = ipe  ;  jend = jpe  ;  kend = kwpe   ! W stagger
  case(3:5,7:20) !pt
    iend = ipe  ;  jend = jpe  ;  kend = kpe   ! W stagger
  CASE DEFAULT
    PRINT *,'not recognized variable, ivar = ',ivar
    CALL arpsstop('Unrecognized variable id in initfilt_sub',1)
  END SELECT

  ALLOCATE(idvar (kps:kend),   STAT = istatus )
  idvar(:) = 1
  !
  !  1.  create isotropic filter
  !                                    layout of 3-d aspect tensor elements
  !                                     x  y  z
  !                               x     1  6  5
  !                               y     6  2  4
  !                               z     5  4  3
  !    for 2 d case, elements 4 and 5 are zero, and element 3 is set to 1.
  !     extra space aspect(7,.,.) is a place holder for work space.

  ALLOCATE(aspect(1:7,     ips:iend,  jps:jend,  kps:kend)  )

  if(stat_iso==1)then                 ! for isotopic recursive filter
      do k=kps,kend
        kl = k
        do j=jps,jend
          jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
          do i=ips,iend
            il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
            aspect(1,i,j,k)=rlenxy(il,jl,kl)**2
            aspect(2,i,j,k)=rlenxy(il,jl,kl)**2
            aspect(3,i,j,k)=rlenz(il,jl,kl) **2      !rlenxy**2
            aspect(4,i,j,k)=0.
            aspect(5,i,j,k)=0.
            aspect(6,i,j,k)=0.
            aspect(7,i,j,k)=0.
          end do
        end do
      end do

  else                                    ! For anisotropic filter

    ALLOCATE(work0(1:ngauss,ips:iend,  jps:jend,  kps:kend)  )
    ALLOCATE(flow (         ips:iend,  jps:jend,  kps:kend)  )

    !user specified or updated, determined by the caller function
    ! we can use increment (x_ctr) as updated bkg error field
    DO k = kps,kend
      kl = k
      DO j = jps, jend
        jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
        DO i = ips, iend
          il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
          flow(i,j,k) = bkg_err(il,jl,kl)
        END DO
      END DO
    END DO
    do i=1,ngauss
      work0(i,:,:,:)=flow(:,:,:)
    end do

    gradfmax=0.
    do k=kps,kend
      kl = k
      kp=min(k+1,kend)
      km=max(k-1,kps)
      dzi=1./max(1,kp-km)
      do j=jps,jend
        jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
        jp=min(j+1,jend)
        jm=max(j-1,jps)
        dyi=1./max(1,jp-jm)
        do i=ips,iend
           il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
           ip=min(i+1,iend)
           im=max(i-1,ips)
           dxi=1./max(1,ip-im)
           fx=dxi*(work0(1,ip,j,k)-work0(1,im,j,k))
           fy=dyi*(work0(1,i,jp,k)-work0(1,i,jm,k))
           fz=dzi*(work0(1,i,j,kp)-work0(1,i,j,km))
           gradfmax=max(abs(fx),abs(fy),abs(fz),gradfmax)
  !this is S**(-1) for anisotropic example 2
            a1=(1./rlenxy(il,jl,kl)**2) + ( fx*fx/(rlenf**2) )
            a2=(1./rlenxy(il,jl,kl)**2) + ( fy*fy/(rlenf**2) )
            a3=(1./rlenz(il,jl,kl)**2)  + ( fz*fz/(rlenf**2) )
            a4=                 ( fz*fy/(rlenf**2) )
            a5=                 ( fz*fx/(rlenf**2) )
            a6=                 ( fx*fy/(rlenf**2) )
  !following is inversion of S**(-1) to get S
            biga1=a2*a3-a4*a4
            biga2=a1*a3-a5*a5
            biga3=a1*a2-a6*a6
            biga4=a5*a6-a1*a4
            biga5=a4*a6-a2*a5
            biga6=a4*a5-a3*a6
            det=a1*biga1+a6*biga6+a5*biga5
            aspect(1,i,j,k)=biga1/det
            aspect(2,i,j,k)=biga2/det
            aspect(3,i,j,k)=biga3/det
            aspect(4,i,j,k)=biga4/det
            aspect(5,i,j,k)=biga5/det
            aspect(6,i,j,k)=biga6/det
            aspect(7,i,j,k)=0.
        end do
      end do
    end do

    DEALLOCATE( flow  )
    DEALLOCATE( work0 )

  end if !stat_iso==?

  aspect=aspect/2

  IF (iVar <= 6) THEN
    select case (iVar)
    case(1)  !u
      !idvar(:) = 1
      call init_raf4(aspect,triad4,ngauss,rgauss,npass,                    &
                  normal,binom,ifilt_ord,ufilter,                          &
                  nsmooth,nsmooth_shapiro,                                 &
                  1,idvar,kvar_start(ivar),kvar_end(ivar),var_names(ivar), &
                  ids, ide+1, jds, jde, kds, kde,            & ! domain indices
                  ips, iupe,  jps, jpe, kps, kpe,            & ! patch indices
                  mype, npes)
    case(2)  !v
      !idvar(:) = 2
      call init_raf4(aspect,triad4,ngauss,rgauss,npass,                    &
                  normal,binom,ifilt_ord,vfilter,                          &
                  nsmooth,nsmooth_shapiro,                                 &
                  1,idvar,kvar_start(ivar),kvar_end(ivar),var_names(ivar), &
                  ids, ide, jds, jde+1, kds, kde,            & ! domain indices
                  ips, ipe, jps, jvpe,  kps, kpe,            & ! patch indices
                  mype, npes)
    case(3)  !p
      !idvar(:) = 4
      call init_raf4(aspect,triad4,ngauss,rgauss,npass,                    &
                  normal,binom,ifilt_ord,pfilter,                          &
                  nsmooth,nsmooth_shapiro,                                 &
                  1,idvar,kvar_start(ivar),kvar_end(ivar),var_names(ivar), &
                  ids, ide, jds, jde, kds, kde,              & ! domain indices
                  ips, ipe, jps, jpe, kps, kpe,              & ! patch indices
                  mype, npes)
    case(4) !pt
      !idvar(:) = 4
      call init_raf4(aspect,triad4,ngauss,rgauss,npass,                    &
                  normal,binom,ifilt_ord,tfilter,                          &
                  nsmooth,nsmooth_shapiro,                                 &
                  1,idvar,kvar_start(ivar),kvar_end(ivar),var_names(ivar), &
                  ids, ide, jds, jde, kds, kde,              & ! domain indices
                  ips, ipe, jps, jpe, kps, kpe,              & ! patch indices
                  mype, npes)
    case(5)  !qv
      !idvar(:) = 4
      call init_raf4(aspect,triad4,ngauss,rgauss,npass,                    &
                  normal,binom,ifilt_ord,qvfilter,                         &
                  nsmooth,nsmooth_shapiro,                                 &
                  1,idvar,kvar_start(ivar),kvar_end(ivar),var_names(ivar), &
                  ids, ide, jds, jde, kds, kde,              & ! domain indices
                  ips, ipe, jps, jpe, kps, kpe,              & ! patch indices
                  mype, npes)
    case(6)  !w
      !idvar(:) = 3
      call init_raf4(aspect,triad4,ngauss,rgauss,npass,                    &
                  normal,binom,ifilt_ord,wfilter,                          &
                  nsmooth,nsmooth_shapiro,                                 &
                  1,idvar,kvar_start(ivar),kvar_end(ivar),var_names(ivar), &
                  ids, ide, jds, jde, kds, kde+1,             & ! domain indices
                  ips, ipe, jps, jpe, kps, kwpe,              & ! patch indices
                  mype, npes)
    END SELECT
  ELSE IF (iVar > 6+nscalarq) THEN !en_member
    !idvar(:) = 4
    CALL init_raf4(aspect,triad4,ngauss,rgauss,npass,                    &
                normal,binom,ifilt_ord,enfilter,                         &
                nsmooth,nsmooth_shapiro,                                 &
                1,idvar,kvar_start(ivar),kvar_end(ivar),var_names(ivar), &
                ids, ide, jds, jde, kds, kde,              & ! domain indices
                ips, ipe, jps, jpe, kps, kpe,              & ! patch indices
                mype, npes)
  ELSE
    nq = iVar -6
    CALL init_raf4(aspect,triad4,ngauss,rgauss,npass,                    &
                normal,binom,ifilt_ord,qscalarfilter(:,nq),              &
                nsmooth,nsmooth_shapiro,                                 &
                1,idvar,kvar_start(ivar),kvar_end(ivar),var_names(ivar), &
                ids, ide, jds, jde, kds, kde,              & ! domain indices
                ips, ipe, jps, jpe, kps, kpe,              & ! patch indices
                mype, npes)

  END IF

  DEALLOCATE( aspect )
  DEALLOCATE( kvar_start )
  DEALLOCATE( kvar_end   )
  DEALLOCATE( idvar )

  RETURN
END SUBROUTINE initfilt_sub

!
!#######################################################################
!
!subroutine forwardfilter
!
!#######################################################################
!
SUBROUTINE forwardfilter(nx,ny,nz,nscalarq,psi,phi,p_ctr,t_ctr,q_ctr,w_ctr,    &
                         qscalar_ctr,   &
                         gdu_err,gdv_err,gdp_err,gdt_err,gdq_err,gdw_err,      &
                         gdqscalar_err, tem1)
!
!NOTE:  THIS CODE LOOKS LIKE AN MPI CODE, BUT ONLY WORKS CORRECTLY WITH 1 PROCESSOR
!         (raflib.f90 is a correctly functioning mpi code however)

!  run 2-d tests to explore limits to allowable variability of rf, with old smoothing, and
!    with Jim's 4-color triad blending.
  USE mpimod
  USE filtpara
  USE kinds
  USE raflib, only: filter_cons,raf4_ad,raf4,raf_sm4_ad,raf_sm4

  IMPLICIT NONE

  INTEGER, INTENT(IN)    :: nx,ny,nz,nscalarq
  REAL,    INTENT(INOUT) :: psi(nx,ny,nz),phi(nx,ny,nz),w_ctr(nx,ny,nz)
  REAL,    INTENT(INOUT) :: p_ctr(nx,ny,nz),t_ctr(nx,ny,nz),q_ctr(nx,ny,nz)
  REAL,    INTENT(INOUT) :: qscalar_ctr(nx,ny,nz,nscalarq)
  REAL,    INTENT(IN)    :: gdu_err(nx,ny,nz),gdv_err(nx,ny,nz),gdp_err(nx,ny,nz)
  REAL,    INTENT(IN)    :: gdt_err(nx,ny,nz), gdq_err(nx,ny,nz), gdw_err(nx,ny,nz)

  REAL,    INTENT(IN)    :: gdqscalar_err(nx,ny,nz,nscalarq)

  REAL,    INTENT(INOUT) :: tem1(nx,ny,nz)

!-----------------------------------------------------------------------

  INTEGER    :: ngauss

  INTEGER    :: istatus
  INTEGER    :: i,j,k,nq
  INTEGER    :: il,jl,kl

  REAL(r_single), ALLOCATABLE :: work(:,:,:,:)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  ngauss=1                              !  extra feature needed in GSI--does nothing here

  ! for now just run with one pe, so no subdomains
  !ids=1   ; ide=nx  ; jds=1   ; jde=ny  ; kds=1   ; kde=nz
  !ips=ids ; ipe=ide ; jps=jds ; jpe=jde ; kps=kds ; kpe=kde
  !mype=0  ; npes=1
  !              extra feature for GSI--can do multiple 2 and/or 3d variables simultaneously
  !                    not used here
  !nvars=1

  ALLOCATE(work(ngauss,ips:iupe,jps:jpe,kps:kpe), STAT = istatus)

  !
  ! PSI, U stagger
  !
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, iupe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = psi(il,jl,kl)
      END DO
    END DO
  END DO

  CALL raf4(work,ufilter,ngauss,                &
                ids, ide+1, jds, jde,           &                       ! domain indices
                ips, iupe, jps, jpe, kps, kpe,  &                       ! patch indices
                npes)

  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, iupe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        psi(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(psi, nx, ny, nz, 0, 0, 1, tem1)
    CALL mpsendrecv2dew(psi, nx, ny, nz, 0, 0, 0, tem1)
    CALL mpsendrecv2dns(psi, nx, ny, nz, 0, 0, 1, tem1)
  END IF

  DEALLOCATE( work )

  !
  ! PHI, V stagger
  !
  ALLOCATE(work(ngauss,ips:ipe,jps:jvpe,kps:kpe), STAT = istatus)

  DO k = kps, kpe
    kl = k
    DO j = jps, jvpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = phi(il,jl,kl)
      END DO
    END DO
  END DO

  CALL raf4(work,vfilter,ngauss,                &
                ids, ide, jds, jde+1,           &                       ! domain indices
                ips, ipe, jps, jvpe, kps, kpe,  &                       ! patch indices
                npes)

  DO k = kps, kpe
    kl = k
    DO j = jps, jvpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        phi(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(phi, nx, ny, nz, 0, 0, 2, tem1)
    CALL mpsendrecv2dns(phi, nx, ny, nz, 0, 0, 2, tem1)
    CALL mpsendrecv2dns(phi, nx, ny, nz, 0, 0, 0, tem1)
  END IF

  DEALLOCATE( work )

  !
  ! P_CTR, M stagger
  !
  ALLOCATE(work(ngauss,ips:ipe,jps:jpe,kps:kpe), STAT = istatus)

  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = p_ctr(il,jl,kl)
      END DO
    END DO
  END DO
  CALL raf4(work,pfilter,ngauss,               &
                 ids, ide, jds, jde,           &                          ! domain indices
                 ips, ipe, jps, jpe, kps, kpe, &                          ! patch indices
                 npes)
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        p_ctr(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(p_ctr, nx, ny, nz, 0, 0, 0, tem1)
    CALL mpsendrecv2dns(p_ctr, nx, ny, nz, 0, 0, 0, tem1)
  END IF

  !
  ! T_CTR, M stagger
  !
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = t_ctr(il,jl,kl)
      END DO
    END DO
  END DO

  CALL raf4(work,tfilter,ngauss,               &
                 ids, ide, jds, jde,           &                          ! domain indices
                 ips, ipe, jps, jpe, kps, kpe, &                          ! patch indices
                 npes)
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        t_ctr(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(t_ctr, nx, ny, nz, 0, 0, 0, tem1)
    CALL mpsendrecv2dns(t_ctr, nx, ny, nz, 0, 0, 0, tem1)
  END IF

  !
  ! Q_CTR, M stagger
  !
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = q_ctr(il,jl,kl)
      END DO
    END DO
  END DO

  CALL raf4(work,qvfilter,ngauss,             &
                ids, ide, jds, jde,           &                         ! domain indices
                ips, ipe, jps, jpe, kps, kpe, &                         ! patch indices
                npes)
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        q_ctr(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(q_ctr, nx, ny, nz, 0, 0, 0, tem1)
    CALL mpsendrecv2dns(q_ctr, nx, ny, nz, 0, 0, 0, tem1)
  END IF

  DEALLOCATE( work )
  !
  ! W_CTR, W stagger
  !
  ALLOCATE(work(ngauss,ips:ipe,jps:jpe,kps:kwpe), STAT = istatus)

  DO k = kps, kwpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = w_ctr(il,jl,kl)
      END DO
    END DO
  END DO

  CALL raf4(work,wfilter,ngauss,               &
                ids, ide, jds, jde,            &                        ! domain indices
                ips, ipe, jps, jpe, kps, kwpe, &                        ! patch indices
                npes)

  DO k = kps, kwpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        w_ctr(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(w_ctr, nx, ny, nz, 0, 0, 3, tem1)
    CALL mpsendrecv2dns(w_ctr, nx, ny, nz, 0, 0, 3, tem1)
  END IF

  DEALLOCATE( work )

  !
  ! QC_CTR, M stagger
  !
  ALLOCATE(work(ngauss,ips:ipe,jps:jpe,kps:kpe), STAT = istatus)

  DO nq = 1, nscalarq

    DO k = kps, kpe
      kl = k
      DO j = jps, jpe
        jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
        DO i = ips, ipe
          il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
          work(1,i,j,k) = qscalar_ctr(il,jl,kl,nq)
        END DO
      END DO
    END DO

    CALL raf4(work,qscalarfilter(:,nq),ngauss,             &
                  ids, ide, jds, jde,           &                          ! domain indices
                  ips, ipe, jps, jpe, kps, kpe, &                          ! patch indices
                  npes)

    DO k = kps, kpe
      kl = k
      DO j = jps, jpe
        jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
        DO i = ips, ipe
          il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
          qscalar_ctr(il,jl,kl,nq) = work(1,i,j,k)
        END DO
      END DO
    END DO

    IF (npes > 1) THEN
      CALL mpsendrecv2dew(qscalar_ctr(:,:,:,nq), nx, ny, nz, 0, 0, 0, tem1)
      CALL mpsendrecv2dns(qscalar_ctr(:,:,:,nq), nx, ny, nz, 0, 0, 0, tem1)
    END IF

  END DO

  DEALLOCATE( work )

  psi=psi*gdu_err
  phi=phi*gdv_err
  w_ctr=w_ctr*gdw_err
  p_ctr=p_ctr*gdp_err
  t_ctr=t_ctr*gdt_err
  q_ctr=q_ctr*gdq_err

  DO nq = 1, nscalarq
    qscalar_ctr(:,:,:,nq)=qscalar_ctr(:,:,:,nq)*gdqscalar_err(:,:,:,nq)
  END DO

  RETURN
END SUBROUTINE forwardfilter
!
!
!#######################################################################
!
! SUBROUTINE adjointfilter
!
!#######################################################################
!
SUBROUTINE adjointfilter(nx,ny,nz,nscalarq,psi,phi,p_ctr,t_ctr,q_ctr,w_ctr,  &
                         qscalar_ctr,                                        &
                         gdu_err,gdv_err,gdp_err,gdt_err,gdq_err,gdw_err,    &
                         gdqscalar_err, tem1)
!
!NOTE:  THIS CODE LOOKS LIKE AN MPI CODE, BUT ONLY WORKS CORRECTLY WITH 1 PROCESSOR
!         (raflib.f90 is a correctly functioning mpi code however)

!  run 2-d tests to explore limits to allowable variability of rf, with old smoothing, and
!    with Jim's 4-color triad blending.
  USE mpimod
  USE filtpara
  USE kinds
  USE raflib, only: filter_cons,raf4_ad,raf4,raf_sm4_ad,raf_sm4
  IMPLICIT NONE

  INTEGER, INTENT(IN)    :: nx,ny,nz,nscalarq
  REAL,    INTENT(INOUT) :: psi(nx,ny,nz),phi(nx,ny,nz),w_ctr(nx,ny,nz)
  REAL,    INTENT(INOUT) :: p_ctr(nx,ny,nz),t_ctr(nx,ny,nz),q_ctr(nx,ny,nz)
  REAL,    INTENT(INOUT) :: qscalar_ctr(nx,ny,nz,nscalarq)
  REAL,    INTENT(IN)    :: gdu_err(nx,ny,nz),gdv_err(nx,ny,nz),gdp_err(nx,ny,nz)
  REAL,    INTENT(IN)    :: gdt_err(nx,ny,nz),gdq_err(nx,ny,nz),gdw_err(nx,ny,nz)
  REAL,    INTENT(IN)    :: gdqscalar_err(nx,ny,nz,nscalarq)

  REAL, INTENT(INOUT) :: tem1(nx,ny,nz)

!-----------------------------------------------------------------------

  INTEGER    :: ngauss

  INTEGER    :: istatus
  INTEGER    :: i,j,k,nq
  INTEGER    :: il,jl,kl

  REAL(r_single), ALLOCATABLE :: work(:,:,:,:)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  ngauss=1                              !  extra feature needed in GSI--does nothing here

  ! for now just run with one pe, so no subdomains
  !ids=1   ; ide=nx  ; jds=1   ; jde=ny  ; kds=1   ; kde=nz
  !ips=ids ; ipe=ide ; jps=jds ; jpe=jde ; kps=kds ; kpe=kde
  !mype=0  ; npes=1
  !              extra feature for GSI--can do multiple 2 and/or 3d variables simultaneously
  !                    not used here
  !nvars=1
  !
  psi=psi*gdu_err
  phi=phi*gdv_err
  w_ctr=w_ctr*gdw_err
  p_ctr=p_ctr*gdp_err
  t_ctr=t_ctr*gdt_err
  q_ctr=q_ctr*gdq_err

  DO nq = 1, nscalarq
    qscalar_ctr(:,:,:,nq)=qscalar_ctr(:,:,:,nq)*gdqscalar_err(:,:,:,nq)
  END DO

  ALLOCATE(work(ngauss,ips:iupe,jps:jpe,kps:kpe), STAT = istatus)

  !
  ! PSI, U stagger
  !
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, iupe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = psi(il,jl,kl)
      END DO
    END DO
  END DO

  call raf4_ad(work,ufilter,ngauss,           &
              ids, ide+1, jds, jde,           &                         ! domain indices
              ips, iupe,  jps, jpe, kps, kpe, &                         ! patch indices
                    npes)
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, iupe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        psi(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(psi, nx, ny, nz, 0, 0, 1, tem1)
    CALL mpsendrecv2dew(psi, nx, ny, nz, 0, 0, 0, tem1)
    CALL mpsendrecv2dns(psi, nx, ny, nz, 0, 0, 1, tem1)
  END IF

  DEALLOCATE( work )
  !
  ! PHI, V stagger
  !
  ALLOCATE(work(ngauss,ips:ipe,jps:jvpe,kps:kpe), STAT = istatus)

  DO k = kps, kpe
    kl = k
    DO j = jps, jvpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = phi(il,jl,kl)
      END DO
    END DO
  END DO

  CALL raf4_ad(work,vfilter,ngauss,           &
              ids, ide, jds, jde+1,           &                         ! domain indices
              ips, ipe, jps, jvpe, kps, kpe,  &                         ! patch indices
               npes)
  DO k = kps, kpe
    kl = k
    DO j = jps, jvpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        phi(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(phi, nx, ny, nz, 0, 0, 2, tem1)
    CALL mpsendrecv2dns(phi, nx, ny, nz, 0, 0, 2, tem1)
    CALL mpsendrecv2dns(phi, nx, ny, nz, 0, 0, 0, tem1)
  END IF

  DEALLOCATE( work )

  ALLOCATE(work(ngauss,ips:ipe,jps:jpe,kps:kpe), STAT = istatus)

  !
  ! P_CTR, M stagger
  !
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = p_ctr(il,jl,kl)
      END DO
    END DO
  END DO

  CALL raf4_ad(work,pfilter,ngauss,          &
               ids, ide, jds, jde,           &                          ! domain indices
               ips, ipe, jps, jpe, kps, kpe, &                          ! patch indices
               npes)

  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        p_ctr(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(p_ctr, nx, ny, nz, 0, 0, 0, tem1)
    CALL mpsendrecv2dns(p_ctr, nx, ny, nz, 0, 0, 0, tem1)
  END IF

  !
  ! T_CTR, M stagger
  !
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = t_ctr(il,jl,kl)
      END DO
    END DO
  END DO

  CALL raf4_ad(work,tfilter,ngauss,          &
               ids, ide, jds, jde,           &                          ! domain indices
               ips, ipe, jps, jpe, kps, kpe, &                          ! patch indices
               npes)

  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        t_ctr(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(t_ctr, nx, ny, nz, 0, 0, 0, tem1)
    CALL mpsendrecv2dns(t_ctr, nx, ny, nz, 0, 0, 0, tem1)
  END IF

  !
  ! Q_CTR, M stagger
  !
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = q_ctr(il,jl,kl)
      END DO
    END DO
  END DO

  CALL raf4_ad(work,qvfilter,ngauss,         &
               ids, ide, jds, jde,           &                          ! domain indices
               ips, ipe, jps, jpe, kps, kpe, &                          ! patch indices
               npes)
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        q_ctr(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(q_ctr, nx, ny, nz, 0, 0, 0, tem1)
    CALL mpsendrecv2dns(q_ctr, nx, ny, nz, 0, 0, 0, tem1)
  END IF

  DEALLOCATE( work )

  !
  ! W_CTR, W stagger
  !
  ALLOCATE(work(ngauss,ips:ipe,jps:jpe,kps:kwpe), STAT = istatus)

  DO k = kps, kwpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = w_ctr(il,jl,kl)
      END DO
    END DO
  END DO

  CALL raf4_ad(work,wfilter,ngauss,          &
              ids, ide, jds, jde,            &                          ! domain indices
              ips, ipe, jps, jpe, kps, kwpe, &                          ! patch indices
              npes)

  DO k = kps, kwpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        w_ctr(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(w_ctr, nx, ny, nz, 0, 0, 3, tem1)
    CALL mpsendrecv2dns(w_ctr, nx, ny, nz, 0, 0, 3, tem1)
  END IF

  DEALLOCATE( work )

  ALLOCATE(work(ngauss,ips:ipe,jps:jpe,kps:kpe), STAT = istatus)

  !
  ! QSCALAR_CTR, M stagger
  !
  DO nq = 1,nscalarq

    DO k = kps, kpe
      kl = k
      DO j = jps, jpe
        jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
        DO i = ips, ipe
          il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
          work(1,i,j,k) = qscalar_ctr(il,jl,kl,nq)
        END DO
      END DO
    END DO

    CALL raf4_ad(work,qscalarfilter(:,nq),ngauss,        &
                ids, ide, jds, jde,           &                          ! domain indices
                ips, ipe, jps, jpe, kps, kpe, &                          ! patch indices
                npes)
    DO k = kps, kpe
      kl = k
      DO j = jps, jpe
        jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
        DO i = ips, ipe
          il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
          qscalar_ctr(il,jl,kl,nq) = work(1,i,j,k)
        END DO
      END DO
    END DO

    IF (npes > 1) THEN
      CALL mpsendrecv2dew(qscalar_ctr(:,:,:,nq), nx, ny, nz, 0, 0, 0, tem1)
      CALL mpsendrecv2dns(qscalar_ctr(:,:,:,nq), nx, ny, nz, 0, 0, 0, tem1)
    END IF

  END DO

  DEALLOCATE( work )

  RETURN
END SUBROUTINE adjointfilter
!
!
!#######################################################################
!
! subroutine forwardfilter2
!
!#######################################################################
!
SUBROUTINE forwardfilter2(nx,ny,nz,gvar,tem1)
!
!NOTE:  THIS CODE LOOKS LIKE AN MPI CODE, BUT ONLY WORKS CORRECTLY WITH 1 PROCESSOR
!         (raflib.f90 is a correctly functioning mpi code however)

!  run 2-d tests to explore limits to allowable variability of rf, with old smoothing, and
!    with Jim's 4-color triad blending.
  USE mpimod
  USE filtpara
  USE kinds
  USE raflib, only : filter_cons,raf4_ad,raf4,raf_sm4_ad,raf_sm4

  IMPLICIT NONE

  INTEGER, INTENT(IN)    :: nx,ny,nz
  REAL,    INTENT(INOUT) :: gvar(nx,ny,nz)
  REAL,    INTENT(INOUT) :: tem1(nx,ny,nz)

!-----------------------------------------------------------------------

  INTEGER    :: istatus
  INTEGER    :: i,j,k
  INTEGER    :: il,jl,kl

  INTEGER    :: ngauss
  REAL(r_single), ALLOCATABLE :: work(:,:,:,:)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  ngauss=1                              !  extra feature needed in GSI--does nothing here

  ! for now just run with one pe, so no subdomains
  !ids=1   ; ide=nx  ; jds=1   ; jde=ny  ; kds=1   ; kde=nz
  !ips=ids ; ipe=ide ; jps=jds ; jpe=jde ; kps=kds ; kpe=kde
  !mype=0  ; npes=1
  !              extra feature for GSI--can do multiple 2 and/or 3d variables simultaneously
  !                    not used here

  ALLOCATE(work(ngauss,ips:ipe,jps:jpe,kps:kpe), STAT = istatus)

  !
  ! GVAR, M stagger
  !
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = gvar(il,jl,kl)
      END DO
    END DO
  END DO

  CALL raf4(work,enfilter,ngauss,             &
              ids, ide, jds, jde,             &                     ! domain indices
              ips, ipe, jps, jpe, kps, kpe,   &                     ! patch indices
              npes)
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        gvar(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(gvar, nx, ny, nz, 0, 0, 0, tem1)
    CALL mpsendrecv2dns(gvar, nx, ny, nz, 0, 0, 0, tem1)
  END IF

  DEALLOCATE( work )

  RETURN
END SUBROUTINE forwardfilter2
!
!
!#######################################################################
!
! subroutine adjointfilter2
!
!#######################################################################
!
SUBROUTINE adjointfilter2(nx,ny,nz,gvar,tem1)
!
!NOTE:  THIS CODE LOOKS LIKE AN MPI CODE, BUT ONLY WORKS CORRECTLY WITH 1 PROCESSOR
!         (raflib.f90 is a correctly functioning mpi code however)
!
!  run 2-d tests to explore limits to allowable variability of rf, with old smoothing, and
!    with Jim's 4-color triad blending.
  USE mpimod
  USE filtpara
  USE kinds
  USE raflib, only: filter_cons,raf4_ad,raf4,raf_sm4_ad,raf_sm4

  IMPLICIT NONE

  INTEGER, INTENT(IN)    :: nx,ny,nz
  REAL,    INTENT(INOUT) :: gvar(nx,ny,nz)
  REAL,    INTENT(INOUT) :: tem1(nx,ny,nz)

!-----------------------------------------------------------------------

  INTEGER    :: istatus
  INTEGER    :: i,j,k
  INTEGER    :: il,jl,kl

  INTEGER    :: ngauss
  REAL(r_single), ALLOCATABLE :: work(:,:,:,:)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  ngauss=1                              !  extra feature needed in GSI--does nothing here

  ! for now just run with one pe, so no subdomains
  !ids=1   ; ide=nx  ; jds=1   ; jde=ny  ; kds=1   ; kde=nz
  !ips=ids ; ipe=ide ; jps=jds ; jpe=jde ; kps=kds ; kpe=kde
  !mype=0  ; npes=1
  !              extra feature for GSI--can do multiple 2 and/or 3d variables simultaneously
  !                    not used here

  ALLOCATE(work(ngauss,ips:ipe,jps:jpe,kps:kpe), STAT = istatus)

  !
  ! GVAR, M stagger
  !
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        work(1,i,j,k) = gvar(il,jl,kl)
      END DO
    END DO
  END DO

  CALL raf4_ad(work,enfilter,ngauss,        &
              ids, ide, jds, jde,           &                          ! domain indices
              ips, ipe, jps, jpe, kps, kpe, &                          ! patch indices
              npes)
  DO k = kps, kpe
    kl = k
    DO j = jps, jpe
      jl = j - (jype-1)*(ny-3)       ! from global index j to local index jl
      DO i = ips, ipe
        il = i - (ixpe-1)*(nx-3)     ! from global index i to local index il
        gvar(il,jl,kl) = work(1,i,j,k)
      END DO
    END DO
  END DO

  IF (npes > 1) THEN
    CALL mpsendrecv2dew(gvar, nx, ny, nz, 0, 0, 0, tem1)
    CALL mpsendrecv2dns(gvar, nx, ny, nz, 0, 0, 0, tem1)
  END IF

  DEALLOCATE( work )

  RETURN
END subroutine adjointfilter2
