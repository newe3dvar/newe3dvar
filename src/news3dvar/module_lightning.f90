MODULE module_lightning

  USE model_precision

  IMPLICIT NONE
  PRIVATE

  INTEGER, PARAMETER   :: nlgt_max = 2
  INTEGER              :: nlgtfil
  CHARACTER(LEN=256)   :: lightning_files(nlgt_max)
  CHARACTER(LEN=16)    :: srclgt(nlgt_max)
  INTEGER, ALLOCATABLE :: iuselgt(:,:)
  INTEGER              :: lightning_output

  REAL, ALLOCATABLE    :: qvobs(:,:,:)
  REAL, ALLOCATABLE    :: iwpobs(:,:)
  REAL, ALLOCATABLE    :: iwp(:,:)          ! work array, use outside
  REAL, ALLOCATABLE    :: hgtlcl(:,:)       ! work array, use internal
  REAL, ALLOCATABLE    :: gridlight(:,:,:)  ! work array, use internal

  LOGICAL :: lightning_array_allocated

!-----------------------------------------------------------------------
!
! Public interface
!
!-----------------------------------------------------------------------

  PUBLIC :: nlgtfil, iuselgt
  PUBLIC :: qvobs, iwpobs, iwp
  PUBLIC :: allocate_lightning, deallocate_lightning, lightning_set_usage
  PUBLIC :: lightning_read_nml_options, lightning_costf, lightning_gradt
  PUBLIC :: read_lightning_file, compute_lightning

  CONTAINS

  SUBROUTINE lightning_read_nml_options(unum,npass,myproc,istatus)

  !#####################################################################

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: unum, myproc, npass
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    NAMELIST /var_lightning/ nlgtfil, lightning_files, srclgt, iuselgt, &
                             lightning_output

  !---------------------------------------------------------------------

    INTEGER :: i,j

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ALLOCATE(iuselgt(nlgt_max,npass), STAT = istatus)
    CALL check_alloc_status(istatus, "lgtobs:iuselgt")

    nlgtfil = 0 ! default must always be 0, then skip read file below !
    lightning_files = (/' ',' '/)
    srclgt          = (/' ',' '/)
    iuselgt(:,:)    = 0
    lightning_output = 0

    IF (myproc == 0) THEN
      READ(unum, var_lightning,  END=350)
      WRITE(*,*) 'Namelist block var_lightning sucessfully read.'

      GOTO 400

      350 CONTINUE  ! Error with namelist reading
      WRITE(*,*) 'ERROR: reading namelist block var_lightning.'
      istatus = -2
      RETURN

      400 CONTINUE  ! Successful read namelist

      WRITE(*,'(5x,a,I0,a)') 'nlgtfil  = ', nlgtfil,','
      DO i = 1, nlgtfil
        WRITE(*,'(5x,a,i0,3a)')         'lightning_files(',i,') = "', TRIM(lightning_files(i)),'",'
        WRITE(*,'(5x,a,i0,3a)')         'srclgt(',i,')          = "', TRIM(srclgt(i)),'",'
        WRITE(*,'(5x,a,i0,a,10(i2,a))') 'iuselgt(',i,',:)       = ',(iuselgt(i,j),', ',j=1,npass)
      END DO
      WRITE(*,'(5x,a,I0,a)') 'lightning_output   = ', lightning_output,','

    END IF

    CALL mpupdatei(nlgtfil, 1)
    CALL mpupdatei(iuselgt, npass*nlgt_max)

    RETURN
  END SUBROUTINE lightning_read_nml_options

  !#####################################################################
  !
  ! Allocate them
  !
  !#####################################################################

  SUBROUTINE allocate_lightning(nx,ny,nz,istatus)

    IMPLICIT NONE
    INTEGER, INTENT(IN)  :: nx,ny,nz
    INTEGER, INTENT(OUT) :: istatus

  !  print*,'allocating the light arrays',nx,ny,nz

    allocate(qvobs(nx,ny,nz), STAT = istatus)
    allocate(iwpobs(nx,ny),   STAT = istatus)
    allocate(hgtlcl(nx,ny),   STAT = istatus)
    CALL check_alloc_status(istatus, "module_lightning:hgtlcl")

    ALLOCATE(gridlight(nx,ny,nlgt_max), STAT = istatus)
    CALL check_alloc_status(istatus, "module_lightning:gridlight")
    gridlight(:,:,:) = 0

    lightning_array_allocated = .TRUE.

    RETURN
  END SUBROUTINE allocate_lightning

  !#####################################################################
  !
  ! DEALLOCATE
  !
  !#####################################################################

  SUBROUTINE deallocate_lightning(istatus)
    IMPLICIT NONE
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (lightning_array_allocated) THEN
      DEALLOCATE(qvobs,  STAT = istatus)
      DEALLOCATE(iwpobs, STAT = istatus)
      DEALLOCATE(iwp,    STAT = istatus)
    END IF

    RETURN
  END SUBROUTINE deallocate_lightning

  !#####################################################################
  !
  ! Work array management
  !
  !#####################################################################

  SUBROUTINE work_array_swap(nx,ny,istatus)

    IMPLICIT NONE
    INTEGER, INTENT(IN)  :: nx,ny
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    DEALLOCATE(hgtlcl,    STAT = istatus)
    DEALLOCATE(gridlight, STAT = istatus)

    ALLOCATE(iwp(nx,ny),  STAT = istatus)

    RETURN
  END SUBROUTINE work_array_swap

  !#####################################################################

  SUBROUTINE lightning_set_usage(myproc,ipass,lgtsw,istatus)

    INTEGER, INTENT(IN)  :: myproc,ipass
    INTEGER, INTENT(OUT) :: lgtsw

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER           :: isrc, icnt
    CHARACTER(LEN=80) :: lgtsrc(nlgt_max)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    lgtsw = 0
    IF (ipass > 0) THEN

      IF( nlgtfil /= 0 ) THEN
        icnt = 0
        DO isrc=1,nlgtfil
          IF(iuselgt(isrc,ipass) > 0) THEN
            lgtsw=1
            icnt = icnt+1
            lgtsrc(icnt) = srclgt(isrc)
          END IF
        END DO
      END IF
    ELSE    ! chk_opt == 1, ipass == 0
      IF( nlgtfil /= 0) THEN
        lgtsw = 1
        lgtsrc = srclgt
      END IF
    END IF

    CALL mpmaxi(lgtsw)

    IF (myproc == 0) THEN
      IF(lgtsw == 0) THEN
        WRITE(6,'(3x,a,26x,a)') 'Lightning data',' none'
      ELSE
        WRITE(6,'(3x,a,26x,a)') 'Lightning data',' Using'
        DO isrc = 1, icnt
          WRITE(6,'(9x,I0,2a)') isrc,' - ',TRIM(lgtsrc(isrc))
        END DO
      END IF
      WRITE(6,*) ' '
    END IF

    RETURN
  END SUBROUTINE lightning_set_usage

  !#####################################################################

  SUBROUTINE read_lightning_file(myproc,nx,ny,nxlg,nylg,dx,xlatlc,xlonlc,istatus)

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: myproc
    INTEGER, INTENT(IN)  :: nx,ny,nxlg,nylg
    REAL,    INTENT(IN)  :: xlonlc(nx,ny),xlatlc(nx,ny)
    REAL,    INTENT(IN)  :: dx
    INTEGER, INTENT(OUT) :: istatus

    !--------------------------------------------------------------------

    INTEGER :: nrecords

    INTEGER :: i,j, ii, jj, n
    REAL    :: box
    INTEGER :: ibox, ni, nj
    REAL, POINTER     :: lat(:),lon(:), time(:)
    REAL, ALLOCATABLE :: xlat(:,:),xlon(:,:)
    REAL, ALLOCATABLE :: gridlight_global(:,:)

    !
    ! For output the lightning count on grid
    !
    CHARACTER (LEN=256) :: vfnam
    INTEGER :: nunit

    INTEGER :: dimids(2)
    CHARACTER(LEN=40) :: dimnames(2)
    INTEGER           :: dimvalues(2)

    CHARACTER(LEN=40) :: iattnames (1),rattnames(1),cattnames(1)
    INTEGER           :: iattvalues(1)
    REAL              :: rattvalues(1)
    CHARACTER(LEN=80) :: cattvalues(1)
    INTEGER           :: cattlens  (1)

    CHARACTER(LEN=40) :: varnames(1)
    CHARACTER(LEN=80) :: coordstr

    !CHARACTER(LEN=80) :: timestr

    CHARACTER(LEN=40) :: i1dnames(1),i2dnames(1),r1dnames(1),r2dnames(3)
    CHARACTER(LEN=80) :: i1dlongs(1),i2dlongs(1),r1dlongs(1),r2dlongs(3)
    CHARACTER(LEN=20) :: i1dunits(1),i2dunits(1),r1dunits(2),r2dunits(3)
    INTEGER           :: i1dimids(1),r1dimids(1),i2dimids(2,1),r2dimids(2,3)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (myproc == 0) THEN
      ALLOCATE(xlat(nxlg,nylg), STAT = istatus)
      ALLOCATE(xlon(nxlg,nylg), STAT = istatus)
    ELSE
      ALLOCATE(xlat(1,1),       STAT = istatus)
      ALLOCATE(xlon(1,1),       STAT = istatus)
    END IF

    CALL mpimerge2d(xlatlc,nx,ny,xlat)
    CALL mpimerge2d(xlonlc,nx,ny,xlon)

    ! TO avoid chicken pox effects when dealing with a finer grid compare dto
    ! the 9-10 km GLM resolution, we need to assign values to a range of neighbors
    box  = (9000./dx)/2.-1.
    ibox = CEILING(box)

    DO n = 1, nlgtfil

      IF (myproc == 0) THEN
        !  (i) Read raw lightning file:

        ALLOCATE(gridlight_global(nxlg,nylg))
        gridlight_global(:,:) =0.

        IF (srclgt(n) == 'ENTLN') THEN
          CALL read_ENTLN_lightning_file(lightning_files(n),nrecords,lat,lon,time,istatus)
        ELSE IF (srclgt(n) == 'GLM') THEN
          CALL read_GLM_lightning_file(lightning_files(n),nrecords,lat,lon,istatus)

          ALLOCATE(time(nrecords), STAT = istatus)
          time(1:nrecords) = 1.0
        ELSE
          istatus = -1
          WRITE(*,'(1x,2a)') 'ERROR: unsupported lightning data source: ',srclgt(n)
          GOTO 700
        END IF

        ! (ii)  Now we have lat lon of the grid and lat lon of lightning records: let's
        !       project it onto the WRF grid:

        DO i=1,nrecords

          IF(time(i).GT.0.) THEN

             if (lat(i) .ge.  20.  .AND. lat(i) .le.  55.) THEN ! CONUS ONLY
               if (lon(i) .ge. -130. .AND. lon(i) .le. -60.) THEN ! CONUS ONLY

                 outer : DO ii=2,nxlg-1 ! assume gridded lightning to be zero at boundaries
                   DO jj=2,nylg-1 ! assume gridded lightning to be zero at boundaries
                      IF(  (lat(i) .ge. xlat(ii,jj)) .and. (lat(i) .lt. xlat(ii,jj+1))  ) then
                        IF(  (360+lon(i) .ge. 360+xlon(ii,jj)) .and. (360+lon(i) .lt. 360+xlon(ii+1,jj))  ) then ! LON are negative !
                          ! grid_light(intervals(i)-(starttime-1),a,b)=grid_light(intervals(i)-(starttime-1),a,b)+1
                          gridlight_global(ii,jj)=gridlight_global(ii,jj)+1

                          !! TO avoid chicken pox effects. Assume the 9-10 km GLM resolution
                          !DO nj = jj-ibox, jj+ibox
                          !  DO ni = ii-ibox, ii+ibox
                          !    gridlight_global(ni,nj)=gridlight_global(ni,nj)+1
                          !  END DO
                          !END DO
                          EXIT outer
                        ENDIF
                      ENDIF
                   ENDDO
                 ENDDO outer

               endif
             endif

          ENDIF

        ENDDO  ! DO i=1,nrecords

     !    (iii) zero-strip boundaryies

        box=NINT(30000./dx) ! assume dx in meters and boundary 'zero-strip' of 30 km

        do i=1,nxlg-3
          do j=1,nylg-3 ! set light as zero in boundaries anyway - dont want to nudge there

            if (i.le.box .or. i.ge.(nxlg-box)) gridlight_global(i,j)=0
            if (j.le.box .or. j.ge.(nylg-box)) gridlight_global(i,j)=0

!            if one truly cares about the density values; need be spread
!            eqwually amongts grid points
!            gridlight_global(i,j)=int(0.1111111111*gridlight_global(i,j)) ! spread value over 9 grid points -> divide by 9

          end do
        end do

        print *,'max light global =', maxval(gridlight_global(:,:))

        IF (lightning_output > 0) THEN

          vfnam = './gridlight_global_'//TRIM(srclgt(n))
          WRITE(6,'(a,a)') ' lightning output netCDF filename: ',vfnam
          CALL netopen(TRIM(vfnam)//'.nc','W',nunit)

          dimnames(1) = 'nx'; dimvalues(1) = nxlg
          dimnames(2) = 'ny'; dimvalues(2) = nylg
          CALL net_def_raddims(nunit,2,dimnames,dimvalues,dimids,istatus)

          !WRITE(timestr,'(I4,5(a,I2.2))') iyear,'-',imon,'-',iday,'_',ihr,':',imin,':',isec

          cattnames(1) = 'data_source';   cattvalues(1) = srclgt(n); cattlens(1) = 16
          iattnames(1)  = 'nrecords';     iattvalues(1) = nrecords

          CALL net_wrt_radatts(nunit,1,0,1,iattnames,rattnames,cattnames, &
                         iattvalues,rattvalues,cattvalues,cattlens,istatus)

          r2dnames(1) = 'lat';       r2dimids(:,1) = dimids(:); r2dlongs(1) = 'latitude' ;               r2dunits(1) = 'Degree N'
          r2dnames(2) = 'lon';       r2dimids(:,2) = dimids(:); r2dlongs(2) = 'longitude';               r2dunits(2) = 'Degree E'
          r2dnames(3) = 'gridlight'; r2dimids(:,3) = dimids(:); r2dlongs(3) = 'lightning count on grid'; r2dunits(3) = ''

          CALL net_def_radvars(nunit,0,i1dnames,i1dimids,i1dlongs,i1dunits,   &
                                     0,i2dnames,i2dimids,i2dlongs,i2dunits,   &
                                     0,r1dnames,r1dimids,r1dlongs,r1dunits,   &
                                     3,r2dnames,r2dimids,r2dlongs,r2dunits,   &
                                 istatus)

          !varnames = (/'gridlight'/)
          !coordstr = "lon lat"
          !CALL net_var_addcoords(nunit,1,varnames,coordstr,istatus)
          CALL net_add_atts(nunit,'gridlight',1,(/'coordinates'/),(/"lon lat"/), istatus)

          CALL netwrt2d(nunit,0,0,'lat',      xlat,            nxlg,nylg)
          CALL netwrt2d(nunit,0,0,'lon',      xlon,            nxlg,nylg)
          CALL netwrt2d(nunit,0,0,'gridlight',gridlight_global,nxlg,nylg)

          CALL netclose(nunit)

        END IF

      ENDIF ! IF (myproc == 0) THEN

      700 CONTINUE
      CALL mpupdatei(istatus,1)
      IF (istatus /= 0) THEN
        CALL arpsstop('ERROR lightning data source',1)
      END IF

      !  (iV) split the global array into tiles !
      CALL mpisplit2d(gridlight_global,nx,ny,gridlight(:,:,n))

      IF (myproc == 0) THEN
        DEALLOCATE(gridlight_global)
        DEALLOCATE(lat,lon,time)
      END IF

    END DO ! nlgtfil

    DEALLOCATE(xlat,xlon)

    RETURN
  END SUBROUTINE read_lightning_file

  !#####################################################################

  SUBROUTINE read_GLM_lightning_file(lightning_file,nrecords,lat,lon,istatus)

    !USE IFPOSIX
    use netcdf

    IMPLICIT NONE

    CHARACTER(LEN=256)    :: lightning_file
    INTEGER, INTENT(OUT)  :: nrecords
    REAL,    INTENT(OUT), POINTER  :: lon(:),lat(:)
    INTEGER, INTENT(OUT) :: istatus

!--------------------------------------------------------------------


!   ONLY ALLOC VARS THA ARE USED ONCE ...nto the case for this guy
!
!    REAL :: gridlight_global(nxlg,nylg)

!    character :: lightning_file*200
!    real,allocatable :: intervals(:),lat(:),lon(:),time(:)
!   assume a large size because strikeNum = UNLIMITED in NETCDF file
!    integer, parameter :: len=85399 ! 1000000
     integer :: nfl
!    character :: command*160
!    INTEGER :: tmp,system

!     NETCDF STUFF
      integer :: ncid
      integer :: cmode, status
      integer :: variddat
      integer :: nflID


  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  ! begin EXEC code
!  lightning_file='/lfs3/projects/hpc-wof1/afierro/news3dvar/input/light.d01.out.txt'
!   wrfinput='/lfs3/projects/hpc-wof1/afierro/news3dvar/input/wrfinput_d01'

!   very important !


      WRITE(*,'(1x,2a)') 'Opening GLM lightning file ', TRIM(lightning_file)

      cmode = NF90_NOWRITE
      status = nf90_open(lightning_file,cmode,ncid)
      if (status /= nf90_noerr) CALL handle_ncd_error(status,'Open lightning file',.TRUE.)

      ! GET the unlimited dimension number

      !WRITE(*,*) 'getting lightning dimension'

! get dimension IDs
!      status = nf90_inq_dimid(ncid, "number_of_events", nevID)
!      status = nf90_inq_dimid(ncid, "number_of_groups", ngrID)
      status = nf90_inq_dimid(ncid, "number_of_flashes",  nflID)
! get dimension values
!      status = nf90_inquire_dimension(ncid, nevID, len=nev)
!      status = nf90_inquire_dimension(ncid, ngrID, len=ngr)
      status = nf90_inquire_dimension(ncid, nflID, len=nfl)

      WRITE(*,*) 'Number of GLM lightning [flash] records:',nfl
      nrecords = nfl

      allocate(lat(nfl))
      allocate(lon(nfl))
!     allocate(time(nfl)) ! don't care about the time because files will contan
!     houryl accumulated flashes; doen in python script with retcode(blah blah)
!     allocate(timetmp(nfl))

      lat(:)=0. ; lon(:)=0.

      !write(*,*) 'GLM LAT'

! get flash_lat  ID
      status = NF90_INQ_VARID(ncid,'flash_lat',variddat)
      if (status /= nf90_noerr) CALL handle_ncd_error(status,'inq lat',.TRUE.)
! get flash_lat array
      status = NF90_GET_VAR(ncid,variddat,lat)
      if (status /= nf90_noerr) call handle_ncd_error(status,'get lat',.TRUE.)
      !write(*,*) 'GLM LON'
! get flash_lon  ID
      status = NF90_INQ_VARID(ncid,'flash_lon',variddat)
      if (status /= nf90_noerr) call handle_ncd_error(status,'inq lon',.TRUE.)
! get flash_lon array
      status = NF90_GET_VAR(ncid,variddat,lon)
      if (status /= nf90_noerr) call handle_ncd_error(status,'get lon',.TRUE.)

      status=NF90_CLOSE(ncid)

    RETURN
  END SUBROUTINE read_GLM_lightning_file

  !#####################################################################

  SUBROUTINE read_ENTLN_lightning_file(lightning_file,nrecords,lat,lon,time,istatus)

    !USE IFPOSIX
    use netcdf

    IMPLICIT NONE

    CHARACTER(LEN=256)    :: lightning_file
    INTEGER, INTENT(OUT)  :: nrecords
    REAL,    INTENT(OUT), POINTER  :: lon(:),lat(:),time(:)
    INTEGER, INTENT(OUT) :: istatus

    !--------------------------------------------------------------------

!   ONLY ALLOC VARS THA ARE USED ONCE ...nto the case for this guy
!
!    REAL :: gridlight_global(nxlg,nylg)

!    character :: lightning_file*200
!    real,allocatable :: intervals(:),lat(:),lon(:),time(:)
!   assume a large size because strikeNum = UNLIMITED in NETCDF file
!    integer, parameter :: len=85399 ! 1000000
!    real :: lat(len),lon(len),time(len),timetmp(len)
    real,allocatable :: timetmp(:)
!    character :: command*160
!    INTEGER :: tmp,system

    INTEGER :: i, ierror, iatime(9)

!     NETCDF STUFF
    integer :: ncid,record_id,records
    integer :: cmode, status
    integer :: variddat

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  ! begin EXEC code

! lightning file to be supplied by news3dvar namelist

!   very important !

      write(*,*) 'opening ENTLN lightning NC file'

      cmode = NF90_NOWRITE
      status = nf90_open(lightning_file,cmode,ncid)
      if (status /= nf90_noerr) CALL handle_ncd_error(status,'open lightning file',.TRUE.)

!       GET the unlimited dimension number

      write(*,*) 'getting lightning dimension'

      status = NF90_INQUIRE(ncid, unlimitedDimID = record_id)
      IF(status /= NF90_NOERR) write(6,*) 'GRID_INFO_NETCDF: ERROR FINDING TIME DIM',NF90_STRERROR(status)

      status = NF90_INQUIRE_DIMENSION(ncid, record_id, len = nrecords)
      IF(status /= NF90_NOERR) write(6,*) 'GRID_INFO_NETCDF: ERROR READING TIME DIM',NF90_STRERROR(status)

      write(*,*) 'number of lightning records',nrecords

      allocate(lat(nrecords))
      allocate(lon(nrecords))
      allocate(time(nrecords))
      allocate(timetmp(nrecords))

      lat(:)=0. ; lon(:)=0. ; time(:)=0.

      write(*,*) 'opening lightning file', lightning_file

      write(*,*) 'LAT'
! get lat  ID
      status = NF90_INQ_VARID(ncid,'lat',variddat)
      if (status /= nf90_noerr) CALL handle_ncd_error(status,'inq lat',.TRUE.)
! get lat array
      status = NF90_GET_VAR(ncid,variddat,lat)
      if (status /= nf90_noerr) CALL handle_ncd_error(status,'get lat',.TRUE.)
      write(*,*) 'LON'
! get lon  ID
      status = NF90_INQ_VARID(ncid,'lon',variddat)
      if (status /= nf90_noerr) CALL handle_ncd_error(status,'inq lon',.TRUE.)
! get lon array
      status = NF90_GET_VAR(ncid,variddat,lon)
      if (status /= nf90_noerr) CALL handle_ncd_error(status,'get lon',.TRUE.)
      write(*,*) 'TIME'
! get time  ID
      status = NF90_INQ_VARID(ncid,'time',variddat)
      if (status /= nf90_noerr) CALL handle_ncd_error(status,'inq time',.TRUE.)
! get time  array
      status = NF90_GET_VAR(ncid,variddat,timetmp)
      if (status /= nf90_noerr) CALL handle_ncd_error(status,'get time',.TRUE.)

      status=NF90_CLOSE(ncid)

!  CONVERT TIME, which is in EPOCH time to today's format:

!       DO i=1,len
       DO i=1,nrecords
!       write(*,*)'time,lat,lon',timetmp(i),lat(i),lon(i)
         if (timetmp(i).gt.0.) then
           !call PXFLOCALTIME (int(timetmp(i)), iatime, ierror)
           CALL abss2ctim(int(timetmp(i))+315619200,iatime(6),iatime(5),iatime(4),iatime(3),iatime(2),iatime(1))
           ! 315619200 - isecond of 1970-01-01 from 1960-01-01
           ! starting from 1960
           ! timetmp starting from 1970

!  iatime - output array returning the following:
!        1 - seconds (0 - 61, for leap seconds)
!        2 - minutes (0 - 59)
!        3 - hours (0 - 23)
!        4 - day of the month (1 - 31)
!        5 - month of the year (1 - 12)
!        6 - Gregorian year (e.g., 2006)
!        7 - Day of the week (0 = sunday)
!        8 - Day of the year (1 - 366)
!        9 - DST flag (0 = standard, 1 = DST)

           time(i)=iatime(1)+60.*iatime(2) ! +3600.*iatime(3)
!       write(*,*) 'time',time(i)
         endif
       ENDDO

    DEALLOCATE(timetmp)

    RETURN
  END SUBROUTINE read_ENTLN_lightning_file

  !#####################################################################

  SUBROUTINE lightning_costf(nx,ny,nz,q_ctr,                            &
                             ibgn,iend,jbgn,jend,kbgn,kend,             &
                             f_light,istatus)

    INTEGER,  INTENT(IN)  :: nx,ny,nz
    REAL(P),  INTENT(IN)  :: q_ctr(nx,ny,nz)
    INTEGER,  INTENT(IN)  :: ibgn,iend,jbgn,jend,kbgn,kend

    REAL(DP), INTENT(OUT) :: f_light

    INTEGER,  INTENT(OUT) :: istatus

    INCLUDE 'mp.inc'

  !---------------------------------------------------------------------

    INTEGER  :: i,j,k
    REAL(DP) :: zqq, f_innov,b_innov  !  rms and bias
    INTEGER :: i_innov

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    f_light = 0.0D0

    !print*,'lightning_opt',lightning_opt
    !print*,'VCOSTF qobsng2(5,1)',qobsng2(5,1)

    i_innov = 0
    f_innov = 0.0D0
    b_innov = 0.0D0

    DO k=kbgn,kend
      DO j=jbgn,jend
        DO i=ibgn,iend

          IF (qvobs(i,j,k) > 0.0) THEN ! compute costf onyl where lightning is assimilated because q_ctr !=0 -
            zqq = q_ctr(i,j,k) - qvobs(i,j,k)
            i_innov = i_innov + 1
            f_innov = f_innov + zqq*zqq
            b_innov = b_innov + (qvobs(i,j,k)-q_ctr(i,j,k))

            !print*,'i,j,kq_ctr=',i,j,k,q_ctr(i,j,k), qvobs(i,j,k) ,zqq
            !zqq = zqq*zqq / ( qobsng2(5,1) )
            !zqq = zqq*zqq / 0.00000001 ! ORIG
            zqq = zqq*zqq / 0.00001 ! 0.0001
            !zqq = zqq*zqq / 1.6000002E-05 ! / (qobsng2(5,1) )

             !  TURN OFF QV:
             ! zqq =0.0

            f_light    = f_light +  zqq
          END IF

        END DO
      END DO
    END DO

    !WYH      iwp(:,:)=0. ! modeled value set to zero to avoid adding up?
    !WYH
    !WYH      DO j=1, ny-1
    !WYH        DO i=1, nx-1
    !WYH          DO k=1, nz-1
    !WYH!           compute model IWP:
    !WYH
    !WYH!           w(i,j,k) =  t_ctr(i,j,k)*( p_ctr(i,j,k)/p0 )**rddcp ! temp in Kelvin
    !WYH            w(i,j,k) = anx(i,j,k,4)*(anx(i,j,k,3)/p0)**rddcp    ! temp in Kelvin
    !WYH!            temp22 = anx(i,j,k,4)*(anx(i,j,k,3)/1.e5)**0.286 ! temp in Kelvin
    !WYH            rho=anx(i,j,k,3)/(287.*w(i,j,k)) ! rho_air in kg/m3
    !WYH
    !WYH!            if (k.gt.1) iwp(i,j)=iwp(i,j) + rho*(zp(i,j,k)-zp(i,j,k-1))*( qh_ctr(i,j,k) )           ! Hail/graupel contribution relatively smaller. Assume units of Qg in kg/kg
    !WYH!            if (k.eq.1) iwp(i,j)=iwp(i,j) +rho*(zp(i,j,k)-0.)*( qh_ctr(i,j,k) )
    !WYH            if (k.gt.1) iwp(i,j)=iwp(i,j) + rho*(zp(i,j,k)-zp(i,j,k-1))*( qr_ctr(i,j,k) )  ! rain contribution
    !WYH            if (k.eq.1) iwp(i,j)=iwp(i,j) +rho*(zp(i,j,k)-0.)*( qr_ctr(i,j,k) )
    !WYH
    !WYH          END DO ! k=1, nz-1
    !WYH
    !WYH! IWP assimilation via lightning:
    !WYH
    !WYH          IF (iwp(i,j).gt.35.) iwp(i,j)=35.0 ! to remain consistent with obs derived IWP
    !WYH
    !WYH          IF (iwp(i,j) >= 0.0) THEN
    !WYH            zqh = iwp(i,j) - iwpobs(i,j)
    !WYH!            zqh = zqh*zqh /0.001      ! ORIG: weakorig as in 100%qvsat_2_WSM6_IWP_ONLY_LCL/weakorig or 100%qvsat_2_WSM6_IWP_LCL
    !WYH            zqh = zqh*zqh /10.     !
    !WYH
    !WYH!  TURN OFF IWP:
    !WYH            zqh =0.0
    !WYH
    !WYH            f_oqsng    = f_oqsng +  zqh  ! ok to add normalized errors of Qv + IWP.
    !WYH          END IF
    !WYH
    !WYH        END DO ! i=1, nx-1
    !WYH      END DO !j=1, ny-1

    CALL mptotali(i_innov)
    CALL mpsumdp(f_innov,1)
    CALL mpsumdp(b_innov,1)
    !IF (myproc == 0) WRITE(*,'(1x,a,F30.15,a,F30.15,a)')                &
    !   'Lightning DA STATS, RMS_innovation/residual = ',SQRT(f_innov/i_innov), &
    !   ', MEAN_innovation/residual = ', b_innov/i_innov,'.'

    RETURN
  END SUBROUTINE lightning_costf

  !#####################################################################

  SUBROUTINE lightning_gradt(nx,ny,nz,q_ctr,q_grd,istatus)

    INTEGER,  INTENT(IN)     :: nx,ny,nz
    REAL(P),  INTENT(IN)     :: q_ctr(nx,ny,nz)
    REAL(P),  INTENT(INOUT)  :: q_grd(nx,ny,nz)

    INTEGER,  INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER  :: i,j,k

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !        q_grd(:,:,:) = 0.

    !    print*,'lightning_opt=,',lightning_opt
    !    print*,'background errors, gdu_err,gdv_err,gdp_err,gdt_err,gdq_err,gdw_err:',gdu_err(1,1,1),gdv_err(1,1,1),gdp_err(1,1,1),gdt_err(1,1,1),gdq_err(1,1,1),gdw_err(1,1,1)
    !    print*,'obs errors, qobsng(1:6,1):',qobsng(1,1),qobsng(2,1),qobsng(3,1),qobsng(4,1),qobsng(5,1),qobsng(6,1)

        DO k=1, nz-1
          DO j=1, ny-1
            DO i=1, nx-1

             IF (qvobs(i,j,k) > 0.0) THEN
    !          print*,'VGRADF qobsng(5,1)=',qobsng(5,1)
    !          print*,'qvobs(i,j,k)=',qvobs(i,j,k)
    !          print*,'q_ctr(i,j,k)=',q_ctr(i,j,k)
    !           print*,'i,j,k, q_ctr gradc=',i,j,k,q_ctr(i,j,k), qvobs(i,j,k),q_ctr(i,j,k)-qvobs(i,j,k)
    !          q_grd(i,j,k)=( q_ctr(i,j,k)-qvobs(i,j,k) ) / ( qobsng(5,1) ) ! 1.6000002E-05 ! /( qobsng(5,1) )
    !          q_grd(i,j,k)=( q_ctr(i,j,k)-qvobs(i,j,k) ) / 0.00000001 ! 1.6000002E-05 ! /( qobsng(5,1) )
               q_grd(i,j,k)= q_grd(i,j,k) + ( q_ctr(i,j,k)-qvobs(i,j,k) ) / 0.00001 ! 0.0001 ! 1.6000002E-05 ! /( qobsng(5,1) )
    !          print*,'q_grd(i,j,k)=',q_grd(i,j,k)

    !        TURN OFF Qv:
    !          q_grd(i,j,k) = 0.

             END IF

            END DO
          END DO
        END DO

    !WYH    temp1=0.
    !WYH
    !WYH    DO j=1, ny-1
    !WYH      DO i=1, nx-1
    !WYH        DO k=1, nz-1
    !WYH
    !WYH         IF (iwpobs(i,j) >= 0.0) THEN
    !WYH!           temp1 = ( iwp(i,j)-iwpobs(i,j) ) /0.00001 !! ( assume IWP error^2 smaller < Qv_obserr^2=1.e-4). Thsi way more impact on the assimilation.
    !WYH!           temp1 = ( iwp(i,j)-iwpobs(i,j) ) /0.001 ! ! weakorig as in 100%qvsat_2_WSM6_IWP_ONLY_LCL/weakorig or 100%qvsat_2_WSM6_IWP_LCL
    !WYH           temp1 = ( iwp(i,j)-iwpobs(i,j) ) /10. !
    !WYH!           temp1 = ( iwp(i,j)-iwpobs(i,j) ) /0.00000001
    !WYH         ENDIF
    !WYH!        TURN OFF IWP:
    !WYH          temp1 = 0.
    !WYH
    !WYH         temp2 = anx(i,j,k,4)*(anx(i,j,k,3)/p0)**rddcp
    !WYH         rho=anx(i,j,k,3)/(287.*temp2) ! rho_air in kg/m3
    !WYH
    !WYH!         if (k.eq.1) qh_grd(i,j,k) =qh_grd(i,j,k)+rho*(zp(i,j,k)-0.)*temp1 ! chain rule (dJ/diwp * diwp/dqh))
    !WYH!         if (k.gt.1) qh_grd(i,j,k) =qh_grd(i,j,k)+rho*(zp(i,j,k)-zp(i,j,k-1))*temp1
    !WYH         if (k.eq.1) qr_grd(i,j,k) =qr_grd(i,j,k)+rho*(zp(i,j,k)-0.)*temp1 ! chain rule (dJ/diwp * diwp/dqh))
    !WYH         if (k.gt.1) qr_grd(i,j,k) =qr_grd(i,j,k)+rho*(zp(i,j,k)-zp(i,j,k-1))*temp1
    !WYH
    !WYH        END DO
    !WYH      END DO
    !WYH
    !WYH    END DO

    RETURN
  END SUBROUTINE lightning_gradt

  !#####################################################################

  SUBROUTINE compute_lightning(myproc,nproc_x,nproc_y,nx,ny,nz,dx,nscalar,zs,  &
                               pr,pt,qv,qscalar,istatus)

    INTEGER, INTENT(IN)  :: myproc,nproc_x,nproc_y
    INTEGER, INTENT(IN)  :: nx,ny,nz,nscalar
    REAL,    INTENT(IN)  :: dx ! in meters
    REAL,    INTENT(IN)  :: zs(nx,ny,nz)
    REAL,    INTENT(IN)  :: pr(nx,ny,nz),pt(nx,ny,nz),qv(nx,ny,nz)
    REAL,    INTENT(IN)  :: qscalar(nx,ny,nz,nscalar)
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    CALL adaslcl(nx,ny,nz,zs,pr,pt,qv,hgtlcl)

    CALL compute_qvobs(myproc,nproc_x,nproc_y,nx,ny,nz,dx,nscalar,pr,pt,qv,zs,qscalar,istatus)

    !This does not work well; creates downdrafts. So 'remove'
    !CALL compute_iwp(nx,ny,istatus)

    CALL work_array_swap(nx,ny,istatus)

    RETURN
  END SUBROUTINE compute_lightning

  !#####################################################################

  SUBROUTINE compute_qvobs(myproc,nproc_x,nproc_y,nx,ny,nz,dx,nscalar,pr,pt,qv,zs,qscalar,istatus)

  !-----------------------------------------------------------------------
  !
  !  LIGHTNING related variables
  !
  !-----------------------------------------------------------------------
  !

    USE module_ensemble, only : nensvirtl,iensmbl, icycle

    IMPLICIT NONE
    !INCLUDE 'grid.inc'          ! Grid & map parameters.

    INTEGER, INTENT(IN)  :: myproc,nproc_x,nproc_y
    REAL,    INTENT(IN)  :: dx ! in meters already declared in grid.inc
    integer :: nx,ny,nz,nscalar
    real    :: pr(nx,ny,nz),pt(nx,ny,nz),qv(nx,ny,nz) ! Qv in kg/kg
    real    :: zs(nx,ny,nz) ! meters
    real    :: qscalar(nx,ny,nz,nscalar)
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    REAL    :: total_vapor,total_vapor_gp ! kg/kg

    integer :: i,j,k, n
    REAL    :: temp22
    REAL    :: qvsat, rh, dum
    LOGICAL :: have_lgt
    real    :: rho_air(nx,ny,nz)

    INTEGER,parameter:: iseed=-100
    INTEGER :: iseed1
    REAL, PARAMETER:: errQv = SQRT(0.1)

    !INCLUDE 'mp.inc'

    REAL :: GASDEV

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Splitting of the data into tiles for each process
  !  Once lightning density data are tiled, compute QVobs for each tile using ANX arrays
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    iseed1 = (iseed-(nensvirtl+10)*iensmbl+icycle)*(myproc+1)

    qvobs(:,:,:) = 0.
    rho_air(:,:,:) = 0.

    DO k=2,nz-2
      DO j=2,ny-2
        DO i=2,nx-2

  !       WSM6 of WRF
  !          tr=273.16/t(i,j,k)
  !          qvs(i,j,k)=6.1078E+2*exp(log(tr)*(xa))*exp(xb*(1.-tr))
  !          qvs(i,j,k) = 611.73 * qvs(i,j,k) / (p(i,j,k) - qvs(i,j,k))
  !          qvs(i,j,k) = max(qvs(i,j,k),1.e-07)

  !   Clausius Clap. - Const Lv
  !         qvs(i,k,j)=(611.73/press(i,k,j))*(6.1078E+2*exp((2.5000E+6/461.5)*((1/273.15)-(1/(temp(i,k,j)) ) ) ) )

  !   Teten's formula - valid for -30 to 35C range. Ok for here !
  !         esw= 6.1078E+2*exp( 17.625*temp(i,k,j) / (temp(i,k,j)+243.04 )  )
  !         qvsat = 611.73 * esw/(press(i,k,j)-esw)

            temp22 = pt(i,j,k)*(pr(i,j,k)/1.e5)**0.286
            temp22=temp22-273.15 ! Tetens requires deg C !

  !  BUILT IN FCTION F_QVSAT DOES NOT WORK - USE TETEN FORMULATION
  !          qvsat = f_qvsat(temp22 ,anx(i,j,k,PTR_P) )
            temp22= 6.1078E+2*exp( 17.625*temp22 / (temp22+243.04 )  ) ! sat vap press in Pa
            qvsat = 611.73 * temp22/(pr(i,j,k)-temp22) ! in g/kg whilst model in kg/kg

            rh = 1000.*qv(i,j,k)/qvsat

  !          print*, 'i,j,k,Qv,P,rh,qvsat',i,j,k,anx(i,j,k,PTR_QV),anx(i,j,k,PTR_P),rh,qvsat

  !         modify QV model to a QVOBS based on gridlight:

            temp22 = pt(i,j,k)*(pr(i,j,k)/1.e5)**0.286


            rho_air(i,j,k)=pr(i,j,k)/(temp22*287.)
            if (temp22.eq.0.) PRINT*,'pt(i,j,k),pr(i,j,k)',i,j,k,pt(i,j,k),pr(i,j,k)
            !PRINT*,'temp22,rhoair',temp22,rho_air(i,j,k)

            !Q_CTRL IS NOT = Q_BACKGROUND !!!!!
            qvobs(i,j,k)=-99. ! anx(i,j,k,PTR_QV)
            !qvobs(i,j,k)=qv(i,j,k)

!            IF ( temp22.le.273.15 .and. temp22.ge.253.15 ) THEN ! 0 to -20C layer as in F12 - can always be changed

!             IF ( temp22.le.283.15 .and. temp22.ge.253.15 ) THEN ! deep layer -  MF14

!              print*,'hgtlcl(i,j) INSIDE', I, J, hgtlcl(i,j)

!            if (i.eq.20.and.j.eq.20) print*,'INSIDE', hgtlcl(20,20)
!            if (i.eq.10.and.j.eq.10) print*,'INSIDE', hgtlcl(10,10)
!            if (i.eq.5.and.j.eq.5) print*,'INSIDE', hgtlcl(5,5)

!           IF ( temp22.le.283.15 .and. temp22.ge.273.15 ) THEN

!             if (rh.le.1.0 .and. gridlight(i,j).gt.50) then ! LMA larger FED thresh
!               if (rh.le.1.0 .and. gridlight(i,j).gt.1) then ! LMA larger FED thresh
!            if (rh.le.1.0 .and. gridlight(i,j).gt.10) then !  ENTLN Thresh
!            if (rh.le.1.0 .and. gridlight(i,j).gt.50) then !

!             if (rh.le.0.8 .and. gridlight(i,j).gt.50) then ! LMA larger FED thresh
!             if (rh.le.0.8 .and. gridlight(i,j).gt.10) then ! ENTLN- increase Theta-V and hence CAPE where light is observed
!              if (hgtlcl(i,j).gt.300. .and.  rh.le.1.0 .and.gridlight(i,j).gt.20) then ! FOR LMA FED's to avoid adding QV mass in the anvil area !
!              if (hgtlcl(i,j).gt.300. .and.  rh.le.1.0 .and.gridlight(i,j).gt.0) then ! increase Theta-V and hence CAPE where light is observed
!              if (hgtlcl(i,j).gt.300. .and.  rh.le.1.0 .and.gridlight(i,j).ge.0.5*maxval(gridlight_global(:,:))) then ! increase Theta-V and hence CAPE where light is observed
!              if (gridlight(i,j).ge.10.) then ! increase Theta-V and hence CAPE where light is observed

            have_lgt = .FALSE.
            DO n = 1, nlgtfil
              if (gridlight(i,j,n) > 0) have_lgt = .TRUE.
            END DO


  !          TESTS to SUPPRESS SPURIOUS STORMS. Works.
          DO n = 1, nlgtfil
          IF (gridlight(i,j,n).eq.0) then ! in the model, z is in meters and Qv in kg/kg
            if (hgtlcl(i,j).gt.50.and.qv(i,j,k).gt.0.) then ! hgtlcl = 0  --> Qv=NaN. and qv(i,j,k)=0 --> RH=qvsat=NaN
              if (hgtlcl(i,j).gt.2000.) hgtlcl(i,j)=2000. ! have more influence in higher terrain areras where LCL are high
              !IF (zs(i,j,k).gt.hgtlcl(i,j) .and. zs(i,j,k).lt.(hgtlcl(i,j)+3000.) ) THEN
              ! ! test 1
              ! !if (rh.ge.0.95) qv(i,j,k)=0.6666*qv(i,j,k)
              ! ! test 2
              ! !if (rh.ge.0.80) qv(i,j,k)=0.60*qv(i,j,k)
              !ENDIF
            endif
          endif
          ENDDO


          DO n = 1, nlgtfil
            if (have_lgt) then ! in the model, z is in meters and Qv in kg/kg
!               if (gridlight(i,j,n).gt.0) then ! in the model, z is in meters and Qv in kg/kg
              if (gridlight(i,j,n).gt.0) then ! in the model, z is in meters and Qv in kg/kg
                if (hgtlcl(i,j).gt.50.and.qv(i,j,k).gt.0.) then ! hgtlcl = 0  --> Qv=NaN. and qv(i,j,k)=0 --> RH=qvsat=NaN

                  if (hgtlcl(i,j).gt.2000.) hgtlcl(i,j)=2000. ! have more influence in higher terrain areras where LCL are high

                      IF (zs(i,j,k).gt.hgtlcl(i,j) .and. zs(i,j,k).lt.(hgtlcl(i,j)+3000.) ) THEN
                        !IF (zs(i,j,k).gt.hgtlcl(i,j) .and. zs(i,j,k).lt.15000. ) THEN
                        !IF (zs(i,j,k).gt.hgtlcl(i,j) .and. zs(i,j,k).lt.5000. ) THEN
                        !
                        !print*,'zs(i,j,k),hgtlcl(i,j)',zs(i,j,k),hgtlcl(i,j)
                        !
                        !print*,'zs(i,j,1),zs(i,j,nz-1)',zs(i,j,1),zs(i,j,nz-1)
                        !print*,'qv(i,j,1),pr(i,j,1),pt(i,j,1)',qv(i,j,1),pr(i,j,1),pt(i,j,1)
                        !
                        !qvobs(i,j,k)=1.e-3*0.8*qvsat ! model QV must be in kg/kg
                        !
                        !   qvobs(i,j,k)=1.e-3*1.0*qvsat ! model QV must be in kg/kg
                        !
                        !if (rh.ge.1.0) then
                        if (rh.ge.0.95) then
                          qvobs(i,j,k)=qv(i,j,k)
                        else
                          !qvobs(i,j,k)=1.e-3*1.0*qvsat
                          qvobs(i,j,k)=1.e-3*0.95*qvsat
                          !qvobs(i,j,k)=1.e-3*1.25*qvsat
                        endif

                        !IF(iensmbl/=0) THEN
                        !   qvobs(i,j,k)=qvobs(i,j,k)+errQv*GASDEV(iseed1)
                        !END IF

              !print*,'zs(i,j,k),hgtlcl(i,j),k',zs(i,j,k),hgtlcl(i,j),k
              !print*,'qvsat,qv(i,j,k),qvobs(i,j,k),gridlight(i,j)',qvsat,qv(i,j,k),qvobs(i,j,k),gridlight(i,j)
              !
              !if (rh.le.0.8) then ! F12
              !
              !
              !  gridlight(i,j)=0.2*gridlight(i,j)
              !
              !  qvobs(i,j,k)=1.e-3*0.8*qvsat+1.e-3*0.2*qvsat*tanh(0.25*gridlight(i,j))*(1-tanh(0.25*(1000*qscalar(i,j,k,5))**2.2))
              !
              !  for LMA scaling ! no action
              !  if (gridlight(i,j).le.20) then
              !  qvobs(i,j,k)=anx(i,j,k,5) ! absolutely no action for small FED rates
              !  else
              !  qvobs(i,j,k)=1.e-3*0.8*qvsat+1.e-3*0.2*qvsat*tanh(0.25*(gridlight(i,j)))*(1-tanh(0.25*(1000*qscalar(i,j,k,5))**2.2))
              !  endif
              !
              !  print*, 'i,j,k,rh,qvsat,qvobs 1',i,j,k,rh,1.e-3*qvsat,qvobs(i,j,k)
              !  if (rh.le.0.5) then
              !  if (gridlight(i,j).le.50) then
              !    qvobs(i,j,k)=anx(i,j,k,5) ! absolutely no action for small FED rates
              !  else
              !    qvobs(i,j,k)=1.e-3*0.8*qvsat+1.e-3*0.2*qvsat*max( tanh(0.25*(gridlight(i,j)-50)),0.0)*(1-tanh(0.1*(1000*qscalar(i,j,k,5))**2.2))
              !  endif
              !endif
              !
              !COMPUTE QV TOTAL MASS
              !
              !
              !print*, 'ZS', zs(i,j,nk+1)-zs(i,j,k)
              !print*, 'QVDIFF', qvobs(i,j,k)-qv(i,j,k)

                      END IF ! zs(i,j,k).gt.hgtlcl(i,j) ...

  !     THis creates downdrafts...
  !            if (rh.ge.0.7 .and.gridlight(i,j).eq.0) then ! supress storms where no light is observed
  !            qvobs(i,j,k)=1.e-3*0.3*qvsat
  !!           print*, 'i,j,k,rh,qvsat,qvobs 2',i,j,k,rh,1.e-3*qvsat,qvobs(i,j,k)
  !            endif

                endif !
              endif !
            endif ! if (gridlight(i,j).gt.0) then
          ENDDO

        end do
      end do
    end do

    !print*, 'maxval(qvobs)=',maxval(qvobs(:,:,:))

    IF (.False.) THEN
    total_vapor = 0.

    DO k = 2, nz-2
      DO j = 2, ny-2
        DO i = 2, nx-2
         DO n = 1, nlgtfil
          if (gridlight(i,j,n).gt.0) then ! (qvobs(i,j,k)=-99 ELSEWHERE !! only focus where is was added)
            if (hgtlcl(i,j).gt.50.and.qv(i,j,k).gt.0.) then ! hgtlcl = 0  --> Qv=NaN. and qv(i,j,k)=0 --> RH=qvsat=NaN
            if (hgtlcl(i,j).gt.2000.) hgtlcl(i,j)=2000. ! have more influence in higher terrain areras where LCL are high
              IF (zs(i,j,k).gt.hgtlcl(i,j) .and. zs(i,j,k).lt.(hgtlcl(i,j)+3000.) ) THEN
                total_vapor = total_vapor + rho_air(i,j,k)*(qvobs(i,j,k)-qv(i,j,k))*dx*dx*(zs(i,j,k+1)-zs(i,j,k))
                !print*, 'qvobs(i,j,k)-qv(i,j,k))',qvobs(i,j,k)-qv(i,j,k)
                !print*, 'zs(i,j,k+1)-zs(i,j,k)',zs(i,j,k+1)-zs(i,j,k)
                !print*, 'dx, rho_air(i,j,k)',dx, rho_air(i,j,k)
              ENDIF
            endif
          endif
         END DO
       END DO
      END DO
    ENDDO

    ! Physical tile/patch size
    !(nx-3)*(ny-3)*(nz-3)

    CALL mptotal(total_vapor)
    total_vapor_gp = total_vapor/((nx-3)*(ny-3)*(nz-3)*nproc_x*nproc_y)
    ! grid-pt average QV that was added (total_vapor is in global space)

    ! Total physical domain size (boundary/staggering pts removed)

    !PRINT *, '(nx-3),(ny-3),(nz-3)',nx-3,ny-3,nz-3
    IF (myproc == 0) PRINT *, 'grid point-averaged water vapor added by LDA (kg)=',total_vapor_gp

    ! Outside lightning (columns) areas, remove the same amount of Qv.
    ! Recall that QVOBS MUST BE -99 in non-lightning AREAS so modify QV directly !

    DO k = 2, nz-2
      DO j = 2, ny-2
        DO i = 2, nx-2

         DO n = 1, nlgtfil
          if (gridlight(i,j,n).eq.0) then ! (qvobs(i,j,k)=-99 ELSEWHERE !! only focus where is was added)
            temp22=total_vapor_gp/ ( rho_air(i,j,k)*dx*dx*(zs(i,j,k+1)-zs(i,j,k)))
            !PRINT *,'grid point-averaged Qv=',temp22
!           to turn off removal simply comment out this single line
!            qv(i,j,k)=qv(i,j,k)-temp22
          endif
         ENDDO

        END DO
      END DO
    END DO

    !CALL mptotoal(total_vapor)

    ! Total physical domain size (boundary/staggering pts removed)
    !(nx-3)*nproc_x * (ny-3)*nproc_y * (nz-3)

    IF (myproc == 0) PRINT *, 'total_vapor (tile) added by LDA (kg)=',total_vapor,', myproc = ', myproc
    !PRINT *, 'grid pt av Qv added by LDA (kg)=',total_vapor/((nx-3)*nproc_x *(ny-3)*nproc_y * (nz-3))

    CALL mpsendrecv2dew(qv,nx,ny,nz,0,0,0,rho_air)
    CALL mpsendrecv2dns(qv,nx,ny,nz,0,0,0,rho_air)
    END IF

    RETURN
  END SUBROUTINE compute_qvobs

  !#####################################################################

  SUBROUTINE compute_iwp(nx,ny,istatus)!,qscalar,nscalar)
  !-----------------------------------------------------------------------
  !
  !  LIGHTNING related variables
  !
  !-----------------------------------------------------------------------
  !

   IMPLICIT NONE

   INTEGER, INTENT(IN)  :: nx,ny
   INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

   integer :: i,j,n

  ! real :: qscalar(nx,ny,nz,nscalar)
  ! Qscalar(:,:,:,P_QX) = _QC = 1; P_QR = 2; P_QI = 3; P_QS = 4; P_QG = 5; P_QH = 6;

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Splitting of the data into tiles for each process
  !  Once lightning density data is tiled, computed QVobs for each tile using ANX arrays above
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  ! each process compute 3D 'Qvobs' field from local anx in parallel
  !  qsrcsng(PTR_QV,jsrc)=0.004 is the background error
  !  qobsng normalized squared  observation error
  ! nvar = 1,2,3,4,5 = U, V, P, Theta, Qv

    iwpobs(:,:)=0.0

    DO n = 1, nlgtfil
      DO j=1,ny
        DO i=1,nx

          IF (gridlight(i,j,n) > 0) THEN

             !iwpobs(i,j)=100*0.02*gridlight(i,j) + 2.5 ! in kg/m2; derived from F15, JAS whereby LCV = 100*FoD.
             iwpobs(i,j) = 0.5*gridlight(i,j,n) ! in kg/m2; derived from F15, JAS whereby LCV = 100*FoD.

             IF (iwpobs(i,j) > 35.) iwpobs(i,j) = 35.0

          END IF

        END DO
      END DO
    END DO

    RETURN
  END SUBROUTINE compute_iwp

END MODULE module_lightning
