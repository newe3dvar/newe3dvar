!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%
!%%%% MODULE to keep track cpu and wall clock timers
!%%%%
!%%%% Mainly adpated from arpslib3d.f90 & globcst.inc
!%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MODULE runtime_accts

  IMPLICIT NONE        ! Force explicit declarations

  INTEGER, PARAMETER :: num_accts = 12  ! not larger than max_acct (22) in globcst.inc

  INTEGER, PARAMETER :: iniana_acct=1,inputobs_acct=2,                  &
       inputbg_acct=3, recrsub_acct=4,recrsub1_acct=5,                  &
       recrsub2_acct=6,recrsub3_acct=7,varmain_acct=8,                  &
       cloudana_acct=9,ensemble_acct=10,                                &
       outputem_acct=11,miscel_acct=12

  CHARACTER(LEN=20), PARAMETER :: acct_descp(num_accts) = (/            &
      'Initialization       ', 'Read Observations    ',                 &
      'Read background      ', 'Recursive filter     ',                 &
      '    RF sb1 (init)    ', '    RF sb2 (forward) ',                 &
      '    RF sb3 (adjoint) ', 'Main VAR analysis    ',                 &
      'Cloud analysis       ', 'Hybrid Ensemble      ',                 &
      'Dump analysis vars   ', 'Miscellaneous        '  /)

  CONTAINS


  !##################################################################
  !##################################################################
  !######                                                      ######
  !######              SUBROUTINE ACCT_REPORT_VAR              ######
  !######                                                      ######
  !######                     Developed by                     ######
  !######     Center for Analysis and Prediction of Storms     ######
  !######                University of Oklahoma                ######
  !######                                                      ######
  !##################################################################
  !##################################################################

  SUBROUTINE accts_report_var

  !-----------------------------------------------------------------------
  !
  !  PURPOSE:
  !
  !  Output the timing statistics for the 3DVAR analysis.
  !
  !-----------------------------------------------------------------------
  !
  !  AUTHOR: Y. Wang (2017/01/04)
  !  Based on acct_report_arps in src/arps/arpslib3d.f90.
  !
  !  MODIFICATION HISTORY:
  !
  !
  !-----------------------------------------------------------------------
  !
  !  INPUT:
  !
  !  OUTPUT:
  !
  !-----------------------------------------------------------------------
  !
  !
  !-----------------------------------------------------------------------
  !
  !
  !  Variable Declarations:
  !
  !-----------------------------------------------------------------------


  !-----------------------------------------------------------------------
  !
  !  Miscellaneous local variables:
  !
  !-----------------------------------------------------------------------

    INTEGER :: i

    REAL :: tc, tw
    REAL :: wt

    DOUBLE PRECISION :: cpu_io, wall_io


  !-----------------------------------------------------------------------
  !
  !  Include files:
  !
  !-----------------------------------------------------------------------

    INCLUDE 'globcst.inc'
    INCLUDE 'mp.inc'

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    CALL flush(6)

    IF (use_acct == 0 ) RETURN

    ! A barrier before and after printing stats avoids spurious output
    ! in the body of the stats output.
    CALL mpbarrier

    IF (myproc == 0) THEN

      IF( mp_opt == 1 ) then
        wt = 1.0/( nproc_x*nproc_y )
      ELSE
        wt = 1.0
      ENDIF

      acct_cpu_time(recrsub_acct)  = acct_cpu_time(recrsub1_acct) +acct_cpu_time(recrsub2_acct) +acct_cpu_time(recrsub3_acct)
      acct_wall_time(recrsub_acct) = acct_wall_time(recrsub1_acct)+acct_wall_time(recrsub2_acct)+acct_wall_time(recrsub3_acct)

      IF(total_cpu  == 0d0) total_cpu =1d0
      IF(total_wall == 0d0) total_wall=1d0
      tc = 100.0/total_cpu
      tw = 100.0/total_wall

      !
      !  Print out CPU usage.
      !
      WRITE (6,'(/a/)') "Analysis CPU Summary:"

      IF (mp_opt > 0) THEN
        WRITE  (6,'(a,/,a)') &
        ' Process                 CPU time                   WALL CLOCK time (mean)', &
        ' ---------------------   ----------------------     ----------------------'
      ELSE
        WRITE  (6,'(a,/,a)') &
        ' Process                 CPU time                   WALL CLOCK time',        &
        ' ---------------------   ----------------------     ----------------------'
      END IF

      DO i = 1, num_accts
        WRITE(6,'(1x,2a,2x,1p,e13.6,a,2x,0p,f6.2,a,4x,1p,e13.6,a,2x,0p,f6.2,a)') &
         acct_descp(i),' :',acct_cpu_time(i), 's', acct_cpu_time(i)*tc,  '%',    &
                        acct_wall_time(i)*wt, 's', acct_wall_time(i)*tw, '%'
      END DO

      WRITE(6,'(/1x,a,2x,1p,e13.6,a,1x,0p,f7.2,a,1x,1p,e13.6,a,1x,0p,f7.2,a)')   &
         'Entire Program  :',total_cpu,'s',total_cpu/60.,' min',                 &
                             total_wall*wt,'s',total_wall*wt/60.,' min'

      cpu_io  = acct_cpu_time(iniana_acct)  + acct_cpu_time(inputobs_acct)       &
              + acct_cpu_time(inputbg_acct) + acct_cpu_time(outputem_acct)
      wall_io = acct_wall_time(iniana_acct) + acct_wall_time(inputobs_acct)      &
              + acct_wall_time(inputbg_acct)+ acct_wall_time(outputem_acct)

      WRITE(6,'(/1x,a,2x,1p,e13.6,a,2x,0p,7x,4x,1p,e13.6,a,2x,0p,6x)')           &
         'Without Init/IO :', total_cpu-cpu_io,'s',(total_wall-wall_io)*wt,'s'

    END IF
    CALL flush(6)
    CALL mpbarrier

    RETURN
  END SUBROUTINE accts_report_var

END MODULE runtime_accts
