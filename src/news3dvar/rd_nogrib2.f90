!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE RDNMCGRB2                  ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE rd_mrmsgrb2(nx_ext,ny_ext,nz_ext,gribfile,grbflen, dtstart,  &
                       ix1,ix2,jy1,jy2,latsw,lonsw,                     &
                       maxvar,varids, var2dindx, var2dlvl,              &
                       var_grb2d, lvldbg, iret)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  This subroutine is a dummy subroutine for case when NCEP GRIB2 library
!  is not linked.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  08/01/2006
!
!  MODIFICATIONS:
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE


  INTEGER, INTENT(IN) :: nx_ext,ny_ext,nz_ext

  INTEGER, INTENT(IN) :: grbflen,dtstart

  INTEGER, INTENT(IN)  :: ix1, ix2, jy1, jy2

  CHARACTER(LEN=*), INTENT(IN) :: gribfile

  REAL,    INTENT(OUT) :: latsw           ! Latitude  of South West corner point
  REAL,    INTENT(OUT) :: lonsw           ! Longitude of South West corner point

  INTEGER, INTENT(IN)  :: maxvar
  INTEGER, INTENT(IN)  :: lvldbg
  INTEGER, INTENT(IN)  :: varids(4,maxvar)
  INTEGER, INTENT(IN)  :: var2dindx(maxvar)
  REAL,    INTENT(IN)  :: var2dlvl(maxvar)
  REAL,    INTENT(OUT) :: var_grb2d(nx_ext,ny_ext,maxvar)

  INTEGER  :: iret         ! Return flag
!
!-----------------------------------------------------------------------
!
!  Temporary arrays to read GRIB file
!
!-----------------------------------------------------------------------
!

!  REAL    :: rcdata(nx_ext*ny_ext)           ! temporary data array
!  REAL    :: var_grb2d(nx_ext,ny_ext,n2dvs,n2dlvt)        ! GRIB 2-D variables
!  REAL    :: var_grb3d(nx_ext,ny_ext,nz_ext,n3dvs,n3dlvt) ! GRIB 3-D variables
!  INTEGER :: var_lev3d(nz_ext,n3dvs,n3dlvt)
                                       ! Levels (hybrid) for each 3-D variable

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  WRITE(6,'(/,2(a,/),/,a,/,/,3(a,/))')                                  &
  'WARNING: GRIB2 libary is not linked. To link NCEP GRIB2 library,',   &
  '         you should compile the program as:',                        &
  '         $> build -io grib2 [Other_options] <program_name>',         &
  '   NOTE: You must first install libraries libjasper.a(JPEG2000),',   &
  '         libpng.a(PNG) and libz.a. See src/external/g2lib/README',   &
  '         for more details.'

  iret = -2

  RETURN
END SUBROUTINE rd_mrmsgrb2
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE RDGRB2DIMS                 ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE rdgrb2dims(gribfile,grbflen,nx_ext,ny_ext,iret)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  This subroutine is a dummy subroutine for case when NCEP GRIB2 library
!  is not linked.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  08/18/2007
!
!  MODIFICATIONS:
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE


  INTEGER  :: nx_ext,ny_ext

  INTEGER, INTENT(IN) :: grbflen

  CHARACTER(LEN=*), INTENT(IN) :: gribfile

  INTEGER  :: iret         ! Return flag

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  WRITE(6,'(/,2(a,/),/,a,/,/,3(a,/))')                                  &
  'WARNING: GRIB2 libary is not linked. To link NCEP GRIB2 library,',   &
  '         you should compile the program as:',                        &
  '         $> build -io grib2 [Other_options] <program_name>',         &
  '   NOTE: You must first install libraries libjasper.a(JPEG2000),',   &
  '         libpng.a(PNG) and libz.a. See src/external/g2lib/README',   &
  '         for more details.'
  iret = -2

  RETURN
END SUBROUTINE rdgrb2dims
