!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Constraint using diagnostic divergence equation
!  to help build some kind of balance
!
!  tmp.debug.gge
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MODULE constraint_divergence

  USE model_precision

  IMPLICIT NONE

!-----------------------------------------------------------------------
!
! Control options
!
!-----------------------------------------------------------------------
  SAVE
  PRIVATE

  INTEGER :: div_opt
  LOGICAL :: use_arps_metrics
  REAL(P), ALLOCATABLE :: wgt_div_h(:), wgt_div_v(:)

!-----------------------------------------------------------------------
!
! Public interface
!
!-----------------------------------------------------------------------
  PUBLIC :: div_opt, use_arps_metrics

  PUBLIC :: consdiv3_read_nml_options
  PUBLIC :: compute_div3_arps, compute_adjdiv3_arps
  PUBLIC :: cal_div3_wrf,cal_adjdiv3_wrf

  CONTAINS

  SUBROUTINE consdiv3_read_nml_options(unum,npass,myproc,istatus)

  !#####################################################################

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: unum, myproc
    INTEGER, INTENT(IN)  :: npass
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    NAMELIST /var_diverge/ div_opt,use_arps_metrics,wgt_div_h,wgt_div_v

  !---------------------------------------------------------------------

    INTEGER :: i

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !IF (npass > maxpass) THEN
    !  WRITE(*,'(1x,a,2(I0,a),/,7x,a)') 'ERROR: Request npass (',npass,  &
    !          ')is larger then this maxpass (',maxpass,') in constraint_divergence.', &
    !          'Program will abort.'
    !  istatus = -1
    !  RETURN
    !END IF

    ALLOCATE(wgt_div_h(npass), STAT = istatus)
    CALL check_alloc_status(istatus, "constraint_diagnostic_div:wgt_dpec")
    ALLOCATE(wgt_div_v(npass), STAT = istatus)
    CALL check_alloc_status(istatus, "constraint_diagnostic_div:wgt_hBal")

    div_opt = 0
    use_arps_metrics = .FALSE.
    DO i=1,npass
      wgt_div_h(i) = -1.0
      wgt_div_v(i) = -1.0
    ENDDO

    IF (myproc == 0) THEN
      READ(unum, var_diverge,  END=350)
      WRITE(*,*) 'Namelist block var_diverge sucessfully read.'

      GOTO 400

      350 CONTINUE
      WRITE(*,*) 'ERROR: reading namelist block var_diverge.'
      istatus = -2
      RETURN

      400 CONTINUE

      WRITE(*,'(5x,a,I0,a)') 'div_opt = ', div_opt,','
      WRITE(*,'(5x,a,L,a)')  'use_arps_metrics = ', use_arps_metrics,','
      IF ( div_opt == 1 ) THEN
         WRITE(*,'(5x,a)',ADVANCE='NO') 'div3 horizontal weighting coefficients: '
         WRITE(*,'(1x,10(G9.2,a))') (wgt_div_h(i),', ', i =1 , npass )
         WRITE(*,'(5x,a)',ADVANCE='NO') 'div3 vertical weighting coefficients: '
         WRITE(*,'(1x,10(G9.2,a))') (wgt_div_v(i),', ', i =1 , npass )
      END IF

    END IF

    CALL mpupdatei(div_opt,           1)
    CALL mpupdatel(use_arps_metrics,  1)
    CALL mpupdater(wgt_div_h,     npass)
    CALL mpupdater(wgt_div_v,     npass)

    RETURN
  END SUBROUTINE consdiv3_read_nml_options

  !#####################################################################

  !-----------------------------------------------------------------------
  !
  !  Compute the momentum divergence term, defined as
  !
  !  div = d(u*rhostr)/dx + d(v*rhostr)/dy + d(wcont*rhostr)/dz.
  !
  !  NOTE: u, v & w should be on vector points when using in divergence constraint
  !        and all other arps model constraints in the future.
  !
  !-----------------------------------------------------------------------
  SUBROUTINE compute_div3_arps(ipass,nx,ny,nz,dx,dy,dz,dxinv,dyinv,dzinv,&
                               u_ctr,v_ctr,w_ctr,div3,                  &
                               mapfct,j1,j2,j3,aj3z,j3inv,              &
                               rhostr,rhostru,rhostrv,rhostrw,          &
                               wcont,tem1,tem2,tem3, tem4,istatus)

    INTEGER, INTENT(IN)  :: ipass
    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL(P), INTENT(IN)  :: dx, dy, dz
    REAL(P), INTENT(IN)  :: dxinv,dyinv,dzinv

    REAL(P), INTENT(IN)  :: u_ctr(nx,ny,nz),v_ctr(nx,ny,nz),w_ctr(nx,ny,nz)

    REAL(P), INTENT(IN)  :: mapfct(nx,ny,8)   ! Map factors at scalar, u and v points

    REAL(P), INTENT(IN)  :: j1    (nx,ny,nz)  ! Coordinate transformation Jacobian
                                 ! defined as - d( zp )/d( x ).
    REAL(P), INTENT(IN)  :: j2    (nx,ny,nz)  ! Coordinate transformation Jacobian
                                 ! defined as - d( zp )/d( y ).
    REAL(P), INTENT(IN)  :: j3    (nx,ny,nz)  ! Coordinate transformation Jacobian
                                 ! defined as d( zp )/d( z ).
    REAL(P), INTENT(IN)  :: aj3z  (nx,ny,nz)  ! Coordinate transformation Jacobian defined
                                 ! as d( zp )/d( z ) AVERAGED IN THE Z-DIR.
    REAL(P), INTENT(IN)  :: j3inv (nx,ny,nz)     ! Inverse of j3

    REAL(P), INTENT(IN)  :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3

    REAL(P), INTENT(IN)  :: rhostru(nx,ny,nz)    ! Averaged rhostr at u points (kg/m**3).
    REAL(P), INTENT(IN)  :: rhostrv(nx,ny,nz)    ! Averaged rhostr at v points (kg/m**3).
    REAL(P), INTENT(IN)  :: rhostrw(nx,ny,nz)    ! Averaged rhostr at w points (kg/m**3).

    REAL(P), INTENT(OUT) :: div3(nx,ny,nz)

    REAL(P), INTENT(OUT) :: wcont(nx,ny,nz)
    REAL(P), INTENT(OUT) :: tem1(nx,ny,nz), tem2(nx,ny,nz)
    REAL(P), INTENT(OUT) :: tem3(nx,ny,nz), tem4(nx,ny,nz)

    INTEGER, INTENT(OUT) :: istatus

  !-----------------------------------------------------------------------
    INTEGER :: i,j,k

    INCLUDE 'bndry.inc'     ! to include ebc, nbc, sbc, wbc etc.
    INCLUDE 'mp.inc'

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ! First, convert u_ctr, v_ctr, w_ctr from scalar grid to vector grid
    ! of u, v, w

    !DO k= 1,nz-1
    !  DO j= 1,ny-1
    !    DO i= 2,nx-1
    !      !u(i,j,k) = 0.5*(anx(i,j,k,1)+u_ctr(i,j,k)+anx(i-1,j,k,1)+u_ctr(i-1,j,k))
    !      u(i,j,k) = 0.5*( u_ctr(i,j,k) + u_ctr(i-1,j,k) )
    !    END DO
    !  END DO
    !END DO
    !
    !DO k= 1,nz-1
    !  DO j= 1,ny-1
    !    u(1,j,k)  = u(2,j,k)
    !    u(nx,j,k) = u(nx-1,j,k)
    !  END DO
    !END DO
    !
    !DO k= 1,nz-1
    !  DO j= 2,ny-1
    !    DO i= 1,nx-1
    !      !v(i,j,k) = 0.5*(anx(i,j,k,2)+v_ctr(i,j,k)+anx(i,j-1,k,2)+v_ctr(i,j-1,k))
    !      v(i,j,k) = 0.5*( v_ctr(i,j,k) + v_ctr(i,j-1,k) )
    !    END DO
    !  END DO
    !END DO
    !
    !DO k= 1,nz-1
    !  DO i= 1,nx-1
    !    v(i,1,k)  = v(i,2,k)
    !    v(i,ny,k) = v(i,ny-1,k)
    !  END DO
    !END DO
    !
    !DO k=2,nz-1
    !  DO j= 1,ny-1
    !    DO i= 1,nx-1
    !      !w(i,j,k) = 0.5*(anx(i,j,k,6)+w_ctr(i,j,k) + anx(i,j,k-1,6)+w_ctr(i,j,k-1))
    !      w(i,j,k) = 0.5*( w_ctr(i,j,k) + w_ctr(i,j,k-1) )
    !    END DO
    !  END DO
    !END DO
    !
    !DO j= 1,ny-1
    !  DO i= 1,nx-1
    !    w(i,j,1)  = w(i,j,2)
    !    w(i,j,nz) = w(i,j,nz-1)
    !  END DO
    !END DO
    !
    !IF (mp_opt > 0) THEN
    !  !CALL acct_interrupt(mp_acct)
    !  CALL mpsendrecv2dew(u, nx, ny, nz, ebc, wbc, 1, tem4)
    !  CALL mpsendrecv2dns(u, nx, ny, nz, nbc, sbc, 1, tem4)
    !
    !  CALL mpsendrecv2dew(v, nx, ny, nz, ebc, wbc, 2, tem4)
    !  CALL mpsendrecv2dns(v, nx, ny, nz, nbc, sbc, 2, tem4)
    !
    !  !CALL mpsendrecv2dew(w, nx, ny, nz, ebc, wbc, 3, tem4)
    !  !CALL mpsendrecv2dns(w, nx, ny, nz, nbc, sbc, 3, tem4)
    !  !CALL acct_stop_inter
    !END IF

    !u = u_ctr
    !v = v_ctr
    !w = w_ctr

    CALL wcontra(nx,ny,nz,u_ctr,v_ctr,w_ctr,mapfct,j1,j2,j3,aj3z,       &
                 rhostr,rhostru,rhostrv,rhostrw,wcont,tem1,tem2)
    !
    ! tem1 is on U grid, tem2 on V grid, tem3 on W grid, and div3 is on scalar grid
    !
    IF(wgt_div_h(ipass) > 0.0 ) THEN
      DO k=1,nz-1
        DO j=1,ny-1
          DO i=1,nx
            tem1(i,j,k)=u_ctr(i,j,k)*rhostru(i,j,k)*mapfct(i,j,5)
          END DO
        END DO
      END DO

      DO k=1,nz-1
        DO j=1,ny
          DO i=1,nx-1
            tem2(i,j,k)=v_ctr(i,j,k)*rhostrv(i,j,k)*mapfct(i,j,6)
          END DO
        END DO
      END DO
    END IF

    IF(wgt_div_v(ipass) > 0.0 ) THEN
      DO k=1,nz
        DO j=1,ny-1
          DO i=1,nx-1
           tem3(i,j,k)=wcont(i,j,k)*rhostrw(i,j,k)
          END DO
        END DO
      END DO
    END IF

    IF( (wgt_div_h(ipass) > 0.0) .AND. (wgt_div_v(ipass) > 0.0)) THEN

  !if (mp_opt == 0) THEN
  !WRITE(100,*) ibgn,iend,jbgn,jend,tem1(201:203,2,2),f_div
  !else if (myproc == 0) THEN
  !  write(100+myproc,*) ibgn,iend,jbgn,jend,tem1(201:203,2,2),f_div
  !else if (myproc == 1) THEN
  !  write(100+myproc,*) ibgn,iend,jbgn,jend,tem1(1:3,2,2)
  !end if
          !USEWRF
          !IF (modelopt == 2) THEN  ! for WRF
          !  DO k = 1, nz-1
          !    DO j=jbgn,jend
          !      DO i=ibgn,iend
          !        tem4(i,j,k) = 1.0/(zp(i,j,k+1)-zp(i,j,k))
          !      END DO
          !    END DO
          !  END DO
          !ELSE  ! For ARPS
    !        tem4(:,:,:) = dzinv
          !END IF
          !USEWRF

        DO k=1,nz-1
          DO j=1,ny-1
            DO i=1,nx-1
              div3(i,j,k) = 1./wgt_div_h(ipass) * j3inv(i,j,k)          &
                         * ( mapfct(i,j,7)                              &
                         * ((tem1(i+1,j,k)-tem1(i,j,k))*dxinv           &
                          +(tem2(i,j+1,k)-tem2(i,j,k))*dyinv))          &
                         +1./wgt_div_v(ipass) * j3inv(i,j,k)            &
                         * ((tem3(i,j,k+1)-tem3(i,j,k))*dzinv )
            END DO
          END DO
        END DO

    ELSE IF( (wgt_div_h(ipass) > 0.0) .AND. (wgt_div_v(ipass) < 0.0)) THEN
        DO k=1,nz-1
          DO j=1,ny-1
            DO i=1,nx-1
              div3(i,j,k) = 1./wgt_div_h(ipass) * j3inv(i,j,k) * ( mapfct(i,j,7) &
                            * ((tem1(i+1,j,k)-tem1(i,j,k))*dxinv        &
                              +(tem2(i,j+1,k)-tem2(i,j,k))*dyinv))
            END DO
          END DO
        END DO
    END IF

    IF (mp_opt > 0) THEN
      !CALL acct_interrupt(mp_acct)
      CALL mpsendrecv2dew(div3, nx, ny, nz, ebc, wbc, 0, tem4)
      CALL mpsendrecv2dns(div3, nx, ny, nz, nbc, sbc, 0, tem4)
      !CALL acct_stop_inter
    END IF

    RETURN
  END SUBROUTINE compute_div3_arps

  !#####################################################################

  SUBROUTINE compute_adjdiv3_arps(ipass,nx,ny,nz,dx,dy,dz,dxinv,dyinv,dzinv,  &
                               u_grd,v_grd,w_grd,div3,                  &
                               mapfct,j1,j2,j3,aj3z,j3inv,              &
                               rhostr,rhostru,rhostrv,rhostrw,          &
                               u,v,w,wcont,                             &
                               tem1,tem2,tem3, tem4,istatus)

    INTEGER, INTENT(IN)  :: ipass
    INTEGER, INTENT(IN)  :: nx,ny,nz
    REAL(P), INTENT(IN)  :: dx, dy, dz
    REAL(P), INTENT(IN)  :: dxinv,dyinv,dzinv

    REAL(P), INTENT(INOUT) :: u_grd(nx,ny,nz),v_grd(nx,ny,nz),w_grd(nx,ny,nz)

    REAL(P), INTENT(IN)  :: mapfct(nx,ny,8)   ! Map factors at scalar, u and v points

    REAL(P), INTENT(IN)  :: j1    (nx,ny,nz)  ! Coordinate transformation Jacobian
                                 ! defined as - d( zp )/d( x ).
    REAL(P), INTENT(IN)  :: j2    (nx,ny,nz)  ! Coordinate transformation Jacobian
                                 ! defined as - d( zp )/d( y ).
    REAL(P), INTENT(IN)  :: j3    (nx,ny,nz)  ! Coordinate transformation Jacobian
                                 ! defined as d( zp )/d( z ).
    REAL(P), INTENT(IN)  :: aj3z  (nx,ny,nz)  ! Coordinate transformation Jacobian defined
                                 ! as d( zp )/d( z ) AVERAGED IN THE Z-DIR.
    REAL(P), INTENT(IN)  :: j3inv (nx,ny,nz)     ! Inverse of j3

    REAL(P), INTENT(IN)  :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3

    REAL(P), INTENT(IN)  :: rhostru(nx,ny,nz)    ! Averaged rhostr at u points (kg/m**3).
    REAL(P), INTENT(IN)  :: rhostrv(nx,ny,nz)    ! Averaged rhostr at v points (kg/m**3).
    REAL(P), INTENT(IN)  :: rhostrw(nx,ny,nz)    ! Averaged rhostr at w points (kg/m**3).

    REAL(P), INTENT(IN)  :: div3(nx,ny,nz)
    REAL(P), INTENT(INOUT) :: u(nx,ny,nz), v(nx,ny,nz), w(nx,ny,nz)
    REAL(P), INTENT(INOUT) :: wcont(nx,ny,nz)
    REAL(P), INTENT(INOUT) :: tem1(nx,ny,nz), tem2(nx,ny,nz)
    REAL(P), INTENT(INOUT) :: tem3(nx,ny,nz), tem4(nx,ny,nz)

    INTEGER, INTENT(OUT) :: istatus

  !-----------------------------------------------------------------------
    INTEGER :: i,j,k

    INCLUDE 'bndry.inc'     ! to include ebc, nbc, sbc, wbc etc.
    INCLUDE 'mp.inc'

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ! tem1 is on U grid, tem2 on V grid, tem3 on W grid
    tem1(:,:,:)  = 0.0
    tem2(:,:,:)  = 0.0
    tem3(:,:,:)  = 0.0
    tem4(:,:,:)  = 0.0

    IF( (wgt_div_h(ipass) > 0.0) .and. (wgt_div_v(ipass) > 0.0)) THEN

      !USEWRF
      !IF (modelopt == 2) THEN  ! for WRF
      !  DO k = 1, nz-1
      !    DO j=1, ny-1
      !      DO i=1, nx-1
      !        tem4(i,j,k) = 1.0/(zp(i,j,k+1)-zp(i,j,k))
      !      END DO
      !    END DO
      !  END DO
      !ELSE  ! For ARPS
     !   tem4(:,:,:) = dzinv
      !END IF
      !USEWRF

      DO k=1,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            tem1(i+1,j,k)= tem1(i+1,j,k) + 1./wgt_div_h(ipass)*j3inv(i,j,k)*    &
                                              mapfct(i,j,7)*div3(i,j,k)*dxinv
            tem1(i  ,j,k)= tem1(i  ,j,k) - 1./wgt_div_h(ipass)*j3inv(i,j,k)*    &
                                              mapfct(i,j,7)*div3(i,j,k)*dxinv

            tem2(i,j+1,k)= tem2(i,j+1,k) + 1./wgt_div_h(ipass)*j3inv(i,j,k)*    &
                                              mapfct(i,j,7)*div3(i,j,k)*dyinv
            tem2(i  ,j,k)= tem2(i  ,j,k) - 1./wgt_div_h(ipass)*j3inv(i,j,k)*    &
                                              mapfct(i,j,7)*div3(i,j,k)*dyinv

            tem3(i,j,k+1)= tem3(i,j,k+1) + 1./wgt_div_v(ipass)*j3inv(i,j,k)*    &
                                              div3(i,j,k)*dzinv
            tem3(i  ,j,k)= tem3(i  ,j,k) - 1./wgt_div_v(ipass)*j3inv(i,j,k)*    &
                                              div3(i,j,k)*dzinv
          END DO
        END DO
      END DO
    ELSE IF( (wgt_div_h(ipass) > 0.0) .AND. (wgt_div_v(ipass) < 0.0)) THEN
      DO k=1,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            tem1(i+1,j,k)= tem1(i+1,j,k) + 1./wgt_div_h(ipass)*j3inv(i,j,k)*   &
                                              mapfct(i,j,7)*div3(i,j,k)*dxinv
            tem1(i  ,j,k)= tem1(i  ,j,k) - 1./wgt_div_h(ipass)*j3inv(i,j,k)*   &
                                              mapfct(i,j,7)*div3(i,j,k)*dxinv

            tem2(i,j+1,k)= tem2(i,j+1,k) + 1./wgt_div_h(ipass)*j3inv(i,j,k)*   &
                                              mapfct(i,j,7)*div3(i,j,k)*dyinv
            tem2(i  ,j,k)= tem2(i  ,j,k) - 1./wgt_div_h(ipass)*j3inv(i,j,k)*   &
                                              mapfct(i,j,7)*div3(i,j,k)*dyinv
          END DO
        END DO
      END DO
    END IF

    IF (mp_opt > 0) THEN
      !CALL acct_interrupt(mp_acct)
      CALL mpsendrecv2dew(tem1, nx, ny, nz, ebc, wbc, 1, tem4)
      CALL mpsendrecv2dns(tem1, nx, ny, nz, nbc, sbc, 1, tem4)

      CALL mpsendrecv2dew(tem2, nx, ny, nz, ebc, wbc, 2, tem4)
      CALL mpsendrecv2dns(tem2, nx, ny, nz, nbc, sbc, 2, tem4)
      !CALL acct_stop_inter
    END IF

    !
    ! tem19->wcont_grd
    ! tem18->w_grd, tem17->v_grd, tem16->u_grd (on vector grids)
    !
    u=0.0; v=0.0; w=0.0; wcont=0.0

    IF(wgt_div_v(ipass) > 0.0 ) THEN
      DO k=1,nz
        DO j=1,ny-1
          DO i=1,nx-1
            wcont(i,j,k)=tem3(i,j,k)*rhostrw(i,j,k) !wcont_grd
            tem3(i,j,k)=0.0
          END DO
        END DO
      END DO
    ENDIF

    IF(wgt_div_h(ipass) > 0.0 ) THEN
      DO k=1,nz-1
        DO j=1,ny
          DO i=1,nx-1
            v(i,j,k)=tem2(i,j,k)*rhostrv(i,j,k)*mapfct(i,j,6) !v_grd
            tem2(i,j,k)=0.0
          END DO
        END DO
      END DO

      DO k=1,nz-1
        DO j=1,ny-1
          DO i=1,nx
            u(i,j,k)=tem1(i,j,k)*rhostru(i,j,k)*mapfct(i,j,5) !u_grd
            tem1(i,j,k) = 0.0
          END DO
        END DO
      END DO
    ENDIF

    ! No. 2 message passing inside which is commented out.
    tem1=0.0; tem2=0.0
    CALL adwcontra(nx,ny,nz,u,v,w,mapfct,j1,j2,j3,aj3z,                 &
                   rhostr,rhostru,rhostrv,rhostrw,wcont,tem1,tem2,tem4)

    !
    ! u,v,w from above are at vector points,
    ! should be converted to scalar points
    ! in order to be added to total gradients

     !DO j= 1,ny-1
     !  DO i= 1,nx-1
     !    w(i,j,2)    = w(i,j,2)+w(i,j,1)
     !    w(i,j,nz-1) = w(i,j,nz-1)+w(i,j,nz)
     !  END DO
     !END DO
     !
     !DO k=2,nz-1
     !  DO j= 1,ny-1
     !    DO i= 1,nx-1
     !      w_grd(i,j,k)   = w_grd(i,j,k)   + 0.5*w(i,j,k)
     !      w_grd(i,j,k-1) = w_grd(i,j,k-1) + 0.5*w(i,j,k)
     !    END DO
     !  END DO
     !END DO
     !
     !IF (loc_y == 1) THEN
     !  DO k= 1,nz-1
     !    DO i= 1,nx-1
     !      v(i,2,k)  = v(i,2,k)+v(i,1,k)
     !    END DO
     !  END DO
     !END IF
     !
     !IF (loc_y == nproc_y) THEN
     !  DO k= 1,nz-1
     !    DO i= 1,nx-1
     !      v(i,ny-1,k) = v(i,ny-1,k)+v(i,ny,k)
     !    END DO
     !  END DO
     !END IF
     !
     !DO k= 1,nz-1
     !  DO j= 2,ny-1
     !    DO i= 1,nx-1
     !      v_grd(i,j,k)   = v_grd(i,j,k)   + 0.5*v(i,j,k)
     !      v_grd(i,j-1,k) = v_grd(i,j-1,k) + 0.5*v(i,j,k)
     !    END DO
     !  END DO
     !END DO
     !
     !IF (mp_opt > 0) THEN
     !  !CALL acct_interrupt(mp_acct)
     !  CALL mpsendrecv2dew(v_grd, nx, ny, nz, 1, 1, 0, tem4)
     !  CALL mpsendrecv2dns(v_grd, nx, ny, nz, 1, 1, 0, tem4)
     !  !CALL acct_stop_inter
     !END IF
     !
     !IF (loc_x == 1) THEN
     !  DO k= 1,nz-1
     !    DO j= 1,ny-1
     !      u(2,j,k)  = u(2,j,k)+u(1,j,k)
     !    END DO
     !  END DO
     !END IF
     !
     !IF (loc_x == nproc_x) THEN
     !  DO k= 1,nz-1
     !    DO j= 1,ny-1
     !      u(nx-1,j,k) = u(nx-1,j,k)+u(nx,j,k)
     !    END DO
     !  END DO
     !END IF
     !
     !DO k= 1,nz-1
     !  DO j= 1,ny-1
     !    DO i= 2,nx-1
     !      u_grd(i,j,k)   = u_grd(i,j,k)   + 0.5*u(i,j,k)
     !      u_grd(i-1,j,k) = u_grd(i-1,j,k) + 0.5*u(i,j,k)
     !    END DO
     !  END DO
     !END DO
     !
     !IF (mp_opt > 0) THEN
     !  !CALL acct_interrupt(mp_acct)
     !  CALL mpsendrecv2dew(u_grd, nx, ny, nz, 1, 1, 0, tem4)
     !  CALL mpsendrecv2dns(u_grd, nx, ny, nz, 1, 1, 0, tem4)
     !  !CALL acct_stop_inter
     !END IF

     u_grd = u_grd + u
     v_grd = v_grd + v
     w_grd = w_grd + w

    !DO k = 1, nz-1
    !  DO j = 1, ny-1
    !    DO i = 1, nx-1
    !      u_grd(i,j,k) = u_grd(i,j,k) + 0.5*(tem16(i,j,k)+tem16(i+1,j,k))
    !      v_grd(i,j,k) = v_grd(i,j,k) + 0.5*(tem17(i,j,k)+tem17(i,j+1,k))
    !      w_grd(i,j,k) = w_grd(i,j,k) + 0.5*(tem18(i,j,k)+tem18(i,j,k+1))
    !    END DO
    !  END DO
    !END DO

    RETURN
  END SUBROUTINE compute_adjdiv3_arps

  !#####################################################################

  SUBROUTINE cal_div3_wrf( ipass,nx,ny,nz, rdx, rdy,                    &
                           u_ctr, v_ctr, w_ctr, div,                    &
                           msfuy, msfvx, msftx, msfty,                  &
                           dn, dnw, rdzw,                               &
                           fnm, fnp, cf1, cf2, cf3, zx, zy,             &
                           tmp1, hat,hatavg,istatus )

  !---------------------------------------------------------------------
  !
  !  PURPOSE:
  !  Compute 3D divergence for the WRF model state
  !
  !---------------------------------------------------------------------
  !
  !  AUTHOR:
  !
  !  Y. Wang, (10/28/2016)
  !  Adapted from cal_deform_and_div in module_diffusion_em.f90 of WRFV3.7.1.
  !
  !  MODIFICATION HISTORY:
  !
  !---------------------------------------------------------------------

    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: ipass
    INTEGER, INTENT( IN ) :: nx,ny,nz

    REAL(P),    INTENT( IN ) :: rdx, rdy, cf1, cf2, cf3

    REAL(P), DIMENSION( 1:nz ),         INTENT( IN ) :: fnm, fnp, dn, dnw
    REAL(P), DIMENSION( 1:nx , 1:ny ),  INTENT( IN ) :: msftx, msfty,   &
                                                        msfuy, msfvx

    REAL(P), DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( IN )  ::  zx, zy,rdzw
    REAL(P), DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( IN )  ::  u_ctr, v_ctr, w_ctr   ! on mass grid

    REAL(P), DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( OUT ) :: div

    !REAL(P), DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( INOUT ) ::  u, v, w             ! work array on staggered grid
    REAL(P), DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( INOUT ) :: tmp1, hat, hatavg

    INTEGER, INTENT(OUT) :: istatus

    INCLUDE 'mp.inc'

  !---------------------------------------------------------------------
  !
  ! Local variables.
  !
  !---------------------------------------------------------------------

    INTEGER :: ids, ide, jds, jde, kds, kde,                            &
               ims, ime, jms, jme, kms, kme,                            &
               its, ite, jts, jte, kts, kte

    INTEGER :: i, j, k, ktf, ktes1, ktes2, i_start, i_end, j_start, j_end

    REAL(P) :: tmp, tmpzx, tmpzy, tmpzeta_z, cft1, cft2

    REAL(P), DIMENSION( 1:nx, 1:ny ) :: mm, zzavg, zeta_zd12

! End declarations.
!-----------------------------------------------------------------------

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ids = 2; ide = nx-1;   jds = 2; jde = ny-1;   kds = 2; kde = nz-1
    ims = 1; ime = nx;     jms = 1; jme = ny;     kms = 1; kme = nz
    its = 2; ite = nx-1;   jts = 2; jte = ny-1;   kts = 2; kte = nz-1

    ! First, convert u_ctr, v_ctr, w_ctr from scalar grid to vector grid
    ! of u, v, w

    !DO k= 1,nz-1
    !  DO j= 1,ny-1
    !    DO i= 2,nx-1
    !      !u(i,j,k) = 0.5*(anx(i,j,k,1)+u_ctr(i,j,k)+anx(i-1,j,k,1)+u_ctr(i-1,j,k))
    !      u(i,j,k) = 0.5*( u_ctr(i,j,k) + u_ctr(i-1,j,k) )
    !    END DO
    !  END DO
    !END DO
    !
    !DO k= 1,nz-1
    !  DO j= 1,ny-1
    !    u(1,j,k)  = u(2,j,k)
    !    u(nx,j,k) = u(nx-1,j,k)
    !  END DO
    !END DO
    !
    !DO k= 1,nz-1
    !  DO j= 2,ny-1
    !    DO i= 1,nx-1
    !      !v(i,j,k) = 0.5*(anx(i,j,k,2)+v_ctr(i,j,k)+anx(i,j-1,k,2)+v_ctr(i,j-1,k))
    !      v(i,j,k) = 0.5*( v_ctr(i,j,k) + v_ctr(i,j-1,k) )
    !    END DO
    !  END DO
    !END DO
    !
    !DO k= 1,nz-1
    !  DO i= 1,nx-1
    !    v(i,1,k)  = v(i,2,k)
    !    v(i,ny,k) = v(i,ny-1,k)
    !  END DO
    !END DO
    !
    !DO k=2,nz-1
    !  DO j= 1,ny-1
    !    DO i= 1,nx-1
    !      !w(i,j,k) = 0.5*(anx(i,j,k,6)+w_ctr(i,j,k) + anx(i,j,k-1,6)+w_ctr(i,j,k-1))
    !      w(i,j,k) = 0.5*( w_ctr(i,j,k) + w_ctr(i,j,k-1) )
    !    END DO
    !  END DO
    !END DO
    !
    !DO j= 1,ny-1
    !  DO i= 1,nx-1
    !    w(i,j,1)  = w(i,j,2)
    !    w(i,j,nz) = w(i,j,nz-1)
    !  END DO
    !END DO

    !u = u_ctr
    !v = v_ctr
    !w = w_ctr

    ! Not necessary because halo zone update value in 1 & nx; 1 & ny
    ! but the indices below are all from 2 to nx-1/ny-1 at most
    !IF (mp_opt > 0) THEN
    !  !CALL acct_interrupt(mp_acct)
    !  CALL mpsendrecv2dew(u, nx, ny, nz, 1, 1, 1, tmp1)
    !  CALL mpsendrecv2dns(u, nx, ny, nz, 1, 1, 1, tmp1)
    !
    !  CALL mpsendrecv2dew(v, nx, ny, nz, 1, 1, 2, tmp1)
    !  CALL mpsendrecv2dns(v, nx, ny, nz, 1, 1, 2, tmp1)
    !
    !  !CALL mpsendrecv2dew(w, nx, ny, nz, ebc, wbc, 3, tem4)
    !  !CALL mpsendrecv2dns(w, nx, ny, nz, nbc, sbc, 3, tem4)
    !  !CALL acct_stop_inter
    !END IF

    div = 0.0  ! make sure boundary value does not involve in computation

! Comments 10-MAR-2005
! Treat all differentials as 'div-style' [or 'curl-style'],
! i.e., du/dX becomes (in map coordinate space) mx*my * d(u/my)/dx,
! NB - all equations referred to here are from Chen and Dudhia 2002, from the
! WRF physics documents web pages:
! http://www.mmm.ucar.edu/wrf/users/docs/wrf-doc-physics.pdf

!=======================================================================
! In the following section, calculate 3-d divergence and the first three
! (defor11, defor22, defor33) of six deformation terms.

    ktes1   = kte-1
    ktes2   = kte-2

    cft2    = - 0.5 * dnw(ktes1) / dn(ktes1)
    cft1    = 1.0 - cft2

    ktf     = MIN( kte, kde-1 )

    i_start = its
    i_end   = MIN( ite, ide-1 )
    j_start = jts
    j_end   = MIN( jte, jde-1 )

! Square the map scale factor.

    DO j = j_start, j_end
    DO i = i_start, i_end
      mm(i,j) = msftx(i,j) * msfty(i,j)
    END DO
    END DO

!-----------------------------------------------------------------------
! Calculate du/dx.

! Apply a coordinate transformation to zonal velocity, u_ctr.

!write(0,*) 'U hat', i_start, i_end+1, j_start, j_end, kts, ktf
    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end+1
      hat(i,j,k) = u_ctr(i,j,k) / msfuy(i,j)
    END DO
    END DO
    END DO

! Average in x and z.

!write(0,*) 'U use1', i_start, i_end+1, j_start, j_end, kts, ktf

    DO k=kts+1,ktf
    DO j=j_start,j_end
    DO i=i_start,i_end
      hatavg(i,j,k) = 0.5 *  &
                    ( fnm(k) * ( hat(i,j,k)   + hat(i+1,j,  k) ) +      &
                      fnp(k) * ( hat(i,j,k-1) + hat(i+1,j,k-1) ) )
    END DO
    END DO
    END DO

! Extrapolate to top and bottom of domain (to w levels).

!write(0,*) 'U hatavg', i_start, i_end, j_start, j_end, 1, kte

    DO j = j_start, j_end
    DO i = i_start, i_end
      hatavg(i,j,kts) =  0.5 * (  &
                         cf1 * hat(i,  j,kts  ) +  &
                         cf2 * hat(i,  j,kts+1) +  &
                         cf3 * hat(i,  j,kts+2) +  &
                         cf1 * hat(i+1,j,kts  ) +  &
                         cf2 * hat(i+1,j,kts+1) +  &
                         cf3 * hat(i+1,j,kts+2) )
      hatavg(i,j,kte) =  0.5 * (  &
                        cft1 * ( hat(i,j,ktes1) + hat(i+1,j,ktes1) ) +  &
                        cft2 * ( hat(i,j,ktes2) + hat(i+1,j,ktes2) ) )
    END DO
    END DO

    ! Comments 10-MAR-05
    ! Eqn 13a: D11=defor11= 2m^2 * (partial du^/dX + partial dpsi/dx * partial du^/dpsi)
    ! Below, D11 is set = 2*tmp1
    ! => tmp1 = m^2 * (partial du^/dX + partial dpsi/dx * partial du^/dpsi)
    ! tmpzx = averaged value of dpsi/dx (=zx)

!write(0,*) 'U use2', i_start, i_end, j_start, j_end, kts, ktf+1

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      tmpzx       = 0.25 * (  &
                    zx(i,j,k  ) + zx(i+1,j,k  ) +  &
                    zx(i,j,k+1) + zx(i+1,j,k+1) )
      tmp1(i,j,k) = ( hatavg(i,j,k+1) - hatavg(i,j,k) ) *tmpzx * rdzw(i,j,k)
      ! tmp1 to here = partial dpsi/dx * partial du^/dpsi:
    END DO
    END DO
    END DO

!write(0,*) 'U use3', i_start, i_end+1, j_start, j_end, kts, ktf

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      tmp1(i,j,k) = mm(i,j) * ( rdx * ( hat(i+1,j,k) - hat(i,j,k) ) -   &
                                tmp1(i,j,k) )
    END DO
    END DO
    END DO

! End calculation of du/dx.
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
! Calculate zonal divergence (du/dx) and add it to the divergence array.

    IF(wgt_div_h(ipass) > 0.0 ) THEN
      DO k = kts, ktf
      DO j = j_start, j_end
      DO i = i_start, i_end
        div(i,j,k) = tmp1(i,j,k) /wgt_div_h(ipass)
      END DO
      END DO
      END DO
    END IF
! End calculation of zonal divergence.
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
! Calculate dv/dy.

! Apply a coordinate transformation to meridional velocity, v.

    DO k = kts, ktf
    DO j = j_start, j_end+1
    DO i = i_start, i_end
      !WYH ! Because msfvx at the poles will be undefined (1./0.), we will have
      !WYH ! trouble.  But we are OK since v at the poles is 0., and that takes
      !WYH ! precedence in this case.
      !WYH IF ((config_flags%polar) .AND. ((j == jds) .OR. (j == jde))) THEN
      !WYH    hat(i,k,j) = 0.
      !WYH ELSE ! normal code
      hat(i,j,k) = v_ctr(i,j,k) / msfvx(i,j)
      !WYH ENDIF
    END DO
    END DO
    END DO

! Account for the slope in y of eta surfaces.

    DO k=kts+1,ktf
    DO j=j_start,j_end
    DO i=i_start,i_end
      hatavg(i,j,k) = 0.5 * (  &
                      fnm(k) * ( hat(i,j,k  ) + hat(i,j+1,k  ) ) +      &
                      fnp(k) * ( hat(i,j,k-1) + hat(i,j+1,k-1) ) )
    END DO
    END DO
    END DO

! Extrapolate to top and bottom of domain (to w levels).

    DO j = j_start, j_end
    DO i = i_start, i_end
      hatavg(i,j,kts)   =  0.5 * (  &
                         cf1 * hat(i,j,  kts    ) +  &
                         cf2 * hat(i,j,  kts+1  ) +  &
                         cf3 * hat(i,j,  kts+2  ) +  &
                         cf1 * hat(i,j+1,kts    ) +  &
                         cf2 * hat(i,j+1,kts+2  ) +  &
                         cf3 * hat(i,j+1,kts+3  ) )
      hatavg(i,j,kte) =  0.5 * (  &
                        cft1 * ( hat(i,j,ktes1) + hat(i,j+1,ktes1) ) +  &
                        cft2 * ( hat(i,j,ktes2) + hat(i,j+1,ktes2) ) )
    END DO
    END DO

    ! Comments 10-MAR-05
    ! Eqn 13b: D22=defor22= 2m^2 * (partial dv^/dY + partial dpsi/dy * partial dv^/dpsi)
    ! Below, D22 is set = 2*tmp1
    ! => tmp1 = m^2 * (partial dv^/dY + partial dpsi/dy * partial dv^/dpsi)
    ! tmpzy = averaged value of dpsi/dy (=zy)

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      tmpzy       =  0.25 * (  &
                     zy(i,j,k  ) + zy(i,j+1,k  ) +  &
                     zy(i,j,k+1) + zy(i,j+1,k+1)  )
      tmp1(i,j,k) = ( hatavg(i,j,k+1) - hatavg(i,j,k) ) * tmpzy * rdzw(i,j,k)
      ! tmp1 to here = partial dpsi/dy * partial dv^/dpsi:
    END DO
    END DO
    END DO

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      tmp1(i,j,k) = mm(i,j) * (  &
                    rdy * ( hat(i,j+1,k) - hat(i,j,k) ) - tmp1(i,j,k) )
    END DO
    END DO
    END DO

! End calculation of dv/dy.
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
! Calculate meridional divergence (dv/dy) and add it to the divergence
! array.

    IF(wgt_div_h(ipass) > 0.0 ) THEN
      DO k = kts, ktf
      DO j = j_start, j_end
      DO i = i_start, i_end
        div(i,j,k) = div(i,j,k) + tmp1(i,j,k)/wgt_div_h(ipass)
      END DO
      END DO
      END DO
    END IF

! End calculation of meridional divergence.
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
! Comments 10-MAR-05
! Eqn 13c: D33=defor33= 2 * partial dw/dz
! Below, D33 is set = 2*tmp1
! => tmp1 = partial dw/dz

! Calculate dw/dz.

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      tmp1(i,j,k) = ( w_ctr(i,j,k+1) - w_ctr(i,j,k) ) * rdzw(i,j,k)
    END DO
    END DO
    END DO

! End calculation of dw/dz.
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
! Calculate vertical divergence (dw/dz) and add it to the divergence
! array.

    IF(wgt_div_v(ipass) > 0.0 ) THEN
      DO k = kts, ktf
      DO j = j_start, j_end
      DO i = i_start, i_end
        div(i,j,k) = div(i,j,k) + tmp1(i,j,k)/wgt_div_v(ipass)
      END DO
      END DO
      END DO
    END IF

! End calculation of vertical divergence.
!-----------------------------------------------------------------------

! Three-dimensional divergence is now finished and values are in array
! "div."  Also, the first three (defor11, defor22, defor33) of six
! deformation terms are now calculated at pressure points.
!=======================================================================

    IF (mp_opt > 0) THEN
      !CALL acct_interrupt(mp_acct)
      CALL mpsendrecv2dew(div, nx, ny, nz, 1, 1, 0, tmp1)
      CALL mpsendrecv2dns(div, nx, ny, nz, 1, 1, 0, tmp1)
      !CALL acct_stop_inter
    END IF

    RETURN
  END SUBROUTINE cal_div3_wrf

  !#####################################################################

  SUBROUTINE cal_adjdiv3_wrf( ipass,nx,ny,nz,rdx, rdy,                  &
                              u_grd, v_grd, w_grd, div,                 &
                              msfuy, msfvx, msftx, msfty,               &
                              dn, dnw, rdzw,                            &
                              fnm, fnp, cf1, cf2, cf3, zx, zy,          &
                              u,v,w,tmp1, hat, hatavg,istatus )

  !---------------------------------------------------------------------
  !
  !  PURPOSE:
  !
  !  Adjunt subroutine of cal_div3_wrf
  !
  !---------------------------------------------------------------------
  !
  !  AUTHOR:
  !
  !
  !  MODIFICATION HISTORY:
  !
  !---------------------------------------------------------------------

    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: ipass

    INTEGER, INTENT( IN ) :: nx,ny,nz

    REAL(P), INTENT( IN ) :: rdx, rdy, cf1, cf2, cf3

    REAL(P), DIMENSION( 1:nz ),        INTENT( IN ) :: fnm, fnp, dn, dnw

    REAL(P), DIMENSION( 1:nx , 1:ny ), INTENT( IN ) :: msftx, msfty,    &
                                                       msfvx, msfuy

    REAL(P), DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( INOUT ) :: u_grd, v_grd, w_grd
    REAL(P), DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( IN )  :: zx, zy, rdzw
    REAL(P), DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( IN )  :: div

    REAL(P), DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( INOUT ) :: u, v, w
    REAL(P), DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( INOUT ) :: tmp1, hat, hatavg

    INTEGER, INTENT(OUT) :: istatus

    INCLUDE 'mp.inc'

  !---------------------------------------------------------------------
  !
  ! Local variables.
  !
  !---------------------------------------------------------------------

    INTEGER :: ids, ide, jds, jde, kds, kde,                            &
               ims, ime, jms, jme, kms, kme,                            &
               its, ite, jts, jte, kts, kte

    INTEGER :: i, j, k, ktf, ktes1, ktes2, i_start, i_end, j_start, j_end

    REAL(P)    :: tmp, tmpzx, tmpzy, tmpzeta_z, cft1, cft2

    REAL(P), DIMENSION( 1:nx, 1:ny ) :: mm, zzavg, zeta_zd12

! End declarations.
!-----------------------------------------------------------------------

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    ids = 1; ide = nx;   jds = 1; jde = ny;   kds = 1; kde = nz-1
    ims = 1; ime = nx;   jms = 1; jme = ny;   kms = 1; kme = nz
    its = 1; ite = nx;   jts = 1; jte = ny;   kts = 1; kte = nz-1


! Comments 10-MAR-2005
! Treat all differentials as 'div-style' [or 'curl-style'],
! i.e., du/dX becomes (in map coordinate space) mx*my * d(u/my)/dx,
! NB - all equations referred to here are from Chen and Dudhia 2002, from the
! WRF physics documents web pages:
! http://www.mmm.ucar.edu/wrf/users/docs/wrf-doc-physics.pdf

!=======================================================================
! In the following section, calculate 3-d divergence and the first three
! (defor11, defor22, defor33) of six deformation terms.

    ktes1   = kte-1
    ktes2   = kte-2

    cft2    = - 0.5 * dnw(ktes1) / dn(ktes1)
    cft1    = 1.0 - cft2

    ktf     = MIN( kte, kde-1 )

    i_start = its
    i_end   = MIN( ite, ide-1 )
    j_start = jts
    j_end   = MIN( jte, jde-1 )

!Jidong, Fill adjunct code here
!-----------------------------------------------------------------------
! NOT sure how to deal with this for adjoint code
! Three-dimensional divergence is now finished and values are in array
! "div."  Also, the first three (defor11, defor22, defor33) of six
! deformation terms are now calculated at pressure points.
!=======================================================================

    !IF (mp_opt > 0) THEN
    !  !CALL acct_interrupt(mp_acct)
    !  CALL mpsendrecv2dew(div, nx, ny, nz, 1, 1, 0, tmp1)
    !  CALL mpsendrecv2dns(div, nx, ny, nz, 1, 1, 0, tmp1)
    !  !CALL acct_stop_inter
    !END IF

! Square the map scale factor.

    DO j = j_start, j_end
    DO i = i_start, i_end
      mm(i,j) = msftx(i,j) * msfty(i,j)
    END DO
    END DO

    u = 0.0
    v = 0.0
    w = 0.0

    tmp1 = 0.0
!-----------------------------------------------------------------------
! Calculate ADJOINT of vertical divergence (dw/dz) and add it to the divergence
! array.

    IF(wgt_div_v(ipass) > 0.0 ) THEN
      DO k = kts, ktf
      DO j = j_start, j_end
      DO i = i_start, i_end
        tmp1(i,j,k) = div(i,j,k)/wgt_div_v(ipass)
      END DO
      END DO
      END DO
    END IF

!-----------------------------------------------------------------------
! Comments 10-MAR-05
! Eqn 13c: D33=defor33= 2 * partial dw/dz
! Below, D33 is set = 2*tmp1
! => tmp1 = partial dw/dz

! Calculate Adjoint of dw/dz.

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
!     tmp1(i,j,k) = ( w(i,j,k+1) - w(i,j,k) ) * rdzw(i,j,k)
      w(i,j,k+1) = w(i,j,k+1)+tmp1(i,j,k)* rdzw(i,j,k)
      w(i,j,k)   = w(i,j,k)  -tmp1(i,j,k)* rdzw(i,j,k)
      tmp1(i,j,k) = 0.0
    END DO
    END DO
    END DO

! End calculation of dw/dz.
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
! Calculate ADJOINT of meridional divergence (dv/dy) and add it to the divergence
! array.

    IF(wgt_div_h(ipass) > 0.0 ) THEN
      DO k = kts, ktf
      DO j = j_start, j_end
      DO i = i_start, i_end
        tmp1(i,j,k) = div(i,j,k)/wgt_div_h(ipass)
      END DO
      END DO
      END DO
    ELSE
      tmp1(:,:,:) = 0.0
    END IF

    ! Comments 10-MAR-05
    ! Eqn 13b: D22=defor22= 2m^2 * (partial dv^/dY + partial dpsi/dy * partial dv^/dpsi)
    ! Below, D22 is set = 2*tmp1
    ! => tmp1 = m^2 * (partial dv^/dY + partial dpsi/dy * partial dv^/dpsi)
    ! tmpzy = averaged value of dpsi/dy (=zy)

    hat(:,:,:) = 0.0

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
!     tmp1(i,j,k) = mm(i,j) * (  &
!                   rdy * ( hat(i,j+1,k) - hat(i,j,k) ) - tmp1(i,j,k) )
      hat(i,j+1,k)=hat(i,j+1,k)+mm(i,j) * rdy * tmp1(i,j,k)
      hat(i,j,k)  =  hat(i,j,k)-mm(i,j) * rdy * tmp1(i,j,k)
      tmp1(i,j,k) =            -mm(i,j) * tmp1(i,j,k)
    END DO
    END DO
    END DO

    hatavg(:,:,:) = 0.0
    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      tmpzy       =  0.25 * (  &
                     zy(i,j,k  ) + zy(i,j+1,k  ) +  &
                     zy(i,j,k+1) + zy(i,j+1,k+1)  )
!     tmp1(i,j,k) = ( hatavg(i,j,k+1) - hatavg(i,j,k) ) * tmpzy * rdzw(i,j,k)
      hatavg(i,j,k+1)=hatavg(i,j,k+1)+tmp1(i,j,k) * tmpzy * rdzw(i,j,k)
      hatavg(i,j,k)  =hatavg(i,j,k)  -tmp1(i,j,k) * tmpzy * rdzw(i,j,k)
      tmp1(i,j,k) = 0.0
    END DO
    END DO
    END DO


! Extrapolate to top and bottom of domain (to w levels).

    DO j = j_start, j_end
    DO i = i_start, i_end
!     hatavg(i,j,kte) =  0.5 * (  &
!                       cft1 * ( hat(i,j,ktes1) + hat(i,j+1,ktes1) ) +  &
!                       cft2 * ( hat(i,j,ktes2) + hat(i,j+1,ktes2) ) )
      hat(i,j,  ktes1) = hat(i,j,  ktes1) +0.5 * cft1 *hatavg(i,j,kte)
      hat(i,j+1,ktes1) = hat(i,j+1,ktes1) +0.5 * cft1 *hatavg(i,j,kte)
      hat(i,j,  ktes2) = hat(i,j,  ktes2) +0.5 * cft2 *hatavg(i,j,kte)
      hat(i,j+1,ktes2) = hat(i,j+1,ktes2) +0.5 * cft2 *hatavg(i,j,kte)
      hatavg(i,j,kte) = 0.0
!     hatavg(i,j,1)   =  0.5 * (  &
!                        cf1 * hat(i,j,1  ) +  &
!                        cf2 * hat(i,j,2  ) +  &
!                        cf3 * hat(i,j,3  ) +  &
!                        cf1 * hat(i,j+1,1) +  &
!                        cf2 * hat(i,j+1,2) +  &
!                        cf3 * hat(i,j+1,3) )
     hat(i,j,  kts  )=hat(i,j,  kts  )+0.5 *cf1 *hatavg(i,j,kts)
     hat(i,j,  kts+1)=hat(i,j,  kts+1)+0.5 *cf2 *hatavg(i,j,kts)
     hat(i,j,  kts+2)=hat(i,j,  kts+2)+0.5 *cf3 *hatavg(i,j,kts)
     hat(i,j+1,kts  )=hat(i,j+1,kts  )+0.5 *cf1 *hatavg(i,j,kts)
     hat(i,j+1,kts+1)=hat(i,j+1,kts+1)+0.5 *cf2 *hatavg(i,j,kts)
     hat(i,j+1,kts+2)=hat(i,j+1,kts+2)+0.5 *cf3 *hatavg(i,j,kts)
     hatavg(i,j,kts) = 0.0
    END DO
    END DO

! Account for the slope in y of eta surfaces.

    DO k=kts+1,ktf-1
    DO j=j_start,j_end
    DO i=i_start,i_end
!     hatavg(i,j,k) = 0.5 * (  &
!                     fnm(k) * ( hat(i,j,k  ) + hat(i,j+1,k  ) ) +      &
!                     fnp(k) * ( hat(i,j,k-1) + hat(i,j+1,k-1) ) )
      hat(i,j,k  )  =hat(i,j,k  )  + 0.5 * fnm(k) *hatavg(i,j,k)
      hat(i,j+1,k  )=hat(i,j+1,k  )+ 0.5 * fnm(k) *hatavg(i,j,k)
      hat(i,j,k-1)  =hat(i,j,k-1)  + 0.5 * fnp(k) *hatavg(i,j,k)
      hat(i,j+1,k-1)=hat(i,j+1,k-1)+ 0.5 * fnp(k) *hatavg(i,j,k)
      hatavg(i,j,k)=0.0
    END DO
    END DO
    END DO
!-----------------------------------------------------------------------
! Calculate ADJOINT of dv/dy.

! Apply a coordinate transformation to meridional velocity, v.

    DO k = kts, ktf
    DO j = j_start, j_end+1
    DO i = i_start, i_end
      !WYH ! Because msfvx at the poles will be undefined (1./0.), we will have
      !WYH ! trouble.  But we are OK since v at the poles is 0., and that takes
      !WYH ! precedence in this case.
      !WYH IF ((config_flags%polar) .AND. ((j == jds) .OR. (j == jde))) THEN
      !WYH    hat(i,k,j) = 0.
      !WYH ELSE ! normal code
      !hat(i,j,k) = v(i,j,k) / msfvx(i,j)
      v(i,j,k) = v(i,j,k) + hat(i,j,k) / msfvx(i,j)
      hat(i,j,k)= 0.0
      !WYH ENDIF
    END DO
    END DO
    END DO

!-----------------------------------------------------------------------
! Calculate the ADJOINT of zonal divergence (du/dx) and add it to the divergence array.

    IF(wgt_div_h(ipass) > 0.0 ) THEN
      DO k = kts, ktf
      DO j = j_start, j_end
      DO i = i_start, i_end
        !div(i,j,k) = tmp1(i,j,k)
        tmp1(i,j,k) = div(i,j,k)/wgt_div_h(ipass)
      END DO
      END DO
      END DO
    ELSE
      tmp1(:,:,:) = 0.0
    END IF

! End calculation of zonal divergence.
!-----------------------------------------------------------------------

    ! Comments 10-MAR-05
    ! Eqn 13a: D11=defor11= 2m^2 * (partial du^/dX + partial dpsi/dx * partial du^/dpsi)
    ! Below, D11 is set = 2*tmp1
    ! => tmp1 = m^2 * (partial du^/dX + partial dpsi/dx * partial du^/dpsi)
    ! ADJOINT of tmpzx = averaged value of dpsi/dx (=zx)

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
!     tmp1(i,j,k) = mm(i,j) * ( rdx * ( hat(i+1,j,k) - hat(i,j,k) ) -   &
!                   tmp1(i,j,k))
      hat(i+1,j,k)=hat(i+1,j,k)+mm(i,j) *rdx *tmp1(i,j,k)
      hat(i,j,k)  =hat(i,j,k)  -mm(i,j) *rdx *tmp1(i,j,k)
      tmp1(i,j,k) =            -mm(i,j) *tmp1(i,j,k)
    END DO
    END DO
    END DO

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      tmpzx       = 0.25 * (  &
                    zx(i,j,k  ) + zx(i+1,j,k  ) +  &
                    zx(i,j,k+1) + zx(i+1,j,k+1) )
!     tmp1(i,j,k) = ( hatavg(i,j,k+1) - hatavg(i,j,k) ) *tmpzx * rdzw(i,j,k)
      ! tmp1 to here = partial dpsi/dx * partial du^/dpsi:
      hatavg(i,j,k+1)=hatavg(i,j,k+1)+tmp1(i,j,k)*tmpzx * rdzw(i,j,k)
      hatavg(i,j,k)  =hatavg(i,j,k)  -tmp1(i,j,k)*tmpzx * rdzw(i,j,k)
      tmp1(i,j,k) = 0.0
    END DO
    END DO
    END DO

!-----------------------------------------------------------------------
! Calculate ADJOINT of du/dx.

! Extrapolate to top and bottom of domain (to w levels).

    DO j = j_start, j_end
    DO i = i_start, i_end
!     hatavg(i,j,1)   =  0.5 * (  &
!                        cf1 * hat(i,  j,1) +  &
!                        cf2 * hat(i,  j,2) +  &
!                        cf3 * hat(i,  j,3) +  &
!                        cf1 * hat(i+1,j,1) +  &
!                        cf2 * hat(i+1,j,2) +  &
!                        cf3 * hat(i+1,j,3) )
      hat(i,  j,kts  )=hat(i,  j,kts  )+0.5 *cf1 *hatavg(i,j,kts)
      hat(i,  j,kts+1)=hat(i,  j,kts+1)+0.5 *cf2 *hatavg(i,j,kts)
      hat(i,  j,kts+2)=hat(i,  j,kts+2)+0.5 *cf3 *hatavg(i,j,kts)
      hat(i+1,j,kts  )=hat(i+1,j,kts  )+0.5 *cf1 *hatavg(i,j,kts)
      hat(i+1,j,kts+1)=hat(i+1,j,kts+1)+0.5 *cf2 *hatavg(i,j,kts)
      hat(i+1,j,kts+2)=hat(i+1,j,kts+2)+0.5 *cf3 *hatavg(i,j,kts)
      hatavg(i,j,kts)=0.0

!     hatavg(i,j,kte) =  0.5 * (  &
!                       cft1 * ( hat(i,j,ktes1) + hat(i+1,j,ktes1) ) +  &
!                       cft2 * ( hat(i,j,ktes2) + hat(i+1,j,ktes2) ) )
      hat(i,  j,ktes1) = hat(i,  j,ktes1) +0.5 *cft1 *hatavg(i,j,kte)
      hat(i+1,j,ktes1) = hat(i+1,j,ktes1) +0.5 *cft1 *hatavg(i,j,kte)
      hat(i,  j,ktes2) = hat(i,  j,ktes2) +0.5 *cft2 *hatavg(i,j,kte)
      hat(i+1,j,ktes2) = hat(i+1,j,ktes2) +0.5 *cft2 *hatavg(i,j,kte)
      hatavg(i,j,kte)=0.0

    END DO
    END DO
! Average in x and z.

    DO k=kts+1,ktf
    DO j=j_start,j_end
    DO i=i_start,i_end
!     hatavg(i,j,k) = 0.5 *  &
!                   ( fnm(k) * ( hat(i,j,k)   + hat(i+1,j,  k) ) +      &
!                     fnp(k) * ( hat(i,j,k-1) + hat(i+1,j,k-1) ) )

      hat(i,  j,k  ) = hat(i,  j,k  ) + 0.5 *fnm(k) *hatavg(i,j,k)
      hat(i+1,j,k  ) = hat(i+1,j,k  ) + 0.5 *fnm(k) *hatavg(i,j,k)
      hat(i,  j,k-1) = hat(i,  j,k-1) + 0.5 *fnp(k) *hatavg(i,j,k)
      hat(i+1,j,k-1) = hat(i+1,j,k-1) + 0.5 *fnp(k) *hatavg(i,j,k)
      hatavg(i,j,k)  = 0.0
    END DO
    END DO
    END DO
! Apply a coordinate transformation to zonal velocity, u.

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end+1
!     hat(i,j,k) = u(i,j,k) / msfuy(i,j)
      u(i,j,k) = u(i,j,k)+ hat(i,j,k) / msfuy(i,j)
      hat(i,j,k) = 0.0
    END DO
    END DO
    END DO

    !
    ! u,v,w from above are at vector points,
    ! should be converted to scalar points
    ! in order to be added to total gradients

     !DO j= 1,ny-1
     !  DO i= 1,nx-1
     !    w(i,j,2)    = w(i,j,2)+w(i,j,1)
     !    w(i,j,nz-1) = w(i,j,nz-1)+w(i,j,nz)
     !  END DO
     !END DO
     !
     !DO k=2,nz-1
     !  DO j= 1,ny-1
     !    DO i= 1,nx-1
     !      w_grd(i,j,k)   = w_grd(i,j,k)   + 0.5*w(i,j,k)
     !      w_grd(i,j,k-1) = w_grd(i,j,k-1) + 0.5*w(i,j,k)
     !    END DO
     !  END DO
     !END DO
     !
     !IF (loc_y == 1) THEN
     !  DO k= 1,nz-1
     !    DO i= 1,nx-1
     !      v(i,2,k)  = v(i,2,k)+v(i,1,k)
     !    END DO
     !  END DO
     !END IF
     !
     !IF (loc_y == nproc_y) THEN
     !  DO k= 1,nz-1
     !    DO i= 1,nx-1
     !      v(i,ny-1,k) = v(i,ny-1,k)+v(i,ny,k)
     !    END DO
     !  END DO
     !END IF
     !
     !DO k= 1,nz-1
     !  DO j= 2,ny-1
     !    DO i= 1,nx-1
     !      v_grd(i,j,k)   = v_grd(i,j,k)   + 0.5*v(i,j,k)
     !      v_grd(i,j-1,k) = v_grd(i,j-1,k) + 0.5*v(i,j,k)
     !    END DO
     !  END DO
     !END DO
     !
     !IF (mp_opt > 0) THEN
     !  !CALL acct_interrupt(mp_acct)
     !  CALL mpsendrecv2dew(v_grd, nx, ny, nz, 1, 1, 0, tmp1)
     !  CALL mpsendrecv2dns(v_grd, nx, ny, nz, 1, 1, 0, tmp1)
     !  !CALL acct_stop_inter
     !END IF
     !
     !
     !IF (loc_x == 1) THEN
     !  DO k= 1,nz-1
     !    DO j= 1,ny-1
     !      u(2,j,k)  = u(2,j,k)+u(1,j,k)
     !    END DO
     !  END DO
     !END IF
     !
     !IF (loc_x == nproc_x) THEN
     !  DO k= 1,nz-1
     !    DO j= 1,ny-1
     !      u(nx-1,j,k) = u(nx-1,j,k)+u(nx,j,k)
     !    END DO
     !  END DO
     !END IF
     !
     !DO k= 1,nz-1
     !  DO j= 1,ny-1
     !    DO i= 2,nx-1
     !      u_grd(i,j,k)   = u_grd(i,j,k)   + 0.5*u(i,j,k)
     !      u_grd(i-1,j,k) = u_grd(i-1,j,k) + 0.5*u(i,j,k)
     !    END DO
     !  END DO
     !END DO
     !
     !IF (mp_opt > 0) THEN
     !  !CALL acct_interrupt(mp_acct)
     !  CALL mpsendrecv2dew(u_grd, nx, ny, nz, 1, 1, 0, tmp1)
     !  CALL mpsendrecv2dns(u_grd, nx, ny, nz, 1, 1, 0, tmp1)
     !  !CALL acct_stop_inter
     !END IF

     u_grd = u_grd + u
     v_grd = v_grd + v
     w_grd = w_grd + w

! End calculation of meridional divergence.
!-----------------------------------------------------------------------

    RETURN
  END SUBROUTINE cal_adjdiv3_wrf

END MODULE constraint_divergence
