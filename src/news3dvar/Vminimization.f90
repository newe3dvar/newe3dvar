!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE MINIMIZATION               ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Main driver for gradient check and minimization.
!
!-----------------------------------------------------------------------
!
!  AUTHOR:
!
!  Jidong Gao, CAPS, July, 2000
!
!  MODIFICATIONS:
!
!  Yunheng Wang (11/01/2007)
!  Added two more temporary arrays for parallelizing recursive filter.
!
!-----------------------------------------------------------------------
!
SUBROUTINE minimization(nx,ny,nz,nvar,nzua,nzret,                       &
           mapfct,j1,j2,j3,aj3x,aj3y,aj3z,j3inv,rhobar,                 &
           dnw,dn,fnm,fnp,cf1,cf2,cf3,                                  &
           mxsng,mxua,mxret,mxcolret,                                   &
           nsrcsng,nsrcua,nsrcret,ncat,                                 &
           ipass,xs,ys,zs,x,y,z,zp,hterain,                             &
           icatg,xcor,nam_var,                                          &
           indexsng,usesng,xsng,ysng,hgtsng,thesng,                     &
           odifsng,qobsng,qualsng,isrcsng,nlevsng,nobsng,               &
           indexua,useua,xua,yua,hgtua,theua,                           &
           odifua,qobsua,qualua,isrcua,nlevsua,nobsua,                  &
           ref_mos_3d,                                                  &
           xretc,yretc,hgtretc,theretc,                                 &
           odifret,qobsret,qualret,                                     &
           iret,isrcret,nlevret,ncolret,                                &
           srcsng,srcua,srcret,                                         &
           iusesng,iuseua,iuseret,                                      &
           corsng,corua,corret,oanxsng,oanxua,oanxret,                  &
           nz_tab,hqback,qback,                                         &
           nsngfil,nuafil,nretfil,                                      &
           anx,tk,rhostr,                                               &
           tem1,tem2,tem3,tem4,tem5,tem6,tem4d,                         &
           tem7,tem8,tem9,tem10,tem11,tem12,tem13,tem14,tem15,istatus)
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx         Number of grid points in the x-direction (east/west)
!    ny         Number of grid points in the y-direction (north/south)
!    nz         Number of grid points in the vertical
!    nvar       Number of analysis variables
!    nzua       Maximumnumber of levels in UA observations
!    nzret      Maximum number of levels in a retrieval column
!    mxsng      Maximum number of single level data
!    mxua       Maximum number of upper air observations
!    mxcolret   Maximum number of retrieval columns
!    ipass      Number of current iterations
!
!    xs         x location of scalar pts (m)
!    ys         y location of scalar pts (m)
!    zs         z location of scalar pts (m)
!
!    xsng       x location of single-level data
!    ysng       y location of single-level data
!    hgtsng     elevation of single-level data
!    thesng     theta (potential temperature) of single-level data
!
!    obsng      single-level observations
!    odifsng    difference between single-level obs and analysis
!    qobsng     normalized observation error
!    qualsng    single-level data quality indicator
!    isrcsng    index of single-level data source
!    nobsng     number of single-level observations
!
!    xua        x location of upper air data
!    yua        y location of upper air data
!    hgtua      elevation of upper air data
!    theua      theta (potential temperature) of upper air data
!
!    obsua      upper air observations
!    odifua     difference between upper air obs and analysis
!    qobsua     normalized observation error
!    qualua     upper air data quality indicator
!    isrcua     index of upper air data source
!    nlevsua    number of levels of data for each upper air location
!    nobsua     number of upper air observations
!
!    anx        Analyzed variables (or first guess)
!    qback      Background (first guess) error
!
!    istatus  status indicator
!
!    latret   latitude of retrieval radar  (degrees N)
!    lonret   longitude of retrieval radar (degrees E)
!    xretc    x location of retrieval column
!    yretc    y location of retrieval column
!    iret     retrieval number
!    nlevret  number of levels of retrieval data in each column
!    hgtretc  height (m MSL) of retrieval observations
!    obsret   retrieval observations
!    odifret  difference between retrieval observation and analysis
!    qobsret  normalized observation error
!    qualret  retrieval data quality indicator
!    ncolret  number of retr columns read-in
!    istatus  status indicator
!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  !USE model_precision
  !USE module_varpara
  USE Vmodule_3dvar

  USE module_radar_grid, ONLY: radargrid_init, radargrid_final
  USE module_aeri  !JJ
  USE module_ensemble
  USE module_convobs,   ONLY : convobs_set_usage, convsw
  USE module_radarobs,  ONLY : grdtlt_flag, radarobs_set_usage
  USE module_cwpobs,    ONLY : cwpobs_set_usage
  USE module_tpwobs,    ONLY : tpwobs_set_usage
  USE module_lightning, ONLY : lightning_set_usage

  USE constraint_divergence
  USE constraint_diagnostic_div
  USE constraint_mslp
  USE constraint_thermo
  USE constraint_smooth

  IMPLICIT NONE

  INCLUDE 'phycst.inc'
  INCLUDE 'grid.inc'
  !INCLUDE 'varpara.inc'

  !INCLUDE '3dvarcputime.inc'

  INCLUDE 'bndry.inc'   ! to include web, ebc, nbc, sbc etc.
  INCLUDE 'globcst.inc' ! to include mp_acct
  INCLUDE 'mp.inc'      ! to include mp_opt, myproc

  REAL :: cputimearray(10)
  COMMON /cputime3dvar/ cputimearray

!-----------------------------------------------------------------------
!
!  Input Sizing Arguments
!
!-----------------------------------------------------------------------
!
  INTEGER, INTENT(IN) :: nx,ny,nz
  INTEGER, INTENT(IN) :: nvar
  INTEGER, INTENT(IN) :: nzua,nzret
  INTEGER, INTENT(IN) :: mxsng,mxua,mxret,mxcolret
  INTEGER, INTENT(IN) :: nsrcsng,nsrcua,nsrcret,ncat
  INTEGER, INTENT(IN) :: ipass
  INTEGER, INTENT(IN) :: nsngfil,nuafil,nretfil

  INTEGER, INTENT(IN) :: indexsng(mxsng), indexua(mxua)
  LOGICAL, INTENT(IN) :: usesng(mxsng), useua(mxua)
!
!-----------------------------------------------------------------------
!
!  Input Grid Arguments
!
!-----------------------------------------------------------------------
!
  REAL(P), INTENT(IN) :: x     (nx)        ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL(P), INTENT(IN) :: y     (ny)        ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL(P), INTENT(IN) :: z     (nz)        ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL(P), INTENT(IN) :: zp    (nx,ny,nz)  ! The physical height coordinate defined at
                               ! w-point of the staggered grid.
  REAL(P), INTENT(IN) :: hterain(nx,ny)    ! The height of the terrain.

  REAL(P), INTENT(IN) :: mapfct(nx,ny,8)   ! Map factors at scalar, u and v points

  REAL(P), INTENT(IN) :: j1    (nx,ny,nz)  ! Coordinate transformation Jacobian
                               ! defined as - d( zp )/d( x ).
  REAL(P), INTENT(IN) :: j2    (nx,ny,nz)  ! Coordinate transformation Jacobian
                               ! defined as - d( zp )/d( y ).
  REAL(P), INTENT(IN) :: j3    (nx,ny,nz)  ! Coordinate transformation Jacobian
                               ! defined as d( zp )/d( z ).
  REAL(P), INTENT(IN) :: aj3x  (nx,ny,nz)  ! Coordinate transformation Jacobian defined
                               ! as d( zp )/d( z ) AVERAGED IN THE X-DIR.
  REAL(P), INTENT(IN) :: aj3y  (nx,ny,nz)  ! Coordinate transformation Jacobian defined
                               ! as d( zp )/d( z ) AVERAGED IN THE Y-DIR.
  REAL(P), INTENT(IN) :: aj3z  (nx,ny,nz)  ! Coordinate transformation Jacobian defined
                               ! as d( zp )/d( z ) AVERAGED IN THE Z-DIR.
  REAL(P), INTENT(IN) :: j3inv (nx,ny,nz)  ! Inverse of j3
  REAL(P), INTENT(IN) :: rhobar(nx,ny,nz)  ! Base state density
!
  REAL(P), INTENT(IN) :: xs(nx)
  REAL(P), INTENT(IN) :: ys(ny)
  REAL(P), INTENT(IN) :: zs(nx,ny,nz)

  REAL(P), INTENT(IN) :: dnw(nz), dn(nz), fnm(nz), fnp(nz)
  REAL(P), INTENT(IN) :: cf1, cf2, cf3

  INTEGER, INTENT(IN) :: icatg(nx,ny)
  REAL(P), INTENT(IN) :: xcor(ncat,ncat)
  REAL(P), INTENT(IN) :: ref_mos_3d(nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  Input Observation Arguments
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=6) :: nam_var(nvar)
  REAL(P) :: xsng(mxsng)
  REAL(P) :: ysng(mxsng)
  !REAL(DP), DIMENSION(:), ALLOCATABLE :: xsng_p, ysng_p
  !REAL(P),  DIMENSION(:), ALLOCATABLE :: zsng_1, zsng_2
  !INTEGER,  DIMENSION(:), ALLOCATABLE :: ihgtsng
  REAL(P) :: hgtsng(mxsng)
  REAL(P) :: thesng(mxsng)
  REAL(P) :: obsng(nvar,mxsng)
  REAL(P) :: odifsng(nvar,mxsng)
  REAL(P) :: qobsng(nvar,mxsng)
  INTEGER :: qualsng(nvar,mxsng)
  INTEGER :: isrcsng(mxsng)
  INTEGER :: nlevsng(mxsng)
  INTEGER :: nobsng
!
  REAL(P) :: xua(mxua)
  REAL(P) :: yua(mxua)
  !REAL(DP), DIMENSION(:),   ALLOCATABLE:: xua_p, yua_p
  !REAL(P),  DIMENSION(:,:), ALLOCATABLE:: zua_1, zua_2
  !INTEGER,  DIMENSION(:,:), ALLOCATABLE:: ihgtua
  REAL(P) :: hgtua(nzua,mxua)
  REAL(P) :: theua(nzua,mxua)
  REAL(P) :: obsua(nvar,nzua,mxua)
  REAL(P) :: odifua(nvar,nzua,mxua)
  REAL(P) :: qobsua(nvar,nzua,mxua)
  INTEGER :: qualua(nvar,nzua,mxua)
  INTEGER :: nlevsua(mxua)
  INTEGER :: isrcua(mxua)
  INTEGER :: nobsua
!
  REAL(P) :: xretc(mxcolret)
  REAL(P) :: yretc(mxcolret)
  REAL(P) :: hgtretc(nzret,mxcolret)
  REAL(P) :: theretc(nzret,mxcolret)
  REAL(P) :: obsret(nvar,nzret,mxcolret)
  REAL(P) :: odifret(nvar,nzret,mxcolret)
  REAL(P) :: qobsret(nvar,nzret,mxcolret)
  INTEGER :: qualret(nvar,nzret,mxcolret)
  INTEGER :: nlevret(mxcolret)
  INTEGER :: iret(mxcolret)
  INTEGER :: isrcret(0:mxret)
  INTEGER :: ncolret
!
!-----------------------------------------------------------------------
!
!  Input Analysis Control Variables
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=8) :: srcsng(nsrcsng)
  CHARACTER (LEN=8) :: srcua (nsrcua )
  CHARACTER (LEN=8) :: srcret(nsrcret)

  INTEGER, INTENT(IN) :: iusesng(0:nsrcsng)
  INTEGER, INTENT(IN) :: iuseua(0:nsrcua)
  INTEGER, INTENT(IN) :: iuseret(0:nsrcret)
!
!-----------------------------------------------------------------------
!
!  Output Variables at Observation Locations
!
!-----------------------------------------------------------------------
!
  REAL(P) :: corsng(mxsng,nvar)
  REAL(P) :: corua(nzua,mxua,nvar)
! REAL(P) :: corret(nzret,mxcolret,nvar)
  REAL(P) :: corret(1,1,1)
!
  REAL(P) :: oanxsng(nvar,mxsng)
  REAL(P) :: oanxua(nvar,nzua,mxua)
  REAL(P) :: oanxret(nvar,nzret,mxcolret)
!
  INTEGER :: nz_tab
  REAL(P) :: hqback(nz_tab)
  REAL(P) :: qback(nvar,nz_tab)
!
  REAL(P) :: qback_wk (nz_tab)
  REAL(P) :: hqback_wk(nz_tab)
!
!-----------------------------------------------------------------------
!
!  Model Grid State
!
!-----------------------------------------------------------------------
!
  REAL(P), INTENT(INOUT) :: anx(nx,ny,nz,nvar)
  REAL(P), INTENT(IN)    :: tk(nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  Variable declarations for varirational analysis: by gao
!
!-----------------------------------------------------------------------
!
  REAL(P) :: eps,gtol,ftol
  REAL(P) :: cfun
  DOUBLE PRECISION :: cfun_single(nvar+1),cfun_total
  INTEGER          :: mp,lp,iprint(2),iflag,point
  LOGICAL          :: diagco
  COMMON /va15dd/mp,lp, gtol

! INTEGER :: icall
  REAL(P) :: rchek,gxnn
  REAL(P) :: cfun1

  REAL(P), ALLOCATABLE :: fa(:), fr(:)
!
!-----------------------------------------------------------------------
!
!  end of variable declarations for variational anaylsis:
!
!-----------------------------------------------------------------------
!
! Pass through variables for using diagnostic divergence equation constraint
!
  REAL(P), INTENT(IN)  :: rhostr(nx,ny,nz)  ! Base state density rhobar times j3

!-----------------------------------------------------------------------
!
!  Work arrays
!
!-----------------------------------------------------------------------
!
  REAL(P), INTENT(OUT) :: tem1(nx,ny,nz), tem2(nx,ny,nz), tem3(nx,ny,nz), tem4(nx,ny,nz)
  REAL(P), INTENT(OUT) :: tem5(nx,ny,nz), tem6(nx,ny,nz), tem7(nx,ny,nz), tem8(nx,ny,nz)
  REAL(P), INTENT(OUT) :: tem9(nx,ny,nz), tem10(nx,ny,nz)
  REAL(P), INTENT(OUT) :: tem11(nx,ny,nz),tem12(nx,ny,nz),tem13(nx,ny,nz)
  REAL(P), INTENT(OUT) :: tem14(nx,ny,nz),tem15(nx,ny,nz)
  REAL(P), INTENT(OUT) :: tem4d(nx,ny,nz,nscalarq)

  LOGICAL :: ownsng(mxsng), ownua(mxua)          ! with global index
!
!-----------------------------------------------------------------------
!
!  Return status
!
!-----------------------------------------------------------------------
!
  INTEGER :: istatus
  INTEGER :: ips, ipe, jps, jpe, kps, kpe
  INTEGER :: stag1, stag2
  INTEGER :: raduvobs
!
!-----------------------------------------------------------------------
!
!  Misc.local variables
!
!-----------------------------------------------------------------------
!
  CHARACTER(LEN=80) :: sngsrc(nsrcsng), uasrc(nsrcua)
  REAL(P) :: ftabinv,setexp
  INTEGER :: i,j,k,l,nq
  INTEGER :: isrc,icnt

  INTEGER :: sngsw,uasw,radsw,lgtsw,retsw
  INTEGER :: cwpsw, tpwsw, aerisw
  INTEGER :: num, numsingle
  REAL(P) :: rpass,zrngsq,thrngsq
  REAL(P) :: sum1,sum2,sum3,sum4,sum5,sum6
  REAL(P) :: f_cputime
  REAL(P) :: cpuin,cpuout
  INTEGER :: ount

  REAL(P) :: uval, vval, tval, pval, wval, qval
  REAL(P) :: qsval
  INTEGER :: ii, jj
  LOGICAL :: ref_use, ref_da

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  ount = 6
!
!-----------------------------------------------------------------------
!
!  allocate control variable arrays
!
!-----------------------------------------------------------------------
!
  tem1 =0.0; tem2 =0.0; tem3 =0.0; tem4 =0.0; tem5 =0.0
  tem6 =0.0; tem7 =0.0; tem8 =0.0; tem9 =0.0; tem10=0.0; tem11=0.0
  tem12=0.0; tem13=0.0; tem14=0.0; tem15=0.0
!
!-------------------------------------------------------
!
  CALL rhouvw(nx,ny,nz,rhostr,rhostru,rhostrv,rhostrw)

!
!    PRINT*,'qbackin=',(qback(1,i),i=1,nz_tab)
!
   ! Background error for U

  IF (idealCase_opt /= 1) THEN

   !IF (ibeta2==1 .AND. ibeta1==1) THEN
   !
   !  !CALL linear (num,hqback_wk,qback_wk,log(anx(i,j,k,PTR_P)),gdu_err(i,j,k) )
   !
   !ELSE IF (ibeta1==1) THEN
     num = 0
     DO isrc = 1, nz_tab
       IF( qback(PTR_U,isrc) > -900.0) THEN
         num = num+1
         qback_wk (num) =  qback(PTR_U,isrc)
         hqback_wk(num) =  hqback(isrc)
       END IF
     END DO

     DO k = 1,nz
       DO j = 1,ny
         DO i = 1,nx
           CALL linear(num,hqback_wk,qback_wk,zs(i,j,k),gdu_err(i,j,k) )
         END DO
       END DO
     END DO
     ! avoid negative number
     gdu_err(:,:,nz-1) = gdu_err(:,:,nz-2)
     gdu_err(:,:,nz) = gdu_err(:,:,nz-1)

     ! Background error for V
     num = 0
     DO isrc = 1, nz_tab
       IF( qback(PTR_V,isrc) > -900.0) THEN
         num = num+1
         qback_wk(num)  = qback(PTR_V,isrc)
         hqback_wk(num) = hqback(isrc)
       end if
     END DO

     DO k = 1,nz
       DO j = 1,ny
         DO i = 1,nx
           CALL linear(num,hqback_wk,qback_wk,zs(i,j,k),gdv_err(i,j,k) )
         END DO
       END DO
     END DO
     gdv_err(:,:,nz-1) = gdv_err(:,:,nz-2)
     gdv_err(:,:,nz) = gdv_err(:,:,nz-1)

     ! Background error for P
     num = 0
     DO isrc = 1, nz_tab
        IF( qback(PTR_P,isrc) > -900.0) THEN
          num = num+1
          qback_wk(num)  = qback(PTR_P,isrc)
          hqback_wk(num) = hqback(isrc)
        END IF
     END DO

     DO k = 1,nz
       DO j = 1,ny
         DO i = 1,nx
           CALL linear(num,hqback_wk,qback_wk,zs(i,j,k),gdp_err(i,j,k) )
         END DO
       END DO
     END DO
     gdp_err(:,:,nz-1) = gdp_err(:,:,nz-2)
     gdp_err(:,:,nz) = gdp_err(:,:,nz-1)

     ! Background error for T
     num = 0
     DO isrc = 1, nz_tab
       IF( qback(PTR_PT,isrc) > -900.0) THEN
         num = num+1
         qback_wk(num) =  qback(PTR_PT,isrc)
         hqback_wk(num) = hqback(isrc)
       END IF
     END DO

     DO k = 1,nz
       DO j = 1,ny
         DO i = 1,nx
           CALL linear(num,hqback_wk,qback_wk,zs(i,j,k),gdt_err(i,j,k) )
         END DO
       END DO
     END DO
     gdt_err(:,:,nz-1) = gdt_err(:,:,nz-2)
     gdt_err(:,:,nz) = gdt_err(:,:,nz-1)

     ! Background error for QV
     num = 0
     DO isrc = 1, nz_tab
       IF( qback(PTR_QV,isrc) > -900.0) THEN
         num = num+1
         qback_wk(num)  = qback(PTR_QV,isrc)
         hqback_wk(num) = hqback(isrc)
       END IF
     END DO

     DO k = 1,nz
       DO j = 1,ny
         DO i = 1,nx
           CALL linear(num,hqback_wk,qback_wk,zs(i,j,k),gdq_err(i,j,k) )
         END DO
       END DO
     END DO
     gdq_err(:,:,nz-1) = gdq_err(:,:,nz-2)
     gdq_err(:,:,nz) = gdq_err(:,:,nz-1)

     ! Background error for W

     num = 0
     DO isrc = 1, nz_tab
        if( qback(PTR_W,isrc) > -900.0) then
        num = num+1
        qback_wk(num) = qback(PTR_W,isrc)
       hqback_wk(num) = hqback(isrc)
        end if
     END DO

     DO k = 1,nz
       DO j = 1,ny
         DO i = 1,nx
           CALL linear(num,hqback_wk,qback_wk,zs(i,j,k),gdw_err(i,j,k))
         END DO
       END DO
     END DO
     gdw_err(:,:,nz-1) = gdw_err(:,:,nz-2)
     gdw_err(:,:,nz) = gdw_err(:,:,nz-1)
  !
  !  DO k = 1,nz
  !    DO j = 1,ny
  !      DO i = 1,nx
  !        gdw_err(i,j,k) = gdu_err(i,j,k)
  !      END DO
  !    END DO
  !  END DO

     !background error for qr/qs/qh
     !TO BE Changed
     !You need to set gdqi_err and gdqc_err here or somewhere
     !right now just let them equal gdqh_err

  !  gdqr_err = bkgerr_qr * bkgerr_qr
  !  gdqs_err = bkgerr_qs * bkgerr_qs
  !  gdqh_err = bkgerr_qh * bkgerr_qh
  !  gdqi_err = gdqh_err
  !  gdqc_err = gdqh_err

  !  IF (P_QC > 0) gdqscalar_err(:,:,:,P_QC) = (0.5E-3)**2
  !  IF (P_QI > 0) gdqscalar_err(:,:,:,P_QI) = (0.5E-3)**2
  !
  !  IF (P_QR > 0) gdqscalar_err(:,:,:,P_QR) = (0.9E-3)**2
  !
  !  IF (P_QS > 0) gdqscalar_err(:,:,:,P_QS) = (0.6E-3)**2
  !
  !  IF (P_QG > 0) gdqscalar_err(:,:,:,P_QG) = (0.8E-3)**2
  !  IF (P_QH > 0) gdqscalar_err(:,:,:,P_QH) = (0.8E-3)**2

  !  IF (P_QC > 0) gdqscalar_err(:,:,:,P_QC) = (0.2E-3)**2
  !  IF (P_QI > 0) gdqscalar_err(:,:,:,P_QI) = (0.8E-4)**2

  !  IF (P_QR > 0) gdqscalar_err(:,:,:,P_QR) = (0.3E-3)**2

  !  IF (P_QS > 0) gdqscalar_err(:,:,:,P_QS) = (0.8E-4)**2

  !  IF (P_QG > 0) gdqscalar_err(:,:,:,P_QG) = (0.3E-3)**2
  !  IF (P_QH > 0) gdqscalar_err(:,:,:,P_QH) = (0.3E-3)**2

     IF (P_QC > 0) CALL TbasedInterp(nx, ny, nz, 1.0, tk, 278.15, 243.15,         &
                                     0.2E-3, 0.0, gdqscalar_err(:,:,:,P_QC))
     IF (P_QI > 0) CALL TbasedInterp(nx, ny, nz, 1.0, tk, 278.15, 243.15,         &
                                     0.0, 0.2E-3, gdqscalar_err(:,:,:,P_QI))
     IF (P_QR > 0) CALL TbasedInterp(nx, ny, nz, 1.0, tk, 278.15, 268.15,         &
                                     0.3E-3, 0.0, gdqscalar_err(:,:,:,P_QR))
     IF (P_QS > 0) CALL TbasedInterp(nx, ny, nz, 1.0, tk, 278.15, 243.15,         &
                                     0.0, 0.6E-3, gdqscalar_err(:,:,:,P_QS))
     IF (P_QG > 0) CALL TbasedInterp(nx, ny, nz, 1.0, tk, 278.15, 243.15,         &
                                     0.2E-3, 0.3E-3, gdqscalar_err(:,:,:,P_QG))
     IF (P_QH > 0) CALL TbasedInterp(nx, ny, nz, 1.0, tk, 278.15, 243.15,         &
                                     0.2E-3, 0.3E-3, gdqscalar_err(:,:,:,P_QH))

!
!----------------if ( cntl_var==0) u & v as control variable-------------
!
    !IF(cntl_var == 0) THEN

  !    IF( .FALSE. ) THEN   ! Fixed error matrices
  !      gdu_err(:,:,:) = 1.0
  !      gdv_err(:,:,:) = 1.0
  !      gdp_err(:,:,:) = 1.0
  !      gdt_err(:,:,:) = 1.0
  !      gdq_err(:,:,:) = 1.0
  !      gdw_err(:,:,:) = 1.0
  !    ELSE
        DO k = 1,nz
          DO j = 1,ny
            DO i = 1,nx
              if(gdu_err(i,j,k) < 0) gdu_err(i,j,k) = 0.0
              if(gdv_err(i,j,k) < 0) gdv_err(i,j,k) = 0.0
              if(gdp_err(i,j,k) < 0) gdp_err(i,j,k) = 0.0
              if(gdt_err(i,j,k) < 0) gdt_err(i,j,k) = 0.0
              if(gdq_err(i,j,k) < 0) gdq_err(i,j,k) = 0.0
              if(gdw_err(i,j,k) < 0) gdw_err(i,j,k) = 0.0
              gdu_err(i,j,k) = SQRT( gdu_err(i,j,k) )
              gdv_err(i,j,k) = SQRT( gdv_err(i,j,k) )
              gdp_err(i,j,k) = SQRT( gdp_err(i,j,k) )
              gdt_err(i,j,k) = SQRT( gdt_err(i,j,k) )
              gdq_err(i,j,k) = SQRT( gdq_err(i,j,k) )
              gdw_err(i,j,k) = SQRT( gdw_err(i,j,k) )
            END DO
          END DO
        END DO
        !gdqscalar_err(:,:,:,:) = SQRT( gdqscalar_err(:,:,:,:) )
  !    ENDIF

      IF( ipass > 1 ) THEN
        gdu_err(:,:,:) = gdu_err(:,:,:)*(0.90)**(ipass-1)
        gdv_err(:,:,:) = gdv_err(:,:,:)*(0.90)**(ipass-1)
        gdp_err(:,:,:) = gdp_err(:,:,:)*(0.90)**(ipass-1)
        gdt_err(:,:,:) = gdt_err(:,:,:)*(0.90)**(ipass-1)
        !gdq_err(:,:,:) = gdq_err(:,:,:)*(0.90)**(ipass-1)
        gdw_err(:,:,:) = gdw_err(:,:,:)*(0.90)**(ipass-1)
        gdqscalar_err(:,:,:,:)= gdqscalar_err(:,:,:,:)*(0.90)**(ipass-1)
      END IF  !end of ipass >1

    !END IF

    stag1 = 1
    stag2 = 2

  ELSE ! setting standard error for ideal case

    DO k = 1,nz
      DO j = 1,ny
        DO i = 1,nx
          gdu_err(i,j,k) = 3.0
          gdv_err(i,j,k) = 3.0
          gdp_err(i,j,k) = 80.0
          gdt_err(i,j,k) = 2.5
          gdq_err(i,j,k) = 0.8E-3
          gdw_err(i,j,k) = 3.0
        END DO
      END DO
    END DO
    gdqscalar_err(:,:,:,:) = 0.6E-3
    gdqscalar_err(:,:,:,P_QR) = 0.4E-3
    gdqscalar_err(:,:,:,P_QS) = 0.8E-4
    IF (P_QG > 0) gdqscalar_err(:,:,:,P_QG) = 0.6E-3
    IF (P_QH > 0) gdqscalar_err(:,:,:,P_QH) = 0.6E-3
    IF (P_QC > 0) gdqscalar_err(:,:,:,P_QC) = 0.5E-3
    IF (P_QI > 0) gdqscalar_err(:,:,:,P_QI) = 0.5E-3
  END IF


!!!!!IF( iensmbl /= 0 .and. ref_opt == 1 .and. icycle==1 ) THEN  !This need further testing!!!
!IF( ibeta2==1 .and. ipass==1 .and. ref_opt == 1 .and. icycle==1 ) THEN
!     iseed1 = iseed-10*iensmbl + icycle
!   DO k = 1,nz
!     DO j = 1,ny
!       DO i = 1,nx
!         IF (ref_mos_3d(i,j,k) > thresh_ref) THEN
!          DO  l=1, nensvirtl

!           ue(i,j,k,l)=                                               &
!                     ue(i,j,k,l)+GASDEV(iseed1)*gdu_err(i,j,k)*0.05
!           ve(i,j,k,l)=                                               &
!                     ve(i,j,k,l)+GASDEV(iseed1)*gdv_err(i,j,k)*0.05
!           we(i,j,k,l)=                                               &
!                     we(i,j,k,l)+GASDEV(iseed1)*gdw_err(i,j,k)*0.05


!          qve(i,j,k,l)=                                               &
!                    qve(i,j,k,l)+GASDEV(iseed1)*gdq_err(i,j,k)*10.0
!
!       ptprte(i,j,k,l)=                                               &
!                 ptprte(i,j,k,l)+GASDEV(iseed1)*gdt_err(i,j,k)*10.0
!
!        pprte(i,j,k,l)=                                               &
!                  pprte(i,j,k,l)+GASDEV(iseed1)*gdp_err(i,j,k)
!
!     IF (P_QC > 0) qscalare(i,j,k,P_QC,l)=                            &
!         qscalare(i,j,k,P_QC,l)+GASDEV(iseed1)*gdqc_err(i,j,k)*10.0
!     IF (P_QR > 0) qscalare(i,j,k,P_QR,l)=                            &
!         qscalare(i,j,k,P_QR,l)+GASDEV(iseed1)*gdqr_err(i,j,k)*10.0
!     IF (P_QI > 0) qscalare(i,j,k,P_QI,l)=                            &
!         qscalare(i,j,k,P_QI,l)+GASDEV(iseed1)*gdqi_err(i,j,k)*10.0
!     IF (P_QS > 0) qscalare(i,j,k,P_QS,l)=                            &
!         qscalare(i,j,k,P_QS,l)+GASDEV(iseed1)*gdqs_err(i,j,k)*10.0
!     IF (P_QH > 0) qscalare(i,j,k,P_QH,l)=                            &
!         qscalare(i,j,k,P_QH,l)+GASDEV(iseed1)*gdqh_err(i,j,k)*10.0
!
!          END DO
!         ENDIF
!       END DO
!     END DO
!   END DO

!ELSE

!     iseed1 = iseed-10*iensmbl + icycle
!   DO k = 1,nz
!     DO j = 1,ny
!       DO i = 1,nx
!         IF (ref_mos_3d(i,j,k) > thresh_ref) THEN
!          DO  l=1, nensvirtl
!           ue(i,j,k,l)=                                               &
!                     ue(i,j,k,l)+GASDEV(iseed1)*gdu_err(i,j,k)*0.05
!           ve(i,j,k,l)=                                               &
!                     ve(i,j,k,l)+GASDEV(iseed1)*gdv_err(i,j,k)*0.05
!           we(i,j,k,l)=                                               &
!                     we(i,j,k,l)+GASDEV(iseed1)*gdw_err(i,j,k)*0.05
!          qve(i,j,k,l)=                                               &
!                    qve(i,j,k,l)+GASDEV(iseed1)*gdq_err(i,j,k)*0.05
!       ptprte(i,j,k,l)=                                               &
!                 ptprte(i,j,k,l)+GASDEV(iseed1)*gdt_err(i,j,k)*0.05
!        pprte(i,j,k,l)=                                               &
!                  pprte(i,j,k,l)+GASDEV(iseed1)*gdp_err(i,j,k)*0.05
!     IF (P_QC > 0) qscalare(i,j,k,P_QC,l)=                            &
!         qscalare(i,j,k,P_QC,l)+GASDEV(iseed1)*gdqc_err(i,j,k)*0.1
!     IF (P_QR > 0) qscalare(i,j,k,P_QR,l)=                            &
!         qscalare(i,j,k,P_QR,l)+GASDEV(iseed1)*gdqr_err(i,j,k)*0.1
!     IF (P_QI > 0) qscalare(i,j,k,P_QI,l)=                            &
!         qscalare(i,j,k,P_QI,l)+GASDEV(iseed1)*gdqi_err(i,j,k)*0.1
!     IF (P_QS > 0) qscalare(i,j,k,P_QS,l)=                            &
!         qscalare(i,j,k,P_QS,l)+GASDEV(iseed1)*gdqs_err(i,j,k)*0.1
!     IF (P_QH > 0) qscalare(i,j,k,P_QH,l)=                            &
!         qscalare(i,j,k,P_QH,l)+GASDEV(iseed1)*gdqh_err(i,j,k)*0.1
!          END DO
!         ENDIF
!       END DO
!     END DO
!   END DO

!END IF

!DO  l=1, nensvirtl
! CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,ue(:,:,:,l),          &
!                                                  tem1,ue(:,:,:,l))
! CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,ve(:,:,:,l),          &
!                                                  tem1,ve(:,:,:,l))
! CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,we(:,:,:,l),          &
!                                                  tem1,we(:,:,:,l))
! CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,ptprte(:,:,:,l)       &
!                                                 ,tem7,ptprte(:,:,:,l))
! CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,pprte(:,:,:,l)        &
!                                                 ,tem7,pprte(:,:,:,l))
! CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,qve(:,:,:,l),         &
!                                                  tem1,qve(:,:,:,l))
! CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,qscalare(:,:,:,1,l),  &
!                                                  tem1,qscalare(:,:,:,1,l))
! CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,qscalare(:,:,:,2,l),  &
!                                                  tem1,qscalare(:,:,:,2,l))
! CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,qscalare(:,:,:,3,l),  &
!                                                  tem1,qscalare(:,:,:,3,l))
! CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,qscalare(:,:,:,4,l),  &
!                                                  tem1,qscalare(:,:,:,4,l))
! CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,qscalare(:,:,:,5,l),  &
!                                                  tem1,qscalare(:,:,:,5,l))
!END DO

!
!-----------------------------------------------------------------------
!
! Set flags
!
!-----------------------------------------------------------------------

  ownsng(:)  = .FALSE.
  ownua (:)  = .FALSE.

  DO i=1,numctr
    ctrv(i)=0.0
    xgus(i)=0.0
    grad(i)=0.0
  END DO

  sngsw = 0
  uasw  = 0
  cwpsw = 0
  tpwsw = 0
  radsw = 0
  lgtsw = 0
  retsw = 0
  aerisw= 0

  IF( nsngfil /= 0 ) sngsw = 1
  IF( nuafil  /= 0 ) uasw  = 1
  IF( nretfil /= 0 ) retsw = 1

  ref_use = ANY(ref_opt == 1)
  ref_da  = ( ref_opt(ipass) == 1 .OR. hydro_opt == 1 )

!-------------------------------------------------------------------
!
!  caculating some parameters for interpolation ! added by Jidong Gao
!
!-------------------------------------------------------------------
!

  !
  ! SNG
  !
  DO i = 1, nobsng
    nlevsng(i) = 1
    IF (indexsng(i) == myproc) ownsng(i) = .TRUE.
  END DO

  CALL allocate_3dvar_workarray_sng(mxsng,0.0,istatus)

  CALL map_to_mod2(nx,ny,mxsng,nobsng,usesng,xs,ys,xsng,ysng,xsng_p,ysng_p)

  CALL map_to_modz(1, mxsng, nlevsng, nobsng,nx,ny,nz,zs,               &
                   xsng_p,ysng_p,hgtsng,ihgtsng,zsng_1,zsng_2)

  DO i = 1, nobsua
    IF (indexua(i) == myproc) ownua(i) = .TRUE.
  END DO

  CALL allocate_3dvar_workarray_ua (mxua,nzua,0.0,istatus)

  CALL map_to_mod2(nx,ny,mxua,nobsua,useua,xs,ys,xua,yua,xua_p,yua_p)
  CALL map_to_modz(nzua, mxua, nlevsua, nobsua, nx,ny,nz,zs,            &
                   xua_p,yua_p, hgtua, ihgtua,zua_1,zua_2)

  CALL radargrid_init(ref_use,nx,ny,nz,xs,ys,zs,myproc,istatus)

  CALL aeriobs_init(nx,ny,nz,xs,ys,zs,myproc,nprocs,istatus)

!-------------------------------------------------------------------
!
! Get the hradius_3d   Anwei Lai
!
!-------------------------------------------------------------------

  CALL set_ipass_hradius(0,nx,ny,nz,ref_mos_3d,                         &
                         hradius(ipass),hradius_3d,istatus)

!-------------------------------------------------------------------
!
! Specifiy vertical influence radius
!
!-------------------------------------------------------------------

  CALL set_ipass_vradius(vradius_opt(ipass),nx,ny,nz,zp,                &
                         vradius(ipass),radius_z,istatus)
  IF (myproc == 0 ) THEN
    WRITE(*,*) 'The vertical influence radius is specified in grid points as:'
    WRITE(*,*) '---- The followings are k, zp, dzp, vradius at i=3,j=3 ----'
    i=3; j =3
    DO k =1, nz-1
      WRITE(*,'(1x,I3,3F10.2)') k, zp(i,j,k), zp(i,j,k+1)-zp(i,j,k), radius_z(i,j,k)
    END DO
  END IF


  IF (ref_da) THEN

    !to assign a small value to the zero bkg where obs > thresh_ref
    IF (ref_opt(ipass) == 1) THEN
      DO k= 1,nz-1
        DO j= 1,ny-1
          DO i= 1,nx-1
            IF (grdtlt_flag == 1) THEN
              anx(i,j,k,6+P_QR) = max(anx(i,j,k,6+P_QR),1E-6)
              anx(i,j,k,6+P_QS) = max(anx(i,j,k,6+P_QS),1E-6)
              IF (P_QH > 0) anx(i,j,k,6+P_QH) = max(anx(i,j,k,6+P_QH),1E-6)
              IF (P_QG > 0) anx(i,j,k,6+P_QG) = max(anx(i,j,k,6+P_QG),1E-6)
            ELSE
              IF (ref_mos_3d(i,j,k) > 0.0) THEN
                IF (tk(i,j,k) > 273.15 .AND. P_QR > 0) anx(i,j,k,6+P_QR) = max(anx(i,j,k,6+P_QR),5E-6)
                IF (tk(i,j,k) < 273.15 .AND. P_QS > 0) anx(i,j,k,6+P_QS) = max(anx(i,j,k,6+P_QS),5E-6)
                IF (tk(i,j,k) < 273.15 .AND. P_QH > 0) anx(i,j,k,6+P_QH) = max(anx(i,j,k,6+P_QH),5E-6)
                IF (tk(i,j,k) < 273.15 .AND. P_QG > 0) anx(i,j,k,6+P_QG) = max(anx(i,j,k,6+P_QG),5E-6)
              END IF
            END IF
          END DO
        END DO
      END DO
    END IF

    !IF (ibeta2==1) THEN

      !tem2 = anx(:,:,:,6+P_QR)
      !tem3 = anx(:,:,:,6+P_QS)
      !IF (P_QH > 0) tem4 = tem4 + anx(:,:,:,6+P_QH)
      !IF (P_QG > 0) tem4 = tem4 + anx(:,:,:,6+P_QG)

      !CALL reflec_g(nx,ny,nz,ref_mos_3d,intvl_T,thresh_ref,rhobar,      &
      !              tem2,tem3,tem4,tk,tem5)

      !CALL ens_hydro_perturb(myproc,nx,ny,nz,nscalar,tk,tem5,           &
      !                       ref_mos_3d,thresh_ref,gdqscalar_err,       &
      !                       P_QR,P_QS,P_QG,P_QH,tem1,istatus)

    !END IF              !ibeta2

    CALL set_ipass_hradius(0,nx,ny,nz,ref_mos_3d,                       &
                           hradius_ref(ipass),hradius_3d_ref,istatus)

    CALL set_ipass_vradius(vradius_opt_ref(ipass),nx,ny,nz,zp,          &
                           vradius_ref(ipass),radius_z_ref,istatus)

    IF (myproc == 0 ) THEN
      WRITE(*,*) 'The vradius for qr/qs/qh is specified in grid points as:'
      WRITE(*,*) '---- The followings are k, zp, dzp, vradius_ref at i=3,j=3 ----'
      i=3; j =3
      DO k =1, nz-1
        WRITE(*,'(1x,I3,3F10.2)') k, zp(i,j,k), zp(i,j,k+1)-zp(i,j,k), radius_z_ref(i,j,k)
      END DO
    END IF !IF (myproc == 0 ) THEN

  END IF !IF (ref_opt ==1. OR. hydro_opt==1)


  IF ( ibeta2==1 ) THEN

    hradius_3d_ens = hradius_ens(ipass)   !w_coef*hradius_ens(ipass)

    !BEGIN: to specifiy vertical influence radius
    CALL ens_zradius(ipass,nx,ny,nz,zp,myproc,istatus)

  END IF !IF ( ibeta2==1) THEN

  CALL consthermo_checkflags(ipass,nx,ny,nz,x,y,dx,dy,ctrlat,mapfct,    &
               coriopt,eradius,omega,anx(:,:,:,1),anx(:,:,:,2),istatus)

  CALL consmslp_checkflags(ipass,nx,ny,   xs,ys,istatus)

  CALL consdiv_checkflags (ipass,nx,ny,nz, x, y,istatus)  ! determine whether to do diagnostic divergence constrain

  IF(chk_opt == 1) THEN
    !ipass = 1

    CALL mpmaxi(sngsw)    ! All processors must have the same flags
    CALL mpmaxi(uasw)
    !CALL mpmaxi(cwpsw)
    !CALL mpmaxi(tpwsw)
    !CALL mpmaxi(radsw)

    raduvobs = 0
    IF (ANY(vrob_opt > 0) .OR. ANY(ref_opt > 0)) raduvobs = 1
    CALL convobs_set_usage(myproc,0,istatus)
    CALL cwpobs_set_usage(myproc,0,cwpsw,istatus)
    CALL tpwobs_set_usage(myproc,0,tpwsw,istatus)
    CALL radarobs_set_usage(myproc,0,raduvobs,ref_use,radsw,istatus)
    CALL aeriobs_set_usage(myproc,0,aerisw,istatus)
    CALL lightning_set_usage(myproc,0,lgtsw,istatus)

    CALL acct_interrupt(recrsub1_acct)

    IF (flag_legacyfilter==1) then

      IF ( ibeta1==1 ) THEN

      tem7=0.0;tem8=0.0;tem9=0.0;tem4=0.0;tem5=0.0
      CALL scale_factor(nx,ny,nz,gdscal,ipass_filt(1),'gdscal',         &
                        hradius_3d,radius_z,tem7,tem8,tem9,tem4,tem5)

      IF ( ref_da ) THEN
        tem7=0.0;tem8=0.0;tem9=0.0;tem4=0.0;tem5=0.0
        CALL scale_factor(nx,ny,nz,gdscal_ref,ipass_filt(1),'gdscal_ref',&
                          hradius_3d_ref,radius_z_ref,tem7,tem8,tem9,tem4,tem5)
      END IF !IF (ref_opt == 1. OR. hydro_opt==1)

      END IF

      IF ( ibeta2==1 ) THEN

        tem7=0.0;tem8=0.0;tem9=0.0;tem4=0.0;tem5=0.0
        CALL scale_factor(nx,ny,nz,gdscal_ens,ipass_filt(1),'gdscal_ens',&
                          hradius_3d_ens,radius_z_ens,tem7,tem8,tem9,tem4,tem5)
      END IF !IF ( ibeta2==1 )

    ELSE  ! option 2 for flag_legacyfilter=2

       CALL initfilter(modelopt,flag_iso,hradius_3d,radius_z,           &
                     hradius_3d_ref,radius_z_ref,                       &
                     ref_opt,hydro_opt,                                 &
                     nx,ny,nz,nscalarq,gdu_err,gdv_err,                 &
                     gdp_err,gdt_err,gdq_err,gdw_err,                   &
                     gdqscalar_err )
       IF ( ibeta2==1 ) THEN
         CALL initfilter2(modelopt,flag_iso, hradius_3d_ens,            &
                          radius_z_ens,nx,ny,nz,nscalarq)
       END IF

    END IF
    CALL  acct_stop_inter

    rchek=1.0E-1
    ALLOCATE (fa(20))
    ALLOCATE (fr(20))
!
!-----------------------------------------------------------------------
!
!      OBTAIN THE COST FUNCTION AND THE GRADIENT.
!
!-----------------------------------------------------------------------
!
    CALL costf(ount,numctr,ctrv,cfun_single,                            &
          gdu_err,gdv_err,gdp_err,gdt_err,gdq_err,gdw_err,              &
          gdqscalar_err, qscalar_ctr,                                   &
          u_ctr,v_ctr,p_ctr,t_ctr,q_ctr,w_ctr, psi, phi,                &
          gdscal, nx,ny,nz,gdscal_ref,                                  &
          nvar,nzua,nzret,                                              &
          mapfct,j1,j2,j3,aj3x,aj3y,aj3z,j3inv,rhobar,                  &
          dnw,dn,fnm,fnp,cf1,cf2,cf3,     & ! Added for WRF divergence constraint
          rhostr,rhostru, rhostrv, rhostrw, div3,                       &
          mxsng,mxua,mxret,mxcolret,                                    &
          nsrcsng,nsrcua,nsrcret,ncat,                                  &
          ipass,xs,ys,zs,x,y,z,zp,hterain,                              &
          icatg,xcor,nam_var,                                           &
          ownsng,usesng,xsng,ysng,hgtsng,thesng,                        &
          obsng,odifsng,qobsng,qualsng,isrcsng,nlevsng,nobsng,          &
          ownua,useua,xua,yua,hgtua,theua,                              &
          obsua,odifua,qobsua,qualua,isrcua,nlevsua,nobsua,             &
          xretc,yretc,hgtretc,theretc,                                  &
          obsret,odifret,qobsret,qualret,                               &
          iret,isrcret,nlevret,ncolret,                                 &
          srcsng,srcua,srcret,                                          &
          iusesng(:),iuseua(:),iuseret(:),                              &
          corsng,corua,corret,                                          &
          xsng_p,ysng_p,ihgtsng,xua_p,yua_p,ihgtua,                     &
          zsng_1,zsng_2,zua_1,zua_2,                                    &
          oanxsng,oanxua,oanxret,                                       &
          sngsw,uasw,radsw,lgtsw,cwpsw,tpwsw,retsw,aerisw,              &
          hradius_3d,hradius_3d_ref,radius_z,ref_mos_3d,radius_z_ref,   &
          anx,tk,tem1,tem2,tem3,tem4,tem5,tem6,tem7,istatus)

    CALL mpsumdp(cfun_single,nvar+1)

    cfun_total=0.0
    DO i=1,nvar+1
      IF (myproc == 0) print*,'cfun_single(',i,') = ',cfun_single(i)
      cfun_total=cfun_total+cfun_single(i)
    ENDDO

    !CALL mpsumdp(cfun_total,1)

    cfun = cfun_total
    IF (myproc == 0) PRINT *, 'cfun  ==== ',cfun

    CALL gradt(numctr,ctrv,grad,                                        &
          gdu_err,gdv_err,gdp_err,gdt_err,gdq_err,gdw_err,gdqscalar_err,&
          u_ctr,v_ctr,p_ctr,t_ctr,q_ctr,w_ctr,qscalar_ctr, psi, phi,    &
          gdscal, nx,ny,nz,gdscal_ref,                                  &
          nvar,nzua,nzret,                                              &
          mapfct,j1,j2,j3,aj3x,aj3y,aj3z,j3inv,rhobar,                  &
          dnw,dn,fnm,fnp,cf1,cf2,cf3,     & ! Added for WRF divergence constraint
          rhostr,rhostru, rhostrv, rhostrw, div3,                       &
          mxsng,mxua,mxret,mxcolret,                                    &
          nsrcsng,nsrcua,nsrcret,ncat,                                  &
          ipass,xs,ys,zs,x,y,z,zp,hterain,                              &
          icatg,xcor,nam_var,xsng,ysng,hgtsng,thesng,                   &
          obsng,odifsng,qobsng,qualsng,isrcsng,nlevsng,nobsng,          &
          xua,yua,hgtua,theua,                                          &
          obsua,odifua,qobsua,qualua,isrcua,nlevsua,nobsua,             &
          xretc,yretc,hgtretc,theretc,                                  &
          obsret,odifret,qobsret,qualret,                               &
          usesng,useua,                                                 &
          iret,isrcret,nlevret,ncolret,                                 &
          srcsng,srcua,srcret,                                          &
          iusesng(:),iuseua(:),iuseret(:),                              &
          corsng,corua,corret,                                          &
          xsng_p,ysng_p,ihgtsng,xua_p,yua_p,ihgtua,                     &
          zsng_1,zsng_2,zua_1,zua_2,                                    &
          oanxsng,oanxua,oanxret,                                       &
          sngsw,uasw,radsw,lgtsw,cwpsw,tpwsw,retsw,aerisw,              &
          hradius_3d,hradius_3d_ref,radius_z,ref_mos_3d,radius_z_ref,   &
          anx,tk,                                                       &
          tem1,tem2,tem3,tem4,tem5,tem6,tem4d,                          &
          tem7,tem8,tem9,tem10,tem11,tem12,tem13,tem14,tem15,istatus)

    gxnn=0.
    DO i=1,numctr
      gxnn=gxnn+grad(i)*grad(i)
    END DO
    CALL mptotal(gxnn)

    IF (myproc == 0) PRINT *,'gxnn  ==== ',gxnn

    DO j=1,20
      rchek=rchek*1.0E-1
      DO i=1,numctr
        xgus(i)=ctrv(i)+rchek*grad(i)
      END DO

      IF (myproc == 0 )  WRITE(ount,'(1x,a,I3,a,/,1x,7(a,I0))')           &
               '=========== Checking No. - ',j,  ' ==========',           &
               'sngsw = ',sngsw,', uasw = ',uasw,  ', convsw = ',convsw,  &
             ', lgtsw = ',lgtsw,', cwpsw = ',cwpsw,', tpwsw = ',tpwsw,    &
             ', radsw = ',radsw


!
!-----------------------------------------------------------------------
!
!      OBTAIN THE COST FUNCTION AND THE GRADIENT.
!
!-----------------------------------------------------------------------
!
      CALL costf(ount,numctr,xgus,cfun_single,                          &
            gdu_err,gdv_err,gdp_err,gdt_err,gdq_err,gdw_err,            &
            gdqscalar_err, qscalar_ctr,                                 &
            u_ctr,v_ctr,p_ctr,t_ctr,q_ctr,w_ctr, psi, phi,              &
            gdscal, nx,ny,nz,gdscal_ref,                                &
            nvar,nzua,nzret,                                            &
            mapfct,j1,j2,j3,aj3x,aj3y,aj3z,j3inv,rhobar,                &
            dnw,dn,fnm,fnp,cf1,cf2,cf3,     & ! Added for WRF divergence constraint
            rhostr,rhostru, rhostrv, rhostrw, div3,                     &
            mxsng,mxua,mxret,mxcolret,                                  &
            nsrcsng,nsrcua,nsrcret,ncat,                                &
            ipass,xs,ys,zs,x,y,z,zp,hterain,                            &
            icatg,xcor,nam_var,                                         &
            ownsng,usesng,xsng,ysng,hgtsng,thesng,                      &
            obsng,odifsng,qobsng,qualsng,isrcsng,nlevsng,nobsng,        &
            ownua,useua,xua,yua,hgtua,theua,                            &
            obsua,odifua,qobsua,qualua,isrcua,nlevsua,nobsua,           &
            xretc,yretc,hgtretc,theretc,                                &
            obsret,odifret,qobsret,qualret,                             &
            iret,isrcret,nlevret,ncolret,                               &
            srcsng,srcua,srcret,                                        &
            iusesng(:),iuseua(:),iuseret(:),                            &
            corsng,corua,corret,                                        &
            xsng_p,ysng_p,ihgtsng,xua_p,yua_p,ihgtua,                   &
            zsng_1,zsng_2,zua_1,zua_2,                                  &
            oanxsng,oanxua,oanxret,                                     &
            sngsw,uasw,radsw,lgtsw,cwpsw, tpwsw,retsw,aerisw,           &
            hradius_3d,hradius_3d_ref,radius_z,ref_mos_3d,radius_z_ref, &
            anx,tk,tem1,tem2,tem3,tem4,tem5,tem6,tem7,istatus)

      CALL mpsumdp(cfun_single,nvar+1)

      cfun_total=0
      DO i=1,nvar+1
        cfun_total=cfun_total+cfun_single(i)
      END DO

      cfun1=cfun_total

      IF (myproc == 0) WRITE(*,'(1x,a,I2.2,2(a,f35.20))') 'j = ',j,': cfun1 = ',cfun1,', diff = ',cfun1-cfun

      fa(j)=rchek
      fr(j)=(cfun1-cfun)/(gxnn*rchek)
    END DO

    IF (myproc == 0) THEN
      DO j=1,20
        WRITE(*,*) '  Rchek=',fa(j),'  fr==',fr(j)
      END DO
    END IF

    DEALLOCATE (fa, fr)
    CALL arpsstop('Check done.',0)

  END IF

  IF(assim_opt == 1) THEN
!
!-----------------------------------------------------------------------
!
!  Data assimilation begins here.
!
!-----------------------------------------------------------------------
!
    DO i=1,numctr
      ctrv(i)=0.0
    END DO
!
!-----------------------------------------------------------------------
!
!  Loop through ipass analysis iterations
!
!-----------------------------------------------------------------------
!
    icall  = 0
    eps    = 1.0E-12
    ftol   = 1.0E-4
    !iprint = 0
    iprint(:) = -1
    iflag  = 0
    diagco = .false.
    grad   = 0.0

    CALL acct_interrupt(recrsub1_acct)
    IF (flag_legacyfilter==1) then

      IF ( ibeta1==1 ) THEN

      tem7=0.0;tem8=0.0;tem9=0.0;tem4=0.0;tem5=0.0
      CALL scale_factor(nx,ny,nz,gdscal,ipass_filt(ipass),'gdscal',     &
                        hradius_3d,radius_z,tem7,tem8,tem9,tem4,tem5)

      IF (ref_da) THEN
        tem7=0.0;tem8=0.0;tem9=0.0;tem4=0.0;tem5=0.0
        CALL scale_factor(nx,ny,nz,gdscal_ref,ipass_filt(ipass),'gdscal_ref', &
                          hradius_3d_ref,radius_z_ref,tem7,tem8,tem9,tem4,tem5)
      END IF !IF (ref_opt == 1 .OR. hydro_opt==1)

      END IF

      IF ( ibeta2==1 ) THEN

        tem7=0.0;tem8=0.0;tem9=0.0;tem4=0.0;tem5=0.0
        CALL scale_factor(nx,ny,nz,gdscal_ens,ipass_filt(ipass),'gdscal_ens', &
                          hradius_3d_ens,radius_z_ens,tem7,tem8,tem9,tem4,tem5)
      END IF !IF ( ibeta2==1 ) THEN

    ELSE ! option 2 for flag_legacyfilter=2

      CALL initfilter(modelopt,flag_iso,hradius_3d,radius_z,            &
                     hradius_3d_ref,radius_z_ref,                       &
                     ref_opt(ipass),hydro_opt,                          &
                     nx,ny,nz,nscalarq,gdu_err,gdv_err,                 &
                     gdp_err,gdt_err,gdq_err,gdw_err,                   &
                     gdqscalar_err )

      IF ( ibeta2==1 ) THEN
        CALL initfilter2(modelopt,flag_iso, hradius_3d_ens,radius_z_ens,&
                         nx,ny,nz,nscalarq)
      END IF
    END IF

    CALL  acct_stop_inter

!
!-----------------------------------------------------------------------
!
!  Set single-level usage switch based in iusesng
!
!-----------------------------------------------------------------------
!
      IF (myproc == 0) WRITE(6,'(/a,i4/)') ' Source usage switches for pass: ',ipass

      sngsw=0
      IF( nsngfil /= 0 ) THEN
        icnt = 0
        DO isrc=1,nsrcsng
          IF(iusesng(isrc) > 0) THEN
            sngsw=1
            icnt = icnt+1
            sngsrc(icnt) = srcsng(isrc)
          END IF
        END DO
      END IF
      CALL mpmaxi(sngsw)    ! All processors must have the same flags

      IF (myproc == 0) THEN
        IF (sngsw == 0) THEN
          WRITE(6,'(3x,a,23x,a)') 'Single level data',' none'
        ELSE
          WRITE(6,'(3x,a,23x,a)') 'Single level data',' Using'
          DO isrc= 1,icnt
            WRITE(6,'(9x,I0,2a)') isrc,' - ',TRIM(sngsrc(isrc))
          END DO
        END IF
        WRITE(6,*) ' '
      END IF
!
!-----------------------------------------------------------------------
!
!  Set multiple-level usage switch based in iuseua
!
!-----------------------------------------------------------------------
!
      uasw=0
      IF( nuafil /= 0 ) THEN
        icnt = 0
        DO isrc=1,nsrcua
          IF(iuseua(isrc) > 0) THEN
            uasw=1
            icnt = icnt+1
            uasrc(icnt) = srcua(isrc)
          END IF
        END DO
      END IF
      CALL mpmaxi(uasw)


      IF (myproc == 0) THEN
        IF (uasw == 0) THEN
          WRITE(6,'(3x,a,21x,a)') 'Multiple level data',' none'
        ELSE
          WRITE(6,'(3x,a,21x,a)') 'Multiple level data',' Using'
          DO isrc= 1,icnt
            WRITE(6,'(9x,I0,2a)') isrc,' - ',TRIM(uasrc(isrc))
          END DO
        END IF
        WRITE(6,*) ' '
      END IF
!
!-----------------------------------------------------------------------
!
!  Set conventional observation usage switch based in iuseconv
!
!-----------------------------------------------------------------------
!
      CALL convobs_set_usage(myproc,ipass,istatus)
!
!-----------------------------------------------------------------------
!
!  Set cloud water path usage switch based in iusecwp
!
!-----------------------------------------------------------------------
!
      CALL cwpobs_set_usage(myproc,ipass,cwpsw,istatus)
!
!-----------------------------------------------------------------------
!
!  Set total precipitable water usage switch based in iusetpw
!
!-----------------------------------------------------------------------
!
      CALL tpwobs_set_usage(myproc,ipass,tpwsw,istatus)
!
!-----------------------------------------------------------------------
!
!  Set radar-data usage switch based in iuserad
!
!-----------------------------------------------------------------------
!
      raduvobs = vrob_opt(ipass)+ref_opt(ipass)
      CALL radarobs_set_usage(myproc,ipass,raduvobs,ref_use,radsw,istatus)

!
!-----------------------------------------------------------------------
!
!  Set lightning data usage switch based on iuselgt
!
!-----------------------------------------------------------------------

      CALL lightning_set_usage(myproc,ipass,lgtsw,istatus)
!
!-----------------------------------------------------------------------
!
!  Set AERI-data usage switch based on iuseaeri
!
!-----------------------------------------------------------------------
!
      CALL aeriobs_set_usage(myproc,0,aerisw,istatus)
!
!-----------------------------------------------------------------------
!
!  Set ret usage switch based in iuseret
!
!-----------------------------------------------------------------------
!
      retsw=0
      IF( nretfil /= 0 ) THEN
        DO isrc=1,nsrcret
          IF(iuseret(isrc) > 0) THEN
            retsw=1
          END IF
        END DO
      END IF

      IF (myproc == 0) THEN
        IF (retsw == 0) THEN
          WRITE(6,'(3x,a,20x,a)') 'Retrieval radar data',' none'
        ELSE
          WRITE(6,'(3x,a,20x,a)') 'Retrieval radar data',' Using'
          DO isrc= 1,icnt
            WRITE(6,'(9x,I0,2a)') isrc,' - ',srcret(isrc)
          END DO
        END IF
        WRITE(6,*) ' '
      END IF
!
!=======================================================================
!
      20    CONTINUE        ! iteration begin below

!-----------------------------------------------------------------------
!
!      OBTAIN THE COST FUNCTION AND THE GRADIENT.
!
!-----------------------------------------------------------------------
!
      IF (myproc == 0 )  WRITE(ount,'(1x,a,I3,a,/,1x,7(a,I0))')           &
               '=========== iteration - ',icall, ' ==========',           &
               'sngsw = ',sngsw,', uasw = ',uasw,  ', convsw = ',convsw,  &
             ', lgtsw = ',lgtsw,', cwpsw = ',cwpsw,', tpwsw = ',tpwsw,    &
             ', radsw = ',radsw

      !CALL mpbarrier
      !if (icall > 1) call arpsstop(" ",1)
      cpuin = f_cputime()

      CALL costf(ount,numctr,ctrv,cfun_single,                          &
            gdu_err,gdv_err,gdp_err,gdt_err,gdq_err,gdw_err,            &
            gdqscalar_err, qscalar_ctr,                                 &
            u_ctr,v_ctr,p_ctr,t_ctr,q_ctr,w_ctr, psi, phi,              &
            gdscal, nx,ny,nz,gdscal_ref,                                &
            nvar,nzua,nzret,                                            &
            mapfct,j1,j2,j3,aj3x,aj3y,aj3z,j3inv,rhobar,                &
            dnw,dn,fnm,fnp,cf1,cf2,cf3,     & ! Added for WRF divergence constraint
            rhostr,rhostru, rhostrv, rhostrw, div3,                     &
            mxsng,mxua,mxret,mxcolret,                                  &
            nsrcsng,nsrcua,nsrcret,ncat,                                &
            ipass,xs,ys,zs,x,y,z,zp,hterain,                            &
            icatg,xcor,nam_var,                                         &
            ownsng,usesng,xsng,ysng,hgtsng,thesng,                      &
            obsng,odifsng,qobsng,qualsng,isrcsng,nlevsng,nobsng,        &
            ownua,useua,xua,yua,hgtua,theua,                            &
            obsua,odifua,qobsua,qualua,isrcua,nlevsua,nobsua,           &
            xretc,yretc,hgtretc,theretc,                                &
            obsret,odifret,qobsret,qualret,                             &
            iret,isrcret,nlevret,ncolret,                               &
            srcsng,srcua,srcret,                                        &
            iusesng(:),iuseua(:),iuseret(:),                            &
            corsng,corua,corret,                                        &
            xsng_p,ysng_p,ihgtsng,xua_p,yua_p,ihgtua,                   &
            zsng_1,zsng_2, zua_1,zua_2,                                 &
            oanxsng,oanxua,oanxret,                                     &
            sngsw,uasw,radsw,lgtsw,cwpsw, tpwsw, retsw,aerisw,          &
            hradius_3d,hradius_3d_ref,radius_z,ref_mos_3d,radius_z_ref, &
            anx,tk,tem1,tem2,tem3,tem4,tem5,tem6,tem7,istatus)

      cpuout=f_cputime()
      cputimearray(1)=cputimearray(1)+(cpuout-cpuin)

      cfun_total=0
      DO i=1,nvar+1
        cfun_total=cfun_total+cfun_single(i)
      END DO

!-----------------------------------------------------------------------
!
!  MPI messages
!
!-----------------------------------------------------------------------

      CALL mpsumdp(cfun_total,1)

      IF (myproc == 0) WRITE(ount,'(/,1x,a,F20.2,/)') 'After COSTF, cfun_total = ',cfun_total

      cfun=cfun_total

!-----------------------------------------------------------------------
!
!  Calculate the gradient of the cost function
!
!-----------------------------------------------------------------------
      cpuin=f_cputime()

      CALL gradt(numctr,ctrv,grad,                                      &
            gdu_err,gdv_err,gdp_err,gdt_err,gdq_err,gdw_err,gdqscalar_err, &
            u_ctr,v_ctr,p_ctr,t_ctr,q_ctr,w_ctr,qscalar_ctr,  psi, phi, &
            gdscal, nx,ny,nz,gdscal_ref,                                &
            nvar,nzua,nzret,                                            &
            mapfct,j1,j2,j3,aj3x,aj3y,aj3z,j3inv,rhobar,                &
            dnw,dn,fnm,fnp,cf1,cf2,cf3,     & ! Added for WRF divergence constraint
            rhostr,rhostru, rhostrv, rhostrw, div3,                     &
            mxsng,mxua,mxret,mxcolret,                                  &
            nsrcsng,nsrcua,nsrcret,ncat,                                &
            ipass,xs,ys,zs,x,y,z,zp,hterain,                            &
            icatg,xcor,nam_var,                                         &
            xsng,ysng,hgtsng,thesng,                                    &
            obsng,odifsng,qobsng,qualsng,isrcsng,nlevsng,nobsng,        &
            xua,yua,hgtua,theua,                                        &
            obsua,odifua,qobsua,qualua,isrcua,nlevsua,nobsua,           &
            xretc,yretc,hgtretc,theretc,                                &
            obsret,odifret,qobsret,qualret,                             &
            usesng,useua,                                               &
            iret,isrcret,nlevret,ncolret,                               &
            srcsng,srcua,srcret,                                        &
            iusesng(:),iuseua(:),iuseret(:),                            &
            corsng,corua,corret,                                        &
            xsng_p,ysng_p,ihgtsng,xua_p,yua_p,ihgtua,                   &
            zsng_1,zsng_2,zua_1,zua_2,                                  &
            oanxsng,oanxua,oanxret,                                     &
            sngsw,uasw,radsw,lgtsw,cwpsw,tpwsw,retsw,aerisw,            &
            hradius_3d,hradius_3d_ref,radius_z,ref_mos_3d,radius_z_ref, &
            anx,tk,                                                     &
            tem1,tem2,tem3,tem4,tem5,tem6,tem4d,                        &
            tem7,tem8,tem9,tem10,tem11,tem12,tem13,tem14,tem15,istatus)

      cpuout=f_cputime()
      cputimearray(2)=cputimearray(2)+(cpuout-cpuin)
!
!-----------------------------------------------------------------------
!
! Call the LBFGS minimization algorithm.
!
!-----------------------------------------------------------------------
!
      cpuin=f_cputime()

      CALL va15ad(ount,numctr,mgra,ctrv,cfun,grad,diagco,diag,iprint,   &
                  eps,swork,ywork,point,work,iflag,ftol)

      cpuout=f_cputime()
      cputimearray(3)=cputimearray(3)+(cpuout-cpuin)

      IF(iflag <= 0) GO TO 50
      icall=icall + 1
      IF(icall > maxin(ipass) ) GO TO 50
      GO TO 20
      50  CONTINUE

!    END DO  ! end of do loop over ipass

    IF (myproc == 0) PRINT *, '-----------------after minimization!---------------------'
!
!-----------------------------------------------------------------------
!
! The arrays for storing the optimal perturbation
!
!-----------------------------------------------------------------------
!
    DO k = 1,nz
      DO j = 1,ny
        DO i = 1,nx
          u_ctr(i,j,k) = 0.0
          v_ctr(i,j,k) = 0.0
            psi(i,j,k) = 0.0
            phi(i,j,k) = 0.0
          p_ctr(i,j,k) = 0.0
          t_ctr(i,j,k) = 0.0
          q_ctr(i,j,k) = 0.0
          w_ctr(i,j,k) = 0.0
        END DO
      END DO
    END DO
    qscalar_ctr = 0.0

    CALL adtrans(numctr,nx,ny,nz,nscalarq,psi,phi, p_ctr,t_ctr,q_ctr,   &
                 w_ctr,qscalar_ctr,ctrv,tem4,ipass)
!
! ----------------------------------------------------------------------
!
!  Static part
!
!-----------------------------------------------------------------------
!
    IF( ibeta1==1 ) THEN

      CALL acct_interrupt(recrsub2_acct)

      IF( flag_legacyfilter ==1 ) THEN

        tem7=0.0; tem8=0.0; tem9=0.0; tem4=0.0
        CALL ctr_to_vbl(ipass_filt(ipass),hradius_3d,radius_z,          &
                        nx,ny,nz,stag1,gdu_err, gdscal,  psi, tem7,tem8,tem9,tem4)

        tem7=0.0; tem8=0.0; tem9=0.0; tem4=0.0
        CALL ctr_to_vbl(ipass_filt(ipass),hradius_3d,radius_z,          &
                        nx,ny,nz,stag2,gdv_err, gdscal,  phi, tem7,tem8,tem9,tem4)

        tem7=0.0; tem8=0.0; tem9=0.0; tem4=0.0
        CALL ctr_to_vbl(ipass_filt(ipass),hradius_3d,radius_z,          &
                        nx,ny,nz,3,gdw_err, gdscal,w_ctr,  tem7,tem8,tem9,tem4)

        tem7=0.0; tem8=0.0; tem9=0.0; tem4=0.0
        CALL ctr_to_vbl(ipass_filt(ipass),hradius_3d,radius_z,          &
                        nx,ny,nz,0,gdp_err, gdscal,p_ctr, tem7,tem8,tem9,tem4)

        tem7=0.0; tem8=0.0; tem9=0.0; tem4=0.0
        CALL ctr_to_vbl(ipass_filt(ipass),hradius_3d,radius_z,          &
                        nx,ny,nz,0,gdt_err, gdscal,t_ctr, tem7,tem8,tem9,tem4)

        tem7=0.0; tem8=0.0; tem9=0.0; tem4=0.0
        CALL ctr_to_vbl(ipass_filt(ipass),hradius_3d,radius_z(:,:,:),    &
                        nx,ny,nz,0,gdq_err, gdscal, q_ctr, tem7,tem8,tem9,tem4)
!
!        CALL ctr_to_vbl(ipass_filt(ipass),0.5*hradius_ref(ipass),radius_z_ref,    &
!                        nx,ny,nz,0,gdq_err, gdscal,q_ctr, tem7,tem8,tem9,tem4)

        IF (ref_da) THEN
          DO nq = 1, nscalarq
          tem7=0.0; tem8=0.0; tem9=0.0; tem4=0.0
          CALL ctr_to_vbl(ipass_filt(ipass),hradius_3d_ref,radius_z_ref,&
                          nx,ny,nz,0,gdqscalar_err(:,:,:,nq), gdscal_ref,&
                          qscalar_ctr(:,:,:,nq), tem7,tem8,tem9,tem4)
          END DO

        END IF !IF (ref_opt ==1 .OR. hydro_opt==1)

      ELSE   ! 2 option for filter flag_legacyfilter ==2

        CALL forwardfilter(nx,ny,nz,nscalar,psi,phi,                    &
                           p_ctr,t_ctr,q_ctr,w_ctr,qscalar_ctr,         &
                        gdu_err,gdv_err,gdp_err,gdt_err,gdq_err,gdw_err,&
                           gdqscalar_err,tem4)

      END IF   !end of filter option

      CALL  acct_stop_inter

      psi(:,:,:)  =  psi(:,:,:)*beta1
      phi(:,:,:)  =  phi(:,:,:)*beta1
      p_ctr(:,:,:)=p_ctr(:,:,:)*beta1
      t_ctr(:,:,:)=t_ctr(:,:,:)*beta1
      q_ctr(:,:,:)=q_ctr(:,:,:)*beta1
      w_ctr(:,:,:)=w_ctr(:,:,:)*beta1
      IF (ref_da) THEN
        qscalar_ctr(:,:,:,:)= qscalar_ctr(:,:,:,:)*beta1
      END IF !IF (ref_opt ==1 .OR. hydro_opt==1)

    END IF


    IF( ibeta2==1 ) THEN
      CALL acct_interrupt(ensemble_acct)

      CALL ens_cost_ctr(nx,ny,nz,nscalarq,flag_legacyfilter,qobsrad_bdyzone(ipass), &
                        ipass,ipass_filt(ipass),ref_opt(ipass), hydro_opt, cwpsw,&
                        psi,phi,w_ctr,t_ctr,p_ctr,q_ctr,qscalar_ctr,      &
                        tem1,tem2,tem3,tem4,tem5,istatus)
      CALL  acct_stop_inter

    END IF    !end of ibeta2==1

    psi(:,:,:)  =   psi(:,:,:)+ anx(:,:,:,PTR_U)
    phi(:,:,:)  =   phi(:,:,:)+ anx(:,:,:,PTR_V)
    w_ctr(:,:,:)= w_ctr(:,:,:)+ anx(:,:,:,PTR_W)
    t_ctr(:,:,:)= t_ctr(:,:,:)+ anx(:,:,:,PTR_PT)
    p_ctr(:,:,:)= p_ctr(:,:,:)+ anx(:,:,:,PTR_P)
    q_ctr(:,:,:)= q_ctr(:,:,:)+ anx(:,:,:,PTR_QV)

    IF( ref_da) THEN
      DO nq = 1, nscalarq
        qscalar_ctr(:,:,:,nq)=qscalar_ctr(:,:,:,nq)+ anx(:,:,:,PTR_LN+nq)
      END DO
    END IF
!
!-----------------------------------------------------------------------
!
! Option cntl_var = 0  U,V as control variables
! Option cntl_var = 1  psi, phi as control variable
!
!-----------------------------------------------------------------------
!
    !IF(cntl_var  == 0 ) THEN

      DO k = 1, nz-1
        DO j = 1, ny-1
          DO i = 1, nx
            u_ctr(i,j,k) =  psi(i,j,k)
          END DO
        END DO
      END DO

      DO k = 1, nz-1
        DO j = 1, ny
          DO i = 1, nx-1
            v_ctr(i,j,k) =  phi(i,j,k)
          END DO
        END DO
      END DO

    !ELSE
    !
    !  DO k = 1, nz-1
    !    DO j = 2, ny-1
    !      DO i = 2, nx-1
    !        u_ctr(i,j,k) = ( psi(i-1,j+1,k)+psi(i,j+1,k)                &
    !                        -psi(i-1,j-1,k)-psi(i,j-1,k) )/dy/4.        &
    !                     + ( phi(i,  j,  k)-phi(i-1,j,k) )/dx
    !        u_ctr(i,j,k) = u_ctr(i,j,k)*mapfct(i,j,2)
    !
    !        v_ctr(i,j,k) = ( psi(i+1,j-1,k)+psi(i+1,j,k)                &
    !                        -psi(i-1,j-1,k)-psi(i-1,j,k) )/dx/4.        &
    !                     + ( phi(i,  j,  k)-phi(i,j-1,k) )/dy
    !        v_ctr(i,j,k) = v_ctr(i,j,k)*mapfct(i,j,3)
    !      END DO
    !    END DO
    !
    !    DO j=2,ny-1
    !      u_ctr( 1,j,k)=u_ctr( 2,j,k)+u_ctr( 2,j,k)-u_ctr( 3,j,k)
    !      v_ctr( 1,j,k)=v_ctr( 2,j,k)+v_ctr( 2,j,k)-v_ctr( 3,j,k)
    !      u_ctr(nx,j,k)=u_ctr(nx-1,j,k)+u_ctr(nx-1,j,k)-u_ctr(nx-2,j,k) ! Not necessary since analysis is
    !      v_ctr(nx,j,k)=v_ctr(nx-1,j,k)+v_ctr(nx-1,j,k)-v_ctr(nx-2,j,k) ! on SCALAR grid
    !    END DO
    !
    !    DO i=2,nx-1
    !      u_ctr(i, 1,k)=u_ctr(i, 2,k)+u_ctr(i, 2,k)-u_ctr(i, 3,k)
    !      v_ctr(i, 1,k)=v_ctr(i, 2,k)+v_ctr(i, 2,k)-v_ctr(i, 3,k)
    !      u_ctr(i,ny,k)=u_ctr(i,ny-1,k)+u_ctr(i,ny-1,k)-u_ctr(i,ny-2,k) ! Not necessary since analysis is
    !      v_ctr(i,ny,k)=v_ctr(i,ny-1,k)+v_ctr(i,ny-1,k)-v_ctr(i,ny-2,k) ! on SCALAR grid
    !    END DO
    !
    !    u_ctr(1,1 ,k)  = 0.5*( u_ctr(2,1,k)+u_ctr(1,2,k) )
    !    v_ctr(1,1 ,k)  = 0.5*( v_ctr(2,1,k)+v_ctr(1,2,k) )
    !    u_ctr(1,ny,k)  = 0.5*( u_ctr(1,ny-1,k)+u_ctr(2,ny,k) )      ! Not necessary since analysis is
    !    v_ctr(1,ny,k)  = 0.5*( v_ctr(1,ny-1,k)+v_ctr(2,ny,k) )      ! on SCALAR grid
    !
    !    u_ctr(nx,1,k)  = 0.5*( u_ctr(nx-1,1,k)+u_ctr(nx,2,k) )      ! Not necessary since analysis is
    !    v_ctr(nx,1,k)  = 0.5*( v_ctr(nx-1,1,k)+v_ctr(nx,2,k) )      ! on SCALAR grid
    !    u_ctr(nx,ny,k) = 0.5*( u_ctr(nx,ny-1,k)+u_ctr(nx-1,ny,k) )
    !    v_ctr(nx,ny,k) = 0.5*( v_ctr(nx,ny-1,k)+v_ctr(nx-1,ny,k) )
    !
    !  END DO
    !
    !  IF (mp_opt > 0) THEN
    !    CALL acct_interrupt(mp_acct)
    !    CALL mpsendrecv2dew(u_ctr, nx, ny, nz, ebc, wbc, 0, tem4)
    !    CALL mpsendrecv2dns(u_ctr, nx, ny, nz, nbc, sbc, 0, tem4)
    !
    !    CALL mpsendrecv2dew(v_ctr, nx, ny, nz, ebc, wbc, 0, tem4)
    !    CALL mpsendrecv2dns(v_ctr, nx, ny, nz, nbc, sbc, 0, tem4)
    !    CALL acct_stop_inter
    !  END IF
    !END IF

!    IF (modelopt==1) THEN
!      CALL smooth3d(nx,ny,nz,1,nx,  1,ny-1,1,nz-1,1,0.05,zp,u_ctr,tem1,u_ctr)
!      CALL smooth3d(nx,ny,nz,1,nx-1,1,ny,  1,nz-1,2,0.05,zp,v_ctr,tem2,v_ctr)
!      CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz,  3,0.05,zp,w_ctr,tem3,w_ctr)
!      CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,p_ctr,tem4,p_ctr)
!      CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,t_ctr,tem5,t_ctr)
!      CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,q_ctr,tem6,q_ctr)
!      CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,qc_ctr,tem7,qc_ctr)
!      CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,qr_ctr,tem8,qr_ctr)
!      CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,qi_ctr,tem9,qi_ctr)
!      CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,qs_ctr,tem10,qs_ctr)
!      CALL smooth3d(nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,0,0.05,zp,qh_ctr,tem11,qh_ctr)
!    ELSE IF (modelopt==2) THEN
!      ips = 1; ipe = nx-1; jps = 1; jpe = ny-1; kps = 2; kpe = nz-2
!      IF (loc_x == 1) ips = 2; IF (loc_x == nproc_x) ipe = nx-2
!      IF (loc_y == 1) jps = 2; IF (loc_y == nproc_y) jpe = ny-2
!      CALL smooth3d(nx,ny,nz,ips,ipe+1,jps,jpe,  kps,kpe,  1,0.05,zp,u_ctr,tem1,u_ctr)
!      CALL smooth3d(nx,ny,nz,ips,ipe,  jps,jpe+1,kps,kpe,  2,0.05,zp,v_ctr,tem2,v_ctr)
!      CALL smooth3d(nx,ny,nz,ips,ipe,  jps,jpe,  kps,kpe+1,3,0.05,zp,w_ctr,tem3,w_ctr)
!      CALL smooth3d(nx,ny,nz,ips,ipe,  jps,jpe,  kps,kpe,  0,0.05,zp,p_ctr,tem4,p_ctr)
!      CALL smooth3d(nx,ny,nz,ips,ipe,  jps,jpe,  kps,kpe,  0,0.05,zp,t_ctr,tem5,t_ctr)
!      CALL smooth3d(nx,ny,nz,ips,ipe,  jps,jpe,  kps,kpe,  0,0.05,zp,q_ctr,tem6,q_ctr)
!      CALL smooth3d(nx,ny,nz,ips,ipe,  jps,jpe,  kps,kpe,  0,0.05,zp,qc_ctr,tem7,qc_ctr)
!      CALL smooth3d(nx,ny,nz,ips,ipe,  jps,jpe,  kps,kpe,  0,0.05,zp,qr_ctr,tem8,qr_ctr)
!      CALL smooth3d(nx,ny,nz,ips,ipe,  jps,jpe,  kps,kpe,  0,0.05,zp,qi_ctr,tem9,qi_ctr)
!      CALL smooth3d(nx,ny,nz,ips,ipe,  jps,jpe,  kps,kpe,  0,0.05,zp,qs_ctr,tem10,qs_ctr)
!      CALL smooth3d(nx,ny,nz,ips,ipe,  jps,jpe,  kps,kpe,  0,0.05,zp,qh_ctr,tem11,qh_ctr)
!    END IF

!
!-----------------------------------------------------------------------
!
!  add the optimal perturbation to background
!
!-----------------------------------------------------------------------
!
!------------------changeb by GAO---------------------------------------
    DO k = 1, nz
      DO j = 1, ny
        DO i = 1, nx
          anx(i,j,k,PTR_U)  = u_ctr(i,j,k)          !+ anx(i,j,k,1)
          anx(i,j,k,PTR_V)  = v_ctr(i,j,k)          !+ anx(i,j,k,2)
          anx(i,j,k,PTR_P)  = p_ctr(i,j,k)          !+ anx(i,j,k,3)
          anx(i,j,k,PTR_PT) = t_ctr(i,j,k)          !+ anx(i,j,k,4)
          anx(i,j,k,PTR_QV) = q_ctr(i,j,k)          !+ anx(i,j,k,5)
          anx(i,j,k,PTR_W)  = w_ctr(i,j,k)          !+ anx(i,j,k,6)
          IF ( anx(i,j,k,PTR_QV) <= 0.) anx(i,j,k,PTR_QV) = 0.0
        END DO
      END DO
    END DO

    IF (ref_da) THEN
      DO k = 1, nz
        DO j = 1, ny
          DO i = 1, nx
            DO nq = 1, nscalarq
              anx(i,j,k,PTR_LN+nq) = qscalar_ctr(i,j,k,nq)        !+ anx(i,j,k,7)
              IF ( anx(i,j,k,PTR_LN+nq) <= 0.) anx(i,j,k,PTR_LN+nq) = 0.0
            END DO

            IF(idealCase_opt /= 1) THEN
              IF ( anx(i,j,k,PTR_LN+P_QR) <= 1E-7 ) THEN
                anx(i,j,k,PTR_LN+P_QR) = 0.0
              END IF

              IF ( anx(i,j,k,PTR_LN+P_QS) <= 1E-7 ) THEN
                anx(i,j,k,PTR_LN+P_QS) =  0.0
              END IF

              IF ( anx(i,j,k,PTR_LN+P_QG) <= 1E-7 ) THEN
                anx(i,j,k,PTR_LN+P_QG) =  0.0
              END IF

              IF ( anx(i,j,k,PTR_LN+P_QH) <= 1E-7 ) THEN
                anx(i,j,k,PTR_LN+P_QH) =  0.0
              END IF
            END IF

          END DO
        END DO
      END DO
    END IF ! IF (ref_opt==1.OR. hydro_opt==1)

  END IF  ! assim_opt == 1

!----------------------------------------------------------------------
!
! Clear up before Return
!
!----------------------------------------------------------------------

  istatus=0

  CALL radargrid_final(istatus)

  RETURN
END SUBROUTINE minimization
