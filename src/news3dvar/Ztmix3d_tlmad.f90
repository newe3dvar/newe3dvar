
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TLMMIXUVW                  ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tlmmixuvw(nx,ny,nz,nstepss,nbasic,                           &
           u,v,w,ptprt,pprt,qv,qscalar,tke,                             &
           ubar,vbar,ptbar,pbar,rhostr,qvbar,                           &
           usflx,vsflx, x,y,z,zp, j1,j2,j3,stabchk,                     &
           umix,vmix,wmix,km,lendel,defsq,                              &
           tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,                &
           tem10,tem11,tem12)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the total mixing (turbulent mixing and the externally
!  imposed computational mixing) for the momentum equations. This
!  subroutine also calculates the turbulent mixing coefficient ,km,
!  which is used to calculate mixing terms for temperature and
!  water quantities. The mixing coefficient is based on Smagorinsky's
!  formulation. For the computational mixing, there are two options:
!  second order and fourth order mixing. The mixing applies only to
!  the perturbations quantities.
!
!  TLMMIXUVW is the tangent linear model of MIXUVW.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  04/22/96
!
!  MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nstepss  The number of integrations plus one.
!    nbasic   Index of the current basic state time level.
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates at a given time level (m/s)
!    ptprt    Perturbation potential temperature at a given time level (K)
!    pprt     Perturbation pressure at a given time level (Pascal)
!    qv       Water vapor specific humidity at a given time level (kg/kg)
!    qc       Cloud water mixing ratio at a given time level (kg/kg)
!    qr       Rainwater mixing ratio at a given time level (kg/kg)
!    qi       Cloud ice mixing ratio at a given time level (kg/kg)
!    qs       Snow mixing ratio at a given time level (kg/kg)
!    qh       Hail mixing ratio at a given time level (kg/kg)
!    tke      Turbulent Kinetic Energy ((m/s)**2)
!
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!
!    usflx    Surface flux of u-momentum
!    vsflx    Surface flux of v-momentum
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space (m)
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!    Basic state variables:
!    nu,nv,nw,nptprt,npprt,nqv,nqc,nqr,nqi,nqs,nqh
!    nustr,nvstr,nwstr.
!
!  OUTPUT:
!
!    umix     Total mixing in u equation (kg/(m*s)**2)
!    vmix     Total mixing in v equation (kg/(m*s)**2)
!    wmix     Total mixing in w equation (kg/(m*s)**2)
!    km       Turbulent mixing coefficient for momentum ( m**2/s ).
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3).
!    defsq    Deformation squared (1/s**2)
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!    tem4     Temporary work array.
!    tem5     Temporary work array.
!    tem6     Temporary work array.
!    tem7     Temporary work array.
!    tem8     Temporary work array.
!    tem9     Temporary work array.
!    tem10    Temporary work array.
!    tem11    Temporary work array.
!    tem12    Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  !INCLUDE 'variable.inc'    ! Basic state variable.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  INTEGER :: nstepss      ! Index of total current basic state time levels.
  INTEGER :: nbasic       ! Index of the current basic state time level.
!
  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)
  REAL :: ptprt (nx,ny,nz)     ! Perturbation potential temperature (K)
  REAL :: pprt  (nx,ny,nz)     ! Perturbation pressure (Pascal)
!
  REAL :: qv    (nx,ny,nz)     ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq)     ! Cloud water mixing ratio (kg/kg)
  REAL :: tke   (nx,ny,nz)     ! Turbulent Kinetic Energy ((m/s)**2)
!
  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific humidity
                               ! (kg/kg)

  REAL :: usflx (nx,ny)        ! surface flux of u-momentum (kg/(m*s**2))
  REAL :: vsflx (nx,ny)        ! surface flux of v-momentum (kg/(m*s**2))
  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.

                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.


  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! d( zp )/d( z ).
  REAL :: stabchk(nx,ny)
!
  REAL :: umix  (nx,ny,nz)     ! Turbulent mixing on u-momentum.
  REAL :: vmix  (nx,ny,nz)     ! Turbulent mixing on v-momentum.
  REAL :: wmix  (nx,ny,nz)     ! Turbulent mixing on w-momentum.

  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient for
                               ! momentum. ( m**2/s )

  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)

  REAL :: defsq (nx,ny,nz)     ! Deformation squared (1/s**2)
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
  REAL :: tem4  (nx,ny,nz)     ! Temporary work array
  REAL :: tem5  (nx,ny,nz)     ! Temporary work array
  REAL :: tem6  (nx,ny,nz)     ! Temporary work array
  REAL :: tem7  (nx,ny,nz)     ! Temporary work array
  REAL :: tem8  (nx,ny,nz)     ! Temporary work array
  REAL :: tem9  (nx,ny,nz)     ! Temporary work array
  REAL :: tem10 (nx,ny,nz)     ! Temporary work array
  REAL :: tem11 (nx,ny,nz)     ! Temporary work array
  REAL :: tem12 (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: nxyz,i,j,k

!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Control parameters for subgrid scalar turbulent mixing are passed
!  in globcst.inc, these include:
!
!  tmixopt: Tubulent mixing option
!           = 0, zero turbulent mixing.
!           = 1, constant mixing coefficient.
!           = 2, Smagorinsky mixing coefficient.
!           = 3, Smagorinsky + constant coefficient mixing.
!  tmixcst: Constant mixing coefficient. (Options 1 and 3)
!
!-----------------------------------------------------------------------
!
! IF( tmixopt == 0) THEN

!   nxyz = nx*ny*nz
!   CALL flzero(umix,nxyz)
!   CALL flzero(vmix,nxyz)
!   CALL flzero(wmix,nxyz)

!  ELSE

    CALL tlmtmixuvw(nx,ny,nz,nstepss,nbasic,                            &
         u,v,w,ptprt,pprt,qv,qscalar,tke,                               &
         ubar,vbar,ptbar,pbar,rhostr,qvbar,                             &
         usflx,vsflx, x,y,z,zp, j1,j2,j3,stabchk,                       &
         umix,vmix,wmix,km, lendel,defsq,                               &
         tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,                  &
         tem10,tem11,tem12)

!  END IF
!
  IF( cmix2nd == 1) THEN
!
!-----------------------------------------------------------------------
!
!  Calculate the second order computational mixing terms and add
!  them to the turbulent mixing terms umix, vmix, wmix.
!
!-----------------------------------------------------------------------
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem7(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
        END DO
      END DO
    END DO

    CALL tlmcmix2uvw(nx,ny,nz,                                          &
                  u,v,w, ubar,vbar,tem7,                                &
                  umix,vmix,wmix,                                       &
                  tem1,tem2,tem3,tem4,tem5,tem6)

  END IF
!
IF(1==0) THEN
  IF( cmix4th == 1) THEN
!
!-----------------------------------------------------------------------
!
!  Calculate the fourth order computational mixing terms and add
!  them to the turbulent mixing terms umix, vmix, wmix.
!
!-----------------------------------------------------------------------
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem7(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
        END DO
      END DO
    END DO

    CALL cmix4uvw0(nx,ny,nz,                                             &
                  u,v,w, ubar,vbar,tem7,                                &
                  umix,vmix,wmix,                                       &
                  tem1,tem2,tem3,tem4,tem5,tem6)
  END IF

END IF !(1==0)
!
!
  RETURN
END SUBROUTINE tlmmixuvw
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TLMTMIXUVW                 ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE tlmtmixuvw(nx,ny,nz,nstepss,nbasic,                          &
           u,v,w,ptprt,pprt,qv,qscalar,tke,                      &
           ubar,vbar,ptbar,pbar,rhostr,qvbar,                           &
           usflx,vsflx, x,y,z,zp, j1,j2,j3,stabchk,                     &
           umix,vmix,wmix,km,lendel,defsq,                              &
           nsqed,tau11,tau12,tau13,tau22,tau23,tau33,tem1,tem2,         &
           tem3,tem4,tem5)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the turbulent mixing terms for the momentum equations. These
!  terms are expressed in the form of a stress tensor.
!
!  TLMTMIXUVW is the tangent linear model of TMIXUVW.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  04/24/1995
!
!  MODIFICATION HISTORY:
!
!  06/23/96   (Jidong Gao)
!  Convered to version 4.0
!
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nstepss  The number of integrations plus one.
!    nbasic   Index of the current basic state time level.
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates at a given time level (m/s)
!    ptprt    Perturbation potential temperature at a given time level (K)
!    pprt     Perturbation pressure at a given time level (Pascal)
!    qv       Water vapor specific humidity at a given time level (kg/kg)
!    qc       Cloud water mixing ratio at a given time level (kg/kg)
!    qr       Rainwater mixing ratio at a given time level (kg/kg)
!    qi       Cloud ice mixing ratio at a given time level (kg/kg)
!    qs       Snow mixing ratio at a given time level (kg/kg)
!    qh       Hail mixing ratio at a given time level (kg/kg)
!    tke      Turbulent Kinetic Energy ((m/s)**2)
!
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!
!    usflx    Surface flux of u-momentum
!    vsflx    Surface flux of v-momentum
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space (m)
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!    Basic state variables:
!    nu,nv,nw,nptprt,npprt,nqv,nqc,nqr,nqi,nqs,nqh
!    nustr,nvstr,nwstr.
!
!  OUTPUT:
!
!    umix     Total mixing in u equation (kg/(m*s)**2)
!    vmix     Total mixing in v equation (kg/(m*s)**2)
!    wmix     Total mixing in w equation (kg/(m*s)**2)
!    km       Turbulent mixing coefficient for momentum ( m**2/s ).
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3)
!    defsq    Deformation squared (1/s**2)
!
!  WORK ARRAYS:
!
!    real nsqed  Temporary array containing the Brunt Vaisala frquency
!    real tau11  Temporary work array
!    real tau12  Temporary work array
!    real tau13  Temporary work array
!    real tau22  Temporary work array
!    real tau23  Temporary work array
!    real tau33  Temporary work array
!    real tem1   Temporary work array
!    real tem2   Temporary work array
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'

  !INCLUDE 'variable.inc'    ! Basic state variable.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  INTEGER :: tpast             ! Index of time level for the past time.
  INTEGER :: tpresent          ! Index of time level for the present
  INTEGER :: nstepss      ! Index of total current basic state time levels.
  INTEGER :: nbasic       ! Index of the current basic state time level.

  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)
  REAL :: ptprt (nx,ny,nz)     ! Perturbation potential temperature (K)
  REAL :: pprt  (nx,ny,nz)     ! Perturbation pressure (Pascal)
!
  REAL :: qv    (nx,ny,nz)     ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq)     ! Cloud water mixing ratio (kg/kg)
  REAL :: tke   (nx,ny,nz)     ! Turbulent Kinetic Energy ((m/s)**2)
!
  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific humidity
                               ! (kg/kg)

  REAL :: usflx (nx,ny)        ! surface flux of u-momentum (kg/(m*s**2))
  REAL :: vsflx (nx,ny)        ! surface flux of v-momentum (kg/(m*s**2))
  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.


  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! d( zp )/d( z ).
  REAL :: stabchk(nx,ny)
!
  REAL :: umix  (nx,ny,nz)     ! Turbulent mixing on u-momentum.
  REAL :: vmix  (nx,ny,nz)     ! Turbulent mixing on v-momentum.
  REAL :: wmix  (nx,ny,nz)     ! Turbulent mixing on w-momentum.

  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient for
                               ! momentum. ( m**2/s )

  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)

  REAL :: defsq (nx,ny,nz)     ! Deformation squared (1/s**2)
!
  REAL :: nsqed (nx,ny,nz)     ! Temporary work array
  REAL :: tau11 (nx,ny,nz)     ! Temporary work array
  REAL :: tau12 (nx,ny,nz)     ! Temporary work array
  REAL :: tau13 (nx,ny,nz)     ! Temporary work array
  REAL :: tau22 (nx,ny,nz)     ! Temporary work array
  REAL :: tau23 (nx,ny,nz)     ! Temporary work array
  REAL :: tau33 (nx,ny,nz)     ! Temporary work array
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
  REAL :: tem4  (nx,ny,nz)     ! Temporary work array
  REAL :: tem5  (nx,ny,nz)     ! Temporary work array
!
!
!! define array should be deleted aftering testing !!!
  REAL :: bem1(nx,ny,nz)
  REAL :: bem2(nx,ny,nz)
  REAL :: bem3(nx,ny,nz)
  REAL :: bem4(nx,ny,nz)
  REAL :: bem5(nx,ny,nz)
  REAL :: bem6(nx,ny,nz)
  REAL :: sum1, sum2, sum3
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
  REAL :: tema,temb,temc
  REAL :: zpup,zplow

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Calculate the static stability parameter N squared (Brunt-Vaisala
!  frequency). The arrays tem1,tem2,tau11,tau12 are used as work
!  arrays for the following call.
!
!  If tmixopt.eq.1, stabnsq or nsqed is useless.
!-----------------------------------------------------------------------
!
IF(1==0) THEN
  IF (tmixopt /= 1) THEN
    CALL stabnsq0(nx,ny,nz,                                             &
                 ptprt,pprt,qv,qscalar,ptbar,qvbar,pbar,j3,             &
                 nsqed,tem1,tem2,tem3,tem4)
  END IF
!
END IF
!
!-----------------------------------------------------------------------
!
!  Calculate the deformation tensor components Dij, which are stored
!  in arrays Tauij. deform is linear.
!
!-----------------------------------------------------------------------

  IF(1==0) THEN
    DO k=1,nz
    DO j=1,ny
    DO i=1,nx
      bem1(i,j,k) = u(i,j,k)
      bem2(i,j,k) = v(i,j,k)
      bem3(i,j,k) = w(i,j,k)
    END DO
    END DO
    END DO
  END IF
    tau11=0.0;tau12=0.0;tau13=0.0
    tau22=0.0;tau23=0.0;tau33=0.0

  CALL deform0(nx,ny,nz,u,v,w,j1,j2,j3,                                  &
      tau11,tau12,tau13,tau22,tau23,tau33,tem1,tem2,tem3)
!
  IF (1==0) THEN
    sum1=0.
    CALL product(tau11,tau11,                                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'tau11==',             sum2
    CALL product(tau12,tau12,                                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'tau12==',             sum2
    CALL product(tau13,tau13,                                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'tau13==',             sum2
    CALL product(tau22,tau22,                                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'tau22==',             sum2
    CALL product(tau23,tau23,                                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'tau23==',             sum2
    CALL product(tau33,tau33,                                              &
                 nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,sum2)
    sum1=sum1+sum2
    PRINT*,'tau33==',             sum2
!   PRINT*,'deformm0 (AQ)*(AQ)= ',sum1

    u(:,:,:)=0.0
    v(:,:,:)=0.0
    w(:,:,:)=0.0
!
   CALL addeform(nx,ny,nz,u,v,w,j1,j2,j3,                                &
       tau11,tau12,tau13,tau22,tau23,tau33,tem1,tem2,tem3)

    sum3=0.
    CALL product(u,bem1,                                                 &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    PRINT*,'uuuuu==',             sum2
!
    CALL product(v,bem2,                                                 &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    PRINT*,'vvvvv==',             sum2
!
    CALL product(w,bem3,                                                 &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    PRINT*,'wwwww==',             sum2

    PRINT*,'deformm0*(AQ) sum1 = ',sum1
    PRINT*,'deformm0*(AQ) sum3 = ',sum3
    STOP
  END IF
!
!-----------------------------------------------------------------------
!
!  Calculate the turbulent mixing coefficient, km, using
!  Smagorinsky's formulation
!  If tmixopt.eq.1, then turbulent mixing coefficient is assigned
!  as constants. When tmixopt.eq.1, cftmix is linear!!!
!
!-----------------------------------------------------------------------
!

  CALL cftmix0(nx,ny,nz,tau11,tau12,tau13,tau22,tau23,tau33,             &
              nsqed, zp,tke, km,lendel,defsq, tem1)

!
!-----------------------------------------------------------------------
!
!  Calculate the stress tensor Tauij = km * (rhostr/j3) * Dij.
!
!  Please note umix and vmix are used here to temporarily store
!  tau31 and tau32, which are used immediatedly by wmixtrm.
!  umix and vmix are freed afterwards.
!
!  If km is constant, than Tauij is linear!
!
!-----------------------------------------------------------------------
!

  IF(1==0) THEN
    DO k=1,nz
    DO j=1,ny
    DO i=1,nx
      bem1(i,j,k) = tau11(i,j,k)
      bem2(i,j,k) = tau22(i,j,k)
      bem3(i,j,k) = tau33(i,j,k)
      bem4(i,j,k) = tau12(i,j,k)
      bem5(i,j,k) = tau13(i,j,k)
      bem6(i,j,k) = tau23(i,j,k)
    END DO
    END DO
    END DO
  END IF


  CALL stress0(nx,ny,nz,j3,zp,                                           &
              km,rhostr,tau11,tau12,tau13,tau22,tau23,tau33,             &
              umix,vmix,                                                 &
              tem1,tem2,tem3)
!
  IF (1==0) THEN
    sum1=0.
    CALL product(tau11,tau11,                                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'tau11==',             sum2
    CALL product(tau12,tau12,                                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'tau12==',             sum2
    CALL product(tau13,tau13,                                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'tau13==',             sum2
    CALL product(tau22,tau22,                                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'tau22==',             sum2
    CALL product(tau23,tau23,                                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'tau23==',             sum2
    CALL product(tau33,tau33,                                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'tau33==',             sum2

    CALL product(umix,umix,                                                &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'umix ==',             sum2
    CALL product(vmix,vmix,                                                &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'vmix ==',             sum2
!   PRINT*,'deformm0 (AQ)*(AQ)= ',sum1
!
!
  CALL adstress(nx,ny,nz,j3,zp,                                         &
              km,rhostr,tau11,tau12,tau13,tau22,tau23,tau33,            &
              umix,vmix,                                                &
              tem1,tem2,tem3)

    sum3=0.
    CALL product(tau11,bem1,                                                 &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    PRINT*,'tau11==',             sum2
!
    CALL product(tau22,bem2,                                                 &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    PRINT*,'tau22==',             sum2
!
    CALL product(tau33,bem3,                                                 &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    PRINT*,'tau33==',             sum2
!
    CALL product(tau12,bem4,                                                 &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    PRINT*,'tau12==',             sum2
!
    CALL product(tau13,bem5,                                                 &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    PRINT*,'tau13==',             sum2
!
    CALL product(tau23,bem6,                                                 &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    PRINT*,'tau23==',             sum2

    PRINT*,'stress0*(AQ) sum1 = ',sum1
    PRINT*,'stress0*(AQ) sum3 = ',sum3
    STOP
  END IF

  IF(1==0) THEN
    DO k=1,nz
    DO j=1,ny
    DO i=1,nx
      bem1(i,j,k) = umix(i,j,k)
      bem2(i,j,k) = vmix(i,j,k)
      bem3(i,j,k) = tau33(i,j,k)
    END DO
    END DO
    END DO
  END IF

  CALL wmixtrm0(nx,ny,nz, j1,j2,j3, umix,vmix,tau33,                     &
               wmix, tem1, tem2, tem3)
!
  IF (1==0) THEN
    sum1=0.
    CALL product(wmix,wmix,                                                &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'wmix ==',             sum2
!   PRINT*,'deformm0 (AQ)*(AQ)= ',sum1
!
!
  umix=0.0; vmix=0.0; tau33=0.0
  CALL adwmixtrm(nx,ny,nz, j1,j2,j3, umix,vmix,tau33,                   &
               wmix, tem1,tem2,tem3)

    sum3=0.
    CALL product(umix,bem1,                                                 &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    PRINT*,'tau11==',             sum2
!
    CALL product(vmix,bem2,                                                 &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    PRINT*,'tau22==',             sum2
!
    CALL product(tau33,bem3,                                                 &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    PRINT*,'tau33==',             sum2
!
    PRINT*,'wmixtrm0*(AQ) sum1 = ',sum1
    PRINT*,'wmixtrm0*(AQ) sum3 = ',sum3
    STOP
  END IF

!
!-----------------------------------------------------------------------
!
!
!  Set the surface momentum fluxes:
!

!  usflx and vsflx, which are calculated in the surface physics
!  subroutine sfcflx.
!
!  If sfcphy = 0, the surface momentum fluxes are calculated in the same way
!  as in the the domain interior (i.e., using a subgrid scale turbulence
!  parameterization).
!
!-----------------------------------------------------------------------
!
IF(1==0) THEN
  IF( sfcphy /= 0 ) THEN
!
    IF ( (sflxdis == 0) .OR. (sflxdis == 1 .AND.                        &
           (sfcphy == 1.OR.sfcphy == 3).AND.cdmlnd == 0.0) .OR.         &
           (sflxdis == 2) ) THEN
!
!-----------------------------------------------------------------------
!
!  tau13 at the surface = usflx.
!
!-----------------------------------------------------------------------
!
      DO j=2,ny-2
        DO i=2,nx-1
          tau13(i,j,2) = usflx(i,j)
        END DO
      END DO
!
!-----------------------------------------------------------------------
!
!  tau23 at the surface = vsflx.
!
!-----------------------------------------------------------------------
!
      DO j=2,ny-1
        DO i=2,nx-2
          tau23(i,j,2) = vsflx(i,j)
        END DO
      END DO

    ELSE IF (sflxdis == 1)  THEN

      DO j=2,ny-2
        DO i=2,nx-1
!         tema = 0.5*(stabchk(i,j)+stabchk(i-1,j))
	  temb = stabchk(i  ,j) - ( ptprt(i  ,j,2)+ptbar(i  ,j,2) )
	  temc = stabchk(i-1,j) - ( ptprt(i-1,j,2)+ptbar(i-1,j,2) )
	  tema = 0.5*(temb + temc)

          DO k=2,nz-1
            zpup =0.5*(zp(i,j,k)+zp(i-1,j,k))
            zplow=0.5*(zp(i,j,2)+zp(i-1,j,2))
            IF ( (tema <= 0.) .OR. ((zpup-zplow) > pbldpth0))  CYCLE

            tau13(i,j,k)=0.5*usflx(i,j)*                                &
                         (1.0-(zpup-zplow)/pbldpth0)**2

          END DO
        END DO
      END DO

      DO j=2,ny-1
        DO i=2,nx-2
!         tema = 0.5*(stabchk(i,j)+stabchk(i,j-1))
	  temb = stabchk(i  ,j) - ( ptprt(i  ,j,2)+ptbar(i  ,j,2) )
	  temc = stabchk(i,j-1) - ( ptprt(i,j-1,2)+ptbar(i,j-1,2) )

          DO k=2,nz-1
            zpup =0.5*(zp(i,j,k)+zp(i,j-1,k))
            zplow=0.5*(zp(i,j,2)+zp(i,j-1,2))
            IF ( (tema <= 0.) .OR. ((zpup-zplow) > pbldpth0))  CYCLE

            tau23(i,j,k)=0.5*vsflx(i,j)*                                &
                         (1.0-(zpup-zplow)/pbldpth0)**2

          END DO
        END DO
      END DO

    END IF
  END IF
!!
END IF
!
!-----------------------------------------------------------------------
!
!  Calculate the divergence of the stresses. This is the turbulent
!  mixing on u and v momentum (umix and vmix).
!  umixtrm, vmixtrm and wmixtrm are linear.
!-----------------------------------------------------------------------
!
  IF(1==0) THEN
    DO k=1,nz
    DO j=1,ny
    DO i=1,nx
      bem1(i,j,k) = tau11(i,j,k)
      bem2(i,j,k) = tau12(i,j,k)
      bem3(i,j,k) = tau13(i,j,k)
    END DO
    END DO
    END DO
    umix=0.0; tem1=0.0; tem2=0.0; tem3=0.0
  END IF
!
  CALL umixtrm0(nx,ny,nz, j1,j2,j3, tau11,tau12,tau13,                   &
               umix, tem1, tem2, tem3)
!
  IF (1==0) THEN
    sum1=0.
    CALL product(umix,umix,                                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'umixtrm0 (AQ)*(AQ)= ',sum1
!
    tau11=0.0; tau12=0.0; tau13=0.0
!
  CALL adumixtrm(nx,ny,nz, j1,j2,j3, tau11,tau12,tau13,                &
               umix, tem1,tem2,tem3)
!
    sum3=0.
    CALL product(tau11,bem1,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    print*,'tau11======',sum2
!
    CALL product(tau12,bem2,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    print*,'tau12======',sum2
!
    CALL product(tau13,bem3,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    print*,'tau13======',sum2

    PRINT*,'umixtrm0*(AQ) sum1 = ',sum1
    PRINT*,'umixtrm0*(AQ) sum3 = ',sum3
    STOP
  END IF


  IF(1==0) THEN
    DO k=1,nz
    DO j=1,ny
    DO i=1,nx
      bem1(i,j,k) = tau12(i,j,k)
      bem2(i,j,k) = tau22(i,j,k)
      bem3(i,j,k) = tau23(i,j,k)
    END DO
    END DO
    END DO
  END IF

  CALL vmixtrm0(nx,ny,nz, j1,j2,j3, tau12,tau22,tau23,                   &
               vmix, tem1, tem2, tem3)
!
!
  IF (1==0) THEN
    sum1=0.
    CALL product(vmix,vmix,                                              &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum1=sum1+sum2
    PRINT*,'vmixtrm0 (AQ)*(AQ)= ',sum1

    tau12=0.0; tau22=0.0; tau23=0.0
    CALL advmixtrm(nx,ny,nz, j1,j2,j3, tau12,tau22,tau23,                &
               vmix, tem1,tem2,tem3)

    sum3=0.
    CALL product(tau12,bem1,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    print*,'tau12======',sum2
!
    CALL product(tau22,bem2,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    print*,'tau22======',sum2
!
    CALL product(tau23,bem3,                                             &
                 nx,ny,nz,1,nx,1,ny,1,nz,sum2)
    sum3=sum3+sum2
    print*,'tau23======',sum2

    PRINT*,'vmixtrm0*(AQ) sum1 = ',sum1
    PRINT*,'vmixtrm0*(AQ) sum3 = ',sum3
    STOP
  END IF

!
!-----------------------------------------------------------------------
!
!  Set the mixing terms on the lateral boundaries to zero
!
!  The umix and vmix terms are set for completness, they are not
!  used in any calculations.
!
!-----------------------------------------------------------------------
!
  IF (1 == 0) THEN  ! If they are not used, why calculate them? David

   DO 51 k=2,nz-2
   DO 51 j=1,ny-1
     umix(1,j,k)=umix(2,j,k)
     umix(nx,j,k)=umix(nx-1,j,k)
  51    CONTINUE

   DO 52 k=2,nz-2
   DO 52 i=1,nx-1
     vmix(i,1,k)=vmix(i,2,k)
     vmix(i,ny,k)=vmix(i,ny-1,k)
   52    CONTINUE
!
!
!-----------------------------------------------------------------------
!
!  Set the mixing terms at the lateral boundaries equal to those
!  at the neighboring interior points.  This is done to essure
!  the boundary points get a similar amount of mixing as the interior
!  points. The boundary values will be used only in the case of
!  radiation boundary condition.
!
!-----------------------------------------------------------------------
    DO k=1,nz-1
      DO i=1,nx
        umix(i,1,k)=umix(i,2,k)
        umix(i,ny-1,k)=umix(i,ny-2,k)
      END DO
    END DO

    DO k=1,nz-1
      DO j=1,ny-1
        umix(1,j,k)=umix(2,j,k)
        umix(nx,j,k)=umix(nx-1,j,k)
      END DO
    END DO

    DO k=1,nz-1
      DO i=1,nx-1
        vmix(i,1,k)=vmix(i,2,k)
        vmix(i,ny,k)=vmix(i,ny-1,k)
      END DO
    END DO

    DO k=1,nz-1
      DO j=1,ny
        vmix(1,j,k)=vmix(2,j,k)
        vmix(nx-1,j,k)=vmix(nx-2,j,k)
      END DO
    END DO

    DO k=1,nz-1
      DO i=1,nx-1
        wmix(i,1,k)=wmix(i,2,k)
        wmix(i,ny-1,k)=wmix(i,ny-2,k)
      END DO
    END DO

    DO k=1,nz-1
      DO j=1,ny-1
        wmix(1,j,k)=wmix(2,j,k)
        wmix(nx-1,j,k)=wmix(nx-2,j,k)
      END DO
    END DO
  END IF

  RETURN
END SUBROUTINE tlmtmixuvw






!  Beginning coding the ADJSUBROUTINE

!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADTLMMIXUVW                ######
!######                                                      ######
!######                Copyright (c) 1995                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adtlmmixuvw(nx,ny,nz,nstepss,nbasic,                         &
           u,v,w,ptprt,pprt,qv,qscalar,tke,                      &
           ubar,vbar,ptbar,pbar,rhostr,qvbar,                           &
           usflx,vsflx, x,y,z,zp, j1,j2,j3,stabchk,                     &
           umix,vmix,wmix,km,lendel,defsq,                              &
           tem1,tem2,tem3,tem4,tem5,tem6,                               &
           tem7,tem8,tem9,tem10,tem11,tem12)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operations on TLMMIXUVW. TLMMIXUVW calculates
!  the total mixing (turbulent mixing and the externally
!  imposed computational mixing) for the momentum equations of TLM. This
!  subroutine also calculates the turbulent mixing coefficient ,km,
!  which will be used to calculate mixing terms for temperature and
!  water quantities. The mixing coefficient is based on Smagorinsky's
!  formulation. For the computational mixing, there are two options:
!  second order and fourth order mixing. The mixing applies only to
!  the perturbations and not the base state.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  11/12/1995
!
!  MODIFICATION HISTORY:
!  Jidong Gao
!  08/09/1996 Update to ARPS4.0.22
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nstepss  Index of total current basic state time levels.
!    nbasic   Index of the current basic state time level.
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates at a given time level (m/s)
!    ptprt    Perturbation potential temperature at a given time level (K)
!    pprt     Perturbation pressure at a given time level (Pascal)
!    qv       Water vapor specific humidity at a given time level (kg/kg)
!    qc       Cloud water mixing ratio at a given time level (kg/kg)
!    qr       Rainwater mixing ratio at a given time level (kg/kg)
!    qi       Cloud ice mixing ratio at a given time level (kg/kg)
!    qs       Snow mixing ratio at a given time level (kg/kg)
!    qh       Hail mixing ratio at a given time level (kg/kg)
!    tke      Turbulent Kinetic Energy ((m/s)**2)
!
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!
!    usflx    Surface flux of u-momentum
!    vsflx    Surface flux of v-momentum
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space (m)
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    umix     Total mixing in u equation (kg/(m*s)**2)
!    vmix     Total mixing in v equation (kg/(m*s)**2)
!    wmix     Total mixing in w equation (kg/(m*s)**2)
!    km       Turbulent mixing coefficient for momentum ( m**2/s ).
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3).
!    defsq    Deformation squared (1/s**2)
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!    tem4     Temporary work array.
!    tem5     Temporary work array.
!    tem6     Temporary work array.
!    tem7     Temporary work array.
!    tem8     Temporary work array.
!    tem9     Temporary work array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  !INCLUDE 'variable.inc'    ! Basic state variables.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
  INTEGER :: nstepss      ! Index of total current basic state time levels.
  INTEGER :: nbasic       ! Index of the current basic state time level.
!
  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)
  REAL :: ptprt (nx,ny,nz)     ! Perturbation potential temperature (K)
  REAL :: pprt  (nx,ny,nz)     ! Perturbation pressure (Pascal)
!
  REAL :: qv    (nx,ny,nz)     ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq)     ! Cloud water mixing ratio (kg/kg)
  REAL :: tke   (nx,ny,nz)     ! Turbulent Kinetic Energy ((m/s)**2)
!
  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific humidity
                               ! (kg/kg)

  REAL :: usflx (nx,ny)        ! surface flux of u-momentum (kg/(m*s**2))
  REAL :: vsflx (nx,ny)        ! surface flux of v-momentum (kg/(m*s**2))
  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.

                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.


  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! d( zp )/d( z ).

  REAL :: stabchk(nx,ny)

!
  REAL :: umix  (nx,ny,nz)     ! Turbulent mixing on u-momentum.
  REAL :: vmix  (nx,ny,nz)     ! Turbulent mixing on v-momentum.
  REAL :: wmix  (nx,ny,nz)     ! Turbulent mixing on w-momentum.

  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient for
                               ! momentum. ( m**2/s )

  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)

  REAL :: defsq (nx,ny,nz)     ! Deformation squared (1/s**2)
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
  REAL :: tem4  (nx,ny,nz)     ! Temporary work array
  REAL :: tem5  (nx,ny,nz)     ! Temporary work array
  REAL :: tem6  (nx,ny,nz)     ! Temporary work array
  REAL :: tem7  (nx,ny,nz)     ! Temporary work array
  REAL :: tem8  (nx,ny,nz)     ! Temporary work array
  REAL :: tem9  (nx,ny,nz)     ! Temporary work array
  REAL :: tem10  (nx,ny,nz)     ! Temporary work array
  REAL :: tem11  (nx,ny,nz)     ! Temporary work array
  REAL :: tem12  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: nxyz,i,j,k

!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

IF(1==0) THEN
  IF( raydmp /= 0) THEN
!
!-----------------------------------------------------------------------
!
!    Calculate the upper level Rayleigh damping on u, v and w perturbations.
!    The terms are accumulated in arrays umix, vmix and wmix.
!
!-----------------------------------------------------------------------
!
    CALL rdmpuvw0(nx,ny,nz,                                              &
                 u,v,w, ubar,vbar,rhostr, zp,                           &
                 umix,vmix,wmix, tem1)

  END IF

  IF( cmix4th == 1) THEN
!
!-----------------------------------------------------------------------
!
!  Calculate the fourth order computational mixing terms and add
!  them to the turbulent mixing terms umix, vmix, wmix.
!
!-----------------------------------------------------------------------
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem7(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
        END DO
      END DO
    END DO

    CALL cmix4uvw0(nx,ny,nz,                                             &
                  u,v,w, ubar,vbar,tem7,                                &
                  umix,vmix,wmix,                                       &
                  tem1,tem2,tem3,tem4,tem5,tem6)
  END IF
!
END IF ! (1==0)
!
!
  IF( cmix2nd == 1) THEN
!
!-----------------------------------------------------------------------
!
!  Calculate the second order computational mixing terms and add
!  them to the turbulent mixing terms umix, vmix, wmix.
!
!-----------------------------------------------------------------------
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tem7(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
        END DO
      END DO
    END DO
     tem8=0.0;tem9=0.0;tem10=0.0
    CALL adtlmcmix2uvw(nx,ny,nz,                                        &
                  u,v,w, ubar,vbar,tem7,                                &
                  umix,vmix,wmix,                                       &
                  tem1,tem2,tem3,tem4,tem5,tem6,tem8,tem9,tem10)

  END IF
!
!-----------------------------------------------------------------------
!
!  Control parameters for subgrid scalar turbulent mixing are passed
!  in globcst.inc, these include:
!
!  tmixopt: Tubulent mixing option
!           = 0, zero turbulent mixing.
!           = 1, constant mixing coefficient.
!           = 2, Smagorinsky mixing coefficient.
!           = 3, Smagorinsky + constant coefficient mixing.
!  tmixcst: Constant mixing coefficient. (Options 1 and 3)
!
!-----------------------------------------------------------------------
!
! IF( tmixopt == 0) THEN
!
!   nxyz = nx*ny*nz
!   CALL flzero(umix,nxyz) ! File umix with zero.
!   CALL flzero(vmix,nxyz)
!   CALL flzero(wmix,nxyz)
!
! ELSE
    CALL adtlmtmixuvw(nx,ny,nz,nstepss,nbasic,                          &
         u,v,w,ptprt,pprt,qv,qscalar,tke,                               &
         ubar,vbar,ptbar,pbar,rhostr,qvbar,                             &
         usflx,vsflx, x,y,z,zp, j1,j2,j3,stabchk,                       &
         umix,vmix,wmix,km, lendel,defsq,                               &
         tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,                  &
         tem10,tem11,tem12)

! END IF

  RETURN
END SUBROUTINE adtlmmixuvw
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADTLMTMIXUVW               ######
!######                                                      ######
!######                Copyright (c) 1995                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adtlmtmixuvw(nx,ny,nz,nstepss,nbasic,                        &
           u,v,w,ptprt,pprt,qv,qscalar,tke,                             &
           ubar,vbar,ptbar,pbar,rhostr,qvbar,                           &
           usflx,vsflx, x,y,z,zp, j1,j2,j3,stabchk,                     &
           umix,vmix,wmix,km,lendel,defsq,                              &
           nsqed,tau11,tau12,tau13,tau22,tau23,tau33,tem1,tem2,         &
           tem3,tem4,tem5)
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operations on TLMTMIXUVW. TLMTMIXUVW calculates
!  the turbulent mixing terms for the momentum equations of TLM.
!  These terms are expressed in the form of a stress tensor.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  11/11/1995
!
!  MODIFICATION HISTORY:
!     Yidi Liu (7/8/96) Update adjoint to 4.0
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nstepss  Index of total current basic state time levels.
!    nbasic   Index of the current basic state time level.
!
!    u        x component of velocity at a given time level (m/s)
!    v        y component of velocity at a given time level (m/s)
!    w        Vertical component of velocity in Cartesian
!             coordinates at a given time level (m/s)
!    ptprt    Perturbation potential temperature at a given time level (K)
!    pprt     Perturbation pressure at a given time level (Pascal)
!    qv       Water vapor specific humidity at a given time level (kg/kg)
!    qc       Cloud water mixing ratio at a given time level (kg/kg)
!    qr       Rainwater mixing ratio at a given time level (kg/kg)
!    qi       Cloud ice mixing ratio at a given time level (kg/kg)
!    qs       Snow mixing ratio at a given time level (kg/kg)
!    qh       Hail mixing ratio at a given time level (kg/kg)
!    tke      Turbulent Kinetic Energy ((m/s)**2)
!
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhostr   Base state density rhobar times j3 (kg/m**3)
!    qvbar    Base state water vapor specific humidity (kg/kg)
!
!    usflx    Surface flux of u-momentum
!    vsflx    Surface flux of v-momentum
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space (m)
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    umix     Total mixing in u equation (kg/(m*s)**2)
!    vmix     Total mixing in v equation (kg/(m*s)**2)
!    wmix     Total mixing in w equation (kg/(m*s)**2)
!  If tmixopt=1, the following are not outputs.
!    km       Turbulent mixing coefficient for momentum ( m**2/s ).
!    lendel   The length scale normalized by (dx*dy*dzp)**(1/3)
!    defsq    Deformation squared (1/s**2)
!
!  WORK ARRAYS:
!
!    real nsqed  Temporary array containing the Brunt Vaisala frquency
!    real tau11  Temporary work array
!    real tau12  Temporary work array
!    real tau13  Temporary work array
!    real tau22  Temporary work array
!    real tau23  Temporary work array
!    real tau33  Temporary work array
!    real tem1   Temporary work array
!    real tem2   Temporary work array
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    work arrays may be used for other purposes and therefore their
!    contents may be overwritten. Please examine the usage of work
!    arrays before you alter the code.)
!
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
  INCLUDE 'phycst.inc'
  !INCLUDE 'variable.inc'
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
  INTEGER :: tpast             ! Index of time level for the past time.
  INTEGER :: tpresent          ! Index of time level for the present
  INTEGER :: nstepss      ! Index of total current basic state time levels.
  INTEGER :: nbasic       ! Index of the current basic state time level.

  REAL :: u     (nx,ny,nz)     ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz)     ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz)     ! Total w-velocity (m/s)
  REAL :: ptprt (nx,ny,nz)     ! Perturbation potential temperature (K)
  REAL :: pprt  (nx,ny,nz)     ! Perturbation pressure (Pascal)
!
  REAL :: qv    (nx,ny,nz)     ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalarq)     ! Cloud water mixing ratio (kg/kg)
  REAL :: tke   (nx,ny,nz)     ! Turbulent Kinetic Energy ((m/s)**2)
!
  REAL :: ubar  (nx,ny,nz)     ! Base state u-velocity (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state v-velocity (m/s)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal).
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor specific humidity
                               ! (kg/kg)

  REAL :: usflx (nx,ny)        ! surface flux of u-momentum (kg/(m*s**2))
  REAL :: vsflx (nx,ny)        ! surface flux of v-momentum (kg/(m*s**2))
  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.


  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! d( zp )/d( z ).
  REAL :: stabchk(nx,ny)
!
  REAL :: umix  (nx,ny,nz)     ! Turbulent mixing on u-momentum.
  REAL :: vmix  (nx,ny,nz)     ! Turbulent mixing on v-momentum.
  REAL :: wmix  (nx,ny,nz)     ! Turbulent mixing on w-momentum.

  REAL :: km    (nx,ny,nz)     ! The turbulent mixing coefficient for
                               ! momentum. ( m**2/s )

  REAL :: lendel(nx,ny,nz)     ! The length scale normalized by
                               ! (dx*dy*dzp)**(1/3)

  REAL :: defsq (nx,ny,nz)     ! Deformation squared (1/s**2)
!
  REAL :: nsqed (nx,ny,nz)     ! Temporary work array
  REAL :: tau11 (nx,ny,nz)     ! Temporary work array
  REAL :: tau12 (nx,ny,nz)     ! Temporary work array
  REAL :: tau13 (nx,ny,nz)     ! Temporary work array
  REAL :: tau22 (nx,ny,nz)     ! Temporary work array
  REAL :: tau23 (nx,ny,nz)     ! Temporary work array
  REAL :: tau33 (nx,ny,nz)     ! Temporary work array
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
  REAL :: tem4  (nx,ny,nz)     ! Temporary work array
  REAL :: tem5  (nx,ny,nz)     ! Temporary work array
!
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
  REAL :: tema,temb,temc
  REAL :: zpup,zplow

!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Since The boundary values will be used only in the case of
!  radiation boundary condition, we may ignore them when non-
!  radiation boundary condition is used. The boundary condition
!  codes are developed but not connected to the rest.
!
  IF (1 == 0) THEN
    DO k=1,nz-1
      DO j=1,ny-1
        wmix(2,j,k)=wmix(1,j,k)
        wmix(nx-2,j,k)=wmix(nx-1,j,k)
      END DO
    END DO

    DO k=1,nz-1
      DO i=2,nx-2,nx-4
        wmix(i,2,k)=wmix(i,2,k)+wmix(i,1,k)
        wmix(i,ny-2,k)=wmix(i,ny-2,k)+wmix(i,ny-1,k)
      END DO
    END DO
    DO k=1,nz-1
      DO i=3,nx-3
        wmix(i,2,k)=wmix(i,1,k)
        wmix(i,ny-2,k)=wmix(i,ny-1,k)
      END DO
    END DO


    DO k=1,nz-1
      DO j=1,ny
        vmix(2,j,k)=vmix(1,j,k)
        vmix(nx-2,j,k)=vmix(nx-1,j,k)
      END DO
    END DO

    DO k=1,nz-1
      DO i=2,nx-2,nx-4
        vmix(i,ny-1,k)=vmix(i,ny-1,k)+vmix(i,ny,k)
        vmix(i,2,k)=vmix(i,2,k)+vmix(i,1,k)
      END DO
    END DO
    DO k=1,nz-1
      DO i=3,nx-3
        vmix(i,ny-1,k)=vmix(i,ny,k)
        vmix(i,2,k)=vmix(i,1,k)
      END DO
    END DO


    DO k=1,nz-1
      DO j=1,ny-1
        umix(2,j,k)=umix(1,j,k)
        umix(nx-1,j,k)=umix(nx,j,k)
      END DO
    END DO
    DO k=1,nz-1
      DO i=2,nx-1,nx-3
        umix(i,2,k)=umix(i,2,k)+umix(i,1,k)
        umix(i,ny-2,k)=umix(i,ny-2,k)+umix(i,ny-1,k)
      END DO
    END DO
    DO k=1,nz-1
      DO i=3,nx-2
        umix(i,2,k)=umix(i,1,k)
        umix(i,ny-2,k)=umix(i,ny-1,k)
      END DO
    END DO
  END IF ! ignore

!     wash up for taus

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tau11(i,j,k)=0.
        tau12(i,j,k)=0.
        tau13(i,j,k)=0.
        tau23(i,j,k)=0.
        tau22(i,j,k)=0.
        tau33(i,j,k)=0.
      END DO
    END DO
  END DO
!
!     wash up for tem4, tem5
!
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tem4(i,j,k)=0.
        tem5(i,j,k)=0.
      END DO
    END DO
  END DO
!
!
! CALL advmixtrm(nx,ny,nz, j1,j2,j3, tem4,tau22,tau23,                  &
  CALL advmixtrm(nx,ny,nz, j1,j2,j3,tau12,tau22,tau23,                  &
               vmix, tem1, tem2,tem3)
!
!
  CALL adumixtrm(nx,ny,nz, j1,j2,j3, tau11,tau12,tau13,                 &
               umix, tem1, tem2,tem3)
!
! DO k=2,nz-2
!   DO j=2,ny-1
!     DO i=2,nx-1
!       tau12(i,j,k)=tau12(i,j,k)+tem4(i,j,k)
!     END DO
!   END DO
! END DO
! DO k=2,nz-2
!   DO j=2,ny-1
!     tau12(1,j,k)=tem4(1,j,k)
!     tau12(nx,j,k)=tem4(nx,j,k)
!   END DO
! END DO
!-----------------------------------------------------------------------
!
!
!  Set the surface momentum fluxes:
!
!  If sfcphy = 1, the surface momentum fluxes are set to the values in
!  usflx and vsflx, which are calculated in the surface physics
!  subroutine sfcflx.
!
!  If sfcphy = 0, the surface momentum fluxes are calculated in the same way
!  as in the the domain interior (i.e., using a subgrid scale turbulence
!  parameterization).
!
!-----------------------------------------------------------------------
!
IF(1==0) THEN
  IF( sfcphy /= 0 ) THEN
    IF ( (sflxdis == 0) .OR. (sflxdis == 1 .AND.                        &
           (sfcphy == 1.OR.sfcphy == 3).AND.cdmlnd == 0.0) .OR.         &
           (sflxdis == 2) ) THEN
!
!-----------------------------------------------------------------------
!
!  tau13 at the surface = usflx.
!
!-----------------------------------------------------------------------
!
      DO j=2,ny-2
        DO i=2,nx-1
          tau13(i,j,2) = usflx(i,j)
        END DO
      END DO
!
!-----------------------------------------------------------------------
!
!  tau23 at the surface = vsflx.
!
!-----------------------------------------------------------------------
!
      DO j=2,ny-1
        DO i=2,nx-2
          tau23(i,j,2) = vsflx(i,j)
        END DO
      END DO

    ELSE IF (sflxdis == 1)  THEN

      DO j=2,ny-2
        DO i=2,nx-1
!         tema = 0.5*(stabchk(i,j)+stabchk(i-1,j))
	  temb = stabchk(i  ,j) - ( ptprt(i  ,j,2)+ptbar(i  ,j,2) )
	  temc = stabchk(i-1,j) - ( ptprt(i-1,j,2)+ptbar(i-1,j,2) )
	  tema = 0.5*(temb + temc)
          DO k=2,nz-1
            zpup =0.5*(zp(i,j,k)+zp(i-1,j,k))
            zplow=0.5*(zp(i,j,2)+zp(i-1,j,2))
            IF ( (tema <= 0.) .OR. ((zpup-zplow) > pbldpth0))  CYCLE

            tau13(i,j,k)=0.5*usflx(i,j)*                                &
                           (1.0-(zpup-zplow)/pbldpth0)**2

          END DO
        END DO
      END DO

      DO j=2,ny-1
        DO i=2,nx-2
!         tema = 0.5*(stabchk(i,j)+stabchk(i,j-1))
	  temb = stabchk(i  ,j) - ( ptprt(i  ,j,2)+ptbar(i  ,j,2) )
	  temc = stabchk(i,j-1) - ( ptprt(i,j-1,2)+ptbar(i,j-1,2) )
	  tema = 0.5*(temb + temc)
          DO k=2,nz-1
            zpup =0.5*(zp(i,j,k)+zp(i,j-1,k))
            zplow=0.5*(zp(i,j,2)+zp(i,j-1,2))
            IF ( (tema <= 0.) .OR. ((zpup-zplow) > pbldpth0))  CYCLE

            tau23(i,j,k)=0.5*vsflx(i,j)*                                &
                         (1.0-(zpup-zplow)/pbldpth0)**2

          END DO
        END DO
      END DO

    END IF
  END IF
END IF  ! 1==0
!-----------------------------------------------------------------------
!
!  Calculate the turbulent mixing coefficient, km, using
!  Smagorinsky's formulation
!  If tmixopt.eq.1, then turbulent mixing coefficient is assigned as constant
!  When tmixopt.eq.1, cftmix is linear!!! In this case there is no
!  need to calculate its adjoint. So it is placed here. Otherwise it
!  should placed after adstrsss.
!
!-----------------------------------------------------------------------
!
!     wash up for tem4
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tem4(i,j,k)=0.
        tem5(i,j,k)=0.
      END DO
    END DO
  END DO
!
  CALL adwmixtrm(nx,ny,nz, j1,j2,j3, tem4,tem5,tau33,                   &
               wmix, tem1, tem2,tem3)

  CALL cftmix0(nx,ny,nz,tau11,tau12,tau13,tau22,tau23,tau33,             &
              nsqed, zp,tke, km,lendel,defsq, tem1)

!
  CALL adstress(nx,ny,nz,j3,zp,                                         &
              km,rhostr,tau11,tau12,tau13,tau22,tau23,tau33,            &
              tem4,tem5,                                                &
              tem1,tem2,tem3)
!-----------------------------------------------------------------------
!
!  Adjoint on the deformation tensor components Dij, which are stored
!  in arrays Tauij. deform is linear.
!
!-----------------------------------------------------------------------

  CALL addeform(nx,ny,nz,u,v,w,j1,j2,j3,                                &
      tau11,tau12,tau13,tau22,tau23,tau33,tem1,tem2,tem3)

!-----------------------------------------------------------------------
!
!  Adjoint on calculating
!  the static stability parameter N squared (Brunt-Vaisala
!  frequency). The arrays tem1,tem2,tau11,tau12 are used as work
!  arrays for the following call.
!
!  If tmixopt.eq.1, stabnsq or nsqed is useless.
!-----------------------------------------------------------------------
!
IF(1==0) THEN
  IF (tmixopt /= 1) THEN
    CALL stabnsq0(nx,ny,nz,                                              &
                 ptprt,pprt,qv,qscalar,ptbar,qvbar,pbar,j3,             &
             nsqed,tem1,tem2,tau11,tau12)
  END IF
!
END IF
!
  RETURN
END SUBROUTINE adtlmtmixuvw

!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADUMIXTRM                  ######
!######                                                      ######
!######                Copyright (c) 1995                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adumixtrm(nx,ny,nz,j1,j2,j3,tau11,tau12,tau13,               &
           umix,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operations on UMIXTRM. UMIXTRM calculates
!  the turbulent mixing term in u equation as the
!  divergence of the turbluent momentum fluxes.
!
!  j3 * DIV U = difx0(j3 * tau11) + dify0(avgx0(avgsv0(j3)) * tau12)
!               + difz0 (tau13 + (j1 * avgz0(avgx0(tau11))) + (avgy0
!               ((avgx0(j2)) * (avgz0(tau12))))
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  11/07/1995
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!    tau11    Deformation tensor component (1/s)
!    tau12    Deformation tensor component (1/s)
!    tau13    Deformation tensor component (1/s)
!
!
!  OUTPUT:
!
!    umix     The divergence of u mixing term
!
!  WORKING ARRAYS:
!
!    tem1     Temporary working array.
!    tem2     Temporary working array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    working arrays may be used for other purposes therefore their
!    contents overwritten. Please examine the usage of working arrays
!    before you alter the code.)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! - d(zp)/dx
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! - d(zp)/dy
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               !   d(zp)/dz
!
  REAL :: tau11 (nx,ny,nz)     ! Deformation stress component.
  REAL :: tau12 (nx,ny,nz)     ! Deformation stress component.
  REAL :: tau13 (nx,ny,nz)     ! Deformation stress component.
!
  REAL :: umix  (nx,ny,nz)     ! Divergence of u mixing term
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary working array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary working array.
  REAL :: tem3  (nx,ny,nz)     ! Temporary working array.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global constants that control model
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!-------------------------------------------------------------------
!
!  Adjoint on conbining the second term tem1 and umix to
!  get the final umix divergence.
!
!-------------------------------------------------------------------
  tem1 = 0.
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        tem1(i,j,k)=umix(i,j,k)
      END DO
    END DO
  END DO
!-----------------------------------------------------------------------
!
!  Adjoint on calculating the second term of the u mixing divergence term.
!  dify0(avgx0(avgsv0(j3)) * Tau12).
!
!-----------------------------------------------------------------------
!  The k range is changed to eliminate the redundancy in forward code.
  tem3 = 0.
  onvf = 0
  CALL addify0(tem3, onvf,                                               &
            nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1, dy, tem1)
!
  tem1 = 0.
  CALL avgsv0(j3,nx,ny,nz, 1,nx-1, 1,nz-1, tem1)
  tem2 = 0.
  onvf = 1
  CALL avgx0(tem1,onvf,                                                  &
             nx,ny,nz, 2,nx-1, 1,ny, 1,nz-1, tem2)

!  The k range is changed to eliminate the redundancy in forward code.
  tem1 = 0.
  CALL adaamult0(tem2,tem1,                                             &
              nx,ny,nz, 2,nx-1, 1,ny, 1,nz-1, tem3)
  tau12=tau12+tem1
!-----------------------------------------------------------------------
!
!  Adjoint on combining the first and third terms of u
!  mixing Divergence (tem2 + umix)
!
!-----------------------------------------------------------------------
  tem2 = 0.
  DO k=2,nz-2
    DO j=1,ny-1
      DO i=2,nx-1
        tem2(i,j,k)=umix(i,j,k)
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Adjoint on calculating the first term of the u mixing divergence term.
!  difx0(j3 * Tau11)
!
!-----------------------------------------------------------------------
  tem1 = 0.
  onvf = 1
!  The k range is changed to eliminate the redundancy in forward code.
  CALL addifx0(tem1, onvf,                                               &
            nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1, dx, tem2)
!
!  The k range is changed to eliminate the redundancy in forward code.
  tem3 = 0.
  CALL adaamult0(j3, tem3,                                               &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem1)
  tau11 = tau11 + tem3
!-----------------------------------------------------------------------
  tem1 = 0.
  onvf = 0
  CALL addifz0(tem1, onvf,                                               &
            nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-2, dz, umix)
  umix= 0.
!
  IF( ternopt /= 0) THEN
!
!-------------------------------------------------------------------
!
!  Combine the three terms of the third Umix divergence term.
!
!-------------------------------------------------------------------
    tem3 = 0.
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=2,nx-1
!          tem1(i,j,k)=tau13(i,j,k) + tem2(i,j,k) + umix(i,j,k)
           tau13(i,j,k) = tau13(i,j,k)+tem1(i,j,k)
            tem3(i,j,k) = tem1(i,j,k)
            umix(i,j,k) = tem1(i,j,k)
        END DO
      END DO
    END DO
!
IF(1==0) THEN
!
!  At this point, UMIX =  avgy0 (avgx0(j2) * avgz0 (tau12)).
!
!-----------------------------------------------------------------------
!
!  Calculate the second part of the third term.
!  j1 * (avgz0 (avgx0(tau11))). This term is zero when ternopt=0.
!
!-----------------------------------------------------------------------
    tem2 = 0.
    CALL adaamult0(tem2,j1,                                                &
                nx,ny,nz,2,nx-1, 1,ny-1, 2,nz-1,tem3)
!
    tem1 = 0.
    onvf = 1
    CALL adavgz0(tem1,onvf,                                                &
              nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1, tem2)
    tem3 = 0.
    onvf = 1
    CALL adavgx0(tem3 ,onvf,                                               &
              nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1, tem1)
    tau11=tau11+tem3
!-----------------------------------------------------------------------
!
!  Calculate the third part of the third term first.
!  avgy0 (avgx0(j2) * avgz0 (tau12)). This term is zero when ternopt=0.
!
!-----------------------------------------------------------------------
    tem1= 0.
    onvf = 1
    CALL avgx0(j2,onvf,                                                  &
              nx,ny,nz, 2,nx-1, 1,ny, 1,nz, tem1)

    tem3 = 0.
    onvf = 0
    CALL adavgy0(tem3,onvf,                                                &
              nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1, umix)

    tem2 = 0.
    CALL aamult0(tem1,tem2,                                              &
                nx,ny,nz, 2,nx-1, 1,ny, 2,nz-1, tem3)

    tem3 = 0.
    onvf = 1
    CALL adavgz0(tem3 ,onvf,                                               &
              nx,ny,nz, 1,nx, 1,ny, 2,nz-1, tem2)
    tau12=tau12 +tem3
END IF
  ELSE   ! When ternopt=0, tem2=umix=0.0

    DO k=2,nz-1
      DO j=1,ny-1
        DO i=2,nx-1
          tau13(i,j,k)=tau13(i,j,k)+tem1(i,j,k)
        END DO
      END DO
    END DO
  END IF
  RETURN
END SUBROUTINE adumixtrm

!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADVMIXTRM                  ######
!######                                                      ######
!######                Copyright (c) 1995                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE advmixtrm(nx,ny,nz,j1,j2,j3,tau12,tau22,tau23,               &
           vmix,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the turbulent mixing term in v equation as the
!  divergence of the turbluent momentum fluxes.
!
!  j3 * DIV V = difx0(avgy0(avgsu0(j3)) * tau12) + dify0(j3 * tau22)
!               + difz0 (tau23 + avgx0(avgy0(j1) * avgz0 (tau12)) +
!               (j2 * (avgz0 (avgy0(tau22)))))
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  11/07/94
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!    tau12    Deformation tensor component (1/s)
!    tau22    Deformation tensor component (1/s)
!    tau23    Deformation tensor component (1/s)
!
!
!  OUTPUT:
!
!    vmix     The divergence of v mixing term
!
!  WORKING ARRAYS:
!
!    tem1     Temporary working array.
!    tem2     Temporary working array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    working arrays may be used for other purposes therefore their
!    contents overwritten. Please examine the usage of working arrays
!    before you alter the code.)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! - d(zp)/dx
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! - d(zp)/dy
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               !   d(zp)/dz
!
  REAL :: tau12 (nx,ny,nz)     ! Deformation stress component.
  REAL :: tau22 (nx,ny,nz)     ! Deformation stress component.
  REAL :: tau23 (nx,ny,nz)     ! Deformation stress component.
!
  REAL :: vmix  (nx,ny,nz)     ! Divergence of v mixing term
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary working array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary working array.
  REAL :: tem3  (nx,ny,nz)     ! Temporary working array.     [

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global constants that control model
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!-------------------------------------------------------------------
!
!  Adjoint operation on combining
!  the second term TEM2 and VMIX to get the final v
!  mixing divergence
!
!-------------------------------------------------------------------
  tem2 = 0.0
  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        tem2(i,j,k)=vmix(i,j,k)
      END DO
    END DO
  END DO
!-----------------------------------------------------------------------
!
!  Adjoint operation on calculating
!  the second term of the V mixing divergence term.
!  dify0(j3 * Tau22)
!
!-----------------------------------------------------------------------
!  The k range is changed to eliminate the redundancy in forward code.
  tem1 = 0.
  onvf = 1
  CALL addify0(tem1, onvf,                                               &
            nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1, dy, tem2)

!  The k range is changed to eliminate the redundancy in forward code.

  CALL adaamult0(j3,tau22,                                               &
              nx,ny,nz,1,nx-1,1,ny-1,1,nz-1,tem1)
  tem1 = 0.
!-----------------------------------------------------------------------
!
!  Adjoint on combining  the first and third terms of vmix (tem1 + vmix)
!
!-----------------------------------------------------------------------
  tem1 = 0.0
  DO k=2,nz-2
    DO j=2,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=vmix(i,j,k)
      END DO
    END DO
  END DO
!-----------------------------------------------------------------------
!
!  Adjoint operation on calculating
!  the first term of the V mixing divergence term.
!  difx0(avgy0(avgsu0(j3)) * Tau12)
!
!-----------------------------------------------------------------------
  onvf = 0
!  The k range is changed to eliminate the redundancy in forward code.
  tem3 = 0.0
  CALL addifx0(tem3, onvf,                                               &
            nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1, dx, tem1)

  tem1 = 0.
  CALL avgsu0(j3,nx,ny,nz, 1,ny-1, 1,nz-1, tem1)
  tem2 = 0.
  onvf = 1
  CALL avgy0(tem1,onvf,                                                  &
            nx,ny,nz, 1,nx, 2,ny-1, 1,nz-1, tem2)

  tem1 = 0.
  CALL adaamult0(tem2, tem1,                                             &
              nx,ny,nz, 1,nx, 2,ny-1, 1,nz-1, tem3)
  tau12=tau12+tem1
!-----------------------------------------------------------------------
  tem1 = 0.
  onvf = 0
  CALL addifz0(tem1, onvf,                                               &
            nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-2, dz, vmix)
  vmix = 0.
!-----------------------------------------------------------------------

  IF( ternopt /= 0) THEN

!-----------------------------------------------------------------------
!
!  Combine the three terms of the third Vmix divergence term
!
!-----------------------------------------------------------------------
    tem2 = 0.
    DO k=2,nz-1
      DO j=2,ny-1
        DO i=1,nx-1
!          tem1(i,j,k)=tau23(i,j,k) + tem2(i,j,k) + vmix(i,j,k)
          tau23(i,j,k) = tau23(i,j,k)+ tem1(i,j,k)
           tem2(i,j,k) = tem1(i,j,k)
           vmix(i,j,k) = tem1(i,j,k)
        END DO
      END DO
    END DO
!
!  At this point, VMIX = j2 * avgz0(avgy0(tau22))
!
IF(1==0) THEN
!-----------------------------------------------------------------------
!
!  Calculate the second part of the third term.
!  avgx0(avgy0(j1) * avgz0(Tau12)). This term is zero when ternopt=0.
!
!-----------------------------------------------------------------------
    tem1 = 0.
    onvf = 1
    CALL avgy0(j1,onvf,                                                  &
              nx,ny,nz, 1,nx, 2,ny-1, 1,nz, tem1)

    tem3 = 0.
    onvf = 0
    CALL adavgx0(tem3,onvf,                                                &
              nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, tem2)
    tem2 = 0.
    CALL adaamult0(tem1,tem2,                                              &
                nx,ny,nz, 1,nx, 2,ny-1, 2,nz-1, tem3)

    tem3 = 0.
    onvf = 1
    CALL adavgz0(tem3 ,onvf,                                               &
              nx,ny,nz, 1,nx, 1,ny, 2,nz-1, tem2)
    tau12=tau12+ tem3
!-----------------------------------------------------------------------
!
!  Calculate the third part of the third term first.
!  j2 * avgz0(avgy0(tau22)). This term is zero when ternopt=0.
!
!-----------------------------------------------------------------------
    tem2 = 0.0
    CALL adaamult0(j2,tem2,                                               &
                nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, vmix)

    tem1 = 0.
    onvf = 1
    CALL adavgz0(tem1,onvf,                                                 &
              nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1, tem2)

    tem3 = 0.
    onvf = 1
    CALL adavgy0(tem3 ,onvf,                                                &
              nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1, tem1)
    tau22=tau22+tem3
END IF
  ELSE    ! When ternopt=0, tem2=vmix=0.0

    DO k=2,nz-1
      DO j=2,ny-1
        DO i=1,nx-1
          tau23(i,j,k)=tau23(i,j,k)+tem1(i,j,k)
        END DO
      END DO
    END DO

  END IF

  RETURN
END SUBROUTINE advmixtrm
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADWMIXTRM                  ######
!######                                                      ######
!######                Copyright (c) 1995                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adwmixtrm(nx,ny,nz,j1,j2,j3,tau13,tau23,tau33,               &
           wmix,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operations on WMIXTRM. WMIXTRM calculates
!  the turbulent mixing term in w equation as the
!  divergence of the turbluent momentum fluxes.
!
!  j3 * DIV W = difx0(avgz0(avgsu0(j3)) * tau13) + dify0(avgz0(avgsv0(j3))
!               * tau23) + difz0(tau33 + avgx0(avgz0(j1) * avgz0(tau13))
!               + (avgy0(avgz0(j2) * avgz0(tau23))))
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  11/07/94
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!    tau13    Deformation tensor component (1/s)
!    tau23    Deformation tensor component (1/s)
!    tau33    Deformation tensor component (1/s)
!
!
!  OUTPUT:
!
!    wmix     The divergence of w mixing term
!
!  WORKING ARRAYS:
!
!    tem1     Temporary working array.
!    tem2     Temporary working array.
!
!   (These arrays are defined and used locally (i.e. inside this
!    subroutine), they may also be passed into routines called by
!    this one. Exiting the call to this subroutine, these temporary
!    working arrays may be used for other purposes therefore their
!    contents overwritten. Please examine the usage of working arrays
!    before you alter the code.)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! - d(zp)/dx
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! - d(zp)/dy
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               !   d(zp)/dz
!
  REAL :: tau13 (nx,ny,nz)     ! Deformation stress component.
  REAL :: tau23 (nx,ny,nz)     ! Deformation stress component.
  REAL :: tau33 (nx,ny,nz)     ! Deformation stress component.
!
  REAL :: wmix  (nx,ny,nz)     ! Divergence of w mixing term
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary working array.
  REAL :: tem2  (nx,ny,nz)     ! Temporary working array.
  REAL :: tem3  (nx,ny,nz)     ! Temporary working array.

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'     ! Global constants that control model
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!-----------------------------------------------------------------------
!
!  Routines called:
!
!-----------------------------------------------------------------------
!
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!-----------------------------------------------------------------------
!
!  Combine the second term tem1 and wmix to get the final w
!  mixing divergence.
!
!-----------------------------------------------------------------------
  tem1 = 0.
  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=wmix(i,j,k)
      END DO
    END DO
  END DO

!-----------------------------------------------------------------------
!
!  Calculate the second term of the W mixing divergence term.
!  dify0(avgz0(avgsv0(j3)) * Tau23)
!
!-----------------------------------------------------------------------
  tem3 = 0.
  onvf = 0
  CALL addify0(tem3, onvf,                                               &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, dy, tem1)

  tem1 = 0.
  CALL avgsv0(j3,nx,ny,nz,1,nx-1,1,nz-1,tem1)
  tem2 = 0.
  onvf = 1
  CALL avgz0(tem1,onvf,                                                  &
            nx,ny,nz, 1,nx-1, 1,ny, 2,nz-1, tem2)

  tem1 = 0.0
  CALL adaamult0(tem2,tem1 ,                                             &
            nx,ny,nz, 1,nx-1, 1,ny, 2,nz-1, tem3)
  tau23 =tau23+ tem1
!-----------------------------------------------------------------------
!
!  Combine the first and third terms of W mixing Divergence (tem1 + vmix)
!
!-----------------------------------------------------------------------

  DO k=2,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        tem1(i,j,k)=wmix(i,j,k)
      END DO
    END DO
  END DO
!-----------------------------------------------------------------------
!
!  Calculate the first term of the W mixing divergence term.
!  difx (avgz0(avgsu0(j3)) * Tau13)
!
!-----------------------------------------------------------------------

  tem3 = 0.
  onvf = 0
  CALL addifx0(tem3, onvf,                                               &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, dx, tem1)

  tem1 = 0.
  CALL avgsu0(j3,nx,ny,nz,1,ny-1,1,nz-1,tem1)
  tem2 = 0.
  onvf = 1
  CALL avgz0(tem1,onvf,                                                  &
            nx,ny,nz, 1,nx, 1,ny-1, 2,nz-1, tem2)
  tem1 = 0.
  CALL adaamult0(tem2, tem1,                                             &
              nx,ny,nz, 1,nx, 1,ny-1, 2,nz-1, tem3)
  tau13 =tau13 +tem1
!-----------------------------------------------------------------------
  tem1 = 0.
  onvf = 1
  CALL addifz0(tem1, onvf,                                               &
            nx,ny,nz, 1,nx-1, 1,ny-1, 2,nz-1, dz, wmix)
  wmix = 0.
!-----------------------------------------------------------------------
!
!  NOTE: In order to reduce memory usage, order dependance has been
!        introduced in the calculation of the third term in the
!        w mixing divergence term, which reduces the possible term-
!        wise parallelism.  This is due to the constraint that only two
!        temporary arrays being avaliable for passage into this subroutine.
!
!
!-----------------------------------------------------------------------
!

  IF( ternopt /= 0 ) THEN

!-----------------------------------------------------------------------
!
!  Combine the three terms of the third w mix divergence term
!
!-----------------------------------------------------------------------
    tem2 = 0.
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
!         tem1(i,j,k)=tau33(i,j,k) + tem2(i,j,k) + wmix(i,j,k)
          wmix(i,j,k) = tem1(i,j,k)
          tem2(i,j,k) = tem1(i,j,k)
         tau33(i,j,k) = tau33(i,j,k)+ tem1(i,j,k)
        END DO
      END DO
    END DO
!
!  At this point, wmix = avgy0(avgz0(j2) * avgz0(tau23)).
!
!-----------------------------------------------------------------------
!
!  Calculate the second part of the third term.
!  avgx0(avgz0(j1) * avgz0(tau13)). This term is zero when ternopt=0.
!
!-----------------------------------------------------------------------
IF(1==0) THEN
    tem1 = 0.
    onvf = 0
    CALL avgz0(j1,onvf,                                                  &
              nx,ny,nz, 1,nx, 1,ny-1, 1,nz-1, tem1)

    tem3 = 0.
    onvf = 0
    CALL adavgx0(tem3,onvf,                                              &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, tem2)

    tem2 = 0.
    CALL adaamult0(tem1,tem2,                                            &
                nx,ny,nz, 1,nx, 1,ny-1, 1,nz-1, tem3)

    tem3  = 0.
    onvf = 0
    CALL adavgz0(tem3 ,onvf,                                             &
              nx,ny,nz, 1,nx, 1,ny-1, 1,nz-1, tem2)
    tau13 = tau13 + tem3
!-----------------------------------------------------------------------
!
!  Calculate the third part of the third term first.
!  avgy0(avgz0(j2) * avgz0(tau23)). This term is zero when ternopt=0.
!
!-----------------------------------------------------------------------
    tem1 = 0.
    onvf = 0
    CALL avgz0(j2,onvf,                                                  &
              nx,ny,nz, 1,nx-1, 1,ny, 1,nz-1, tem1)

    tem3 = 0.
    onvf = 0
    CALL adavgy0(tem3,onvf,                                                &
              nx,ny,nz, 1,nx-1, 1,ny-1, 1,nz-1, wmix)

    tem2 = 0.
    CALL adaamult0(tem1,tem2,                                              &
                nx,ny,nz, 1,nx-1, 1,ny, 1,nz-1, tem3)

    tem1 = 0.
    onvf = 0
    CALL adavgz0(tem1,onvf,                                               &
              nx,ny,nz, 1,nx-1, 1,ny, 1,nz-1, tem2)
    tau23= tau23 +tem1
END IF
  ELSE    ! When ternopt=0, term2=wmix=0.0

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tau33(i,j,k)=tau33(i,j,k)+ tem1(i,j,k)
        END DO
      END DO
    END DO

  END IF

  RETURN
END SUBROUTINE adwmixtrm
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE SMIXTRM                    ######
!######                                                      ######
!######                Copyright (c) 1995                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adsmixtrm(nx,ny,nz,h1,h2,h3,rhostr,x,y,z,zp,                 &
           j1,j2,j3,smix,                                               &
           tem1,tem2,tem3)
!
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operations on SMIXTRM.
!  SMIXTRM calculates the turbulent mixing term for a scalar from the
!  turbulent fluxes h1, h2 and h3.
!
!  smix = difx0(avgx0(j3) * h1) + dify0(avgy0(j3) * h2) +
!         difz0(h3 + avgx0(avgz0(h1) * j1) + avgy0(avgz0(h2) * j2))
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  07/10/1995
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    h1       Turbulent heat flux, a local array.
!    h2       Turbulent heat flux, a local array.
!    h3       Turbulent heat flux, a local array.
!
!    rhostr   Base state air density times j3 (kg/m**3)
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space (m)
!
!    j1       Coordinate transform Jacobian -d(zp)/dx
!    j2       Coordinate transform Jacobian -d(zp)/dy
!    j3       Coordinate transform Jacobian  d(zp)/dz
!
!  OUTPUT:
!
!    smix     The mixing term for a scalar calculated from its turbulent fluxes
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array
!    tem2     Temporary work array
!    tem3     Temporary work array
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
!
  REAL :: h1    (nx,ny,nz)     ! Turbulent flux in x direction
  REAL :: h2    (nx,ny,nz)     ! Turbulent flux in y direction
  REAL :: h3    (nx,ny,nz)     ! Turbulent flux in z direction

  REAL :: rhostr(nx,ny,nz)     ! Base state air density times j3 (kg/m**3)
                               ! momentum. ( m**2/s )

  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.

  REAL :: j1    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transform Jacobian defined as
                               ! d( zp )/d( z ).
!
  REAL :: smix  (nx,ny,nz)     ! Turbulent mixing for a scalar
!
  REAL :: tem1  (nx,ny,nz)     ! Temporary work array
  REAL :: tem2  (nx,ny,nz)     ! Temporary work array
  REAL :: tem3  (nx,ny,nz)     ! Temporary work array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'             !Added by Ying 10/15/2001
!
!-----------------------------------------------------------------------


!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Routines called:
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!cccccccccccc Adjoint code begins here ccccccccccccccccccccccccccccccccc
!
!-----------------------------------------------------------------------
!
!  Adjoint on combining TEM3 + SMIX to give the final SMIX.
!
!-----------------------------------------------------------------------
!
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        tem1(i,j,k)=0.
        tem2(i,j,k)=0.
        tem3(i,j,k)=0.
      END DO
    END DO
  END DO
  DO k=2,nz-2
    DO j=2,ny-2
      DO i=2,nx-2
        tem3(i,j,k)=smix(i,j,k)
        smix(i,j,k)=smix(i,j,k)
      END DO
    END DO
  END DO
!
  onvf = 0
!
!  Inputs: tem2(2--nx-2,2--ny-2,2--nz-1)
!  Outtput: tem2(2--nx-2,2--ny-2,2--nz-2)
!
  CALL addifz0(tem2,onvf,                                                &
            nx,ny,nz, 2,nx-2, 2,ny-2, 2,nz-2,dz,tem3)

!-----------------------------------------------------------------------
!
!  Adjoint calculating the third term in the smix relation.
!  difz0(h3 + avgx0(avgz0(h1) * j1) + avgy0(avgz0(h2) * j2))
!
!-----------------------------------------------------------------------


  IF( ternopt /= 0 ) THEN

!-----------------------------------------------------------------------
!
!  Calculate the third part of the third term.
!  avgy0(avgz0(h2) * j2))
!
!-----------------------------------------------------------------------

    onvf = 1
    CALL avgz0(h2,onvf,                                                  &
              nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1,tem1)

    CALL aamult0(tem1,j2,                                                &
                nx,ny,nz, 1,nx-1, 2,ny-1, 2,nz-1,tem1)

    onvf = 0
    CALL avgy0(tem1,onvf,                                                &
              nx,ny,nz, 1,nx-1, 2,ny-2, 2,nz-1,tem3)

!-----------------------------------------------------------------------
!
!  Calculate the second part of the third term....
!  avgx0(avgz0(h1) * j1)
!
!-----------------------------------------------------------------------

    onvf = 1
    CALL avgz0(h1,onvf,                                                  &
              nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1,tem1)

    CALL aamult0(tem1,j1,                                                &
                nx,ny,nz, 2,nx-1, 1,ny-1, 2,nz-1,tem1)

    onvf = 0
    CALL avgx0(tem1,onvf,                                                &
              nx,ny,nz, 2,nx-2, 1,ny-1, 2,nz-1,tem2)
!
!-----------------------------------------------------------------------
!
!  Combine tem2 + tem3 + H3 in the third term.
!
!-----------------------------------------------------------------------
!

    DO k=2,nz-1
      DO j=2,ny-2
        DO i=2,nx-2
          tem2(i,j,k)=tem2(i,j,k) + tem3(i,j,k) + h3(i,j,k)
        END DO
      END DO
    END DO

  ELSE  ! No terrain, tem2=tem3=0.0

    DO k=2,nz-1
      DO j=2,ny-2
        DO i=2,nx-2
          h3(i,j,k)=tem2(i,j,k)
        END DO
      END DO
    END DO

  END IF
!-----------------------------------------------------------------------
!
!  Adjoint calculating the second term in the smix relation.
!  dify0(avgy0(j3) * h2)
!
!-----------------------------------------------------------------------

  DO k=1,nz-1
    DO j=2,ny-2
      DO i=2,nx-2
        tem2(i,j,k)=smix(i,j,k)
        smix(i,j,k)=smix(i,j,k)
      END DO
    END DO
  END DO

  onvf = 0
  CALL addify0(tem1,onvf,                                                &
            nx,ny,nz, 1,nx-1, 2,ny-2, 1,nz-1,dy,tem2)

  onvf = 1
  CALL avgy0(j3,onvf,                                                    &
            nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1,tem3)

  CALL adaamult0(tem3,h2,                                                &
              nx,ny,nz, 1,nx-1, 2,ny-1, 1,nz-1,tem1)

!-----------------------------------------------------------------------
!
!  Adjoint calculating term difx0(avgx0(j3) * h1)
!
!-----------------------------------------------------------------------

  onvf = 0
  CALL addifx0(tem1,onvf,                                                &
            nx,ny,nz, 2,nx-2, 1,ny-1, 1,nz-1,dx,smix)

  onvf = 1
  CALL avgx0(j3,onvf,                                                    &
            nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1,tem3)

  CALL adaamult0(tem3,h1,                                                &
              nx,ny,nz, 2,nx-1, 1,ny-1, 1,nz-1,tem1)

  RETURN
END SUBROUTINE adsmixtrm

