PROGRAM news3dvar
!
!##################################################################
!##################################################################
!######                                                      ######
!######                   PROGRAM news3dvar                  ######
!######            3DVAR Variational Data Analysis System    ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
!  PURPOSE:
!
!  This program is a driver for original arps data analysis system
!  (ADAS) and three-dimensional variational data assimilation
!  (3DVAR) on the ARPS grid.
!
!  To run news3dvar
!    build news3dvar
!    bin/news3dvar < news3dvar.input
!
!  The driver establishes sizing of variables
!  and adjustable parameters.
!  It calls a routine to set up the grid, calls the data readers,
!  a routine to analyse the data and another to write it out.
!
!-----------------------------------------------------------------------
!  AUTHOR:
!
!  FOR 3DVAR:
!  06/10/2000 (Jidong Gao)
!            Basic version, shared with ADAS input & output
!
!  MODIFICATION HISTORY:
!
!  03/09/2017 (Yunheng Wang)
!  Separated Completely from the ARPS package.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  !USE module_varpara
  USE Vmodule_3dvar
  USE module_modelArray
  USE module_arpsArray
  USE module_analysisIAU
  USE module_analysisArrays
  USE module_ensemble
  USE arps_dtaread
  USE wrfio_api
  USE runtime_accts

  USE constraint_diagnostic_div , only : consdiv_deallocate_wrk, hBal_opt, read_tendency
  USE constraint_mslp           , only : consmslp_deallocate_wrk, mslp_opt
  USE constraint_thermo         , only : consthermo_deallocate_wrk
  USE constraint_smooth         , only : consmth_deallocate_wrk
  USE constraint_divergence     , only : use_arps_metrics

  USE module_radarobs
  USE module_convobs
  USE module_cwpobs, ONLY : pseudo_opt, satcwpobs, ncwpfil
  USE module_tpwobs, ONLY : ntpwfil
  USE module_lightning
  USE module_aeri  ! JJ
  USE module_pseudoQobs

  IMPLICIT NONE
  INCLUDE 'alloc.inc'
  INCLUDE 'adas.inc'
  INCLUDE 'nudging.inc'
  INCLUDE 'exbc.inc'
  INCLUDE 'radcst.inc'
  INCLUDE 'mp.inc'     ! Message passing parameters
  !INCLUDE 'varpara.inc'

  REAL :: cputimearray(10)
  COMMON /cputime3dvar/ cputimearray
!
!-----------------------------------------------------------------------
!
!  Analysis scratch space
!
!-----------------------------------------------------------------------
!
  INTEGER, ALLOCATABLE :: knt(:,:)
  !REAL,    ALLOCATABLE :: rngsqi(:)
  REAL,    ALLOCATABLE :: wgtsum(:,:)
  REAL,    ALLOCATABLE :: zsum(:,:)
  REAL,    ALLOCATABLE :: sqsum(:,:)
!
!-----------------------------------------------------------------------
!
!  Additional grid variables
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: xs(:), xslg(:), xlg(:)
  REAL, ALLOCATABLE :: ys(:), yslg(:), ylg(:)
  REAL, ALLOCATABLE :: zs(:,:,:)
  REAL, ALLOCATABLE :: latgr(:,:), longr(:,:)
  REAL, ALLOCATABLE :: rhostr(:,:,:)
!
!-----------------------------------------------------------------------
!
!  Temporary arrays.  Some are only used for MPI
!
!-----------------------------------------------------------------------
!
  REAL,    ALLOCATABLE :: tem1  (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem2  (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem3  (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem4  (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem5  (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem6  (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem7  (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem8  (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem9  (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem10 (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem11 (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem12 (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem13 (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem14 (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem15 (:,:,:)     ! Temporary work array.
  REAL,    ALLOCATABLE :: tem4d (:,:,:,:)   ! Temporary work array.

!
! These arrays are continually cleaned up, so they get recycled frequently
! between different data types.
!

  INTEGER, ALLOCATABLE :: item1(:)          ! Temporary work array.

  REAL, ALLOCATABLE :: tem1soil (:,:,:)     ! Temporary work array.
  REAL, ALLOCATABLE :: tem2soil (:,:,:)     ! Temporary work array.
  REAL, ALLOCATABLE :: tem3soil (:,:,:)     ! Temporary work array.
  REAL, ALLOCATABLE :: tem4soil (:,:,:)     ! Temporary work array.
  REAL, ALLOCATABLE :: tem5soil (:,:,:)     ! Temporary work array.
!
!-----------------------------------------------------------------------
!
!  Cloud analysis variables other than model's ones:
!
!-----------------------------------------------------------------------
!
  REAL,    ALLOCATABLE :: ref_mos_3d (:,:,:)
                     ! 3D remapped radar reflectivity field as input
                     ! may be modified by sate. VIS as output
  REAL,    ALLOCATABLE :: ref_comp_2d(:,:)
  REAL,    ALLOCATABLE :: rhv_mos_3d (:,:,:)
  REAL,    ALLOCATABLE :: zdr_mos_3d (:,:,:)
  REAL,    ALLOCATABLE :: kdp_mos_3d (:,:,:)
  REAL,    ALLOCATABLE :: cloud_ceiling(:,:)    ! cloud ceiling (m)
  REAL,    ALLOCATABLE :: vil(:,:)              ! vertically integrated liquid
  REAL,    ALLOCATABLE :: w_cld (:,:,:)         ! vertical velocity (m/s) in clouds
  REAL,    ALLOCATABLE :: qscalar_cld(:,:,:,:)
  INTEGER, ALLOCATABLE :: cldpcp_type_3d(:,:,:) ! cloud/precip type field

  INTEGER, ALLOCATABLE :: icing_index_3d(:,:,:) ! icing severity index
  LOGICAL, ALLOCATABLE :: l_mask_pcptype(:,:)   ! analyze precip type
!
!-----------------------------------------------------------------------
!
!  Analysis variables
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: anx(:,:,:,:)
!
!-----------------------------------------------------------------------
!
!  Cross-category correlations
!
!-----------------------------------------------------------------------
!
  REAL,    ALLOCATABLE :: xcor(:,:)
  INTEGER, ALLOCATABLE :: icatg(:,:)
!
!-----------------------------------------------------------------------
!
!  ARPS dimensions:
!
!  nx, ny, nz: Dimensions of analysis grid.
!
!-----------------------------------------------------------------------
!
  INTEGER :: nx       ! Number of grid points in the x-direction
  INTEGER :: ny       ! Number of grid points in the y-direction
  INTEGER :: nz       ! Number of grid points in the z-direction
  INTEGER :: nzsoil   ! Number of soil levels

  INTEGER :: nxlg     ! Number of grid points in the x-direction large grid
  INTEGER :: nylg     ! Number of grid points in the y-direction large grid

!
!-----------------------------------------------------------------------
!
!  Soil types.
!
!-----------------------------------------------------------------------
!
  INTEGER :: nstyps            ! Number of soil types
!
!-----------------------------------------------------------------------
!
!  Misc
!
!-----------------------------------------------------------------------
!
  INTEGER :: nt                ! Number of time levels of data
  INTEGER :: ncat              ! Number of correlation categories
  REAL    :: rlim              ! Largest possible radius of influence
  REAL    :: rlimv             ! Same, but for radial velocity only
  INTEGER :: mosaicopt
  INTEGER :: raduvobs

!-----------------------------------------------------------------------
!
!  Analysis parameters
!
!-----------------------------------------------------------------------
!
  REAL,    PARAMETER :: rhmin=0.05  ! rh safety net value to prevent neg qv
  INTEGER, PARAMETER :: iqspr=3     ! Use qobs of pstn to combine x,y,elev
  INTEGER, PARAMETER :: klim= 1     ! Min of one other sfc station for QC
!
!-----------------------------------------------------------------------
!
!  Indices of specific observation variables
!
!-----------------------------------------------------------------------
!
  INTEGER, PARAMETER :: ipres=3   ! Pressure
  INTEGER, PARAMETER :: iptmp=4   ! Potential Temperature
  INTEGER, PARAMETER :: iqv=5     ! Specific Humidity
!

  CHARACTER (LEN=256) :: grdbasfn, timdepfn
  CHARACTER (LEN=10)  :: timsnd
  CHARACTER (LEN=256) :: dirfn
  !INTEGER :: tmstrln

  INTEGER :: nx_m, ny_m, nz_m, nzsoil_m, nstyps_m, nscalar_m
  REAL    :: dx_m, dy_m
  INTEGER :: iproj_m, mphyopt_m
  REAL    :: trulat1_m, trulat2_m, trulon_m
  REAL    :: ctrlat_m, ctrlon_m
!
!-----------------------------------------------------------------------
!
!  ARPS cloud cover analysis variables:
!
!-----------------------------------------------------------------------
!
! Remapping parameters of the remapped radar data grid
!
  INTEGER :: strhopt_rad                  ! streching option
  INTEGER :: mapproj_rad                  ! map projection indicator
  REAL    :: dx_rad,dy_rad,dz_rad,dzmin_rad  ! grid spcngs
  REAL    :: ctrlat_rad,ctrlon_rad           ! central lat and lon
  REAL    :: tlat1_rad,tlat2_rad,tlon_rad    ! true lat and lon
  REAL    :: scale_rad                       ! map scale factor
!
!-----------------------------------------------------------------------
!
!  Common block that stores remapping parameters for the radar
!  data file.
!
!-----------------------------------------------------------------------
!
  COMMON /remapfactrs_rad/ strhopt_rad,mapproj_rad
  COMMON /remapfactrs_rad2/ dx_rad,dy_rad,dz_rad,dzmin_rad,             &
           ctrlat_rad,ctrlon_rad,                                       &
           tlat1_rad,tlat2_rad,tlon_rad,scale_rad
!
!-----------------------------------------------------------------------
!
!  ARPS include files
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'phycst.inc'
  INCLUDE 'bndry.inc'
  INCLUDE 'grid.inc'        ! Grid parameters
  INCLUDE 'adjust.inc'
!
!-----------------------------------------------------------------------
!
!  Surface Station variables
!
!-----------------------------------------------------------------------
!
!  INTEGER :: isrcsng(mx_sng)
!  INTEGER :: icatsng(mx_sng)
!  REAL :: latsng(mx_sng,ntime)
!  REAL :: lonsng(mx_sng,ntime)
!  REAL :: hgtsng(mx_sng,ntime)
!  REAL :: xsng(mx_sng)
!  REAL :: ysng(mx_sng)
!  REAL :: trnsng(mx_sng)
!  INTEGER :: timesng(mx_sng,ntime)
!  CHARACTER (LEN=5) :: stnsng(mx_sng,ntime)
!
!-----------------------------------------------------------------------
!
!  Surface (single-level) read-in observation variables
!
!-----------------------------------------------------------------------
!
!  CHARACTER (LEN=8) :: wx(mx_sng,ntime)
!  CHARACTER (LEN=8) :: csrcsng(mx_sng,ntime)
!  CHARACTER (LEN=1) :: store_emv(mx_sng,5,ntime)
!  CHARACTER (LEN=4) :: store_amt(mx_sng,5,ntime)
!  INTEGER :: kloud(mx_sng,ntime),idp3(mx_sng,ntime)
!  REAL :: store_hgt(mx_sng,5,ntime)
!  REAL :: obrdsng(mx_sng,nvar_sng,ntime)
!
!  REAL :: obsng  (nvar_anx_3DVAR,mx_sng)
!  REAL :: odifsng(nvar_anx_3DVAR,mx_sng)
!  REAL :: oanxsng(nvar_anx_3DVAR,mx_sng)
!  REAL :: corsng(mx_sng,nvar_anx_3dvar)
!  REAL :: thesng(mx_sng)
!
!-----------------------------------------------------------------------
!
!  Upper Air Station variables
!
!-----------------------------------------------------------------------
!
!  INTEGER :: isrcua(mx_ua)
!  REAL :: xua(mx_ua)
!  REAL :: yua(mx_ua)
!  REAL :: trnua(mx_ua)
!  REAL :: elevua(mx_ua)
!  REAL :: hgtua(nz_ua,mx_ua)
!  INTEGER :: nlevsua(mx_ua)
!  CHARACTER (LEN=5) :: stnua(mx_ua)
!
!-----------------------------------------------------------------------
!
!  Upper-air observation variables
!
!-----------------------------------------------------------------------
!
!  REAL :: obsua (nvar_anx_3DVAR,nz_ua,mx_ua)
!  REAL :: odifua(nvar_anx_3DVAR,nz_ua,mx_ua)
!  REAL :: oanxua(nvar_anx_3DVAR,nz_ua,mx_ua)
!  REAL :: corua(nz_ua,mx_ua,nvar_anx_3dvar)
!  REAL :: theua(nz_ua,mx_ua)
!
!-----------------------------------------------------------------------
!
!  Radar site variables and observation variables
!
!  Moved to module_radarobs.f90
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Retrieval radar variables
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=5) :: stnret(mx_ret)
  INTEGER :: isrcret(0:mx_ret)
  REAL    :: latret(mx_ret)
  REAL    :: lonret(mx_ret)
  REAL    :: elvret(mx_ret)
!
!-----------------------------------------------------------------------
!
!  Retrieval observation variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: iret(mx_colret)
  INTEGER :: nlevret(mx_colret)
  REAL    :: latretc(mx_colret)
  REAL    :: lonretc(mx_colret)
  REAL    :: xretc(mx_colret)
  REAL    :: yretc(mx_colret)
  REAL    :: trnretc(mx_colret)
  REAL    :: hgtretc(nz_ret,mx_colret)
  REAL    :: theretc(nz_ret,mx_colret)
  REAL    :: qrret(nz_ret,mx_colret)

!g REAL :: obsret (nvar_anx_3DVAR,nz_ret,mx_colret)
!g REAL :: odifret(nvar_anx_3DVAR,nz_ret,mx_colret)
!g REAL :: oanxret(nvar_anx_3DVAR,nz_ret,mx_colret)

!g REAL :: corret(nz_ret,mx_colret,nvar_anx_3dvar)
  REAL :: corret(1,1,1) ! 3dvar does not need this any more

!
!-----------------------------------------------------------------------
!
!  Namen de variables
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=6) :: var_sfc(nvar_sng)
  DATA var_sfc/ 't     ','td    ','dd    ','ff    ',                    &
                'urot  ','vrot  ','pstn  ','pmsl  ',                    &
                'alt   ','ceil  ','low   ','cover ',                    &
                'solar ','vis'/
  CHARACTER (LEN=6) :: var_ua(nvar_ua)
  DATA var_ua / 'press ',                                               &
                'temp  ','dewpt ','dd    ','ff    '/

  CHARACTER (LEN=6) :: var_anx(12)
  DATA var_anx/ 'u     ','v     ','press ','theta ','qv    ','w     ',  &
                '      ','      ','      ','      ','      ','      ' /

!
!-----------------------------------------------------------------------
!
!  Source-dependent parameters
!  Qsrc is the expected square error it is used for
!  setting the QC threshold (qclim) and for relative
!  weighting of observations.
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=80) :: srcback
  REAL    :: hqback(nz_tab)
  REAL    :: qback_temp(6)
  INTEGER :: nlvqback

  CHARACTER (LEN=80) :: srcsng_full(nsrc_sng)

  CHARACTER (LEN=80) :: srcua_full(nsrc_ua)
  INTEGER :: nlvuatab(nsrc_ua)
  REAL    :: huaqsrc(nz_tab,nsrc_ua)

  CHARACTER (LEN=80) :: srcret_full(nsrc_ret)
  INTEGER :: nlvrttab(nsrc_ret)
  REAL    :: hrtqsrc(nz_tab,nsrc_ret)

!g  REAL :: qback  (nvar_anx_3DVAR,nz_tab)
!g  REAL :: qsrcsng(nvar_anx_3DVAR,nsrc_sng)
!g  REAL :: qsrcua (nvar_anx_3DVAR,nz_tab,nsrc_ua)
!g  REAL :: qsrcret(nvar_anx_3DVAR,nz_tab,nsrc_ret)
!
!-----------------------------------------------------------------------
!
!  Quality Control Parameters and Variables
!
!-----------------------------------------------------------------------
!
  REAL, PARAMETER :: rmiss = -99., dpmiss = -9999.

  REAL :: qcmulsng(nsrc_sng)
  REAL :: qcmulua(nsrc_ua)
  REAL :: qcmulret(nsrc_ret)

!
!-----------------------------------------------------------------------
!
!  Filenames and such
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=12)  :: suffix
  CHARACTER (LEN=256) :: froot
  CHARACTER (LEN=256) :: qclist
  CHARACTER (LEN=256) :: qcsave
  CHARACTER (LEN=256) :: stats        ! runname + '.lst'
  INTEGER :: istatus,istat_radar
  INTEGER :: nobsng,nobsrd(ntime)
  INTEGER :: i4timanx
  INTEGER :: lenfnm,maxsuf,lensuf,dotloc,mxroot,lenroot
  INTEGER :: iqclist,iqcsave

  CHARACTER (LEN=256) :: status_file  ! runname + '.adasstat'
  CHARACTER (LEN=256) :: stnlist_file  ! runname + '.adasstn'

  INTEGER :: n_used_sng(nsrc_sng), n_used_ua(nsrc_sng)
  INTEGER :: jsta, klev, ngoodlev

!
!-----------------------------------------------------------------------
!
!  MPI bookkeeping
!
!-----------------------------------------------------------------------
!

  INTEGER :: indexsng(mx_sng)    ! indexes each data point according to
                                 ! which local proc domain it resides
  INTEGER :: sngstats(mx_sng)    ! for "adasstn" file
  INTEGER :: sngstats_tmp(mx_sng) ! for "adasstn" file
  INTEGER :: sngstats2(mx_sng)    ! for output stats
  LOGICAL :: usesng(mx_sng)       ! whether each obs should be use in 3dvar

  INTEGER :: indexua(mx_ua)      ! indexes each data point according to
                                 ! which local proc domain it resides
  INTEGER :: uastats(mx_ua)      ! for "adasstn" file
  INTEGER :: uastats_tmp(mx_ua)  ! for "adasstn" file
  LOGICAL :: useua(mx_ua)        ! whether each obs should be use in 3dvar

  INTEGER :: indexret(mx_colret) ! indexes each data point according to
                                 ! which local proc domain it resides
                                 ! WARNING:  used not yet coded!!!

  INTEGER :: ksngmax              ! Max "ksng" value.
  INTEGER :: kuamax               ! Max "kua" value.
  INTEGER :: kradmax              ! Max "krad" value.
  INTEGER, ALLOCATABLE :: ksng(:) ! Number of obs owned by each processor
  INTEGER, ALLOCATABLE :: kua(:)  ! Number of obs owned by each processor
  INTEGER, ALLOCATABLE :: krad(:) ! Number of obs owned by each processor

  !INTEGER :: iproc,iprocv        ! Radii of influence "i" direction
  !INTEGER :: jproc,jprocv        ! Radii of influence "j" direction
  !
  !INTEGER, ALLOCATABLE :: mpi_map(:,:) ! Map of communication entries
  !INTEGER :: nmap                ! Number of entries in "mpi_map".
  !
  !INTEGER, ALLOCATABLE :: mpi_mapv(:,:) ! Same for radial velocity
  !INTEGER :: nmapv               ! Number of entries in "mpi_mapv".

!
!-----------------------------------------------------------------------
!
!  Physical constants
!
!-----------------------------------------------------------------------
!
  REAL, PARAMETER :: kts2ms=0.514444, mb2pa=100., f2crat=(5./9.)
!
!-----------------------------------------------------------------------
!
!  ptlapse is minimum potential temperature lapse rate to use in
!  superadiabatic check.  Used to ensure initial static stability.
!
!-----------------------------------------------------------------------

  REAL, PARAMETER :: ptlapse = (1.0/4000.)    ! deg K / m
!
!-----------------------------------------------------------------------
!
!  Function f_qvsat and inline directive for Cray PVP
!
!-----------------------------------------------------------------------
!
  REAL :: f_qvsat

!fpp$ expand (f_qvsat)
!!dir$ inline always f_qvsat
!
!-----------------------------------------------------------------------
!
!  Misc local variables
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=8  ) :: rdsource
  CHARACTER (LEN=8  ) :: srcstr
  CHARACTER (LEN=256) :: basdmpfn
  CHARACTER (LEN=80 ) :: header
  INTEGER :: i,j,k,l,nq,ios,ktab,isrc,jsrc,ivar,ifile
  INTEGER :: grdbas,lbasdmpf
  INTEGER :: nobsua,ncolret,itype,kntgood
  REAL :: latua,lonua

  REAL :: tgrid,qvmin,qvsat,qsrcmax,rngsq
  REAL :: temp, tvbar

  !INTEGER :: ixrad(mx_rad),jyrad(mx_rad)

  INTEGER, PARAMETER  :: retqropt=1

  INTEGER :: imstat

  INTEGER :: cntl_var_rh2
            ! = 1: relative humindity is used in analysis
            ! = 0: specific humindity is used in analysis

  INTEGER :: nvar_anx
  INTEGER :: ipass, ierr
  REAL    :: f_cputime
  REAL    :: cpuin, cpuout

  CHARACTER (LEN=8) :: tmp
  INTEGER :: lentmp
  INTEGER :: ierror
  LOGICAL :: ref_use

  CHARACTER(LEN=9) :: PROGNAME='news3dvar'

  CHARACTER(LEN=256) :: namelist_filename
  INTEGER :: wrfoutopt

  CHARACTER(LEN=40) :: qnames_wrf(40)
  INTEGER           :: nqw

  CHARACTER(LEN=256) :: filename

  CHARACTER (LEN=256) :: wrf_file
  CHARACTER (LEN=19)  :: timestring,timstr
  INTEGER             :: ncid
  INTEGER             :: nx_wrf, ny_wrf, nz_wrf, nzsoil_wrf
  INTEGER             :: nxlg_wrf, nylg_wrf
  INTEGER             :: iproj_wrf

  REAL, ALLOCATABLE, DIMENSION(:,:,:) :: pxb, qvxb

  REAL, ALLOCATABLE :: xlat(:,:),xlon(:,:)
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  CALL acct_init(1)
  CALL set_acct(iniana_acct)

!-----------------------------------------------------------------------

  cputimearray=0.0
  nt = 1    ! Number of time levels of data
  ncat = 4  ! Number of correlation categories

!-----------------------------------------------------------------------
!  Set grid mode to non-nesting and grid index, mgrid, to 1.
!-----------------------------------------------------------------------

  nestgrd=0
  mgrid=1

!-----------------------------------------------------------------------
!  read in ARPS namelists
!-----------------------------------------------------------------------

  namelist_filename = ' '
  CALL init_model_para(namelist_filename)

!-----------------------------------------------------------------------
!  Get grid specific information
!-----------------------------------------------------------------------
!

  IF ( modelopt == 2 ) THEN
    WRITE(timestring,'(i4.4,5(a,i2.2))') year,'-',month,'-',day,        &
                                        '_',hour,':',minute,':',second

    wrf_file = inifile

    IF (nproc_x_in > 1 .OR. nproc_y_in > 1) THEN
      WRITE(filename,'(2a)') TRIM(inifile),'_0000'
    ELSE
      WRITE(filename,'(a)') TRIM(inifile)
    END IF

    IF (myproc == 0) THEN  ! Works with one joined file only at present
      CALL get_wrf_domsize(TRIM(filename),timestring,nx_wrf,ny_wrf,     &
                           nz_wrf,nzsoil,nt,nxlg_wrf,nylg_wrf,istatus)
      nx = nxlg_wrf + 2
      ny = nylg_wrf + 2
      nz = nz_wrf + 2

      WRITE(*,'(1x,3(a,I0),/,1x,3(a,I0),a)')                            &
          'INFO: Use global dimensions          nx     = ',nx,          &
                            ', ny     = ',ny,', nz     = ',nz,          &
          '      based on WRF domain dimensions nx_wrf = ',nxlg_wrf,    &
                            ', ny_wrf = ',nylg_wrf,', nz_wrf = ',nz_wrf,'.'

      CALL get_wrf_grid( TRIM(filename),iproj_wrf,sclfct,trulat1,trulat2,&
                         trulon,ctrlat,ctrlon,dx,dy,mphyopt,istatus )

      IF(iproj_wrf == 0) THEN        ! NO MAP PROJECTION
        mapproj = 0
      ELSE IF(iproj_wrf == 1) THEN   ! LAMBERT CONFORMAL
        mapproj = 2
      ELSE IF(iproj_wrf == 2) THEN   ! POLAR STEREOGRAPHIC
        mapproj = 1
      ELSE IF(iproj_wrf == 3) THEN   ! MERCATOR
        mapproj = 3
      ELSE
        WRITE(6,*) 'Unknown map projection, ', iproj_wrf
        istatus = -555
      END IF
    END IF

  ELSE

    IF (myproc == 0) THEN

      CALL get_dims_from_data(inifmt,TRIM(inifile),                     &
                              nx,ny,nz,nzsoil,nstyps, istatus)

      WRITE(*,'(1x,2(a,I0),a)')                                         &
          'INFO: Get global dimensions nx = ',nx,', ny = ',ny,'.'

      CALL get_arps_grid( inifmt,inifile,mapproj,sclfct,trulat1,trulat2,&
                         trulon,ctrlat,ctrlon,dx,dy,mphyopt,istatus )
    END IF

    IF (mphyopt == 0) THEN  ! mphyopt is not in the ARPS history file
      mphyopt = 2
    END IF

  END IF
  CALL mpupdatei(istatus,1)
  IF( istatus /= 0 ) CALL arpsstop(                                   &
      'Problem occurred when trying to get dimensions from data.', 1)

  CALL mpupdatei(nx,1)
  CALL mpupdatei(ny,1)
  CALL mpupdatei(nz,1)
  CALL mpupdatei(nzsoil,1)

  CALL mpupdatei(mapproj,1)
  CALL mpupdater(sclfct,1)
  CALL mpupdater(trulat1,1)
  CALL mpupdater(trulat2,1)
  CALL mpupdater(trulon,1)
  CALL mpupdater(ctrlat,1)
  CALL mpupdater(ctrlon,1)
  CALL mpupdater(dx,1)
  CALL mpupdater(dy,1)
  CALL mpupdatei(mphyopt,1)

  dxinv = 1.0/dx
  dyinv = 1.0/dy
  dzinv = 1.0/dz   ! dz reads in from namelist and only used with ARPS model

  nxlg = nx
  nylg = ny

  IF (MOD(nx-3,nproc_x) /= 0) THEN
    WRITE(*,'(1x,a,2(I0,a))') 'ERROR: dimension size in X direction (',nx,') is not a multiple of nproc_x (',nproc_x,').'
    CALL arpsstop('ERROR: dimension problem.',1)
  END IF

  IF (MOD(ny-3,nproc_y) /= 0) THEN
    WRITE(*,'(1x,a,2(I0,a))') 'ERROR: dimension size in Y direction (',ny,') is not a multiple of nproc_x (',nproc_y,').'
    CALL arpsstop('ERROR: dimension problem.',1)
  END IF

  nx = (nx-3)/nproc_x+3
  ny = (ny-3)/nproc_y+3

  IF (MOD((nxlg-3),nproc_x_out) /= 0) THEN
    WRITE(6,'(/,1x,2a,I4,a,/,8x,a,I5,a,I4,a,/)') 'ERROR: ',         &
    'wrong size of nproc_x_out = ',nproc_x_out,'.',                 &
    'The grid size is ',nxlg,                                       &
    ' and it''s physical size is not dividable by ',nproc_x_out,'.'
    CALL arpsstop('Wrong value of nproc_x_out.',1)
  END IF

  IF (MOD((nylg-3),nproc_y_out) /= 0) THEN
    WRITE(6,'(/,1x,2a,I4,a,/,8x,a,I5,a,I4,a,/)') 'ERROR: ',         &
    'wrong size of nproc_y_out = ',nproc_y_out,'.',                 &
    'The grid size is ',nylg,                                       &
    ' and it''s physical size is not dividable by ',nproc_y_out,'.'
    CALL arpsstop('Wrong value of nproc_y_out.',1)
  END IF

!-----------------------------------------------------------------------
!
! From Now on, nx/ny are local sizes instead of global sizes
!
!-----------------------------------------------------------------------

  IF (modelopt == 2) THEN

    ALLOCATE(pxb (nx,ny,nz), STAT = istatus)
    ALLOCATE(qvxb(nx,ny,nz), STAT = istatus)

    pxb=0.0
    qvxb=0.0
  ELSE
    CALL mpupdatei(nstyps,1)
  END IF

  CALL init_scalar_array(istatus) ! based on modelopt & mphyopt, both in globcst.inc
  ! mphyopt was mapped to a similar ARPS option inside for using cloud analysis
  ALLOCATE(xlat(nx,ny),  STAT = istatus)
  ALLOCATE(xlon(nx,ny),  STAT = istatus)

!-----------------------------------------------------------------------
!  Read in adas & 3dvar namelist parameters
!-----------------------------------------------------------------------
!
  CALL initadas(namelist_filename)

  CALL init3dvar(cntl_var_rh2, npass, nx, ny, namelist_filename)  ! used dx/dy

  ref_use = ANY(ref_opt==1)

  IF (ref_use .OR. hydro_opt==1 .OR. satcwpobs==1) THEN
    nvar_anx = PTR_LN+nscalarq

    DO nq = 1, nscalarq
      WRITE(var_anx(PTR_LN+nq),'(a)') TRIM(qnames(nq))
    END DO
  ELSE
    nvar_anx = 6
  END IF

!-----------------------------------------------------------------------
!
! Allocate analysis arrays
!
!-----------------------------------------------------------------------

  CALL varpara_set_varindex(nx,ny,nz,istatus)

  CALL allocate_3dvarObs_arrays(progname,ntime,nz_tab,nscalar,          &
                mx_sng,mx_ua,mx_colret,nz_ua,nz_ret,nvar_anx,           &
                nvar_sng,nsrc_sng,nsrc_ua,nsrc_ret,istatus)

  IF (istatus /= 0) THEN
    WRITE(*,'(1x,/,A,I2,A)') 'ERROR: allocate_3dvarObs_arrays, status= ',istatus, '.'
    CALL arpsstop('FATAL: Unable to allocate array. Program abort.',1)
  END IF

  CALL allocate_modelArray    (nx,ny,nz,nzsoil,nstyps,modelopt,istatus)

  ALLOCATE(knt(nvar_anx,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:knt")
  !ALLOCATE(rngsqi(nvar_anx),STAT=istatus)
  !CALL check_alloc_status(istatus, "news3dvar:rngsqi")
  ALLOCATE(wgtsum(nvar_anx,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:wgtsum")
  ALLOCATE(zsum(nvar_anx,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:zsum")
  ALLOCATE(sqsum(nvar_anx,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:sqsum")

  ALLOCATE(xs(nx),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:xs")
  ALLOCATE(ys(ny),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:ys")

  ALLOCATE(xlg(nxlg),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:xlg")
  ALLOCATE(ylg(nylg),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:ylg")

  ALLOCATE(xslg(nxlg),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:xslg")
  ALLOCATE(yslg(nylg),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:yslg")

  ALLOCATE(zs(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:zs")
  ALLOCATE(latgr(nx,ny),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:latgr")
  ALLOCATE(longr(nx,ny),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:longr")

  ALLOCATE(rhostr(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:rhostr")

  ALLOCATE(tem1(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:tem1")
  ALLOCATE(tem2(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:tem2")
  ALLOCATE(tem3(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:tem3")
  ALLOCATE(tem4(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:tem4")
  ALLOCATE(tem5(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:tem5")
  ALLOCATE(tem6(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:tem6")
  ALLOCATE(tem7(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:tem7")
  ALLOCATE(tem8(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:tem8")
  ALLOCATE(tem9(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:tem9")
  ALLOCATE(tem10(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:tem10")
  ALLOCATE(tem11(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:tem11")
  ALLOCATE(tem12(nx,ny,nz), STAT = istatus)
  CALL check_alloc_status(istatus, "3dvar:tem12")
  ALLOCATE(tem13(nx,ny,nz), STAT = istatus)
  CALL check_alloc_status(istatus, "before minimization: tem13")
  ALLOCATE(tem14(nx,ny,nz), STAT = istatus)
  CALL check_alloc_status(istatus, "before minimization: tem14")

  ALLOCATE(anx(nx,ny,nz,nvar_anx),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:anx")

  ALLOCATE(xcor(ncat,ncat),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:xcor")
  ALLOCATE(icatg(nx,ny),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:icatg")

  !nmap = ( 2 * iproc + 1 ) * ( 2 * jproc + 1 )
  !ALLOCATE(mpi_map(nmap,2),STAT=istatus)
  !CALL check_alloc_status(istatus, "news3dvar:mpi:map")
  !
  !nmapv = ( 2 * iprocv + 1 ) * ( 2 * jprocv + 1 )
  !ALLOCATE(mpi_mapv(nmap,2),STAT=istatus)
  !CALL check_alloc_status(istatus, "news3dvar:mpi:mapv")

  CALL ens_allocate_varwrk(nx,ny,nz,istatus)

  IF (myproc == 0) WRITE (*,*) "Finished allocating arrays"

!-----------------------------------------------------------------------
! Initialize the allocated arrays to zero.
!-----------------------------------------------------------------------

  knt=0.0
  !rngsqi=0.0
  wgtsum=0.0
  zsum=0.0
  sqsum=0.0

  xs=0.0;    xslg = 0.0;  xlg = 0.0
  ys=0.0;    yslg = 0.0;  ylg = 0.0
  zs=0.0
  latgr=0.0; longr=0.0

  tem1=0.0
  tem2=0.0
  tem3=0.0
  tem4=0.0
  tem5=0.0
  tem6=0.0
  tem7=0.0
  tem8=0.0
  tem9=0.0
  tem10=0.0
  tem11=0.0
  tem12=0.0

  qsrcsng = 0.0

  anx=0.0

  xcor=0.0
  icatg=0

!
!-----------------------------------------------------------------------
!
!  Read in quality control info.  Most of the information will not be
!  broadcast, as processor 0 will handle pre-processing of obs before
!  the data gets sent to the other processors.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Set expected _squared_ background error for each variable
!  This will depend on the particular background field used,
!  season, age of forecast, etc.
!
!-----------------------------------------------------------------------
!

  IF (myproc == 0) THEN
     WRITE(6,'(/,a,a)') ' Reading ', TRIM(backerrfil)
     OPEN(4,FILE=trim(backerrfil),STATUS='old')
     READ(4,'(a80)') srcback
     READ(4,'(a80)') header
     IF (ibeta1 == 1 .AND. ibeta2 == 0) THEN
        WRITE(6,'(//,a,/a)') 'Background Std Error for',srcback
        WRITE(6,'(1x,a)')                                                  &
            '   k    hgt(m)  u(m/s)  v(m/s) pres(mb) temp(K)  RH(%)'
        DO ktab=1,nz_tab
          READ(4,*,iostat=istatus) hqback(ktab),                           &
                                   qback(PTR_P, ktab),                     &
                                   qback(PTR_PT,ktab),                     &
                                   qback(PTR_QV,ktab),                     &
                                   qback(PTR_U, ktab),                     &
                                   qback(PTR_V, ktab)
          IF( istatus /= 0 ) EXIT
          WRITE(6,'(1x,i4,f10.2,5f8.2)') ktab,hqback(ktab),                &
                                  (qback(ivar,ktab),ivar=1,5)
        END DO

        IF (qback(PTR_P, 1) < 10) THEN
          qback(PTR_P,:) = 100. * qback(PTR_P,:)
        END IF
        IF( cntl_var_rh2 == 1 ) THEN
          qback(PTR_QV,:) = 0.01 * qback(PTR_QV,:)
        ELSE
          qback(PTR_QV,:) = 0.005
        END IF
        qback(PTR_W,:) = qback(PTR_U,:)
        nlvqback=ktab-1
     ELSE !IF (ibeta1 == 1 .AND. ibeta2 == 1) THEN
         IF (srcback /= 'NEWS-E 6-hour Forecasts') CALL arpsstop("Only Background Error derived from 6-hour NEWS-E forecast is currently supported.",1)
         WRITE(6,'(//,a,/a)') 'Background Std Error for',srcback
         IF (nscalarq > 5) THEN
             WRITE(6,'(1x,a)')              &
                 '   k    hgt(m)  u(m/s)  v(m/s)  w(m/s)   p(Pa)    T(K) QV(kg/kg)       QC       QR       QS       QI       QG       QH'
         ELSE
             WRITE(6,'(1x,a)')              &
                 '   k    hgt(m)  u(m/s)  v(m/s)  w(m/s)   p(Pa)    T(K) QV(kg/kg)       QC       QR       QS       QI    QG/QH'
         END IF
         DO ktab=1,nz_tab
             READ(4,*,iostat=istatus) hqback(ktab), qback(PTR_P, ktab), qback(PTR_PT, ktab), qback(PTR_QV, ktab),       &
                                      qback(PTR_U, ktab), qback(PTR_V, ktab), qback(PTR_W, ktab), qback_temp(1),        &
                                      qback_temp(2), qback_temp(3), qback_temp(4), qback_temp(5), qback_temp(6)
             IF (istatus /= 0) EXIT
             IF (P_QC > 0) qback(6+P_QC, ktab) = qback_temp(1)
             IF (P_QR > 0) qback(6+P_QR, ktab) = qback_temp(2)
             IF (P_QS > 0) qback(6+P_QS, ktab) = qback_temp(3)
             IF (P_QI > 0) qback(6+P_QI, ktab) = qback_temp(4)
             IF (P_QG > 0 .AND. P_QH > 0) THEN
               qback(6+P_QG, ktab) = qback_temp(5)
               qback(6+P_QH, ktab) = qback_temp(6)
             ELSE
               IF (P_QG > 0) qback(6+P_QG, ktab) = qback_temp(5) + qback_temp(6)
               IF (P_QH > 0) qback(6+P_QH, ktab) = qback_temp(5) + qback_temp(6)
             END IF
             !IF (hqback(ktab)<=10000) THEN
             !  qback(PTR_QV,ktab)=0.0005/EXP(hqback(ktab)/3000)
             !ELSE
             !  qback(PTR_QV,ktab)=1E-5
             !END IF
             !qback(PTR_QV,ktab)=0.005
             IF (nscalarq > 5) THEN
                 WRITE(6,'(1x, i4, f10.3, 5f8.3, 2x, 7(f8.6,1x))') ktab, hqback(ktab), qback(PTR_U, ktab),   &
                                     qback(PTR_V, ktab), qback(PTR_W, ktab), qback(PTR_P, ktab),             &
                                     qback(PTR_PT, ktab), qback(PTR_QV, ktab), qback(6+P_QC, ktab),          &
                                     qback(6+P_QR, ktab), qback(6+P_QS, ktab), qback(6+P_QI, ktab),          &
                                     qback(6+P_QG, ktab), qback(6+P_QH, ktab)
             ELSE
                 WRITE(6,'(1x, i4, f10.3, 5f8.3, 2x, 7(f8.6,1x))') ktab, hqback(ktab), qback(PTR_U, ktab),   &
                                     qback(PTR_V, ktab), qback(PTR_W, ktab), qback(PTR_P, ktab),             &
                                     qback(PTR_PT, ktab), qback(PTR_QV, ktab), qback(6+P_QC, ktab),          &
                                     qback(6+P_QR, ktab), qback(6+P_QS, ktab), qback(6+P_QI, ktab),          &
                                     qback(max(6+P_QG, 6+P_QH), ktab)
             END IF
         END DO
         nlvqback=ktab-1
!    ELSE
!        WRITE(6,'(a)') ' Background error covariances are completely derived from ensemble member.'
     END IF
     CLOSE(4)

     DO jsrc=1,nsrc_sng
       IF ( ANY(iusesng(jsrc,:) > 0) ) THEN
        IF(srcsng(jsrc) /= 'NULL') THEN
           WRITE(6,'(/,a,a)') ' Reading ', TRIM(sngerrfil(jsrc))
           OPEN(4,FILE=trim(sngerrfil(jsrc)),STATUS='old')
           READ(4,'(a8)') rdsource
           IF(rdsource /= srcsng(jsrc)) THEN
              WRITE(6,'(a,i4,a,a,a,a,a)')                               &
              ' Mismatch of source names',jsrc,                         &
              ' read ',rdsource,' expected ',srcsng(jsrc)
              CALL arpsstop("mismatch",1)
           END IF
           READ(4,'(a80)') srcsng_full(jsrc)
           READ(4,*) qcmulsng(jsrc)
           READ(4,'(a80)') header
           READ(4,*) qsrcsng(PTR_P, jsrc), qsrcsng(PTR_PT,jsrc),        &
                     qsrcsng(PTR_QV,jsrc), qsrcsng(PTR_U, jsrc),        &
                     qsrcsng(PTR_V, jsrc)
             WRITE(6,'(1x,4a)') 'Single-level std error for ',          &
                              srcsng(jsrc),' - ',srcsng_full(jsrc)
           WRITE(6,'(a,f5.1)') ' QC multiplier: ',qcmulsng(jsrc)
           WRITE(6,'(1x,a)')                                            &
               '   u (m/s)  v (m/s)  pres(mb) temp(K) RH(%)'
           WRITE(6,'(1x,5f8.2)') (qsrcsng(ivar,jsrc),ivar=1,5)
           qsrcsng(PTR_P,jsrc)=100.*qsrcsng(PTR_P,jsrc)
           IF( cntl_var_rh2 == 1 ) THEN
             qsrcsng(PTR_QV,jsrc)=0.01*qsrcsng(PTR_QV,jsrc)
           ELSE
!             qsrcsng(PTR_QV,jsrc)=0.002
!            qsrcsng(PTR_QV,jsrc)=0.004
!03/23/2019
             qsrcsng(PTR_QV,jsrc)=0.001
           ENDIF
           qsrcsng(PTR_W,jsrc)=qsrcsng(PTR_U,jsrc)
           CLOSE(4)
        ELSE
          DO k=1,npass
            iusesng(jsrc,k) = 0
          END DO
        END IF
      END IF
     END DO

     DO jsrc=1,nsrc_ua
       IF ( ANY(iuseua(jsrc,:) > 0) ) THEN
         IF(srcua(jsrc) /= 'NULL') THEN
           WRITE(6,'(/,a,a)') ' Reading ', TRIM(uaerrfil(jsrc))
           OPEN(4,FILE=trim(uaerrfil(jsrc)),STATUS='old')
           READ(4,'(a8)') rdsource
           IF(rdsource /= srcua(jsrc)) THEN
              WRITE(6,'(a,i4,a,a,a,a,a)')                               &
                 ' Mismatch of source names',jsrc,                      &
                 ' read ',rdsource,' expected ',srcua(jsrc)
              CALL arpsstop("mismatch",1)
           END IF
           READ(4,'(a80)') srcua_full(jsrc)
           READ(4,*) qcmulua(jsrc)
           READ(4,'(a80)') header
            WRITE(6,'(1x,4a)') 'UA data std error for ',                &
                             srcua(jsrc),' - ',srcua_full(jsrc)
           WRITE(6,'(a,f5.1)') ' QC multiplier: ',qcmulua(jsrc)
           WRITE(6,'(1x,a)')                                            &
              '   k    hgt(m)  u(m/s)  v(m/s) pres(mb) temp(K)  RH(%)'
           DO ktab=1,nz_tab
              READ(4,*,iostat=istatus) huaqsrc(ktab,jsrc),              &
                              qsrcua(PTR_P, ktab,jsrc),                 &
                              qsrcua(PTR_PT,ktab,jsrc),                 &
                              qsrcua(PTR_QV,ktab,jsrc),                 &
                              qsrcua(PTR_U, ktab,jsrc),                 &
                              qsrcua(PTR_V, ktab,jsrc)
              IF( istatus /= 0 ) EXIT
              WRITE(6,'(1x,i4,f10.2,5f8.2)') ktab,huaqsrc(ktab,jsrc),   &
                 (qsrcua(ivar,ktab,jsrc),ivar=1,5)
              qsrcua(PTR_P,ktab,jsrc)=100.*qsrcua(PTR_P,ktab,jsrc)
              IF( cntl_var_rh2 == 1 ) THEN
                qsrcua(PTR_QV,ktab,jsrc)=0.01*qsrcua(PTR_QV,ktab,jsrc)
              ELSE
                !IF (hqback(ktab)<=10000) THEN
                !  qsrcua(PTR_QV,ktab,jsrc)=0.002/EXP(huaqsrc(ktab,jsrc)/3000)
                !ELSE
                !  qsrcua(PTR_QV,ktab,jsrc)=1E-5
                !  qsrcua(PTR_QV,ktab,jsrc)=0.005
                !03/23/2019
                 qsrcsng(PTR_QV,jsrc)=0.001
                !END IF
              ENDIF
              qsrcua(PTR_W,ktab,jsrc)=qsrcua(PTR_U,ktab,jsrc)    !for w GAO
           END DO
           nlvuatab(jsrc)=ktab-1
           CLOSE(4)
         ELSE
           DO k=1,npass
             iuseua(jsrc,k) = 0
           END DO
         END IF
       ELSE
         nlvuatab(:) = 1  ! nz_tab
       END IF
     END DO
     DO jsrc=1,nsrc_ret
       IF (ANY(iuseret(jsrc,:) > 0) ) THEN
        IF(srcret(jsrc) /= 'NULL') THEN
           WRITE(6,'(/,a,a)') ' Reading ', TRIM(reterrfil(jsrc))
           OPEN(4,FILE=trim(reterrfil(jsrc)),STATUS='old')
           READ(4,'(a8)') rdsource
           IF(rdsource /= srcret(jsrc)) THEN
             WRITE(6,'(a,i4,a,a,a,a,a)')                                &
                ' Mismatch of source names',jsrc,                       &
                ' read ',rdsource,' expected ',srcret(jsrc)
             CALL arpsstop("mismatch",1)
           END IF
           READ(4,'(a80)') srcret_full(jsrc)
           READ(4,*) qcmulret(jsrc)
           READ(4,'(a80)') header
            WRITE(6,'(1x,a,a,2a)') 'Retrieval std error for ',          &
                                srcret(jsrc),' - ',srcret_full(jsrc)
           WRITE(6,'(a,f5.1)') ' QC multiplier: ',qcmulret(jsrc)
           WRITE(6,'(1x,a)')                                            &
               '   k    hgt(m)  u(m/s)  v(m/s) pres(mb) temp(K)  RH(%)'
           DO ktab=1,nz_tab
              READ(4,*,iostat=istatus) hrtqsrc(ktab,jsrc),              &
                           qsrcret(3,ktab,jsrc), qsrcret(4,ktab,jsrc),  &
                           qsrcret(5,ktab,jsrc), qsrcret(1,ktab,jsrc),  &
                           qsrcret(2,ktab,jsrc)
              IF( istatus /= 0 ) EXIT
              WRITE(6,'(1x,i4,f10.2,5f8.2)') ktab,hrtqsrc(ktab,jsrc),   &
                              (qsrcret(ivar,ktab,jsrc),ivar=1,5)
              qsrcret(3,ktab,jsrc)=100.*qsrcret(3,ktab,jsrc)
              IF( cntl_var_rh2 == 1 ) THEN
                qsrcret(5,ktab,jsrc)=0.01*qsrcret(5,ktab,jsrc)
              ELSE
!                IF (hqback(ktab)<=10000) THEN
!                  qsrcret(5,ktab,jsrc)=0.002/EXP(hrtqsrc(ktab,jsrc)/3000)
!                ELSE
!                  qsrcret(5,ktab,jsrc)=1E-5
                  qsrcret(5,ktab,jsrc)=0.005
!                END IF
              ENDIF
           END DO
           nlvrttab(jsrc)=ktab-1
           CLOSE(4)
        ELSE
          DO k=1,npass
            iuseret(jsrc,k) = 0
          END DO
        END IF
      END IF
     END DO
!
!-----------------------------------------------------------------------
!
!  Change standard error to standard error variance by squaring
!  Calculate quality control thresholds
!
!-----------------------------------------------------------------------
!
    WRITE(*,*) 'using qback ', nvar_anx, nlvqback
    DO ktab=1,nlvqback
      DO ivar=1,nvar_anx
        qback(ivar,ktab)=qback(ivar,ktab)*qback(ivar,ktab)
      END DO
    END DO

    DO isrc=1,nsrc_sng
      DO ivar=1,nvar_anx
        qsrcsng(ivar,isrc)=qsrcsng(ivar,isrc)*qsrcsng(ivar,isrc)
        qcthrsng(ivar,isrc)=qcmulsng(isrc)*                             &
                         SQRT(qback(ivar,1)+qsrcsng(ivar,isrc))
        barqclim(ivar,isrc)=qcmulsng(isrc)*SQRT(qsrcsng(ivar,isrc))
      END DO
      qcthrsng(iqv,isrc)=min(qcthrsng(iqv,isrc),0.012)
      barqclim(iqv,isrc)=min(barqclim(iqv,isrc),0.012)
    END DO

    DO isrc=1,nsrc_ua
      DO ivar=1,nvar_anx
        qsrcmax=0.
        DO ktab=1,nlvuatab(isrc)
          qsrcua(ivar,ktab,isrc) = qsrcua(ivar,ktab,isrc)*qsrcua(ivar,ktab,isrc)
          qsrcmax=AMAX1(qsrcmax,qsrcua(ivar,ktab,isrc))
        END DO
        qcthrua(ivar,isrc)=qcmulua(isrc)*                               &
                           SQRT(qback(ivar,1)+qsrcmax)
      END DO
      qcthrua(iqv,isrc)=min(qcthrua(iqv,isrc),0.012)
    END DO

    DO isrc=1,nsrc_ret
      DO ivar=1,nvar_anx
        qsrcmax=0.
        DO ktab=1,nlvrttab(isrc)
          qsrcret(ivar,ktab,isrc) =                                     &
              qsrcret(ivar,ktab,isrc)*qsrcret(ivar,ktab,isrc)
          qsrcmax=AMAX1(qsrcmax,qsrcret(ivar,ktab,isrc))
        END DO
        qcthrret(ivar,isrc)=qcmulret(isrc)*                             &
                            SQRT(qback(ivar,1)+qsrcmax)
      END DO
      qcthrret(iqv,isrc)=min(qcthrret(iqv,isrc),0.012)
    END DO
  END IF

!
!  Broadcast read variables to all procs.  Some are dead, so we keep them
!  that way.  Others are only used by processor zero initially.
!
!  Also broadcast variables that may have been updated.
!

  CALL mpupdater(hqback, nz_tab)
  CALL mpupdater(qback, (nvar_anx*nz_tab))
  CALL mpupdatei(nlvqback, 1)

  CALL mpupdatec(srcsng,   (8*nsrc_sng))
  CALL mpupdater(qcthrsng, (nvar_anx*nsrc_sng))
  CALL mpupdater(qcthrua, (nvar_anx*nsrc_ua))
  CALL mpupdater(qcthrret, (nvar_anx*nsrc_ret))

  CALL mpupdatei(iusesng,(nsrc_sng+1)*mx_pass)
  CALL mpupdatei(iuseua, (nsrc_ua+1) *mx_pass)
  CALL mpupdatei(iuseret,(nsrc_ret+1)*mx_pass)

  CALL radarobs_init_site(myproc,mx_pass,qback(PTR_V,1),istatus)

  !CALL make_mpi_map(mpi_map, nmap, iproc, jproc, nx,ny)
  !CALL make_mpi_map(mpi_mapv,nmapv,iprocv,jprocv,nx,ny)

!
!-----------------------------------------------------------------------
!
!  Set cross-correlations between numerical categories.
!  Roughly 1=clear,2=some evidence of outflow,3=precip,4=conv precip
!  This could be read in as a table.
!
!-----------------------------------------------------------------------
!
  DO j=1,ncat
    DO i=1,ncat
      xcor(i,j)=1.0-(IABS(i-j))*0.25
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Initialize grids, forming first guess fields based on
!  the model initial option specified in the ARPS init file.
!
!-----------------------------------------------------------------------
!
  CALL set_acct(inputbg_acct)

  IF (modelopt== 2) THEN
    nstyps = 1

    !use_arps_metrics = .FALSE.     ! only set j1,j2,j3inv and mapfct meaning changes
    !use_arps_metrics = .TRUE.      ! Use all jacobian matrixs and do not use
                                    ! fnm,fnp,cf1,cf2,cf3 and dn etc.

    IF (myproc == 0) WRITE(*,'(/1x,a)') 'Calling initwrfgrd .....'
    CALL initwrfgrd(wrf_file,nx,ny,nz,nzsoil,                           &
                    x,y,z,zp,zpsoil,hterain,mapfct,xlat,xlon,           &
                    j1,j2,j3,aj3x,aj3y,aj3z,j3inv,                      &
                    u,v,w,pt,pres,phprt,qv,qscalar,                     &
                    qvbar,rhobar,phbar,                                 &
                    nqw,qnames_wrf, wmixr, p_top, psfc, varsfc,         &
                    mu,mub, dnw, dn, znw, znu,                          &
                    use_arps_metrics,fnm,fnp,cf1,cf2,cf3,               &
                    tem1,tem2,tem3,tem4,tem5,istatus)

    !CALL print3dnc_lg(200,'U',u,nx,ny,nz)
    ! It is rhobar coming out from "initwrfgrd"
    rhostr(:,:,:) = rhobar(:,:,:)*j3(:,:,:)

    pxb(:,:,:)    = pres(:,:,:)   ! Save background information
    qvxb(:,:,:)   = qv(:,:,:)     ! Save background information

  ELSE

    ALLOCATE(tem1soil(nx,ny,nzsoil),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:tem1soil")
    ALLOCATE(tem2soil(nx,ny,nzsoil),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:tem2soil")
    ALLOCATE(tem3soil(nx,ny,nzsoil),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:tem3soil")
    ALLOCATE(tem4soil(nx,ny,nzsoil),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:tem4soil")
    ALLOCATE(tem5soil(nx,ny,nzsoil),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:tem5soil")

    CALL initgrdvar(nx,ny,nz,nzsoil,1,nstyps,1,                         &
                  x,y,z,zp,zpsoil,hterain,mapfct,                       &
                  j1,j2,j3,j3soil,aj3x,aj3y,aj3z,j3inv,j3soilinv,       &
                  u,v,w,wcont,ptprt,pprt,qv,qscalar,tke,                &
                  udteb, udtwb, vdtnb, vdtsb,                           &
                  pdteb,pdtwb ,pdtnb ,pdtsb,                            &
                  trigs1,trigs2,ifax1,ifax2,                            &
                  wsave1,wsave2,vwork1,vwork2,                          &
                  ubar,vbar,tem1,ptbar,pbar,tem10,tem10,                &
                  rhostr,tem10,qvbar,ppi,csndsq,                        &
                  soiltyp,stypfrct,vegtyp,lai,roufns,veg,               &
                  tsoil,qsoil,wetcanp,snowdpth,qvsfc,                   &
                  ptcumsrc,qcumsrc,w0avg,nca,kfraincv,                  &
                  cldefi,xland,bmjraincv,                               &
                  raing,rainc,prcrate,exbcbuf,                          &
                  radfrc,radsw,rnflx,radswnet,radlwin,                  &
                  usflx,vsflx,ptsflx,qvsflx,                            &
                  tem1soil,tem2soil,tem3soil,tem4soil,tem5soil,         &
                  tem1,tem2,tem3,tem4,tem5,                             &
                  tem6,tem7,tem8,tem9)
    ! It is rhostr coming out from "initgrdvar"
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          rhobar(i,j,k)=rhostr(i,j,k)*j3inv(i,j,k)
        END DO
      END DO
    END DO

    IF ( hBal_opt==1 ) THEN

      CALL read_tendency(nx,ny,nz,istatus)
      IF (istatus /= 0) CALL ARPSSTOP('Reading error for time tendency.',1)

    END IF

    DEALLOCATE( tem1soil, tem2soil, tem3soil, tem4soil, tem5soil )

    !
    !  Deallocate some arrays needed only for initgrdvar
    !
    CALL deallocate_arpsArray_after_initgrdvar(istatus)

    pt(:,:,:)   = ptprt(:,:,:) + ptbar(:,:,:)
    pres(:,:,:) = pprt(:,:,:)  + pbar(:,:,:)
  END IF

  !CALL print3dnc_lg(100,'uanl',u,nx,ny,nz)
  !CALL print3dnc_lg(100,'vanl',v,nx,ny,nz)

  curtim = tstart   ! Added by G. Ge on 3/3/200
!
!-----------------------------------------------------------------------
!
!  Set location of scalar fields.
!
!-----------------------------------------------------------------------
!
  DO i=1,nx-1
    xs(i)=0.5*(x(i)+x(i+1))
  END DO
  xs(nx)=2.0*xs(nx-1)-xs(nx-2)
  DO j=1,ny-1
    ys(j)=0.5*(y(j)+y(j+1))
  END DO
  ys(ny)=2.0*ys(ny-1)-ys(ny-2)
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        zs(i,j,k)=0.5*(zp(i,j,k)+zp(i,j,k+1))
      END DO
    END DO
  END DO

  DO k=1,nz-1                   ! set nx
    DO j=1,ny-1
        zs(nx,j,k) = zs(nx-1,j,k)
    END DO
  END DO

  DO k=1,nz-1                   ! set ny
    DO i=1,nx
        zs(i,ny,k) = zs(i,ny-1,k)
    END DO
  END DO

  IF (mp_opt > 0) THEN
    !CALL acct_interrupt(mp_acct)
    CALL mpsendrecv2dew(zs, nx, ny, nz, ebc, wbc, 1, tem1)   ! set nx
    CALL mpsendrecv2dns(zs, nx, ny, nz, nbc, sbc, 2, tem1)   ! set ny
    !CALL acct_stop_inter
  END IF

  DO i=1,nx                     ! set nz
    DO j=1,ny
      zs(i,j,nz)=2.*zs(i,j,nz-1)-zs(i,j,nz-2)
    END DO
  END DO

  CALL xytoll(nx,ny,xs,ys,latgr,longr)

!
!-----------------------------------------------------------------------
!
! In the MPI world, some subroutines will need to know characteristics
! of the large grid.  To simply later subroutine calls (one call not two
! surrounded by IF/ELSE/ENDIF, everybody that always needs the large
! grid will use the large grid variables.  Large grid info is used by
! early decision making when determining what obs are needed.
!
!-----------------------------------------------------------------------
!

  IF (mp_opt > 0 ) THEN
     CALL mpimerge1dx(x,nx,xlg)
     CALL mpimerge1dy(y,ny,ylg)

     CALL mpupdater(xlg,nxlg)
     CALL mpupdater(ylg,nylg)

     CALL mpimerge1dx(xs,nx,xslg)
     CALL mpimerge1dy(ys,ny,yslg)

     CALL mpupdater(xslg,nxlg)
     CALL mpupdater(yslg,nylg)
  ELSE
     xlg  = x
     ylg  = y

     xslg = xs
     yslg = ys
  END IF

  CALL ctim2abss( year,month,day,hour,minute,second, i4timanx)
!
!-----------------------------------------------------------------------
!
!  Identify the background field correlation category for
!  each 2-d point.   This is based on precip rate, cumulus
!  parameterization switch, and surface relative humidity.
!
!-----------------------------------------------------------------------
!
  IF (modelopt == 1) THEN
    ! It use prcrate, which is available in the ARPS model only
    CALL setcat(nx,ny,nz,nscalar,ccatopt,zs,                            &
              pt,pres,qv,qscalar,prcrate,icatg)
  END IF

!-----------------------------------------------------------------------
!
!  analysis array for hybrid related method
!
!-----------------------------------------------------------------------
!
  IF (ibeta2 == 1) THEN

    CALL set_acct(ensemble_acct)

!-----------------------------------------------------------------------
!  Get ensemble dimension size, use iensmbl as control member
!-----------------------------------------------------------------------

    IF (iensmbl > 0) THEN
      l = iensmbl
    ELSE
      l = 1
    END IF
    !
    !  Set up the grdbas file name.
    !
    CALL get_input_dirname(ensdirname,l,dirfn)
    IF (myproc == 0) WRITE(*,'(1x,2a)')'Ensember dirfn = ',dirfn
    !
    !  Obtain the grid dimensions from history dump file.
    !
    IF (modelopt == 1) THEN   ! more work if nx_m /= nx
      WRITE(grdbasfn,'(2a,I3.3,a)')  TRIM(dirfn),'enf',l,'.hdfgrdbas'
      IF (myproc == 0) WRITE(6,'(1x,a)') 'Obtain the grid dimension from history dump file.'

      IF (myproc == 0) WRITE(6,'(1x,a)') 'Current assimilation time (sec) is 000000'

      WRITE(timdepfn,'(2a,I3.3,2a)') TRIM(dirfn),'enf',l,'.hdf000000'
      IF (myproc == 0) print*,'timdepfn=',timdepfn,grdbasfn

      CALL dtaread_init(grdbasfn,timdepfn,3,lvldbg,istatus)
      CALL dtaread_get_dim(nx_m,ny_m,nz_m,nzsoil_m,nstyps_m,istatus)
      IF( istatus /= 0 ) THEN
        PRINT*,'Problem occured when trying to get dimensions from data.'
        PRINT*,'Program stopped.'
        CALL arpsstop('ERROR: get_dims_from_data inside ensemble read.',1)
      END IF

      CALL dtaread_final( istatus )
    ELSE

      IF (ensfileopt == 1) THEN      ! WRF INPUT
        WRITE(timdepfn,'(2a)') TRIM(dirfn),'wrfinput_d01'
      ELSE IF (ensfileopt == 2) THEN ! WRF Forecast
        WRITE(timdepfn,'(3a)') TRIM(dirfn),'wrfout_d01_',timestring
      ELSE IF (ensfileopt == 3) THEN      ! WRF INPUT
        WRITE(timdepfn,'(2a)') TRIM(dirfn),'wrfinput_d01_ic'
      ELSE IF (ensfileopt == 4) THEN ! WRF Forecast from NEWS-e
        WRITE(timdepfn,'(4a,I0)') TRIM(dirfn),'wrffcst_d01_',timestring,'_',l
      ELSE  ! WRF OUTPUT
        IF (enstimeopt == 1) THEN
          WRITE(timdepfn,'(4a,I0)') TRIM(dirfn),'wrfout_d01_',timestring,'_',l
        ELSE IF (enstimeopt == 2) THEN
          WRITE(timdepfn,'(2a,I2.2,a,4a)') TRIM(dirfn),'wrfout_en',l,'_',   &
                                           ensintdate,'_',ensinttime
        END IF
      END IF
      IF (nproc_x_in > 1 .OR. nproc_y_in > 1) THEN
        WRITE(filename,'(2a)') TRIM(timdepfn),'_0000'
      ELSE
        WRITE(filename,'(a)') TRIM(timdepfn)
      END IF

      IF (myproc == 0) THEN  ! Works with one joined file only at present
        CALL get_wrf_domsize(TRIM(filename),timestring,nx_wrf,ny_wrf,     &
                             nz_wrf,nzsoil,nt,nxlg_wrf,nylg_wrf,istatus)
        nx_m = nxlg_wrf + 2
        ny_m = nylg_wrf + 2
        nz_m = nz_wrf + 2

        WRITE(*,'(1x,3(a,I0),/,7x,3(a,I0),a)')                          &
            'INFO: Use global ensemble dimensions nx_m   = ',nx_m,      &
                      ', ny_m   = ',ny_m,      ', nz_m   = ',nz_m,      &
            'based on WRF domain dimensions nx_wrf = ',nxlg_wrf,        &
                      ', ny_wrf = ',nylg_wrf,  ', nz_wrf = ',nz_wrf,'.'

        IF (MOD(nx_m-3,nproc_x) /= 0 .OR. MOD(ny_m-3,nproc_y) /= 0 ) THEN
          WRITE(*,'(1x,a,7x,2(a,I0),/,7x,2(a,I0),a)')                     &
              'ERROR: Inconsistent dimension and process number.',        &
              'nproc_x = ',nproc_x,', nproc_y = ',nproc_y,                &
              'nx_m    = ',nx_m,   ', ny_m    = ',ny_m,'.'
          istatus = -1
          CALL arpsstop('ERROR: Wrong number of processes.',1)
        END IF

        nx_m = (nx_m-3)/nproc_x+3
        ny_m = (ny_m-3)/nproc_y+3

        CALL get_wrf_grid( TRIM(filename),iproj_m,sclfct,trulat1_m,trulat2_m, &
                           trulon_m,ctrlat_m,ctrlon_m,dx_m,dy_m,mphyopt_m,istatus )

        ! Sanity checking
        IF(iproj_m /= iproj_wrf) THEN
          WRITE(6,'(1x,a,I0,2(a,I2.2),a)') 'ERROR: Ensember member (',l, &
             ') has different map projection (',iproj_m,                 &
             ') as the control run (',iproj_wrf,').'
          istatus = -2
        END IF

        IF(ABS(trulat1_m-trulat1) > 1.0E-2 .OR. ABS(trulat2_m-trulat2) > 1.0E-2 &
           .OR. ABS(trulon_m-trulon) > 1.0E-2) THEN
          WRITE(6,'(1x,a,I0,6(a,F8.2),a)') 'ERROR: Ensember member (',l,  &
             ') has different map projection parameters (',trulat1_m,',',trulat2_m,';',trulon_m, &
             ') as the control run (',trulat1,',',trulat2,';',trulon,').'
          istatus = -2
        END IF

        IF(ABS(ctrlat_m-ctrlat) > 1.0E-2 .OR. ABS(ctrlon_m-ctrlon) > 1.0E-2) THEN
          WRITE(6,'(1x,a,I0,4(a,F8.2),a)') 'ERROR: Ensember member (',l, &
             ') has different domain center (',ctrlat_m,',',ctrlon_m,    &
             ') as the control run (',ctrlat,',',ctrlon,').'
          istatus = -2
        END IF

        !IF(ABS(dx_m-dx) > 1.0E-2 .OR. ABS(dy_m-dy) > 1.0E-2) THEN
        !  WRITE(6,'(1x,a,I0,4(a,F8.2),a)') 'ERROR: Ensember member (',iensmbl, &
        !     ') has different domain resolution (',dx_m,',',dy_m,              &
        !     ') as the control run (',dx,',',dy,').'
        !  istatus = -2
        !END IF

      END IF
      CALL mpupdatei(istatus,1)
      IF (istatus /= 0) THEN
        CALL arpsstop('ERROR: Initialization with WRF ensemble data file.',1)
      END IF
      CALL mpupdatei(nx_m,1)
      CALL mpupdatei(ny_m,1)
      CALL mpupdatei(nz_m,1)
      CALL mpupdater(dx_m,1)
      CALL mpupdater(dy_m,1)

    END IF

    nscalar_m = nscalar

    CALL ens2ctrl_init(nx,ny,nz,dx,dy,zp,nx_m,ny_m,nz_m,dx_m,dy_m,istatus)
    IF (istatus /= 0) CALL arpsstop('ERROR in ens2ctrl_init.',1)

!-----------------------------------------------------------------------
!
!  Allocate Ensemble arrays
!
!-----------------------------------------------------------------------
!
    IF (myproc == 0) WRITE(6,'(1x,a)') 'Allocating arrays...'

    CALL allocate4DarrayE(modelopt,ref_use,nzsoil_m,nstyps_m,nscalar_m,istatus)
                                              ! allocate ensembles array

!-----------------------------------------------------------------------
!
!  Read background from ensemble forecast
!
!-----------------------------------------------------------------------
!

    IF (myproc == 0) WRITE(6,'(1x,a)') 'Reading ensemble background'

    CALL enbgread4var(nx,ny,nz,nx_m,ny_m,nz_m,tem1,tem2,tem3,tem4,istatus)

    IF (ref_use .AND. ref_enctrl) THEN
      CALL enrefcalc4var(nx,ny,nz,ref_mos_3d,                           &
                         tem1,tem2,tem3,tem4,tem5,tem6,istatus)
    END IF
    !print*,'after reading ensemble files'

  END IF  !end of ibeta2==1

!
!-----------------------------------------------------------------------
!
!  Load "background fields" for analysis
!  Note, analysis is done at staggered points for U, V, W etc.
!
!-----------------------------------------------------------------------
!
  anx = 0.0

  anx(:,:,:,PTR_P ) = pres(:,:,:)
  anx(:,:,:,PTR_PT) = pt(:,:,:)
  anx(:,:,:,PTR_QV) = qv(:,:,:)
  anx(:,:,:,PTR_U ) = u(:,:,:)  ! 0.5*(u(i,j,k)+u(i+1,j,k))
  anx(:,:,:,PTR_V ) = v(:,:,:)  ! 0.5*(v(i,j,k)+v(i,j+1,k))
  anx(:,:,:,PTR_W)  = w(:,:,:)  ! 0.5*(w(i,j,k)+w(i,j,k+1))

  IF (ref_use .OR. hydro_opt==1 .OR. satcwpobs==1) THEN
    DO nq = 1, nscalarq
      anx(:,:,:, PTR_LN+nq) = qscalar(:,:,:,nq)
    END DO
  END IF

  !CALL print3dnc_lc(200,'uanl',anx(:,:,:,PTR_U), nx,ny,nz,2,2)
  !CALL print3dnc_lc(200,'tanl',anx(:,:,:,PTR_PT),nx,ny,nz,2,2)
  !
  !CALL arpsstop(' ',0)
  !CALL print3dnc_lg(200,'uanl',anx(:,:,:,PTR_U) ,nx,ny,nz)
  !CALL print3dnc_lg(200,'vanl',anx(:,:,:,PTR_V) ,nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  convert potential temperature to temperature
!
!-----------------------------------------------------------------------
!
  tk(:,:,:)= anx(:,:,:,PTR_PT)*(anx(:,:,:,PTR_P)/p0)**rddcp

!
!-----------------------------------------------------------------------
!
!  convert specific humidity to relative humidity ( modified by Ming Hu)
!
!-----------------------------------------------------------------------
!
  IF( cntl_var_rh2 == 1 ) THEN

    CALL getqvs(nx,ny,nz, 1,nx-1,1,ny-1,1,nz-1, anx(1,1,1,PTR_P),tk,qv)

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          anx(i,j,k,PTR_QV)= anx(i,j,k,PTR_QV)/qv(i,j,k)
          IF( anx(i,j,k,PTR_QV) > 1.0 ) anx(i,j,k,PTR_QV)=1.0
          IF( anx(i,j,k,PTR_QV) < 0.0 ) anx(i,j,k,PTR_QV)=0.0
        END DO
      END DO
    END DO

  ENDIF

  IF (mp_opt > 0) THEN
    DO k=1,nvar_anx
      CALL mpsendrecv2dew(anx(1,1,1,k),nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(anx(1,1,1,k),nx,ny,nz,nbc,sbc,0,tem1)
    END DO
  END IF

!-----------------------------------------------------------------------
!
!  Save background fields on staggered grid in the increment
!  arrays.
!
!  PROBABLY NEEDS MPI WORK!
!
!-----------------------------------------------------------------------
!
  IF(incrdmp > 0) THEN
    CALL iau_initialization(incrdmp,nx,ny,nz,istatus)
    CALL incrsave(nx,ny,nz,u,v,w,pres,pt,qv,qscalar,istatus)
  END IF

  CALL set_acct(inputobs_acct)

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Read and preprocess the single level data, all done on processor 0.
!  Broadcast the data to the rest of the ADAS world.
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  nobsng=0
  IF (nsngfil > 0) THEN

    maxsuf=LEN(suffix)
    mxroot=LEN(froot)
    iqclist = 0
    iqcsave = 0

    IF (myproc == 0) THEN
      DO ifile=1,nsngfil
        WRITE(6,'(1x,a,i4,2a)') 'Processing file ', ifile, ' ',TRIM(sngfname(ifile))
        lensuf = INDEX(sngfname(ifile),'prepbufr',.TRUE.)
        IF (lensuf > 0) THEN    ! PrepBUFR files
          PRINT *, 'Calling prepbufrobs'
          CALL prepbufrobs(ntime,mx_sng,                                &
              nvar_sng,nvar_anx,nsrc_sng,nobsng,ipres,iptmp,iqv,        &
              sngfname(ifile),sngtmchk(ifile),blackfil,                 &
              var_sfc,var_anx,srcsng,qsrcsng,                           &
              rmiss,iqspr,sprdist,nobsrd,timesng,                       &
              stnsng,csrcsng,isrcsng,latsng,lonsng,hgtsng,xsng,ysng,    &
              nxlg,nylg,xslg,yslg,                                      &
              wx,kloud,idp3,store_emv,store_hgt,store_amt,              &
              obrdsng,obsng,qualrdsng,qobsng,qualsng,                   &
              rngsq,klim,wlim,qcthrsng,barqclim,                        &
              knt,wgtsum,zsum,sqsum,iqclist,iqcsave,istatus)

        ELSE

          lenfnm=LEN_TRIM(sngfname(ifile))
          CALL exsufx(sngfname(ifile),lenfnm,suffix,maxsuf,dotloc,lensuf)


          IF(lensuf == 3 .AND. suffix(1:3) == 'lso') THEN

            CALL exfroot(sngfname(ifile),lenfnm,froot,mxroot,lenroot)
            WRITE(qclist,'(a,a)') froot(1:lenroot),'.sqc'
            WRITE(qcsave,'(a,a)') froot(1:lenroot),'.lsq'
!
!-----------------------------------------------------------------------
!
!  Open the files for listing QC info
!  To suppress listing set the unit numbers to zero
!
!-----------------------------------------------------------------------
!

            CALL getunit(iqclist)
            PRINT *, 'Opening qclist: ', TRIM(qclist)
            OPEN(iqclist,IOSTAT=ios,FILE=trim(qclist),STATUS='unknown')
            IF(ios /= 0) iqclist=0
            CALL getunit(iqcsave)
            PRINT *, 'Opening qclist: ', TRIM(qcsave)
            OPEN(iqcsave,IOSTAT=ios,FILE=trim(qcsave),STATUS='unknown')
            IF(ios /= 0) iqcsave=0

!
!-----------------------------------------------------------------------
!
!  Read surface data, QC and convert units.
!
!-----------------------------------------------------------------------
!

            rngsq=sfcqcrng*sfcqcrng
            PRINT *, 'Calling prepsfcobs'
            CALL prepsfcobs(ntime,mx_sng,                                 &
                nvar_sng,nvar_anx,nsrc_sng,nobsng,ipres,iptmp,iqv,        &
                sngfname(ifile),sngtmchk(ifile),blackfil,                 &
                var_sfc,var_anx,srcsng,qsrcsng,                           &
                rmiss,iqspr,sprdist,nobsrd,timesng,                       &
                stnsng,csrcsng,isrcsng,latsng,lonsng,hgtsng,xsng,ysng,    &
                nxlg,nylg,xslg,yslg,                                      &
                wx,kloud,idp3,store_emv,store_hgt,store_amt,              &
                obrdsng,obsng,qualrdsng,qobsng,qualsng,                   &
                rngsq,klim,wlim,qcthrsng,barqclim,                        &
                knt,wgtsum,zsum,sqsum,iqclist,iqcsave,istatus)

            IF(iqclist /= 0) THEN
              CLOSE(iqclist)
              CALL retunit(iqclist)
            END IF
            IF(iqcsave /= 0) THEN
              CLOSE(iqcsave)
              CALL retunit(iqcsave)
            END IF

          ELSE IF(lensuf == 2 .AND. suffix(1:2) == 'nc') THEN
          !
          ! METAR observations in netCDF format
          !
          ! simulate processing for lso files
!
!-----------------------------------------------------------------------
!
!  Open the files for listing QC info
!  To suppress listing set the unit numbers to zero
!
!-----------------------------------------------------------------------
!
            CALL exfroot(sngfname(ifile),lenfnm,froot,mxroot,lenroot)
            WRITE(qclist,'(a,a)') froot(1:lenroot),'.sqc'
            WRITE(qcsave,'(a,a)') froot(1:lenroot),'.lsq'

            CALL getunit(iqclist)
            PRINT *, 'Opening qclist: ', TRIM(qclist)
            OPEN(iqclist,IOSTAT=ios,FILE=trim(qclist),STATUS='unknown')
            IF(ios /= 0) iqclist=0
            CALL getunit(iqcsave)
            PRINT *, 'Opening qclist: ', TRIM(qcsave)
            OPEN(iqcsave,IOSTAT=ios,FILE=trim(qcsave),STATUS='unknown')
            IF(ios /= 0) iqcsave=0

!
!-----------------------------------------------------------------------
!
!  Read surface data, QC and convert units.
!
!-----------------------------------------------------------------------
!

            rngsq=sfcqcrng*sfcqcrng
            PRINT *, 'Calling prepmetarobs'
            CALL prepmetarobs(ntime,mx_sng,                               &
                nvar_sng,nvar_anx,nsrc_sng,nobsng,ipres,iptmp,iqv,        &
                sngfname(ifile),sngtmchk(ifile),blackfil,                 &
                var_sfc,var_anx,srcsng,qsrcsng,                           &
                rmiss,iqspr,sprdist,nobsrd,timesng,                       &
                stnsng,csrcsng,isrcsng,latsng,lonsng,hgtsng,xsng,ysng,    &
                nxlg,nylg,xslg,yslg,                                      &
                wx,kloud,idp3,store_emv,store_hgt,store_amt,              &
                obrdsng,obsng,qualrdsng,qobsng,qualsng,                   &
                rngsq,klim,wlim,qcthrsng,barqclim,                        &
                knt,wgtsum,zsum,sqsum,iqclist,iqcsave,istatus)

            IF(iqclist /= 0) THEN
              CLOSE(iqclist)
              CALL retunit(iqclist)
            END IF
            IF(iqcsave /= 0) THEN
              CLOSE(iqcsave)
              CALL retunit(iqcsave)
            END IF

          ELSE  ! other than 'lso' file or 'METAR' file

!
!-----------------------------------------------------------------------
!
!  Read other single-level data and convert units.  Note that we are
!  using the global domain here, even if we're in MPI mode, as only
!  processor 0 is doing this.
!
!-----------------------------------------------------------------------
!
            PRINT *, 'Calling prepsglobs'
            CALL prepsglobs(mx_sng,ntime,srcsng,nsrc_sng,                   &
                  sngfname(ifile),stnsng,latsng,lonsng,xsng,ysng,           &
                  hgtsng,obsng,qobsng,qualsng,isrcsng,qsrcsng,              &
                  csrcsng,nxlg,nylg,nz,nvar_anx,anx,xlg,ylg,xslg,yslg,zp,   &
                  tem1,tem2,tem3,tem4,tem5,tem6,                            &
                  rmiss,nobsng,istatus)
          END IF
        END IF  ! PrepBUFR files

      END DO
    END IF ! myproc == 0
!
!-----------------------------------------------------------------------
!
!  MPI issues.  Broadcast the number of data points, then the data.  If
!  there are no data values, we don't waste anyone's time.
!
!-----------------------------------------------------------------------
!
    CALL mpupdatei(nobsng,1)
    ALLOCATE(item1(nobsng),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:item1:nobsng")
    ALLOCATE(ksng(nprocs),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:ksng:nprocs")

    IF (mp_opt > 0) THEN
  !   CALL mpupdatei(nobsng,1)
      IF (nobsng > 0) THEN
        CALL mpupdatei(timesng,mx_sng*ntime)
        CALL mpupdatec(stnsng,mx_sng*ntime*5)
        CALL mpupdatec(csrcsng,mx_sng*ntime*8)
        CALL mpupdatei(isrcsng,mx_sng)
        CALL mpupdater(latsng,mx_sng*ntime)
        CALL mpupdater(lonsng,mx_sng*ntime)
        CALL mpupdater(hgtsng,mx_sng*ntime)
        CALL mpupdater(xsng,mx_sng)
        CALL mpupdater(ysng,mx_sng)
        CALL mpupdatec(wx,mx_sng*ntime*8)
        CALL mpupdatei(kloud,mx_sng*ntime)
        CALL mpupdatei(idp3,mx_sng*ntime)
        CALL mpupdatec(store_emv,mx_sng*5*ntime)
        CALL mpupdater(store_hgt,mx_sng*5*ntime)
        CALL mpupdatec(store_amt,mx_sng*5*ntime*4)
        CALL mpupdater(obrdsng,mx_sng*nvar_sng*ntime)
        CALL mpupdater(obsng,nvar_anx*mx_sng)
        CALL mpupdater(qobsng,nvar_anx*mx_sng)
        CALL mpupdatei(qualsng,nvar_anx*mx_sng)
        CALL mpupdater(knt,nvar_anx*nz)
        CALL mpupdater(wgtsum,nvar_anx*nz)
        CALL mpupdater(zsum,nvar_anx*nz)

!
!-----------------------------------------------------------------------
!      We need to know which processor "owns" which obs, so they can be
!      consulted on an "as needed" basis.
!-----------------------------------------------------------------------
!

        CALL mpiprocess(nobsng,indexsng,usesng,nprocs,ksng,ksngmax,       &
                        isrcsng,item1,nx,ny,xsng,ysng,xs,ys,0)

        !CALL data_filt(nobsng,isrcsng,indexsng,nmap,mpi_map)

!
!-----------------------------------------------------------------------
!
!  Although we're similar to PREPSFCGLOBS, and need to do one final task
!  (compute heights), we now use the local grid variables, not the
!  global.
!
!-----------------------------------------------------------------------
!
        CALL prepsglmdcrs_sm(mx_sng,ntime,latsng,lonsng,                  &
                             xsng,ysng,hgtsng,csrcsng,                    &
                             nx,ny,nz,nvar_anx,anx,x,y,xs,ys,zp,          &
                             tem1,tem2,tem3,tem4,tem5,tem6,               &
                             rmiss,nobsng,indexsng,istatus)

      END IF  ! nobsng > 0

    ELSE
      CALL mpiprocess(nobsng,indexsng,usesng,nprocs,ksng,ksngmax,         &
                      isrcsng,item1,nx,ny,xsng,ysng,xs,ys,0)

    END IF  ! mp_opt > 0

    DEALLOCATE (item1)
!
!-----------------------------------------------------------------------
!
!  Calculate initial obs differences for single-level data
!
!-----------------------------------------------------------------------
!

    IF (myproc == 0 ) PRINT *, 'Calling grdtosng'
    CALL grdtosng(nx,ny,nz,nz_tab,mx_sng,nvar_anx,nobsng,               &
                  x,y,xs,ys,zp,icatg,anx,qback,hqback,nlvqback,         &
                  tem1,tem2,tem3,tem4,tem5,tem6,                        &
                  stnsng,isrcsng,icatsng,hgtsng,xsng,ysng,              &
                  obsng,qobsng,qualsng,                                 &
                  odifsng,oanxsng,thesng,trnsng,indexsng,srcsng,        &
                  nsrc_sng)
    IF (mp_opt > 0) CALL mpbarrier

  END IF  ! do SNG data processing

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Read and preprocess the lightning data.
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! Force reading lightning as the above monkey business does not work...
  IF (nlgtfil > 0) THEN

    CALL allocate_lightning(nx,ny,nz,istatus)

!     print*,'nx,ny,nz',nx,ny,nz

!     CALL mpupdater(qsrcsng(5,8),1)  ! ? -WYH

     qobsng(5,:)=0.00001 ! qsrcsng(5,8)
!     print*,'qsrcsng(5,8)',qsrcsng(5,8)

    ! COMPUTE SYNTHETIC QVOBS IF THER IS NONZERO LIGHTNING
    ! SEEN BY ALL PROCESSES BECAUSE gridlight_global is tiled in there

    CALL read_lightning_file(myproc,nx,ny,nxlg,nylg,dx,xlat,xlon,istatus)

    CALL compute_lightning(myproc,nproc_x,nproc_y,nx,ny,nz,dx,nscalar,zs, &
                  anx(:,:,:,PTR_P),anx(:,:,:,PTR_PT),anx(:,:,:,PTR_QV), &
                  qscalar,istatus)
  END IF
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Read aeri temperature and qv data
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!

  IF (aeri_opt == 1) THEN
     CALL allocateAERIobs(istatus)
     CALL readAERI(myproc,nx,ny,nz,xs,ys,zs,istatus) !JJ
  END IF

!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Read upper-air data, QC and convert units
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
  nobsua = 0
  IF (nuafil > 0) THEN

    IF (myproc == 0) THEN
      PRINT *, 'Calling prepuaobs'
      CALL prepuaobs(nx,ny,nz,nvar_anx,                                  &
                     nz_ua,mx_ua,nz_tab,nsrc_ua,mx_ua_file,              &
                     anx,x,y,xs,ys,zp,tem1,tem2,tem3,tem4,tem5,tem6,tem7,&
                     nuafil,uafname,srcua,                               &
                     stnua,elevua,xua,yua,hgtua,obsua,                   &
                     qsrcua,huaqsrc,nlvuatab,                            &
                     qobsua,qualua,isrcua,nlevsua,                       &
                     rmiss,nobsua,istatus)
    END IF

!
!-----------------------------------------------------------------------
!
!  MPI issues.  Broadcast the number of data points, then the data.  If
!  there are no data values, we don't waste anyone's time.
!
!-----------------------------------------------------------------------
!

    CALL mpupdatei(nobsua,1)

    ALLOCATE(item1(nobsua),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:item1:nobsua")
    ALLOCATE(kua(nprocs),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:kua:nprocs")

    IF (mp_opt > 0) THEN
      IF (nobsua > 0) THEN
        CALL mpupdatec(stnua,mx_ua*5)
        CALL mpupdater(elevua,mx_ua)
        CALL mpupdater(xua,mx_ua)
        CALL mpupdater(yua,mx_ua)
        CALL mpupdater(hgtua,nz_ua*mx_ua)
        CALL mpupdater(obsua,nvar_anx*nz_ua*mx_ua)
        CALL mpupdater(qobsua,nvar_anx*nz_ua*mx_ua)
        CALL mpupdatei(nlvuatab,nsrc_ua)
        CALL mpupdatei(qualua,nvar_anx*nz_ua*mx_ua)
        CALL mpupdatei(isrcua,mx_ua)
        CALL mpupdatei(nlevsua,mx_ua)

!
!-----------------------------------------------------------------------
!      We need to know which processor "owns" which obs, so they can be
!      consulted on an "as needed" basis.
!-----------------------------------------------------------------------
!

        CALL mpiprocess(nobsua,indexua,useua,nprocs,kua,kuamax,           &
            isrcua,item1,nx,ny,xua,yua,xs,ys,0)

        !CALL data_filt(nobsua,isrcua,indexua,nmap,mpi_map)

      END IF
    ELSE
      CALL mpiprocess(nobsua,indexua,useua,nprocs,kua,kuamax,             &
                      isrcua,item1,nx,ny,xua,yua,xs,ys,0)

    END IF

    DEALLOCATE (item1)
!
!-----------------------------------------------------------------------
!
!  Calculate initial obs differences for upper-air data
!
!-----------------------------------------------------------------------
!
    IF (myproc == 0) PRINT *, 'Calling grdtoua'

    CALL grdtoua(nx,ny,nz,nz_tab,nz_ua,mx_ua,nvar_anx,nobsua,           &
                x,y,xs,ys,zp,anx,qback,hqback,nlvqback,                 &
                tem1,tem2,tem3,tem4,tem5,tem6,                            &
                stnua,isrcua,elevua,xua,yua,hgtua,                        &
                obsua,qobsua,qualua,nlevsua,                              &
                odifua,oanxua,theua,trnua,indexua)
    IF (mp_opt > 0) CALL mpbarrier

  END IF  ! do UA data processing
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Read reflectivity data and do interpolation to the model grid
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!

  !IF (myproc == 0) PRINT *, 'Allocating full ref_mos_3d',nx,ny,nz
  ALLOCATE(ref_mos_3d(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:ref_mos_3d")
  ALLOCATE(rhv_mos_3d(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:rhv_mos_3d")
  ALLOCATE(zdr_mos_3d(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:zdr_mos_3d")
  ALLOCATE(kdp_mos_3d(nx,ny,nz),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:kdp_mos_3d")

  ref_mos_3d = dpmiss
  rhv_mos_3d = dpmiss
  zdr_mos_3d = dpmiss
  kdp_mos_3d = dpmiss

  istat_radar = 0          ! need to process radar mosaic in radar data block
  mosaicopt   = 1          ! We need to construct radar mosaic from radar data file

  IF (refsrc == 4) THEN    ! Stage IV precipitation dataset
    CALL read_intrp_stage4(refile,reffmt,iboxs,iboxe,jboxs,jboxe,       &
                     reforder,nx,ny,nz,xs,ys,zs,ref_mos_3d,timstr,istatus)
    refout = refout+200    ! Ensure the program will stop

  ELSE IF (refsrc > 0) THEN
    !CALL print3dnc_lg(200,'radref',ref_mos_3d,nx,ny,nz)
    CALL read_reflectivity(refsrc,refile,reffmt,iboxs,iboxe,jboxs,jboxe,&
                           reforder,nx,ny,nz,xs,ys,zs,ref_mos_3d,timstr,istatus)
    IF (istatus == 0) THEN
      istat_radar = 1
      mosaicopt = 0         ! do not process radar mosaic in subroutine prepradar
      IF (myproc == 0) WRITE(*,'(1x,a)')                                &
                'Radar mosaic was read in from MRMS reflectivity files.'
    END IF
    !print *, 'MRMS: ref_mos_3d = ',MAXVAL(ref_mos_3d), MINVAL(ref_mos_3d)
    !CALL print3dnc_lg(200,'mrms',ref_mos_3d,nx,ny,nz)
  END IF
  !IF (myproc == 0) WRITE(*,*) 'refsrc = ',refsrc,' : ref_mos_3d = ',MAXVAL(ref_mos_3d), MINVAL(ref_mos_3d)
  !CALL arpsstop('',0)

  IF (mp_opt > 0) THEN
    CALL mpsendrecv2dew(ref_mos_3d,nx,ny,nz,ebc,wbc,0,tem1)
    CALL mpsendrecv2dns(ref_mos_3d,nx,ny,nz,nbc,sbc,0,tem1)
  END IF

  IF (refout > 1) THEN
    CALL write_reflectivity(MOD(refout,200),dirname,refsrc,(reffmt>200),timstr,&
                            nx,ny,nz,ref_mos_3d,xlat,xlon,zs,istatus)

    IF (refout > 200) CALL arpsstop('INFO: output reflectivity/total precip termination.',0)
  END IF

!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Read radar data, unfold and convert units
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!

  IF (nradfil > 0) THEN

    IF (myproc == 0) PRINT *, 'Calling prepradar'
!
!-----------------------------------------------------------------------
!      We need to know which processor "owns" which obs, so they can be
!      consulted on an "as needed" basis.  Unlike non-radar data, the
!      processing is different, and is done in "prepradar" since all the
!      bookkeeping we need is done there in temporary arrays.  Since the
!      "krad" array is small, and gets passed around a few times, we'll
!      always allocate it.
!-----------------------------------------------------------------------
!
    raduvobs = 0
    IF (ANY(vrob_opt > 0)) raduvobs = 1
    CALL prepradar(nx,ny,nz,nz_tab,nvar_anx,mx_pass,                    &
               raduvobs,radrhobs,radistride,radkstride,mosaicopt,       &
               npass,refrh,rhradobs,                                    &
               x,y,zp,xs,ys,zs,hterain,latgr,longr,anx,qback,hqback,nlvqback,&
               ref_mos_3d,rhv_mos_3d,zdr_mos_3d,kdp_mos_3d,             &
               tem1,istat_radar,istatus)
    IF (istatus == 99) THEN
      WRITE(*,'(/,1x,a,/)') 'WARNING: no valid radar data read. Reset vrob_opt = 0.'
      vrob_opt(:) = 0
      nradfil = 0
      GOTO 500
    END IF

    ALLOCATE(item1(ncolrad),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:item1:ncolrad")

    ALLOCATE(krad(nprocs),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:rad:nprocs")

    IF ( mp_opt > 0 ) THEN

      IF ( ncolrad_total > 0 ) THEN
!
!-----------------------------------------------------------------------
!      Some data needs to be collected and distributed.
!-----------------------------------------------------------------------
!

        ! note that the array indexrad is redefined from above
        CALL mpiprocess_complex(ncolrad_total,ncolrad,indexrad,oindexrad,   &
                          userad, nprocs,krad,kradmax,isrcradr,item1,       &
                          nx,ny,xradc,yradc,xs,ys,1,istatus)

        !CALL data_filt(ncolrad,irad,indexrad,nmapv,mpi_mapv)

      END IF  ! there are observations

    ELSE  ! set array indexrad & userad for 3DVAR
      CALL mpiprocess(ncolrad,indexrad,userad,nprocs,krad,kradmax,      &
                      isrcradr,item1,nx,ny,xradc,yradc,xs,ys,1)

    END IF

    DEALLOCATE(item1,krad)

  END IF  ! done RAD data preprocessing

  500 CONTINUE

!
!#######################################################################
!
!  Process the 3D radar mosaic and stop the program as requested
!
!  Note that prepradar filter ref_mos_3d with "thresh_ref".
!
!#######################################################################
!
  !IF (refsrc == 4) THEN    ! Stage IV precipitation dataset
  !  CALL read_intrp_stage4(refile,reffmt,iboxs,iboxe,jboxs,jboxe,       &
  !                   reforder,nx,ny,nz,xs,ys,zs,ref_mos_3d,timstr,istatus)
  !  refout = refout+200    ! Ensure the program will stop
  !
  !ELSE IF (refsrc > 0) THEN
  !  !CALL print3dnc_lg(200,'radref',ref_mos_3d,nx,ny,nz)
  !  CALL read_reflectivity(refsrc,refile,reffmt,iboxs,iboxe,jboxs,jboxe,&
  !                         reforder,nx,ny,nz,xs,ys,zs,ref_mos_3d,timstr,istatus)
  !  IF (istatus == 0) THEN
  !    istat_radar = 1
  !    mosaicopt = 0         ! do not process radar mosaic in subroutine prepradar
  !    IF (myproc == 0) WRITE(*,'(1x,a)')                                &
  !              'Radar mosaic was read in from MRMS reflectivity files.'
  !  END IF
  !  !print *, 'MRMS: ref_mos_3d = ',MAXVAL(ref_mos_3d), MINVAL(ref_mos_3d)
  !  !CALL print3dnc_lg(200,'mrms',ref_mos_3d,nx,ny,nz)
  !END IF
  !!IF (myproc == 0) WRITE(*,*) 'refsrc = ',refsrc,' : ref_mos_3d = ',MAXVAL(ref_mos_3d), MINVAL(ref_mos_3d)
  !!CALL arpsstop('',0)
  !
  !IF (mp_opt > 0) THEN
  !  CALL mpsendrecv2dew(ref_mos_3d,nx,ny,nz,ebc,wbc,0,tem1)
  !  CALL mpsendrecv2dns(ref_mos_3d,nx,ny,nz,nbc,sbc,0,tem1)
  !END IF

  !IF (refout > 1) THEN
  !  CALL write_reflectivity(MOD(refout,200),dirname,refsrc,(reffmt>200),timstr,&
  !                          nx,ny,nz,ref_mos_3d,xlat,xlon,zs,istatus)
  !
  !  IF (refout > 200) CALL arpsstop('INFO: output reflectivity/total precip termination.',0)
  !END IF

  ALLOCATE(ref_comp_2d(nx,ny),STAT=istatus)
  CALL check_alloc_status(istatus, "news3dvar:ref_comp_2d")

  ref_comp_2d = MAXVAL(ref_mos_3d,3)

  !DO j = 1, ny
  !  DO i = 1, nx
  !    ref_comp_2d(i,j)=MAXVAL(ref_mos_3d(i,j,:))
  !  END DO
  !END DO
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Read conventional observations from prepbufr
!  Multilevel observation may have problems! Need more test!
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
  IF (nconvfil > 0) THEN

    CALL prep_convtbl(p_top,istatus)
    IF (istatus /= 0 ) THEN
      IF (myproc == 0) WRITE(*,'(1x,a)') 'WARNING: ERROR reading convinfo/errtable file. Continue without conventional data assimilation...'
      nconvfil = 0
      iuseconv = 0
      CALL dealloc_convobs()
      GOTO 550
    END IF

    IF (myproc==0) PRINT*,'Calling prepconvobs'
    CALL prepconvobs(timestring,nx,ny,xs,ys,nxlg,nylg,xslg,yslg,istatus)

    IF (istatus /= 0 ) THEN
      IF (myproc == 0) WRITE(*,'(1x,a)') 'WARNING: ERROR reading Prepbufr file. Continue without conventional data assimilation...'
      nconvfil = 0
      iuseconv = 0
      CALL dealloc_convobs()
      GOTO 550
    END IF

    IF (myproc==0) PRINT*,'Calling grdtoconv'
    CALL grdtoconv(nx, ny, nz, xs, ys, nvar_anx, anx, psfc, varsfc)

    IF (mp_opt > 0) CALL mpbarrier

  END IF ! done conventional observation processing

  550 CONTINUE
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Read satellite cloud water path data
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
  IF (ncwpfil > 0) THEN

    IF (myproc==0) PRINT*,'Calling prepcwpobs'
    CALL prepcwpobs(nx,ny,nz,nxlg,nylg,xs,ys,idealCase_opt,istatus)
    !IF (mp_opt > 0) CALL mpbarrier
    IF (istatus /= 0 ) THEN
      IF (myproc == 0) WRITE(*,'(1x,a)') 'WARNING: ERROR reading CWP file. Continue without CWP assimilation...'
      ncwpfil = 0
      GOTO 600
    END IF

    IF (myproc==0) PRINT*,'Calling grdtocwp'
    CALL grdtocwp(nx,ny,nz,anx(:,:,:,PTR_P),tk,xs,ys,                   &
                  qscalar(:,:,:,P_QC),qscalar(:,:,:,P_QI),ref_comp_2d,istatus)
    IF (pseudo_opt==1) THEN
      CALL grdtohydro(nx,ny,nz,xs,ys,                                   &
                     qscalar(:,:,:,P_QC),qscalar(:,:,:,P_QC),istatus)
    END IF

    IF (mp_opt > 0) CALL mpbarrier

  END IF ! done CWP data processing

  600 CONTINUE
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Read satellite total precipitable water
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
  IF (ntpwfil > 0) THEN
    IF (myproc==0) PRINT*,'Calling preptpwobs'
    CALL preptpwobs(nx,ny,nz,nxlg,nylg,xs,ys,idealCase_opt,istatus)
    !IF (mp_opt > 0) CALL mpbarrier

    IF (myproc==0) PRINT*,'Calling grdtotpw'
    CALL grdtotpw(nx,ny,nz,zp,xs,ys,qv,rhobar,anx(:,:,:,PTR_P),psfc,p_top,idealCase_opt,tem1,istatus)
    IF (mp_opt > 0) CALL mpbarrier

  END IF ! done TPW data processing

!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  Read retrieval data.
!
!  Retrieval code has *NOT* been tested, so it *WILL* not be MPI ready!
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
  ncolret = 0
  IF (nretfil > 0) THEN

    IF (myproc == 0) PRINT *, 'Calling prepretr'
    CALL prepretr(nx,ny,nz,nvar_anx,                                    &
                  nz_ret,mx_ret,mx_colret,nz_tab,nsrc_ret,              &
                  nretfil,retfname,                                     &
                  isrcret,srcret,nlvrttab,qsrcret,hrtqsrc,              &
                  stnret,latret,lonret,elvret,                          &
                  latretc,lonretc,xretc,yretc,iret,nlevret,             &
                  hgtretc,obsret,qrret,qobsret,qualret,                 &
                  rmiss,ncolret,tem1,istatus)

!
!-----------------------------------------------------------------------
!
!  Now get the retrieval data so that each processor only
!  contains the data it needs for its local domain plus the radius of
!  influence, where applicable.
!
!  Not guaranteed MPI ready.
!
!-----------------------------------------------------------------------
!

    IF (myproc == 0) PRINT *, 'Calling grdtoret'
    CALL grdtoret(nx,ny,nz,nz_tab,                                      &
                  nz_ret,mx_ret,mx_colret,nvar_anx,ncolret,             &
                  x,y,xs,ys,zp,anx,qback,hqback,nlvqback,               &
                  tem1,tem2,tem3,tem4,tem5,tem6,                        &
                  stnret,iret,xretc,yretc,hgtretc,                      &
                  obsret,qobsret,qualret,nlevret,                       &
                  odifret,oanxret,theretc,trnretc)
    IF (mp_opt > 0) CALL mpbarrier

  END IF ! do RET data processing
!
!-----------------------------------------------------------------------
!
! convert observation from q to RH  !Ming Hu
!-----------------------------------------------------------------------
!
  IF( cntl_var_rh2 == 1 ) THEN
    CALL qv2rhobs(nvar_anx,mx_sng,nobsng,obsng,qualsng,                 &
                  nz_ua,mx_ua,nobsua,obsua,qualua,nlevsua,              &
                  nz_ret,mx_colret,ncolret,obsret,qualret,nlevret)
  ENDIF
!
!-----------------------------------------------------------------------
!
!  Quality-control observation differences
!
!-----------------------------------------------------------------------
!
  IF( idealCase_opt==0 ) THEN

    IF (myproc == 0) PRINT *, 'Calling qcdiff'
    CALL qcdiff(nvar_anx,nrad_anx,nvar_rad,mx_sng,nsrc_sng,             &
              indexsng,indexua,indexrad,indexret,                       &
              usesng,useua,userad,                                      &
              nz_ua,mx_ua,nsrc_ua,                                      &
              nz_radar,mx_rad,mx_colrad,nsrc_rad,                       &
              nz_ret,mx_ret,mx_colret,nsrc_ret,                         &
              nobsng,nobsua,ncolrad,ncolret,var_anx,                    &
              stnsng,isrcsng,hgtsng,obsng,oanxsng,odifsng,              &
              qcthrsng,qualsng,                                         &
              stnua,isrcua,hgtua,obsua,oanxua,odifua,                   &
              qcthrua,qualua,nlevsua,                                   &
              stnradar,irad,isrcradr,hgtradc,obsrad,odifrad,            &
              qcthrradv,qualrad,nlevrad,                                &
              stnret,iret,isrcret,hgtretc,                              &
              obsret,oanxret,odifret,                                   &
              qcthrret,qualret,nlevret,                                 &
              wgtsum,zsum)

  END IF
!
!-----------------------------------------------------------------------
!
!  Analyze data
!
!-----------------------------------------------------------------------
!
  !CALL print3dnc_lg(300,'uanl',anx(:,:,:,PTR_U) ,nx,ny,nz)
  !CALL print3dnc_lg(300,'vanl',anx(:,:,:,PTR_V) ,nx,ny,nz)


!-----------------------------------------------------------------------
!
!  allocate variable array for 3DVAR only
!
!-----------------------------------------------------------------------

  ALLOCATE(tem15(nx,ny,nz), STAT = istatus)
  CALL check_alloc_status(istatus, "before minimization: tem15")
  ALLOCATE(tem4d(nx,ny,nz,nscalar), STAT = istatus)
  CALL check_alloc_status(istatus, "before minimization: tem4d")

  tem13=0.0; tem14=0.0; tem15=0.0
  tem4d=0.0

  CALL allocate_3dvar_workarray1(nx,ny,nz,nscalarq,0.0,istatus)
!
!-----------------------------------------------------------------------
!
!  Call cloud analysis firstly ,then do 3dvar
!
!-----------------------------------------------------------------------
!
  IF ( MAXVAL(cldfirst_opt) > 0 ) THEN

    CALL set_acct(cloudana_acct)

    CALL allocatePseudoQobs(nx,ny,nz,ref_use,hydro_opt)

    ALLOCATE(cloud_ceiling(nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:cloud_ceiling")
    cloud_ceiling=0.
    ALLOCATE(vil(nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:vil")
    vil=0.
    ALLOCATE(w_cld(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:w_cld")
    w_cld=0.
    ALLOCATE(cldpcp_type_3d(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:cldpcp_type_3d")
    cldpcp_type_3d=0
    ALLOCATE(icing_index_3d(nx,ny,nz),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:icing_index_3d")
    icing_index_3d=0
    ALLOCATE(l_mask_pcptype(nx,ny),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:l_mask_pcptype")
    l_mask_pcptype=.false.
    ALLOCATE(qscalar_cld(nx,ny,nz,nscalar),STAT=istatus)
    CALL check_alloc_status(istatus, "news3dvar:qscalar_cld")
    qscalar_cld=0

    IF ( cloudopt == 0 .and. icycle == 0 ) THEN
      cloudopt = 2
      IF (myproc == 0) print *, 'cloudopt initially not set, now set to 2.'
    ELSE IF ( cloudopt == 0 .and. icycle > 0 )  THEN
      cloudopt = 2
      !cloudopt = 4
    ELSE
      IF (myproc == 0) THEN
        WRITE(*,'(1x,a,/,7x,a,I0)') 'ERROR: currently cloudopt and cldfirst_opt should not be used at the same time.', &
                   'cloudopt = ', cloudopt
      END IF
      CALL arpsstop('ERROR: cloudopt/cldfirst_opt.',-1)

    END IF
    IF (myproc == 0) print *, 'applying cloud analysis before the minimization'

    tem8  = 0.0
    tem9  = 0.0
    tem10 = 0.0
    tem11 = 0.0
    tem12 = 0.0
    tem13 = 0.0    ! use as ptbar in cmpclddrv
    tem14 = 0.0    ! use as pbar in cmpclddrv

    icatg(:,:) = 1
    CALL cmpclddrv(nx,ny,nz,nxlg,nylg,i4timanx,                         &
           xs,ys,xslg,yslg,zs,j3,hterain,latgr,longr,                   &
           pres,pt,qv,qscalar,w,icatg,                                  &
           tem13,tem14,qvbar,rhobar,                                    &
           nobsng,indexsng,stnsng,isrcsng,csrcsng,xsng,ysng,            &
           timesng,latsng,lonsng,hgtsng,                                &
           kloud,store_amt,store_hgt,                                   &
           stnradar,isrcradr,latradar,lonradar,elvradar,                &
           istat_radar,ref_mos_3d,zdr_mos_3d,kdp_mos_3d,rhv_mos_3d,     &
           qscalar_cld,tem1,tem2,w_cld,cloud_ceiling,vil,               &
           cldpcp_type_3d,icing_index_3d,l_mask_pcptype,                &
           tem3,tem4,tem5,tem6,tem7,                                    &
           1,tem8,tem9,tem4d,tem10,tem11,tem12)

    cloudopt = 0 !turn off so that cmpclddrv will not be called again


    DEALLOCATE (cloud_ceiling, vil, cldpcp_type_3d)
    DEALLOCATE (icing_index_3d, l_mask_pcptype, qscalar_cld, w_cld )
    IF (myproc == 0) PRINT *, 'END of cloud analysis before the minimization'
  END IF ! IF ( cldfirst_opt > 0 ) THEN

  CALL set_acct(varmain_acct)

  CALL allocate_3dvar_workarray2(nx,ny,nz,nscalarq,ref_use,hydro_opt,   &
                                 nensvirtl,ibeta1,ibeta2,0.0,istatus)

!-----------------------------------------------------------------------
!
!  Cycle in npass
!
!-----------------------------------------------------------------------
!

  DO ipass=1,npass

    !IF (myproc == 0) WRITE(6,'(1x,a,I2)') 'Analysis pass is:',ipass

    cpuin=f_cputime()
    CALL FLUSH (06)

    IF (myproc == 0) WRITE(6,'(1x,a,I0,a)') 'Calling minimization for analysis pass (',ipass,') ...'

    !CALL print3dnc_lg(200+ipass,'uanl',anx(:,:,:,PTR_U) ,nx,ny,nz)
    !CALL print3dnc_lg(200+ipass,'vanl',anx(:,:,:,PTR_V) ,nx,ny,nz)
    CALL minimization(nx,ny,nz,nvar_anx,nz_ua,nz_ret,                   &
            mapfct,j1,j2,j3,aj3x,aj3y,aj3z,j3inv,rhobar,                &
            dnw,dn,fnm,fnp,cf1,cf2,cf3,     & ! Added for WRF divergence constraint
            mx_sng,mx_ua,mx_ret,mx_colret,                              &
            nsrc_sng,nsrc_ua,nsrc_ret,ncat,                             &
            ipass,xs,ys,zs,x,y,z,zp,hterain,                            &
            icatg,xcor,var_anx,                                         &
            indexsng,usesng,xsng,ysng,hgtsng,thesng,                    &
            odifsng,qobsng,qualsng,isrcsng,icatsng,nobsng,              &
            indexua,useua,xua,yua,hgtua,theua,                          &
            odifua,qobsua,qualua,isrcua,nlevsua,nobsua,                 &
            ref_mos_3d,                                                 &
            xretc,yretc,hgtretc,theretc,                                &
            odifret,qobsret,qualret,                                    &
            iret,isrcret,nlevret,ncolret,                               &
            srcsng,srcua,srcret,                                        &
            iusesng(:,ipass),iuseua(:,ipass),iuseret(:,ipass),          &
            corsng,corua,corret,oanxsng,oanxua,oanxret,                 &
            nlvqback,hqback,qback,                                      &
            nsngfil,nuafil,nretfil,                                     &
            anx,tk,rhostr,                                              &
            tem1,tem2,tem3,tem4,tem5,tem6,tem4d,                        &
            tem7,tem8,tem9,tem10,tem11,tem12,tem13,tem14,tem15,istatus)

    !CALL print3dnc_lg(210+ipass,'uanl',anx(:,:,:,PTR_U) ,nx,ny,nz)
    !CALL print3dnc_lg(210+ipass,'vanl',anx(:,:,:,PTR_V) ,nx,ny,nz)

    cpuout=f_cputime()
    !IF (myproc == 0) WRITE(*,*) '=========== out minimization ==========='

    IF (ipass /= npass ) THEN

!
!-----------------------------------------------------------------------
!
!  Calculate initial obs differences
!
!-----------------------------------------------------------------------
!
      IF (nsngfil > 0) THEN
        !
        !  for single-level data
        !
        CALL grdtosng(nx,ny,nz,nz_tab,mx_sng,nvar_anx,nobsng,           &
                      x,y,xs,ys,zp,icatg,anx,qback,hqback,nlvqback,     &
                      tem1,tem2,tem3,tem4,tem5,tem6,                    &
                      stnsng,isrcsng,icatsng,hgtsng,xsng,ysng,          &
                      obsng,qobsng,qualsng,                             &
                      odifsng,oanxsng,thesng,trnsng,indexsng,srcsng,    &
                      nsrc_sng)
      END IF

      IF (nuafil > 0) THEN
        !
        !  for upper level data
        !
        CALL grdtoua(nx,ny,nz,nz_tab,nz_ua,mx_ua,nvar_anx,nobsua,       &
                     x,y,xs,ys,zp,anx,qback,hqback,nlvqback,            &
                     tem1,tem2,tem3,tem4,tem5,tem6,                     &
                     stnua,isrcua,elevua,xua,yua,hgtua,                 &
                     obsua,qobsua,qualua,nlevsua,                       &
                     odifua,oanxua,theua,trnua,indexua)
      END IF

      IF (nconvfil > 0 .AND. iuseconv(ipass+1) > 0) THEN
        !
        !  for conventional observation from prepbufr
        !
        CALL grdtoconv(nx, ny, nz, xs, ys, nvar_anx, anx, psfc, varsfc)
      END IF

      IF (ncwpfil > 0) THEN
        !
        !  for cwp data
        !
        CALL grdtocwp(nx,ny,nz,anx(:,:,:,PTR_P),tk,xs,ys,               &
                      anx(:,:,:,PTR_LN+P_QC),                   &
                      anx(:,:,:,PTR_LN+P_QI),ref_comp_2d,istatus)

        IF (pseudo_opt==1) THEN
          CALL grdtohydro(nx,ny,nz,xs,ys,                               &
                          anx(:,:,:,PTR_LN+P_QC),               &
                          anx(:,:,:,PTR_LN+P_QI),istatus)
        END IF

      END IF

      IF (ntpwfil > 0) THEN
        !
        !  for tpw data
        !
        CALL grdtotpw(nx,ny,nz,zp,xs,ys,anx(:,:,:,PTR_QV),rhobar,       &
                      anx(:,:,:,PTR_P),psfc,p_top,idealCase_opt,tem1,istatus)
      END IF

      IF (nradfil > 0) THEN
        !
        !  for radar data
        !
        IF (grdtlt_flag ==1 ) THEN
          CALL prcsstlt(nx,ny,nz,nz_tab,nvar_anx,nvar_rad,              &
                 nrad_anx,mx_rad,nsrc_rad,nz_radar,mx_colrad,           &
                 raduvobs,radrhobs,refrh,rhradobs,                      &
                 xs,ys,zs,hterain,anx,qback,hqback,nlvqback,            &
                 srcrad,qsrcrad,qcthrradv,                              &
                 isrcradr,stnradar,latradar,lonradar,elvradar,elvang,bmwidth, &
                 latradc,lonradc,xradc,yradc,irad,nlevrad,              &
                 hgtradc,obsrad,oanxrad,odifrad,iradc,jradc,            &
                 distrad,uazmrad,vazmrad,                               &
                 theradc,trnradc,qobsrad,qualrad,dsdr,dhdr,eleva,k3L,k3U,&
                 ncolrad,indexrad,istatus,tem1)
        ELSE
          CALL prcssrad(nx,ny,nz,ipass+1,nz_tab,nvar_anx,nvar_rad,      &
                    nrad_anx,mx_rad,nsrc_rad,nz_radar,mx_colrad,        &
                    raduvobs,radrhobs,refrh,rhradobs,                   &
                    x,y,zp,xs,ys,zs,hterain,anx,qback,hqback,nlvqback,  &
                    srcrad,qsrcrad,qcthrradv,                           &
                    isrcradr,stnradar,latradar,lonradar,elvradar,       &
                    latradc,lonradc,xradc,yradc,irad,nlevrad,           &
                    hgtradc,obsrad,oanxrad,odifrad, iradc,jradc,kradc,  &
                    mosaicopt,ref_mos_3d,distrad,uazmrad,vazmrad,       &
                    theradc,trnradc,qobsrad,qualrad,dsdr,dhdr,          &
                    ncolrad,indexrad,istatus,tem1)
        END IF !IF (grdtlt_flag ==1 )
      END IF

      IF( idealCase_opt==0 ) THEN

        IF (myproc == 0) PRINT *, 'Calling qcdiff'
        CALL qcdiff(nvar_anx,nrad_anx,nvar_rad,mx_sng,nsrc_sng,         &
                    indexsng,indexua,indexrad,indexret,                 &
                    usesng, useua,userad,                               &
                    nz_ua,mx_ua,nsrc_ua,                                &
                    nz_radar,mx_rad,mx_colrad,nsrc_rad,                 &
                    nz_ret,mx_ret,mx_colret,nsrc_ret,                   &
                    nobsng,nobsua,ncolrad,ncolret,var_anx,              &
                    stnsng,isrcsng,hgtsng,obsng,oanxsng,odifsng,        &
                    qcthrsng,qualsng,                                   &
                    stnua,isrcua,hgtua,obsua,oanxua,odifua,             &
                    qcthrua,qualua,nlevsua,                             &
                    stnradar,irad,isrcradr,hgtradc,obsrad,odifrad,      &
                    qcthrradv,qualrad,nlevrad,                          &
                    stnret,iret,isrcret,hgtretc,                        &
                    obsret,oanxret,odifret,                             &
                    qcthrret,qualret,nlevret,                           &
                    wgtsum,zsum)
      END IF !end of idealCase_opt==0

    END IF

    cpuout=f_cputime()
    IF (myproc == 0) THEN
       WRITE(*,*) '=========== Minimization done for pass (',ipass,') =========='
       WRITE(*,*) 'The cpu time used by this analysis is:',cpuout-cpuin
       WRITE(*,*) ' The cost function time is:', cputimearray(1)
       WRITE(*,*) ' The gradient part is:     ', cputimearray(2)
       WRITE(*,*) ' The minimization part is: ', cputimearray(3)
    END IF
    cputimearray=0.0

  END DO ! ipass

  !-----------------------------------------------------------------------
  !
  !  Free-up some memory.
  !
  !-----------------------------------------------------------------------
  !
  DEALLOCATE(ref_comp_2d)
  CALL deallocate_3dvar_workarray(istatus)

  IF (nconvfil > 0) CALL dealloc_convobs()

  IF (ibeta2 == 1) THEN
    CALL deallocate4DarrayE(modelopt,istatus)
    CALL ens_deallocate_varwrk(istatus)
  END IF

  CALL consthermo_deallocate_wrk( istatus )
  CALL consmth_deallocate_wrk ( istatus )
  CALL consmslp_deallocate_wrk( istatus )
  CALL consdiv_deallocate_wrk ( istatus )

  IF (myproc == 0) WRITE(6,*) ' The end of 3DVar analysis '

!-----------------------------------------------------------------------
!
!  Post-analysis handling
!
!-----------------------------------------------------------------------
!

  IF(idealCase_opt==1) THEN

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx
          u(i,j,k)= anx(i,j,k,PTR_U) ! 0.5*(anx(i,j,k,PTR_U)+anx(i-1,j,k,PTR_U))
        END DO
      END DO
    END DO

    !DO k=1,nz-1
    !  DO j=1,ny-1
    !    u(1,j,k)  = u(2,j,k)
    !    u(nx,j,k) = u(nx-1,j,k)
    !  END DO
    !END DO
    !
    !IF (mp_opt > 0) THEN
    !  CALL mpsendrecv2dew(u,nx,ny,nz,ebc,wbc,1,tem1)
    !END IF

    DO k=1,nz-1
      DO j=1,ny
        DO i=1,nx-1
          v(i,j,k)= anx(i,j,k,PTR_V) ! 0.5*(anx(i,j,k,PTR_V)+anx(i,j-1,k,PTR_V))
        END DO
      END DO
    END DO

    !DO k=1,nz-1
    !  DO i=1,nx-1
    !    v(i,1,k)  = v(i,2,k)
    !    v(i,ny,k) = v(i,ny-1,k)
    !  END DO
    !END DO
    !
    !IF (mp_opt > 0) THEN
    !  CALL mpsendrecv2dns(v,nx,ny,nz,nbc,sbc,2,tem1)
    !END IF

    DO k=1,nz
      DO j=1,ny-1
        DO i=1,nx-1
          w(i,j,k)= anx(i,j,k,PTR_W) ! 0.5*(anx(i,j,k,PTR_W)+anx(i,j,k-1,PTR_W))
        END DO
      END DO
    END DO

    !DO j = 1, ny-1
    !  DO i = 1, nx-1
    !    w(i,j,1)  = w(i,j,2)
    !    w(i,j,nz) = w(i,j,nz-1)
    !  END DO
    !END DO

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          pres(i,j,k)   = anx(i,j,k,PTR_P )
          pt(i,j,k)     = anx(i,j,k,PTR_PT)
          qv(i,j,k)     = anx(i,j,k,PTR_QV)
        END DO
      END DO
    END DO

    IF (ref_use .OR. hydro_opt==1 .OR. satcwpobs==1) THEN
      DO nq = 1, nscalarq
        DO k=1,nz-1
          DO j=1,ny-1
            DO i=1,nx-1
              qscalar(i,j,k,nq) = MAX(0.0, anx(i,j,k, PTR_LN+nq))
            END DO
          END DO
        END DO
      END DO
    END IF

  ELSE  ! the following is NOT for idealized case

!
!-----------------------------------------------------------------------
!
!  convert relative humidity to specific humidity( modified by Ming Hu)
!
!-----------------------------------------------------------------------
!
    IF( cntl_var_rh2 == 1 ) THEN
      DO k=1,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            tem10(i,j,k)= anx(i,j,k,PTR_PT)*(anx(i,j,k,PTR_P)/p0)**rddcp
          END DO
        END DO
      END DO
      CALL getqvs(nx,ny,nz, 1,nx-1,1,ny-1,1,nz-1, anx(1,1,1,PTR_P),tem10,qv)
      DO k=1,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            IF( anx(i,j,k,PTR_QV) > 1.0 ) anx(i,j,k,PTR_QV)=1.0
            IF( anx(i,j,k,PTR_QV) < 0.0 ) anx(i,j,k,PTR_QV)=0.0
            anx(i,j,k,PTR_QV)= anx(i,j,k,PTR_QV)*qv(i,j,k)
          END DO
        END DO
      END DO
    END IF
!
!-----------------------------------------------------------------------
!
!  Use reflectivity to establish cloud water
!
!-----------------------------------------------------------------------
!
    IF( radcldopt > 0 ) THEN
      IF (modelopt == 2) THEN
        DO k = 1, nz
          tvbar = 0.0
          DO j = 2, ny-2
            DO i = 2, nx-2
              tvbar = tvbar + pt(i,j,k)
            END DO
          END DO
          tem13(:,:,k) = tvbar/((nx-3)*(ny-3))
        END DO
      ELSE
        tem13(:,:,:) = ptbar(:,:,:)
      END IF

      CALL radmcro(nx,ny,nz,ref_mos_3d,                                 &
                 radqvopt,radqcopt,radqropt,radptopt,                   &
                 refsat,rhradobs,                                       &
                 refcld,cldrad,ceilopt,ceilmin,dzfill,                  &
                 refrain,radsetrat,radreflim,radptgain,                 &
                 xs,ys,zs,zp,                                           &
                 anx(1,1,1,PTR_P),anx(1,1,1,PTR_PT),anx(1,1,1,PTR_QV),  &
                 tem13,qvbar,rhobar,                                    &
                 qscalar,tem1)
  END IF
!
!-----------------------------------------------------------------------
!
!  Transfer analysed fields to ARPS variables for writing.
!
!-----------------------------------------------------------------------
!
    IF (mslp_opt >= 2) THEN !apply warm core structure temperature adjustment
       DO k=1,nz-1
         DO j=1,ny-1
           DO i=1,nx-1
             anx(i,j,k,PTR_PT) =  pt(i,j,k)*( pres(i,j,k) /anx(i,j,k,PTR_P) )**rddcp
           END DO
         END DO
       END DO
    END IF  !IF (mslp_opt >= 2)

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          pres(i,j,k) = anx(i,j,k,PTR_P )
          pt(i,j,k)   = anx(i,j,k,PTR_PT)
          !tgrid =anx(i,j,k,PTR_PT) * (anx(i,j,k,PTR_P)/p0) ** rddcp
          !qvsat=f_qvsat( anx(i,j,k,PTR_P),tgrid)
          !qvmin=rhmin*qvsat
          !qv(i,j,k)=MAX(qvmin,MIN(anx(i,j,k,PTR_QV),qvsat))
          qv(i,j,k)=MAX(0.0,anx(i,j,k,PTR_QV))
        END DO
      END DO
    END DO

    IF (mp_opt > 0) THEN
      CALL mpsendrecv2dew(pres,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(pres,nx,ny,nz,nbc,sbc,0,tem1)

      CALL mpsendrecv2dew(pt,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(pt,nx,ny,nz,nbc,sbc,0,tem1)

      CALL mpsendrecv2dew(qv,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(qv,nx,ny,nz,nbc,sbc,0,tem1)
    END IF

    IF(spradopt == 1) THEN

!-----------------------------------------------------------------------
!
!    Check for superadiabatic layers in each column.
!    For spradopt=1,
!    Where superadiabatic levels are found, make the next level down
!    cooler, applying the minimum potential temperature lapse rate,
!    ptlapse.
!
!-----------------------------------------------------------------------
!
      DO j=1,ny-1
        DO i=1,nx-1
          pt(i,j,nz-1)=anx(i,j,nz-1,PTR_PT)
          DO k=nz-2,2,-1
            anx(i,j,k,PTR_PT)=MIN(anx(i,j,k,PTR_PT),                    &
                  (anx(i,j,k+1,PTR_PT)-ptlapse*(zs(i,j,k+1)-zs(i,j,k))))
            pt(i,j,k)=anx(i,j,k,PTR_PT)
          END DO
        END DO
      END DO

    ELSE IF(spradopt == 2) THEN
!
!-----------------------------------------------------------------------
!
!    For spradopt=2,
!    Where superadiabatic levels are found, make the next level up
!    warmer, applying the minimum potential temperature lapse rate,
!    ptlapse.
!
!-----------------------------------------------------------------------
!
      DO j=1,ny-1
        DO i=1,nx-1
          pt(i,j,2)=anx(i,j,2,PTR_PT)
          DO k=3,nz-1
            anx(i,j,k,PTR_PT)=MAX(anx(i,j,k,PTR_PT),                      &
                  (anx(i,j,k-1,PTR_PT)+ptlapse*(zs(i,j,k)-zs(i,j,k-1))))
            pt(i,j,k)=anx(i,j,k,PTR_PT)
          END DO
        END DO
      END DO

    END IF

!-----------------------------------------------------------------------
!
! Transfer analysis wind fields
!
!-----------------------------------------------------------------------

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx
          u(i,j,k)= anx(i,j,k,PTR_U)
        END DO
      END DO
    END DO

    DO k=1,nz-1
      DO j=1,ny
        DO i=1,nx-1
          v(i,j,k)= anx(i,j,k,PTR_V)
        END DO
      END DO
    END DO

    DO k=1,nz
      DO j=1,ny-1
        DO i=1,nx-1
          w(i,j,k)= anx(i,j,k,PTR_W)
        END DO
      END DO
    END DO

    !
    ! qscalar will change even no obs is asssimilated
    !
    IF (ref_use .OR. hydro_opt==1 .OR. satcwpobs==1) THEN
      DO nq = 1, nscalarq
        DO k=1,nz-1
          DO j=1,ny-1
            DO i=1,nx-1
              qscalar(i,j,k,nq) = MAX(0.0, anx(i,j,k, PTR_LN+nq))
            END DO
          END DO
        END DO
      END DO
    END IF

    !CALL print3dnc_lg(630,'uanl',u,nx,ny,nz)
    !CALL print3dnc_lg(630,'vanl',v,nx,ny,nz)
!-----------------------------------------------------------------------
!
!  Free-up some memory.
!
!-----------------------------------------------------------------------
!
    DEALLOCATE(anx)
    IF (ALLOCATED(irad))    DEALLOCATE(irad)
    IF (ALLOCATED(nlevrad)) DEALLOCATE(nlevrad)
    IF (ALLOCATED(latradc)) DEALLOCATE(latradc,lonradc,xradc,yradc,trnradc,distrad)
    IF (ALLOCATED(uazmrad)) DEALLOCATE(uazmrad,vazmrad,dsdr,dhdr)
    IF (ALLOCATED(hgtradc)) DEALLOCATE(hgtradc,theradc,obsrad,odifrad,oanxrad)
    IF (ALLOCATED(corrad))  DEALLOCATE(corrad)
!
!-----------------------------------------------------------------------
!
!  Call cloud analysis.
!
!-----------------------------------------------------------------------
!
    CALL set_acct(cloudana_acct)

    IF( cloudopt > 0 ) THEN

      ALLOCATE(cloud_ceiling(nx,ny),STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:cloud_ceiling")
      cloud_ceiling=0.
      ALLOCATE(vil(nx,ny),STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:vil")
      vil=0.
      ALLOCATE(w_cld(nx,ny,nz),STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:w_cld")
      w_cld=0.
      ALLOCATE(cldpcp_type_3d(nx,ny,nz),STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:cldpcp_type_3d")
      cldpcp_type_3d=0
      ALLOCATE(icing_index_3d(nx,ny,nz),STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:icing_index_3d")
      icing_index_3d=0
      ALLOCATE(l_mask_pcptype(nx,ny),STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:l_mask_pcptype")
      l_mask_pcptype=.false.
      ALLOCATE(qscalar_cld(nx,ny,nz,nscalar),STAT=istatus)
      CALL check_alloc_status(istatus, "news3dvar:qscalar_cld")
      qscalar_cld=0

      tem1=0.
      tem2=0.
      tem3=0.
      tem4=0.
      tem5=0.
      tem6=0.
      tem7=0.
      tem8=0.
      tem9=0.
      tem10=0.
      tem11=0.0
      tem12=0.0
      tem13 = 0.0    ! use as pbar in cmpclddrv
      tem14 = 0.0    ! use as ptbar in cmpclddrv
      tem4d=0.0

      icatg(:,:) = 0
      DO j=2,ny-1
        DO i=2,nx-1
          DO k=2,nz-1
            IF(w(i,j,k) > 6.0) THEN
              icatg(i,j) =1
              exit
            END IF
          END DO
        END DO
      END DO

      CALL cmpclddrv(nx,ny,nz,nxlg,nylg,i4timanx,                       &
             xs,ys,xslg,yslg,zs,j3,hterain,latgr,longr,                 &
             pres,pt,qv,qscalar,w,icatg,                                &
             tem13,tem14,qvbar,rhobar,                                  &
             nobsng,indexsng,stnsng,isrcsng,csrcsng,xsng,ysng,          &
             timesng,latsng,lonsng,hgtsng,                              &
             kloud,store_amt,store_hgt,                                 &
             stnradar,isrcradr,latradar,lonradar,elvradar,              &
             istat_radar,ref_mos_3d,zdr_mos_3d,kdp_mos_3d,rhv_mos_3d,   &
             qscalar_cld,tem1,tem2,w_cld,cloud_ceiling,vil,             &
             cldpcp_type_3d,icing_index_3d,l_mask_pcptype,              &
             tem3,tem4,tem5,tem6,tem7,                                  &
             0,tem8,tem9,tem4d,tem10,tem11,tem12)

      DEALLOCATE (cloud_ceiling, vil, cldpcp_type_3d)
      DEALLOCATE (icing_index_3d, l_mask_pcptype, qscalar_cld, w_cld )
    END IF

    DEALLOCATE( tem4d,  STAT = istatus)

    IF(retqropt > 0 .AND. ncolret > 0) CALL retmcro(nx,ny,nz,           &
                   mx_ret,nsrc_ret,nvar_anx,nz_ret,mx_colret,           &
                   srcret,isrcret,iret,nlevret,                         &
                   xretc,yretc,hgtretc,qrret,ncolret,                   &
                   dzfill,                                              &
                   xs,ys,zs,qscalar(:,:,:,P_QR))  ! This will be problematic
                   ! Since P_QR may be not defined, but it is safe now because
                   ! we do not do retrieval analysis at present.

    CALL set_acct(miscel_acct)

!
!-----------------------------------------------------------------------
!
!  Hydrostatic adjustment option
!
!-----------------------------------------------------------------------
!
    CALL hydradjust(hydradj,modelopt,nx,ny,nz,nscalarq,dz, zs,          &
                    rhobar,j3, qvbar, qv, qscalar, pt,pres,             &
                    tem1, tem2, tem3, istatus)
!
!-----------------------------------------------------------------------
!
!  Adjust wind fields to minimize 3-d divergence
!
!-----------------------------------------------------------------------
!
    !CALL print3dnc_lg(730,'uanl',u,nx,ny,nz)
    !CALL print3dnc_lg(730,'vanl',v,nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  Enforce boundary conditions
!  Use zero gradient for lateral bc, and rigid for top,bottom.
!
!-----------------------------------------------------------------------
!
    CALL lbcadjust(modelopt,nx,ny,nz,nscalar,zs,                        &
                   u,v,w,pt,pres,qv,qscalar,istatus)
!
!-----------------------------------------------------------------------
!
!  Adjust surface (skin) temperature based on radiation and
!  current air temperature.
!
!-----------------------------------------------------------------------
!
    IF(tsfcopt > 0 ) THEN

      CALL tsfcadjust(modelopt, nx,ny,nz,nzsoil, x,y,z,zp,j3inv,rhostr, &
                      qv,qscalar,pt,pres,                               &
                      tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,          &
                      tem9,tem10,tem11,tem12,tem13,tem14,tem15,istatus)

    END IF

    DEALLOCATE(tem6, tem7, tem8, tem9)
    DEALLOCATE(tem10,tem11,tem12,tem13,tem14,tem15)

!
!-----------------------------------------------------------------------
!
!  Calculate analysis increments and write to file, if desired.
!
!-----------------------------------------------------------------------
!
    IF(incrdmp > 0) THEN
      CALL incrcalc(nx,ny,nz,u,v,w,pres,pt,qv,qscalar,istatus)

      CALL incrdump(incrdmp,incrhdfcompr,incdmpf,                       &
                    uincdmp,vincdmp,wincdmp,                            &
                    pincdmp,ptincdmp,qvincdmp,                          &
                    qcincdmp,qrincdmp,qiincdmp,qsincdmp,qhincdmp,       &
                    istatus)

    END IF
!
!-----------------------------------------------------------------------
!
!  Data dump of the model grid and base state arrays:
!
!-----------------------------------------------------------------------
!
    IF (mp_opt > 0) THEN
      CALL mpsendrecv2dew(u,nx,ny,nz,ebc,wbc,1,tem1)
      CALL mpsendrecv2dns(u,nx,ny,nz,nbc,sbc,1,tem1)

      CALL mpsendrecv2dew(v,nx,ny,nz,ebc,wbc,2,tem1)
      CALL mpsendrecv2dns(v,nx,ny,nz,nbc,sbc,2,tem1)

      CALL mpsendrecv2dew(w,nx,ny,nz,ebc,wbc,3,tem1)
      CALL mpsendrecv2dns(w,nx,ny,nz,nbc,sbc,3,tem1)

      CALL mpsendrecv2dew(pt,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(pt,nx,ny,nz,nbc,sbc,0,tem1)

      CALL mpsendrecv2dew(pres,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(pres,nx,ny,nz,nbc,sbc,0,tem1)

      CALL mpsendrecv2dew(qv,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(qv,nx,ny,nz,nbc,sbc,0,tem1)

      DO nq=1,nscalarq
        CALL mpsendrecv2dew(qscalar(:,:,:,nq),nx,ny,nz,ebc,wbc,0,tem1)
        CALL mpsendrecv2dns(qscalar(:,:,:,nq),nx,ny,nz,nbc,sbc,0,tem1)
      END DO

    END IF
!
!-----------------------------------------------------------------------
!
    tem1(:,:,:) = 0.

  END IF !for testing idealized case---skip some adhoc processing for ADAS 1==1

  CALL set_acct(outputem_acct)

  IF(modelopt == 1 ) THEN     ! for ARPS model

    ptprt(:,:,:) = pt(:,:,:)   - ptbar(:,:,:)
    pprt(:,:,:)  = pres(:,:,:) - pbar(:,:,:)

    IF( hdmpfmt == 5 ) GO TO 700
    IF( hdmpfmt == 9 ) GO TO 700
!
!-----------------------------------------------------------------------
!
!  Find a unique name basdmpfn(1:lbasdmpf) for the grid and
!  base state array dump file
!
!-----------------------------------------------------------------------
!

    CALL gtbasfn(runname(1:lfnkey),dirname,ldirnam,hdmpfmt,             &
                 mgrid,nestgrd,basdmpfn,lbasdmpf)

    IF (myproc == 0) WRITE(6,'(1x,a,a)')                                &
         'Data dump of grid and base state arrays into file ',          &
          basdmpfn(1:lbasdmpf)

    CALL setcornerll( nx,ny,x,y )   ! set the corner lat/lon

    grdbas = 1                ! Dump out grid and base state arrays only

!      blocking inserted for ordering i/o for message passing
    DO i=0,nprocs-1,dumpstride

      IF(myproc >= i.AND.myproc <= i+dumpstride-1)THEN

        CALL dtadump(nx,ny,nz,nzsoil,nstyps,                            &
               hdmpfmt,nchdmp,basdmpfn(1:lbasdmpf),                     &
               grdbas,filcmprs,                                         &
               u,v,w,ptprt,pprt,qv,                                     &
               qscalar,                                                 &
               ref_mos_3d,tem1,tem1,                                    &
               ubar,vbar,tem1,ptbar,pbar,rhobar,qvbar,                  &
               x,y,z,zp,zpsoil,                                         &
               soiltyp,stypfrct,vegtyp,lai,roufns,veg,                  &
               tsoil,qsoil,wetcanp,snowdpth,                            &
               raing,rainc,prcrate,                                     &
               radfrc,radsw,rnflx,radswnet,radlwin,                     &
               usflx,vsflx,ptsflx,qvsflx,                               &
               tem3,tem4,tem5)
      END IF
      IF (mp_opt > 0) CALL mpbarrier
    END DO

!
!-----------------------------------------------------------------------
!
!  Find a unique name hdmpfn(1:ldmpf) for history dump data set
!  at time 'curtim'.
!
!-----------------------------------------------------------------------
!
    700 CONTINUE

    CALL gtdmpfn(runname(1:lfnkey),dirname,                             &
               ldirnam,curtim,hdmpfmt,                                  &
               mgrid,nestgrd, hdmpfn, ldmpf)

    IF (myproc == 0)                                                    &
      WRITE(6,'(1x,a,a)') 'History data dump in file ',hdmpfn(1:ldmpf)

    grdbas = 0                ! No base state or grid array is dumped.

!    blocking inserted for ordering i/o for message passing
    DO i=0,nprocs-1,dumpstride

      IF(myproc >= i.AND.myproc <= i+dumpstride-1)THEN

        CALL dtadump(nx,ny,nz,nzsoil,nstyps,                            &
               hdmpfmt,nchdmp,hdmpfn(1:ldmpf),                          &
               grdbas,filcmprs,                                         &
               u,v,w,ptprt,pprt,qv,                                     &
               qscalar,                                                 &
               ref_mos_3d,tem1,tem1,                                    &
               ubar,vbar,tem1,ptbar,pbar,rhobar,qvbar,                  &
               x,y,z,zp,zpsoil,                                         &
               soiltyp,stypfrct,vegtyp,lai,roufns,veg,                  &
               tsoil,qsoil,wetcanp,snowdpth,                            &
               raing,rainc,prcrate,                                     &
               radfrc,radsw,rnflx,radswnet,radlwin,                     &
               usflx,vsflx,ptsflx,qvsflx,                               &
               tem3,tem4,tem5)
      END IF
      IF (mp_opt > 0) CALL mpbarrier
    END DO

  ELSE  ! modelopt==2 for WRF model

    !CALL print3dnc_lg(930,'uanl',u,nx,ny,nz)
    !CALL print3dnc_lg(930,'vanl',v,nx,ny,nz)

    !CALL print3dnc_lg(300,'phprt',phprt,nx,ny,nz)
    CALL diag_ph_mu(nx,ny,nz,pxb,pres,pt,qvxb,qv,                       &
                    wmixr, phbar, p_top, psfc,                          &
                    mub, hterain, dnw, znw, znu,                        &
                    mu,phprt)
    !CALL print3dnc_lg(400,'phprt',phprt,nx,ny,nz)

    IF (mp_opt > 0) THEN
      CALL mpsendrecv2dew(mu,nx,ny,1,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(mu,nx,ny,1,nbc,sbc,0,tem1)

      CALL mpsendrecv2dew(phprt,nx,ny,nz,ebc,wbc,3,tem1)
      CALL mpsendrecv2dns(phprt,nx,ny,nz,nbc,sbc,3,tem1)
    END IF

    lentmp = INDEX(inifile,'wrfout_d0',.TRUE.)
    IF (lentmp > 0) THEN
      WRITE(filename,'(a)') inifile(lentmp:) ! 'wrfout_d01'
    ELSE
      WRITE(filename,'(a)') 'wrfinput_d01'
    END IF

    IF (myproc == 0) WRITE(*,'(1x,4a)') 'dumping analysis to file: <', TRIM(dirname),TRIM(filename), '>.'

    !CALL print3dnc_lg(200,'U',u,nx,ny,nz)

    CALL dtadumpwrf(inifile,dirname,filename,refout,nx,ny,nz,zp,        &
                         u,v,w,pt,mu,phprt,qv, qscalar,                 &
                         phbar,psfc,ref_mos_3d,tem1, istatus)

  END IF  !end of output

  CALL deallocate_lightning(istatus)
  IF (aeri_opt ==1) CALL deallocateAERIobs(istatus)

  CALL acct_finish
  CALL accts_report_var

  IF (mp_opt > 0) THEN
    IF (myproc == 0) WRITE(6,'(/,5x,a)') '==== '//PROGNAME//'_MPI terminated normally ===='
    CALL mpexit(0)
  ELSE
    WRITE(6,'(/,5x,a)') '==== '//PROGNAME//' terminated normally ===='
    STOP
  END IF

END PROGRAM news3dvar
!
!#######################################################################
!
SUBROUTINE exsufx(fname,lenfnm,suffix,maxsuf,dotloc,lensuf)
  IMPLICIT NONE
  INTEGER :: lenfnm
  INTEGER :: maxsuf
  CHARACTER (LEN=lenfnm) :: fname
  CHARACTER (LEN=maxsuf) :: suffix
  INTEGER :: lensuf
  INTEGER :: dotloc
!-----------------------------------------------------------------------
  LOGICAL, PARAMETER :: back = .TRUE.
  INTEGER :: i,j

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  dotloc=0
  lensuf=0
  DO i=1,maxsuf
    suffix(i:i)=' '
  END DO
  dotloc=INDEX(fname,'.',back)
  IF(dotloc > 0) THEN
    lensuf=MIN(maxsuf,lenfnm-dotloc)
    suffix=fname((dotloc+1):lenfnm)
  END IF
  RETURN
END SUBROUTINE exsufx
!
!#######################################################################
!
SUBROUTINE exfroot(fname,lenfnm,froot,mxroot,lenroot)
  IMPLICIT NONE
  INTEGER :: lenfnm
  INTEGER :: mxroot
  CHARACTER (LEN=lenfnm) :: fname
  CHARACTER (LEN=mxroot) :: froot
  INTEGER :: lenroot

  INTEGER :: i
  INTEGER :: slashloc
  INTEGER :: dotloc
  LOGICAL, PARAMETER :: back = .TRUE.

  slashloc=INDEX(fname,'/',back)
  dotloc=INDEX(fname,'.',back)
  lenroot=(dotloc-slashloc)-1
  froot=fname((slashloc+1):(dotloc-1))
  RETURN
END SUBROUTINE exfroot
!
!##################################################################
!##################################################################
!######                                                      ######
!######                  SUBROUTINE SETCAT                   ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE setcat(nx,ny,nz,nscalar,ccatopt,zs,                          &
                  pt,pres,qv,qscalar,prcrate,icatg)
!
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Assign categories to grid locations according to the background
!  state to account for decorrelation across areas with active
!  parameterized convection and those without.
!
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR: Keith Brewster
!  8/7/98
!
!  MODIFICATION HISTORY:
!
!  2/2/06  Kevin Thomas
!  MPI the counts.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!  OUTPUT:
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INCLUDE 'mp.inc'
  INTEGER :: nx,ny,nz,nscalar
  INTEGER :: ccatopt
  REAL :: zs    (nx,ny,nz)  ! The physical height coordinate defined at
                            ! w-point of the staggered grid.
  REAL :: pt  (nx,ny,nz)    ! Potential temperature (K)
  REAL :: pres(nx,ny,nz)    ! Pressure (Pascal)
  REAL :: qv    (nx,ny,nz)  ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalar)

  REAL :: prcrate(nx,ny,4)     ! precipitation rate (kg/(m**2*s))
                               ! prcrate(1,1,1) = total precipitation rate
                               ! prcrate(1,1,2) = grid scale precip. rate
                               ! prcrate(1,1,3) = cumulus precip. rate
                               ! prcrate(1,1,4) = microphysics precip. rate
  INTEGER :: icatg(nx,ny)
!
  REAL :: prctrw,bltop,rhrkul,thevrkul
  PARAMETER (prctrw=1.0E-04,    & !  (kg/(m**2*s))
         bltop=1000.,   & !  m
         rhrkul=0.8,                                                    &
             thevrkul=9.0)  !  K**2
!
  INTEGER :: i,j
!  integer knt,k
  INTEGER :: nxlg,nylg
  INTEGER :: knt1,knt2a,knt2b,knt3,knt4
  INTEGER :: is_dup
  REAL :: pr,temp,qvsat,rh
!  real rhsum,rhmean
!  real the,thesum,thesq,themean,thevar
!
!-----------------------------------------------------------------------
!
!  Function f_qvsat and inline directive for Cray PVP
!
!-----------------------------------------------------------------------
!
  REAL :: f_qvsat

!fpp$ expand (f_qvsat)
!!dir$ inline always f_qvsat

!
!-----------------------------------------------------------------------
!
!  Include files
!
!-----------------------------------------------------------------------
!
!
  INCLUDE 'phycst.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
  knt1=0
  knt2a=0
  knt2b=0
  knt3=0
  knt4=0
!
!-----------------------------------------------------------------------
!
!  Initialize with default of no precip or precip influence
!
!  We jump thru some extra hoops to make sure the MPI and non-MPI counts
!  are the same.  If we don't, overlap points can be counted twice.
!
!-----------------------------------------------------------------------
!
  DO j=1,ny-1
    DO i=1,nx-1
      icatg(i,j)=1
    END DO
  END DO
!
  IF(ccatopt == 1) THEN
    DO j=1,ny-1
      is_dup = 0
      IF ((mp_opt > 0) .AND. (j >= ny-2) .AND. (loc_y .NE. nproc_y)) is_dup=1
      DO i=1,nx-1
        IF ((mp_opt > 0) .AND. (i >= nx-2) .AND. (loc_x .NE. nproc_x)) is_dup=1
!
!-----------------------------------------------------------------------
!
!  Is convective precip turned on or heavy rain occuring?
!
!-----------------------------------------------------------------------
!
        IF (prcrate(i,j,3) > 0. .OR. prcrate(i,j,1) > prctrw) THEN
          IF (is_dup == 0) knt4=knt4+1
          icatg(i,j)=4
!      print *, ' set i,j:',i,j,' cat 4',' prcrate(1):',
!    :             prcrate(i,j,1),' prcrate(3):',prcrate(i,j,3)
!
!-----------------------------------------------------------------------
!
!  Is it currently raining?
!
!-----------------------------------------------------------------------
!
        ELSE IF ( prcrate(i,j,1) > 0. ) THEN
          IF (is_dup == 0) knt3=knt3+1
          icatg(i,j)=3
!      print *, ' set i,j:',i,j,' cat 3',' prcrate(1):',
!    :             prcrate(i,j,1),' prcrate(3):',prcrate(i,j,3)
!
!-----------------------------------------------------------------------
!
!  Evidence of rain-cooled air?
!  Lapse rate close to a moist adiabatic lapse rate in the lowest 2-km,
!  and a high relative humidity in that layer.  Or a high relative
!  humidity at the first level above ground.
!
!-----------------------------------------------------------------------
!
        ELSE
!
!-----------------------------------------------------------------------
!
!  For the layer 0-2km above first grid level (k=2).
!  Calculate the mean relative humidity.
!  Calculate the standard deviation of saturated equivalent
!  potential temperature.
!
!-----------------------------------------------------------------------
!
!      rhsum=0.
!      thesum=0.
!      thesq=0.
!      knt=0
!      DO 40 k=2,nz-1
!        IF(k.gt.2 .and. (zs(i,j,k)-zs(i,j,2)).gt.bltop) GO TO 41
!        knt=knt+1
!        pr=pprt(i,j,k)+pbar(i,j,k)
!        temp=(ptprt(i,j,k)+ptbar(i,j,k)) * (pr/p0)**rddcp
!        qvsat=f_qvsat( pr,temp )
!        rh=min(1.,(qv(i,j,k)/qvsat))
!        the=F_PT2PTE( pr,(ptprt(i,j,k)+ptbar(i,j,k)),qvsat)
!        rhsum=rhsum+rh
!        thesum=thesum+the
!        thesq=thesq+(the*the)
!  40     CONTINUE
!  41     CONTINUE
!      IF(knt.gt.0) THEN
!        rhmean=rhsum/float(knt)
!        themean=thesum/float(knt)
!      ELSE
!        rhmean=0.
!        themean=0.
!      END IF
!      IF(knt.gt.1) THEN
!        thevar=(thesq-((thesum*thesum)/float(knt)))/float(knt-1)
!      ELSE
!        thevar=9999.
!      END IF
!
!      IF (rhmean.gt.rhrkul .and. thevar.lt.thevrkul) THEN
!        knt2a=knt2a+1
!        icatg(i,j)=2
!        print *, ' set i,j:',i,j,' cat 2',' prcrate(1):',
!    :             prcrate(i,j,1),' prcrate(3):',prcrate(i,j,3)
!      ELSE
!
          pr=pres(i,j,2)
          temp=pt(i,j,2) * (pr/p0)**rddcp
          qvsat=f_qvsat( pr,temp )
          rh=qv(i,j,2)/qvsat
          IF(rh > 0.8) THEN
            IF (is_dup == 0) knt2b=knt2b+1
            icatg(i,j)=2
!          print *, ' set i,j:',i,j,' cat 2*',' RH(2) = ',rh
          END IF

!      END IF
!      IF (mod(i,20).eq.0 .and. mod(j,10).eq.0)  THEN
!        print *, i,j,' rhmean=',rhmean,
!    :               ' thevar=',thevar,' icat=',icatg(i,j)
!        print *, ' '
!      END IF

        END IF
      END DO
    END DO
    IF (mp_opt > 0 ) THEN
      CALL mpsumi(knt2a,1)
      CALL mpsumi(knt2b,1)
      CALL mpsumi(knt3,1)
      CALL mpsumi(knt4,1)
      nxlg = (nx - 3) * nproc_x + 3
      nylg = (ny - 3) * nproc_y + 3
      knt1=((nxlg-1)*(nylg-1))-knt2a-knt2b-knt3-knt4
    ELSE
      knt1=((nx-1)*(ny-1))-knt2a-knt2b-knt3-knt4
    END IF

    IF (myproc == 0 ) THEN
      WRITE(6,'(1x,a)') 'Background precip correlation category assignment counts'
      WRITE(6,'(5x,5(a,i8))') 'cat  1: ',knt1, ', cat 2a: ',knt2a       &
                           ,', cat 2b: ',knt2b,', cat  3: ',knt3        &
                           ,', cat  4: ',knt4
    END IF
  END IF

  CALL iedgfill(icatg,1,nx,1,nx-1, 1,ny,1,ny-1,1,1,1,1)

  RETURN
END SUBROUTINE setcat

!#######################################################################

SUBROUTINE cvtsrcstr(srcname,srcstr)
!
! Sets name to upper case and replaces spaces with underscores.
!
  IMPLICIT NONE
  CHARACTER(LEN=8),INTENT(IN)  :: srcname
  CHARACTER(LEN=8),INTENT(OUT) :: srcstr

  CHARACTER(LEN=1),PARAMETER :: underscore = '_'
  INTEGER, PARAMETER :: iaspace = 32
  INTEGER :: i,lensrc,jchar

  srcstr = srcname

!
! "srcstr" values can *MUST* start with a letter.  Therefore "88D" is illegal.
! Put an "R" in front of the name.
!

  IF (srcstr(1:3) == "88D") THEN
    srcstr = "R"//srcname
  ENDIF

  lensrc=LEN_TRIM(srcstr)

  DO i=1,lensrc
    jchar=IACHAR(srcstr(i:i))
    IF(jchar == iaspace ) THEN
      srcstr(i:i) = underscore
    ELSE IF( jchar > 96 .AND. jchar < 123) THEN
      srcstr(i:i) = ACHAR( (jchar-32) )
    END IF
  END DO
  RETURN
END SUBROUTINE cvtsrcstr

!#######################################################################
!
! hydradjust for the ARPS model only
!
! Note that this subroutine operate on perturbed variables ptprt and pprt
!
!#######################################################################

SUBROUTINE hydradjust(hydradj,modelopt,nx,ny,nz,nscalarq,dz, zs,        &
                      rhobar,j3, qvbar, qv, qscalar, pt, pres,          &
                      tem1, tem2, tem3, istatus)
  USE module_arpsArray

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: hydradj, modelopt
  INTEGER, INTENT(IN)  :: nx, ny, nz, nscalarq
  REAL,    INTENT(IN)  :: dz
  REAL,    INTENT(IN)  :: zs(nx,ny,nz)
  REAL,    INTENT(IN)  :: rhobar(nx,ny,nz)
  REAL,    INTENT(IN)  :: j3(nx,ny,nz)
  REAL,    INTENT(IN)  :: qvbar(nx,ny,nz)
  REAL,    INTENT(IN)  :: qv(nx,ny,nz)
  REAL,    INTENT(INOUT)  :: pt(nx,ny,nz)
  REAL,    INTENT(INOUT)  :: pres(nx,ny,nz)
  REAL,    INTENT(IN)  :: qscalar(nx,ny,nz,nscalarq)

  REAL,    INTENT(INOUT) :: tem1(nx,ny,nz), tem2(nx,ny,nz), tem3(nx,ny,nz)

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INTEGER :: i,j,k, nq
  REAL    :: temp, pconst, qvprm, qtot, tvbar

  INCLUDE 'phycst.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  IF (modelopt == 2) THEN
    IF (hydradj > 0) WRITE(*,'(/,1x,a,/)') 'WARNING: hydradj option is not supported for the WRF model.'
    RETURN
  END IF

  ptprt(:,:,:) = pt(:,:,:)   - ptbar(:,:,:)
  pprt(:,:,:)  = pres(:,:,:) - pbar(:,:,:)

  CALL allocate_arpsArray_for_hydr(nx,ny,nz,istatus)
!
!-----------------------------------------------------------------------
!
!  Calculate and store the sound wave speed squared in csndsq.
!  Use original definition of sound speed.
!
!-----------------------------------------------------------------------
!
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        csndsq(i,j,k)= cpdcv*pbar(i,j,k)/rhobar(i,j,k)
      END DO
    END DO
  END DO

  IF(hydradj == 1 .OR. hydradj == 2) THEN
    pconst=0.5*g*dz
!
!-----------------------------------------------------------------------
!
!  Create thermal buoyancy at each scalar point,
!  which is stored in tem2
!
!-----------------------------------------------------------------------
!
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          qvprm=qv(i,j,k)-qvbar(i,j,k)
          qtot = 0.0
          DO nq=1,nscalarq
            qtot=qtot+qscalar(i,j,k,nq)
          END DO
          tem2(i,j,k)=j3(i,j,k)*rhobar(i,j,k)*                          &
                    g*( (ptprt(i,j,k)/ptbar(i,j,k)) +                   &
                        (qvprm/(rddrv+qvbar(i,j,k))) -                  &
                        ((qvprm+qtot)/(1.+qvbar(i,j,k))) )
        END DO
      END DO
    END DO
!
!-----------------------------------------------------------------------
!
!  Average thermal buoyancy to w points
!  which is stored in tem3
!
!-----------------------------------------------------------------------
!
    CALL avgsw(tem2,nx,ny,nz,1,nx,1,ny, tem3)

    IF(hydradj == 1) THEN

      DO i=1,nx
        DO j=1,ny
          tem1(i,j,1)=pprt(i,j,1)
          DO k=2,nz-2
            tem1(i,j,k)=                                                &
                  ( (1. - (pconst*j3(i,j,k-1)/csndsq(i,j,k-1)) )*       &
                      tem1(i,j,k-1) +                                   &
                      dz*tem3(i,j,k) ) /                                &
                      (1. + (pconst*j3(i,j,k)/csndsq(i,j,k)) )
            IF(j == 26 .AND. MOD(i,10) == 0) THEN
              IF(k == nz-2) PRINT *, '            Point i= ',i, '  j=26'
              PRINT *, ' k,pprt,tem1,diff =',                           &
                  k,pprt(i,j,k),tem1(i,j,k),                            &
                  (tem1(i,j,k)-pprt(i,j,k))
            END IF
            pprt(i,j,k)=tem1(i,j,k)
          END DO
          pprt(i,j,nz-1)=pprt(i,j,nz-2)
        END DO
      END DO

    ELSE IF(hydradj == 2) THEN

      DO i=1,nx
        DO j=1,ny
          tem1(i,j,nz-1)=pprt(i,j,nz-1)
          DO k=nz-2,2,-1
            tem1(i,j,k)=                                                &
                  ( (1.+ (pconst*j3(i,j,k+1)/csndsq(i,j,k+1)) )*        &
                      tem1(i,j,k+1) -                                   &
                      dz*tem3(i,j,k+1) ) /                              &
                      (1.- (pconst*j3(i,j,k  )/csndsq(i,j,k  )) )
            IF(j == 26 .AND. MOD(i,10) == 0) THEN
              IF(k == nz-2) PRINT *, '            Point i= ',i, '  j=26'
              PRINT *, ' k,pprt,tem1,diff =',                           &
                  k,pprt(i,j,k),tem1(i,j,k),                            &
                  (tem1(i,j,k)-pprt(i,j,k))
            END IF
            pprt(i,j,k)=tem1(i,j,k)
          END DO
          pprt(i,j,1)=pprt(i,j,2)
        END DO
      END DO
    END IF

  ELSE IF (hydradj == 3) THEN
!
!-----------------------------------------------------------------------
!
!  Calculate total pressure, store in tem1.
!  Calculate virtual temperature, store in tem2.
!
!-----------------------------------------------------------------------
!
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          tem1(i,j,k) = pbar(i,j,k)+pprt(i,j,k)
          temp = (ptbar(i,j,k)+ptprt(i,j,k))*                           &
                 ((tem1(i,j,k)/p0)**rddcp)
          tem2(i,j,k) = temp*(1.0+rvdrd*qv(i,j,k))/                     &
                             (1.0+qv(i,j,k))
        END DO
      END DO
    END DO
!
!-----------------------------------------------------------------------
!
!  Integrate hydrostatic equation, begining at k=2
!
!-----------------------------------------------------------------------
!
    pconst=-g/rd
    DO k=3,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          tvbar=0.5*(tem2(i,j,k)+tem2(i,j,k-1))
          tem1(i,j,k)=tem1(i,j,k-1)*                                    &
                     EXP(pconst*(zs(i,j,k)-zs(i,j,k-1))/tvbar)
          pprt(i,j,k)=tem1(i,j,k)-pbar(i,j,k)
        END DO
      END DO
    END DO
    DO j=1,ny-1
      DO i=1,nx-1
        tvbar=tem2(i,j,2)
        tem1(i,j,1)=tem1(i,j,2)*                                        &
                   EXP(pconst*(zs(i,j,1)-zs(i,j,2))/tvbar)
        pprt(i,j,1)=tem1(i,j,1)-pbar(i,j,1)
      END DO
    END DO
  END IF

  CALL deallocate_arpsArray_for_hydr(istatus)

  pt(:,:,:)    = ptprt(:,:,:) + ptbar(:,:,:)
  pres(:,:,:)  = pprt(:,:,:)  + pbar(:,:,:)

  RETURN
END SUBROUTINE hydradjust
!
!#######################################################################
!
! lbcadjust
!
!  Enforce boundary conditions
!  Use zero gradient for lateral bc, and rigid for top,bottom.
!
!#######################################################################

SUBROUTINE lbcadjust(modelopt,nx,ny,nz,nscalar,zs,                      &
                     u,v,w,pt,pres,qv,qscalar,istatus)

  USE module_arpsArray

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: modelopt
  INTEGER, INTENT(IN)  :: nx, ny, nz, nscalar
  REAL,    INTENT(IN)  :: zs(nx,ny,nz)
  REAL,    INTENT(INOUT)  :: u(nx,ny,nz)
  REAL,    INTENT(INOUT)  :: v(nx,ny,nz)
  REAL,    INTENT(INOUT)  :: w(nx,ny,nz)
  REAL,    INTENT(INOUT)  :: pt(nx,ny,nz)
  REAL,    INTENT(INOUT)  :: pres(nx,ny,nz)
  REAL,    INTENT(INOUT)  :: qv(nx,ny,nz)
  REAL,    INTENT(INOUT)  :: qscalar(nx,ny,nz,nscalar)

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INTEGER :: i,j,k, nq
  REAL    :: temp, pconst, qvprm, qtot, tvbar, pr1,pr2

  INCLUDE 'bndry.inc'
  INCLUDE 'phycst.inc'
  INCLUDE 'mp.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  CALL allocate_arpsArray_for_lbc(nx,ny,nz,istatus)
!
!-----------------------------------------------------------------------
!
!  Enforce boundary conditions
!  Use zero gradient for lateral bc, and rigid for top,bottom.
!
!-----------------------------------------------------------------------
!

  ebc_global = 3
  wbc_global = 3
  sbc_global = 3
  nbc_global = 3
  tbc = 1
  bbc = 1

  ebc = ebc_global
  wbc = wbc_global
  nbc = nbc_global
  sbc = sbc_global

  IF (loc_x > 1)       wbc = 0
  IF (loc_x < nproc_x) ebc = 0
  IF (loc_y > 1)       sbc = 0
  IF (loc_y < nproc_y) nbc = 0

  DO k=1,nz
    DO j=1,ny
      pdteb(j,k)=0.
      pdtwb(j,k)=0.
    END DO
    DO i=1,nx
      pdtnb(i,k)=0.
      pdtsb(i,k)=0.
    END DO
  END DO

  CALL bcu(nx,ny,nz,0., u,                                              &
           pdteb,pdtwb,pdtnb,pdtsb,ebc,wbc,nbc,sbc,tbc,bbc,             &
           ebc_global,wbc_global,nbc_global,sbc_global)

  CALL bcv(nx,ny,nz,0., v,                                              &
           pdteb,pdtwb,pdtnb,pdtsb,ebc,wbc,nbc,sbc,tbc,bbc,             &
           ebc_global,wbc_global,nbc_global,sbc_global)

  CALL lbcw(nx,ny,nz,0.0,w,wcont,                                       &
            pdteb,pdtwb,pdtnb,pdtsb,ebc,wbc,nbc,sbc,                    &
            ebc_global,wbc_global,nbc_global,sbc_global)

  CALL bcsclr(nx,ny,nz,0.,pt,pt,pt,                                     &
              pdteb,pdtwb,pdtnb,pdtsb,ebc,wbc,nbc,sbc,tbc,bbc,          &
              ebc_global,wbc_global,nbc_global,sbc_global)

  CALL bcsclr(nx,ny,nz,0., qv,qv,qv,                                    &
           pdteb,pdtwb,pdtnb,pdtsb,ebc,wbc,nbc,sbc,tbc,bbc,             &
           ebc_global,wbc_global,nbc_global,sbc_global)

  DO nq=1,nscalar
    CALL bcsclr(nx,ny,nz,0., qscalar(:,:,:,nq),qscalar(:,:,:,nq),       &
             qscalar(:,:,:,nq),                                         &
             pdteb,pdtwb,pdtnb,pdtsb,ebc,wbc,nbc,sbc,tbc,bbc,           &
             ebc_global,wbc_global,nbc_global,sbc_global)
  END DO

  !CALL print3dnc_lg(830,'uanl',u,nx,ny,nz)
  !CALL print3dnc_lg(830,'vanl',v,nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  Apply extrapolated gradient bc to pressure for geostrophic
!  type balance to winds along lateral boundaries.  Consistent
!  with horizontal winds constant.
!
!-----------------------------------------------------------------------
!
  DO k=2,nz-2
    DO j=1,ny-1
      pres(1,j,k)=2.*pres(2,j,k)-pres(3,j,k)
    END DO
  END DO

  DO k=2,nz-2
    DO j=1,ny-1
      pres(nx-1,j,k)=2.*pres(nx-2,j,k)-pres(nx-3,j,k)
    END DO
  END DO

  DO k=2,nz-2
    DO i=1,nx-1
      pres(i,ny-1,k)=2.*pres(i,ny-2,k)-pres(i,ny-3,k)
    END DO
  END DO

  DO k=2,nz-2
    DO i=1,nx-1
      pres(i,1,k)=2.*pres(i,2,k)-pres(i,3,k)
    END DO
  END DO

!
!-----------------------------------------------------------------------
!
!  For top and bottom bc, apply hydrostatic pressure equation to
!  total pressure, then subtract pbar.
!
!-----------------------------------------------------------------------
!
  pconst=-g/rd
  DO j=1,ny-1
    DO i=1,nx-1
      pr2 = pres(i,j,2)
      temp = pt(i,j,2)* ((pr2/p0)**rddcp)
      tvbar = temp*(1.0+rvdrd*qv(i,j,2))/(1.0+qv(i,j,2))
      pr1=pr2*EXP(pconst*(zs(i,j,1)-zs(i,j,2))/tvbar)
      pres(i,j,1)=pr1

      pr2 = pres(i,j,nz-2)
      temp = pt(i,j,nz-2)*((pr2/p0)**rddcp)
      tvbar= temp*(1.0+rvdrd*qv(i,j,nz-2))/(1.0+qv(i,j,nz-2))
      pr1=pr2*EXP(pconst*(zs(i,j,nz-1)-zs(i,j,nz-2))/tvbar)
      pres(i,j,nz-1)=pr1
    END DO
  END DO

  CALL deallocate_arpsArray_for_lbc(istatus)

  RETURN
END SUBROUTINE lbcadjust

!
!#######################################################################
!
! tsfcadjust
!
!  Adjust surface (skin) temperature based on radiation and
!  current air temperature.
!
! Note that pt and pres should have already been separated as
!   pprt  pbar
!   ptprt ptbar
!
!#######################################################################
!
SUBROUTINE tsfcadjust(model, nx,ny,nz,nzsoil, x,y,z,zp,j3inv,rhostr, &
                      qv,qscalar,pt, pres,                              &
                      tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,          &
                      tem9,tem10,tem11,tem12,tem13,tem14,tem15,istatus)

  USE module_arpsArray

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: nx, ny, nz, nzsoil
  INCLUDE 'globcst.inc'
  INCLUDE 'radcst.inc'

  INTEGER, INTENT(IN)  :: model
  REAL,    INTENT(IN)  :: x     (nx)
  REAL,    INTENT(IN)  :: y     (ny)
  REAL,    INTENT(IN)  :: z     (nz)
  REAL,    INTENT(IN)  :: zp    (nx,ny,nz)
  REAL,    INTENT(IN)  :: j3inv (nx,ny,nz)
  REAL,    INTENT(IN)  :: rhostr (nx,ny,nz)
  REAL,    INTENT(IN)  :: qv(nx,ny,nz)
  REAL,    INTENT(IN)  :: pt(nx,ny,nz)
  REAL,    INTENT(IN)  :: pres(nx,ny,nz)
  REAL,    INTENT(IN)  :: qscalar(nx,ny,nz,nscalar)

  REAL,    INTENT(INOUT), DIMENSION(nx,ny,nz) :: tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8, &
                                                 tem9,tem10,tem11,tem12,tem13,tem14,tem15

  INTEGER, INTENT(OUT) :: istatus
!
!-----------------------------------------------------------------------
!
  INTEGER :: rbufsz

!
!-----------------------------------------------------------------------
!
!  Radiation variables only needed for adjtsfc
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: rsirbm(:,:), rsirdf(:,:), rsuvbm(:,:), rsuvdf(:,:)
  REAL, ALLOCATABLE :: cosz(:,:), cosss(:,:)
  REAL, ALLOCATABLE :: fdirir(:,:), fdifir(:,:), fdirpar(:,:), fdifpar(:,:)
  REAL, ALLOCATABLE :: radbuf(:), sh(:,:)           !added by DTD

  REAL, ALLOCATABLE :: temrad1 (:,:,:)   ! Temporary work array.
  REAL, ALLOCATABLE :: temrad2 (:,:,:)   ! Temporary work array.

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0
!
!-----------------------------------------------------------------------
!
  IF (model == 2) THEN
    WRITE(*,'(1x,a)') 'WARNING: tsfc adjustment is not (yet) supported.'
    !CALL arpsstop('TODO: Support tsfcopt for the WRF model',1)
    RETURN
  END IF

  pprt(:,:,:)  = pres(:,:,:) - pbar(:,:,:)
  ptprt(:,:,:) = pt(:,:,:)   - ptbar(:,:,:)

  IF (radopt == 2) THEN
    rbufsz = n2d_radiat*nx*ny + n3d_radiat*nx*ny*nz
  ELSE
    rbufsz = 1
  END IF

  ALLOCATE(rsirbm(nx,ny))
  ALLOCATE(rsirdf(nx,ny))
  ALLOCATE(rsuvbm(nx,ny))
  ALLOCATE(rsuvdf(nx,ny))
  rsirbm = 0.
  rsirdf = 0.
  rsuvbm = 0.
  rsuvdf = 0.

  ALLOCATE(cosz(nx,ny))
  ALLOCATE(cosss(nx,ny))
  cosz = 0.
  cosss = 0.


  ALLOCATE(fdirir(nx,ny))
  ALLOCATE(fdifir(nx,ny))
  ALLOCATE(fdirpar(nx,ny))
  ALLOCATE(fdifpar(nx,ny))
  fdirir = 0.
  fdifir = 0.
  fdirpar= 0.
  fdifpar= 0.

  tem10 = 0.
  tem11 = 0.
  tem12 = 0.
  tem13 = 0.
  tem14 = 0.
  tem15 = 0.

  ALLOCATE(temrad1(nx,ny,nz), STAT = istatus)
  CALL check_alloc_status(istatus, "before adjtsfc: temrad1")
  ALLOCATE(temrad2(nx,ny,nz), STAT = istatus)
  CALL check_alloc_status(istatus, "before adjtsfc: temrad2")
  temrad1 = 0.
  temrad2 = 0

  ALLOCATE(radbuf(rbufsz))
  ALLOCATE(sh(nx,ny))
  radbuf = 0.
  sh = 0.

  CALL adjtsfc(nx,ny,nz,nscalar,rbufsz,                             &
             ptprt,pprt,qv,qscalar,                                 &
             ptbar,pbar,ppi,rhostr,                                 &
             x,y,z,zp,j3inv,                                        &
             soiltyp, tsoil(1,1,1,0),qsoil(1,1,1,0),snowdpth,       &
             radfrc, radsw, rnflx, radswnet, radlwin,               &
             rsirbm,rsirdf,rsuvbm,rsuvdf,                           &
             cosz, cosss,                                           &
             fdirir,fdifir,fdirpar,fdifpar,                         &
             tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,               &
             tem9,tem10,tem11,tem12,tem13,tem14,tem15,temrad1,      &
             radbuf, sh, temrad2)

  ! DTD: placed call to fix_soil_nstp here, to propagate adjusted soil temperatures above
  ! to the other soil types, if nstyp > 1
  ! The call to adtsfc above only adjusts the soil temperatures for one soil type

  CALL fix_soil_nstyp(nx,ny,nzsoil,1,nstyp,tsoil, qsoil,wetcanp)

  DEALLOCATE(rsirbm,rsirdf,rsuvbm,rsuvdf)
  DEALLOCATE(cosz,cosss)
  DEALLOCATE(fdirir,fdifir,fdirpar,fdifpar)
  DEALLOCATE(temrad1,temrad2)
  DEALLOCATE(radbuf,sh)

  !pres(:,:,:)  = pprt(:,:,:)  + pbar(:,:,:)
  !pt(:,:,:)    = ptprt(:,:,:) + ptbar(:,:,:)

  RETURN
END SUBROUTINE tsfcadjust
