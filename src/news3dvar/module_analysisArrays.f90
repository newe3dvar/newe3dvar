MODULE module_analysisArrays
!-----------------------------------------------------------------------
!
! MODULE contains variables for
!
!   Surface Station   / Surface observation    (mx_sng)
!   Upper-ari Station / Upper-air observation  (mx_ua)
!
! HISTORY:
!
!   11/06/2007 (Yunheng Wang)
!   Initial version.
!
!   03/14/2016 (Y. Wang)
!   Took out radar-related variables to module_radarobs.f90.
!
!-----------------------------------------------------------------------
!
!  Surface Station variables
!
!-----------------------------------------------------------------------
!
  INTEGER, ALLOCATABLE :: isrcsng(:)
  INTEGER, ALLOCATABLE :: icatsng(:)
  REAL,    ALLOCATABLE :: latsng(:,:)
  REAL,    ALLOCATABLE :: lonsng(:,:)
  REAL,    ALLOCATABLE :: hgtsng(:,:)
  REAL,    ALLOCATABLE :: xsng(:)
  REAL,    ALLOCATABLE :: ysng(:)
  REAL,    ALLOCATABLE :: trnsng(:)
  INTEGER, ALLOCATABLE :: timesng(:,:)
  CHARACTER(LEN=5), ALLOCATABLE :: stnsng(:,:)
!
!-----------------------------------------------------------------------
!
!  Surface (single-level) read-in observation variables
!
!-----------------------------------------------------------------------
!
  CHARACTER(LEN=8), ALLOCATABLE :: wx(:,:)
  CHARACTER(LEN=8), ALLOCATABLE :: csrcsng(:,:)
  CHARACTER(LEN=1), ALLOCATABLE :: store_emv(:,:,:)
  CHARACTER(LEN=4), ALLOCATABLE :: store_amt(:,:,:)
  INTEGER, ALLOCATABLE :: kloud(:,:),idp3(:,:)
  REAL,    ALLOCATABLE :: store_hgt(:,:,:)
  REAL,    ALLOCATABLE :: obrdsng(:,:,:)

  REAL,    ALLOCATABLE :: obsng  (:,:)
  REAL,    ALLOCATABLE :: odifsng(:,:)
  REAL,    ALLOCATABLE :: oanxsng(:,:)
  REAL,    ALLOCATABLE :: corsng(:,:)
  REAL,    ALLOCATABLE :: thesng(:)

!-----------------------------------------------------------------------
!
!  Upper Air Station variables
!
!-----------------------------------------------------------------------
!
  INTEGER, ALLOCATABLE :: isrcua(:)
  REAL,    ALLOCATABLE :: xua(:)
  REAL,    ALLOCATABLE :: yua(:)
  REAL,    ALLOCATABLE :: trnua(:)
  REAL,    ALLOCATABLE :: elevua(:)
  REAL,    ALLOCATABLE :: hgtua(:,:)
  INTEGER, ALLOCATABLE :: nlevsua(:)
  CHARACTER (LEN=5), ALLOCATABLE :: stnua(:)
!
!-----------------------------------------------------------------------
!
!  Upper-air observation variables
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: obsua (:,:,:)
  REAL, ALLOCATABLE :: odifua(:,:,:)
  REAL, ALLOCATABLE :: oanxua(:,:,:)
  REAL, ALLOCATABLE :: corua(:,:,:)
  REAL, ALLOCATABLE :: theua(:,:)
!
!-----------------------------------------------------------------------
!
!  Radar site variables
!
!-----------------------------------------------------------------------
!
  !CHARACTER (LEN=5), ALLOCATABLE :: stnrad(:)
  !INTEGER, ALLOCATABLE :: isrcrad(:)
  !REAL,    ALLOCATABLE :: latrad(:),lonrad(:)
  !REAL,    ALLOCATABLE :: elvrad(:)
  !REAL,    ALLOCATABLE :: elvang(:,:)
!
!-----------------------------------------------------------------------
!
!  Radar observation variables
!
!-----------------------------------------------------------------------
  !INTEGER, ALLOCATABLE :: irad(:)
  !INTEGER, ALLOCATABLE :: nlevrad(:)
  !REAL,    ALLOCATABLE :: latradc(:)
  !REAL,    ALLOCATABLE :: lonradc(:)
  !REAL,    ALLOCATABLE :: xradc(:)
  !REAL,    ALLOCATABLE :: yradc(:)
  !REAL,    ALLOCATABLE :: trnradc(:)
  !REAL,    ALLOCATABLE :: distrad(:)
  !REAL,    ALLOCATABLE :: uazmrad(:)
  !REAL,    ALLOCATABLE :: vazmrad(:)
  !REAL,    ALLOCATABLE :: hgtradc(:,:)
  !REAL,    ALLOCATABLE :: theradc(:,:)
  !REAL,    ALLOCATABLE :: obsrad(:,:,:)
  !
  !REAL,    ALLOCATABLE :: odifrad(:,:,:)
  !REAL,    ALLOCATABLE :: oanxrad(:,:,:)
  !REAL,    ALLOCATABLE :: corrad(:,:,:)
  !REAL,    ALLOCATABLE :: dsdr(:,:)
  !REAL,    ALLOCATABLE :: dhdr(:,:)
  !
  !INTEGER, ALLOCATABLE :: iradc(:),jradc(:)
  !INTEGER, ALLOCATABLE :: kradc(:,:)
  !INTEGER, ALLOCATABLE :: k3L(:,:), k3U(:,:)
  !REAL,    ALLOCATABLE :: eleva(:,:)

!
!-----------------------------------------------------------------------
!
!  Quality Control Variables
!
!-----------------------------------------------------------------------
!
  INTEGER, ALLOCATABLE :: qualrdsng(:,:,:)
  INTEGER, ALLOCATABLE :: qualsng(:,:)
  REAL   , ALLOCATABLE :: qobsng(:,:)

  INTEGER, ALLOCATABLE :: qualua(:,:,:)
  REAL   , ALLOCATABLE :: qobsua(:,:,:)

  !INTEGER, ALLOCATABLE :: qualrad(:,:,:)
  !REAL,    ALLOCATABLE :: qobsrad(:,:,:)
!
!-----------------------------------------------------------------------
!
!  other Variables  !9/6/2013 by  Guoqing Ge
!
!-----------------------------------------------------------------------
!
  REAL   , ALLOCATABLE :: obsret(:,:,:)
  REAL   , ALLOCATABLE :: odifret(:,:,:)
  REAL   , ALLOCATABLE :: oanxret(:,:,:)

  REAL   , ALLOCATABLE :: qback(:,:)
  REAL   , ALLOCATABLE :: qsrcsng(:,:)
  REAL   , ALLOCATABLE :: qsrcua(:,:,:)
  REAL   , ALLOCATABLE :: qsrcret(:,:,:)

  REAL   , ALLOCATABLE :: qcthrsng(:,:)
  REAL   , ALLOCATABLE :: barqclim(:,:)
  REAL   , ALLOCATABLE :: qcthrua(:,:)

  INTEGER, ALLOCATABLE :: qualret(:,:,:)
  REAL   , ALLOCATABLE :: qobsret(:,:,:)
  REAL   , ALLOCATABLE :: qcthrret(:,:)

  CONTAINS

  !
  ! Allocate them
  !
  SUBROUTINE allocate_3dvarObs_arrays(progname,ntime,nz_tab,nscalar,    &
                mx_sng,mx_ua,mx_colret,nz_ua,nz_ret,nvar_anx,           &
                nvar_sng,nsrc_sng,nsrc_ua,nsrc_ret,istatus)

    IMPLICIT NONE
    CHARACTER(LEN=4), INTENT(IN)  :: progname
    INTEGER,          INTENT(IN)  :: ntime,  nz_tab,   nvar_anx, nscalar
    INTEGER,          INTENT(IN)  :: mx_ua,  nz_ua,    nsrc_ua
    INTEGER,          INTENT(IN)  :: mx_sng, nvar_sng, nsrc_sng
    INTEGER,          INTENT(IN)  :: mx_colret,nz_ret, nsrc_ret

    INTEGER,          INTENT(OUT) :: istatus

  !---------------------------------------------------------------------


!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!-----------------------------------------------------------------------
!
!  Surface Station variables and Surface (single-level) read-in observation variables
!
!-----------------------------------------------------------------------

    ALLOCATE( isrcsng(mx_sng), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:isrcsng")
    ALLOCATE( icatsng(mx_sng), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:icatsng")
    ALLOCATE( latsng(mx_sng,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:latsng")
    ALLOCATE( lonsng(mx_sng,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:longsng")
    ALLOCATE( hgtsng(mx_sng,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:hgtsng")
    ALLOCATE( xsng(mx_sng), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:xsng")
    ALLOCATE( ysng(mx_sng), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:ysng")
    ALLOCATE( trnsng(mx_sng), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:trnsng")
    ALLOCATE( timesng(mx_sng,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:timesng")
    ALLOCATE( stnsng(mx_sng,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:stnsng")

    ALLOCATE( wx(mx_sng,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:wx")
    ALLOCATE( csrcsng(mx_sng,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:csrcsng")
    ALLOCATE( store_emv(mx_sng,5,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:store_emv")
    ALLOCATE( store_amt(mx_sng,5,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:store_amt")
    ALLOCATE( kloud(mx_sng,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:kloud")
    ALLOCATE( idp3(mx_sng,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:kdp3")
    ALLOCATE( store_hgt(mx_sng,5,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:store_hgt")
    ALLOCATE( obrdsng(mx_sng,nvar_sng,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:obrdsng")

    ALLOCATE( obsng  (nvar_anx,mx_sng), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:obsng")
    ALLOCATE( odifsng(nvar_anx,mx_sng), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:odifsng")
    ALLOCATE( oanxsng(nvar_anx,mx_sng), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:oanxsng")

    ALLOCATE( thesng(mx_sng), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:thesng")

!-----------------------------------------------------------------------
!
!  Upper Air Station variables
!
!-----------------------------------------------------------------------
!
    ALLOCATE( isrcua(mx_ua), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:isrcua")
    ALLOCATE( xua(mx_ua), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:xua")
    ALLOCATE( yua(mx_ua), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:yua")
    ALLOCATE( trnua(mx_ua), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:trnua")
    ALLOCATE( elevua(mx_ua), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:elevua")
    ALLOCATE( hgtua(nz_ua,mx_ua), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:hgtua")
    ALLOCATE( nlevsua(mx_ua), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:nlevsua")
    ALLOCATE( stnua(mx_ua), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:stnua")

!
!-----------------------------------------------------------------------
!
!  Upper-air observation variables
!
!-----------------------------------------------------------------------
!
    ALLOCATE( obsua (nvar_anx,nz_ua,mx_ua), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:obsua")
    ALLOCATE( odifua(nvar_anx,nz_ua,mx_ua), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:odifua")
    ALLOCATE( oanxua(nvar_anx,nz_ua,mx_ua), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:oanxua")
    ALLOCATE( theua(nz_ua,mx_ua), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:theua")
!
!-----------------------------------------------------------------------
!
!  Radar site variables
!
!  Radial velocity processing is less common then other data types.  If
!  we're not doing it, don't allocated unnecessary variables, especially
!  the really big ones.  The critical ones are stil left.
!
!  Moved to module_radarobs.f90
!
!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!
! Different handling of ADAS and ARPS 3dvar
!
!-----------------------------------------------------------------------

    IF (progname == 'ADAS') THEN
      ALLOCATE( corsng(nvar_anx,mx_sng), STAT = istatus )
      CALL check_alloc_status(istatus, "adas:corsng")
      !ALLOCATE(  corrad(nvar_rad,nz_rdr,mx_colrad) , STAT = istatus )
      !CALL check_alloc_status(istatus, "adas:corrad")
      ALLOCATE( corua(nvar_anx,nz_ua,mx_ua), STAT = istatus )
      CALL check_alloc_status(istatus, "adas:corua")
    ELSE
      ALLOCATE( corsng(mx_sng,nvar_anx), STAT = istatus )
      CALL check_alloc_status(istatus, "adas:corsng")
      ALLOCATE( corua(nz_ua,mx_ua,nvar_anx), STAT = istatus )
      CALL check_alloc_status(istatus, "adas:corua")
    END IF

!-----------------------------------------------------------------------
!
! Quality control variables
!
!-----------------------------------------------------------------------

    ALLOCATE( qualrdsng(mx_sng,nvar_sng,ntime), STAT = istatus )
    CALL check_alloc_status(istatus, "adas:qualrdsng")
    ALLOCATE( qualsng(nvar_anx,mx_sng),    STAT = istatus )
    CALL check_alloc_status(istatus, "adas:qualsng")
    ALLOCATE( qobsng(nvar_anx,mx_sng),     STAT = istatus )
    CALL check_alloc_status(istatus, "adas:qobsng")

    ALLOCATE( qualua(nvar_anx,nz_ua,mx_ua),    STAT = istatus )
    CALL check_alloc_status(istatus, "adas:qualua")
    ALLOCATE(qobsua (nvar_anx,nz_ua,mx_ua),    STAT = istatus )
    CALL check_alloc_status(istatus, "adas:qobsua")

!
!-----------------------------------------------------------------------
!
!  other Variables  !9/6/2013 by  Guoqing Ge
!
!-----------------------------------------------------------------------
!
    ALLOCATE( qcthrret(nvar_anx,nsrc_ret) )
    ALLOCATE( qobsret(nvar_anx,nz_ret,mx_colret) )
    ALLOCATE( qualret(nvar_anx,nz_ret,mx_colret) )

    ALLOCATE( qcthrua(nvar_anx,nsrc_ua) )
    ALLOCATE( barqclim(nvar_anx,nsrc_sng) )
    ALLOCATE( qcthrsng(nvar_anx,nsrc_sng) )
    ALLOCATE( qsrcret(nvar_anx,nz_tab,nsrc_ret) )
    ALLOCATE( qsrcua (nvar_anx,nz_tab,nsrc_ua) )
    ALLOCATE( qsrcsng(nvar_anx,nsrc_sng) )
    ALLOCATE( qback  (nvar_anx,nz_tab), STAT = istatus )
    CALL check_alloc_status(istatus, "allocate_adas_3dvar_arrays:qback")

    ALLOCATE( oanxret(nvar_anx,nz_ret,mx_colret) )
    ALLOCATE( odifret(nvar_anx,nz_ret,mx_colret) )
    ALLOCATE( obsret (nvar_anx,nz_ret,mx_colret) )

    qcthrret = 0.0
    qobsret  = 0.0
    qualret  = 0.0
    qcthrua  = 0.0
    barqclim = 0.0
    qcthrsng = 0.0
    qsrcret  = 0.0
    qsrcua   = 0.0
    qsrcsng  = 0.0
    qback    = 0.0
    oanxret  = 0.0
    odifret  = 0.0
    obsret   = 0.0

    RETURN
  END SUBROUTINE allocate_3dvarObs_arrays

  !
  ! DEALLOCATE
  !
  SUBROUTINE deallocate_3dvarObs_arrays( istatus )

    IMPLICIT NONE
    INTEGER, INTENT(OUT) :: istatus

!
!-----------------------------------------------------------------------
!
!  Surface Station variables
!
!-----------------------------------------------------------------------
!
    DEALLOCATE( isrcsng, STAT = istatus )
    DEALLOCATE( icatsng, STAT = istatus )
    DEALLOCATE( latsng,  STAT = istatus )
    DEALLOCATE( lonsng,  STAT = istatus )
    DEALLOCATE( hgtsng,  STAT = istatus )
    DEALLOCATE( xsng,    STAT = istatus )
    DEALLOCATE( ysng,    STAT = istatus )
    DEALLOCATE( trnsng,  STAT = istatus )
    DEALLOCATE( timesng, STAT = istatus )
    DEALLOCATE( stnsng,  STAT = istatus )
!
!-----------------------------------------------------------------------
!
!  Surface (single-level) read-in observation variables
!
!-----------------------------------------------------------------------
!
    DEALLOCATE( wx,        STAT = istatus )
    DEALLOCATE( csrcsng,   STAT = istatus )
    DEALLOCATE( store_emv, STAT = istatus )
    DEALLOCATE( store_amt, STAT = istatus )
    DEALLOCATE( kloud,     STAT = istatus )
    DEALLOCATE( store_hgt, STAT = istatus )
    DEALLOCATE( obrdsng,   STAT = istatus )

    DEALLOCATE( obsng  ,   STAT = istatus )
    DEALLOCATE( odifsng,   STAT = istatus )
    DEALLOCATE( oanxsng,   STAT = istatus )
    DEALLOCATE( corsng,    STAT = istatus )
    DEALLOCATE( thesng,    STAT = istatus )

!-----------------------------------------------------------------------
!
!  Upper Air Station variables
!
!-----------------------------------------------------------------------
!
    DEALLOCATE( isrcua,  STAT = istatus )
    DEALLOCATE( xua,     STAT = istatus )
    DEALLOCATE( yua,     STAT = istatus )
    DEALLOCATE( trnua,   STAT = istatus )
    DEALLOCATE( elevua,  STAT = istatus )
    DEALLOCATE( hgtua,   STAT = istatus )
    DEALLOCATE( nlevsua, STAT = istatus )
    DEALLOCATE( stnua,   STAT = istatus )

!
!-----------------------------------------------------------------------
!
!  Upper-air observation variables
!
!-----------------------------------------------------------------------
!
    DEALLOCATE( obsua , STAT = istatus )
    DEALLOCATE( odifua, STAT = istatus )
    DEALLOCATE( oanxua, STAT = istatus )
    DEALLOCATE( corua,  STAT = istatus )
    DEALLOCATE( theua,  STAT = istatus )

!
!-----------------------------------------------------------------------
!
!  Quality Control Variables
!
!-----------------------------------------------------------------------
!
    DEALLOCATE( qualrdsng, STAT = istatus )
    DEALLOCATE( qualsng, qobsng,    STAT = istatus )
    DEALLOCATE( qualua,  qobsua,    STAT = istatus )
!
!-----------------------------------------------------------------------
!
!  other Variables  !9/6/2013 by  Guoqing Ge
!
!-----------------------------------------------------------------------
!
    DEALLOCATE( qcthrret, qsrcret, obsret, qobsret, qualret, oanxret, odifret )
    DEALLOCATE( qcthrua,  qsrcua   )
    DEALLOCATE( qcthrsng, qsrcsng  )
    DEALLOCATE( barqclim, qback )

    RETURN
  END SUBROUTINE deallocate_3dvarObs_arrays

END MODULE module_analysisArrays
