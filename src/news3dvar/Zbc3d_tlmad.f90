!
!  Coding ADJSUBROUTINE
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADBCU                      ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adbcu(nx,ny,nz,dtsml,                                        &
           u, udteb,udtwb,udtnb,udtsb,                                  &
           ebc,wbc,nbc,sbc,tbc,bbc)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Forform the adjoint of
!  Set the boundary conditions for the u-velocity component. Please
!  note that the values at the corner points may depend on the order
!  that e-w, n-s and t-b boundary conditions are applied.
!
   !!!!!!!For radiation and external bcs, lateral bcs are not done
   !!!!!!!in this code since ebc,wbc,nbc,sbc are set to zero.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR: Jidong GAO
!  05/10/96.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    dtsml    The small time step size (s)
!
!    u        Interior domain values of u-velocity at tfuture (m/s)
!
!    udteb    Time tendency of the u field at the east boundary
!    udtwb    Time tendency of the u field at the west boundary
!    udtnb    Time tendency of the u field at the north boundary
!    udtsb    Time tendency of the u field at the south boundary
!
!    ebc      Parameter defining east   boundary condition type.
!    wbc      Parameter defining west   boundary condition type.
!    nbc      Parameter defining north  boundary condition type.
!    sbc      Parameter defining south  boundary condition type.
!    tbc      Parameter defining top    boundary condition type.
!    bbc      Parameter defining bottom boundary condition type.
!
!  OUTPUT:
!
!    u        The u-velocity over the entire domain at tfuture (m/s)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in x, y and z
                               ! directions

  REAL :: dtsml                ! The small time step size (s)

  REAL :: u  (nx,ny,nz)        ! Total u-velocity at tfuture (m/s)

  REAL :: udteb (ny,nz)        ! Time tendency of u at east boundary
  REAL :: udtwb (ny,nz)        ! Time tendency of u at west boundary
  REAL :: udtnb (nx,nz)        ! Time tendency of u at north boundary
  REAL :: udtsb (nx,nz)        ! Time tendency of u at south boundary
!
!-----------------------------------------------------------------------
!
!  The following integer parameters define the type of condition
!  at each boundary.
!
!   1 for rigid wall (mirror) type boundary condition.
!   2 for periodic boundary condition.
!   3 for zero normal gradient boundary condition.
!   4 for open (radiation) boundary condition.
!   5 for user (externally) specified boundary condition.
!
!-----------------------------------------------------------------------
!
  INTEGER :: ebc               ! Parameter defining east   boundary
                               ! condition type.
  INTEGER :: wbc               ! Parameter defining west   boundary
                               ! condition type.
  INTEGER :: nbc               ! Parameter defining north  boundary
                               ! condition type.
  INTEGER :: sbc               ! Parameter defining south  boundary
                               ! condition type.
  INTEGER :: tbc               ! Parameter defining top    boundary
                               ! condition type.
  INTEGER :: bbc               ! Parameter defining bottom boundary
                               ! condition type.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Set the west boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(wbc == 0) GO TO 5001

  IF(wbc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO j=1,ny-1
        u(1,j,k)=-u(3,j,k)
      END DO
    END DO

  ELSE IF(wbc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO j=1,ny-1
        u(1,j,k)=u(nx-2,j,k)
      END DO
    END DO

  ELSE IF(wbc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO j=1,ny-1
        u(1,j,k)=u(3,j,k)
      END DO
    END DO

  ELSE IF(wbc == 4) THEN
                                    ! Radiation condition.
    DO k=2,nz-2
      DO j=2,ny-2
        u(1,j,k)=u(1,j,k)+udtwb(j,k)*dtsml
      END DO
    END DO

  ELSE IF(wbc == 5 .OR. wbc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-2
      DO j=1,ny-1
        u(1,j,k)=u(3,j,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCU', wbc
    STOP

  END IF

  5001  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the east boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(ebc == 0) GO TO 5002

  IF(ebc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO j=1,ny-1
        u(nx,j,k)=-u(nx-2,j,k)
      END DO
    END DO

  ELSE IF(ebc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO j=1,ny-1
        u(nx,j,k)=u(3,j,k)
      END DO
    END DO

  ELSE IF(ebc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO j=1,ny-1
        u(nx,j,k)=u(nx-2,j,k)
      END DO
    END DO

  ELSE IF(ebc == 4) THEN
                                    ! Radiation condition.
    DO k=2,nz-2
      DO j=2,ny-2
        u(nx,j,k)=u(nx,j,k)+udteb(j,k)*dtsml
      END DO
    END DO

  ELSE IF(ebc == 5 .OR. ebc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-2
      DO j=1,ny-1
        u(nx,j,k)=u(nx-2,j,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCU', ebc
    STOP

  END IF

  5002  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the north boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(nbc == 0) GO TO 5003

  IF(nbc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO i=1,nx
        u(i,ny-1,k)=u(i,ny-2,k)
      END DO
    END DO

  ELSE IF(nbc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO i=1,nx
        u(i,ny-1,k)=u(i,2,k)
      END DO
    END DO

  ELSE IF(nbc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO i=1,nx
        u(i,ny-1,k)=u(i,ny-2,k)
      END DO
    END DO

  ELSE IF(nbc == 4) THEN
                                    ! Radiation condition.
    DO k=2,nz-2
      DO i=2,nx-1
        u(i,ny-1,k)=u(i,ny-1,k)+udtnb(i,k)*dtsml
      END DO
    END DO

  ELSE IF(nbc == 5 .OR. nbc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-2
      DO i=1,nx
        u(i,ny-1,k)=u(i,ny-2,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCU', nbc
    STOP

  END IF

  5003  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the south boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(sbc == 0) GO TO 5004

  IF(sbc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO i=1,nx
        u(i,1,k)=u(i,2,k)
      END DO
    END DO

  ELSE IF(sbc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO i=1,nx
        u(i,1,k)=u(i,ny-2,k)
      END DO
    END DO

  ELSE IF(sbc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO i=1,nx
        u(i,1,k)=u(i,2,k)
      END DO
    END DO

  ELSE IF(sbc == 4) THEN         ! Radiation condition.

    DO k=2,nz-2
      DO i=2,nx-1
        u(i,1,k)=u(i,1,k)+udtsb(i,k)*dtsml
      END DO
    END DO

  ELSE IF(sbc == 5 .OR. sbc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-2
      DO i=1,nx
        u(i,1,k)=u(i,2,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCU', sbc
    STOP

  END IF

  5004  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the southwest corner based on the
!  boundary condition types on the south and west boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF((sbc == 4.OR.sbc == 0).AND.wbc == 4) THEN

    DO k=2,nz-2
      u(1,1,k)=u(1,1,k)+udtwb(1,k)*dtsml
    END DO

  ELSE IF((sbc == 1.OR.sbc == 3.OR.sbc == 5).AND.wbc == 4) THEN

    DO k=2,nz-2
      u(1,1,k)=u(1,2,k)
    END DO

  ELSE IF(sbc == 2.AND.wbc == 4) THEN

    DO k=2,nz-2
      u(1,1,k)=u(1,ny-2,k)
    END DO

  ELSE IF(sbc == 4.AND.wbc == 1) THEN

    DO k=2,nz-2
      u(1,1,k)=-u(3,1,k)
    END DO

  ELSE IF(sbc == 4.AND.wbc == 2) THEN

    DO k=2,nz-2
      u(1,1,k)=u(nx-2,1,k)
    END DO

  ELSE IF(sbc == 4.AND.(wbc == 3.OR.wbc == 5)) THEN

    DO k=2,nz-2
      u(1,1,k)=u(3,1,k)
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the southeast corner based on the
!  boundary condition types on the south and east boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF((sbc == 4.OR.sbc == 0).AND.ebc == 4) THEN

    DO k=2,nz-2
      u(nx,1,k)=u(nx,1,k)+udteb(1,k)*dtsml
    END DO

  ELSE IF((sbc == 1.OR.sbc == 3.OR.sbc == 5).AND.ebc == 4) THEN

    DO k=2,nz-2
      u(nx,1,k)=u(nx,2,k)
    END DO

  ELSE IF(sbc == 2.AND.ebc == 4) THEN

    DO k=2,nz-2
      u(nx,1,k)=u(nx,ny-2,k)
    END DO

  ELSE IF(sbc == 4.AND.ebc == 1) THEN

    DO k=2,nz-2
      u(nx,1,k)=-u(nx-2,1,k)
    END DO

  ELSE IF(sbc == 4.AND.ebc == 2) THEN

    DO k=2,nz-2
      u(nx,1,k)=u(3,1,k)
    END DO

  ELSE IF(sbc == 4.AND.(ebc == 3.OR.ebc == 5)) THEN

    DO k=2,nz-2
      u(nx,1,k)=u(nx-2,1,k)
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the northwest corner based on the
!  boundary condition types on the north and west boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF((nbc == 4.OR.nbc == 0).AND.wbc == 4) THEN

    DO k=2,nz-2
      u(1,ny-1,k)=u(1,ny-1,k)+udtwb(ny-1,k)*dtsml
    END DO

  ELSE IF((nbc == 1.OR.nbc == 3.OR.nbc == 5).AND.wbc == 4) THEN

    DO k=2,nz-2
      u(1,ny-1,k)=u(1,ny-2,k)
    END DO

  ELSE IF(nbc == 2.AND.wbc == 4) THEN

    DO k=2,nz-2
      u(1,ny-1,k)=u(1,2,k)
    END DO

  ELSE IF(nbc == 4.AND.wbc == 1) THEN

    DO k=2,nz-2
      u(1,ny-1,k)=-u(3,ny-1,k)
    END DO

  ELSE IF(nbc == 4.AND.wbc == 2) THEN

    DO k=2,nz-2
      u(1,ny-1,k)=u(nx-2,ny-1,k)
    END DO

  ELSE IF(nbc == 4.AND.(wbc == 3.OR.wbc == 5)) THEN

    DO k=2,nz-2
      u(1,ny-1,k)=u(3,ny-1,k)
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the northeast corner based on the
!  boundary condition types on the north and east boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF((nbc == 4.OR.nbc == 0).AND.ebc == 4) THEN

    DO k=2,nz-2
      u(nx,ny-1,k)=u(nx,ny-1,k)+udteb(ny-1,k)*dtsml
    END DO

  ELSE IF((nbc == 1.OR.nbc == 3.OR.nbc == 5).AND.ebc == 4) THEN

    DO k=2,nz-2
      u(nx,ny-1,k)=u(nx,ny-2,k)
    END DO

  ELSE IF(nbc == 2.AND.ebc == 4) THEN

    DO k=2,nz-2
      u(nx,ny-1,k)=u(nx,2,k)
    END DO

  ELSE IF(nbc == 4.AND.ebc == 1) THEN

    DO k=2,nz-2
      u(nx,ny-1,k)=-u(nx-2,ny-1,k)
    END DO

  ELSE IF(nbc == 4.AND.ebc == 2) THEN

    DO k=2,nz-2
      u(nx,ny-1,k)=u(3,ny-1,k)
    END DO

  ELSE IF(nbc == 4.AND.(ebc == 3.OR.ebc == 5)) THEN

    DO k=2,nz-2
      u(nx,ny-1,k)=u(nx-2,ny-1,k)
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Set the top boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(tbc == 0) GO TO 5005

  IF(tbc == 1) THEN             ! Rigid lid boundary condition

    DO j=1,ny-1
      DO i=1,nx
!      u(i,j,nz-1)=u(i,j,nz-2)
        u(i,j,nz-2)=u(i,j,nz-2)+u(i,j,nz-1)
        u(i,j,nz-1)=0.0
      END DO
    END DO

  ELSE IF(tbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny-1
      DO i=1,nx
        u(i,j,nz-1)=u(i,j,2)
      END DO
    END DO

  ELSE IF(tbc == 3) THEN         ! Zero normal gradient condition.

    DO j=1,ny-1
      DO i=1,nx
        u(i,j,nz-1)=u(i,j,nz-2)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCU', tbc
    STOP

  END IF

  5005  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the bottom boundary conditions
!
!-----------------------------------------------------------------------
!

  IF(bbc == 0) GO TO 5006

  IF(bbc == 1) THEN             ! Non-penetrative ground condition

    DO j=1,ny-1
      DO i=1,nx
!      u(i,j,1)=u(i,j,2)
        u(i,j,2)=u(i,j,2)+u(i,j,1)
        u(i,j,1)=0.
      END DO
    END DO

  ELSE IF(bbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny-1
      DO i=1,nx
        u(i,j,1)=u(i,j,nz-2)
      END DO
    END DO

  ELSE IF(bbc == 3) THEN         ! Zero normal gradient condition.

    DO j=1,ny-1
      DO i=1,nx
        u(i,j,1)=u(i,j,2)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCU', bbc
    STOP

  END IF

  5006  CONTINUE

  RETURN

  900   FORMAT(1X,'Invalid boundary condition option found in ',a,      &
               /1X,'The option was ',i3,' Job stopped.')
END SUBROUTINE adbcu
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADBCV                      ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adbcv(nx,ny,nz,dtsml,                                        &
           v, vdteb,vdtwb,vdtnb,vdtsb,                                  &
           ebc,wbc,nbc,sbc,tbc,bbc)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!    Perform the adjoint of
!  Set the boundary conditions for the v-velocity component. Please note
!  that the values at the corner points may depend on the order that e-w,
!  n-s and t-b boundary conditions are applied.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR: Jidong Gao
!  05/20/96.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    dtsml    The small time step size (s)
!
!    v        Interior domain values of v-velocity at tfuture (m/s)
!
!    vdteb    Time tendency of the v field at the east boundary
!    vdtwb    Time tendency of the v field at the west boundary
!    vdtnb    Time tendency of the v field at the north boundary
!    vdtsb    Time tendency of the v field at the south boundary
!
!    ebc      Parameter defining east   boundary condition type.
!    wbc      Parameter defining west   boundary condition type.
!    nbc      Parameter defining north  boundary condition type.
!    sbc      Parameter defining south  boundary condition type.
!    tbc      Parameter defining top    boundary condition type.
!    bbc      Parameter defining bottom boundary condition type.
!
!  OUTPUT:
!
!    v        The v-velocity over the entire domain at tfuture (m/s)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in x, y and z
                               ! directions

  REAL :: dtsml                ! The small time step size (s)

  REAL :: v     (nx,ny,nz)     ! Total v-velocity at tfuture (m/s)

  REAL :: vdteb (ny,nz)        ! Time tendency of v at east boundary
  REAL :: vdtwb (ny,nz)        ! Time tendency of v at west boundary
  REAL :: vdtnb (nx,nz)        ! Time tendency of v at north boundary
  REAL :: vdtsb (nx,nz)        ! Time tendency of v at south boundary
!
!-----------------------------------------------------------------------
!
!  The following integer parameters define the type of condition
!  at each boundary.
!
!   1 for rigid wall (mirror) type boundary condition.
!   2 for periodic boundary condition.
!   3 for zero normal gradient boundary condition.
!   4 for open (radiation) boundary condition.
!   5 for user (externally) specified boundary condition.
!
!-----------------------------------------------------------------------
!
  INTEGER :: ebc               ! Parameter defining east   boundary
                               ! condition type.
  INTEGER :: wbc               ! Parameter defining west   boundary
                               ! condition type.
  INTEGER :: nbc               ! Parameter defining north  boundary
                               ! condition type.
  INTEGER :: sbc               ! Parameter defining south  boundary
                               ! condition type.
  INTEGER :: tbc               ! Parameter defining top    boundary
                               ! condition type.
  INTEGER :: bbc               ! Parameter defining bottom boundary
                               ! condition type.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Set the west boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(wbc == 0) GO TO 5001

  IF(wbc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO j=1,ny
        v(1,j,k)=v(2,j,k)
      END DO
    END DO

  ELSE IF(wbc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO j=1,ny
        v(1,j,k)=v(nx-2,j,k)
      END DO
    END DO

  ELSE IF(wbc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO j=1,ny
        v(1,j,k)=v(2,j,k)
      END DO
    END DO

  ELSE IF(wbc == 4) THEN
                                    ! Radiation condition.
    DO k=2,nz-2
      DO j=2,ny-1
        v(1,j,k)=v(1,j,k)+vdtwb(j,k)*dtsml
      END DO
    END DO

  ELSE IF(wbc == 5 .OR. wbc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-2
      DO j=1,ny
        v(1,j,k)=v(2,j,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCV', wbc
    STOP

  END IF

  5001  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the east boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(ebc == 0) GO TO 5002

  IF(ebc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO j=1,ny
        v(nx-1,j,k)=v(nx-2,j,k)
      END DO
    END DO

  ELSE IF(ebc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO j=1,ny
        v(nx-1,j,k)=v(2,j,k)
      END DO
    END DO

  ELSE IF(ebc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO j=1,ny
        v(nx-1,j,k)=v(nx-2,j,k)
      END DO
    END DO

  ELSE IF(ebc == 4) THEN
                                    ! Radiation condition.
    DO k=2,nz-2
      DO j=2,ny-1
        v(nx-1,j,k)=v(nx-1,j,k)+vdteb(j,k)*dtsml
      END DO
    END DO

  ELSE IF(ebc == 5 .OR. ebc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-2
      DO j=1,ny
        v(nx-1,j,k)=v(nx-2,j,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCV', ebc
    STOP

  END IF

  5002  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the north boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(nbc == 0) GO TO 5003

  IF(nbc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO i=1,nx-1
        v(i,ny,k)=-v(i,ny-2,k)
      END DO
    END DO

  ELSE IF(nbc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO i=1,nx-1
        v(i,ny,k)=v(i,3,k)
      END DO
    END DO

  ELSE IF(nbc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO i=1,nx-1
        v(i,ny,k)=v(i,ny-2,k)
      END DO
    END DO

  ELSE IF(nbc == 4) THEN         ! Radiation condition.

    DO k=2,nz-2
      DO i=2,nx-2
        v(i,ny,k)=v(i,ny,k)+vdtnb(i,k)*dtsml
      END DO
    END DO

  ELSE IF(nbc == 5 .OR. nbc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-2
      DO i=1,nx-1
        v(i,ny,k)=v(i,ny-2,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCV', nbc
    STOP

  END IF

  5003  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the south boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(sbc == 0) GO TO 5004

  IF(sbc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO i=1,nx-1
        v(i,1,k)=-v(i,3,k)
      END DO
    END DO

  ELSE IF(sbc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO i=1,nx-1
        v(i,1,k)=v(i,ny-2,k)
      END DO
    END DO

  ELSE IF(sbc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO i=1,nx-1
        v(i,1,k)=v(i,3,k)
      END DO
    END DO

  ELSE IF(sbc == 4) THEN
                                    ! Radiation condition.
    DO k=2,nz-2
      DO i=2,nx-2
        v(i,1,k)=v(i,1,k)+vdtsb(i,k)*dtsml
      END DO
    END DO

  ELSE IF(sbc == 5 .OR. sbc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-2
      DO i=1,nx-1
        v(i,1,k)=v(i,3,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCV', sbc
    STOP

  END IF

  5004  CONTINUE

!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the southwest corner based on the
!  boundary condition types on the south and west boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF(sbc == 4.AND.(wbc == 4.OR.wbc == 0)) THEN

    DO k=2,nz-2
      v(1,1,k)=v(1,1,k)+vdtsb(1,k)*dtsml
    END DO

  ELSE IF(sbc == 1.AND.wbc == 4) THEN

    DO k=2,nz-2
      v(1,1,k)=-v(1,3,k)
    END DO

  ELSE IF(sbc == 2.AND.wbc == 4) THEN

    DO k=2,nz-2
      v(1,1,k)=v(1,ny-2,k)
    END DO

  ELSE IF((sbc == 3.OR.sbc == 5).AND.wbc == 4) THEN

    DO k=2,nz-2
      v(1,1,k)=v(1,3,k)
    END DO

  ELSE IF(sbc == 4.AND.(wbc == 1.OR.wbc == 3.OR.wbc == 5)) THEN

    DO k=2,nz-2
      v(1,1,k)=v(2,1,k)
    END DO

  ELSE IF(sbc == 4.AND.wbc == 2) THEN

    DO k=2,nz-2
      v(1,1,k)=v(nx-2,1,k)
    END DO

  END IF
!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the southeast corner based on the
!  boundary condition types on the south and east boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF(sbc == 4.AND.(ebc == 4.OR.ebc == 0)) THEN

    DO k=2,nz-2
      v(nx-1,1,k)=v(nx-1,1,k)+vdtsb(nx-1,k)*dtsml
    END DO

  ELSE IF(sbc == 1.AND.ebc == 4) THEN

    DO k=2,nz-2
      v(nx-1,1,k)=-v(nx-1,3,k)
    END DO

  ELSE IF(sbc == 2.AND.ebc == 4) THEN

    DO k=2,nz-2
      v(nx-1,1,k)=v(nx-1,ny-2,k)
    END DO

  ELSE IF((sbc == 3.OR.sbc == 5).AND.ebc == 4) THEN

    DO k=2,nz-2
      v(nx-1,1,k)=v(nx-1,3,k)
    END DO

  ELSE IF(sbc == 4.AND.(ebc == 1.OR.ebc == 3.OR.ebc == 5)) THEN

    DO k=2,nz-2
      v(nx-1,1,k)=v(nx-2,1,k)
    END DO

  ELSE IF(sbc == 4.AND.ebc == 2) THEN

    DO k=2,nz-2
      v(nx-1,1,k)=v(2,1,k)
    END DO

  END IF
!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the northwest corner based on the
!  boundary condition types on the north and west boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF(nbc == 4.AND.(wbc == 4.OR.wbc == 0)) THEN

    DO k=2,nz-2
      v(1,ny,k)=v(1,ny,k)+vdtnb(1,k)*dtsml
    END DO

  ELSE IF(nbc == 1.AND.wbc == 4) THEN

    DO k=2,nz-2
      v(1,ny,k)=-v(1,ny-2,k)
    END DO

  ELSE IF(nbc == 2.AND.wbc == 4) THEN

    DO k=2,nz-2
      v(1,ny,k)=v(1,3,k)
    END DO

  ELSE IF((nbc == 3.OR.nbc == 5).AND.wbc == 4) THEN

    DO k=2,nz-2
      v(1,ny,k)=v(1,ny-2,k)
    END DO

  ELSE IF(nbc == 4.AND.(wbc == 1.OR.wbc == 3.OR.wbc == 5)) THEN

    DO k=2,nz-2
      v(1,ny,k)=v(2,ny,k)
    END DO

  ELSE IF(nbc == 4.AND.wbc == 2) THEN

    DO k=2,nz-2
      v(1,ny,k)=v(nx-2,ny,k)
    END DO

  END IF
!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the northeast corner based on the
!  boundary condition types on the north and east boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF(nbc == 4.AND.(ebc == 4.OR.ebc == 0)) THEN

    DO k=2,nz-2
      v(nx-1,ny,k)=v(nx-1,ny,k)+vdtnb(nx-1,k)*dtsml
    END DO

  ELSE IF(nbc == 1.AND.ebc == 4) THEN

    DO k=2,nz-2
      v(nx-1,ny,k)=-v(nx-1,ny-2,k)
    END DO

  ELSE IF(nbc == 2.AND.ebc == 4) THEN

    DO k=2,nz-2
      v(nx-1,ny,k)=v(nx-1,3,k)
    END DO

  ELSE IF((nbc == 3.OR.nbc == 5).AND.ebc == 4) THEN

    DO k=2,nz-2
      v(nx-1,ny,k)=v(nx-1,ny-2,k)
    END DO

  ELSE IF(nbc == 4.AND.(ebc == 1.OR.ebc == 3.OR.ebc == 5)) THEN

    DO k=2,nz-2
      v(nx-1,ny,k)=v(nx-2,ny,k)
    END DO

  ELSE IF(nbc == 4.AND.ebc == 2) THEN

    DO k=2,nz-2
      v(nx-1,ny,k)=v(2,ny,k)
    END DO

  END IF
!
!-----------------------------------------------------------------------
!
!  Set the top boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(tbc == 0) GO TO 5005

  IF(tbc == 1) THEN             ! Rigid lid boundary condition

    DO j=1,ny
      DO i=1,nx-1
!      v(i,j,nz-1)=v(i,j,nz-2)
        v(i,j,nz-2)=v(i,j,nz-2)+v(i,j,nz-1)
        v(i,j,nz-1)=0.
      END DO
    END DO

  ELSE IF(tbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny
      DO i=1,nx-1
        v(i,j,nz-1)=v(i,j,2)
      END DO
    END DO

  ELSE IF(tbc == 3) THEN         ! Zero normal gradient condition.

    DO j=1,ny
      DO i=1,nx-1
        v(i,j,nz-1)=v(i,j,nz-2)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCV', tbc
    STOP

  END IF

  5005  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the bottom boundary conditions
!
!-----------------------------------------------------------------------
!

  IF(bbc == 0) GO TO 5006

  IF(bbc == 1) THEN             ! Non-penetrative ground condition

    DO j=1,ny
      DO i=1,nx-1
!      v(i,j,1)=v(i,j,2)
        v(i,j,2)=v(i,j,2)+v(i,j,1)
        v(i,j,1)=0.0
      END DO
    END DO

  ELSE IF(bbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny
      DO i=1,nx-1
        v(i,j,1)=v(i,j,nz-2)
      END DO
    END DO

  ELSE IF(bbc == 3) THEN         ! Zero normal gradient condition.

    DO j=1,ny
      DO i=1,nx-1
        v(i,j,1)=v(i,j,2)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCV', bbc
    STOP

  END IF

  5006  CONTINUE

  RETURN

  900   FORMAT(1X,'Invalid boundary condition option found in ',a,      &
               /1X,'The option was ',i3,' Job stopped.')
END SUBROUTINE adbcv
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADLBCW                     ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adlbcw(nx,ny,nz,dtsml,                                       &
           w,wcont,wdteb,wdtwb,wdtnb,wdtsb,                             &
           ebc,wbc,nbc,sbc)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint of
!  Set the lateral boundary conditions for the w-velocity component.
!  Please note that the values at the corner points may depend on
!  the order that e-w and n-s boundary conditions are applied.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR: Jidong Gao
!  05/10/96.
!
!  MODIFICATION HISTORY:
!
!  Separated the application of external and user-supplied BC
!  processing from radiation BC.  Corrected application of
!  rigid or zero-gradient conditions mixed with radiation
!  conditions at SE corner.
!
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    dtsml    The small time step size (s)
!
!    w        Interior domain values of w-velocity at tfuture (m/s)
!    wcont    Contravariant vertical velocity (m/s)
!
!    wdteb    Time tendency of the w field at the east boundary
!    wdtwb    Time tendency of the w field at the west boundary
!    wdtnb    Time tendency of the w field at the north boundary
!    wdtsb    Time tendency of the w field at the south boundary
!
!    ebc      Parameter defining east   boundary condition type.
!    wbc      Parameter defining west   boundary condition type.
!    nbc      Parameter defining north  boundary condition type.
!    sbc      Parameter defining south  boundary condition type.
!
!  OUTPUT:
!
!    w        The w-velocity over the entire domain at tfuture (m/s)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in x, y and z
                               ! directions

  REAL :: dtsml                ! The small time step size (s)

  REAL :: w     (nx,ny,nz)     ! Total w-velocity at tfuture (m/s)
  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)

  REAL :: wdteb (ny,nz)        ! Time tendency of w at east boundary
  REAL :: wdtwb (ny,nz)        ! Time tendency of w at west boundary
  REAL :: wdtnb (nx,nz)        ! Time tendency of w at north boundary
  REAL :: wdtsb (nx,nz)        ! Time tendency of w at south boundary
!
!-----------------------------------------------------------------------
!
!  The following integer parameters define the type of condition
!  at each boundary.
!
!   1 for rigid wall (mirror) type boundary condition.
!   2 for periodic boundary condition.
!   3 for zero normal gradient boundary condition.
!   4 for open (radiation) boundary condition.
!   5 for user (externally) specified boundary condition.
!   6 for nested grid.
!
!-----------------------------------------------------------------------
!
  INTEGER :: ebc               ! Parameter defining east   boundary
                               ! condition type.
  INTEGER :: wbc               ! Parameter defining west   boundary
                               ! condition type.
  INTEGER :: nbc               ! Parameter defining north  boundary
                               ! condition type.
  INTEGER :: sbc               ! Parameter defining south  boundary
                               ! condition type.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Set the west boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(wbc == 0) GO TO 5001

  IF(wbc == 1) THEN            ! Rigid wall boundary condition

    DO k=2,nz-1
      DO j=1,ny-1
        w(1,j,k)=w(2,j,k)
      END DO
    END DO

  ELSE IF(wbc == 2) THEN        ! Periodic boundary condition.

    DO k=2,nz-1
      DO j=1,ny-1
        w(1,j,k)=w(nx-2,j,k)
      END DO
    END DO

  ELSE IF(wbc == 3) THEN        ! Zero normal gradient condition.

    DO k=2,nz-1
      DO j=1,ny-1
        w(2,j,k)=w(2,j,k)+w(1,j,k)
        w(1,j,k)=0.
      END DO
    END DO

  ELSE IF(wbc == 4) THEN
                                    ! Radiation condition.
    DO k=2,nz-1
      DO j=2,ny-2
        w(1,j,k)=w(1,j,k)+wdtwb(j,k)*dtsml
      END DO
    END DO

  ELSE IF(wbc == 5 .OR. wbc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-1
      DO j=1,ny-1
        w(1,j,k)=w(2,j,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCW', wbc
    STOP

  END IF

  5001  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the east boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(ebc == 0) GO TO 5002

  IF(ebc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-1
      DO j=1,ny-1
        w(nx-1,j,k)=w(nx-2,j,k)
      END DO
    END DO

  ELSE IF(ebc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-1
      DO j=1,ny-1
        w(nx-1,j,k)=w(2,j,k)
      END DO
    END DO

  ELSE IF(ebc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-1
      DO j=1,ny-1
        w(nx-2,j,k)=w(nx-2,j,k)+w(nx-1,j,k)
        w(nx-1,j,k)=0.
      END DO
    END DO

  ELSE IF(ebc == 4) THEN
                                    ! Radiation condition.
    DO k=2,nz-1
      DO j=2,ny-2
        w(nx-1,j,k)=w(nx-1,j,k)+wdteb(j,k)*dtsml
      END DO
    END DO

  ELSE IF(ebc == 5 .OR. ebc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-1
      DO j=1,ny-1
        w(nx-1,j,k)=w(nx-2,j,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCW', ebc
    STOP

  END IF

  5002  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the north boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(nbc == 0) GO TO 5003

  IF(nbc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-1
      DO i=1,nx-1
        w(i,ny-1,k)=w(i,ny-2,k)
      END DO
    END DO

  ELSE IF(nbc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-1
      DO i=1,nx-1
        w(i,ny-1,k)=w(i,2,k)
      END DO
    END DO

  ELSE IF(nbc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-1
      DO i=1,nx-1
        w(i,ny-2,k)=w(i,ny-2,k)+w(i,ny-1,k)
        w(i,ny-1,k)=0.0
      END DO
    END DO

  ELSE IF(nbc == 4) THEN         ! Radiation condition.

    DO k=2,nz-1
      DO i=2,nx-2
        w(i,ny-1,k)=w(i,ny-1,k)+wdtnb(i,k)*dtsml
      END DO
    END DO

  ELSE IF(nbc == 5 .OR. nbc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-1
      DO i=1,nx-1
        w(i,ny-1,k)=w(i,ny-2,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCW', nbc
    STOP

  END IF

  5003  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the south boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(sbc == 0) GO TO 5004

  IF(sbc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-1
      DO i=1,nx-1
        w(i,1,k)=w(i,2,k)
      END DO
    END DO

  ELSE IF(sbc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-1
      DO i=1,nx-1
        w(i,1,k)=w(i,ny-2,k)
      END DO
    END DO

  ELSE IF(sbc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-1
      DO i=1,nx-1
        w(i,2,k)=w(i,2,k)+w(i,1,k)
        w(i,1,k)=0.
      END DO
    END DO

  ELSE IF(sbc == 4) THEN         ! Radiation condition.

    DO k=2,nz-1
      DO i=2,nx-2
        w(i,1,k)=w(i,1,k)+wdtsb(i,k)*dtsml
      END DO
    END DO

  ELSE IF(sbc == 5 .OR.sbc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-1
      DO i=1,nx-1
        w(i,1,k)=w(i,2,k)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCW', sbc
    STOP

  END IF

  5004  CONTINUE

!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the southwest corner based on the
!  boundary condition types on the south and west boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF(sbc == 4.AND.wbc == 4) THEN

    DO k=2,nz-2
      w(1,1,k)=w(1,1,k)+wdtwb(1,k)*dtsml
    END DO

  ELSE IF((sbc == 1.OR.sbc == 3.OR.sbc == 5).AND.wbc == 4) THEN

    DO k=2,nz-2
      w(1,1,k)=w(1,2,k)
    END DO

  ELSE IF(sbc == 2.AND.wbc == 4) THEN

    DO k=2,nz-2
      w(1,1,k)=w(1,ny-2,k)
    END DO

  ELSE IF(sbc == 4.AND.(wbc == 1.OR.wbc == 3.OR.wbc == 5)) THEN

    DO k=2,nz-2
      w(1,1,k)=w(2,1,k)
    END DO

  ELSE IF(sbc == 4.AND.wbc == 2) THEN

    DO k=2,nz-2
      w(1,1,k)=w(nx-2,1,k)
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the southeast corner based on the
!  boundary condition types on the south and east boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF(sbc == 4.AND.ebc == 4) THEN

    DO k=2,nz-2
      w(nx-1,1,k)=w(nx-1,1,k)+wdteb(1,k)*dtsml
    END DO

  ELSE IF((sbc == 1.OR.sbc == 3.OR.sbc == 5).AND.ebc == 4) THEN

    DO k=2,nz-2
      w(nx-1,1,k)=w(nx-1,2,k)
    END DO

  ELSE IF(sbc == 2.AND.ebc == 4) THEN

    DO k=2,nz-2
      w(nx-1,1,k)=w(nx-1,ny-2,k)
    END DO

  ELSE IF(sbc == 4.AND.(ebc == 1.OR.ebc == 3.OR.ebc == 5)) THEN

    DO k=2,nz-2
      w(nx-1,1,k)=w(nx-2,1,k)
    END DO

  ELSE IF(sbc == 4.AND.ebc == 2) THEN

    DO k=2,nz-2
      w(nx-1,1,k)=w(2,1,k)
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the northwest corner based on the
!  boundary condition types on the north and west boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF(nbc == 4.AND.wbc == 4) THEN

    DO k=2,nz-2
      w(1,ny-1,k)=w(1,ny-1,k)+wdtwb(ny-1,k)*dtsml
    END DO

  ELSE IF((nbc == 1.OR.nbc == 3.OR.nbc == 5).AND.wbc == 4) THEN

    DO k=2,nz-2
      w(1,ny-1,k)=w(1,ny-2,k)
    END DO

  ELSE IF(nbc == 2.AND.wbc == 4) THEN

    DO k=2,nz-2
      w(1,ny-1,k)=w(1,2,k)
    END DO

  ELSE IF(nbc == 4.AND.(wbc == 1.OR.wbc == 3.OR.wbc == 5)) THEN

    DO k=2,nz-2
      w(1,ny-1,k)=w(2,ny-1,k)
    END DO

  ELSE IF(nbc == 4.AND.wbc == 2) THEN

    DO k=2,nz-2
      w(1,ny-1,k)=w(nx-2,ny-1,k)
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the northeast corner based on the
!  boundary condition types on the north and east boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF(nbc == 4.AND.ebc == 4) THEN

    DO k=2,nz-2
      w(nx-1,ny-1,k)=w(nx-1,ny-1,k)+wdteb(ny-1,k)*dtsml
    END DO

  ELSE IF((nbc == 1.OR.nbc == 3.OR.nbc == 5).AND.ebc == 4) THEN

    DO k=2,nz-2
      w(nx-1,ny-1,k)=w(nx-1,ny-2,k)
    END DO

  ELSE IF(nbc == 2.AND.ebc == 4) THEN

    DO k=2,nz-2
      w(nx-1,ny-1,k)=w(nx-1,2,k)
    END DO

  ELSE IF(nbc == 4.AND.(ebc == 1.OR.ebc == 3.OR.ebc == 5)) THEN

    DO k=2,nz-2
      w(nx-1,ny-1,k)=w(nx-2,ny-1,k)
    END DO

  ELSE IF(nbc == 4.AND.ebc == 2) THEN

    DO k=2,nz-2
      w(nx-1,ny-1,k)=w(2,ny-1,k)
    END DO

  END IF

  5006  CONTINUE

  RETURN

  900   FORMAT(1X,'Invalid boundary condition option found in ',a,      &
               /1X,'The option was ',i3,' Job stopped.')
END SUBROUTINE adlbcw
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADBCP                      ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adbcp(nx,ny,nz,dtsml,                                        &
           pprt, pdteb,pdtwb,pdtnb,pdtsb,                               &
           ebc,wbc,nbc,sbc,tbc,bbc)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the Adjoint of
!  Set the boundary conditions for the perturbation pressure.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR: Jidong Gao
!  05/22/96.
!
!  MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    dtsml    The small time step size (s)
!
!    pprt     Interior domain perturbation pressure at tfuture
!             (Pascal)
!
!    pdteb    Time tendency of the pprt field at the east boundary
!    pdtwb    Time tendency of the pprt field at the west boundary
!    pdtnb    Time tendency of the pprt field at the north boundary
!    pdtsb    Time tendency of the pprt field at the south boundary
!
!    ebc      Parameter defining east   boundary condition type.
!    wbc      Parameter defining west   boundary condition type.
!    nbc      Parameter defining north  boundary condition type.
!    sbc      Parameter defining south  boundary condition type.
!    tbc      Parameter defining top    boundary condition type.
!    bbc      Parameter defining bottom boundary condition type.
!
!  OUTPUT:
!
!    pprt     Purturbation pressure over the entire domain at tfuture
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in x, y and z
                               ! directions

  REAL :: dtsml                ! The small time step size (s)

  REAL :: pprt  (nx,ny,nz)     ! Perturbation presure at tfuture
                               ! (Pascal)

  REAL :: pdteb (ny,nz)        ! Time tendency of pprt field at east
                               ! boundary
  REAL :: pdtwb (ny,nz)        ! Time tendency of pprt field at west
                               ! boundary
  REAL :: pdtnb (nx,nz)        ! Time tendency of pprt field at north
                               ! boundary
  REAL :: pdtsb (nx,nz)        ! Time tendency of pprt field at south
                               ! boundary
!
!-----------------------------------------------------------------------
!
!  The following integer parameters define the type of condition
!  at each boundary.
!
!   1 for rigid wall (mirror) type boundary condition.
!   2 for periodic boundary condition.
!   3 for zero normal gradient boundary condition.
!   4 for open (radiation) boundary condition.
!   5 for user (externally) specified boundary condition.
!   6 for nested grid.
!
!-----------------------------------------------------------------------
!
  INTEGER :: ebc               ! Parameter defining east   boundary
                               ! condition type.
  INTEGER :: wbc               ! Parameter defining west   boundary
                               ! condition type.
  INTEGER :: nbc               ! Parameter defining north  boundary
                               ! condition type.
  INTEGER :: sbc               ! Parameter defining south  boundary
                               ! condition type.
  INTEGER :: tbc               ! Parameter defining top    boundary
                               ! condition type.
  INTEGER :: bbc               ! Parameter defining bottom boundary
                               ! condition type.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Set the west boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(wbc == 0) GO TO 5001

  IF(wbc == 1) THEN            ! Rigid wall boundary condition

    DO k=2,nz-2
      DO j=1,ny-1
        pprt(1,j,k)=pprt(2,j,k)
      END DO
    END DO

  ELSE IF(wbc == 2) THEN        ! Periodic boundary condition.

    DO k=2,nz-2
      DO j=1,ny-1
        pprt(1,j,k)=pprt(nx-2,j,k)
      END DO
    END DO

  ELSE IF(wbc == 3) THEN        ! Zero normal gradient condition.

    DO k=2,nz-2
      DO j=1,ny-1
        pprt(1,j,k)=pprt(2,j,k)
      END DO
    END DO

  ELSE IF(wbc == 4) THEN
                                   ! Radiation condition.
    DO k=2,nz-2
      DO j=2,ny-2
        pprt(1,j,k)=pprt(1,j,k)+pdtwb(j,k)*dtsml
      END DO
    END DO

  ELSE IF(wbc == 5 .OR. wbc == 6) THEN
                                   ! External or user specified condition.

    DO k=2,nz-2
      DO j=1,ny-1
!      pprt(1,j,k)=pprt(2,j,k)
        pprt(2,j,k) = pprt(2,j,k) +pprt(1,j,k)
        pprt(1,j,k)=0.0
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCP', wbc
    STOP

  END IF

  5001  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the east boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(ebc == 0) GO TO 5002

  IF(ebc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO j=1,ny-1
        pprt(nx-1,j,k)=pprt(nx-2,j,k)
      END DO
    END DO

  ELSE IF(ebc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO j=1,ny-1
        pprt(nx-1,j,k)=pprt(2,j,k)
      END DO
    END DO

  ELSE IF(ebc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO j=1,ny-1
        pprt(nx-1,j,k)=pprt(nx-2,j,k)
      END DO
    END DO

  ELSE IF(ebc == 4) THEN
                                    ! Radiation condition.
    DO k=2,nz-2
      DO j=2,ny-2
        pprt(nx-1,j,k)=pprt(nx-1,j,k)+pdteb(j,k)*dtsml
      END DO
    END DO

  ELSE IF(ebc == 5 .OR. ebc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-2
      DO j=1,ny-1
!      pprt(nx-1,j,k)=pprt(nx-2,j,k)
        pprt(nx-2,j,k)=pprt(nx-2,j,k) +pprt(nx-1,j,k)
        pprt(nx-1,j,k)=0.0
      END DO
    END DO

  ELSE
    WRITE(6,900) 'BCP', ebc
    STOP

  END IF

  5002  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the north boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(nbc == 0) GO TO 5003

  IF(nbc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO i=1,nx-1
        pprt(i,ny-1,k)=pprt(i,ny-2,k)
      END DO
    END DO

  ELSE IF(nbc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO i=1,nx-1
        pprt(i,ny-1,k)=pprt(i,2,k)
      END DO
    END DO

  ELSE IF(nbc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO i=1,nx-1
        pprt(i,ny-1,k)=pprt(i,ny-2,k)
      END DO
    END DO

  ELSE IF(nbc == 4) THEN         ! Radiation condition.

    DO k=2,nz-2
      DO i=2,nx-2
        pprt(i,ny-1,k)=pprt(i,ny-1,k)+pdtnb(i,k)*dtsml
      END DO
    END DO

  ELSE IF(nbc == 5 .OR. nbc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-2
      DO i=1,nx-1
!      pprt(i,ny-1,k)=pprt(i,ny-2,k)
        pprt(i,ny-2,k)=pprt(i,ny-2,k)+pprt(i,ny-1,k)
        pprt(i,ny-1,k)=0.0
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCP', nbc
    STOP

  END IF

  5003  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the south boundary conditions
!
!-----------------------------------------------------------------------
!
!MP bc 2d real     !Message passing marker.  Keep with following boundary code.

  IF(sbc == 0) GO TO 5004

  IF(sbc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO i=1,nx-1
        pprt(i,1,k)=pprt(i,2,k)
      END DO
    END DO

  ELSE IF(sbc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO i=1,nx-1
        pprt(i,1,k)=pprt(i,ny-2,k)
      END DO
    END DO

  ELSE IF(sbc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO i=1,nx-1
        pprt(i,1,k)=pprt(i,2,k)
      END DO
    END DO

  ELSE IF(sbc == 4) THEN         ! Radiation condition.

    DO k=2,nz-2
      DO i=2,nx-2
        pprt(i,1,k)=pprt(i,1,k)+pdtsb(i,k)*dtsml
      END DO
    END DO

  ELSE IF(sbc == 5 .OR.sbc == 6) THEN
                                    ! External or user specified condition.
    DO k=2,nz-2
      DO i=1,nx-1
!      pprt(i,1,k)=pprt(i,2,k)
        pprt(i,2,k)=pprt(i,2,k) +pprt(i,1,k)
        pprt(i,1,k)=0.0
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCP', sbc
    STOP

  END IF

  5004  CONTINUE

!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the southwest corner based on the
!  boundary condition types on the south and west boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF(sbc == 4.AND.wbc == 4) THEN

    DO k=2,nz-2
      pprt(1,1,k)=pprt(1,1,k)+pdtwb(1,k)*dtsml
    END DO

  ELSE IF((sbc == 1.OR.sbc == 3.OR.sbc == 5).AND.wbc == 4) THEN

    DO k=2,nz-2
      pprt(1,1,k)=pprt(1,2,k)
    END DO

  ELSE IF(sbc == 2.AND.wbc == 4) THEN

    DO k=2,nz-2
      pprt(1,1,k)=pprt(1,ny-2,k)
    END DO

  ELSE IF(sbc == 4.AND.(wbc == 1.OR.wbc == 3.OR.wbc == 5)) THEN

    DO k=2,nz-2
      pprt(1,1,k)=pprt(2,1,k)
    END DO

  ELSE IF(sbc == 4.AND.wbc == 2) THEN

    DO k=2,nz-2
      pprt(1,1,k)=pprt(nx-2,1,k)
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the southeast corner based on the
!  boundary condition types on the south and east boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF(sbc == 4.AND.ebc == 4) THEN

    DO k=2,nz-2
      pprt(nx-1,1,k)=pprt(nx-1,1,k)+pdteb(1,k)*dtsml
    END DO

  ELSE IF((sbc == 1.OR.sbc == 3.OR.sbc == 5).AND.ebc == 4) THEN

    DO k=2,nz-2
      pprt(nx-1,1,k)=pprt(nx-1,2,k)
    END DO

  ELSE IF(sbc == 2.AND.ebc == 4) THEN

    DO k=2,nz-2
      pprt(nx-1,1,k)=pprt(nx-1,ny-2,k)
    END DO

  ELSE IF(sbc == 4.AND.(ebc == 1.OR.ebc == 3.OR.ebc == 5)) THEN

    DO k=2,nz-2
      pprt(nx-1,1,k)=pprt(nx-2,1,k)
    END DO

  ELSE IF(sbc == 4.AND.ebc == 2) THEN

    DO k=2,nz-2
      pprt(nx-1,1,k)=pprt(2,1,k)
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the northwest corner based on the
!  boundary condition types on the north and west boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF(nbc == 4.AND.wbc == 4) THEN

    DO k=2,nz-2
      pprt(1,ny-1,k)=pprt(1,ny-1,k)+pdtwb(ny-1,k)*dtsml
    END DO

  ELSE IF((nbc == 1.OR.nbc == 3.OR.nbc == 5).AND.wbc == 4) THEN

    DO k=2,nz-2
      pprt(1,ny-1,k)=pprt(1,ny-2,k)
    END DO

  ELSE IF(nbc == 2.AND.wbc == 4) THEN

    DO k=2,nz-2
      pprt(1,ny-1,k)=pprt(1,2,k)
    END DO

  ELSE IF(nbc == 4.AND.(wbc == 1.OR.wbc == 3.OR.wbc == 5)) THEN

    DO k=2,nz-2
      pprt(1,ny-1,k)=pprt(2,ny-1,k)
    END DO

  ELSE IF(nbc == 4.AND.wbc == 2) THEN

    DO k=2,nz-2
      pprt(1,ny-1,k)=pprt(nx-2,ny-1,k)
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the northeast corner based on the
!  boundary condition types on the north and east boundaries.
!
!-----------------------------------------------------------------------
!
!MP bc corner      !Message passing marker.  Keep with following boundary code.

  IF(nbc == 4.AND.ebc == 4) THEN

    DO k=2,nz-2
      pprt(nx-1,ny-1,k)=pprt(nx-1,ny-1,k)+pdteb(ny-1,k)*dtsml
    END DO

  ELSE IF((nbc == 1.OR.nbc == 3.OR.nbc == 5).AND.ebc == 4) THEN

    DO k=2,nz-2
      pprt(nx-1,ny-1,k)=pprt(nx-1,ny-2,k)
    END DO

  ELSE IF(nbc == 2.AND.ebc == 4) THEN

    DO k=2,nz-2
      pprt(nx-1,ny-1,k)=pprt(nx-1,2,k)
    END DO

  ELSE IF(nbc == 4.AND.(ebc == 1.OR.ebc == 3.OR.ebc == 5)) THEN

    DO k=2,nz-2
      pprt(nx-1,ny-1,k)=pprt(nx-2,ny-1,k)
    END DO

  ELSE IF(nbc == 4.AND.ebc == 2) THEN

    DO k=2,nz-2
      pprt(nx-1,ny-1,k)=pprt(2,ny-1,k)
    END DO

  END IF

!-----------------------------------------------------------------------
!
!  Set the top boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(tbc == 0) GO TO 5005

  IF(tbc == 1) THEN             ! Rigid lid boundary condition

    DO j=1,ny-1
      DO i=1,nx-1
!      pprt(i,j,nz-1)=pprt(i,j,nz-2)
        pprt(i,j,nz-2)=pprt(i,j,nz-2) +pprt(i,j,nz-1)
!      pprt(i,j,nz-2)=               +pprt(i,j,nz-1)
        pprt(i,j,nz-1)=0.0
      END DO
    END DO

  ELSE IF(tbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny-1
      DO i=1,nx-1
        pprt(i,j,nz-1)=pprt(i,j,2)
      END DO
    END DO

  ELSE IF(tbc == 3) THEN         ! Zero normal gradient condition.

    DO j=1,ny-1
      DO i=1,nx-1
        pprt(i,j,nz-1)=pprt(i,j,nz-2)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCP', tbc
    STOP

  END IF

  5005  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the bottom boundary conditions
!
!-----------------------------------------------------------------------
!

  IF(bbc == 0) GO TO 5006

  IF(bbc == 1) THEN             ! Non-penetrative ground condition

    DO j=1,ny-1
      DO i=1,nx-1
!      pprt(i,j,1)=2*pprt(i,j,2)-pprt(i,j,3)  ! Cst gradient extrapolation
        pprt(i,j,2)=pprt(i,j,2)+2.*pprt(i,j,1)  ! Cst gradient extrapolation
        pprt(i,j,3)=pprt(i,j,3)-pprt(i,j,1)  ! Cst gradient extrapolation
!      pprt(i,j,2)=           +2.*pprt(i,j,1)  ! Cst gradient extrapolation
!      pprt(i,j,3)=           -pprt(i,j,1)  ! Cst gradient extrapolation
        pprt(i,j,1)=0.0
      END DO
    END DO

  ELSE IF(bbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny-1
      DO i=1,nx-1
        pprt(i,j,1)=pprt(i,j,nz-2)
      END DO
    END DO

  ELSE IF(bbc == 3) THEN         ! Zero normal gradient condition.

    DO j=1,ny-1
      DO i=1,nx-1
        pprt(i,j,1)=pprt(i,j,2)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCP', bbc
    STOP

  END IF

  5006  CONTINUE

  RETURN

  900   FORMAT(1X,'Invalid boundary condition option found in ',a,      &
               /1X,'The option was ',i3,' Job stopped.')
END SUBROUTINE adbcp

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADBCQV                     ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adbcqv(nx,ny,nz,dtbig,                                       &
           qv,qvbar,qdteb,qdtwb,qdtnb,qdtsb,                            &
           ebc,wbc,nbc,sbc,tbc,bbc)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Set the boundary conditions for qv.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR: Ming Xue
!  06/27/95
!  Created subroutine BCQV, based on BCQ. BCQV handles special
!  top and bottom boundary conditions for qv.
!
!
!  MODIFICATION HISTORY:
!
!  07/06/95 (Ming Xue)
!  Changed the top and bottom BC for qvprt back to zero gradient.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    dtbig    The large time step size (s)
!
!    qv       Water vapor mixing ratio (kg/kg)
!    qvbar    Base-state water vapor mixing ratio (kg/kg)
!
!    qdteb    Time tendency of the q field at the east boundary
!    qdtwb    Time tendency of the q field at the west boundary
!    qdtnb    Time tendency of the q field at the north boundary
!    qdtsb    Time tendency of the q field at the south boundary
!
!    ebc      Parameter defining east   boundary condition type.
!    wbc      Parameter defining west   boundary condition type.
!    nbc      Parameter defining north  boundary condition type.
!    sbc      Parameter defining south  boundary condition type.
!    tbc      Parameter defining top    boundary condition type.
!    bbc      Parameter defining bottom boundary condition type.
!
!  OUTPUT:
!
!    qv       qv over the entire domain at tfuture (m/s)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in x, y and z
                               ! directions

  REAL :: dtbig                ! Big time step size (s)

  INTEGER :: nt                ! no. of t-levels of t-dependent arrays.
  INTEGER :: tpast             ! Index of time level for past time.
  INTEGER :: tpresent          ! Index of time level for present time.
  INTEGER :: tfuture           ! Index of time level for furture time.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  REAL :: qv    (nx,ny,nz,nt)  ! water vapor mixing ratio (kg/kg)
  REAL :: qvbar (nx,ny,nz)     ! base-state water vapro mixing ratio (kg/kg)

  REAL :: qdteb(ny,nz)         ! Time tendency of q at east boundary
  REAL :: qdtwb(ny,nz)         ! Time tendency of q at west boundary
  REAL :: qdtnb(nx,nz)         ! Time tendency of q at north boundary
  REAL :: qdtsb(nx,nz)         ! Time tendency of q at south boundary
!
!-----------------------------------------------------------------------
!
!  The following integer parameters define the type of condition
!  at each boundary.
!
!   1 for rigid wall (mirror) type boundary condition.
!   2 for periodic boundary condition.
!   3 for zero normal gradient boundary condition.
!   4 for open (radiation) boundary condition.
!   5 for user (externally) specified boundary condition.
!
!-----------------------------------------------------------------------
!
  INTEGER :: ebc               ! Parameter defining east   boundary
                               ! condition type.
  INTEGER :: wbc               ! Parameter defining west   boundary
                               ! condition type.
  INTEGER :: nbc               ! Parameter defining north  boundary
                               ! condition type.
  INTEGER :: sbc               ! Parameter defining south  boundary
                               ! condition type.
  INTEGER :: tbc               ! Parameter defining top    boundary
                               ! condition type.
  INTEGER :: bbc               ! Parameter defining bottom boundary
                               ! condition type.

  INTEGER :: i,j
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!


!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions for qv.
!
!-----------------------------------------------------------------------
!
  CALL adbcsclr(nx,ny,nz,dtbig,                                         &
              qv(1,1,1,tpast),qv(1,1,1,tpresent),qv(1,1,1,tfuture),     &
              qdteb,qdtwb,qdtnb,qdtsb,                                  &
              ebc,wbc,nbc,sbc,tbc,bbc)

  RETURN
END SUBROUTINE adbcqv
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                  SUBROUTINE ADVBCWCONT0              ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE advbcwcont0(nx,ny,nz,wcont)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint of
!  Set the top and bottom boundary conditions for wcont.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR:
!  5/22/96 Jidong Gao
!
!  MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    wcont    Contravariant vertical velocity (m/s)
!
!  OUTPUT:
!
!    wcont    Top and bottom values of wcont.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE             ! Force explicit declarations

  INTEGER :: nx,ny,nz          ! Number of grid points in x, y and z
                               ! directions

  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'bndry.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!
!-----------------------------------------------------------------------
!
!  Set the top boundary condition
!
!-----------------------------------------------------------------------
!
  IF(tbc == 1) THEN             ! Rigid lid boundary condition

    DO j=1,ny-1
      DO i=1,nx-1
!      wcont(i,j,nz)=-wcont(i,j,nz-2)
!      wcont(i,j,nz-1)=0.0
!
        wcont(i,j,nz-2)=wcont(i,j,nz-2)-wcont(i,j,nz)
        wcont(i,j,nz)=0.0    !commented by gge 2012/7/10
        wcont(i,j,nz-1)=0.0  !commented by gge 2012/7/10
      END DO
    END DO

  ELSE IF(tbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny-1
      DO i=1,nx-1
!       wcont(i,j,nz)=wcont(i,j,3)
!        
        wcont(i,j,3) = wcont(i,j,3) + wcont(i,j,nz)
        wcont(i,j,nz)= 0.
      END DO
    END DO

  ELSE IF(tbc == 3) THEN         ! Zero normal gradient condition.

    DO j=1,ny-1
      DO i=1,nx-1
!       wcont(i,j,nz)=wcont(i,j,nz-1)
!
        wcont(i,j,nz-1) = wcont(i,j,nz-1) + wcont(i,j,nz)
        wcont(i,j,nz) = 0.
      END DO
    END DO

  ELSE

    WRITE(6,900) 'VBCWCONT', tbc
    STOP

  END IF
!
!-----------------------------------------------------------------------
!
!  Set the bottom boundary condition
!
!-----------------------------------------------------------------------
!
  IF(bbc == 1) THEN             ! Non-penetrative ground condition

    DO j=1,ny-1
      DO i=1,nx-1
!       wcont(i,j,1)=-wcont(i,j,3)
!       wcont(i,j,2)=0.0
!
        wcont(i,j,3)=wcont(i,j,3)-wcont(i,j,1)
        wcont(i,j,1)=0.0
        wcont(i,j,2)=0.0
      END DO
    END DO

  ELSE IF(bbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny-1
      DO i=1,nx-1
!       wcont(i,j,1)=wcont(i,j,nz-2)
!
        wcont(i,j,nz-2)=wcont(i,j,nz-2)+wcont(i,j,1)
        wcont(i,j,1)=0.
      END DO
    END DO

  ELSE IF(bbc == 3) THEN         ! Zero normal gradient condition.

    DO j=1,ny-1
      DO i=1,nx-1
!       wcont(i,j,1)=wcont(i,j,2)
! 
        wcont(i,j,2)=wcont(i,j,2)+wcont(i,j,1)
        wcont(i,j,1)=0.0
      END DO
    END DO

  ELSE

    WRITE(6,900) 'VBCWCONT', bbc
    STOP

  END IF

  RETURN

  900   FORMAT(1X,'Invalid boundary condition option found in ',a,      &
               /1X,'The option was ',i3,' Job stopped.')
END SUBROUTINE advbcwcont0
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADVBCW                     ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE advbcw(nx,ny,nz,w,wcont,tbc,bbc,u,v,                         &
           rhostr,rhostru,rhostrv,rhostrw,                              &
           j1,j2,j3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform adjoint operations on VBCW. VBCW
!  sets the top and bottom boundary conditions for w.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR: Jidong Gao
!          05/26/1996
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    w        Interior domain values of w-velocity at tfuture (m/s)
!    wcont    Contravariant vertical velocity (m/s)
!
!    tbc      Parameter defining top    boundary condition type.
!    bbc      Parameter defining bottom boundary condition type.
!
!    rhostr   j3 times base state density rhobar (kg/m**3).
!    rhostru  Average rhostr at u points (kg/m**3).
!    rhostrv  Average rhostr at v points (kg/m**3).
!    rhostrw  Average rhostr at w points (kg/m**3).
!
!  OUTPUT:
!
!    w        The w-velocity over the entire domain at tfuture (m/s)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in x, y and z
                               ! directions
!
  REAL :: w     (nx,ny,nz)     ! Total w-velocity at tfuture (m/s)
  REAL :: wcont (nx,ny,nz)     ! Contravariant vertical velocity (m/s)

  REAL :: u     (nx,ny,nz)     ! u-velocity at tfuture (m/s)
  REAL :: v     (nx,ny,nz)     ! v-velocity at tfuture (m/s)
  REAL :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3.
  REAL :: rhostru(nx,ny,nz)    ! Averaged rhostr at u points (kg/m**3).
  REAL :: rhostrv(nx,ny,nz)    ! Averaged rhostr at v points (kg/m**3).
  REAL :: rhostrw(nx,ny,nz)    ! Averaged rhostr at w points (kg/m**3).

  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian defined as
                               ! d( zp )/d( z ).
!
!-----------------------------------------------------------------------
!
!  The following integer parameters define the type of condition
!  at each boundary.
!
!   1 for rigid wall (mirror) type boundary condition.
!   2 for periodic boundary condition.
!   3 for zero normal gradient boundary condition.
!   4 for open (radiation) boundary condition.
!   5 for user (externally) specified boundary condition.
!
!-----------------------------------------------------------------------
!
  INTEGER :: tbc   ! Parameter defining top    boundary condition type.
  INTEGER :: bbc   ! Parameter defining bottom boundary condition type.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: ismstp,nsmstp
  INTEGER :: i, j
  REAL :: urho1,urho2,vrho1,vrho2,wrho1,tems
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!-----------------------------------------------------------------------
!
!  Adjoint on bottom boundary conditions
!
!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!
!  Set the bottom boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(bbc == 0) GO TO 6006

  IF(bbc == 1) THEN             ! Non-penetrative ground condition

    DO j=1,ny-1
      DO i=1,nx-1
!       w(i,j,2) =-((u(i  ,j,2)+u(i  ,j,1))*j1(i,j,2)
!    :               +(u(i+1,j,2)+u(i+1,j,1))*j1(i+1,j,2)
!    :               +(v(i,j  ,2)+v(i,j  ,1))*j2(i,j,2)
!    :               +(v(i,j+1,2)+v(i,j+1,1))*j2(i,j+1,2)) *0.25
        u(i  ,j,1) =u(i  ,j,1)-w(i,j,2)*j1(i,j,2)*0.25
        u(i  ,j,2) =u(i  ,j,2)-w(i,j,2)*j1(i,j,2)*0.25
        u(i+1,j,1) =u(i+1,j,1)-w(i,j,2)*j1(i+1,j,2)*0.25
        u(i+1,j,2) =u(i+1,j,2)-w(i,j,2)*j1(i+1,j,2)*0.25
        v(i,j  ,1) =v(i,j  ,1)-w(i,j,2)*j2(i,j,2)*0.25
        v(i,j  ,2) =v(i,j  ,2)-w(i,j,2)*j2(i,j,2)*0.25
        v(i,j+1,1) =v(i,j+1,1)-w(i,j,2)*j2(i,j+1,2)*0.25
        v(i,j+1,2) =v(i,j+1,2)-w(i,j,2)*j2(i,j+1,2)*0.25
        w(i,j,2)=0.0
      END DO
    END DO
!
    DO j=1,ny-1
      DO i=1,nx-1

!-----------------------------------------------------------------------
!
!  Note wrho1 above is calculated for k=3, wrho1(1)=-wrho1(3)
!  based on non-penetrative ground condition.
!
!-----------------------------------------------------------------------
        wrho1 = - w(i,j,1)/rhostrw(i,j,3)
        tems  = - w(i,j,1)/rhostrw(i,j,3)
        w(i,j,1)= 0.0

        wcont(i,j,3) = wcont(i,j,3)                                     &
            + 0.5*(j3(i,j,2)+j3(i,j,3))*rhostrw(i,j,3)*wrho1
        wrho1 =0.0
        vrho1 =0.5*tems*j2(i,j  ,3)
        vrho2 =0.5*tems*j2(i,j+1,3)
        v(i,j+1,2)=v(i,j+1,2)                                           &
                  +0.5*vrho2*rhostrv(i,j+1,2)
        v(i,j+1,3)=v(i,j+1,3)                                           &
                  +0.5*vrho2*rhostrv(i,j+1,3)
        vrho2 =0.
        v(i,j,2)=v(i,j,2)                                               &
                  +0.5*vrho1*rhostrv(i,j,2)
        v(i,j,3)=v(i,j,3)                                               &
                  +0.5*vrho1*rhostrv(i,j,3)
        vrho1 =0.
        urho1 =0.5*tems*j1(i,j,3)
        urho2 =0.5*tems*j1(i+1,j,3)
        tems =0.
        u(i+1,j,2) = u(i+1,j,2)+ 0.5*urho2*rhostru(i+1,j,2)
        u(i+1,j,3) = u(i+1,j,3)+ 0.5*urho2*rhostru(i+1,j,3)
        urho2=0.
        u(i  ,j,2) = u(i  ,j,2)+ 0.5*urho1*rhostru(i  ,j,2)
        u(i  ,j,3) = u(i  ,j,3)+ 0.5*urho1*rhostru(i  ,j,3)
        urho1=0.

      END DO
    END DO
  ELSE IF(bbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny-1
      DO i=1,nx-1
        w(i,j,1)=w(i,j,nz-2)
      END DO
    END DO

  ELSE IF(bbc == 3) THEN         ! Zero normal gradient condition.

    DO j=1,ny-1
      DO i=1,nx-1
        w(i,j,1)=w(i,j,2)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCW', bbc
    STOP

  END IF

  6006  CONTINUE
!-----------------------------------------------------------------------
!
!  Adjoint on the top boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(tbc == 0) GO TO 6005

  IF(tbc == 1) THEN             ! Rigid lid boundary condition
    DO j=1,ny-1
      DO i=1,nx-1
        w(i,j,nz-1)= 0.0
      END DO
    END DO
!
    DO j=1,ny-1
      DO i=1,nx-1
!-----------------------------------------------------------------------
!
!  Note wrho1 above is calculated for k=nz-2, wrho1(nz)=-wrho1(nz-2)
!  based on grid lid  condition.
!
!-----------------------------------------------------------------------
        wrho1 = - w(i,j,nz)/rhostrw(i,j,nz-2)
        tems  = - w(i,j,nz)/rhostrw(i,j,nz-2)
        w(i,j,nz)= 0.0

        wcont(i,j,nz-2) =wcont(i,j,nz-2)                                &
                  + 0.5*(j3(i,j,nz-2)+j3(i,j,nz-3))                     &
                        *rhostrw(i,j,nz-2)*wrho1
        wrho1 =0.0
        vrho1 =0.5*tems*j2(i,j  ,nz-2)
        vrho2 =0.5*tems*j2(i,j+1,nz-2)
        v(i,j+1,nz-2)=v(i,j+1,nz-2)                                     &
                  +0.5*vrho2*rhostrv(i,j+1,nz-2)
        v(i,j+1,nz-3)=v(i,j+1,nz-3)                                     &
                  +0.5*vrho2*rhostrv(i,j+1,nz-3)
        vrho2 =0.
        v(i,j,nz-2)=v(i,j,nz-2)                                         &
                  +0.5*vrho1*rhostrv(i,j,nz-2)
        v(i,j,nz-3)=v(i,j,nz-3)                                         &
                  +0.5*vrho1*rhostrv(i,j,nz-3)
        vrho1 =0.
        urho1 =0.5*tems*j1(i  ,j,nz-2)
        urho2 =0.5*tems*j1(i+1,j,nz-2)
        tems =0.
        u(i+1,j,nz-2) =u(i+1,j,nz-2)+0.5*urho2*rhostru(i+1,j,nz-2)
        u(i+1,j,nz-3) =u(i+1,j,nz-3)+0.5*urho2*rhostru(i+1,j,nz-3)
        urho2=0.
        u(i  ,j,nz-2) =u(i  ,j,nz-2)+0.5*urho1*rhostru(i  ,j,nz-2)
        u(i  ,j,nz-3) =u(i  ,j,nz-3)+0.5*urho1*rhostru(i  ,j,nz-3)
        urho1=0.

      END DO
    END DO



  ELSE IF(tbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny-1
      DO i=1,nx-1
        w(i,j,nz)=w(i,j,3)
      END DO
    END DO

  ELSE IF(tbc == 3) THEN         ! Zero normal gradient condition.

    DO j=1,ny-1
      DO i=1,nx-1
        w(i,j,nz)=w(i,j,nz-1)
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCW', tbc
    STOP

  END IF

  6005  CONTINUE
!
!-----------------------------------------------------------------------
  RETURN

  900   FORMAT(1X,'Invalid boundary condition option found in ',a,      &
               /1X,'The option was ',i3,' Job stopped.')
END SUBROUTINE advbcw
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE ADBCQ                        ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adbcq(nx,ny,nz,dtbig,                                        &
           q, qdteb,qdtwb,qdtnb,qdtsb,                                  &
           ebc,wbc,nbc,sbc,tbc,bbc)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Set the boundary conditions for all of the water quantities
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR: JIDONG GAO
!  07/03/96.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    dtbig    The large time step size (s)
!
!    q        mixing ratio for one of the water variables at all
!             time levels (kg/kg)
!
!    qdteb    Time tendency of the q field at the east boundary
!    qdtwb    Time tendency of the q field at the west boundary
!    qdtnb    Time tendency of the q field at the north boundary
!    qdtsb    Time tendency of the q field at the south boundary
!
!    ebc      Parameter defining east   boundary condition type.
!    wbc      Parameter defining west   boundary condition type.
!    nbc      Parameter defining north  boundary condition type.
!    sbc      Parameter defining south  boundary condition type.
!    tbc      Parameter defining top    boundary condition type.
!    bbc      Parameter defining bottom boundary condition type.
!
!  OUTPUT:
!
!    q        Array q over the entire domain at tfuture (m/s)
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in x, y and z
                               ! directions

  REAL :: dtbig                ! Big time step size (s)

  INTEGER :: nt                ! no. of t-levels of t-dependent arrays.
  INTEGER :: tpast             ! Index of time level for past time.
  INTEGER :: tpresent          ! Index of time level for present time.
  INTEGER :: tfuture           ! Index of time level for furture time.

  PARAMETER (nt=1, tpast=1, tpresent=1, tfuture=1)

  REAL :: q     (nx,ny,nz,nt)  ! mixing ratio for one of the water/ice
                               ! variables (kg/kg)

  REAL :: qdteb(ny,nz)         ! Time tendency of q at east boundary
  REAL :: qdtwb(ny,nz)         ! Time tendency of q at west boundary
  REAL :: qdtnb(nx,nz)         ! Time tendency of q at north boundary
  REAL :: qdtsb(nx,nz)         ! Time tendency of q at south boundary
!
!-----------------------------------------------------------------------
!
!  The following integer parameters define the type of condition
!  at each boundary.
!
!   1 for rigid wall (mirror) type boundary condition.
!   2 for periodic boundary condition.
!   3 for zero normal gradient boundary condition.
!   4 for open (radiation) boundary condition.
!   5 for user (externally) specified boundary condition.
!
!-----------------------------------------------------------------------
!
  INTEGER :: ebc               ! Parameter defining east   boundary
                               ! condition type.
  INTEGER :: wbc               ! Parameter defining west   boundary
                               ! condition type.
  INTEGER :: nbc               ! Parameter defining north  boundary
                               ! condition type.
  INTEGER :: sbc               ! Parameter defining south  boundary
                               ! condition type.
  INTEGER :: tbc               ! Parameter defining top    boundary
                               ! condition type.
  INTEGER :: bbc               ! Parameter defining bottom boundary
                               ! condition type.
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions for q.
!
!-----------------------------------------------------------------------
!
  CALL adbcsclr(nx,ny,nz,dtbig,                                         &
              q(1,1,1,tpast),q(1,1,1,tpresent),q(1,1,1,tfuture),        &
              qdteb,qdtwb,qdtnb,qdtsb,                                  &
              ebc,wbc,nbc,sbc,tbc,bbc)

  RETURN
END SUBROUTINE adbcq
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADBCSCLR                   ######
!######                                                      ######
!######                Copyright (c) 1992                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adbcsclr(nx,ny,nz,dtbig,                                     &
           s1,s2,s3,sdteb,sdtwb,sdtnb,sdtsb,                            &
           ebc,wbc,nbc,sbc,tbc,bbc)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint of
!  Set the boundary conditions for a scalar s. Please note that the values
!  at the corner points may depend on the order that the e-w, n-s and t-b
!  boundary conditions are applied.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR: Jidong GAO
!  05/25/96.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    dtbig    The large time step size (s)
!
!    s1       A scalar variable at time tpast
!    s2       A scalar variable at time tpresent
!    s3       A scalar variable at time tfuture
!
!    sdteb    Time tendency of the s field at the east boundary
!    sdtwb    Time tendency of the s field at the west boundary
!    sdtnb    Time tendency of the s field at the north boundary
!    sdtsb    Time tendency of the s field at the south boundary
!
!    ebc      Parameter defining east   boundary condition type.
!    wbc      Parameter defining west   boundary condition type.
!    nbc      Parameter defining north  boundary condition type.
!    sbc      Parameter defining south  boundary condition type.
!    tbc      Parameter defining top    boundary condition type.
!    bbc      Parameter defining bottom boundary condition type.
!
!  OUTPUT:
!
!    s3       Scalar array s3 over the entire domain tfuture
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz          ! Number of grid points in x, y and z
                               ! directions
!
  REAL :: dtbig                ! The big time step size (s)
!
  REAL :: s1    (nx,ny,nz)     ! A scalar variable at time tpast.
  REAL :: s2    (nx,ny,nz)     ! A scalar variable at time tpresent.
  REAL :: s3    (nx,ny,nz)     ! A scalar variable at time tfuture.
!
  REAL :: sdteb(ny,nz)  ! Time tendency of s field at east boundary
  REAL :: sdtwb(ny,nz)  ! Time tendency of s field at west boundary
  REAL :: sdtnb(nx,nz)  ! Time tendency of s field at north boundary
  REAL :: sdtsb(nx,nz)  ! Time tendency of s field at south boundary
!
!-----------------------------------------------------------------------
!
!  The following integer parameters define the type of condition
!  at each boundary.
!
!   1 for rigid wall (mirror) type boundary condition.
!   2 for periodic boundary condition.
!   3 for zero normal gradient boundary condition.
!   4 for open (radiation) boundary condition.
!   5 for user (externally) specified boundary condition.
!
!-----------------------------------------------------------------------
!
  INTEGER :: ebc   ! Parameter defining east   boundary condition type.
  INTEGER :: wbc   ! Parameter defining west   boundary condition type.
  INTEGER :: nbc   ! Parameter defining north  boundary condition type.
  INTEGER :: sbc   ! Parameter defining south  boundary condition type.
  INTEGER :: tbc   ! Parameter defining top    boundary condition type.
  INTEGER :: bbc   ! Parameter defining bottom boundary condition type.
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!-----------------------------------------------------------------------
!
!  Set the bottom boundary conditions
!
!-----------------------------------------------------------------------
!

  IF(bbc == 0) GO TO 5006

  IF(bbc == 1) THEN             ! Non-penetrative ground condition

    DO j=1,ny-1
      DO i=1,nx-1
!      s3(i,j,1)=s3(i,j,2)
        s3(i,j,2)=s3(i,j,2)+s3(i,j,1)
        s3(i,j,1)=0.
      END DO
    END DO

  ELSE IF(bbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny-1
      DO i=1,nx-1
        s3(i,j,1)=s3(i,j,nz-2)
      END DO
    END DO

  ELSE IF(bbc == 3) THEN         ! Zero normal gradient condition.

    DO j=1,ny-1
      DO i=1,nx-1
!      s3(i,j,1)=s3(i,j,2)
        s3(i,j,2)=s3(i,j,2)+s3(i,j,1)
        s3(i,j,1)=0.0
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCSCLR', bbc
    STOP

  END IF

  5006  CONTINUE
!
!-----------------------------------------------------------------------
!
!  Set the top boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(tbc == 0) GO TO 5005

  IF(tbc == 1) THEN             ! Rigid lid boundary condition

    DO j=1,ny-1
      DO i=1,nx-1
!      s3(i,j,nz-1)=s3(i,j,nz-2)
        s3(i,j,nz-2)=s3(i,j,nz-2)+s3(i,j,nz-1)
        s3(i,j,nz-1)=0.
      END DO
    END DO

  ELSE IF(tbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny-1
      DO i=1,nx-1
        s3(i,j,nz-1)=s3(i,j,2)
      END DO
    END DO

  ELSE IF(tbc == 3) THEN         ! Zero normal gradient condition.

    DO j=1,ny-1
      DO i=1,nx-1
!c        s3(i,j,nz-1)=s3(i,j,nz-2)
        s3(i,j,nz-2)=s3(i,j,nz-2)+s3(i,j,nz-1)
        s3(i,j,nz-1)=0.
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCSCLR', tbc
    STOP

  END IF

  5005  CONTINUE
!

!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the northeast corner based on the
!  boundary condition types on the north and east boundaries.
!
!-----------------------------------------------------------------------
!
  IF(nbc == 4.AND.ebc == 4) THEN

    DO k=2,nz-2
      s3(nx-1,ny-1,k)=s1(nx-1,ny-1,k)+sdteb(ny-1,k)*2.*dtbig
    END DO

  ELSE IF(nbc == 2.AND.ebc == 4) THEN

    DO k=2,nz-2
      s3(nx-1,ny-1,k)=s3(nx-1,2,k)
    END DO

  ELSE IF((nbc == 1.OR.nbc == 3).AND.ebc == 4) THEN

    DO k=2,nz-2
      s3(nx-1,ny-1,k)=s3(nx-1,ny-2,k)
    END DO

  ELSE IF(nbc == 4.AND.ebc == 2) THEN

    DO k=2,nz-2
      s3(nx-1,ny-1,k)=s3(2,ny-1,k)
    END DO

  ELSE IF(nbc == 4.AND.ebc == 1) THEN

    DO k=2,nz-2
      s3(nx-1,ny-1,k)=s3(nx-2,ny-1,k)
    END DO

  ELSE IF(nbc == 4.AND.ebc == 3) THEN

    DO k=2,nz-2
      s3(nx-1,ny-1,k)=s3(nx-2,ny-1,k)
    END DO

  END IF

!
!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the northwest corner based on the
!  boundary condition types on the north and west boundaries.
!
!-----------------------------------------------------------------------
!
  IF(nbc == 4.AND.wbc == 4) THEN

    DO k=2,nz-2
      s3(1,ny-1,k)=s1(1,ny-1,k)+sdtwb(ny-1,k)*2.*dtbig
    END DO

  ELSE IF(nbc == 2.AND.wbc == 4) THEN

    DO k=2,nz-2
      s3(1,ny-1,k)=s3(1,2,k)
    END DO

  ELSE IF((nbc == 1.OR.nbc == 3).AND.wbc == 4) THEN

    DO k=2,nz-2
      s3(1,ny-1,k)=s3(1,ny-2,k)
    END DO

  ELSE IF(nbc == 4.AND.wbc == 2) THEN

    DO k=2,nz-2
      s3(1,ny-1,k)=s3(nx-2,ny-1,k)
    END DO

  ELSE IF(nbc == 4.AND.wbc == 1) THEN

    DO k=2,nz-2
      s3(1,ny-1,k)=s3(2,ny-1,k)
    END DO

  ELSE IF(nbc == 4.AND.wbc == 3) THEN

    DO k=2,nz-2
      s3(1,ny-1,k)=s3(2,ny-1,k)
    END DO

  END IF

!
!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the southeast corner based on the
!  boundary condition types on the south and east boundaries.
!
!-----------------------------------------------------------------------
!
  IF(sbc == 4.AND.ebc == 4) THEN

    DO k=2,nz-2
      s3(nx-1,1,k)=s1(nx-1,1,k)+sdteb(1,k)*2.*dtbig
    END DO

  ELSE IF(sbc == 2.AND.ebc == 4) THEN

    DO k=2,nz-2
      s3(nx-1,1,k)=s3(nx-1,ny-2,k)
    END DO

  ELSE IF((sbc == 1.OR.sbc == 3).AND.ebc == 4) THEN

    DO k=2,nz-2
      s3(nx-1,1,k)=s3(nx-1,2,k)
    END DO

  ELSE IF(sbc == 4.AND.ebc == 2) THEN

    DO k=2,nz-2
      s3(nx-1,1,k)=s3(2,1,k)
    END DO

  ELSE IF(sbc == 4.AND.(ebc == 1.OR.ebc == 3)) THEN

    DO k=2,nz-2
      s3(nx-1,1,k)=-s3(nx-2,1,k)
    END DO

  END IF

!
!
!-----------------------------------------------------------------------
!
!  Set the boundary conditions at the southwest corner based on the
!  boundary condition types on the south and west boundaries.
!
!-----------------------------------------------------------------------
!
  IF(sbc == 4.AND.wbc == 4) THEN

    DO k=2,nz-2
      s3(1,1,k)=s1(1,1,k)+sdtwb(1,k)*2.*dtbig
    END DO

  ELSE IF(sbc == 2.AND.wbc == 4) THEN

    DO k=2,nz-2
      s3(1,1,k)=s3(1,ny-2,k)
    END DO

  ELSE IF((sbc == 1.OR.sbc == 3).AND.wbc == 4) THEN

    DO k=2,nz-2
      s3(1,1,k)=s3(1,2,k)
    END DO

  ELSE IF(sbc == 4.AND.wbc == 2) THEN

    DO k=2,nz-2
      s3(1,1,k)=s3(nx-2,1,k)
    END DO

  ELSE IF(sbc == 4.AND.(wbc == 1.OR.wbc == 3)) THEN

    DO k=2,nz-2
      s3(1,1,k)=s3(2,1,k)
    END DO

  END IF
!
!
!-----------------------------------------------------------------------
!
!  Set the south boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(sbc == 0) GO TO 5004

  IF(sbc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO i=1,nx-1
        s3(i,1,k)=s3(i,2,k)
      END DO
    END DO

  ELSE IF(sbc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO i=1,nx-1
        s3(i,1,k)=s3(i,ny-2,k)
      END DO
    END DO

  ELSE IF(sbc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO i=1,nx-1
!      s3(i,1,k)=s3(i,2,k)
        s3(i,2,k)=s3(i,2,k)+s3(i,1,k)
        s3(i,1,k)=0.
      END DO
    END DO

  ELSE IF(sbc == 4) THEN
                                    ! Radiation or user specified condition.

    DO k=2,nz-2
      DO i=2,nx-2
!      s3(i,1,k)=s1(i,1,k)+sdtsb(i,k)*2.*dtbig
        s1(i,1,k)=s3(i,1,k)
        s3(i,1,k)=0.0
      END DO
    END DO

  ELSE IF(sbc == 5 .OR.sbc == 6 ) THEN

    DO k=2,nz-2
      DO i=1,nx-1
        s3(i,1,k)=s1(i,1,k)+sdtsb(i,k)*2.*dtbig
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCSCLR', sbc
    STOP

  END IF

  5004  CONTINUE

!
!
!-----------------------------------------------------------------------
!
!  Set the north boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(nbc == 0) GO TO 5003

  IF(nbc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO i=1,nx-1
        s3(i,ny-1,k)=s3(i,ny-2,k)
      END DO
    END DO

  ELSE IF(nbc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO i=1,nx-1
        s3(i,ny-1,k)=s3(i,2,k)
      END DO
    END DO

  ELSE IF(nbc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO i=1,nx-1
!      s3(i,ny-1,k)=s3(i,ny-2,k)
        s3(i,ny-2,k)=s3(i,ny-2,k)+s3(i,ny-1,k)
        s3(i,ny-1,k)=0.
      END DO
    END DO

  ELSE IF(nbc == 4) THEN
                                    ! Radiation or user specified condition.

    DO k=2,nz-2
      DO i=2,nx-2
!      s3(i,ny-1,k)=s1(i,ny-1,k)+sdtnb(i,k)*2.*dtbig
        s1(i,ny-1,k)=s3(i,ny-1,k)
        s3(i,ny-1,k)=0.0
      END DO
    END DO

  ELSE IF(nbc == 5 .OR. nbc == 6 ) THEN

    DO k=2,nz-2
      DO i=1,nx-1
        s3(i,ny-1,k)=s1(i,ny-1,k)+sdtnb(i,k)*2.*dtbig
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCSCLR', nbc
    STOP

  END IF

  5003  CONTINUE
!
!
!-----------------------------------------------------------------------
!
!  Set the east boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(ebc == 0) GO TO 5002

  IF(ebc == 1) THEN             ! Rigid wall boundary condition

    DO k=2,nz-2
      DO j=1,ny-1
        s3(nx-1,j,k)=s3(nx-2,j,k)
      END DO
    END DO

  ELSE IF(ebc == 2) THEN         ! Periodic boundary condition.

    DO k=2,nz-2
      DO j=1,ny-1
        s3(nx-1,j,k)=s3(2,j,k)
      END DO
    END DO

  ELSE IF(ebc == 3) THEN         ! Zero normal gradient condition.

    DO k=2,nz-2
      DO j=1,ny-1
!c        s3(nx-1,j,k)=s3(nx-2,j,k)
        s3(nx-2,j,k)=s3(nx-2,j,k)+s3(nx-1,j,k)
        s3(nx-1,j,k)=0.
      END DO
    END DO

  ELSE IF(ebc == 4 .OR. ebc == 5 .OR. ebc == 6 ) THEN
                                    ! Radiation or user specified condition.

    DO k=2,nz-2
      DO j=2,ny-2
!      s3(nx-1,j,k)=s1(nx-1,j,k)+sdteb(j,k)*2.*dtbig
        s1(nx-1,j,k)=s3(nx-1,j,k)
        s3(nx-1,j,k)=0.0
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCSCLR', ebc
    STOP

  END IF

  5002  CONTINUE
!
!
!-----------------------------------------------------------------------
!
!  Set the west boundary conditions
!
!-----------------------------------------------------------------------
!
  IF(wbc == 0) GO TO 5001

  IF(wbc == 1) THEN            ! Rigid wall boundary condition

    DO k=2,nz-2
      DO j=1,ny-1
        s3(1,j,k)=s3(2,j,k)
      END DO
    END DO

  ELSE IF(wbc == 2) THEN        ! Periodic boundary condition.

    DO k=2,nz-2
      DO j=1,ny-1
        s3(1,j,k)=s3(nx-2,j,k)
      END DO
    END DO

  ELSE IF(wbc == 3) THEN        ! Zero normal gradient condition.

    DO k=2,nz-2
      DO j=1,ny-1
!c        s3(1,j,k)=s3(2,j,k)
        s3(2,j,k)=s3(2,j,k)+s3(1,j,k)
        s3(1,j,k)=0.
      END DO
    END DO

  ELSE IF(wbc == 4 .OR. wbc == 5 .OR. wbc == 6 ) THEN
                                    ! Radiation or user specified condition.

    DO k=2,nz-2
      DO j=2,ny-2
!      s3(1,j,k)=s1(1,j,k)+sdtwb(j,k)*2.*dtbig
        s1(1,j,k)=s3(1,j,k)
        s3(1,j,k)=0.0
      END DO
    END DO

  ELSE

    WRITE(6,900) 'BCSCLR', wbc
    STOP

  END IF

  5001  CONTINUE
!
  RETURN

  900   FORMAT(1X,'Invalid boundary condition option found in ',a,      &
               /1X,'The option was ',i3,' Job stopped.')
END SUBROUTINE adbcsclr
