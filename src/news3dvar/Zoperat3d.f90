!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE AAMULT0                    ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE aamult0(a,b,nx,ny,nz,                                         &
           ibgn,iend,jbgn,jend,kbgn,kend, ab)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the element-wise product of arrays a and b.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/01/2 (K. Brewster)
!  Further facelift.
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        First multiplier array
!    b        Second multiplier array
!
!    nx       First dimension of arrays a, b and ab
!    ny       Second dimension of arrays a, b and ab
!    nz       Third dimension of arrays a, b and ab
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!  OUTPUT:
!
!    ab       Element-wise product (a*b) over range specified by
!             the starting and ending indices given above.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a (nx,ny,nz)         ! Input array 1
  REAL :: b (nx,ny,nz)         ! Input array 2

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend

  REAL :: ab(nx,ny,nz)         ! Product array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        ab(i,j,k)=a(i,j,k)*b(i,j,k)

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE aamult0

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE AVGX0                      ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE avgx0(a, onvf, nx,ny,nz,                                      &
           ibgn,iend,jbgn,jend,kbgn,kend,                               &
           aavg)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform a spatial average operation on array (a) in the x direction.
!  The average for the two input values is defined at the midpoint
!  between the two values.
!  If the averaged variable aavg is on the grid volume face, onvf =1
!  otherwise, onvf = 0
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/01/2 (K. Brewster)
!  Further facelift.
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume face,
!             onvf =1; otherwise, onvf = 0
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!  OUTPUT:
!
!    aavg     Result of average operation on array
!             (a) in the x direction
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a   (nx,ny,nz)       ! Input array
  INTEGER :: onvf              ! Integer grid point indicator

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                ! Integer indicator of multiplication
  REAL :: aavg(nx,ny,nz)       ! Averaged array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,iright,ileft
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  IF ( onvf == 1) THEN

    iright = 0
    ileft = -1

  ELSE

    iright = 1
    ileft = 0

  END IF

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        aavg(i,j,k)=(a(i+iright,j,k)                                    &
                    +a(i+ileft ,j,k))*0.5

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE avgx0

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE AVG2X0                     ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE avg2x0(a, nx,ny,nz,                                           &
           ibgn,iend,jbgn,jend,kbgn,kend, aavg)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform a spatial average operation on array a in the x direction.
!  Averaging is over the interval 2*deltax.
!  The averaged variable is defined at the midpoint between the two
!  points for the two input values.
!
!-----------------------------------------------------------------------
!
!  AUTHOR:  David E. Jahn
!  9/30/93
!
!  MODIFICATION HISTORY:
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    ibgn     Index in first dimension to begin multiplication
!    iend     Index in first dimension to end multiplication
!    jbgn     Index in second dimension to begin multiplication
!    jend     Index in second dimension to end multiplication
!    kbgn     Index in third dimension to begin multiplication
!    kend     Index in third dimension to end multiplication
!
!  OUTPUT:
!
!    aavg     Result of average operation on array a in x direction
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a   (nx,ny,nz)       ! Input array

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                ! Integer indicator of multiplication

  REAL :: aavg(nx,ny,nz)       ! Averaged array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        aavg(i,j,k)=(a(i-1,j,k)                                         &
                    +a(i+1,j,k))*0.5

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE avg2x0

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE AVGY0                      ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE avgy0(a, onvf, nx,ny,nz,                                      &
           ibgn,iend,jbgn,jend,kbgn,kend, aavg)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform a spatial average operation on array a in the y direction.
!  The average for the two input values is defined at the midpoint
!  between the two values.
!  If the averaged variable aavg is on the grid volume face, onvf =1
!  otherwise, onvf = 0
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/01/2 (K. Brewster)
!  Further facelift.
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume face,
!             onvf =1; otherwise, onvf = 0
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!  OUTPUT:
!
!    aavg     Result of average operation on array (a) in the
!             y direction
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a   (nx,ny,nz)       ! Input array
  INTEGER :: onvf              ! Integer grid point indicator

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                ! Integer indicator of multiplication
  REAL :: aavg(nx,ny,nz)       ! Average array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,jright,jleft
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF ( onvf == 1) THEN

    jright = 0
    jleft = -1

  ELSE

    jright = 1
    jleft = 0

  END IF

  DO k=kbgn,kend
    DO i=ibgn,iend
      DO j=jbgn,jend

        aavg(i,j,k)=(a(i,j+jright,k)+a(i,j+jleft,k))*0.5

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE avgy0

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE AVG2Y0                     ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE avg2y0(a, nx,ny,nz,                                           &
           ibgn,iend,jbgn,jend,kbgn,kend, aavg)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform a spatial average operation on array a in the y direction.
!  Averaging is over a distance 2y*delta-y.
!  The averaged variable is defined at the midpoint between the two
!  points for the two input values.
!
!-----------------------------------------------------------------------
!
!  AUTHOR:  David E. Jahn
!  9/30/93
!
!  MODIFICATION HISTORY:
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    ibgn     Index in first dimension to begin operation
!    iend     Index in first dimension to end operation
!    jbgn     Index in second dimension to begin operation
!    jend     Index in second dimension to end operation
!    kbgn     Index in third dimension to begin operation
!    kend     Index in third dimension to end operation
!
!  OUTPUT:
!
!    aavg     Result of average operation on array a in y direction
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a   (nx,ny,nz)       ! Input array

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                ! Integer indicator of multiplication
  REAL :: aavg(nx,ny,nz)       ! Average array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  DO k=kbgn,kend
    DO i=ibgn,iend
      DO j=jbgn,jend

        aavg(i,j,k)=(a(i,j+1,k)+a(i,j-1,k))*0.5

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE avg2y0


!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE AVGZ0                      ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE avgz0(a, onvf, nx,ny,nz,                                      &
           ibgn,iend,jbgn,jend,kbgn,kend, aavg)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform a spatial average operation on array (a) in the z direction.
!  The average of the two input values is defined at the midpoint
!  between the two values.
!  If the averaged variable aavg is on the grid volume face, onvf =1
!  otherwise, onvf = 0
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/01/2 (K. Brewster)
!  Further facelift.
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume
!             face,onvf =1; otherwise, onvf = 0
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!  OUTPUT:
!
!    aavg     Result of average operation on array (a) in the z direction
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a   (nx,ny,nz)       ! Input array
  INTEGER :: onvf              ! Integer grid point indicator

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                ! Integer indicator of multiplication
  REAL :: aavg(nx,ny,nz)       ! Average array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,kup,kdown
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF ( onvf == 1) THEN

    kup = 0
    kdown = -1

  ELSE

    kup = 1
    kdown = 0

  END IF

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        aavg(i,j,k)=(a(i,j,k+kup)+a(i,j,k+kdown ))*0.5

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE avgz0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE AVG2Z0                     ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE avg2z0(a, nx,ny,nz,                                           &
           ibgn,iend,jbgn,jend,kbgn,kend, aavg)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform a spatial average operation on array a in the z direction.
!  Averaging is over interval 2*deltaz.
!  The averaged variable is defined at the midpoint between the two
!  points for the two input values.
!
!-----------------------------------------------------------------------
!
!  AUTHOR:  David E. Jahn
!  9/30/93
!
!  MODIFICATION HISTORY:
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    ibgn     Index in first dimension to begin operation
!    iend     Index in first dimension to end operation
!    jbgn     Index in second dimension to begin operation
!    jend     Index in second dimension to end operation
!    kbgn     Index in third dimension to begin operation
!    kend     Index in third dimension to end operation
!
!  OUTPUT:
!
!    aavg     Result of average operation on array a in z direction
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a   (nx,ny,nz)       ! Input array

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                ! Integer indicator of multiplication
  REAL :: aavg(nx,ny,nz)       ! Average array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        aavg(i,j,k)=(a(i,j,k+1)+a(i,j,k-1))*0.5

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE avg2z0



!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DIFX0                      ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE difx0(a, onvf, nx,ny,nz,                                      &
           ibgn,iend,jbgn,jend,kbgn,kend, dx, adifx)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform a finite difference operation on an array (a) in the x
!  direction: adifx = d( a ) / dx. The output variable is defined at
!  the midpoint between the two points whose values are differenced
!  (i.e., the output and input arrays are staggered). The output
!  variable adifx is defined on the grid volume face when onvf =1;
!  otherwise, onvf = 0.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/01/2 (K. Brewster)
!  Further facelift.
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume
!             face, onvf =1; otherwise, onvf = 0
!
!    nx       First dimension of arrays a and adifx
!    ny       Second dimension of arrays a and adifx
!    nz       Third dimension of arrays a and adfix
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!    dx       Grid spacing in x direction (m)
!
!  OUTPUT:
!
!    adifx    Differenced array del(a)/delx
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a    (nx,ny,nz)      ! Input array
  INTEGER :: onvf              ! Integer grid point indicator

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                ! Integer indicator of multiplication
  REAL :: dx                   ! Grid spacing in x direction (m)
  REAL :: adifx(nx,ny,nz)      ! Differenced array del(a)/delx
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,iright,ileft
  REAL :: dxinv
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF ( onvf == 1) THEN

    iright = 0
    ileft = -1

  ELSE

    iright = 1
    ileft = 0

  END IF

  dxinv = 1.0/dx

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        adifx(i,j,k)=(a(i+iright,j,k)-a(i+ileft ,j,k))*dxinv

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE difx0

!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DIF2X0                     ######
!######                                                      ######
!######                Copyright (c) 1993                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE dif2x0(a, nx,ny,nz,                                           &
           ibgn,iend,jbgn,jend,kbgn,kend, dx, adifx)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform a centered finite difference operation on an array a over
!  2 grid distance in the x direction: adifx = d( a ) / dx.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: David E. Jahn
!  4/19/93
!
!  MODIFICATION HISTORY:
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    nx       First dimension of arrays a and adifx
!    ny       Second dimension of arrays a and adifx
!    nz       Third dimension of arrays a and adfix
!
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume
!             face,onvf =1; otherwise, onvf = 0
!
!    ibgn     Index in first dimension to begin operation
!    iend     Index in first dimension to end operation
!    jbgn     Index in second dimension to begin operation
!    jend     Index in second dimension to end operation
!    kbgn     Index in third dimension to begin operation
!    kend     Index in third dimension to end operation
!
!    dx       Grid spacing in x direction (m)
!
!  OUTPUT:
!
!    adifx    Differenced array del(a)/delx
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a    (nx,ny,nz)      ! Input array

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                ! Integer indicator of multiplication
  REAL :: dx                   ! Grid spacing in x direction (m)
  REAL :: adifx(nx,ny,nz)      ! Differenced array del(a)/delx

!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,iright,ileft
  REAL :: dxinv
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  dxinv = 0.5/dx

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        adifx(i,j,k)=(a(i+1,j,k)-a(i-1,j,k))*dxinv

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE dif2x0



!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DIFY0                      ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE dify0(a, onvf, nx,ny,nz,                                      &
           ibgn,iend,jbgn,jend,kbgn,kend, dy, adify)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform a finite difference operation on an array (a) in the y
!  direction: adify = d( a ) / dy. The output variable is defined at
!  the midpoint between the two points whose values are differenced
!  (i.e., the output and input arrays are staggered). The output
!  variable adifx is defined on the grid volume face when onvf =1;
!  otherwise, onvf = 0.

!  direction. adify = d( a ) / dy. The output variable is defined at
!  the midpoint between the two points whose values are differenced.
!  The output variable adify is defined on the grid volume face when
!  onvf =1; otherwise, onvf = 0.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/01/2 (K. Brewster)
!  Further facelift.
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume
!             face, onvf =1; otherwise, onvf = 0
!
!    nx       First dimension of arrays a and adify
!    ny       Second dimension of arrays a and adify
!    nz       Third dimension of arrays a and adify
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!    dy       Grid spacing in y direction (m)
!
!  OUTPUT:
!
!    adify    Differenced array del(a)/dely
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a   (nx,ny,nz)       ! Input array
  INTEGER :: onvf              ! Integer grid point indicator

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                ! Integer indicator of multiplication
  REAL :: dy                   ! Grid spacing in y direction (m)
  REAL :: adify(nx,ny,nz)      ! Differenced array del(a)/dely
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,jright,jleft
  REAL :: dyinv
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF ( onvf == 1) THEN

    jright = 0
    jleft = -1

  ELSE

    jright = 1
    jleft = 0

  END IF

  dyinv = 1.0/dy

  DO k=kbgn,kend
    DO i=ibgn,iend
      DO j=jbgn,jend

        adify(i,j,k)=(a(i,j+jright,k)-a(i,j+jleft,k))*dyinv

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE dify0

!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DIF2Y0                     ######
!######                                                      ######
!######                Copyright (c) 1993                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE dif2y0(a, nx,ny,nz,                                           &
           ibgn,iend,jbgn,jend,kbgn,kend, dy, adify)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform a centered finite difference operation on an array a over
!  2 grid distance in the y direction: adify = d( a ) / dy.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: David E. Jahn
!  4\19\93
!
!  MODIFICATION HISTORY:
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume
!             face, onvf =1; otherwise, onvf = 0
!
!    nx       First dimension of arrays a and adify
!    ny       Second dimension of arrays a and adify
!    nz       Third dimension of arrays a and adify
!
!    ibgn     Index in first dimension to begin operation
!    iend     Index in first dimension to end operation
!    jbgn     Index in second dimension to begin operation
!    jend     Index in second dimension to end operation
!    kbgn     Index in third dimension to begin operation
!    kend     Index in third dimension to end operation
!
!    dy       Grid spacing in y direction (m)
!
!  OUTPUT:
!
!    adify    Differenced array del(a)/dely
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a   (nx,ny,nz)       ! Input array

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                ! Integer indicator of multiplication
  REAL :: dy                   ! Grid spacing in y direction (m)
  REAL :: adify(nx,ny,nz)      ! Differenced array del(a)/dely
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,jright,jleft
  REAL :: dyinv
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  dyinv = 0.5/dy
!  
  DO k=kbgn,kend
    DO i=ibgn,iend
      DO j=jbgn,jend

        adify(i,j,k)=(a(i,j+1,k)-a(i,j-1,k))*dyinv

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE dif2y0


!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DIFZ0                      ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE difz0(a, onvf, nx,ny,nz,                                      &
           ibgn,iend,jbgn,jend,kbgn,kend, dz, adifz)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform a finite difference operation on an array (a) in the z
!  direction: adifz = d( a ) / dz. The output variable is defined at
!  the midpoint between the two points whose values are differenced
!  (i.e., the output and input arrays are staggered). The output
!  variable adifx is defined on the grid volume face when onvf =1;
!  otherwise, onvf = 0.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/01/2 (K. Brewster)
!  Further facelift.
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    nx       First dimension of arrays a and adifz
!    ny       Second dimension of arrays a and adifz
!    nz       Third dimension of arrays a and adifz
!
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume
!             face, onvf =1; otherwise, onvf = 0
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!    dz       Grid spacing in z direction (m)
!
!  OUTPUT:
!
!    adifz    Differenced array del(a)/delz
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a    (nx,ny,nz)      ! Input array
  INTEGER :: onvf              ! Integer grid point indicator

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                ! Integer indicator of multiplication
  REAL :: dz                   ! Grid spacing in z direction (m)
  REAL :: adifz(nx,ny,nz)      ! Differenced array del(a)/delz
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,kup,kdown
  REAL :: dzinv
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF ( onvf == 1) THEN

    kup = 0
    kdown = -1

  ELSE

    kup = 1
    kdown = 0

  END IF

  dzinv = 1.0/dz

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        adifz(i,j,k)=(a(i ,j,k+kup)-a(i ,j,k+kdown ))*dzinv

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE difz0

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DIFXX0                     ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE difxx0(a, nx,ny,nz,                                           &
           ibgn,iend,jbgn,jend,kbgn,kend, dx, adifxx)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the second order difference of an array (a) in the
!  x direction.  The operator is defined as del**2(a)/delx**2.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/01/2 (K. Brewster)
!  Further facelift.
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    nx       First dimension of arrays a and adifxx
!    ny       Second dimension of arrays a and adifxx
!    nz       Third dimension of arrays a and adifxx
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!    dx       Grid spacing in x direction (m)
!
!  OUTPUT:
!
!    adifxx   Differenced array del**2(a)/delx**2
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a    (nx,ny,nz)      ! Input array

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                ! Integer indicator of multiplication
  REAL :: dx                   ! Grid spacing in x direction (m)
  REAL :: adifxx(nx,ny,nz)     ! Differenced array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL :: dxinv2
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!-----------------------------------------------------------------------
!
!  NOTE:
!
!  The order of calculation in the following formula should not
!  be changed. The order affects the machine trunction error
!  in the result.
!
!-----------------------------------------------------------------------
!
  dxinv2 = 1.0/(dx*dx)

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend
        adifxx(i,j,k)=((a(i+1,j,k)-a(i,j,k))-(a(i,j,k)-a(i-1,j,k)))     &
                     *dxinv2

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE difxx0

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DIFYY0                     ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE difyy0(a, nx,ny,nz,                                           &
           ibgn,iend,jbgn,jend,kbgn,kend, dy, adifyy)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the second order finite difference of an array (a) in the
!  y direction.  The operator is defined as del**2(a)/dely**2.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/01/2 (K. Brewster)
!  Further facelift.
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        input array
!
!    nx       First dimension of arrays a and adifyy
!    ny       Second dimension of arrays a and adifyy
!    nz       Third dimension of arrays a and adifyy
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!    dy       grid spacing in y direction (m)
!
!  OUTPUT:
!
!    adifyy   difference array del**2(a)/dely**2
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz         ! Number of grid points in 3 directions

  REAL :: a     (nx,ny,nz)      ! Input array

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                 ! Integer indicator of multiplication
  REAL :: dy                    ! Grid spacing in y direction (m)
  REAL :: adifyy(nx,ny,nz)      ! Differenced array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL :: dyinv2
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  dyinv2 = 1.0/(dy*dy)

  DO k=kbgn,kend
    DO i=ibgn,iend
      DO j=jbgn,jend
!
!-----------------------------------------------------------------------
!
!  NOTE:
!
!  The order of calculation in the following formula should not
!  be changed. The order affects the machine trunction error
!  in the result.
!
!-----------------------------------------------------------------------
!
        adifyy(i,j,k)=((a(i,j+1,k)-a(i,j,k))-(a(i,j,k)-a(i,j-1,k)))     &
                     *dyinv2

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE difyy0

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DIFZZ0                     ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE difzz0(a, nx,ny,nz,                                           &
           ibgn,iend,jbgn,jend,kbgn,kend, dz, adifzz)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate the second order finite difference of an array (a) in the
!  z direction.  The operator is defined as del**2(a)/delz**2.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Ming Xue
!  10/10/91
!
!  MODIFICATION HISTORY:
!
!  5/05/92 (M. Xue)
!  Added full documentation.
!
!  6/01/2 (K. Brewster)
!  Further facelift.
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    nx       First dimension of arrays a and adifzz
!    ny       Second dimension of arrays a and adifzz
!    nz       Third dimension of arrays a and adifzz
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!    dz       Grid spacing in the vertical direction (m)
!
!  OUTPUT:
!
!    adifzz   Differenced array del**2(a)/delz**2
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz         ! Number of grid points in 3 directions

  REAL :: a     (nx,ny,nz)      ! Input array

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                 ! Integer indicator of multiplication
  REAL :: dz                    ! Grid spacing in the vertical
                                ! direction (m)
  REAL :: adifzz(nx,ny,nz)      ! Output difference array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL :: dzinv2
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!-----------------------------------------------------------------------
!
!  NOTE:
!
!  The order of calculation in the following formula should not
!  be changed. The order affects the machine trunction error
!  in the result.
!
!-----------------------------------------------------------------------
!
  dzinv2 = 1.0/(dz*dz)

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        adifzz(i,j,k)=((a(i,j,k+1)-a(i,j,k))-(a(i,j,k)-a(i,j,k-1)))     &
                     *dzinv2

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE difzz0

!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE AVGSU0                     ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE avgsu0(s,nx,ny,nz,jbgn,jend,kbgn,kend,su)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Average scalar array s to u points, up to the x boundary.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR:
!  2/15/93 (M. Xue and H. Jin)
!
!  MODIFICATION HISTORY:
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    s        An array defined at the scalar point.
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    jbgn     Starting point for j computation
!    jend     Ending point for j computation
!    kbgn     Starting point for k computation
!    kend     Ending point for k computation
!
!  OUTPUT:
!
!    su       An array averaged from array 's' defined at the
!             scalar point to the u-point.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  INTEGER :: jbgn,jend         ! Domain of j computations
  INTEGER :: kbgn,kend         ! Domain of k computations

  REAL :: s (nx,ny,nz)         ! Input array
  REAL :: su(nx,ny,nz)         ! Averaged array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'bndry.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  onvf = 1
  CALL avgx0(s, onvf,                                                    &
            nx,ny,nz, 2,nx-1, jbgn,jend, kbgn,kend, su)
  CALL bcsu0(nx,ny,nz,jbgn,jend,kbgn,kend,ebc,wbc,su)

  RETURN
END SUBROUTINE avgsu0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE AVGSV0                     ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE avgsv0(s,nx,ny,nz,ibgn,iend,kbgn,kend,sv)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Average scalar array s to v points, up to the y boundary.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR:
!  2/15/93 (M. Xue and H. Jin)
!
!  MODIFICATION HISTORY:
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    s        An array defined at the scalar point.
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!    ibgn     Starting point for i computation
!    iend     Ending point for i computation
!    kbgn     Starting point for k computation
!    kend     Ending point for k computation
!
!  OUTPUT:
!
!    su       An array averaged from array 's' defined at the
!             scalar point to the v-point.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  INTEGER :: ibgn,iend         ! Domain of i computations
  INTEGER :: kbgn,kend         ! Domain of k computations

  REAL :: s (nx,ny,nz)         ! Input array
  REAL :: sv(nx,ny,nz)         ! Averaged array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'bndry.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  onvf = 1
  CALL avgy0(s, onvf,                                                    &
            nx,ny,nz, ibgn,iend, 2,ny-1, kbgn,kend, sv)

  CALL bcsv0(nx,ny,nz,ibgn,iend,kbgn,kend,nbc,sbc,sv)

  RETURN
END SUBROUTINE avgsv0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE AVGSW0                     ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE avgsw0(s,nx,ny,nz,ibgn,iend,jbgn,jend,sw)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Average scalar array s to w points, up to k=1, and nz.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR:
!  2/15/93 (M. Xue and H. Jin)
!
!  MODIFICATION HISTORY:
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    s        An array defined at the scalar point.
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    ibgn     Starting point for i computation
!    iend     Ending point for i computation
!    jbgn     Starting point for j computation
!    jend     Ending point for j computation
!
!  OUTPUT:
!
!    sw       An array averaged from array 's' defined at the
!             scalar point to the w-point.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  INTEGER :: ibgn,iend         ! Domain of i computation
  INTEGER :: jbgn,jend         ! Domain of j computation

  REAL :: s (nx,ny,nz)         ! Input array
  REAL :: sw(nx,ny,nz)         ! Averaged array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'bndry.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  onvf = 1

  CALL avgz0(s, onvf,                                                    &
            nx,ny,nz, ibgn,iend, jbgn,jend, 2,nz-1, sw)

!!print*,'tbc,bbc==',tbc,bbc
  CALL bcsw0(nx,ny,nz,ibgn,iend,jbgn,jend,tbc,bbc,sw)

  RETURN
END SUBROUTINE avgsw0
!
!
!  The following perform the adjoints of operators. The code can be 
!  significantly reduced !!!!!!!!!!!!
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADAVGX0                    ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adavgx0(a, onvf, nx,ny,nz,                                    &
           ibgn,iend,jbgn,jend,kbgn,kend, aavg)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operation on AVGX.
!  AVGX performs a spatial average operation on array (a) in the x direction.
!  The average for the two input values is defined at the midpoint
!  between the two values.
!  If the averaged variable aavg is on the grid volume face, onvf =1
!  otherwise, onvf = 0
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  07/05/1994
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    a        Input array
!
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume face,
!             onvf =1; otherwise, onvf = 0
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!  OUTPUT:
!
!    aavg     Result of average operation on array (a) in the x direction
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions
!
  REAL :: a   (nx,ny,nz)       ! Input array
  INTEGER :: onvf
!
  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
!
  REAL :: aavg(nx,ny,nz)       ! Averaged array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,iright,ileft
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  IF ( onvf == 1) THEN

    iright = 0
    ileft = -1

  ELSE

    iright = 1
    ileft = 0

  END IF

! a = 0.0

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend
        a(i+iright,j,k) = a(i+iright,j,k) + aavg(i,j,k)*0.5
        a(i+ileft ,j,k) = a(i+ileft ,j,k) + aavg(i,j,k)*0.5
        aavg(i,j,k)=0.0

      END DO
    END DO    
  END DO

  RETURN
END SUBROUTINE adavgx0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADAVGY0                    ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adavgy0(a, onvf, nx,ny,nz,                                    &
           ibgn,iend,jbgn,jend,kbgn,kend, aavg)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operation on AVGY.
!  AVGY performs a spatial average operation on array a in the y direction.
!  The average for the two input values is defined at the midpoint
!  between the two values.
!  If the averaged variable aavg is on the grid volume face, onvf =1
!  otherwise, onvf = 0
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  07/07/1994
!
!  MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    a        Input array
!
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume face,
!             onvf =1; otherwise, onvf = 0
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!  OUTPUT:
!
!    aavg     Result of average operation on array (a) in the y direction
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions
!
  REAL :: a   (nx,ny,nz)       ! Input array
  INTEGER :: onvf
!
  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
!
  REAL :: aavg(nx,ny,nz)       ! Average array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,jright,jleft
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF ( onvf == 1) THEN

    jright = 0
    jleft = -1

  ELSE

    jright = 1
    jleft = 0

  END IF

! a = 0.0

  DO k=kbgn,kend
    DO i=ibgn,iend
      DO j=jbgn,jend

       a(i,j+jright,k) = a(i,j+jright,k) +aavg(i,j,k)*0.5
       a(i,j+jleft,k)  = a(i,j+jleft,k)  +aavg(i,j,k)*0.5
       aavg(i,j,k) = 0.0

      END DO
    END DO
  END DO


  RETURN
END SUBROUTINE adavgy0
!
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADAVGZ0                    ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adavgz0(a, onvf, nx,ny,nz,                                    &
           ibgn,iend,jbgn,jend,kbgn,kend, aavg)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
! 
!  Perform the adjoint operation on AVGZ.
!  AVGZ performs a spatial average operation
!  on array (a) in the z direction.
!  The average of the two input values is defined at the midpoint
!  between the two values.
!  If the averaged variable aavg is on the grid volume face, onvf =1
!  otherwise, onvf = 0
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  07/07/1994
!  
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
! 
!  INPUT:
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    a        Input array
! 
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume face,
!             onvf =1; otherwise, onvf = 0
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!  OUTPUT:
!   
!    aavg     Result of average operation on array (a) in the z direction
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!   
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions
!
  REAL :: a   (nx,ny,nz)       ! Input array
  INTEGER :: onvf
!
  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
!
  REAL :: aavg(nx,ny,nz)       ! Average array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,kup,kdown
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF ( onvf == 1) THEN
  
    kup = 0
    kdown = -1
    
  ELSE
  
    kup = 1
    kdown = 0
    
  END IF
  
! a = 0.0

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        a(i,j,k+kup)   = a(i,j,k+kup)   + aavg(i,j,k)*0.5
        a(i,j,k+kdown )= a(i,j,k+kdown )+ aavg(i,j,k)*0.5
        aavg(i,j,k) = 0.0

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE 

!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADDIFX0                    ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE addifx0(a, onvf, nx,ny,nz,                                    &
           ibgn,iend,jbgn,jend,kbgn,kend, dx, adifx)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  ADDIFX performs the adjoint operations on DIFX.
!  DIFX performs a finite difference operation
!  on an array (a) in the x direction.
!  adifx = d( a ) / dx. The output variable is defined at the midpoint
!  between the two points whose values are differenced.
!  The output variable adifx is defined on the grid volume face when
!  onvf =1; otherwise, onvf = 0.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  07/07/1994
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       First dimension of arrays a and adifx
!    ny       Second dimension of arrays a and adifx
!    nz       Third dimension of arrays a and adfix
!
!    a        Input array
!
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume face,
!             onvf =1; otherwise, onvf = 0
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!    dx       Grid spacing in x direction (m)
!
!  OUTPUT:
!
!    adifx    Differenced array del(a)/delx
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions
!
  REAL :: a    (nx,ny,nz)      ! Input array
  INTEGER :: onvf
!
  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
!
  REAL :: dx
  REAL :: adifx(nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,iright,ileft
  REAL :: dxinv
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF ( onvf == 1) THEN

    iright = 0
    ileft = -1

  ELSE

    iright = 1
    ileft = 0

  END IF

  dxinv = 1.0/dx

! a = 0.0

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        a(i+iright,j,k) = a(i+iright,j,k) + adifx(i,j,k)*dxinv
        a(i+ileft ,j,k) = a(i+ileft ,j,k) - adifx(i,j,k)*dxinv
        adifx(i,j,k) = 0.0

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE addifx0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADDIFY0                    ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE addify0(a, onvf, nx,ny,nz,                                    &
           ibgn,iend,jbgn,jend,kbgn,kend, dy, adify)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operaation on DIFY.
!  DIFY performs a finite difference operation on an
!  array (a) in the y direction.
!  adify = d( a ) / dy. The output variable is defined at the midpoint
!  between the two points whose values are differenced.
!  The output variable adify is defined on the grid volume face when
!  onvf =1; otherwise, onvf = 0.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  07/07/1994
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       First dimension of arrays a and adify
!    ny       Second dimension of arrays a and adify
!    nz       Third dimension of arrays a and adify
!
!    a        Input array
!
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume face,
!             onvf =1; otherwise, onvf = 0
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!    dy       Grid spacing in y direction (m)
!
!  OUTPUT:
!
!    adify    Differenced array del(a)/dely
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions
!
  REAL :: a   (nx,ny,nz)       ! Input array
  INTEGER :: onvf
!
  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
!
  REAL :: dy
  REAL :: adify(nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,jright,jleft
  REAL :: dyinv
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF ( onvf == 1) THEN

    jright = 0
    jleft = -1

  ELSE

    jright = 1
    jleft = 0

  END IF

  dyinv = 1.0/dy

! a = 0.0

  DO k=kbgn,kend
    DO i=ibgn,iend
      DO j=jbgn,jend

        a(i,j+jright,k) = a(i,j+jright,k) + adify(i,j,k)*dyinv
        a(i,j+jleft,k)  = a(i,j+jleft,k)  - adify(i,j,k)*dyinv
        adify(i,j,k)    = 0.0

      END DO
    END DO
  END DO
!
  RETURN
END SUBROUTINE addify0
!

!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADDIFZ0                    ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE addifz0(a, onvf, nx,ny,nz,                                    &
           ibgn,iend,jbgn,jend,kbgn,kend, dz, adifz)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operaation on DIFZ.
!  DIFZ performs a finite difference operation on an array
!  (a) in the z direction.
!  adifz = d( a ) / dz. The output variable is defined at the midpoint
!  between the two points whose values are differenced.
!  The output variable adifz is defined on the grid volume face when
!  onvf =1; otherwise, onvf = 0.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  07/07/1994
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       First dimension of arrays a and adifz
!    ny       Second dimension of arrays a and adifz
!    nz       Third dimension of arrays a and adifz
!
!    a        Input array
!
!    onvf     Integer grid point indicator
!             If the averaged variable aavg is on the grid volume face,
!             onvf =1; otherwise, onvf = 0
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!  OUTPUT:
!
!    adifz    Differenced array del(a)/delz
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions
!
  REAL :: a    (nx,ny,nz)      ! Input array
  INTEGER :: onvf
!
  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
!
  REAL :: dz
  REAL :: adifz(nx,ny,nz)      ! Differenced array del(a)/delz
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k,kup,kdown
  REAL :: dzinv
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF ( onvf == 1) THEN
  
    kup = 0
    kdown = -1
    
  ELSE
  
    kup = 1
    kdown = 0
    
  END IF
  
  dzinv = 1.0/dz
  
! a = 0.0
  
  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        a(i ,j,k+kup)   = a(i ,j,k+kup)   + adifz(i,j,k)*dzinv
        a(i ,j,k+kdown )= a(i ,j,k+kdown )- adifz(i,j,k)*dzinv
        adifz(i,j,k) = 0.0

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE addifz0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADAAMULT0                  ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adaamult0(a,b,nx,ny,nz,                                       &
           ibgn,iend,jbgn,jend,kbgn,kend, ab)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Perform the adjoint operation on AAMULT.
!  AAMULT calculates the element-wise product of arrays a and b.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  07/07/1994
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx       First dimension of arrays a, b and ab
!    ny       Second dimension of arrays a, b and ab
!    nz       Third dimension of arrays a, b and ab
!
!    a        First multiplier array in AAMULT, which must be
!             the costant input in the adjoint code.
!    ab       Element-wise product (a*b) over range specified by
!             the starting and ending indices given above.
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!  OUTPUT:
!
!    b        Second multiplier array in AAMULT.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions
!
  REAL :: a (nx,ny,nz)         ! Input array 1
  REAL :: b (nx,ny,nz)         ! Input array 2
!
  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
!
  REAL :: ab(nx,ny,nz)         ! Product array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        b(i,j,k)=a(i,j,k)*ab(i,j,k)
        ab(i,j,k) = 0.0

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE adaamult0
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADDIFXX0                   ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE addifxx0(a, nx,ny,nz,                                         &
           ibgn,iend,jbgn,jend,kbgn,kend, dx, adifxx)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Adjoint of DIFXX, which calculates the second order difference
!  of an array (a) in the
!  x direction.  The operator is defined as del**2(a)/delx**2.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  05/27/96
!
!  MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    nx       First dimension of arrays a and adifxx
!    ny       Second dimension of arrays a and adifxx
!    nz       Third dimension of arrays a and adifxx
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!    dx       Grid spacing in x direction (m)
!
!  OUTPUT:
!
!    adifxx   Differenced array del**2(a)/delx**2
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  REAL :: a    (nx,ny,nz)      ! Input array

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                ! Integer indicator of multiplication
  REAL :: dx                   ! Grid spacing in x direction (m)
  REAL :: adifxx(nx,ny,nz)     ! Differenced array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL :: dxinv2
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!-----------------------------------------------------------------------
!
!  NOTE:
!
!  The order of calculation in the following formula should not
!  be changed. The order affects the machine trunction error
!  in the result.
!
!-----------------------------------------------------------------------
!
  dxinv2 = 1.0/(dx*dx)
      
! a = 0.0

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend
        a(i+1,j,k) = a(i+1,j,k) + adifxx(i,j,k)*dxinv2
        a(i,j,k)   = a(i,j,k)   - adifxx(i,j,k)*dxinv2
        a(i,j,k)   = a(i,j,k)   - adifxx(i,j,k)*dxinv2
        a(i-1,j,k) = a(i-1,j,k) + adifxx(i,j,k)*dxinv2
        adifxx(i,j,k) = 0.0
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE addifxx0
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADDIFYY0                   ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE addifyy0(a, nx,ny,nz,                                         &
           ibgn,iend,jbgn,jend,kbgn,kend, dy, adifyy)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Adjoint of DIFYY which calculates the second order finite
!  difference of an array (a) in the
!  y direction.  The operator is defined as del**2(a)/dely**2.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  05/27/96
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        input array
!
!    nx       First dimension of arrays a and adifyy
!    ny       Second dimension of arrays a and adifyy
!    nz       Third dimension of arrays a and adifyy
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!    dy       grid spacing in y direction (m)
!
!  OUTPUT:
!
!    adifyy   difference array del**2(a)/dely**2
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz         ! Number of grid points in 3 directions

  REAL :: a     (nx,ny,nz)      ! Input array

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                 ! Integer indicator of multiplication
  REAL :: dy                    ! Grid spacing in y direction (m)
  REAL :: adifyy(nx,ny,nz)      ! Differenced array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL :: dyinv2
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  dyinv2 = 1.0/(dy*dy)

! a = 0.0

  DO k=kbgn,kend
    DO i=ibgn,iend
      DO j=jbgn,jend
!
!-----------------------------------------------------------------------
!
!  NOTE:
!  
!  The order of calculation in the following formula should not
!  be changed. The order affects the machine trunction error
!  in the result.
!
!-----------------------------------------------------------------------
!  
        a(i,j+1,k) = a(i,j+1,k) + adifyy(i,j,k)*dyinv2
        a(i,j,k)   = a(i,j,k)   - adifyy(i,j,k)*dyinv2        
        a(i,j,k)   = a(i,j,k)   - adifyy(i,j,k)*dyinv2
        a(i,j-1,k) = a(i,j-1,k) + adifyy(i,j,k)*dyinv2
        adifyy(i,j,k) = 0.0

      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE addifyy0

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADDIFZZ                    ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######    Center for Analysis and Prediction of Storms      ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE addifzz0(a, nx,ny,nz,                                         &
           ibgn,iend,jbgn,jend,kbgn,kend, dz, adifzz)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Adjoint of DIFZZ which calculates the second order finite
!  difference of an array (a) in the
!  z direction.  The operator is defined as del**2(a)/delz**2.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!  05/27/96
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    a        Input array
!
!    nx       First dimension of arrays a and adifzz
!    ny       Second dimension of arrays a and adifzz
!    nz       Third dimension of arrays a and adifzz
!
!    ibgn     i-index where multiplication begins.
!    iend     i-index where multiplication ends.
!    jbgn     j-index where multiplication begins.
!    jend     j-index where multiplication ends.
!    kbgn     k-index where multiplication begins.
!    kend     k-index where multiplication ends.
!
!    dz       Grid spacing in the vertical direction (m)
!
!  OUTPUT:
!
!    adifzz   Differenced array del**2(a)/delz**2
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz         ! Number of grid points in 3 directions

  REAL :: a     (nx,ny,nz)      ! Input array

  INTEGER :: ibgn,iend,jbgn,jend,kbgn,kend
                                 ! Integer indicator of multiplication
  REAL :: dz                    ! Grid spacing in the vertical
                                ! direction (m)
  REAL :: adifzz(nx,ny,nz)      ! Output difference array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL :: dzinv2
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!-----------------------------------------------------------------------
!
!  NOTE:
!
!  The order of calculation in the following formula should not
!  be changed. The order affects the machine trunction error
!  in the result.
!
!-----------------------------------------------------------------------
!
  dzinv2 = 1.0/(dz*dz)

! a = 0.0

  DO k=kbgn,kend
    DO j=jbgn,jend
      DO i=ibgn,iend

        a(i,j,k+1) = a(i,j,k+1) + adifzz(i,j,k)*dzinv2
        a(i,j,k)   = a(i,j,k)   - adifzz(i,j,k)*dzinv2
        a(i,j,k)   = a(i,j,k)   - adifzz(i,j,k)*dzinv2
        a(i,j,k-1) = a(i,j,k-1) + adifzz(i,j,k)*dzinv2
        adifzz(i,j,k) = 0.0

      END DO
    END DO
  END DO


  RETURN
END SUBROUTINE addifzz0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE AdAVGSV0                   ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adavgsv0(s,nx,ny,nz,ibgn,iend,kbgn,kend,sv)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Average scalar array s to v points, up to the y boundary.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR:
!  2/15/93 (M. Xue and H. Jin)
!
!  MODIFICATION HISTORY:
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    s        An array defined at the scalar point.
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    IBgn     Starting point for i computation
!    iend     Ending point for i computation
!    kbgn     Starting point for k computation
!    kend     Ending point for k computation
!
!  OUTPUT:
!
!    su       An array averaged from array 's' defined at the
!             scalar point to the v-point.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  INTEGER :: ibgn,iend         ! Domain of i computations
  INTEGER :: kbgn,kend         ! Domain of k computations

  REAL :: s (nx,ny,nz)         ! Input array
  REAL :: sv(nx,ny,nz)         ! Averaged array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'bndry.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
   CALL adbcsv0(nx,ny,nz,ibgn,iend,kbgn,kend,nbc,sbc,sv)

  onvf = 1
  CALL adavgy0(s, onvf,                                                    &
            nx,ny,nz, ibgn,iend, 2,ny-1, kbgn,kend, sv)

  RETURN
END SUBROUTINE adavgsv0

!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADAVGSU0                   ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adavgsu0(s,nx,ny,nz,jbgn,jend,kbgn,kend,su)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Average scalar array s to u points, up to the x boundary.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR:
!  2/15/93 (M. Xue and H. Jin)
!
!  MODIFICATION HISTORY:
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    s        An array defined at the scalar point.
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    jbgn     Starting point for j computation
!    jend     Ending point for j computation
!    kbgn     Starting point for k computation
!    kend     Ending point for k computation
!
!  OUTPUT:
!
!    su       An array averaged from array 's' defined at the
!             scalar point to the u-point.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  INTEGER :: jbgn,jend         ! Domain of j computations
  INTEGER :: kbgn,kend         ! Domain of k computations

  REAL :: s (nx,ny,nz)         ! Input array
  REAL :: su(nx,ny,nz)         ! Averaged array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'bndry.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  CALL adbcsu0(nx,ny,nz,jbgn,jend,kbgn,kend,ebc,wbc,su)

  onvf = 1
  CALL adavgx0(s, onvf,                                                    &
            nx,ny,nz, 2,nx-1, jbgn,jend, kbgn,kend, su)

  RETURN
END SUBROUTINE adavgsu0
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADAVGSW0                   ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######  Center for the Analysis and Prediction of Storms    ######
!######    University of Oklahoma.  All rights reserved.     ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE adavgsw0(s,nx,ny,nz,ibgn,iend,jbgn,jend,sw)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Average scalar array s to w points, up to k=1, and nz.
!
!-----------------------------------------------------------------------
!
!
!  AUTHOR:
!  2/15/93 (M. Xue and H. Jin)
!
!  MODIFICATION HISTORY:
!
!  9/10/94 (Weygandt & Y. Lu)
!  Cleaned up documentation.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    s        An array defined at the scalar point.
!
!    nx       First dimension of arrays a and aavg
!    ny       Second dimension of arrays a and aavg
!    nz       Third dimension of arrays a and aavg
!
!    ibgn     Starting point for i computation
!    iend     Ending point for i computation
!    jbgn     Starting point for j computation
!    jend     Ending point for j computation
!
!  OUTPUT:
!
!    sw       An array averaged from array 's' defined at the
!             scalar point to the w-point.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx, ny, nz        ! Number of grid points in 3 directions

  INTEGER :: ibgn,iend         ! Domain of i computation
  INTEGER :: jbgn,jend         ! Domain of j computation

  REAL :: s (nx,ny,nz)         ! Input array
  REAL :: sw(nx,ny,nz)         ! Averaged array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: onvf
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'bndry.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  onvf = 1

  CALL adbcsw0(nx,ny,nz,ibgn,iend,jbgn,jend,tbc,bbc,sw)
  print*,'tbc,bbc adjoint====',tbc,bbc
  CALL adavgz0(s, onvf,                                                    &
            nx,ny,nz, ibgn,iend, jbgn,jend, 2,nz-1, sw)

  RETURN
END SUBROUTINE adavgsw0
!
SUBROUTINE A3DMAX1(a,m1,m2,i1,i2,n1,n2,j1,j2,l1,l2,k1,k2,                 &
                 amax,amin,aver,arms)
      implicit none 

      integer m1,n1,l1,m2,n2,l2
      real    a(m1:m2,n1:n2,l1:l2)

      real amin, amax, aver, arms 
!                
      integer i1,i2,j1,j2,k1,k2,i,j,k 
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!     Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
      amax=a(i1,j1,k1)

      DO 10 k=k1,k2
      DO 10 j=j1,j2
      DO 10 i=i1,i2
        amax = max(amax, a(i,j,k))
 10   CONTINUE

      amin=a(i1,j1,k1)

      DO 20 k=k1,k2
      DO 20 j=j1,j2
      DO 20 i=i1,i2
        amin = min(amin, a(i,j,k))
 20   CONTINUE

      aver=0.0

      DO 30 k=k1,k2
      DO 30 j=j1,j2
      DO 30 i=i1,i2
        aver = aver + a(i,j,k)
 30   CONTINUE
        aver = aver/(i2-i1+1)/(j2-j1+1)/(k2-k1+1)


      arms=0.0

      DO 40 k=k1,k2
      DO 40 j=j1,j2
      DO 40 i=i1,i2
        arms = arms + (a(i,j,k)-aver)**2
 40   CONTINUE
        arms = sqrt(arms/(i2-i1+1)/(j2-j1+1)/(k2-k1+1))

      RETURN
      END

