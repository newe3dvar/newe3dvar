!
!##################################################################
!##################################################################
!######                                                      ######
!######                 SUBROUTINE GRADT                     ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculating the gradient of costfunction.
!
!-----------------------------------------------------------------------
!
!  AUTHOR:
!
!  Jidong Gao, CAPS, July, 2000
!
!-----------------------------------------------------------------------
!
SUBROUTINE gradt(numctr,ctrv,grad,                                      &
           gdu_err,gdv_err,gdp_err,gdt_err,gdq_err,gdw_err,gdqscalar_err,&
           u_ctr,v_ctr,p_ctr,t_ctr,q_ctr,w_ctr,qscalar_ctr, psi, phi,   &
           gdscal, nx,ny,nz,gdscal_ref,                                 &
           nvar,nzua,nzret,                                             &
           mapfct,j1,j2,j3,aj3x,aj3y,aj3z,j3inv,rhobar,                 &
           dnw,dn,fnm,fnp,cf1,cf2,cf3,     & ! Added for WRF divergence constraint
           rhostr,rhostru, rhostrv, rhostrw, div3,                      &
           mxsng,mxua,mxret,mxcolret,                                   &
           nsrcsng,nsrcua,nsrcret,ncat,                                 &
           npass,xs,ys,zs,x,y,z,zp,hterain,                             &
           icatg,xcor,nam_var,xsng,ysng,hgtsng,thesng,                  &
           obsng,odifsng,qobsng,qualsng,isrcsng,nlevsng,nobsng,         &
           xua,yua,hgtua,theua,                                         &
           obsua,odifua,qobsua,qualua,isrcua,nlevsua,nobsua,            &
           xretc,yretc,hgtretc,theretc,                                 &
           obsret,odifret,qobsret,qualret,                              &
           usesng,useua,                                                &
           iret,isrcret,nlevret,ncolret,                                &
           srcsng,srcua,srcret,                                         &
           iusesng,iuseua,iuseret,                                      &
           corsng,corua,corret,                                         &
           xsng_p,ysng_p,ihgtsng,xua_p,yua_p,ihgtua,                    &
           zsng_1,zsng_2,zua_1,zua_2,                                   &
           oanxsng,oanxua,oanxret,                                      &
           sngsw,uasw,radsw,lgtsw,cwpsw,tpwsw,retsw,aerisw,             &
           hradius_3d,hradius_3d_ref,                                   &
           radius_z,ref_mos_3d,radius_z_ref,                            &
           anx,tk,                                                      &
           u_grd,v_grd,p_grd,t_grd,q_grd,w_grd,qscalar_grd,             &
           tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,tem9,istatus )
!
!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  USE model_precision
  USE module_varpara

  USE module_radar_grid
  USE module_lightning
  USE module_aeri !JJ
  USE module_ensemble
  USE module_convobs
  USE module_cwp
  USE module_tpw
  USE module_PseudoQobs

  USE constraint_divergence
  USE constraint_diagnostic_div
  USE constraint_mslp
  USE constraint_thermo
  USE constraint_smooth

  IMPLICIT NONE
!
!  INCLUDE 'varpara.inc'
!
  INCLUDE 'phycst.inc'
  INCLUDE 'grid.inc'
  INCLUDE 'bndry.inc'
  INCLUDE 'globcst.inc'   ! to include mp_acct etc.
  INCLUDE 'mp.inc'
!-----------------------------------------------------------------------
!
!  Input Sizing Arguments
!
!-----------------------------------------------------------------------
!
  INTEGER, INTENT(IN) :: nx,ny,nz
  INTEGER, INTENT(IN) :: sngsw,uasw,radsw,lgtsw,retsw, aerisw
  INTEGER, INTENT(IN) :: cwpsw, tpwsw
  INTEGER, INTENT(IN) :: nvar
  INTEGER, INTENT(IN) :: nzua,nzret
  INTEGER, INTENT(IN) :: mxsng,mxua,mxret,mxcolret
  INTEGER, INTENT(IN) :: nsrcsng,nsrcua,nsrcret,ncat
  INTEGER, INTENT(IN) :: npass
  REAL,    INTENT(IN) :: radius_z(nx,ny,nz),radius_z_ref(nx,ny,nz)
  REAL,    INTENT(IN) :: hradius_3d(nx,ny,nz),hradius_3d_ref(nx,ny,nz)
  REAL,    INTENT(IN) :: ref_mos_3d(nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  input grid arguments
!
!-----------------------------------------------------------------------
!
  REAL :: x     (nx)           ! The x-coord. of the physical and
                               ! computational grid. Defined at u-point.
  REAL :: y     (ny)           ! The y-coord. of the physical and
                               ! computational grid. Defined at v-point.
  REAL :: z     (nz)           ! The z-coord. of the computational grid.
                               ! Defined at w-point on the staggered grid.
  REAL :: zp    (nx,ny,nz)     ! The physical height coordinate defined at
                               ! w-point of the staggered grid.
  REAL :: hterain(nx,ny)       ! The height of the terrain.

  REAL :: mapfct(nx,ny,8)      ! Map factors at scalar, u and v points

  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! defined as - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! defined as - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! defined as d( zp )/d( z ).
  REAL :: aj3x  (nx,ny,nz)     ! Coordinate transformation Jacobian defined
                               ! as d( zp )/d( z ) AVERAGED IN THE X-DIR.
  REAL :: aj3y  (nx,ny,nz)     ! Coordinate transformation Jacobian defined
                               ! as d( zp )/d( z ) AVERAGED IN THE Y-DIR.
  REAL :: aj3z  (nx,ny,nz)     ! Coordinate transformation Jacobian defined
                               ! as d( zp )/d( z ) AVERAGED IN THE Z-DIR.
  REAL :: j3inv (nx,ny,nz)     ! Inverse of j3
  REAL, INTENT(IN) :: rhobar(nx,ny,nz)     ! Base state density

  REAL, INTENT(IN) :: rhostr(nx,ny,nz)     ! Base state density rhobar times j3
  REAL :: rhostru(nx,ny,nz)    ! Averaged rhostr at u points (kg/m**3).
  REAL :: rhostrv(nx,ny,nz)    ! Averaged rhostr at v points (kg/m**3).
  REAL :: rhostrw(nx,ny,nz)    ! Averaged rhostr at w points (kg/m**3).

  REAL    :: xs(nx)
  REAL    :: ys(ny)
  REAL    :: zs(nx,ny,nz)

  REAL(P), INTENT(IN) :: dnw(nz), dn(nz), fnm(nz), fnp(nz)
  REAL(P), INTENT(IN) :: cf1, cf2, cf3

  INTEGER :: icatg(nx,ny)
  REAL    :: xcor(ncat,ncat)
!
!-----------------------------------------------------------------------
!
!  Input Observation Arguments
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=6) :: nam_var(nvar)
  REAL :: xsng(mxsng)
  REAL :: ysng(mxsng)
  REAL :: hgtsng(mxsng)
  REAL :: thesng(mxsng)
  REAL :: obsng(nvar,mxsng)
  REAL :: odifsng(nvar,mxsng)
  REAL :: qobsng(nvar,mxsng)
  INTEGER :: qualsng(nvar,mxsng)
  INTEGER :: isrcsng(mxsng)
  INTEGER :: nlevsng(mxsng)
  INTEGER :: nobsng

!  REAL :: xsng_p(mxsng),ysng_p(mxsng)
  DOUBLE PRECISION :: xsng_p(mxsng),ysng_p(mxsng)
  REAL    :: zsng_1(mxsng),zsng_2(mxsng)
  INTEGER :: ihgtsng(mxsng)

  REAL    :: xua(mxua)
  REAL    :: yua(mxua)
  REAL    :: hgtua(nzua,mxua)
  REAL    :: theua(nzua,mxua)
  REAL    :: obsua(nvar,nzua,mxua)
  REAL    :: odifua(nvar,nzua,mxua)
  REAL    :: qobsua(nvar,nzua,mxua)
  INTEGER :: qualua(nvar,nzua,mxua)
  INTEGER :: nlevsua(mxua)
  INTEGER :: isrcua(mxua)
  INTEGER :: nobsua

!  REAL :: xua_p(mxua),yua_p(mxua)
  DOUBLE PRECISION :: xua_p(mxua),yua_p(mxua)
  REAL    :: zua_1(nzua,mxua),zua_2(nzua,mxua)
  INTEGER :: ihgtua(nzua,mxua)

  LOGICAL, INTENT(IN) :: usesng(mxsng), useua(mxua)

  REAL    :: xretc(mxcolret)
  REAL    :: yretc(mxcolret)
  REAL    :: hgtretc(nzret,mxcolret)
  REAL    :: theretc(nzret,mxcolret)
  REAL    :: obsret(nvar,nzret,mxcolret)
  REAL    :: odifret(nvar,nzret,mxcolret)
  REAL    :: qobsret(nvar,nzret,mxcolret)
  INTEGER :: qualret(nvar,nzret,mxcolret)
  INTEGER :: nlevret(mxcolret)
  INTEGER :: iret(mxcolret)
  INTEGER :: isrcret(0:mxret)
  INTEGER :: ncolret

!
!-----------------------------------------------------------------------
!
!  Input Analysis Control Variables
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=8) :: srcsng(nsrcsng)
  CHARACTER (LEN=8) :: srcua (nsrcua )
  CHARACTER (LEN=8) :: srcret(nsrcret)

  INTEGER :: iusesng(0:nsrcsng)
  INTEGER :: iuseua(0:nsrcua)
  INTEGER :: iuseret(0:nsrcret)
!
!-----------------------------------------------------------------------
!
!  Output Variables at Observation Locations
!
!-----------------------------------------------------------------------
!
  REAL :: corsng(mxsng,nvar)
  REAL :: corua(nzua,mxua,nvar)
! REAL :: corret(nzret,mxcolret,nvar)
  REAL :: corret(1,1,1)

  REAL :: oanxsng(nvar,mxsng)
  REAL :: oanxua(nvar,nzua,mxua)
  REAL :: oanxret(nvar,nzret,mxcolret)
!
!-----------------------------------------------------------------------
!
!  Model Grid State
!
!-----------------------------------------------------------------------
!
  REAL, INTENT(IN) :: anx(nx,ny,nz,nvar)
  REAL, INTENT(IN) :: tk(nx,ny,nz)

!
!-----------------------------------------------------------------------
!
!  Work arrays
!
!-----------------------------------------------------------------------
!

  REAL, INTENT(OUT) ::   tem1(nx,ny,nz)
  REAL, INTENT(OUT) ::   tem2(nx,ny,nz)
  REAL, INTENT(OUT) ::   tem3(nx,ny,nz)
  REAL, INTENT(OUT) ::   tem4(nx,ny,nz)
  REAL, INTENT(OUT) ::   tem5(nx,ny,nz)
  REAL, INTENT(OUT) ::   tem6(nx,ny,nz)
  REAL, INTENT(OUT) ::   tem7(nx,ny,nz)
  REAL, INTENT(OUT) ::   tem8(nx,ny,nz)
  REAL, INTENT(OUT) ::   tem9(nx,ny,nz)

!
!-----------------------------------------------------------------------
!
!  Return status
!
!-----------------------------------------------------------------------
!
  INTEGER :: istatus
  INTEGER, SAVE :: itera = 0
!
!-----------------------------------------------------------------------
!
!  Misc.local variables
!
!-----------------------------------------------------------------------
!
  !INTEGER :: ibgn, iend, jbgn, jend
  REAL    :: ftabinv,setexp
  INTEGER :: i,j,k,l,nq,isrc,icol
  REAL    :: rpass,zrngsq,thrngsq


  REAL    :: uradc, vradc, wradc
  REAL(P) :: uval, vval, tval, pval, wval, qval
  REAL(P) :: qsval

!
!----------------------------------------------------------------------
!
!    input:
!    -----
!      numctr: dimension of control variables
!      ctrv:   current perturbation (1d arrays)
!
!    output:
!    ------
!     gradient of costfunction
!
!-----------------------------------------------------------------------
!
!
  INTEGER :: numctr,iflag,num
  REAL :: ctrv (numctr), grad (numctr)
  REAL :: gdu_err(nx,ny,nz)
  REAL :: gdv_err(nx,ny,nz)
  REAL :: gdp_err(nx,ny,nz)
  REAL :: gdt_err(nx,ny,nz)
  REAL :: gdq_err(nx,ny,nz)
  REAL :: gdw_err(nx,ny,nz)
  REAL :: gdscal(nx,ny,nz)
  REAL :: gdscal_ref(nx,ny,nz)
  REAL :: gdqscalar_err(nx,ny,nz,nscalarq)

  REAL ::   u_ctr(nx,ny,nz)
  REAL ::   v_ctr(nx,ny,nz)
  REAL ::   p_ctr(nx,ny,nz)
  REAL ::   t_ctr(nx,ny,nz)
  REAL ::   q_ctr(nx,ny,nz)
  REAL ::   w_ctr(nx,ny,nz)
  REAL ::     psi(nx,ny,nz)
  REAL ::     phi(nx,ny,nz)
  REAL :: qscalar_ctr(nx,ny,nz,nscalarq)

  REAL, INTENT(OUT) :: u_grd(nx,ny,nz), v_grd(nx,ny,nz), w_grd(nx,ny,nz)
  REAL, INTENT(OUT) :: p_grd(nx,ny,nz), t_grd(nx,ny,nz), q_grd(nx,ny,nz)
  REAL, INTENT(OUT) :: qscalar_grd(nx,ny,nz,nscalarq)

  !REAL, DIMENSION (:,:,:), ALLOCATABLE :: u,v,w,wcont
  !REAL, ALLOCATABLE :: qscalar_grd(:,:,:,:)

!  REAL,DIMENSION (:,:,:), ALLOCATABLE :: u_grd,v_grd,p_grd,t_grd,q_grd,w_grd

  REAL    :: sum1,sum2
  INTEGER :: isum

  REAL    :: div3(nx,ny,nz)

  INTEGER :: ivar
  REAL    :: temp1,temp2
  REAL(DP):: sumu,sumv,sumw,sump,sumt,sumqv,sumqs
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  itera = itera + 1

  !ibgn = 1
  !iend = nx-1
  !jbgn = 1
  !jend = ny-1
  !
  !IF (loc_x > 1)       ibgn = 2
  !IF (loc_x < nproc_x) iend = nx-2
  !
  !IF (loc_y > 1)       jbgn = 2
  !IF (loc_y < nproc_y) jend = ny-2


  !ALLOCATE ( u(nx,ny,nz), STAT = istatus )
  !CALL check_alloc_status(istatus, "gradt:u")
  !u = 0.0
  !ALLOCATE ( v(nx,ny,nz), STAT = istatus )
  !CALL check_alloc_status(istatus, "gradt:v")
  !v = 0.0
  !ALLOCATE ( w(nx,ny,nz), STAT = istatus )
  !CALL check_alloc_status(istatus, "gradt:w")
  !w = 0.0

  !IF (ref_opt ==1 .OR. hydro_opt==1) THEN
  !  ALLOCATE ( qscalar_grd(nx,ny,nz,nscalarq), STAT = istatus )
  !  CALL check_alloc_status(istatus, "gradt:qscalar_grd")
  !  qscalar_grd = 0.0
  !ELSE
  !  ALLOCATE ( qscalar_grd(1,1,1,1), STAT = istatus )
  !  CALL check_alloc_status(istatus, "gradt:qscalar_grd")
  !  qscalar_grd = 0.0
  !END IF

  !ALLOCATE ( wcont(nx,ny,nz), STAT = istatus )
  !CALL check_alloc_status(istatus, "gradt:wcont")
  !wcont = 0.0

  IF( ibeta2==1 ) gr_en = 0.0

! G. Ge (5/15/2012):
! control variables have been converted to analysis increments
! inside costf(...) function by appling recursive filter.
! some codes will need these increments to compute total variables
!  (such as nu, nv, nw, np, nt, nq, nqr, nqs, nqh )
! hence u_ctr, v_ctr, p_ctr, t_ctr, q_ctr, w_ctr will not be changed
! until at the end of gradt(...)
!
   psi(:,:,:)    = 0.
   phi(:,:,:)    = 0.
   ! u_ctr(:,:,:) = 0.
   ! v_ctr(:,:,:) = 0.
   ! p_ctr(:,:,:) = 0.
   ! t_ctr(:,:,:) = 0.
   ! q_ctr(:,:,:) = 0.
   ! w_ctr(:,:,:) = 0.
   !qr_ctr(:,:,:) = 0.
   !qs_ctr(:,:,:) = 0.
   !qh_ctr(:,:,:) = 0.

! CALL adtrans(numctr,nx,ny,nz,u,v,p_ctr,t_ctr,q_ctr,w,qr_ctr,qs_ctr,qh_ctr,ctrv,tem4)
! !see the above G. Ge description, this adtrans(...) will be called at the end
!
! All the following arrays are on scalar grid
!
  u_grd(:,:,:) = 0.
  v_grd(:,:,:) = 0.
  p_grd(:,:,:) = 0.
  t_grd(:,:,:) = 0.
  q_grd(:,:,:) = 0.
  w_grd(:,:,:) = 0.
  IF ( ref_opt(npass) == 1 .OR. hydro_opt == 1 ) THEN
    qscalar_grd= 0.
  END IF
!
!  observation for single level data
!--------------------------------------------------------------
!

  IF(sngsw > 0) THEN

    num = 0

!   CALL adlinearint_3d(nx,ny,nz,u_grd(1,1,1),x(1),ys(1),zs(1,1,1),     &
!          usesng, 1, mxsng, nlevsng, nobsng, 1,                        &
!          xsng_p,ysng_p,zsng_1,zsng_2,hgtsng,ihgtsng,corsng(1,1),tem4 )

!   CALL adlinearint_3d(nx,ny,nz,v_grd(1,1,1),xs(1),y(1),zs(1,1,1),     &
!          usesng, 1, mxsng, nlevsng, nobsng, 2,                        &
!          xsng_p,ysng_p,zsng_1,zsng_2,hgtsng,ihgtsng,corsng(1,2),tem4 )

!   CALL adlinearint_3d(nx,ny,nz,p_grd(1,1,1),xs(1),ys(1),zs(1,1,1),    &
!          usesng, 1, mxsng, nlevsng, nobsng, 3,                        &
!          xsng_p,ysng_p,zsng_1,zsng_2,hgtsng,ihgtsng,corsng(1,3),tem4 )

!   CALL adlinearint_3d(nx,ny,nz,t_grd(1,1,1),xs(1),ys(1),zs(1,1,1),    &
!          usesng, 1, mxsng, nlevsng, nobsng, 4,                        &
!          xsng_p,ysng_p,zsng_1,zsng_2,hgtsng,ihgtsng,corsng(1,4),tem4 )

!   CALL adlinearint_3d(nx,ny,nz,q_grd(1,1,1),xs(1),ys(1),zs(1,1,1),    &
!          usesng, 1, mxsng, nlevsng, nobsng, 5,                        &
!          xsng_p,ysng_p,zsng_1,zsng_2,hgtsng,ihgtsng,corsng(1,5),tem4 )

    DO i = 1, nobsng
      IF ( usesng(i) ) THEN
        CALL alinearint_2df(nx, ny, u_grd(:,:,2), xsng_p(i), ysng_p(i), corsng(i,1))
        CALL alinearint_2df(nx, ny, v_grd(:,:,2), xsng_p(i), ysng_p(i), corsng(i,2))
        CALL alinearint_2df(nx, ny, p_grd(:,:,2), xsng_p(i), ysng_p(i), corsng(i,3))
        CALL alinearint_2df(nx, ny, t_grd(:,:,2), xsng_p(i), ysng_p(i), corsng(i,4))
        CALL alinearint_2df(nx, ny, q_grd(:,:,2), xsng_p(i), ysng_p(i), corsng(i,5))
      END IF
    END DO

    IF (mp_opt > 0) THEN
      CALL mpsendrecv2dew(u_grd, nx, ny, nz, ebc, wbc, 0, tem1)
      CALL mpsendrecv2dns(u_grd, nx, ny, nz, nbc, sbc, 0, tem1)
      CALL mpsendrecv2dew(v_grd, nx, ny, nz, ebc, wbc, 0, tem1)
      CALL mpsendrecv2dns(v_grd, nx, ny, nz, nbc, sbc, 0, tem1)
      CALL mpsendrecv2dew(p_grd, nx, ny, nz, ebc, wbc, 0, tem1)
      CALL mpsendrecv2dns(p_grd, nx, ny, nz, nbc, sbc, 0, tem1)
      CALL mpsendrecv2dew(t_grd, nx, ny, nz, ebc, wbc, 0, tem1)
      CALL mpsendrecv2dns(t_grd, nx, ny, nz, nbc, sbc, 0, tem1)
      CALL mpsendrecv2dew(q_grd, nx, ny, nz, ebc, wbc, 0, tem1)
      CALL mpsendrecv2dns(q_grd, nx, ny, nz, nbc, sbc, 0, tem1)
    END IF

    iflag = 0
!
!  call alinearint_3d(nx,ny,nz,w_grd(1,1,1),xs(1),ys(1),zs(1,1,1),
!    :                6,xsng(i),ysng(i),hgtsng(i),obsng(6,i),iflag )
!
!
      IF(iflag == 1) num = num+1
!
!    print*, 'num of valid data in grad cal===',num
!
  END IF

  IF (convsw > 0) THEN

    CALL conv_gradt(nx, ny, nz, p_grd, t_grd, q_grd, u_grd, v_grd, tem1)

  END IF

!
!  observation cost function for upper level data
!  --------------------------------------------------------------
!
  IF(uasw > 0) THEN

    CALL adlinearint_3d(nx,ny,nz,u_grd(1,1,1),x(1),ys(1),zs(1,1,1),     &
             useua,nzua, mxua, nlevsua, nobsua, 1,                      &
             xua_p,yua_p,zua_1,zua_2,hgtua,ihgtua,corua(1,1,1), tem4 )

    CALL adlinearint_3d(nx,ny,nz,v_grd(1,1,1),xs(1),y(1),zs(1,1,1),     &
             useua,nzua, mxua, nlevsua, nobsua, 2,                      &
             xua_p,yua_p,zua_1,zua_2,hgtua,ihgtua,corua(1,1,2), tem4 )

    CALL adlinearint_3d(nx,ny,nz,p_grd(1,1,1),xs(1),ys(1),zs(1,1,1),    &
             useua,nzua, mxua, nlevsua, nobsua, 3,                      &
             xua_p,yua_p,zua_1,zua_2,hgtua,ihgtua,corua(1,1,3), tem4 )

    CALL adlinearint_3d(nx,ny,nz,t_grd(1,1,1),xs(1),ys(1),zs(1,1,1),    &
             useua,nzua, mxua, nlevsua, nobsua, 4,                      &
             xua_p,yua_p,zua_1,zua_2,hgtua,ihgtua,corua(1,1,4), tem4 )

    CALL adlinearint_3d(nx,ny,nz,q_grd(1,1,1),xs(1),ys(1),zs(1,1,1),    &
             useua,nzua, mxua, nlevsua, nobsua, 5,                      &
             xua_p,yua_p,zua_1,zua_2,hgtua,ihgtua,corua(1,1,5), tem4 )
!
!       CALL alinearint_3d(nx,ny,nz,w_grd(1,1,1),xs(1),ys(1),zs(1,1,1), &
!          nzua, mxua, nlevsua, nobsua,                                 &
!          6,xua_p,yua_p,zua_1,zua_2,hgtua,ihgtua,corua(1,1,6) )
!
  END IF

  IF (lgtsw > 0) THEN
    CALL lightning_gradt(nx,ny,nz,q_ctr,q_grd,istatus)
  END IF

  IF (aerisw > 0) THEN !JJ
    CALL aeri_gradt(nx,ny,nz,q_grd,t_grd,xs,ys,zs,tem4,istatus)
  END IF
!
!------------------------------------------------------------
! incorporate pseudo observation from cloud analysis
! in increment form (dqv, dpt)
!------------------------------------------------------------
!
  IF (cldfirst_opt(npass) ==1) THEN
    CALL pseudoQ_gradt(nx,ny,nz,nscalarq,ref_opt(npass),hydro_opt,      &
                       t_grd,q_grd,qscalar_grd,tem4,                    &
                       pseudo_pt(npass),pseudo_qv(npass),pseudo_qs(npass), & !add haiqin
                       istatus)
  END IF !end of cldfirst_opt ==1

!-----------------------------------------------------------------------
!
!  loading MSLP  (mslp)
!
! ----------------------------------------------------------------------
!
  IF ( do_mslp ) THEN

    CALL consmslp_gradt(nx,ny,p0,rddcp,zs(:,:,2),p_ctr(:,:,2),t_ctr(:,:,2), &
                        p_grd(:,:,2),istatus )
  END IF
!
! --------------------------------------------------------
!  loading cwp data
! --------------------------------------------------------
!
  IF(cwpsw > 0) THEN

    CALL cwp_gradt(nx,ny,nz,anx(:,:,:,PTR_P),tk,qscalar_grd,            &
                   tem1,tem2,tem3,tem4,tem5,tem6,tem7,tem8,istatus)

  END IF
!
! --------------------------------------------------------
!  loading tpw data
! --------------------------------------------------------
!
  IF(tpwsw > 0) THEN

    IF (idealCase_opt==1) THEN
      CALL tpw_gradt(nx,ny,nz,zp,rhobar,q_ctr,q_grd,tem1,tem2,istatus)
    ELSE IF (idealCase_opt==0) THEN
      CALL pw_gradt(nx,ny,nz,anx(:,:,:,PTR_P),q_ctr,q_grd,tem1,tem2,istatus)
    END IF

  END IF
!
! --------------------------------------------------------
!  loading radar data
! --------------------------------------------------------
!
  IF(radsw > 0) THEN

    CALL radargrid_gradt(nx,ny,nz,zs,u_grd,v_grd,w_grd,qscalar_grd,     &
                    p_ctr,t_ctr,qscalar_ctr,                            &
                    ref_mos_3d,intvl_T,thresh_ref,rhobar,               &
                    ibgn,iend,jbgn,jend,kbgn,kend,                      &
                    vrob_opt(npass),ref_opt(npass),                     &
                    tem1,tem2,tem3,tem4,tem5,tem6,                      &
                    tem7,tem8,tem9,istatus)

  END IF    !  IF(radsw > 0) THEN

  IF( div_opt== 1 ) THEN

    IF (modelopt == 1 .OR. use_arps_metrics) THEN
       CALL compute_adjdiv3_arps(npass,nx,ny,nz,dx,dy,dz,dxinv,dyinv,dzinv, &
                                 u_grd,v_grd,w_grd,div3,                &
                                 mapfct,j1,j2,j3,aj3z,j3inv,            &
                                 rhostr,rhostru,rhostrv,rhostrw,        &
                                 tem1,tem2,tem3,tem4,                   &
                                 tem5,tem6,tem7,tem8,istatus)
    ELSE
      ! call for WRF div3 adjoin
      CALL cal_adjdiv3_wrf( npass,nx,ny,nz,dxinv, dyinv,                &
                            u_grd, v_grd, w_grd, div3,                  &
                            !msfuy, msfvx, msftx, msfty,                &
                            mapfct(:,:,6),mapfct(:,:,7),mapfct(:,:,4),mapfct(:,:,5), &
                            dn, dnw, j3inv,                             &
                            fnm, fnp, cf1, cf2, cf3, j1, j2,            &
                            tem1,tem2,tem3, tem4,tem5,tem6,istatus )
    END IF


  END IF  !IF( div_opt== 1 ) THEN

!------------------------------------------------------------------
!  BEGIN: DPEC codes (diagnostic divergence equation constraint)
!------------------------------------------------------------------

  IF ( do_dpec .OR. do_hBal ) THEN
     CALL consdiv_gradt(nx,ny,nz,nscalarq,x,y,z,zp, j1,j2,j3,j3inv,mapfct, &
                        rhostr,dxinv,dyinv,dzinv,                          &
                        !u_ctr,v_ctr,w_ctr,p_ctr,t_ctr,q_ctr,qscalar_ctr,&
                        anx(:,:,:,PTR_U),anx(:,:,:,PTR_V),anx(:,:,:,PTR_W),tem1,    &
                        u_grd,v_grd,w_grd,p_grd,t_grd,q_grd,            &
                        qscalar_grd,istatus)
      !u=0.0; v=0.0; w=0.0
  END IF  ! do_dpec .OR. do_hBal

!------------------------------------------------------------------
!  END: DPEC codes (diagnostic divergence equation constraint)
!------------------------------------------------------------------
!
!  IF( thermo_opt == 1 ) THEN
!    CALL consthermo_gradt(mp_opt,nx,ny,nz,p0,rddcp,g,                   &
!                          dxinv,dyinv,dzinv,j3inv,                      &
!                          u_ctr,v_ctr,p_ctr,t_ctr,                      &
!                          anx(i,j,k,1),anx(i,j,k,2),                    &
!                          anx(i,j,k,3),anx(i,j,k,4),                    &
!                          u_grd,v_grd,p_grd,t_grd,istatus)
!  END IF
!!
!! currently smth_opt is not fully tested and not used
!!
!  IF( smth_opt== 1 ) THEN
!    CALL consmth_gradt(npass,nx,ny,nz,u_grd,v_grd,w_grd,istatus)
!  END IF
!!
!--IF cntl_var == 0 u & v are control varibles---------------------
!
!
!   sumu = 0.0
!   sumv = 0.0
!   sumw = 0.0
!   sump = 0.0
!   sumt = 0.0
!   sumqv = 0.0
!   sumqc = 0.0
!   sumqr = 0.0
!   sumqi = 0.0
!   sumqs = 0.0
!   sumqh = 0.0

!   DO k = 1, nz
!     DO j = 1, ny
!       DO i = 1, nx
!         sumu =sumu +  u_grd(i,j,k)**2
!         sumv =sumv +  v_grd(i,j,k)**2
!         sumw =sumw +  w_grd(i,j,k)**2
!         sump =sump +  p_grd(i,j,k)**2
!         sumt =sumt +  t_grd(i,j,k)**2
!         sumqv=sumqv+  q_grd(i,j,k)**2
!         sumqc=sumqc+ qc_grd(i,j,k)**2
!         sumqr=sumqr+ qr_grd(i,j,k)**2
!         sumqi=sumqi+ qi_grd(i,j,k)**2
!         sumqs=sumqs+ qs_grd(i,j,k)**2
!         sumqh=sumqh+ qh_grd(i,j,k)**2
!       END DO
!     END DO
!   END DO
!     print*,'sumu===',sqrt( sumu/nx/ny/nz )
!     print*,'sumv===',sqrt( sumv/nx/ny/nz )
!     print*,'sumw===',sqrt( sumw/nx/ny/nz )
!     print*,'sump===',sqrt( sump/nx/ny/nz )
!     print*,'sumt===',sqrt( sumt/nx/ny/nz )
!     print*,'sumqv==',sqrt( sumqv/nx/ny/nz )
!     print*,'sumqc==',sqrt( sumqc/nx/ny/nz )
!     print*,'sumqr==',sqrt( sumqr/nx/ny/nz )
!     print*,'sumqi==',sqrt( sumqi/nx/ny/nz )
!     print*,'sumqs==',sqrt( sumqs/nx/ny/nz )
!     print*,'sumqh==',sqrt( sumqh/nx/ny/nz )


  !IF( cntl_var == 0 ) THEN

    DO k = 1, nz
      DO j = 1, ny
        DO i = 1, nx
          psi(i,j,k)= u_grd(i,j,k)
          phi(i,j,k)= v_grd(i,j,k)
        END DO
      END DO
    END DO

  !ELSE    ! Should be checked for MPI
  !
  !  DO k = 1, nz
  !    IF (loc_x == nproc_x .AND. loc_y == nproc_y) THEN
  !      v_grd(nx-1,ny,k)=v_grd(nx-1,ny,k)+0.5*v_grd(nx,ny,k)
  !      v_grd(nx,ny-1,k)=v_grd(nx,ny-1,k)+0.5*v_grd(nx,ny,k)
  !      v_grd(nx,ny,k)=0.0
  !      u_grd(nx-1,ny,k)=u_grd(nx-1,ny,k)+0.5*u_grd(nx,ny,k)
  !      u_grd(nx,ny-1,k)=u_grd(nx,ny-1,k)+0.5*u_grd(nx,ny,k)
  !      u_grd(nx,ny,k)=0.0
  !    END IF
  !
  !    IF (loc_x == nproc_x .AND. loc_y == 1) THEN
  !      v_grd(nx,2,k)=v_grd(nx,2,k)+0.5*v_grd(nx,1,k)
  !      v_grd(nx-1,1,k)=v_grd(nx-1,1,k)+0.5*v_grd(nx,1,k)
  !      v_grd(nx,1,k)=0.0
  !      u_grd(nx,2,k)=u_grd(nx,2,k)+0.5*u_grd(nx,1,k)
  !      u_grd(nx-1,1,k)=u_grd(nx-1,1,k)+0.5*u_grd(nx,1,k)
  !      u_grd(nx,1,k)=0.0
  !    END IF
  !
  !    IF (loc_x == 1 .AND. loc_y == nproc_y) THEN
  !      v_grd(2,ny,k)=v_grd(2,ny,k)+0.5*v_grd(1,ny,k)
  !      v_grd(1,ny-1,k)=v_grd(1,ny-1,k)+0.5*v_grd(1,ny,k)
  !      v_grd(1,ny,k)=0.0
  !      u_grd(2,ny,k)=u_grd(2,ny,k)+0.5*u_grd(1,ny,k)
  !      u_grd(1,ny-1,k)=u_grd(1,ny-1,k)+0.5*u_grd(1,ny,k)
  !      u_grd(1,ny,k)=0.0
  !    END IF
  !
  !    IF (loc_x == 1 .AND. loc_y == 1) THEN
  !      v_grd(1,2,k)=v_grd(1,2,k)+0.5*v_grd(1,1,k)
  !      v_grd(2,1,k)=v_grd(2,1,k)+0.5*v_grd(1,1,k)
  !      v_grd(1,1,k)=0.0
  !      u_grd(1,2,k)=u_grd(1,2,k)+0.5*u_grd(1,1,k)
  !      u_grd(2,1,k)=u_grd(2,1,k)+0.5*u_grd(1,1,k)
  !      u_grd(1,1,k)=0.0
  !    END IF
  !
  !    IF (loc_y == nproc_y) THEN
  !      DO i=2,nx-1
  !        v_grd(i,ny-2,k)=v_grd(i,ny-2,k)-v_grd(i,ny,k)
  !        v_grd(i,ny-1,k)=v_grd(i,ny-1,k)+v_grd(i,ny,k)
  !        v_grd(i,ny-1,k)=v_grd(i,ny-1,k)+v_grd(i,ny,k)
  !        v_grd(i,ny,k)=0.0
  !        u_grd(i,ny-2,k)=u_grd(i,ny-2,k)-u_grd(i,ny,k)
  !        u_grd(i,ny-1,k)=u_grd(i,ny-1,k)+u_grd(i,ny,k)
  !        u_grd(i,ny-1,k)=u_grd(i,ny-1,k)+u_grd(i,ny,k)
  !        u_grd(i,ny,k)=0.0
  !      END DO
  !    END IF
  !
  !    IF (loc_y == 1) THEN
  !      DO i=2,nx-1
  !        v_grd(i,   3,k)=v_grd(i,   3,k)-v_grd(i, 1,k)
  !        v_grd(i,   2,k)=v_grd(i,   2,k)+v_grd(i, 1,k)
  !        v_grd(i,   2,k)=v_grd(i,   2,k)+v_grd(i, 1,k)
  !        v_grd(i, 1,k)=0.0
  !        u_grd(i,   3,k)=u_grd(i,   3,k)-u_grd(i, 1,k)
  !        u_grd(i,   2,k)=u_grd(i,   2,k)+u_grd(i, 1,k)
  !        u_grd(i,   2,k)=u_grd(i,   2,k)+u_grd(i, 1,k)
  !        u_grd(i, 1,k)=0.0
  !      END DO
  !    END IF
  !
  !    IF (loc_x == nproc_x) THEN
  !      DO j=2,ny-1
  !        v_grd(nx-2,j,k)=v_grd(nx-2,j,k)-v_grd(nx,j,k)
  !        v_grd(nx-1,j,k)=v_grd(nx-1,j,k)+v_grd(nx,j,k)
  !        v_grd(nx-1,j,k)=v_grd(nx-1,j,k)+v_grd(nx,j,k)
  !        v_grd(nx,j,k)=0.0
  !        u_grd(nx-2,j,k)=u_grd(nx-2,j,k)-u_grd(nx,j,k)
  !        u_grd(nx-1,j,k)=u_grd(nx-1,j,k)+u_grd(nx,j,k)
  !        u_grd(nx-1,j,k)=u_grd(nx-1,j,k)+u_grd(nx,j,k)
  !        u_grd(nx,j,k)=0.0
  !      END DO
  !    END IF
  !
  !    IF (loc_x == 1) THEN
  !      DO j=2,ny-1
  !        v_grd(3,j,k)=v_grd(   3,j,k)-v_grd( 1,j,k)
  !        v_grd(2,j,k)=v_grd(   2,j,k)+v_grd( 1,j,k)
  !        v_grd(2,j,k)=v_grd(   2,j,k)+v_grd( 1,j,k)
  !        v_grd(1,j,k)=0.0
  !        u_grd(3,j,k)=u_grd(   3,j,k)-u_grd( 1,j,k)
  !        u_grd(2,j,k)=u_grd(   2,j,k)+u_grd( 1,j,k)
  !        u_grd(2,j,k)=u_grd(   2,j,k)+u_grd( 1,j,k)
  !        u_grd(1,j,k)=0.0
  !      END DO
  !    END IF
  !
  !    DO j = 2, ny-1
  !      DO i = 2, nx-1
  !
  !        u_grd(i,j,k)   = u_grd(i,j,k)*mapfct(i,j,2)
  !        psi(i-1,j+1,k) = psi(i-1,j+1,k) + u_grd(i,j,k)/dy/4.
  !        psi(i,  j+1,k) = psi(i,  j+1,k) + u_grd(i,j,k)/dy/4.
  !        psi(i-1,j-1,k) = psi(i-1,j-1,k) - u_grd(i,j,k)/dy/4.
  !        psi(i,  j-1,k) = psi(i,  j-1,k) - u_grd(i,j,k)/dy/4.
  !
  !        phi(i,  j,k)  = phi(i,  j,k) + u_grd(i,j,k)/dx
  !        phi(i-1,j,k)  = phi(i-1,j,k) - u_grd(i,j,k)/dx
  !        u_grd(i,j,k)  = 0.0   ! unnecessary - WYH
  !
  !        v_grd(i,j,k)   = v_grd(i,j,k)*mapfct(i,j,3)
  !        psi(i+1,j-1,k) = psi(i+1,j-1,k) + v_grd(i,j,k)/dx/4.
  !        psi(i+1,  j,k) = psi(i+1,  j,k) + v_grd(i,j,k)/dx/4.
  !        psi(i-1,j-1,k) = psi(i-1,j-1,k) - v_grd(i,j,k)/dx/4.
  !        psi(i-1,j,  k) = psi(i-1,  j,k) - v_grd(i,j,k)/dx/4.
  !
  !        phi(i,  j,k)  = phi(i,  j,k) + v_grd(i,j,k)/dy
  !        phi(i,j-1,k)  = phi(i,j-1,k) - v_grd(i,j,k)/dy
  !        v_grd(i,j,k)  = 0.0   ! unnecessary - WYH
  !
  !      END DO
  !    END DO
  !
  !    IF (loc_x > 1) THEN ! count 1st column's effect on 2nd column
  !      DO j = 2, ny-1
  !        v_grd(1,j,k) = v_grd(1,j,k)*mapfct(1,j,3)
  !        psi(2,j-1,k) = psi(2,j-1,k) + v_grd(1,j,k)/dx/4.
  !        psi(2,  j,k) = psi(2,  j,k) + v_grd(1,j,k)/dx/4.
  !        v_grd(1,j,k) = 0.0
  !      END DO
  !    END IF
  !
  !    IF (loc_y > 1) THEN ! count 1st row's effect on 2nd row
  !      DO i = 2, nx-1
  !        u_grd(i,1,k) = u_grd(i,1,k)*mapfct(i,1,2)
  !        psi(i-1,2,k) = psi(i-1,2,k) + u_grd(i,1,k)/dy/4.
  !        psi(i,  2,k) = psi(i,  2,k) + u_grd(i,1,k)/dy/4.
  !        u_grd(i,1,k) = 0.0
  !      END DO
  !    END IF
  !  END DO
  !
  !  !
  !  ! Msg for u_grd, v_grd, psi, phi
  !  !
  !  IF (mp_opt > 0) THEN   ! Maybe not exactly the same, should be checked later
! !     CALL acct_interrupt(mp_acct)
  !    CALL mpsendrecv2dew(psi, nx, ny, nz, ebc, wbc, 1, tem4)
  !    CALL mpsendrecv2dns(psi, nx, ny, nz, nbc, sbc, 1, tem4)
  !
  !    CALL mpsendrecv2dew(phi, nx, ny, nz, ebc, wbc, 2, tem4)
  !    CALL mpsendrecv2dns(phi, nx, ny, nz, nbc, sbc, 2, tem4)
! !     CALL acct_stop_inter
  !  END IF
  !
  !
  !END IF

  IF( ibeta2==1 ) THEN

    CALL acct_interrupt(ensemble_acct)
     !sumu = 0.0
     !DO l=1, nensvirtl
     !  DO k = 1, nz
     !    DO j = 1, ny
     !      DO i = 1, nx
     !        sumu =sumu +  gr_en(i,j,k,l)**2
     !      END DO
     !    END DO
     !  END DO
     !END DO
     !print*,'s1m_en=',sqrt( sumu/nx/ny/nz/nensvirtl )

    CALL ens_gradc_gr(nx,ny,nz,nscalarq,flag_legacyfilter,qobsrad_bdyzone(npass),&
                      npass,ipass_filt(npass),ref_opt(npass), hydro_opt,         &
                      cwpsw,psi,phi,w_grd,t_grd,p_grd,q_grd,qscalar_grd,         &
                      tem1,tem2,tem3,tem4,tem5,istatus)



     !sumu = 0.0
     !DO l=1, nensvirtl
     !  DO k = 1, nz
     !    DO j = 1, ny
     !      DO i = 1, nx
     !        sumu =sumu +  gr_en(i,j,k,l)**2
     !      END DO
     !    END DO
     !  END DO
     !END DO
     !print*,'s3m_en=',sqrt( sumu/nx/ny/nz/nensvirtl )
     !stop
     CALL acct_stop_inter

  END IF ! ibeta2==1

  IF( ibeta1==1 ) THEN

    psi(:,:,:)   =   psi(:,:,:)*beta1
    phi(:,:,:)   =   phi(:,:,:)*beta1
    p_grd(:,:,:) = p_grd(:,:,:)*beta1
    t_grd(:,:,:) = t_grd(:,:,:)*beta1
    q_grd(:,:,:) = q_grd(:,:,:)*beta1
    w_grd(:,:,:) = w_grd(:,:,:)*beta1

    IF (ref_opt(npass)==1 .OR. hydro_opt==1) THEN
        qscalar_grd(:,:,:,:)= qscalar_grd(:,:,:,:)*beta1
    END IF

    CALL acct_interrupt(recrsub3_acct)
    IF( flag_legacyfilter ==1 ) THEN

      tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
      CALL  vbl_to_ctr(ipass_filt(npass),hradius_3d,radius_z,           &
                       nx,ny,nz,1,gdu_err,gdscal,  psi,                 &
                       tem1,tem2,tem3,tem4)

      tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
      CALL  vbl_to_ctr(ipass_filt(npass),hradius_3d,radius_z,           &
                       nx,ny,nz,2,gdv_err,gdscal,  phi,                 &
                       tem1,tem2,tem3,tem4)

      tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
      CALL  vbl_to_ctr(ipass_filt(npass),hradius_3d,radius_z,           &
                       nx,ny,nz,3,gdw_err,gdscal,w_grd,                 &
                       tem1,tem2,tem3,tem4)

      tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
      CALL  vbl_to_ctr(ipass_filt(npass),hradius_3d,radius_z,           &
                       nx,ny,nz,0,gdp_err,gdscal,p_grd,                 &
                       tem1,tem2,tem3,tem4)

      tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
      CALL  vbl_to_ctr(ipass_filt(npass),hradius_3d,radius_z,           &
                       nx,ny,nz,0,gdt_err,gdscal,t_grd,                 &
                       tem1,tem2,tem3,tem4)

      tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
      CALL  vbl_to_ctr(ipass_filt(npass),hradius_3d,radius_z,           &
                       nx,ny,nz,0,gdq_err,gdscal,q_grd,                 &
                       tem1,tem2,tem3,tem4)

!      CALL  vbl_to_ctr(ipass_filt(npass),0.5*hradius_ref(npass),radius_z_ref,       &
!                       nx,ny,nz,0,gdq_err,gdscal,q_grd,                 &
!                       tem1,tem2,tem3,tem4)

      IF (ref_opt(npass)==1 .OR. hydro_opt==1) THEN

        DO nq = 1, nscalarq
          tem1=0.0;tem2=0.0;tem3=0.0;tem4=0.0
          CALL vbl_to_ctr(ipass_filt(npass),hradius_3d_ref,radius_z_ref, &
                          nx,ny,nz,0,gdqscalar_err(:,:,:,nq),gdscal_ref,&
                          qscalar_grd(:,:,:,nq),tem1,tem2,tem3,tem4)
        END DO
      END IF  ! end of ref_opt .OR. hydro_opt==1

    ELSE ! (flag_legacyfilter ==2 ) then

      !CALL print3dnc_lc(100+itera*10,'ugrd',psi,  nx,ny,nz,2,2)
      !CALL print3dnc_lc(100+itera*10,'vgrd',phi,  nx,ny,nz,2,2)
      !CALL print3dnc_lc(100+itera*10,'wgrd',w_grd,nx,ny,nz,2,2)
      CALL  adjointfilter(nx,ny,nz,nscalarq,psi,phi,p_grd,t_grd,q_grd,w_grd,qscalar_grd,&
                       gdu_err,gdv_err,gdp_err,gdt_err,gdq_err,gdw_err,                 &
                       gdqscalar_err, tem4 )
      !CALL print3dnc_lc(200+itera*10,'ugrd',psi,  nx,ny,nz,2,2)
      !CALL print3dnc_lc(200+itera*10,'vgrd',phi,  nx,ny,nz,2,2)
      !CALL print3dnc_lc(200+itera*10,'wgrd',w_grd,nx,ny,nz,2,2)
    END IF
    CALL  acct_stop_inter

  END IF  !ibeta1

!
!------------return gradients from scalars to vector points--------------
!

  ! reuse u_ctr, v_ctr, w_ctr since it will be reset in costf of next interation
  !
  u_ctr=0.0; v_ctr=0.0; p_ctr=0.0;  t_ctr=0.0; q_ctr=0.0; w_ctr=0.0;  qscalar_ctr=0.0
  CALL adtrans(numctr,nx,ny,nz,nscalarq,u_ctr,v_ctr,p_ctr,t_ctr,q_ctr,w_ctr,qscalar_ctr,&
               ctrv,tem4,npass)


  !w_ctr = 0.0  ! w_ctr will be actually w_grd below
  !DO k=1, nz-1
  !  DO j=1, ny-1
  !    DO i=1, nx-1
  !      w_ctr(i,j,k)   = w_ctr(i,j,k)  + 0.5*w_grd(i,j,k)
  !      w_ctr(i,j,k+1) = w_ctr(i,j,k+1)+ 0.5*w_grd(i,j,k)
  !    END DO
  !  END DO
  !END DO

  DO k = 1, nz-1
    DO j = 1, ny-1
      DO i = 1, nx
        u_ctr(i,j,k) = psi(i,j,k) + u_ctr(i,j,k)
      END DO
    END DO
  END DO


  DO k = 1, nz-1
    DO j = 1, ny
      DO i = 1, nx-1
        v_ctr(i,j,k) = phi(i,j,k) + v_ctr(i,j,k)
      END DO
    END DO
  END DO

  DO k = 1, nz
    DO j = 1, ny-1
      DO i = 1, nx-1
        w_ctr(i,j,k) = w_grd(i,j,k) + w_ctr(i,j,k)
      END DO
    END DO
  END DO

  DO k = 1, nz-1
    DO j = 1, ny-1
      DO i = 1, nx-1
        p_grd(i,j,k) = p_grd(i,j,k) + p_ctr(i,j,k)
        t_grd(i,j,k) = t_grd(i,j,k) + t_ctr(i,j,k)
        q_grd(i,j,k) = q_grd(i,j,k) + q_ctr(i,j,k)
      END DO
    END DO
  END DO

  IF (ref_opt(npass) ==1 .OR. hydro_opt==1) THEN
    DO nq = 1, nscalarq
      DO k = 1, nz-1
        DO j = 1, ny-1
          DO i = 1, nx-1
            qscalar_grd(i,j,k,nq) = qscalar_grd(i,j,k,nq) + qscalar_ctr(i,j,k,nq)
          END DO
        END DO
      END DO
    END DO
  END IF
!-----------------------------------------------------------------------
!
! Debugging message
!
!-----------------------------------------------------------------------

  IF (lvldbg > 1) THEN

    !CALL print3dnc_lg(100+itera*10,'ugrd',u_grd,nx,ny,nz)
    !CALL print3dnc_lg(100+itera*10,'vgrd',v_grd,nx,ny,nz)
    !CALL print3dnc_lg(100+itera*10,'wgrd',w_grd,nx,ny,nz)

    !CALL print3dnc_lc(200+itera*10,'ugrd',u_grd,nx,ny,nz,2,2)
    !CALL print3dnc_lc(200+itera*10,'vgrd',v_grd,nx,ny,nz,2,2)
    !CALL print3dnc_lc(200+itera*10,'wgrd',w_grd,nx,ny,nz,2,2)

    sumu = 0.0
    sumv = 0.0
    sumw = 0.0
    sump = 0.0
    sumt = 0.0
    sumqv = 0.0
    sumqs = 0.0

    DO k = kbgn, kend
      DO j = jbgn, jend
        DO i = ibgn, iendu
          sumu =sumu +  u_grd(i,j,k)**2
        END DO
      END DO
    END DO

    DO k = kbgn, kend
      DO j = jbgn, jendv
        DO i = ibgn, iend
          sumv =sumv +  v_grd(i,j,k)**2
        END DO
      END DO
    END DO

    DO k = kbgn, kendw
      DO j = jbgn, jend
        DO i = ibgn, iend
          sumw =sumw +  w_grd(i,j,k)**2
        END DO
      END DO
    END DO

    DO k = kbgn, kend
      DO j = jbgn, jend
        DO i = ibgn, iend
          sump =sump +  p_grd(i,j,k)**2
          sumt =sumt +  t_grd(i,j,k)**2
          sumqv=sumqv+  q_grd(i,j,k)**2
          DO nq = 1, nscalarq
            sumqs=sumqs+ qscalar_grd(i,j,k,nq)**2
          END DO
        END DO
      END DO
    END DO

    CALL mpsumdp( sumu,  1 )
    CALL mpsumdp( sumv,  1 )
    CALL mpsumdp( sumw,  1 )
    CALL mpsumdp( sump , 1 )
    CALL mpsumdp( sumt , 1 )
    CALL mpsumdp( sumqv, 1 )
    CALL mpsumdp( sumqs, 1 )

    IF (myproc == 0) THEN
       print*,'sumu===',sqrt( sumu  )
       print*,'sumv===',sqrt( sumv  )
       print*,'sumw===',sqrt( sumw  )
       print*,'sump===',sqrt( sump  )
       print*,'sumt===',sqrt( sumt  )
       print*,'sumqv==',sqrt( sumqv )
       print*,'sumqs==',sqrt( sumqs )
    END IF

    IF (itera == 1) CALL arpsstop('ITERATION',0)
  END IF

!-----------------------------------------------------------------------
!
! END of Debugging message
!
!-----------------------------------------------------------------------

  CALL trans(numctr,nx,ny,nz,nscalarq,u_ctr,v_ctr,p_grd,t_grd,q_grd,w_ctr, &
             qscalar_grd,grad,npass)

  !DEALLOCATE( u, v, w, wcont )
  !DEALLOCATE( qscalar_grd    )

  RETURN
END SUBROUTINE gradt

!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE ADWCONTRA                  ######
!######                                                      ######
!######                Copyright (c) 1994                    ######
!######  Center for the Analysis and Prediction of Storms    ######
!######  University of Oklahoma.  All rights reserved.       ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE adwcontra(nx,ny,nz,u,v,w,mapfct,j1,j2,j3,aj3z,               &
                     rhostr,rhostru,rhostrv,rhostrw,wcont,ustr,vstr,tem1)
!
!-----------------------------------------------------------------------
!
! PURPOSE:
!
!   Perform the adjoint operations on WCONTRA. WCONTRA
!   calculates wcont, the contravariant vertical velocity (m/s)
!
!-----------------------------------------------------------------------
!
! AUTHOR:   Jidong Gao
! 5/20/96 converted to ARPS4.0.22
!
!-----------------------------------------------------------------------
!
! INPUT:
!
!       nx       Number of grid points in the x-direction (east/west)
!       ny       Number of grid points in the y-direction (north/south)
!       nz       Number of grid points in the vertical
!
!       u        x component of velocity at all time levels (m/s)
!       v        y component of velocity at all time levels (m/s)
!       w        Vertical component of Cartesian velocity
!                at all time levels (m/s)
!
!    mapfct   Map factors at scalar, u and v points
!
!       j1       Coordinate transform Jacobian -d(zp)/dx
!       j2       Coordinate transform Jacobian -d(zp)/dy
!       j3       Coordinate transform Jacobian  d(zp)/dz
!    aj3z     Avgz of the coordinate transformation Jacobian  d(zp)/dz
!
!       rhostr   j3 times base state density rhobar(kg/m**3).
!       rhostru  Average rhostr at u points (kg/m**3).
!       rhostrv  Average rhostr at v points (kg/m**3).
!       rhostrw  Average rhostr at w points (kg/m**3).
!
! OUTPUT:
!
!       wcont    Vertical component of contravariant velocity in
!                computational coordinates (m/s)
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
! Variable Declarations.
!
!-----------------------------------------------------------------------
!
  INTEGER, INTENT(IN) :: nx,ny,nz          ! The number of grid points in 3
                                           ! directions

  REAL, INTENT(INOUT) :: u     (nx,ny,nz)  ! Total u-velocity (m/s)
  REAL, INTENT(INOUT) :: v     (nx,ny,nz)  ! Total v-velocity (m/s)
  REAL, INTENT(INOUT) :: w     (nx,ny,nz)  ! Total w-velocity (m/s)

  REAL,    INTENT(IN) :: mapfct(nx,ny,8)   ! Map factors at scalar, u and v points

  REAL,    INTENT(IN) :: j1    (nx,ny,nz)  ! Coordinate transform Jacobian
                                           ! defined as - d( zp )/d( x ).
  REAL,    INTENT(IN) :: j2    (nx,ny,nz)  ! Coordinate transform Jacobian
                                           ! defined as - d( zp )/d( y ).
  REAL,    INTENT(IN) :: j3    (nx,ny,nz)  ! Coordinate transform Jacobian
                                           ! defined as d( zp )/d( z ).
  REAL,    INTENT(IN) :: aj3z  (nx,ny,nz)  ! Coordinate transformation Jacobian defined
                                           ! as d( zp )/d( z ) AVERAGED IN THE Z-DIR.

  REAL,    INTENT(IN) :: rhostr(nx,ny,nz)  ! j3 times base state density rhobar
                                           ! (kg/m**3).
  REAL,    INTENT(IN) :: rhostru(nx,ny,nz) ! Average rhostr at u points (kg/m**3).
  REAL,    INTENT(IN) :: rhostrv(nx,ny,nz) ! Average rhostr at v points (kg/m**3).
  REAL,    INTENT(IN) :: rhostrw(nx,ny,nz) ! Average rhostr at w points (kg/m**3).

  REAL, INTENT(OUT)   :: wcont (nx,ny,nz)  ! Vertical velocity in computational
                                           ! coordinates (m/s)

  REAL, INTENT(INOUT) :: ustr  (nx,ny,nz)  ! temporary work array
  REAL, INTENT(INOUT) :: vstr  (nx,ny,nz)  ! temporary work array

  REAL, INTENT(INOUT) :: tem1  (nx,ny,nz)  ! temporary work array
!
!-----------------------------------------------------------------------
!
! Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'bndry.inc'
  INCLUDE 'mp.inc'
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
!  INTEGER :: onvf
!  REAL    :: tem2  (nx,ny,nz)     ! temporary work array
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!     Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  CALL advbcwcont(nx,ny,nz,wcont)

  IF( crdtrns == 0 ) THEN  ! No coord. transformation case.

    DO k= 2,nz-1
      DO j= 1,ny-1
        DO i= 1,nx-1
          w(i,j,k)=w(i,j,k) + wcont(i,j,k)
          wcont(i,j,k)=0.0
        END DO
      END DO
    END DO

  ELSEIF( ternopt == 0) THEN

    DO k= 2,nz-1
      DO j= 1,ny-1
        DO i= 1,nx-1
          w(i,j,k)=w(i,j,k) + wcont(i,j,k)/aj3z(i,j,k)
          wcont(i,j,k)=0.0
        END DO
      END DO
    END DO

  ELSE

    ! ustr is on U grid, vstr on V grid below
    DO k= 2,nz-1
      DO j= 1,ny-1
        DO i= 1,nx-1
!         wcont(i,j,k)= (                                               &
!             ((ustr(i  ,j,k)+ustr(i  ,j,k-1))*j1(i  ,j,k)              &
!             +(ustr(i+1,j,k)+ustr(i+1,j,k-1))*j1(i+1,j,k)              &
!             +(vstr(i  ,j,k)+vstr(i  ,j,k-1))*j2(i  ,j,k)              &
!             +(vstr(i,j+1,k)+vstr(i,j+1,k-1))*j2(i,j+1,k))             &
!             * mapfct(i,j,8)                                           &
!             / rhostrw(i,j,k) + w(i,j,k)                               &
!             ) /aj3z(i,j,k)
!
         ustr(i,j,k  )=ustr(i,j,k  ) + wcont(i,j,k)*j1(i,j,k)           &
            *mapfct(i,j,8)/rhostrw(i,j,k)/aj3z(i,j,k)
         ustr(i,j,k-1)=ustr(i,j,k-1) + wcont(i,j,k)*j1(i,j,k)           &
            *mapfct(i,j,8)/rhostrw(i,j,k)/aj3z(i,j,k)
         ustr(i+1,j,k  )=ustr(i+1,j,k  ) + wcont(i,j,k)*j1(i+1,j,k)     &
            *mapfct(i,j,8)/rhostrw(i,j,k)/aj3z(i,j,k)
         ustr(i+1,j,k-1)=ustr(i+1,j,k-1) + wcont(i,j,k)*j1(i+1,j,k)     &
            *mapfct(i,j,8)/rhostrw(i,j,k)/aj3z(i,j,k)
         vstr(i  ,j,k  )=vstr(i  ,j,k  ) + wcont(i,j,k)*j2(i,j  ,k)     &
            *mapfct(i,j,8)/rhostrw(i,j,k)/aj3z(i,j,k)
         vstr(i  ,j,k-1)=vstr(i  ,j,k-1) + wcont(i,j,k)*j2(i,j  ,k)     &
            *mapfct(i,j,8)/rhostrw(i,j,k)/aj3z(i,j,k)
         vstr(i,j+1,k  )=vstr(i,j+1,k  ) + wcont(i,j,k)*j2(i,j+1,k)     &
            *mapfct(i,j,8)/rhostrw(i,j,k)/aj3z(i,j,k)
         vstr(i,j+1,k-1)=vstr(i,j+1,k-1) + wcont(i,j,k)*j2(i,j+1,k)     &
            *mapfct(i,j,8)/rhostrw(i,j,k)/aj3z(i,j,k)
         w(i,j,k)=w(i,j,k) + wcont(i,j,k)/aj3z(i,j,k)
         wcont(i,j,k) = 0.0
        END DO
      END DO
    END DO

! No. 2
!
!    IF (mp_opt > 0) THEN
!      !CALL acct_interrupt(mp_acct)
!      CALL mpsendrecv2dew(ustr, nx, ny, nz, ebc, wbc, 1, tem1)
!      CALL mpsendrecv2dns(ustr, nx, ny, nz, nbc, sbc, 1, tem1)
!
!      CALL mpsendrecv2dew(vstr, nx, ny, nz, ebc, wbc, 2, tem1)
!      CALL mpsendrecv2dns(vstr, nx, ny, nz, nbc, sbc, 2, tem1)
!      !CALL acct_stop_inter
!    END IF

    DO k= 1,nz-1
      DO j= 1,ny
        DO i= 1,nx-1
!         vstr(i,j,k)=v(i,j,k)*rhostrv(i,j,k)

          v(i,j,k)=v(i,j,k)+vstr(i,j,k)*rhostrv(i,j,k)
          vstr(i,j,k) = 0.0
        END DO
      END DO
    END DO

    DO k= 1,nz-1
      DO j= 1,ny-1
        DO i= 1,nx
!         ustr(i,j,k)=u(i,j,k)*rhostru(i,j,k)

          u(i,j,k)=u(i,j,k)+ustr(i,j,k)*rhostru(i,j,k)
          ustr(i,j,k)=0.0
        END DO
      END DO
    END DO

  END IF

  RETURN
END SUBROUTINE adwcontra
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                  SUBROUTINE ADVBCWCONT               ######
!######                                                      ######
!######                Copyright (c) 1992-1994               ######
!######  Center for the Analysis and Prediction of Storms    ######
!######  University of Oklahoma.  All rights reserved.       ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE advbcwcont(nx,ny,nz,wcont)
!
!-----------------------------------------------------------------------
!
! PURPOSE:
!
!     Perform the adjoint of
!     Set the top and bottom boundary conditions for wcont.
!
!-----------------------------------------------------------------------
!
! AUTHOR:
!     5/22/96 Jidong Gao
!
!     MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
! INPUT :
!
!       nx       Number of grid points in the x-direction (east/west)
!       ny       Number of grid points in the y-direction (north/south)
!       nz       Number of grid points in the vertical
!
!       wcont    Contravariant vertical velocity (m/s)
!
! OUTPUT:
!
!       wcont    Top and bottom values of wcont.
!
!-----------------------------------------------------------------------
!
! Variable Declarations:
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE                 ! Force explicit declarations

  INTEGER, INTENT(IN) ::  nx,ny,nz  ! Number of grid points in x, y and z
                                    ! directions

  REAL, INTENT(INOUT) :: wcont (nx,ny,nz)   ! Contravariant vertical velocity (m/s)
!
!-----------------------------------------------------------------------
!
! Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k
!
!-----------------------------------------------------------------------
!
!     Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'bndry.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!-----------------------------------------------------------------------
!
! Set the top boundary condition
!
!-----------------------------------------------------------------------
!
  IF(tbc == 1) THEN             ! Rigid lid boundary condition

    DO j=1,ny-1
      DO i=1,nx-1
!         wcont(i,j,nz)=-wcont(i,j,nz-2)
!         wcont(i,j,nz-1)=0.0

          wcont(i,j,nz-2)=wcont(i,j,nz-2)-wcont(i,j,nz)
          wcont(i,j,nz)=0.0
          wcont(i,j,nz-1)=0.0
      END DO
    END DO

  ELSE IF(tbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny-1
      DO i=1,nx-1
!         wcont(i,j,nz)=wcont(i,j,3)

          wcont(i,j,3)=wcont(i,j,3)+wcont(i,j,nz)
          wcont(i,j,nz)=0.0
      END DO
    END DO

  ELSE IF(tbc == 3.OR.tbc == 4) THEN ! Zero normal gradient condition.

    DO j=1,ny-1
      DO i=1,nx-1
!         wcont(i,j,nz)=wcont(i,j,nz-1)

          wcont(i,j,nz-1)=wcont(i,j,nz-1)+wcont(i,j,nz)
          wcont(i,j,nz) = 0.0
      END DO
    END DO

  ELSE

    WRITE(6,900) 'VBCWCONT', tbc
    CALL arpsstop ("",1)
!    stop

  END IF
!
!-----------------------------------------------------------------------
!
! Set the bottom boundary condition
!
!-----------------------------------------------------------------------
!
  IF(bbc == 1) THEN             ! Non-penetrative ground condition

    DO j=1,ny-1
      DO i=1,nx-1
!         wcont(i,j,1)=-wcont(i,j,3)
!         wcont(i,j,2)=0.0

          wcont(i,j,3)=wcont(i,j,3)-wcont(i,j,1)
          wcont(i,j,1)=0.0
          wcont(i,j,2)=0.0
      END DO
    END DO

  ELSE IF(bbc == 2) THEN         ! Periodic boundary condition.

    DO j=1,ny-1
      DO i=1,nx-1
!         wcont(i,j,1)=wcont(i,j,nz-2)

          wcont(i,j,nz-2)=wcont(i,j,nz-2)+wcont(i,j,1)
          wcont(i,j,1) = 0.0
      END DO
    END DO

  ELSE IF(bbc == 3) THEN         ! Zero normal gradient condition.

    DO j=1,ny-1
      DO i=1,nx-1
!         wcont(i,j,1)=wcont(i,j,2)

          wcont(i,j,2)=wcont(i,j,2)+wcont(i,j,1)
          wcont(i,j,1)= 0.0
      END DO
    END DO

  ELSE

    write(6,900) 'VBCWCONT', bbc
    CALL arpsstop('Wrong bbc in VBCWCONT.',1)
!    STOP
  END IF

  900   format(1x,'Invalid boundary condition option found in ',a,      &
              /1x,'The option was ',i3,' Job stopped.')

  RETURN
END SUBROUTINE advbcwcont

