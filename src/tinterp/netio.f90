!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE handle_ncd_error           ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE nf_handle_err(ierr,sub_name)

!-----------------------------------------------------------------------
!
! PURPOSE:
!   Write error message to the standard output if ierr is not zero
!   and abort the program
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  2015/10/08
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!

  IMPLICIT NONE
  INTEGER,          INTENT(IN) :: ierr
  CHARACTER(LEN=*), INTENT(IN) :: sub_name

!-----------------------------------------------------------------------

  CHARACTER(LEN=80) :: errmsg

  INCLUDE 'netcdf.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF(ierr /= NF_NOERR) THEN
    errmsg = NF_STRERROR(ierr)
    WRITE(6,*) 'NetCDF error: ',errmsg
    WRITE(6,*) 'Program stopped while calling ', sub_name
    CALL pstop('Stop triggered in handle_ncd_error',ierr)
  END IF

  RETURN
END SUBROUTINE nf_handle_err

!###################### Open a NCD file ###############################

SUBROUTINE open_data_file ( filename, openmode, filemode, ncid, istatus )
!
!-----------------------------------------------------------------------
!
! PURPOSE:
!   Open a NCD file for 'r','w' or 'm'
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  2015/10/08
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------

  IMPLICIT NONE

  CHARACTER(LEN=*), INTENT(IN)  :: filename
  CHARACTER(LEN=1), INTENT(IN)  :: openmode
  INTEGER,          INTENT(IN)  :: filemode
  INTEGER,          INTENT(OUT) :: ncid, istatus

!------------------------------------------------------------------

  LOGICAL :: fexists
  INTEGER :: file_mode

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  IF (openmode == 'r') THEN
    INQUIRE(FILE = filename, EXIST = fexists)
    IF (fexists) THEN
      file_mode = NF_NOWRITE
    ELSE
      istatus = -1
      WRITE(6,'(2a)') 'ERROR: File not found: ', filename
      RETURN
    ENDIF

    istatus = NF_OPEN(TRIM(filename),file_mode,ncid)
    CALL nf_handle_err(istatus,'NF_OPEN('//TRIM(filename)//' in open_data_file.')

  ELSE IF (openmode == 'w') THEN
    SELECT CASE (filemode)
    case (0)
      file_mode = NF_CLOBBER
    case (1)
      file_mode = IOR(NF_CLOBBER,NF_64BIT_OFFSET)
    case (2)
      file_mode = IOR(NF_CLOBBER,NF_NETCDF4)
    case (3)
      file_mode = IOR(NF_CLASSIC_MODEL,NF_NETCDF4)
    CASE DEFAULT
      istatus = -3
      WRITE(6,'(1x,a,I0)') 'ERROR: Unsupported file mode : ', filemode
      RETURN
    END SELECT


    istatus = NF_CREATE(TRIM(filename),file_mode,ncid)
    CALL nf_handle_err(istatus,'NF_CREATE('//TRIM(filename)//' in open_data_file.')

  ELSE IF (openmode == 'm') THEN
    INQUIRE(FILE = filename, EXIST = fexists)
    IF (fexists) THEN
      file_mode = NF_WRITE
    ELSE
      istatus = -1
      WRITE(6,'(2a)') 'ERROR: File not found: ', filename
      RETURN
    ENDIF

    istatus = NF_OPEN(TRIM(filename),file_mode,ncid)
    CALL nf_handle_err(istatus,'NF_OPEN('//TRIM(filename)//' in open_data_file.')

  ELSE
    istatus = -2
    WRITE(6,'(2a)') 'ERROR: Unsupported file opening mode : ', openmode
  END IF

  IF (istatus < 0) CALL pstop('Stop triggered in open_data_file',istatus)

  RETURN
END SUBROUTINE open_data_file

!###################### Close an opened NCD file ######################

SUBROUTINE close_data_file ( ncid, istatus )
!
!-----------------------------------------------------------------------
!
! PURPOSE:
!   Close an opened NCD file.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  2015/10/08
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: ncid
  INTEGER, INTENT(OUT) :: istatus

!------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  istatus = NF_CLOSE(ncid)
  CALL nf_handle_err(istatus,'NF_CLOSE in close_data_file')

  RETURN
END SUBROUTINE close_data_file

!################### Get dimension from a NCD file #####################

SUBROUTINE get_dims_from_data ( filename, hisfmt,nx,ny,nz,nzsoil,       &
                                nxlg,nylg, istatus )
!
!-----------------------------------------------------------------------
!
! PURPOSE:
!   Get dimension from a NCD file. The NCD file is still not opened and
!   it should be close after the job is done.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  2015/10/08
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
  IMPLICIT NONE

  CHARACTER(LEN=256), INTENT(IN)  :: filename
  INTEGER,            INTENT(IN)  :: hisfmt
  INTEGER,            INTENT(OUT) :: nx, ny, nz, nzsoil
  INTEGER,            INTENT(OUT) :: nxlg, nylg
  INTEGER,            INTENT(OUT) :: istatus

!------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER :: ncid, dimid

  CHARACTER(LEN=20), PARAMETER :: dimnames(4,0:1) = reshape(            &
    (/ 'west_east_stag      ','south_north_stag    ','bottom_top_stag     ','soil_layers_stag    ',         &
       'west_east_stag      ','south_north_stag    ','num_metgrid_levels  ','num_st_layers       ' /),      &
                                                 shape(dimnames))
  CHARACTER(LEN=20), PARAMETER :: dimaltns(4,0:1) = reshape(            &
    (/ 'None                ','None                ','None                ','None                ',         &
       'None                ','None                ','None                ','num_soilt_levels    ' /),      &
                                                 shape(dimaltns))

  INTEGER :: i, dims(4)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  CALL open_data_file( filename, 'r', 0, ncid, istatus)

  DO i = 1, 4
    !WRITE(*,'(1x,a,i0,2a,i0)') 'Retrieving dimension: ',i,TRIM(dimnames(i,hisfmt)),' for ',hisfmt
    istatus = nf_inq_dimid(ncid,dimnames(i,hisfmt),dimid)
    IF (istatus /= NF_NOERR) THEN
      IF (dimaltns(i,hisfmt) /= 'None                ') THEN
        istatus = nf_inq_dimid(ncid,dimaltns(i,hisfmt),dimid)
        IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,            &
               'get_dims_from_data:nf_inq_dimid('//TRIM(dimaltns(i,hisfmt))//')')
      ELSE
          CALL nf_handle_err(istatus,                &
               'get_dims_from_data:nf_inq_dimid('//TRIM(dimnames(i,hisfmt))//')')
      END IF
    END IF


    istatus = nf_inq_dimlen(ncid,dimid,dims(i))
    IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,                &
       'get_dims_from_data:nf_inq_dimlen('//TRIM(dimnames(i,hisfmt))//')')
  END DO

  nx = dims(1)
  ny = dims(2)
  nz = dims(3)
  nzsoil = dims(4)

  istatus = NF_GET_ATT_INT(ncid,NF_GLOBAL,'WEST-EAST_GRID_DIMENSION',  nxlg)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'get_dims_from_data:NF_GET_ATT_INT(WEST-EAST_GRID_DIMENSION)')

  istatus = NF_GET_ATT_INT(ncid,NF_GLOBAL,'SOUTH-NORTH_GRID_DIMENSION',nylg)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'get_dims_from_data:NF_GET_ATT_INT(SOUTH-NORTH_GRID_DIMENSION)')

  CALL close_data_file( ncid, istatus )

  RETURN
END SUBROUTINE get_dims_from_data

!################### Get times from a NCD file #####################

SUBROUTINE get_time_from_data ( ncid, iyr,imo,iday,ihr,imin,isec,istatus )
!
!-----------------------------------------------------------------------
!
! PURPOSE:
!   Get time from a NCD file. The NCD file should be already opened.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  2015/10/08
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------

  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: ncid
  INTEGER, INTENT(OUT) :: iyr,imo,iday,ihr,imin,isec
  INTEGER, INTENT(OUT) :: istatus

!------------------------------------------------------------------
!
!  Misc. local variables
!
!------------------------------------------------------------------

  CHARACTER(LEN=19) :: timestr
  INTEGER :: dimid, varid
  INTEGER :: itime

  CHARACTER(LEN=1) :: ach

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  itime = 1
  istatus = NF_INQ_VARID(ncid, 'Times', varid)
  CALL nf_handle_err(istatus,'NF_INQ_VARID in get_time_from_data')

  istatus = NF_GET_VARA_TEXT(ncid,varid,(/1,itime/),(/19,1/),timestr)
  CALL nf_handle_err(istatus,'NF_GET_VARA_TEXT in get_time_from_data')

  READ(timestr,'(I4.4,a1,I2.2,a1,I2.2,a1,I2.2,a1,I2.2,a1,I2.2)')        &
                 iyr,ach,imo,ach,iday,ach,ihr,ach,imin,ach,isec

  RETURN
END SUBROUTINE get_time_from_data

!################### Write times to a NCD file #####################

SUBROUTINE set_time_to_data ( ncid, timestr, istatus )
!
!-----------------------------------------------------------------------
!
! PURPOSE:
!   Get time from a NCD file. The NCD file should be already opened.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  2015/10/08
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------

  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: ncid
  CHARACTER(LEN=19), INTENT(IN) :: timestr
  INTEGER, INTENT(OUT) :: istatus

!------------------------------------------------------------------
!
!  Misc. local variables
!
!------------------------------------------------------------------

  INTEGER :: dimid, varid

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0
  !WRITE(timestr,'(I4.4,a1,I2.2,a1,I2.2,a1,I2.2,a1,I2.2,a1,I2.2)')       &
  !               iyr,'-',imo,'-',iday,'_',ihr,':',imin,':',isec

  istatus = nf_inq_varid(ncid,'Times',varid)

  istatus = NF_PUT_VARA_TEXT(ncid,varid,(/1,1/),(/19,1/),timestr)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'set_time_to_data:NF_PUT_VARA_TEXT')

  RETURN
END SUBROUTINE set_time_to_data

!################# # Read a variable from an opened NCD file ########

SUBROUTINE get_var_from_data( fin, varname, varout,nsizes, ndims,       &
                              stagger,istatus )
!
!-----------------------------------------------------------------------
!
! PURPOSE:
!   Get data from a NCD file. The NCD file should be already opened.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  2015/10/08
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------

  IMPLICIT NONE

  INTEGER,          INTENT(IN)    :: fin
  CHARACTER(LEN=*), INTENT(IN)    :: varname
  INTEGER,          INTENT(OUT)   :: ndims
  INTEGER,          INTENT(INOUT) :: nsizes(4)
                                  ! nsizes(4) - IN  Maximum array size
                                  !           - OUT Actuall array size
                                  ! nsizes(1:3) - Array sizes with each dimension
  REAL,             INTENT(OUT)   :: varout(nsizes(4))
  CHARACTER(LEN=1), INTENT(OUT)   :: stagger

  INTEGER,          INTENT(OUT)   :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER :: varid
  INTEGER :: vartype,varnatts
  INTEGER :: vardimids(NF_MAX_DIMS)

  INTEGER :: vdim, dimlen, unlimdimid, nxid, nyid
  CHARACTER(LEN=32) :: dimname

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  !
  ! get variable id
  !
  istatus = nf_inq_varid(fin,varname,varid)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'get_var_from_data:nf_inq_varid:'//TRIM(varname))

  !
  ! Get more variable information
  !
  istatus = nf_inq_var(fin,varid,varname,vartype,ndims,vardimids,varnatts)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'get_var_from_data:nf_inq_var')

  !
  ! Get dimensions
  !
  istatus = nf_inq_unlimdim(fin,unlimdimid)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'get_var_from_data:nf_inq_unlimdim')

  istatus = nf_inq_dimid(fin,'west_east_stag',nxid)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'get_var_from_data:nf_inq_dimid')

  istatus = nf_inq_dimid(fin,'south_north_stag',nyid)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'get_var_from_data:nf_inq_dimid')

  stagger = ' '

  IF (ndims >= 1 .AND. ndims <= 4) THEN
    nsizes(:) = 1
    DO vdim = 1, ndims
      istatus = nf_inq_dim(fin,vardimids(vdim),dimname,dimlen)
      IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'get_var_from_data:nf_inq_dim')

      IF (vardimids(vdim) /= unlimdimid ) THEN
        nsizes(vdim) = dimlen
        nsizes(4) = nsizes(4)*dimlen

        IF (vardimids(vdim) == nxid) THEN
          stagger = 'X'
        ELSE IF (vardimids(vdim) == nyid) THEN
          stagger = 'Y'
        END IF
      END IF

    END DO
    ndims = ndims - 1    ! Do not count the unlimited dimension

    istatus = nf_get_var_real(fin,varid,varout)
    IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'get_var_from_data:nf_get_var_real - '//TRIM(varname))
  ELSE
    WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported variable rank size ',ndims,'.'
    istatus = -1
  END IF

  RETURN
END SUBROUTINE get_var_from_data

!################# # Read a variable from an opened NCD file ########

SUBROUTINE write_var_to_data( fout, vname, varin, nsize, istatus )
!
!-----------------------------------------------------------------------
!
! PURPOSE:
!   Write data to a NCD file. The NCD file should be already opened.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  2015/10/08
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------

  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: fout
  CHARACTER(LEN=*), INTENT(IN)  :: vname
  INTEGER,          INTENT(IN)  :: nsize

  REAL,             INTENT(IN)  :: varin(nsize)

  INTEGER,          INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER :: varid, vdim
  INTEGER :: vartype, ndims
  INTEGER :: vardimids(NF_MAX_DIMS)
  INTEGER :: varnatts, nsizes, dimlen

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  !
  ! get variable id
  !
  istatus = nf_inq_varid(fout,vname,varid)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'write_var_to_data:nf_inq_varid:'//vname)

  !
  ! Get more variable information
  !
  istatus = nf_inq_var(fout,varid,TRIM(vname),vartype,ndims,vardimids,varnatts)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'write_var_to_data:nf_inq_var:'//vname)

  nsizes = 1
  DO vdim = 1, ndims
    istatus = nf_inq_dimlen(fout,vardimids(vdim),dimlen)
    CALL nf_handle_err(istatus,'write_var_to_data:nf_inq_dim')
    nsizes = nsizes*dimlen
  END DO

  IF (nsizes /= nsize) THEN
    WRITE(*,'(1x,a,/,8x,2(a,I0))')                                      &
         'ERROR: Array size does not match for '//TRIM(vname),          &
         'Passed in ',nsize,', expected ',nsizes
    istatus = -1
    CALL nf_handle_err(istatus,'write_var_to_data:wrong array size')
  END IF

  istatus = nf_put_var_real(fout,varid,varin)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'write_var_to_data:nf_put_var_real')

  RETURN
END SUBROUTINE write_var_to_data

!######### Copy file definition from one file to another file #########

SUBROUTINE copy_data_definitions ( fin, fout,compress_level,            &
                  fixdim,nproc_x_out, nproc_y_out, iloc,jloc,           &
                  nvarmax,nvar,var_names,filetitle, datestr, istatus  )

  IMPLICIT NONE

  INTEGER,           INTENT(IN)  :: fin, fout,compress_level
  LOGICAL,           INTENT(IN)  :: fixdim
  INTEGER,           INTENT(IN)  :: nproc_x_out, nproc_y_out
  INTEGER,           INTENT(IN)  :: iloc, jloc
  INTEGER,           INTENT(IN)  :: nvarmax
  INTEGER,           INTENT(INOUT)  :: nvar
  CHARACTER(LEN=40), INTENT(INOUT)  :: var_names(nvarmax)
  CHARACTER(LEN=* ), INTENT(IN)  :: filetitle
  CHARACTER(LEN=19), INTENT(IN)  :: datestr
  INTEGER,           INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  CHARACTER(LEN=32) :: dimname, attname
  CHARACTER(LEN=40) :: varname

  INTEGER :: ndims, dimid, dimlen, odimid, unlimdimid

  INTEGER :: ngatts, attnum

  INTEGER :: varid, nvars, ovarid
  INTEGER :: vartype, varndims, varnatts, vardimids(NF_MAX_DIMS)

  INTEGER :: lenstr

  INTEGER :: nxd, nyd, nxf, nyf, nxs,nys
  INTEGER :: ips,ipe,jps,jpe
  INTEGER :: iss,ise,jss,jse

  LOGICAL :: gathervariables  ! Gather time-dependent variables
              ! 1. nvar == nvarmax and nvar > 0, define variables in var_names only
              ! 2. nvar /= nvarmax and nvar > 0, do nothing special
              ! 3. nvar /= nvarmax and nvar == 0, collect all time-dependent variables to var_names

  LOGICAL :: name_in_set

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  IF (fixdim) THEN
    istatus = NF_GET_ATT_INT(fin,NF_GLOBAL,'WEST-EAST_GRID_DIMENSION',  nxd)
    IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:NF_GET_ATT_INT')

    istatus = NF_GET_ATT_INT(fin,NF_GLOBAL,'SOUTH-NORTH_GRID_DIMENSION',nyd)
    IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:NF_GET_ATT_INT')

    IF (MOD(nxd-1,nproc_x_out) /= 0 .OR. MOD(nyd-1,nproc_y_out) /= 0) THEN
      istatus = -1
      WRITE(*,'(1x,a,/,2(8x,2(a,I8),/))')                               &
          'ERROR: Dimension size is not divisible by patch size.',      &
          'nxd         = ',nxd,         ', nyd         = ',nyd,         &
          'nproc_x_out = ', nproc_x_out,', nproc_y_out = ',nproc_y_out
      CALL nf_handle_err(istatus,'copy_data_definitions:wrong dimension size')
    END IF

    nxs = (nxd-1)/nproc_x_out;  nxf = nxs
    nys = (nyd-1)/nproc_y_out;  nyf = nys

    IF (iloc == nproc_x_out) nxf = nxf+1
    IF (jloc == nproc_y_out) nyf = nyf+1

    iss = (iloc-1)*nxs+1    ; ips = iss
    ise = iss+nxs-1         ; ipe = ips+nxf-1
    jss = (jloc-1)*nys+1    ; jps = jss
    jse = jss+nys-1         ; jpe = jps+nyf-1

  END IF

!-----------------------------------------------------------------------
!
! Copy dimensions definitions
!
!-----------------------------------------------------------------------

  istatus = nf_inq_unlimdim(fin,unlimdimid)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:nf_inq_unlimdim')

  istatus = nf_inq_ndims(fin,ndims)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:nf_inq_ndims')

  DO dimid = 1, ndims

    istatus = nf_inq_dim(fin,dimid,dimname,dimlen)
    IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:nf_inq_dim')

    IF (dimid == unlimdimid) dimlen = NF_UNLIMITED

    IF (fixdim) THEN
      SELECT CASE (TRIM(dimname))
      CASE ('west_east')
        dimlen = nxs
      CASE ('west_east_stag')
        dimlen = nxf
      CASE ('south_north')
        dimlen = nys
      CASE ('south_north_stag')
        dimlen = nyf
      END SELECT
    END IF

    istatus = nf_def_dim(fout,dimname,dimlen,odimid)
    IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:nf_def_dim')

  END DO

!-----------------------------------------------------------------------
!
! Copy global attributes
!
!-----------------------------------------------------------------------

  istatus = nf_inq_natts(fin,ngatts)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:nf_inq_natts')

  DO attnum = 1, ngatts

    istatus = nf_inq_attname(fin,NF_GLOBAL,attnum,attname)
    IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:nf_inq_attname')

    istatus = nf_copy_att(fin,NF_GLOBAL,attname,fout,NF_GLOBAL)
    IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:nf_copy_att')

  END DO

  IF (fixdim) THEN
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'WEST-EAST_PATCH_START_UNSTAG', NF_INT,1,iss)
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'WEST-EAST_PATCH_END_UNSTAG',   NF_INT,1,ise)
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'WEST-EAST_PATCH_START_STAG',   NF_INT,1,ips)
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'WEST-EAST_PATCH_END_STAG',     NF_INT,1,ipe)
    CALL nf_handle_err(istatus,'copy_data_definitions:NF_PUT_ATT_INT')

    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'SOUTH-NORTH_PATCH_START_UNSTAG', NF_INT,1,jss)
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'SOUTH-NORTH_PATCH_END_UNSTAG',   NF_INT,1,jse)
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'SOUTH-NORTH_PATCH_START_STAG',   NF_INT,1,jps)
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'SOUTH-NORTH_PATCH_END_STAG',     NF_INT,1,jpe)
    CALL nf_handle_err(istatus,'copy_data_definitions:NF_PUT_ATT_INT')
  END IF

  lenstr = LEN_TRIM(filetitle)
  istatus = NF_PUT_ATT_TEXT(fout,NF_GLOBAL,'TITLE',lenstr,TRIM(filetitle))
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:NF_PUT_ATT_TEXT')

  lenstr = LEN_TRIM(DateStr)
  istatus = NF_PUT_ATT_TEXT(fout,NF_GLOBAL,'SIMULATION_START_DATE',lenstr,TRIM(DateStr))
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:NF_PUT_ATT_TEXT')

!-----------------------------------------------------------------------
!
! Copy variable definitions
!
!-----------------------------------------------------------------------

  istatus = nf_inq_nvars(fin,nvars)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:nf_inq_nvars')

  gathervariables  = .FALSE.
  IF (nvar <= 0 .AND. nvarmax > nvars)  THEN
    nvar = 0
    gathervariables  = .TRUE.
  END IF

  !write(*,*) (varid,var_names(varid),varid=1,nvar)
  DO varid = 1, nvars
    istatus = nf_inq_var(fin,varid,varname,vartype,varndims,vardimids,varnatts)
    IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:nf_inq_var')

    IF (nvarmax == nvar .AND. nvar > 0) THEN !
      IF ( .NOT. name_in_set(varname,var_names,nvar) ) CYCLE
    ELSE     ! remember all time-dependent variables
      IF (gathervariables) THEN
        IF ( vardimids(varndims) == unlimdimid .AND. varname /= 'Times') THEN
          nvar = nvar+1
          var_names(nvar) = varname
        END IF
      END IF
    END IF

    istatus = nf_def_var(fout,varname,vartype,varndims,vardimids,ovarid)
    IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:nf_def_var')

    IF (compress_level > 0) THEN   ! Turn on deflate compression
      istatus = NF_DEF_VAR_DEFLATE(fout, ovarid, 0, 1, compress_level)
      IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:NF_DEF_VAR_deflate')
    END IF

    !
    ! Copy variable attributes
    !
    DO attnum = 1,varnatts
      istatus = nf_inq_attname(fin,varid,attnum,attname)
      IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:nf_inq_attname')

      istatus = nf_copy_att(fin,varid,attname,fout,ovarid)
      IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:nf_copy_att')
    END DO

  END DO

!-----------------------------------------------------------------------
!
! End file definition mode
!
!-----------------------------------------------------------------------

  istatus = nf_enddef(fout)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:nf_enddef')

!-----------------------------------------------------------------------
!
! Write time string (it needs an updated value).
!
!-----------------------------------------------------------------------

  istatus = nf_inq_varid(fout,'Times',ovarid)

  istatus = NF_PUT_VARA_TEXT(fout,ovarid,(/1,1/),(/19,1/),DateStr)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_data_definitions:NF_PUT_VARA_TEXT')

  RETURN
END SUBROUTINE copy_data_definitions

!############ Copy variables from one file to another file #############

SUBROUTINE copy_input_vars ( fin, fout, nexcl, excludesets,istatus )
  IMPLICIT NONE

  INTEGER,           INTENT(IN)  :: fin, fout
  INTEGER,           INTENT(IN)  :: nexcl
  CHARACTER(LEN=40), INTENT(IN)  :: excludesets(nexcl)
  INTEGER,           INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  CHARACTER(LEN=40) :: varname

  INTEGER :: varid, nvars, idim
  INTEGER :: vartype, varndims, varnatts, vardimids(NF_MAX_DIMS)

  INTEGER :: narrsize, narrisizemax, narrasizemax
  INTEGER :: startidx(NF_MAX_DIMS), countidx(NF_MAX_DIMS)

  INTEGER, ALLOCATABLE :: varari(:)
  REAL,    ALLOCATABLE :: vararr(:)

!-----------------------------------------------------------------------

  LOGICAL :: name_in_set

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  istatus = nf_inq_nvars(fin,nvars)
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_input_vars:nf_inq_nvars')

  narrisizemax = 0
  narrasizemax = 0

  startidx(:)  = 1

  DO varid = 1, nvars

    countidx(:)  = 1
    narrsize     = 1

    istatus = nf_inq_var(fin,varid,varname,vartype,varndims,vardimids,varnatts)
    IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_input_vars:nf_inq_var')

    IF (name_in_set(varname,excludesets,nexcl)) CYCLE

    !istatus = nf_copy_var(fin,varid,fout)
    !IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_input_vars:nf_copy_var:'//TRIM(varname))

    DO idim = 1, varndims
      istatus = nf_inq_dimlen(fin,vardimids(idim),countidx(idim))
      IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_input_vars:nf_inq_dimlen'//TRIM(varname))

      narrsize = narrsize*countidx(idim)
    END DO

    SELECT CASE (vartype)

    CASE (NF_INT)
      IF (narrsize > narrisizemax) THEN   ! Allocate input array only when necessary
        IF (ALLOCATED(varari)) DEALLOCATE(varari, STAT = istatus)
        ALLOCATE(varari(narrsize), STAT = istatus)
        narrisizemax = narrsize
      END IF

      istatus = NF_GET_VARA_INT(fin,varid,startidx,countidx,varari)
      IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_input_vars:NF_GET_VARA_INT:'//TRIM(varname))

      istatus = NF_PUT_VARA_INT(fout,varid,startidx,countidx,varari)
      IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_input_vars:nf_put_vara_INT:'//TRIM(varname))

    CASE (NF_FLOAT)

      IF (narrsize > narrasizemax) THEN   ! Allocate input array only when necessary
        IF (ALLOCATED(vararr)) DEALLOCATE(vararr, STAT = istatus)
        ALLOCATE(vararr(narrsize), STAT = istatus)
        narrasizemax = narrsize
      END IF

      istatus = NF_GET_VARA_REAL(fin,varid,startidx,countidx,vararr)
      IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_input_vars:NF_GET_VARA_REAL:'//TRIM(varname))

      istatus = NF_PUT_VARA_REAL(fout,varid,startidx,countidx,vararr)
      IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'copy_input_vars:nf_put_vara_REAL:'//TRIM(varname))
    !CASE (NF_CHAR)
    CASE DEFAULT
      WRITE(6,'(1x,a,I2,2a)') 'ERROR: unsupported variable type = ',vartype,' for variable ',TRIM(varname)
      istatus = -4
    END SELECT

  END DO

  IF (ALLOCATED(varari)) DEALLOCATE(varari)
  IF (ALLOCATED(vararr)) DEALLOCATE(vararr)

  RETURN
END SUBROUTINE copy_input_vars

!################### Fix NCD file title and time #######################

SUBROUTINE fix_file_title( fout, filetitle, DateStr, istatus)

  IMPLICIT NONE

  INTEGER,           INTENT(IN)  :: fout
  CHARACTER(LEN=*),  INTENT(IN)  :: filetitle
  CHARACTER(LEN=19), INTENT(IN)  :: DateStr
  INTEGER,           INTENT(OUT) :: istatus

  INCLUDE 'netcdf.inc'

!-----------------------------------------------------------------------

  INTEGER :: lenstr
  CHARACTER(LEN=20) :: attname

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  attname = 'TITLE'
  lenstr = LEN_TRIM(filetitle)
  istatus = NF_PUT_ATT_TEXT(fout,NF_GLOBAL,TRIM(attname),lenstr,TRIM(filetitle))
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'fix_file_title:NF_PUT_ATT_TEXT')

  attname = 'START_DATE'
  lenstr = LEN_TRIM(DateStr)
  istatus = NF_PUT_ATT_TEXT(fout,NF_GLOBAL,TRIM(attname),lenstr,TRIM(DateStr))
  IF (istatus /= NF_NOERR) CALL nf_handle_err(istatus,'fix_file_title:NF_PUT_ATT_TEXT')

  RETURN
END SUBROUTINE fix_file_title

LOGICAL FUNCTION name_in_set(vname,chset,nset)

  IMPLICIT NONE
  CHARACTER(LEN=*), INTENT(IN)  :: vname
  INTEGER,          INTENT(IN)  :: nset
  CHARACTER(LEN=*), INTENT(IN)  :: chset(nset)

!-----------------------------------------------------------------------

  INTEGER :: i

  CHARACTER(LEN=40) :: upcase

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  name_in_set = .FALSE.

  DO i = 1, nset

    IF (TRIM(upcase(vname)) == TRIM(upcase(chset(i))) ) THEN
      name_in_set = .TRUE.
      EXIT
    END IF

  END DO

  RETURN
END FUNCTION
