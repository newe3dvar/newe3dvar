!
!##################################################################
!##################################################################
!######                                                      ######
!######                   PROGRAM TINTERP                    ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!

PROGRAM tinterp
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  This program interpolates two history data on grid of the same
!  size to a time inbetween them. The output will be written into a new
!  history dump file.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!
!  10/07/2015. Written based on ARPSTINTRP.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  USE module_mp

  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Dimension of the base grid (input data).
!
!-----------------------------------------------------------------------
!
  INTEGER :: nx,ny,nz
  INTEGER :: nzsoil

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  !INCLUDE 'mp.inc'
  INTEGER, PARAMETER :: nfilemax = 2
  INTEGER, PARAMETER :: nvarmax = 100
  INTEGER, PARAMETER :: nouttimemax=100
  CHARACTER(LEN=80),  PARAMETER :: ftitle    = 'OUTPUT FROM TINTERP 1.0 '
!
!-----------------------------------------------------------------------
!
!  Data arrays. The last dimension is for the two sets of variables.
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: datain(:,:)

!
!-----------------------------------------------------------------------
!
!  namelist Declarations:
!
!-----------------------------------------------------------------------
!
  NAMELIST /message_passing/ nproc_x, nproc_y,                          &
                             nproc_x_in,  nproc_y_in,                   &
                             nproc_x_out, nproc_y_out

  INTEGER :: hinfmt, hisfmt
  CHARACTER (LEN=256) :: hisfile(nfilemax)

  NAMELIST /INPUT/hinfmt,hisfile,hisfmt

  INTEGER :: nvarlist
  CHARACTER(LEN=40) :: varlist(nvarmax)

  NAMELIST /intrp_opt/ nvarlist, varlist

  INTEGER :: use_data_t
  CHARACTER (LEN=19) :: initime  ! Real time in form of 'year-mo-dy:hr:mn:ss'

  INTEGER :: nouttime
  REAL    :: outtime(nouttimemax)
  CHARACTER(LEN=256) :: outdir, outfname(nouttimemax)
  INTEGER :: lvldbg, outfmt, compression_level

  NAMELIST /output/ outdir, outfmt, outfname,                           &
                    use_data_t,initime, nouttime,outtime, lvldbg

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k, l, n, nd
  REAL    :: amin, amax
  INTEGER :: nsize, nsizes(4), nvardim

  CHARACTER(LEN=1) :: stagger

  CHARACTER (LEN=256) :: filename
  CHARACTER (LEN=256) :: nlfile
  INTEGER :: lenstr, endstr

  INTEGER :: nchin(nfilemax), nchout(nouttimemax)

  INTEGER :: unum         ! unit number for reading in namelist
  INTEGER :: istatus
  LOGICAL :: iexist

  INTEGER :: nhisfile, nout, ncount

  INTEGER :: times(nfilemax)
  REAL    :: curtime
  CHARACTER (LEN=19) :: timestr

  REAL    :: alpha, beta
  INTEGER :: iabstinit,iabstinit1
  INTEGER :: ioffset
  INTEGER :: year,month,day,hour,minute,second
  INTEGER :: year1,month1,day1,hour1,minute1,second1
  INTEGER :: ioutabst,ioutabstinit

  INTEGER :: nxlg, nylg

  INTEGER :: nexcld
  CHARACTER(LEN=40) :: excldlist(nvarmax)
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  CALL mpinit_proc(istatus)

  IF(myproc == 0) THEN
    WRITE(6,'(/9(/2x,a)/)')                                             &
     '###############################################################', &
     '###############################################################', &
     '###                                                         ###', &
     '###                Welcome to TINTERP                       ###', &
     '###                                                         ###', &
     '###############################################################', &
     '###############################################################'

    unum = COMMAND_ARGUMENT_COUNT()
    IF (unum > 0) THEN
      CALL GET_COMMAND_ARGUMENT(1, nlfile, lenstr, istatus )
      IF ( nlfile(1:1) == ' ' .OR. istatus /= 0 ) THEN  ! Use standard input to be backward-compatible
        unum = 5
      ELSE
        INQUIRE(FILE=TRIM(nlfile),EXIST=iexist)
        IF (.NOT. iexist) THEN
          WRITE(6,'(1x,3a)') 'WARNING: namelist file - ',               &
                TRIM(nlfile),' does not exist. Falling back to standard input.'
          unum = 5
        END IF
      END IF
    ELSE
      unum = 5
    END IF

    IF (unum /= 5) THEN
      unum = 38
      OPEN(unum,FILE=TRIM(nlfile),STATUS='OLD',FORM='FORMATTED')
      WRITE(*,'(1x,3a,/,1x,a,/)') 'Reading TINTERP namelist from file - ', &
              TRIM(nlfile),' ... ','========================================'
    ELSE
      WRITE(*,'(2(1x,a,/))') 'Waiting namelist from standard input ... ', &
                             '========================================'
    END IF

  END IF

!
!-----------------------------------------------------------------------
!
!  Read namelist message_passing
!
!-----------------------------------------------------------------------
!
  nproc_x_in  = 1
  nproc_y_in  = 1

  nproc_x_out = 1
  nproc_y_out = 1

  IF(myproc == 0) THEN
    READ(unum,message_passing, END=100)
    WRITE(6,'(1x,a)')'Namelist message_passing was successfully read.'
  END IF
  CALL mpupdatei(nproc_x,1)
  CALL mpupdatei(nproc_y,1)
  CALL mpupdatei(nproc_x_in, 1)
  CALL mpupdatei(nproc_y_in, 1)
  CALL mpupdatei(nproc_x_out,1)
  CALL mpupdatei(nproc_y_out,1)

  CALL mpinit_var

  readsplit = 0
  IF (mp_opt > 0) THEN
    IF (nproc_x_in == 1 .AND. nproc_y_in ==1) THEN
      readsplit = 1
    ELSE
      IF (nproc_x_in /= nproc_x) nproc_x_in = nproc_x      ! still did not support join or split reading
      IF (nproc_y_in /= nproc_y) nproc_y_in = nproc_y
    END IF
  END IF

!
!-----------------------------------------------------------------------
!
!  Read namelist INPUT
!
!-----------------------------------------------------------------------
!

  hinfmt    = 2
  hisfmt    = 0
  nhisfile  = 2
  hisfile(1) = 'X'
  hisfile(2) = 'Y'

  IF(myproc == 0) THEN
    READ(unum,INPUT, END=100)
    WRITE(6,'(1x,a)') ' Input control parameters read in are:'

    WRITE(6,'(1x,a,i3)') ' hinfmt      =', hinfmt
    WRITE(6,'(1x,a,i3)') ' hisfmt      =', hisfmt

    DO i=1,nhisfile
      WRITE(6,'(3x,a,i0,3a)') ' hisfile(',i,') = "',TRIM(hisfile(i)),'"'
    END DO
  END IF
  CALL mpupdatei(hinfmt,1)
  CALL mpupdatei(hisfmt,1)
  CALL mpupdatec(hisfile, 256, nhisfile)

  IF (mp_opt > 0) THEN

    DO i = 1, nhisfile
      WRITE(filename,'(2a,I4.4)') TRIM(hisfile(i)),'_',myproc
      hisfile(i) = filename
      !write(*,*) filename
    END DO

  END IF

!
!-----------------------------------------------------------------------
!
!  Read namelist INTRP_OPT
!
!-----------------------------------------------------------------------
!

  nvarlist    = 0
  varlist     = ' '

  IF(myproc == 0) THEN
    READ(unum,intrp_opt, END=100)
    WRITE(6,'(1x,a)') ' Intrp control parameters read in are:'

    WRITE(6,'(1x,a,i3)') ' nvarlist  = ', nvarlist
    DO i=1,nvarlist
      WRITE(6,'(3x,a,i0,3a)') ' varlist(',i,')="',TRIM(varlist(i)),'"'
    END DO
  END IF
  CALL mpupdatei(nvarlist,1)
  CALL mpupdatec(varlist, 40, nvarmax)
!
!-----------------------------------------------------------------------
!
!  Set the control parameters for output:
!
!-----------------------------------------------------------------------
!
  use_data_t = 1
  initime ='0000-00-00:00:00:00'

  nouttime = 1
  outtime(1) = 0.0

  outdir     = './'
  outfmt     = 1
  outfname   = ' '

  lvldbg = 0

  IF (myproc == 0) THEN
    WRITE(6,'(/a/)')                                                    &
        ' Reading in control parameters for the output data files..'

    READ(unum,output,END=100)

    lenstr = LEN_TRIM(outdir)

    IF( lenstr == 0 ) THEN
      outdir = './'
      lenstr = 2
    END IF

    IF (outdir(lenstr:lenstr) /= '/') THEN
      filename = outdir
      WRITE(outdir,'(2a)') TRIM(filename),'/'
    END IF

    CALL inquiredir(TRIM(outdir),iexist)

    IF( .NOT. iexist ) THEN
      !CALL unixcmd( 'mkdir -p '//dirname(1:ldirnam) )
      CALL makedir(outdir,istatus)
      WRITE(6,'(5x,a,2(/5x,a))') 'Required output directory <'          &
        //TRIM(outdir)//'> not found.', 'It was created by the program.'
    END IF

  END IF
  CALL mpupdatec(outdir,256)
  CALL mpupdatei(outfmt,1)
  CALL mpupdatec(outfname,256,nouttimemax)
  CALL mpupdatei(use_data_t,1)
  CALL mpupdatec(initime,19)

  CALL mpupdatei(nouttime,1)
  CALL mpupdater(outtime,nouttimemax)

  CALL mpupdatei(lvldbg,1)

  compression_level = 0
  IF (outfmt > 100) THEN
    compression_level = outfmt/100
    IF (outfmt < 202 ) THEN
      WRITE(*,'(1x,a,I0,a)') 'ERROR: Only netCDF4 format support compression, please check "outfmt", it is ',outfmt,' currently.'
      CALL pstop("Unsupported output file mode",1)
    END IF
    outfmt = MOD(outfmt,100)
  END IF

  IF (unum /= 5 .AND. myproc == 0) THEN
    CLOSE( unum )
  END IF
!
!
!-----------------------------------------------------------------------
!
!  Get dataset dimensions
!
!-----------------------------------------------------------------------
!

  !IF (myproc == 0) THEN
  CALL get_dims_from_data(hisfile(1),hisfmt,nx,ny,nz,nzsoil,nxlg,nylg,istatus)
  !END IF
  !CALL mpupdatei(nx,1)
  !CALL mpupdatei(ny,1)
  !CALL mpupdatei(nz,1)
  !CALL mpupdatei(nzsoil,1)
  !CALL mpupdatei(nxlg,1)
  !CALL mpupdatei(nylg,1)

  IF (nouttime == 1) THEN
    nout   = 1
    ncount = 2
  ELSE
    nout   = 3
    ncount = 3
  END IF

!-----------------------------------------------------------------------
!
!  Allocate the arrays.
!
!-----------------------------------------------------------------------
!
  nsize = nx*ny*nz
  ALLOCATE(datain(nsize,ncount))
  datain(:,:) = 0.0

!
!-----------------------------------------------------------------------
!
!  Get times in each input file
!
!-----------------------------------------------------------------------
!
  istatus = 0

  DO nd=1,2

    filename = hisfile(nd)

    IF (myproc == 0) WRITE(6,'(/a,a,a)')                                &
        ' Data set ', TRIM(filename) ,' to be processed.'

    CALL open_data_file(hisfile(nd),'r',0,nchin(nd),istatus)

    CALL get_time_from_data(nchin(nd),year,month,day,hour,minute,second,istatus)

    CALL ctim2abss(year,month,day,hour,minute,second,iabstinit)

    IF (myproc == 0) WRITE(6,'(/1x,a,I4,2(a,i2.2),3(a,i2.2),a,I13,a/)') &
        'History data read in date: ',year,'-',month,'-', day,          &
        ', time: ', hour,':',minute,':',second,', abs seconds = ',iabstinit,' (s)'

    IF(nd == 1) THEN  ! Save the values for data set 1
      year1  = year
      month1 = month
      day1   = day
      hour1  = hour
      minute1= minute
      second1= second
      iabstinit1 = iabstinit
    END IF

    times(nd) = iabstinit

  END DO

  IF( istatus /= 0 ) GO TO 9002            ! Read was unsuccessful

  IF( use_data_t == 1) THEN ! Use init time in input file
    ioutabstinit=iabstinit1
  ELSE
    READ(initime,'(i4.4,1x,i2.2,1x,i2.2,1x,i2.2,1x,i2.2,1x,i2.2)')      &
                   year1,month1,day1,hour1,minute1,second1
    CALL ctim2abss(year1,month1,day1,hour1,minute1,second1,ioutabstinit)
  END IF

  !ioffset = ioutabstinit - iabstinit1
  times(1) = times(1) - ioutabstinit
  times(2) = times(2) - ioutabstinit

!-----------------------------------------------------------------------
!
!  Create output file and open it for output
!
!-----------------------------------------------------------------------
!

  DO l=1,nouttime
    curtime = outtime(l)

    IF (myproc == 0) WRITE(6,'(/,1X,a,i4,2(a,i2.2),3(a,i2.2),a,i0,a,F8.0)')   &
        'The reference time is ',                                       &
        year1,'-',month1,'-',day1,' ',hour1,':',minute1,':',second1,    &
        ', the ',l,'th output time relative is ',curtime

    IF ( curtime > MAX(times(1),times(2)) .OR.                          &
         curtime < MIN(times(1),times(2))) THEN
      WRITE (*,*) "WARNING: Performing extrapolation.  Desired time ",  &
                  "is outside the range of the reference files."
    END IF

    IF( times(2) == times(1)) THEN
      WRITE (*,*) "ERROR: times in reference files are the same, ",     &
                  "can't perform interpolation."
      CALL pstop('No interpolation',1)
    END IF

    WRITE(*,'(1x,2(a,I10),a,F10.0)') 'times(1) = ', times(1),', times(2) = ', times(2),', curtime = ',curtime

    alpha = (times(2)-curtime)/(times(2)-times(1))
    beta = 1.0-alpha

    IF (myproc == 0) WRITE (6,'(1x,2(a,F12.7))')                        &
         'Relative weights: file 1 - ',alpha,', file 2 - ',beta

    iabstinit = ioutabstinit + INT(curtime)
    CALL abss2ctim(iabstinit,year,month,day,hour,minute,second)
    WRITE(timestr,'(I4.4,a1,I2.2,a1,I2.2,a1,I2.2,a1,I2.2,a1,I2.2)')     &
                  year,'-',month,'-',day,'_',hour,':',minute,':',second

    CALL get_out_filename(outdir,outfname(l),hisfile(1),timestr,        &
                  filename,istatus)

    WRITE(*,'(/,1x,3a,/)') 'Creating output file ',TRIM(filename),' ......'

    CALL open_data_file(filename,'w',outfmt,nchout(l),istatus)

    CALL copy_data_definitions ( nchin(1), nchout(l),compression_level, &
                  .FALSE.,1, 1, 1,1,                                    &
                  nvarmax,nvarlist,varlist,ftitle, timestr, istatus  )

    nexcld = nvarlist+1
    DO n = 1,nvarlist
      WRITE(excldlist(i),'(a)') TRIM(varlist(i))
    END DO
    excldlist(nexcld) = 'Times'

    CALL copy_input_vars ( nchin(1), nchout(l), nexcld, excldlist, istatus )

    !CALL set_time_to_data(nchout(l), timestr, istatus)

!-----------------------------------------------------------------------
!
!  Calculate the interpolated fields be looping over each variables
!
!-----------------------------------------------------------------------
!
    DO n = 1, nvarlist

      WRITE(*,'(3x,a,I0,3a)') 'Processing variable (',n,'): ',TRIM(varlist(n)),' ......'

      nsizes(4) = nsize
      datain(:,:) = 0.0

      DO nd = 1, nhisfile
        CALL get_var_from_data(nchin(nd),varlist(n),datain(:,nd),       &
                               nsizes, nvardim, stagger,istatus)
      END DO

      DO k=1,nsizes(4)
        datain(k,nout)=alpha*datain(k,1)+beta*datain(k,2)
      END DO

      CALL write_var_to_data( nchout(l),varlist(n),datain(:,nout),      &
                              nsizes(4), istatus)
!
!-----------------------------------------------------------------------
!
!  Print out the max/min of output variables.
!
!-----------------------------------------------------------------------
!
      IF (lvldbg > 1) THEN
        amax = MAXVAL(datain(:,1))
        amin = MINVAL(datain(:,1))
        IF (myproc == 0) WRITE(6,'(5x,2(a,e13.6))')                     &
          'Min. and max. at 1st time: amin = ', amin,',  amax = ',amax

        amax = MAXVAL(datain(:,2))
        amin = MINVAL(datain(:,2))
        IF (myproc == 0) WRITE(6,'(5x,2(a,e13.6))')                     &
          'Min. and max. at 2nd time: amin = ', amin,',  amax = ',amax

        amax = MAXVAL(datain(:,nout))
        amin = MINVAL(datain(:,nout))
        IF (myproc == 0) WRITE(6,'(5x,2(a,e13.6))')                     &
          'Min. and max. at new time: amin = ', amin,',  amax = ',amax
      END IF

    END DO

    CALL close_data_file(nchout(l),istatus)

    CALL mpbarrier
    !
    ! Write a Ready file
    !
    lenstr = INDEX(filename,  'WRFOUT_d',.TRUE.)
    IF (lenstr < 1) THEN
      lenstr = INDEX(filename,'met_em.d',.TRUE.)
    END IF
    endstr = INDEX(filename,'_',.TRUE.)
    k = LEN_TRIM(filename)
    IF (endstr /= k-4) endstr = k+1

    IF (lenstr > 0 .AND. myproc == 0) THEN
      WRITE(nlfile,'(3a)') filename(1:lenstr+5),'Ready',TRIM(filename(lenstr+6:endstr-1))

      unum = 103
      OPEN(UNIT=unum, FILE=nlfile, IOSTAT=istatus, STATUS="NEW", ACTION="WRITE")
      !IF ( istatus /= 0 ) STOP "Error opening file name"

      WRITE(unum,'(1x,a)') TRIM(filename)

      CLOSE(UNIT=unum, IOSTAT=istatus)
      !IF ( istatus /= 0 ) STOP "Error closing file unit unum"
    END IF

  END DO       ! l=1,nouttime

!
!-----------------------------------------------------------------------
!
!  Close file and stop the program
!
!-----------------------------------------------------------------------
!

  DO nd = 1, nhisfile
    CALL close_data_file(nchin(nd), istatus)
  END DO

  CALL mpbarrier

  IF (mp_opt > 0) THEN
    IF (myproc == 0) WRITE(6,'(/,a)') ' ==== Normal successful completion of TINTERP_MPI. ===='
  ELSE
    WRITE(6,'(/,a)') ' ==== Normal successful completion of TINTERP. ===='
  END IF
  CALL pstop(' ',0)

  100   WRITE(6,'(a)')                                                  &
          'Error reading NAMELIST file. Program TINTERP stopped.'
  CALL pstop(' ',9104)


  9002  CONTINUE
  WRITE(6,'(1x,a,i2,/1x,a)')                                            &
      'Data read was unsuccessful. ireturn =', istatus,                 &
      'Job stopped in TINTERP.'

  CALL pstop(' ',9002)

END PROGRAM tinterp

SUBROUTINE get_out_filename(outdir,outfname,infile,timestr,filename,istatus)
  IMPLICIT NONE

  CHARACTER(LEN=256), INTENT(IN)  :: outdir, infile, outfname
  CHARACTER(LEN=19),  INTENT(IN)  :: timestr
  CHARACTER(LEN=256), INTENT(OUT) :: filename

  INTEGER,            INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INTEGER :: indx1, indx2, lenstr

  CHARACTER(LEN=256) :: tmpstr, tailstr
  CHARACTER(LEN=7)   :: substr

  LOGICAL :: iexist

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  lenstr = LEN_TRIM(outfname)

  IF (lenstr < 1) THEN
    indx1 = INDEX(infile,'/',.TRUE.)
    IF (indx1 < 1) indx1 = 0

    substr = timestr(1:7)
    indx2 = INDEX(infile,substr)

    IF(indx2 <= indx1) THEN
      WRITE(tmpstr,'(2a)') '_from_tinterp_',timestr
    ELSE
      WRITE(tmpstr,'(2a)') infile(indx1+1:indx2-1),timestr
    END IF

    indx2 = INDEX(infile,':',.TRUE.)
    IF (indx2 > indx1) THEN
      WRITE(tailstr,'(a)') TRIM(infile(indx2+3:))
    ELSE
      tailstr = ' '
    END IF

    WRITE(filename,'(3a)') TRIM(outdir),TRIM(tmpstr),TRIM(tailstr)
  ELSE
    indx1 = LEN_TRIM(outdir)
    IF (outdir(indx1:indx1) /= '/') THEN
      WRITE(tmpstr,'(2a)') TRIM(outdir),'/'
    ELSE
      WRITE(tmpstr,'(a)') TRIM(outdir)
    END IF

    WRITE(filename,'(2a)') TRIM(tmpstr),TRIM(outfname)

  END IF

  RETURN
END SUBROUTINE get_out_filename
