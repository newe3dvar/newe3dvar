!
!##################################################################
!##################################################################
!######                                                      ######
!######                   PROGRAM GETRADAR                   ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
PROGRAM  get_radar
!
!-----------------------------------------------------------------------
!
! PURPOSE: get radar within a given model domain
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!
!  01/13/2016. Written based on plt_radar.f90.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!

  IMPLICIT NONE

  INTEGER, PARAMETER :: nnwgrdmax = 20
!
!-----------------------------------------------------------------------
!
!  namelist Declarations:
!
!-----------------------------------------------------------------------
!

  INTEGER :: nnwgrd
  INTEGER :: nx(nnwgrdmax),ny(nnwgrdmax)
  INTEGER :: mapproj(nnwgrdmax)
  REAL    :: dx(nnwgrdmax),dy(nnwgrdmax),ctrlat(nnwgrdmax),ctrlon(nnwgrdmax)
	REAL    :: trulat1(nnwgrdmax),trulat2(nnwgrdmax),trulon(nnwgrdmax)

  NAMELIST /model_grid/ nnwgrd,nx,ny,dx,dy,ctrlat,ctrlon,               &
	                      mapproj,trulat1,trulat2,trulon

  REAL    :: extrng_x(nnwgrdmax), extrng_y(nnwgrdmax)
  NAMELIST /grid_options/ extrng_x, extrng_y

	CHARACTER(LEN=256) :: radarinfo
  NAMELIST /rad_info/ radarinfo

	LOGICAL :: radar_order
	INTEGER :: dbg_lvl
	NAMELIST /output/ radar_order, dbg_lvl

!
!-----------------------------------------------------------------------
!
!  Radar information
!
!-----------------------------------------------------------------------
!

  INTEGER, PARAMETER :: maxrtab=300
  CHARACTER (LEN=12) :: radtab(maxrtab)
  REAL :: rtablat(maxrtab)
  REAL :: rtablon(maxrtab)

  INTEGER, PARAMETER :: maxrad=100
  REAL :: radlat(maxrad), tmp_lat
  REAL :: radlon(maxrad), tmp_lon
	REAL :: rad_dist(maxrad), tmp_dist

  INTEGER :: nrad, nradtab
  CHARACTER (LEN=12) :: radname(maxrad), tmp_name

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  CHARACTER(LEN=256) :: nlfile, dummy

	INTEGER :: istatus, unum, lenstr
	INTEGER :: n, i, j

  REAL    :: rad_grd_lat1, rad_grd_lat2, rad_grd_lon1, rad_grd_lon2

  INTEGER :: ilat,ilatmin,ilatsec,ilon,ilonmin,ilonsec

  REAL    :: xorig,yorig, xl, yl, ctrx, ctry, swx, swy
  LOGICAL :: fexist
  REAL    :: truelat(2)
  REAL    :: rlat,rlon
	CHARACTER(LEN=20) :: afmt, bfmt
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  istatus = 0

  WRITE(6,'(/9(/2x,a)/)')                                             &
   '###############################################################', &
   '###############################################################', &
   '###                                                         ###', &
   '###                Welcome to GETRADAR                      ###', &
   '###                                                         ###', &
   '###############################################################', &
   '###############################################################'

  unum = COMMAND_ARGUMENT_COUNT()
  IF (unum > 0) THEN
    CALL GET_COMMAND_ARGUMENT(1, nlfile, lenstr, istatus )
    IF ( nlfile(1:1) == ' ' .OR. istatus /= 0 ) THEN  ! Use standard input to be backward-compatible
      unum = 5
    ELSE
      INQUIRE(FILE=TRIM(nlfile),EXIST=fexist)
      IF (.NOT. fexist) THEN
        WRITE(6,'(1x,3a)') 'WARNING: namelist file - ',               &
              TRIM(nlfile),' does not exist. Falling back to standard input.'
        unum = 5
      END IF
    END IF
  ELSE
    unum = 5
  END IF

  IF (unum /= 5) THEN
    unum = 38
    OPEN(unum,FILE=TRIM(nlfile),STATUS='OLD',FORM='FORMATTED')
    WRITE(*,'(1x,3a,/,1x,a,/)') 'Reading GETRADAR namelist from file - ', &
            TRIM(nlfile),' ... ','========================================'
  ELSE
    WRITE(*,'(2(1x,a,/))') 'Waiting namelist from standard input ... ', &
                           '========================================'
  END IF

!
!-----------------------------------------------------------------------
!
!  Input control parameters for model grids
!
!-----------------------------------------------------------------------
!

  READ(unum,model_grid,ERR=100)

  DO n = 1, nnwgrd
    IF ( mapproj(n) == 0 ) THEN
      trulat1(n) = ctrlat(n)
      trulat2(n) = ctrlat(n)
      trulon(n)  = ctrlon(n)
    END IF
	END DO
!
!-----------------------------------------------------------------------
!
!  Set the output grid and the variable control parameters
!
!-----------------------------------------------------------------------
!
  extrng_x = 0.0
	extrng_y = 0.0
  READ(unum,grid_options, ERR=100)

  READ(unum,rad_info, ERR=100)

  INQUIRE(FILE=TRIM(radarinfo),EXIST=fexist)
  IF (.NOT. fexist) THEN
    WRITE(6,'(1x,3a)') 'WARNING: radar information file - ',            &
          TRIM(radarinfo),' does not exist. Program aborting.'
  END IF

  READ(unum,output, ERR=100)

  GO TO 10
  100   WRITE(6,'(a)') 'Error reading NAMELIST file. Program stopped.'
  STOP

  10    CONTINUE

  IF (unum /= 5) THEN
    CLOSE( unum )
  END IF
!
!-----------------------------------------------------------------------
!
!  Read in radar table
!
!-----------------------------------------------------------------------
!
  OPEN(31,file=TRIM(radarinfo),STATUS='old',IOSTAT=istatus)
  IF(istatus /= 0) THEN
    WRITE(6,'(1x,a)') ' Did not find radarinfo.dat.'
		STOP
  END IF

  !
  ! Read station table data
  !
  READ(31,'(a1)') dummy
  DO i=1,maxrtab
    READ(31,'(a4,20x,i4,2i8,i10,i7,i8)',iostat=istatus) radtab(i), &
         ilat,ilatmin,ilatsec,ilon,ilonmin,ilonsec
    IF(istatus /= 0) EXIT
    rtablat(i)=float(ilat)+(float(ilatmin)/60.)+(float(ilatsec)/3600.)
    rtablon(i)=float(ilon)+(float(ilonmin)/60.)+(float(ilonsec)/3600.)
    rtablon(i)=-1*rtablon(i)
  END DO
  nradtab=i-1
  WRITE(6,'(a,i6,a)') ' Read in ',nradtab,' NEXRAD radars from table'
!
!-----------------------------------------------------------------------
!
!  Loop over each output grid
!
!-----------------------------------------------------------------------
!
  DO n = 1, nnwgrd
    !
    !
    !  Set up map projection.
    !
    xl = (nx(n)-1)*dx(n)
    yl = (ny(n)-1)*dy(n)

    truelat(1) = trulat1(n)
    truelat(2) = trulat2(n)

    CALL setmapr(mapproj(n),1.0,truelat,trulon(n))

    CALL lltoxy( 1,1, ctrlat(n),ctrlon(n), ctrx, ctry )

    swx = ctrx - xl*0.5
    swy = ctry - yl*0.5

    CALL setorig( 1, swx, swy)

    xorig = 0.0
    yorig = 0.0

    !CALL xstpjgrd(mapproj(n),trulat1(n),trulat2(n),trulon(n),           &
    !              ctrlat(n),ctrlon(n),xl,yl,xorig,yorig)
    !
    CALL xytoll(1,1,xorig-extrng_x,yorig-extrng_y,rad_grd_lat1,rad_grd_lon1)

    WRITE(6,'(/1x,a,2f12.6)')                                           &
        'Lat/lon at the SW corner of base grid= ',rad_grd_lat1,rad_grd_lon1

    CALL xytoll(1,1,(xorig+xl+extrng_x),(yorig+yl+extrng_y),rad_grd_lat2,rad_grd_lon2)
    WRITE(6,'(1x,a,2f12.6/)')                                           &
        'Lat/lon at the NE corner of base grid= ',rad_grd_lat2,rad_grd_lon2

    !
    ! Match radar name and table name
    !
    nrad=0
    DO i=1,nradtab
      IF((rtablat(i)>rad_grd_lat1).AND.(rtablat(i)<rad_grd_lat2).AND.   &
         (rtablon(i)>rad_grd_lon1).AND.(rtablon(i)<rad_grd_lon2)) THEN

          nrad=nrad+1
          radlat(nrad)=rtablat(i)
          radlon(nrad)=rtablon(i)
          radname(nrad)=radtab(i)

      END IF
    END DO

   !
   ! find the distance between center and radars
	 !

    IF (radar_order) THEN

			rlat = (rad_grd_lat1+rad_grd_lat2)/2
			rlon = (rad_grd_lon1+rad_grd_lon2)/2
      DO i=1, nrad
        rad_dist(i) = (radlat(i)-rlat)**2+(radlon(i)-rlon)**2
      END DO

      DO i=1, nrad
        DO j=1, nrad-i
          IF(rad_dist(j)> rad_dist(j+1)) THEN
            tmp_dist = rad_dist(j)
            tmp_lat  = radlat(j)
            tmp_lon  = radlon(j)
            tmp_name = radname(j)

            rad_dist(j) = rad_dist(j+1)
            radlat(j)   = radlat(j+1)
            radlon(j)   = radlon(j+1)
            radname(j)  = radname(j+1)

            rad_dist(j+1) = tmp_dist
            radlat(j+1)   = tmp_lat
            radlon(j+1)   = tmp_lon
            radname(j+1)  = tmp_name
          END IF
        END DO
      END DO

    END IF

    !
    ! Output rad list
    !
    IF (nrad > 0) THEN
		  WRITE(afmt,'(a,I0,a)') '(1x,I2,a,',nrad,'a)'
		  WRITE(bfmt,'(a,I0,a)') '(1x,a,',nrad,'(F10.4,a),a)'

      WRITE(*,FMT=afmt) n, ': ', (radname(i),i=1,nrad)
      WRITE(*,FMT=bfmt) 'lats = (/', (radlat(i),',',i=1,nrad), ' /)'
      WRITE(*,FMT=bfmt) 'lons = (/', (radlon(i),',',i=1,nrad), ' /)'
    ELSE
      WRITE(*,*) 'No radar was found within domain.'
    END IF

	END DO
!
!-----------------------------------------------------------------------
!
!  End of the program
!
!-----------------------------------------------------------------------
!

  STOP
END PROGRAM  get_radar

