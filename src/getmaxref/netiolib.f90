
!
!##################################################################
!##################################################################
!######                                                      ######
!######                  SUBROUTINE getref1d                 ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE getref1d(ncid, pixel, refval, pixel_x, pixel_y,               &
                   pixel_count ,istatus)

!-----------------------------------------------------------------------
!
! PURPOSE:
!   Read in reflectivity data from netCDF file.
!
!-----------------------------------------------------------------------

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: ncid         ! NetCDF file hander
  INTEGER, INTENT(IN)  :: pixel
  REAL,    INTENT(OUT) :: refval     (pixel)
  INTEGER, INTENT(OUT) :: pixel_x    (pixel)
  INTEGER, INTENT(OUT) :: pixel_y    (pixel)
  INTEGER, INTENT(OUT) :: pixel_count(pixel)
  INTEGER, INTENT(OUT) :: istatus

  INTEGER  :: varid

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

!-----------------------------------------------------------------------
!
!  read data from netcdf file
!
!-----------------------------------------------------------------------

  istatus = NF_INQ_VARID(ncid,'MergedReflectivityQCComposite',varid)
  CALL nf_handle_error(istatus,'NF_INQ_VARID in getrefd')
  istatus = NF_GET_VAR_REAL(ncid,varid,refval)
  CALL nf_handle_error(istatus,'NF_GET_VAR_REAL in getrefd')

  istatus = NF_INQ_VARID(ncid,'pixel_x',varid)
  CALL nf_handle_error(istatus,'NF_INQ_VARID in getrefd')
  istatus = NF_GET_VAR_INT(ncid,varid,pixel_x)
  CALL nf_handle_error(istatus,'NF_GET_VAR_REAL in getrefd')

  istatus = NF_INQ_VARID(ncid,'pixel_y',varid)
  CALL nf_handle_error(istatus,'NF_INQ_VARID in getrefd')
  istatus = NF_GET_VAR_INT(ncid,varid,pixel_y)
  CALL nf_handle_error(istatus,'NF_GET_VAR_REAL in getrefd')

  istatus = NF_INQ_VARID(ncid,'pixel_count',varid)
  CALL nf_handle_error(istatus,'NF_INQ_VARID in getrefd')
  istatus = NF_GET_VAR_INT(ncid,varid,pixel_count)
  CALL nf_handle_error(istatus,'NF_GET_VAR_REAL in getrefd')

  RETURN
END SUBROUTINE getref1d
!
!##################################################################
!##################################################################
!######                                                      ######
!######                  SUBROUTINE getref2d                 ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE getref2d(ncid, lat,lon, dim2_ref,dim2_lat,dim2_lon,istatus)
!-----------------------------------------------------------------------
!
! PURPOSE:
!   Read in reflectivity data from netCDF file.
!
!-----------------------------------------------------------------------

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: ncid         ! NetCDF file hander
  INTEGER, INTENT(IN)  :: lat,lon
  REAL,    INTENT(OUT) :: dim2_ref(lon,lat)
  REAL,    INTENT(OUT) :: dim2_lat(lon,lat)
  REAL,    INTENT(OUT) :: dim2_lon(lon,lat)
  INTEGER, INTENT(OUT) :: istatus

  INTEGER  :: varid

  INCLUDE 'netcdf.inc'

	INTEGER :: i, j

	REAL, ALLOCATABLE :: latin(:), lonin(:)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  ALLOCATE(latin(lat), STAT = istatus)
  ALLOCATE(lonin(lon), STAT = istatus)

!-----------------------------------------------------------------------
!
!  read data from netcdf file
!
!-----------------------------------------------------------------------

  istatus = NF_INQ_VARID(ncid,'MergedReflectivityQCComposite',varid)
  CALL nf_handle_error(istatus,'NF_INQ_VARID in getrefd')
  istatus = NF_GET_VAR_REAL(ncid,varid,dim2_ref)
  CALL nf_handle_error(istatus,'NF_GET_VAR_REAL in getrefd')

  istatus = NF_INQ_VARID(ncid,'Lat',varid)
  CALL nf_handle_error(istatus,'NF_INQ_VARID in getrefd')
  istatus = NF_GET_VAR_REAL(ncid,varid,latin)
  CALL nf_handle_error(istatus,'NF_GET_VAR_REAL in getrefd')

  istatus = NF_INQ_VARID(ncid,'Lon',varid)
  CALL nf_handle_error(istatus,'NF_INQ_VARID in getrefd')
  istatus = NF_GET_VAR_REAL(ncid,varid,lonin)
  CALL nf_handle_error(istatus,'NF_GET_VAR_REAL in getrefd')

  DO j = 1, lat
		DO i = 1, lon
			dim2_lon(i,j) = lonin(i)
			dim2_lat(i,j) = latin(j)
		END DO
	END DO

  DEALLOCATE(latin, lonin)

  RETURN
END SUBROUTINE getref2d
!
!##################################################################
!##################################################################
!######                                                      ######
!######                  SUBROUTINE open_ref_file            ######
!######                                                      ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE open_ref_file(filename,ncidout)
!
!------------------------------------------------------------------
!
!  PURPOSE:
!
!    Open a netCDF file and return file handler.
!
!    NOTE: it is required to call close_ref_file explicitly to close
!          the opened file in your calling program.
!
!------------------------------------------------------------------

  IMPLICIT NONE
  CHARACTER(LEN=*), INTENT(IN)  :: filename
  INTEGER,          INTENT(OUT) :: ncidout

!------------------------------------------------------------------
!
!  Misc. local variable
!
!------------------------------------------------------------------
  INTEGER :: istatus
  LOGICAL :: fexists

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  INQUIRE(FILE = filename, EXIST = fexists)
  IF (fexists) THEN
    istatus = NF_OPEN(TRIM(filename),NF_NOWRITE,ncidout)
    CALL nf_handle_error(istatus,'NF_OPEN in open_ref_file')
  ELSE
    WRITE(6,'(2a)') 'File not found: ', filename
    STOP 'open_ref_file'
  ENDIF

  RETURN

END SUBROUTINE open_ref_file
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE close_ref_file               ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE close_ref_file(ncid)
!
!------------------------------------------------------------------
!
!  PURPOSE:
!
!     Close the netCDF file which is opened using open_ref_file.
!
!------------------------------------------------------------------
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: ncid

!------------------------------------------------------------------
!
!  Misc. local variable
!
!------------------------------------------------------------------
!
  INTEGER :: istatus

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

  istatus = NF_CLOSE(ncid)
  CALL nf_handle_error(istatus,'NF_CLOSE in close_ref_file')

  RETURN
END SUBROUTINE close_ref_file
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE get_ref_att                ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_ref_att(ncid,Longitude,Latitude,                        &
                       LonGridSpacing,LatGridSpacing)
!-----------------------------------------------------------------------
!
! PURPOSE
!
!   Retieve  NetCDF file variables which are stored
!   as Global attributes.
!
!-----------------------------------------------------------------------
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: ncid
  REAL,    INTENT(OUT) :: Longitude
  REAL,    INTENT(OUT) :: Latitude
  REAL,    INTENT(OUT) :: LonGridSpacing
  REAL,    INTENT(OUT) :: LatGridSpacing

  INCLUDE 'netcdf.inc'

  INTEGER  :: istatus

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'Longitude',Longitude)
  CALL nf_handle_error(istatus,'NF_GET_ATT_REAL in get_ref_att')
  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'Latitude',Latitude)
  CALL nf_handle_error(istatus,'NF_GET_ATT_REAL in get_ref_att')
  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'LonGridSpacing',LonGridSpacing)
  CALL nf_handle_error(istatus,'NF_GET_ATT_REAL in get_ref_att')
  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'LatGridSpacing',LatGridSpacing)
  CALL nf_handle_error(istatus,'NF_GET_ATT_REAL in get_ref_att')

  RETURN
END SUBROUTINE get_ref_att

!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE get_ref_dimension          ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_ref_dimension(filename,file_dim,lon,lat,pixel)

!------------------------------------------------------------------------
!
! PURPOSE:
!   Read dimension parameters and the time level which match the time
!   string from the parameters.
!
!   Please note this subroutine will open a file to read and then close
!   it. So it does not leave any opened handler for NetCDF file.
!
!------------------------------------------------------------------------
!
  IMPLICIT NONE
  CHARACTER(LEN=*), INTENT(IN)  :: filename
	INTEGER,          INTENT(IN)  :: file_dim
  INTEGER,          INTENT(OUT) :: lon
  INTEGER,          INTENT(OUT) :: lat
  INTEGER,          INTENT(OUT) :: pixel

!------------------------------------------------------------------------
!
!  Misc. Local variables
!
!------------------------------------------------------------------------

  INTEGER :: ncid, istatus
  LOGICAL :: fexists
  INTEGER :: dimid,varid
  INTEGER :: dateStrLen, strlen, ntime

  CHARACTER(LEN=19) :: temstr

  INCLUDE 'netcdf.inc'

  INTEGER :: i

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Begining of executable code
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  INQUIRE(FILE = filename, EXIST = fexists)
  IF (fexists) THEN
    istatus = NF_OPEN(TRIM(filename),NF_NOWRITE,ncid)
    CALL nf_handle_error(istatus,'NF_OPEN in get_ref_dimension')
  ELSE
    WRITE(6,'(2a)') 'File not found: ', filename
    STOP 'open_ref_file'
  ENDIF

!-----------------------------------------------------------------------
!
! Get WRF dimensions
!
!-----------------------------------------------------------------------

  istatus = NF_INQ_DIMID(ncid,'Lon',dimid)
  CALL nf_handle_error(istatus,'NF_INQ_DIMID in get_ref_dimension')
  istatus = NF_INQ_DIMLEN(ncid,dimid,lon)
  CALL nf_handle_error(istatus,'NF_INQ_DIMLEN in get_ref_dimension')

  istatus = NF_INQ_DIMID(ncid,'Lat',dimid)
  CALL nf_handle_error(istatus,'NF_INQ_DIMID in get_ref_dimension')
  istatus = NF_INQ_DIMLEN(ncid,dimid,lat)
  CALL nf_handle_error(istatus,'NF_INQ_DIMLEN in get_ref_dimension')

  IF (file_dim == 1) THEN
    istatus = NF_INQ_DIMID(ncid,'pixel',dimid)
    CALL nf_handle_error(istatus,'NF_INQ_DIMID in get_ref_dimension')
    istatus = NF_INQ_DIMLEN(ncid,dimid,pixel)
    CALL nf_handle_error(istatus,'NF_INQ_DIMLEN in get_ref_dimension')
	ELSE
		pixel = -1
  END IF

  istatus = NF_CLOSE(ncid)
  CALL nf_handle_error(istatus,'NF_CLOSE in get_ref_dimension')

  RETURN
END SUBROUTINE get_ref_dimension
!
!##################################################################
!##################################################################
!######                                                      ######
!######                 SUBROUTINE nf_handle_error           ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE nf_handle_error(ierr,sub_name)

!------------------------------------------------------------------------
!
! PURPOSE:
!   Write error message to the standard output if ierr contains an error
!
!------------------------------------------------------------------------

  IMPLICIT NONE
  INTEGER,          INTENT(IN) :: ierr
  CHARACTER(LEN=*), INTENT(IN) :: sub_name
  CHARACTER(LEN=80) :: errmsg

  INCLUDE 'netcdf.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF(ierr /= NF_NOERR) THEN
    errmsg = NF_STRERROR(ierr)
    WRITE(6,*) 'NetCDF error: ',errmsg
    WRITE(6,*) 'Program stopped while calling ', sub_name
    STOP
  END IF

  RETURN
END SUBROUTINE nf_handle_error

!
!##################################################################
!##################################################################
!######                                                      ######
!######             SUBROUTINE check_alloc_status            ######
!######                                                      ######
!##################################################################
!##################################################################

SUBROUTINE check_alloc_status(istatus, message )

!-----------------------------------------------------------------------
!
! PURPOSE: Check status of array allocation.
!
!-----------------------------------------------------------------------
!
! AUTHOR: Richard Carpenter, 2001/12/07
!
! MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------

  IMPLICIT NONE

  INTEGER,       INTENT(IN) :: istatus
  CHARACTER*(*), INTENT(IN) :: message

  IF (istatus /= 0) THEN
    WRITE(*,'(1x,/,A,I2,3A)') 'ERROR: check_alloc_status, status= ',    &
                              istatus, ', [', TRIM(message), '].'
    !CALL arpsstop('FATAL: Unable to allocate array. Program ends',1)
		STOP
  END IF

  RETURN
END SUBROUTINE check_alloc_status
