PROGRAM getMaxRef
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  read ReflectivityQCComposite from file, then find the lon and lat of
!  max ReflectivityQCComposite.
!
!-----------------------------------------------------------------------
!
!  AUTHOR:
!
!  MODIFICATION HISTORY:
!
!    Y. Wang (01/09/206)
!    Rewrote namelist paramters and integrated into the official package.
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    ref_file path and name of ReflectivityQCComposite file
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: lon, lat, pixel

  REAL    :: Latitude,Longitude
  REAL    :: LatGridSpacing, LonGridSpacing

  REAL,    ALLOCATABLE ::  refval(:)
  INTEGER, ALLOCATABLE ::  pixel_x(:)
  INTEGER, ALLOCATABLE ::  pixel_y(:)
  INTEGER, ALLOCATABLE ::  pixel_count(:)

  ! avg_opt = 0
  REAL,    ALLOCATABLE ::  pixel_avg_ref(:)

  REAL,    ALLOCATABLE ::  pixel_lon(:)
  REAL,    ALLOCATABLE ::  pixel_lat(:)
  INTEGER, ALLOCATABLE ::  pixel_tag(:)

  ! avg_opt = 1
  REAL,    ALLOCATABLE ::  dim2_ref(:,:), dim2_lon(:,:), dim2_lat(:,:)
  REAL,    ALLOCATABLE ::  dim2_avg_ref(:,:)
  INTEGER, ALLOCATABLE ::  dim2_tag(:,:)

	! Results
  REAL,    ALLOCATABLE :: maxrefdots_ref(:)
  REAL,    ALLOCATABLE :: maxrefdots_avg_ref(:)
  REAL,    ALLOCATABLE :: maxrefdots_lon(:)
  REAL,    ALLOCATABLE :: maxrefdots_lat(:)
  INTEGER, ALLOCATABLE :: maxrefdots_pixel(:)
  INTEGER, ALLOCATABLE :: maxrefdots_tag(:)
  INTEGER              :: maxrefdots_realnum

  REAL,    ALLOCATABLE :: result_maxrefdots_ref(:)
  REAL,    ALLOCATABLE :: result_maxrefdots_avg_ref(:)
  REAL,    ALLOCATABLE :: result_maxrefdots_lon(:)
  REAL,    ALLOCATABLE :: result_maxrefdots_lat(:)
  INTEGER, ALLOCATABLE :: result_maxrefdots_pixel(:)
  INTEGER              :: result_maxrefdots_realnum


!-----------------------------------------------------------------------
!
! Namelist variables
!
!-----------------------------------------------------------------------

  CHARACTER(LEN=256) :: ref_file
  INTEGER            :: file_dim

  NAMELIST /ReflectivityQCComposite_file/ref_file, file_dim

  INTEGER :: lonlatconfine
  REAL    :: lonmin,lonmax,latmin,latmax
  REAL    :: minref

  INTEGER :: avg_opt
  INTEGER :: sizeofwindow
  REAL    :: lonspan1,latspan1,lonspan2,latspan2
  NAMELIST /options/ minref,lonlatconfine,lonmin,lonmax,latmin,latmax,  &
	                   avg_opt, sizeofwindow,                             &
	                   lonspan1,latspan1,lonspan2,latspan2

  INTEGER :: maxdotnum, maxdotnum_more_search
  INTEGER :: printinfo

  NAMELIST /output/ maxdotnum,maxdotnum_more_search,printinfo

!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER, ALLOCATABLE :: loopnums(:)
  INTEGER              :: loopcount

  INTEGER :: i,j,ipixel,isearch,iOK,istep,istepcount, itemp
  REAL    :: maxref,sumref, rtemp, avg_ref
	INTEGER :: ii,jj
	INTEGER :: pixelcount, ilon,ilat
	REAL    :: rlat, rlon

  CHARACTER (LEN=256) :: nlfile
  INTEGER :: lenstr
  INTEGER :: istatus
  LOGICAL :: iexist

  INTEGER :: unum
  INTEGER :: ncid

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Beginning of executable code
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  WRITE(6,'(/,9(/,2x,a),/)')                                            &
   '#################################################################', &
   '#################################################################', &
   '####                                                         ####', &
   '####              Welcome to program GetMaxRef               ####', &
   '####                                                         ####', &
   '#################################################################', &
   '#################################################################'

  unum = COMMAND_ARGUMENT_COUNT()
  IF (unum > 0) THEN
    CALL GET_COMMAND_ARGUMENT(1, nlfile, lenstr, istatus )
    IF ( nlfile(1:1) == ' ' .OR. istatus /= 0 ) THEN  ! Use standard input to be backward-compatible
      unum = 5
    ELSE
      INQUIRE(FILE=TRIM(nlfile),EXIST=iexist)
      IF (.NOT. iexist) THEN
        WRITE(6,'(1x,3a)') 'WARNING: namelist file - ',               &
              TRIM(nlfile),' does not exist. Falling back to standard input.'
        unum = 5
      END IF
    END IF
  ELSE
    unum = 5
  END IF

  IF (unum /= 5) THEN
    unum = 38
    OPEN(unum,FILE=TRIM(nlfile),STATUS='OLD',FORM='FORMATTED')
    WRITE(*,'(1x,3a,/,1x,a,/)') 'Reading GETMAXREF namelist from file - ', &
            TRIM(nlfile),' ... ','========================================'
  ELSE
    WRITE(*,'(2(1x,a,/))') 'Waiting namelist from standard input ... ', &
                           '========================================'
  END IF

!-----------------------------------------------------------------------
!
! Get NAMELIST parameters
!
!-----------------------------------------------------------------------

  !
	! ReflectivityQCComposite_file
	!
  file_dim = 1
  READ(unum,NML = ReflectivityQCComposite_file)

  WRITE(*,'(1x,a)') '&ReflectivityQCComposite_file'
  WRITE(*,'(3x,2a)')      'ref_file = ',TRIM(ref_file)
  WRITE(*,'(3x,a,I0)')    'file_dim = ',file_dim
  WRITE(*,'(1x,a,/)') '/'

  !
	! Options
	!
  lonlatconfine = 0
  sizeofwindow = 14
  READ(unum,NML = options)

  IF (file_dim == 2) THEN   ! 2-dim file force to use 2 dimensional average
		avg_opt = 1
	END IF

  WRITE(*,'(1x,a)') '&options'

  WRITE(*,'(3x,a,F10.4)') 'minref   = ',minref
  IF (lonlatconfine > 0) THEN
      WRITE(*,'(3x,a,I0,/,5x,4(a,F12.4))')                              &
                       'lonlatconfine = ',lonlatconfine,                &
                       'lonmin=',lonmin,', lonmax=',lonmax,             &
                     ', latmin=',latmin,', latmax=',latmax
  END IF

  WRITE(*,'(3x,a,I0,/,2(3x,2(A,F10.4),/))')                             &
       'sizeofwindow = ',sizeofwindow,                                  &
       'lonspan1 = ',lonspan1,', latspan1 = ',latspan1,                 &
       'lonspan2 = ',lonspan2,', latspan2 = ',latspan2
  WRITE(*,'(1x,a,/)') '/'


  !
	! OUTPUT
	!
  READ(unum,NML = output)
  WRITE(*,'(1x,a)') '&output'
  WRITE(*,'(3x,a,I0,/,3x,a,I0,/,3x,a,I0)')                              &
       'maxdotnum = ',maxdotnum,                                        &
       'maxdotnum_more_search = ',maxdotnum_more_search,                &
       'printinfo = ',printinfo
  WRITE(*,'(1x,a,/)') '/'


  IF (unum /= 5) THEN
    CLOSE( unum )
  END IF

!-----------------------------------------------------------------------
!
! Get dimensions
!
!-----------------------------------------------------------------------
!
  CALL get_ref_dimension(TRIM(ref_file),file_dim,lon,lat,pixel)
  IF (printinfo == 1) THEN
    WRITE(6,'(A,3(/A14,I14))') 'ref grid dimensions: ',               &
                          'lon =',lon, ' lat=',lat,' pixel=',pixel
  ENDIF

!-----------------------------------------------------------------------
!
!  Allocate adas arrays
!
!-----------------------------------------------------------------------


  IF (file_dim == 1) THEN
    ALLOCATE(refval(pixel),      STAT=istatus)
    !CALL check_alloc_status(istatus, "refval")
    ALLOCATE(pixel_x(pixel),     STAT=istatus)
    ALLOCATE(pixel_y(pixel),     STAT=istatus)
    ALLOCATE(pixel_count(pixel), STAT=istatus)
  END IF

  IF(avg_opt > 0) THEN
	  ALLOCATE(dim2_ref(lon,lat),     STAT=istatus)
	  ALLOCATE(dim2_lon(lon,lat),     STAT=istatus)
	  ALLOCATE(dim2_lat(lon,lat),     STAT=istatus)
	  ALLOCATE(dim2_tag(lon,lat),     STAT=istatus)
	  ALLOCATE(dim2_avg_ref(lon,lat), STAT=istatus)
	ELSE
    ALLOCATE(pixel_lon(pixel),      STAT=istatus)
    ALLOCATE(pixel_lat(pixel),      STAT=istatus)
    ALLOCATE(pixel_tag(pixel),      STAT=istatus)
    ALLOCATE(pixel_avg_ref(pixel),  STAT=istatus)
	END IF

  ALLOCATE(maxrefdots_ref(maxdotnum+maxdotnum_more_search),     STAT=istatus)
  ALLOCATE(maxrefdots_avg_ref(maxdotnum+maxdotnum_more_search), STAT=istatus)
  ALLOCATE(maxrefdots_lon(maxdotnum+maxdotnum_more_search),     STAT=istatus)
  ALLOCATE(maxrefdots_lat(maxdotnum+maxdotnum_more_search),     STAT=istatus)
  ALLOCATE(maxrefdots_pixel(maxdotnum+maxdotnum_more_search),   STAT=istatus)
  ALLOCATE(maxrefdots_tag(maxdotnum+maxdotnum_more_search),     STAT=istatus)

  ALLOCATE(result_maxrefdots_ref(maxdotnum),     STAT=istatus)
  ALLOCATE(result_maxrefdots_avg_ref(maxdotnum), STAT=istatus)
  ALLOCATE(result_maxrefdots_lon(maxdotnum),     STAT=istatus)
  ALLOCATE(result_maxrefdots_lat(maxdotnum),     STAT=istatus)
  ALLOCATE(result_maxrefdots_pixel(maxdotnum),   STAT=istatus)

!
!-----------------------------------------------------------------------
!
!  Call getrefd to reads data
!
!-----------------------------------------------------------------------
!
  CALL open_ref_file(TRIM(ref_file),ncid)

  CALL get_ref_att(ncid,Longitude,Latitude,LonGridSpacing,LatGridSpacing)

  IF (printinfo == 1) THEN
    WRITE(6,'(A14,2(/A14,F14.4))') 'ref attribute: ',                   &
      'Longitude =',Longitude, 'Latitude=',Latitude,                    &
      'LonGridSpacing=',LonGridSpacing,'LatGridSpacing=',LatGridSpacing
  ENDIF

	IF (file_dim == 1) THEN
    CALL getref1d(ncid, pixel, refval, pixel_x, pixel_y, pixel_count ,istatus)
	ELSE
    CALL getref2d(ncid, lat,lon, dim2_ref, dim2_lat, dim2_lon,istatus)
  END IF

  CALL close_ref_file(ncid)

  IF (avg_opt > 0) THEN
!-----------------------------------------------------------------------
!
! Get 2 dimension data
!
!-----------------------------------------------------------------------

    IF (file_dim == 1) THEN
      DO ilat = 1, lat
        DO ilon = 1, lon
          dim2_ref(ilat,ilon)     = 0
          dim2_avg_ref(ilat,ilon) = 0
          dim2_tag(ilat,ilon)     = 0
		  		dim2_lon(ilat,ilon)     = 0
		  		dim2_lat(ilat,ilon)     = 0
        END DO
      END DO

      DO i=1, pixel

        IF ((pixel_count(i) > 50) .OR. (refval(i) < minref)) CYCLE

		  	pixelcount = 0
        DO j = 0, pixel_count(i)-1
		  		pixelcount = pixel_x(i)*lon+pixel_y(i)+j
		  		ilat = pixelcount/lon + 1
		  		ilon = MOD(pixelcount,lon)+1

          IF (lonlatconfine /= 0) THEN
  	  			rlon = Longitude+(ilon-1)*LonGridSpacing
	    			rlat = Latitude-(ilat-1)*LatGridSpacing
            IF (rlon < lonmin  .OR. rlon > lonmax .OR.                    &
                rlat < latmin  .OR. rlat > latmax ) THEN
              CYCLE
            END IF
          END IF

          dim2_ref(ilat,ilon) = refval(i)
		  		dim2_lon(ilat,ilon) = rlon
		  		dim2_lat(ilat,ilon) = rlat
          dim2_tag(ilat,ilon) = 1
		  		!PRINT *, i, pixel_x(i),pixel_y(i),pixel_count(i),	j,ilat,ilon
        END DO

      END DO

		ELSE

      DO ilat = 1, lat
        DO ilon = 1, lon
          dim2_avg_ref(ilon,ilat) = 0
          dim2_tag(ilon,ilat)     = 1
				END DO
			END DO

      DO ilat = 1, lat
        DO ilon = 1, lon
          IF (dim2_ref(ilon,ilat) < minref) dim2_tag(ilon,ilat) = 0
				END DO
			END DO

      IF (lonlatconfine /= 0) THEN
		    DO ilat = 1, lat
          DO ilon = 1, lon
  	  			rlon = dim2_lon(ilon,ilat)
	    			rlat = dim2_lat(ilon,ilat)
            IF (rlon < lonmin  .OR. rlon > lonmax .OR.                  &
                rlat < latmin  .OR. rlat > latmax ) THEN
      					dim2_tag(ilon,ilat) = 0
            END IF
          END DO
        END DO
      END IF

		END IF
!-----------------------------------------------------------------------
!
! Get 2 dimension avage ref
!
!-----------------------------------------------------------------------

    DO ilat = 1, lat
      DO ilon = 1, lon
        sumref=0
        istep=0
        DO j=-1*sizeofwindow, sizeofwindow
           DO i=-1*sizeofwindow, sizeofwindow
							jj = ilat+j
							ii = ilon+i
							IF ( dim2_tag(ilon,ilat) > 0 .AND.                        &
							     jj >= 1 .AND. jj <= lat .AND. ii >= 1 .AND. ii <= lon ) THEN
                istep  = istep+1
                sumref = sumref+dim2_ref(ii,jj)
							END IF
           END DO
         END DO
				 IF (istep > 0) dim2_avg_ref(ilon,ilat)=sumref / istep
      END DO
    END DO

!-----------------------------------------------------------------------
!
! Get the Position of max ref
!
!-----------------------------------------------------------------------
    maxrefdots_realnum=0
    DO isearch=1, maxdotnum+maxdotnum_more_search
      ! look for dot with biggest ref
      maxref=-999
      ii=-1
      jj=-1
      DO ilat=1, lat
        DO ilon=1, lon
          IF (dim2_tag(ilon,ilat)==0) CYCLE
          IF (dim2_avg_ref(ilon,ilat) > maxref) THEN
            jj=ilat
            ii=ilon
            maxref=dim2_avg_ref(ilon,ilat)
          ENDIF
        ENDDO
      ENDDO
      IF (ii /= -1) THEN

        rlat = dim2_lat(ii,jj)
				rlon = dim2_lon(ii,jj)

        ! Save finding dot to array
        maxrefdots_realnum                     = maxrefdots_realnum+1
        maxrefdots_avg_ref(maxrefdots_realnum) = dim2_avg_ref(ii,jj)
        maxrefdots_ref(maxrefdots_realnum)     = dim2_ref(ii,jj)
        maxrefdots_lon(maxrefdots_realnum)     = rlon
        maxrefdots_lat(maxrefdots_realnum)     = rlat

        dim2_tag(ii,jj)=0

        ! Set tag of dots near the biggest ref dot
        DO ilat=1, lat
          DO ilon=1, lon
            IF (dim2_lon(ilon,ilat) >= rlon-lonspan1 .AND.              &
                dim2_lon(ilon,ilat) <= rlon+lonspan1 .AND.              &
                dim2_lat(ilon,ilat) >= rlat-latspan1 .AND.              &
                dim2_lat(ilon,ilat) <= rlat+latspan1) THEN
              dim2_tag(ilon,ilat)=0
            ENDIF
			  	END DO
        END DO

      END IF
		END DO

	ELSE

    loopcount=2*sizeofwindow+1
    ALLOCATE(loopnums(loopcount),STAT=istatus)
    loopnums(1)=0
    DO i=1,sizeofwindow
      loopnums(i*2)=i
      loopnums(i*2+1)=i*(-1)
    END DO

    pixelcount = 0
    DO i=1, pixel
      pixel_tag(i)=1 ! 1 is usefull, 0 is useless
      !pixel_lon(i)=Longitude+(pixel_y(i)-1)*LonGridSpacing
      !pixel_lat(i)=Latitude-(pixel_x(i)-1)*LatGridSpacing
      pixel_lon(i)=Longitude+pixel_y(i)*LonGridSpacing
      pixel_lat(i)=Latitude-pixel_x(i)*LatGridSpacing
      !pixelcount = pixelcount + pixel_count(i)
	  	!WRITE(*,'(1x,i8,2I10,F10.2,2F10.2,I10)') i, pixel_x(i),pixel_y(i),refval(i), &
	  	!         pixel_lon(i),pixel_lat(i),pixel_count(i)
    END DO

    !WRITE(*,'(1x,a,I0,I10)') 'pixelcount = ',pixelcount, pixel
	  !WRITE(*,'(1x,4I10)') MINVAL(pixel_x),MAXVAL(pixel_x),MINVAL(pixel_y),MAXVAL(pixel_y)

!-----------------------------------------------------------------------
!
! Get average ref
!
!-----------------------------------------------------------------------

    DO i=1, pixel

      IF ((pixel_count(i)>50).OR.(refval(i)<minref) .OR.                  &
              (i<=10) .OR. (i>(pixel-10)) ) THEN
        pixel_tag(i)=0
        CYCLE
      ENDIF

      IF (lonlatconfine /= 0) THEN
        IF (pixel_lon(i) < lonmin  .OR. pixel_lon(i) >lonmax .OR.         &
            pixel_lat(i) < latmin  .OR. pixel_lat(i) >latmax) THEN
          pixel_tag(i)=0
          CYCLE
        ENDIF
      ENDIF

      sumref=0; istepcount=0;
      DO istep=1, loopcount
        j=i+loopnums(istep)
        IF (refval(j) <= 0) CYCLE
        IF (pixel_y(j+1) < pixel_y(j)) EXIT
        sumref=sumref+refval(i+loopnums(istep))*pixel_count(i+loopnums(istep))
        istepcount=istepcount+pixel_count(i+loopnums(istep))
        IF (istepcount>loopcount) EXIT
      END DO
      pixel_avg_ref(i)=sumref / istepcount
      pixel_tag(i)=1

    END DO

!-----------------------------------------------------------------------
!
! Get the Position of max ref
!
!-----------------------------------------------------------------------

    maxrefdots_realnum=0
    do isearch=1, maxdotnum+maxdotnum_more_search
      ! look for dot with biggest ref
      maxref=-999
      ipixel=-1
      DO i=1, pixel
        IF (pixel_tag(i)==0) THEN
          CYCLE
        ENDIF
        !IF (refval(i-1) < (refval(i)*0.2) .OR. refval(i+1) < (refval(i)*0.2)) THEN
        !  pixel_tag(i)=0
        !  CYCLE
        !ENDIF
        IF (pixel_avg_ref(i)>maxref) THEN
          ipixel=i
          maxref=pixel_avg_ref(i)
        ENDIF
      END DO
      IF (ipixel /= -1) THEN
        ! Save finding dot to array
        maxrefdots_realnum = maxrefdots_realnum+1
        maxrefdots_ref(maxrefdots_realnum)     = refval(ipixel)
        maxrefdots_avg_ref(maxrefdots_realnum) = pixel_avg_ref(ipixel)
        maxrefdots_lon(maxrefdots_realnum)     = pixel_lon(ipixel)
        maxrefdots_lat(maxrefdots_realnum)     = pixel_lat(ipixel)
        maxrefdots_pixel(maxrefdots_realnum)   = ipixel

        pixel_tag(ipixel)=0
        ! Set tag of dots near the biggest ref dot
        DO i=1, pixel
          IF (pixel_lon(i) >= pixel_lon(ipixel)-lonspan1 .AND.            &
              pixel_lon(i) <= pixel_lon(ipixel)+lonspan1 .AND.            &
              pixel_lat(i) >= pixel_lat(ipixel)-latspan1 .AND.            &
              pixel_lat(i) <= pixel_lat(ipixel)+latspan1) THEN
            pixel_tag(i)=0
          ENDIF
        ENDDO
      ELSE
        EXIT
      ENDIF
    END DO

	END IF

!-----------------------------------------------------------------------
!
! get the result maxdot array
!
!-----------------------------------------------------------------------

  DO i=1, maxrefdots_realnum
    maxrefdots_tag(i)=1
  END DO

  result_maxrefdots_realnum=0
  i=1
  DO WHILE (i<=maxrefdots_realnum .AND. result_maxrefdots_realnum < maxdotnum)

    result_maxrefdots_realnum = result_maxrefdots_realnum+1
    result_maxrefdots_ref(result_maxrefdots_realnum)     = maxrefdots_ref(i)
    result_maxrefdots_avg_ref(result_maxrefdots_realnum) = maxrefdots_avg_ref(i)
    result_maxrefdots_lon(result_maxrefdots_realnum)     = maxrefdots_lon(i)
    result_maxrefdots_lat(result_maxrefdots_realnum)     = maxrefdots_lat(i)
    result_maxrefdots_pixel(result_maxrefdots_realnum)   = maxrefdots_pixel(i)

    DO j=i, maxrefdots_realnum
      IF (maxrefdots_lon(j) >= maxrefdots_lon(i)-lonspan2 .AND.         &
          maxrefdots_lon(j) <= maxrefdots_lon(i)+lonspan2 .AND.         &
          maxrefdots_lat(j) >= maxrefdots_lat(i)-latspan2 .AND.         &
          maxrefdots_lat(j) <= maxrefdots_lat(i)+latspan2) THEN
        maxrefdots_tag(j)=0
      ENDIF
    END DO

    i=i+1
    DO WHILE (i <= maxrefdots_realnum .AND. maxrefdots_tag(i)==0 )
      i=i+1
    END DO

  END DO

!-----------------------------------------------------------------------
!
! Print information
!
!-----------------------------------------------------------------------

  IF (printinfo == 1) THEN

    WRITE(6,'(A,I2,A)') '------------result dot num:',result_maxrefdots_realnum,' -----------------'
    WRITE(6,'(/A)')'the array maxrefdots:'
    DO i=1, maxrefdots_realnum
      WRITE(6,'(1x,I4,2(A,F6.2),2(A,F10.2),A,I10)') i,                    &
           ': dots_ref=',maxrefdots_ref(i),'; dots_avg_ref=',maxrefdots_avg_ref(i), &
           ', dots_lon=',maxrefdots_lon(i),'; dots_lat=',maxrefdots_lat(i),         &
           ', dots_pixel=',maxrefdots_pixel(i)
    END DO
    WRITE(6,'(A14,A14,A14)') 'Relectivety','Longitude','Latitude'

  END IF

  IF (result_maxrefdots_realnum > maxdotnum) THEN
    itemp=maxdotnum
  ELSE
    itemp=result_maxrefdots_realnum
  END IF

  DO i=1, itemp
    IF (printinfo == 1) WRITE(6,'(F14.4,F14.4,F14.4)') result_maxrefdots_ref(i),result_maxrefdots_lon(i)+0.25,result_maxrefdots_lat(i)
    WRITE(*,'(1x,I2.2,a,2F12.4)') i, ' = ',result_maxrefdots_lon(i)+0.25,result_maxrefdots_lat(i)
  END DO

  !STOP
END PROGRAM getMaxRef
