PROGRAM model2rad
!
!##################################################################
!##################################################################
!######                                                      ######
!######                   PROGRAM MODEL2RAD                  ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Generate fake radar data from an ARPS history file.
!
!  It shares with the model the include file 'globcst.inc'
!  for storage parameters.
!
!  It reads in a history file produced by ARPS 4.0 in a user
!  specified format.
!
!  Parameters grdin,basin,mstin,icein,trbin are read in from the
!  data file itself, therefore are determined internally.
!  Arrays that are not read in retain their initial zero values.
!  These parameters are passed among subroutines through
!  a common block defined in 'indtflg.inc'.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Keith Brewster, CAPS, December, 1995
!
!  MODIFICATION HISTORY:
!
!  12/06/2011 (Y. Wang)
!  Upgraded to arps5.3. Updated the reflectivity formula and output
!  interface.
!
!-----------------------------------------------------------------------
!
!  DATA ARRAYS READ IN:
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (km)
!    zp       z coordinate of grid points in computational space (m)
!    zpsoil   z coordinate of soil levels (m)
!    uprt     x component of perturbation velocity (m/s)
!    vprt     y component of perturbation velocity (m/s)
!    wprt     Vertical component of perturbation velocity in Cartesian
!             coordinates (m/s).
!
!    ptprt    Perturbation potential temperature (K)
!    pprt     Perturbation pressure (Pascal)
!
!    qvprt    Perturbation water vapor mixing ratio (kg/kg)
!    qc       Cloud water mixing ratio (kg/kg)
!    qr       Rainwater mixing ratio (kg/kg)
!    qi       Cloud ice mixing ratio (kg/kg)
!    qs       Snow mixing ratio (kg/kg)
!    qh       Hail mixing ratio (kg/kg)
!
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    wbar     Base state z velocity component (m/s)
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhobar   Base state air density (kg/m**3)
!    qvbar    Base state water vapor mixing ratio (kg/kg)
!
!  CALCULATED DATA ARRAYS:
!
!    u        x component of velocity (m/s)
!    v        y component of velocity (m/s)
!    w        z component of velocity (m/s)
!
!  WORK ARRAYS:
!
!    tem1     Temporary work array.
!    tem2     Temporary work array.
!    tem3     Temporary work array.
!
!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
!  Module files:
!
!-----------------------------------------------------------------------
!
  USE wrfio_api, only: wrf_get_dimensions

  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'
  INCLUDE 'indtflg.inc'
  INCLUDE 'phycst.inc'
  INCLUDE 'mp.inc'
!
!-----------------------------------------------------------------------
!
!  Arrays to be read in:
!
!-----------------------------------------------------------------------
!

  REAL, ALLOCATABLE  :: x     (:)         ! The x-coord. of the physical and
                                          ! computational grid. Defined at u-point.
  REAL, ALLOCATABLE  :: y     (:)         ! The y-coord. of the physical and
                                          ! computational grid. Defined at v-point.
  REAL, ALLOCATABLE  :: z     (:)         ! The z-coord. of the computational grid.
                                          ! Defined at w-point on the staggered grid.
  REAL, ALLOCATABLE  :: zp    (:,:,:)     ! The physical height coordinate defined at
                                          ! w-point of the staggered grid.
  REAL, ALLOCATABLE  :: zpsoil(:,:,:)     ! Soil level depth.
  REAL, ALLOCATABLE  :: mapfct(:,:,:)     ! Map factors at scalar, u and v points

  REAL, ALLOCATABLE  :: u     (:,:,:)     ! Total u-velocity (m/s)
  REAL, ALLOCATABLE  :: v     (:,:,:)     ! Total v-velocity (m/s)
  REAL, ALLOCATABLE  :: w     (:,:,:)     ! Total w-velocity (m/s)
  REAL, ALLOCATABLE  :: qv    (:,:,:)     ! Total water vapor specific
                                          ! humidity (kg/kg)

  REAL, ALLOCATABLE  :: qscalar(:,:,:,:)

  REAL, ALLOCATABLE  :: tke   (:,:,:)     ! Turbulent kinetic energy
  REAL, ALLOCATABLE  :: kmh   (:,:,:)     ! The turbulent mixing coefficient for
                                          ! momentum. ( m**2/s ) horizontal
  REAL, ALLOCATABLE  :: kmv   (:,:,:)     ! The turbulent mixing coefficient for
                                          ! momentum. ( m**2/s ) vertical

  REAL, ALLOCATABLE  :: ubar  (:,:,:)     ! Base state u-velocity (m/s)
  REAL, ALLOCATABLE  :: vbar  (:,:,:)     ! Base state v-velocity (m/s)
  REAL, ALLOCATABLE  :: wbar  (:,:,:)     ! Base state w-velocity (m/s)
  REAL, ALLOCATABLE  :: ptbar (:,:,:)     ! Base state potential temperature (K)
  REAL, ALLOCATABLE  :: rhobar(:,:,:)     ! Base state air density (kg/m**3)
  REAL, ALLOCATABLE  :: pbar  (:,:,:)     ! Base state pressure (Pascal)
  REAL, ALLOCATABLE  :: qvbar (:,:,:)     ! Base state water vapor specific humidity
                                          ! (kg/kg)

  INTEGER, ALLOCATABLE  :: soiltyp(:,:,:)   ! Soil type
  INTEGER, ALLOCATABLE  :: vegtyp(:,:)      ! Vegetation type
  REAL, ALLOCATABLE  :: stypfrct(:,:,:)     ! Soil type fraction
  REAL, ALLOCATABLE  :: lai    (:,:)      ! Leaf Area Index
  REAL, ALLOCATABLE  :: roufns (:,:)      ! Surface roughness
  REAL, ALLOCATABLE  :: veg    (:,:)      ! Vegetation fraction
  REAL, ALLOCATABLE  :: hterain(:,:)      ! The height of the terrain

  REAL, ALLOCATABLE :: tsoil  (:,:,:,:)   ! soil temperature (K)
  REAL, ALLOCATABLE :: qsoil  (:,:,:,:)   ! soil moisture (g/kg)

  REAL, ALLOCATABLE  :: wetcanp(:,:)       ! Canopy water amount
  REAL, ALLOCATABLE  :: snowdpth(:,:)      ! Snow depth (m)

  REAL, ALLOCATABLE  :: raing  (:,:)       ! Grid supersaturation rain
  REAL, ALLOCATABLE  :: rainc  (:,:)       ! Cumulus convective rain
  REAL, ALLOCATABLE  :: prcrate(:,:,:)     ! precipitation rate (kg/(m**2*s))
                                           ! prcrate(1,1,1) = total precip. rate
                                           ! prcrate(1,1,2) = grid scale precip. rate
                                           ! prcrate(1,1,3) = cumulus precip. rate
                                           ! prcrate(1,1,4) = microphysics precip. rate

  REAL, ALLOCATABLE  :: radfrc(:,:,:)      ! Radiation forcing (K/s)
  REAL, ALLOCATABLE  :: radsw (:,:)        ! Solar radiation reaching the surface
  REAL, ALLOCATABLE  :: rnflx (:,:)        ! Net radiation flux absorbed by surface
  REAL, ALLOCATABLE :: radswnet (:,:) ! Net solar radiation, SWin - SWout
  REAL, ALLOCATABLE :: radlwin  (:,:) ! Incoming longwave radiation

  REAL, ALLOCATABLE  :: usflx (:,:)        ! Surface flux of u-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE  :: vsflx (:,:)        ! Surface flux of v-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE  :: ptsflx(:,:)        ! Surface heat flux (K*kg/(m*s**2))
  REAL, ALLOCATABLE  :: qvsflx(:,:)        ! Surface moisture flux (kg/(m**2*s))

  REAL, ALLOCATABLE  :: uprt  (:,:,:)     ! Perturbation u-velocity (m/s)
  REAL, ALLOCATABLE  :: vprt  (:,:,:)     ! Perturbation v-velocity (m/s)
  REAL, ALLOCATABLE  :: wprt  (:,:,:)     ! Perturbation w-velocity (m/s)
  REAL, ALLOCATABLE  :: ptprt (:,:,:)     ! Perturbation potential temperature (K)
  REAL, ALLOCATABLE  :: pprt  (:,:,:)     ! Perturbation pressure (Pascal)
  REAL, ALLOCATABLE  :: qvprt (:,:,:)     ! Perturbation water vapor specific
                                          ! humidity (kg/kg)

!
!-----------------------------------------------------------------------
!
!  Other data variables
!
!-----------------------------------------------------------------------
!
  REAL :: time
  REAL, ALLOCATABLE  :: tk (:,:,:)     ! Temperature (K)
!
!-----------------------------------------------------------------------
!
!  Temporary working arrays:
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE  :: tem1(:,:,:)
  REAL, ALLOCATABLE  :: tem2(:,:,:)
  REAL, ALLOCATABLE  :: tem3(:,:,:)
  REAL, ALLOCATABLE  :: tem4(:,:,:)
  REAL, ALLOCATABLE  :: tem5(:,:,:)
!
!-----------------------------------------------------------------------
!
!  ARPS dimensions:
!
!  nx, ny, nz: Dimensions of computatioal grid. When run on MPP
!              with PVM or MPI, they represent of the size of the
!              subdomains. See below.
!
!              Given nx, ny and nz, the physical domain size will be
!              xl=(nx-3)*dx by yl=(ny-3)*dy by zh=(nz-3)*dz. The
!              variables nx, ny, nz, dx, dy and dz are read in from
!              the input file by subroutine INITPARA.
!
!-----------------------------------------------------------------------
!
  INTEGER :: nx       ! Number of grid points in the x-direction
  INTEGER :: ny       ! Number of grid points in the y-direction
  INTEGER :: nz       ! Number of grid points in the z-direction
  INTEGER :: nzsoil   ! Number of soil levels
  INTEGER :: nstyps   ! Number of soil types
!
!-----------------------------------------------------------------------
!
!  User request stuff
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=4)   :: radid
  CHARACTER (LEN=256) :: grdbasfn,filename
  CHARACTER (LEN=256) :: rfname
  REAL :: latrad,lonrad,elvrad, latradc, lonradc
  REAL :: thref,elvmin,elvmax,rngmin,rngmax
!
  INTEGER :: hinfmt, ll_or_xy, grdtlt_flag
  INTEGER :: dmpfmt,hdf4cmpr,rfopt
  INTEGER :: isource
  REAL :: refelvmin,refelvmax
  REAL :: refrngmin,refrngmax
!
!-----------------------------------------------------------------------
!
!  Scalar grid location variables
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE  :: lat(:,:),lon(:,:)

!-----------------------------------------------------------------------
!
!  Radar variables
!
!-----------------------------------------------------------------------
!
  INCLUDE 'remap.inc'

  REAL, ALLOCATABLE :: gridhgt(:,:,:)
  REAL, ALLOCATABLE :: gridrange(:,:,:)
  REAL, ALLOCATABLE :: griddata(:,:,:,:)
  !REAL, ALLOCATABLE :: gridnyq(:,:,:)
  !REAL, ALLOCATABLE :: gridtim(:,:,:)
  REAL, ALLOCATABLE :: xs(:),ys(:)
  REAL, ALLOCATABLE :: zps(:,:,:)
  REAL, ALLOCATABLE :: outelev(:), outtim(:)

  REAL, ALLOCATABLE :: alpha(:,:,:,:)

  REAL, PARAMETER   :: pi = 4.0 * atan(1.0)
  REAL              :: cx(6)

!
!-----------------------------------------------------------------------
!
!  Radar angles
!
!-----------------------------------------------------------------------
!
  REAL,    PARAMETER :: rmissing = -9999.0
  REAL,    PARAMETER :: novalues = -99.0

  INTEGER, PARAMETER :: maxtilt = 14
  REAL,    PARAMETER :: hlfwid  = 0.45
  INTEGER :: ntilt
  REAL :: ang(maxtilt)
  REAL :: ang11(maxtilt),ang12(maxtilt)
  REAL :: ang31(maxtilt),ang32(maxtilt)
  DATA ang11 / 0.5,1.5,2.4,3.4,4.3,5.3,6.2,7.5,8.7,                     &
               10.0,12.0,14.0,16.7,19.5/
  DATA ang12 / 0.5,1.5,2.4,3.4,4.3,6.0,9.9,14.6,19.5,                   &
               19.5,19.5,19.5,19.5,19.5/
  DATA ang31 / 0.5,1.5,2.5,3.5,4.5,4.5,4.5,4.5,4.5,                     &
                4.5, 4.5, 4.5, 4.5, 4.5/
  DATA ang32 / 0.5,1.5,2.5,3.5,4.5,4.5,4.5,4.5,4.5,                     &
                4.5, 4.5, 4.5, 4.5, 4.5/
!
!-----------------------------------------------------------------------
!
! Variables for terminal velocity estimate
!
!-----------------------------------------------------------------------
!
  REAL :: denom,refz,rhofact,s1,s2,vt
  REAL, PARAMETER :: zfrez= 3000.      ! freezing level (m)
  REAL, PARAMETER :: zice = 8000.      ! level above which entirely ice
  REAL, PARAMETER :: h0   = 7000.
!
!-----------------------------------------------------------------------
!
!  Misc internal variables
!
!-----------------------------------------------------------------------
!
  INTEGER, PARAMETER :: mxradvr=10, nradvr=7
  INTEGER :: iradvr(mxradvr)
  DATA iradvr /1,2,3,4,5,6,7,0,0,0/

  CHARACTER (LEN=60) :: vcptxt
  INTEGER :: i,j,k,nq,itilt,mode
  INTEGER :: nt,istatus
  INTEGER :: ngchan,nchanl,lenfil,lengbf,nchin
  INTEGER :: iradfmt,vcpnum,myr,nqw
  INTEGER :: knt,knttry,kntref,kntrng,kntang
  INTEGER :: abstsec,iyr,imon,iday,ihr,imin,isec
  CHARACTER(LEN=40) :: qnames_wrf(40)
  REAL :: latnot(2)
  REAL :: gdx,gdy,xctr,yctr,x0sc,y0sc,radarx,radary,delx,dely
  REAL :: azim,dist,delz,eleva,range,dhdr,dsdr,rngsc,rff, zdr, kdp, rhv
  REAL :: ddrot,uazmrad,vazmrad
  REAL :: perc,prctry,prcref,prcdst,prcang, rdummy
  INTEGER :: k3L, k3U, kmid
  LOGICAL :: angmatch
  REAL :: height, bmwidth, elevc, vr, temAng

  INTEGER :: nxny, idat, ioffset, joffset, ncoltot
  INTEGER :: wrfopt, iunit, idummy
  INTEGER :: unum
  CHARACTER(LEN=256) :: outdirname
  CHARACTER(LEN=20)  :: file_ext
  LOGICAL :: iexist
  INTEGER :: radnumvar, grdtiltver
  REAL    :: dazim

  LOGICAL, ALLOCATABLE :: havdat(:,:)

  INTEGER, ALLOCATABLE :: icolp(:)
  INTEGER, ALLOCATABLE :: jcolp(:)
  REAL,    ALLOCATABLE :: xcolp(:)
  REAL,    ALLOCATABLE :: ycolp(:)
  REAL,    ALLOCATABLE :: zcolp(:,:)

  REAL,    ALLOCATABLE :: colvel(:,:)
  REAL,    ALLOCATABLE :: colref(:,:)
  REAL,    ALLOCATABLE :: colrhv(:,:)
  REAL,    ALLOCATABLE :: colzdr(:,:)
  REAL,    ALLOCATABLE :: colkdp(:,:)
  REAL,    ALLOCATABLE :: colnyq(:,:)
  REAL,    ALLOCATABLE :: coltim(:,:)
  !REAL,    ALLOCATABLE :: colhgt(:,:)
  REAL,    ALLOCATABLE :: elvang(:),vrsc(:)

!
!-----------------------------------------------------------------------
!
!  WRF variables
!
!-----------------------------------------------------------------------
!

  INTEGER :: iproj_wrf
  CHARACTER(LEN=19) :: timestring
  INTEGER :: nx_wrf, ny_wrf

  REAL :: p_top
  REAL,    ALLOCATABLE :: znw(:), znu(:), dnw(:)
  REAL,    ALLOCATABLE :: mu(:,:), mub(:,:)

  REAL, ALLOCATABLE  :: phprt (:,:,:)     ! Geopotential Height (m**2/s**2)
  REAL, ALLOCATABLE  :: phb   (:,:,:)
  REAL, ALLOCATABLE  :: psfc  (:,:), varsfc(:,:,:)

  REAL, ALLOCATABLE  :: fnm(:),fnp(:),dn(:)
  REAL               :: cf1, cf2,cf3

!-----------------------------------------------------------------------
!
! NAMELIST DECLARATION
!
!-----------------------------------------------------------------------

  INTEGER :: dualopt
  REAL    :: wavelen

  NAMELIST /file_name/ modelopt,hinfmt,grdbasfn,filename
  NAMELIST /radar_info/ radid,grdtlt_flag,ll_or_xy, latrad, lonrad,radarx, radary, &
                        elvrad, vcpnum, rngmin,rngmax, rfopt, thref, dualopt, wavelen
  !NAMELIST /wrf_read/ year,month,day,hour,minute,second, hisfile
  !NAMELIST /wrf_grid/ dx,dy,dz,strhopt,zrefsfc,dlayer1,dlayer2,strhtune,zflat,&
  !                ctrlat,ctrlon,crdorgnopt
  NAMELIST /others/ dzmin

  INTEGER, PARAMETER :: dmpzero = 0

  !CHARACTER(LEN=256) :: dirname
  INTEGER            :: dualpout

  NAMELIST /output/ dirname,dmpfmt, hdf4cmpr, dualpout, lvldbg

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  CALL mpinit_var

  WRITE(6,'(/11(/5x,a)/)')                                              &
     '###############################################################', &
     '###############################################################', &
     '###                                                         ###', &
     '###                  Welcome to MODEL2RAD                   ###', &
     '###      This program reads in the history dump data        ###', &
     '###      sets generated by ARPS/WRF, and generates a fake   ###', &
     '###      radar data file.                                   ###', &
     '###                                                         ###', &
     '###############################################################', &
     '###############################################################'

  unum = 0
  CALL GET_COMMAND_ARGUMENT(1, outdirname, ldirnam, istatus )
  !print*,'nlfile==', nlfile
  IF ( outdirname(1:1) == ' ' .OR. istatus /= 0 ) THEN  ! Use standard input to be backward-compatible
    unum = 5
  ELSE
    INQUIRE(FILE=outdirname,EXIST=iexist)
    IF (.NOT. iexist) THEN
      WRITE(6,'(1x,3a)') 'WARNING: namelist file - ',                   &
            TRIM(outdirname),' does not exist. Falling back to standard input.'
      unum = 5
    END IF
  END IF

  IF (unum /= 5) THEN
    CALL getunit( unum )
    OPEN(unum,FILE=TRIM(outdirname),STATUS='OLD',FORM='FORMATTED')
    WRITE(6,'(1x,3a,/,1x,a,/)') 'Reading MODEL2RAD namelist from file - ',   &
            TRIM(outdirname),' ... ','========================================'
  ELSE
    WRITE(6,'(2(1x,a,/))') 'Waiting namelist from standard input ... ', &
                           '========================================'
  END IF

!-----------------------------------------------------------------------
!
!  Get the name of the input data set.
!
!-----------------------------------------------------------------------
!
  modelopt=1
  READ(unum,file_name,END=100)
  WRITE(*,'(/a)')' Namelist file_name sucessfully read in.'

  lengbf=LEN_trim(grdbasfn)
  lenfil=LEN_trim(filename)

  IF (modelopt == 1) WRITE(*,'(/a,a)')' Grid-base file name is ', grdbasfn(1:lengbf)
  WRITE(*,'(/a,a)')' The  data  set name is ', filename(1:lenfil)

  !IF ( modelopt == 2 ) THEN
  !   READ(5,wrf_read,END=100)
  !   WRITE(6,'(/a)')' Namelist wrf_read sucessfully read in.'
  !   WRITE(6,'(2a)')' The history file is ', trim(hisfile)
  !   READ(5,wrf_grid,END=100)
  !   WRITE(6,'(/a)')' Namelist wrf_grid sucessfully read in.'
  !
  !END IF

  IF ( modelopt == 1 ) THEN
    CALL get_dims_from_data(hinfmt,filename(1:lenfil),                  &
                            nx,ny,nz,nzsoil,nstyps, istatus)
    nproc_x = 1  !this is a parameter for non mpi setting
    nproc_y = 1  !this is a parameter for non mpi setting
  ELSE
    nproc_x = 1  !this is a parameter for non mpi setting
    nproc_y = 1  !this is a parameter for non mpi setting
    nproc_x_in = 1
    nproc_y_in = 1

    CALL get_wrf_domtime(TRIM(filename),1,timestring,istatus)
    READ (timestring, '(i4.4,1x,i2.2,1x,i2.2,1x,i2.2,1x,i2.2,1x,i2.2)' ) &
                      year,month,day,hour,minute,second

    CALL get_wrf_domsize(TRIM(filename),timestring,nx_wrf,ny_wrf,       &
                         nz,nzsoil,nt,nx,ny,istatus)

    nx = nx+2   ! to be used in the ARPS grid
    ny = ny+2
    nz = nz+2
    CALL get_wrf_domscalar(TRIM(filename),nscalar,nscalarq,qnames,      &
                           P_QC,P_QR,P_QI,P_QS,P_QG,P_QH,               &
                           P_NC,P_NR,P_NI,P_NS,P_NG,P_NH,               &
                           P_ZR,P_ZI,P_ZS,P_ZG,P_ZH,                    &
                           istatus)
    !P_QH=P_QG

    !CALL wrf_get_dimensions(filename,numdigits,nx,ny,nz,nzsoi,nstyps,nscalar,     &
    !                        P_QC,P_QR,P_QI,P_QS,P_QG,P_QH,                        &
    !                        P_NC,P_NR,P_NI,P_NS,P_NG,P_NH,                        &
    !                        P_ZR,P_ZI,P_ZS,P_ZG,P_ZH,qnames,                      &
    !                        mapproj,ctrlat,ctrlon,trulat1,trulat2,trulon,         &
    !                        dx,dy,ireturn )
    !P_QH=P_QG

    nstyps = 1
    mpfctopt=1
    ternopt=2

  END IF

  CALL mpinit_var

!-----------------------------------------------------------------------
!
!  Initialize variables.
!
!-----------------------------------------------------------------------
!
  istatus=0
  nstyp=nstyps
  denom=1./(zice-zfrez)

  ALLOCATE(x(nx))
  ALLOCATE(y(ny))
  ALLOCATE(z(nz))
  ALLOCATE(vrsc(nz))
  ALLOCATE(elvang(nz))
  ALLOCATE(zp(nx,ny,nz))
  ALLOCATE(zpsoil(nx,ny,nzsoil))
  x=0
  y=0
  z=0
  zp=0
  zpsoil=0

  ALLOCATE(u(nx,ny,nz))
  u=0
  ALLOCATE(v(nx,ny,nz))
  v=0
  ALLOCATE(w(nx,ny,nz))
  w=0
  ALLOCATE(qv(nx,ny,nz))
  qv=0

  ALLOCATE(qscalar(nx,ny,nz,nscalar), STAT = istatus)
  qscalar = 0.0
  ALLOCATE(tke(nx,ny,nz))
  tke=0
  ALLOCATE(kmh(nx,ny,nz))
  kmh=0
  ALLOCATE(kmv(nx,ny,nz))
  kmv=0
  ALLOCATE(ubar(nx,ny,nz))
  ubar=0
  ALLOCATE(vbar(nx,ny,nz))
  vbar=0
  ALLOCATE(wbar(nx,ny,nz))
  wbar=0
  ALLOCATE(ptbar(nx,ny,nz))
  ptbar=0
  ALLOCATE(rhobar(nx,ny,nz))
  rhobar=0
  ALLOCATE(pbar(nx,ny,nz))
  pbar=0
  ALLOCATE(qvbar(nx,ny,nz))
  qvbar=0
  ALLOCATE(soiltyp(nx,ny,nstyps))
  soiltyp=0
  ALLOCATE(stypfrct(nx,ny,nstyps))
  stypfrct=0
  ALLOCATE(vegtyp(nx,ny))
  vegtyp=0
  ALLOCATE(lai(nx,ny))
  lai=0
  ALLOCATE(roufns(nx,ny))
  roufns=0
  ALLOCATE(veg(nx,ny))
  veg=0
  ALLOCATE(tsoil   (nx,ny,nzsoil,0:nstyps))
  tsoil = 0
  ALLOCATE(qsoil   (nx,ny,nzsoil,0:nstyps))
  qsoil = 0

  ALLOCATE(wetcanp(nx,ny))
  wetcanp=0
  ALLOCATE(snowdpth(nx,ny))
  snowdpth=0
  ALLOCATE(raing(nx,ny))
  raing=0
  ALLOCATE(rainc(nx,ny))
  rainc=0
  ALLOCATE(prcrate(nx,ny,4))
  prcrate=0
  ALLOCATE(radfrc(nx,ny,nz))
  radfrc=0
  ALLOCATE(radsw(nx,ny))
  radsw=0
  ALLOCATE(rnflx(nx,ny))
  rnflx=0
  ALLOCATE(radswnet (nx,ny))
  radswnet = 0
  ALLOCATE(radlwin (nx,ny))
  radlwin = 0

  ALLOCATE(usflx(nx,ny))
  usflx=0
  ALLOCATE(vsflx(nx,ny))
  vsflx=0
  ALLOCATE(ptsflx(nx,ny))
  ptsflx=0
  ALLOCATE(qvsflx(nx,ny))
  qvsflx=0
  ALLOCATE(ptprt(nx,ny,nz))
  ptprt=0
  ALLOCATE(pprt(nx,ny,nz))
  pprt=0

  ALLOCATE(tk(nx,ny,nz))
  tk=0

  ALLOCATE(tem1(nx,ny,nz))
  ALLOCATE(tem2(nx,ny,nz))
  ALLOCATE(tem3(nx,ny,nz))
  ALLOCATE(tem4(nx,ny,nz))
  ALLOCATE(tem5(nx,ny,nz))

  tem1=0
  tem2=0
  tem3=0
  tem4=0
  tem5=0

  ALLOCATE(lat(nx,ny))
  ALLOCATE(lon(nx,ny))
  lat=0
  lon=0

!-------------------------------------------------------------
!
! Allocate for variables from remap_d.inc
!
!-------------------------------------------------------------

  ALLOCATE (xs(nx),ys(ny))
  ALLOCATE (zps(nx,ny,nz))

  ALLOCATE(havdat(nx,ny), STAT = istatus)
!
!------------------------------------------------------------------------
!
! Get the radar information
!
!------------------------------------------------------------------------

  dualopt = 0
  wavelen = 107.

  READ(unum,radar_info,END=100)
  WRITE(*,'(/a)')' Namelist radar_info sucessfully read in.'

  mphyopt=MOD(rfopt,1000)

  READ(unum, others, END=100)

  dirname  = ' '
  dmpfmt   = 3
  hdf4cmpr = 1
  dualpout = 0
  READ(unum, OUTPUT, END=100)

  ldirnam = LEN_TRIM(dirname)
  IF( ldirnam == 0 ) THEN
    outdirname = './'
    ldirnam=2

    WRITE(*,'(5x,a)') 'Output files will be in the current work directory.'
  ELSE
    CALL get_output_dirname(1,dirname,idummy,1,outdirname,istatus)
  END IF

  radnumvar = 2
  IF (dualpout > 0) THEN
    IF (dualopt < 1 ) THEN
      WRITE(*,'(1x,a,3(I0,a))') 'ERROR: unsupported option combinations: dualopt = ',dualopt, &
                                ', dualpout = ',dualpout,'.'
      STOP
    END IF
    radnumvar = 5
  END IF

  IF (unum /= 5) THEN
    CLOSE(unum)
    CALL retunit(unum)
  END IF

!-----------------------------------------------------------------------
!
!  Set elevation angles
!
!-----------------------------------------------------------------------

  IF(vcpnum == 11) THEN
    ntilt=14
    vcptxt='Storm mode  14 tilts 0.5-19.5 deg'
    DO itilt=1,ntilt
      ang(itilt)=ang11(itilt)
    END DO
  ELSE IF (vcpnum == 12) THEN
    ntilt=9
    vcptxt='Storm mode   9 tilts 0.5-19.5 deg'
    DO itilt=1,ntilt
      ang(itilt)=ang12(itilt)
    END DO
  ELSE IF (vcpnum == 31) THEN
    ntilt=5
    vcptxt='Clear-air    5 tilts 0.5- 4.5 deg'
    DO itilt=1,ntilt
      ang(itilt)=ang31(itilt)
    END DO
  ELSE IF (vcpnum == 32) THEN
    ntilt=5
    vcptxt='Clear-air    5 tilts 0.5- 4.5 deg'
    DO itilt=1,ntilt
      ang(itilt)=ang32(itilt)
    END DO
  ELSE
    WRITE(6,*) vcpnum,' is a bogus vcpnum'
    STOP
  END IF
  elvmin=ang(1)-hlfwid
  elvmax=ang(ntilt)+hlfwid
  refelvmin=ang(1)
  refelvmax=ang(ntilt)
  ALLOCATE (outelev(ntilt), STAT = istatus)
  ALLOCATE (outtim (ntilt), STAT = istatus)
  DO itilt=1,ntilt
     outelev(itilt) = ang(itilt)
  END DO
  outtim = 0.0

  IF (grdtlt_flag == 1 ) THEN
    ALLOCATE (gridhgt(nx,ny,ntilt))
    ALLOCATE (gridrange(nx,ny,ntilt))
    ALLOCATE (griddata(nx,ny,ntilt,radnumvar))
    gridhgt     = rmissing
    gridrange   = rmissing
    griddata    = rmissing
  ELSE
    ALLOCATE (griddata(nx,ny,nz,radnumvar))
    griddata = rmissing

    !ALLOCATE (gridnyq(nx,ny,nz))
    !gridnyq=100.
    !ALLOCATE (gridtim(nx,ny,nz))
    !gridtim=0.
  END IF

!
!-----------------------------------------------------------------------
!
!  Read all input data arrays
!
!-----------------------------------------------------------------------
!
  IF ( modelopt == 1 ) THEN
    ALLOCATE(uprt(nx,ny,nz),  STAT = istatus)
    uprt=0
    ALLOCATE(vprt(nx,ny,nz),  STAT = istatus)
    vprt=0
    ALLOCATE(wprt(nx,ny,nz),  STAT = istatus)
    wprt=0
    ALLOCATE(qvprt(nx,ny,nz), STAT = istatus)
    qvprt=0

    CALL dtaread(nx,ny,nz,nzsoil,nstyps,                                &
                 hinfmt,nchin,grdbasfn(1:lengbf),lengbf,                &
                 filename(1:lenfil),lenfil,time,                        &
                 x,y,z,zp,zpsoil,uprt,vprt,wprt,ptprt,pprt,             &
                 qvprt, qscalar, tke, kmh, kmv,                         &
                 ubar, vbar, wbar, ptbar, pbar, rhobar, qvbar,          &
                 soiltyp,stypfrct,vegtyp,lai,roufns,veg,                &
                 tsoil,qsoil,wetcanp,snowdpth,                          &
                 raing,rainc,prcrate,                                   &
                 radfrc,radsw,rnflx,radswnet,radlwin,                   &
                 usflx,vsflx,ptsflx,qvsflx,                             &
                 istatus, tem1,tem2,tem3)

    u  = ubar  + uprt
    v  = vbar  + vprt
    w  = wbar  + wprt
    qv = qvbar + qvprt

    DEALLOCATE(uprt, vprt, wprt, qvprt)

    curtim = time

  ELSE IF ( modelopt == 2 ) THEN

    CALL get_wrf_grid( TRIM(filename),iproj_wrf,sclfct,trulat1,trulat2,&
                       trulon,ctrlat,ctrlon,dx,dy,idummy,istatus )

    IF(iproj_wrf == 0) THEN        ! NO MAP PROJECTION
      mapproj = 0
      mpfctopt= 0
    ELSE IF(iproj_wrf == 1) THEN   ! LAMBERT CONFORMAL
      mapproj = 2
    ELSE IF(iproj_wrf == 2) THEN   ! POLAR STEREOGRAPHIC
      mapproj = 1
    ELSE IF(iproj_wrf == 3) THEN   ! MERCATOR
      mapproj = 3
    ELSE
      WRITE(6,*) 'Unknown map projection, ', iproj_wrf
      istatus = -555
    END IF

    dz = 0.0
    strhopt = 0

    ALLOCATE(mapfct(nx,ny,8), STAT = istatus)
    ALLOCATE(phb(nx,ny,nz),   STAT = istatus)
    ALLOCATE(hterain(nx,ny),  STAT = istatus)
    ALLOCATE(phprt(nx,ny,nz), STAT = istatus)
    mapfct=0
    phb=0.0
    hterain=0
    phprt=0

    ALLOCATE(mu (nx,ny), STAT = istatus)
    ALLOCATE(mub(nx,ny), STAT = istatus)
    ALLOCATE(psfc(nx,ny),STAT = istatus)
    ALLOCATE(varsfc(nx,ny,4),STAT = istatus)
    ALLOCATE(dnw(nz),    STAT = istatus)
    ALLOCATE(znw(nz),    STAT = istatus)
    ALLOCATE(znu(nz),    STAT = istatus)

    ALLOCATE(dn (nz),    STAT = istatus)
    ALLOCATE(fnm(nz),    STAT = istatus)
    ALLOCATE(fnp(nz),    STAT = istatus)

    CALL initwrfgrd(filename,nx,ny,nz,nzsoil,                           &
                    x,y,z,zp,zpsoil,hterain,mapfct,lat,lon,             &
                    tem1,tem1,tem1,tem2,tem1,tem1,tem2,                 &
                    u,v,w,ptprt,pprt,phprt,qv,qscalar,                  &
                    ubar,vbar,ptbar,pbar,qvbar,rhobar,phb,              &
                    nqw,qnames_wrf,tem3, p_top, psfc, varsfc,           &
                    mu,mub,dnw,dn, znw,znu,                             &
                    .FALSE.,fnm,fnp,cf1,cf2,cf3,                        &
                    tem1,tem2,tem1,tem2,tem3,istatus)

    DEALLOCATE(mapfct,phb,phprt,hterain)
    DEALLOCATE(mu,mub,dnw,znw,znu)
    DEALLOCATE( dn, fnm,fnp, psfc, varsfc )
  END IF

  DEALLOCATE(ubar,vbar,wbar,qvbar)

!
!-----------------------------------------------------------------------
!
!  istatus = 0 for a successful read
!
!-----------------------------------------------------------------------
!
  IF( istatus == 0 ) THEN   ! successful read

!
!-----------------------------------------------------------------------
!
!  Set map projection parameters
!
!-----------------------------------------------------------------------
!
    latnot(1)=trulat1
    latnot(2)=trulat2
    CALL setmapr(mapproj,sclfct,latnot,trulon)
    CALL lltoxy(1,1,ctrlat,ctrlon,xctr,yctr)
!
    gdx=x(2)-x(1)
    x0sc=xctr - 0.5*(nx-3)*gdx
    gdy=y(2)-y(1)
    y0sc=yctr - 0.5*(ny-3)*gdy
    CALL setorig(1,x0sc,y0sc)
!
!-----------------------------------------------------------------------
!
!  Calculate lat,lon locations of the scalar grid points
!
!-----------------------------------------------------------------------
!
    DO i=1,nx-1
      xs(i)=0.5*(x(i)+x(i+1))
    END DO
    xs(nx)=2.*xs(nx-1)-xs(nx-2)
    DO j=1,ny-1
      ys(j)=0.5*(y(j)+y(j+1))
    END DO
    ys(ny)=2.*ys(ny-1)-ys(ny-2)

    CALL xytoll(nx,ny,xs,ys,lat,lon)

    IF (ll_or_xy == 1 ) THEN ! radar specified with latrad, lonrad
      CALL lltoxy(1,1,latrad,lonrad,radarx,radary)
    ELSE
      CALL xytoll(1,1,radarx,radary,latrad, lonrad)
    END IF
!
!-----------------------------------------------------------------------
!
!  Move z field onto the scalar grid.
!
!-----------------------------------------------------------------------
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          zps(i,j,k)=0.5*(zp(i,j,k)+zp(i,j,k+1))
        END DO
      END DO
    END DO
    DO j=1,ny-1
      DO i=1,nx-1
        zps(i,j,nz)=(2.*zps(i,j,nz-1))-zps(i,j,nz-2)
      END DO
    END DO
!
!-----------------------------------------------------------------------
!
!  Combine wind perturbations and mean fields and put them
!  on scalar grid.
!
!-----------------------------------------------------------------------
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          u(i,j,k)=0.5*(u(i,j,k) + u(i+1,j,k))
        END DO
      END DO
    END DO
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          v(i,j,k)=0.5*(v(i,j,k) + v(i,j+1,k))
        END DO
      END DO
    END DO
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          w(i,j,k)=0.5*(w(i,j,k) + w(i,j,k+1))
        END DO
      END DO
    END DO
!
!-----------------------------------------------------------------------
!
!  Get reflectivity from qr, qs and qh.
!
!-----------------------------------------------------------------------
!
    DO k=1, nz-1
      DO j=1, ny-1
        DO i=1, nx-1
          tem2(i,j,k) = ptbar(i,j,k)+ptprt(i,j,k)   ! tem2 is potentail temperature
          tem3(i,j,k) = pbar(i,j,k)+pprt(i,j,k)     ! tem3 is pressure
          tk(i,j,k)   = tem2(i,j,k) * ((tem3(i,j,k)/p0)**rddcp)
        END DO
      END DO
    END DO

    CALL edgfill(tk  ,1,nx,1,nx-1, 1,ny,1,ny-1, 1,nz,1,nz-1)
    CALL edgfill(tem3,1,nx,1,nx-1, 1,ny,1,ny-1, 1,nz,1,nz-1)

    DEALLOCATE(ptbar, pbar, ptprt, pprt)

    !IF (rfopt == 2) THEN
    !  CALL reflec_ferrier(nx,ny,nz, rhobar, qr, qs, qh, tk, tem1)
    !ELSE
    !  CALL reflec(nx,ny,nz, rhobar, qr, qs, qh, tem1)
    !ENDIF

    IF (dualopt > 0) THEN
      !NOTE: mphyopt = MOD(rfopt,1000)

      IF (dualopt == 10) THEN
        CALL DualPolObs(nx,ny,nz,rhobar,tk,qscalar,tem1,tem2,           &
                        tem3,tem4,tem5,istatus)
        IF (istatus /= 0) CALL arpsstop('ERROR in DualPolObs',1)
      ELSE IF (dualopt == 1 .OR. dualopt == 2) THEN

        IF(dualopt == 2) THEN
          ALLOCATE(alpha(nx,ny,nz,6), STAT = istatus)
          alpha = 0.0

          SELECT CASE (mphyopt)
          CASE (2:4)
            alpha(:,:,:,2) = alpharain
            alpha(:,:,:,3) = alphaice
            alpha(:,:,:,4) = alphasnow
            alpha(:,:,:,5) = alphahail
            alpha(:,:,:,6) = alphahail
          CASE (5:10,12,14,118,205:207,209)
            alpha(:,:,:,2) = alpharain
            alpha(:,:,:,3) = alphaice
            alpha(:,:,:,4) = alphasnow
            alpha(:,:,:,5) = alphagrpl
            alpha(:,:,:,6) = alphahail
          CASE (11,208)

            cx(1) = (pi/6.)*rhow
            cx(2) = (pi/6.)*rhow
            cx(3) = 440.0      ! Constant value used in MY scheme for ice
            cx(4) = (pi/6.)*rhosnow
            cx(5) = (pi/6.)*rhogrpl
            cx(6) = (pi/6.)*rhohail

            DO nq=2,6
              CALL solve_alpha(nx,ny,nz,rhobar,cx(nq),                  &
                       qscalar(:,:,:,nq),qscalar(:,:,:,nq+6),           &
                       qscalar(:,:,:,nq+11),alpha(:,:,:,nq))
            ENDDO
          END SELECT
        ENDIF

        CALL ZhhFromDualPolCG(nx,ny,nz,rhobar,tk,qscalar,tem1,tem2,     &
                              dualopt,wavelen,alpha)
        CALL ZdrFromDualPolCG(nx,ny,nz,rhobar,tk,qscalar,tem3,          &
                              dualopt,wavelen,alpha)
        CALL ZdpFromDualPolCG(nx,ny,nz,rhobar,tk,qscalar,tem4,          &
                              dualopt,wavelen,alpha)
        CALL rhvFromDualPolCG(nx,ny,nz,rhobar,tk,qscalar,tem5,          &
                              dualopt,wavelen,alpha)
      ELSE
        WRITE(*,'(1x,a)') 'ERROR: Unsupported rfopt option, rfopt = ', rfopt
        CALL arpsstop('ERROR: Unsupported rfopt option',1)
      END IF

      IF (lvldbg > 100) THEN
        CALL print3dnc_lg(100,'dual_ref',tem1,nx,ny,nz)
        CALL print3dnc_lg(200,'dual_rrf',tem2,nx,ny,nz)
        CALL print3dnc_lg(300,'dual_rhv',tem5,nx,ny,nz)
        CALL print3dnc_lg(400,'dual_zdr',tem3,nx,ny,nz)
        CALL print3dnc_lg(500,'dual_kdp',tem4,nx,ny,nz)
      END IF
    ELSE IF (mphyopt >= 8 .AND. mphyopt <= 12) THEN
      CALL reflec_MM(nx,ny,nz,rhobar,qscalar,tk,tem1)

    ! Lin,Schultz,Straka Lin,or WSM6 schemes
    ELSE IF (mphyopt >= 2 .AND. mphyopt <= 7) THEN
      CALL reflec_ferrier(nx,ny,nz, rhobar, qscalar, tk, tem1)
      !CALL print3dnc_lg(200,'tem1',tem1,nx,ny,nz)

    ! Warm rain schemes
    ELSE IF (mphyopt == 1) THEN
      IF(P_QR > 0) THEN
        CALL reflec_wr(nx,ny,nz, rhobar, qscalar(:,:,:,P_QR),tem1)
      ELSE
        tem1 = 0.0
      END IF

    ELSE IF (rfopt > 100 .AND. rfopt < 200) THEN
      wrfopt = MOD(rfopt,100)

      SELECT CASE (wrfopt)

      CASE (1,3,4)     ! Kessler, WSM 3-class
        print *,' Calling reflec_wrf ...'
        CALL reflec_wrf(nx,ny,nz,qv,qscalar(:,:,:,P_QR),                &
                        qscalar(:,:,:,P_QS),qscalar(:,:,:,P_QH),        &
                        0,tem3,tk,tem1)
      CASE (2,6,10,16) ! Lin, WSM 5- and 6- classes)
        print *,' Calling reflec_wrf ...'
        CALL reflec_wrf(nx,ny,nz,qv,qscalar(:,:,:,P_QR),                &
                        qscalar(:,:,:,P_QS),qscalar(:,:,:,P_QG),        &
                        1,tem3,tk,tem1)
                                             ! mixed-phase
      CASE (5)     ! Ferrier microphysics scheme (as in WRF_POST)
        print *,' Calling reflec_ferrier_wrf ...'
        tem5(:,:,:) = 2.0
        CALL reflec_ferrier_wrf(nx,ny,nz,qv,qscalar(:,:,:,P_QC),        &
                               qscalar(:,:,:,P_QR),qscalar(:,:,:,P_QS), &
                               tem3,tk,tem1,tem4,tem5)
      CASE (8) ! Thompson microphysics scheme (old version)
        print *,' Calling CALREF9s ...'
        CALL CALREF9s(nx,ny,nz,qscalar(:,:,:,P_QR),                     &
                      qscalar(:,:,:,P_QS),qscalar(:,:,:,P_QG),          &
                      tem3,tk,tem1)
      CASE (9,17)
        print *,' Calling reflec_wrf ...'
        tem4(:,:,:) = qscalar(:,:,:,P_QG)+qscalar(:,:,:,P_QH)
        CALL reflec_wrf(nx,ny,nz,qv,qscalar(:,:,:,P_QR),                &
                        qscalar(:,:,:,P_QS),tem4,                       &
                        1,tem3,tk,tem1) ! mixed-phase
      CASE DEFAULT
        WRITE(6,'(1x,a,I2,a)') 'ERROR: Unknown mphyopt = ',rfopt,'.'
        CALL arpsstop('ERROR: Unsupported mphyopt option',1)
      END SELECT

    ELSE
      WRITE(*,'(1x,a,I0,a)') 'Invalid microphysics option (',mphyopt,'), reflectivity set to zero.'
      tem1 = 0.0
    END IF
!
!-----------------------------------------------------------------------
!
!  Get radial velocity from 3-d velocity components
!
!-----------------------------------------------------------------------
!
    havdat(:,:) = .FALSE.
    bmwidth = 1.0

    knt=0
    kntref=0
    kntang=0
    kntrng=0
    IF (grdtlt_flag ==1 ) THEN !
      DO j=2,ny-2
      DO i=2,nx-2
        CALL xytoll(1,1,xs(i),ys(j),latradc,lonradc)
        CALL disthead(latradc,lonradc,latrad,lonrad,azim,dist)
        CALL ddrotuv(1,lonradc,azim,1.,ddrot,uazmrad,vazmrad)
        !!! compute vrsc(k)
        DO k=2,nz-1
          delz=zps(i,j,k) - elvrad
          CALL beamelv(delz,dist,elvang(k),rngsc)
          CALL dhdrange( elvang(k),rngsc,dhdr )
          dsdr=SQRT(AMAX1(0.,(1.-dhdr*dhdr)))

          vrsc(k)=(uazmrad*u(i,j,k) + vazmrad*v(i,j,k))      &
                 * dsdr + w(i,j,k)*dhdr
          !convert tem1 in dBZ to tem1 in mm**6/m**3
          tem1(i,j,k) = 10.0 ** (tem1(i,j,k)/10.0)
        END DO
        !!!
        DO itilt=1,ntilt
          CALL bmhgt_elvs(ang(itilt), dist, height)
          delz = height
          CALL beamelv(delz,dist,eleva,rngsc)
          CALL dhdrange(eleva,rngsc,dhdr)
          gridhgt(i,j,itilt)=height +elvrad
          IF (rngsc < rngmin .or. rngsc > rngmax) THEN
            kntrng=kntrng+1
            continue ! goto next tilt
          ELSE
            gridrange(i,j,itilt) = rngsc
          END IF
          !!! to determine the k3U, k3L
          !!!
          kmid = nz/2
          temAng = ang(itilt) - bmwidth/2
          CALL bmhgt_elvs(temAng, dist, height)
          height = height + elvrad
          IF(height < zps(i,j,kmid)) THEN
            DO k=kmid,2,-1
              IF(height > zps(i,j,k)) EXIT
            END DO
            k3L=k+1
          ELSE
            DO k=kmid,nz-1
              IF(height < zps(i,j,k)) EXIT
            END DO
            k3L=k-1
          END IF
          IF (k3L < 2) k3L = 2
          !--k3U
          temAng = ang(itilt) + bmwidth/2
          CALL bmhgt_elvs(temAng, dist, height)
          height = height + elvrad
          IF(height < zps(i,j,kmid) ) THEN
            DO k=kmid,2,-1
              IF(height > zps(i,j,k)) EXIT
            END DO
            k3U=k+1
          ELSE
            DO k=kmid,nz-1
              IF(height < zps(i,j,k)) EXIT
            END DO
            k3U=k-1
          END IF
          IF (k3U > nz-1) k3U = nz-1
          !
          ! to get model counterpart of Vr, Ref considering beam broadening
          !
          CALL vrzwtavg1D_3dvar(2,nz,k3L, k3U, zps(i,j,:), elvang, ang(itilt), tem1(i,j,:), bmwidth, rff)
          IF (rff > thref ) THEN
            CALL vrzwtavg1D_3dvar(1,nz,k3L, k3U, zps(i,j,:), elvang, ang(itilt), vrsc, bmwidth, vr)
            havdat(i,j) = .TRUE.
            knt = knt +1
            !
            ! Find precipitation terminal velocity
            ! For now a fixed freezing level is used, reversible with rmvterm
            !
            IF(1==0) THEN

              refz=10.**(0.1*rff)
              rhofact=EXP(0.4*height/h0)
              IF(height < zfrez) THEN
                vt=2.6*(refz**0.107)*rhofact
              ELSE IF(height < zice) THEN
                s1=(zice-height)*denom
                s2=2.*(height-zfrez)*denom
                vt=s1*2.6*(refz**0.107)*rhofact + s2
              ELSE
                vt=2.0
              END IF
            ELSE
              vt=0.0
            END IF ! 1==0

            griddata(i,j,itilt,1) = vr - vt*dhdr
            griddata(i,j,itilt,2) = rff
            IF (dualpout > 0) THEN
              CALL vrzwtavg1D_3dvar(2,nz,k3L, k3U, zps(i,j,:), elvang, ang(itilt), tem3(i,j,:), bmwidth, zdr)
              CALL vrzwtavg1D_3dvar(2,nz,k3L, k3U, zps(i,j,:), elvang, ang(itilt), tem4(i,j,:), bmwidth, kdp)
              CALL vrzwtavg1D_3dvar(2,nz,k3L, k3U, zps(i,j,:), elvang, ang(itilt), tem5(i,j,:), bmwidth, rhv)
              griddata(i,j,itilt,3) = rhv
              griddata(i,j,itilt,4) = zdr
              griddata(i,j,itilt,5) = kdp
            END IF
          ELSE
            griddata(i,j,itilt,2) = novalues
            IF (dualpout > 0) THEN
              griddata(i,j,itilt,3) = novalues
              griddata(i,j,itilt,4) = novalues
              griddata(i,j,itilt,5) = novalues
            END IF
            kntref=kntref+1
          END IF !IF (rff >thref )

        END DO !itilt=1,ntilt
!       IF (havdat(i,j) ==0 ) kntang=kntang + 1 !the counting for kntang may not be correct
        IF (havdat(i,j)) kntang=kntang + 1 !the counting for kntang may not be correct

      END DO !i=2,nx-2
      END DO!j=2,ny-2
    ELSE
      DO j=2,ny-2
      DO i=2,nx-2
        delx=xs(i)-radarx
        dely=ys(j)-radary
        dist=sqrt(delx*delx+dely*dely)
        DO k=2,nz-2
          delz=zps(i,j,k)-elvrad
          CALL beamelv(delz,dist,eleva,range)
          IF(range > rngmin .AND. range < rngmax ) THEN
            IF(eleva > elvmin .AND. eleva < elvmax ) THEN
              !angmatch=.false.
              !DO itilt=1,ntilt
              !  IF(ABS(eleva-ang(itilt)) < hlfwid) THEN
                  angmatch=.true.
              !    EXIT
              !  END IF
              !END DO

              IF( angmatch ) THEN
                IF(tem1(i,j,k) > thref) THEN
                  havdat(i,j) = .TRUE.
                  knt=knt+1
                  griddata(i,j,k,2)=tem1(i,j,k)
                  IF (dualpout > 0) THEN
                    griddata(i,j,k,3)=tem5(i,j,k)
                    griddata(i,j,k,4)=tem3(i,j,k)
                    griddata(i,j,k,5)=tem4(i,j,k)
                  END IF
!
! Find precipitation terminal velocity
! For now a fixed freezing level is used, reversible with rmvterm
!
                  refz=10.**(0.1*griddata(i,j,k,2))
                  rhofact=EXP(0.4*zps(i,j,k)/h0)
                  IF(zps(i,j,k) < zfrez) THEN
                    vt=2.6*(refz**0.107)*rhofact
                  ELSE IF(zps(i,j,k) < zice) THEN
                    s1=(zice-zps(i,j,k))*denom
                    s2=2.*(zps(i,j,k)-zfrez)*denom
                    vt=s1*2.6*(refz**0.107)*rhofact + s2
                  ELSE
                    vt=2.0
                  END IF
!
! Find local viewing angles
!
                  CALL dhdrange(eleva,range,dhdr)
                  dsdr=SQRT(AMAX1(0.,(1.-dhdr*dhdr)))
                  uazmrad=delx/dist
                  vazmrad=dely/dist
                  griddata(i,j,k,1)=u(i,j,k)*uazmrad*dsdr               &
                                   +v(i,j,k)*vazmrad*dsdr               &
                                   +(w(i,j,k)-vt)*dhdr
                ELSE
                  griddata(i,j,k,2) = novalues    ! observed but less then thref
                  kntref=kntref+1
                END IF
              ELSE
                kntang=kntang+1
              END IF

            ELSE
              kntang=kntang+1
            END IF
          ELSE
            kntrng=kntrng+1
          END IF
        END DO
      END DO
      END DO
    END IF !IF (grdtlt_flag ==1)
!
!-----------------------------------------------------------------------
!
!  Write the data counters.
!
!-----------------------------------------------------------------------
!
    knttry=(nx-3)*(ny-3)*(nz-3)
    perc  =100.*FLOAT(knt)/FLOAT(knttry)
    prcdst=100.*FLOAT(kntrng)/FLOAT(knttry)
    prcang=100.*FLOAT(kntang)/FLOAT(knttry)
    prcref=100.*FLOAT(kntref)/FLOAT(knttry)
!
    WRITE(6,'(a,i4,3a)')        ' Used VCP Number: ', vcpnum,' i.e. "',TRIM(vcptxt),'"'
    WRITE(6,'(a,i12)')          ' Total points:   ', knttry
    WRITE(6,'(a,i12,f6.1,a)')   ' Generated data: ', knt,perc,' %'
    WRITE(6,'(a,i12,f6.1,a)')   ' Range rejected: ', kntrng,prcdst,' %'
    WRITE(6,'(a,i12,f6.1,a)')   ' Angle samp rej: ', kntang,prcang,' %'
    WRITE(6,'(a,i12,f6.1,a/)')  ' Ref thresh rej: ', kntref,prcref,' %'
!
!-----------------------------------------------------------------------
!
!  Write the radar data out.
!
!-----------------------------------------------------------------------
!
    iradfmt=1
    WRITE(*,'(1x,a,I4.4,5(a,I2.2))') 'Data time = ',year,'-',month,'-',day,'_',hour,':',minute,':',second

    IF ( modelopt == 1 ) THEN
      !print*,'year=',year,month,day,hour,minute,second
      CALL ctim2abss(year,month,day,hour,minute,second, abstsec)

      abstsec=abstsec+curtim
      CALL abss2ctim( abstsec, iyr, imon, iday, ihr, imin, isec )
    ELSE
      !print*,'year=',year,month,day,hour,minute,second
      iyr=year;imon=month;iday=day;ihr=hour;imin=minute;isec=second
    END IF
    CALL ctim2abss(iyr, imon, iday, ihr, imin, isec, abstsec)

!   if (imin > 56) THEN
!      ihr = ihr+1
!      imin=0
!   END IF
!   print*,'iyr=',iyr, imon, iday, ihr, imin, isec

    myr=MOD(iyr,100)

!    IF ( modelopt == 1 ) THEN
!
!       IF (dmpfmt == 1) THEN
!         WRITE(rfname,'(a,a,i2.2,i2.2,i2.2,a,i2.2,i2.2)')               &
!             radid,'.',myr,imon,iday,'.',minute,minute
!!            radid,'.',myr,imon,iday,'.',ihr,imin
!       ELSE
!         WRITE(rfname,'(a,a,i2.2,i2.2,i2.2,a,i2.2,i2.2,a)')             &
!             radid,'.',myr,imon,iday,'.',minute,minute,'.hdf4'
!!            radid,'.',myr,imon,iday,'.',ihr,imin
!       END IF
!    ELSE IF ( modelopt == 2 ) THEN
!       WRITE(rfname,'(a,a,i2.2,i2.2,i2.2,a,i2.2,i2.2,a)')               &
!            radid,'.',myr,imon,iday,'.',ihr,minute,'.hdf4'
!!           radid,'.',myr,imon,iday,'.',ihr,imin,'.hdf4'
!    END IF

    IF (dmpfmt > 100) THEN
      grdtiltver = 0
      dmpfmt = mod(dmpfmt,100)
    ELSE
      grdtiltver = 1
    END IF

    IF (dmpfmt == 1) THEN
      file_ext = 'bin '
    ELSE IF (dmpfmt == 3) THEN
      file_ext = 'hdf4'
    ELSE IF (dmpfmt == 7) THEN
      file_ext = 'nc  '
    END IF

    WRITE(rfname,'(3a,3i2.2,a,2i2.2,2a)') TRIM(outdirname),             &
            radid,'.',myr,imon,iday,'.',ihr,minute,'.',TRIM(file_ext)

    PRINT *, ' Writing data into ',TRIM(rfname)

    IF (grdtlt_flag < 1 ) THEN  ! gridded radar format

      nxny=nx*ny
      ALLOCATE(icolp(nxny),stat=istatus)
      CALL check_alloc_status(istatus,'f88d2arps:icolp')
      ALLOCATE(jcolp(nxny),stat=istatus)
      CALL check_alloc_status(istatus,'f88d2arps:jcolp')
      ALLOCATE(xcolp(nxny),stat=istatus)
      CALL check_alloc_status(istatus,'f88d2arps:xcolp')
      ALLOCATE(ycolp(nxny),stat=istatus)
      CALL check_alloc_status(istatus,'f88d2arps:ycolp')
      ALLOCATE(zcolp(nz,nxny),stat=istatus)
      CALL check_alloc_status(istatus,'f88d2arps:zcolp')

      ALLOCATE(colref(nz,nxny),stat=istatus)
      CALL check_alloc_status(istatus,'f88d2arps:colref')
      ALLOCATE(colvel(nz,nxny),stat=istatus)
      CALL check_alloc_status(istatus,'f88d2arps:colvel')
      ALLOCATE(colnyq(nz,nxny),stat=istatus)
      CALL check_alloc_status(istatus,'f88d2arps:colnyq')
      ALLOCATE(coltim(nz,nxny),stat=istatus)
      CALL check_alloc_status(istatus,'f88d2arps:coltim')
      !ALLOCATE(colhgt(nz,nxny),stat=istatus)
      !CALL check_alloc_status(istatus,'f88d2arps:colhgt')

      colref(:,:) = refmis
      colvel(:,:) = velmis
      colnyq(:,:) = rmissing
      coltim(:,:) = rmissing
      !colhgt(:,:) = -999.0

      IF (dualpout > 0) THEN
        ALLOCATE(colrhv(nz,nxny),stat=istatus)
        CALL check_alloc_status(istatus,'f88d2arps:colrhv')
        ALLOCATE(colzdr(nz,nxny),stat=istatus)
        CALL check_alloc_status(istatus,'f88d2arps:colzdr')
        ALLOCATE(colkdp(nz,nxny),stat=istatus)
        CALL check_alloc_status(istatus,'f88d2arps:colkdp')
      ELSE
        ALLOCATE(colrhv(1,1),  stat=istatus)
        ALLOCATE(colzdr(1,1),  stat=istatus)
        ALLOCATE(colkdp(1,1),  stat=istatus)
      END IF
      colrhv(:,:) = -999.0
      colzdr(:,:) = -999.0
      colkdp(:,:) = -999.0

      idat=0
      ioffset=0
      joffset=0

      DO j=2,ny-2
        DO i=2,nx-2
          IF(havdat(i,j)) THEN
            idat=idat+1
            icolp(idat)=i+ioffset
            jcolp(idat)=j+joffset
            xcolp(idat)=xs(i)
            ycolp(idat)=ys(j)
            DO k=1,nz
              zcolp(k,idat)=zps(i,j,k)
              colref(k,idat) = griddata(i,j,k,2)
              colvel(k,idat) = griddata(i,j,k,1)
              colnyq(k,idat) = 100.0
              coltim(k,idat) = 0.0
              !colhgt(k,idat) = gridhgt(i,j,k)
            END DO
          END IF
        END DO
      END DO

      ncoltot = idat

      IF (dualpout>0) THEN
        idat = 0
        DO j=2,ny-2
          DO i=2,nx-2
            IF(havdat(i,j)) THEN
              idat=idat+1
              DO k=1,nz
                colrhv(k,idat) = griddata(i,j,k,3)
                colzdr(k,idat) = griddata(i,j,k,4)
                colkdp(k,idat) = griddata(i,j,k,5)
              END DO
            END IF
          END DO
        END DO

      END IF

      IF (lvldbg > 10) THEN
        CALL print3dnc_lg(100,'grid_vel',griddata(:,:,:,1),nx,ny,nz)
        CALL print3dnc_lg(200,'grid_ref',griddata(:,:,:,2),nx,ny,nz)
        IF (dualpout>0) THEN
          CALL print3dnc_lg(300,'grid_rhv',griddata(:,:,:,3),nx,ny,nz)
          CALL print3dnc_lg(400,'grid_zdr',griddata(:,:,:,4),nx,ny,nz)
          CALL print3dnc_lg(500,'grid_kdp',griddata(:,:,:,5),nx,ny,nz)
        END IF
      END IF

      isource = 1

      CALL wrtradcol(nx,ny,nxny,nz,ncoltot,ncoltot,                     &
                  dmpfmt,iradfmt,hdf4cmpr,dmpzero,dualpout,             &
                  rfname,radid,latrad,lonrad,elvrad,                    &
                  iyr,imon,iday,ihr,imin,isec,vcpnum,isource,           &
                  refelvmin,refelvmax,rngmin,rngmax,                    &
                  icolp,jcolp,xcolp,ycolp,zcolp,                        &
                  colref,colvel,colnyq,coltim,colrhv,colzdr,colkdp,     &
                  istatus)
    ELSE  ! grid-tilted radar format

      dazim = 0.5

      CALL wrtgridtilt(rfname,dmpfmt,0,ntilt,nx,ny,nz,ntilt,radid,      &
              latrad,lonrad,radarx,radary,elvrad,dazim,rngmin,rngmax,   &
              gridhgt,gridrange,tem1,tem2,                              &
              griddata,outtim,tem3,abstsec,outelev,                     &
              gridhgt,gridrange,tem1,tem2,                              &
              outelev,xs,ys,zps,vcpnum,isource,grdtiltver,radnumvar)
      !ELSE
      !  CALL getunit(iunit)
      !  OPEN(iunit,FILE=TRIM(rfname),STATUS='UNKNOWN',FORM='UNFORMATTED')
      !  WRITE(iunit) radid
      !  WRITE(iunit) ireftim,abstsec,vcpnum,isource,idummy,               &
      !           idummy,idummy,idummy,idummy,idummy
      !  runname = 'pseudo_grdtlt'
      !  WRITE(iunit) runname
      !  WRITE(iunit) iradfmt,strhopt,mapproj, rngmin, rngmax,             &
      !                  2   ,ncoltot  , ntilt,  idummy,idummy
      !              !typelev,ncoltot  , ntilt,  idummy,idummy
      !  WRITE(iunit) dx,dy,dz,dzmin,ctrlat,                               &
      !               ctrlon,trulat1,trulat2,trulon,sclfct,                &
      !               latrad,lonrad,elvrad,ang(1), ang(ntilt)
      !  WRITE(iunit) nradvr,iradvr
      !  WRITE(iunit) -999.0, -999.0, -999.0, -999.0 !xmin,xmax,ymin,ymax
      !
      !  WRITE(iunit) iyr,imon,iday,ihr,imin,isec
      !  WRITE(iunit) radarx,radary,0.5 !dazim
      !  WRITE(iunit) outelev
      !  WRITE(iunit) outtim
      !
      !  DO idat=1, ncoltot
      !    CALL xytoll(1,1,xs(icolp(idat)),ys(jcolp(idat)),latradc,lonradc)
      !
      !    elevc = 0.5 *(zps(icolp(idat),jcolp(idat),1) + zps(icolp(idat),jcolp(idat),2))
      !    WRITE(iunit) icolp(idat),jcolp(idat),xcolp(idat),ycolp(idat),   &
      !              latradc, lonradc, elevc, ntilt
      !    WRITE(iunit) (colhgt(k,idat),k=1,ntilt)
      !    WRITE(iunit) (colhgt(k,idat),k=1,ntilt) !(colrng(k,idat),k=1,ntilt)
      !    WRITE(iunit) (colvel(k,idat),k=1,ntilt)
      !    WRITE(iunit) (colref(k,idat),k=1,ntilt)
      !  END DO
      !
      !  CLOSE(iunit)
      !END IF
    END IF

  END IF                                       ! successful read

  DEALLOCATE(x,y,z,vrsc,elvang,zp,zpsoil)
  DEALLOCATE(u,v,w,qv,qscalar,tke,kmh,kmv,rhobar)
  DEALLOCATE(soiltyp,stypfrct,vegtyp,lai,roufns,veg,tsoil,qsoil,wetcanp,snowdpth)
  DEALLOCATE(raing,rainc,prcrate,radfrc,radsw,rnflx,radswnet,radlwin,usflx,vsflx,ptsflx,qvsflx)
  DEALLOCATE(tk,tem1,tem2,tem3,lat,lon)
  DEALLOCATE(xs,ys,zps,havdat)

  DEALLOCATE(griddata)
  IF (ALLOCATED(gridhgt)) DEALLOCATE(gridhgt,gridrange)

  GOTO 110

  100 CONTINUE

  Write(6,'(1x,a)')'Namelist read end encountered. Program stopped.'

  110 CONTINUE

  STOP
END PROGRAM model2rad
