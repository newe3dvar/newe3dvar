!#######################################################################
!#######################################################################
!#########                                                      ########
!#########                                                      ########
!#########          Simplified Dualpol Operator                 ########
!#########                                                      ########
!#########                     Developed by                     ########
!#########       National Severe Storm Laboratory, NOAA         ########
!#########                                                      ########
!#######################################################################
!#######################################################################

SUBROUTINE DualPolObs(nx,ny,nz,rho,t,qscalar,rff,ref_h,                 &
                      Zdr,kdph,rhv,istatus)

  USE module_DualFO

!-----------------------------------------------------------------------
! Force explicit declarations.
!-----------------------------------------------------------------------

  IMPLICIT NONE

!-----------------------------------------------------------------------
! Include files.
!-----------------------------------------------------------------------

  !INCLUDE 'microPara.inc'
  INCLUDE 'globcst.inc'

!-----------------------------------------------------------------------
! Declare arguments.
!-----------------------------------------------------------------------

  INTEGER, INTENT(IN) :: nx,ny,nz      ! Dimensions of grid

  REAL,    INTENT(IN) :: rho(nx,ny,nz) ! Air density (kg m**-3)
  REAL,    INTENT(IN) :: t(nx,ny,nz)   ! air tempreature (K)
  REAL                :: qscalar(nx,ny,nz,nscalar)

  REAL,    INTENT(OUT) :: rff(nx,ny,nz)   ! Reflectivity (dBZ)
  REAL,    INTENT(OUT) :: ref_h(nx,ny,nz) ! Reflectivity (mm**6/m**3)

  REAL,    INTENT(OUT) :: kdph(nx,ny,nz)  ! Specific differential phase
  REAL,    INTENT(OUT) :: Zdr(nx,ny,nz)   ! Differential reflectivity
  REAL,    INTENT(OUT) :: rhv(nx,ny,nz)   ! cross-correlation coefficient

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
! Declare local variables.
!-----------------------------------------------------------------------

  INTEGER :: i,j,k

  TYPE(T_obs_dualp) :: obs_dualp
  TYPE(T_para_qNt)  :: var_qNt

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  CALL init_para(istatus)

  !SELECT CASE (mphyopt)
  !CASE(1:4)
  !  grpl_ON = 0
  !  hl_ON   = 1
  !CASE(5:7,106,108,110,116)
  !  grpl_ON = 1
  !  hl_ON   = 0
  !CASE DEFAULT
  !  WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported mphyopt = ',mphyopt,'.'
  !  istatus = -1
  !  RETURN
  !END SELECT

  IF (P_QG > 0) THEN
    grpl_ON = 1
  ELSE
    grpl_ON = 0
  END IF

  IF (P_QH > 0) THEN
    hl_ON = 1
  ELSE
    hl_ON = 0
  END IF

  WRITE(*,'(1x,3(a,I0),a)') "mphyopt = ", mphyopt, ", grpl_ON = ", grpl_ON, ", hl_ON = ",hl_ON, '.'

  rff = 0.0
  ref_h = 0.0

!-----------------------------------------------------------------------
! Now loop through the scalar grid points.
!-----------------------------------------------------------------------


  !print *, 'P_QG = ',P_QG, P_QH, P_QC, P_QR, P_QI, P_QS
  !CALL print3dnc_lg(100,'qg',qscalar(:,:,:,P_QG),nx,ny,nz)
  !CALL print3dnc_lg(200,'qh',qscalar(:,:,:,P_QH),nx,ny,nz)
  !CALL print3dnc_lg(300,'qc',qscalar(:,:,:,P_QC),nx,ny,nz)
  !CALL print3dnc_lg(400,'qr',qscalar(:,:,:,P_QR),nx,ny,nz)
  !CALL print3dnc_lg(500,'qi',qscalar(:,:,:,P_QI),nx,ny,nz)
  !CALL print3dnc_lg(600,'qs',qscalar(:,:,:,P_QS),nx,ny,nz)
  !
  !print *, 'P_QG = ',P_NG, P_NH, P_NC, P_NR, P_NI, P_NS
  !CALL print3dnc_lg(100,'qng',qscalar(:,:,:,P_NG),nx,ny,nz)
  !CALL print3dnc_lg(200,'qnh',qscalar(:,:,:,P_NH),nx,ny,nz)
  !!CALL print3dnc_lg(300,'qnc',qscalar(:,:,:,P_NC),nx,ny,nz)
  !CALL print3dnc_lg(400,'qnr',qscalar(:,:,:,P_NR),nx,ny,nz)
  !CALL print3dnc_lg(500,'qni',qscalar(:,:,:,P_NI),nx,ny,nz)
  !CALL print3dnc_lg(600,'qns',qscalar(:,:,:,P_NS),nx,ny,nz)

  DO k = 1,nz-1
    DO j = 1,ny-1
      DO i = 1,nx-1

  !DO k = 17,17
  !  DO j = 28,28
  !    DO i = 31,31

          !print*,'P_QC=',P_QC,P_QI,P_QR,P_QS,P_QH,P_QG,P_NC,P_NI, &
          !               P_NR,P_NS,P_NH,P_NG, rho(i,j,k)
          !print*,'scalar==',qscalar(i,j,k,P_QC),qscalar(i,j,k,P_QI), &
          !    qscalar(i,j,k,P_QR),qscalar(i,j,k,P_QS), &
          !    qscalar(i,j,k,P_QH),qscalar(i,j,k,P_QG), &
          !    qscalar(i,j,k,P_NC),qscalar(i,j,k,P_NI), &
          !    qscalar(i,j,k,P_NR),qscalar(i,j,k,P_NS), &
          !    qscalar(i,j,k,P_NH),qscalar(i,j,k,P_NG)


           !qscalar(i,j,k,P_QC) = 9.0622534e-03
           !qscalar(i,j,k,P_QI) = 9.0622534e-03
           !qscalar(i,j,k,P_QR) =69.0622534e-03
           !qscalar(i,j,k,P_QS) = 9.0622534e-03
           !qscalar(i,j,k,P_QH) =69.0622534e-03
           !qscalar(i,j,k,P_QG) = 9.0622534e-03


           !qscalar(i,j,k,P_NS) = 1.0e5
           !qscalar(i,j,k,P_NI) = 1.0e5
           !qscalar(i,j,k,P_NC) = 1.0e5
           !qscalar(i,j,k,P_NR) = 1.0e5
           !qscalar(i,j,k,P_NS) = 1.0e5
           !qscalar(i,j,k,P_NH) = 1.0e5
           !qscalar(i,j,k,P_NG) = 1.0e5

           !qscalar(i,j,k,P_NS) = 1000.0
           !qscalar(i,j,k,P_NI) = 1000.0
           !qscalar(i,j,k,P_NC) = 1000.0
           !qscalar(i,j,k,P_NR) = 1000.0
           !qscalar(i,j,k,P_NS) = 1000.0
           !qscalar(i,j,k,P_NH) = 1000.0
           !qscalar(i,j,k,P_NG) = 1000.0

           obs_dualp = initRef()
           var_qNt = init_q_nt()

!write(0,*) i,j,k, t(i,j,k),rho(i,j,k),qscalar(i,j,k,:)
           CALL rff_sub(t(i,j,k),rho(i,j,k),qscalar(i,j,k,:),           &
                        obs_dualp,var_qNt,istatus)
           IF (istatus /= 0) RETURN

           !IF (obs_dualp%T_log_ref > 0) print *, i,j,k, obs_dualp%T_log_ref
           rff(i,j,k)   = obs_dualp%T_log_ref
           ref_h(i,j,k) = obs_dualp%T_sum_ref_h

           Zdr(i,j,k)  = obs_dualp%T_log_zdr
           kdph(i,j,k) = obs_dualp%T_kdp
           rhv(i,j,k)  = obs_dualp%T_sum_ref_hv
      END DO ! DO i
    END DO ! DO j
  END DO ! DO  k
  !print*,'DmcMAX==',DmcX
  !print*,'DmiMAX==',DmiX
  !print*,'DmrMAX==',DmrX
  !print*,' '
  !print*,'DmsdMAX=',DmsdX
  !print*,'DmgdMAX=',DmgdX
  !print*,'DmhdMAX=',DmhdX
  !print*,' '
  !print*,'DmswMAX=',DmswX
  !print*,'DmgwMAX=',DmgwX
  !print*,'DmhwMAX=',DmhwX
  !print*,' '
  !print*,'QrMX====',QrMX
  !print*,'QsdMX===',QsdMX
  !print*,'QswMX===',QswMX
  !print*,'QhdMX===',QhdMX
  !print*,'QhwMX===',QhwMX
  !print*,'QgdMX===',QgdMX
  !print*,'QgwMX===',QgwMX

  RETURN
END SUBROUTINE DualPolObs


!#######################################################################
!#######################################################################
!#########                                                      ########
!#########               SUBROUTINE rff_sub                     ########
!#########                                                      ########
!#########                   Developed by                       ########
!#########       National Severe Storm Laboratory, NOAA         ########
!#########                                                      ########
!#######################################################################
!#######################################################################

SUBROUTINE rff_sub(ta,rho,qscalar,obs_dualp,var_qNt,istatus)

  USE module_DualFO

!-----------------------------------------------------------------------
! Force explicit declarations.
!-----------------------------------------------------------------------

  IMPLICIT NONE

!-----------------------------------------------------------------------
! Include files.
!-----------------------------------------------------------------------

  !INCLUDE 'microPara.inc'
  INCLUDE 'globcst.inc'

!-----------------------------------------------------------------------
! Declare arguments.
!-----------------------------------------------------------------------
  REAL :: qscalar(nscalar)
  REAL :: ta,rho

  !INTEGER :: dualpol

  TYPE(T_obs_dualp) :: obs_dualp
  TYPE(T_para_qNt) :: var_qNt

  INTEGER, INTENT(OUT) :: istatus
  !local variables
  REAL :: no_value = missing

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  SELECT CASE (mphyopt)
  CASE(2:8,102,106)  ! single moment schemes
    IF(grpl_ON == 0 .and. hl_ON == 0) THEN
      var_qNt = assign_para_qNt(qscalar(P_QC),qscalar(P_QI),qscalar(P_QR), &
                 qscalar(P_QS), no_value, no_value,                        &
                 no_value, no_value, no_value,                             &
                 no_value, no_value, no_value)
    ELSE IF(grpl_ON == 0 .and. hl_ON == 1) THEN
      var_qNt = assign_para_qNt(qscalar(P_QC),qscalar(P_QI),qscalar(P_QR), &
                 qscalar(P_QS), qscalar(P_QH),no_value,                    &
                 no_value,no_value,no_value,                               &
                 no_value,no_value,no_value)
    ELSE IF(grpl_ON == 1 .and. hl_ON == 0) THEN
      var_qNt = assign_para_qNt(qscalar(P_QC),qscalar(P_QI),qscalar(P_QR), &
                 qscalar(P_QS),no_value,qscalar(P_QG),                     &
                 no_value,no_value,no_value,                               &
                 no_value,no_value,no_value)
    ELSE IF(grpl_ON == 1 .and. hl_ON == 1) THEN
      var_qNt = assign_para_qNt(qscalar(P_QC),qscalar(P_QI),qscalar(P_QR), &
                 qscalar(P_QS),qscalar(P_QH),qscalar(P_QG),                &
                 no_value,no_value,no_value,                               &
                 no_value,no_value,no_value)
    ENDIF
  CASE(9:14,109:110) !double moment schemes
    IF(grpl_ON == 0 .and. hl_ON == 0) THEN
      var_qNt = assign_para_qNt(qscalar(P_QC),qscalar(P_QI),qscalar(P_QR), &
                 qscalar(P_QS),no_value,no_value,                          &
                 qscalar(P_NC),qscalar(P_NI),qscalar(P_NR),                &
                 qscalar(P_NS), no_value,no_value)
    ELSE IF(grpl_ON == 0 .and. hl_ON == 1) THEN
      var_qNt = assign_para_qNt(qscalar(P_QC),qscalar(P_QI),qscalar(P_QR), &
                 qscalar(P_QS),qscalar(P_QH),no_value,                     &
                 qscalar(P_NC),qscalar(P_NI),qscalar(P_NR),                &
                 qscalar(P_NS),qscalar(P_NH),no_value)
    ELSE IF(grpl_ON == 1 .and. hl_ON == 0) THEN
      var_qNt = assign_para_qNt(qscalar(P_QC),qscalar(P_QI),qscalar(P_QR), &
                 qscalar(P_QS), no_value,qscalar(P_QG),                    &
                 qscalar(P_NC), qscalar(P_NI),qscalar(P_NR),               &
                 qscalar(P_NS), no_value,qscalar(P_NG))
    ELSE IF(grpl_ON == 1 .and. hl_ON == 1) THEN
      var_qNt = assign_para_qNt(qscalar(P_QC),qscalar(P_QI),qscalar(P_QR), &
                 qscalar(P_QS),qscalar(P_QH),qscalar(P_QG),                &
                 qscalar(P_NC),qscalar(P_NI),qscalar(P_NR),                &
                 qscalar(P_NS),qscalar(P_NH),qscalar(P_NG))
    ENDIF
  CASE (108)   ! no QNCLOUD
      var_qNt = assign_para_qNt(qscalar(P_QC),qscalar(P_QI),qscalar(P_QR), &
                 qscalar(P_QS),no_value,qscalar(P_QG),                     &
                 no_value,     qscalar(P_NI),qscalar(P_NR),                &
                 no_value,no_value,no_value)

  CASE(116,118) ! double moment for rain only
    IF(grpl_ON == 0 .and. hl_ON == 0) THEN
      var_qNt = assign_para_qNt(qscalar(P_QC),qscalar(P_QI),qscalar(P_QR), &
                 qscalar(P_QS),no_value,no_value,                          &
                 qscalar(P_NC),no_value,qscalar(P_NR),                     &
                 no_value,no_value,no_value)
    ELSE IF(grpl_ON == 0 .and. hl_ON == 1) THEN
      var_qNt = assign_para_qNt(qscalar(P_QC),qscalar(P_QI),qscalar(P_QR), &
                 qscalar(P_QS),qscalar(P_QH),no_value,                     &
                 qscalar(P_NC),no_value,qscalar(P_NR),                     &
                 no_value,no_value,no_value)
    ELSE IF(grpl_ON == 1 .and. hl_ON == 0) THEN
      var_qNt = assign_para_qNt(qscalar(P_QC),qscalar(P_QI),qscalar(P_QR), &
                 qscalar(P_QS),no_value,qscalar(P_QG),                     &
                 qscalar(P_NC),no_value,qscalar(P_NR),                     &
                 no_value,no_value,no_value)
    ELSE IF(grpl_ON == 1 .and. hl_ON == 1) THEN
      var_qNt = assign_para_qNt(qscalar(P_QC),qscalar(P_QI),qscalar(P_QR), &
                 qscalar(P_QS), qscalar(P_QH),qscalar(P_QG),               &
                 qscalar(P_NC),no_value,qscalar(P_NR),                     &
                 no_value,no_value,no_value)
    ENDIF
  CASE DEFAULT
    WRITE(0,*) 'ERROR: unsuported mphyopt = ',mphyopt
    istatus = -1
    RETURN
  END SELECT

  !dualpol_opt = 6 ! dualpol
  !WRITE(*,'(1x,a)') 'Calling refl_new_g .....'
  CALL refl_new_g(ta,rho,var_qNt,obs_dualp)

  RETURN

END SUBROUTINE rff_sub
