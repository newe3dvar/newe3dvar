MODULE a2s_subs

 USE wrfio_api, only: wrf_get_dimensions
 IMPLICIT NONE

!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'
  INCLUDE 'indtflg.inc'
  INCLUDE 'phycst.inc'
  INCLUDE 'mp.inc'
!
!-----------------------------------------------------------------------
!
! Variable Delaration
!
!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!
!  Arrays to be read in:
!
!-----------------------------------------------------------------------
!

  REAL, ALLOCATABLE  :: x     (:)         ! The x-coord. of the physical and
                                          ! computational grid. Defined at
                                          ! u-point.
  REAL, ALLOCATABLE  :: y     (:)         ! The y-coord. of the physical and
                                          ! computational grid. Defined at
                                          ! v-point.
  REAL, ALLOCATABLE  :: z     (:)         ! The z-coord. of the computational grid.
                                          ! Defined at w-point on the staggered
                                          ! grid.
  REAL, ALLOCATABLE  :: zp    (:,:,:)     ! The physical height coordinate defined at
                                          ! w-point of the staggered grid.
  REAL, ALLOCATABLE  :: zpsoil(:,:,:)     ! Soil level depth.
  REAL, ALLOCATABLE  :: mapfct(:,:,:)     ! Map factors at scalar, u and v points
  REAL, ALLOCATABLE  :: j1    (:,:,:)     ! Coordinate transformation Jacobian defined
                                          ! as - d( zp )/d( x )
  REAL, ALLOCATABLE  :: j2    (:,:,:)     ! Coordinate transformation Jacobian defined
                                          ! as - d( zp )/d( y )
  REAL, ALLOCATABLE  :: j3    (:,:,:)     ! Coordinate transformation Jacobian defined
                                          ! as d( zp )/d( z )
  REAL, ALLOCATABLE  :: j3soil(:,:,:)     ! Coordinate transformation Jacobian defined
                                          ! as d( zp )/d( z )
  REAL, ALLOCATABLE  :: aj3x  (:,:,:)     ! Coordinate transformation Jacobian defined
                                          ! as d( zp )/d( z ) AVERAGED IN THE
                                          ! X-DIR.
  REAL, ALLOCATABLE  :: aj3y  (:,:,:)     ! Coordinate transformation Jacobian defined
                                          ! as d( zp )/d( z ) AVERAGED IN THE
                                          ! Y-DIR.
  REAL, ALLOCATABLE  :: aj3z  (:,:,:)     ! Coordinate transformation Jacobian defined
                                          ! as d( zp )/d( z ) AVERAGED IN THE
                                          ! Z-DIR.
  REAL, ALLOCATABLE  :: j3inv (:,:,:)     ! Inverse of j3
  REAL, ALLOCATABLE  :: j3soilinv(:,:,:)  ! Inverse of J3soil.

  REAL, ALLOCATABLE  :: u     (:,:,:)     ! Total u-velocity (m/s)
  REAL, ALLOCATABLE  :: v     (:,:,:)     ! Total v-velocity (m/s)
  REAL, ALLOCATABLE  :: w     (:,:,:)     ! Total w-velocity (m/s)

  REAL, ALLOCATABLE  :: qscalar(:,:,:,:)

  REAL, ALLOCATABLE  :: tke   (:,:,:)     ! Turbulent kinetic energy
  REAL, ALLOCATABLE  :: kmh   (:,:,:)     ! The turbulent mixing coefficient for
                                          ! momentum. ( m**2/s ) horizontal
  REAL, ALLOCATABLE  :: kmv   (:,:,:)     ! The turbulent mixing coefficient for
                                          ! momentum. ( m**2/s ) vertical

  REAL, ALLOCATABLE  :: ubar  (:,:,:)     ! Base state u-velocity (m/s)
  REAL, ALLOCATABLE  :: vbar  (:,:,:)     ! Base state v-velocity (m/s)
  REAL, ALLOCATABLE  :: wbar  (:,:,:)     ! Base state w-velocity (m/s)
  REAL, ALLOCATABLE  :: ptbar (:,:,:)     ! Base state potential temperature (K)
  REAL, ALLOCATABLE  :: rhobar(:,:,:)     ! Base state air density (kg/m**3)
  REAL, ALLOCATABLE  :: pbar  (:,:,:)     ! Base state pressure (Pascal)
  REAL, ALLOCATABLE  :: phb   (:,:,:)     ! Base state geopotential height ( m**2/s**2 )
  REAL, ALLOCATABLE  :: qvbar (:,:,:)     ! Base state water vapor specifichumidity
                                          ! (kg/kg)

  INTEGER, ALLOCATABLE  :: soiltyp(:,:,:)   ! Soil type
  INTEGER, ALLOCATABLE  :: vegtyp(:,:)    ! Vegetation type
  REAL, ALLOCATABLE  :: stypfrct(:,:,:)     ! Soil type fraction
  REAL, ALLOCATABLE  :: lai    (:,:)      ! Leaf Area Index
  REAL, ALLOCATABLE  :: roufns (:,:)      ! Surface roughness
  REAL, ALLOCATABLE  :: veg    (:,:)      ! Vegetation fraction
  REAL, ALLOCATABLE  :: hterain(:,:)      ! The height of the terrain

  REAL, ALLOCATABLE :: tsoil  (:,:,:,:)   ! soil temperature (K)
  REAL, ALLOCATABLE :: qsoil  (:,:,:,:)   ! soil moisture (g/kg)

  REAL, ALLOCATABLE  :: wetcanp(:,:)       ! Canopy water amount
  REAL, ALLOCATABLE  :: snowdpth(:,:)      ! Snow depth (m)

  REAL, ALLOCATABLE  :: raing  (:,:)       ! Grid supersaturation rain
  REAL, ALLOCATABLE  :: rainc  (:,:)       ! Cumulus convective rain
  REAL, ALLOCATABLE  :: prcrate(:,:,:)     ! precipitation rate (kg/(m**2*s))
                                           ! prcrate(1,1,1) = total precip. rate
                                           ! prcrate(1,1,2) = grid scale precip.
                                           ! rate
                                           ! prcrate(1,1,3) = cumulus precip.
                                           ! rate
                                           ! prcrate(1,1,4) = microphysics
                                           ! precip. rate

  REAL, ALLOCATABLE  :: radfrc(:,:,:)      ! Radiation forcing (K/s)
  REAL, ALLOCATABLE  :: radsw (:,:)        ! Solar radiation reaching the surface
  REAL, ALLOCATABLE  :: rnflx (:,:)        ! Net radiation flux absorbed by surface
  REAL, ALLOCATABLE :: radswnet (:,:) ! Net solar radiation, SWin - SWout
  REAL, ALLOCATABLE :: radlwin  (:,:) ! Incoming longwave radiation

  REAL, ALLOCATABLE  :: usflx (:,:)        ! Surface flux of u-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE  :: vsflx (:,:)        ! Surface flux of v-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE  :: ptsflx(:,:)        ! Surface heat flux (K*kg/(m*s**2))
  REAL, ALLOCATABLE  :: qvsflx(:,:)        ! Surface moisture flux (kg/(m**2*s))

  REAL, ALLOCATABLE  :: uprt  (:,:,:)     ! Perturbation u-velocity (m/s)
  REAL, ALLOCATABLE  :: vprt  (:,:,:)     ! Perturbation v-velocity (m/s)
  REAL, ALLOCATABLE  :: wprt  (:,:,:)     ! Perturbation w-velocity (m/s)
  REAL, ALLOCATABLE  :: ptprt (:,:,:)     ! Perturbation potential temperature (K)
  REAL, ALLOCATABLE  :: pprt  (:,:,:)     ! Perturbation pressure (Pascal)
  REAL, ALLOCATABLE  :: phprt (:,:,:)     ! Geopotential Height (m**2/s**2)
  REAL, ALLOCATABLE  :: rhoprt(:,:,:)     ! Perturbation air density (kg/m**3)
  REAL, ALLOCATABLE  :: qvprt (:,:,:)     ! Perturbation water vapor specific
                                          ! humidity (kg/kg)
!
!-----------------------------------------------------------------------
!
!  Other data variables
!
!-----------------------------------------------------------------------
!
  REAL :: time
  REAL, ALLOCATABLE  :: tk (:,:,:)     ! Temperature (K)
  REAL, ALLOCATABLE  :: xs(:),ys(:),zs(:,:,:)
                       !x y z location of scalar pts
!
!-----------------------------------------------------------------------
!
!  WRF variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: numdigits      ! number of patch digits in WRF file names
  REAL :: p_top
  REAL,    ALLOCATABLE :: znw(:), znu(:), dnw(:)
  REAL,    ALLOCATABLE :: mu(:,:), mub(:,:), wmixr(:,:,:)
  REAL,    ALLOCATABLE :: psfc(:,:), varsfc(:,:,:)
!
!-----------------------------------------------------------------------
!
!  Temporary working arrays:
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE  :: tem1(:,:,:)
  REAL, ALLOCATABLE  :: tem2(:,:,:)
  REAL, ALLOCATABLE  :: tem3(:,:,:)
  REAL, ALLOCATABLE  :: rh(:,:,:),qv(:,:,:)
!
!-----------------------------------------------------------------------
!
!  ARPS dimensions
!
!-----------------------------------------------------------------------
!
  INTEGER :: nx       ! Number of grid points in the x-direction
  INTEGER :: ny       ! Number of grid points in the y-direction
  INTEGER :: nz       ! Number of grid points in the z-direction
  INTEGER :: nzsoil   ! Number of soil levels
  INTEGER :: nstyps   ! Number of soil types
!
!-----------------------------------------------------------------------
!
!  Scalar grid location variables
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE  :: lat(:,:),lon(:,:)
!
!-----------------------------------------------------------------------
!
!   Satellite variables
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: cld_base(:,:),cld_top(:,:),ptot(:,:,:)
  INTEGER, ALLOCATABLE :: cld_base_grided(:,:),cld_top_grided(:,:)
  REAL, ALLOCATABLE :: cwp(:,:), hydro_ref(:,:), iwp(:,:), tpw(:,:)
  REAL, ALLOCATABLE :: level300(:,:)
  INTEGER, ALLOCATABLE :: tpwflag(:,:)
!
!-----------------------------------------------------------------------
!
!   Output variables
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE, DIMENSION(:) :: cwpout,xsout,ysout,tpwout
  REAL, ALLOCATABLE, DIMENSION(:) :: cldbshgt,cldtphgt
  INTEGER, ALLOCATABLE, DIMENSION(:) :: phsout,tpwfout
  CHARACTER(LEN=128)            :: cwpoutfile,tpwoutfile
!
!-----------------------------------------------------------------------
!
!  User request stuff
!
!-----------------------------------------------------------------------
!
  INTEGER :: hinfmt
  CHARACTER (LEN=256) :: filename, grdbasfn
! INTEGER :: gen_cwp, gen_tpw
  INTEGER :: ll_or_xy
  INTEGER :: dmpfmt, hdf4cmpr

  INTEGER :: lenstr      ! Length of a string
  LOGICAL :: iexist      ! Flag set by inquire statement for file
                         ! existence
!
!-----------------------------------------------------------------------
!
!  Misc internal variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: istatus, unum, ireturn, i, j, k
  INTEGER :: lengbf, lenfil, nchin, nqw
  REAL :: gdx,gdy,xctr,yctr,x0sc,y0sc,satx,saty,latsat,lonsat
  CHARACTER(LEN=40) :: qnames_wrf(40)
  REAL :: latnot(2)
! REAL, ALLOCATABLE  :: comprf(:,:)
! REAL          :: thresh_ref

 CONTAINS

  SUBROUTINE namelist_read

  CHARACTER (LEN=256) :: namelist_filename, nlfile

!-----------------------------------------------------------------------
!
! NAMELIST DECLARATION
!
!-----------------------------------------------------------------------

  NAMELIST /file_name/ modelopt,hinfmt,filename,grdbasfn
  NAMELIST /sat_info/ ll_or_xy,latsat,lonsat,satx,saty,dmpfmt,hdf4cmpr
  NAMELIST /wrf_info/ year,month,day,hour,minute,second,dx,dy,dz,       &
                      strhopt,zrefsfc,dlayer1,dlayer2,strhtune,         &
                      zflat,ctrlat,ctrlon,crdorgnopt
  NAMELIST /others/ dzmin

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

   unum = 0
   CALL GET_COMMAND_ARGUMENT(1, nlfile, lenstr, istatus )
   WRITE(6,'(1x,2a)') 'nlfile==', nlfile
   IF ( nlfile(1:1) == ' ' .OR. istatus /= 0 ) THEN  ! Use standard input to be backward-compatible
     unum = 5
   ELSE
     INQUIRE(FILE=nlfile,EXIST=iexist)
     IF (.NOT. iexist) THEN
       WRITE(6,'(1x,3a)') 'WARNING: namelist file - ',               &
            TRIM(nlfile),' does not exist. Falling back to standard input.'
       unum = 5
     END IF
   END IF
   IF (unum /= 5) THEN
     namelist_filename = nlfile
     CALL getunit( unum )
     OPEN(unum,FILE=TRIM(nlfile),STATUS='OLD',FORM='FORMATTED')
     WRITE(6,'(1x,3a,/,1x,a,/)') 'Reading ARPS namelist from file - ',   &
             TRIM(nlfile),' ... ','========================================'
   ELSE
     WRITE(6,'(2(1x,a,/))') 'Waiting namelist from standard input ... ', &
                            '========================================'
   END IF

   modelopt=9999
   READ(unum,file_name)
   WRITE(6,'(/a)')' Namelist file_name sucessfully read in.'
   READ(unum,sat_info)
   WRITE(6,'(/a)')' Namelist sat_info sucessfully read in.'

   lengbf=LEN_trim(grdbasfn)
   lenfil=LEN_trim(filename)

   IF ( modelopt == 1 ) THEN

      WRITE(6,'(/a,a)')' Grid-base file name is ', grdbasfn
      WRITE(6,'(/a,a)')' The  data  set name is ', filename

      CALL get_dims_from_data(hinfmt,filename(1:lenfil),                    &
                              nx,ny,nz,nzsoil,nstyps, ireturn)
      nproc_x = 1  !this is a parameter for non mpi setting
      nproc_y = 1  !this is a parameter for non mpi setting

   ELSE IF ( modelopt == 2 ) THEN
      READ(unum,wrf_info)
      WRITE(6,'(/a)')' Namelist wrf_info sucessfully read in.'
      WRITE(6,'(2a)')' The WRF history file is ', filename

      nproc_x = 1  !this is a parameter for non mpi setting
      nproc_y = 1  !this is a parameter for non mpi setting
      nproc_x_in = 1
      nproc_y_in = 1
      CALL wrf_get_dimensions(filename,numdigits,nx,ny,nz,nzsoil,nstyps,nscalar,  &
                              P_QC,P_QR,P_QI,P_QS,P_QG,P_QH,     &
                              P_NC,P_NR,P_NI,P_NS,P_NG,P_NH,     &
                              P_ZR,P_ZI,P_ZS,P_ZG,P_ZH,qnames,   &
                              mapproj,ctrlat,ctrlon,trulat1,trulat2,trulon,      &
                              dx,dy,ireturn )
      P_QH=P_QG
      CALL mpinit_var

   END IF

   READ(unum,others)
   WRITE(6,'(/a)')' Namelist others sucessfully read in.'

  END SUBROUTINE namelist_read

  SUBROUTINE var_init

  istatus=0
  dmpfmt=3
  hdf4cmpr=1
  if (modelopt==2) then
    sclfct=1
    mpfctopt=1
    ternopt=2
  end if
  nstyp=nstyps
! denom=1./(zice-zfrez)

  ALLOCATE(x(nx))
  ALLOCATE(y(ny))
  ALLOCATE(z(nz))
  ALLOCATE (xs(nx),ys(ny))
  ALLOCATE(zs(nx,ny,nz))
! ALLOCATE(vrsc(nz))
! ALLOCATE(elvang(nz))
  ALLOCATE(zp(nx,ny,nz))
  ALLOCATE(zpsoil(nx,ny,nzsoil))
  x=0
  y=0
  z=0
  zs=0
  zp=0
  zpsoil=0

  IF ( modelopt == 2 ) THEN
  ALLOCATE(j1(nx,ny,nz))
  ALLOCATE(j2(nx,ny,nz))
  ALLOCATE(j3(nx,ny,nz))
  ALLOCATE(j3soil(nx,ny,nzsoil))
  ALLOCATE(aj3x(nx,ny,nz))
  ALLOCATE(aj3y(nx,ny,nz))
  ALLOCATE(aj3z(nx,ny,nz))
  ALLOCATE(j3inv(nx,ny,nz))
  ALLOCATE(j3soilinv(nx,ny,nzsoil))
  ALLOCATE(mapfct(nx,ny,8))
  ALLOCATE(phb(nx,ny,nz))
  ALLOCATE(hterain(nx,ny))
  j1=0
  j2=0
  j3=0
  j3soil=0
  aj3x=0
  aj3y=0
  aj3z=0
  j3inv=0
  j3soilinv=0
  mapfct=0
  phb=0.0
  hterain=0
  END IF


  ALLOCATE(u(nx,ny,nz))
  u=0
  ALLOCATE(v(nx,ny,nz))
  v=0
  ALLOCATE(w(nx,ny,nz))
  w=0
  ALLOCATE(qscalar(nx,ny,nz,nscalar), STAT = istatus)
  qscalar = 0.0
  ALLOCATE(tke(nx,ny,nz))
  tke=0
  ALLOCATE(kmh(nx,ny,nz))
  kmh=0
  ALLOCATE(kmv(nx,ny,nz))
  kmv=0
  ALLOCATE(ubar(nx,ny,nz))
  ubar=0
  ALLOCATE(vbar(nx,ny,nz))
  vbar=0
  ALLOCATE(wbar(nx,ny,nz))
  wbar=0
  ALLOCATE(ptbar(nx,ny,nz))
  ptbar=0
  ALLOCATE(rhobar(nx,ny,nz))
  rhobar=0
  ALLOCATE(pbar(nx,ny,nz))
  pbar=0
  ALLOCATE(qvbar(nx,ny,nz))
  qvbar=0
  ALLOCATE(soiltyp(nx,ny,nstyps))
  soiltyp=0
  ALLOCATE(stypfrct(nx,ny,nstyps))
  stypfrct=0
  ALLOCATE(vegtyp(nx,ny))
  vegtyp=0
  ALLOCATE(lai(nx,ny))
  lai=0
  ALLOCATE(roufns(nx,ny))
  roufns=0
  ALLOCATE(veg(nx,ny))
  veg=0
  ALLOCATE(tsoil   (nx,ny,nzsoil,0:nstyps))
  tsoil = 0
  ALLOCATE(qsoil   (nx,ny,nzsoil,0:nstyps))
  qsoil = 0
! ALLOCATE(comprf(nx,ny))
! comprf= 0

  ALLOCATE(wetcanp(nx,ny))
  wetcanp=0
  ALLOCATE(snowdpth(nx,ny))
  snowdpth=0
  ALLOCATE(raing(nx,ny))
  raing=0
  ALLOCATE(rainc(nx,ny))
  rainc=0
  ALLOCATE(prcrate(nx,ny,4))
  prcrate=0
  ALLOCATE(radfrc(nx,ny,nz))
  radfrc=0
  ALLOCATE(radsw(nx,ny))
  radsw=0
  ALLOCATE(rnflx(nx,ny))
  rnflx=0
  ALLOCATE(radswnet (nx,ny))
  radswnet = 0
  ALLOCATE(radlwin (nx,ny))
  radlwin = 0

  ALLOCATE(usflx(nx,ny))
  usflx=0
  ALLOCATE(vsflx(nx,ny))
  vsflx=0
  ALLOCATE(ptsflx(nx,ny))
  ptsflx=0
  ALLOCATE(qvsflx(nx,ny))
  qvsflx=0
  ALLOCATE(uprt(nx,ny,nz))
  uprt=0
  ALLOCATE(vprt(nx,ny,nz))
  vprt=0
  ALLOCATE(wprt(nx,ny,nz))
  wprt=0
  ALLOCATE(ptprt(nx,ny,nz))
  ptprt=0
  ALLOCATE(pprt(nx,ny,nz))
  pprt=0
  ALLOCATE(phprt(nx,ny,nz))
  phprt=0
  ALLOCATE(rhoprt(nx,ny,nz))
  rhoprt=0
  ALLOCATE(qvprt(nx,ny,nz))
  qvprt=0
  ALLOCATE(tk(nx,ny,nz))
  tk=0

  ALLOCATE(tem1(nx,ny,nz))
  tem1=0
  ALLOCATE(tem2(nx,ny,nz))
  tem2=0
  ALLOCATE(tem3(nx,ny,nz))
  tem3=0
  ALLOCATE(rh(nx,ny,nz))
  rh=0
  ALLOCATE(qv(nx,ny,nz))
  qv=0

  ALLOCATE(lat(nx,ny))
  lat=0
  ALLOCATE(lon(nx,ny))
  lon=0
  ALLOCATE(ptot(nx,ny,nz))
  ptot=0

!-----------------------------------------------------------------------
!
! Allocate for satellite variables
!
!-----------------------------------------------------------------------

  ALLOCATE(cld_base(nx,ny))
  cld_base=-999
  ALLOCATE(cld_top(nx,ny))
  cld_top=-999
  ALLOCATE(cld_base_grided(nx,ny))
  cld_base=-999
  ALLOCATE(cld_top_grided(nx,ny))
  cld_top=-999
  ALLOCATE(cwp(nx,ny))
  cwp=0.
  ALLOCATE(hydro_ref(nx,ny))
  hydro_ref=0.
! ALLOCATE(iwp(nx,ny))
! iwp=0.
  ALLOCATE(level300(nx,ny))
  ALLOCATE(tpw(nx,ny))
  tpw=0.
  ALLOCATE(tpwflag(nx,ny))
  tpwflag=0

  ALLOCATE(cwpout(nx*ny))
  ALLOCATE(xsout(nx*ny))
  ALLOCATE(ysout(nx*ny))
  ALLOCATE(phsout(nx*ny))
  ALLOCATE(cldbshgt(nx*ny))
  ALLOCATE(cldtphgt(nx*ny))
  ALLOCATE(tpwout(nx*ny))
  ALLOCATE(tpwfout(nx*ny))

  END SUBROUTINE var_init

  SUBROUTINE data_read

    REAL, ALLOCATABLE :: fnm(:),fnp(:),dn(:)
    REAL              :: cf1, cf2,cf3

    IF ( modelopt == 1 ) THEN

      CALL dtaread(nx,ny,nz,nzsoil,nstyps,                              &
                   hinfmt,nchin,grdbasfn(1:lengbf),lengbf,              &
                   filename(1:lenfil),lenfil,time,                      &
                   x,y,z,zp,zpsoil,uprt,vprt,wprt,ptprt,pprt,           &
                   qvprt, qscalar, tke, kmh, kmv,                       &
                   ubar, vbar, wbar, ptbar, pbar, rhobar, qvbar,        &
                   soiltyp,stypfrct,vegtyp,lai,roufns,veg,              &
                   tsoil,qsoil,wetcanp,snowdpth,                        &
                   raing,rainc,prcrate,                                 &
                   radfrc,radsw,rnflx,radswnet,radlwin,                 &
                   usflx,vsflx,ptsflx,qvsflx,                           &
                   ireturn, tem1,tem2,tem3)
      curtim = time

    ELSE IF ( modelopt == 2 ) THEN

      ALLOCATE(mu (nx,ny), STAT = ireturn)
      ALLOCATE(mub(nx,ny), STAT = ireturn)
      ALLOCATE(psfc(nx,ny),STAT = ireturn)
      ALLOCATE(varsfc(nx,ny,4),STAT = ireturn)
      ALLOCATE(dnw(nz),    STAT = ireturn)
      ALLOCATE(znw(nz),    STAT = ireturn)
      ALLOCATE(znu(nz),    STAT = ireturn)
      ALLOCATE(wmixr(nx,ny,nz), STAT = ireturn)
      wmixr=0.0

      ALLOCATE(dn (nz),    STAT = ireturn)
      ALLOCATE(fnm(nz),    STAT = ireturn)
      ALLOCATE(fnp(nz),    STAT = ireturn)

      CALL initwrfgrd(filename,nx,ny,nz,nzsoil,                         &
                      x,y,z,zp,zpsoil,hterain,mapfct,lat,lon,           &
                      j1,j2,j3,aj3x,aj3y,aj3z,j3inv,                    &
                      uprt,vprt,wprt,ptprt,pprt,phprt,qvprt,qscalar,    &
                      ubar,vbar,ptbar,pbar,qvbar,rhobar,phb,            &
                      nqw,qnames_wrf,wmixr,p_top, psfc,varsfc,          &
                      mu,mub,dnw,dn,znw,znu,                            &
                      .TRUE.,fnm,fnp,cf1,cf2,cf3,                       &
                      j3soil,j3soilinv,tem1,tem2,tem3,ireturn)

      DEALLOCATE( dn, fnm,fnp, psfc, varsfc )
    END IF

    ptot=pbar+pprt

    IF ( ireturn==0 ) return

  END SUBROUTINE data_read

  SUBROUTINE grid_set

!
!-----------------------------------------------------------------------
!
!  Set map projection parameters
!
!-----------------------------------------------------------------------
!
    latnot(1)=trulat1
    latnot(2)=trulat2
    CALL setmapr(mapproj,sclfct,latnot,trulon)
    CALL lltoxy(1,1,ctrlat,ctrlon,xctr,yctr)
!
    gdx=x(2)-x(1)
    x0sc=xctr - 0.5*(nx-3)*gdx
    gdy=y(2)-y(1)
    y0sc=yctr - 0.5*(ny-3)*gdy
    CALL setorig(1,x0sc,y0sc)
!
!-----------------------------------------------------------------------
!
!  Calculate lat,lon locations of the scalar grid points
!
!-----------------------------------------------------------------------
!
    DO i=1,nx-1
      xs(i)=0.5*(x(i)+x(i+1))
    END DO
    xs(nx)=2.*xs(nx-1)-xs(nx-2)
    DO j=1,ny-1
      ys(j)=0.5*(y(j)+y(j+1))
    END DO
    ys(ny)=2.*ys(ny-1)-ys(ny-2)

    CALL xytoll(nx,ny,xs,ys,lat,lon)

    IF (ll_or_xy == 1 ) THEN ! radar specified with latrad, lonrad
      CALL lltoxy(1,1,latsat,lonsat,satx,saty)
    ELSE
      CALL xytoll(1,1,satx,saty,latsat, lonsat)
    END IF
!
!-----------------------------------------------------------------------
!
!  Move z field onto the scalar grid.
!
!-----------------------------------------------------------------------
!
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          zs(i,j,k)=0.5*(zp(i,j,k)+zp(i,j,k+1))
        END DO
      END DO
    END DO
    DO j=1,ny-1
      DO i=1,nx-1
        zs(i,j,nz)=(2.*zs(i,j,nz-1))                                &
                       -zs(i,j,nz-2)
      END DO
    END DO

  END SUBROUTINE grid_set

SUBROUTINE dump_sat

INTEGER :: nobs,ncwp,ntpw

  nobs=0
  DO j=2,ny-1
    DO i=2,nx-1
      nobs=nobs+1
      IF (cwp(i,j)>5E-2) THEN
      phsout(nobs)=2
      cwpout(nobs)=cwp(i,j)
      cldbshgt(nobs)=ptot(i,j,cld_base_grided(i,j))
      cldtphgt(nobs)=ptot(i,j,cld_top_grided(i,j))
      ELSE IF (cwp(i,j)>=0 .and. hydro_ref(i,j)>0) THEN
        phsout(nobs)=5
        cwpout(nobs)=0
        cldbshgt(nobs)=ptot(i,j,cld_base_grided(i,j))
        cldtphgt(nobs)=ptot(i,j,cld_top_grided(i,j))
      ELSE
        phsout(nobs)=4
        cwpout(nobs)=0.0
        cldbshgt(nobs)=101315.0
        cldtphgt(nobs)=1000.0
      END IF
      tpwout(nobs)=tpw(i,j)*100
      tpwfout(nobs)=tpwflag(i,j)
      xsout(nobs)=xs(i)
      ysout(nobs)=ys(j)
    END DO
  END DO

  IF (1==1) THEN
    ncwp=0;ntpw=0
    DO i=1,nobs
      IF (cwpout(i)>1E-2) ncwp=ncwp+1
      IF (tpwfout(i)==1) ntpw=ntpw+1
    END DO
    print*,'number of cwp : ',ncwp
    print*,'number of tpw : ',ntpw
    print*,'total grid points : ',nobs
  END IF


! IF (gen_cwp==1) THEN
  write(cwpoutfile,'(a,i4.4,4i2.2,a)')          &
        'goes',year,month,day,hour,minute,'.dat'
  open(123,file=cwpoutfile,form='unformatted')
  write(123) nx-2,ny-2,nobs
  write(123) (phsout(i),i=1,nobs)
  write(123) (xsout(i),i=1,nobs)
  write(123) (ysout(i),i=1,nobs)
  write(123) (cldbshgt(i),i=1,nobs)
  write(123) (cldtphgt(i),i=1,nobs)
  write(123) (cwpout(i),i=1,nobs)
  close(123)
! END IF

! IF (gen_tpw==1) THEN
  write(tpwoutfile,'(a,i4.4,4i2.2,a)')          &
        'goesr',year,month,day,hour,minute,'.dat'
  open(123,file=tpwoutfile,form='unformatted')
  write(123) nx-2,ny-2,nobs
  write(123) (xsout(i),i=1,nobs)
  write(123) (ysout(i),i=1,nobs)
  write(123) (tpwout(i),i=1,nobs)
  write(123) (tpwfout(i),i=1,nobs)
  close(123)
! END IF

  IF (0==1) THEN
    j=0
    DO i=1,nobs
      IF (tpwfout(i)>0) j=j+1
    END DO
    PRINT*,j
  END IF

  if (0==1) then
    write(cwpoutfile,'(a,i4.4,4i2.2,a)')          &
          'goes',year,month,day,hour,minute,'.txt'
    open(124,file=cwpoutfile,status='replace')
    write(124,*) nx,ny,nobs
    write(124,*) phsout
    write(124,*) xsout
    write(124,*) ysout
    write(124,*) cldbshgt
    write(124,*) cldtphgt
    write(124,*) cwpout
    close(124)
  end if

  if (0==1) then
    print*,tpwout
  end if

END SUBROUTINE dump_sat


END MODULE a2s_subs
