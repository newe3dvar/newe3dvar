SUBROUTINE calc_cld(nx,ny,nz,ptprt,ptbar,ptot,qvprt,qscalar,nscalar, &
                    cld_base_grided,cld_top_grided,wmixr)

  IMPLICIT NONE

  INCLUDE 'phycst.inc'

  INTEGER :: nx,ny,nz,nscalar
  REAL, DIMENSION(nx,ny,nz)  :: ptprt,ptbar,ptot,qvprt,wmixr
  REAL :: qscalar(nx,ny,nz,nscalar)
  INTEGER, DIMENSION(nx,ny)  :: cld_base_grided,cld_top_grided
  INTEGER :: imid,jmid,kk

  INTEGER :: i,j,k
  INTEGER :: kmax,locc,locr,loci,locs,loch
  REAL :: maxc,maxr,maxi,maxs,maxh
  REAL :: pmb,tc,tk,td,wmr,thepcl,plcl,tlcl
  REAL :: plnhi,plnlo,plnlcl,whi,wlo,ptmp,thresh,hydhgt

  REAL :: oe,wmr2td

    imid=nx/2
    jmid=ny/2
    thresh=1.0E-15
    hydhgt=1.0E-7

!   DO k=1,nz
!     DO j=1,ny
!       DO i=1,nx
!         tem1(i,j,k)= ptprt(i,j,k)*(ptprt(i,j,k)/p0)**rddcp
!       END DO
!     END DO
!   END DO
!   CALL getqvs(nx,ny,nz, 1,nx-1,1,ny-1,1,nz-1, ptprt, tem1,qv)
!   DO k=1,nz-1
!     DO j=1,ny-1
!       DO i=1,nx-1
!         IF (qv(i,j,k)/=0) THEN
!         rh(i,j,k)= qvprt(i,j,k)/qv(i,j,k)
!         END IF
!         IF( rh(i,j,k) > 1.0 ) rh(i,j,k)=1.0
!         IF( rh(i,j,k) < 0.0 ) rh(i,j,k)=0.0
!       END DO
!     END DO
!   END DO

    DO j=1,ny-1
      DO i=1,nx-1
        pmb=0.01*ptot(i,j,2)
        tk=(ptprt(i,j,2)+ptbar(i,j,2))*((1000./pmb)**rddcp)
        tc=tk-273.15
        !wmr=1000.*(qvprt(i,j,2)/(1.-qvprt(i,j,2)))
        wmr=1000.*wmixr(i,j,2)
        td=wmr2td(pmb,wmr)
        thepcl=oe(tc,td,pmb)
        CALL ptlcl(pmb,tc,td,plcl,tlcl)
        plcl=plcl*100.
        DO k=3,nz-2
          ptmp=ptot(i,j,k)
          IF(ptmp < plcl) EXIT
        END DO
        cld_base_grided(i,j)=k-1
        maxc=maxval(qscalar(i,j,:,1))
        maxr=maxval(qscalar(i,j,:,2))
        maxi=maxval(qscalar(i,j,:,3))
        maxs=maxval(qscalar(i,j,:,4))
        maxh=maxval(qscalar(i,j,:,5))
        IF (maxc>=thresh .or. maxr>=thresh .or. maxi>=thresh    &
            .or. maxs>=thresh .or. maxh>=thresh ) THEN
          locc=maxloc(qscalar(i,j,:,1),DIM=1)
          locr=maxloc(qscalar(i,j,:,2),DIM=1)
          loci=maxloc(qscalar(i,j,:,3),DIM=1)
          locs=maxloc(qscalar(i,j,:,4),DIM=1)
          loch=maxloc(qscalar(i,j,:,5),DIM=1)
          DO kk=max(locc,locr,loci,locs,loch),nz-2
            IF(qscalar(i,j,kk,1)<=hydhgt .and. qscalar(i,j,kk,2)<=hydhgt .and.  &
               qscalar(i,j,kk,3)<=hydhgt .and. qscalar(i,j,kk,4)<=hydhgt .and.  &
               qscalar(i,j,kk,5)<=hydhgt) EXIT
          END DO
          cld_top_grided(i,j)=kk
        ELSE
          cld_top_grided(i,j)=-1
        END IF
      END DO
    END DO

END SUBROUTINE calc_cld

SUBROUTINE calc_cwp(nx,ny,nz,cld_base_grided,cld_top_grided,    &
                    qscalar,ptot,cwp,hydro)

  IMPLICIT NONE

  INCLUDE 'globcst.inc'

  INTEGER :: nx,ny,nz
  INTEGER, DIMENSION(nx,ny) :: cld_base_grided,cld_top_grided
  REAL, DIMENSION(nx,ny,nz) :: ptot
  REAL :: qscalar(nx,ny,nz,nscalar)
  INTEGER :: i,j,k
  REAL, DIMENSION(nx,ny) :: cwp, hydro !,cwp
  REAL, PARAMETER :: g=9.81 !m s^-2

  DO i=2,nx-1
  DO j=2,ny-1
! IF (cld_base_grided(i,j) > 0 .and. comprf(i,j) >= thresh ) THEN
  IF (cld_base_grided(i,j) > 0 .and. cld_base_grided(i,j)<=cld_top_grided(i,j) ) THEN
  DO k=cld_base_grided(i,j),cld_top_grided(i,j)
! DO k=2,nz-1
!   cwp(i,j)=cwp(i,j)+0.5*(qscalar(i,j,k,1)           &
!            +qscalar(i,j,k+1,1)+qscalar(i,j,k,2)     &
!            +qscalar(i,j,k+1,2)+qscalar(i,j,k,3)     &
!            +qscalar(i,j,k+1,3)+qscalar(i,j,k,4)     &
!            +qscalar(i,j,k+1,4)+qscalar(i,j,k,5)     &
!            +qscalar(i,j,k+1,5))*(zps(i,j,k+1)       &
!            -zps(i,j,k))*0.5*(rhobar(i,j,k)+rhobar(i,j,k+1))
!   lwp(i,j)=lwp(i,j)+0.5*(qscalar(i,j,k,1)           &
!            +qscalar(i,j,k+1,1)+qscalar(i,j,k,2)     &
!            +qscalar(i,j,k+1,2))*(zps(i,j,k+1)       &
!            -zps(i,j,k))*0.5*(rhobar(i,j,k)+rhobar(i,j,k+1))
!   iwp(i,j)=iwp(i,j)+0.5*(qscalar(i,j,k,3)           &
!            +qscalar(i,j,k+1,3)+qscalar(i,j,k,4)     &
!            +qscalar(i,j,k+1,4)+qscalar(i,j,k,5)     &
!            +qscalar(i,j,k+1,5))*(zps(i,j,k+1)       &
!            -zps(i,j,k))*0.5*(rhobar(i,j,k)+rhobar(i,j,k+1))
!   cwp(i,j)=cwp(i,j)+(qscalar(i,j,k,1)               &
!            +qscalar(i,j,k,2)+qscalar(i,j,k,3)       &
!            +qscalar(i,j,k,4)+qscalar(i,j,k,5))      &
!            *(zps(i,j,k+1)-zps(i,j,k))*rhobar(i,j,k)
!   lwp(i,j)=lwp(i,j)+(qscalar(i,j,k,1)               &
!            +qscalar(i,j,k,2))*(zps(i,j,k+1)         &
!            -zps(i,j,k))*rhobar(i,j,k)
!   iwp(i,j)=iwp(i,j)+(qscalar(i,j,k,3)               &
!            +qscalar(i,j,k,4)+qscalar(i,j,k,5))      &
!            *(zps(i,j,k+1)-zps(i,j,k))*rhobar(i,j,k)
    hydro(i,j)=hydro(i,j)+(qscalar(i,j,k,2) +         &
               qscalar(i,j,k,4)+qscalar(i,j,k,5)) *   &
               (ptot(i,j,k)-ptot(i,j,k+1))/g
    cwp(i,j)=cwp(i,j)+(qscalar(i,j,k,1) +             &
             qscalar(i,j,k,3)) *                      &
             (ptot(i,j,k)-ptot(i,j,k+1))/g
  END DO
  ELSE
  END IF
    IF (cwp(i,j) < 0.0) cwp(i,j) = 0.0
    IF (hydro(i,j) < 0.0) hydro(i,j) = 0.0
!   IF (iwp(i,j) < 0.0) iwp(i,j) = 0.0
  END DO
  END DO


END SUBROUTINE calc_cwp

