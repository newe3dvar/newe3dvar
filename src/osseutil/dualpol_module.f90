
MODULE module_DualFO      ! DualPol Forward Operator

!-----------------------------------------------------------------------
!
! PURPOSE:
!
! Some parameters and subroutines used by dualpol opertors
! such as Zhh, Zdr, and Kdp.
!
! By Jidong Gao September 10, 2017
!
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
! Declare parameters.
!-----------------------------------------------------------------------

  IMPLICIT NONE
  SAVE

  REAL, PARAMETER :: pi = 3.141592   ! pi
  REAL, PARAMETER :: degKtoC=273.15  ! Conversion from K to C
  REAL, PARAMETER :: missing = -9999.0

  REAL, PARAMETER :: intvl =15.0

  REAL, PARAMETER :: Tm = -15.0         ! maximum deposition growth
  REAL, PARAMETER :: sg_phi=0.349066    ! pi/9.0 Standard deviation of canting angle

  REAL, PARAMETER :: epsQ  = 1.e-15
  REAL, PARAMETER :: epsQw = 1.e-15

  REAL, PARAMETER :: epsN  = 1.e-3
  REAL, PARAMETER :: epsNw = 1.e-3

  REAL, PARAMETER :: c_unit = 1.0e+06


  INTEGER :: grpl_ON
  INTEGER :: hl_ON
  !INTEGER :: dualpol_opt

  REAL :: DmcX
  REAL :: DmiX
  REAL :: DmrX
  REAL :: DmsdX
  REAL :: DmswX
  REAL :: DmgdX
  REAL :: DmgwX
  REAL :: DmhdX
  REAL :: DmhwX

  REAL :: QrMX
  REAL :: QsdMX
  REAL :: QswMX
  REAL :: QhdMX
  REAL :: QhwMX
  REAL :: QgdMX
  REAL :: QgwMX


!-----------------------------------------------------------------------
! Variables to can be changed by parameter retrieval
!-----------------------------------------------------------------------

  REAL :: rhor
  REAL :: rhoh
  REAL :: rhos
  REAL :: rhog
  REAL :: rhoc
  REAL :: rhoi

  REAL :: N0r
  REAL :: N0h
  REAL :: N0s
  REAL :: N0g

!-----------------------------------------------------------------------
! Variables to can be changed for meling ice
!-----------------------------------------------------------------------
  REAL :: fos        ! Maximum fraction of rain-snow mixture
  REAL :: foh        ! Maximum fraction of rain-hail mixture
  REAL :: fog        ! Maximum fraction of rain-hail mixture

!-----------------------------------------------------------------------
!  Declare new observation type
!-----------------------------------------------------------------------

  TYPE T_obs_dualp
    REAL :: T_log_ref, T_sum_ref_h, T_sum_ref_v
    REAL :: T_log_zdr, T_sum_ref_hv, T_kdp

    REAL :: T_ref_r_h, T_ref_s_h, T_ref_h_h,T_ref_g_h
    REAL :: T_ref_rs_h,T_ref_rh_h,T_ref_rg_h
    REAL :: T_ref_c_h, T_ref_i_h

    REAL :: T_ref_r_v, T_ref_s_v, T_ref_h_v, T_ref_g_v
    REAL :: T_ref_rs_v, T_ref_rh_v, T_ref_rg_v
    REAL :: T_ref_c_v, T_ref_i_v
  END TYPE T_obs_dualp

!-----------------------------------------------------------------------
!  Declare new DSD parameter data type
!-----------------------------------------------------------------------

  TYPE T_para_qNt
    REAL :: T_qr, T_qs, T_qh, T_qg, T_qc,T_qi
    REAL :: T_Ntr, T_Nts, T_Nth, T_Ntg, T_Ntc,T_Nti
  END TYPE T_para_qNt

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! SUBROUTINES AND FUNCTIONS
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  CONTAINS

  SUBROUTINE init_para(istatus)

    IMPLICIT NONE

    !INTEGER, INTENT(IN)  :: mphyopt
    !INTEGER, INTENT(IN)  :: P_QC, P_QR, P_QI, P_QS, P_QH, P_QG
    !INTEGER, INTENT(IN)  :: P_NC, P_NR, P_NI, P_NS, P_NH, P_NG
    !INTEGER, INTENT(IN)  ::       P_ZR, P_ZI, P_ZS, P_ZH, P_ZG
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    N0r=8.0E+06 ! Intercept parameter in 1/(m^4) for rain.
    N0s=3.0E+06 ! Intercept parameter in 1/(m^4) for snow.
    N0h=4.0E+04 ! Intercept parameter in 1/(m^4) for hail.
    N0g=4.0E+05 ! Intercept parameter in 1/(m^4) for graupel.

    rhor=1000. ! Density of rain (kg m**-3)
    rhoh=917.  ! Density of hail (kg m**-3)
    rhos=100.  ! Density of snow (kg m**-3)
    rhog=500.  ! Density of graupel (kg m**-3)
    rhoc=1000. ! Density of cloud water (kg m**-3)
    rhoi=917.  ! Density of ice (kg m**-3)

    DmcX = 0.0
    DmiX = 0.0
    DmrX = 0.0
    DmsdX = 0.0
    DmswX = 0.0
    DmgdX = 0.0
    DmgwX = 0.0
    DmhdX = 0.0
    DmhwX = 0.0

    QrMX  = 999.0
    QsdMX  = 999.0
    QswMX  = 999.0
    QhdMX  = 999.0
    QhwMX  = 999.0
    QgdMX  = 999.0
    QgwMX  = 999.0

    !PTR_QC = -1; PTR_QR = -1; PTR_QI = -1; PTR_QS = -1; PTR_QH = -1; PTR_QG = -1;
    !PTR_NC = -1; PTR_NR = -1; PTR_NI = -1; PTR_NS = -1; PTR_NH = -1; PTR_NG = -1;
    !             PTR_ZR = -1; PTR_ZI = -1; PTR_ZS = -1; PTR_ZH = -1; PTR_ZG = -1;
    !grpl_ON = 0; hl_ON = 0
    !
    !PTR_QC = P_QC; PTR_QR = P_QR; PTR_QI = P_QI; PTR_QS = P_QS; PTR_QH = P_QH; PTR_QG = P_QG;
    !PTR_NC = P_NC; PTR_NR = P_NR; PTR_NI = P_NI; PTR_NS = P_NS; PTR_NH = P_NH; PTR_NG = P_NG;
    !               PTR_ZR = P_ZR; PTR_ZI = P_ZI; PTR_ZS = P_ZS; PTR_ZH = P_ZH; PTR_ZG = P_ZG;
    !
    !!SELECT CASE (mphyopt)
    !!CASE(1:4)
    !!  grpl_ON = 0
    !!  hl_ON   = 1
    !!CASE(5:7,106,108,110,116)
    !!  grpl_ON = 1
    !!  hl_ON   = 0
    !!CASE DEFAULT
    !!  WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported mphyopt = ',mphyopt,'.'
    !!  istatus = -1
    !!  RETURN
    !!END SELECT
    !
    !grpl_ON = (PTR_QG > 0)
    !hl_ON   = (PTR_QH > 0)

    RETURN
  END SUBROUTINE init_para

  !#####################################################################

  TYPE(T_obs_dualp) FUNCTION initRef()
    initRef%T_sum_ref_h = 0.
    initRef%T_sum_ref_v = 0.
    initRef%T_log_zdr = missing
    initRef%T_log_ref = 0.
    initRef%T_sum_ref_hv = 0.
    initRef%T_kdp = 0.

    initRef%T_ref_r_h = 0.
    initRef%T_ref_s_h = 0.
    initRef%T_ref_h_h = 0.
    initRef%T_ref_g_h = 0.
    initRef%T_ref_rs_h = 0.
    initRef%T_ref_rh_h = 0.
    initRef%T_ref_rg_h = 0.
    initRef%T_ref_c_h = 0.
    initRef%T_ref_i_h = 0.

    initRef%T_ref_r_v = 0.
    initRef%T_ref_s_v = 0.
    initRef%T_ref_h_v = 0.
    initRef%T_ref_g_v = 0.
    initRef%T_ref_rs_v = 0.
    initRef%T_ref_rh_v = 0.
    initRef%T_ref_rg_v = 0.
    initRef%T_ref_c_v = 0.
    initRef%T_ref_i_v = 0.

  END FUNCTION initRef

  TYPE(T_para_qNt) FUNCTION init_q_nt()
    init_q_nt%T_qr = 0.0
    init_q_nt%T_qs = 0.0
    init_q_nt%T_qh = 0.0
    init_q_nt%T_qg = 0.0
    init_q_nt%T_qc = 0.0
    init_q_nt%T_qi = 0.0

    init_q_nt%T_Ntr = 0.0
    init_q_nt%T_Nts = 0.0
    init_q_nt%T_Nth = 0.0
    init_q_nt%T_Ntg = 0.0
    init_q_nt%T_Ntc = 0.0
    init_q_nt%T_Nti = 0.0
  END FUNCTION init_q_nt

  TYPE(T_para_qNt) FUNCTION assign_para_qNt(varqc,varqi,var1,var2,var3,var4, &
                        varnc,varni,var5,var6,var7,var8)
    REAL :: var1,var2,var3,var4,var5,var6,var7,var8
    REAL :: varqc,varqi,varnc,varni

    assign_para_qNt%T_qr = var1
    assign_para_qNt%T_qs = var2
    assign_para_qNt%T_qh = var3
    assign_para_qNt%T_qg = var4
    assign_para_qNt%T_qc = varqc
    assign_para_qNt%T_qi = varqi

    assign_para_qNt%T_Ntr = var5
    assign_para_qNt%T_Nts = var6
    assign_para_qNt%T_Nth = var7
    assign_para_qNt%T_Ntg = var8
    assign_para_qNt%T_Ntc = varnc
    assign_para_qNt%T_Nti = varni
  END FUNCTION assign_para_qNt

  !
  !#####################################################################
  !#####################################################################
  !#########                                                   #########
  !#########              SUBROUTINE refl_new_g                #########
  !#########                                                   #########
  !#####################################################################
  !#####################################################################

  SUBROUTINE  refl_new_g(ta,rhoa,var_qNt,obs_dualp)

  !-----------------------------------------------------------------------
  !
  ! PURPOSE:
  !
  ! Compute radar observations by simplified dualpol formulation derived
  ! by Dr. Guifu Zhang
  !
  !-----------------------------------------------------------------------
  !
  ! AUTHOR:  Jidong Gao, 9/26/2017
  !
  !-----------------------------------------------------------------------
  ! Force explicit declarations.
  !-----------------------------------------------------------------------

    IMPLICIT NONE

    INTEGER :: MFflg = 0

    REAL :: ta
  !-----------------------------------------------------------------------
  ! Declare arguments.
  !-----------------------------------------------------------------------

    TYPE(T_para_qNt) :: var_qNt
    TYPE(T_obs_dualp) :: obs_dualp

    REAL :: rhoa
    REAL :: qr,qs,qh,qg, qc,qi
    REAL :: ntr,nts,nth,ntg, ntc,nti

    REAL :: Ntsw,Nthw,Ntgw

    REAL :: fracqrs,fracqrh,fracqrg
    REAL :: fracqs,fracqh,fracqg
    REAL :: fms,fmh,fmg
    REAL :: fws,fwh,fwg
    REAL :: rhoms,rhomh,rhomg
    REAL :: qrf,qsf,qhf,qgf

    REAL :: refc_h, refi_h,refr_h,refs_h,refh_h,refg_h,refrs_h,refrh_h,refrg_h
    REAL :: refc_v, refi_v,refr_v,refs_v,refh_v,refg_v,refrs_v,refrh_v,refrg_v
    REAL :: kdpc,kdpi,kdpr,kdps,kdph,kdpg,kdprs,kdprh,kdprg
    REAL :: roohvc,roohvi,roohvr,roohvs,roohvh,roohvg,roohvrs,roohvrh,roohvrg

    REAL :: totlh,totlv,totlhv,totlk
    REAL :: totlh1,totlh2
    REAL :: rtem

    REAL :: tair_C, z_ab, tem_zh, temp_kdp
    REAL :: Dmc,Dmi,Dmr,Dmsd,Dmsw,Dmgd,Dmgw,Dmhd,Dmhw

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  ! Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !-----------------------------------------------------------------------
  ! Initialization
  !-----------------------------------------------------------------------

    IF (grpl_ON == 0 .and. hl_ON == 0) THEN
       fos = 0.5;  foh = 0.0; fog = 0.0
    ELSE IF(grpl_ON == 0 .and. hl_ON == 1) THEN
       fos = 0.5;  foh = 0.3; fog = 0.0
    ELSE IF(grpl_ON == 1 .and. hl_ON == 0) THEN
       fos = 0.5;  foh = 0.0; fog = 0.3
    ELSE IF(grpl_ON == 1 .and. hl_ON == 1) THEN
       fos = 0.25; foh = 0.2; fog = 0.25
    ENDIF

    qr = var_qNt%T_qr
    qs = var_qNt%T_qs
    qh = var_qNt%T_qh
    qg = var_qNt%T_qg

    qc = var_qNt%T_qc
    qi = var_qNt%T_qi

    ntr = var_qNt%T_Ntr
    nts = var_qNt%T_Nts
    nth = var_qNt%T_Nth
    ntg = var_qNt%T_Ntg

    ntc = var_qNt%T_Ntc
    nti = var_qNt%T_Nti

    !print*,'ta2==',ta,qr,qs,qh,qg,qc,qi,ntr,nts,nth,ntg,ntc, nti

    qrf   = 0.; qsf   = 0.; qhf    = 0.; qgf   = 0.
    totlh = 0.; totlv = 0.; totlhv = 0.; totlk = 0.
    fracqrs = 0.; fracqs = 0.; fms = 0.; fws = 0.; rhoms = 100.
    fracqrh = 0.; fracqh = 0.; fmh = 0.; fwh = 0.; rhomh = 913.
    fracqrg = 0.; fracqg = 0.; fmg = 0.; fwg = 0.; rhomg = 400.

    if(qr < 0.0) qr =0.0
    if(qs < 0.0) qs =0.0
    if(qh < 0.0) qh =0.0
    if(qg < 0.0) qg =0.0

  !-----------------------------------------------------------------------
  ! Calculate the fraction of water and ice.
  ! For the variable definition, see "FUNCTION rainIceRefl".
  !-----------------------------------------------------------------------

    IF (MFflg == 0) THEN

      CALL fractionWater2(qr,qs,fos,rhos,fracqrs,fracqs,fms,fws,rhoms,ntr,nts,1)
      IF(hl_ON == 1)  &
        CALL fractionWater2(qr,qh,foh,rhoh,fracqrh,fracqh,fmh,fwh,rhomh,ntr,nth,2)
      IF(grpl_ON == 1) &
        CALL fractionWater2(qr,qg,fog,rhog,fracqrg,fracqg,fmg,fwg,rhomg,ntr,ntg,3)

      qrf = qr - fracqrs - fracqrh - fracqrg
      if(qrf < 0.0) qrf = 0.0

      qsf = qs - fracqs
      if(qsf < 0.0) qsf = 0.0
      qhf = qh - fracqh
      if(qhf < 0.0) qhf = 0.0
      qgf = qg - fracqg
      if(qgf < 0.0) qgf = 0.0

    ELSE IF (MFflg == 2) THEN

      qrf = qr
      qsf = qs; fms = 0.0;
      IF(hl_ON == 1)   qhf = qh; fmh = 0.0;
      IF(grpl_ON == 1) qgf = qg; fmg = 0.0;

    END IF

     !print*,'         '
     !print*,'MFfig====',MFflg
  !
  !   print*,'fms,fws==',fms,fws,qr,qs,rhos,rhoms
  !   print*,'fmh,fwh==',fmh,fwh,qr,qh,rhoh,rhomh
  !   print*,'fmg,fwg==',fmg,fwg,qr,qg,rhog,rhomg

  !-----------------------------------------------------------------------
  ! Calculate the mean diameter
  !-----------------------------------------------------------------------

  ! CALL calc_N0x_mp(rhoa,rhoms,rhomh,rhomg,ntr,nts,nth,ntg,qrf,qsf,   &
  !                    fms,qhf,fmh,qgf,fmg)
  ! print*,'epsQ======',epsQ,epsN
  ! stop

  IF(1==1) THEN

    IF(qc < epsQ .OR. ntc < epsN) THEN
      qc  = 0.0
      ntc = 0.0
    ENDIF

    IF(qi < epsQ .OR. nti < epsN) THEN
      qi  = 0.0
      nti = 0.0
    END IF

    IF(qrf < epsQ .OR. ntr < epsN) THEN
      qrf = 0.0
      ntr = 0.0
    ENDIF

    IF(qsf < epsQ .OR. nts < epsN) THEN
       qsf = 0.0
       nts = 0.0
    ENDIF

    IF(fms < epsQw.OR. nts < epsNw) THEN
      fms = 0.0
    ENDIF

    IF(qhf < epsQ .OR. nth < epsN) THEN
      qhf = 0.0
      nth = 0.0
    ENDIF

    IF(fmh < epsQw.OR. nth < epsNw) THEN
      fmh = 0.0
    ENDIF

    IF(qgf < epsQ .OR. ntg < epsN) THEN
      qgf = 0.0
      ntg = 0.0
    ENDIF

    IF(fmg < epsQw.OR. ntg < epsNw) THEN
       fmg = 0.0
    ENDIF
  END IF

      CALL cal_Dmx(rhoa,qc, ntc,rhoc, Dmc,epsN ,pi)
      CALL cal_Dmx(rhoa,qi, nti,rhoi, Dmi,epsN ,pi)
      CALL cal_Dmx(rhoa,qrf,ntr,rhor, Dmr,epsN ,pi)

      CALL cal_Dmx(rhoa,qsf,nts,rhos, Dmsd,epsN,pi)
      CALL cal_Dmx(rhoa,fms,nts,rhoms,Dmsw,epsNw,pi)
      IF(hl_ON == 1) THEN
        CALL cal_Dmx(rhoa,qhf,nth,rhoh, Dmhd,epsN,pi)
        CALL cal_Dmx(rhoa,fmh,nth,rhomh,Dmhw,epsNw,pi)
      ENDIF

      IF(grpl_ON == 1) THEN
        CALL cal_Dmx(rhoa,qgf,ntg,rhog, Dmgd,epsN,pi)
        CALL cal_Dmx(rhoa,fmg,ntg,rhomg,Dmgw,epsNw,pi)
      ENDIF ! end of grpl_ON == 1

          ! print*,'        '
          ! print*,'Dmgd====',rhoa,qgf,ntr,ntg,rhog, Dmgd
          ! print*,'Dmgw====',rhoa,fmg,ntr,ntg,rhomg,Dmgw

          !print*,'Dmc=====',Dmc,rhoa,rhog
          !print*,'Dmi=====',Dmi
          !print*,'Dmr=====',Dmr
          !print*,'Dmsd=====',Dmsd
          !print*,'Dmsw=====',Dmsw
          !print*,'Dmhd=====',Dmhd
          !print*,'Dmhw=====',Dmhw
          !print*,'Dmgd=====',Dmgd
          !print*,'Dmgw=====',Dmgw
          IF(Dmc> DmcX) DmcX = Dmc
          IF(Dmi> DmiX) DmiX = Dmi
          IF(Dmr> DmrX) DmrX = Dmr
          IF(Dmsd> DmsdX) DmsdX = Dmsd
          IF(Dmsw> DmswX) DmswX = Dmsw
          IF(Dmhd> DmhdX) DmhdX = Dmhd
          IF(Dmhw> DmhwX) DmhwX = Dmhw
          IF(Dmgd> DmgdX) DmgdX = Dmgd
          IF(Dmgw> DmgwX) DmgwX = Dmgw

          IF( fracqs < QsdMX .and. fracqs>epsQ) QsdMX = fracqs
          IF( fracqrs< QswMX .and. fracqrs>epsQ) QswMX = fracqrs

          IF( fracqh < QhdMX .and. fracqh>epsQ) QhdMX = fracqh
          IF( fracqrh< QhwMX .and. fracqrh>epsQ) QhwMX = fracqrh

          IF( fracqg < QgdMX .and. fracqg>epsQ) QgdMX = fracqg
          IF( fracqrg< QgwMX .and. fracqrg>epsQ) QgwMX = fracqrg

  !
  !   IF(Dmsw>60.0) print*,'fms=',fms,qr,qs,fracqrs,fracqs  &
  !                              ,ntr,nts,Dmsw
  !   IF(Dmhw>70.0) print*,'fmh=',fmh,qr,qh,fracqrh,fracqh  &
  !                              ,ntr,nth,Dmhw
      IF(Dmgw>50.0) print*,'fmg=',fmg,qr,qg,fracqrg,fracqg  &
                                 ,ntr,ntg,Dmgw

           IF(Dmr >  5.5 ) Dmr  =  5.5
           !IF(Dmsd> 15.0 ) Dmsd = 15.0
           !IF(Dmgd> 15.0 ) Dmgd = 15.0
           !IF(Dmhd> 35.0 ) Dmhd = 35.0

           IF(Dmsw> 12.0 ) Dmsw = 12.0
           !fms = 0.0
           IF(Dmgw> 12.0 ) Dmgw = 12.0
           !fmg = 0.0
           IF(Dmhw> 12.0 ) Dmhw = 12.0
           !fmh = 0.0

  !-----------------------------------------------------------------------
  ! Calculate radar observations.
  !-----------------------------------------------------------------------

    refc_h=0.; refi_h=0.0; refr_h=0.; refs_h=0.; refh_h=0.; refg_h=0.
    refrs_h=0.; refrh_h=0.; refrg_h=0.

    refc_v=0.; refi_v=0.0; refr_v=0.; refs_v=0.; refh_v=0.; refg_v=0.
    refrs_v=0.; refrh_v=0.; refrg_v=0.

    roohvc=0.; roohvi=0.; roohvr=0.; roohvs=0.; roohvh=0.; roohvg = 0.
    roohvrs=0.; roohvrh=0.; roohvrg=0.

    kdpc=0.; kdpi=0.; kdpr=0.; kdps=0.; kdph=0.; kdpg=0.
    kdprs=0.; kdprh=0.; kdprg=0.

  !Beginning to calculate radar obs.

    refc_h = 11250 *(rhoa*qc)/(pi*rhoc)*Dmc*Dmc*Dmc*c_unit
    refc_v = 1.0
    roohvc = 1.0
    kdpc   = 0.0

    refi_h = 2580 *(rhoa*qi)/(pi*rhoi)*Dmi*Dmi*Dmi*c_unit

    !print*,'tair_C==',tair_C
    !z_ab   = 1.67-0.0009*(tair_C-Tm)*(tair_C-Tm)

    !IF(z_ab>1.2) THEN
    !  refi_v = z_ab*exp(-2.0*sg_phi*sg_phi*(sqrt(z_ab)-1.0/sqrt(z_ab)))
    !ELSE
    !  refi_v = 0.0
    !END IF

     tair_C = ta - degKtoC

     z_ab   = 1.1 +0.57*exp(-(tair_C-Tm)*(tair_C-Tm)/50.0)
     refi_v = z_ab*exp(-2.0*sg_phi*sg_phi*(sqrt(z_ab)-1.0/sqrt(z_ab)))

    roohvi = exp(-2.0*sg_phi*sg_phi*(sqrt(z_ab)-1.0/sqrt(z_ab))/3.0)

    !z_ab   = 0.53-0.00065*(tair_C-Tm)*(tair_C-Tm)

    !IF(z_ab>0.2) THEN
    !  kdpi   = z_ab*exp(-2.0*sg_phi*sg_phi)*(rhoa*qi)/rhoi*c_unit
    !ELSE
    !  kdpi   = 0.0
    !END IF

      z_ab   = 0.2 +0.33*exp(-(tair_C-Tm)*(tair_C-Tm)/50.0)
      kdpi   = z_ab*exp(-2.0*sg_phi*sg_phi)*(rhoa*qi)/rhoi*c_unit

    !if( kdpi<0.0) print*,'kdpi==',kdpi

    CALL rdObs_r(rhoa,rhor,Dmr,qrf,refr_h,refr_v,roohvr,kdpr,c_unit)


    CALL rdObs_s(1,rhoa,rhos, 0.0,Dmsd,qsf,refs_h, refs_v, roohvs,     &
                                          kdps,c_unit,pi,nts,epsN)
    IF (fms >  epsQ) THEN
      CALL rdObs_s(2,rhoa,rhoms,fws,Dmsw,fms,refrs_h,refrs_v,roohvrs,    &
                                          kdprs,c_unit,pi,nts ,epsNw)
    END IF


    IF(hl_ON == 1) THEN
      CALL rdObs_h(1,rhoa,rhoh, 0.0,Dmhd,qhf,refh_h, refh_v, roohvh,     &
                                           kdph,c_unit,pi,nth,epsN)
      IF (fmh >  epsQ) THEN
      CALL rdObs_h(2,rhoa,rhomh,fwh,Dmhw,fmh,refrh_h,refrh_v,roohvrh,    &
                                          kdprh,c_unit,pi,nth ,epsNw)
      END IF
    ENDIF

    IF(grpl_ON == 1) THEN
      CALL rdObs_g(1,rhoa,rhog, 0.0,Dmgd,qgf,refg_h, refg_v, roohvg,     &
                                           kdpg,c_unit,pi,ntg,epsN)
      IF (fmg >  epsQ) THEN
      CALL rdObs_g(2,rhoa,rhomg,fwg,Dmgw,fmg,refrg_h,refrg_v,roohvrg,    &
                                          kdprg,c_unit,pi,ntg ,epsNw)
      END IF
    ENDIF

  ! print*,'refc_h c i r s h g rs rh rg=',refc_h,refi_h,refr_h,refs_h,refh_h,refg_h,  &
  !                                    refrs_h,refrh_h,refrg_h


    totlh =   refc_h+refi_h+refr_h+refs_h+refg_h+refh_h                  &
                               +refrs_h+refrh_h+refrg_h

  ! totlh = refr_h

    !IF (ta >= (degKtoC + intvl ) ) then
    !  totlh = refc_h+refr_h+refrs_h+refrh_h+refrg_h
    !ELSE IF (ta <= (degKtoC - intvl )) then
    !  totlh = refi_h+refs_h+refg_h+refh_h
    !ELSE
    !  rtem=(ta-degKtoC+intvl)/(2*intvl)
    !  totlh1= refc_h+refr_h+refrs_h+refrh_h+refrg_h
    !  totlh2 = refi_h+refs_h+refg_h+refh_h
    !  totlh  = rtem*totlh1 + (1-rtem)*totlh2
    !END IF

    if(totlh > 0.) obs_dualp%T_log_ref = 10*log10(MAX(1.0,totlh))

  !  IF( obs_dualp%T_log_ref>101.0 ) THEN
  !    print*,'ntr,qrf,fmg,fog,rhomg,fracqrg,fwg,Dmgw=',ntr,ntg,qrf,fmg,fog,rhomg,fracqrg,fwg,Dmgw,Dmgd
  !    stop
  !  END IF

  ! print*,'refl====',totlh, obs_dualp%T_log_ref

  ! print*,' '
  ! print*,'refc_h c i r s h g rs rh rg=',refc_h,refi_h,refr_h,refs_h,refh_h,refg_h,  &
  !           refrs_h,refrh_h,refrg_h
   !print*,'refc_h===', refc_h
   !print*,'refi_h===', refi_h

  !   print*,'qr qs qh qg=',qr,qs,qh,qg
  !   print*,'nr ns nh ng=',ntr,nts,nth,ntg
  !   print*,'fracqrs=====',fracqrs,fracqrh,fracqrg

  ! print*,'refl====',totlh, obs_dualp%T_log_ref
  ! stop
  ! print*,'Zdr_c i r s h g rs rh rg=',refc_v,refi_v,refr_v,     &
  !           refs_v,refg_h,refh_v,refrs_v,refrh_v,refrg_v

    totlv = 0.0
    if(refc_v>1.0e-7 )  totlv=totlv+ refc_h/refc_v
    if(refi_v>1.0e-7 )  totlv=totlv+ refi_h/refi_v
    if(refr_v>1.0e-7 )  totlv=totlv+ refr_h/refr_v
    if(refs_v>1.0e-7 )  totlv=totlv+ refs_h/refs_v
    if(refg_v>1.0e-7 )  totlv=totlv+ refg_h/refg_v
    if(refh_v>1.0e-7 )  totlv=totlv+ refh_h/refh_v
    if(refrs_v>1.0e-7 ) totlv=totlv+refrs_h/refrs_v
    if(refrh_v>1.0e-7 ) totlv=totlv+refrh_h/refrh_v
    if(refrg_v>1.0e-7 ) totlv=totlv+refrg_h/refrg_v

    totlv = totlv

    if(totlv > 1.0e-7 ) obs_dualp%T_log_zdr = 10.*LOG10(MAX(1.0,totlh/totlv))
  ! if(totlv > 1.0e-7 ) obs_dualp%T_log_zdr = 10.*LOG10(MAX(1.0,refr_v     ))

  ! print*,' '
  ! IF (obs_dualp%T_log_zdr >20.0) THEN
  !   if(refc_v>1.0e-7 )  print*,'ratio_c=',  refc_h/refc_v,refc_h,refc_v
  !   if(refi_v>1.0e-7 )  print*,'ratio_i=',  refi_h/refi_v,refi_h,refi_v
  !   if(refr_v>1.0e-7 )  print*,'ratio_r=',  refr_h/refr_v,refr_h,refr_v
  !   if(refs_v>1.0e-7 )  print*,'ratio_s=',  refs_h/refs_v,refs_h,refs_v
  !   if(refg_v>1.0e-7 )  print*,'ratio_g=',  refg_h/refg_v,refg_h,refg_v
  !   if(refh_v>1.0e-7 )  print*,'ratio_h=',  refh_h/refh_v,refh_h,refh_v
  !   if(refrs_v>1.0e-7 ) print*,'ratiors=', refrs_h/refrs_v,refrs_h,refrs_v
  !   if(refrh_v>1.0e-7 ) print*,'ratiorh=', refrh_h/refrh_v,refrh_h,refrh_v
  !   if(refrg_v>1.0e-7 ) print*,'ratiorg=', refrg_h/refrg_v,refrg_h,refrg_v
  ! END IF

  ! print*,'Zdr=====',totlh, obs_dualp%T_log_zdr



    totlk = kdpc+kdpi+kdpr+kdps+kdph+kdpg+kdprs+kdprg+kdprh
    obs_dualp%T_kdp = totlk

  ! print*,' '
  ! print*,' '
  ! print*,'refc_h c i r s h g rs rh rg=',refc_h,refi_h,refr_h,refs_h,refh_h,refg_h,  &
  !           refrs_h,refrh_h,refrg_h
  ! print*,'Zdr_c i r s h g rs rh rg=',refc_v,refi_v,refr_v,     &
  !           refs_v,refg_h,refh_v,refrs_v,refrh_v,refrg_v
  ! print*,'kdpc+kdpi+kdpr+kdps+kdph+kdpg+kdprs+kdprg+kdprh=',totlk
  ! print*,kdpc,kdpi,kdpr,kdps,kdph,kdpg,kdprs,kdprg,kdprh
  ! print*,'Kdp=====', obs_dualp%T_kdp

    totlhv= roohvc*refc_h+roohvi*refi_h+roohvr*refr_h          &
           +roohvs*refs_h+roohvh*refh_h+roohvg*refg_h          &
           +roohvrs*refrs_h+roohvrh*refrh_h+roohvrg*refrg_h

    IF( totlh>1.0e-7 ) THEN
      obs_dualp%T_sum_ref_hv = totlhv/totlh
    ELSE
      obs_dualp%T_sum_ref_hv = missing
    END IF

  ! IF( obs_dualp%T_sum_ref_hv>1.0 ) THEN
  !   print*,' '
  !   print*,'qcirshg=',qc,qi,qr,qs,qh,qg
  !   print*,'Ncirshg=',ntc,nti,ntr,nts,nth,ntg
  !   print*,'gamar fws fwh fwg=',fws,fwh,fwg
  !   print*,'density of c,i,r,s,h,g,rs,rh,rg a=',rhoc,rhoi,rhor,rhos,rhoh,rhog,rhoms,rhomh,rhomg,rhoa
  !   print*,'fra_qrf,qsd, qsw,qhd,qhw=',qrf,qsf,fms,qhf,fmh,qgf,fmg
  !   print*,'Dm cir_sd_sw hd hw gd gw=',Dmc,Dmi,Dmr,Dmsd,Dmsw,Dmhd,Dmhw,Dmgd,Dmgw
  !   print*,'refc_h c i r s h g rs rh rg=',refc_h,refi_h,refr_h,refs_h,refh_h,refg_h,  &
  !           refrs_h,refrh_h,refrg_h
  !   print*,'Zdr_c i r s h g rs rh rg=',refc_v,refi_v,refr_v,     &
  !           refs_v,refg_h,refh_v,refrs_v,refrh_v,refrg_v
  !   print*,'kdpc+kdpi+kdpr+kdps+kdph+kdpg+kdprs+kdprg+kdprh=',totlk
  !   print*,kdpc,kdpi,kdpr,kdps,kdph,kdpg,kdprs,kdprg,kdprh
  !   print*,'Rhv_cirshg rs rh rg=',roohvc,roohvi,roohvr,roohvs,roohvh,roohvg,  &
  !                  roohvrs,roohvrh,roohvrg
  !   print*,'Rhv=====', obs_dualp%T_sum_ref_hv
  !   print*,'refl====', obs_dualp%T_log_ref
  !   print*,'Zdr=====', obs_dualp%T_log_zdr
  !   print*,'Kdp=====', obs_dualp%T_kdp

  !   obs_dualp%T_sum_ref_hv = missing
  !   stop
  !   if( obs_dualp%T_log_ref >10.0 ) stop
  ! END IF

  END SUBROUTINE refl_new_g

  SUBROUTINE rdObs_r(rhoa,rhor,Dmr,qrf,refr_h,refr_v,roohvr,kdpr,c_unit)
    IMPLICIT NONE

    REAL   :: rhoa,rhor,Dmr,qrf
    REAL   :: refr_h,refr_v,roohvr,kdpr

    REAL   :: Dmr2,Dmr3,Dmr4
    REAL   :: tem_zr,tem_kdp
    REAL   :: c_unit

    Dmr2 = Dmr*Dmr
    Dmr3 = Dmr2*Dmr
    Dmr4 = Dmr3*Dmr
    tem_zr = -1.725+28.49*Dmr+36.046*Dmr2-1.746*Dmr3-0.4899*Dmr4
    refr_h = rhoa*qrf*tem_zr*tem_zr/rhor*c_unit
  ! refr_v = 1.019-0.143*Dmr+0.317*Dmr2-0.065*Dmr3+0.00416*Dmr4
    refr_v = 1.019-0.163*Dmr+0.317*Dmr2-0.066*Dmr3+0.00416*Dmr4
    roohvr = 0.999+0.00826*Dmr-0.0117*Dmr2+0.00361*Dmr3-0.000344*Dmr4
    tem_kdp= -0.0356*Dmr+0.132*Dmr2+0.0032*Dmr3-0.00302*Dmr4
    kdpr   =  rhoa*tem_kdp*qrf/rhor*c_unit

  END SUBROUTINE rdObs_r


  SUBROUTINE rdObs_s(flg,rhoa,rhos,fws,Dms,qsf,                       &
                         refs_h,refs_v,roohvs,kdps,c_unit,pi,nt,epsN)

    IMPLICIT NONE
    INTEGER :: flg
    REAL   :: rhoa,rhos,fws,Dms,qsf,nt
    REAL   :: refs_h,refs_v,roohvs,kdps

    REAL   :: Az0,Az1,Az2,Az3
    REAL   :: Ad0,Ad1,Ad2,Ad3
    REAL   :: Ar0,Ar1,Ar2,Ar3
    REAL   :: Ak0,Ak1,Ak2,Ak3
    REAL   :: fws2,fws3,Dms2,Dms3,tem_zs,tem_kdp
    REAL   :: c_unit,pi,refs_h_zs
    REAL   :: epsN

  IF( nt > epsN ) THEN

    IF(flg ==1) THEN

    Az0 = 0.129
    Az1 = 0.00317
    Az2 = -0.00103
    Az3 = 0.0000468

    Ad0 = 1.0294
    Ad1 = 0.000591
    Ad2 = 0.000657

    Ak0 = 0.00654
    Ak1 = 0.000482
    Ak2 = 0.00000703

    Ar0 = 1.000
    Ar1 = 0.00034
    Ar2 =-0.00004

    ELSE

    fws2 = fws*fws
    fws3 = fws2*fws
    Az0 = 0.129     +6.155*fws    -8.232*fws2    +3.1363*fws3
    Az1 = 0.00317   -0.1077*fws   +0.7365*fws2   -0.9733*fws3
    Az2 = -0.00103  +0.02356*fws  -0.1601*fws2   +0.2111*fws3
    Az3 = 0.0000468 -0.001353*fws +0.007767*fws2 -0.009673*fws3

    Ad0 = 1.0294    +1.0031*fws   +1.3774*fws2   -1.9357*fws3
    Ad1 = 0.000591  +0.0432*fws   -0.1643*fws2   +0.2955*fws3
    Ad2 = 0.000657  -0.00572*fws  +0.0368*fws2   -0.0472*fws3

    Ak0 = 0.00654   -0.04334*fws  +6.4457*fws2   -5.5737*fws3
    Ak1 = 0.000482  -0.02021*fws  +0.1189*fws2   +0.03579*fws3
    Ak2 = 0.00000703-0.00215*fws  +0.01008*fws2  -0.01920*fws3

    Ar0 = 1.000     -0.00590*fws  +0.00198*fws2  +0.00284*fws3
    Ar1 = 0.00034   -0.0115 *fws  +0.06093*fws2  -0.00704*fws3
    Ar2 =-0.00004   +0.00120*fws  -0.00643*fws2  +0.00680*fws3

    END IF

    CALL   CalRadV(Az0,Az1,Az2,Az3,Ad0,Ad1,Ad2,Ar0,Ar1,Ar2,Ak0,Ak1,Ak2,    &
              rhoa,rhos,Dms,qsf,refs_h,refs_v,roohvs,kdps,c_unit,pi,nt)
  ELSE
      refs_h=0.0;refs_v=0.0;roohvs=0.0;kdps=0.0
  END IF  ! end of IF( nt > epsN ) THEN

  ! print*,'refs_h_zs===',refs_h_zs
  ! print*,'refs_h_zs==',refs_h_zs,refs_h, Dms,tem_zs,rhos
  ! print*,'refs_h           =',tem_zs, refs_h
  ! print*,'dms rhoa qsf rhos unit pi =',dms,rhoa,qsf,rhos,refs_h,c_unit,pi
  ! print*,'refs_h tem_zs    =',tem_zs, refs_h
  ! stop

  END SUBROUTINE rdObs_s

  SUBROUTINE rdObs_h(flg,rhoa,rhoh,fwh,Dmh,qhf,                        &
                         refh_h,refh_v,roohvh,kdph,c_unit,pi,nt,epsN)
    IMPLICIT NONE
    INTEGER :: flg
    REAL   :: rhoa,rhoh,fwh,Dmh,qhf,nt
    REAL   :: refh_h,refh_v,roohvh,kdph
    REAL   :: Az0,Az1,Az2,Az3
    REAL   :: Ad0,Ad1,Ad2,Ad3
    REAL   :: Ar0,Ar1,Ar2,Ar3
    REAL   :: Ak0,Ak1,Ak2,Ak3
    REAL   :: fwh2,fwh3,Dmh2,Dmh3,tem_zh,tem_kdp
    REAL   :: c_unit,pi,refh_h_zh
    REAL   :: epsN

  IF( nt > epsN ) THEN

    IF(flg ==1) THEN

    Az0 = 0.4629
    Az1 = 0.00378
    Az2 = -0.000945
    Az3 = 0.0000173

    Ad0 = 1.0370
    Ad1 = 0.00224
    Ad2 = -0.000006

    Ak0 = 0.0402
    Ak1 = 0.00111
    Ak2 = 0.000046

    Ar0 = 0.9713
    Ar1 = 0.00595
    Ar2 = -0.000382

    ELSE

    fwh2 = fwh*fwh
    fwh3 = fwh2*fwh
    Az0 = 0.4629    +3.2277*fwh   -8.3043*fwh2   +6.112*fwh3
    Az1 = 0.00378   -0.1122*fwh   +0.9452*fwh2   -0.7858*fwh3
    Az2 = -0.000945 +0.00682*fwh  -0.0507*fwh2   +0.0399*fwh3
    Az3 = 0.0000173 -0.000143*fwh +0.000798*fwh2 -0.000592*fwh3

    Ad0 = 1.0370    +0.2936*fwh   +1.2434*fwh2   -0.2639*fwh3
    Ad1 = 0.00224   +0.0532*fwh   -0.1490*fwh2   +0.0913*fwh3
    Ad2 = -0.000006 -0.00138*fwh  +0.00293*fwh2  -0.00160*fwh3

    Ak0 = 0.0402    +0.8951*fwh    +2.3449*fwh2   -1.0413*fwh3
    Ak1 = 0.00111   +0.0569*fwh    -0.2058*fwh2   +0.1062*fwh3
    Ak2 = 0.000046  -0.00255*fwh   +0.00389*fwh2  -0.00201*fwh3

    Ar0 = 0.9713    +0.1725*fwh    -0.4710*fwh2   +0.3086*fwh3
    Ar1 = 0.00595   -0.0995*fwh    +0.2258*fwh2   -0.1325*fwh3
    Ar2 = -0.000382 +0.00366*fwh   -0.00725*fwh2  +0.00408*fwh3

    END IF   !end of flg ==1

    CALL   CalRadV(Az0,Az1,Az2,Az3,Ad0,Ad1,Ad2,Ar0,Ar1,Ar2,Ak0,Ak1,Ak2,    &
              rhoa,rhoh,Dmh,qhf,refh_h,refh_v,roohvh,kdph,c_unit,pi,nt)
  ELSE
      refh_h=0.0;refh_v=0.0;roohvh=0.0;kdph=0.0

  END IF  !end of nt>epsN

  ! print*,'refh_h_zh===',refh_h_zh
  ! print*,'dms rhoa qsf rhos unit pi =',dmh,rhoa,qhf,rhoh,refh_h,c_unit,pi
  ! print*,'refh_h tem_zs    =',tem_zh, refh_h
  ! stop

  END SUBROUTINE rdObs_h

  SUBROUTINE rdObs_g(flg,rhoa,rhog,fwg,Dmg,qgf,                        &
                          refg_h,refg_v,roohvg,kdpg,c_unit,pi,nt,epsN)
    IMPLICIT NONE
    INTEGER :: flg
    REAL   :: rhoa,rhog,fwg,Dmg,qgf,nt
    REAL   :: refg_h,refg_v,roohvg,kdpg
    REAL   :: Az0,Az1,Az2,Az3
    REAL   :: Ad0,Ad1,Ad2,Ad3
    REAL   :: Ar0,Ar1,Ar2,Ar3
    REAL   :: Ak0,Ak1,Ak2,Ak3
    REAL   :: fwg2,fwg3,Dmg2,Dmg3,tem_zg,tem_kdp
    REAL   :: c_unit,pi,refg_h_zg
    REAL   :: epsN

    IF( nt > epsN ) THEN

      IF(flg ==1) THEN

      Az0 = 0.3756
      Az1 = -0.006341
      Az2 = 0.000634
      Az3 = -0.0000260

      Ad0 = 1.0201
      Ad1 = 0.002114
      Ad2 = -0.0000299

      Ak0 = 0.01149
      Ak1 = 0.000843
      Ak2 = -0.00006195

      Ar0 = 0.9997
      Ar1 = -0.0007775
      Ar2 = 0.00002523

      ELSE

      fwg2 = fwg*fwg
      fwg3 = fwg2*fwg

      Az0 = 0.3756     +4.5397*fwg   -7.7145*fwg2   +4.2537*fwg3
      Az1 = -0.006341  +0.3159*fwg   -1.3283*fwg2   +1.0148*fwg3
      Az2 = 0.000634   -0.04708*fwg  +0.1756*fwg2   -0.1051*fwg3
      Az3 = -0.0000260 +0.001266*fwg -0.004041*fwg2 +0.001172*fwg3

      Ad0 = 1.0201     +0.5910*fwg   -0.4174*fwg2   +0.9998*fwg3
      Ad1 = 0.002114   -0.05702*fwg  +0.3124*fwg2   -0.1830*fwg3
      Ad2 = -0.0000299 +0.003635*fwg -0.01227*fwg2  +0.0023*fwg3

      Ak0 = 0.01149    +0.7294*fwg   +1.1475*fwg2   -0.1594*fwg3
      Ak1 = 0.000843   -0.02974*fwg  +0.2448*fwg2   -0.1146*fwg3
      Ak2 = -0.00006195+0.001715*fwg -0.009643*fwg2 -0.0007701*fwg3

      Ar0 = 0.9997     -0.2033*fwg   +0.5903*fwg2   -0.4298*fwg3
      Ar1 = -0.0007775 +0.02505*fwg  -0.1047*fwg2   +0.08195*fwg3
      Ar2 = 0.00002523 -0.002283*fwg +0.006095*fwg2 -0.003489*fwg3

      END IF  ! end of flg == 1

      CALL   CalRadV(Az0,Az1,Az2,Az3,Ad0,Ad1,Ad2,Ar0,Ar1,Ar2,Ak0,Ak1,Ak2,    &
                  rhoa,rhog,Dmg,qgf,refg_h,refg_v,roohvg,kdpg,c_unit,pi,nt)
    ELSE
        refg_h=0.0;refg_v=0.0;roohvg=0.0;kdpg=0.0
    END IF ! end of IF( nt > epsN ) THEN

    !IF(refg_h>120.0) THEN
    ! print*,'Az0 Az1 Az2 Az3==',Az0,Az1,Az2,Az3
    ! print*,'rhoa,rhog,Dmg,qgf,fwg =',rhoa,rhog,Dmg,qgf,fwg
    ! !stop
    !END IF

  END SUBROUTINE rdObs_g

  SUBROUTINE CalRadV(Az0,Az1,Az2,Az3,Ad0,Ad1,Ad2,Ar0,Ar1,Ar2,Ak0,Ak1,Ak2,    &
               rhoa,rhox,Dmx,qxf,refx_h,refx_v,roohvx,kdpx,c_unit,pi,nt)
    IMPLICIT NONE
    REAL   :: Az0,Az1,Az2,Az3
    REAL   :: Ad0,Ad1,Ad2
    REAL   :: Ar0,Ar1,Ar2
    REAL   :: Ak0,Ak1,Ak2

    REAL   :: rhoa,rhox,Dmx,qxf
    REAL   :: refx_h,refx_v,roohvx,kdpx
    REAL   :: Dmx2,Dmx3,tem_zx,tem_kdp
    REAL   :: c_unit,pi, refx_h_zx, nt

    Dmx2 = Dmx*Dmx
    Dmx3 = Dmx2*Dmx
    tem_zx = Az0 +Az1*Dmx +Az2*Dmx2 +Az3*Dmx3
      refx_h_zx = 11250.0*rhoa/(pi*rhox)*Dmx3*qxf*c_unit
     !refx_h_zx = 11.25*(rhoa*qxf/pi/1.0)**2*64/nt*c_unit*c_unit
    refx_h = refx_h_zx*tem_zx*tem_zx

    !IF(refx_h>120.0) THEN
    ! print*,'                 '
    ! print*,'tem_zx refx_h_zx=',tem_zx,refx_h_zx
    ! print*,'rhoa,rhog,Dmg,qgf =',rhoa,rhox,Dmx,qxf
    ! stop
    !END IF

    refx_v = Ad0 +Ad1*Dmx +Ad2*Dmx2
    roohvx = Ar0 +Ar1*Dmx +Ar2*Dmx2

    tem_kdp= Ak0 +Ak1*Dmx +Ak2*Dmx2
    kdpx   = rhoa*tem_kdp*qxf/rhox*c_unit

  END SUBROUTINE CalRadV

  SUBROUTINE cal_Dmx(rhoa,qx,Ntx,rhoqx,Dmx,epsN,pi)
  !
  !-----------------------------------------------------------------------
  !  PURPOSE:  Calculates mean-mass diameter
  !-----------------------------------------------------------------------
  !
  !  AUTHOR: Jidong Gao
  !  (09/20/2017)
  !
  !  MODIFICATION HISTORY:
  !
  !-----------------------------------------------------------------------
  !  Variable Declarations:
  !-----------------------------------------------------------------------
  !
    REAL :: rhoa,qx
    REAL :: rhoqx
    REAL :: Ntx,Dmx
    REAL :: epsN,pi

          IF( Ntx > epsN ) THEN
            Dmx = 4*(rhoa*qx/(pi*rhoqx*Ntx))**(1./3.)*1000.0
          ELSE
            Dmx = 0.0
          END IF

  END SUBROUTINE cal_Dmx

  SUBROUTINE fractionWater2(qr,qi,fo,density_ice,fracqr,fracqi,fm,fw,rhom,ntr,nti,flg)

  !-----------------------------------------------------------------------
  !
  ! PURPOSE:
  !
  ! This subroutine calculates the fractions of water, dry ice (snow or
  ! hail), the mixture. It also calculate the density of mixture.
  !-----------------------------------------------------------------------
  !
  !-----------------------------------------------------------------------
  ! Force explicit declarations.
  !-----------------------------------------------------------------------

    IMPLICIT NONE

  !-----------------------------------------------------------------------
  ! Declare variables.
  !-----------------------------------------------------------------------

    REAL :: qr, qi, fo, density_ice
    REAL,    INTENT(OUT) :: fracqr, fracqi, fm, fw, rhom
    REAL,    INTENT(IN)  :: ntr,nti
    INTEGER, INTENT(IN)  :: flg

    REAL :: fr

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  ! Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    fr = 0.
    fw = 0.
    fracqr = 0.
    fracqi = 0.
    fm = 0.
    rhom = 0.

    IF( ntr > 0.0 ) THEN
      !print *, "ntr = ",ntr, nti
      IF( nti/ntr <1.0E-5) RETURN
    END IF

  !-----------------------------------------------------------------------
  ! Calculate the fraction of mleting ice (fr) based on the ratio between
  ! qr and qi. fo is the maximum allowable fraction of melting snow.
  !-----------------------------------------------------------------------
    IF (qr > 0. .AND. qi > 0.) THEN
      fr = fo*(MIN(qi/qr,qr/qi))**.3
    !print*,'qr qi fo fr=', qr,qi,fr,fo,qi/qr,qr/qi
    ENDIF
      IF (fr >fo) fr=fo

      fracqi = fr * qi
      fracqr = fr * qr
      IF(fracqi< 1E-9) THEN
       fracqi = 0.0
       fracqr = 0.0
       return
      END IF

      IF(fracqr< 1E-9) THEN
       fracqi = 0.0
       fracqr = 0.0
       return
      END IF
      !IF(flg==1 .and. fracqr > 100*fracqi ) fracqr= 100*fracqi
      !IF(flg==2 .and. fracqr > 100*fracqi ) fracqr= 100*fracqi
      !IF(flg==3 .and. fracqr > 100*fracqi ) fracqr= 100*fracqi
      !IF(flg==1 .and. fracqi > 100*fracqr ) fracqi= 100*fracqr
      !IF(flg==2 .and. fracqi > 100*fracqr ) fracqi= 100*fracqr
      !IF(flg==3 .and. fracqi > 100*fracqr ) fracqi= 100*fracqr
     ! IF(flg==1 .and. fracqr > 100*fracqi ) fracqr= 100*fracqi
     !IF(flg==3 .and. fracqr > 100*fracqi ) fracqr= 100*fracqi

      fm = fracqr + fracqi

    IF (fm .EQ. 0. .AND. qr > 0.) THEN
      fw = 1.
    ELSE IF (fm > 0.) THEN
      fw = fracqr/fm
    ENDIF
      IF (fw > 1.0) fw = 1.0

    rhom = 1000.*fw**2. + (1.-fw**2.)*density_ice

   !print*,'qr fracqr fracqi fw rhom=',qr, fracqr,fracqi,fw,rhom

  END SUBROUTINE fractionWater2

END MODULE module_DualFO
