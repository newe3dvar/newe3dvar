SUBROUTINE calc_300(nx,ny,nz,pbar,pprt,level300,tpwflag,cwp,hydro_ref)

  IMPLICIT NONE

  INTEGER :: nx,ny,nz,i,j,k
  REAL, DIMENSION(nx,ny,nz) :: pbar,pprt
  REAL, DIMENSION(nx,ny)    :: level300,cwp,hydro_ref
  INTEGER :: tpwflag(nx,ny)
  REAL, ALLOCATABLE :: ptemp(:,:,:)

  ALLOCATE(ptemp(nx,ny,nz))
  ptemp(:,:,:)=pbar(:,:,:)+pprt(:,:,:)

  DO i=2,nx-1
    DO j=2,ny-1
      IF (cwp(i,j)<=5E-2 .and. hydro_ref(i,j)<=5E-2) THEN
        DO k=2,nz-1
          IF (ptemp(i,j,k) < 30000.0) THEN
            level300(i,j)=k
            tpwflag(i,j)=1
            EXIT
          END IF
        END DO
      ELSE
        tpwflag(i,j)=0
      END IF
    END DO
  END DO

  DEALLOCATE(ptemp)

END SUBROUTINE calc_300

SUBROUTINE calc_tpw(nx,ny,nz,level300,qv,zps,rhobar,tpw,tpwflag)

  IMPLICIT NONE

  INTEGER :: nx,ny,nz
  REAL, DIMENSION(nx,ny,nz) :: zps,rhobar,qv
  INTEGER :: i,j,k
  REAL, DIMENSION(nx,ny) :: tpw,level300
  INTEGER :: tpwflag(nx,ny)
  REAL, PARAMETER :: rhowater=1000.0

  DO i=2,nx-1
    DO j=2,ny-1
      IF ( tpwflag(i,j) == 1 ) THEN
        DO k=2,level300(i,j)
          tpw(i,j)=tpw(i,j)+qv(i,j,k)*(zps(i,j,k+1)-zps(i,j,k))*rhobar(i,j,k)/rhowater
        END DO
        IF (tpw(i,j) < 0.0) tpw(i,j) = 0.0
      ELSE
        tpw(i,j) = 0.0
      END IF
    END DO
  END DO

END SUBROUTINE calc_tpw

