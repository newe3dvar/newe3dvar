PROGRAM zmosaic2model
!
!
!-----------------------------------------------------------------------
!
!  Program for generating a reflectivity file in ARPS/WRF grid from
!        NSSL Mosaic reflectivity
!
!  Author: Ming Hu, CAPS. University of Oklahma.
!  First written: 04/06/2006.
!
!  History:
!
!    5/15/2007 Fanyou Kong
!      Rewrite to remove mosaicfile, and to include 2D field read
!        (e.g., cref, 1h_rad_hsr, etc)
!      Also remove NAMELIST jobname (not used)
!      Add NetCDF file existence check, and uncompress if applicable
!
!    O5/16/2007 Y. Wang
!    Added check for "/" at the end of mosaicPath.
!    Changed wrtvar1 to wrtvar2.
!
!    04/20/2015 (Y. Wang)
!    Changed to read NCEP operational GRIB2 file for NSSL MRMS fields.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------

  IMPLICIT NONE

  INCLUDE 'netcdf.inc'

!--------------------------------------------------------------
!  ARPS grid
!--------------------------------------------------------------
  REAL, allocatable :: lath(:,:)    ! Latitude of each terrain point
  REAL, allocatable :: lonh(:,:)    ! Longitude of each terrain point
  REAL, allocatable :: zp(:,:,:)    ! Physical height coordinate defined at
                                    ! w-point of the staggered grid.

  REAL, allocatable :: ref3d(:,:,:) !3D reflectivity in mosaic vertical grid
  REAL, allocatable :: ref_arps_3d(:,:,:)  ! 3D reflectivity in arps grid

  REAL, allocatable :: var2d(:,:)   ! 2D variable
  REAL, allocatable :: compref(:,:) ! composite reflectivity
  REAL, allocatable :: hsr_1hr(:,:) ! 1-h accumulate precipitation

!--------------------------------------------------------------
!  Namelist
!--------------------------------------------------------------
!
! grid
!
  INTEGER :: nx, ny, nz
  REAL    :: dx, dy, dz
  REAL    :: ctrlat, ctrlon

  INTEGER :: strhopt
  REAL    :: dzmin, zrefsfc, dlayer1, dlayer2, strhtune, zflat

  INTEGER :: mapproj
  REAL    :: trulat1, trulat2, trulon, sclfct

  INTEGER :: ternopt, ternfmt
  CHARACTER(LEN=256) :: terndta


  INTEGER :: modelopt
  CHARACTER(LEN=256) :: modelfile
  NAMELIST /model_grid/ modelopt, modelfile,                            &
            nx,ny, nz, dx,dy,dz, ctrlat,ctrlon,                         &
            strhopt,dzmin,zrefsfc,dlayer1,dlayer2,strhtune,zflat,       &
            mapproj,trulat1,trulat2,trulon,sclfct,                      &
            ternopt,terndta,ternfmt
!
!  For reflectiivty mosaic
!
  CHARACTER(LEN=256) :: mosaicPath
  CHARACTER(LEN=15)  :: mosaictime(50)
  CHARACTER(LEN=256) :: mosaicTile(14)
  INTEGER :: tversion,mosaic_opt,ifcompositeRef,numvolume,ntiles

  NAMELIST /mosaicpara/mosaicPath,tversion,mosaic_opt, &
                       ifcompositeRef,numvolume,       &
                       mosaictime,ntiles,mosaicTile

  CHARACTER(LEN=256) :: dirname
  INTEGER :: lvldbg, dmpfmt

  NAMELIST /output/ dirname, dmpfmt, lvldbg

!--------------------------------------------------------------
!  Grid of NSSL mosaic
!--------------------------------------------------------------
  INTEGER ::   mscNlon   ! number of longitude of mosaic data
  INTEGER ::   mscNlat   ! number of latitude of mosaic data
  INTEGER ::   mscNlev   ! number of vertical levels of mosaic data
  REAL, allocatable :: msclon(:)        ! longitude of mosaic data
  REAL, allocatable :: msclat(:)        ! latitude of mosaic data
  REAL, allocatable :: msclev(:)        ! level of mosaic data
  REAL, allocatable :: mscValue(:,:)   ! reflectivity

  REAL :: lonMin,latMin,lonMax,latMax,dlon,dlat

  CHARACTER(LEN=80), PARAMETER :: product_name(3) = (/'no supported',   &
                                       'MergedReflectivityQCComposite', &
                                       'RadarOnly_QPE_01H'              &
                                       /)
  REAL,    PARAMETER :: hgtmsl(3)   = (/ 0.0, 0.5, 0.0 /)
  INTEGER, PARAMETER :: varids(4,3) =  RESHAPE(         & ! order is IMPORTANT
                   ! Discipline  Category  Parameter Layer/Level_Type
                        (/ 209,   3,   5, 100,    & ! 1 xxxx
                           209,  10,   0, 102,    & ! 2 MergedReflectivityQCComposite
                           209,   6,   2, 102     & ! 3 RadarOnly_QPE_01H
                         /), (/ 4, 3 /) )

!-----------------------------------------------------------------------
!
!  WRF VARIABLES
!
!-----------------------------------------------------------------------
!
  INTEGER :: nx_wrf,ny_wrf,nz_wrf,nzsoil_wrf,nt,nxlg_wrf,nylg_wrf
  INTEGER :: iproj_wrf

!-----------------------------------------------------------------------
!
!  LOCAL VARIABLES:
!
!-----------------------------------------------------------------------
!
  CHARACTER*256 tempfile
  CHARACTER(LEN=6) :: tmpstr

  INTEGER :: lfnkey

  INTEGER :: NCID

  LOGICAL :: fexist

  INTEGER :: maxlvl

  REAL    :: swx,swy,ctrx,ctry    ! Temporary variables.

  INTEGER :: i,j,k,ifl,im,ifn

  REAL    :: rlon,rlat
  INTEGER :: ip,jp
  REAL    :: rip,rjp
  REAL    :: dip,djp
  REAL    :: w1,w2,w3,w4
  REAL    :: ref1,ref2,ref3,ref4
  INTEGER :: iyr,imon,iday,ihr,imin,isec

  INTEGER :: istatus
  REAL    :: amax,amin

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  WRITE(6,'(/11(/5x,a)/)')                                              &
     '###############################################################', &
     '###############################################################', &
     '###                                                         ###', &
     '###                 Welcome to ZMOSAIC2MODEL                ###', &
     '###      This program reads in NSSL Mosaic reflectivity     ###', &
     '###      and generates a reflectivity file in ARPS/WRF      ###', &
     '###      grid.                                              ###', &
     '###                                                         ###', &
     '###############################################################', &
     '###############################################################'
!
!-----------------------------------------------------------------------
!
!  Set up the default values for all the variables to be read in
!  using the namelist method. In case the user does not specify a
!  particular value, this value will be used.
!
!-----------------------------------------------------------------------
!
  nx = 67
  ny = 67
  nz = 53
!  runname = 'may20'
  dx = 1000.0
  dy = 1000.0
  dz = 400.0

  ctrlat  =  35.0
  ctrlon  = -100.0

  mapproj  = 0
  trulat1  =   30.0
  trulat2  =   60.0
  trulon   = -100.0
  sclfct   =    1.0
!
!-----------------------------------------------------------------------
!
!  Read and Print of Namelist variables and parameters.
!
!-----------------------------------------------------------------------
!
!  READ (5,jobname,ERR=980,END=990)
!  WRITE(6,'(/a,a)') 'The name of this run is: ', runname

  READ (5,mosaicpara,ERR=980,END=990)
  WRITE(6,*) 'Mosaic option:   ', mosaic_opt
  WRITE(6,*) 'Number of times: ', numvolume
  IF (tversion > 0) WRITE(6,*) 'Number of tiles: ', ntiles

  READ(5,model_grid,ERR=980,END=990)

  READ(5,output,ERR=980,END=990)

  lfnkey = LEN_TRIM(mosaicpath)
  IF (mosaicpath(lfnkey:lfnkey) /= '/') THEN
  	tempfile = mosaicpath
  	WRITE(mosaicpath,'(2a)') TRIM(tempfile), '/'
  END IF

!-----------------------------------------------------------------------
!
!  Decode model grid from modelfile
!
!-----------------------------------------------------------------------

  IF (modelopt == 2) THEN

    nt = -99
    CALL get_wrf_domsize(TRIM(modelfile),tmpstr,nx_wrf,ny_wrf,          &
                         nz_wrf,nzsoil_wrf,nt,nxlg_wrf,nylg_wrf,istatus)

    WRITE(6,'(/a,2(2(a,I3)))') '  WRF grid dimensions: ',               &
                    'nx = ',nxlg_wrf, ', ny= ',nylg_wrf,', nz= ',nz
    WRITE(6,'(a,I0)') '  WRF Time = ', nt

    CALL get_wrf_grid( TRIM(modelfile),iproj_wrf,sclfct,trulat1,trulat2,  &
                       trulon,ctrlat,ctrlon,dx,dy,nt,istatus )

    IF(iproj_wrf == 0) THEN        ! NO MAP PROJECTION
      mapproj = 0
    ELSE IF(iproj_wrf == 1) THEN   ! LAMBERT CONFORMAL
      mapproj = 2
    ELSE IF(iproj_wrf == 2) THEN   ! POLAR STEREOGRAPHIC
      mapproj = 1
    ELSE IF(iproj_wrf == 3) THEN   ! MERCATOR
      mapproj = 3
    ELSE
      WRITE(6,*) 'Unknown map projection, ', iproj_wrf
      istatus = -555
    END IF

    nx = nxlg_wrf+2
    ny = nylg_wrf+2
    nz =   nz_wrf+2

  END IF

!-----------------------------------------------------------------------
!
!  get the latitude and longitude of ARPS domain
!
!-----------------------------------------------------------------------

  ALLOCATE(lath(0:nx,0:ny), STAT = istatus)
  ALLOCATE(lonh(0:nx,0:ny), STAT = istatus)
  ALLOCATE(compref(nx,ny),  STAT = istatus)
  ALLOCATE(hsr_1hr(nx,ny),  STAT = istatus)

  WRITE(*,'(/,1x,a,/)') ' Calling getcoordinate ...'
  CALL getcoordinate(nx,ny,dx,dy,ctrlat,ctrlon,                         &
                     mapproj,trulat1,trulat2,trulon,                    &
                     lath,lonh,istatus)

  CALL a3dmax0(lath,0,nx,0,nx,0,ny,0,ny,1,1,1,1,latmax,latmin)
  CALL a3dmax0(lonh,0,nx,0,nx,0,ny,0,ny,1,1,1,1,lonmax,lonmin)
  WRITE(*,*) 'lonmax,lonmin = ',lonmax,lonmin
  WRITE(*,*) 'latmax,latmin = ',latmax,latmin
  IF(mosaic_opt == 1) THEN
    allocate(zp(nx,ny,nz),              STAT = istatus)
    allocate(ref_arps_3d(nx,ny,nz),     STAT = istatus)
    IF (modelopt == 2) THEN
      CALL getVertcoordinate_wrf(modelfile,nx_wrf,ny_wrf,nz_wrf,        &
                                 nx,ny,nz,zp,istatus)
    ELSE
      CALL getVertcoordinate_arps(nx,ny,nz,dx,dy,dz,                    &
                   mapproj,trulat1,trulat2,trulon,sclfct,ctrlat,ctrlon, &
                   zrefsfc,strhopt,dlayer1,dlayer2,dzmin,strhtune,zflat,&
                   ternopt,ternfmt,terndta,zp)
    END IF
  END IF
!-----------------------------------------------------------------------
!
!  begin to read in mosaic file
!
!-----------------------------------------------------------------------

  IF (tversion == 0) THEN
    maxlvl  = 1    ! expected level(s) in one file
    ALLOCATE(ref3d(nx,ny,maxlvl), STAT = istatus)
    ref3d=-99999.0

    mscNlon = 7000
    mscNlat = 3500
    mscNlev = 33

    dlat = 0.01
    dlon = 0.01
    ALLOCATE(mscValue(mscNlon,mscNlat), STAT = istatus)
  END IF

  nv: DO ifl=1, numvolume
    !
    !  get dimension of mosaic field and allocate arrays
    !
    IF (tversion > 0 ) THEN   ! Tiled NSSL research system

      DO im=1, ntiles
        tempfile=trim(mosaicPath)//trim(mosaictime(ifl))//trim(mosaicTile(im))
        write(*,*) 'process file No.',im,'with name:',trim(tempfile)

        INQUIRE(FILE=trim(tempfile), EXIST = fexist )
        IF( .NOT.fexist) THEN
          INQUIRE(FILE=trim(tempfile)//'.gz', EXIST = fexist )
            IF(fexist) THEN
              CALL uncmprs(trim(tempfile)//'.gz')
            ELSE
              PRINT *,trim(tempfile),' or its .gz file does not exist, SKIP'
              CYCLE nv
            END IF
        END IF

         IF( tversion == 14 ) then
           call GET_DIM_ATT_Mosaic(tempfile,mscNlon,mscNlat,mscNlev, &
                     lonMin,latMin,lonMax,latMax,dlon,dlat)
         ELSEIF( tversion == 8 ) then
           call GET_DIM_ATT_Mosaic8(tempfile,mscNlon,mscNlat,mscNlev, &
                     lonMin,latMin,lonMax,latMax,dlon,dlat,mosaic_opt)

         ELSE
           write(*,*) ' unknown tile version !!!'
           stop 123
         ENDIF

         allocate(msclon(mscNlon))
         allocate(msclat(mscNlat))
         allocate(msclev(mscNlev))
         allocate(mscValue(mscNlon,mscNlat))

         if(im == 1) then
           maxlvl=mscNlev
           allocate(ref3d(nx,ny,maxlvl))
           ref3d=-99999.0
         else
           if(maxlvl .ne. mscNlev) then
              write(*,*) ' The vertical dimensions are different in two tiles'
              stop
           endif
         endif

         msclon(1)=lonMin
         DO i=1,mscNlon-1
           msclon(i+1)=msclon(i)+dlon
         ENDDO
         msclat(1)=latMin
         DO i=1,mscNlat-1
           msclat(i+1)=msclat(i)+dlat
         ENDDO
  !
  !  ingest mosaic file and interpolation
  !
         call OPEN_Mosaic(tempfile, NCID)

         if(tversion == 14 ) then
           call Check_DIM_ATT_Mosaic(NCID,mscNlon,mscNlat,mscNlev,  &
                 lonMin,latMin,lonMax,latMax,dlon,dlat)
         elseif(tversion == 8 ) then
           call Check_DIM_ATT_Mosaic8(NCID,mscNlon,mscNlat,mscNlev,  &
                 lonMin,latMin,lonMax,latMax,dlon,dlat,mosaic_opt)
         endif

         DO k=1, mscNlev

           IF(mosaic_opt == 1) THEN
           call  GET_Mosaic_sngl_Mosaic(NCID,mscNlon,mscNlat,k,mscValue)
           ELSE IF(mosaic_opt == 2) THEN
           call  GET_Mosaic_cref_Mosaic(NCID,mscNlon,mscNlat,mscValue)
           ELSE IF(mosaic_opt == 3) THEN
           call  GET_Mosaic_hsr_Mosaic(NCID,mscNlon,mscNlat,mscValue)
           ELSE
             print *,'mosaic_opt=',mosaic_opt,' is invalid, STOP!!!'
             stop
           END IF

  !         DO j=0,ny
  !         DO i=0,nx
           DO j=1,ny
           DO i=1,nx
              rlat=lath(i,j)
              rlon=lonh(i,j)

              if(tversion == 14 ) then
                 rip=(rlon-lonMin)/dlon+1
                 rjp=(rlat-latMin)/dlat+1
                 ip=int(rip)
                 jp=int(rjp)
                 dip=rip-ip
                 djp=rjp-jp
              elseif(tversion == 8 ) then
                 rip=(rlon-lonMin)/dlon+1
                 rjp=(latMax-rlat)/dlat+1
                 ip=int(rip)
                 jp=int(rjp)
                 dip=rip-ip
                 djp=rjp-jp
              else
                 write(*,*) ' Unknown Mosaic format !!'
                 stop 123
              endif

              if( ip >= 1 .and. ip < mscNlon ) then
              if( jp >= 1 .and. jp < mscNlat ) then
  ! inside mosaic domain
                w1=(1.0-dip)*(1.0-djp)
                w2=dip*(1.0-djp)
                w3=dip*djp
                w4=(1.0-dip)*djp
                ref1=mscValue(ip,jp)
                ref2=mscValue(ip+1,jp)
                ref3=mscValue(ip+1,jp+1)
                ref4=mscValue(ip,jp+1)
                if(ref1 > -500.0 .and. ref2 > -500.0 .and.  &
                 ref3 > -500.0 .and. ref4 > -500.0 ) then
                   ref3d(i,j,k)=(ref1*w1+ref2*w2+ref3*w3+ref4*w4)/10.0
                endif
              endif
              endif
           ENDDO
           ENDDO
         ENDDO ! mscNlev
  !
         call CLOSE_Mosaic(NCID)

        deallocate(msclon)
        deallocate(msclat)
        deallocate(msclev)
        deallocate(mscValue)

      ENDDO   ! ntiles
    ELSE

      WRITE(tmpstr,'(F6.2)') hgtmsl(mosaic_opt)+100.

      WRITE(tempfile,'(4a,a,3a)') TRIM(mosaicPath), 'MRMS_',            &
        TRIM(product_name(mosaic_opt)),'_',tmpstr(2:),'_',mosaictime(ifl),'.grib2'

      WRITE(*,'(/,1x,a,i3,2a,/)') 'Reading file No.',ifl,' : ',TRIM(tempfile)

      ifn = LEN_TRIM(tempfile)
      CALL rdmrmsgrib2(mscNlon,mscNlat,maxlvl,tempfile,ifn,mosaictime(ifl), &
                       1, mscNlon, 1, mscNlat, dlon, dlat,              &
                       varids(:,mosaic_opt), hgtmsl(mosaic_opt)*1000.,  &
                       latMin,lonMin,mscValue,lvldbg,istatus)
      IF (istatus /= 0) THEN
        WRITE(*,'(1x,2a,/)') 'ERROR: reading grib2 file in rdmrmsgrib2 for ',mosaictime(ifl)
        CYCLE
      END IF

      IF (lonmin > 180) lonmin = lonmin-360.

      !latmin = latmin + (jy1-1)*dlat
      !lonmin = lonmin + (ix1-1)*dlon

      IF (lvldbg > 0) THEN
        WRITE(*,'(1x,a,2F10.3)') 'lonmin,latmin = ',lonmin,latmin
      END IF
      !
      ! Interpolat horizontally to ARPS grid
      !
      k = 1
      DO j=1,ny
        DO i=1,nx
          rlat=lath(i,j)
          rlon=lonh(i,j)

          rip=(rlon-lonMin)/dlon+1
          rjp=(rlat-latMin)/dlat+1
          ip=int(rip)
          jp=int(rjp)
          dip=rip-ip
          djp=rjp-jp

          IF( ip >= 1 .and. ip < mscNlon ) then
            IF( jp >= 1 .and. jp < mscNlat ) then
              ! inside mosaic domain
              w1=(1.0-dip)*(1.0-djp)
              w2=dip*(1.0-djp)
              w3=dip*djp
              w4=(1.0-dip)*djp
              ref1=mscValue(ip,jp)
              ref2=mscValue(ip+1,jp)
              ref3=mscValue(ip+1,jp+1)
              ref4=mscValue(ip,jp+1)
              IF( ref1 > -500.0 .AND. ref2 > -500.0 .AND.               &
                  ref3 > -500.0 .AND. ref4 > -500.0 ) THEN
                ref3d(i,j,k)=(ref1*w1+ref2*w2+ref3*w3+ref4*w4)
              END IF
            END IF
          END IF
        END DO
      END DO

    END IF

    WRITE(*,'(/,1x,2a)') 'Successfully read and processed mosaic data at ', trim(mosaictime(ifl))

    CALL a3dmax0(ref3d(:,:,1),1,nx,1,nx,1,ny,1,ny,1,1,1,1, amax,amin)
    PRINT *,'var2d -- amax, amin: ',amax, amin

    IF(mosaic_opt == 1) THEN
      CALL vert_interp_ref(nx,ny,nz,mscNlev,ref3d,ref_arps_3d,zp)
      WRITE(*,*) 'successfully get reflectivity in ARPS grid at ', &
                 trim(mosaictime(ifl))
      !
      !  dump out colume data for data analysis
      !
      READ(mosaictime(ifl),'(i4,i2,i2,1x,i2,i2)') iyr,imon,iday,ihr,imin
      isec = 0
      call wtradcol_Mosaic(nx,ny,nz,mosaictime(ifl),1,                  &
                           iyr,imon,iday,ihr,imin,isec,                 &
                           lath,lonh,zp,ref_arps_3d)

      ref_arps_3d = -99999.0
      call readadcol_Mosaic(nx,ny,nz,mosaictime(ifl),ref_arps_3d)
      !
      ! get composite reflectivity
      !
      IF (ifcompositeRef==1) THEN
        compref=-99999.0
        DO k=2, nz-1
          DO j=1,ny
            DO i=1,nx
              compref(i,j)=max(compref(i,j),ref_arps_3d(i,j,k))
            ENDDO
          ENDDO
        ENDDO
        !
        !  dump out composite reflectivity
        !
        DO j=1,ny
          DO i=1,nx
            if( compref(i,j) < 0.0 ) compref(i,j)=0
            if( compref(i,j) > 100.0 )  write(*,*) i,j,compref(i,j)
          ENDDO
        ENDDO

        CALL wrtvar2(nx,ny,1,compref, 'compst','composite reflectivity', &
             'dBZ',0,mosaictime(ifl),dirname,dmpfmt,5,0,istatus)

      END IF  ! ifcompositeRef=1

    ELSE IF(mosaic_opt == 2) THEN
      DO j=1,ny
        DO i=1,nx
          compref(i,j) = max(ref3d(i,j,1), 0.0)
        ENDDO
      ENDDO
      CALL wrtvar2(nx,ny,1,compref, 'compst','composite reflectivity', &
           'dBZ',0,mosaictime(ifl),dirname,dmpfmt,5,0,istatus)
    ELSE IF(mosaic_opt == 3) THEN
      DO j=1,ny
        DO i=1,nx
          !hsr_1hr(i,j) = max(ref3d(i,j,1)/25.4, 0.0)  ! convert to inch
          hsr_1hr(i,j) = max(ref3d(i,j,1), 0.0) ! keep mm unit
        ENDDO
      ENDDO
      call wrtvar2(nx,ny,1,hsr_1hr, 'hsr1hr','1-h precipitation', &
           !'in',0,mosaictime(ifl),dirname,dmpfmt,0,0,iSTATUS)
           'mm',0,mosaictime(ifl),dirname,dmpfmt,5,0,istatus)
    ELSE
      WRITE(*,'(1x,a,I0,2a)') 'ERROR: mosaic_opt = ',mosaic_opt,' invalid for ',trim(mosaictime(ifl))
      CYCLE
    END IF


  222 CONTINUE

  END DO nv   !numvolume

  IF (tversion == 0) THEN
    DEALLOCATE(ref3d)
    DEALLOCATE(mscValue)
  END IF

  DEALLOCATE(lath)
  DEALLOCATE(lonh)
  DEALLOCATE(compref)
  DEALLOCATE(hsr_1hr)
  IF(mosaic_opt == 1) THEN
    DEALLOCATE(zp)
    DEALLOCATE(ref_arps_3d)
  END IF

  CALL exit(0)

  980   CONTINUE
  WRITE(6,'(/1x,a,a)')                                                  &
      'Error occured when reading namelist input file. Program stopped.'

  CALL exit(980)

  990   CONTINUE
  WRITE(6,'(/1x,a,a)')                                                  &
      'End of file reached when reading namelist input file. ',         &
      'Program stopped.'

  CALL exit(990)

  981   CONTINUE
  WRITE(6,'(/1x,a,a)')                                                  &
      'Error occured when reading mosaic file name. Program stopped.'

  CALL exit(981)

  991   CONTINUE
  WRITE(6,'(/1x,a,a)')                                                  &
      'End of file reached when reading mosaic file path. ',            &
      'Program stopped.'
  CALL exit(991)

  STOP
END PROGRAM zmosaic2model
