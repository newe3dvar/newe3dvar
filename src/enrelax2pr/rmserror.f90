SUBROUTINE rmserror(nx,ny,nz,nen,                                    &
                    uen,ven,wen,ptprten,pprten,                      &  
                    qven,qcen,qren,qien,qsen,qhen,                   &  
                    umean,vmean,wmean,ptprtmean,pprtmean,            &  
                    qvmean,qcmean,qrmean,qimean,qsmean,qhmean,       &  
                    uo,vo,wo,ptprto,pprto,qvo,qco,qro,qio,qso,qho,   &  
                    reflo,typeer,                                    &
                    sua,sva,swa,sptprta,spprta,sqva,                 &
                    sqca,sqra,sqia,sqsa,sqha )
!  
!-----------------------------------------------------------------------  
!  
!  PURPOSE:  
!  
!  Compute the ensemble mean error and ensemble spread  
!  
!-----------------------------------------------------------------------  
!  
  IMPLICIT NONE  

  INTEGER :: nx,ny,nz,nen    
  INTEGER :: typeer  
  REAL :: uen(nx,ny,nz,nen)  
  REAL :: ven(nx,ny,nz,nen)  
  REAL :: wen(nx,ny,nz,nen)  
  REAL :: ptprten(nx,ny,nz,nen)  
  REAL :: pprten(nx,ny,nz,nen)  
  REAL :: qven(nx,ny,nz,nen)  
  REAL :: qcen(nx,ny,nz,nen)  
  REAL :: qren(nx,ny,nz,nen)  
  REAL :: qien(nx,ny,nz,nen)  
  REAL :: qsen(nx,ny,nz,nen)  
  REAL :: qhen(nx,ny,nz,nen)  
  
  REAL :: umean(nx,ny,nz)  
  REAL :: vmean(nx,ny,nz)  
  REAL :: wmean(nx,ny,nz)  
  REAL :: ptprtmean(nx,ny,nz)  
  REAL :: pprtmean(nx,ny,nz)  
  REAL :: qvmean(nx,ny,nz)  
  REAL :: qcmean(nx,ny,nz)  
  REAL :: qrmean(nx,ny,nz)  
  REAL :: qimean(nx,ny,nz)  
  REAL :: qsmean(nx,ny,nz)  
  REAL :: qhmean(nx,ny,nz)  
  
  REAL :: uo(nx,ny,nz)         !x component of velocity of ref run  
  REAL :: vo(nx,ny,nz)         !y component of velocity of ref run  
  REAL :: wo(nx,ny,nz)         !z component of velocity of ref run  
  REAL :: ptprto(nx,ny,nz)  
  REAL :: pprto(nx,ny,nz)  
  REAL :: qvo(nx,ny,nz)  
  REAL :: qco(nx,ny,nz)  
  REAL :: qro(nx,ny,nz)  
  REAL :: qio(nx,ny,nz)  
  REAL :: qso(nx,ny,nz)  
  REAL :: qho(nx,ny,nz)  
  REAL :: reflo(nx,ny,nz)  
  
  REAL :: eu   
  REAL :: ev   
  REAL :: ew   
  REAL :: eptprt   
  REAL :: epprt   
  REAL :: eqv   
  REAL :: eqc   
  REAL :: eqr   
  REAL :: eqi   
  REAL :: eqs   
  REAL :: eqh   
  
  REAL :: su(nx,ny,nz)  
  REAL :: sv(nx,ny,nz)  
  REAL :: sw(nx,ny,nz)  
  REAL :: sptprt(nx,ny,nz)  
  REAL :: spprt(nx,ny,nz)  
  REAL :: sqv(nx,ny,nz)  
  REAL :: sqc(nx,ny,nz)  
  REAL :: sqr(nx,ny,nz)  
  REAL :: sqi(nx,ny,nz)  
  REAL :: sqs(nx,ny,nz)  
  REAL :: sqh(nx,ny,nz)  
  
  REAL :: sua,sva,swa,sptprta,spprta  
  REAL :: sqva,sqca,sqra  
  REAL :: sqia,sqsa,sqha  

  INTEGER :: tmp  
  INTEGER :: i,j,k,l  
  
!  
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
!  
!  Beginning of executable code...  
!  
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
!-----------------------------------------------------------------------  
! 1. compute the ensemble mean error  
!-----------------------------------------------------------------------  
  eu=0.0  
  ev=0.0  
  ew=0.0  
  eptprt=0.0  
  epprt=0.0  
  eqv=0.0  
  eqc=0.0  
  eqr=0.0  
  eqi=0.0  
  eqs=0.0  
  eqh=0.0  
  tmp=0  
  
  do k=1,nz-1  
   do j=1,ny-1  
    do i=1,nx-1  
     if(reflo(i,j,k) > 10.0)then  
     eu=eu+(umean(i,j,k)-uo(i,j,k))**2    
     ev=ev+(vmean(i,j,k)-vo(i,j,k))**2  
     ew=ew+(wmean(i,j,k)-wo(i,j,k))**2  
     eptprt=eptprt+(ptprtmean(i,j,k)-ptprto(i,j,k))**2  
     epprt=epprt+(pprtmean(i,j,k)-pprto(i,j,k))**2  
     eqv=eqv+(qvmean(i,j,k)*1000.-qvo(i,j,k)*1000.)**2  
     eqc=eqc+(qcmean(i,j,k)*1000.-qco(i,j,k)*1000.)**2  
     eqr=eqr+(qrmean(i,j,k)*1000.-qro(i,j,k)*1000.)**2  
     eqi=eqi+(qimean(i,j,k)*1000.-qio(i,j,k)*1000.)**2  
     eqs=eqs+(qsmean(i,j,k)*1000.-qso(i,j,k)*1000.)**2  
     eqh=eqh+(qhmean(i,j,k)*1000.-qho(i,j,k)*1000.)**2  
     tmp=tmp+1  
     endif  
    enddo  
   enddo  
  enddo    
  
  eu=sqrt(eu/tmp)  
  ev=sqrt(ev/tmp)  
  ew=sqrt(ew/tmp)  
  eptprt=sqrt(eptprt/tmp)  
  epprt=sqrt(epprt/tmp)  
  eqv=sqrt(eqv/tmp)  
  eqc=sqrt(eqc/tmp)  
  eqr=sqrt(eqr/tmp)  
  eqi=sqrt(eqi/tmp)  
  eqs=sqrt(eqs/tmp)  
  eqh=sqrt(eqh/tmp)  
  write(*,*)'tmp_error',tmp  
  
!-----------------------------------------------------------------------  
! 2. compute the ensemble spread  
!-----------------------------------------------------------------------  
  CALL spread(nx,ny,nz,nen,uen,ven,wen,pprten,ptprten,qven,qcen,      &
              qren,qien,qsen,qhen,umean,vmean,wmean,pprtmean,     &
              ptprtmean,qvmean,qcmean,qrmean,qimean,qsmean,qhmean,&
              su,sv,sw,spprt,sptprt,sqv,sqc,sqr,sqi,sqs,sqh)
!-----------------------------------------------------------------------  
! 3. compute the average ensemble mean error and ensemble spread  
!-----------------------------------------------------------------------  
  CALL aver(nx,ny,nz,su,reflo,sua,tmp) 
  CALL aver(nx,ny,nz,sv,reflo,sva,tmp)
  CALL aver(nx,ny,nz,sw,reflo,swa,tmp)
  CALL aver(nx,ny,nz,spprt,reflo,spprta,tmp)
  CALL aver(nx,ny,nz,sptprt,reflo,sptprta,tmp)
  CALL aver(nx,ny,nz,sqv,reflo,sqva,tmp)
  CALL aver(nx,ny,nz,sqc,reflo,sqca,tmp)
  CALL aver(nx,ny,nz,sqr,reflo,sqra,tmp)
  CALL aver(nx,ny,nz,sqi,reflo,sqia,tmp)
  CALL aver(nx,ny,nz,sqs,reflo,sqsa,tmp)
  CALL aver(nx,ny,nz,sqh,reflo,sqha,tmp)

!----------------------------------------------------------------------------  
! 4. output into data file  
!----------------------------------------------------------------------------  
  if(typeer == 1)then  
  open(unit=50,file='emf.dat',position='APPEND') 
  open(unit=51,file='esf.dat',position='APPEND') 
  write(50,*)eu,ev,ew,eptprt,epprt,eqv,eqc,eqr,eqi,eqs,eqh  
  write(51,*)sua,sva,swa,sptprta,spprta,sqva,sqca,sqra,sqia,sqsa,sqha  
  close(50)  
  close(51)  
  else  
  open(unit=60,file='ema.dat',position='APPEND')  
  open(unit=61,file='esa.dat',position='APPEND')  
  write(60,*)eu,ev,ew,eptprt,epprt,eqv,eqc,eqr,eqi,eqs,eqh  
  write(61,*)sua,sva,swa,sptprta,spprta,sqva,sqca,sqra,sqia,sqsa,sqha  
  close(60)  
  close(61)  
  endif  
  return 
END SUBROUTINE rmserror  
  
SUBROUTINE spread(nx,ny,nz,nen,uen,ven,wen,pprten,ptprten,qven,qcen,      &
                  qren,qien,qsen,qhen,umean,vmean,wmean,pprtmean,     &
                  ptprtmean,qvmean,qcmean,qrmean,qimean,qsmean,qhmean,&
                  su,sv,sw,spprt,sptprt,sqv,sqc,sqr,sqi,sqs,sqh)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate ensemble spread
!
!-----------------------------------------------------------------------

  IMPLICIT NONE

! INCLUDE 'enkf.inc'
!
!
! input:
!
  INTEGER :: nx,ny,nz,nen
  REAL :: uen(nx,ny,nz,nen)
  REAL :: ven(nx,ny,nz,nen)
  REAL :: wen(nx,ny,nz,nen)
  REAL :: ptprten(nx,ny,nz,nen)
  REAL :: pprten(nx,ny,nz,nen)
  REAL :: qven(nx,ny,nz,nen)
  REAL :: qcen(nx,ny,nz,nen)
  REAL :: qren(nx,ny,nz,nen)
  REAL :: qien(nx,ny,nz,nen)
  REAL :: qsen(nx,ny,nz,nen)
  REAL :: qhen(nx,ny,nz,nen)

  REAL :: umean(nx,ny,nz)
  REAL :: vmean(nx,ny,nz)
  REAL :: wmean(nx,ny,nz)
  REAL :: ptprtmean(nx,ny,nz)
  REAL :: pprtmean(nx,ny,nz)
  REAL :: qvmean(nx,ny,nz)
  REAL :: qcmean(nx,ny,nz)
  REAL :: qrmean(nx,ny,nz)
  REAL :: qimean(nx,ny,nz)
  REAL :: qsmean(nx,ny,nz)
  REAL :: qhmean(nx,ny,nz)

!
! output:
!
  REAL :: su(nx,ny,nz)
  REAL :: sv(nx,ny,nz)
  REAL :: sw(nx,ny,nz)
  REAL :: sptprt(nx,ny,nz)
  REAL :: spprt(nx,ny,nz)
  REAL :: sqv(nx,ny,nz)
  REAL :: sqc(nx,ny,nz)
  REAL :: sqr(nx,ny,nz)
  REAL :: sqi(nx,ny,nz)
  REAL :: sqs(nx,ny,nz)
  REAL :: sqh(nx,ny,nz)

!
! local
!
INTEGER :: i, j, k, l

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  su=0.0
  sv=0.0
  sw=0.0
  sptprt=0.0
  spprt=0.0
  sqv=0.0
  sqc=0.0
  sqr=0.0
  sqi=0.0
  sqs=0.0
  sqh=0.0

  do k=1,nz-1
   do j=1,ny-1
    do i=1,nx-1
     do l=1,nen
       su(i,j,k)=su(i,j,k)+(uen(i,j,k,l)-umean(i,j,k))**2
       sv(i,j,k)=sv(i,j,k)+(ven(i,j,k,l)-vmean(i,j,k))**2
       sw(i,j,k)=sw(i,j,k)+(wen(i,j,k,l)-wmean(i,j,k))**2
       sptprt(i,j,k)=sptprt(i,j,k)+(ptprten(i,j,k,l)-ptprtmean(i,j,k))**2
       spprt(i,j,k)=spprt(i,j,k)+(pprten(i,j,k,l)-pprtmean(i,j,k))**2
       sqv(i,j,k)=sqv(i,j,k)+(qven(i,j,k,l)*1000.-qvmean(i,j,k)*1000.)**2
       sqc(i,j,k)=sqc(i,j,k)+(qcen(i,j,k,l)*1000.-qcmean(i,j,k)*1000.)**2
       sqr(i,j,k)=sqr(i,j,k)+(qren(i,j,k,l)*1000.-qrmean(i,j,k)*1000.)**2
       sqi(i,j,k)=sqi(i,j,k)+(qien(i,j,k,l)*1000.-qimean(i,j,k)*1000.)**2
       sqs(i,j,k)=sqs(i,j,k)+(qsen(i,j,k,l)*1000.-qsmean(i,j,k)*1000.)**2
       sqh(i,j,k)=sqh(i,j,k)+(qhen(i,j,k,l)*1000.-qhmean(i,j,k)*1000.)**2
     enddo
     su(i,j,k)=sqrt(su(i,j,k)/(nen-1))
     sv(i,j,k)=sqrt(sv(i,j,k)/(nen-1))
     sw(i,j,k)=sqrt(sw(i,j,k)/(nen-1))
     sptprt(i,j,k)=sqrt(sptprt(i,j,k)/(nen-1))
     spprt(i,j,k)=sqrt(spprt(i,j,k)/(nen-1))
     sqv(i,j,k)=sqrt(sqv(i,j,k)/(nen-1))
     sqc(i,j,k)=sqrt(sqc(i,j,k)/(nen-1))
     sqr(i,j,k)=sqrt(sqr(i,j,k)/(nen-1))
     sqi(i,j,k)=sqrt(sqi(i,j,k)/(nen-1))
     sqs(i,j,k)=sqrt(sqs(i,j,k)/(nen-1))
     sqh(i,j,k)=sqrt(sqh(i,j,k)/(nen-1))
    enddo
   enddo
  enddo

END
  
SUBROUTINE aver(nx,ny,nz,sc,reflo,sca,tmp)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate ensemble spread
!
!-----------------------------------------------------------------------

IMPLICIT NONE

!
! input:
!
INTEGER :: nx, ny, nz
REAL :: sc(nx,ny,nz)
REAL :: reflo(nx,ny,nz)
!
! output:
!
REAL :: sca
INTEGER :: tmp
!
! local
!
INTEGER :: i, j, k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  sca=0.0
  tmp=0 
  do k=1,nz-1
   do j=1,ny-1
    do i=1,nx-1
     if(reflo(i,j,k) > 10.0)then
     sca=sca+sc(i,j,k)
     tmp=tmp+1
     endif
    enddo
   enddo
  enddo

  sca=sca/tmp
  
END 
