MODULE Model4DARRAYA
!-----------------------------------------------------------------------
!
! PURPOSE: used for hybrid ensemble input data
!
!-----------------------------------------------------------------------

  IMPLICIT NONE
  SAVE

!-----------------------------------------------------------------------
!  Ensemble arrays:
!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!  Ensemble arrays:
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE, TARGET :: ua      (:,:,:,:) ! Total u-velocity (m/s)
  REAL, ALLOCATABLE, TARGET :: va      (:,:,:,:) ! Total v-velocity (m/s)
  REAL, ALLOCATABLE, TARGET :: wa      (:,:,:,:) ! Total w-velocity (m/s)
  REAL, ALLOCATABLE, TARGET :: ptprta  (:,:,:,:) ! Perturbation potential temperature (K)
  REAL, ALLOCATABLE, TARGET :: pprta   (:,:,:,:) ! Perturbation pressure (Pascal)
  REAL, ALLOCATABLE, TARGET :: pha     (:,:,:,:) ! Perturbation Geopotential in WRF
! REAL, ALLOCATABLE, TARGET :: mua     (:,:,:)  ! perturbation dry air mass in column (Pa)
  REAL, ALLOCATABLE, TARGET :: qva     (:,:,:,:) ! Water vapor specific humidity (kg/kg)
  REAL, ALLOCATABLE, TARGET :: qscalara(:,:,:,:,:)
  REAL, ALLOCATABLE :: tkea     (:,:,:,:) ! Turbulent Kinetic Energy ((m/s)**2)

  REAL, ALLOCATABLE :: tsoila   (:,:,:,:,:)! Deep soil temperature (K)
  REAL, ALLOCATABLE :: qsoila   (:,:,:,:,:)! Deep soil temperature (K)
  REAL, ALLOCATABLE :: wetcanpa (:,:,:,:)! Canopy water amount
  REAL, ALLOCATABLE :: snowdptha(:,:,:) ! Snow depth (m)
  REAL, ALLOCATABLE :: rainga   (:,:,:)   ! Grid supersaturation rain
  REAL, ALLOCATABLE :: rainca   (:,:,:)   ! Cumulus convective rain
  REAL, ALLOCATABLE :: prcratea (:,:,:,:)! precipitation rate (kg/(m**2*s))
  REAL, ALLOCATABLE :: radfrca  (:,:,:,:) ! Radiation forcing (K/s)
  REAL, ALLOCATABLE :: radswa   (:,:,:)   ! Solar radiation reaching the surface
  REAL, ALLOCATABLE :: rnflxa   (:,:,:)   ! Net radiation flux absorbed by surface
  REAL, ALLOCATABLE :: radswneta(:,:,:) ! Net solar radiation, SWin - SWout
  REAL, ALLOCATABLE :: radlwina (:,:,:) ! Incoming longwave radiation
  REAL, ALLOCATABLE :: usflxa   (:,:,:) ! Surface flux of u-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE :: vsflxa   (:,:,:) ! Surface flux of v-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE :: ptsflxa  (:,:,:) ! Surface heat flux (K*kg/(m*s**2))
  REAL, ALLOCATABLE :: qvsflxa  (:,:,:) ! Surface moisture flux (kg/(m**2*s))

  REAL, ALLOCATABLE :: rhobara  (:,:,:,:) ! Air density (kg/m^3)

  CONTAINS

  SUBROUTINE allocateModel4DarrayA(nx,ny,nz,nensmbl,nzsoil,nstyps,nscalar,modelopt)

    INTEGER :: nx,ny,nz,nzsoil,nstyps,nensmbl,istatus
    INTEGER :: nscalar,modelopt

    ALLOCATE(ua     (nx,ny,nz,nensmbl),stat=istatus)
    CALL check_alloc_status(istatus, "ua")
    ALLOCATE(va     (nx,ny,nz,nensmbl),stat=istatus)
    CALL check_alloc_status(istatus, "va")
    ALLOCATE(wa     (nx,ny,nz,nensmbl),stat=istatus)
    CALL check_alloc_status(istatus, "wa")
    ALLOCATE(ptprta (nx,ny,nz,nensmbl),stat=istatus)
    CALL check_alloc_status(istatus, "ptprta")
    ALLOCATE(rhobara(nx,ny,nz,nensmbl),stat=istatus)
    CALL check_alloc_status(istatus, "rhobara")
    ALLOCATE(qva  (nx,ny,nz,nensmbl),stat=istatus)
    CALL check_alloc_status(istatus, "qva")
    ALLOCATE(qscalara(nx,ny,nz,nscalar,nensmbl),stat=istatus)

    IF (modelopt==2) THEN
      ALLOCATE(pprta  (nx,ny,nz,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "pprta")
      ALLOCATE(pha  (nx,ny,nz,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "pha")
    ELSE IF (modelopt==1) THEN
      ALLOCATE(pprta  (nx,ny,nz,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "pprta")

      ALLOCATE(tkea (nx,ny,nz,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "tkea")

      ALLOCATE(tsoila   (nx,ny,nzsoil,0:nstyps,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "tsoila")
      ALLOCATE(qsoila   (nx,ny,nzsoil,0:nstyps,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "qsoila")
      ALLOCATE(wetcanpa (nx,ny,0:nstyps,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "wetcanpa")
      ALLOCATE(snowdptha (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "snowdptha")


      ALLOCATE(rainga(nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "rainga")
      ALLOCATE(rainca(nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "rainca")
      ALLOCATE(prcratea(nx,ny,4,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "prcratea")
      ALLOCATE(radfrca(nx,ny,nz,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "radfrca")
      ALLOCATE(radswa (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "radswa")
      ALLOCATE(rnflxa (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "rnflxa")
      ALLOCATE(radswneta (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "radswneta")
      ALLOCATE(radlwina (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "radlwina")
      ALLOCATE(usflxa (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "usflxa")
      ALLOCATE(vsflxa (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "vsflxa")
      ALLOCATE(ptsflxa(nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "ptsflxa")
      ALLOCATE(qvsflxa(nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "qvsflxa")
    END IF

    RETURN
  END SUBROUTINE allocateModel4DarrayA

  !#####################################################################

  SUBROUTINE deallocateModel4DarrayA(modelopt)
    INTEGER :: modelopt

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (modelopt==1) THEN
      DEALLOCATE(ua,va,wa,ptprta,pprta,qva,qscalara,            &
                 tkea,tsoila,qsoila,wetcanpa,                   &
                 snowdptha,rainga,rainca,prcratea,              &
                 radfrca,radswa,rnflxa,radswneta,               &
                 radlwina,usflxa,vsflxa,ptsflxa,qvsflxa)
    ELSE IF (modelopt==2) THEN
      DEALLOCATE(ua,va,wa,ptprta,pprta,pha,qva,qscalara)
    END IF

    RETURN
  END SUBROUTINE deallocateModel4DarrayA

END MODULE Model4DARRAYA
