MODULE module_ModelArray
!
!
! 1.1 background state vector
!     -----------------------
!
  IMPLICIT NONE
  save
!
!
!-----------------------------------------------------------------------
!
!  Arrays defining model grid
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: x     (:)      ! The x-coord. of the physical and
                                      ! computational grid. Defined at u-point.
  REAL, ALLOCATABLE :: y     (:)      ! The y-coord. of the physical and
                                      ! computational grid. Defined at v-point.
  REAL, ALLOCATABLE :: z     (:)      ! The z-coord. of the computational grid.
                                      ! Defined at w-point on the staggered grid.
  REAL, ALLOCATABLE :: zp    (:,:,:)  ! The physical height coordinate defined at
                                      ! w-point of the staggered grid.
  REAL, ALLOCATABLE :: zpsoil(:,:,:)  ! Soil level depth.
!
!-----------------------------------------------------------------------
!
!  Model Time-dependent variables
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: u     (:,:,:)  ! Total u-velocity (m/s)
  REAL, ALLOCATABLE :: v     (:,:,:)  ! Total v-velocity (m/s)
  REAL, ALLOCATABLE :: w     (:,:,:)  ! Total w-velocity (m/s)
  REAL, ALLOCATABLE :: ptprt (:,:,:)  ! Perturbation potential temperature (K)
  REAL, ALLOCATABLE :: pprt  (:,:,:)  ! Perturbation pressure (Pascal)
  REAL, ALLOCATABLE :: ph    (:,:,:)  ! Geopotential

  REAL, ALLOCATABLE :: qv    (:,:,:)  ! Water vapor specific humidity (kg/kg)
  REAL, ALLOCATABLE :: qscalar(:,:,:,:)

  REAL, ALLOCATABLE :: tke   (:,:,:)  ! Turbulent Kinetic Energy ((m/s)**2)
  REAL, ALLOCATABLE :: kmh   (:,:,:)  ! Horizontal turb. mixing coef. for
                                      ! momentum. ( m**2/s )
  REAL, ALLOCATABLE :: kmv   (:,:,:)  ! Vertical turb. mixing coef. for
                                      ! momentum. ( m**2/s )
  INTEGER, ALLOCATABLE :: soiltyp (:,:,:)   ! Soil type
  REAL, ALLOCATABLE :: stypfrct(:,:,:)   ! Fraction of soil type
  INTEGER, ALLOCATABLE :: vegtyp  (:,:)          ! Vegetation type
  REAL, ALLOCATABLE :: roufns  (:,:)          ! Surface roughness
  REAL, ALLOCATABLE :: lai     (:,:)          ! Leaf Area Index
  REAL, ALLOCATABLE :: veg     (:,:)          ! Vegetation fraction

  REAL, ALLOCATABLE :: tsoil  (:,:,:,:)   ! Deep soil temperature (K)
  REAL, ALLOCATABLE :: qsoil  (:,:,:,:)   ! Deep soil temperature (K)
  REAL, ALLOCATABLE :: wetcanp(:,:,:)   ! Canopy water amount
  REAL, ALLOCATABLE :: snowdpth(:,:)           ! Snow cover

  REAL, ALLOCATABLE :: raing(:,:)         ! Grid supersaturation rain
  REAL, ALLOCATABLE :: rainc(:,:)         ! Cumulus convective rain
  REAL, ALLOCATABLE :: prcrate(:,:,:)     ! precipitation rate (kg/(m**2*s))
                                 ! prcrate(:,:,:,:) = total precip. rate
                                 ! prcrate(:,:,:,:) = grid scale precip. rate
                                 ! prcrate(:,:,:,:) = cumulus precip. rate
                                 ! prcrate(:,:,:,:) = microphysics precip. rate

  REAL, ALLOCATABLE :: radfrc(:,:,:)  ! Radiation forcing (K/s)
  REAL, ALLOCATABLE :: radsw (:,:)    ! Solar radiation reaching the surface
  REAL, ALLOCATABLE :: rnflx (:,:)    ! Net radiation flux absorbed by surface
  REAL, ALLOCATABLE :: radswnet(:,:)  ! Net shortwave radiation
  REAL, ALLOCATABLE :: radlwin(:,:)   ! Incoming longwave radiation

  REAL, ALLOCATABLE :: usflx (:,:)    ! Surface flux of u-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE :: vsflx (:,:)    ! Surface flux of v-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE :: ptsflx(:,:)    ! Surface heat flux (K*kg/(m*s**2))
  REAL, ALLOCATABLE :: qvsflx(:,:)    ! Surface moisture flux (kg/(m**2*s))

  REAL, ALLOCATABLE :: ubar  (:,:,:)  ! Base state u-velocity (m/s)
  REAL, ALLOCATABLE :: vbar  (:,:,:)  ! Base state v-velocity (m/s)
  REAL, ALLOCATABLE :: wbar  (:,:,:)  ! Base state v-velocity (m/s)
  REAL, ALLOCATABLE :: ptbar (:,:,:)  ! Base state potential temperature (K)
  REAL, ALLOCATABLE :: pbar  (:,:,:)  ! Base state pressure (Pascal).
  REAL, ALLOCATABLE :: rhobar(:,:,:)  ! Base state density rhobar times j3.
  REAL, ALLOCATABLE :: qvbar (:,:,:)  ! Base state water vapor specific
                                      ! humidity(kg/kg)

  REAL, ALLOCATABLE :: uprt   (:,:,:)    ! Perturbation u-velocity (m/s)
  REAL, ALLOCATABLE :: vprt   (:,:,:)    ! Perturbation v-velocity (m/s)
  REAL, ALLOCATABLE :: qvprt  (:,:,:)    ! Perturbation water vapor specific humidity (kg/kg)

  CONTAINS

  SUBROUTINE allocateModelArray(nx,ny,nz,nzsoil,nstyps,nscalar,modelopt)

    INTEGER:: nx,ny,nz,nzsoil,istatus
    INTEGER:: nstyps,nscalar,modelopt
!
!-----------------------------------------------------------------------
!
!  Allocate adas arrays
!
!-----------------------------------------------------------------------
!

    ALLOCATE(u    (nx,ny,nz),STAT=istatus)
    ALLOCATE(v    (nx,ny,nz),STAT=istatus)
    ALLOCATE(w    (nx,ny,nz),STAT=istatus)
    ALLOCATE(ptprt(nx,ny,nz),STAT=istatus)
    ALLOCATE(pprt (nx,ny,nz),STAT=istatus)
    ALLOCATE(ph (nx,ny,nz),STAT=istatus)
    ALLOCATE(ptbar(nx,ny,nz),STAT=istatus)
    ALLOCATE(pbar (nx,ny,nz),STAT=istatus)
    ALLOCATE(rhobar(nx,ny,nz),STAT=istatus)
    ALLOCATE(qv   (nx,ny,nz),STAT=istatus)
    ALLOCATE(qscalar(nx,ny,nz,nscalar),STAT=istatus)

    IF (modelopt==1) THEN
      ALLOCATE(x(nx),STAT=istatus)
      ALLOCATE(y(ny),STAT=istatus)
      ALLOCATE(z(nz),STAT=istatus)
      ALLOCATE(zp  (nx,ny,nz),STAT=istatus)
      ALLOCATE(zpsoil(nx,ny,nzsoil),STAT=istatus)
      ALLOCATE(tke  (nx,ny,nz),STAT=istatus)

      ALLOCATE(kmh(nx,ny,nz))
      ALLOCATE(kmv(nx,ny,nz))

      ALLOCATE( soiltyp(nx,ny,nstyps))
      ALLOCATE(stypfrct(nx,ny,nstyps))
      ALLOCATE(vegtyp(nx,ny))
      ALLOCATE(roufns(nx,ny))
      ALLOCATE(lai(nx,ny))
      ALLOCATE(veg(nx,ny))

      ALLOCATE(tsoil(nx,ny,nzsoil,0:nstyps))
      ALLOCATE(qsoil(nx,ny,nzsoil,0:nstyps))
      ALLOCATE(wetcanp(nx,ny,0:nstyps))
      ALLOCATE(snowdpth(nx,ny))

      ALLOCATE(raing(nx,ny))
      ALLOCATE(rainc(nx,ny))
      ALLOCATE(prcrate(nx,ny,4))
      ALLOCATE(radfrc(nx,ny,nz))
      ALLOCATE(radsw(nx,ny))
      ALLOCATE(rnflx(nx,ny))
      ALLOCATE(radswnet(nx,ny))
      ALLOCATE(radlwin(nx,ny))

      ALLOCATE(usflx(nx,ny))
      ALLOCATE(vsflx(nx,ny))
      ALLOCATE(ptsflx(nx,ny))
      ALLOCATE(qvsflx(nx,ny))

      ALLOCATE(ubar (nx,ny,nz),STAT=istatus)
      ALLOCATE(vbar (nx,ny,nz),STAT=istatus)
      ALLOCATE(wbar (nx,ny,nz),STAT=istatus)
      ALLOCATE(qvbar(nx,ny,nz),STAT=istatus)

      ALLOCATE(radfrc(nx,ny,nz),STAT=istatus)
      ALLOCATE(radsw (nx,ny),STAT=istatus)
      ALLOCATE(rnflx (nx,ny),STAT=istatus)
      ALLOCATE(radswnet (nx,ny),STAT=istatus)
      ALLOCATE(radlwin (nx,ny),STAT=istatus)

      ALLOCATE( uprt (nx,ny,nz),STAT=istatus )
      ALLOCATE( vprt (nx,ny,nz),STAT=istatus )
      ALLOCATE( qvprt(nx,ny,nz),STAT=istatus )
    END IF

    u     (:,:,:) = 0.0
    v     (:,:,:) = 0.0
    w     (:,:,:) = 0.0
    ptprt (:,:,:) = 0.0
    pprt  (:,:,:) = 0.0
    ph    (:,:,:) = 0.0
    ptbar (:,:,:)    = 0.0
    pbar  (:,:,:)    = 0.0
    rhobar(:,:,:)    = 0.0
    qv    (:,:,:) = 0.0
    qscalar(:,:,:,:) = 0.0

    IF (modelopt==1) THEN
      uprt  (:,:,:) = 0.0
      vprt  (:,:,:) = 0.0
      qvprt (:,:,:) = 0.0
      tke   (:,:,:) = 0.0
      kmh   (:,:,:) = 0.0
      kmv   (:,:,:) = 0.0
      radfrc(:,:,:) = 0.0

      ubar  (:,:,:)    = 0.0
      vbar  (:,:,:)    = 0.0
      wbar  (:,:,:)    = 0.0
      qvbar (:,:,:)    = 0.0

      x     (:)        = 0.0
      y     (:)        = 0.0
      z     (:)        = 0.0
      zp    (:,:,:)    = 0.0

      zpsoil(:,:,:)    = 0.0

      tsoil(:,:,:,:) = 0.0
      qsoil(:,:,:,:) = 0.0

      wetcanp(:,:,:) = 0.0

      soiltyp (:,:,:) = 0
      stypfrct(:,:,:) = 0.0

      snowdpth(:,:) = 0.0

      vegtyp (:,:) = 0
      lai    (:,:) = 0.0
      roufns (:,:) = 0.0
      veg    (:,:) = 0.0

      raing  (:,:)   = 0.0
      rainc  (:,:)   = 0.0
      prcrate(:,:,:) = 0.0
      radsw  (:,:)   = 0.0
      rnflx  (:,:)   = 0.0
      radswnet(:,:)  = 0.0
      radlwin(:,:)   = 0.0
      usflx  (:,:)   = 0.0
      vsflx  (:,:)   = 0.0
      ptsflx (:,:)   = 0.0
      qvsflx (:,:)   = 0.0
    END IF

  END  SUBROUTINE allocateModelArray

  !#####################################################################

  SUBROUTINE deallocateModelArray(modelopt)
    INTEGER :: modelopt

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    DEALLOCATE(u      )
    DEALLOCATE(v      )
    DEALLOCATE(w      )
    DEALLOCATE(ptprt  )
    DEALLOCATE(pprt   )
    DEALLOCATE(ph     )
    DEALLOCATE(ptbar  )
    DEALLOCATE(pbar   )
    DEALLOCATE(rhobar )
    DEALLOCATE(qv     )
    DEALLOCATE(qscalar)

    IF (modelopt==1) THEN
      DEALLOCATE(x      )
      DEALLOCATE(y      )
      DEALLOCATE(z      )
      DEALLOCATE(zp     )
      DEALLOCATE(zpsoil )
      DEALLOCATE(tke    )

      DEALLOCATE(kmh      )
      DEALLOCATE(kmv      )

      DEALLOCATE( soiltyp )
      DEALLOCATE(stypfrct )
      DEALLOCATE(vegtyp   )
      DEALLOCATE(roufns   )
      DEALLOCATE(lai      )
      DEALLOCATE(veg      )

      DEALLOCATE(tsoil    )
      DEALLOCATE(qsoil    )
      DEALLOCATE(wetcanp  )
      DEALLOCATE(snowdpth )

      DEALLOCATE(raing    )
      DEALLOCATE(rainc    )
      DEALLOCATE(prcrate  )
      DEALLOCATE(radfrc   )
      DEALLOCATE(radsw    )
      DEALLOCATE(rnflx    )
      DEALLOCATE(radswnet )
      DEALLOCATE(radlwin  )

      DEALLOCATE(usflx    )
      DEALLOCATE(vsflx    )
      DEALLOCATE(ptsflx   )
      DEALLOCATE(qvsflx   )

      DEALLOCATE(ubar   )
      DEALLOCATE(vbar   )
      DEALLOCATE(qvbar  )

      DEALLOCATE(radfrc )
      DEALLOCATE(radsw  )
      DEALLOCATE(rnflx  )
      DEALLOCATE(radswnet)
      DEALLOCATE(radlwin)

      DEALLOCATE( uprt )
      DEALLOCATE( vprt )
      DEALLOCATE( qvprt )
    END IF

    RETURN
  END SUBROUTINE deallocateModelArray

END MODULE module_ModelArray

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MODULE Model4DArrayE
!-----------------------------------------------------------------------
!
! PURPOSE: used for hybrid ensemble input data
!
!-----------------------------------------------------------------------

  IMPLICIT NONE
  SAVE

!-----------------------------------------------------------------------
!  Ensemble arrays:
!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!  Ensemble arrays:
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE, TARGET :: ue      (:,:,:,:) ! Total u-velocity (m/s)
  REAL, ALLOCATABLE, TARGET :: ve      (:,:,:,:) ! Total v-velocity (m/s)
  REAL, ALLOCATABLE, TARGET :: we      (:,:,:,:) ! Total w-velocity (m/s)
  REAL, ALLOCATABLE, TARGET :: ptprte  (:,:,:,:) ! Perturbation potential temperature (K)
  REAL, ALLOCATABLE, TARGET :: pprte   (:,:,:,:) ! Perturbation pressure (Pascal)
  REAL, ALLOCATABLE, TARGET :: phe     (:,:,:,:) ! Perturbation Geopotential in WRF
! REAL, ALLOCATABLE, TARGET :: mue     (:,:,:)  ! perturbation dry air mass in column (Pa)
  REAL, ALLOCATABLE, TARGET :: qve     (:,:,:,:) ! Water vapor specific humidity (kg/kg)
  REAL, ALLOCATABLE, TARGET :: qscalare(:,:,:,:,:)
  REAL, ALLOCATABLE :: tkee     (:,:,:,:) ! Turbulent Kinetic Energy ((m/s)**2)

  REAL, ALLOCATABLE :: tsoile   (:,:,:,:,:)! Deep soil temperature (K)
  REAL, ALLOCATABLE :: qsoile   (:,:,:,:,:)! Deep soil temperature (K)
  REAL, ALLOCATABLE :: wetcanpe (:,:,:,:)! Canopy water amount
  REAL, ALLOCATABLE :: snowdpthe(:,:,:) ! Snow depth (m)
  REAL, ALLOCATABLE :: rainge   (:,:,:)   ! Grid supersaturation rain
  REAL, ALLOCATABLE :: raince   (:,:,:)   ! Cumulus convective rain
  REAL, ALLOCATABLE :: prcratee (:,:,:,:)! precipitation rate (kg/(m**2*s))
  REAL, ALLOCATABLE :: radfrce  (:,:,:,:) ! Radiation forcing (K/s)
  REAL, ALLOCATABLE :: radswe   (:,:,:)   ! Solar radiation reaching the surface
  REAL, ALLOCATABLE :: rnflxe   (:,:,:)   ! Net radiation flux absorbed by surface
  REAL, ALLOCATABLE :: radswnete(:,:,:) ! Net solar radiation, SWin - SWout
  REAL, ALLOCATABLE :: radlwine (:,:,:) ! Incoming longwave radiation
  REAL, ALLOCATABLE :: usflxe   (:,:,:) ! Surface flux of u-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE :: vsflxe   (:,:,:) ! Surface flux of v-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE :: ptsflxe  (:,:,:) ! Surface heat flux (K*kg/(m*s**2))
  REAL, ALLOCATABLE :: qvsflxe  (:,:,:) ! Surface moisture flux (kg/(m**2*s))
  REAL, ALLOCATABLE :: en_ctr   (:,:,:,:)
  REAL, ALLOCATABLE :: gr_en    (:,:,:,:)

  REAL, ALLOCATABLE :: rhobare  (:,:,:,:)

  REAL, ALLOCATABLE, DIMENSION(:,:,:)   :: umean,vmean,wmean,ptmean,phmean,qvmean
  REAL, ALLOCATABLE :: qscalarmean(:,:,:,:)

  CONTAINS

  SUBROUTINE allocateModel4DarrayE(nx,ny,nz,nensmbl,nzsoil,nstyps,nscalar,modelopt,relaxopt)

    INTEGER :: nx,ny,nz,nzsoil,nstyps,nensmbl,istatus
    INTEGER :: nscalar,modelopt,relaxopt

    IF (relaxopt>=2 .and. modelopt==2) THEN
      ALLOCATE(umean (nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus, "umean")
      ALLOCATE(vmean (nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus, "vmean")
      ALLOCATE(wmean (nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus, "wmean")
      ALLOCATE(ptmean (nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus, "ptmean")
      ALLOCATE(phmean (nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus, "phmean")
      ALLOCATE(qvmean (nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus, "qvmean")
      ALLOCATE(qscalarmean(nx,ny,nz,nscalar),stat=istatus)
      CALL check_alloc_status(istatus, "qscalarmean")
      umean=0.0;vmean=0.0;wmean=0.0
      ptmean=0.0;phmean=0.0;qvmean=0.0
      qscalarmean=0.0
    END IF


    ALLOCATE(ue     (nx,ny,nz,nensmbl),stat=istatus)
    CALL check_alloc_status(istatus, "ue")
    ALLOCATE(ve     (nx,ny,nz,nensmbl),stat=istatus)
    CALL check_alloc_status(istatus, "ve")
    ALLOCATE(we     (nx,ny,nz,nensmbl),stat=istatus)
    CALL check_alloc_status(istatus, "we")
    ALLOCATE(ptprte (nx,ny,nz,nensmbl),stat=istatus)
    CALL check_alloc_status(istatus, "ptprte")
    ALLOCATE(rhobare(nx,ny,nz,nensmbl),stat=istatus)
    CALL check_alloc_status(istatus, "rhobare")

    ALLOCATE(qve  (nx,ny,nz,nensmbl),stat=istatus)
    CALL check_alloc_status(istatus, "qve")
    ALLOCATE(qscalare(nx,ny,nz,nscalar,nensmbl),stat=istatus)

    IF (modelopt==2) THEN
      ALLOCATE(pprte  (nx,ny,nz,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "pprte")
      ALLOCATE(phe  (nx,ny,nz,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "phe")
    ELSE IF (modelopt==1) THEN
      ALLOCATE(pprte  (nx,ny,nz,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "pprte")

      ALLOCATE(tkee (nx,ny,nz,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "tkee")

      ALLOCATE(tsoile   (nx,ny,nzsoil,0:nstyps,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "tsoile")
      ALLOCATE(qsoile   (nx,ny,nzsoil,0:nstyps,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "qsoile")
      ALLOCATE(wetcanpe (nx,ny,0:nstyps,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "wetcanpe")
      ALLOCATE(snowdpthe (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "snowdpthe")


      ALLOCATE(rainge(nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "rainge")
      ALLOCATE(raince(nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "raince")
      ALLOCATE(prcratee(nx,ny,4,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "prcratee")
      ALLOCATE(radfrce(nx,ny,nz,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "radfrce")
      ALLOCATE(radswe (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "radswe")
      ALLOCATE(rnflxe (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "rnflxe")
      ALLOCATE(radswnete (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "radswnete")
      ALLOCATE(radlwine (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "radlwine")
      ALLOCATE(usflxe (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "usflxe")
      ALLOCATE(vsflxe (nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "vsflxe")
      ALLOCATE(ptsflxe(nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "ptsflxe")
      ALLOCATE(qvsflxe(nx,ny,nensmbl),stat=istatus)
      CALL check_alloc_status(istatus, "qvsflxe")
    END IF

    RETURN
  END SUBROUTINE allocateModel4DarrayE

  !#####################################################################

  SUBROUTINE deallocateModel4DarrayE(modelopt)
    INTEGER :: modelopt

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (modelopt==1) THEN
      DEALLOCATE(ue,ve,we,ptprte,pprte,qve,qscalare,                    &
                 tkee,tsoile,qsoile,wetcanpe,                           &
                 snowdpthe,rainge,raince,prcratee,                      &
                 radfrce,radswe,rnflxe,radswnete,                       &
                 radlwine,usflxe,vsflxe,ptsflxe,qvsflxe)
    ELSE IF (modelopt==2) THEN
      DEALLOCATE(ue,ve,we,ptprte,pprte,phe,qve,qscalare)
    END IF

    RETURN
  END SUBROUTINE deallocateModel4DarrayE

END MODULE Model4DArrayE

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MODULE module_TruthArray
!
!
! 1.1 background state vector
!     -----------------------
!
  IMPLICIT NONE
  save
!
!
!-----------------------------------------------------------------------
!
!  Arrays defining model grid
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: xo    (:)      ! The x-coord. of the physical and
                                      ! computational grid. Defined at u-point.
  REAL, ALLOCATABLE :: yo    (:)      ! The y-coord. of the physical and
                                      ! computational grid. Defined at v-point.
  REAL, ALLOCATABLE :: zo    (:)      ! The z-coord. of the computational grid.
                                      ! Defined at w-point on the staggered grid.
  REAL, ALLOCATABLE :: zpo   (:,:,:)  ! The physical height coordinate defined at
                                      ! w-point of the staggered grid.
  REAL, ALLOCATABLE :: zpsoilo(:,:,:)  ! Soil level depth.
!
!-----------------------------------------------------------------------
!
!  Model Time-dependent variables
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: uo     (:,:,:)  ! Total u-velocity (m/s)
  REAL, ALLOCATABLE :: vo     (:,:,:)  ! Total v-velocity (m/s)
  REAL, ALLOCATABLE :: wo     (:,:,:)  ! Total w-velocity (m/s)
  REAL, ALLOCATABLE :: ptprto (:,:,:)  ! Perturbation potential temperature (K)
  REAL, ALLOCATABLE :: pprto  (:,:,:)  ! Perturbation pressure (Pascal)
  REAL, ALLOCATABLE :: pho    (:,:,:)  ! Geopotential (m**2/s**2)

  REAL, ALLOCATABLE :: qvo    (:,:,:)  ! Water vapor specific humidity (kg/kg)
  REAL, ALLOCATABLE :: qscalaro(:,:,:,:)

  REAL, ALLOCATABLE :: tkeo   (:,:,:)  ! Turbulent Kinetic Energy ((m/s)**2)
  REAL, ALLOCATABLE :: kmho   (:,:,:)  ! Horizontal turb. mixing coef. for
                                      ! momentum. ( m**2/s )
  REAL, ALLOCATABLE :: kmvo   (:,:,:)  ! Vertical turb. mixing coef. for
                                      ! momentum. ( m**2/s )
  INTEGER, ALLOCATABLE :: soiltypo (:,:,:)   ! Soil type
  REAL, ALLOCATABLE :: stypfrcto(:,:,:)   ! Fraction of soil type
  INTEGER, ALLOCATABLE :: vegtypo  (:,:)          ! Vegetation type
  REAL, ALLOCATABLE :: roufnso  (:,:)          ! Surface roughness
  REAL, ALLOCATABLE :: laio     (:,:)          ! Leaf Area Index
  REAL, ALLOCATABLE :: vego     (:,:)          ! Vegetation fraction

  REAL, ALLOCATABLE :: tsoilo  (:,:,:,:)   ! Deep soil temperature (K)
  REAL, ALLOCATABLE :: wetcanpo(:,:,:)   ! Canopy water amount
  REAL, ALLOCATABLE :: snowdptho(:,:)           ! Snow cover
  REAL, ALLOCATABLE :: qsoilo  (:,:,:,:)   ! Deep soil temperature (K)

  REAL, ALLOCATABLE :: raingo(:,:)         ! Grid supersaturation rain
  REAL, ALLOCATABLE :: rainco(:,:)         ! Cumulus convective rain
  REAL, ALLOCATABLE :: prcrateo(:,:,:)     ! precipitation rate (kg/(m**2*s))
                                 ! prcrate(:,:,:,:) = total precip. rate
                                 ! prcrate(:,:,:,:) = grid scale precip. rate
                                 ! prcrate(:,:,:,:) = cumulus precip. rate
                                 ! prcrate(:,:,:,:) = microphysics precip. rate

  REAL, ALLOCATABLE :: radfrco(:,:,:)  ! Radiation forcing (K/s)
  REAL, ALLOCATABLE :: radswo (:,:)    ! Solar radiation reaching the surface
  REAL, ALLOCATABLE :: rnflxo (:,:)    ! Net radiation flux absorbed by surface
  REAL, ALLOCATABLE :: radswneto(:,:)  ! Net shortwave radiation
  REAL, ALLOCATABLE :: radlwino(:,:)   ! Incoming longwave radiation

  REAL, ALLOCATABLE :: usflxo (:,:)    ! Surface flux of u-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE :: vsflxo (:,:)    ! Surface flux of v-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE :: ptsflxo(:,:)    ! Surface heat flux (K*kg/(m*s**2))
  REAL, ALLOCATABLE :: qvsflxo(:,:)    ! Surface moisture flux (kg/(m**2*s))

  REAL, ALLOCATABLE :: ubaro  (:,:,:)  ! Base state u-velocity (m/s)
  REAL, ALLOCATABLE :: vbaro  (:,:,:)  ! Base state v-velocity (m/s)
  REAL, ALLOCATABLE :: wbaro  (:,:,:)  ! Base state v-velocity (m/s)
  REAL, ALLOCATABLE :: ptbaro (:,:,:)  ! Base state potential temperature (K)
  REAL, ALLOCATABLE :: pbaro  (:,:,:)  ! Base state pressure (Pascal).
  REAL, ALLOCATABLE :: rhobaro(:,:,:)  ! Base state density rhobar times j3.
  REAL, ALLOCATABLE :: qvbaro (:,:,:)  ! Base state water vapor specific
                                      ! humidity(kg/kg)

  REAL, ALLOCATABLE :: uprto   (:,:,:)    ! Perturbation u-velocity (m/s)
  REAL, ALLOCATABLE :: vprto   (:,:,:)    ! Perturbation v-velocity (m/s)
  REAL, ALLOCATABLE :: qvprto  (:,:,:)    ! Perturbation water vapor specific humidity (kg/kg)

!
  CONTAINS

  SUBROUTINE allocateTruthArray(nx,ny,nz,nzsoil,nstyps,nscalar,modelopt)

    INTEGER:: nx,ny,nz,nzsoil,istatus
    INTEGER:: nstyps,nscalar,modelopt
!
!-----------------------------------------------------------------------
!
!  Allocate adas arrays
!
!-----------------------------------------------------------------------
!

    ALLOCATE(uo    (nx,ny,nz),STAT=istatus)
    ALLOCATE(vo    (nx,ny,nz),STAT=istatus)
    ALLOCATE(wo    (nx,ny,nz),STAT=istatus)
    ALLOCATE(ptprto(nx,ny,nz),STAT=istatus)
    ALLOCATE(pprto (nx,ny,nz),STAT=istatus)
    ALLOCATE(pho   (nx,ny,nz),STAT=istatus)
    ALLOCATE(ptbaro(nx,ny,nz),STAT=istatus)
    ALLOCATE(pbaro (nx,ny,nz),STAT=istatus)
    ALLOCATE(rhobaro(nx,ny,nz),STAT=istatus)
    ALLOCATE(qvo   (nx,ny,nz),STAT=istatus)
    ALLOCATE(qscalaro(nx,ny,nz,nscalar),STAT=istatus)

    IF (modelopt==1) THEN
      ALLOCATE(xo(nx),STAT=istatus)
      ALLOCATE(yo(ny),STAT=istatus)
      ALLOCATE(zo(nz),STAT=istatus)
      ALLOCATE(zpo  (nx,ny,nz),STAT=istatus)
      ALLOCATE(zpsoilo(nx,ny,nzsoil),STAT=istatus)
      ALLOCATE(tkeo  (nx,ny,nz),STAT=istatus)

      ALLOCATE(kmho(nx,ny,nz))
      ALLOCATE(kmvo(nx,ny,nz))

      ALLOCATE( soiltypo(nx,ny,nstyps))
      ALLOCATE(stypfrcto(nx,ny,nstyps))
      ALLOCATE(vegtypo(nx,ny))
      ALLOCATE(roufnso(nx,ny))
      ALLOCATE(laio(nx,ny))
      ALLOCATE(vego(nx,ny))

      ALLOCATE(tsoilo(nx,ny,nzsoil,0:nstyps))
      ALLOCATE(qsoilo(nx,ny,nzsoil,0:nstyps))
      ALLOCATE(wetcanpo(nx,ny,0:nstyps))
      ALLOCATE(snowdptho(nx,ny))

      ALLOCATE(raingo(nx,ny))
      ALLOCATE(rainco(nx,ny))
      ALLOCATE(prcrateo(nx,ny,4))
      ALLOCATE(radfrco(nx,ny,nz))
      ALLOCATE(radswo(nx,ny))
      ALLOCATE(rnflxo(nx,ny))
      ALLOCATE(radswneto(nx,ny))
      ALLOCATE(radlwino(nx,ny))

      ALLOCATE(usflxo(nx,ny))
      ALLOCATE(vsflxo(nx,ny))
      ALLOCATE(ptsflxo(nx,ny))
      ALLOCATE(qvsflxo(nx,ny))

      ALLOCATE(ubaro (nx,ny,nz),STAT=istatus)
      ALLOCATE(vbaro (nx,ny,nz),STAT=istatus)
      ALLOCATE(wbaro (nx,ny,nz),STAT=istatus)
      ALLOCATE(qvbaro(nx,ny,nz),STAT=istatus)

      ALLOCATE(radfrco(nx,ny,nz),STAT=istatus)
      ALLOCATE(radswo (nx,ny),STAT=istatus)
      ALLOCATE(rnflxo (nx,ny),STAT=istatus)
      ALLOCATE(radswneto (nx,ny),STAT=istatus)
      ALLOCATE(radlwino (nx,ny),STAT=istatus)

      ALLOCATE( uprto (nx,ny,nz),STAT=istatus )
      ALLOCATE( vprto (nx,ny,nz),STAT=istatus )
      ALLOCATE( qvprto(nx,ny,nz),STAT=istatus )
    END IF

    uo     (:,:,:) = 0.0
    vo     (:,:,:) = 0.0
    wo     (:,:,:) = 0.0
    ptprto (:,:,:) = 0.0
    pprto  (:,:,:) = 0.0
    pho    (:,:,:) = 0.0
    ptbaro (:,:,:) = 0.0
    pbaro  (:,:,:) = 0.0
    rhobaro(:,:,:) = 0.0
    qvo    (:,:,:) = 0.0
    qscalaro(:,:,:,:) = 0.0

    IF (modelopt==1) THEN
      uprto  (:,:,:) = 0.0
      vprto  (:,:,:) = 0.0
      qvprto (:,:,:) = 0.0
      tkeo   (:,:,:) = 0.0
      kmho   (:,:,:) = 0.0
      kmvo   (:,:,:) = 0.0
      radfrco(:,:,:) = 0.0

      ubaro  (:,:,:)    = 0.0
      vbaro  (:,:,:)    = 0.0
      wbaro  (:,:,:)    = 0.0
      qvbaro (:,:,:)    = 0.0

      xo     (:)        = 0.0
      yo     (:)        = 0.0
      zo     (:)        = 0.0
      zpo    (:,:,:)    = 0.0

      zpsoilo(:,:,:)    = 0.0

      tsoilo(:,:,:,:) = 0.0
      qsoilo(:,:,:,:) = 0.0

      wetcanpo(:,:,:) = 0.0

      soiltypo (:,:,:) = 0
      stypfrcto(:,:,:) = 0.0

      snowdptho(:,:) = 0.0

      vegtypo (:,:) = 0
      laio    (:,:) = 0.0
      roufnso (:,:) = 0.0
      vego    (:,:) = 0.0

      raingo  (:,:)   = 0.0
      rainco  (:,:)   = 0.0
      prcrateo(:,:,:) = 0.0
      radswo  (:,:)   = 0.0
      rnflxo  (:,:)   = 0.0
      radswneto(:,:)  = 0.0
      radlwino(:,:)   = 0.0
      usflxo  (:,:)   = 0.0
      vsflxo  (:,:)   = 0.0
      ptsflxo (:,:)   = 0.0
      qvsflxo (:,:)   = 0.0
    END IF

    RETURN
  END  SUBROUTINE allocateTruthArray

  !#####################################################################

  SUBROUTINE deallocateTruthArray(modelopt)
    INTEGER :: modelopt

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    DEALLOCATE(uo      )
    DEALLOCATE(vo      )
    DEALLOCATE(wo      )
    DEALLOCATE(ptprto  )
    DEALLOCATE(pprto   )
    DEALLOCATE(pho     )
    DEALLOCATE(ptbaro  )
    DEALLOCATE(pbaro   )
    DEALLOCATE(rhobaro )
    DEALLOCATE(qvo     )
    DEALLOCATE(qscalaro)

    IF (modelopt==1) THEN
      DEALLOCATE(xo      )
      DEALLOCATE(yo      )
      DEALLOCATE(zo      )
      DEALLOCATE(zpo     )
      DEALLOCATE(zpsoilo )
      DEALLOCATE(tkeo    )

      DEALLOCATE(kmho      )
      DEALLOCATE(kmvo      )

      DEALLOCATE( soiltypo )
      DEALLOCATE(stypfrcto )
      DEALLOCATE(vegtypo   )
      DEALLOCATE(roufnso   )
      DEALLOCATE(laio      )
      DEALLOCATE(vego      )

      DEALLOCATE(tsoilo    )
      DEALLOCATE(qsoilo    )
      DEALLOCATE(wetcanpo  )
      DEALLOCATE(snowdptho )

      DEALLOCATE(raingo    )
      DEALLOCATE(rainco    )
      DEALLOCATE(prcrateo  )
      DEALLOCATE(radfrco   )
      DEALLOCATE(radswo    )
      DEALLOCATE(rnflxo    )
      DEALLOCATE(radswneto )
      DEALLOCATE(radlwino  )

      DEALLOCATE(usflxo    )
      DEALLOCATE(vsflxo    )
      DEALLOCATE(ptsflxo   )
      DEALLOCATE(qvsflxo   )

      DEALLOCATE(ubaro   )
      DEALLOCATE(vbaro   )
      DEALLOCATE(qvbaro  )

      DEALLOCATE(radfrco )
      DEALLOCATE(radswo  )
      DEALLOCATE(rnflxo  )
      DEALLOCATE(radswneto)
      DEALLOCATE(radlwino)

      DEALLOCATE( uprto )
      DEALLOCATE( vprto )
      DEALLOCATE( qvprto )
    END IF

    RETURN
  END SUBROUTINE deallocateTruthArray

END MODULE module_TruthArray

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MODULE module_GuessArray
!
!
! 1.1 background state vector
!     -----------------------
!
  IMPLICIT NONE
  save
!
!
!-----------------------------------------------------------------------
!
!  Arrays defining model grid
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: xb    (:)      ! The x-coord. of the physical and
                                      ! computational grid. Defined at u-point.
  REAL, ALLOCATABLE :: yb    (:)      ! The y-coord. of the physical and
                                      ! computational grid. Defined at v-point.
  REAL, ALLOCATABLE :: zb    (:)      ! The z-coord. of the computational grid.
                                      ! Defined at w-point on the staggered grid.
  REAL, ALLOCATABLE :: zpb   (:,:,:)  ! The physical height coordinate defined at
                                      ! w-point of the staggered grid.
  REAL, ALLOCATABLE :: zpsoilb(:,:,:)  ! Soil level depth.
!
!-----------------------------------------------------------------------
!
!  Model Time-dependent variables
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: ub     (:,:,:)  ! Total u-velocity (m/s)
  REAL, ALLOCATABLE :: vb     (:,:,:)  ! Total v-velocity (m/s)
  REAL, ALLOCATABLE :: wb     (:,:,:)  ! Total w-velocity (m/s)
  REAL, ALLOCATABLE :: ptprtb (:,:,:)  ! Perturbation potential temperature (K)
  REAL, ALLOCATABLE :: pprtb  (:,:,:)  ! Perturbation pressure (Pascal)
  REAL, ALLOCATABLE :: phb    (:,:,:)  ! Geopotential (m**2/s**2)

  REAL, ALLOCATABLE :: qvb    (:,:,:)  ! Water vapor specific humidity (kg/kg)
  REAL, ALLOCATABLE :: qscalarb(:,:,:,:)

  REAL, ALLOCATABLE :: tkeb   (:,:,:)  ! Turbulent Kinetic Energy ((m/s)**2)
  REAL, ALLOCATABLE :: kmhb   (:,:,:)  ! Horizontal turb. mixing coef. for
                                      ! momentum. ( m**2/s )
  REAL, ALLOCATABLE :: kmvb   (:,:,:)  ! Vertical turb. mixing coef. for
                                      ! momentum. ( m**2/s )
  INTEGER, ALLOCATABLE :: soiltypb (:,:,:)   ! Soil type
  REAL, ALLOCATABLE :: stypfrctb(:,:,:)   ! Fraction of soil type
  INTEGER, ALLOCATABLE :: vegtypb  (:,:)          ! Vegetation type
  REAL, ALLOCATABLE :: roufnsb  (:,:)          ! Surface roughness
  REAL, ALLOCATABLE :: laib     (:,:)          ! Leaf Area Index
  REAL, ALLOCATABLE :: vegb     (:,:)          ! Vegetation fraction

  REAL, ALLOCATABLE :: tsoilb  (:,:,:,:)   ! Deep soil temperature (K)
  REAL, ALLOCATABLE :: wetcanpb(:,:,:)   ! Canopy water amount
  REAL, ALLOCATABLE :: snowdpthb(:,:)           ! Snow cover
  REAL, ALLOCATABLE :: qsoilb  (:,:,:,:)   ! Deep soil temperature (K)

  REAL, ALLOCATABLE :: raingb(:,:)         ! Grid supersaturation rain
  REAL, ALLOCATABLE :: raincb(:,:)         ! Cumulus convective rain
  REAL, ALLOCATABLE :: prcrateb(:,:,:)     ! precipitation rate (kg/(m**2*s))
                                 ! prcrate(:,:,:,:) = total precip. rate
                                 ! prcrate(:,:,:,:) = grid scale precip. rate
                                 ! prcrate(:,:,:,:) = cumulus precip. rate
                                 ! prcrate(:,:,:,:) = microphysics precip. rate

  REAL, ALLOCATABLE :: radfrcb(:,:,:)  ! Radiation forcing (K/s)
  REAL, ALLOCATABLE :: radswb (:,:)    ! Solar radiation reaching the surface
  REAL, ALLOCATABLE :: rnflxb (:,:)    ! Net radiation flux absorbed by surface
  REAL, ALLOCATABLE :: radswnetb(:,:)  ! Net shortwave radiation
  REAL, ALLOCATABLE :: radlwinb(:,:)   ! Incoming longwave radiation

  REAL, ALLOCATABLE :: usflxb (:,:)    ! Surface flux of u-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE :: vsflxb (:,:)    ! Surface flux of v-momentum (kg/(m*s**2))
  REAL, ALLOCATABLE :: ptsflxb(:,:)    ! Surface heat flux (K*kg/(m*s**2))
  REAL, ALLOCATABLE :: qvsflxb(:,:)    ! Surface moisture flux (kg/(m**2*s))

  REAL, ALLOCATABLE :: ubarb  (:,:,:)  ! Base state u-velocity (m/s)
  REAL, ALLOCATABLE :: vbarb  (:,:,:)  ! Base state v-velocity (m/s)
  REAL, ALLOCATABLE :: wbarb  (:,:,:)  ! Base state v-velocity (m/s)
  REAL, ALLOCATABLE :: ptbarb (:,:,:)  ! Base state potential temperature (K)
  REAL, ALLOCATABLE :: pbarb  (:,:,:)  ! Base state pressure (Pascal).
  REAL, ALLOCATABLE :: rhobarb(:,:,:)  ! Base state density rhobar times j3.
  REAL, ALLOCATABLE :: qvbarb (:,:,:)  ! Base state water vapor specific
                                      ! humidity(kg/kg)

  REAL, ALLOCATABLE :: uprtb   (:,:,:)    ! Perturbation u-velocity (m/s)
  REAL, ALLOCATABLE :: vprtb   (:,:,:)    ! Perturbation v-velocity (m/s)
  REAL, ALLOCATABLE :: qvprtb  (:,:,:)    ! Perturbation water vapor specific humidity (kg/kg)

!
  CONTAINS

  SUBROUTINE allocateGuessArray(nx,ny,nz,nzsoil,nstyps,nscalar,modelopt)

    INTEGER:: nx,ny,nz,nzsoil,istatus
    INTEGER:: nstyps,nscalar,modelopt
!
!-----------------------------------------------------------------------
!
!  Allocate adas arrays
!
!-----------------------------------------------------------------------
!

    ALLOCATE(ub    (nx,ny,nz),STAT=istatus)
    ALLOCATE(vb    (nx,ny,nz),STAT=istatus)
    ALLOCATE(wb    (nx,ny,nz),STAT=istatus)
    ALLOCATE(ptprtb(nx,ny,nz),STAT=istatus)
    ALLOCATE(pprtb (nx,ny,nz),STAT=istatus)
    ALLOCATE(phb   (nx,ny,nz),STAT=istatus)
    ALLOCATE(ptbarb(nx,ny,nz),STAT=istatus)
    ALLOCATE(pbarb (nx,ny,nz),STAT=istatus)
    ALLOCATE(rhobarb(nx,ny,nz),STAT=istatus)
    ALLOCATE(qvb   (nx,ny,nz),STAT=istatus)
    ALLOCATE(qscalarb(nx,ny,nz,nscalar),STAT=istatus)

    IF (modelopt==1) THEN
      ALLOCATE(xb(nx),STAT=istatus)
      ALLOCATE(yb(ny),STAT=istatus)
      ALLOCATE(zb(nz),STAT=istatus)
      ALLOCATE(zpb  (nx,ny,nz),STAT=istatus)
      ALLOCATE(zpsoilb(nx,ny,nzsoil),STAT=istatus)
      ALLOCATE(tkeb  (nx,ny,nz),STAT=istatus)

      ALLOCATE(kmhb(nx,ny,nz))
      ALLOCATE(kmvb(nx,ny,nz))

      ALLOCATE( soiltypb(nx,ny,nstyps))
      ALLOCATE(stypfrctb(nx,ny,nstyps))
      ALLOCATE(vegtypb(nx,ny))
      ALLOCATE(roufnsb(nx,ny))
      ALLOCATE(laib(nx,ny))
      ALLOCATE(vegb(nx,ny))

      ALLOCATE(tsoilb(nx,ny,nzsoil,0:nstyps))
      ALLOCATE(qsoilb(nx,ny,nzsoil,0:nstyps))
      ALLOCATE(wetcanpb(nx,ny,0:nstyps))
      ALLOCATE(snowdpthb(nx,ny))

      ALLOCATE(raingb(nx,ny))
      ALLOCATE(raincb(nx,ny))
      ALLOCATE(prcrateb(nx,ny,4))
      ALLOCATE(radfrcb(nx,ny,nz))
      ALLOCATE(radswb(nx,ny))
      ALLOCATE(rnflxb(nx,ny))
      ALLOCATE(radswnetb(nx,ny))
      ALLOCATE(radlwinb(nx,ny))

      ALLOCATE(usflxb(nx,ny))
      ALLOCATE(vsflxb(nx,ny))
      ALLOCATE(ptsflxb(nx,ny))
      ALLOCATE(qvsflxb(nx,ny))

      ALLOCATE(ubarb (nx,ny,nz),STAT=istatus)
      ALLOCATE(vbarb (nx,ny,nz),STAT=istatus)
      ALLOCATE(wbarb (nx,ny,nz),STAT=istatus)
      ALLOCATE(qvbarb(nx,ny,nz),STAT=istatus)

      ALLOCATE(radfrcb(nx,ny,nz),STAT=istatus)
      ALLOCATE(radswb (nx,ny),STAT=istatus)
      ALLOCATE(rnflxb (nx,ny),STAT=istatus)
      ALLOCATE(radswnetb (nx,ny),STAT=istatus)
      ALLOCATE(radlwinb (nx,ny),STAT=istatus)

      ALLOCATE( uprtb (nx,ny,nz),STAT=istatus )
      ALLOCATE( vprtb (nx,ny,nz),STAT=istatus )
      ALLOCATE( qvprtb(nx,ny,nz),STAT=istatus )
    END IF

    ub     (:,:,:) = 0.0
    vb     (:,:,:) = 0.0
    wb     (:,:,:) = 0.0
    ptprtb (:,:,:) = 0.0
    pprtb  (:,:,:) = 0.0
    phb    (:,:,:) = 0.0
    ptbarb (:,:,:) = 0.0
    pbarb  (:,:,:) = 0.0
    rhobarb(:,:,:) = 0.0
    qvb    (:,:,:) = 0.0
    qscalarb(:,:,:,:) = 0.0

    IF (modelopt==1) THEN
      uprtb  (:,:,:) = 0.0
      vprtb  (:,:,:) = 0.0
      qvprtb (:,:,:) = 0.0
      tkeb   (:,:,:) = 0.0
      kmhb   (:,:,:) = 0.0
      kmvb   (:,:,:) = 0.0
      radfrcb(:,:,:) = 0.0

      ubarb  (:,:,:)    = 0.0
      vbarb  (:,:,:)    = 0.0
      wbarb  (:,:,:)    = 0.0
      qvbarb (:,:,:)    = 0.0

      xb     (:)        = 0.0
      yb     (:)        = 0.0
      zb     (:)        = 0.0
      zpb    (:,:,:)    = 0.0

      zpsoilb(:,:,:)    = 0.0

      tsoilb(:,:,:,:) = 0.0
      qsoilb(:,:,:,:) = 0.0

      wetcanpb(:,:,:) = 0.0

      soiltypb (:,:,:) = 0
      stypfrctb(:,:,:) = 0.0

      snowdpthb(:,:) = 0.0

      vegtypb (:,:) = 0
      laib    (:,:) = 0.0
      roufnsb (:,:) = 0.0
      vegb    (:,:) = 0.0

      raingb  (:,:)   = 0.0
      raincb  (:,:)   = 0.0
      prcrateb(:,:,:) = 0.0
      radswb  (:,:)   = 0.0
      rnflxb  (:,:)   = 0.0
      radswnetb(:,:)  = 0.0
      radlwinb(:,:)   = 0.0
      usflxb  (:,:)   = 0.0
      vsflxb  (:,:)   = 0.0
      ptsflxb (:,:)   = 0.0
      qvsflxb (:,:)   = 0.0
    END IF

    RETURN
  END  SUBROUTINE allocateGuessArray

  !#####################################################################

  SUBROUTINE deallocateGuessArray(modelopt)
    INTEGER :: modelopt
!

    DEALLOCATE(ub      )
    DEALLOCATE(vb      )
    DEALLOCATE(wb      )
    DEALLOCATE(ptprtb  )
    DEALLOCATE(pprtb   )
    DEALLOCATE(phb     )
    DEALLOCATE(ptbarb  )
    DEALLOCATE(pbarb   )
    DEALLOCATE(rhobarb )
    DEALLOCATE(qvb     )
    DEALLOCATE(qscalarb)

    IF (modelopt==1) THEN
      DEALLOCATE(xb      )
      DEALLOCATE(yb      )
      DEALLOCATE(zb      )
      DEALLOCATE(zpb     )
      DEALLOCATE(zpsoilb )
      DEALLOCATE(tkeb    )

      DEALLOCATE(kmhb      )
      DEALLOCATE(kmvb      )

      DEALLOCATE( soiltypb )
      DEALLOCATE(stypfrctb )
      DEALLOCATE(vegtypb   )
      DEALLOCATE(roufnsb   )
      DEALLOCATE(laib      )
      DEALLOCATE(vegb      )

      DEALLOCATE(tsoilb    )
      DEALLOCATE(qsoilb    )
      DEALLOCATE(wetcanpb  )
      DEALLOCATE(snowdpthb )

      DEALLOCATE(raingb    )
      DEALLOCATE(raincb    )
      DEALLOCATE(prcrateb  )
      DEALLOCATE(radfrcb   )
      DEALLOCATE(radswb    )
      DEALLOCATE(rnflxb    )
      DEALLOCATE(radswnetb )
      DEALLOCATE(radlwinb  )

      DEALLOCATE(usflxb    )
      DEALLOCATE(vsflxb    )
      DEALLOCATE(ptsflxb   )
      DEALLOCATE(qvsflxb   )

      DEALLOCATE(ubarb   )
      DEALLOCATE(vbarb   )
      DEALLOCATE(qvbarb  )

      DEALLOCATE(radfrcb )
      DEALLOCATE(radswb  )
      DEALLOCATE(rnflxb  )
      DEALLOCATE(radswnetb)
      DEALLOCATE(radlwinb)

      DEALLOCATE( uprtb )
      DEALLOCATE( vprtb )
      DEALLOCATE( qvprtb )
    END IF

    RETURN
  END SUBROUTINE deallocateGuessArray

END MODULE module_GuessArray
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE TEMPER                     ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE temper( nx,ny,nz,theta,ptbar, ppert, pbar, t )

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!    Using a version of Poisson's formula, calculate temperature.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Joe Bradley
!    12/05/91
!
!  MODIFICATIONS:
!    Modified by Ming Xue so that arrays are only defined at
!             one time level.
!    6/09/92  Added full documentation and phycst include file for
!             rddcp=Rd/Cp  (K. Brewster)
!
!-----------------------------------------------------------------------
!
!  INPUT:
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    theta    Potential temperature (degrees Kelvin)
!    ppert    Perturbation pressure (Pascals)
!    pbar     Base state pressure (Pascals)
!
!  OUTPUT:
!
!    t        Temperature (degrees Kelvin)
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz
!
  REAL :: theta(nx,ny,nz)      ! potential temperature (degrees Kelvin)
  REAL :: ppert(nx,ny,nz)      ! perturbation pressure (Pascals)
  REAL :: pbar (nx,ny,nz)      ! base state pressure (Pascals)
  REAL :: ptbar (nx,ny,nz)      ! base state potential temperature (K)
!
  REAL :: t    (nx,ny,nz)      ! temperature (degrees Kelvin)
!
!-----------------------------------------------------------------------
!
!  Include file
!
!-----------------------------------------------------------------------
!
! INCLUDE 'phycst.inc'
!
  REAL :: p0        ! Surface reference pressure, is 100000 Pascal.
  PARAMETER( p0     = 1.0E5 )
!
  REAL :: cp        ! Specific heat of dry air at constant pressure
                    ! (m**2/(s**2*K)).
  PARAMETER( cp     = 1004.0 )
!
  REAL :: rd        ! Gas constant for dry air  (m**2/(s**2*K))
  PARAMETER( rd     = 287.0 )
!
  REAL :: rddcp
  PARAMETER( rddcp  = rd/cp )
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!-----------------------------------------------------------------------
!
!  Calculate the temperature using Poisson's formula.
!
!-----------------------------------------------------------------------
!
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        t(i,j,k) = (theta(i,j,k)+ptbar(i,j,k)) *                         &
             (((ppert(i,j,k) + pbar(i,j,k)) / p0) ** rddcp)
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE temper

SUBROUTINE temperwrf( nx,ny,nz,theta,ptbar, ppert, pbar, t )

!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz
!
  REAL :: theta(nx-2,ny-2,nz-2)      ! potential temperature (degrees Kelvin)
  REAL :: ppert(nx-2,ny-2,nz-2)      ! perturbation pressure (Pascals)
  REAL :: pbar (nx-2,ny-2,nz-2)      ! base state pressure (Pascals)
  REAL :: ptbar(nx-2,ny-2,nz-2)      ! base state potential temperature (K)
!
  REAL :: t    (nx-2,ny-2,nz-2)      ! temperature (degrees Kelvin)

  REAL :: p0        ! Surface reference pressure, is 100000 Pascal.
  PARAMETER( p0     = 1.0E5 )
!
  REAL :: cp        ! Specific heat of dry air at constant pressure
                    ! (m**2/(s**2*K)).
  PARAMETER( cp     = 1004.0 )
!
  REAL :: rd        ! Gas constant for dry air  (m**2/(s**2*K))
  PARAMETER( rd     = 287.0 )
!
  REAL :: rddcp
  PARAMETER( rddcp  = rd/cp )

  INTEGER :: i,j,k

!-----------------------------------------------------------------------
!
!  Calculate the temperature using Poisson's formula.
!
!-----------------------------------------------------------------------
!
  DO k=1,nz-2
    DO j=1,ny-2
      DO i=1,nx-2
        t(i,j,k) = (theta(i,j,k)+ptbar(i,j,k)) *                         &
             (((ppert(i,j,k) + pbar(i,j,k)) / p0) ** rddcp)
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE temperwrf


!
!########################################################################
!########################################################################
!#########                                                      #########
!#########               SUBROUTINE REFLEC_FERRIER2             #########
!#########                                                      #########
!#########                     Developed by                     #########
!#########     Center for Analysis and Prediction of Storms     #########
!#########                University of Oklahoma                #########
!#########                                                      #########
!########################################################################
!########################################################################

SUBROUTINE reflec_ferrier2(nx,ny,nz, rho, qr, qs, qh, t, rff)

!-----------------------------------------------------------------------
!
! PURPOSE:
!
! This subroutine estimates logarithmic radar reflectivity using
! equations customized for use with the ARPS Lin-Tao microphysics
! package.
!
! The equations (after some algebra to optimize code) are as follows:
!
! dBZ = 10*LOG10(Ze)
!
! Ze = Zer + Zes + Zeh (contributions from rain, snow, and hail).
!
!         k * 720                                 1.75
!   Zer = --------------------------- * (rho * qr)
!           1.75      0.75       1.75
!         pi     * N0r     * rhor
!
!   Zes = Zesnegf for "dry" snow (T < 0 C), or Zes = Zesposf for "wet"
!   snow (T > 0 C)
!
!                          2                     0.25
!             k * 720 * |K|                * rhos
!                          ice                                    1.75
!   Zesnegf = --------------------------------------- * (rho * qs)
!               1.75       2          0.75         2
!             pi     *  |K|      * N0s     * rhoice
!                          water
!
!             k * 720                                 1.75
!   Zesposf = --------------------------- * (rho * qs)
!               1.75      0.75       1.75
!             pi     * N0s     * rhos
!
!          /  k * 720                     \ 0.95             1.6625
!   Zeh = |   ---------------------------  |     * (rho * qh)
!          \    1.75      0.75       1.75 /
!           \ pi     * N0h     * rhoh    /
!
!-----------------------------------------------------------------------
!
! REFERENCES:
!
! Jahn, D., D. Weber, E. Kemp, and H. Neeman, 2000:  Evidence of
!   convective-induced turbulence outside the immediate storm region:
!   Part III.  CAPS report submitted to AlliedSignal/Honeywell, 37pp.
!
! Ferrier, B. S., W.-K. Tao, and J. Simpson, 1995:  A double-moment
!   multiple-phase four-class bulk ice scheme.  Part II:  Simulations
!   of convective storms in different large-scale environments and
!   comparisons with other bulk parameterizations.  J. Atmos. Sci.,
!   45, 3846-3879.
!
! McCumber, M., W.-K. Tao, J. Simpson, R. Penc, and S.-T. Soong, 1991:
!   Comparison of ice-phase microphysical parameterization schemes using
!   numerical simulations of tropical convection.  J. Appl. Meteor.,
!   30, 985-1004.
!
! Smith, P. L., 1984:  Equivalent radar reflectivity factors for snow
!   and ice particle.  J. Climate Appl. Meteor., 23, 1258-1260.
!
! Smith, P. L., Jr., C. G. Myers, and H. D. Orville, 1975:  Radar
!   reflectivity factor calculations in numerical cloud models using
!   bulk parameterization of precipitation.  J. Appl. Meteor., 14,
!   1156-1165.
!
!-----------------------------------------------------------------------
!
! AUTHOR:  Henry Neeman, Spring 2000.
!
! MODIFICATION HISTORY:
!
! Eric Kemp, 8 October 2001
! Reformatted code for ARPS Fortran 90 standard.
!
! Ming Xue, 16 Oct. 2001
!   Changed ni,nj,nk to nx,ny,nz. Change loop bounds. Changed the order
!   of argument list. Removed IF( ice == 0 ) check.
!
! 04/05/03 (Keith Brewster)
!   Clean-up of some unused parameters for clarity.
!
!-----------------------------------------------------------------------
!
! Force explicit declarations.
!
!-----------------------------------------------------------------------

  IMPLICIT NONE

!-----------------------------------------------------------------------
! Include files.
!-----------------------------------------------------------------------

! INCLUDE 'globcst.inc'

!-----------------------------------------------------------------------
! Declare arguments.
!-----------------------------------------------------------------------

  INTEGER, INTENT(IN) :: nx,ny,nz ! Dimensions of grid

  REAL, INTENT(IN) :: rho(nx,ny,nz) ! Air density (kg m**-3)
  REAL, INTENT(IN) :: qr(nx,ny,nz) ! Rain mixing ratio (kg kg**-1)
  REAL, INTENT(IN) :: qs(nx,ny,nz) ! Snow mixing ratio (kg kg**-1)
  REAL, INTENT(IN) :: qh(nx,ny,nz) ! Hail mixing ratio (kg kg**-1)
  REAL, INTENT(IN) :: t(nx,ny,nz) ! Temperature (K)

  REAL, INTENT(INOUT) :: rff(nx,ny,nz) ! Reflectivity (dBZ)

!-----------------------------------------------------------------------
! Declare local parameters.
!-----------------------------------------------------------------------

  REAL,PARAMETER :: ki2 = 0.176 ! Dielectric factor for ice if other
                                !   than melted drop diameters are used.
  REAL,PARAMETER :: kw2=0.93 ! Dielectric factor for water.

  REAL,PARAMETER :: degKtoC=273.15 ! Conversion factor from degrees K to
                                   !   degrees C

  REAL,PARAMETER :: m3todBZ=1.0E+18 ! Conversion factor from m**3 to
                                    !   mm**6 m**-3.

  REAL,PARAMETER :: pi=3.1415926 ! Pi.

  REAL,PARAMETER :: pipowf=7.0/4.0 ! Power to which pi is raised.

  REAL,PARAMETER :: N0r=8.0E+06 ! Intercept parameter in 1/(m^4) for rain.
  REAL,PARAMETER :: N0s=3.0E+06 ! Intercept parameter in 1/(m^4) for snow.
  REAL,PARAMETER :: N0h=4.0E+04 ! Intercept parameter in 1/(m^4) for hail.

  REAL,PARAMETER :: N0xpowf=3.0/4.0 ! Power to which N0r,N0s & N0h are
                                    !   raised.

  REAL,PARAMETER :: approxpow=0.95 ! Approximation power for hail
                                   !   integral.

  REAL,PARAMETER :: rqrpowf=7.0/4.0 ! Power to which product rho * qr
                                    !   is raised.
  REAL,PARAMETER :: rqsnpowf=7.0/4.0 ! Power to which product rho * qs
                                     !   is raised (dry snow).
  REAL,PARAMETER :: rqsppowf=7.0/4.0 ! Power to which product rho * qs
                                     !   is raised (wet snow).

  REAL,PARAMETER :: rqhpowf=(7.0/4.0)*approxpow ! Power to which product
                                                !   rho * qh is raised.

  REAL,PARAMETER :: rhoi=917.  ! Density of ice (kg m**-3)
  REAL,PARAMETER :: rhor=1000. ! Density of rain (kg m**-3)
  REAL,PARAMETER :: rhos=100.  ! Density of snow (kg m**-3)
  REAL,PARAMETER :: rhoh=913.  ! Density of hail (kg m**-3)

  REAL,PARAMETER :: rhoipowf=2.0     ! Power to which rhoi is raised.
  REAL,PARAMETER :: rhospowf=1.0/4.0 ! Power to which rhos is raised.
  REAL,PARAMETER :: rhoxpowf=7.0/4.0 ! Power to which rhoh is raised.

  REAL,PARAMETER :: Zefact=720.0 ! Multiplier for Ze components.

  REAL,PARAMETER :: lg10mul=10.0 ! Log10 multiplier

!-----------------------------------------------------------------------
! Declare local variables.
!-----------------------------------------------------------------------

  REAL :: rcomp,scomp,hcomp,sumcomp
  INTEGER :: i,j,k
  REAL(8),PARAMETER :: Zerf=3630803470.863780
  REAL(8),PARAMETER :: Zesnegf=958893421.2925436
  REAL(8),PARAMETER :: Zesposf=426068366492.9870
  REAL(8),PARAMETER :: Zehf   =61263782772.99985
  REAL,   PARAMETER :: intvl=3.0
  REAL :: rtem

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!-----------------------------------------------------------------------
! First gather all the constants together.  (These are treated as
! variables because Fortran 90 does not allow real exponents when
! calculating parameters).
!-----------------------------------------------------------------------
!
! Zerf = (m3todBZ * Zefact) /  &
!                 ((pi ** pipowf) * (N0r ** N0xpowf) *  &
!                  (rhor ** rhoxpowf))
! Zesnegf = ((m3todBZ * Zefact   * Ki2 * (rhos ** rhospowf)) /  &
!                  ((pi ** pipowf) * Kw2 * (N0s ** N0xpowf) *  &
!                   (rhoi ** rhoipowf)))
! Zesposf = ((m3todBZ * Zefact) /  &
!                  ((pi ** pipowf) * (N0s ** N0xpowf) *  &
!                   (rhos ** rhoxpowf)))
! Zehf = (((m3todBZ * Zefact) /  &
!                   ((pi ** pipowf) * (N0h ** N0xpowf) *  &
!                    (rhoh ** rhoxpowf))) ** approxpow)
!
! print*,'Zerf  Zesnegf  Zesposf Zehf==',Zerf,Zesnegf,Zesposf,Zehf
!
!-----------------------------------------------------------------------
! Now loop through the scalar grid points.
!-----------------------------------------------------------------------
            rcomp = 0.0
            scomp = 0.0
            hcomp = 0.0

  DO k = 1,nz         ! Eric 8/8/03
    DO j = 1,ny
      DO i = 1,nx

!-----------------------------------------------------------------------
! Check for bad air density value.
!-----------------------------------------------------------------------

        IF (rho(i,j,k) <= 0.0) THEN
          rff(i,j,k) = 0.0
        ELSE

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from rain.
!-----------------------------------------------------------------------

          IF (qr(i,j,k) <= 0.0) THEN
            rcomp = 0.0
          ELSE
            rcomp = Zerf*((qr(i,j,k)*rho(i,j,k)) ** rqrpowf)
          END IF

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from snow (dry or wet).
!-----------------------------------------------------------------------
IF(1==1) THEN
          IF (qs(i,j,k) <= 0.0) THEN
            scomp = 0.0
          ELSE IF (t(i,j,k) <= degKtoC) THEN
            scomp = Zesnegf*((qs(i,j,k)*rho(i,j,k)) ** rqsnpowf)
          ELSE
            scomp = Zesposf*((qs(i,j,k)*rho(i,j,k)) ** rqsppowf)
          END IF

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from hail.
!-----------------------------------------------------------------------

          IF (qh(i,j,k) <= 0.0) THEN
            hcomp = 0.0
          ELSE
            hcomp = Zehf*((qh(i,j,k)*rho(i,j,k)) ** rqhpowf)
          END IF
END IF
!-----------------------------------------------------------------------
! Now add the contributions and convert to logarithmic reflectivity
! factor dBZ.
!-----------------------------------------------------------------------
!         sumcomp = rcomp + scomp + hcomp
!
          if (t(i,j,k) >= (degKtoC + intvl ) ) then
            sumcomp = rcomp
          elseif (t(i,j,k) <= (degKtoC - intvl )) then
            sumcomp = scomp + hcomp
          else
            rtem=(t(i,j,k)-degKtoC+intvl)/(2*intvl)
            sumcomp=rtem*rcomp+(1-rtem)*(scomp+hcomp)
          end if
!
!          if(sumcomp>1.0) print*,'sumcomp===',sumcomp
!
         IF( sumcomp>1.0 ) THEN
           rff(i,j,k) = lg10mul * LOG10(sumcomp)
         ELSE
           rff(i,j,k) = 0.0
         END IF
        END IF !  IF (rho(i,j,k) <= 0.0) ... ELSE ...

      END DO ! DO i
    END DO ! DO j
  END DO ! DO k

  RETURN
END SUBROUTINE reflec_ferrier2

SUBROUTINE reflec_ferrierwrf(nx,ny,nz, rho, qr, qs, qh, t, rff)

  IMPLICIT NONE

  INTEGER, INTENT(IN) :: nx,ny,nz ! Dimensions of grid

  REAL, INTENT(IN) :: rho(nx-2,ny-2,nz-2) ! Air density (kg m**-3)
  REAL, INTENT(IN) :: qr(nx-2,ny-2,nz-2)  ! Rain mixing ratio (kg kg**-1)
  REAL, INTENT(IN) :: qs(nx-2,ny-2,nz-2)  ! Snow mixing ratio (kg kg**-1)
  REAL, INTENT(IN) :: qh(nx-2,ny-2,nz-2)  ! Hail mixing ratio (kg kg**-1)
  REAL, INTENT(IN) :: t(nx-2,ny-2,nz-2)   ! Temperature (K)

  REAL, INTENT(INOUT) :: rff(nx-2,ny-2,nz-2) ! Reflectivity (dBZ)

  REAL,PARAMETER :: ki2 = 0.176 ! Dielectric factor for ice if other
                                !   than melted drop diameters are used.
  REAL,PARAMETER :: kw2=0.93 ! Dielectric factor for water.

  REAL,PARAMETER :: degKtoC=273.15 ! Conversion factor from degrees K to
                                   !   degrees C

  REAL,PARAMETER :: m3todBZ=1.0E+18 ! Conversion factor from m**3 to
                                    !   mm**6 m**-3.

  REAL,PARAMETER :: pi=3.1415926 ! Pi.

  REAL,PARAMETER :: pipowf=7.0/4.0 ! Power to which pi is raised.

  REAL,PARAMETER :: N0r=8.0E+06 ! Intercept parameter in 1/(m^4) for rain.
  REAL,PARAMETER :: N0s=3.0E+06 ! Intercept parameter in 1/(m^4) for snow.
  REAL,PARAMETER :: N0h=4.0E+04 ! Intercept parameter in 1/(m^4) for hail.

  REAL,PARAMETER :: N0xpowf=3.0/4.0 ! Power to which N0r,N0s & N0h are
                                    !   raised.

  REAL,PARAMETER :: approxpow=0.95 ! Approximation power for hail
                                   !   integral.

  REAL,PARAMETER :: rqrpowf=7.0/4.0 ! Power to which product rho * qr
                                    !   is raised.
  REAL,PARAMETER :: rqsnpowf=7.0/4.0 ! Power to which product rho * qs
                                     !   is raised (dry snow).
  REAL,PARAMETER :: rqsppowf=7.0/4.0 ! Power to which product rho * qs
                                     !   is raised (wet snow).

  REAL,PARAMETER :: rqhpowf=(7.0/4.0)*approxpow ! Power to which product
                                                !   rho * qh is raised.

  REAL,PARAMETER :: rhoi=917.  ! Density of ice (kg m**-3)
  REAL,PARAMETER :: rhor=1000. ! Density of rain (kg m**-3)
  REAL,PARAMETER :: rhos=100.  ! Density of snow (kg m**-3)
  REAL,PARAMETER :: rhoh=913.  ! Density of hail (kg m**-3)

  REAL,PARAMETER :: rhoipowf=2.0     ! Power to which rhoi is raised.
  REAL,PARAMETER :: rhospowf=1.0/4.0 ! Power to which rhos is raised.
  REAL,PARAMETER :: rhoxpowf=7.0/4.0 ! Power to which rhoh is raised.

  REAL,PARAMETER :: Zefact=720.0 ! Multiplier for Ze components.

  REAL,PARAMETER :: lg10mul=10.0 ! Log10 multiplier

  REAL :: rcomp,scomp,hcomp,sumcomp
  INTEGER :: i,j,k
  REAL(8),PARAMETER :: Zerf=3630803470.863780
  REAL(8),PARAMETER :: Zesnegf=958893421.2925436
  REAL(8),PARAMETER :: Zesposf=426068366492.9870
  REAL(8),PARAMETER :: Zehf   =61263782772.99985
  REAL,   PARAMETER :: intvl=3.0
  REAL :: rtem

!-----------------------------------------------------------------------
! Now loop through the scalar grid points.
!-----------------------------------------------------------------------
            rcomp = 0.0
            scomp = 0.0
            hcomp = 0.0

  DO k = 1,nz-2         ! Eric 8/8/03
    DO j = 1,ny-2
      DO i = 1,nx-2

!-----------------------------------------------------------------------
! Check for bad air density value.
!-----------------------------------------------------------------------

        IF (rho(i,j,k) <= 0.0) THEN
          rff(i,j,k) = 0.0
        ELSE

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from rain.
!-----------------------------------------------------------------------

          IF (qr(i,j,k) <= 0.0) THEN
            rcomp = 0.0
          ELSE
            rcomp = Zerf*((qr(i,j,k)*rho(i,j,k)) ** rqrpowf)
          END IF

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from snow (dry or wet).
!-----------------------------------------------------------------------
IF(1==1) THEN
          IF (qs(i,j,k) <= 0.0) THEN
            scomp = 0.0
          ELSE IF (t(i,j,k) <= degKtoC) THEN
            scomp = Zesnegf*((qs(i,j,k)*rho(i,j,k)) ** rqsnpowf)
          ELSE
            scomp = Zesposf*((qs(i,j,k)*rho(i,j,k)) ** rqsppowf)
          END IF

!-----------------------------------------------------------------------
! Calculate reflectivity contribution from hail.
!-----------------------------------------------------------------------

          IF (qh(i,j,k) <= 0.0) THEN
            hcomp = 0.0
          ELSE
            hcomp = Zehf*((qh(i,j,k)*rho(i,j,k)) ** rqhpowf)
          END IF
END IF
!-----------------------------------------------------------------------
! Now add the contributions and convert to logarithmic reflectivity
! factor dBZ.
!-----------------------------------------------------------------------
!         sumcomp = rcomp + scomp + hcomp
!
          if (t(i,j,k) >= (degKtoC + intvl ) ) then
            sumcomp = rcomp
          elseif (t(i,j,k) <= (degKtoC - intvl )) then
            sumcomp = scomp + hcomp
          else
            rtem=(t(i,j,k)-degKtoC+intvl)/(2*intvl)
            sumcomp=rtem*rcomp+(1-rtem)*(scomp+hcomp)
          end if
!
!          if(sumcomp>1.0) print*,'sumcomp===',sumcomp
!
         IF( sumcomp>1.0 ) THEN
           rff(i,j,k) = lg10mul * LOG10(sumcomp)
         ELSE
           rff(i,j,k) = 0.0
         END IF
        END IF !  IF (rho(i,j,k) <= 0.0) ... ELSE ...

      END DO ! DO i
    END DO ! DO j
  END DO ! DO k

  RETURN
END SUBROUTINE reflec_ferrierwrf
