!
!##################################################################
!##################################################################
!######                                                      ######
!######                   PROGRAM ENRELAX2PR                 ######
!######                                                      ######
!##################################################################
!##################################################################
!

PROGRAM enrelax2pr
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  relaxint to prior and incease ensemble spread
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Jidong Gao
!
!  4/25/2014. Written based on ENRELAX2PR.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  USE module_GuessArray
  USE module_ModelArray
  USE module_TruthArray
  USE Model4DArrayE
  USE Model4DARRAYA

  IMPLICIT NONE

!
!-----------------------------------------------------------------------
!
!  Dimension of the base grid (input data).
!
!-----------------------------------------------------------------------
!
  INTEGER :: nx,ny,nz
  INTEGER :: nzsoil, nstyps
!
!-----------------------------------------------------------------------
!
!  MODEL arrays. The last dimension is for the two sets of variables.
!
!-----------------------------------------------------------------------
!
  REAL, ALLOCATABLE :: rf_ob (:,:,:)       ! Temporary array
  REAL, ALLOCATABLE :: rf_bg (:,:,:)       ! Temporary array

  REAL, ALLOCATABLE :: tem1  (:,:,:)       ! Temporary array
  REAL, ALLOCATABLE :: tem2  (:,:,:)       ! Temporary array
  REAL, ALLOCATABLE :: tem3  (:,:,:)       ! Temporary array
  REAL, ALLOCATABLE :: tem4  (:,:,:)       ! Temporary array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i, j, k, l, nq
  REAL :: amin, amax

  CHARACTER (LEN=256) :: basdmpfn
  INTEGER :: lbasdmpf
  CHARACTER (LEN=256) :: ternfn,sfcoutfl,soiloutfl,temchar
  INTEGER :: lternfn,lfn
  INTEGER :: iss,is

  REAL :: zpmax
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'phycst.inc'
  INCLUDE 'globcst.inc'
  INCLUDE 'bndry.inc'
  INCLUDE 'indtflg.inc'
  INCLUDE 'grid.inc'
  INCLUDE 'exbc.inc'
  INCLUDE 'mp.inc'

!-----------------------------------------------------------------------

  INTEGER :: hinfmt,houtfmt, nchin, nchout
  CHARACTER (LEN=256) :: filename,filename_e,filename_a
  CHARACTER (LEN=256) :: grdbasfn,grdbasfn_e,grdbasfn_a
  CHARACTER (LEN=256) :: rdinname_e, rdinname_a, tmpfname
  CHARACTER (LEN=256) :: dirfn_e,dirfn_a
  CHARACTER (LEN=10) :: emname
  INTEGER :: lenfil,lengbf

  INTEGER :: grdbas
  INTEGER :: ireturn

  REAL :: time
  INTEGER :: gboutcnt, vroutcnt

! DATA    gboutcnt, vroutcnt /0,0/
  DATA    vroutcnt /0/

  INTEGER :: nfilemax
  PARAMETER (nfilemax=1)

  INTEGER :: nouttime
  INTEGER, PARAMETER :: nouttimemax=40

  CHARACTER (LEN=256) :: hisfile(nfilemax)
  INTEGER :: nhisfile,nd, length, lenstr
  CHARACTER (LEN=80) :: timsnd
  CHARACTER(LEN=19) :: timestring
  CHARACTER (LEN=80) :: new_runname
  INTEGER :: tmstrln, numdigits, nt

  REAL    :: times(nfilemax), outtime(nouttimemax), alpha, beta
  INTEGER :: iabstinit,iabstinit1
  INTEGER :: ioffset
  INTEGER :: year1,month1,day1,hour1,minute1,second1,ioutabst,          &
             ioutabstinit

  INTEGER :: nxlg, nylg
  INTEGER :: nx_wrf,ny_wrf
  INTEGER :: nensmbl,isub
!
!-----------------------------------------------------------------------
!
!  namelist Declarations:
!
!-----------------------------------------------------------------------
!
  INTEGER :: use_data_t
  CHARACTER (LEN=19) :: initime  ! Real time in form of 'year-mo-dy:hr:mn:ss'

  REAL    :: cfun1,mix_rate,inf_rate,inf_temp
  integer :: assim_time,iter,relaxopt

  NAMELIST /message_passing/ nproc_x, nproc_y,                          &
                             nproc_x_in,  nproc_y_in,                   &
                             nproc_x_out, nproc_y_out

  NAMELIST /input/ modelopt, hinfmt,grdbasfn,nhisfile,hisfile,rdinname_e, &
                   rdinname_a,assim_time,iter,relaxopt,mix_rate,inf_rate

  INTEGER :: idealCase_opt
  CHARACTER (LEN=256) :: grdbasfno,hisfileo,grdbasfnb,hisfileb,rdinname_t
  NAMELIST /ideal_para/idealCase_opt,grdbasfno,hisfileo,                &
                                     grdbasfnb,hisfileb,rdinname_t

  NAMELIST /output/ runname,use_data_t,initime,dirname, nensmbl,        &
            hdmpfmt,grbpkbit,hdfcompr,filcmprs,basout,grdout,           &
            varout,mstout,iceout,tkeout,trbout,rainout,sfcout,          &
            landout, exbcdmp,exbchdfcompr,                              &
            qcexout,qrexout,qiexout,qsexout,qhexout,                    &
            ngbrz,zbrdmp,sfcdmp,soildmp

  INTEGER :: unum         ! unit number for reading in namelist
  CHARACTER(LEN=256)   :: nlfile
  INTEGER :: istatus, P_QGH
  LOGICAL :: iexist

  INTEGER :: n_po
  CHARACTER (LEN=2) :: nfil,niter

  REAL :: rmsu1,rmsv1,rmsw1,rmst1,rmsp1,rmsqv1,rmsrf1
  REAL, ALLOCATABLE :: rmsqs1(:)
  REAL :: rmsu2,rmsv2,rmsw2,rmst2,rmsp2,rmsqv2,rmsrf2
  REAL, ALLOCATABLE :: rmsqs2(:)

  REAL :: sprd_u1,sprd_v1,sprd_w1,sprd_t1,sprd_p1,sprd_qv1,sprd_rf1
  REAL, ALLOCATABLE :: sprd_qs1(:)
  REAL :: sprd_u2,sprd_v2,sprd_w2,sprd_t2,sprd_p2,sprd_qv2,sprd_rf2
  REAL, ALLOCATABLE :: sprd_qs2(:)


  INTERFACE
    SUBROUTINE readwrfvar(nx,ny,nz,nscalar,qnames,file_name,     &
                          u, v, w, pt, ptbar, pp, pbar, ph,      &
                          rhobar, qv, full3d, istatus,           &
                          idealopt, refmos3d)
      IMPLICIT NONE

      INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
      INTEGER :: nscalar

      CHARACTER(LEN=*), INTENT(IN) :: qnames(nscalar)
      CHARACTER(LEN=256) :: file_name

      REAL :: u (nx,ny,nz)         ! x component of perturbation velocity
                                         ! (m/s)
      REAL :: v (nx,ny,nz)         ! y component of perturbation velocity
                                         ! (m/s)
      REAL :: w (nx,ny,nz)         ! vertical component of perturbation
                                         ! velocity in Cartesian coordinates
                                         ! (m/s).
      REAL :: pt (nx,ny,nz)        ! perturbation potential temperature (K)

      REAL :: ph (nx,ny,nz)        ! Mass variale, geopotential for WRF

      REAL :: qv(nx,ny,nz)         ! perturbation water vapor mixing ratio
                                         ! (kg/kg)
      REAL :: full3d(nx,ny,nz,nscalar)

      REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
      REAL :: rhobar(nx,ny,nz)     ! Base state air density (kg/m**3)
      REAL :: pp    (nx,ny,nz)     ! perturbation pressure (Pascal)
      REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal)

      INTEGER, OPTIONAL, INTENT(IN)         :: idealopt
      REAL,    OPTIONAL, INTENT(OUT)        :: refmos3d (nx,ny,nz)

      INTEGER :: istatus

    END SUBROUTINE readwrfvar
  END INTERFACE
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  mgrid = 1
  nestgrd = 0

! alpha = 0.5
! beta = 1.0-alpha

  CALL mpinit_proc(0)

  IF(myproc == 0) THEN
    WRITE(6,'(/9(/2x,a)/)')                                             &
     '###############################################################', &
     '###############################################################', &
     '###                                                         ###', &
     '###                Welcome to ENRELAX2PR                    ###', &
     '###                                                         ###', &
     '###############################################################', &
     '###############################################################'

    unum = COMMAND_ARGUMENT_COUNT()
    IF (unum > 0) THEN
      CALL GET_COMMAND_ARGUMENT(1, nlfile, lenstr, istatus )
      IF ( nlfile(1:1) == ' ' .OR. istatus /= 0 ) THEN  ! Use standard input to be backward-compatible
        unum = 5
      ELSE
        INQUIRE(FILE=TRIM(nlfile),EXIST=iexist)
        IF (.NOT. iexist) THEN
          WRITE(6,'(1x,3a)') 'WARNING: namelist file - ',TRIM(nlfile),  &
                ' does not exist. Falling back to standard input.'
          unum = 5
        END IF
      END IF
    ELSE
      unum = 5
    END IF

    IF (unum /= 5) THEN
      CALL getunit( unum )
      OPEN(unum,FILE=TRIM(nlfile),STATUS='OLD',FORM='FORMATTED')
      WRITE(*,'(1x,3a,/,1x,a,/)') 'Reading ENRELAX2PR namelist from file - ', &
              TRIM(nlfile),' ... ','========================================'
    ELSE
      WRITE(*,'(2(1x,a,/))') 'Waiting namelist from standard input ... ', &
                             '========================================'
    END IF

  END IF

!
!-----------------------------------------------------------------------
!
!  Set the default parameters
!
!-----------------------------------------------------------------------
!
! lvldbg=100
  modelopt  = 999
  hinfmt    = 10
  grdbasfn  = 'X'
  nhisfile  = 1
  hisfile(1) = 'X'
  use_data_t = 1
  initime ='0000-00-00:00:00:00'
  mix_rate=0.5
  inf_rate=1.025

  nouttime = 1
  outtime(1) = 0.0

  gboutcnt  = 0

  runname   = 'runname_not_set'

  dirname   = './'
  exbcdmp   = 1
  exbchdfcompr = 0
  hdmpfmt   = 1
  grbpkbit  = 16
  hdfcompr  = 0
  filcmprs  = 0
  basout    = 0
  grdout    = 0
  varout    = 1
  mstout    = 1
  iceout    = 1
  tkeout    = 1
  trbout    = 0
  rainout   = 0
  sfcout    = 0
  radout    = 0
  flxout    = 0
  snowout   = 0
  landout   = 0
  qcexout   = 0
  qrexout   = 0
  qiexout   = 0
  qsexout   = 0
  qhexout   = 0
  sfcdmp    = 1
  soildmp   = 1
  ngbrz     = 5
  zbrdmp    = 10000.0
!
!-----------------------------------------------------------------------
!
!  Read namelist
!
!-----------------------------------------------------------------------
!
  nproc_x_in  = 1
  nproc_y_in  = 1

  nproc_x_out = 1
  nproc_y_out = 1

  IF(myproc == 0) THEN
    READ(unum,message_passing)
    WRITE(6,'(a)')'Namelist message_passing was successfully read.'
  END IF
  CALL mpupdatei(nproc_x,1)
  CALL mpupdatei(nproc_y,1)
!  CALL mpupdatei(readsplit,1)
  CALL mpupdatei(nproc_x_in, 1)
  CALL mpupdatei(nproc_y_in, 1)
  CALL mpupdatei(nproc_x_out,1)
  CALL mpupdatei(nproc_y_out,1)

  CALL mpinit_var

  readsplit(:) = 0
  IF (mp_opt > 0) THEN
    IF (nproc_x_in == 1 .AND. nproc_y_in ==1) THEN
      readsplit(:) = 1
    ELSE
      IF (nproc_x_in /= nproc_x) nproc_x_in = nproc_x      ! still did not support join or split reading
      IF (nproc_y_in /= nproc_y) nproc_y_in = nproc_y
    END IF
  END IF

  nhisfile = 1

  IF(myproc == 0) THEN
    READ(unum,input, END=100)
    WRITE(6,'(/a/)') ' Input control parameters read in are:'

    IF ( modelopt == 999 ) THEN
      WRITE(6,'(a)') 'The model of input file has not been set.'
      WRITE(6,'(a)') 'Using the default setting modelopt = 1 (For ARPS).'
      modelopt=1
    ELSE IF ( modelopt /= 999 .and. modelopt /= 1 .and. modelopt /= 2 ) THEN
      WRITE(6,'(a)') 'The model option is not supported.'
      WRITE(6,'(a)') 'Job stopped due to the wrong modelopt.'
      CALL arpsstop('ERROR: unsuportted model option.',1)
    END IF

    WRITE(6,'(1x,a,i3)') ' modelopt    = ', modelopt

    IF( hinfmt == 5) THEN
      WRITE(6,'(2(2x,a))')                                                &
          'The Savi3D not supported as an input file format',             &
          'Job stopped in ENRELAX2PR.'
      CALL arpsstop('ERROR: unsuportted file format.',1)
    END IF

    WRITE(6,'(1x,a,i3)') ' hinfmt      = ', hinfmt
    WRITE(6,'(1x,a,i3)') ' nhisfile    = ', nhisfile

    length = LEN_TRIM( grdbasfn )
    WRITE(6,'(1x,a,a)')  ' grdbasfn    = ', grdbasfn(1:length)

    DO i=1,nhisfile
      length = LEN_TRIM( hisfile(i) )
      WRITE(6,'(1x,a,i3,a,a)') ' hisfile(',i,') = ',hisfile(i)(1:length)
    END DO

    IF( modelopt == 2 ) THEN
      IF( relaxopt == 1 .or. relaxopt == 2 ) THEN
        IF( mix_rate < 0 .or. mix_rate > 1 ) THEN
          WRITE(6,'(2x,a)')                                                &
              'The mixing ratio of the forecast and analysis must be greater than 0 and less than 1.'
          CALL arpsstop('ERROR: unsuportted mixing ratio.',1)
        END IF
        WRITE(6,'(1x,a,f5.3)') ' mix_rate   = ', mix_rate
      ELSE IF ( relaxopt == 3 ) THEN
        WRITE(6,'(1x,a,f5.3)') ' inf_rate   = ', inf_rate
      ELSE
        WRITE(6,'(1x,a)') ' Unsupported option for relaxation coefficient. Default option (mixing) is selected.'
        relaxopt=2
        WRITE(6,'(1x,a,f5.3)') ' mix_rate   = ', mix_rate
      END IF
    ELSE
      IF( relaxopt >= 2 ) THEN
        WRITE(6,'(1x,a)') ' Relaxation mode without recenter process is not supported for arps background. Default option (mixing) is selected.'
        relaxopt=1
      END IF
      IF( mix_rate < 0 .or. mix_rate > 1 ) THEN
        WRITE(6,'(2x,a)')                                                &
            'The mixing ratio of the forecast and analysis must be greater than 0 and less than 1.'
        CALL arpsstop('ERROR: unsuportted mixing ratio.',1)
      END IF
      WRITE(6,'(1x,a,f5.3)') ' mix_rate   = ', mix_rate
    END IF
  END IF
  CALL mpupdatei(modelopt,1)
  CALL mpupdatei(hinfmt,1)
  CALL mpupdatec(grdbasfn,256)
  CALL mpupdatec(hisfile, 256*nhisfile)
  CALL mpupdatec(rdinname_e,256)
  CALL mpupdatec(rdinname_a,256)
  CALL mpupdatei(assim_time,256)
  CALL mpupdatei(iter      ,256)
  CALL mpupdatei(relaxopt  ,1)
  CALL mpupdater(mix_rate  ,1)
  CALL mpupdater(inf_rate  ,1)
  beta=mix_rate
  alpha=1.0-beta
!
!-----------------------------------------------------------------------
!
!  Set the control parameters for ideal_para:
!
!-----------------------------------------------------------------------
!
  IF (myproc == 0) THEN
    WRITE(6,'(/a/)')                                                      &
        ' Reading in control parameters for the ideal_para data files..'

    READ(unum,ideal_para,END=100)

    WRITE(6,'(/2x,a,i5)')                                                 &
        'The namelist for ideal_para ssuccessfully readin==',idealCase_opt
  END IF

  CALL mpupdatei(idealCase_opt,1)
  CALL mpupdatec(grdbasfno,256)
  CALL mpupdatec(hisfileo,256)
  CALL mpupdatec(rdinname_t,256)
  CALL mpupdatec(grdbasfnb,256)
  CALL mpupdatec(hisfileb,256)
!
!-----------------------------------------------------------------------
!
!  Set the control parameters for output:
!
!-----------------------------------------------------------------------
!
  IF (myproc == 0) THEN
    WRITE(6,'(/a/)')                                                      &
        ' Reading in control parameters for the output data files..'

    READ(unum,output,END=100)

    IF ( exbchdfcompr > 4 ) rayklow = -1

    WRITE(6,'(/2x,a,a)')                                                  &
        'The run name to be used for constructing output file name is ',  &
        runname
  END IF

  CALL mpupdatec(runname,80)

  new_runname = runname

  CALL mpupdatei(use_data_t,1)
  CALL mpupdatec(initime,19)

  CALL mpupdatei(nouttime,1)
  CALL mpupdater(outtime,nouttimemax)
  CALL mpupdatei(gboutcnt,1)

  CALL mpupdatec(dirname,256)

  CALL mpupdatei(hdmpfmt,1)
  CALL mpupdatei(grbpkbit,1)
  CALL mpupdatei(hdfcompr,1)

  CALL mpupdatei(filcmprs,1)
  CALL mpupdatei(basout,1)
  CALL mpupdatei(grdout,1)
  CALL mpupdatei(varout,1)
  CALL mpupdatei(mstout,1)
  CALL mpupdatei(iceout,1)
  CALL mpupdatei(tkeout,1)
  CALL mpupdatei(trbout,1)
  CALL mpupdatei(rainout,1)
  CALL mpupdatei(sfcout,1)
  CALL mpupdatei(landout,1)
  CALL mpupdatei(exbcdmp,1)
  CALL mpupdatei(exbchdfcompr,1)

  CALL mpupdatei(qcexout,1)
  CALL mpupdatei(qrexout,1)
  CALL mpupdatei(qiexout,1)
  CALL mpupdatei(qsexout,1)
  CALL mpupdatei(qhexout,1)

  CALL mpupdatei(ngbrz,1)
  CALL mpupdater(zbrdmp,1)

  CALL mpupdatei(sfcdmp,1)
  CALL mpupdatei(soildmp,1)

  totout = 1

  IF (mp_opt > 0 .AND. readsplit(FINDX_H) <= 0) THEN
    CALL gtsplitfn(hisfile(1),1,1,loc_x,loc_y,1,1,                      &
                   0,0,1,lvldbg,filename,ireturn)
  ELSE
    WRITE(filename,'(a)') TRIM(hisfile(1))
  END IF

  IF (myproc == 0) THEN
    IF ( modelopt == 1 ) THEN
      CALL get_dims_from_data(hinfmt,trim(filename),                      &
                              nx,ny,nz,nzsoil,nstyps, ireturn)
      IF (readsplit(FINDX_H) > 0) THEN
        nx = (nx-3) / nproc_x + 3
        ny = (ny-3) / nproc_y + 3
      END IF
    ELSE IF ( modelopt == 2 ) THEN
      CALL get_wrf_domtime(TRIM(filename),1,timestring,istatus)
      READ (timestring, '(i4.4,1x,i2.2,1x,i2.2,1x,i2.2,1x,i2.2,1x,i2.2)' ) &
                        year,month,day,hour,minute,second

      CALL get_wrf_domsize(TRIM(filename),timestring,nx_wrf,ny_wrf,     &
                           nz,nzsoil,nt,nx,ny,istatus)

      nx=(nx-1) / nproc_x + 3
      ny=(ny-1) / nproc_y + 3
      nz=nz+2

      CALL get_wrf_domscalar(TRIM(filename),nscalar,nscalarq,qnames,    &
                              P_QC,P_QR,P_QI,P_QS,P_QG,P_QH,            &
                              P_NC,P_NR,P_NI,P_NS,P_NG,P_NH,            &
                              P_ZR,P_ZI,P_ZS,P_ZG,P_ZH,                 &
                              istatus)
    END IF
  END IF
  CALL mpupdatei(nx,1)
  CALL mpupdatei(ny,1)
  CALL mpupdatei(nz,1)
  CALL mpupdatei(nzsoil,1)

  CALL mpupdatei(nensmbl,   1)

  CALL mpupdatei(nscalar, 1)
  CALL mpupdatei(nscalarq,1)
  CALL mpupdatei(P_QC,1);
  CALL mpupdatei(P_QR,1)
  CALL mpupdatei(P_QI,1)
  CALL mpupdatei(P_QS,1)
  CALL mpupdatei(P_QG,1)
  CALL mpupdatei(P_QH,1)
  CALL mpupdatei(P_NC,1)
  CALL mpupdatei(P_NR,1)
  CALL mpupdatei(P_NI,1)
  CALL mpupdatei(P_NS,1)
  CALL mpupdatei(P_NG,1)
  CALL mpupdatei(P_NH,1)
  CALL mpupdatei(P_ZR,1)
  CALL mpupdatei(P_ZI,1)
  CALL mpupdatei(P_ZS,1)
  CALL mpupdatei(P_ZG,1)
  CALL mpupdatei(P_ZH,1)

  CALL mpupdatec(qnames,nscalar*40)
  CALL mpupdatec(qdescp,nscalar*40)

  CALL mpupdatei(nstyps,1)
  IF (nstyps <= 0) nstyps = 1
  nstyp = nstyps
  IF (nstyps <= 0) nstyps = 1
  nstyp = nstyps

  splitdmp = 0
  IF (hdmpfmt > 100 .OR. soildmp > 100 .OR. exbcdmp > 100) THEN
    splitdmp = hdmpfmt/100
    hdmpfmt  = MOD(hdmpfmt,100)

    splitexbc = exbcdmp/100
    exbcdmp   = MOD(exbcdmp,100)

    splitsoil = soildmp/100
    soildmp   = MOD(soildmp,100)

    IF (nproc_x_out > 1 .OR. nproc_y_out > 1) THEN

      IF (MOD(nproc_x_out,nproc_x) /= 0) THEN
        WRITE(6,'(/,1x,2a,I4,a,I4,/)') 'ERROR: ',                       &
        'wrong size of nproc_x_out = ',nproc_x_out,'.',                 &
        'It must be a multipler of nproc_x = ',nproc_x
        CALL arpsstop('Wrong value of nproc_x_out.',1)
      END IF

      IF (MOD(nproc_y_out,nproc_y) /= 0) THEN
        WRITE(6,'(/,1x,2a,I4,a,I4,/)') 'ERROR: ',                       &
        'wrong size of nproc_y_out = ',nproc_y_out,'.',                 &
        'It must be a multipler of nproc_y = ',nproc_y
        CALL arpsstop('Wrong value of nproc_x_out.',1)
      END IF

      nxlg = (nx-3)*nproc_x + 3
      nylg = (ny-3)*nproc_y + 3
      IF (MOD((nxlg-3),nproc_x_out) /= 0) THEN
        WRITE(6,'(/,1x,2a,I4,a,/,8x,a,I5,a,I4,a,/)') 'ERROR: ',         &
        'wrong size of nproc_x_out = ',nproc_x_out,'.',                 &
        'The grid size is ',nxlg,                                       &
        ' and it''s physical size is not dividable by ',nproc_x_out,'.'
        CALL arpsstop('Wrong value of nproc_x_out.',1)
      END IF

      IF (MOD((nylg-3),nproc_y_out) /= 0) THEN
        WRITE(6,'(/,1x,2a,I4,a,/,8x,a,I5,a,I4,a,/)') 'ERROR: ',         &
        'wrong size of nproc_y_out = ',nproc_y_out,'.',                 &
        'The grid size is ',nylg,                                       &
        ' and it''s physical size is not dividable by ',nproc_y_out,'.'
        CALL arpsstop('Wrong value of nproc_y_out.',1)
      END IF

    END IF
  END IF

  joindmp(:) = 0
  IF (mp_opt > 0 .AND. nproc_x_out == 1 .AND. nproc_y_out == 1) THEN
    joindmp(:) = 1
  END IF

  IF (unum /= 5 .AND. myproc == 0) THEN
    CLOSE( unum )
    CALL retunit( unum )
  END IF

  ALLOCATE(rf_ob(nx,ny,nz))
  ALLOCATE(rf_bg(nx,ny,nz))
  ALLOCATE(tem1(nx,ny,nz))
  ALLOCATE(tem2(nx,ny,nz))
  ALLOCATE(tem3(nx,ny,nz))
  ALLOCATE(tem4(nx,ny,nz))

  CALL allocateModelArray(nx,ny,nz,nzsoil,nstyps,nscalar,modelopt)
  CALL allocateGuessArray(nx,ny,nz,nzsoil,nstyps,nscalar,modelopt)
  CALL allocateTruthArray(nx,ny,nz,nzsoil,nstyps,nscalar,modelopt)
  CALL allocateModel4DarrayE(nx,ny,nz,nensmbl,nzsoil,nstyps,nscalar,modelopt,relaxopt)
  CALL allocateModel4DarrayA(nx,ny,nz,nensmbl,nzsoil,nstyps,nscalar,modelopt)
                                                       ! allocate ensembles array

  ALLOCATE(rmsqs1  (nscalarq), STAT = istatus)
  ALLOCATE(rmsqs2  (nscalarq), STAT = istatus)
  ALLOCATE(sprd_qs1(nscalarq), STAT = istatus)
  ALLOCATE(sprd_qs2(nscalarq), STAT = istatus)

  IF (P_QG > 0 .AND. P_QH > 0) THEN
    P_QGH = 2222
  ELSE IF (P_QG > 0) THEN
    P_QGH = P_QG
  ELSE IF (P_QH > 0) THEN
    P_QGH = P_QH
  ELSE
    WRITE(*,'(1x,a)') 'ERROR: no graupel and hail found in the data file.'
    CALL arpsstop('No graupel or hail',1)
  END IF

!-----------------------------------------------------------------------
  IF (idealCase_opt==0 ) THEN
    IF ( modelopt == 1 ) THEN
      rf_ob=100.
    ELSE IF ( modelopt == 2 ) THEN
      rf_ob=-9999.
    END IF
  END IF

  IF (idealCase_opt==1 ) THEN

    lengbf=LEN(grdbasfno)
    CALL strlnth( grdbasfno, lengbf)

    IF (myproc == 0) WRITE(6,'(/a,a)')' The grid/base name is ', grdbasfno(1:lengbf)

    lenfil=LEN_TRIM(hisfileo)

    IF (myproc == 0) WRITE(6,'(/a,a,a)')                                &
          ' Data set ', hisfileo(1:lenfil) ,' to be processed.'
!
!-----------------------------------------------------------------------
!
!  Read all input data arrays
!
!-----------------------------------------------------------------------

    IF ( modelopt == 1 ) THEN
      CALL dtaread(nx,ny,nz,nzsoil,nstyps,                              &
          hinfmt,nchin,grdbasfno(1:lengbf),lengbf,                      &
          hisfileo(1:lenfil),lenfil,time, x,y,z,zp,zpsoil,              &
          uprto(1,1,1 ),vprto(1,1,1 ), wo(1,1,1 ),ptprto(1,1,1 ),       &
          pprto(1,1,1 ),qvprto(1,1,1 ),qscalaro(1,1,1,1 ),              &
          tkeo (1,1,1  ),                                               &
          kmho(1,1,1  ),kmvo (1,1,1  ),                                 &
          ubaro,vbaro,wbaro,ptbaro,pbaro,rhobaro,qvbaro,                &
          soiltypo,stypfrcto,vegtypo,laio,roufnso,vego,                 &
          tsoilo(1,1,1,0  ),qsoilo(1,1,1,0  ),                          &
          wetcanpo(1,1,0  ),snowdptho(1,1  ),                           &
          raingo(1,1  ),rainco(1,1  ),prcrateo(1,1,1  ),                &
          radfrco(1,1,1  ),radswo(1,1  ),rnflxo(1,1  ),                 &
          radswneto(1,1  ),radlwino(1,1  ),                             &
          usflxo(1,1  ),vsflxo(1,1  ),ptsflxo(1,1  ),qvsflxo(1,1  ),    &
          ireturn, tem1, tem2, tem3)

      uo(:,:,:) = uprto(:,:,:) + ubaro(:,:,:)
      vo(:,:,:) = vprto(:,:,:) + vbaro(:,:,:)
      qvo(:,:,:) =qvprto(:,:,:) +qvbaro(:,:,:)

!     print* ,'ubar1111==',(ubaro(i,10,10),i=30,41)
!     print* ,'ptbar111==',(ptbaro(i,10,10),i=30,41)
!     print* ,'rhobar11==',(rhobaro(i,10,10),i=30,41)
!     print* ,'u1111111==',(uo(i,10,10),i=30,41)
!     print* ,'ptprt111==',(ptprto(i,10,10),i=30,41)
!     print* ,'pprt1111==',(pprto(i,10,10),i=30,41)
!     print* ,'pbar1111==',(pbaro(i,10,10),i=30,41)

    ELSE IF ( modelopt == 2 ) THEN

      CALL readwrfvar(nx,ny,nz,nscalar,qnames,hisfileo,                 &
                      uo, vo, wo, ptprto, ptbaro, pprto, pbaro, pho,    &
                      rhobaro, qvo, qscalaro, istatus)

!   print* ,'uo1111111==',(uo(i,10,10),i=30,41)
!   print* ,'vo1111111==',(vo(i,10,10),i=30,41)
!   print* ,'wo1111111==',(wo(i,10,10),i=30,41)
!   print* ,'ptprto111==',(ptprto(i,10,10),i=30,41)
!   print* ,'pprto111111==',(pprto(i,10,10),i=30,41)
!   print* ,'pho111111==',(pho(i,10,10),i=30,41)
!   print* ,'pbaro111111==',(pbaro(i,10,10),i=30,41)
!   print* ,'qvo111111==',(qvo(i,10,10),i=30,41)
!   print* ,'qscalaro1==',(qscalaro(i,10,10,1),i=30,41)

    END IF

    IF (myproc == 0) WRITE(6,'(/a,/2(a,i2),a,i4,a,/3(a,i2),a,f13.3,a/)')&
        'History data read in for time: ',                              &
        'month=',month,',   day=',   day,',   year=',year,',',          &
        'hour =',hour ,',minute=',minute,', second=',second,            &
        ', time=',time,'(s)'


!-----------------------------------------------------------------------

    lengbf=LEN_TRIM(grdbasfnb)

    IF (myproc == 0) WRITE(6,'(/a,a)')' The grid/base name is ', grdbasfnb(1:lengbf)

    lenfil=LEN_TRIM(hisfileb)

    IF (myproc == 0) WRITE(6,'(/a,a,a)')                                &
          ' Data set ', hisfileb(1:lenfil) ,' to be processed.'
!
!-----------------------------------------------------------------------
!
!  Read all input data arrays
!
!-----------------------------------------------------------------------

    CALL setgbrd (0)

    IF ( modelopt == 1 ) THEN

      CALL dtaread(nx,ny,nz,nzsoil,nstyps,                              &
           hinfmt,nchin,grdbasfnb(1:lengbf),lengbf,                     &
           hisfileb(1:lenfil),lenfil,time, x,y,z,zp,zpsoil,             &
           uprtb(1,1,1 ),vprtb(1,1,1 ), wb(1,1,1 ),ptprtb(1,1,1 ),      &
           pprtb(1,1,1 ),qvprtb(1,1,1 ),qscalarb(1,1,1,1 ),             &
           tkeb (1,1,1  ),                                              &
           kmhb(1,1,1  ),kmvb (1,1,1  ),                                &
           ubarb,vbarb,wbarb,ptbarb,pbarb,rhobarb,qvbarb,               &
           soiltypb,stypfrctb,vegtypb,laib,roufnsb,vegb,                &
           tsoilb(1,1,1,0  ),qsoilb(1,1,1,0  ),                         &
           wetcanpb(1,1,0  ),snowdpthb(1,1  ),                          &
           raingb(1,1  ),raincb(1,1  ),prcrateb(1,1,1  ),               &
           radfrcb(1,1,1  ),radswb(1,1  ),rnflxb(1,1  ),                &
           radswnetb(1,1  ),radlwinb(1,1  ),                            &
           usflxb(1,1  ),vsflxb(1,1  ),ptsflxb(1,1  ),qvsflxb(1,1  ),   &
           ireturn, tem1, tem2, tem3)

      ub(:,:,:) = uprtb(:,:,:) + ubarb(:,:,:)
      vb(:,:,:) = vprtb(:,:,:) + vbarb(:,:,:)
      qvb(:,:,:) =qvprtb(:,:,:) +qvbarb(:,:,:)

!   print* ,'ubar1111==',(ubarb(i,10,10),i=30,41)
!   print* ,'ptbar111==',(ptbarb(i,10,10),i=30,41)
!   print* ,'rhobar11==',(rhobarb(i,10,10),i=30,41)
!   print* ,'u1111111==',(ub(i,10,10),i=30,41)
!   print* ,'ptprt111==',(ptprtb(i,10,10),i=30,41)
!   print* ,'pprt1111==',(pprtb(i,10,10),i=30,41)
!   print* ,'pbar1111==',(pbarb(i,10,10),i=30,41)

    ELSE IF ( modelopt == 2 ) THEN

      CALL readwrfvar(nx,ny,nz,nscalar,qnames,hisfileb,                 &
                      ub, vb, wb, ptprtb, ptbarb, pprtb, pbarb, phb,    &
                      rhobarb, qvb, qscalarb, istatus)

!   print* ,'ub1111111==',(ub(i,10,10),i=30,41)
!   print* ,'vb1111111==',(vb(i,10,10),i=30,41)
!   print* ,'wb1111111==',(wb(i,10,10),i=30,41)
!   print* ,'ptprtb111==',(ptprtb(i,10,10),i=30,41)
!   print* ,'pprtb111111==',(pprtb(i,10,10),i=30,41)
!   print* ,'phb111111==',(phb(i,10,10),i=30,41)
!   print* ,'qvb111111==',(qvb(i,10,10),i=30,41)
!   print* ,'qscalarb1==',(qscalarb(i,10,10,1),i=30,41)

    END IF

    IF (myproc == 0) WRITE(6,'(/a,/2(a,i2),a,i4,a,/3(a,i2),a,f13.3,a/)')&
      'History data read in for time: ',                                &
      'month=',month,',   day=',   day,',   year=',year,',',            &
      'hour =',hour ,',minute=',minute,', second=',second,              &
      ', time=',time,'(s)'
!
!------------------------------------------------------------------------
!
!  open( 21, file='./gnuplot/var_u_h',  position='APPEND' )
!  open( 22, file='./gnuplot/var_v_h',  position='APPEND' )
!  open( 23, file='./gnuplot/var_w_h',  position='APPEND' )
!  open( 24, file='./gnuplot/var_pt_h', position='APPEND' )
!  open( 25, file='./gnuplot/var_pp_h', position='APPEND' )
!  open( 26, file='./gnuplot/var_qv_h', position='APPEND' )
!  open( 27, file='./gnuplot/var_qc_h', position='APPEND' )
!  open( 28, file='./gnuplot/var_qr_h', position='APPEND' )
!  open( 29, file='./gnuplot/var_qi_h', position='APPEND' )
!  open( 30, file='./gnuplot/var_qs_h', position='APPEND' )
!  open( 31, file='./gnuplot/var_qh_h', position='APPEND' )
!  open( 212,file='./gnuplot/var_rf_h', position='APPEND' )

    CAll temper(nx,ny,nz, ptprto, ptbaro, pprto, pbaro, tem1)

    IF (P_QGH > 1000) THEN
      tem2(:,:,:) = qscalaro(:,:,:,P_QG)+qscalaro(:,:,:,P_QH)
    ELSE
      tem2(:,:,:) = qscalaro(:,:,:,P_QGH)
    END IF
    CALL reflec_ferrier2(nx,ny,nz,rhobaro,qscalaro(:,:,:,P_QR),         &
            qscalaro(:,:,:,P_QS),tem2,tem1,rf_ob)

    IF (P_QGH > 1000) THEN
      tem3(:,:,:) = qscalarb(:,:,:,P_QG)+qscalarb(:,:,:,P_QH)
    ELSE
      tem3(:,:,:) = qscalarb(:,:,:,P_QGH)
    END IF
    CALL reflec_ferrier2(nx,ny,nz,rhobarb,qscalarb(1,1,1,P_QR),         &
            qscalarb(1,1,1,P_QS),tem3,tem1,rf_bg)

    rmsu1 =0.0
    rmsv1 =0.0
    rmsw1 =0.0
    rmst1 =0.0
    rmsp1 =0.0
    rmsqv1=0.0
    rmsqs1(:)=0.0
    rmsrf1=0.0

    n_po= 0

    DO k=2,nz-1
      DO j=2,ny-1
        DO i=2,nx-1

          IF( rf_ob(i ,j ,k)>10.0 ) THEN
            n_po =n_po+1
            rmsu1=rmsu1+   ( uo(i,j,k)-  ub(i,j,k) )**2
            rmsv1=rmsv1+   ( vo(i,j,k)-  vb(i,j,k) )**2
            rmsw1=rmsw1+   ( wo(i,j,k)-  wb(i,j,k) )**2
            rmst1=rmst1+   (ptprto(i,j,k)- ptprtb(i,j,k) )**2
            IF ( modelopt == 1 ) rmsp1=rmsp1+   ( pprto(i,j,k)-  pprtb(i,j,k) )**2
            IF ( modelopt == 2 ) rmsp1=rmsp1+   ( pho(i,j,k)-  phb(i,j,k) )**2
            rmsqv1=rmsqv1 +(qvo(i,j,k)- qvb(i,j,k) )**2
            DO nq = 1,nscalarq
              rmsqs1(nq)=rmsqs1(nq) +(qscalaro(i,j,k,nq)- qscalarb(i,j,k,nq) )**2
            END DO
            rmsrf1=rmsrf1 +(rf_ob(i,j,k)- rf_bg(i,j,k) )**2
          END IF
        END DO
      END DO
    END DO

    CALL mptotal(rmsu1)
    CALL mptotal(rmsv1)
    CALL mptotal(rmsw1)
    CALL mptotal(rmst1)
    CALL mptotal(rmsp1)
    CALL mptotal(rmsqv1)
    CALL mpsumr(rmsqs1,nscalarq)
    CALL mptotal(rmsrf1)
    CALL mptotali(n_po)

    IF (myproc==0) THEN
      IF (n_po>0) THEN
        rmsu1=sqrt(rmsu1/n_po)
        rmsv1=sqrt(rmsv1/n_po)
        rmsw1=sqrt(rmsw1/n_po)
        rmst1=sqrt(rmst1/n_po)
        rmsp1=sqrt(rmsp1/n_po)
        rmsqv1=sqrt( rmsqv1/n_po)
        rmsqs1(:)=sqrt( rmsqs1(:)/n_po)
        rmsrf1=sqrt( rmsrf1/n_po)
      END IF
!
!
      print*,'3DVAR rmsu1   ===', rmsu1
      print*,'3DVAR rmsv1   ===', rmsv1
      print*,'3DVAR rmsw1   ===', rmsw1
      print*,'3DVAR rmst1   ===', rmst1
      print*,'3DVAR rmsp1   ===', rmsp1
      print*,'3DVAR rmsqv1, ===', rmsqv1*1000.0
      print*,'3DVAR rmsqs1(:), ===', rmsqs1(:)*1000.0
      print*,'3DVAR rmsrf1, ===', rmsrf1
      print*,'               '

      open( 21, file='./gnuplot/var_u_h',  position='APPEND' )
      open( 22, file='./gnuplot/var_v_h',  position='APPEND' )
      open( 23, file='./gnuplot/var_w_h',  position='APPEND' )
      open( 24, file='./gnuplot/var_pt_h', position='APPEND' )
      open( 25, file='./gnuplot/var_pp_h', position='APPEND' )
      open( 26, file='./gnuplot/var_qv_h', position='APPEND' )
      open( 30, file='./gnuplot/var_qs_h', position='APPEND' )
      open( 212,file='./gnuplot/var_rf_h', position='APPEND' )

      write(21, *) 30+5*(iter-1), rmsu1
      write(22, *) 30+5*(iter-1), rmsv1
      write(23, *) 30+5*(iter-1), rmsw1
      write(24, *) 30+5*(iter-1), rmst1
      write(25, *) 30+5*(iter-1), rmsp1
      write(26, *) 30+5*(iter-1), rmsqv1*1000.0
      write(30, *) 30+5*(iter-1), rmsqs1(:)*1000.0
      write(212,*) 30+5*(iter-1), rmsrf1
    END IF

  END IF ! read in reference truth only for idealized case

!
!-----------------------------------------------------------------------
!
!  read in 3DVAR analysis
!
!-----------------------------------------------------------------------
!
  lengbf=LEN_TRIM(grdbasfn)
  IF (myproc == 0) WRITE(6,'(/a,a)')' The grid/base name is ', grdbasfn(1:lengbf)

!    ireturn = 0

!   IF (mp_opt > 0 .AND. readsplit(FINDX_H) <= 0) THEN
!     CALL gtsplitfn(hisfile(1),1,1,loc_x,loc_y,1,1,                   &
!                    0,0,1,lvldbg,filename,ireturn)
!   ELSE
      filename = hisfile(1)
!   END IF

  lenfil=LEN_TRIM(filename)
  IF (myproc == 0) WRITE(6,'(/a,a,a)')                                  &
        ' Data set ', filename(1:lenfil) ,' to be processed.'
!
!-----------------------------------------------------------------------
!
!  Read all input data arrays
!
!-----------------------------------------------------------------------
  CALL setgbrd (0)

  IF ( modelopt == 1 ) THEN

    CALL dtaread(nx,ny,nz,nzsoil,nstyps,                                &
        hinfmt,nchin,grdbasfn(1:lengbf),lengbf,                         &
        filename(1:lenfil),lenfil,time, x,y,z,zp,zpsoil,                &
        uprt(1,1,1   ),vprt (1,1,1   ),  w(1,1,1   ),ptprt(1,1,1   ),   &
        pprt(1,1,1   ),qvprt(1,1,1   ),qscalar(1,1,1,1   ),             &
        tke  (1,1,1   ),                                                &
        kmh (1,1,1   ),kmv  (1,1,1   ),                                 &
        ubar,vbar,wbar,ptbar,pbar,rhobar,qvbar,                         &
        soiltyp,stypfrct,vegtyp,lai,roufns,veg,                         &
        tsoil(1,1,1,0   ),qsoil(1,1,1,0   ),                            &
        wetcanp(1,1,0   ),snowdpth(1,1   ),                             &
        raing(1,1   ),rainc(1,1   ),prcrate(1,1,1   ),                  &
        radfrc(1,1,1   ),radsw(1,1   ),rnflx(1,1   ),                   &
        radswnet(1,1   ),radlwin(1,1   ),                               &
        usflx(1,1   ),vsflx(1,1   ),ptsflx(1,1   ),qvsflx(1,1   ),      &
        ireturn, tem1, tem2, tem3)


    u(:,:,:)  =  uprt(:,:,:) + ubar(:,:,:)
    v(:,:,:)  =  vprt(:,:,:) + vbar(:,:,:)
    qv(:,:,:) = qvprt(:,:,:) +qvbar(:,:,:)

!   print* ,'ubar2222==',(ubar(i,10,10),i=30,41)
!   print* ,'ptbar222==',(ptbar(i,10,10),i=30,41)
!   print* ,'rhobar22==',(rhobar(i,10,10),i=30,41)
!   print* ,'u2222222==',(u(i,10,10),i=30,41)
!   print* ,'ptprt222==',(ptprt(i,10,10),i=30,41)
!   print* ,'pprt2222==',(pprt(i,10,10),i=30,41)
!   print* ,'pbar2222==',(pbar(i,10,10),i=30,41)

  ELSE IF ( modelopt == 2 ) THEN

    CALL readwrfvar(nx,ny,nz,nscalar,qnames,filename,                   &
                    u, v, w, ptprt, ptbar, pprt, pbar, ph,              &
                    rhobar, qv, qscalar, istatus,                       &
                    idealopt=idealCase_opt,refmos3d=rf_ob)

!   print* ,'u1111111==',(u(i,10,10),i=30,41)
!   print* ,'v1111111==',(v(i,10,10),i=30,41)
!   print* ,'w1111111==',(w(i,10,10),i=30,41)
!   print* ,'ptprt111==',(ptprt(i,10,10),i=30,41)
!   print* ,'pprt111111==',(pprt(i,10,10),i=30,41)
!   print* ,'ph111111==',(ph(i,10,10),i=30,41)
!   print* ,'qv111111==',(qv(i,10,10),i=30,41)

  END IF

  IF (myproc == 0) WRITE(6,'(/a,/2(a,i2),a,i4,a,/3(a,i2),a,f13.3,a/)')  &
        'History data read in for time: ',                              &
        'month=',month,',   day=',   day,',   year=',year,',',          &
        'hour =',hour ,',minute=',minute,', second=',second,            &
        ', time=',time,'(s)'

  IF(idealCase_opt==1) THEN  !statistical score only for control member
!  open( 21, file='./gnuplot/var_u_h',  position='APPEND' )
!  open( 22, file='./gnuplot/var_v_h',  position='APPEND' )
!  open( 23, file='./gnuplot/var_w_h',  position='APPEND' )
!  open( 24, file='./gnuplot/var_pt_h', position='APPEND' )
!  open( 25, file='./gnuplot/var_pp_h', position='APPEND' )
!  open( 26, file='./gnuplot/var_qv_h', position='APPEND' )
!  open( 27, file='./gnuplot/var_qc_h', position='APPEND' )
!  open( 28, file='./gnuplot/var_qr_h', position='APPEND' )
!  open( 29, file='./gnuplot/var_qi_h', position='APPEND' )
!  open( 30, file='./gnuplot/var_qs_h', position='APPEND' )
!  open( 31, file='./gnuplot/var_qh_h', position='APPEND' )
!  open( 212,file='./gnuplot/var_rf_h', position='APPEND' )
!
    rf_bg=0.0

    CAll temper(nx,ny,nz, ptprto, ptbaro, pprto, pbaro, tem2)

    IF (P_QGH > 1000) THEN
      tem1(:,:,:) = qscalar(:,:,:,P_QG)+qscalar(:,:,:,P_QH)
    ELSE
      tem1(:,:,:) = qscalar(:,:,:,P_QGH)
    END IF
    CALL reflec_ferrier2(nx,ny,nz,rhobar ,qscalar(1,1,1,P_QR),          &
            qscalar (1,1,1,P_QS),tem1,tem2,rf_bg)
!
    rmsu2 =0.0
    rmsv2 =0.0
    rmsw2 =0.0
    rmst2 =0.0
    rmsp2 =0.0
    rmsqv2=0.0
    rmsqs2(:)=0.0
    rmsrf2=0.0

    n_po= 0


    do k=2,nz-1
      do j=2,ny-1
        do i=2,nx-1

          IF( rf_ob(i ,j ,k)>10.0 ) then
            n_po =n_po+1
            rmsu2=rmsu2+   ( uo(i,j,k)-  u(i,j,k) )**2
            rmsv2=rmsv2+   ( vo(i,j,k)-  v(i,j,k) )**2
            rmsw2=rmsw2+   ( wo(i,j,k)-  w(i,j,k) )**2
            rmst2=rmst2+   (ptprto(i,j,k)- ptprt(i,j,k) )**2
            IF ( modelopt == 1 ) rmsp2=rmsp2+   ( pprto(i,j,k)-  pprt(i,j,k) )**2
            IF ( modelopt == 2 ) rmsp2=rmsp2+   ( pho(i,j,k)-  ph(i,j,k) )**2
            rmsqv2=rmsqv2 +(qvo(i,j,k)- qv(i,j,k) )**2
            DO nq = 1, nscalarq
              rmsqs2(nq)=rmsqs2(nq) +(qscalaro(i,j,k,nq)- qscalar(i,j,k,nq) )**2
            END DO
            rmsrf2=rmsrf2 +(rf_ob(i,j,k)- rf_bg(i,j,k) )**2
          END IF
        end do
      end do
    end do

    CALL mptotal(rmsu2)
    CALL mptotal(rmsv2)
    CALL mptotal(rmsw2)
    CALL mptotal(rmst2)
    CALL mptotal(rmsp2)
    CALL mptotal(rmsqv2)
    CALL mpsumr(rmsqs2,nscalarq)
    CALL mptotal(rmsrf2)
    CALL mptotali(n_po)

    IF (myproc==0) THEN
      IF (n_po>0) THEN
       rmsu2=sqrt(rmsu2/n_po)
       rmsv2=sqrt(rmsv2/n_po)
       rmsw2=sqrt(rmsw2/n_po)
       rmst2=sqrt(rmst2/n_po)
       rmsp2=sqrt(rmsp2/n_po)
       rmsqv2=sqrt( rmsqv2/n_po )
       rmsqs2(:)=sqrt( rmsqs2(:)/n_po )
       rmsrf2=sqrt( rmsrf2/n_po )
      END IF

      print*,'3DVAR rmsu2   ===', rmsu2
      print*,'3DVAR rmsv2   ===', rmsv2
      print*,'3DVAR rmsw2   ===', rmsw2
      print*,'3DVAR rmst2   ===', rmst2
      print*,'3DVAR rmsp2   ===', rmsp2
      print*,'3DVAR rmsqv2, ===', rmsqv2*1000.0
      print*,'3DVAR rmsqs2(:), ===', rmsqs2(:)*1000.0
      print*,'3DVAR rmsrf2, ===', rmsrf2
      print*,'               '

      write(21, *) 30+5*(iter-1), rmsu2
      write(22, *) 30+5*(iter-1), rmsv2
      write(23, *) 30+5*(iter-1), rmsw2
      write(24, *) 30+5*(iter-1), rmst2
      write(25, *) 30+5*(iter-1), rmsp2
      write(26, *) 30+5*(iter-1), rmsqv2*1000.0
      write(30, *) 30+5*(iter-1), rmsqs2(:)*1000.0
      write(212,*) 30+5*(iter-1), rmsrf2
    END IF

  END IF

!   CALL ctim2abss(year,month,day,hour,minute,second,iabstinit)

!     year1  = year
!     month1 = month
!     day1   = day
!     hour1  = hour
!     minute1= minute
!     second1= second
!     iabstinit1 = iabstinit

!   times(nd) = time + int(iabstinit - iabstinit1)

!   ELSE IF (modelopt == 2) THEN

!-----------------------------------------------------------------------
!
!     Read WRF history files
!
!-----------------------------------------------------------------------
!
!     WRITE(filename,'(2a,I4.4,5(a,I2.2))')  TRIM(dirfn),'wrfout_d01_',    &
!                  year,'-',month,'-',day,'_',hour,':',minute,':',second

!     CALL dtaread4wrf(nx,ny,nz,nscalar,qnames,filename,numdigits,         &
!          x,y,z,zp,u (:,:,: ), v (:,:,: ),w (:,:,: ),                     &
!          ptprte(:,:,: ),pprte(:,:,: ),qv (:,:,: ),qscalar (:,:,:,: ),    &
!          ptbar, pbar, rhobar,                                            &
!          ph (:,:,: ), mu (:,: ), istatus)

!     temr01(:,:,:,l)=zp(:,:,:)

!   END IF


! END DO
!
!-----------------------------------------------------------------------

  ldirnam=LEN_TRIM(dirname)

  IF (mp_opt > 0 .AND. readsplit(FINDX_H) <= 0) THEN
    CALL gtsplitfn(grdbasfn,1,1,loc_x,loc_y,1,1,                        &
                   0,0,1,lvldbg,filename,ireturn)
    lengbf = LEN_TRIM(filename)
    grdbasfn(1:lengbf) = TRIM(filename)
  END IF

!
!-----------------------------------------------------------------------
!
!  Loop over data files
!
!-----------------------------------------------------------------------
!
  ireturn = 0

  DO nd=1,nensmbl

    WRITE(emname,'(I6.6)') int(assim_time)

    IF ( modelopt == 1 ) THEN

!  IF( iter==1 ) THEN
      CALL get_input_dirname(rdinname_e,nd,dirfn_e)
      WRITE(grdbasfn_e,'(a,a,I3.3,a)') TRIM(dirfn_e),'enf',nd,'.hdfgrdbas'
      WRITE(filename_e,'(a,a,I3.3,a)') TRIM(dirfn_e),'enf',nd,'.hdf'//trim(emname)
!     WRITE(filename_e,'(a,a,I3.3,a)') TRIM(dirfn_e),'enf',nd,'.hdf000000'
      lenfil=LEN(filename_e)

!  END IF
!
!-----------------------------------------------------------------------
!
!  Read all input data arrays
!
!-----------------------------------------------------------------------
!
      CALL dtaread(nx,ny,nz,nzsoil,nstyps,                                &
          hinfmt,nchin,grdbasfn_e(1:lengbf),lengbf,                       &
          filename_e(1:lenfil),lenfil,time, x,y,z,zp,zpsoil,              &
          uprt(1,1,1   ),vprt (1,1,1   ),we (1,1,1,nd),ptprte(1,1,1,nd),  &
          pprte(1,1,1,nd),qvprt(1,1,1   ),qscalare(1,1,1,1,nd),           &
          tkee  (1,1,1,nd),                                               &
          kmh (1,1,1   ),kmv  (1,1,1   ),                                 &
          ubar,vbar,wbar,ptbar,pbar,rhobar,qvbar,                         &
          soiltyp,stypfrct,vegtyp,lai,roufns,veg,                         &
          tsoile(1,1,1,0,nd),qsoile(1,1,1,0,nd),                          &
          wetcanpe(1,1,0,nd),snowdpthe(1,1,nd),                           &
          rainge(1,1,nd),raince(1,1,nd),prcratee(1,1,1,nd),               &
          radfrce(1,1,1,nd),radswe(1,1,nd),rnflxe(1,1,nd),                &
          radswnete(1,1,nd),radlwine(1,1,nd),                             &
          usflxe(1,1,nd),vsflxe(1,1,nd),ptsflxe(1,1,nd),qvsflxe(1,1,nd),  &
          ireturn, tem1, tem2, tem3)

      print*,'ubarens=',(ubar(i,10,10), i=1,10)
      print*,'rhobare=', (rhobar(i,10,10),i=1,10)
      print*,'ptprte=',  (ptprt(i,10,10),i=1,10)
      print*,'pprte=',   (pprt(i,10,10),i=1,10)

      ue(:,:,:,nd)  = uprt(:,:,:) + ubar(:,:,:)
      ve(:,:,:,nd)  = vprt(:,:,:) + vbar(:,:,:)
      qve(:,:,:,nd) = qvprt(:,:,:) +qvbar(:,:,:)

    ELSE IF ( modelopt == 2 ) THEN

!   CALL get_input_dirname(rdinname_e,nd,dirfn_e)
!   WRITE(grdbasfn_e,'(a,a,I3.3,a)') TRIM(dirfn_e),'enf',nd,'.hdfgrdbas'
!   WRITE(filename_e,'(a,a,I3.3,a)') TRIM(dirfn_e),'enf',nd,'.hdf'//trim(emname)
!   lenfil=LEN(filename_e)
      READ(initime,'(i4.4,1x,i2.2,1x,i2.2,1x,i2.2,1x,i2.2,1x,i2.2)')    &
                   year,month,day,hour,minute,second
!   CALL get_input_dirname(rdinname_e,nd,dirfn_e)
      WRITE(filename_e,'(2a,I4.4,5(a,I2.2),a,I0)') TRIM(rdinname_e),'wrfout_d01_',  &
                       year,'-',month,'-',day,'_',hour,':',minute,':',second,'_',nd
      IF (myproc==0) print*,filename_e

      lenfil=LEN(filename_e)

      CALL readwrfvar(nx,ny,nz,nscalar,qnames,filename_e,               &
                    ue(:,:,:,nd), ve(:,:,:,nd), we(:,:,:,nd),           &
                    ptprte(:,:,:,nd), ptbar,                            &
                    pprte(:,:,:,nd), pbar, phe(:,:,:,nd),               &
                    rhobare(:,:,:,nd), qve(:,:,:,nd),                   &
                    qscalare(:,:,:,:,nd), istatus)

      IF (myproc==0) print*,'Ensemble variable reading has finished!'

!   print* ,'ue111111==',(ue(i,10,10,nd),i=30,41)
!   print* ,'ve111111==',(ve(i,10,10,nd),i=30,41)
!   print* ,'we111111==',(we(i,10,10,nd),i=30,41)
!   print* ,'ptprte11==',(ptprte(i,10,nd,10),i=30,41)
!   print* ,'phe11111==',(phe(i,10,10,nd),i=30,41)
!   print* ,'qve11111==',(qve(i,10,10,nd),i=30,41)
!   print* ,'qscalare1==',(qscalare(i,10,10,1,nd),i=30,41)

    END IF

!   WRITE(emname,'(I6.6)') int(assim_time)
    WRITE(emname,'(I6.6)') int(0.0       )

    IF ( modelopt == 1 ) THEN

      CALL get_input_dirname(rdinname_a,nd,dirfn_a)
      WRITE(grdbasfn_a,'(a,a,I3.3,a)') TRIM(dirfn_a),'ena',nd,'.hdfgrdbas'
      WRITE(filename_a,'(a,a,I3.3,a)') TRIM(dirfn_a),'ena',nd,'.hdf'//trim(emname)
      lenfil=LEN(filename_a)

      CALL dtaread(nx,ny,nz,nzsoil,nstyps,                              &
          hinfmt,nchin,grdbasfn_a(1:lengbf),lengbf,                     &
          filename_a(1:lenfil),lenfil,time, x,y,z,zp,zpsoil,            &
          uprt(1,1,1   ),vprt(1,1,1   ),wa (1,1,1,nd),ptprta(1,1,1,nd), &
          pprta(1,1,1,nd),qvprt(1,1,1   ),qscalara(1,1,1,1,nd),         &
          tkea  (1,1,1,nd),                                             &
          kmh (1,1,1   ),kmv  (1,1,1   ),                               &
          ubar,vbar,wbar,ptbar,pbar,rhobar,qvbar,                       &
          soiltyp,stypfrct,vegtyp,lai,roufns,veg,                       &
          tsoila(1,1,1,0,nd),qsoila(1,1,1,0,nd),                        &
          wetcanpa(1,1,0,nd),snowdptha(1,1,nd),                         &
          rainga(1,1,nd),rainca(1,1,nd),prcratea(1,1,1,nd),             &
          radfrca(1,1,1,nd),radswa(1,1,nd),rnflxa(1,1,nd),              &
          radswneta(1,1,nd),radlwina(1,1,nd),                           &
          usflxa(1,1,nd),vsflxa(1,1,nd),ptsflxa(1,1,nd),qvsflxa(1,1,nd),&
          ireturn, tem1, tem2, tem3)

! print*,'ubarana=',(ubar(i,10,10), i=1,10)
! print*,'rhobara=', (rhobar(i,10,10),i=1,10)
! print*,'ptprta=',  (ptprt(i,10,10),i=1,10)
! print*,'pprta=',   (pprt(i,10,10),i=1,10)
!
! stop
!
      ua(:,:,:,nd) = uprt(:,:,:) + ubar(:,:,:)
      va(:,:,:,nd) = vprt(:,:,:) + vbar(:,:,:)
      qva(:,:,:,nd) =qvprt(:,:,:) +qvbar(:,:,:)

    ELSE IF ( modelopt == 2 ) THEN

!     CALL get_input_dirname(rdinname_a,nd,dirfn_a)
      WRITE(filename_a,'(2a,I4.4,5(a,I2.2),a,I0)') TRIM(rdinname_a),'wrfout_d01_',  &
                       year,'-',month,'-',day,'_',hour,':',minute,':',second,'_',nd
      IF (myproc==0) print*,filename_a
      lenfil=LEN(filename_a)

      CALL readwrfvar(nx,ny,nz,nscalar,qnames,filename_a,               &
                      ua(:,:,:,nd), va(:,:,:,nd), wa(:,:,:,nd),         &
                      ptprta(:,:,:,nd), ptbar,                          &
                      pprta(:,:,:,nd), pbar, pha(:,:,:,nd),             &
                      rhobara(:,:,:,nd), qva(:,:,:,nd),                 &
                      qscalara(:,:,:,:,nd), istatus)

!   IF (myproc==0) print*,'Analysis variable reading has finished!'
!   print* ,'qscalara1==',(qscalara(i,10,10,1,nd),i=30,41)

!   IF (myproc == 0) WRITE(6,'(/1x,a,i2.2,a/)')                              &
!       'Min. and max. of data for member ',nd,' before relaxing process are:'

!   IF (modelopt==1) THEN
!     CALL a3dmax0(pprta(1,1,1,nd),1,nx-3,1,nx-3,1,ny-3,1,ny-3,1,nz-3,1,nz-3,     &
!                 amax,amin)
!     IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'pprtamin = ', amin,',  pprtamax =',amax
!   ELSE IF (modelopt==2) THEN
!     CALL a3dmax0(pha(1,1,1,nd),1,nx-3,1,nx-3,1,ny-3,1,ny-3,1,nz-3,1,nz-3, &
!                 amax,amin)
!     IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'phamin = ', amin,',  phamax =',amax
!   END IF

!   CALL a3dmax0(rhobara(1,1,1,nd),1,nx-3,1,nx-3,1,ny-3,1,ny-3,1,nz-3,1,nz-3, &
!               amax,amin)
!   IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'rhobarmin=', amin,',  rhobarmax =',amax

!   CALL a3dmax0(ua  (1,1,1,nd),1,nx-2,1,nx-2,1,ny-3,1,ny-3,1,nz-3,1,nz-3,    &
!               amax,amin)
!   IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'uamin = ', amin,',  uamax =',amax

!   CALL a3dmax0(va  (1,1,1,nd),1,nx-3,1,nx-3,1,ny-2,1,ny-2,1,nz-3,1,nz-3,    &
!               amax,amin)
!   IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'vamin = ', amin,',  vamax =',amax

!   CALL a3dmax0(wa(1,1,1,nd),1,nx-3,1,nx-3,1,ny-3,1,ny-3,1,nz-2,1,nz-2,      &
!               amax,amin)
!   IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'wamin = ', amin,',  wamax =',amax

!   CALL a3dmax0(ptprta(1,1,1,nd),1,nx-3,1,nx-3,1,ny-3,1,ny-3,1,nz-3,1,nz-3,  &
!               amax,amin)
!   IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'ptprtmin= ', amin,', ptprtmax =',amax

!   CALL a3dmax0(qva  (1,1,1,nd),1,nx-3,1,nx-3,1,ny-3,1,ny-3,1,nz-3,1,nz-3,   &
!               amax,amin)
!   IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'qvamin= ', amin,', qvamax =',amax

!   DO nq = 1,nscalar

!     CALL a3dmax0(qscalara(:,:,:,nq,nd),1,nx-3,1,nx-3,1,ny-3,1,ny-3,         &
!                  1,nz-3,1,nz-3,amax,amin)

!     IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') TRIM(qnames(nq))//'min   = ', amin,    &
!                           ',  '//TRIM(qnames(nq))//'max   =',  amax

!   END DO

    END IF

    IF (myproc == 0) WRITE(6,'(/a,/2(a,i2),a,i4,a,/3(a,i2),a,f13.3,a/)')&
        'History data read in for time: ',                              &
        'month=',month,',   day=',   day,',   year=',year,',',          &
        'hour =',hour ,',minute=',minute,', second=',second,            &
        ', time=',time,'(s)'

!   CALL ctim2abss(year,month,day,hour,minute,second,iabstinit)

!   IF(nd == 1) THEN  ! Save the values for data set 1
!     year1  = year
!     month1 = month
!     day1   = day
!     hour1  = hour
!     minute1= minute
!     second1= second
!     iabstinit1 = iabstinit
!   END IF

!   ELSE IF (modelopt == 1) THEN

!     WRITE(filename,'(2a,I4.4,5(a,I2.2))')  TRIM(dirfn),'wrfout_d01_',    &
!                  year,'-',month,'-',day,'_',hour,':',minute,':',second

!     CALL dtaread4wrf(nx,ny,nz,nscalar,qnames,filename,numdigits,         &
!          x,y,z,zp,ue(:,:,:,l), ve(:,:,:,l),we(:,:,:,l),                  &
!          ptprte(:,:,:,l),pprte(:,:,:,l),qve(:,:,:,l),qscalare(:,:,:,:,l),&
!          ptbar, pbar, rhobar,                                            &
!          phe(:,:,:,l), mue(:,:,l), istatus)

!     temr01(:,:,:,l)=zp(:,:,:)

!   END IF

  END DO    !end of nd

  IF( hinfmt == 9 .AND. ireturn == 2 ) THEN
    WRITE(6,'(/1x,a/)') 'The end of GrADS file was reached.'
    CLOSE ( nchin )
    CALL retunit( nchin )
    GO TO 9001
  END IF

  IF( ireturn /= 0 ) GO TO 9002            ! Read was unsuccessful

  IF( use_data_t == 1) THEN ! Use init time in input file
    ioutabstinit=iabstinit1
    year  = year1
    month = month1
    day   = day1
    hour  = hour1
    minute= minute1
    second= second1
  ELSE
    READ(initime,'(i4.4,1x,i2.2,1x,i2.2,1x,i2.2,1x,i2.2,1x,i2.2)')      &
                   year,month,day,hour,minute,second
    CALL ctim2abss(year,month,day,hour,minute,second,ioutabstinit)
  END IF

!PRINT*,'ioutabstinit=',ioutabstinit
!PRINT*,'iabstinit1=',iabstinit1

  IF ( modelopt == 1 ) THEN

    CALL perturb(nx,ny,nz,nscalar,nscalarq,nensmbl,ue ,ve ,we ,pprte ,  &
                  ptprte ,qve ,qscalare, 0 )

    CALL perturb(nx,ny,nz,nscalar,nscalarq,nensmbl,ua ,va ,wa ,pprta ,  &
                  ptprta ,qva ,qscalara, 0 )

  ELSE IF ( modelopt == 2 ) THEN

    CALL perturb(nx,ny,nz,nscalar,nscalarq,nensmbl,ue ,ve ,we ,phe ,    &
                  ptprte ,qve ,qscalare, 0 )

    IF (relaxopt == 1) THEN
      CALL perturb(nx,ny,nz,nscalar,nscalarq,nensmbl,ua ,va ,wa ,pha ,    &
                    ptprta ,qva ,qscalara, 0 )
    ELSE IF (relaxopt >= 2) THEN
      CALL perturb(nx,ny,nz,nscalar,nscalarq,nensmbl,ua ,va ,wa ,pha ,    &
                    ptprta ,qva ,qscalara, 1 )
    ELSE
      CALL arpsstop('Invalid relaxation option',1)
    END IF

  END IF

!IF(idealCase_opt==1) THEN  !statistical score only for control member

   IF ( modelopt == 1 ) THEN
     CALL  ensSpread(nx,ny,nz,nscalar,nscalarq,nensmbl,rf_ob,           &
                ue,ve,we,ptprte,pprte,qve,qscalare ,                    &
                sprd_u1,sprd_v1,sprd_w1,sprd_t1,sprd_p1,sprd_qv1,       &
                sprd_qs1 )
   ELSE IF ( modelopt == 2 ) THEN
     CALL  ensSpread(nx,ny,nz,nscalar,nscalarq,nensmbl,rf_ob,           &
                ue,ve,we,ptprte,phe,qve,qscalare ,                      &
                sprd_u1,sprd_v1,sprd_w1,sprd_t1,sprd_p1,sprd_qv1,       &
                sprd_qs1 )
   END IF

   IF (myproc==0) THEN
     open( 21, file='./gnuplot/sprd_u_h',  position='APPEND' )
     open( 22, file='./gnuplot/sprd_v_h',  position='APPEND' )
     open( 23, file='./gnuplot/sprd_w_h',  position='APPEND' )
     open( 24, file='./gnuplot/sprd_pt_h', position='APPEND' )
     open( 25, file='./gnuplot/sprd_pp_h', position='APPEND' )
     open( 26, file='./gnuplot/sprd_qv_h', position='APPEND' )
     open( 30, file='./gnuplot/sprd_qs_h', position='APPEND' )

     write(21, *) 20+5*(iter-1), sprd_u1
     write(22, *) 20+5*(iter-1), sprd_v1
     write(23, *) 20+5*(iter-1), sprd_w1
     write(24, *) 20+5*(iter-1), sprd_t1
     write(25, *) 20+5*(iter-1), sprd_p1
     write(26, *) 20+5*(iter-1), sprd_qv1
     write(30, *) 20+5*(iter-1), sprd_qs1
   END IF

   IF ( modelopt == 1 ) THEN
     CALL  ensSpread(nx,ny,nz,nscalar,nscalarq,nensmbl,rf_ob,           &
                ua,va,wa,ptprta,pprta,qva,qscalara ,                    &
                sprd_u1,sprd_v1,sprd_w1,sprd_t1,sprd_p1,sprd_qv1,       &
                sprd_qs1 )
   ELSE IF ( modelopt == 2 ) THEN
     CALL  ensSpread(nx,ny,nz,nscalar,nscalarq,nensmbl,rf_ob,           &
                ua,va,wa,ptprta,pha,qva,qscalara ,                      &
                sprd_u1,sprd_v1,sprd_w1,sprd_t1,sprd_p1,sprd_qv1,       &
                sprd_qs1 )
   END IF

   IF (myproc==0) THEN
     write(21, *) 20+5*(iter-1), sprd_u1
     write(22, *) 20+5*(iter-1), sprd_v1
     write(23, *) 20+5*(iter-1), sprd_w1
     write(24, *) 20+5*(iter-1), sprd_t1
     write(25, *) 20+5*(iter-1), sprd_p1
     write(26, *) 20+5*(iter-1), sprd_qv1
     write(30, *) 20+5*(iter-1), sprd_qs1(:)
   END IF

!END IF

  DO isub=1,nensmbl

!   curtim = outtime(l)

!   IF (myproc == 0) WRITE(6,'(/a,/2(a,i2),a,i4,a,/3(a,i2),a,f13.3,a/)')&
!       'In output file, the reference time is',                        &
!       'month=',month,',   day=',   day,',   year=',year,',',          &
!       'hour =',hour ,',minute=',minute,', second=',second,            &
!       ', & the time relative to this reference =',curtim

!   IF ( curtim > MAX(times(1),times(2)) .OR.                           &
!        curtim < MIN(times(1),times(2))) THEN
!     WRITE (*,*) "WARNING: Performing extrapolation.  Desired time ",  &
!                 "is outside the range of the reference files."
!   END IF

!   IF( times(2) == times(1)) THEN
!     WRITE (*,*) "ERROR: times in reference files are the same, ",     &
!                 "can't perform interpolation."
!     CALL arpsstop('No interpolation',1)
!   END IF

!   IF (myproc == 0) WRITE (6,'(/,1x,2(a,F12.7))')                      &
!        'Relative weights: file 1 - ',alpha,', file 2 - ',beta

!-----------------------------------------------------------------------
!
!  Calculate total fields from that for base state and perturbations
!
!-----------------------------------------------------------------------
!
! IF (modelopt==1) THEN

    IF (relaxopt==2 .and. modelopt==2) THEN
      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            ua    (i,j,k,isub)=umean (i,j,k)+                             &
                      alpha*ua(i,j,k,isub)+beta*ue(i,j,k,isub)

            va    (i,j,k,isub)=vmean (i,j,k)+                             &
                      alpha*va(i,j,k,isub)+beta*ve(i,j,k,isub)

            wa    (i,j,k,isub)=wmean (i,j,k)+                             &
                      alpha*wa(i,j,k,isub)+beta*we(i,j,k,isub)

            ptprta(i,j,k,isub)=ptmean(i,j,k)+                             &
                      alpha*ptprta(i,j,k,isub)+beta*ptprte(i,j,k,isub)

            pha   (i,j,k,isub)=phmean(i,j,k)+                             &
                      alpha*pha(i,j,k,isub)+beta*phe(i,j,k,isub)

            qva   (i,j,k,isub)=qvmean(i,j,k)+                             &
                      alpha*qva  (i,j,k,isub)+beta*qve  (i,j,k,isub)

            DO nq=1,nscalarq
              qscalara(i,j,k,nq,isub)=qscalarmean(i,j,k,nq)+              &
                      alpha*qscalara(i,j,k,nq,isub)+beta*qscalare(i,j,k,nq,isub)
              IF (qscalara(i,j,k,nq,isub)<0) qscalara(i,j,k,nq,isub)=0.0
              IF (qscalara(i,j,k,nq,isub)>1) qscalara(i,j,k,nq,isub)=1.0
            END DO

          END DO
        END DO
      END DO
    ELSE IF (relaxopt==3 .and. modelopt==2) THEN
      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            inf_temp=inf_rate
            IF (rf_ob(i,j,k)>10) inf_temp=inf_rate**2
            ua    (i,j,k,isub)=umean (i,j,k) + inf_temp * ua    (i,j,k,isub)

            va    (i,j,k,isub)=vmean (i,j,k) + inf_temp * va    (i,j,k,isub)

            wa    (i,j,k,isub)=wmean (i,j,k) + inf_temp * wa    (i,j,k,isub)

            ptprta(i,j,k,isub)=ptmean(i,j,k) + inf_temp * ptprta(i,j,k,isub)

            pha   (i,j,k,isub)=phmean(i,j,k) + inf_temp * pha   (i,j,k,isub)

            qva   (i,j,k,isub)=qvmean(i,j,k) + inf_temp * qva   (i,j,k,isub)

            DO nq=1,nscalarq
              qscalara(i,j,k,nq,isub)=qscalarmean(i,j,k,nq) +             &
                                      inf_temp * qscalara(i,j,k,nq,isub)
              IF (qscalara(i,j,k,nq,isub)<0) qscalara(i,j,k,nq,isub)=0.0
              IF (qscalara(i,j,k,nq,isub)>1) qscalara(i,j,k,nq,isub)=1.0
            END DO

          END DO
        END DO
      END DO
    ELSE
      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            ua    (i,j,k,isub)=u(i,j,k)+                                  &
                      alpha*ua(i,j,k,isub)+beta*ue(i,j,k,isub)

            va    (i,j,k,isub)=v(i,j,k)+                                  &
                      alpha*va(i,j,k,isub)+beta*ve(i,j,k,isub)

            wa    (i,j,k,isub)=w(i,j,k)+                                  &
                      alpha*wa(i,j,k,isub)+beta*we(i,j,k,isub)

            ptprta(i,j,k,isub)=ptprt(i,j,k)+                              &
                      alpha*ptprta(i,j,k,isub)+beta*ptprte(i,j,k,isub)

            IF (modelopt==1) THEN
              pprta (i,j,k,isub)=pprt(i,j,k)+                             &
                        alpha*pprta(i,j,k,isub)+beta*pprte(i,j,k,isub)
            ELSE IF (modelopt==2) THEN
              pha (i,j,k,isub)=ph(i,j,k)+                                 &
                        alpha*pha(i,j,k,isub)+beta*phe(i,j,k,isub)
            END IF

            qva   (i,j,k,isub)=qv  (i,j,k)+                               &
                      alpha*qva  (i,j,k,isub)+beta*qve  (i,j,k,isub)
            IF (qva(i,j,k,isub)<0) qva(i,j,k,isub)=0.0

            DO nq=1,nscalarq
              qscalara(i,j,k,nq,isub)=qscalar(i,j,k,nq)+                  &
                      alpha*qscalara(i,j,k,nq,isub)+beta*qscalare(i,j,k,nq,isub)
              IF (qscalara(i,j,k,nq,isub)<0) qscalara(i,j,k,nq,isub)=0.0
              IF (qscalara(i,j,k,nq,isub)>1) qscalara(i,j,k,nq,isub)=1.0
            END DO

            IF (modelopt==1) THEN
              tkea   (i,j,k,isub)=tke    (i,j,k)+                         &
                        alpha*tkea  (i,j,k,isub)+beta*tkee  (i,j,k,isub)

              radfrca(i,j,k,isub)=radfrc(i,j,k)+                          &
                        alpha*radfrca(i,j,k,isub)+beta*radfrce(i,j,k,isub)
            END IF

          END DO
        END DO
      END DO
    END IF

    IF(modelopt==1) THEN
      IF(1==1) THEN
        DO is=0,nstyp
          DO k=1,nzsoil
            DO j=1,ny-1
              DO i=1,nx-1
                tsoila(i,j,k,is,isub)=tsoil(i,j,k,is)+                            &
                            alpha*tsoila(i,j,k,is,isub)+beta*tsoile(i,j,k,is,isub)
                qsoila(i,j,k,is,isub)=qsoil(i,j,k,is)+                            &
                            alpha*qsoila(i,j,k,is,isub)+beta*qsoile(i,j,k,is,isub)
              END DO
            END DO
          END DO
          DO j=1,ny-1
            DO i=1,nx-1
              wetcanpa(i,j,is,isub)=wetcanp(i,j,is)+                            &
                          alpha*wetcanpa(i,j,is,isub)+beta*wetcanpe(i,j,is,isub)
            END DO
          END DO
        END DO

        DO j=1,ny-1
          DO i=1,nx-1
            snowdptha(i,j,isub)=snowdpth(i,j)+                                &
                         alpha*snowdptha(i,j,isub)+beta*snowdpthe(i,j,isub)
            rainga (i,j,isub)=raing (i,j)+                                    &
                       alpha*rainga (i,j,isub)+beta*rainge  (i,j,isub)
            rainca (i,j,isub)=rainc (i,j)+                                    &
                         alpha*rainca (i,j,isub)+beta*raince  (i,j,isub)
            prcratea(i,j,1,isub)=prcrate(i,j,1)+                              &
                          alpha*prcratea(i,j,1,isub)+beta*prcratee(i,j,1,isub)
            prcratea(i,j,2,isub)=prcrate(i,j,2)+                              &
                          alpha*prcratea(i,j,2,isub)+beta*prcratee(i,j,2,isub)
            prcratea(i,j,3,isub)=prcrate(i,j,3)+                              &
                          alpha*prcratea(i,j,3,isub)+beta*prcratee(i,j,3,isub)
            prcratea(i,j,4,isub)=prcrate(i,j,4)+                              &
                         alpha*prcratea(i,j,4,isub)+beta*prcratee(i,j,4,isub)

            radswa (i,j,isub)=radsw (i,j)+                                    &
                       alpha*radswa  (i,j,isub)+beta*radswe  (i,j,isub)
            rnflxa (i,j,isub)=rnflx (i,j)+                                    &
                         alpha*rnflxa  (i,j,isub)+beta*rnflxe  (i,j,isub)
            radswneta(i,j,isub)=radswnet(i,j)+                                &
                         alpha*radswneta(i,j,isub)+beta*radswnete(i,j,isub)
            radlwina(i,j,isub)=radlwin(i,j)+                                  &
                         alpha*radlwina(i,j,isub)+beta*radlwine(i,j,isub)
            usflxa (i,j,isub)=usflx (i,j)+                                    &
                       alpha*usflxa  (i,j,isub)+beta*usflxe  (i,j,isub)
            vsflxa (i,j,isub)=vsflx (i,j)+                                    &
                       alpha*vsflxa  (i,j,isub)+beta*vsflxe  (i,j,isub)
            ptsflxa(i,j,isub)=ptsflx(i,j)+                                    &
                       alpha*ptsflxa (i,j,isub)+beta*ptsflxe (i,j,isub)
            qvsflxa(i,j,isub)=qvsflx(i,j)+                                    &
                       alpha*qvsflxa (i,j,isub)+beta*qvsflxe (i,j,isub)
          END DO
        END DO
      ELSE
        DO is=0,nstyp
          DO k=1,nzsoil
            DO j=1,ny-1
              DO i=1,nx-1
             tsoila(i,j,k,is,isub)=tsoil(i,j,k,is)
             qsoila(i,j,k,is,isub)=qsoil(i,j,k,is)
              END DO
            END DO
          END DO
          DO j=1,ny-1
            DO i=1,nx-1
             wetcanpa(i,j,is,isub)=wetcanp(i,j,is)
            END DO
          END DO
        END DO

        DO j=1,ny-1
          DO i=1,nx-1
            snowdptha(i,j,isub)=snowdpth(i,j)
            rainga (i,j,isub)=raing (i,j)
            rainca (i,j,isub)=rainc (i,j)
            prcratea(i,j,1,isub)=prcrate(i,j,1)
            prcratea(i,j,2,isub)=prcrate(i,j,2)
            prcratea(i,j,3,isub)=prcrate(i,j,3)
            prcratea(i,j,4,isub)=prcrate(i,j,4)

            radswa (i,j,isub)=radsw (i,j)
            rnflxa (i,j,isub)=rnflx (i,j)
            radswneta(i,j,isub)=radswnet(i,j)
            radlwina(i,j,isub)=radlwin(i,j)
            usflxa (i,j,isub)=usflx (i,j)
            vsflxa (i,j,isub)=vsflx (i,j)
            ptsflxa(i,j,isub)=ptsflx(i,j)
            qvsflxa(i,j,isub)=qvsflx(i,j)
          END DO
        END DO
      END IF
    END IF
!
!-----------------------------------------------------------------------
!
!  Print out the max/min of output variables.
!
!-----------------------------------------------------------------------
!
    IF (myproc == 0) WRITE(6,'(/1x,a,i2.2,a/)')                                &
        'Min. and max. of data for member ',isub,' after relaxing process are:'

    IF (modelopt==1) THEN
      CALL a3dmax0(x,1,nx,1,nx,1,1,1,1,1,1,1,1, amax,amin)
      IF (myproc == 0) WRITE(6,'(/1x,2(a,e13.6))') 'xmin    = ', amin,',  xmax    =',amax

      CALL a3dmax0(y,1,ny,1,ny,1,1,1,1,1,1,1,1, amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'ymin    = ', amin,',  ymax    =',amax

      CALL a3dmax0(z,1,nz,1,nz,1,1,1,1,1,1,1,1, amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'zmin    = ', amin,',  zmax    =',amax

      CALL a3dmax0(zp,1,nx,1,nx-1,1,ny,1,ny-1,1,nz,1,nz,                  &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'zpmin   = ', amin,', zpmax    =',amax

      CALL a3dmax0(ubar,1,nx,1,nx,1,ny,1,ny-1,1,nz,1,nz-1,                &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'ubarmin = ', amin,',  ubarmax =',amax

      CALL a3dmax0(vbar,1,nx,1,nx-1,1,ny,1,ny,1,nz,1,nz-1,                &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'vbarmin = ', amin,',  vbarmax =',amax

      CALL a3dmax0(ptbar,1,nx,1,nx-1,1,ny,1,ny-1,1,nz,1,nz-1,             &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'ptbarmin= ', amin,',  ptbarmax=',amax

      CALL a3dmax0(pbar,1,nx,1,nx-1,1,ny,1,ny-1,1,nz,1,nz-1,              &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'pbarmin = ', amin,',  pbarmax =',amax

      CALL a3dmax0(rhobar,1,nx,1,nx-1,1,ny,1,ny-1,1,nz,1,nz-1,            &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'rhobarmin=', amin,', rhobarmax=',amax

      CALL a3dmax0(qvbar,1,nx,1,nx-1,1,ny,1,ny-1,1,nz,1,nz-1,             &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'qvbarmin= ', amin,',  qvbarmax=',amax

      CALL a3dmax0(ua  (1,1,1,isub),1,nx,1,nx,1,ny,1,ny-1,1,nz,1,nz-1,    &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'uamin = ', amin,',  uamax =',amax

      CALL a3dmax0(va  (1,1,1,isub),1,nx,1,nx-1,1,ny,1,ny,1,nz,1,nz-1,    &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'vamin = ', amin,',  vamax =',amax

      CALL a3dmax0(wa(1,1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,nz,1,nz,       &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'wamin = ', amin,',  wamax =',amax

      CALL a3dmax0(ptprta(1,1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,nz,1,nz-1, &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'ptprtmin= ', amin,',  ptprtmax=',amax

      CALL a3dmax0(pprta(1,1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,nz,1,nz-1,  &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'pprtmin = ', amin,',  pprtmax =',amax

      CALL a3dmax0(qva  (1,1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,nz,1,nz-1, &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'qvamin= ', amin,',  qvamax=',amax

      DO nq = 1,nscalar

        CALL a3dmax0(qscalara(:,:,:,nq,isub),1,nx,1,nx-1,1,ny,1,ny-1,         &
                     1,nz,1,nz-1,amax,amin)

        IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') TRIM(qnames(nq))//'min   = ', amin,    &
                              ',  '//TRIM(qnames(nq))//'max   =',  amax

      END DO

      CALL a3dmax0(tkea(1,1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,nz,1,nz-1,   &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'tkemin  = ', amin,',  tkemax  =',amax

      CALL a3dmax0(kmh(1,1,1     ),1,nx,1,nx-1,1,ny,1,ny-1,1,nz,1,nz-1,   &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'kmhmin  = ', amin,',  kmhmax  =',amax

      CALL a3dmax0(kmv(1,1,1     ),1,nx,1,nx-1,1,ny,1,ny-1,1,nz,1,nz-1,   &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'kmvmin  = ', amin,',  kmvmax  =',amax

      CALL a3dmax0(rainga(1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'raingmin= ', amin,',  raingmax=',amax

      CALL a3dmax0(rainca(1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'raincmin= ', amin,',  raincmax=',amax

      CALL a3dmax0(prcratea(1,1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,           &
                   1,1,1,1,amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'prcr1min= ', amin,',  prcr1max=',amax

      CALL a3dmax0(prcratea(1,1,2,isub),1,nx,1,nx-1,1,ny,1,ny-1,           &
                   1,1,1,1,amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'prcr2min= ', amin,',  prcr2max=',amax

      CALL a3dmax0(prcratea(1,1,3,isub),1,nx,1,nx-1,1,ny,1,ny-1,           &
                   1,1,1,1,amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'prcr3min= ', amin,',  prcr3max=',amax

      CALL a3dmax0(prcratea(1,1,4,isub),1,nx,1,nx-1,1,ny,1,ny-1,           &
                   1,1,1,1,amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'prcr4min= ', amin,',  prcr4max=',amax

      DO iss = 0, nstyp

        CALL a3dmax0(tsoila(1,1,1,iss,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1, &
                     amax,amin)
        IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6),a,i3)')                    &
            'tsoil_sfcmin = ', amin,',  tsoil_sfcmax =',amax,' for soil type=',iss

        CALL a3dmax0(tsoila(1,1,2,iss,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1, &
                     amax,amin)
        IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6),a,i3)')                    &
            'tsoil_dpmin= ', amin,',  tsoil_dpmax=',amax,' for soil type=',iss

        CALL a3dmax0(qsoila(1,1,1,iss,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1, &
                     amax,amin)
        IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6),a,i3)')                    &
            'qsoil_sfcmin = ', amin,',  qsoil_sfcmax =',amax,' for soil type=',iss

        CALL a3dmax0(qsoila(1,1,2,iss,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1, &
                     amax,amin)
        IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6),a,i3)')                    &
            'qsoil_dpmin = ', amin,',  qsoil_dpmax =',amax,' for soil type=',iss

        CALL a3dmax0(wetcanpa(1,1,iss,isub),                                 &
                     1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,amax,amin)
        IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6),a,i3)')                    &
            'wetcmin = ', amin,',  wetcmax =',amax,' for soil type=',iss

      END DO

      CALL a3dmax0(roufns,1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,                  &
                   amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'roufnmin =', amin,', roufnmax =',amax

      CALL a3dmax0(veg,1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,                     &
                   amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'vegmin  = ', amin,',   vegmax =',amax

      CALL a3dmax0(radfrc,1,nx,1,nx-1,1,ny,1,ny-1,1,nz,1,nz-1,              &
                   amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'radfnmin =', amin,', radfnmax =',amax

      CALL a3dmax0(radswa(1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,         &
                   amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'radswmin =', amin,', radswmax =',amax

      CALL a3dmax0(rnflxa(1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,         &
                   amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'rnflxmin =', amin,', rnflxmax =',amax

      CALL a3dmax0(radswneta(1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,      &
                   amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'radswnetmin =', amin,', radswnetmax =',amax

      CALL a3dmax0(radlwina(1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,       &
                   amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'radlwinmin =', amin,', radlwinmax =',amax

      CALL a3dmax0(usflxa(1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,         &
                   amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'usflxnmin =', amin,', usflxmax =',amax

      CALL a3dmax0(vsflxa(1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,         &
                   amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'vsflxmin =', amin,', vsflxmax =',amax

      CALL a3dmax0(ptsflxa(1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,        &
                   amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'ptflxmin =', amin,', ptflxmax =',amax

      CALL a3dmax0(qvsflxa(1,1,isub),1,nx,1,nx-1,1,ny,1,ny-1,1,1,1,1,        &
                   amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'qvflxmin =', amin,', qvflxmax =',amax

      DO k=1,nz
        DO j=1,ny
          DO i=1,nx
            tem1 (i,j,k)= 0.0        ! To be put in place of wbar
          END DO
        END DO
      END DO

    ELSE IF (modelopt==2) THEN

      CALL a3dmax0(pha(1,1,1,isub),1,nx-3,1,nx-3,1,ny-3,1,ny-3,1,nz-3,1,nz-3,     &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'phmin = ', amin,',  phmax =',amax

      CALL a3dmax0(rhobara(1,1,1,isub),1,nx-3,1,nx-3,1,ny-3,1,ny-3,1,nz-3,1,nz-3, &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'rhobarmin=', amin,',  rhobarmax =',amax

      CALL a3dmax0(ua  (1,1,1,isub),1,nx-2,1,nx-2,1,ny-3,1,ny-3,1,nz-3,1,nz-3,    &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'uamin = ', amin,',  uamax =',amax

      CALL a3dmax0(va  (1,1,1,isub),1,nx-3,1,nx-3,1,ny-2,1,ny-2,1,nz-3,1,nz-3,    &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'vamin = ', amin,',  vamax =',amax

      CALL a3dmax0(wa(1,1,1,isub),1,nx-3,1,nx-3,1,ny-3,1,ny-3,1,nz-2,1,nz-2,      &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'wamin = ', amin,',  wamax =',amax

      CALL a3dmax0(ptprta(1,1,1,isub),1,nx-3,1,nx-3,1,ny-3,1,ny-3,1,nz-3,1,nz-3,  &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'ptprtmin= ', amin,', ptprtmax =',amax

      CALL a3dmax0(qva  (1,1,1,isub),1,nx-3,1,nx-3,1,ny-3,1,ny-3,1,nz-3,1,nz-3,   &
                  amax,amin)
      IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') 'qvamin= ', amin,', qvamax =',amax

      DO nq = 1,nscalar

        CALL a3dmax0(qscalara(:,:,:,nq,isub),1,nx-3,1,nx-3,1,ny-3,1,ny-3,         &
                     1,nz-3,1,nz-3,amax,amin)

        IF (myproc == 0) WRITE(6,'(1x,2(a,e13.6))') TRIM(qnames(nq))//'min   = ', amin,    &
                              ',  '//TRIM(qnames(nq))//'max   =',  amax

      END DO

    END IF

!
!-----------------------------------------------------------------------
!
!  Data dump of the model grid and base state arrays:
!
!  First find a unique name basdmpfn(1:lbasdmpf) for the grid and
!  base state array dump file
!
!  If grid/base state data has been written out once, skip
!  the following writing block. Also no need to write out
!  separate data for Savi3D dump. The same for GrADS dump.
!
!-----------------------------------------------------------------------
!
    IF (modelopt==2) THEN
      IF (nocmnt==0) nocmnt=2
    END IF
    CALL mpupdatei(nocmnt,1)
    IF (mp_opt > 0 .AND. joindmp(FINDX_H) > 0) THEN
      WRITE (cmnt(nocmnt),'(a,i4,a,i4,a,i4)')                           &
          ' nx =',(nx-3)*nproc_x+3,', ny =',(ny-3)*nproc_y+3,', nz =',nz
    ELSE
      WRITE (cmnt(nocmnt),'(a,i4,a,i4,a,i4)')                           &
          ' nx =',nx,', ny =',ny,', nz =',nz
    END IF

    IF (modelopt==1) THEN

!     WRITE(runname,'(a,I3.3,a)') 'enr',isub,'.hdf'//trim(emname)
      WRITE(runname,'(a,I3.3,a)') 'enr',isub

      houtfmt = hdmpfmt
      grbpkbit = 16

      CALL gtlfnkey(runname, lfnkey)

      IF(houtfmt /= 9 ) THEN

!       IF( gboutcnt == 1 ) GO TO 500 ! If done already, skip this part.

        CALL gtbasfn(runname(1:lfnkey),dirname,ldirnam,hdmpfmt,              &
                     1,0,basdmpfn,lbasdmpf)

        IF (myproc == 0) PRINT*,'Output grid/base state file is ', basdmpfn(1:lbasdmpf)

        grdbas = 1      ! Dump out grd and base state arrays only

        CALL dtadump(nx,ny,nz,nzsoil,nstyps,hdmpfmt,nchout,                    &
                     basdmpfn(1:lbasdmpf),grdbas,filcmprs,                     &
                     ua(1,1,1,isub),va(1,1,1,isub),                            &
                     wa(1,1,1,isub),ptprta(1,1,1,isub),                        &
                     pprta(1,1,1,isub),qva(1,1,1,isub),                        &
                     qscalara(1,1,1,1,isub),                                   &
                     tkea(1,1,1,isub),kmh (1,1,1     ),kmv (1,1,1     ),       &
!                    tkea(1,1,1,isub),kmha(1,1,1,isub),kmva(1,1,1,isub),       &
                     ubar,vbar,tem1,ptbar,pbar,rhobar,qvbar,                   &
                     x,y,z,zp,zpsoil,                                          &
                     soiltyp,stypfrct,vegtyp,lai,roufns,veg,                   &
                     tsoila(1,1,1,0,isub),qsoila(1,1,1,0,isub),                &
                     wetcanpa(1,1,0,isub),snowdptha(1,1,isub),                 &
                     rainga(1,1,isub),rainca(1,1,isub),prcratea(1,1,1,isub),   &
                     radfrca(1,1,1,isub),radswa(1,1,isub),rnflxa(1,1,isub),    &
                     radswneta(1,1,isub),radlwina(1,1,isub),                   &
                     usflxa(1,1,isub),vsflxa(1,1,isub),                        &
                     ptsflxa(1,1,isub),qvsflxa(1,1,isub),                      &
                     tem2,tem3,tem4)

        gboutcnt = 1

        500     CONTINUE

      END IF
!
!-----------------------------------------------------------------------
!
!  Then the time dependent fields:
!
!-----------------------------------------------------------------------
!
      IF( .NOT. (houtfmt == 9 .AND. vroutcnt == 1) ) THEN
!
!-----------------------------------------------------------------------
!
!  Reconstruct the file name using the specified directory name
!
!-----------------------------------------------------------------------
!
        CALL gtdmpfn(runname(1:lfnkey),dirname,                         &
                   ldirnam,curtim,hdmpfmt,1,0, hdmpfn, ldmpf)

      END IF

      IF (myproc == 0) WRITE(6,'(a,a)') 'Writing ensemble    variable history dump ', &
                                      hdmpfn(1:ldmpf)
      grdbas = 0

      CALL dtadump(nx,ny,nz,nzsoil,nstyps,hdmpfmt,nchout,                    &
                 hdmpfn(1:ldmpf),grdbas,filcmprs,                            &
                 ua(1,1,1,isub),va(1,1,1,isub),                              &
                 wa(1,1,1,isub),ptprta(1,1,1,isub),                          &
                 pprta(1,1,1,isub),qva(1,1,1,isub),                          &
                 qscalara(1,1,1,1,isub),                                     &
                 tkea(1,1,1,isub),kmh(1,1,1     ),kmv(1,1,1     ),           &
!                tkea(1,1,1,isub),kmha(1,1,1,isub),kmva(1,1,1,isub),         &
                 ubar,vbar,tem1,ptbar,pbar,rhobar,qvbar,                     &
                 x,y,z,zp,zpsoil,                                            &
                 soiltyp,stypfrct,vegtyp,lai,roufns,veg,                     &
                 tsoila(1,1,1,0,isub),qsoila(1,1,1,0,isub),                  &
                 wetcanpa(1,1,0,isub),snowdptha(1,1,isub),                   &
                 rainga(1,1,isub),rainca(1,1,isub),prcratea(1,1,1,isub),     &
                 radfrca(1,1,1,isub),radswa(1,1,isub),rnflxa(1,1,isub),      &
                 radswneta(1,1,isub),radlwina(1,1,isub),                     &
                 usflxa(1,1,isub),vsflxa(1,1,isub),                          &
                 ptsflxa(1,1,isub),qvsflxa(1,1,isub),                        &
                 tem2,tem3,tem4)

!
!-----------------------------------------------------------------------
!
!  Write out soil model variable file
!
!-----------------------------------------------------------------------
!
!   IF ( sfcin == 1 ) THEN

!     CALL cvttsnd( curtim, timsnd, tmstrln )

!     soiloutfl = runname(1:lfnkey)//".soilvar."//timsnd(1:tmstrln)
!     lfn = lfnkey + 9 + tmstrln

!     IF( dirname /= ' ' ) THEN
!       temchar = soiloutfl
!       soiloutfl = dirname(1:ldirnam)//'/'//temchar
!       lfn  = lfn + ldirnam + 1
!     END IF

!     !CALL fnversn(soiloutfl, lfn)

!     IF (soildmp > 0) THEN
!       !IF (mp_opt > 0 .AND. joindmp == 0) THEN
!       !  CALL gtsplitfn(soiloutfl,1,1,loc_x,loc_y,1,1,                 &
!       !                 0,0,0,lvldbg,filename,ireturn)
!       !  lfn = LEN_TRIM(filename)
!       !  soiloutfl(1:lfn) = TRIM(filename)
!       !END IF

!       !IF (myproc == 0) PRINT *, 'Writing soil data to ',soiloutfl(1:lfn)

!       IF(mp_opt > 0 .AND. joindmp(FINDX_S) > 0) THEN
!         CALL wrtjoinsoil(nx,ny,nzsoil,nstyps, soiloutfl(1:lfn),       &
!                  dx,dy,zpsoil,                                        &
!                  mapproj,trulat1,trulat2,trulon,sclfct,ctrlat,ctrlon, &
!                  1,1,1,1,1,                                           &
!                  tsoila(1,1,1,0,isub),qsoila(1,1,1,0,isub),             &
!                  wetcanpa(1,1,0,isub),snowdptha(1,1,isub),soiltyp)
!       ELSE
!         CALL wrtsoil(nx,ny,nzsoil,nstyps,soiloutfl(1:lfn),            &
!                  dx,dy,zpsoil,                                        &
!                  mapproj,trulat1,trulat2,trulon,sclfct,ctrlat,ctrlon, &
!                  1,1,1,1,1,                                           &
!                  tsoila(1,1,1,0,isub),qsoila(1,1,1,0,isub),             &
!                  wetcanpa(1,1,0,isub),snowdptha(1,1,isub),soiltyp)
!       END IF

!       IF (soildmp == 1 .AND. (  mp_opt == 0 .OR.                      &
!                                (myproc == 0 .AND. joindmp(FINDX_S) > 0) ) )    &
!         CALL soilcntl(nx,ny,nzsoil,zpsoil,soiloutfl(1:lfn),           &
!                 1,1,1,1,1,x,y)
!     END IF

!   END IF       ! sfcin.eq.1

!-----------------------------------------------------------------------
!
!  Write out surface property data file: sfcoutfl .
!
!-----------------------------------------------------------------------
!
!   IF ( landin == 1 ) THEN

!     sfcoutfl = runname(1:lfnkey)//".sfcdata"
!     lfn = lfnkey + 8

!     IF( dirname /= ' ' ) THEN

!       temchar = sfcoutfl
!       sfcoutfl = dirname(1:ldirnam)//'/'//temchar
!       lfn  = lfn + ldirnam + 1

!     END IF

!     CALL fnversn(sfcoutfl, lfn)

!     IF (sfcdmp > 0) THEN

!       IF (mp_opt > 0 .AND. joindmp(FINDX_T) == 0) THEN
!         CALL gtsplitfn(sfcoutfl,1,1,loc_x,loc_y,1,1,                  &
!                        0,0,0,lvldbg,filename,ireturn)
!         lfn = LEN_TRIM(filename)
!         sfcoutfl(1:lfn) = TRIM(filename)
!       END IF

!       IF (myproc == 0) PRINT *, 'Write surface property data in ',sfcoutfl(1:lfn)

!       IF(mp_opt > 0 .AND. joindmp(FINDX_T) > 0) THEN
!         CALL wrtjoinsfcdt(nx,ny,nstyps,sfcoutfl(1:lfn), dx,dy,        &
!               mapproj,trulat1,trulat2,trulon,sclfct,ctrlat,ctrlon,    &
!               1,1,1,1,1,0,                                            &
!               soiltyp,stypfrct,vegtyp,lai,roufns,veg,veg)
!       ELSE
!         CALL wrtsfcdt(nx,ny,nstyps,sfcoutfl(1:lfn), dx,dy,            &
!               mapproj,trulat1,trulat2,trulon,sclfct,ctrlat,ctrlon,    &
!               1,1,1,1,1,0,                                            &
!               soiltyp,stypfrct,vegtyp,lai,roufns,veg,veg)
!       END IF

!       IF (sfcdmp == 1 .AND. ( (myproc == 0 .AND. joindmp(FINDX_T) > 0) .OR. (mp_opt == 0) ) ) &
!         CALL sfccntl(nx,ny, sfcoutfl(1:lfn),1,1,1,1,1,0, x,y, tem1,tem2)

!     END IF

!   END IF       ! landin.eq.1


    ELSE IF (modelopt == 2) THEN

      CALL get_input_dirname(dirname,l,tmpfname)

!     IF (wrfoutopt == 0) THEN
!       filename  = 'wrfout_sp'
!     ELSE IF (wrfoutopt == 1) THEN
!       filename  = 'wrfinput_da'
!     ELSE
      WRITE(filename_a,'(2a,I4.4,5(a,I2.2),a,I0)') TRIM(rdinname_a),'wrfout_d01_',  &
                     year,'-',month,'-',day,'_',hour,':',minute,':',second,'_',isub
      WRITE(filename,'(a,I2.2,a,I4.4,5(a,I2.2))') 'wrfout_sp',isub,'_',   &
           year,'-',month,'-',day,'_',hour,':',minute,':',second
!     END IF

!   IF (myproc == 0) WRITE(6,'(1x,a,a)')                              &
!      'EnKF analysis data dump in file ',TRIM(filename)

!     CALL dtadump4wrf(wrfoutopt, tmpfname, nx,ny,nz,nzsoil,nstyps,     &
!          tmpdir,filename,numdigits, x,y,z,zp,zpsoil,                  &
!          ua(:,:,:,isub),va(:,:,:,isub),wa(:,:,:,isub),                &
!          ptprta(:,:,:,isub),pprta(:,:,:,isub),                        &
!          qva(:,:,:,isub), qscalara(:,:,:,:,isub),                     &
!          tkea(:,:,:,isub), kmh,kmv,                                   &
!          ubar, vbar, wbar, ptbar, pbar, rhobar, qvbar,                &
!          pha(:,:,:,isub), mua(:,:,isub), ireturn)

      CALL dumpwrfvar(filename_a,tmpfname,filename,nx,ny,nz,ua(:,:,:,isub),  &
                     va(:,:,:,isub),wa(:,:,:,isub),ptprta(:,:,:,isub),       &
                     pha(:,:,:,isub),qva(:,:,:,isub),qscalara(:,:,:,:,isub),istatus)

    END IF


  END DO       ! l=1,nensmbl


!IF(idealCase_opt==1) THEN  !statistical score only for control member

  IF (modelopt==1) THEN
    CALL perturb(nx,ny,nz,nscalar,nscalarq,nensmbl,                     &
               ua,va,wa,pprta,ptprta,qva,qscalara, 0 )

    CALL  ensSpread(nx,ny,nz,nscalar,nscalarq,nensmbl,rf_ob,            &
               ua,va,wa,ptprta,pprta,qva,qscalara,                      &
               sprd_u2,sprd_v2,sprd_w2,sprd_t2,sprd_p2,sprd_qv2,        &
               sprd_qs2 )
  ELSE IF (modelopt==2) THEN
    CALL perturb(nx,ny,nz,nscalar,nscalarq,nensmbl,                     &
               ua,va,wa,pha,ptprta,qva,qscalara, 0 )

    CALL  ensSpread(nx,ny,nz,nscalar,nscalarq,nensmbl,rf_ob,            &
               ua,va,wa,ptprta,pha,qva,qscalara,                        &
               sprd_u2,sprd_v2,sprd_w2,sprd_t2,sprd_p2,sprd_qv2,        &
               sprd_qs2 )
  END IF

  IF (myproc==0) THEN
    write(21, *) 20+5*(iter-1), sprd_u2
    write(22, *) 20+5*(iter-1), sprd_v2
    write(23, *) 20+5*(iter-1), sprd_w2
    write(24, *) 20+5*(iter-1), sprd_t2
    write(25, *) 20+5*(iter-1), sprd_p2
    write(26, *) 20+5*(iter-1), sprd_qv2
    write(30, *) 20+5*(iter-1), sprd_qs2(:)
  END IF

!END IF



  IF (myproc == 0) WRITE(6,'(a)') ' ==== Normal successful completion of ENRELAX2PR. ===='
  CALL arpsstop(' ',0)

  100   WRITE(6,'(a)')                                                  &
          'Error reading NAMELIST file. Program ENRELAX2PR stopped.'
  CALL arpsstop(' ',9104)

  9001  CONTINUE

  WRITE(6,'(/2x,a)')'For the output grid:'
  WRITE(6,'(2x,a,f12.4)')                                               &
      'The latitude  of the output grid center, ctrlat=',ctrlat
  WRITE(6,'(2x,a,f12.4/)')                                              &
      'The longitude of the output grid center, ctrlon=',ctrlon
  WRITE(6,'(2x,a/2x,a,2f15.4,a)')                                       &
      'The SW corner (i,j)=(2,2) of the grid is located at ',           &
      '(',xgrdorg,ygrdorg,') of the input grid.'

  CALL arpsstop(' ',9001)

  9002  CONTINUE
  WRITE(6,'(1x,a,i2,/1x,a)')                                            &
      'Data read was unsuccessful. ireturn =', ireturn,                 &
      'Job stopped in ENRELAX2PR.'

  CALL arpsstop(' ',9002)
END PROGRAM enrelax2pr
