!
!#######################################################################
!
SUBROUTINE ensSpread(nx,ny,nz,nscalar,nscalarq,nen,  rf_ob,             &
                    uen,ven,wen,ptprten,pprten,                         &
                    qven,qscalaren,                                     &
                    sua,sva,swa,sptprta,spprta,sqva,                    &
                    sqsa )
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Compute the ensemble mean error and ensemble spread
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: nx,ny,nz,nen,nscalar,nscalarq
  REAL :: rf_ob(nx,ny,nz)
  REAL :: uen(nx,ny,nz,nen)
  REAL :: ven(nx,ny,nz,nen)
  REAL :: wen(nx,ny,nz,nen)
  REAL :: ptprten(nx,ny,nz,nen)
  REAL :: pprten(nx,ny,nz,nen)
  REAL :: qven(nx,ny,nz,nen)
  REAL :: qscalaren(nx,ny,nz,nscalar,nen)

  REAL, INTENT(OUT) :: sua,sva,swa,sptprta,spprta
  REAL, INTENT(OUT) :: sqva
  REAL, INTENT(OUT) :: sqsa(nscalarq)

!-----------------------------------------------------------------------

  REAL :: su(nx,ny,nz)
  REAL :: sv(nx,ny,nz)
  REAL :: sw(nx,ny,nz)
  REAL :: sptprt(nx,ny,nz)
  REAL :: spprt(nx,ny,nz)
  REAL :: sqv(nx,ny,nz)
  REAL :: sqs(nx,ny,nz,nscalar)

  INTEGER :: tmp
  INTEGER :: nq

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!

!-----------------------------------------------------------------------
! 1. compute the ensemble spread
!-----------------------------------------------------------------------
  CALL spread_sub(nx,ny,nz,nscalar,nen,                                 &
              uen,ven,wen,pprten,ptprten,qven,qscalaren,                &
              su,sv,sw,spprt,sptprt,sqv,sqs)

!-----------------------------------------------------------------------
! 2. compute the average ensemble spread
!-----------------------------------------------------------------------

  CALL aver(nx,ny,nz,su,rf_ob,sua,tmp)
  CALL aver(nx,ny,nz,sv,rf_ob,sva,tmp)
  CALL aver(nx,ny,nz,sw,rf_ob,swa,tmp)
  CALL aver(nx,ny,nz,spprt,rf_ob,spprta,tmp)
  CALL aver(nx,ny,nz,sptprt,rf_ob,sptprta,tmp)
  CALL aver(nx,ny,nz,sqv,rf_ob,sqva,tmp)
  DO nq = 1, nscalarq
    CALL aver(nx,ny,nz,sqs(:,:,:,nq),rf_ob,sqsa(nq),tmp)
  END DO

  RETURN
END SUBROUTINE ensSpread

!
!#######################################################################
!

SUBROUTINE spread_sub(nx,ny,nz,nscalar,nen,                             &
                  uen,ven,wen,pprten,ptprten,qven,qscalaren,            &
                  su,sv,sw,spprt,sptprt,sqv,sqs)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate ensemble spread
!
!-----------------------------------------------------------------------

  IMPLICIT NONE
!
  INTEGER :: nx,ny,nz,nen,nscalar
  REAL :: uen(nx,ny,nz,nen)
  REAL :: ven(nx,ny,nz,nen)
  REAL :: wen(nx,ny,nz,nen)
  REAL :: ptprten(nx,ny,nz,nen)
  REAL :: pprten(nx,ny,nz,nen)
  REAL :: qven(nx,ny,nz,nen)
  REAL :: qscalaren(nx,ny,nz,nscalar,nen)
!
! output:
!
  REAL :: su(nx,ny,nz)
  REAL :: sv(nx,ny,nz)
  REAL :: sw(nx,ny,nz)
  REAL :: sptprt(nx,ny,nz)
  REAL :: spprt(nx,ny,nz)
  REAL :: sqv(nx,ny,nz)
  REAL :: sqs(nx,ny,nz,nscalar)

!
! local
!
  INTEGER :: i, j, k, l,nq

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  su=0.0
  sv=0.0
  sw=0.0
  sptprt=0.0
  spprt=0.0
  sqv=0.0
  sqs(:,:,:,:)=0.0

  DO k=1,nz
   DO J=1,ny
    DO i=1,nx

     DO l=1,nen
       su(i,j,k)=su(i,j,k)+( uen(i,j,k,l) )**2
       sv(i,j,k)=sv(i,j,k)+( ven(i,j,k,l) )**2
       sw(i,j,k)=sw(i,j,k)+( wen(i,j,k,l) )**2
       sptprt(i,j,k)=sptprt(i,j,k)+( ptprten(i,j,k,l) )**2
       spprt(i,j,k)=spprt(i,j,k)+( pprten(i,j,k,l) )**2
       sqv(i,j,k)=sqv(i,j,k)+( qven(i,j,k,l)*1000.0 )**2
       DO nq = 1, nscalar
         sqs(i,j,k,nq)=sqs(i,j,k,nq)+( qscalaren(i,j,k,nq,l)*1000 )**2
       END DO
     END DO

     su(i,j,k)=sqrt(su(i,j,k)/(nen-1))
     sv(i,j,k)=sqrt(sv(i,j,k)/(nen-1))
     sw(i,j,k)=sqrt(sw(i,j,k)/(nen-1))
     sptprt(i,j,k)=sqrt(sptprt(i,j,k)/(nen-1))
     spprt(i,j,k)=sqrt(spprt(i,j,k)/(nen-1))
     sqv(i,j,k)=sqrt(sqv(i,j,k)/(nen-1))
     DO nq = 1, nscalar
       sqs(i,j,k,nq)=sqrt(sqs(i,j,k,nq)/(nen-1))
     END DO

    END DO
   END DO
  END DO

  RETURN
END SUBROUTINE SPREAD_SUB

!
!#######################################################################
!

SUBROUTINE aver(nx,ny,nz,sc,reflo,sca,tmp)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate ensemble spread
!
!-----------------------------------------------------------------------


  IMPLICIT NONE

  INCLUDE 'mp.inc'
!
! input:
!
  INTEGER :: nx, ny, nz
  REAL    :: sc(nx,ny,nz)
  REAL    :: reflo(nx,ny,nz)
!
! output:
!
  REAL    :: sca
  INTEGER :: tmp
!
! local
!
  INTEGER :: i, j, k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  sca=0.0
  tmp=0
  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        IF(reflo(i,j,k) > 10.0)THEN
          sca=sca+sc(i,j,k)
          tmp=tmp+1
        END IF
      END DO
    END DO
  END DO

  CALL mptotal( sca )
  CALL mptotali(tmp )

  IF ( myproc==0 ) THEN
    IF( tmp > 0 ) THEN
      sca=sca/tmp
    END IF
  END IF

END SUBROUTINE aver

!
!#######################################################################
!

SUBROUTINE perturb(nx,ny,nz,nscalar,nscalarq,nensmbl,                   &
                uen,ven,wen,pprten, ptprten,qven,qscalaren,calc_mean)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate ensemble perturbation
!
!-----------------------------------------------------------------------

  USE Model4DArrayE, only : umean,vmean,wmean,ptmean,phmean,qvmean,qscalarmean

  IMPLICIT NONE

!
!
! input:
!
  INTEGER :: nx,ny,nz,nensmbl,nscalar,nscalarq,calc_mean
  REAL :: uen(nx,ny,nz,nensmbl)
  REAL :: ven(nx,ny,nz,nensmbl)
  REAL :: wen(nx,ny,nz,nensmbl)
  REAL :: ptprten(nx,ny,nz,nensmbl)
  REAL :: pprten(nx,ny,nz,nensmbl)
  REAL :: qven(nx,ny,nz,nensmbl)
  REAL :: qscalaren(nx,ny,nz,nscalar,nensmbl)
  REAL :: cfun1
!
! local
!
  INTEGER :: i, j, k, l,nq

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx

        cfun1 = 0.
        DO l=1,nensmbl
          cfun1 = cfun1  + uen(i,j,k,l)
        ENDDO
        cfun1 = cfun1/nensmbl
        IF (calc_mean==1) umean(i,j,k)=cfun1

        DO l=1,nensmbl
          uen(i,j,k,l)= uen(i,j,k,l)-cfun1
        ENDDO

        cfun1 = 0.
        DO l=1,nensmbl
          cfun1 = cfun1  + ven(i,j,k,l)
        ENDDO
        cfun1 = cfun1/nensmbl
        IF (calc_mean==1) vmean(i,j,k)=cfun1

        DO l=1,nensmbl
          ven(i,j,k,l)= ven(i,j,k,l)-cfun1
        ENDDO

        cfun1 = 0.
        DO l=1,nensmbl
          cfun1 = cfun1  + wen(i,j,k,l)
        ENDDO
        cfun1 = cfun1/nensmbl
        IF (calc_mean==1) wmean(i,j,k)=cfun1

        DO l=1,nensmbl
          wen(i,j,k,l)= wen(i,j,k,l)-cfun1
        ENDDO

        cfun1 = 0.
        DO l=1,nensmbl
          cfun1 = cfun1  + pprten(i,j,k,l)
        ENDDO
        cfun1 = cfun1/nensmbl
        IF (calc_mean==1) phmean(i,j,k)=cfun1

        DO l=1,nensmbl
          pprten(i,j,k,l)= pprten(i,j,k,l)-cfun1
        ENDDO

        cfun1 = 0.
        DO l=1,nensmbl
          cfun1 = cfun1  + ptprten(i,j,k,l)
        ENDDO
        cfun1 = cfun1/nensmbl
        IF (calc_mean==1) ptmean(i,j,k)=cfun1

        DO l=1,nensmbl
          ptprten(i,j,k,l)= ptprten(i,j,k,l)-cfun1
        ENDDO

        cfun1 = 0.
        DO l=1,nensmbl
          cfun1 = cfun1  + qven(i,j,k,l)
        ENDDO
        cfun1 = cfun1/nensmbl
        IF (calc_mean==1) qvmean(i,j,k)=cfun1

        DO l=1,nensmbl
          qven(i,j,k,l)= qven(i,j,k,l)-cfun1
!         IF (qven(i,j,k,l)<0) qven(i,j,k,l)=0
        ENDDO

      END DO
    END DO
  END DO

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        DO nq=1, nscalarq

          cfun1 = 0.
          DO l=1,nensmbl
            cfun1 = cfun1  + qscalaren(i,j,k,nq,l)
          ENDDO
          cfun1 = cfun1/nensmbl
          IF (calc_mean==1) qscalarmean(i,j,k,nq)=cfun1
          DO l=1,nensmbl
            qscalaren(i,j,k,nq,l)= qscalaren(i,j,k,nq,l)-cfun1
          ENDDO

        END DO
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE perturb
