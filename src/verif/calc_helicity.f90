SUBROUTINE cal_helicity ( u, v, w,                                      &
                          up_heli_max,  & !up_heli_max16,                &
                          ph, phb, ht,                                  &
                          msfux, msfuy,msfvx, msfvy,                    &
                          rdx, rdy, dn, dnw, rdz, rdzw,                 &
                          fnm, fnp, cf1, cf2, cf3, zx, zy,              &
                          nx,ny,nz, istatus      )
    IMPLICIT NONE

    INTEGER, INTENT( IN )     :: nx, ny, nz

    REAL,    INTENT( IN )     :: rdx, rdy, cf1, cf2, cf3

    REAL, DIMENSION( 1:nz-1 ),           INTENT( IN ) :: fnm, fnp, dn, dnw

    REAL, DIMENSION( 1:nx , 1:ny ),      INTENT( IN ) :: msfux, msfuy, msfvx, msfvy, ht

    REAL, DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( IN ) :: u, v, w, ph, phb
    REAL, DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( IN ) :: zx, zy, rdz, rdzw

    REAL, DIMENSION( 1:nx-1, 1:ny-1 ),   INTENT( OUT ) :: up_heli_max

    INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

    REAL, PARAMETER :: g = 9.81

    REAL, DIMENSION( 1:nx-1, 1:ny-1 )  :: up_heli_max16

    REAL, DIMENSION( 1:nx, 1:ny )             :: uh, uh16

    REAL, DIMENSION( 2:nx-1, 2:ny-1 )         :: mm

    REAL, DIMENSION( 1:nx-1, 1:ny-1, 1:nz-1 ) :: hat
    REAL, DIMENSION( 2:nx-1, 2:ny-1, 1:nz-1 ) :: tmp1, wavg, rvort
    REAL, DIMENSION( 2:nx-1, 2:ny-1, 1:nz   ) :: hatavg

    LOGICAL, DIMENSION( 2:nx-1, 2:ny-1 )      :: use_column, use_column16

    INTEGER :: i, j, k, i_start, i_end, j_start, j_end
    INTEGER :: kts, kte, ktf, ktes1, ktes2

    REAL    :: tmpzx, tmpzy, cft1, cft2
    REAL    :: zl, zu, uh_smth, uh_smth16

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    up_heli_max(:,:)   = 0.0
    up_heli_max16(:,:) = 0.0

    kts     = 1
    kte     = nz
    ktf     = nz-1

    ktes1   = ktf
    ktes2   = ktf-1

    cft2    = - 0.5 * dnw(ktes1) / dn(ktes1)
    cft1    = 1.0 - cft2

    i_start = 2
    i_end   = nx-1
    j_start = 2
    j_end   = ny-1

    DO j = j_start, j_end
      DO i = i_start, i_end
        mm(i,j) = 0.25 * ( msfux(i,j-1) + msfux(i,j) ) * ( msfvy(i-1,j) + msfvy(i,j) )
      END DO
    END DO

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start-1, i_end
       hat(i,j,k) = v(i,j,k) / msfvy(i,j)
    END DO
    END DO
    END DO

    DO k = kts+1, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      hatavg(i,j,k) = 0.5 * (  &
                      fnm(k) * ( hat(i-1,j,k  ) + hat(i,j,k  ) ) +  &
                      fnp(k) * ( hat(i-1,j,k-1) + hat(i,j,k-1) ) )
    END DO
    END DO
    END DO


    DO j = j_start, j_end
    DO i = i_start, i_end
       hatavg(i,j,1)   =  0.5 * (  &
                          cf1 * hat(i-1,j,1) +  &
                          cf2 * hat(i-1,j,2) +  &
                          cf3 * hat(i-1,j,3) +  &
                          cf1 * hat(i  ,j,1) +  &
                          cf2 * hat(i  ,j,2) +  &
                          cf3 * hat(i  ,j,3) )
       hatavg(i,j,kte) =  0.5 * (  &
                          cft1 * ( hat(i,j,ktes1) + hat(i-1,j,ktes1) ) +  &
                          cft2 * ( hat(i,j,ktes2) + hat(i-1,j,ktes2) ) )
    END DO
    END DO


    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      tmpzx       = 0.25 * (  &
                    zx(i,j-1,k  ) + zx(i,j,k  ) +  &
                    zx(i,j-1,k+1) + zx(i,j,k+1) )
      tmp1(i,j,k) = ( hatavg(i,j,k+1) - hatavg(i,j,k) ) *  &
                    0.25 * tmpzx * ( rdzw(i,j,k) + rdzw(i,j-1,k) + &
                                     rdzw(i-1,j-1,k) + rdzw(i-1,j,k) )
    END DO
    END DO
    END DO


    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      rvort(i,j,k) = mm(i,j) * (  &
                     rdx * ( hat(i,j,k) - hat(i-1,j,k) ) - tmp1(i,j,k) )
    END DO
    END DO
    END DO


    DO k =kts, ktf
    DO j =j_start-1, j_end
    DO i =i_start, i_end
      hat(i,j,k) = u(i,j,k) / msfux(i,j)
    END DO
    END DO
    END DO

    DO k=kts+1,ktf
    DO j=j_start,j_end
    DO i=i_start,i_end
      hatavg(i,j,k) = 0.5 * (  &
                      fnm(k) * ( hat(i,j-1,k  ) + hat(i,j,k  ) ) +  &
                      fnp(k) * ( hat(i,j-1,k-1) + hat(i,j,k-1) ) )
    END DO
    END DO
    END DO



    DO j = j_start, j_end
    DO i = i_start, i_end
      hatavg(i,j,1)   =  0.5 * (  &
                         cf1 * hat(i,j-1,1) +  &
                         cf2 * hat(i,j-1,2) +  &
                         cf3 * hat(i,j-1,3) +  &
                         cf1 * hat(i,j  ,1) +  &
                         cf2 * hat(i,j  ,2) +  &
                         cf3 * hat(i,j  ,3) )
      hatavg(i,j,kte) =  0.5 * (  &
                         cft1 * ( hat(i,j-1,ktes1) + hat(i,j,ktes1) ) +  &
                         cft2 * ( hat(i,j-1,ktes2) + hat(i,j,ktes2) ) )
    END DO
    END DO



    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      tmpzy       = 0.25 * (  &
                    zy(i-1,j,k  ) + zy(i,j,k  ) +  &
                    zy(i-1,j,k+1) + zy(i,j,k+1) )
      tmp1(i,j,k) = ( hatavg(i,j,k+1) - hatavg(i,j,k) ) *  &
                    0.25 * tmpzy * ( rdzw(i,j,k) + rdzw(i-1,j,k) + &
                                     rdzw(i-1,j-1,k) + rdzw(i,j-1,k) )
    END DO
    END DO
    END DO

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      rvort(i,j,k) = rvort(i,j,k) -  &
                     mm(i,j) * (  &
                     rdy * ( hat(i,j,k) - hat(i,j-1,k) ) - tmp1(i,j,k) )
    END DO
    END DO
    END DO

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      wavg(i,j,k) = 0.125 * (  &
                    w(i,j  ,k  ) + w(i-1,j  ,k  ) +  &
                    w(i,j-1,k  ) + w(i-1,j-1,k  ) +  &
                    w(i,j  ,k+1) + w(i-1,j  ,k+1) +  &
                    w(i,j-1,k+1) + w(i-1,j-1,k+1) )
    END DO
    END DO
    END DO


    DO j = j_start, j_end
    DO i = i_start, i_end
      use_column(i,j) = .true.
      uh(i,j) = 0.

      use_column16(i,j) = .true.
      uh16(i,j) = 0.

    END DO
    END DO

    DO k = kts, ktf
    DO j = j_start, j_end
    DO i = i_start, i_end
      zl = ( 0.25 * (  &
           (( ph(i  ,j  ,k  ) + phb(i  ,j  ,k  ) ) / g - ht(i  ,j  ) ) +  &
           (( ph(i-1,j  ,k  ) + phb(i-1,j  ,k  ) ) / g - ht(i-1,j  ) ) +  &
           (( ph(i  ,j-1,k  ) + phb(i  ,j-1,k  ) ) / g - ht(i  ,j-1) ) +  &
           (( ph(i-1,j-1,k  ) + phb(i-1,j-1,k  ) ) / g - ht(i-1,j-1) ) ) )

      zu = ( 0.25 * (  &
           (( ph(i  ,j  ,k+1) + phb(i  ,j  ,k+1) ) / g - ht(i  ,j  ) ) +  &
           (( ph(i-1,j  ,k+1) + phb(i-1,j  ,k+1) ) / g - ht(i-1,j  ) ) +  &
           (( ph(i  ,j-1,k+1) + phb(i  ,j-1,k+1) ) / g - ht(i  ,j-1) ) +  &
           (( ph(i-1,j-1,k+1) + phb(i-1,j-1,k+1) ) / g - ht(i-1,j-1) ) ) )


      IF ( zl .GE. 2000. .AND. zu .LE. 5000. ) THEN
        IF ( wavg(i,j,k) .GT. 0. .AND. wavg(i,j,k+1) .GT. 0. ) THEN
          uh(i,j) = uh(i,j) + ( ( wavg(i,j,k) * rvort(i,j,k) + &
                    wavg(i,j,k+1) * rvort(i,j,k+1) ) * 0.5 ) &
                    * ( zu - zl )
        ELSE
          use_column(i,j) = .false.
          uh(i,j) = 0.
        ENDIF
      ENDIF


      IF ( zl .GE. 1000. .AND. zu .LE. 6000. ) THEN
        IF ( wavg(i,j,k) .GT. 0. .AND. wavg(i,j,k+1) .GT. 0. ) THEN
          uh16(i,j) = uh16(i,j) + ( ( wavg(i,j,k) * rvort(i,j,k) + &
                    wavg(i,j,k+1) * rvort(i,j,k+1) ) * 0.5 ) &
                    * ( zu - zl )
        ELSE
          use_column16(i,j) = .false.
          uh16(i,j) = 0.
        ENDIF
      ENDIF
    END DO
    END DO
    END DO



    DO j = 3,ny-2
    DO i = 3,nx-2
      uh_smth = 0.25   *   uh(i  ,j  ) + &
                0.125  * ( uh(i+1,j  ) + uh(i-1,j  ) + &
                           uh(i  ,j+1) + uh(i  ,j-1) ) + &
                0.0625 * ( uh(i+1,j+1) + uh(i+1,j-1) + &
                           uh(i-1,j+1) + uh(i-1,j-1) )

      IF ( use_column(i,j) .and. uh_smth .GT. up_heli_max(i,j) ) THEN
        up_heli_max(i,j) = uh_smth
      ENDIF

      IF ( up_heli_max(i,j) .GT. 1.0e+3 ) THEN
        print *,' i,j,up_heli_max = ', i,j,up_heli_max(i,j)
      ENDIF


      uh_smth16 = 0.25   *   uh16(i  ,j  ) + &
                 .125  * ( uh16(i+1,j  ) + uh16(i-1,j  ) + &
                             uh16(i  ,j+1) + uh16(i  ,j-1) ) + &
                .0625 * ( uh16(i+1,j+1) + uh16(i+1,j-1) + &
                             uh16(i-1,j+1) + uh16(i-1,j-1) )

      IF ( use_column16(i,j) .and. uh_smth16 .GT. up_heli_max16(i,j) ) THEN
        up_heli_max16(i,j) = uh_smth16
      ENDIF

    END DO
    END DO

    RETURN
END SUBROUTINE cal_helicity

!=======================================================================

SUBROUTINE compute_diff_metrics( nx,ny,nz,ph, phb, rdx, rdy,            &
                                 rdz, rdzw, zx, zy, istatus)

!-----------------------------------------------------------------------
! Begin declarations.

    IMPLICIT NONE

    INTEGER, INTENT( IN ) :: nx, ny,nz
    REAL,    INTENT( IN ) :: rdx, rdy

    REAL, DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( IN )  :: ph, phb
    REAL, DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( OUT ) :: rdz, rdzw, zx, zy

    INTEGER, INTENT(OUT)  :: istatus

!-----------------------------------------------------------------------
    REAL, PARAMETER :: g = 9.81

! Local variables.

    REAL, DIMENSION( 1:nx, 1:ny, 1:nz ) :: z_at_w

    INTEGER :: i, j, k, i_start, i_end, j_start, j_end, ktf

! End declarations.
!-----------------------------------------------------------------------

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    ktf = nz-1

! We need rdzw in halo for average to u and v points.

    j_start = 1
    j_end   = ny

    i_start = 1
    i_end   = nx

! Begin with dz computations.
!
! Compute z at w points for rdz and rdzw computations.  We'll switch z
! to z at p points before returning
   DO j = j_start, j_end

      DO k = 1, nz
        DO i = i_start, i_end
          z_at_w(i,j,k) = ( ph(i,j,k) + phb(i,j,k) ) / g
        END DO
      END DO

      DO k = 1, ktf
        DO i = i_start, i_end
          rdzw(i,j,k) = 1.0 / ( z_at_w(i,j,k+1) - z_at_w(i,j,k) )
        END DO
      END DO

      DO k = 2, ktf
        DO i = i_start, i_end
          rdz(i,j,k) = 2.0 / ( z_at_w(i,j,k+1) - z_at_w(i,j,k-1) )
        END DO
      END DO

      DO i = i_start, i_end
        rdz(i,j,1) = 2./(z_at_w(i,j,2)-z_at_w(i,j,1))
      END DO

   END DO
! Now compute zx and zy; we'll assume that the halo for ph and phb is
! properly filled.

    j_start = 1
    j_end   = ny

    i_start = 1
    i_end   = nx

    DO k = 1, nz
    DO j = j_start, j_end
    DO i = 2, i_end
      zx(i,j,k) = rdx * ( phb(i,j,k) - phb(i-1,j,k) ) / g
    END DO
    END DO
    END DO

    DO k = 1, nz
    DO j = j_start, j_end
    DO i = 2, i_end
      zx(i,j,k) = zx(i,j,k) + rdx * ( ph(i,j,k) - ph(i-1,j,k) ) / g
    END DO
    END DO
    END DO

    DO k = 1, nz
    DO j = 2, j_end
    DO i = i_start, i_end
      zy(i,j,k) = rdy * ( phb(i,j,k) - phb(i,j-1,k) ) / g
    END DO
    END DO
    END DO

    DO k = 1, nz
    DO j = 2, j_end
    DO i = i_start, i_end
      zy(i,j,k) = zy(i,j,k) + rdy * ( ph(i,j,k) - ph(i,j-1,k) ) / g
    END DO
    END DO
    END DO

! Some b.c. on zx and zy.

    !DO k=1,ktf
    !  DO j=j_start,j_end
    !     zx(nx,j,k) = rdx * ( phb(nx,j,k) - phb(nx-1,j,k) ) / g
    !  END DO
    !END DO
    !
    !DO k = 1, ktf
    !  DO j = j_start, j_end
    !      zx(nx,j,k) = zx(nx,j,k) + rdx * ( ph(nx,j,k) - ph(nx-1,j,k) ) / g
    !  END DO
    !END DO

    DO k = 1, nz
      DO j = j_start, j_end
          zx(1,j,k) = rdx * ( phb(2,j,k) - phb(1,j,k) ) / g
      END DO
    END DO

    DO k =1,nz
      DO j =j_start,j_end
          zx(1,j,k) = zx(1,j,k) + rdx * ( ph(2,j,k) - ph(1,j,k) ) / g
      END DO
    END DO


    !DO k=1, ktf
    !  DO i =i_start, i_end
    !      zy(i,ny,k) = rdy * ( phb(i,ny,k) - phb(i,ny-1,k) ) / g
    !  END DO
    !END DO
    !
    !DO k = 1, ktf
    !  DO i =i_start, i_end
    !      zy(i,ny,k) = zy(i,ny,k) + rdy * ( ph(i,ny,k) - ph(i,ny-1,k) ) / g
    !  END DO
    !END DO
    !
    DO k = 1, nz
      DO i =i_start, i_end
          zy(i,1,k) = rdy * ( phb(i,2,k) - phb(i,1,k) ) / g
      END DO
    END DO

    DO k = 1, nz
      DO i =i_start, i_end
          zy(i,1,k) = zy(i,1,k) + rdy * ( ph(i,2,k) - ph(i,1,k) ) / g
      END DO
    END DO

    RETURN
END SUBROUTINE compute_diff_metrics

