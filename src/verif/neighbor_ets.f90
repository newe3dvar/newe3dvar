!
!##################################################################
!##################################################################
!######                                                      ######
!######                   PROGRAM NEIGHBOR_ETS               ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
PROGRAM neighbor_ets
!
!#######################################################################
!
! PURPOSE: Compute neighborhood ETS for DBZ
!
!-----------------------------------------------------------------------
!
! AUTHOR: Y. Wang (05/24/2018)
! Based on Alexandre Fierro's Fortran program neighbor_ets_1hr_06APRIL.f
!
! MODIFICATION HISTORY:
!
!
!#######################################################################

  IMPLICIT NONE

  INTEGER, PARAMETER :: ntime_max = 60,  ntime_copy = 100
  INTEGER, PARAMETER :: nthres_max = 30, nradius_max = 20

  INCLUDE 'mp.inc'
!
!-----------------------------------------------------------------------
!
!  NAMELIST parameter (In wrfspread.input)
!
!-----------------------------------------------------------------------
!

  NAMELIST /message_passing/ nproc_x, nproc_y, nproc_x_in, nproc_y_in

  INTEGER :: ntime, ncopy, obsfmt
  CHARACTER(LEN=256) :: fcst_files(ntime_max*ntime_copy), obs_files(ntime_max*ntime_copy)
  NAMELIST /input_files/ ntime,ncopy,fcst_files, obsfmt, obs_files

  INTEGER :: nthres, nradius, vfield
  REAL    :: thres(nthres_max), radius(nradius_max)
  INTEGER :: vradius(nradius_max)

  INTEGER :: iboxs, iboxe, jboxs, jboxe
  INTEGER :: rainaccum

  NAMELIST /verif_parms/ vfield,rainaccum,nthres, thres, nradius, radius, vradius,&
                         iboxs, iboxe, jboxs, jboxe

  INTEGER :: outcontg
  CHARACTER(LEN=256) :: outfile(2), contgfile
  NAMELIST /output/ outfile, outcontg, contgfile
!
!-----------------------------------------------------------------------
!
!  Misc local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: nx, ny, nz
  REAL    :: dx, dy, dxkm, dykm
  INTEGER :: fhits, ohits, nfile

  INTEGER :: i,j,k,nt, nc, ij,kk, im,jm,km, ni
  INTEGER :: num_pts(nradius_max)
  INTEGER, ALLOCATABLE :: ii(:,:),jj(:,:)

  INTEGER :: max_rx,max_ry, irad, jrad
  REAL    :: test
  INTEGER :: r, n, working1

  INTEGER :: istatus, itime
  LOGICAL :: read3d
  INTEGER :: iyr, imn, idy, ihr, imin, isec
  INTEGER :: abstsec0(ntime_copy), abstsec, indx1, indx2
  INTEGER :: timediff(ntime_max)
  CHARACTER(LEN=1) :: ach

  INTEGER :: unum, outunt, ncid
  INTEGER :: lenstr
  CHARACTER(LEN=256) :: tmpstr
  LOGICAL :: fexist

  CHARACTER(LEN=40) :: fcst_fieldname, obs_fieldname

  REAL,    ALLOCATABLE :: dbz3d(:,:,:), rain_prev(:,:,:), varin(:,:), obs_prev(:,:,:)
  REAL,    ALLOCATABLE :: var_fcst(:,:), var_obs(:,:)

  INTEGER :: ntimestep, ntimeout

  ! For vfield = 3, UH
  CHARACTER(LEN=19) :: datestr

  INTEGER :: nx_wrf, ny_wrf, nz_wrf
  REAL    :: rdx, rdy

  INTEGER, ALLOCATABLE :: fHndl(:,:)

  REAL,    ALLOCATABLE :: temd (:,:,:)

  REAL,    ALLOCATABLE :: dn(:), dnw(:), fnm(:), fnp(:)
  REAL                 :: cf1, cf2, cf3

  REAL,    ALLOCATABLE :: msfux(:,:),msfuy(:,:),msfvx(:,:),msfvy(:,:)
  REAL,    ALLOCATABLE :: rdz(:,:,:), rdzw(:,:,:), zx(:,:,:), zy(:,:,:)

  REAL,    ALLOCATABLE ::  u(:,:,:), v(:,:,:), w(:,:,:)
  REAL,    ALLOCATABLE :: ph(:,:,:),phb(:,:,:), ht(:,:)

  ! for computing scores
  REAL,    ALLOCATABLE :: ets(:,:,:), bias(:,:,:)
  INTEGER(KIND=8), ALLOCATABLE :: hits(:,:,:), fals(:,:,:), miss(:,:,:),cneg(:,:,:)
  INTEGER(KIND=8) :: hitl, fall, misl, negl, chance

  INTEGER :: nxsize, nysize, nxres, nyres, myrank
  INTEGER :: xextra, yextra, myxext, myyext
  INTEGER :: ibgn, iend, jbgn, jend, kbgn,kend
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  istatus = 0

!-----------------------------------------------------------------------
!
!  Initialize local variables
!
!-----------------------------------------------------------------------
!
  ! Non-MPI defaults: All others initialized in mpinit_var
  mp_opt = 0
  myproc = 0
  loc_x  = 1
  loc_y  = 1
  nproc_x_in  = 1
  nproc_y_in  = 1
  dumpstride = 1
  readstride = 1

  CALL mpinit_proc(0)

  IF(myproc == 0) THEN

    WRITE(6,'(10(/5x,a),/)')                                            &
  '###################################################################',&
  '###################################################################',&
  '####                                                           ####',&
  '####                Welcome to neighbor_ets                    ####',&
  '####                                                           ####',&
  '####       Program that reads in output from WRF model,        ####',&
  '####       compute the verification scores                     ####',&
  '####                                                           ####',&
  '###################################################################',&
  '###################################################################'

    unum = COMMAND_ARGUMENT_COUNT()
    IF (unum > 0) THEN
      CALL GET_COMMAND_ARGUMENT(1, tmpstr, lenstr, istatus )
      IF ( tmpstr(1:1) == ' ' .OR. istatus /= 0 ) THEN
        ! Use standard input to be backward-compatible
        unum = 5
      ELSE
        INQUIRE(FILE=TRIM(tmpstr),EXIST=fexist)
        IF (.NOT. fexist) THEN
          WRITE(6,'(1x,3a)') 'WARNING: namelist file - ',               &
                TRIM(tmpstr),' does not exist. Falling back to standard input.'
          unum = 5
        END IF
      END IF
    ELSE
      unum = 5
    END IF

    IF (unum /= 5) THEN
      CALL getunit( unum )
      OPEN(unum,FILE=TRIM(tmpstr),STATUS='OLD',FORM='FORMATTED')
      WRITE(*,'(1x,3a,/,1x,a,/)') 'Reading namelist from file - ', &
              TRIM(tmpstr),' ... ','========================================'
    ELSE
      WRITE(*,'(2(1x,a,/))') 'Waiting namelist from standard input ... ', &
                             '========================================'
    END IF

  END IF
!-----------------------------------------------------------------------
!
!  Read NAMELIST variables
!
!-----------------------------------------------------------------------
!
  IF (myproc == 0) THEN
    READ(unum,message_passing)
    WRITE(6,'(a)') ' Namelist block message_passing sucessfully read.'
  END IF
  CALL mpupdatei(nproc_x,1)
  CALL mpupdatei(nproc_y,1)
  CALL mpupdatei(nproc_x_in,1)
  CALL mpupdatei(nproc_y_in,1)

  IF (mp_opt == 0) THEN
    nproc_x = 1
    nproc_y = 1
    nproc_x_in = 1
    nproc_y_in = 1
  END IF

  ntime = 1
  ncopy = 1
  obsfmt = 1
  IF (myproc == 0) THEN
    READ(unum,input_files)
    WRITE(6,'(a)') ' Namelist block input_files sucessfully read.'
  END IF
  CALL mpupdatei(ntime,1)
  CALL mpupdatei(ncopy,1)
  CALL mpupdatei(obsfmt,1)
  CALL mpupdatec(fcst_files,256*ntime*ncopy)
  CALL mpupdatec(obs_files, 256*ntime*ncopy)

  !IF (obsfmt == 1) THEN
    itime = 1
  !ELSE
  !  itime = 0
  !END IF

  read3d = .TRUE.
  IF (obsfmt > 200) THEN
    read3d = .FALSE.
  END IF

  ! different radii (km) over which to compute ETS
  radius(1)=1.0
  radius(2)=4.0
  radius(3)=8.0
  radius(4)=12.0
  radius(5)=16.0
  radius(6)=20.0
  radius(7)=24.0
  radius(8)=32.0
  radius(9)=40.0
  nradius = 9

  ! determine new threshold based on biasav:
  thres(1)=20.0 ! 2.5 !0.10*25.4
  thres(2)=30.0 ! 5.0 !0.25*25.4
  thres(3)=40.0 ! 10.0 !0.50*25.4
  nthres = 3

  iboxs = 0
  iboxe = -1
  jboxs = 0
  jboxe = -1

  vfield = 1
  rainaccum = 0
  IF (myproc == 0) THEN
    READ(unum,verif_parms)
    WRITE(6,'(a)') ' Namelist block verif_parms sucessfully read.'
  END IF
  CALL mpupdatei(vfield,1)
  CALL mpupdatei(rainaccum, 1)
  CALL mpupdatei(nradius,1)
  CALL mpupdater(radius,nradius)
  CALL mpupdatei(nthres, 1)
  CALL mpupdater(thres,nthres)
  CALL mpupdatei(iboxs, 1)
  CALL mpupdatei(iboxe, 1)
  CALL mpupdatei(jboxs, 1)
  CALL mpupdatei(jboxe, 1)

  outcontg = 0
  IF (myproc == 0) THEN
    READ(unum,output)
    WRITE(6,'(a)') ' Namelist block output sucessfully read.'
  END IF
  CALL mpupdatec(outfile,2*256)
  CALL mpupdatec(contgfile,256)
  CALL mpupdatei(outcontg,1)

  IF (unum /= 5 .AND. myproc == 0) THEN
    CLOSE( unum )
    CALL retunit( unum )
  END IF

  outunt = 88

!-----------------------------------------------------------------------
!
!  Get file dimensions
!
!-----------------------------------------------------------------------
!
  fcst_fieldname = ' '
  obs_fieldname  = ' '

  IF (vfield == 1) THEN
    fcst_fieldname = 'REFL_10CM      '
    obs_fieldname  = 'REFMOSAIC3D    '
  ELSE IF (vfield == 2) THEN
    fcst_fieldname = 'RAINNC         '
    obs_fieldname  = 'HOURLYPRCP     '
  ELSE IF (vfield == 3) THEN
    fcst_fieldname = 'UP_HELI_MAX    '
    obs_fieldname  = 'MANY           '
  ELSE
    WRITE(*,'(1x,a,I0,a)') 'ERROR: unsuppported vfield(',vfield,') option.'
    STOP
  END IF

  IF (myproc == 0) THEN
    CALL netopen(fcst_files(1),'R',ncid)

    CALL netreaddim(ncid, 'west_east',   nx,istatus)
    CALL netreaddim(ncid, 'south_north', ny,istatus)
    CALL netreaddim(ncid, 'bottom_top',  nz,istatus)

    CALL netreadattr(ncid,'DX',dx,istatus)
    CALL netreadattr(ncid,'DY',dy,istatus)
    CALL netclose(ncid)

    !  1    5    0    5    0    5    0
    ! 'wrfout_d01_2018-05-31_00:00:00'
    !indx1 = INDEX(fcst_files(1),'wrfout_d',.TRUE.)
    !READ(fcst_files(1)(indx1+11:indx1+29),'(I4.4,5(a,I2.2))') iyr,ach,imn,ach,idy,ach,ihr,ach,imin,ach,isec
    !CALL ctim2abss( iyr,imn,idy,ihr,imin,isec, abstsec0 )

  END IF
  CALL mpupdatei(nx,1)   ! This unstagger dimensions
  CALL mpupdatei(ny,1)   ! valid for the verification field on mass grid
  CALL mpupdatei(nz,1)   !
  CALL mpupdater(dx,1)
  CALL mpupdater(dy,1)

  IF (iboxe < iboxs .OR. jboxe < jboxs) THEN
    iboxs = 1
    iboxe = nx
    jboxs = 1
    jboxe = ny
  END IF

  IF (iboxs < 1 .OR. iboxe > nx) THEN
    WRITE(*,'(1x,3(a,I0))') 'ERROR: invalid box size in x direction: ',iboxs,' -- ', iboxe,', nx = ', nx
    STOP
  END IF

  IF (jboxs < 1 .OR. jboxe > ny) THEN
    WRITE(*,'(1x,3(a,I0))') 'ERROR: invalid box size in y direction: ',jboxs,' -- ', jboxe,', ny = ', ny
    STOP
  END IF


  kbgn  = 1
  kend  = nz

  nxsize = (iboxe-iboxs+1)/nproc_x
  nxres  = MOD((iboxe-iboxs+1),nproc_x)

  nysize = (jboxe-jboxs+1)/nproc_y
  nyres  = MOD((jboxe-jboxs+1),nproc_y)

  DO j = 1, nproc_y
    DO i = 1, nproc_x
      myrank = i-1 + (j-1)*nproc_x

      xextra = 0
      myxext = 0
      IF (i <= nxres) THEN
        xextra = i-1
        myxext = 1
      ELSE
        xextra = nxres
        myxext = 0
      END IF

      yextra = 0
      myyext = 0
      IF (j <= nyres) THEN
        yextra = j-1
        myyext = 1
      ELSE
        yextra = nyres
        myyext = 0
      END IF

      IF (myproc == myrank) THEN
        ibgn = (i-1)*nxsize + iboxs  + xextra
        iend = ibgn + nxsize - 1 + myxext

        jbgn = (j-1)*nysize + jboxs  + yextra
        jend = jbgn + nysize - 1 + myyext
      END IF
    END DO
  END DO

  !WRITE(0,*) myproc, ibgn, iend, jbgn,jend
  !CALL arpsstop('',0)

  dxkm = dx/1000.
  dykm = dy/1000.

  test = MAXVAL(radius)
  irad = 2*CEILING(test/dxkm)
  irad = irad*irad

  IF (myproc == 0) WRITE(*,'(1x,a,I0)') 'Max possible size, ',irad

  ALLOCATE(ii(irad,nradius_max), STAT = istatus)
  ALLOCATE(jj(irad,nradius_max), STAT = istatus)
  ii = 0
  jj = 0

  DO r = 1, nradius
     ! step 1: figure out grid-points to include in neighborhood...
     max_rx=ANINT(radius(r)/dxkm)
     max_ry=ANINT(radius(r)/dykm)

     num_pts(r) = 0

     DO j = -max_ry, max_ry
        DO i = -max_rx, max_rx
          irad=i*dxkm
          jrad=j*dykm

          test=sqrt(irad**2.0 + jrad**2.0)
          IF ( test <= radius(r) ) THEN
             num_pts(r)=num_pts(r) + 1
             ii(num_pts(r),r)=i
             jj(num_pts(r),r)=j
          END IF
        END DO
     END DO
     IF (myproc == 0) WRITE(*,'(1x,a,I0,a,F8.2,a,I0)') 'radius(',r,') = ',radius(r),', num_pts = ', num_pts(r)
  END DO

  ALLOCATE(var_fcst(nx,ny), STAT = istatus)
  ALLOCATE(var_obs(nx,ny),  STAT = istatus)

  IF (vfield == 1) THEN
    ALLOCATE(dbz3d(nx,ny,nz), STAT = istatus)
  ELSE IF(vfield == 2) THEN
    ALLOCATE(rain_prev(nx,ny,ncopy), STAT = istatus)
    ALLOCATE(obs_prev(nx,ny,ncopy), STAT = istatus)
    ALLOCATE(varin(nx,ny),           STAT = istatus)
    rain_prev = 0.0
    obs_prev  = 0.0
  ELSE IF(vfield == 3) THEN

    datestr = '2012-10-25_16:22:00'

    nx_wrf = nx+1    ! stagger dimensions to be used for vfield = 3
    ny_wrf = ny+1
    nz_wrf = nz+1

    rdx = 1.0/dx
    rdy = 1.0/dy

    ALLOCATE(fHndl(1,1), STAT = istatus)

    ALLOCATE(temd (nx_wrf,ny_wrf,nz_wrf), STAT = istatus)

    ALLOCATE(dn(nz),  STAT = istatus)
    ALLOCATE(dnw(nz), STAT = istatus)
    ALLOCATE(fnm(nz), STAT = istatus)
    ALLOCATE(fnp(nz), STAT = istatus)

    ALLOCATE(msfux(nx_wrf,ny_wrf), STAT = istatus)
    ALLOCATE(msfuy(nx_wrf,ny_wrf), STAT = istatus)
    ALLOCATE(msfvx(nx_wrf,ny_wrf), STAT = istatus)
    ALLOCATE(msfvy(nx_wrf,ny_wrf), STAT = istatus)

    ALLOCATE(ht(nx_wrf,ny_wrf),         STAT = istatus)
    ALLOCATE(u(nx_wrf,ny_wrf,nz_wrf),   STAT = istatus)
    ALLOCATE(v(nx_wrf,ny_wrf,nz_wrf),   STAT = istatus)
    ALLOCATE(w(nx_wrf,ny_wrf,nz_wrf),   STAT = istatus)
    ALLOCATE(ph(nx_wrf,ny_wrf,nz_wrf),  STAT = istatus)
    ALLOCATE(phb(nx_wrf,ny_wrf,nz_wrf), STAT = istatus)

    ALLOCATE(rdz(nx_wrf,ny_wrf,nz_wrf),   STAT = istatus)
    ALLOCATE(rdzw(nx_wrf,ny_wrf,nz_wrf),  STAT = istatus)
    ALLOCATE(zx(nx_wrf,ny_wrf,nz_wrf),    STAT = istatus)
    ALLOCATE(zy(nx_wrf,ny_wrf,nz_wrf),    STAT = istatus)

  END IF

  ALLOCATE(hits(nradius,nthres,ntime), STAT = istatus)
  ALLOCATE(fals(nradius,nthres,ntime), STAT = istatus)
  ALLOCATE(miss(nradius,nthres,ntime), STAT = istatus)
  ALLOCATE(cneg(nradius,nthres,ntime), STAT = istatus)
  ALLOCATE(ets (nradius,nthres,ntime), STAT = istatus)
  ALLOCATE(bias(nradius,nthres,ntime), STAT = istatus)

  hits(:,:,:) = 0.
  fals(:,:,:) = 0
  miss(:,:,:) = 0
  cneg(:,:,:) = 0
  ets(:,:,:)  = 0.0
  bias(:,:,:) = 0.0

  loop_nt: DO nt = 1, ntime

    DO nc = 1, ncopy

      nfile = (nt-1)*ncopy + nc

      IF (myproc == 0) THEN
        !WRITE(*,'(1x,a,I0,a,I0,a)') 'Reading file at nt = ',nt,', forecast time: ',(nt-1)*15,' minutes.'
        WRITE(*,'(1x,3a)') 'Reading FCST file "',TRIM(fcst_files(nfile)),'" ....'

        CALL netopen(fcst_files(nfile),'R',ncid)
        IF (vfield == 1) THEN
          CALL netread3d(ncid,0,1,fcst_fieldname,nx,ny,nz,dbz3d)
          var_fcst = MAXVAL(dbz3d,3)
          !CALL smooth9p(dbz_fcst,nx,ny,1,nx,1,ny,0,tem1)!smooth forecast field
        ELSE IF (vfield == 2) THEN
          CALL netread2d(ncid,0,1,fcst_fieldname,nx,ny,varin)
          IF (rainaccum == 0 ) THEN
            var_fcst = varin - rain_prev(:,:,nc)          ! get hourly precipitation
            !WRITE(*,*) 'subtraction ',nt, var_fcst(2,2), rain_prev(2,2,nc)
            rain_prev(:,:,nc) = varin                     ! Assume forecast is accumulated from time 0
          ELSE IF (mod(nt-1,rainaccum) == 0) THEN
            var_fcst = varin - rain_prev(:,:,nc)          ! get hourly precipitation
            !WRITE(*,*) 'subtraction ',nt, var_fcst(2,2), rain_prev(2,2,nc)
            rain_prev(:,:,nc) = varin                     ! Assume forecast is accumulated from time 0
          !ELSE   ! Do not care, should not be used
          END IF

        ELSE IF (vfield == 3) THEN
          CALL netread2d(ncid,0,1,fcst_fieldname,nx,ny,var_fcst)
          fHndl(1,1) = ncid   ! read grid parameters for computing UH
          CALL get_wrf_2d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'MAPFAC_UX','X',                &
                          nx_wrf,ny_wrf,msfux,nx_wrf,ny_wrf,temd,istatus)
          CALL get_wrf_2d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'MAPFAC_UY','X',                &
                          nx_wrf,ny_wrf,msfuy,nx_wrf,ny_wrf,temd,istatus)
          CALL get_wrf_2d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'MAPFAC_VX','Y',                &
                          nx_wrf,ny_wrf,msfvx,nx_wrf,ny_wrf,temd,istatus)
          CALL get_wrf_2d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'MAPFAC_VY','Y',                &
                          nx_wrf,ny_wrf,msfvy,nx_wrf,ny_wrf,temd,istatus)

          CALL get_wrf_1d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'DN',nz,dn,nz,istatus)
          CALL get_wrf_1d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'DNW',nz,dnw,nz,istatus)
          CALL get_wrf_1d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'FNM',nz,fnm,nz,istatus)
          CALL get_wrf_1d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'FNP',nz,fnp,nz,istatus)

          CALL get_wrf_s(fHndl,7,.FALSE.,.TRUE.,1,1,                    &
                          datestr,itime,'CF1',cf1,istatus)
          CALL get_wrf_s(fHndl,7,.FALSE.,.TRUE.,1,1,                    &
                          datestr,itime,'CF2',cf2,istatus)
          CALL get_wrf_s(fHndl,7,.FALSE.,.TRUE.,1,1,                    &
                          datestr,itime,'CF3',cf3,istatus)

        END IF
        CALL netclose(ncid)


        WRITE(*,'(1x,3a)') 'Reading OBS  file "',TRIM(obs_files(nfile)),'" ....'

        CALL netopen(obs_files(nfile),'R',ncid)
        IF (vfield == 1) THEN
          IF (read3d) THEN
            CALL netread3d(ncid,0,itime,obs_fieldname,nx,ny,nz,dbz3d)
            var_obs = MAXVAL(dbz3d,3)
          ELSE
            CALL netread2d(ncid,0,itime,obs_fieldname,nx,ny,var_obs)
          END IF
        ELSE IF (vfield == 2) THEN
          CALL netread2d(ncid,0,itime,obs_fieldname,nx,ny,var_obs)
          var_obs = var_obs + obs_prev(:,:,nc)
          !WRITE(*,*) 'accumulate ',nt, var_obs(2,2), obs_prev(2,2,nc)
          IF (rainaccum == 0) THEN
            obs_prev(:,:,nc) = 0.0
          ELSE IF (mod(nt-1,rainaccum) == 0) THEN
            obs_prev(:,:,nc) = 0.0
            IF (nt == 1) var_obs = 0.0
          ELSE
            obs_prev(:,:,nc) = var_obs
            CYCLE loop_nt                           ! Assume hours inputs
          END IF
        ELSE IF (vfield == 3) THEN
          fHndl(1,1) = ncid
          CALL get_wrf_3d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'U','X',                        &
                          nx_wrf,ny_wrf,nz_wrf,u,                       &
                          nx_wrf,ny_wrf,nz_wrf,temd,istatus)
          CALL get_wrf_3d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'V','Y',                        &
                          nx_wrf,ny_wrf,nz_wrf,v,                       &
                          nx_wrf,ny_wrf,nz_wrf,temd,istatus)
          CALL get_wrf_3d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'W','Z',                        &
                          nx_wrf,ny_wrf,nz_wrf,w,                       &
                          nx_wrf,ny_wrf,nz_wrf,temd,istatus)
          CALL get_wrf_3d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'PH','Z',                       &
                          nx_wrf,ny_wrf,nz_wrf,ph,                      &
                          nx_wrf,ny_wrf,nz_wrf,temd,istatus)
          CALL get_wrf_3d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'PHB','Z',                      &
                          nx_wrf,ny_wrf,nz_wrf,phb,                     &
                          nx_wrf,ny_wrf,nz_wrf,temd,istatus)
          CALL get_wrf_2d(fHndl,7,.FALSE.,.TRUE.,1,1,                   &
                          datestr,itime,'HGT','0',                      &
                          nx_wrf,ny_wrf,ht,nx_wrf,ny_wrf,temd,istatus)

          CALL compute_diff_metrics( nx_wrf,ny_wrf,nz_wrf, ph, phb, rdx, rdy, &
                                     rdz, rdzw, zx, zy, istatus)

          CALL cal_helicity ( u, v, w,                                  &
                          var_obs,  & !up_heli_max16,                &
                          ph, phb, ht,                                  &
                          msfux, msfuy,msfvx, msfvy,                    &
                          rdx, rdy, dn, dnw, rdz, rdzw,                 &
                          fnm, fnp, cf1, cf2, cf3, zx, zy,              &
                          nx_wrf,ny_wrf,nz_wrf, istatus      )
        END IF

        CALL netclose(ncid)

        IF (nfile <= ncopy) THEN
          !  1    5    0    5    0    5    0
          ! 'wrfout_d01_2018-05-31_00:00:00'
          indx1 = INDEX(fcst_files(1),'wrfout_d',.TRUE.)
          READ(fcst_files(nfile)(indx1+11:indx1+29),'(I4.4,5(a,I2.2))') iyr,ach,imn,ach,idy,ach,ihr,ach,imin,ach,isec
          CALL ctim2abss( iyr,imn,idy,ihr,imin,isec, abstsec0(nc) )
        END IF

        !  1    5    0    5    0    5    0
        ! 'wrfout_d01_2018-05-31_00:00:00'
        indx1 = INDEX(fcst_files(nfile),'wrfout_d',.TRUE.)
        READ(fcst_files(nfile)(indx1+11:indx1+29),'(I4.4,5(a,I2.2))') iyr,ach,imn,ach,idy,ach,ihr,ach,imin,ach,isec
        CALL ctim2abss( iyr,imn,idy,ihr,imin,isec, abstsec )

        timediff(nt) = abstsec - abstsec0(nc)
        !print *, nfile, nt, nc, timediff(nt)

      END IF
      CALL mpbcastra(var_fcst,nx*ny,0)
      CALL mpbcastra(var_obs, nx*ny,0)

      DO n = 1, nthres
        DO r = 1, nradius

           hitl = 0
           fall = 0
           misl = 0
           negl = 0

           !print *, 'ibgn, iend, jbgn, jend:', ibgn, iend, jbgn, jend
           !DO k = kbgn,kend
             DO j = jbgn,jend
               DO i = ibgn,iend

                 ohits = 0 ! save mem space instead od declaring 5d array & allocate ...
                 fhits = 0

                 !DO kk = -vradius(r), vradius(r)
                   DO ij = 1, num_pts(r)

                       im = i+ii(ij,r)
                       jm = j+jj(ij,r)
                       !km = k+kk

                       IF (im < 1 .OR. im > nx) CYCLE
                       IF (jm < 1 .OR. jm > ny) CYCLE
                       !IF (km < 1 .OR. km > nz) CYCLE

                       IF (var_fcst(im,jm) >= thres(n) ) THEN ! this will add upp throuhg all i, j in the end. Save mem space
                          fhits = fhits + 1
                       END IF

                       IF ( var_obs(im,jm) >= thres(n) ) THEN
                          ohits = ohits + 1
                       END IF

                   END DO
                 !END DO

                 IF ( (var_fcst(i,j) >= thres(n) .AND. ohits > 0 ) .OR. &
                      (var_obs(i,j)  >= thres(n) .AND. fhits > 0 )) THEN
                     hitl = hitl + 1
                 END IF

                 IF ( var_obs(i,j) >= thres(n) .AND. fhits == 0 ) THEN
                     misl = misl + 1
                 END IF

                 IF ( var_fcst(i,j) >= thres(n) .AND. ohits == 0        &
                      .AND. var_obs(i,j) > -9000.0 ) THEN     ! to filter out missing obs
                     fall = fall + 1
                 END IF

                 IF ( var_fcst(i,j) < thres(n) .AND.                    &
                      var_obs(i,j)  < thres(n) .AND. var_obs(i,j) > -9000.0 ) THEN
                     negl = negl + 1
                 END IF

               END DO
             END DO
           !END DO

           CALL mpbarrier

           !print *, '1:',myproc, hitl,fall,misl,negl
           CALL mptotali8(hitl)
           CALL mptotali8(fall)
           CALL mptotali8(misl)
           CALL mptotali8(negl)

           !print *, '2:',myproc, hitl,fall,misl,negl

           hits(r,n,nt) = hits(r,n,nt) + hitl
           fals(r,n,nt) = fals(r,n,nt) + fall
           miss(r,n,nt) = miss(r,n,nt) + misl
           cneg(r,n,nt) = cneg(r,n,nt) + negl
        END DO
      END DO

    END DO  ! ncopy

    ! CALCULATE ETS for this time

    IF (myproc == 0) THEN
      DO n = 1, nthres
        DO r = 1, nradius
          chance=((hits(r,n,nt)+fals(r,n,nt))*(hits(r,n,nt)+miss(r,n,nt)))/ &
                 (hits(r,n,nt)+fals(r,n,nt)+miss(r,n,nt)+cneg(r,n,nt))

          working1=(hits(r,n,nt)+fals(r,n,nt)+ miss(r,n,nt)-chance)
          IF (working1 > 0) THEN
            ets(r,n,nt)=(hits(r,n,nt)-chance)*1.0 / working1
          ELSE
            ets(r,n,nt)= 0
          END IF

          IF ((hits(r,n,nt)+miss(r,n,nt)) > 0) THEN
            bias(r,n,nt)=(hits(r,n,nt)+fals(r,n,nt))*1.0 /              &
                       (hits(r,n,nt)+miss(r,n,nt))
          ELSE
            bias(r,n,nt)= 0
          END IF
        END DO
      END DO
    END IF

  END DO loop_nt

  IF (vfield == 1) THEN
    DEALLOCATE(dbz3d)
  ELSE IF(vfield == 2) THEN
    DEALLOCATE(rain_prev, varin, obs_prev)
  ELSE IF(vfield == 3) THEN
    DEALLOCATE(fHndl, temd)
    DEALLOCATE(dn, dnw, fnm, fnp )
    DEALLOCATE(msfux, msfuy, msfvx, msfvy )
    DEALLOCATE(rdz, rdzw, zx, zy )

    DEALLOCATE(ht)
    DEALLOCATE(u, v, w, ph, phb )
  END IF

!-----------------------------------------------------------------------
!
!  Write out ETS & BIAS to TEXT file
!
!-----------------------------------------------------------------------
!
  IF (myproc == 0) THEN

    ntimestep = 1
    ntimeout  = ntime
    IF (vfield == 2 .AND. rainaccum > 0) THEN
      ntimestep = rainaccum
      ntimeout  = ntime/rainaccum+1
    END IF

    IF ( ALL( MOD(timediff,3600) == 0) ) THEN
      timediff = timediff/3600        ! use hour
    ELSE
      timediff = timediff/60          ! use minute
    END IF

    WRITE(*,'(1x,3a)') 'Writing to file "',TRIM(outfile(1)),'" ....'
    OPEN(outunt,FILE=outfile(1),FORM = 'FORMATTED')
    WRITE(outunt,'(3(a,I4))')  'nthres=',nthres, ', ntime=',ntimeout, ', nradius=',nradius
    WRITE(outunt,'(a,20F8.2)') 'thres=', (thres(n),n=1,nthres)
    WRITE(outunt,'(a,20F10.2)') 'radius= ',(radius(r),r=1,nradius)
    !WRITE(outunt,'(a5,a3,20I10)') 'nt',' r=',(r,r=1,nradius)
    DO n = 1,nthres
      DO nt = 1, ntime, ntimestep
        WRITE(outunt,'(I8,20F10.5)') timediff(nt),(ets(r,n,nt),r=1,nradius)
      END DO
    END DO
    CLOSE(outunt)
    WRITE(*,'(1x,3a)') '..... Done.'

    WRITE(*,'(1x,3a)') 'Writing to file "',TRIM(outfile(2)),'" ....'
    OPEN(outunt,FILE=outfile(2),FORM = 'FORMATTED')
    WRITE(outunt,'(3(a,I4))')  'nthres=',nthres, ', ntime=',ntimeout, ', nradius=',nradius
    WRITE(outunt,'(a,20F8.2)') 'thres=', (thres(n),n=1,nthres)
    WRITE(outunt,'(a,20F10.2)') 'radius= ',(radius(r),r=1,nradius)
    !WRITE(outunt,'(a5,a3,20I10)') 'nt',' r=',(r,r=1,nradius)
    DO n = 1,nthres
      DO nt = 1, ntime, ntimestep
        WRITE(outunt,'(I8,20F10.5)') timediff(nt),(bias(r,n,nt),r=1,nradius)
      END DO
    END DO
    CLOSE(outunt)
    WRITE(*,'(1x,3a)') '..... Done.'

    IF (outcontg > 0) THEN
      WRITE(*,'(1x,3a)',ADVANCE='NO') 'Writing to contingency file "',TRIM(contgfile),'" ....'
      OPEN(outunt,FILE=contgfile,FORM = 'FORMATTED')
      WRITE(outunt,'(3(a,I4))')  'nthres=',nthres, ', ntime=',ntimeout, ', nradius=',nradius
      WRITE(outunt,'(a,20F8.2)') 'thres=', (thres(n),n=1,nthres)
      WRITE(outunt,'(a,20F10.2)') 'radius= ',(radius(r),r=1,nradius)
      WRITE(outunt,'(8a)') '  radius  ','threshld','  time  ',' : ','    hits  ','    false ','    miss  ','    cneg  '
      WRITE(outunt,'(8a)') '  --------','  ------','  ------','   ','  --------','  --------','  --------','  --------'
      DO r = 1, nradius
        DO n = 1,nthres
          DO nt = 1, ntime, ntimestep
            WRITE(outunt,'(F10.2,F8.2,I8,a,4I10)')                      &
              radius(r),thres(n),timediff(nt),'   ',                    &
              hits(r,n,nt), fals(r,n,nt),miss(r,n,nt),cneg(r,n,nt)
          END DO
      END DO
      END DO
      CLOSE(outunt)
      WRITE(*,'(1x,3a)') '..... Done.'

    END IF

  END IF

  CALL mpbarrier
  CALL arpsstop('===== Normal Termination =====',0)
END PROGRAM neighbor_ets
