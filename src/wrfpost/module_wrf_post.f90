MODULE module_wrf_post
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!  PURPOSE: Module that connects WRF data with ARPS post-processing
!           subroutines.
!
!-----------------------------------------------------------------------
!
!  AUTHOR:  Yunheng Wang (04/22/2013)
!  It was separated from the main program wrf2arps.f90 to avoid too long
!  source file for compilation optimization. It is also reorganized
!  for modularization.
!
!  MODIFICATION HISTORY:
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  IMPLICIT NONE

  LOGICAL :: use_external_grid            ! Use WRF grid to avoid interpolation

  !-------------- on ARPS grid -----------------------------------------
  REAL,    POINTER     :: t2m   (:,:)
  REAL,    POINTER     :: th2m  (:,:)
  REAL,    POINTER     :: qv2m  (:,:)
  REAL,    POINTER     :: u10m  (:,:)
  REAL,    POINTER     :: v10m  (:,:)

  REAL,    POINTER     :: raing(:,:)
  REAL,    POINTER     :: rainc(:,:)
  REAL,    POINTER     :: rainsnow(:,:)
  REAL,    POINTER     :: raingrpl(:,:)
  REAL,    POINTER     :: rainhail(:,:)

  REAL,    POINTER     :: raddn (:,:)     ! sfc total downward radiation flux (W/m*2)

  !-------------------- On External grid -------------------------------

  REAL,    POINTER     :: psfc_ext(:,:)
  REAL,    POINTER     :: t2m_ext (:,:)
  REAL,    POINTER     :: th2m_ext(:,:)
  REAL,    POINTER     :: qv2m_ext(:,:)
  REAL,    POINTER     :: u10m_ext(:,:)
  REAL,    POINTER     :: v10m_ext(:,:)

  REAL,    POINTER     :: raing_ext   (:,:)
                          ! PBL time-step total precipitation (mm)
                          ! whether is it "raing" in ARPS????
  REAL,    POINTER     :: rainc_ext   (:,:)
                          ! time-step cumulus precipitation (mm)
  REAL,    POINTER     :: raingrpl_ext(:,:)
  REAL,    POINTER     :: rainsnow_ext(:,:)
  REAL,    POINTER     :: rainhail_ext(:,:)

  REAL,    POINTER     :: raddn_ext (:,:)

  !---------------------- Flags and Private variables ------------------

  INTEGER, PRIVATE :: nx, ny, nx_ext, ny_ext

  !---------------------- PRIVATE SUBPROCEDURE ------------------

  PRIVATE :: get_wrf_rain, print_maxmin_rain

CONTAINS

  !#####################################################################

  SUBROUTINE init_data2d(use_org_grid, istatus)

  !---------------------------------------------------------------------

    INTEGER, INTENT(IN)  :: use_org_grid

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    use_external_grid = (use_org_grid == 1)

    RETURN
  END SUBROUTINE init_data2d

  !#####################################################################

  SUBROUTINE allocate_data2d_arps(nxin,nyin,istatus)

    INTEGER, INTENT(IN)  :: nxin, nyin
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    nx = nxin
    ny = nyin

    ALLOCATE( t2m(nx,ny),           STAT = istatus )
    ALLOCATE( th2m(nx,ny),          STAT = istatus )
    ALLOCATE( qv2m(nx,ny),          STAT = istatus )
    ALLOCATE( u10m(nx,ny),          STAT = istatus )
    ALLOCATE( v10m(nx,ny),          STAT = istatus )
    ALLOCATE( raddn(nx,ny),         STAT = istatus )

    ALLOCATE( raing(nx,ny),         STAT = istatus )
    ALLOCATE( rainc(nx,ny),         STAT = istatus )
    ALLOCATE( rainsnow(nx,ny),         STAT = istatus )
    ALLOCATE( raingrpl(nx,ny),         STAT = istatus )
    ALLOCATE( rainhail(nx,ny),         STAT = istatus )

    t2m = 0.0
    th2m = 0.0
    qv2m = 0.0
    u10m = 0.0
    v10m = 0.0

    raddn = 0.0

    raing = 0.0
    rainc = 0.0
    rainsnow = 0.0
    raingrpl = 0.0
    rainhail = 0.0

    RETURN
  END SUBROUTINE allocate_data2d_arps

  !#####################################################################

  SUBROUTINE allocate_data2d_ext(nxin,nyin, istatus)

    INTEGER, INTENT(IN)  :: nxin, nyin
    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    nx_ext = nxin
    ny_ext = nyin

    ALLOCATE(psfc_ext     (nx_ext,ny_ext),stat=istatus)
    ALLOCATE(t2m_ext      (nx_ext,ny_ext),stat=istatus)
    ALLOCATE(th2m_ext     (nx_ext,ny_ext),stat=istatus)
    ALLOCATE(qv2m_ext     (nx_ext,ny_ext),stat=istatus)
    ALLOCATE(u10m_ext     (nx_ext,ny_ext),stat=istatus)
    ALLOCATE(v10m_ext     (nx_ext,ny_ext),stat=istatus)

    ALLOCATE(raing_ext    (nx_ext,ny_ext), STAT = istatus)
    ALLOCATE(rainc_ext    (nx_ext,ny_ext), STAT = istatus)
    ALLOCATE(rainsnow_ext (nx_ext,ny_ext), STAT = istatus)
    ALLOCATE(raingrpl_ext (nx_ext,ny_ext), STAT = istatus)
    ALLOCATE(rainhail_ext (nx_ext,ny_ext), STAT = istatus)

    ALLOCATE(raddn_ext    (nx_ext,ny_ext),stat=istatus)

    psfc_ext  = 0.0

    raddn_ext = 0.0
    t2m_ext   = 0.0
    th2m_ext  = 0.0
    qv2m_ext  = 0.0
    u10m_ext  = 0.0
    v10m_ext  = 0.0

    raing_ext = 0.0
    rainc_ext = 0.0
    rainsnow_ext = 0.0
    raingrpl_ext = 0.0
    rainhail_ext = 0.0


    RETURN
  END SUBROUTINE allocate_data2d_ext

  !##################################################################
  !
  SUBROUTINE get_wrf_data2d(fHndl,io_form,multifile,ncmprx,ncmpry,itime,timestr, &
                            tem2d1,istatus)

  !------------------------------------------------------------------------
  !
  !  PURPOSE:
  !
  !  Read in WRF 2D data in one time level (itime) from the opened NetCDF
  !  file (fHndl) and convert those data to ARPS units if needed.
  !
  !------------------------------------------------------------------------
    IMPLICIT NONE

  !------------------------------------------------------------------------
  !
  !  Include files
  !
  !------------------------------------------------------------------

    INCLUDE 'mp.inc'

  !------------------------------------------------------------------------
  !
  !  Variable declaration
  !
  !------------------------------------------------------------------

    INTEGER,      INTENT(IN)  :: ncmprx, ncmpry
    INTEGER,      INTENT(IN)  :: fHndl(ncmprx,ncmpry)
    INTEGER,      INTENT(IN)  :: io_form      ! 5 - PHDF5
                                              ! 7 - NetCDF
    LOGICAL,      INTENT(IN)  :: multifile
    INTEGER,      INTENT(IN)  :: itime   ! Recorder number
    CHARACTER(*), INTENT(IN)  :: timestr

    REAL,         INTENT(INOUT) :: tem2d1 (nx_ext,  ny_ext)

    INTEGER,      INTENT(OUT) :: istatus

  !------------------------------------------------------------------------
  !
  !  Misc. local variables
  !
  !------------------------------------------------------------------

    INTEGER  :: nxlg_ext, nylg_ext
    INTEGER  :: nxd, nyd           ! dimensions in data file
    INTEGER  :: nxd_stag, nyd_stag

    REAL, ALLOCATABLE :: temlg1(:,:)
    REAL, ALLOCATABLE :: temlg2(:,:)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

  !-----------------------------------------------------------------------
  !
  ! Allocate working arrays
  !
  !-----------------------------------------------------------------------

    IF (use_external_grid) THEN
      nxlg_ext = (nx_ext-1)*nproc_x + 1
      nylg_ext = (ny_ext-1)*nproc_y + 1
    ELSE
      nxlg_ext = nx_ext
      nylg_ext = ny_ext
    END IF

    IF (mp_opt > 0 .AND. multifile) THEN
      nxd = (nx_ext - 1)/ncmprx
      nyd = (ny_ext - 1)/ncmpry
      nxd_stag = nxd
      nyd_stag = nyd
      IF (loc_x == nproc_x) nxd_stag = nxd + 1
      IF (loc_y == nproc_y) nyd_stag = nyd + 1

      ALLOCATE(temlg2(1,1),   STAT = istatus)  ! do not use it
    ELSE
      nxd = nxlg_ext - 1
      nyd = nylg_ext - 1
      nxd_stag = nxlg_ext
      nyd_stag = nylg_ext

      ALLOCATE(temlg2(nxlg_ext, nylg_ext ),  STAT = istatus)
    END IF

    ALLOCATE(  temlg1(nxd_stag, nyd_stag ),  STAT = istatus)

  !---------------------------------------------------------------------
  !
  ! Get Near-Sfc variables
  !
  !---------------------------------------------------------------------


    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,                &
                    timestr,itime,1,'Q2','',                              &
                    'west_east','south_north',nx_ext,ny_ext,qv2m_ext(:,:),&
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,                &
                    timestr,itime,1,'T2','',                              &
                    'west_east','south_north',nx_ext,ny_ext,t2m_ext(:,:), &
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,                &
                    timestr,itime,1,'TH2','',                             &
                    'west_east','south_north',nx_ext,ny_ext,th2m_ext(:,:),&
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,                &
                    timestr,itime,1,'PSFC','',                            &
                    'west_east','south_north',nx_ext,ny_ext,psfc_ext(:,:),&
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,                &
                    timestr,itime,1,'U10','',                             &
                    'west_east','south_north',nx_ext,ny_ext,u10m_ext(:,:),&
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,                &
                    timestr,itime,1,'V10','',                             &
                    'west_east','south_north',nx_ext,ny_ext,v10m_ext(:,:),&
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

  !
  !-----------------------------------------------------------------------
  !
  ! Downward radiation flux at surface
  !
  !-----------------------------------------------------------------------

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,              &
                    timestr,itime,1,'SWDOWN','',                        &
                    'west_east','south_north',nx_ext,ny_ext,tem2d1,     &
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,              &
                    timestr,itime,1,'GLW','',                           &
                    'west_east','south_north',nx_ext,ny_ext,raddn_ext,  &
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

    raddn_ext = raddn_ext + tem2d1   ! Total downward radiation flux


  !
  !WDT 2004-01-10 GMB: switch from time-step precipitation to accumulated total precip:
  !from ncdump -h:
  ! RAINC:description = "ACCUMULATED TOTAL CUMULUS PRECIPITATION" ;
  ! RAINNC:description = "ACCUMULATED TOTAL GRID SCALE PRECIPITATION" ;
  ! RAINBL:description = "PBL TIME-STEP TOTAL PRECIPITATION" ;
  ! RAINCV:description = "TIME-STEP CUMULUS PRECIPITATION" ;
  !
  !---------------------------------------------------------------------
  !
  ! Cumulus total precipitation
  !
  !---------------------------------------------------------------------

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,              &
                    timestr,itime,1,'RAINC','',                         &
                    'west_east','south_north',nx_ext,ny_ext,rainc_ext,  &
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

  !---------------------------------------------------------------------
  !
  ! Grid scale total precipitation
  !
  !---------------------------------------------------------------------

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,              &
                    timestr,itime,1,'RAINNC','',                        &
                    'west_east','south_north',nx_ext,ny_ext,raing_ext,  &
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,              &
                    timestr,itime,1,'SNOWNC','',                        &
                    'west_east','south_north',nx_ext,ny_ext,rainsnow_ext(:,:),&
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)


    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,              &
                    timestr,itime,1,'GRAUPELNC','',                     &
                    'west_east','south_north',nx_ext,ny_ext,raingrpl_ext(:,:),&
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,              &
                    timestr,itime,1,'HAILNC','',                        &
                    'west_east','south_north',nx_ext,ny_ext,rainhail_ext(:,:),&
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

  !-----------------------------------------------------------------------
  !
  ! Deallocate the working arrays
  !
  !-----------------------------------------------------------------------

    DEALLOCATE( temlg1, temlg2 )


    RETURN
  END SUBROUTINE get_wrf_data2d

  !##################################################################

  SUBROUTINE get_wrf_rain(fHndl,io_form,multifile,ncmprx,ncmpry,itime,timestr,  &
                          istatus)

  !------------------------------------------------------------------------
  !
  !  PURPOSE:
  !
  !  Read in precipitation related fields in one time level (itime) from
  !  the opened NetCDF file (fHndl)
  !
  !------------------------------------------------------------------------
  !
  !  AUTHOR:
  !  Fanyou Kong (04/07/2007)
  !
  !  MODIFICATION HISTORY:
  !
  !  04/12/2012 (Fanyou Kong)
  !  Add raingrpl_ext and rainhail_ext
  !
  !  04/18/2013 (Fanyou Kong)
  !  Add rainsnow_ext
  !
  !  04/22/2013 (Y. Wang)
  !  Moved to this module and renamed from getwrfrain.
  !
  !------------------------------------------------------------------------
    IMPLICIT NONE

    INTEGER,      INTENT(IN)  :: ncmprx, ncmpry
    INTEGER,      INTENT(IN)  :: fHndl(ncmprx,ncmpry)
    INTEGER,      INTENT(IN)  :: io_form      ! 5 - PHDF5
                                              ! 7 - NetCDF
    LOGICAL,      INTENT(IN)  :: multifile
    INTEGER,      INTENT(IN)  :: itime   ! Recorder number
    CHARACTER(*), INTENT(IN)  :: timestr

    INTEGER,      INTENT(OUT) :: istatus

  !------------------------------------------------------------------------
  !
  !  Include files
  !
  !------------------------------------------------------------------

    INCLUDE 'mp.inc'

  !------------------------------------------------------------------------
  !
  !  Misc. local variables
  !
  !------------------------------------------------------------------

    INTEGER  :: nxlg_ext, nylg_ext
    INTEGER  :: nxd, nyd, nzd            ! dimensions in data file
    INTEGER  :: nxd_stag, nyd_stag, nzd_stag

    REAL, ALLOCATABLE :: temlg1(:,:)
    REAL, ALLOCATABLE :: temlg2(:,:)


  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

  !-----------------------------------------------------------------------
  !
  ! Allocate working arrays
  !
  !-----------------------------------------------------------------------

    IF (use_external_grid ) THEN
      nxlg_ext = (nx_ext-1)*nproc_x + 1
      nylg_ext = (ny_ext-1)*nproc_y + 1
    ELSE
      nxlg_ext = nx_ext
      nylg_ext = ny_ext
    END IF

    IF (mp_opt > 0 .AND. multifile) THEN
      nxd = (nx_ext - 1)/ncmprx
      nyd = (ny_ext - 1)/ncmpry
      nxd_stag = nxd
      nyd_stag = nyd
      IF (loc_x == nproc_x) nxd_stag = nxd + 1
      IF (loc_y == nproc_y) nyd_stag = nyd + 1

      ALLOCATE(temlg2(1,1),   STAT = istatus)  ! do not use it
    ELSE
      nxd = nxlg_ext - 1
      nyd = nylg_ext - 1
      nxd_stag = nxlg_ext
      nyd_stag = nylg_ext

      ALLOCATE(temlg2(nxlg_ext,nylg_ext),   STAT = istatus)
    END IF

    ALLOCATE(temlg1(nxd_stag, nyd_stag), STAT = istatus)

  !
  !WDT 2004-01-10 GMB: switch from time-step precipitation to accumulated total precip:
  !from ncdump -h:
  ! RAINC:description = "ACCUMULATED TOTAL CUMULUS PRECIPITATION" ;
  ! RAINNC:description = "ACCUMULATED TOTAL GRID SCALE PRECIPITATION" ;
  ! RAINBL:description = "PBL TIME-STEP TOTAL PRECIPITATION" ;
  ! RAINCV:description = "TIME-STEP CUMULUS PRECIPITATION" ;
  !
  !-----------------------------------------------------------------------
  !
  ! Cumulus total precipitation
  !
  !-----------------------------------------------------------------------

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,                &
                    timestr,itime,1,'RAINC','',                           &
                    'west_east','south_north',nx_ext,ny_ext,rainc_ext,    &
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

  !-----------------------------------------------------------------------
  !
  ! Grid scale total precipitation
  !
  !-----------------------------------------------------------------------

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,                &
                    timestr,itime,1,'RAINNC','',                          &
                    'west_east','south_north',nx_ext,ny_ext,raing_ext,    &
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)
  !
  !-----------------------------------------------------------------------
  !
  ! Grid scale total snow precipitation
  !
  !-----------------------------------------------------------------------

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,                &
                    timestr,itime,1,'SNOWNC','',                          &
                    'west_east','south_north',nx_ext,ny_ext,rainsnow_ext, &
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)
  !
  !-----------------------------------------------------------------------
  !
  ! Grid scale total graupel precipitation
  !
  !-----------------------------------------------------------------------

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,                &
                    timestr,itime,1,'GRAUPELNC','',                       &
                    'west_east','south_north',nx_ext,ny_ext,raingrpl_ext, &
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)
  !
  !-----------------------------------------------------------------------
  !
  ! Grid scale total hail precipitation
  !
  !-----------------------------------------------------------------------

    CALL get_wrf_2d(fHndl,io_form,multifile,ncmprx,ncmpry,                &
                    timestr,itime,1,'HAILNC','',                          &
                    'west_east','south_north',nx_ext,ny_ext,rainhail_ext, &
                    nxd,nyd,temlg1,nxlg_ext,nylg_ext,temlg2,istatus)

  !-----------------------------------------------------------------------
  !
  ! Deallocate the working arrays
  !
  !-----------------------------------------------------------------------

    DEALLOCATE(temlg1,temlg2)

    RETURN
  END SUBROUTINE get_wrf_rain

  !#####################################################################

!  SUBROUTINE make_data2d(iorder,iscl,jscl,x_ext,y_ext,     &
!                         xs2d,ys2d,dxfld,dyfld,rdxfld,rdyfld,           &
!                         tem1_ext,istatus)
!
!    INTEGER, INTENT(IN)    :: iorder
!    REAL,    INTENT(IN)    :: x_ext(nx_ext),y_ext(ny_ext)
!    REAL,    INTENT(IN)    :: xs2d(nx,ny), ys2d(nx,ny)  ! arps point coordinate in external grid
!    INTEGER, INTENT(IN)    :: iscl(nx,ny), jscl(nx,ny)  ! arps point indices in external array
!    REAL,    INTENT(IN)    :: dxfld(nx_ext), dyfld(ny_ext)
!    REAL,    INTENT(IN)    :: rdxfld(nx_ext),rdyfld(ny_ext)
!    REAL,    INTENT(INOUT) :: tem1_ext(nx_ext,ny_ext,4)
!
!    INTEGER, INTENT(OUT)   :: istatus
!
!  !---------------------------------------------------------------------
!
!    INTEGER :: i, j
!
!  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!    istatus = 0
!
!    IF( .NOT. use_external_grid)  THEN
!
!      CALL mkarps2d(nx_ext,ny_ext,nx,ny,                              &
!                    iorder,iscl,jscl,x_ext,y_ext,                     &
!                    xs2d,ys2d,t2m_ext,t2m,                            &
!                    dxfld,dyfld,rdxfld,rdyfld,                        &
!                    tem1_ext(:,:,1),tem1_ext(:,:,2),                  &
!                    tem1_ext(:,:,3),tem1_ext(:,:,4))
!      CALL mkarps2d(nx_ext,ny_ext,nx,ny,                              &
!                    iorder,iscl,jscl,x_ext,y_ext,                     &
!                    xs2d,ys2d,th2m_ext,th2m,                          &
!                    dxfld,dyfld,rdxfld,rdyfld,                        &
!                    tem1_ext(:,:,1),tem1_ext(:,:,2),                  &
!                    tem1_ext(:,:,3),tem1_ext(:,:,4))
!      CALL mkarps2d(nx_ext,ny_ext,nx,ny,                              &
!                    iorder,iscl,jscl,x_ext,y_ext,                     &
!                    xs2d,ys2d,qv2m_ext,qv2m,                          &
!                    dxfld,dyfld,rdxfld,rdyfld,                        &
!                    tem1_ext(:,:,1),tem1_ext(:,:,2),                  &
!                    tem1_ext(:,:,3),tem1_ext(:,:,4))
!      CALL mkarps2d(nx_ext,ny_ext,nx,ny,                              &
!                    iorder,iscl,jscl,x_ext,y_ext,                     &
!                    xs2d,ys2d,u10m_ext,u10m,                          &
!                    dxfld,dyfld,rdxfld,rdyfld,                        &
!                    tem1_ext(:,:,1),tem1_ext(:,:,2),                  &
!                    tem1_ext(:,:,3),tem1_ext(:,:,4))
!      CALL mkarps2d(nx_ext,ny_ext,nx,ny,                              &
!                    iorder,iscl,jscl,x_ext,y_ext,                     &
!                    xs2d,ys2d,v10m_ext,v10m,                          &
!                    dxfld,dyfld,rdxfld,rdyfld,                        &
!                    tem1_ext(:,:,1),tem1_ext(:,:,2),                  &
!                    tem1_ext(:,:,3),tem1_ext(:,:,4))
!      CALL mkarps2d(nx_ext,ny_ext,nx,ny,                              &
!                    iorder,iscl,jscl,x_ext,y_ext,                     &
!                    xs2d,ys2d,raddn_ext,raddn,                        &
!                    dxfld,dyfld,rdxfld,rdyfld,                        &
!                    tem1_ext(:,:,1),tem1_ext(:,:,2),                  &
!                    tem1_ext(:,:,3),tem1_ext(:,:,4))
!
!
!      !  Processing PBL grid scale precipitation
!
!      !IF(rainout == 1) THEN
!
!        CALL mkarps2d(nx_ext,ny_ext,nx,ny,                              &
!                       iorder,iscl,jscl,x_ext,y_ext,                    &
!                       xs2d,ys2d,raing_ext,raing,                       &
!                       dxfld,dyfld,rdxfld,rdyfld,                       &
!                       tem1_ext(:,:,1),tem1_ext(:,:,2),                 &
!                       tem1_ext(:,:,3),tem1_ext(:,:,4))
!
!        CALL mkarps2d(nx_ext,ny_ext,nx,ny,                              &
!                      iorder,iscl,jscl,x_ext,y_ext,                     &
!                      xs2d,ys2d,rainc_ext,rainc,                        &
!                      dxfld,dyfld,rdxfld,rdyfld,                        &
!                      tem1_ext(:,:,1),tem1_ext(:,:,2),                  &
!                      tem1_ext(:,:,3),tem1_ext(:,:,4))
!
!        CALL mkarps2d(nx_ext,ny_ext,nx,ny,                              &
!                      iorder,iscl,jscl,x_ext,y_ext,                     &
!                      xs2d,ys2d,rainsnow_ext,rainsnow,                  &
!                      dxfld,dyfld,rdxfld,rdyfld,                        &
!                      tem1_ext(:,:,1),tem1_ext(:,:,2),                  &
!                      tem1_ext(:,:,3),tem1_ext(:,:,4))
!
!        CALL mkarps2d(nx_ext,ny_ext,nx,ny,                              &
!                      iorder,iscl,jscl,x_ext,y_ext,                     &
!                      xs2d,ys2d,raingrpl_ext,raingrpl,                  &
!                      dxfld,dyfld,rdxfld,rdyfld,                        &
!                      tem1_ext(:,:,1),tem1_ext(:,:,2),                  &
!                      tem1_ext(:,:,3),tem1_ext(:,:,4))
!
!        CALL mkarps2d(nx_ext,ny_ext,nx,ny,                              &
!                      iorder,iscl,jscl,x_ext,y_ext,                     &
!                      xs2d,ys2d,rainhail_ext,rainhail,                  &
!                      dxfld,dyfld,rdxfld,rdyfld,                        &
!                      tem1_ext(:,:,1),tem1_ext(:,:,2),                  &
!                      tem1_ext(:,:,3),tem1_ext(:,:,4))
!      !END IF
!
!    ELSE
!      DO j = 2,ny-2
!        DO i = 2,nx-2
!          t2m(i,j)      = t2m_ext(i-1,j-1)
!          th2m(i,j)     = th2m_ext(i-1,j-1)
!          qv2m(i,j)     = qv2m_ext(i-1,j-1)
!          u10m(i,j)     = u10m_ext(i-1,j-1)
!          v10m(i,j)     = v10m_ext(i-1,j-1)
!          raddn(i,j)    = raddn_ext(i-1,j-1)
!
!          raing(i,j)    = raing_ext(i-1,j-1)
!          rainc(i,j)    = rainc_ext(i-1,j-1)
!          rainsnow(i,j) = rainsnow_ext(i-1,j-1)
!          raingrpl(i,j) = raingrpl_ext(i-1,j-1)
!          rainhail(i,j) = rainhail_ext(i-1,j-1)
!
!        END DO
!      END DO
!      CALL edgfill(t2m,      1,nx,2,nx-2,1,ny,2,ny-2,1,1,1,1)
!      CALL edgfill(th2m,     1,nx,2,nx-2,1,ny,2,ny-2,1,1,1,1)
!      CALL edgfill(qv2m,     1,nx,2,nx-2,1,ny,2,ny-2,1,1,1,1)
!      CALL edgfill(u10m,     1,nx,2,nx-2,1,ny,2,ny-2,1,1,1,1)
!      CALL edgfill(v10m,     1,nx,2,nx-2,1,ny,2,ny-2,1,1,1,1)
!      CALL edgfill(raddn,    1,nx,2,nx-2,1,ny,2,ny-2,1,1,1,1)
!
!      CALL edgfill(raing,    1,nx,2,nx-2,1,ny,2,ny-2,1,1,1,1)
!      CALL edgfill(rainc,    1,nx,2,nx-2,1,ny,2,ny-2,1,1,1,1)
!      CALL edgfill(rainsnow, 1,nx,2,nx-2,1,ny,2,ny-2,1,1,1,1)
!      CALL edgfill(raingrpl, 1,nx,2,nx-2,1,ny,2,ny-2,1,1,1,1)
!      CALL edgfill(rainhail, 1,nx,2,nx-2,1,ny,2,ny-2,1,1,1,1)
!
!    END IF
!
!    raing(:,:)    = max(raing(:,:),    0.0)
!    rainc(:,:)    = max(rainc(:,:),    0.0)
!    rainsnow(:,:) = max(rainsnow(:,:), 0.0)
!    raingrpl(:,:) = max(raingrpl(:,:), 0.0)
!    rainhail(:,:) = max(rainhail(:,:), 0.0)
!
!    RETURN
!  END SUBROUTINE make_data2d
!
  !#####################################################################

  SUBROUTINE print_maxmin_data2d_arps( myproc, istatus )
    INTEGER, INTENT(IN)  :: myproc
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------
    REAL :: amax, amin

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    CALL a3dmax0(t2m  ,1,nx,1,nx-1,1,ny,1,ny-1,                       &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                        &
                 't2m_min = ', amin,', t2m_max =',amax
    CALL a3dmax0(th2m  ,1,nx,1,nx-1,1,ny,1,ny-1,                      &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                        &
                 'th2m_min= ', amin,', th2m_max=',amax
    CALL a3dmax0(qv2m  ,1,nx,1,nx-1,1,ny,1,ny-1,                      &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                        &
                 'qv2m_min= ', amin,', qv2m_max=',amax
    CALL a3dmax0(u10m  ,1,nx,1,nx-1,1,ny,1,ny-1,                      &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                        &
                 'u10m_min= ', amin,', u10m_max=',amax
    CALL a3dmax0(v10m  ,1,nx,1,nx-1,1,ny,1,ny-1,                      &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                        &
                 'v10m_min= ', amin,', v10m_max=',amax

    CALL a3dmax0(raing  ,1,nx,1,nx-1,1,ny,1,ny-1,                     &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                        &
                 'raing_min= ', amin,', raing_max=',amax
    CALL a3dmax0(rainc  ,1,nx,1,nx-1,1,ny,1,ny-1,                     &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                        &
                 'rainc_min= ', amin,', rainc_max=',amax
    CALL a3dmax0(rainsnow  ,1,nx,1,nx-1,1,ny,1,ny-1,                  &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                        &
                 'rainsnow_min= ', amin,', rainsnow_max=',amax
    CALL a3dmax0(raingrpl  ,1,nx,1,nx-1,1,ny,1,ny-1,                  &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                        &
                 'raingrpl_min= ', amin,', raingrpl_max=',amax
    CALL a3dmax0(rainhail  ,1,nx,1,nx-1,1,ny,1,ny-1,                  &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                        &
                 'rainhail_min= ', amin,', rainhail_max=',amax

    CALL a3dmax0(raddn  ,1,nx,1,nx-1,1,ny,1,ny-1,                     &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                        &
                 'raddn_min= ', amin,', raddn_max=',amax

    RETURN
  END SUBROUTINE print_maxmin_data2d_arps

  !#####################################################################

  SUBROUTINE print_maxmin_data2d_ext( myproc, istatus )
    INTEGER, INTENT(IN)  :: myproc
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------
    REAL :: amax, amin

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    CALL a3dmax0(raddn_ext  ,1,nx_ext,1,nx_ext,1,ny_ext,1,ny_ext,       &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'raddn_ext_min   = ', amin,', raddn_ext_max   = ',amax
    CALL a3dmax0(t2m_ext  ,1,nx_ext,1,nx_ext,1,ny_ext,1,ny_ext,         &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 't2m_ext_min    = ', amin,', t2m_ext_max    = ',amax
    CALL a3dmax0(th2m_ext  ,1,nx_ext,1,nx_ext,1,ny_ext,1,ny_ext,        &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'th2m_ext_min   = ', amin,', th2m_ext_max   = ',amax
    CALL a3dmax0(qv2m_ext  ,1,nx_ext,1,nx_ext,1,ny_ext,1,ny_ext,        &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'qv2m_ext_min   = ', amin,', qv2m_ext_max   = ',amax
    CALL a3dmax0(u10m_ext  ,1,nx_ext,1,nx_ext,1,ny_ext,1,ny_ext,        &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'u10m_ext_min   = ', amin,', u10m_ext_max   = ',amax
    CALL a3dmax0(v10m_ext  ,1,nx_ext,1,nx_ext,1,ny_ext,1,ny_ext,        &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'v10m_ext_min   = ', amin,', v10m_ext_max   = ',amax

    CALL a3dmax0(raing_ext  ,1,nx_ext,1,nx_ext,1,ny_ext,1,ny_ext,       &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'raing_ext_min   = ', amin,', raing_ext_max   = ',amax
    CALL a3dmax0(rainc_ext  ,1,nx_ext,1,nx_ext,1,ny_ext,1,ny_ext,       &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'rainc_ext_min   = ', amin,', rainc_ext_max   = ',amax
    CALL a3dmax0(rainsnow_ext  ,1,nx_ext,1,nx_ext,1,ny_ext,1,ny_ext,    &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'rainsnow_ext_min   = ', amin,', rainsnow_ext_max   = ',amax
    CALL a3dmax0(raingrpl_ext  ,1,nx_ext,1,nx_ext,1,ny_ext,1,ny_ext,    &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'raingrpl_ext_min   = ', amin,', raingrpl_ext_max   = ',amax
    CALL a3dmax0(rainhail_ext  ,1,nx_ext,1,nx_ext,1,ny_ext,1,ny_ext,    &
                 1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'rainhail_ext_min   = ', amin,', rainhail_ext_max   = ',amax

    RETURN
  END SUBROUTINE print_maxmin_data2d_ext

  !#####################################################################

  SUBROUTINE print_maxmin_rain( myproc, nxin, nyin, message, affix,     &
                            varg, varc, vars, varp, varh,               &
                            istatus )
  !---------------------------------------------------------------------

    INTEGER, INTENT(IN)  :: myproc
    INTEGER, INTENT(IN)  :: nxin, nyin
    CHARACTER(LEN=*), INTENT(IN) :: message ! message beforehand
    CHARACTER(LEN=*), INTENT(IN) :: affix   ! variable affix
    REAL,    INTENT(IN)  :: varg(nxin,nyin)
    REAL,    INTENT(IN)  :: varc(nxin,nyin)
    REAL,    INTENT(IN)  :: vars(nxin,nyin)
    REAL,    INTENT(IN)  :: varp(nxin,nyin)
    REAL,    INTENT(IN)  :: varh(nxin,nyin)

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------
    REAL :: amax, amin

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (myproc == 0) WRITE(6,'(1x,a)') TRIM(message)

    CALL a3dmax0(varg,1,nxin,1,nxin,1,nyin,1,nyin,1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'raing_'//TRIM(affix)//'_min   = ', amin,              &
               ', raing_'//TRIM(affix)//'_max   = ', amax

    CALL a3dmax0(varc,1,nxin,1,nxin,1,nyin,1,nyin,1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'rainc_'//TRIM(affix)//'_min   = ', amin,              &
               ', rainc_'//TRIM(affix)//'_max   = ', amax

    CALL a3dmax0(vars,1,nxin,1,nxin,1,nyin,1,nyin,1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'rainsnow_'//TRIM(affix)//'_min   = ', amin,           &
               ', rainsnow_'//TRIM(affix)//'_max   = ', amax

    CALL a3dmax0(varp,1,nxin,1,nxin,1,nyin,1,nyin,1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'raingrpl_'//TRIM(affix)//'_min   = ', amin,           &
               ', raingrpl_'//TRIM(affix)//'_max   = ', amax

    CALL a3dmax0(varh,1,nxin,1,nxin,1,nyin,1,nyin,1,1,1,1,amax,amin)
    IF(myproc == 0) WRITE(6,'(1x,2(a,e13.6))')                          &
                 'rainhail_'//TRIM(affix)//'_min   = ', amin,           &
               ', rainhail_'//TRIM(affix)//'_max   = ', amax

    RETURN
  END SUBROUTINE print_maxmin_rain

  !#####################################################################

  SUBROUTINE deallocate_data2d_arps( istatus )
    INTEGER, INTENT(OUT) :: istatus
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    DEALLOCATE( t2m,           STAT = istatus )
    DEALLOCATE( th2m,          STAT = istatus )
    DEALLOCATE( qv2m,          STAT = istatus )
    DEALLOCATE( u10m,          STAT = istatus )
    DEALLOCATE( v10m,          STAT = istatus )
    DEALLOCATE( raddn,         STAT = istatus )

    DEALLOCATE( raing,         STAT = istatus )
    DEALLOCATE( rainc,         STAT = istatus )
    DEALLOCATE( rainsnow,      STAT = istatus )
    DEALLOCATE( raingrpl,      STAT = istatus )
    DEALLOCATE( rainhail,      STAT = istatus )

    RETURN
  END SUBROUTINE deallocate_data2d_arps

  !#####################################################################

  SUBROUTINE deallocate_data2d_ext( istatus )
    INTEGER, INTENT(OUT) :: istatus
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    DEALLOCATE(psfc_ext,     STAT=istatus)
    DEALLOCATE(t2m_ext ,     STAT=istatus)
    DEALLOCATE(th2m_ext,     STAT=istatus)
    DEALLOCATE(qv2m_ext,     STAT=istatus)
    DEALLOCATE(u10m_ext,     STAT=istatus)
    DEALLOCATE(v10m_ext,     STAT=istatus)

    DEALLOCATE(raing_ext,         STAT=istatus)
    DEALLOCATE(rainc_ext,         STAT=istatus)
    DEALLOCATE(rainsnow_ext,      STAT=istatus)
    DEALLOCATE(raingrpl_ext,      STAT=istatus)
    DEALLOCATE(rainhail_ext,      STAT=istatus)

    DEALLOCATE(raddn_ext,         STAT=istatus)

    RETURN
  END SUBROUTINE deallocate_data2d_ext

END MODULE module_wrf_post
