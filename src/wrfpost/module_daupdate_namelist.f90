!
!##################################################################
!##################################################################
!######                                                      ######
!######                   PROGRAM WRFDAUPDATE                ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
!-----------------------------------------------------------------------
!
! AUTHOR: Y. Wang (10/23/2013)
!   Rewrote for the ARPS system by referring "da_update_bc.f90" in the
!   WRFDA package.
!
! MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
MODULE wrf_daupdate_namelist

  IMPLICIT NONE

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%                                                                   %%
!%% This module contains namelist variables and handle namelist IO.   %%
!%%                                                                   %%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  CHARACTER(LEN=256) :: da_file, wrf_input_in, wrf_td_in
  CHARACTER(LEN=256) :: wrf_bdy_in
  INTEGER            :: domain_id
  INTEGER            :: nproc_dax_in,  nproc_day_in
  INTEGER            :: nproc_inx_in,  nproc_iny_in
  INTEGER            :: nproc_tdx_in,  nproc_tdy_in
  !INTEGER            :: ibgn, iend, jbgn, jend
  CHARACTER(LEN=19)  :: end_time_str

  LOGICAL :: update_lateral_bdy, update_input, update_input_copy_td
  LOGICAL :: define_bdy, copy_input, use_input_w

  INTEGER ::           perturb_ic,          perturb_lbc
  CHARACTER(LEN=19) :: perturb_ic_vars(20), perturb_lbc_vars(20)
  REAL    ::           perturb_ic_sd(20),   perturb_lbc_sd(20)
  REAL    ::           perturb_ic_max(20),  perturb_lbc_max(20)
  INTEGER ::           perturb_ic_seed(20), perturb_lbc_seed(20)
  REAL    ::           perturb_ic_swg(20),  perturb_lbc_swg(20)

  INTEGER :: lvldbg
  CHARACTER(LEN=256) :: outdir, wrf_bdy_out, wrf_input_out
  INTEGER            :: nproc_x_out, nproc_y_out

  INTEGER :: nfilein, nfileda, nfileout, nfiletd
  CHARACTER(LEN=256), ALLOCATABLE :: da_files(:), input_files(:),       &
                                     td_files(:), output_files(:)

  CONTAINS

  SUBROUTINE read_namelist_params(UNTIN,UNTOUT,IAMROOT,istatus)

!-----------------------------------------------------------------------
!
! PURPOSE:
!
!   Read namelist parameters
!
!-----------------------------------------------------------------------

    IMPLICIT NONE
    INTEGER, INTENT(IN)  :: UNTIN, UNTOUT
    LOGICAL, INTENT(IN)  :: IAMROOT

    INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
!
! Namelist variables (local to this subroutine)
!
!-----------------------------------------------------------------------

    INTEGER :: magnitude_processor

!-----------------------------------------------------------------------
!
! Namelist blocks
!
!-----------------------------------------------------------------------

    NAMELIST /INPUT/ da_file, wrf_bdy_in, wrf_input_in, wrf_td_in,      &
                     domain_id,magnitude_processor,end_time_str,        &
                     nproc_dax_in, nproc_day_in,                        &
                     nproc_inx_in, nproc_iny_in, nproc_tdx_in, nproc_tdy_in


    NAMELIST /CONTROL_PARAM/ update_lateral_bdy, update_input,          &
                             update_input_copy_td,use_input_w,          &
                             define_bdy, copy_input
    NAMELIST /PERTURB/ perturb_ic, perturb_ic_vars, perturb_ic_sd,      &
                       perturb_ic_max, perturb_ic_swg, perturb_ic_seed, &
                       perturb_lbc, perturb_lbc_vars, perturb_lbc_sd,   &
                       perturb_lbc_max, perturb_lbc_swg, perturb_lbc_seed

    NAMELIST /OUTPUT/ outdir, lvldbg, wrf_bdy_out, wrf_input_out,       &
                      nproc_x_out, nproc_y_out

!-----------------------------------------------------------------------

    LOGICAL :: fexist
    INTEGER :: indx

    INTEGER :: ipatch, jpatch
    CHARACTER(LEN=20) :: fmtstr

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Begin of executable code ...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

!-----------------------------------------------------------------------
!
! Default values
!
!-----------------------------------------------------------------------

    da_file       = 'wrfvar_output'
    wrf_bdy_in    = 'wrfbdy_d01'
    wrf_input_in  = 'wrfinput_d01'
    wrf_td_in     = 'wrfout_d01'
    domain_id     = 1

    nproc_dax_in  = 1
    nproc_day_in  = 1
    nproc_inx_in  = 1
    nproc_iny_in  = 1
    nproc_tdx_in  = 1
    nproc_tdy_in  = 1
    nproc_x_out   = 1
    nproc_y_out   = 1

    !ibgn = -1
    !iend = -1
    !jbgn = -1
    !jend = -1

    end_time_str = 'yyyy-mm-dd_hh:mm:ss'

    update_lateral_bdy   = .true.
    update_input         = .false.
    update_input_copy_td = .false.

    define_bdy           = .true.
    copy_input           = .true.
    use_input_w          = .false.

    outdir        = './output'
    lvldbg        = 0
    wrf_bdy_out   = 'wrfbdy_d01'
    wrf_input_out = 'wrfinput_d01'

    perturb_ic  = 0
    perturb_ic_vars = ' '
    perturb_ic_sd   = 1.0
    perturb_ic_max  = 1.0
    perturb_ic_seed = 1
    perturb_ic_swg  = 0.5

    perturb_lbc = 0
    perturb_lbc_vars = ' '
    perturb_lbc_sd   = 1.0
    perturb_lbc_max  = 1.0
    perturb_lbc_seed = 4
    perturb_lbc_swg  = 0.5

!-----------------------------------------------------------------------
!
! Read in namelist variables
!
!-----------------------------------------------------------------------

    IF (IAMROOT) THEN

!--------------------------- INPUT_FILES -------------------------------

      READ(UNTIN,NML=input,ERR=999)
      WRITE(UNTOUT,'(1x,a)') 'Namelist block input successfully read.'

!--------------------------- CONTROL_PARAM -----------------------------

      READ(UNTIN,NML=control_param,ERR=999)
      WRITE(UNTOUT,'(1x,a)') 'Namelist block control_param successfully read.'


      IF ( domain_id > 1 ) THEN
         WRITE(unit=UNTOUT, fmt='(1x,a,i2)') 'Nested domain ID = ',domain_id
         WRITE(unit=UNTOUT, fmt='(1x,a)') &
           'No wrfbdy file needed, only input need to be updated.'

         update_input     = .TRUE.

         IF ( update_lateral_bdy ) THEN
            WRITE(unit=UNTOUT, fmt='(1x,a)') &
               'Re-setting update_lateral_bdy to be false for nested domain.'
            update_lateral_bdy = .FALSE.
         END IF
      END IF

!--------------------------- PERTURB      ------------------------------

      READ(UNTIN,NML=perturb,ERR=999)
      WRITE(UNTOUT,'(1x,a)') 'Namelist block perturb successfully read.'

!--------------------------- OUTPUT       ------------------------------

      READ(UNTIN,NML=output,ERR=999)
      WRITE(UNTOUT,'(1x,a)') 'Namelist block output successfully read.'

      CALL inquiredir(TRIM(outdir),fexist)

      IF( .NOT. fexist ) THEN
        !CALL unixcmd( 'mkdir -p '//TRIM(output_path) )
        CALL makedir(outdir,istatus)
        WRITE(UNTOUT,'(5x,a,2(/5x,a))') 'Specified output directory '   &
          //TRIM(outdir)//' not found.',                                &
          'It was created by the program.'
      END IF

      indx = LEN_TRIM(outdir)
      IF (outdir(indx:indx) /= '/') outdir(indx+1:indx+1) = '/'

      WRITE(UNTOUT,'(5x,a)')                                            &
            'Output files will be in directory '//TRIM(outdir)//'.'

      wrf_bdy_out = TRIM(outdir)//TRIM(wrf_bdy_out)

      IF (TRIM(wrf_bdy_out) == TRIM(wrf_bdy_in) .OR.                    &
          TRIM(wrf_bdy_out) == './'//TRIM(wrf_bdy_in) ) THEN
        WRITE(UNTOUT,'(5x,a)')                                          &
            'Output bdy file should not be the same as input bdy file.'
        istatus = -1
        RETURN
      END IF

!-----------------------------------------------------------------------
!
! Get input/output file names
!
!-----------------------------------------------------------------------

      IF (nproc_dax_in > 1 .OR. nproc_day_in > 1) THEN
        nfileda = nproc_dax_in*nproc_day_in
      ELSE
        nfileda = 1
      END IF

      ALLOCATE(da_files(nfileda), STAT = istatus)

      IF (nfileda == 1) THEN
        da_files(1) = da_file
      ELSE
        WRITE(fmtstr,'(a,I0,a,I0,a)') '(2a,I',magnitude_processor,'.',magnitude_processor,')'
        indx = 0
        DO jpatch = 1, nproc_day_in
          DO ipatch = 1, nproc_dax_in
            indx = indx + 1
            WRITE(da_files(indx),FMT=TRIM(fmtstr)) TRIM(da_file),'_',indx-1
          END DO
        END DO
      END IF

      nfilein = nproc_inx_in*nproc_iny_in

      ALLOCATE(input_files (nfilein), STAT = istatus)

      IF (nfilein == 1) THEN
        input_files(1) = wrf_input_in
      ELSE
        indx = 0
        DO jpatch = 1, nproc_iny_in
          DO ipatch = 1, nproc_inx_in
            indx = indx + 1
            WRITE(input_files(indx),FMT=fmtstr) TRIM(wrf_input_in),'_',indx-1
          END DO
        END DO
      END IF

      IF (update_input) THEN

        IF (TRIM(outdir)//TRIM(wrf_input_out) == TRIM(wrf_input_in) .OR.  &
            TRIM(outdir)//TRIM(wrf_input_out) == './'//TRIM(wrf_input_in) ) THEN
          WRITE(UNTOUT,'(5x,a)')                                          &
              'Output input file should not be the same as input file.'
          istatus = -1
          RETURN
        END IF

        nfileout = nproc_x_out*nproc_y_out

        ALLOCATE(output_files(nfileout), STAT = istatus)

        IF (nfileout == 1) THEN
          output_files(1) = TRIM(outdir)//TRIM(wrf_input_out)
        ELSE
          indx = 0
          DO jpatch = 1, nproc_y_out
            DO ipatch = 1, nproc_x_out
              indx = indx + 1
              WRITE(output_files(indx),FMT=fmtstr) TRIM(outdir),TRIM(wrf_input_out)//'_',indx-1
            END DO
          END DO
        END IF

        IF (update_input_copy_td) THEN
          nfiletd    = nproc_tdx_in*nproc_tdy_in
          ALLOCATE(td_files(nfiletd),        STAT = istatus)

          IF (nfiletd == 1) THEN
            td_files(1) = wrf_td_in
          ELSE
            indx = 0
            DO jpatch = 1, nproc_tdy_in
              DO ipatch = 1, nproc_tdx_in
                indx = indx + 1
                WRITE(td_files(indx),FMT=fmtstr) TRIM(wrf_td_in),'_',indx-1
              END DO
            END DO
          END IF
        END IF

      END IF

!-----------------------------------------------------------------------
!
! Error with reading namelist
!
!-----------------------------------------------------------------------
      GOTO 900

      999 CONTINUE
      istatus = -1

      900 CONTINUE
    END IF

    IF (istatus /= 0) THEN
      IF (IAMROOT) WRITE(UNTOUT,'(1x,2a)') 'Error reading NAMELIST file.', &
                         'Job stopped in read_namelist_params.'

      RETURN
      !CALL arpsstop('Namelist input error',1)
    END IF

    RETURN
  END SUBROUTINE read_namelist_params

  !#####################################################################

  SUBROUTINE deallocate_namelist_arrays( istatus )

    INTEGER, INTENT(OUT) :: istatus

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    DEALLOCATE(da_files)
    IF (ALLOCATED(input_files))  DEALLOCATE(input_files)
    IF (ALLOCATED(output_files)) DEALLOCATE(output_files)
    IF (ALLOCATED(td_files))     DEALLOCATE(td_files)

    RETURN
  END SUBROUTINE deallocate_namelist_arrays

END MODULE wrf_daupdate_namelist
