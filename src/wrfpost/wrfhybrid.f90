!
!##################################################################
!##################################################################
!######                                                      ######
!######                   PROGRAM WRFHYBRID                  ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
PROGRAM wrfhybrid
!
!#######################################################################
!
! PURPOSE: Compute mean and/or recenter a set of WRF ensember members
!          with one deterministic WRF netCDF file
!
!-----------------------------------------------------------------------
!
! AUTHOR: Y. Wang (06/29/2016)
!
! MODIFICATION HISTORY:
!
!
!#######################################################################

  use wrfhybrid_ioapi

  IMPLICIT NONE

  INCLUDE 'mp.inc'

  INTEGER, PARAMETER :: MAX_NEN = 100

  INTEGER, PARAMETER :: MAX_VARS = 100

  CHARACTER(LEN=80)  :: outtitle    = 'OUTPUT FROM WRFHYBRID'

  CHARACTER(LEN=20)  :: varid(MAX_VARS)

!
!-----------------------------------------------------------------------
!
!  NAMELIST parameter (In wrfspread.input)
!
!-----------------------------------------------------------------------
!
  INTEGER :: mpimode

  NAMELIST /message_passing/ mpimode, nproc_x, nproc_y, nproc_x_in, nproc_y_in

  INTEGER :: program_mode
  REAL    :: alpha, dfactor
  INTEGER :: bdyzone
  INTEGER :: icontrol, itmpl
  INTEGER :: nvariables
  CHARACTER(LEN=20) :: variables(MAX_VARS)
  CHARACTER(LEN=1)  :: varstag(MAX_VARS)
  CHARACTER(LEN=256) :: outfile_control
  NAMELIST /progmod/ program_mode, alpha, bdyzone, dfactor,icontrol,outfile_control, &
                     nvariables,variables,varstag

  INTEGER            :: nen
  INTEGER            :: io_form_m, grid_id_m, magnitude_processor_m
  CHARACTER(LEN=256) :: dir_extm            ! directory of external data
  CHARACTER(LEN=19)  :: init_time_str_m,start_time_str_m,end_time_str_m
  CHARACTER(LEN=11)  :: history_interval_m
  INTEGER            :: filename_convention_m
  INTEGER            :: filename_append_member

  NAMELIST /wrfmfile/ nen,dir_extm,init_time_str_m,io_form_m,grid_id_m,   &
                      start_time_str_m,history_interval_m,end_time_str_m, &
                      magnitude_processor_m,filename_convention_m,      &
                      filename_append_member

  INTEGER            :: io_form_d, grid_id_d, magnitude_processor_d
  CHARACTER(LEN=256) :: dir_extd            ! directory of external data
  INTEGER            :: filename_convention_d

  NAMELIST /wrfdfile/ dir_extd,io_form_d,grid_id_d,                     &
                      magnitude_processor_d,filename_convention_d

  CHARACTER(LEN=256) :: outdir
  LOGICAL            :: out_join_patches, out_bkg_mean, out_rct_mean,   &
                        out_memb_new
  INTEGER            :: lvldbg
  NAMELIST /output/ outdir, lvldbg, out_join_patches,                   &
                    out_bkg_mean,out_rct_mean,out_memb_new
!
!-----------------------------------------------------------------------
!
!  Misc local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: itinit, itstart, itend, itinterval

  INTEGER :: istatus, unum, idummy
  INTEGER :: itime, ivar, numvar, nvar

  INTEGER :: i,j,k,l,n,m
  INTEGER :: mynen, myens,myene
  INTEGER :: myrank
  INTEGER :: ibgn, iend, jbgn, jend

  LOGICAL :: multifile, patch_split
  INTEGER :: ncmprx, ncmpry
  INTEGER           :: P_ARR(8)
  INTEGER           :: fin, file_mode
  CHARACTER(LEN=40) :: qnames(30)


  CHARACTER(LEN=256) :: tmpstr, wrffile, tmplfile, outfile
  CHARACTER(LEN=19)  :: timestr, fmtstr
  INTEGER            :: lenstr
  LOGICAL            :: fexist

  INTEGER, ALLOCATABLE :: fHndl(:,:,:),fHndlOut(:,:,:)
  INTEGER              :: fHndlvar, fHndlOutM(0:1)

  INTEGER :: nx, ny, nz, nxd, nyd, nxlg, nylg, nxout, nyout
  REAL,    ALLOCATABLE :: vararr(:,:,:,:), vardetr(:,:,:)
  REAL,    ALLOCATABLE :: varmean(:,:,:),  varhyb(:,:,:), varmeanout(:,:,:)
  REAL,    ALLOCATABLE :: temdin(:,:,:), temdout(:)

  REAL    :: varprim

  INTEGER            :: vardim(MAX_VARS)

  CHARACTER(LEN=20)  :: varid_set(MAX_VARS)
  INTEGER            :: nvarmem3d,nvarmem2d
  CHARACTER(LEN=20)  :: memvars3d(MAX_VARS),memvars2d(MAX_VARS)
  CHARACTER(LEN=1)   :: memstag3d(MAX_VARS),memstag2d(MAX_VARS)
  INTEGER            :: ndrop, dropset(MAX_VARS)

!-----------------------------------------------------------------------

  LOGICAL :: name_in_set
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  istatus = 0

!-----------------------------------------------------------------------
!
!  Initialize local variables
!
!-----------------------------------------------------------------------
!
  ! Non-MPI defaults: All others initialized in mpinit_var
  mp_opt = 0
  myproc = 0
  nproc_x_in  = 1
  nproc_y_in  = 1
  dumpstride = 1
  readstride = 1

  CALL mpinit_proc(0)

  IF(myproc == 0) THEN
    WRITE(6,'(10(/5x,a),/)')                                            &
  '###################################################################',&
  '###################################################################',&
  '####                                                           ####',&
  '####                Welcome to WRFHYBRID                       ####',&
  '####                                                           ####',&
  '####       Program that reads in output from WRF model,        ####',&
  '#### compute the statistic variables and recenter as required  ####',&
  '####                                                           ####',&
  '###################################################################',&
  '###################################################################'

    unum = COMMAND_ARGUMENT_COUNT()
    IF (unum > 0) THEN
      CALL GET_COMMAND_ARGUMENT(1, tmpstr, lenstr, istatus )
      IF ( tmpstr(1:1) == ' ' .OR. istatus /= 0 ) THEN
        ! Use standard input to be backward-compatible
        unum = 5
      ELSE
        INQUIRE(FILE=TRIM(tmpstr),EXIST=fexist)
        IF (.NOT. fexist) THEN
          WRITE(6,'(1x,3a)') 'WARNING: namelist file - ',               &
                TRIM(tmpstr),' does not exist. Falling back to standard input.'
          unum = 5
        END IF
      END IF
    ELSE
      unum = 5
    END IF

    IF (unum /= 5) THEN
      CALL getunit( unum )
      OPEN(unum,FILE=TRIM(tmpstr),STATUS='OLD',FORM='FORMATTED')
      WRITE(*,'(1x,3a,/,1x,a,/)') 'Reading WRFSPREAD namelist from file - ', &
              TRIM(tmpstr),' ... ','========================================'
    ELSE
      WRITE(*,'(2(1x,a,/))') 'Waiting namelist from standard input ... ', &
                             '========================================'
    END IF

  END IF

  IF (lvldbg > 0) WRITE(*,*) ' 0 - Reading run-time parameters ...'

!-----------------------------------------------------------------------
!
!  Read in namelist &message_passing and intialize MPI variables
!
!-----------------------------------------------------------------------
!
  mpimode = 0
  IF (myproc == 0) THEN
    READ(unum,message_passing)
    WRITE(6,'(a)') ' Namelist block message_passing sucessfully read.'
  END IF
  CALL mpupdatei(mpimode,1)
  CALL mpupdatei(nproc_x,1)
  CALL mpupdatei(nproc_y,1)
  CALL mpupdatei(nproc_x_in,1)
  CALL mpupdatei(nproc_y_in,1)

  IF (nproc_x_in == 1 .AND. nproc_y_in == 1) THEN
    multifile = .FALSE.
    ncmprx = 1
    ncmprx = 1
  ELSE
    multifile = .TRUE.
  END IF

  IF (nproc_x_in >= nproc_x .AND. nproc_y_in >= nproc_y) THEN
    patch_split = .FALSE.
    IF (mod(nproc_x_in,nproc_x) /= 0 .OR. mod(nproc_y_in,nproc_y) /= 0) THEN
      tmpstr  = 'ERROR: nproc_x_in (nproc_y_in) must be a multiple of nproc_x (nproc_y).'
      istatus = -1
    ELSE
      ncmprx = nproc_x_in / nproc_x
      ncmpry = nproc_y_in / nproc_y
    END IF
  ELSE IF (nproc_x_in <= nproc_x .AND. nproc_y_in <= nproc_y) THEN
    patch_split = .TRUE.
    IF (mod(nproc_x,nproc_x_in) /= 0 .OR. mod(nproc_y,nproc_y_in) /= 0) THEN
      tmpstr  = 'ERROR: nproc_x_in (nproc_y_in) must be a factor of nproc_x (nproc_y).'
      istatus = -2
    ELSE
      ncmprx = nproc_x / nproc_x_in
      ncmpry = nproc_y / nproc_y_in
    END IF
  ELSE
    tmpstr  = 'ERROR: unsupported combination of nproc_x_in and nproc_x, or nproc_y_in and nproc_y.'
    istatus = -3
  END IF

  9999 CONTINUE

  IF (istatus /= 0) THEN
    WRITE(*,'(1x,a)')       TRIM(tmpstr)
    WRITE(*,'(7x,2(a,I4))') ' nproc_x_in = ',nproc_x_in, ', nproc_x = ', nproc_x
    WRITE(*,'(7x,2(a,I4))') ' nproc_y_in = ',nproc_y_in, ', nproc_y = ', nproc_y
    istatus = -1
    !RETURN
    CALL arpsstop('ERROR mpi parameters',1)
  END IF

  !
  !  Initialize message passing variables.
  !
  CALL mpinit_var_gen(mpimode,myrank,nprocs)

  IF (out_join_patches) THEN
    nproc_x_out = 1
    nproc_y_out = 1
  ELSE
    nproc_x_out = nproc_x
    nproc_y_out = nproc_y
  END IF

!
!-----------------------------------------------------------------------
!
!  Read in namelist &progmod
!
!-----------------------------------------------------------------------
!
  program_mode = 0
  alpha        = 0.5
  bdyzone      = 0
  dfactor      = 1.0

  icontrol     = 1
  outfile_control = 'none'
  nvariables   = 12
  variables(1)  = 'PH                  '
  variables(2)  = 'U                   '
  variables(3)  = 'V                   '
  variables(4)  = 'W                   '
  variables(5)  = 'T                   '
  variables(6)  = 'QVAPOR              '
  variables(7)  = 'QCLOUD              '
  variables(8)  = 'QRAIN               '
  variables(9)  = 'QICE                '
  variables(10) = 'QSNOW               '
  variables(11) = 'QGRAUP              '
  variables(12) = 'QHAIL               '
  varstag(1:12) = (/ 'Z','X','Y','Z',' ',' ',' ',' ',' ',' ',' ',' ' /)

  IF (myproc == 0) THEN
    READ(unum,progmod)
    WRITE(6,'(a)') ' Namelist progmod read in successfully.'

    IF (program_mode /= 2) THEN
      dfactor = 1.0                   ! make sure dfactor is 1 for no scaling scenario
    END IF
  END IF   ! myproc == 0
  CALL mpupdatei(program_mode,1)
  CALL mpupdater(alpha,1)
  CALL mpupdatei(bdyzone,1)
  CALL mpupdater(dfactor,1)
  CALL mpupdatei(icontrol,1)
  CALL mpupdatec(outfile_control,256)
  CALL mpupdatei(nvariables,12)
  CALL mpupdatec(variables,20*MAX_VARS)
  CALL mpupdatec(varstag,MAX_VARS)

  ibgn = 1
  iend = nx
  jbgn = 1
  jend = ny
  IF (bdyzone > 0) THEN    ! skip boundary hybriding
    IF (mpimode == 0) THEN ! parallel process patch
      WRITE(*,*) 'No support yet.'
      CALL mpexit(-1)
    ELSE                   ! parallel processing ensemble members
      ibgn = 1+bdyzone
      iend = nx-bdyzone
      jbgn = 1+bdyzone
      jend = ny-bdyzone
    END IF
  END IF
!
!-----------------------------------------------------------------------
!
!  Read in namelist &wrfmfile
!
!-----------------------------------------------------------------------
!
  nen      = 40
  dir_extm = './'

  init_time_str_m         = '0000-00-00_00:00:00'
  start_time_str_m        = '0000-00-00_00:00:00'
  history_interval_m      = '00_00:00:00'
  end_time_str_m          = '0000-00-00_00:00:00'

  io_form_m               = 7
  grid_id_m               = 1
  magnitude_processor_m   = 4
  filename_convention_m   = 2
  filename_append_member  = 0

  IF (myproc == 0) THEN
    READ(unum,wrfmfile)
    WRITE(6,'(a)') ' Namelist wrfmfile read in successfully.'

    !lenstr = LEN_TRIM(dir_extd)
    !IF(lenstr > 0) THEN
    !  IF(dir_extd(lenstr:lenstr) /= '/') THEN
    !    dir_extd(lenstr+1:lenstr+1) = '/'
    !  END IF
    !ELSE
    !  dir_extd = './'
    !END IF

  END IF   ! myproc == 0
  CALL mpupdatei(nen,1)
  CALL mpupdatec(dir_extm,256)
  CALL mpupdatei(io_form_m,1)
  CALL mpupdatec(init_time_str_m,19)
  CALL mpupdatec(start_time_str_m,19)
  CALL mpupdatec(end_time_str_m,19)
  CALL mpupdatec(history_interval_m,11)
  CALL mpupdatei(grid_id_m, 1)
  CALL mpupdatei(magnitude_processor_m,1)
  CALL mpupdatei(filename_convention_m,1)
  CALL mpupdatei(filename_append_member,1)

  CALL get_time_loop_indices(init_time_str_m,start_time_str_m,          &
                             history_interval_m,end_time_str_m,         &
                             itstart,itend,itinterval,itinit,istatus)

!
!-----------------------------------------------------------------------
!
!  Read in namelist &wrfdfile
!
!-----------------------------------------------------------------------
!
  dir_extd = './'

  io_form_d               = 7
  grid_id_d               = 1
  magnitude_processor_d   = 4
  filename_convention_d   = 2

  IF (myproc == 0) THEN
    READ(unum,wrfdfile)
    WRITE(6,'(a)') ' Namelist wrfdfile read in successfully.'

    !lenstr = LEN_TRIM(dir_extd)
    !IF(lenstr > 0) THEN
    !  IF(dir_extd(lenstr:lenstr) /= '/') THEN
    !    dir_extd(lenstr+1:lenstr+1) = '/'
    !  END IF
    !ELSE
    !  dir_extd = './'
    !END IF

  END IF   ! myproc == 0
  CALL mpupdatec(dir_extd,256)
  CALL mpupdatei(io_form_d,1)
  CALL mpupdatei(grid_id_d, 1)
  CALL mpupdatei(magnitude_processor_d,1)
  CALL mpupdatei(filename_convention_d,1)

!-----------------------------------------------------------------------
!
!  Read in namelist &output
!
!-----------------------------------------------------------------------
!
   outdir            = './'
   out_join_patches  = .TRUE.
   out_bkg_mean      = .TRUE.
   out_rct_mean      = .FALSE.
   out_memb_new      = .FALSE.
   lvldbg            = 0

  IF (myproc == 0) THEN
    READ(unum,output)
    WRITE(6,'(a)') ' Namelist output read in successfully.'

    lenstr = LEN_TRIM(outdir)
    IF(lenstr > 0) THEN
      IF(outdir(lenstr:lenstr) /= '/') THEN
        outdir(lenstr+1:lenstr+1) = '/'
      END IF
    ELSE
      outdir = './'
    END IF

  END IF
  CALL mpupdatec(outdir,256)
  CALL mpupdatel(out_join_patches,1)
  CALL mpupdatel(out_bkg_mean,1)
  CALL mpupdatel(out_rct_mean,1)
  CALL mpupdatel(out_memb_new,1)
  CALL mpupdatei(lvldbg,1)

!=======================================================================
!
! NAMELIST readings are done
!
!=======================================================================

  IF (unum /= 5 .AND. myproc == 0) THEN
    CLOSE( unum )
    CALL retunit( unum )
  END IF

  IF (myproc == 0) WRITE(*,'(/,2(1x,a,/))')                             &
      'Beginning of execution ... ',                                    &
      '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'

  IF (mpimode == 0) THEN
    mynen = nen
    myens = 1
    myene = nen
    myrank = 0
  ELSE
    IF (nen > nprocs) THEN
      mynen  = nen/nprocs
      idummy = MOD(nen,nprocs)
      IF (myproc >= nprocs-idummy) THEN
        myens = myproc*mynen+1+(myproc-(nprocs-idummy))
        mynen = mynen+1
        myene = myens+mynen-1
      ELSE
        myens = myproc*mynen+1
        myene = myens+mynen-1
      END IF
    ELSE
      IF (myproc < nen) THEN
        mynen  = 1
        myens  = myproc+1
        myene  = myproc+1
        nprocs = nen
      ELSE
        mynen  = 0
        myens  = 0
        myene  = 0
      END IF
    END IF
    myproc = 0
  END IF

  WRITE(*,'(1x,4(a,I3),a)') 'myproc = ',myproc,', myrank = ',myrank,    &
          ', will process member ',myens,' to ',myene,'.'
!-----------------------------------------------------------------------
!
!  Get file dimensions
!
!-----------------------------------------------------------------------
!
  IF (myproc == 0 .AND. myrank == 0) WRITE(*,*) ' 1 - Getting WRF dimensions ...'

  istatus = -1
  IF (icontrol == 0) THEN
    itmpl = 1
  ELSE
    itmpl = icontrol
  END IF

  CALL get_wrf_file_name(3,itmpl,dir_extm,filename_convention_m,filename_append_member,grid_id_m,&
                         itstart,itinit,wrffile,timestr,lvldbg,istatus)

  P_ARR(:) = 0
  CALL get_wrf_grids(wrffile,magnitude_processor_m,nx,ny,nz,            &
               P_ARR(1),P_ARR(2),P_ARR(3),P_ARR(4),P_ARR(5),P_ARR(6),   &
               P_ARR(7),P_ARR(8),qnames,istatus )

!-----------------------------------------------------------------------
!
!  Get variable set
!
!-----------------------------------------------------------------------

  varid(:) = '                   '
  IF (nvariables > 0) THEN

    DO n = 1, nvariables
      varid(n) = variables(n)
    END DO

  ELSE

    CALL open_ncd_wrf_file ( wrffile, 'r', fin, istatus )
    CALL get_ncd_3dvars ( fin, MAX_VARS, varid, varstag, nvariables, istatus )
    CALL close_ncd_wrf_file ( fin, istatus )

  END IF
  numvar = nvariables

  IF (myproc == 0 .AND. myrank == 0) THEN
    DO l = 1, nen

      CALL get_wrf_file_name(3,l,dir_extm,filename_convention_m,filename_append_member,grid_id_m,&
                           itstart,itinit,wrffile,timestr,lvldbg,istatus)

      CALL open_ncd_wrf_file ( wrffile, 'r', fin, istatus )
      CALL get_ncd_3dvars ( fin, MAX_VARS, memvars3d, memstag3d, nvarmem3d, istatus )
      CALL get_ncd_2dvars ( fin, MAX_VARS, memvars2d, memstag2d, nvarmem2d, istatus )
      CALL close_ncd_wrf_file ( fin, istatus )

      ndrop = 0
      DO n = 1, numvar
        !print *, l, numvar, n, varid(n)
        IF (name_in_set(varid(n),memvars3d,nvarmem3d)) THEN
          vardim(n) = 3
        ELSE IF (name_in_set(varid(n),memvars2d,nvarmem2d)) THEN
          vardim(n) = 2
        ELSE
          WRITE(*,'(1x,3a,I0,a)') 'WARNING: variable ',TRIM(varid(n)),' is not present in ensemble member ',l,'.'
          WRITE(*,'(10x,a)')      'It will be dropped.'
          ndrop = ndrop+1
          dropset(ndrop) = n
        END IF
      END DO

      DO n = ndrop,1,-1
        DO m = dropset(n)+1,numvar
          varid(m-1)   = varid(m)
          varstag(m-1) = varstag(m)
          vardim(m-1)  = vardim(m)
        END DO
        numvar = numvar - 1
      END DO
    END DO   ! member loop
  END IF
  CALL mpupdatei(numvar,1)
  CALL mpupdatec(varid,20*MAX_VARS)
  CALL mpupdatec(varstag,MAX_VARS)
  CALL mpupdatei(vardim,MAX_VARS)

!-----------------------------------------------------------------------
!
!  Allocate working arrays
!
!-----------------------------------------------------------------------
!
  IF (myproc == 0 .AND. myrank == 0) WRITE(*,*) ' 2 - Allocating working arrays ...'

  nxlg = (nx-1)*nproc_x + 1     ! WRF domain indices
  nylg = (ny-1)*nproc_y + 1

  IF (multifile) THEN
    nxd = (nxlg-1) / nproc_x_in + 1
    nyd = (nylg-1) / nproc_y_in + 1
  ELSE
    nxd = nxlg    ! dataset size in the file, staggered
    nyd = nylg
  END IF

  IF (patch_split) THEN
    !
    ! It is required that (nxd_wrf-1) is dividable with ncmprx
    !
    IF (MOD(nxd-1,ncmprx) /= 0 .OR. MOD(nyd-1,ncmpry) /= 0) THEN
      istatus = -4
      tmpstr  = 'ERROR: dimension sizes does not fit with the number of processes.'
      GOTO 9999
    END IF

  END IF

  IF (out_join_patches) THEN
    nproc_x_out = 1;       nxout = NINT((nxlg-1)*dfactor+1)
    nproc_y_out = 1;       nyout = NINT((nylg-1)*dfactor+1)
  ELSE
    nproc_x_out = nproc_x; nxout = NINT((nx-1)*dfactor+1)
    nproc_y_out = nproc_y; nyout = NINT((ny-1)*dfactor+1)
  END IF

  ALLOCATE(fHndl(ncmprx,ncmpry, myens:myene),  STAT = istatus)

  ALLOCATE(vararr (nx, ny, nz, myens:myene),   STAT = istatus)
  ALLOCATE(vardetr(nx, ny, nz),                STAT = istatus)
  ALLOCATE(varmean(nx, ny, nz),                STAT = istatus)
  ALLOCATE(varmeanout(nxout, nyout, nz),       STAT = istatus)
  ALLOCATE(varhyb (nx, ny, nz),          STAT = istatus)

  ALLOCATE(temdin (nxd,nyd,nz),          STAT = istatus)

  ALLOCATE(temdout(nxout*nyout*nz),      STAT = istatus)

  IF (program_mode == 1 ) THEN
    ALLOCATE(fHndlOut(1,1, myens:myene),       STAT = istatus)

    IF (nxd /= nxout .OR. nyd /= nyout) THEN
      out_memb_new = .TRUE.
    END IF

  END IF
  fHndl(:,:,:) = 0

  IF (myproc == 0 .AND. myrank == 0) THEN
    WRITE(*,'(3(6x,3(a,i4),/))')                                        &
       'Process dimensions are: nx    = ',nx,   ', ny    = ',ny,   ', nz = ',nz, &
       'Global  dimensions are: nxlg  = ',nxlg, ', nylg  = ',nylg, ', nz = ',nz, &
       'Output  dimensions are: nxout = ',nxout,', nyout = ',nyout,', nz = ',nz
    WRITE(*,'(6x,a,/,100(8x,I4,2a,/))') 'Variables to be processed:',   &
       (ivar,': ',varid(ivar),ivar = 1, numvar)
  END IF
!-----------------------------------------------------------------------
!
!  Loop over time
!
!-----------------------------------------------------------------------
!

  DO itime = itstart,itend,itinterval

    IF (myproc == 0 .AND. myrank == 0) WRITE(*,'(2x,a,I0,a)')           &
                 '3 - Processing itime = ',itime-itstart,' ...'

    !
    ! Open input ensemble member's file handlers
    !
    IF (program_mode == 1 .AND. .NOT. out_memb_new) THEN
      file_mode = 3
    ELSE
      file_mode = 1
    END IF

    DO l = myens, myene
       istatus = -1
       CALL get_wrf_file_name(3,l,dir_extm,filename_convention_m,filename_append_member,grid_id_m,   &
                              itime,itinit,wrffile,timestr,lvldbg,istatus)

       CALL open_wrf_files(wrffile,io_form_m,patch_split,multifile,file_mode, &
                          ncmprx,ncmpry,magnitude_processor_m,fHndl(:,:,l))

       IF (ANY(fHndl(:,:,l) < 0)) THEN
         istatus = -1
         tmpstr  = 'Failed in open_wrf_files.'
         GO TO 9998
       ELSE
         IF (myproc == 0) THEN
           IF (multifile) THEN
             WRITE(*,'(6x,a,I0,2a)') 'Read member ',l,' from patches of WRF file = ',TRIM(wrffile)
           ELSE
             WRITE(*,'(6x,a,I0,2a)') 'Read member ',l,' from WRF file = ',TRIM(wrffile)
           END IF
         END IF
       END IF
    END DO

    !
    ! Ouput file handlers for ensemble means
    !
    varid_set(1) = "Times"
    DO ivar = 1, numvar
      varid_set(ivar+1) = varid(ivar)
    END DO
    nvar = numvar + 1

    IF ( out_bkg_mean .AND. myrank == 0) THEN

      outtitle = 'WRF ensember mean before filter, OUTPUT FROM WRFHYBRID'

      istatus = -1

      !
      ! Use the icontrol member as mean output template
      !
      IF (icontrol > 0) THEN
        CALL get_wrf_file_name(3,icontrol,dir_extm,filename_convention_m,filename_append_member,grid_id_m, &
                               itime,itinit,wrffile,timestr,lvldbg,istatus)

        IF (multifile) THEN
          WRITE(fmtstr,'(a,2(I0,a))') '(2a,I',magnitude_processor_m,'.',magnitude_processor_m,')'
          WRITE(tmplfile,FMT=TRIM(fmtstr)) TRIM(wrffile),'_',0
        ELSE
          WRITE(tmplfile,'(a)') TRIM(wrffile)
        END IF
      ELSE
        tmplfile = outfile_control
      END IF

      !
      ! Get mean output file name
      !
      CALL get_wrf_file_name(2,-1,outdir,filename_convention_m,0,grid_id_m,  &
                            itime,itinit,outfile,timestr,lvldbg,istatus)

      CALL wrfhybrid_ioinit ( nx,ny,nz,nxout,nyout,dfactor,magnitude_processor_m,  &
                              outfile, nvar, varid_set,                 &
                              tmplfile,outtitle,init_time_str_m,        &
                              fHndlOutM(0), .TRUE.,.TRUE., lvldbg, istatus )

    END IF

    !
    ! Open deterministic file as needed
    !
    IF (program_mode == 1 ) THEN
      istatus = -1
      CALL get_wrf_file_name(3,0,dir_extd,filename_convention_d,0,grid_id_d, &
                             itime,itinit,wrffile,timestr,lvldbg,istatus)

      CALL open_wrf_files(wrffile,io_form_d,patch_split,multifile,1,    &
                         ncmprx,ncmpry,magnitude_processor_d,fHndlvar)

      IF (fHndlvar < 0) THEN
        istatus = -1
        tmpstr  = 'Failed in open_wrf_files.'
        GO TO 9998
      ELSE
        IF (myproc == 0 .AND. myrank == 0) THEN
          IF (multifile) THEN
            WRITE(*,'(6x,2a)') 'Read deterministic data from patches of WRF file = ',TRIM(wrffile)
          ELSE
            WRITE(*,'(6x,2a)') 'Read deterministic data from WRF file = ',TRIM(wrffile)
          END IF
        END IF
      END IF

      !
      ! Ouput file handlers as needed
      !
      outtitle = 'WRF ensember member after recentered, OUTPUT FROM WRFHYBRID'

      IF (.NOT. out_memb_new) THEN
        fHndlOut = fHndl
        CALL wrfhybrid_ioinit ( nx,ny,nz,nxout,nyout,dfactor,magnitude_processor_m,         &
                              outfile, nvar, varid_set,                 &
                              tmplfile,outtitle,init_time_str_m,        &
                              idummy,.FALSE., .TRUE., lvldbg, istatus )
      ELSE

        DO l = myens,myene
          !
          ! Use the orginal file for each member as output template
          !
          CALL get_wrf_file_name(3,l,dir_extm,filename_convention_m,filename_append_member,grid_id_m, &
                                 itime,itinit,wrffile,timestr,lvldbg,istatus)

          IF (multifile) THEN
            WRITE(fmtstr,'(a,2(I0,a))') '(2a,I',magnitude_processor_m,'.',magnitude_processor_m,')'
            WRITE(tmplfile,FMT=TRIM(fmtstr)) TRIM(wrffile),'_',0
          ELSE
            WRITE(tmplfile,'(a)') TRIM(wrffile)
          END IF

          !
          ! Get the file name of new output for each member
          !
          CALL get_wrf_file_name(2,l,outdir,filename_convention_m,filename_append_member,grid_id_m, &
                                 itime,itinit,outfile,timestr,lvldbg,istatus)

          CALL wrfhybrid_ioinit ( nx,ny,nz,nxout,nyout,dfactor,magnitude_processor_m,       &
                              outfile, nvar, varid_set,                 &
                              tmplfile,outtitle,timestr,                &
                              fHndlOut(1,1,l), .TRUE.,.TRUE., lvldbg, istatus )
        END DO
      END IF

      IF ( out_rct_mean .AND. myrank == 0) THEN

        IF (icontrol > 0) THEN
          !
          ! Use the icontrol member as mean output template
          !
          CALL get_wrf_file_name(3,icontrol,dir_extm,filename_convention_m,filename_append_member,grid_id_m, &
                                 itime,itinit,wrffile,timestr,lvldbg,istatus)

          IF (multifile) THEN
            WRITE(fmtstr,'(a,2(I0,a))') '(2a,I',magnitude_processor_m,'.',magnitude_processor_m,')'
            WRITE(tmplfile,FMT=TRIM(fmtstr)) TRIM(wrffile),'_',0
          ELSE
            WRITE(tmplfile,'(a)') TRIM(wrffile)
          END IF
        ELSE
          tmplfile = outfile_control
        END IF

        !
        ! get the file name for output
        !
        outtitle = 'WRF ensember mean after recentered, OUTPUT FROM WRFHYBRID'

        istatus = -1
        CALL get_wrf_file_name(2,0,outdir,filename_convention_d,0,grid_id_d,&
                              itime,itinit,outfile,timestr,lvldbg,istatus)

        CALL wrfhybrid_ioinit ( nx,ny,nz,nxout,nyout,dfactor,magnitude_processor_d,         &
                              outfile, nvar, varid_set,                 &
                              tmplfile,outtitle,timestr,                &
                              fHndlOutM(1), .TRUE.,.TRUE., lvldbg, istatus )
      END IF

    END IF

!-----------------------------------------------------------------------
!
!  Loop over variables
!
!-----------------------------------------------------------------------

    DO ivar = 1, numvar
      IF (myproc == 0 .AND. myrank == 0) WRITE(*,'(6x,a,I0,3a)')         &
                      '4-',ivar,' - Processing var = ',TRIM(varid(ivar)),' ...'

      IF (myproc == 0 .AND. myrank == 0) WRITE(*,'(10x,a,I0,a)')         &
                      '4-',ivar,'.0 Reading ensemble members ...'

!-----------------------------------------------------------------------
!
!  Process 3D variables
!
!-----------------------------------------------------------------------

      IF (vardim(ivar) == 3) THEN

        DO l = myens,myene
          istatus = lvldbg
          IF (lvldbg > 10) THEN
            WRITE(*,'(1x,3(a,I0))') 'Reading member l = ',l,', from ',fHndl(:,:,l),' by ',myrank
          END IF
          CALL get_wrf_3d(fHndl(:,:,l),io_form_m,multifile,patch_split,ncmprx,ncmpry,    &
                    timestr,    1,TRIM(varid(ivar)),varstag(ivar),        &
                    nx,ny,nz,vararr(:,:,:,l),nxd,nyd,nz,temdin,istatus)
        END DO

        !
        !  Compute ensemble mean
        !
        IF (myproc == 0 .AND. myrank == 0) WRITE(*,'(10x,a,I0,3a)')         &
                        '4-',ivar,'.1 Compute and output mean ...'

        !
        ! an alternation implementation, not sure which one is better for cache reusing
        !

        !DO k = 1, nz
        !  DO j = 1, ny
        !    DO i = 1, nx
        !      varmean(i,j,k) = 0.0
        !      DO l = 1, nen
        !        varmean(i,j,k) = varmean(i,j,k)+vararr(i,j,k,l)
        !      END DO
        !      varmean(i,j,k) = varmean(i,j,k)/nen
        !      DO l = 1, nen
        !        varprim(i,j,k,l) = vararr(i,j,k,l)-varmean(i,j,k)
        !      END DO
        !    END DO
        !  END DO
        !END DO

        varmean(:,:,:) = 0.0
        DO l = myens,myene
          DO k = 1, nz
            DO j = 1, ny
              DO i = 1, nx
                 varmean(i,j,k) = varmean(i,j,k)+vararr(i,j,k,l)
              END DO
            END DO
          END DO
        END DO

        IF (mpimode > 0) THEN
          CALL mpsumr(varmean,nx*ny*nz)
        END IF
        varmean(:,:,:) = varmean(:,:,:)/nen

        CALL wrfhybrid_intrp3d(varmean,varmeanout,istatus)

        !
        !  Write out mean for this variable
        !

        !IF (lvldbg > 0) WRITE(*,'(6x,a,I0,3a)')                          &
        !              '4.',ivar,'-O Writting mean files ...'

        IF (out_bkg_mean .AND. myrank == 0) THEN
          CALL wrfhybrid_write3d ( fHndlOutM(0), varid(ivar),varmeanout,  &
                                   varstag(ivar),temdout,istatus )
        END IF

!-----------------------------------------------------------------------
!
!  Process 2D variables
!
!-----------------------------------------------------------------------

     ELSE IF (vardim(ivar) == 2) THEN

        DO l = myens,myene
          istatus = lvldbg
          IF (lvldbg > 10) THEN
            WRITE(*,'(1x,3(a,I0))') 'Reading member l = ',l,', from ',fHndl(:,:,l),' by ',myrank
          END IF
          CALL get_wrf_2d(fHndl(:,:,l),io_form_m,multifile,patch_split, &
                    ncmprx,ncmpry,                                      &
                    timestr,    1,TRIM(varid(ivar)),varstag(ivar),      &
                    nx,ny,vararr(:,:,1,l),nxd,nyd,temdin,istatus)
        END DO

        !
        !  Compute ensemble mean
        !
        IF (myproc == 0 .AND. myrank == 0) WRITE(*,'(10x,a,I0,3a)')         &
                        '4-',ivar,'.1 Compute and output mean ...'

        !
        ! an alternation implementation, not sure which one is better for cache reusing
        !

        !DO k = 1, nz
        !  DO j = 1, ny
        !    DO i = 1, nx
        !      varmean(i,j,k) = 0.0
        !      DO l = 1, nen
        !        varmean(i,j,k) = varmean(i,j,k)+vararr(i,j,k,l)
        !      END DO
        !      varmean(i,j,k) = varmean(i,j,k)/nen
        !      DO l = 1, nen
        !        varprim(i,j,k,l) = vararr(i,j,k,l)-varmean(i,j,k)
        !      END DO
        !    END DO
        !  END DO
        !END DO

        varmean(:,:,1) = 0.0
        DO l = myens,myene
            DO j = 1, ny
              DO i = 1, nx
                 varmean(i,j,1) = varmean(i,j,1)+vararr(i,j,1,l)
              END DO
            END DO
        END DO

        IF (mpimode > 0) THEN
          CALL mpsumr(varmean,nx*ny)
        END IF
        varmean(:,:,1) = varmean(:,:,1)/nen

        CALL wrfhybrid_intrp2d(varmean,varmeanout,istatus)

        !
        !  Write out mean for this variable
        !

        !IF (lvldbg > 0) WRITE(*,'(6x,a,I0,3a)')                          &
        !              '4.',ivar,'-O Writting mean files ...'

        IF (out_bkg_mean .AND. myrank == 0) THEN
          CALL wrfhybrid_write2d ( fHndlOutM(0), varid(ivar),varmeanout(:,:,1),  &
                                   varstag(ivar),temdout,istatus )
        END IF

     END IF

!-----------------------------------------------------------------------
!
!  Recenter ensemble members
!
!-----------------------------------------------------------------------

      IF (program_mode == 1) THEN
        IF (myproc == 0 .AND. myrank == 0) WRITE(*,'(10x,a,I0,3a)') '4-',ivar,   &
                                  '.2 Read deterministic data ...'

        istatus = lvldbg
        CALL get_wrf_3d(fHndlvar,io_form_d,multifile,patch_split,ncmprx,ncmpry,  &
                  timestr,    1,TRIM(varid(ivar)),varstag(ivar),                 &
                  nx,ny,nz,vardetr,nxd,nyd,nz,temdin,istatus)

        IF (myproc == 0 .AND. myrank == 0) WRITE(*,'(10x,a,I0,2(a,F5.2),a)') '4-',ivar,   &
              '.3 Compute new hybrid center, weight for ensemble mean = ',alpha,', weight for deterministic forecast = ',1-alpha,' ...'
        DO k = 1, nz
          DO j = 1, ny
            DO i = 1, nx
               varhyb(i,j,k) = alpha*varmean(i,j,k)+(1.-alpha)*vardetr(i,j,k)
            END DO
          END DO
        END DO

        IF (out_rct_mean .AND. myrank == 0) THEN
          CALL wrfhybrid_write3d ( fHndlOutM(1), varid(ivar),varhyb,             &
                                 varstag(ivar),temdout,istatus )
        END IF

        IF (myproc == 0 .AND. myrank == 0) WRITE(*,'(10x,a,I0,3a)') '4-',ivar,   &
                         '.4 Compute and output recentered members ...'

        DO l = myens, myene
          DO k = 1, nz
            DO j = jbgn, jend
              DO i = ibgn, iend
                vararr(i,j,k,l) = vararr(i,j,k,l)-varmean(i,j,k)+varhyb(i,j,k)
              END DO
            END DO
          END DO
        END DO

        DO l = myens,myene
          IF (lvldbg > 10) THEN
            WRITE(*,'(1x,2(a,I0))') 'Writing member l = ',l,', to ',fHndlOut(1,1,l)
          END IF

          CALL wrfhybrid_write3d ( fHndlOut(1,1,l), varid(ivar),        &
                      vararr(:,:,:,l), varstag(ivar), temdout, istatus )
        END DO

      END IF  ! program_mode > 0

    END DO

!-----------------------------------------------------------------------
!
!  Close opened files
!
!-----------------------------------------------------------------------

    IF (myproc == 0 .AND. myrank == 0) WRITE(*,'(2x,a,/,11x,a,I0,a/)')  &
                    '5 - Clossing opened files ...',                    &
                    '---------- Done it=',itime-itstart,' -----------'

    DO l = myens, myene
      CALL close_wrf_files(fHndl(:,:,l),io_form_m,patch_split,file_mode,ncmprx,ncmpry)
    END DO

    IF (out_bkg_mean .AND. myrank == 0) CALL wrfhybrid_iofinal ( fHndlOutM(0), istatus )

    IF (program_mode == 1) THEN

      CALL close_wrf_files(fHndlvar,io_form_d,patch_split,1,ncmprx,ncmpry)

      IF (out_rct_mean .AND. myrank == 0) CALL wrfhybrid_iofinal ( fHndlOutM(1), istatus )
      IF (out_memb_new) THEN
        DO l = myens,myene
          CALL wrfhybrid_iofinal ( fHndlOut(1,1,l), istatus )
        END DO
      END IF

    END IF

  END DO

!-----------------------------------------------------------------------
!
!  Deallocate working arrays
!
!-----------------------------------------------------------------------
!
  9998 CONTINUE

  IF (mp_opt > 0) THEN
    tmpstr = 'WRFHYBRID_MPI'
  ELSE
    tmpstr = 'WRFHYBRID'
  END IF

  IF (istatus /= 0) THEN
    IF (myproc == 0 .AND. myrank == 0) THEN
      WRITE(*,'(1x,a)')       TRIM(tmpstr)
      WRITE(*,'(/,5x,3a)') '**** ',TRIM(tmpstr),' aborted with error ****'
    END IF
    CALL mpexit(istatus)
  ELSE
    IF (myproc == 0 .AND. myrank == 0) WRITE(*,'(/,5x,3a)') '==== ',TRIM(tmpstr),' terminated normally ===='
    CALL mpexit(0)
    STOP 0
  END IF

END PROGRAM wrfhybrid

!#######################################################################

SUBROUTINE get_time_loop_indices(init_str,start_str,interval_str,end_str,&
                                 istart,iend,iinterval,iinit,istatus)

  IMPLICIT NONE

  CHARACTER(LEN=19), INTENT(IN) :: start_str, end_str, init_str
  CHARACTER(LEN=11), INTENT(IN) :: interval_str
  INTEGER, INTENT(OUT) :: istart,iend,iinterval,iinit
  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INTEGER :: year, month,day, hour, minute, second
  INTEGER :: myr
  LOGICAL :: rewindyr = .FALSE.

  INTEGER :: abstimes, abstimee
  CHARACTER(LEN=1) :: ach

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  READ(start_str,'(I4.4,5(a,I2.2))')                                    &
                  year,ach,month,ach,day,ach,hour,ach,minute,ach,second
  IF (year < 1960) THEN   ! maybe ideal case
    myr  =  year
    year =  1960
    rewindyr = .TRUE.
  END IF
  CALL ctim2abss(year,month,day,hour,minute,second,istart)


  READ(end_str,'(I4.4,a,I2.2,a,I2.2,a,I2.2,a,I2.2,a,I2.2)')             &
                  year,ach,month,ach,day,ach,hour,ach,minute,ach,second

  IF (rewindyr)  year = 1960
  CALL ctim2abss(year,month,day,hour,minute,second,iend)

  READ(interval_str,'(I2.2,a,I2.2,a,I2.2,a,I2.2)')                      &
                                     day,ach,hour,ach,minute,ach,second
  iinterval = day*24*3600+hour*3600+minute*60+second

  READ(init_str,'(I4.4,a,I2.2,a,I2.2,a,I2.2,a,I2.2,a,I2.2)')            &
                 year,ach,month,ach,day,ach,hour,ach,minute,ach,second
  CALL ctim2abss(year,month,day,hour,minute,second,iinit)

  RETURN
END SUBROUTINE get_time_loop_indices

!#######################################################################

SUBROUTINE get_wrf_file_name(mpigroup,l,indir,fcvn,fmem,grid_id,itime,itime0, &
                             wrffile,timestr,lvldbg,istatus)

  IMPLICIT NONE

  INTEGER,            INTENT(IN)  :: mpigroup
  INTEGER,            INTENT(IN)  :: l
  CHARACTER(LEN=256), INTENT(IN)  :: indir
  INTEGER,            INTENT(IN)  :: fcvn, fmem
  INTEGER,            INTENT(IN)  :: itime, itime0
  INTEGER,            INTENT(IN)  :: grid_id
  CHARACTER(LEN=256), INTENT(OUT) :: wrffile
  CHARACTER(LEN=19),  INTENT(OUT) :: timestr
  INTEGER,            INTENT(IN)  :: lvldbg
  INTEGER,            INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  CHARACTER(LEN=256) :: tmpstr
  CHARACTER(LEN=1)   :: cchar

  REAL    :: time
  INTEGER :: iyr,imon,idy,ihr,imin,isec

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = lvldbg

  time = itime - itime0
  CALL get_output_dirname(mpigroup,indir,time,l,tmpstr,istatus)

  CALL abss2ctim( itime,iyr,imon,idy,ihr,imin,isec )
  WRITE(timestr,'(I4.4,5(a,I2.2))') iyr,'-',imon,'-',idy,'_',ihr,':',imin,':',isec

  SELECT CASE (fcvn)
  CASE (0)
    WRITE(wrffile,'(3a,I2.2,2a)') TRIM(tmpstr),'wrfout','_d',grid_id,'_',timestr
  CASE (7)
    WRITE(wrffile,'(3a,I2.2,2a)') TRIM(tmpstr),'wrffcst','_d',grid_id,'_',timestr
  CASE (2)
    !IF (l > 0) THEN
    !  WRITE(wrffile,'(3a,I2.2,a,I0)') TRIM(tmpstr),'wrfinput','_d',grid_id,'.',l
    !ELSE
      WRITE(wrffile,'(3a,I2.2)')      TRIM(tmpstr),'wrfinput','_d',grid_id
    !END IF
  CASE DEFAULT
    WRITE(*,'(1x,a,i0,a)') 'File name convention = ',fcvn,' is still not supported.'
    istatus = -1
  END SELECT

  IF (fmem > 0) THEN
    IF (fmem == 1) THEN
      cchar = '_'
    ELSE IF (fmem == 2) THEN
      cchar = '.'
    END IF
    tmpstr = wrffile
    WRITE(wrffile,'(2a,I0)') TRIM(tmpstr),cchar,l
  END IF

  RETURN
END SUBROUTINE get_wrf_file_name

!#######################################################################

SUBROUTINE get_wrf_grids( filein,numdigits,nx,ny,nz,                    &
                          P_QC,P_QR,P_QI,P_QS,P_QG,P_QH,                &
                          P_NR,P_NI,qnames,istatus )
!#######################################################################
!
! The procedure collect dimensions and grid file information from the
! pass-in file name.
!
! It calls subroutines in wrf_ncd_io.f90
!
!#######################################################################

  IMPLICIT NONE

  CHARACTER(LEN=256),INTENT(IN)  :: filein
  INTEGER,           INTENT(IN)  :: numdigits
  INTEGER,           INTENT(OUT) :: nx, ny, nz
  INTEGER,           INTENT(OUT) :: P_QC,P_QR,P_QI,P_QS,P_QG,P_QH
  INTEGER,           INTENT(OUT) :: P_NR,P_NI
  CHARACTER(LEN=40), INTENT(OUT) :: qnames(30)

  INTEGER,           INTENT(OUT) :: istatus

!---------------------------------------------------------------------

  INCLUDE 'mp.inc'

  CHARACTER(LEN=256) :: filename
  INTEGER :: ncid
  INTEGER :: nxin, nyin, nzin, iproj

  CHARACTER(LEN=20)  :: fmtstr

  INTEGER :: P_NC,P_NS,P_NG,P_NH, P_ZR,P_ZI,P_ZS,P_ZG,P_ZH,             &
             P_NCCN

  INTEGER :: idummy
  REAL    :: rdummy

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  WRITE(fmtstr,'(2(a,I0),a)') '(2a,I',numdigits,'.',numdigits,')'

  IF (nproc_x_in == 1 .AND. nproc_y_in == 1) THEN
    filename = filein
  ELSE
    WRITE(filename,fmt=TRIM(fmtstr)) TRIM(filein),'_',nproc_x_in*nproc_y_in-1
  END IF

  CALL open_ncd_wrf_file(filename, 'r', ncid, istatus)

  CALL get_wrf_dimensions( ncid, nxin,nyin,nzin,idummy,idummy,idummy,   &
                             P_QC,P_QR,P_QI,P_QS,P_QG,P_QH,             &
                             P_NC,P_NR,P_NI,P_NS,P_NG,P_NH,             &
                                  P_ZR,P_ZI,P_ZS,P_ZG,P_ZH,             &
                             P_NCCN,qnames,                             &
                             idummy,rdummy,rdummy,rdummy,rdummy,rdummy, &
                             rdummy,rdummy, istatus )

  IF (myproc == 0) WRITE(*,'(6x,3(a,i0))')                              &
       'WRF file dimensions are: nxin = ',nxin,  ', nyin = ',nyin,', nzin = ',nzin

  IF (nproc_x_in == 1 .AND. nproc_y_in == 1) THEN
    nx = nxin
    ny = nyin
    nz = nzin
  ELSE
    nx = (nxin-1) * nproc_x_in + 1
    ny = (nyin-1) * nproc_y_in + 1
    nz = nzin
  END IF

  nx = (nx-1)/nproc_x + 1   ! return local sizes
  ny = (ny-1)/nproc_y + 1

  CALL close_ncd_wrf_file(ncid,istatus)

  RETURN
END SUBROUTINE get_wrf_grids
