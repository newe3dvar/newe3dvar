!
!##################################################################
!##################################################################
!######                                                      ######
!######                   PROGRAM WRFDAUPDATE                ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################

PROGRAM wrf_daupdate

!#######################################################################
!
! PURPOSE: update WRF BC file based data analysis result in wrfinput file.
!          current version reads only wrf-netcdf file.
!
!-----------------------------------------------------------------------
!
! AUTHOR: Y. Wang (10/23/2013)
!
! MODIFICATION HISTORY:
!
!  01/27/2014 (Y. Wang)
!  Added capability to read WRF split files and extract subdomain from
!  WRF lateral boundary file.
!
!#######################################################################

   USE wrf_daupdate_namelist

   USE wrf_daupdate_netcdf_interface

   USE da_module_couple_uv, ONLY : da_couple_uv

   IMPLICIT NONE

!------------------------- Lateral boundary fields ---------------------

   CHARACTER(LEN=80), PARAMETER  :: BDYTITLE = "OUTPUT FROM WRFDAUPDATE V1.0 for WRFV3.6.1"

   INTEGER,           PARAMETER  :: num3d = 18, num2d = 5
   ! 3D need update
   CHARACTER(len=10), PARAMETER  :: var3d(num3d) = (/'PH        ',      &
                           'U         ','V         ','W         ',      &
                           'T         ','QVAPOR    ','QCLOUD    ',      &
                           'QRAIN     ','QICE      ','QSNOW     ',      &
                           'QGRAUP    ','QHAIL     ','QNCCN     ',      &
                           'QNCLOUD   ','QNRAIN    ','QNSNOW    ',      &
                           'QNICE     ','QNGRAUPEL '                    &
                            /)
   ! 2D need update
   CHARACTER(len=8), PARAMETER  :: varsf(num2d) = (/'MUB     ','MU      ',    &
                                         'MAPFAC_U','MAPFAC_V','MAPFAC_M' /)


   ! update_lateral_bdy
   REAL, ALLOCATABLE, DIMENSION(:,:)   :: mu, mub, msfu, msfv, msfm

   REAL, ALLOCATABLE, DIMENSION(:,:)   :: tend2d, scnd2d, frst2d
   REAL, ALLOCATABLE, DIMENSION(:,:,:) :: tend3d, scnd3d, frst3d

   REAL, POINTER :: u(:,:,:), v(:,:,:)

   REAL, POINTER :: full3d(:,:,:)

!-----------------------------------------------------------------------

   CHARACTER(LEN=10) :: bdyname(4), tenname(4)
   CHARACTER(len=20) :: var_pref, var_name, vbt_name

!----------------------- Time dependent variables ----------------------
  INTEGER, PARAMETER :: nvar_copy = 20, nvar_anal = 19

  CHARACTER(LEN=20) :: var_copy_names(nvar_copy) =(/                    &
                     'P                   ', 'PB                  ',    &
                     'PHB                 ', 'MUB                 ',    &
                     'Q2                  ', 'T2                  ',    &
                     'TH2                 ', 'PSFC                ',    &
                     'TSLB                ', 'SMOIS               ',    &
                     'SH2O                ',    &
                     'SEAICE              ', 'SNOW                ',    &
                     'SNOWH               ', 'CANWAT              ',    &
                     'TSK                 ', 'SNOWC               ',    &
                     'SR                  ', 'SST                 ',    &
                     'REFL_10CM           ' /)
                     ! 'SMCREL              ',

!----------------------- Analyzed variables by DA ----------------------

  CHARACTER(LEN=20) :: var_analysis_names(nvar_anal) =(/                &
                     'U                   ', 'V                   ',    &
                     'W                   ', 'PH                  ',    &
                     'T                   ', 'QVAPOR              ',    &
                     'QCLOUD              ', 'QRAIN               ',    &
                     'QICE                ', 'QGRAUP              ',    &
                     'QHAIL               ', 'QSNOW               ',    &
                     'QNCLOUD             ', 'QNRAIN              ',    &
                     'QNSNOW              ', 'QNICE               ',    &
                     'QNGRAUPEL           ', 'QNCCN               ',    &
                     'MU                  ' /)

!-----------------------------------------------------------------------

  INTEGER  :: nxd, nyd, nzd, nzs          ! domain size (staggered)
  INTEGER  :: ids, ide, jds, jde, kds, kde

  INTEGER :: i,j,k,l,m,n
  INTEGER :: ndims, dims(4)
  INTEGER :: east_end, north_end

  INTEGER :: time_id, itime1, itime2, itime

  INTEGER, ALLOCATABLE, TARGET :: iss(:),ise(:),jss(:),jse(:)  ! patch index  (unstaggered)
  INTEGER, ALLOCATABLE, TARGET :: ips(:),ipe(:),jps(:),jpe(:)  ! patch index  (staggered)

  INTEGER :: nexcl, nvarout, ndamiss
  CHARACTER(LEN=20) :: varexcl(100)
  CHARACTER(LEN=20) :: varoutnames(100), vardamiss(100)
  CHARACTER(LEN=1)  :: varoutstagger(100)
  INTEGER           :: varoutdim(4,100)

!-----------------------------------------------------------------------
!
! Misc. Local variables for Boundary
!
!-----------------------------------------------------------------------

  INTEGER :: UNTIN  = STDIN
  INTEGER :: UNTOUT = STDOUT
  INTEGER :: UNTERR = STDERR

  INTEGER :: istatus, nargs
  CHARACTER(LEN=256) :: NML_FILE, cstring(1)

  INTEGER, ALLOCATABLE :: dahandles(:)
  INTEGER, ALLOCATABLE :: tdhandles(:)
  INTEGER, ALLOCATABLE :: outhandles(:), inphandles(:)

  INTEGER :: bdyinhdl, bdyouthdl, bdyhdls(1)
  CHARACTER(LEN=19)  :: da_time, next_time_str(MAXTIMES), next_time
  INTEGER            :: next_itime(MAXTIMES)
  INTEGER            :: bdyfrq, bdyfrq_new, bdyda_intv
  INTEGER            :: bdymode, time_level

!-----------------------------------------------------------------------
!
! Misc. Local variables for Input
!
!-----------------------------------------------------------------------

  INTEGER :: dimx, dimy, dimz
  INTEGER :: nxo, nyo, nzo, nzso

  INTEGER, ALLOCATABLE, TARGET :: ioss(:), iose(:), iops(:), iope(:)
  INTEGER, ALLOCATABLE, TARGET :: joss(:), jose(:), jops(:), jope(:)

  INTEGER, ALLOCATABLE, TARGET :: idss(:), idse(:), idps(:), idpe(:)
  INTEGER, ALLOCATABLE, TARGET :: jdss(:), jdse(:), jdps(:), jdpe(:)

  INTEGER, POINTER, DIMENSION(:) :: ixs, ixe, iys, iye
  INTEGER, POINTER, DIMENSION(:) :: oxs, oxe, oys, oye

  INTEGER :: nxf_da, nyf_da, nxf_td, nyf_td, nxf_in, nyf_in, nxf_out, nyf_out

  INTEGER :: perturb_idx, perturb_ic_count, perturb_lbc_count
  INTEGER :: iseed0, iseed
  INTEGER :: idate(8)
  INTEGER :: stagdim
  REAL,   ALLOCATABLE :: perturb(:,:,:)
  REAL,   ALLOCATABLE :: tem1(:,:,:), tem2(:,:,:), htweights(:,:,:)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

!-----------------------------------------------------------------------
!
! Process command line for namelist input if present,
! otherwise read STDIN.
!
!-----------------------------------------------------------------------

  WRITE(6,'(14(/5x,a),/)')                                              &
      '###############################################################',&
      '###############################################################',&
      '#####                                                     #####',&
      '#####                 Welcome to WRFDAUPDATE              #####',&
      '#####                                                     #####',&
      '#####      Update WRF input and lateral boundary files    #####',&
      '#####                after a data analysis                #####',&
      '#####                                                     #####',&
      '#####                     Developed by                    #####',&
      '######       National Severe Storm Laboratory, NOAA       #####',&
      '#####                                                     #####',&
      '###############################################################',&
      '###############################################################'

  nargs = COMMAND_ARGUMENT_COUNT()
  IF (nargs == 1) THEN
    CALL GET_COMMAND_ARGUMENT(1, NML_FILE)
    CALL getunit( UNTIN )
    WRITE(UNTOUT,'(1x,3a)') 'Reading namelist parameters from file "',&
                            TRIM(NML_FILE),'"'
    OPEN(UNIT=UNTIN,FILE=TRIM(NML_FILE),STATUS='OLD',ACTION='READ',   &
         FORM='FORMATTED',IOSTAT=istatus)
  ELSE
    WRITE(UNTOUT,'(1x,a)') 'Reading namelist parameters from STDIN.'
  END IF

  IF (istatus /= 0) THEN
    WRITE(UNTOUT,'(1x,3a,I3)') 'IO ERROR with file ',      &
                 TRIM(NML_FILE),', with error code = ',istatus
    CALL arpsstop('IO ERROR with namelist file.',1)
  END IF

!-----------------------------------------------------------------------
!
! Read namelist
!
!-----------------------------------------------------------------------

  CALL read_namelist_params(UNTIN,UNTOUT,.TRUE.,istatus)
  IF (istatus /= 0) CALL arpsstop('ERROR: in read_namelist_params',istatus)

  IF ( UNTIN /= 5 ) THEN
    CLOSE(UNTIN)
    CALL retunit( UNTIN )
  END IF

!-----------------------------------------------------------------------
!
! Open DA files
!
!-----------------------------------------------------------------------

  ALLOCATE(dahandles(nfileda), STAT = istatus)

  !
  ! Open da files
  !
  CALL open_wrf_files(da_files,nfileda,dahandles,0,lvldbg,istatus)
  !print *,' istatus = ',istatus, nfileda

  ALLOCATE(iss(nfileda), STAT = istatus)
  ALLOCATE(ise(nfileda), STAT = istatus)
  ALLOCATE(ips(nfileda), STAT = istatus)
  ALLOCATE(ipe(nfileda), STAT = istatus)
  ALLOCATE(jss(nfileda), STAT = istatus)
  ALLOCATE(jse(nfileda), STAT = istatus)
  ALLOCATE(jps(nfileda), STAT = istatus)
  ALLOCATE(jpe(nfileda), STAT = istatus)

  CALL get_wrf_input_dims(dahandles,nfileda,nxd,nyd,nzd,nzs,            &
                          iss,ise,ips,ipe,jss,jse,jps,jpe,istatus)
  IF (istatus /= 0) THEN
    CALL arpsstop('Error in get_wrf_input_dims for analysis data files ("'//TRIM(da_file)//'").',1)
  END IF

  CALL get_wrf_input_time(dahandles(1),da_time,istatus)

  nxf_da = ipe(nfileda)-ips(nfileda)+1
  nyf_da = jpe(nfileda)-jps(nfileda)+1

!-----------------------------------------------------------------------
!
! Perturbation arrays
!
!-----------------------------------------------------------------------

  perturb_idx = 0
  perturb_ic_count  = 0
  perturb_lbc_count = 0
  IF (perturb_ic > 0 .OR. perturb_lbc > 0) THEN
    ALLOCATE(perturb(nxd,nyd,nzd),   STAT = istatus)

    ALLOCATE(htweights(nxd,nyd,nzd), STAT = istatus)
    ALLOCATE(tem1(nxd,nyd,nzd),      STAT = istatus)
    ALLOCATE(tem2(nxd,nyd,nzd),      STAT = istatus)

    DO k = 1, nzd
      htweights(:,:,k) = k
    END DO

    CALL DATE_AND_TIME(VALUES=idate)
    iseed0=idate(8)
  END IF

!-----------------------------------------------------------------------
!
! Process Lateral boundary conditions
!
!-----------------------------------------------------------------------

  IF (update_lateral_bdy) THEN

    !
    ! Open bdyin files
    !
    cstring(1) = wrf_bdy_in
    CALL open_wrf_files(cstring,1,bdyhdls,0,lvldbg,istatus)
    bdyinhdl = bdyhdls(1)

!-----------------------------------------------------------------------
!
! Get file dimensions
!
!-----------------------------------------------------------------------

    CALL get_bdyin_times(bdyinhdl,next_time_str,next_itime,time_level,istatus)

    CALL set_bdyout_mode(da_time,end_time_str,                          &
                         next_time_str,next_itime, time_level,          &
                         bdymode,itime1,itime2,bdyfrq_new,bdyda_intv,   &
                         lvldbg,istatus)
    IF (istatus /= 0) CALL arpsstop('ERROR: in set_bdyout_mode',istatus)

!    IF (lvldbg > 0) then
      WRITE(UNTOUT, FMT='(4(1x,2a,/))')                                 &
            'da_inittime  = ', trim(da_time),                           &
            'end_time_str = ', trim(end_time_str),                      &
            'frstbdytime  = ', trim(next_time_str(1)),                  &
            'lastbdytime  = ', trim(next_time_str(time_level))
      WRITE(UNTOUT, FMT='((1x,a,I0,/),(1x,a,I0,a,I0/),(1x,a,I0,/))')       &
            'bdy interval     = ',next_itime(itime1+1)-next_itime(itime1), &
            'bdy new interval = ',bdyfrq_new,', ',bdyda_intv,              &
            'bdy create mode  = ',bdymode
!    END IF

    !
    ! Create bdyout files
    !
    CALL create_bdyout_schema(wrf_bdy_out,wrf_bdy_in,define_bdy,lvldbg,istatus)
    IF (istatus /= 0) CALL arpsstop('ERROR: in create_bdyout_schema',istatus)

    cstring(1) = wrf_bdy_out
    CALL open_wrf_files(cstring,1,bdyhdls,2,lvldbg,istatus)
    bdyouthdl = bdyhdls(1)

    CALL find_bdy_datasets(bdyinhdl,num3d,var3d,                        &
                           nexcl,varexcl,nvarout,varoutnames,lvldbg,istatus)

!-----------------------------------------------------------------------
!
! Modify TITLE and START_DATE in wrf_bdy_out.
!
!-----------------------------------------------------------------------

    CALL fix_wrf_title(bdyhdls,1,bdytitle,da_time,lvldbg,istatus)

    CALL wrf_fix_times(bdyouthdl,da_time,next_time_str(itime1+1),lvldbg,istatus)

!-----------------------------------------------------------------------
!
! Read mu, msf etc.
!
!-----------------------------------------------------------------------

    ALLOCATE( mu  (nxd-1,nyd-1), STAT = istatus )
    ALLOCATE( mub (nxd-1,nyd-1), STAT = istatus )
    ALLOCATE( msfu(nxd,  nyd-1), STAT = istatus )
    ALLOCATE( msfv(nxd-1,nyd  ), STAT = istatus )
    ALLOCATE( msfm(nxd-1,nyd-1), STAT = istatus )

    ALLOCATE(inphandles(nfilein), STAT = istatus)

    CALL open_wrf_files(input_files,nfilein,inphandles,0,lvldbg,istatus)

    ALLOCATE(idss(nfilein), STAT = istatus)
    ALLOCATE(idse(nfilein), STAT = istatus)
    ALLOCATE(idps(nfilein), STAT = istatus)
    ALLOCATE(idpe(nfilein), STAT = istatus)
    ALLOCATE(jdss(nfilein), STAT = istatus)
    ALLOCATE(jdse(nfilein), STAT = istatus)
    ALLOCATE(jdps(nfilein), STAT = istatus)
    ALLOCATE(jdpe(nfilein), STAT = istatus)

    CALL get_wrf_input_dims(inphandles,nfilein,nxo,nyo,nzo,nzso,        &
                        idss,idse,idps,idpe,jdss,jdse,jdps,jdpe,istatus)
    IF (istatus /= 0) THEN
      CALL arpsstop('Error in get_wrf_input_dims for WRF input files ("'//TRIM(wrf_input_in)//'").',1)
    END IF

    IF (nxd /= nxo .OR. nyd /= nyo .OR. nzd /= nzo) THEN
      WRITE(UNTOUT,'(1x,a,/2(8x,3(a,I5),/))')                           &
            'ERROR: DA file domain must be matched with input domain.', &
            'DA    nx = ',nxd,', ny = ',nyd,', nz = ',nzd,              &
            'INPUT nx = ',nxo,', ny = ',nyo,', nz = ',nzo
      STOP 2
    END IF

    nxf_in = idpe(nfilein)-idps(nfilein)+1
    nyf_in = jdpe(nfilein)-jdps(nfilein)+1

    CALL init_netcdf_api(nxd,nyd,nzd,MAX(nxf_da,nxf_in),                &
                         MAX(nyf_da,nyf_in),5,istatus)

    CALL get_wrf_da2d(dahandles,nfileda,'MU        ',mu,nxd-1,nyd-1,    &
                      iss,ise,jss,jse,lvldbg,istatus)

    CALL get_wrf_da2d(inphandles,nfilein,'MUB       ',mub,nxd-1,nyd-1,  &
                      idss,idse,jdss,jdse,lvldbg,istatus)

    CALL get_wrf_da2d(inphandles,nfilein,'MAPFAC_U  ',msfu,nxd,  nyd-1, &
                      idps,idpe,jdss,jdse,lvldbg,istatus)

    CALL get_wrf_da2d(inphandles,nfilein,'MAPFAC_V  ',msfv,nxd-1,nyd,   &
                      idss,idse,jdps,jdpe,lvldbg,istatus)

    CALL get_wrf_da2d(inphandles,nfilein,'MAPFAC_M  ',msfm,nxd-1,nyd-1, &
                      idss,idse,jdss,jdse,lvldbg,istatus)


    DEALLOCATE(idss,idse,idps,idpe)
    DEALLOCATE(jdss,jdse,jdps,jdpe)

    CALL close_wrf_files(inphandles, nfilein, istatus)

    DEALLOCATE( inphandles )

!-----------------------------------------------------------------------
!
! Couple and write bdy
!
!-----------------------------------------------------------------------

    IF (time_level < itime1) THEN
       WRITE(unit=UNTOUT, fmt='(1x,a,i2,/,a)') 'time_level = ', time_level,  &
            'We need at least one time-level BDY.'
       CALL arpsstop('Wrong BDY file.',1)
    END IF

    east_end  = nxd
    north_end = nyd

    time_id  = 1

    IF( lvldbg > 0) write(unit=UNTOUT,fmt='(/1x,a/)') 'Processing the lateral boundary condition:'

    ! boundary variables
    bdyname(1)='_BXS'
    bdyname(2)='_BXE'
    bdyname(3)='_BYS'
    bdyname(4)='_BYE'

    ! boundary tendancy variables
    tenname(1)='_BTXS'
    tenname(2)='_BTXE'
    tenname(3)='_BTYS'
    tenname(4)='_BTYE'

    bdyfrq = next_itime(itime1+1)-next_itime(itime1)

!-----------------------------------------------------------------------
! 2D boundary tendency
!-----------------------------------------------------------------------

    DO m=1,4
      var_name='MU' // TRIM(bdyname(m))
      vbt_name='MU' // TRIM(tenname(m))

      CALL wrf_get_dims_bdy( bdyinhdl, TRIM(var_name), dims, ndims, lvldbg, istatus)

      ALLOCATE(frst2d(dims(1), dims(2)))
      ALLOCATE(scnd2d(dims(1), dims(2)))
      ALLOCATE(tend2d(dims(1), dims(2)))
      ! Get variable at second time level
      IF (itime1+1 < time_level ) THEN
         CALL wrf_get_bdy2d( bdyinhdl, TRIM(var_name), scnd2d,          &
                             dims(1), dims(2), itime1+1, lvldbg, istatus )
      ELSE
         CALL wrf_get_bdy2d( bdyinhdl, TRIM(var_name), frst2d,          &
                             dims(1), dims(2), itime1, lvldbg, istatus )
         CALL wrf_get_bdy2d( bdyinhdl, TRIM(vbt_name), tend2d,          &
                             dims(1), dims(2), itime1, lvldbg, istatus )

         scnd2d = frst2d + tend2d * bdyfrq

      END IF

      IF (lvldbg > 32) THEN
         WRITE(unit=UNTOUT, fmt='(1x,a,i2,3a,i2,a,4i6)')                &
              'No.', m, ', Variable: ', TRIM(vbt_name),                 &
              ', ndims =', ndims, ', dims = ', (dims(i), i=1,ndims)

         CALL wrf_get_bdy2d( bdyinhdl, TRIM(vbt_name), tend2d,          &
                             dims(1), dims(2), itime1, lvldbg, istatus )

         WRITE(unit=UNTOUT, fmt='(1x,a,10i12)') ' old ', (i, i=1,dims(2))
         DO j=1,dims(1)
            WRITE(unit=UNTOUT, fmt='(1x,i4, 1x, 10e20.7)')              &
                                  j, (tend2d(j,i), i=1,dims(2))
         END DO
      END IF

      SELECT CASE(m)
      CASE (1)              ! West boundary
        DO l=1,dims(2)
          DO j=1,dims(1)
             frst2d(j,l)=mu(l,j)
          END DO
        END DO
      CASE (2)              ! East boundary
        DO l=1,dims(2)
          DO j=1,dims(1)
             frst2d(j,l)=mu(east_end-l,j)
          END DO
        END DO
      CASE (3)              ! South boundary
        DO l=1,dims(2)
          DO i=1,dims(1)
             frst2d(i,l)=mu(i,l)
          END DO
        END DO
      CASE (4)              ! North boundary
        DO l=1,dims(2)
          DO i=1,dims(1)
             frst2d(i,l)=mu(i,north_end-l)
          END DO
        END DO
      CASE DEFAULT
         WRITE(unit=UNTOUT,fmt=*) 'It is impossible here. mu, m=', m
      END SELECT

      ! Calculate new tendancy
      DO l=1,dims(2)
        DO i=1,dims(1)
          tend2d(i,l)=(scnd2d(i,l)-frst2d(i,l))/bdyfrq_new
        END DO
      END DO

      IF (lvldbg > 32) THEN
        WRITE(unit=UNTOUT, fmt='(1x,a,i2,3a,i2,a,4i6)')                 &
             'No.', m, ', Variable: ', trim(vbt_name),                  &
             ', ndims = ', ndims, ', dims = ', (dims(i), i=1,ndims)

        WRITE(unit=UNTOUT, fmt='(1x,a,10i12)') ' new ', (i, i=1,dims(2))

        DO j=1,dims(1)
           WRITE(unit=UNTOUT, fmt='(1x,i4,1x,10e20.7)')                 &
                                       j, (tend2d(j,i), i=1,dims(2))
        END DO
      END IF

      ! output new variable at first time level
      CALL wrf_put_bdy2d( bdyouthdl, TRIM(var_name), frst2d,            &
                          dims(1), dims(2), time_id, lvldbg, istatus )
      ! output new tendancy
      CALL wrf_put_bdy2d( bdyouthdl, TRIM(vbt_name), tend2d,            &
                          dims(1), dims(2), time_id, lvldbg, istatus)

      DEALLOCATE(frst2d)
      DEALLOCATE(scnd2d)
      DEALLOCATE(tend2d)
    END DO

!-----------------------------------------------------------------------
! For 3D coupled variables (U, V)
!-----------------------------------------------------------------------

    ALLOCATE(u(nxd,  nyd-1,nzd-1), STAT = istatus)
    ALLOCATE(v(nxd-1,nyd,  nzd-1), STAT = istatus)

    ! Get U
    CALL get_wrf_da3d( dahandles, nfileda,'U         ', u,nxd,nyd-1,nzd-1,&
                       ips,ipe,jss,jse,lvldbg,istatus)

    perturb_idx = findname(perturb_lbc_vars,perturb_lbc,'U ')
    IF (perturb_idx > 0) THEN
      iseed = iseed0 + perturb_lbc_seed(perturb_idx)
      CALL normalrand(iseed,nxd*nyd*nzd,perturb)
      perturb(:,:,:) = perturb_ic_sd(perturb_idx)*perturb(:,:,:)
      IF (perturb_lbc_swg(perturb_idx) > 0) THEN
        tem2(:,:,:) = perturb(:,:,:)
        CALL smooth3d(nxd,nyd,nzd,1,nxd,1,nyd-1,1,nzd-1,1, &
                      perturb_lbc_swg(perturb_idx),htweights,tem2,tem1,perturb)
      END IF
      WHERE(perturb > perturb_lbc_max(perturb_idx))      perturb = perturb_lbc_max(perturb_idx)
      WHERE(perturb < -1.0*perturb_lbc_max(perturb_idx)) perturb = -1.0*perturb_lbc_max(perturb_idx)
      DO k = 1, nzd-1
        DO j = 1, nyd-1
          DO i = 1, nxd
            u(i,j,k) = u(i,j,k) + perturb(i,j,k)
          END DO
        END DO
      END DO
      perturb_lbc_count = perturb_lbc_count + 1
    END IF

    ! Get V
    CALL get_wrf_da3d( dahandles, nfileda,'V         ', v,nxd-1,nyd,nzd-1,&
                       iss,ise,jps,jpe,lvldbg, istatus)

    perturb_idx = findname(perturb_lbc_vars,perturb_lbc,'V ')
    IF (perturb_idx > 0) THEN
      iseed = iseed0 + perturb_lbc_seed(perturb_idx)
      CALL normalrand(iseed,nxd*nyd*nzd,perturb)
      perturb(:,:,:) = perturb_ic_sd(perturb_idx)*perturb(:,:,:)
      IF (perturb_lbc_swg(perturb_idx) > 0) THEN
        tem2(:,:,:) = perturb(:,:,:)
        CALL smooth3d(nxd,nyd,nzd,1,nxd-1,1,nyd,1,nzd-1,2, &
                      perturb_lbc_swg(perturb_idx),htweights,tem2,tem1,perturb)
      END IF
      WHERE(perturb > perturb_lbc_max(perturb_idx))      perturb = perturb_lbc_max(perturb_idx)
      WHERE(perturb < -1.0*perturb_lbc_max(perturb_idx)) perturb = -1.0*perturb_lbc_max(perturb_idx)
      DO k = 1, nzd-1
        DO j = 1, nyd
          DO i = 1, nxd-1
            v(i,j,k) = v(i,j,k) + perturb(i,j,k)
          END DO
        END DO
      END DO
      perturb_lbc_count = perturb_lbc_count + 1
    END IF

    IF (lvldbg > 2) THEN
       WRITE(unit=UNTOUT, fmt='(1x,a,f20.12)')                          &
            'Before couple Sample u =', u(nxd/2,nyd/2,nzd/2),           &
            'Before couple Sample v =', v(nxd/2,nyd/2,nzd/2)
    END IF

    !---------------------------------------------------------------------
    ! Couple u, v.
    ids = 1; ide = nxd-1
    jds = 1; jde = nyd-1
    kds = 1; kde = nzd-1

    CALL da_couple_uv ( u, v, mu, mub, msfu, msfv, ids, ide, jds, jde, kds, kde)

    IF (lvldbg > 2) THEN
       WRITE(unit=UNTOUT, fmt='(1x,a,f20.12)')                          &
            'After  couple Sample u =', u(nxd/2,nyd/2,nzd/2),           &
            'After  couple Sample v =', v(nxd/2,nyd/2,nzd/2)
    END IF

!-----------------------------------------------------------------------
! For 3D variables
!-----------------------------------------------------------------------

    DO n=1,num3d

      IF (lvldbg > 1)  &
        WRITE(unit=UNTOUT, fmt='(1x,a,i0, 2a)') '= Processing var3d(', n, ') : ', trim(var3d(n))

      IF ( name_in_set(var3d(n),varoutnames,nvarout) ) THEN

        perturb_idx = findname(perturb_lbc_vars,perturb_lbc,var3d(n))
        IF (perturb_idx > 0) THEN
          iseed = iseed0 + perturb_lbc_seed(perturb_idx)
          CALL normalrand(iseed,nxd*nyd*nzd,perturb)
          perturb(:,:,:) = perturb_ic_sd(perturb_idx)*perturb(:,:,:)
          WHERE(perturb > perturb_lbc_max(perturb_idx))      perturb = perturb_lbc_max(perturb_idx)
          WHERE(perturb < -1.0*perturb_lbc_max(perturb_idx)) perturb = -1.0*perturb_lbc_max(perturb_idx)
          perturb_lbc_count = perturb_lbc_count + 1
        END IF

        ! field in DA and also in BDY
        SELECT CASE(TRIM(var3d(n)))
        CASE ('U')            ! U
          east_end  = nxd+1
          north_end = nyd
          full3d => u
        CASE ('V')            ! V
          east_end  = nxd
          north_end = nyd+1
          full3d => v
        CASE ('W')

          IF (.NOT. use_input_w) THEN
            WRITE(unit=UNTOUT, fmt='(1x,3a,/,5x,3a,L,a)') 'INFO: Field ', trim(var3d(n)), &
               ' will be put in the interpolation list.',               &
               ' And will use background values in "',                  &
               TRIM(wrf_bdy_in),'" as use_input_w = ',use_input_w,'. '
            nexcl = nexcl + 1
            varexcl(nexcl) = TRIM(var3d(n))
            CYCLE
          END IF

          east_end  = nxd
          north_end = nyd

          ALLOCATE(full3d(nxd-1, nyd-1, nzd), STAT = istatus)

          CALL get_wrf_da3d( dahandles, nfileda, TRIM(var3d(n)),          &
                             full3d,nxd-1,nyd-1,nzd,                      &
                             iss,ise,jss,jse,lvldbg, istatus )

          IF (lvldbg > 2) THEN
             WRITE(unit=UNTOUT, fmt='(1x,3a,f20.12)')                     &
                  'Before couple Sample ', trim(var3d(n)),                &
                  '=', full3d(nxd/2,nyd/2,nzd/2)
          END IF

          !
          ! Add perturbations
          !
          IF (perturb_idx > 0) THEN
            IF (perturb_lbc_swg(perturb_idx) > 0) THEN
              tem2(:,:,:) = perturb(:,:,:)
              CALL smooth3d(nxd,nyd,nzd,1,nxd-1,1,nyd-1,1,nzd,3, &
                        perturb_lbc_swg(perturb_idx),htweights,tem2,tem1,perturb)
            END IF
            DO k = 1, nzd
              DO j = 1, nyd-1
                DO i = 1, nxd-1
                  full3d(i,j,k) = full3d(i,j,k) + perturb(i,j,k)
                END DO
              END DO
            END DO
          END IF

          DO k=1,nzd
            DO j=1,nyd-1
              DO i=1,nxd-1
                 full3d(i,j,k)=full3d(i,j,k)*(mu(i,j)+mub(i,j))/msfm(i,j)
              END DO
            END DO
          END DO

          IF (lvldbg > 2) THEN
             write(unit=UNTOUT, fmt='(1x,3a,f20.12)')                     &
                  'After  couple Sample ', trim(var3d(n)),                &
                  '=', full3d(nxd/2,nyd/2,nzd/2)
          END IF

        CASE ('T')

          east_end  = nxd
          north_end = nyd

          ALLOCATE(full3d(nxd-1, nyd-1, nzd-1), STAT = istatus)

          CALL get_wrf_da3d( dahandles, nfileda, trim(var3d(n)),          &
                             full3d,nxd-1,nyd-1,nzd-1,                    &
                             iss,ise,jss,jse,lvldbg,istatus )

          IF (lvldbg > 2) THEN
            WRITE(unit=UNTOUT, fmt='(1x,3a,e20.12,4x)')                   &
                 'Before couple Sample ', trim(var3d(n)),                 &
                 '=', full3d(nxd/2,nyd/2,nzd/2)
          END IF
          !
          ! Add perturbations
          !
          IF (perturb_idx > 0) THEN
            IF (perturb_lbc_swg(perturb_idx) > 0) THEN
              tem2(:,:,:) = perturb(:,:,:)
              CALL smooth3d(nxd,nyd,nzd,1,nxd-1,1,nyd-1,1,nzd-1,0, &
                      perturb_lbc_swg(perturb_idx),htweights,tem2,tem1,perturb)
            END IF
            DO k = 1, nzd-1
              DO j = 1, nyd-1
                DO i = 1, nxd-1
                  full3d(i,j,k) = full3d(i,j,k) + perturb(i,j,k)
                END DO
              END DO
            END DO
          END IF

          DO k=1,nzd-1
            DO j=1,nyd-1
              DO i=1,nxd-1
                full3d(i,j,k)=full3d(i,j,k)*(mu(i,j)+mub(i,j))
              END DO
            END DO
          END DO

          IF (lvldbg > 2) THEN
            WRITE(unit=UNTOUT, fmt='(1x,3a,e20.12,4x)')                   &
                 'After  couple Sample ', trim(var3d(n)),                 &
                 '=', full3d(nxd/2,nyd/2,nzd/2)
          END IF

        CASE ('PH')

          east_end  = nxd
          north_end = nyd

          ALLOCATE(full3d(nxd-1, nyd-1, nzd), STAT = istatus)

          CALL get_wrf_da3d( dahandles, nfileda, trim(var3d(n)),          &
                             full3d,nxd-1,nyd-1,nzd,                      &
                             iss,ise,jss,jse,lvldbg,istatus )

          IF (lvldbg > 2) THEN
            WRITE(unit=UNTOUT, fmt='(1x,3a,e20.12,4x)')                   &
                 'Before couple Sample ', trim(var3d(n)),                 &
                 '=', full3d(nxd/2,nyd/2,nzd/2)
          END IF

          !
          ! Add perturbations
          !
          IF (perturb_idx > 0) THEN
            IF (perturb_lbc_swg(perturb_idx) > 0) THEN
              tem2(:,:,:) = perturb(:,:,:)
              CALL smooth3d(nxd,nyd,nzd,1,nxd-1,1,nyd-1,1,nzd,3, &
                      perturb_lbc_swg(perturb_idx),htweights,tem2,tem1,perturb)
            END IF
            DO k = 1, nzd
              DO j = 1, nyd-1
                DO i = 1, nxd-1
                  full3d(i,j,k) = full3d(i,j,k) + perturb(i,j,k)
                END DO
              END DO
            END DO
          END IF

          DO k=1,nzd
            DO j=1,nyd-1
              DO i=1,nxd-1
                full3d(i,j,k)=full3d(i,j,k)*(mu(i,j)+mub(i,j))
              END DO
            END DO
          END DO

          IF (lvldbg > 2) THEN
            WRITE(unit=UNTOUT, fmt='(1x,3a,e20.12,4x)')                   &
                 'After  couple Sample ', trim(var3d(n)),                 &
                 '=', full3d(nxd/2,nyd/2,nzd/2)
          END IF

        CASE ('QVAPOR', 'QCLOUD', 'QRAIN', 'QICE', 'QSNOW', 'QGRAUP', 'QHAIL', &
              'QNCCN',  'QNCLOUD','QNRAIN','QNICE','QNSNOW','QNGRAUPEL'  )

          east_end  = nxd
          north_end = nyd

          ALLOCATE(full3d(nxd-1, nyd-1, nzd-1), STAT = istatus)

          CALL get_wrf_da3d( dahandles, nfileda, trim(var3d(n)),          &
                             full3d, nxd-1,nyd-1,nzd-1,                   &
                             iss,ise,jss,jse,lvldbg,istatus)
          IF (istatus /= 0) THEN
            WRITE(unit=UNTOUT, fmt='(1x,3a)')                             &
                  'INFO: Field ', trim(var3d(n)), ' does not exist in the DA file. Put in interpolation list.'
            nexcl = nexcl + 1
            varexcl(nexcl) = TRIM(var3d(n))
            CYCLE
          END IF

          IF (lvldbg > 2) THEN
             WRITE(unit=UNTOUT, fmt='(1x,3a,e20.12,4x)')                  &
                  'Before couple Sample ', trim(var3d(n)),                &
                  '=', full3d(nxd/2,nyd/2,nzd/2)
          END IF

          !
          ! Add perturbations
          !
          IF (perturb_idx > 0) THEN
            IF (perturb_lbc_swg(perturb_idx) > 0) THEN
              tem2(:,:,:) = perturb(:,:,:)
              CALL smooth3d(nxd,nyd,nzd,1,nxd-1,1,nyd-1,1,nzd-1,0, &
                      perturb_lbc_swg(perturb_idx),htweights,tem2,tem1,perturb)
            END IF
            DO k = 1, nzd-1
              DO j = 1, nyd-1
                DO i = 1, nxd-1
                  full3d(i,j,k) = full3d(i,j,k) + perturb(i,j,k)
                END DO
              END DO
            END DO
          END IF

          DO k=1,nzd-1
            DO j=1,nyd-1
              DO i=1,nxd-1
                 full3d(i,j,k)=full3d(i,j,k)*(mu(i,j)+mub(i,j))
              END DO
            END DO
          END DO

          IF (lvldbg > 2) THEN
             write(unit=UNTOUT, fmt='(1x,3a,e20.12,4x)')                  &
                  'After  couple Sample ', trim(var3d(n)),                &
                  '=', full3d(nxd/2,nyd/2,nzd/2)
          END IF

        CASE DEFAULT
          WRITE(unit=UNTOUT,fmt=*) 'It is impossible here. var3d(', n, ')=', trim(var3d(n))
        END SELECT

        var_pref=trim(var3d(n))

        do m=1,4
          var_name=trim(var_pref) // trim(bdyname(m))
          vbt_name=trim(var_pref) // trim(tenname(m))

          IF (lvldbg > 1) WRITE(unit=UNTOUT, fmt='(1x,a,i0, 2a)')         &
             '- Processing: bdyname(', m, ')=', trim(var_name)

          CALL wrf_get_dims_bdy( bdyinhdl, TRIM(var_name), dims, ndims, lvldbg, istatus)

          ALLOCATE(frst3d(dims(1), dims(2), dims(3)), STAT = istatus )
          ALLOCATE(scnd3d(dims(1), dims(2), dims(3)), STAT = istatus )
          ALLOCATE(tend3d(dims(1), dims(2), dims(3)), STAT = istatus )

          ! Get variable at second time level
          IF (itime1+1 < time_level) THEN
            CALL wrf_get_bdy3d( bdyinhdl, trim(var_name), scnd3d,         &
                                dims(1), dims(2), dims(3), itime1+1, lvldbg, istatus)
          ELSE
            CALL wrf_get_bdy3d( bdyinhdl, trim(var_name), frst3d,         &
                                dims(1), dims(2), dims(3), itime1, lvldbg, istatus)
            CALL wrf_get_bdy3d( bdyinhdl, trim(vbt_name), tend3d,         &
                                dims(1), dims(2), dims(3), itime1, lvldbg, istatus)
            scnd3d = frst3d+tend3d*bdyfrq
          END IF

          IF (lvldbg > 32) THEN
            WRITE(unit=UNTOUT, fmt='(1x,a,i2,3a,i2,a,4i6)')               &
                 'No.', m, ', Variable: ', trim(vbt_name),                &
                 ', ndims = ', ndims, ', dims = ', (dims(i), i=1,ndims)

            CALL wrf_get_bdy3d( bdyinhdl, trim(vbt_name), tend3d,         &
                                dims(1), dims(2), dims(3), itime1, lvldbg, istatus)

            WRITE(unit=UNTOUT, fmt='(a, 10i20)') ' old ', (i, i=1,dims(3))
            DO j=1,dims(1)
              WRITE(unit=UNTOUT, fmt='(i4, 1x, 10e20.7)')                 &
                    j, (tend3d(j,dims(2)/2,i), i=1,dims(3))
            END DO

          END IF

          SELECT CASE(TRIM(bdyname(m)))
          CASE ('_BXS')              ! West boundary
            DO l=1,dims(3)
              DO k=1,dims(2)
                DO j=1,dims(1)
                   frst3d(j,k,l)=full3d(l,j,k)
                END DO
              END DO
            END DO
          CASE ('_BXE')              ! East boundary
            DO l=1,dims(3)
              DO k=1,dims(2)
                DO j=1,dims(1)
                  frst3d(j,k,l)=full3d(east_end-l,j,k)
                END DO
              END DO
            END DO
          CASE ('_BYS')              ! South boundary
            DO l=1,dims(3)
              DO k=1,dims(2)
                DO i=1,dims(1)
                  frst3d(i,k,l)=full3d(i,l,k)
                END DO
              END DO
            END DO
          CASE ('_BYE')              ! North boundary
            DO l=1,dims(3)
              DO k=1,dims(2)
                DO i=1,dims(1)
                  frst3d(i,k,l)=full3d(i,north_end-l,k)
                END DO
              END DO
            END DO
          CASE DEFAULT
            WRITE(unit=UNTOUT,fmt=*) 'It is impossible here.'
            WRITE(unit=UNTOUT,fmt=*) 'bdyname(', m, ')=', trim(bdyname(m))
            STOP 3
          END SELECT

          IF (lvldbg > 2)  WRITE(unit=UNTOUT, fmt='(3x,a,i0,2a)')         &
             'Cal. tend bdyname(', m, ') : ', trim(vbt_name)

          ! calculate new tendancy
          DO l=1,dims(3)
            DO k=1,dims(2)
              DO i=1,dims(1)
                tend3d(i,k,l)=(scnd3d(i,k,l)-frst3d(i,k,l))/bdyfrq_new
              END DO
            END DO
          END DO

          IF (lvldbg > 32) THEN
            WRITE(unit=UNTOUT, fmt='(a,i2,3a,i2,a,4i6)')                  &
                 'No.', m, ', Variable: ', trim(vbt_name),                &
                 ', ndims = ', ndims, ', dims = ', (dims(i), i=1,ndims)

            WRITE(unit=UNTOUT, fmt='(a, 10i20)') ' new ', (i, i=1,dims(3))

            DO j=1,dims(1)
              WRITE(unit=UNTOUT, fmt='(i4, 1x, 10e20.7)')                 &
                    j, (tend3d(j,dims(2)/2,i), i=1,dims(3))
            END DO

          END IF

          ! output new variable at first time level
          CALL wrf_put_bdy3d( bdyouthdl, trim(var_name), frst3d,          &
                              dims(1), dims(2), dims(3), time_id, lvldbg, istatus)
          CALL wrf_put_bdy3d( bdyouthdl, trim(vbt_name), tend3d,          &
                              dims(1), dims(2), dims(3), time_id, lvldbg, istatus)

          DEALLOCATE(frst3d)
          DEALLOCATE(scnd3d)
          DEALLOCATE(tend3d)
        END DO

        DEALLOCATE(full3d)
      ELSE
        WRITE(unit=UNTOUT, fmt='(1x,3a)') 'INFO: Field ', trim(var3d(n)),' is not in the BDY file.'
      END IF

    END DO
    DEALLOCATE(mu, mub, msfu, msfv, msfm)
    !DEALLOCATE(u, v)

!-----------------------------------------------------------------------
!
! Copy other boundary fields at initial time
!
!-----------------------------------------------------------------------

    IF (nexcl > 0) THEN     ! Ignore HT

      !IF ( bdymode == 1) THEN  ! Do time interpolation
        itime   = itime1
        time_id = 1

        DO n = 1, nexcl
          var_pref=trim(varexcl(n))

          IF (lvldbg > 1)  &
            WRITE(unit=UNTOUT, fmt='(1x,a,i0, 2a)') '% Processing varexcl(', n, ') : ', trim(var_pref)

          !
          ! Do time interpolation for time valid fields
          !
          DO m = 1, 4
            var_name=trim(var_pref) // trim(bdyname(m))
            vbt_name=trim(var_pref) // trim(tenname(m))

            IF (lvldbg > 1) WRITE(unit=UNTOUT, fmt='(1x,a,i0, 2a)')     &
               '- Processing: bdyname(', m, ')=', trim(var_name)

            CALL wrf_get_dims_bdy( bdyinhdl, TRIM(var_name), dims, ndims, lvldbg, istatus)

            IF (ndims == 3) THEN ! 2D variables
              ALLOCATE(frst2d(dims(1), dims(2) ), STAT = istatus )
              ALLOCATE(tend2d(dims(1), dims(2) ), STAT = istatus )

              ! Get variables and its tendency
              CALL wrf_get_bdy2d( bdyinhdl, trim(var_name), frst2d,     &
                                  dims(1), dims(2), itime1, lvldbg, istatus)
              CALL wrf_get_bdy2d( bdyinhdl, trim(vbt_name), tend2d,     &
                                  dims(1), dims(2), itime1, lvldbg, istatus)

              ! calculate new value at da_time
              DO k=1,dims(2)
                DO i=1,dims(1)
                  frst2d(i,k)= frst2d(i,k)+tend2d(i,k)*bdyda_intv
                END DO
              END DO

              ! output new variable at first time level
              CALL wrf_put_bdy2d( bdyouthdl, trim(var_name), frst2d,    &
                              dims(1), dims(2), time_id, lvldbg, istatus)
              CALL wrf_put_bdy2d( bdyouthdl, trim(vbt_name), tend2d,    &
                              dims(1), dims(2), time_id, lvldbg, istatus)

              DEALLOCATE(frst2d)
              DEALLOCATE(tend2d)

            ELSE

              ALLOCATE(frst3d(dims(1), dims(2), dims(3)), STAT = istatus )
              ALLOCATE(tend3d(dims(1), dims(2), dims(3)), STAT = istatus )

              ! Get variables and its tendency
              CALL wrf_get_bdy3d( bdyinhdl, trim(var_name), frst3d,         &
                                  dims(1), dims(2), dims(3), itime1, lvldbg, istatus)
              CALL wrf_get_bdy3d( bdyinhdl, trim(vbt_name), tend3d,         &
                                  dims(1), dims(2), dims(3), itime1, lvldbg, istatus)

              ! calculate new value at da_time
              DO l=1,dims(3)
                DO k=1,dims(2)
                  DO i=1,dims(1)
                    frst3d(i,k,l)= frst3d(i,k,l)+tend3d(i,k,l)*bdyda_intv
                  END DO
                END DO
              END DO

              ! output new variable at first time level
              CALL wrf_put_bdy3d( bdyouthdl, trim(var_name), frst3d,          &
                              dims(1), dims(2), dims(3), time_id, lvldbg, istatus)
              CALL wrf_put_bdy3d( bdyouthdl, trim(vbt_name), tend3d,          &
                              dims(1), dims(2), dims(3), time_id, lvldbg, istatus)

              DEALLOCATE(frst3d)
              DEALLOCATE(tend3d)
            END IF

          END DO

        END DO

      !ELSE IF ( bdymode == 2 ) THEN    ! copy those variables that are not analyzed
      !  itime   = itime1
      !  time_id = 1
      !
      !  DO n = 1, nexcl
      !    var_pref=trim(varexcl(n))
      !
      !    IF (lvldbg > 0) WRITE(UNTOUT,'(1x,a,a8,a,I0,a,I0,a)')         &
      !      'Copying ',TRIM(var_pref),' from ',itime,' to ',time_id,' ...'
      !
      !    DO m = 1, 4
      !      var_name=trim(var_pref) // trim(bdyname(m))
      !      vbt_name=trim(var_pref) // trim(tenname(m))
      !
      !      CALL wrf_copy_bdy(bdyinhdl,itime,var_name,bdyouthdl,time_id,lvldbg,istatus)
      !      CALL wrf_copy_bdy(bdyinhdl,itime,vbt_name,bdyouthdl,time_id,lvldbg,istatus)
      !    END DO
      !  END DO
      !
      !ELSE                      ! We do not know how to set those variables
      !  WRITE(unit=UNTOUT, fmt='(1x,a,/,8x,a)')                         &
      !       'ERROR: there are extra variables that are not analyzed by the DA system.', &
      !       'This program do not know how to set them in the boundary file.'
      !  CALL arpsstop('ERROR: Extra variables that are not analyzed by the DA system.',1)
      !
      !END IF
    END IF

!-----------------------------------------------------------------------
!
! Copy fields other than initial time
!
!-----------------------------------------------------------------------


    !IF ( bdymode == 2) THEN

      DO itime = itime1+1,itime2-1
        time_id = itime - itime1+1

        IF (lvldbg > 0) WRITE(UNTOUT,'(1x,a,I0,a)')                     &
          '-------------- bdy time level = ',time_id,' ----------------'

        IF (lvldbg > 0) WRITE(UNTOUT,'(1x,a,I0,a,I0,a)')                &
          'Copying    Times from ',itime,' to ',time_id,' ...'

        CALL wrf_copy_times(bdyinhdl,itime,bdyouthdl,time_id,lvldbg,istatus)

        var_pref='MU'
        IF (lvldbg > 0) WRITE(UNTOUT,'(1x,a,a8,a,I0,a,I0,a)')           &
          'Copying ',TRIM(var_pref),' from ',itime,' to ',time_id,' ...'

        DO m=1,4
          var_name=trim(var_pref) // trim(bdyname(m))
          vbt_name=trim(var_pref) // trim(tenname(m))

          CALL wrf_copy_bdy(bdyinhdl,itime,var_name,bdyouthdl,time_id,lvldbg,istatus)
          CALL wrf_copy_bdy(bdyinhdl,itime,vbt_name,bdyouthdl,time_id,lvldbg,istatus)

        END DO

        DO n = 1, nvarout
          var_pref=trim(varoutnames(n))

          IF (lvldbg > 0) WRITE(UNTOUT,'(1x,a,a8,a,I0,a,I0,a)')         &
            'Copying ',TRIM(var_pref),' from ',itime,' to ',time_id,' ...'

          DO m = 1, 4
            var_name=trim(var_pref) // trim(bdyname(m))
            vbt_name=trim(var_pref) // trim(tenname(m))

            CALL wrf_copy_bdy(bdyinhdl,itime,var_name,bdyouthdl,time_id,lvldbg,istatus)
            CALL wrf_copy_bdy(bdyinhdl,itime,vbt_name,bdyouthdl,time_id,lvldbg,istatus)
          END DO
        END DO

      END DO
    !END IF

!-----------------------------------------------------------------------
!
! Terminate this section
!
!-----------------------------------------------------------------------


    !
    ! Close files
    !
    bdyhdls(1) = bdyinhdl
    CALL close_wrf_files(bdyhdls,  1,       istatus)
    bdyhdls(1) = bdyouthdl
    CALL close_wrf_files(bdyhdls,  1,       istatus)

  END IF

!-----------------------------------------------------------------------
!
! Update wrfinput file
!
!-----------------------------------------------------------------------

  IF (update_input) THEN

    !CALL copy_wrf_file_hard( nfileout,input_files,output_files,         &
    !                         lvldbg,istatus )

    !
    ! Variables that do not need Copying
    !
    nexcl = nvar_anal
    DO n = 1, nvar_anal
      varexcl(n) = var_analysis_names(n)
    END DO

    IF (update_input_copy_td) THEN
      DO n = 1, nvar_copy
        nexcl = nexcl + 1
        varexcl(nexcl) = var_copy_names(n)
      END DO
    END IF

    CALL copy_wrf_file( nfilein,input_files,nfileout,output_files,      &
                        copy_input,nproc_x_out,nproc_y_out,             &
                        nexcl, varexcl,                                 &
                        nvarout,varoutnames,varoutstagger,varoutdim,    &
                        lvldbg,istatus )

    ALLOCATE(outhandles(nfileout), STAT = istatus)

    CALL open_wrf_files(output_files,nfileout,outhandles,2,lvldbg,istatus)

    ALLOCATE(ioss(nfileout), STAT = istatus)
    ALLOCATE(iose(nfileout), STAT = istatus)
    ALLOCATE(iops(nfileout), STAT = istatus)
    ALLOCATE(iope(nfileout), STAT = istatus)
    ALLOCATE(joss(nfileout), STAT = istatus)
    ALLOCATE(jose(nfileout), STAT = istatus)
    ALLOCATE(jops(nfileout), STAT = istatus)
    ALLOCATE(jope(nfileout), STAT = istatus)

    CALL get_wrf_input_dims(outhandles,nfileout,nxo,nyo,nzo,nzso,       &
                        ioss,iose,iops,iope,joss,jose,jops,jope,istatus)
    IF (istatus /= 0) THEN
      CALL arpsstop('Error in get_wrf_input_dims for output files ("'//TRIM(wrf_input_out)//'").',1)
    END IF

    IF (nxd /= nxo .OR. nyd /= nyo .OR. nzd /= nzo) THEN
      WRITE(UNTOUT,'(1x,a,/2(8x,3(a,I5),/))')                           &
            'ERROR: DA file domain must be matched with input domain.', &
            'DA    nx = ',nxd,', ny = ',nyd,', nz = ',nzd,              &
            'INPUT nx = ',nxo,', ny = ',nyo,', nz = ',nzo
      STOP 2
    END IF

    nxf_out = iope(nfileout)-iops(nfileout)+1
    nyf_out = jope(nfileout)-jops(nfileout)+1

    CALL init_netcdf_api(nxd,nyd,nzd,MAX(nxf_da,nxf_out),               &
                         MAX(nyf_da,nyf_out),1,istatus)

!-----------------------------------------------------------------------
!
! Modify TITLE and START_DATE in wrfinput_out.
!
!-----------------------------------------------------------------------

    CALL fix_wrf_title(outhandles,nfileout,bdytitle,da_time,lvldbg,istatus)

    CALL fix_wrf_times(outhandles,nfileout,da_time,lvldbg,istatus)

!-----------------------------------------------------------------------
!
! Copy time-independent static file from input template to output file
!
!-----------------------------------------------------------------------

    IF (nvarout > 0) THEN

      ALLOCATE(inphandles(nfilein), STAT = istatus)

      CALL open_wrf_files(input_files,nfilein,inphandles,0,lvldbg,istatus)

      ALLOCATE(idss(nfilein), STAT = istatus)
      ALLOCATE(idse(nfilein), STAT = istatus)
      ALLOCATE(idps(nfilein), STAT = istatus)
      ALLOCATE(idpe(nfilein), STAT = istatus)
      ALLOCATE(jdss(nfilein), STAT = istatus)
      ALLOCATE(jdse(nfilein), STAT = istatus)
      ALLOCATE(jdps(nfilein), STAT = istatus)
      ALLOCATE(jdpe(nfilein), STAT = istatus)

      CALL get_wrf_input_dims(inphandles,nfilein,nxo,nyo,nzo,nzso,        &
                          idss,idse,idps,idpe,jdss,jdse,jdps,jdpe,istatus)
      IF (istatus /= 0) THEN
        CALL arpsstop('Error in get_wrf_input_dims for input template files ("'//TRIM(wrf_input_in)//'").',1)
      END IF

      !IF (nxd /= nxo .OR. nyd /= nyo .OR. nzd /= nzo) THEN
      !  WRITE(UNTOUT,'(1x,a,/2(8x,3(a,I5),/))')                           &
      !        'ERROR: DA file domain must be matched with input domain.', &
      !        'DA    nx = ',nxd,', ny = ',nyd,', nz = ',nzd,              &
      !        'INPUT nx = ',nxo,', ny = ',nyo,', nz = ',nzo
      !  STOP 2
      !END IF

      nxf_in = idpe(nfilein)-idps(nfilein)+1
      nyf_in = jdpe(nfilein)-jdps(nfilein)+1

      CALL init_netcdf_api(nxd,nyd,nzd,MAX(nxf_in,nxf_out),             &
                           MAX(nyf_in,nyf_out),1,istatus)

      DO n = 1, nvarout
        var_pref = TRIM(varoutnames(n))

        IF (lvldbg > 0) &
          WRITE(UNTOUT, '(1x,5a,I0,a)') 'Copying field ',TRIM(var_pref),&
                        ' (',varoutstagger(n),',',varoutdim(3,n),') ...'

        IF (varoutstagger(n) == 'X') THEN   ! U stagger
          dimx = nxd;   dimy = nyd-1; dimz = varoutdim(3,n)

          ixs  => idps; ixe  => idpe; iys  => jdss; iye  => jdse
          oxs  => iops; oxe  => iope; oys  => joss; oye  => jose

        ELSE IF (varoutstagger(n) == 'Y') THEN  ! V stagger
          dimx = nxd-1; dimy = nyd;   dimz = varoutdim(3,n)

          ixs  => idss; ixe  => idse; iys  => jdps; iye  => jdpe
          oxs  => ioss; oxe  => iose; oys  => jops; oye  => jope

        ELSE  ! IF (varoutstagger(n) == '')  THEN  ! Mass
          dimx = nxd-1; dimy = nyd-1; dimz = varoutdim(3,n)

          ixs  => idss; ixe  => idse; iys  => jdss; iye  => jdse
          oxs  => ioss; oxe  => iose; oys  => joss; oye  => jose
        !ELSE
        !  WRITE(UNTOUT, '(1x,5a)') 'Unsupported field ',TRIM(var_pref), &
        !  ' when copying from input template file with stagger =',varoutstagger(n),'.'
        !  STOP 3
        END IF

        ALLOCATE(full3d(dimx,dimy,dimz), STAT = istatus)

        IF (dimz > 1) THEN
          CALL get_wrf_da3d( inphandles, nfilein, trim(var_pref),       &
                             full3d,dimx,dimy,dimz,                     &
                             ixs,ixe,iys,iye,lvldbg,istatus)

          CALL put_wrf_da3d( outhandles, nfileout, trim(var_pref),      &
                             full3d, dimx,dimy,dimz,                    &
                             oxs,oxe,oys,oye,lvldbg,istatus)
        ELSE
          CALL get_wrf_da2d( inphandles, nfilein, trim(var_pref),       &
                             full3d, dimx,dimy,                         &
                             ixs,ixe,iys,iye,lvldbg,istatus)

          CALL put_wrf_da2d( outhandles, nfileout, trim(var_pref),      &
                             full3d, dimx,dimy,                         &
                             oxs,oxe,oys,oye,lvldbg,istatus)
        END IF

        DEALLOCATE(full3d)

      END DO

      DEALLOCATE(idss,idse,idps,idpe)
      DEALLOCATE(jdss,jdse,jdps,jdpe)

      CALL close_wrf_files(inphandles, nfilein, istatus)

      DEALLOCATE( inphandles )

    END IF

!-----------------------------------------------------------------------
!
! Copy DA fields to output files
!
!-----------------------------------------------------------------------

    ndamiss = 0
    DO n = 1, nvar_anal

      var_pref = TRIM(var_analysis_names(n))

      IF (lvldbg > 0) &
        WRITE(UNTOUT, '(1x,a)') 'Updating field '//TRIM(var_pref)//' ...'

      SELECT CASE(TRIM(var_pref))
      CASE ('U')
        stagdim = 1
        dimx = nxd
        dimy = nyd-1
        dimz = nzd-1

        ixs  => ips
        ixe  => ipe
        iys  => jss
        iye  => jse

        oxs  => iops
        oxe  => iope
        oys  => joss
        oye  => jose
      CASE ('V')
        stagdim = 2
        dimx = nxd-1
        dimy = nyd
        dimz = nzd-1

        ixs  => iss
        ixe  => ise
        iys  => jps
        iye  => jpe

        oxs  => ioss
        oxe  => iose
        oys  => jops
        oye  => jope

      CASE ('W','PH','PHB')
        stagdim = 3
        dimx = nxd-1
        dimy = nyd-1
        dimz = nzd

        ixs  => iss
        ixe  => ise
        iys  => jss
        iye  => jse

        oxs  => ioss
        oxe  => iose
        oys  => joss
        oye  => jose
      CASE ('MU')
        dimx = nxd-1
        dimy = nyd-1
        dimz = 1

        ixs  => iss
        ixe  => ise
        iys  => jss
        iye  => jse

        oxs  => ioss
        oxe  => iose
        oys  => joss
        oye  => jose
      CASE DEFAULT
        stagdim = 0
        dimx = nxd-1
        dimy = nyd-1
        dimz = nzd-1

        ixs  => iss
        ixe  => ise
        iys  => jss
        iye  => jse

        oxs  => ioss
        oxe  => iose
        oys  => joss
        oye  => jose

      END SELECT

      ALLOCATE(full3d(dimx,dimy,dimz), STAT = istatus)

      IF (lvldbg > 2) THEN
        WRITE(UNTOUT, '(3x,a,3I7)') 'Full   dimensions: ',dimx,dimy,dimz
        DO i = 1, nfileda
          WRITE(UNTOUT,'(3x,a,I5,a,4I6)') 'input  dimensions ',i,':',ixs(i),ixe(i),iys(i),iye(i)
        END DO

        DO i = 1, nfileout
          WRITE(UNTOUT,'(3x,a,I5,a,4I6)') 'Output dimensions ',i,':',oxs(i),oxe(i),oys(i),oye(i)
        END DO
      END IF

      IF (dimz > 1) THEN
        CALL get_wrf_da3d( dahandles, nfileda, trim(var_pref),          &
                           full3d, dimx,dimy,dimz,                      &
                           ixs,ixe,iys,iye,lvldbg,istatus)
        IF (istatus /= 0) THEN
          WRITE(unit=UNTOUT, fmt='(1x,3a)')                             &
                'INFO: Field ', trim(var_pref), ' does not exist. Put in DAmiss list.'
          ndamiss = ndamiss+1
          vardamiss(ndamiss) = TRIM(var_pref)
          CYCLE
        END IF

        !---------------------------------------------------------------
        !
        ! Add perturbation to INPUT 3D fields
        !
        !---------------------------------------------------------------

        perturb_idx = findname(perturb_ic_vars,perturb_ic,var_pref)
        IF (perturb_idx > 0) THEN
          iseed = iseed0 + perturb_ic_seed(perturb_idx)
          CALL normalrand(iseed,nxd*nyd*nzd,perturb)
          perturb(:,:,:) = perturb_ic_sd(perturb_idx)*perturb(:,:,:)
          IF (perturb_ic_swg(perturb_idx) > 0) THEN
            tem2(:,:,:) = perturb(:,:,:)
            CALL smooth3d(nxd,nyd,nzd,1,dimx,1,dimy,1,dimz,stagdim, &
                      perturb_ic_swg(perturb_idx),htweights,tem2,tem1,perturb)
          END IF
          WHERE(perturb > perturb_ic_max(perturb_idx))      perturb = perturb_ic_max(perturb_idx)
          WHERE(perturb < -1.0*perturb_ic_max(perturb_idx)) perturb = -1.0*perturb_ic_max(perturb_idx)
          DO k = 1, dimz
            DO j = 1, dimy
              DO i = 1, dimx
                full3d(i,j,k) = full3d(i,j,k) + perturb(i,j,k)
              END DO
            END DO
          END DO
          perturb_ic_count = perturb_ic_count + 1
        END IF

        CALL put_wrf_da3d( outhandles, nfileout, trim(var_pref),        &
                           full3d, dimx,dimy,dimz,                      &
                           oxs,oxe,oys,oye,lvldbg,istatus)
      ELSE
        CALL get_wrf_da2d( dahandles, nfileda, trim(var_pref),          &
                           full3d, dimx,dimy,                           &
                           ixs,ixe,iys,iye,lvldbg,istatus)
        IF (istatus /= 0) THEN
          WRITE(unit=UNTOUT, fmt='(1x,3a)')                             &
                'INFO: Field ', trim(var_pref), ' does not exist. Skipped'
          CYCLE
        END IF

        CALL put_wrf_da2d( outhandles, nfileout, trim(var_pref),        &
                           full3d, dimx,dimy,                           &
                           oxs,oxe,oys,oye,lvldbg,istatus)
      END IF

      DEALLOCATE(full3d)

    END DO

!-----------------------------------------------------------------------
!
! Copy time dependent fields from WRF OUT file to output files
!
!-----------------------------------------------------------------------

    IF (update_input_copy_td) THEN


      ALLOCATE(tdhandles(nfiletd), STAT = istatus)

      !
      ! Open da files
      !
      CALL open_wrf_files(td_files,nfiletd,tdhandles,0,lvldbg,istatus)
      !print *,' istatus = ',istatus, nfileda

      ALLOCATE(idss(nfiletd), STAT = istatus)
      ALLOCATE(idse(nfiletd), STAT = istatus)
      ALLOCATE(idps(nfiletd), STAT = istatus)
      ALLOCATE(idpe(nfiletd), STAT = istatus)
      ALLOCATE(jdss(nfiletd), STAT = istatus)
      ALLOCATE(jdse(nfiletd), STAT = istatus)
      ALLOCATE(jdps(nfiletd), STAT = istatus)
      ALLOCATE(jdpe(nfiletd), STAT = istatus)

      CALL get_wrf_input_dims(tdhandles,nfiletd,nxo,nyo,nzo,nzso,       &
                       idss,idse,idps,idpe,jdss,jdse,jdps,jdpe,istatus)
      IF (istatus /= 0) THEN
        CALL arpsstop('Error in get_wrf_input_dims for time dependent files ("'//TRIM(wrf_td_in)//'").',1)
      END IF

      CALL get_wrf_input_time(tdhandles(1),next_time,istatus)

      IF (next_time /= da_time) THEN
        WRITE(UNTOUT,'(1x,a,/,2(8x,a,a,/))')                            &
             'ERROR: DA file time is not matched with wrfout files.',   &
             'Found:    ', next_time, 'Expected: ', da_time
        STOP 3
      END IF

      nxf_td = idpe(nfiletd)-idps(nfiletd)+1
      nyf_td = jdpe(nfiletd)-jdps(nfiletd)+1

      CALL init_netcdf_api(nxd,nyd,nzd,MAX(nxf_td,nxf_out),             &
                           MAX(nyf_td,nyf_out),1,istatus)

      !
      ! copy explicitly specified fields in var_copy_names
      !
      DO n = 1, nvar_copy
        var_pref = TRIM(var_copy_names(n))

        IF (lvldbg > 0) &
          WRITE(UNTOUT, '(1x,a)') 'Copying field '//TRIM(var_pref)//' ...'

        SELECT CASE(TRIM(var_pref))
        CASE ( 'P', 'PB', 'qke_adv' )    ! 3D atmospheric variable
          ALLOCATE(full3d(nxd-1,nyd-1,nzd-1), STAT = istatus)

          CALL get_wrf_da3d( tdhandles, nfiletd, trim(var_pref),        &
                             full3d, nxd-1,nyd-1,nzd-1,                 &
                             idss,idse,jdss,jdse,lvldbg,istatus)

          CALL put_wrf_da3d( outhandles, nfileout, trim(var_pref),      &
                             full3d, nxd-1,nyd-1,nzd-1,                 &
                             ioss,iose,joss,jose,lvldbg,istatus)
        CASE ( 'PHB' )       ! 3D staggered atmospheric variable
          ALLOCATE(full3d(nxd-1,nyd-1,nzd), STAT = istatus)

          CALL get_wrf_da3d( tdhandles, nfiletd, trim(var_pref),        &
                             full3d, nxd-1,nyd-1,nzd,                   &
                             idss,idse,jdss,jdse,lvldbg,istatus)

          CALL put_wrf_da3d( outhandles, nfileout, trim(var_pref),      &
                             full3d, nxd-1,nyd-1,nzd,                   &
                             ioss,iose,joss,jose,lvldbg,istatus)

        CASE ('TSLB','SMOIS','SH2O')   ! 3D soil variable

          ALLOCATE(full3d(nxd-1,nyd-1,nzs), STAT = istatus)

          CALL get_wrf_da3d( tdhandles, nfiletd, trim(var_pref),        &
                             full3d, nxd-1,nyd-1,nzs,                   &
                             idss,idse,jdss,jdse,lvldbg,istatus)

          CALL put_wrf_da3d( outhandles, nfileout, trim(var_pref),      &
                             full3d, nxd-1,nyd-1,nzs,                   &
                             ioss,iose,joss,jose,lvldbg,istatus)
        CASE ( 'REFL_10CM' )    ! 3D extra background variable
          ALLOCATE(full3d(nxd-1,nyd-1,nzd-1), STAT = istatus)

          CALL get_wrf_da3d( tdhandles, nfiletd, trim(var_pref),        &
                             full3d, nxd-1,nyd-1,nzd-1,                 &
                             idss,idse,jdss,jdse,lvldbg,istatus)
          IF (istatus == 0) THEN
            CALL put_wrf_da3d( outhandles, nfileout, trim(var_pref),    &
                               full3d, nxd-1,nyd-1,nzd-1,               &
                               ioss,iose,joss,jose,lvldbg,istatus)
          ELSE
            WRITE(*,'(1x,3a)') 'ERROR: Variable ',trim(var_pref),' not exist in background file. Skipped.'
          END IF
        CASE DEFAULT

          ALLOCATE(full3d(nxd-1,nyd-1,1), STAT = istatus)

          CALL get_wrf_da2d( tdhandles, nfiletd, trim(var_pref),        &
                             full3d, nxd-1,nyd-1,                       &
                             idss,idse,jdss,jdse,lvldbg,istatus)

          CALL put_wrf_da2d( outhandles, nfileout, trim(var_pref),      &
                             full3d, nxd-1,nyd-1,                       &
                             ioss,iose,joss,jose,lvldbg,istatus)

        END SELECT


        DEALLOCATE(full3d)

      END DO

      !
      ! copy missed fields from  var_analysis_names
      !
      DO n = 1, ndamiss    ! Suppose all for 3D variables
        var_pref = TRIM(vardamiss(n))

        IF (lvldbg > 0) &
          WRITE(UNTOUT, '(1x,a)') 'Copying field '//TRIM(var_pref)//' ...'

        ALLOCATE(full3d(nxd-1,nyd-1,nzd-1), STAT = istatus)

        CALL get_wrf_da3d( tdhandles, nfiletd, trim(var_pref),          &
                           full3d, nxd-1,nyd-1,nzd-1,                   &
                           idss,idse,jdss,jdse,lvldbg,istatus)

        IF (istatus /= 0) THEN
          WRITE(unit=UNTOUT, fmt='(1x,3a)')                             &
                'INFO: Field ', trim(var_pref), ' does not exist. Skipped.'
          CYCLE
        END IF

        CALL put_wrf_da3d( outhandles, nfileout, trim(var_pref),        &
                           full3d, nxd-1,nyd-1,nzd-1,                   &
                           ioss,iose,joss,jose,lvldbg,istatus)

        DEALLOCATE(full3d)

      END DO

      DEALLOCATE(idss,idse,idps,idpe)
      DEALLOCATE(jdss,jdse,jdps,jdpe)

      CALL close_wrf_files(tdhandles, nfiletd, istatus)

      DEALLOCATE( tdhandles )
    ELSE
      IF (ndamiss > 0) THEN
        WRITE(*,'(1x,100a)') 'WARNING: missing fields not set - ',(TRIM(vardamiss(n)),', ',n=1,ndamiss)
      END IF
    END IF

!-----------------------------------------------------------------------
!
! Close output files
!
!-----------------------------------------------------------------------

    DEALLOCATE(ioss,iose,iops,iope)
    DEALLOCATE(joss,jose,jops,jope)

    !
    ! Close files
    !
    CALL close_wrf_files(outhandles, nfileout, istatus)

    DEALLOCATE(outhandles)

  END IF

!-----------------------------------------------------------------------
!
! Close DA files nicely
!
!-----------------------------------------------------------------------

  CALL close_wrf_files(dahandles, nfileda, istatus)

  DEALLOCATE(iss,ise,ips,ipe)
  DEALLOCATE(jss,jse,jps,jpe)

  DEALLOCATE(dahandles)

!-----------------------------------------------------------------------
!
! Terminate the program nicely
!
!-----------------------------------------------------------------------

  CALL deallocate_namelist_arrays( istatus )
  CALL finalize_netcdf_api( istatus )

  IF (istatus == 0) THEN

    WRITE(UNTOUT,'(/,5x,a)') '-----------------------------------------'

    IF ( update_lateral_bdy ) WRITE(UNTOUT,'(1x,3a,/,5x,3a)')           &
       'Lateral boundary tendency updated in "',TRIM(wrf_bdy_out),'"',  &
       '<-- ',TRIM(da_file),'.'
    WRITE(UNTOUT, '(1x,a,I0)') 'Perturbed variables: ', perturb_lbc_count

    IF ( update_input ) THEN
      WRITE(UNTOUT,'(1x,3a,/,5x,3a)')                                   &
        'Input file updated in "',TRIM(outdir)//TRIM(wrf_input_out),'"',&
        '<-- ',TRIM(da_file),'.'
      IF (update_input_copy_td) THEN
        WRITE(UNTOUT,'(1x,3a)')                                         &
          'Time dependent fields are retrieved from "',TRIM(wrf_td_in),'".'
      END IF
      WRITE(UNTOUT, '(1x,a,I0)') 'Perturbed variables: ', perturb_ic_count
    END IF

    WRITE(UNTOUT,'(/,5x,a)') '==== WRFDAUPDATE terminated normally ===='
  ELSE
    WRITE(UNTOUT,'(/,5x,a)') '**** WRFDAUPDATE aborted with error ****'
  END IF

  CALL arpsstop('Program stopped',istatus)
END PROGRAM wrf_daupdate

!#######################################################################

SUBROUTINE normalrand(iseed,n,rndn)

  IMPLICIT NONE

  INTEGER, INTENT(INOUT) :: iseed            ! random seed
  INTEGER, INTENT(IN)    :: n                ! dimension size

  REAL,    INTENT(OUT)   :: rndn(n)          ! return standard normal distri.

!-----------------------------------------------------------------------

  INTEGER :: i
  REAL, ALLOCATABLE :: work1(:),work2(:)
  REAL, PARAMETER   :: pi=3.141592653589
  REAL :: ave,var

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  ALLOCATE (work1(n),work2(n))

  do i=1,n
    iseed =  mod(iseed*7141+54773,259200)
    work1(i)=float(iseed)/259199.
  enddo
  do i=1,n
    iseed =  mod(iseed*7141+54773,259200)
    work2(i)=float(iseed)/259199.
  enddo

  rndn = sqrt(-2.0*log(work1+tiny(1.0)))*cos(2.0*pi*work2)
                       ! N(0,1) standard normal distribution

!! statistics
!  ave = sum(rndn)/float(n)
!  work1 = rndn-ave
!  var = dot_product(work1,work1)/float(n-1)
!
!  print *,'  AVE,VAR,SQRT(AVR):',ave,var,sqrt(var)

  DEALLOCATE (work1,work2)
  RETURN
END SUBROUTINE normalrand
