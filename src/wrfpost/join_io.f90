!
!##################################################################
!##################################################################
!######                                                      ######
!######           SUBROUTINE joinwrfncdf                     ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE  joinwrfncdf_io(filenames,nfile,attadj,jointime,fileconv,    &
                  magnitude_processor,procs,npatch,                     &
                  ids,ide,idss,idse,jds,jde,jdss,jdse,istride,jstride,  &
                  idsinout,ideinout,idssinout,idseinout,                &
                  jdsinout,jdeinout,jdssinout,jdseinout,                &
                  kps,kpe,kpss,kpse,                                    &
                  outdirname,filetail,nvarout,varlists,debug,istatus)
!
!-----------------------------------------------------------------------
!
! PURPOSE:
!
!    Join WRF files in netCDF patches into one large piece.
!    This version does more IO operations but use less memory.
!
!-----------------------------------------------------------------------
!
! Author: Yunheng Wang (04/27/2007)
!
! MODIFICATIONS:
!
! 03/09/2013 (Y. Wang)
! Added extractions of output box.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
  INTEGER, INTENT(IN)            :: nfile
  LOGICAL, INTENT(IN)            :: attadj
  LOGICAL, INTENT(IN)            :: jointime
  INTEGER, INTENT(IN)            :: fileconv, magnitude_processor
  INTEGER, INTENT(IN)            :: npatch
  INTEGER, INTENT(IN)            :: procs(npatch)
  INTEGER, INTENT(IN)            :: ids,ide,idss,idse,jds,jde,jdss,jdse
  INTEGER, INTENT(IN)            :: idsinout, ideinout, jdsinout, jdeinout
  INTEGER, INTENT(IN)            :: idssinout, idseinout, jdssinout, jdseinout
  INTEGER, INTENT(IN)            :: kps,kpe,kpss,kpse
  INTEGER, INTENT(IN)            :: istride, jstride
  INTEGER, INTENT(INOUT)         :: nvarout
  INTEGER, INTENT(IN)            :: debug
  INTEGER, INTENT(OUT)           :: istatus

  CHARACTER(LEN=*),  INTENT(IN)  :: filenames(nfile)
  CHARACTER(LEN=*),  INTENT(IN)  :: outdirname
  CHARACTER(LEN=5),  INTENT(IN)  :: filetail
  CHARACTER(LEN=20), INTENT(IN)  :: varlists(nvarout)

!
!-----------------------------------------------------------------------
!
! Including files
!
!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

!-----------------------------------------------------------------------
!
! Misc. local variables
!
!-----------------------------------------------------------------------

  INTEGER :: nf, nvar, n
  INTEGER :: strlen
  LOGICAL :: ispatch(NF_MAX_VARS)

  CHARACTER(LEN=256) :: infilename, outfilename
  INTEGER :: finid, foutid

  INTEGER :: idsout, ideout, jdsout, jdeout
  INTEGER :: idssout, idseout, jdssout, jdseout

  !LOGICAL :: paddingx, paddingy
  !INTEGER :: nxdim, nydim
  REAL    :: newctrlat, newctrlon, lat1, lat2,lon1,lon2
  INTEGER :: newlatidx, newlonidx, oldlatidx1, oldlonidx1, oldlatidx2, oldlonidx2
  LOGICAL :: latavg, lonavg
  INTEGER :: latset, lonset

  !
  ! Dimension variables
  !
  CHARACTER(LEN=32), PARAMETER :: xdimname  = 'west_east_stag'
  CHARACTER(LEN=32), PARAMETER :: ydimname  = 'south_north_stag'
  CHARACTER(LEN=32), PARAMETER :: xsdimname = 'west_east'
  CHARACTER(LEN=32), PARAMETER :: ysdimname = 'south_north'
  CHARACTER(LEN=32) :: diminnames(NF_MAX_DIMS)
  CHARACTER(LEN=32) :: dimname

  INTEGER :: nxid, nyid, nxlg, nylg, nxsid, nysid, nxslg, nyslg
  INTEGER :: narrsize, narrisizemax, narrasizemax
  INTEGER :: unlimdimid, unlimdimlen, unlimodimlen, odimid
  INTEGER :: ndims, dimid, dimlen

  INTEGER :: dimina(NF_MAX_DIMS)         ! Dimension size in original file
  !INTEGER :: dimouta(NF_MAX_DIMS)        ! Dimension size in joined files

  !
  ! Attribute variables
  !
  CHARACTER(LEN=32), PARAMETER :: attnm_ips  = 'WEST-EAST_PATCH_START_STAG'
  CHARACTER(LEN=32), PARAMETER :: attnm_ipe  = 'WEST-EAST_PATCH_END_STAG'
  CHARACTER(LEN=32), PARAMETER :: attnm_ipss = 'WEST-EAST_PATCH_START_UNSTAG'
  CHARACTER(LEN=32), PARAMETER :: attnm_ipse = 'WEST-EAST_PATCH_END_UNSTAG'
  CHARACTER(LEN=32), PARAMETER :: attnm_jps  = 'SOUTH-NORTH_PATCH_START_STAG'
  CHARACTER(LEN=32), PARAMETER :: attnm_jpe  = 'SOUTH-NORTH_PATCH_END_STAG'
  CHARACTER(LEN=32), PARAMETER :: attnm_jpss = 'SOUTH-NORTH_PATCH_START_UNSTAG'
  CHARACTER(LEN=32), PARAMETER :: attnm_jpse = 'SOUTH-NORTH_PATCH_END_UNSTAG'
  CHARACTER(LEN=32) :: attname
  INTEGER :: ipsid,  ipeid,  jpsid,  jpeid
  INTEGER :: ipssid, ipseid, jpssid, jpseid
  INTEGER :: ips, ipe, ipss, ipse
  INTEGER :: jps, jpe, jpss, jpse
  INTEGER :: attnum, ngatts
  INTEGER :: istart, jstart, iend, jend, imod, jmod

  CHARACTER(LEN=32), PARAMETER :: attnm_anx = 'WEST-EAST_GRID_DIMENSION'
  CHARACTER(LEN=32), PARAMETER :: attnm_any = 'SOUTH-NORTH_GRID_DIMENSION'
  INTEGER :: anxid, anyid

  CHARACTER(LEN=32), PARAMETER :: attnm_adx = 'DX'
  CHARACTER(LEN=32), PARAMETER :: attnm_ady = 'DY'
  INTEGER :: adxid, adyid
  REAL    :: adx, ady

  !
  ! Dataset varaibles
  !
  INTEGER, PARAMETER :: MAX_RANK = 4    ! Assume the max rank is 5
  CHARACTER(LEN=32) :: varname
  INTEGER :: varid, nvars, ovarid
  INTEGER :: vartype, varndims, varnatts
  INTEGER :: vardimids(MAX_RANK)
  INTEGER :: startidx(MAX_RANK), countidx(MAX_RANK), strideidx(MAX_RANK)
  INTEGER :: outstart(MAX_RANK), outstride(MAX_RANK)
  INTEGER :: vardim, vdimid

  INTEGER :: varidlists(NF_MAX_VARS), varoutidlists(NF_MAX_VARS)

  INTEGER, ALLOCATABLE :: varari(:)
  REAL,    ALLOCATABLE :: vararr(:)
  CHARACTER(LEN=256)   :: tmpstr,fmtstr

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Beginning of executable code below
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  varidlists(:) = 0
  nxlg  = ide-ids+1
  nylg  = jde-jds+1
  nxslg = idse-idss+1
  nyslg = jdse-jdss+1

  startidx(:)  = 1
  narrisizemax = 0
  narrasizemax = 0

  unlimdimlen  = 1

  istatus = 0
  DO nf = 1,nfile

!-----------------------------------------------------------------------
!
! First, Create the merged file based on attributes from the first patch
!
!-----------------------------------------------------------------------

    IF (.NOT. jointime .OR. nf == 1) THEN
      strlen = LEN_TRIM(filenames(nf))
      n = INDEX(filenames(nf),'/',.TRUE.)

      WRITE(outfilename,'(3a)') TRIM(outdirname),                         &
                                filenames(nf)(n+1:strlen),filetail

      IF (jointime .AND. npatch == 1) THEN
        WRITE(infilename, '(a)')       TRIM(filenames(nf))
      ELSE
        WRITE(fmtstr,'(a,2(I1,a))') '(a,a,I',magnitude_processor,'.',magnitude_processor,')'
        WRITE(infilename, FMT=fmtstr) TRIM(filenames(nf)),'_',procs(1)
      END IF

      IF (debug > 0) WRITE(6,'(1x,2a)') 'Opening file - ',TRIM(infilename)
      istatus = nf_open(infilename,NF_NOWRITE,finid)   ! Open file
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      IF (debug > 0) WRITE(6,'(1x,2a)') 'Creating file - ',TRIM(outfilename)
      !istatus = nf_create(TRIM(outfilename),NF_CLOBBER,foutid)                     ! CDF 1
      istatus = NF_CREATE(TRIM(outfilename),IOR(NF_CLOBBER,NF_64BIT_OFFSET),foutid) ! CDF 2
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      !
      ! Set dimensions
      !
      IF (fileconv < 2) THEN
        istatus = nf_inq_dimid(finid,xdimname,nxid)
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        istatus = nf_inq_dimid(finid,ydimname,nyid)
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      ELSE               ! NMM core
        nxid = -1
        nyid = -1
      END IF

      istatus = nf_inq_dimid(finid,xsdimname,nxsid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      istatus = nf_inq_dimid(finid,ysdimname,nysid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      istatus = nf_inq_unlimdim(finid,unlimdimid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      istatus = nf_inq_ndims(finid,ndims)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      IF (debug > 0) WRITE(6,'(5x,a,I2)') 'Copying dimensions - ',ndims
      DO dimid = 1,ndims
        istatus = nf_inq_dim(finid,dimid,dimname,dimlen)
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)

        diminnames(dimid) = dimname
        dimina(dimid)  = dimlen             ! Save dimension id and len
        !dimouta(dimid) = dimlen             ! Output dimension id and len
        IF (dimid == nxid) THEN
          dimlen = (ideinout-idsinout)/istride+1
          !dimouta(dimid) = dimlen
        ELSE IF (dimid == nxsid) THEN
          dimlen = (idseinout-idssinout)/istride+1
          !dimouta(dimid) = dimlen
        ELSE IF (dimid == nyid) THEN
          dimlen = (jdeinout-jdsinout)/jstride+1
          !dimouta(dimid) = dimlen
        ELSE IF (dimid == nysid) THEN
          dimlen = (jdseinout-jdssinout)/jstride+1
          !dimouta(dimid) = dimlen
        ELSE IF (dimid == unlimdimid) THEN
          dimlen = NF_UNLIMITED
        END IF

        IF (debug > 0) WRITE(6,'(9x,a,a20,a,I0)') 'Dimension name - ',dimname, ', length = ',dimlen
        istatus = nf_def_dim(foutid,dimname,dimlen,odimid)
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      END DO

      !
      ! Set Global attributes
      !
      istatus = nf_inq_attid(finid,NF_GLOBAL,TRIM(attnm_ips),ipsid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      istatus = nf_inq_attid(finid,NF_GLOBAL,TRIM(attnm_ipe),ipeid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      istatus = nf_inq_attid(finid,NF_GLOBAL,TRIM(attnm_ipss),ipssid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      istatus = nf_inq_attid(finid,NF_GLOBAL,TRIM(attnm_ipse),ipseid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      istatus = nf_inq_attid(finid,NF_GLOBAL,TRIM(attnm_jps),jpsid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      istatus = nf_inq_attid(finid,NF_GLOBAL,TRIM(attnm_jpe),jpeid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      istatus = nf_inq_attid(finid,NF_GLOBAL,TRIM(attnm_jpss),jpssid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      istatus = nf_inq_attid(finid,NF_GLOBAL,TRIM(attnm_jpse),jpseid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      istatus = nf_inq_attid(finid,NF_GLOBAL,TRIM(attnm_anx),anxid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      istatus = nf_inq_attid(finid,NF_GLOBAL,TRIM(attnm_any),anyid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      istatus = nf_inq_attid(finid,NF_GLOBAL,TRIM(attnm_adx),adxid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      istatus = nf_inq_attid(finid,NF_GLOBAL,TRIM(attnm_ady),adyid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      istatus = nf_get_att_real(finid,NF_GLOBAL,TRIM(attnm_adx),adx)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      istatus = nf_get_att_real(finid,NF_GLOBAL,TRIM(attnm_ady),ady)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      istatus = nf_inq_natts(finid,ngatts)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      IF (attadj) THEN
        idsout  = 1
        ideout  = (ideinout  - idsinout)/istride  + 1
        idssout = 1
        idseout = (idseinout - idssinout)/istride + 1

        jdsout  = 1
        jdeout  = (jdeinout  - jdsinout)/jstride  + 1
        jdssout = 1
        jdseout = (jdseinout - jdssinout)/jstride + 1

        newlatidx = (jdeout-jdsout)/2    ! Added to modify CEN_LAT & CEN_LON
        newlonidx = (ideout-idsout)/2    ! 0 base

        oldlatidx1 = newlatidx*jstride+jdsinout  ! old domain index
        oldlonidx1 = newlonidx*istride+idsinout  ! old domain index

        latavg = .FALSE.
        lonavg = .FALSE.
        latset = 0
        lonset = 0

        IF (MOD((jdeout-jdsout),2) /= 0) latavg = .TRUE.
        IF (MOD((ideout-idsout),2) /= 0) lonavg = .TRUE.

        oldlatidx2 = -99999999
        oldlonidx2 = -99999999
        IF (.NOT. latavg .AND. .NOT. lonavg) THEN
          oldlatidx2 = oldlatidx1-jstride
          oldlonidx2 = oldlonidx1-istride
        END IF

        IF (debug > 0) WRITE(6,'(1x,a,/,1x,a,2L,a,2I4,/,21x,a,2I4,a))') &
          'It is required to adjust global attribute.',                 &
          'latavg,lonavg = ',latavg,lonavg,                             &
          ', lonidx,latidx = ',newlonidx+1,newlatidx+1,                 &
          ', lonidx,latidx = ',oldlonidx1,oldlatidx1,' (in original domain)'

      ELSE
        idsout  = idsinout
        ideout  = ideinout
        idssout = idssinout
        idseout = idseinout

        jdsout  = jdsinout
        jdeout  = jdeinout
        jdssout = jdssinout
        jdseout = jdseinout
      END IF

      IF (debug > 0) WRITE(6,'(5x,a,I2)') 'Copying global attributes - ',ngatts

      DO attnum = 1,ngatts

        istatus = nf_inq_attname(finid,NF_GLOBAL,attnum,attname)
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)

        IF (debug > 0) WRITE(6,'(9x,2a)') 'Attribute name - ',TRIM(attname)

        IF (attnum == ipsid) THEN
          istatus = NF_PUT_ATT_INT(foutid,NF_GLOBAL,TRIM(attnm_ips),NF_INT,1,idsout)
          IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        ELSE IF (attnum == ipeid) THEN
          istatus = NF_PUT_ATT_INT(foutid,NF_GLOBAL,TRIM(attnm_ipe),NF_INT,1,ideout)
          IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        ELSE IF (attnum == jpsid) THEN
          istatus = NF_PUT_ATT_INT(foutid,NF_GLOBAL,TRIM(attnm_jps),NF_INT,1,jdsout)
          IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        ELSE IF (attnum == jpeid) THEN
          istatus = NF_PUT_ATT_INT(foutid,NF_GLOBAL,TRIM(attnm_jpe),NF_INT,1,jdeout)
          IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        ELSE IF (attnum == ipssid) THEN
          istatus = NF_PUT_ATT_INT(foutid,NF_GLOBAL,TRIM(attnm_ipss),NF_INT,1,idssout)
          IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        ELSE IF (attnum == ipseid) THEN
          istatus = NF_PUT_ATT_INT(foutid,NF_GLOBAL,TRIM(attnm_ipse),NF_INT,1,idseout)
          IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        ELSE IF (attnum == jpssid) THEN
          istatus = NF_PUT_ATT_INT(foutid,NF_GLOBAL,TRIM(attnm_jpss),NF_INT,1,jdssout)
          IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        ELSE IF (attnum == jpseid) THEN
          istatus = NF_PUT_ATT_INT(foutid,NF_GLOBAL,TRIM(attnm_jpse),NF_INT,1,jdseout)
          IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        ELSE IF (attnum == anxid) THEN      ! Adjust nx
          IF (istride > 1 .OR. attadj) THEN
            istatus = NF_PUT_ATT_INT(foutid,NF_GLOBAL,TRIM(attnm_anx),NF_INT,1,(ideout-idsout)+1)
            IF (istatus /= NF_NOERR) CALL handle_err(istatus)
          ELSE
            istatus = nf_copy_att(finid,NF_GLOBAL,attname,foutid,NF_GLOBAL)
            IF (istatus /= NF_NOERR) CALL handle_err(istatus)
          END IF
        ELSE IF (attnum == anyid) THEN      ! Adjust ny
          IF (jstride > 1 .OR. attadj) THEN
            istatus = NF_PUT_ATT_INT(foutid,NF_GLOBAL,TRIM(attnm_any),NF_INT,1,(jdeout-jdsout)+1)
            IF (istatus /= NF_NOERR) CALL handle_err(istatus)
          ELSE
            istatus = nf_copy_att(finid,NF_GLOBAL,attname,foutid,NF_GLOBAL)
            IF (istatus /= NF_NOERR) CALL handle_err(istatus)
          END IF
        ELSE IF (attnum == adxid) THEN      ! adjust dx
          istatus = NF_PUT_ATT_REAL(foutid,NF_GLOBAL,TRIM(attnm_adx),NF_REAL,1,adx*istride)
          IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        ELSE IF (attnum == adyid) THEN      ! adjust dy
          istatus = NF_PUT_ATT_REAL(foutid,NF_GLOBAL,TRIM(attnm_ady),NF_REAL,1,ady*jstride)
          IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        ELSE
          istatus = nf_copy_att(finid,NF_GLOBAL,attname,foutid,NF_GLOBAL)
          IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        END IF

      END DO

      !
      ! Define variables
      !
      istatus = nf_inq_nvars(finid,nvars)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      IF (nvarout >= nvars) THEN
        nvarout = nvars
        DO n = 1,nvars
          varidlists(n) = n
        END DO
      ELSE
        nvar = nvarout         ! suppose to process this number
        nvarout = 0            ! actually got
        DO n = 1,nvar
          istatus = nf_inq_varid(finid,TRIM(varlists(n)),ovarid)
          IF (istatus /= NF_NOERR) THEN
            WRITE(6,'(1x,3a)') 'WARNING: Variable ',TRIM(varlists(n)),' not found. Skipped.'
            CYCLE
          END IF
          nvarout = nvarout + 1
          varidlists(nvarout) = ovarid
        END DO
      END IF

      IF (debug > 0) WRITE(6,'(5x,a,I4)') 'Defining variables - ',nvarout

      DO n = 1,nvarout
        varid = varidlists(n)
        istatus = nf_inq_var(finid,varid,varname,vartype,varndims,vardimids,varnatts)
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)

        IF (debug > 0) WRITE(6,'(9x,2a)') 'Variables - ',TRIM(varname)

        ! Dimensions should be in the same order
        istatus = nf_def_var(foutid,varname,vartype,varndims,vardimids,ovarid)
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)

        varoutidlists(n) = ovarid

        DO attnum = 1,varnatts          ! Copy variable attributes
         istatus = nf_inq_attname(finid,varid,attnum,attname)
         IF (istatus /= NF_NOERR) CALL handle_err(istatus)

         istatus = nf_copy_att(finid,varid,attname,foutid,ovarid)
         IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        END DO

      END DO

      istatus = nf_enddef(foutid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      IF(debug > 0) WRITE(6,'(1x,a)') 'Merged file have been defined.'

      istatus = nf_close(finid)                              ! Close file
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

    END IF         ! File created.

    IF (.NOT. jointime) unlimdimlen = 1
    unlimodimlen = 0

!-----------------------------------------------------------------------
!
! Write each patch to the merged file
!
!-----------------------------------------------------------------------

    ispatch(:) = .FALSE.
    patch_loop: DO n = 1,npatch
      IF (jointime .AND. npatch == 1) THEN
        WRITE(infilename, '(a)')       TRIM(filenames(nf))
      ELSE
        WRITE(fmtstr,'(a,2(I1,a))') '(a,a,I',magnitude_processor,'.',magnitude_processor,')'
        WRITE(infilename, FMT=fmtstr) TRIM(filenames(nf)),'_',procs(n)
      END IF

      IF (debug > 0) WRITE(6,'(1x,2a)') 'Opening file - ',TRIM(infilename)

      istatus = nf_open(TRIM(infilename),NF_NOWRITE,finid)   ! Open file
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      !
      ! Get patch indice
      !
      istatus = nf_get_att_int(finid,NF_GLOBAL,TRIM(attnm_ips),ips)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      istatus = nf_get_att_int(finid,NF_GLOBAL,TRIM(attnm_ipe),ipe)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      istatus = nf_get_att_int(finid,NF_GLOBAL,TRIM(attnm_ipss),ipss)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      istatus = nf_get_att_int(finid,NF_GLOBAL,TRIM(attnm_ipse),ipse)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      istatus = nf_get_att_int(finid,NF_GLOBAL,TRIM(attnm_jps),jps)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      istatus = nf_get_att_int(finid,NF_GLOBAL,TRIM(attnm_jpe),jpe)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      istatus = nf_get_att_int(finid,NF_GLOBAL,TRIM(attnm_jpss),jpss)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      istatus = nf_get_att_int(finid,NF_GLOBAL,TRIM(attnm_jpse),jpse)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

!-----------------------------------------------------------------------
!
! Get and save dimension size for this patch
!
!-----------------------------------------------------------------------

      istatus = nf_inq_ndims(finid,ndims)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)

      dimina(:) = 0
      DO dimid = 1,ndims
        istatus = nf_inq_dim(finid,dimid,dimname,dimlen)
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)

        diminnames(dimid) = dimname
        dimina(dimid)     = dimlen             ! Save dimension id and len
      END DO

!-----------------------------------------------------------------------
!
! loop over each variable
!
!-----------------------------------------------------------------------

      var_loop: DO nvar = 1,nvarout

        varid = varidlists(nvar)

        vardimids(:) = 0
        istatus = nf_inq_var(finid,varid,varname,vartype,varndims,vardimids,varnatts)
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)

!-----------------------------------------------------------------------
!
! Handle dimensions
!
!-----------------------------------------------------------------------

        startidx(:)  = 1
        countidx(:)  = 1
        strideidx(:) = 1
        outstart(:)    = 1
        outstride(:)   = 1
        narrsize = 1

        istart = 1  ! in global index of the input domain
        iend   = 1
        jstart = 1
        jend   = 1
        DO vardim = 1, varndims
          vdimid = vardimids(vardim)
          countidx(vardim) = dimina (vdimid)
          IF ( vdimid == nxid) THEN
            IF ( ips > idsinout) THEN
              imod = MOD((ips-idsinout),istride)
              IF (imod == 0) THEN
                startidx(vardim) = 1
              ELSE
                startidx(vardim) = istride - imod + 1
              END IF
              istart = ips+startidx(vardim)-1    ! start index relative to global domain
            ELSE
              startidx(vardim) = idsinout-ips+1
              istart = idsinout
            END IF

            iend   = MIN(ipe,ideinout)
            IF (iend < istart) THEN
              countidx(vardim)  = 0
            ELSE
              countidx(vardim)  = (iend-istart)/istride + 1
            END IF
            strideidx(vardim) = istride

            outstart(vardim) = (istart-idsinout)/istride + 1
            ispatch(nvar) = .TRUE.

          ELSE IF ( vdimid == nyid) THEN
            IF (jps > jdsinout) THEN
              jmod = MOD((jps-jdsinout),jstride)
              IF (jmod == 0) THEN
                startidx(vardim) = 1
              ELSE
                startidx(vardim) = jstride - jmod + 1
              END IF
              jstart = jps+startidx(vardim)-1
            ELSE
              startidx(vardim) = jdsinout-jps+1
              jstart = jdsinout
            END IF

            jend   = MIN(jpe,jdeinout)
            IF (jend < jstart) THEN
              countidx(vardim)  = 0
            ELSE
              countidx(vardim)  = (jend-jstart)/jstride + 1
            END IF
            strideidx(vardim) = jstride

            outstart(vardim) = (jstart - jdsinout)/jstride + 1
            ispatch(nvar) = .TRUE.

          ELSE IF ( vdimid == nxsid) THEN
            IF (ipss > idssinout) THEN
              imod = MOD((ipss-idssinout),istride)
              IF (imod == 0) THEN
                startidx(vardim) = 1
              ELSE
                startidx(vardim) = istride - imod + 1
              END IF
              istart = ipss+startidx(vardim)-1
            ELSE
              startidx(vardim) = idssinout-ipss+1
              istart = idssinout
            END IF

            iend   = MIN(ipse,idseinout)
            IF (iend < istart) THEN
              countidx(vardim)  = 0
            ELSE
              countidx(vardim)  = (iend-istart)/istride + 1
            END IF
            strideidx(vardim) = istride

            outstart(vardim) = (istart - idssinout)/istride + 1
            ispatch(nvar) = .TRUE.
          ELSE IF ( vdimid == nysid) THEN
            IF (jpss > jdssinout) THEN
              jmod = MOD((jpss-jdssinout),jstride)
              IF (jmod == 0) THEN
                startidx(vardim) = 1
              ELSE
                startidx(vardim) = jstride - jmod + 1
              END IF
              jstart = jpss+startidx(vardim)-1
            ELSE
              startidx(vardim) = jdssinout-jpss+1
              jstart = jdssinout
            END IF

            jend   = MIN(jpse,jdseinout)
            IF (jend < jstart) THEN
              countidx(vardim)  = 0
            ELSE
              countidx(vardim)  = (jend-jstart)/jstride + 1
            END IF
            strideidx(vardim) = jstride

            outstart(vardim) = (jstart - jdssinout)/jstride + 1
            ispatch(nvar) = .TRUE.
          ELSE IF (vdimid == unlimdimid) THEN
            outstart(vardim) = unlimdimlen
            IF (unlimodimlen <= 0) THEN
              unlimodimlen = countidx(vardim)
            ELSE
              IF ( unlimodimlen /= countidx(vardim)) THEN
                WRITE(6,'(1x,a,/)') 'ERROR: Inconsisten size for UNLIMITED dimension.'
                istatus = -1
                RETURN
              END IF
            END IF
          ELSE
            outstart(vardim) = 1
          END IF

          IF (countidx(vardim) <= 0) THEN
            IF (debug > 0) THEN
              WRITE(6,'(9x,2a)') 'Processing variables - ',TRIM(varname)
              WRITE(6,'(12x,a,i0,3a)')                                  &
                'Path ',n,' skipped because dimension "',               &
                TRIM(diminnames(vdimid)),'" has zero length.'
            END IF
            CYCLE var_loop
          END IF

          narrsize = countidx(vardim)*narrsize
        END DO

        IF ( n > 1 .AND. (.NOT. ispatch(nvar)) ) THEN
          IF (debug > 2) WRITE(6,'(9x,3a)') 'Variable ',TRIM(varname),' skipped.'
          CYCLE
        ELSE
          IF (debug > 2) THEN
            WRITE(6,'(9x,3(a,I4))') 'Dimensions in Patch : ',procs(n),', istart = ',istart, ', jstart = ',jstart

            DO vardim = 1,varndims
              vdimid = vardimids(vardim)
              WRITE(6,'(12x,a,4(a,I4))') diminnames(vdimid),                  &
              ', outstart = ',outstart(vardim),', size = ', countidx(vardim), &
              ' <-- start = ',startidx(vardim),', stride = ',strideidx(vardim)
            END DO
          END IF
        END IF

        ! do not have to merge, use values from the first file

!        IF (.NOT. ispatch(nvar)) THEN
!
!          IF (debug > 0) WRITE(6,'(9x,2a)') 'Copying variables - ',TRIM(varname)
!
!          istatus = NF_COPY_VAR(finid,varid,foutid)
!          IF (istatus /= NF_NOERR) CALL handle_err(istatus)
!
!        ELSE
          ovarid = varoutidlists(nvar)

          IF (debug > 0) WRITE(6,'(9x,3a,I4)')                          &
             'Copying variables - ',TRIM(varname),' from patch: ',procs(n)

          SELECT CASE (vartype)

!-----------------------------------------------------------------------
!
! Integers
!
!-----------------------------------------------------------------------

          CASE (NF_INT)

            IF (narrsize > narrisizemax) THEN   ! Allocate input array only when necessary
              IF (ALLOCATED(varari)) DEALLOCATE(varari, STAT = istatus)
              ALLOCATE(varari(narrsize), STAT = istatus)
              narrisizemax = narrsize
            END IF

            istatus = NF_GET_VARS_INT(finid,varid,startidx,countidx,strideidx,varari)
            IF (istatus /= NF_NOERR) CALL handle_err(istatus)

            istatus = nf_put_vara_INT(foutid,ovarid,outstart,countidx,varari)
            IF (istatus /= NF_NOERR) CALL handle_err(istatus)

!-----------------------------------------------------------------------
!
! Reals
!
!-----------------------------------------------------------------------

          CASE (NF_FLOAT)

            IF (narrsize > narrasizemax) THEN   ! Allocate input array only when necessary
              IF (ALLOCATED(vararr)) DEALLOCATE(vararr, STAT = istatus)
              ALLOCATE(vararr(narrsize), STAT = istatus)
              narrasizemax = narrsize
            END IF

            istatus = NF_GET_VARS_REAL(finid,varid,startidx,countidx,strideidx,vararr)
            IF (istatus /= NF_NOERR) CALL handle_err(istatus)

            istatus = nf_put_vara_REAL(foutid,ovarid,outstart,countidx,vararr)
            IF (istatus /= NF_NOERR) CALL handle_err(istatus)

!-----------------------------------------------------------------------
!
! Character string
!
!-----------------------------------------------------------------------

          CASE (NF_CHAR)

            istatus = NF_GET_VARS_TEXT(finid,varid,startidx,countidx,strideidx,tmpstr)
            IF (istatus /= NF_NOERR) CALL handle_err(istatus)

            istatus = nf_put_vara_TEXT(foutid,ovarid,outstart,countidx,TRIM(tmpstr))
            IF (istatus /= NF_NOERR) CALL handle_err(istatus)

          CASE DEFAULT
            WRITE(6,'(1x,a,I2)') 'ERROR: unsupported variable type = ',vartype
            istatus = -4
            RETURN
          END SELECT
!        END IF  ! ispatch(nvar)

        !
        ! Added to find the new CEN_LON & CEN_LAT
        !
        IF (attadj .AND. (oldlatidx1 >= jps .AND. oldlatidx1 <= jpe) .AND.  &
                         (oldlonidx1 >= ips .AND. oldlonidx1 <= ipe) ) THEN

          IF ( .NOT. lonavg .AND. latavg ) THEN      ! at U point

            IF (TRIM(varname) == 'XLONG_U' .OR. TRIM(varname) == 'XLAT_U' ) THEN
              newlatidx = (oldlatidx1-jstart)/jstride     ! 0-base
              newlonidx = (oldlonidx1-istart)/istride+1   ! 1-base
              strlen = newlatidx*countidx(1)+newlonidx

              IF (TRIM(varname) == 'XLONG_U' ) THEN
                newctrlon = vararr(strlen)
                IF (debug > 1) WRITE(6,'(1x,a,I4,a,F8.2,a,i3)')         &
                   '*new cen_lon at U point. lonidx = ',newlonidx,      &
                   ', newctrlon = ',newctrlon,' from patch ', procs(n)
                lonset = lonset+1
              ELSE IF (TRIM(varname) == 'XLAT_U') THEN
                newctrlat = vararr(strlen)
                IF (debug > 1) WRITE(6,'(1x,a,I4,a,F8.2,a,i3)')         &
                     '*new cen_lat at U point. latidx = ',newlatidx,    &
                     ', newctrlat = ',newctrlat,' from patch ', procs(n)
                latset = latset+1
              END IF
            END IF

          END IF

          IF ( lonavg .AND. .NOT. latavg ) THEN      ! at V point
            IF (TRIM(varname) == 'XLONG_V' .OR. TRIM(varname) == 'XLAT_V'  ) THEN
              newlatidx = (oldlatidx1-jstart)/jstride
              newlonidx = (oldlonidx1-istart)/istride+1
              strlen = newlatidx*countidx(1)+newlonidx

              IF (TRIM(varname) == 'XLONG_V' ) THEN
                newctrlon = vararr(strlen)
                IF (debug > 1) WRITE(6,'(1x,a,I4,a,F8.2,a,i3)')         &
                   '*new cen_lon at V point. lonidx = ',newlonidx,      &
                   ', newctrlon = ',newctrlon,' from patch ', procs(n)

                lonset = lonset+1
              ELSE IF (TRIM(varname) == 'XLAT_V') THEN
                newctrlat = vararr(strlen)
                IF (debug > 1) WRITE(6,'(1x,a,I4,a,F8.2,a,i3)')         &
                   '*new cen_lat at V point. latidx = ',newlatidx+1,    &
                   ', newctrlat = ',newctrlat,' from patch ', procs(n)
                latset = latset+1
              END IF

            END IF
          END IF

          IF ( lonavg .AND. latavg ) THEN            ! at M point
            IF (TRIM(varname) == 'XLONG' .OR. TRIM(varname) == 'XLAT'  ) THEN
              newlatidx = (oldlatidx1-jstart)/jstride
              newlonidx = (oldlonidx1-istart)/istride+1
              strlen = newlatidx*countidx(1)+newlonidx
              IF (TRIM(varname) == 'XLONG' ) THEN
                newctrlon = vararr(strlen)
                IF (debug > 1) WRITE(6,'(1x,a,I4,a,F8.2,a,i3)')         &
                   '*new cen_lon at M point. lonidx = ',newlonidx,      &
                   ', newctrlon = ',newctrlon,' from patch ',procs(n)
                lonset = lonset+1
              ELSE IF (TRIM(varname) == 'XLAT' ) THEN
                newctrlat = vararr(strlen)
                IF (debug > 1) WRITE(6,'(1x,a,I4,a,F8.2,a,i3)')         &
                   '*new cen_lat at M point. latidx = ',newlatidx,      &
                   ', newctrlat = ',newctrlat,' from patch ', procs(n)
                latset = latset+1
              END IF
            END IF

          END IF

          IF ( .NOT. lonavg .AND. .NOT. latavg ) THEN ! at grid point
            IF (TRIM(varname) == 'XLONG_U' .OR. TRIM(varname) == 'XLAT_V'  ) THEN
              newlatidx = (oldlatidx1-jstart)/jstride
              newlonidx = (oldlonidx1-istart)/istride+1
              strlen = newlatidx*countidx(1)+newlonidx
              IF (TRIM(varname) == 'XLONG_U' ) THEN
                lon1 = vararr(strlen)
                IF (debug > 1) WRITE(6,'(1x,a,F8.2,a,2I4,a,i3)')        &
                    '*set lon1 = ',lon1,' at = ',newlatidx+1,newlonidx, &
                    ' from patch ', procs(n)
              ELSE IF (TRIM(varname) == 'XLAT_V' ) THEN
                lat1 = vararr(strlen)
                IF (debug > 1) WRITE(6,'(1x,a,F8.2,a,2I4,a,i3)')        &
                    '*set lat1 = ',lat1,' at = ',newlatidx+1,newlonidx, &
                    ' from patch ', procs(n)
              END IF
            END IF

          END IF

        END IF

        IF (attadj .AND. (oldlatidx1 >= jps .AND. oldlatidx1 <= jpe) .AND.  &
                         (oldlonidx2 >= ips .AND. oldlonidx2 <= ipe) ) THEN

          IF ( .NOT. lonavg .AND. .NOT. latavg ) THEN ! at grid point

            IF (TRIM(varname) == 'XLAT_V') THEN
              newlatidx = (oldlatidx1-jstart)/jstride
              newlonidx = (oldlonidx2-istart)/istride+1
              strlen = newlatidx*countidx(1)+newlonidx
              lat2 = vararr(strlen)
              IF (debug > 1) WRITE(6,'(9x,a,F8.2,a,2I4)')               &
                  'set lat2 = ',lat2,' at = ',newlatidx+1,newlonidx
            END IF

          END IF

        END IF

        IF (attadj .AND. (oldlatidx2 >= jps .AND. oldlatidx2 <= jpe) .AND.  &
                         (oldlonidx1 >= ips .AND. oldlonidx1 <= ipe) ) THEN

          IF ( .NOT. lonavg .AND. .NOT. latavg ) THEN ! at grid point

            IF (TRIM(varname) == 'XLONG_U') THEN
              newlatidx = (oldlatidx2-jstart)/jstride
              newlonidx = (oldlonidx1-istart)/istride+1
              strlen = newlatidx*countidx(1)+newlonidx
              lon2 = vararr(strlen)
              IF (debug > 1) WRITE(6,'(1x,a,F8.2,a,2I4,a,i3)')          &
                 '*set lon2 = ',lon2,' at = ',newlatidx+1,newlonidx,    &
                 ' from patch ', procs(n)
            END IF

          END IF

        END IF

      END DO var_loop

      istatus = nf_close(finid)                              ! Close file
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
    END DO patch_loop

    unlimdimlen = unlimdimlen + unlimodimlen                 ! Add # of time levels
                                                             ! in output file

!-----------------------------------------------------------------------
!
! Close the output file if applicable
!
!-----------------------------------------------------------------------

    IF (.NOT. jointime .OR. nf == nfile) THEN

      IF (attadj) THEN

        IF ( .NOT. lonavg .AND. .NOT. latavg .AND.                      &
             latset == 0 .AND. lonset == 0) THEN ! at grid point
          newctrlat = 0.5*(lat1+lat2)
          newctrlon = 0.5*(lon1+lon2)
          latset = latset+1
          lonset = lonset+1
        END IF

        IF (latset /= 1 .OR. lonset /= 1) THEN
          WRITE(*,'(1x,2(a,i0),8x,a)')                                  &
            'ERROR: latset = ',latset,', lonset = ',lonset,             &
            'Program aborting ...     Please report.'
          STOP
        END IF

        WRITE(6,'(/,1x,2(a,F8.2),/)')                                   &
           'Changing CEN_LAT & CEN_LON to ',newctrlat,', ',newctrlon

        istatus = NF_REDEF( foutid )
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)

        istatus = NF_PUT_ATT_REAL(foutid,NF_GLOBAL,'CEN_LAT',NF_REAL,1,newctrlat)
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        istatus = NF_PUT_ATT_REAL(foutid,NF_GLOBAL,'MOAD_CEN_LAT',NF_REAL,1,newctrlat)
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)
        istatus = NF_PUT_ATT_REAL(foutid,NF_GLOBAL,'CEN_LON',NF_REAL,1,newctrlon)
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)

        istatus = NF_ENDDEF(foutid)
        IF (istatus /= NF_NOERR) CALL handle_err(istatus)
      END IF

      istatus = nf_close(foutid)
      IF (istatus /= NF_NOERR) CALL handle_err(istatus)
    END IF

  END DO

  RETURN
END SUBROUTINE joinwrfncdf_io
