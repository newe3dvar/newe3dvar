MODULE wrfhybrid_ioapi
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! This module handles writing of WRF netCDF data for WRFHYBRID.
!
! At present, only this two scenarios will be handled
!
!      Running (p), OUT (n)
!
! 1. n=1    ! p = 1 or p > 1
! 2. p=n    !
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  PRIVATE

  LOGICAL :: fileout

  INTEGER :: nxlg, nylg, nx, ny, nz    ! staggered sizes
  INTEGER :: nxout, nyout

  INTEGER :: osx, oex, osy, oey        ! out start(end) x(y)  - relative in subdomain

  INTEGER :: nxfout, nyfout, mfactor

  LOGICAL :: out_onefile

  INTEGER :: debug = 0

  INTEGER, ALLOCATABLE :: iim(:,:), jjm(:,:)
  REAL,    ALLOCATABLE :: wm11(:,:), wm21(:,:), wm12(:,:), wm22(:,:)

  !*********************************************************************
  PUBLIC :: wrfhybrid_ioinit, wrfhybrid_iofinal
  PUBLIC :: wrfhybrid_write2d, wrfhybrid_intrp2d
  PUBLIC :: wrfhybrid_write3d, wrfhybrid_intrp3d

  INCLUDE 'mp.inc'

  CONTAINS

  !###################### initialization ###############################

  SUBROUTINE wrfhybrid_ioinit ( nx_wrf,ny_wrf,nz_wrf,nxmean,nymean,intrpfactor,numdigits,         &
                                filename, numvar, varid_set,            &
                                tmplfilename,filetitle,times,           &
                                fHndl_out,do_open,do_copy,lvldbg, istatus )
    IMPLICIT NONE

    INTEGER,            INTENT(IN)  :: nx_wrf, ny_wrf, nz_wrf
    INTEGER,            INTENT(IN)  :: nxmean, nymean
    REAL,               INTENT(IN)  :: intrpfactor
    CHARACTER(LEN=256), INTENT(IN)  :: filename
    INTEGER,            INTENT(IN)  :: numdigits
    INTEGER,            INTENT(IN)  :: numvar
    CHARACTER(LEN=10),  INTENT(IN)  :: varid_set(numvar)
    CHARACTER(LEN=256), INTENT(IN)  :: tmplfilename
    CHARACTER(LEN=*),   INTENT(IN)  :: filetitle
    CHARACTER(LEN=19),  INTENT(IN)  :: times
    LOGICAL,            INTENT(IN)  :: do_copy,do_open
    INTEGER,            INTENT(OUT) :: fHndl_out
    INTEGER,            INTENT(IN)  :: lvldbg
    INTEGER,            INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: im11, im22, jm11, jm22, imod, jmod
    INTEGER :: mfactor
    REAL    :: denorm

    INTEGER :: n, i, j, iproc
    INTEGER :: nxoutlg, nyoutlg
    CHARACTER(LEN=256) :: message

    INTEGER :: fHndl_tmp
    CHARACTER(LEN=1) :: filemode
    CHARACTER(LEN=20):: fmtstr

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    debug = lvldbg

    nx = nx_wrf; ny = ny_wrf; nz = nz_wrf
    nxlg = (nx-1)*nproc_x + 1
    nylg = (ny-1)*nproc_y + 1

    mfactor = NINT(intrpfactor)
    nxout   = nxmean
    nyout   = nymean

    nxoutlg = (nxout-1)*nproc_x + 1
    nyoutlg = (nyout-1)*nproc_y + 1

    fileout = .FALSE.

    n = nproc_x_out * nproc_y_out

    out_onefile = .FALSE.
    !
    ! It only supports the following
    !
    !  1. Process 0 write
    !  2. All processes write out in one patch
    !  3. All processes write out its own patch
    !
    IF ( n == 1) THEN
      IF (myproc == 0)  fileout = .TRUE.
      osx = 1; oex = nxoutlg;  osy = 1; oey = nyoutlg
      nxfout = nxoutlg; nyfout = nyoutlg
      out_onefile = .TRUE.
    ELSE IF ( nproc_x_out == nproc_x .AND. nproc_y_out == nproc_y ) THEN
      fileout = .TRUE.
      osx = 1; oex = nxout;  osy = 1; oey = nyout
      nxfout = nxout; nyfout = nyout    ! dataset size

    ELSE
      istatus = -2
      message = 'ERROR: unsupported combination of nproc_x_out and nproc_x, or nproc_y_out and nproc_y.'
    END IF

    IF (istatus < 0) THEN
      WRITE(*,'(1x,a)')       TRIM(message)
      WRITE(*,'(7x,3(a,I4))') ' nproc_x = ', nproc_x, ', nproc_x_out = ',nproc_x_out
      WRITE(*,'(7x,3(a,I4))') ' nproc_y = ', nproc_y, ', nproc_y_out = ',nproc_y_out
      CALL arpsstop('ERROR mpi parameters',1)
    END IF

!-----------------------------------------------------------------------
!
! Interpolation parameters
!
!-----------------------------------------------------------------------

    ALLOCATE(iim(nxout,2),      STAT = istatus)
    ALLOCATE(jjm(nyout,2),      STAT = istatus)
    ALLOCATE(wm11(nxout,nyout), STAT = istatus)
    ALLOCATE(wm12(nxout,nyout), STAT = istatus)
    ALLOCATE(wm21(nxout,nyout), STAT = istatus)
    ALLOCATE(wm22(nxout,nyout), STAT = istatus)

    !write(301+myproc,*) nx,ny,nz, nx_m,ny_m,nz_m
    denorm = mfactor*mfactor*1.0
    DO j=1,nyout     ! map from _m dimensions to original dimensions
      jm11 = (j-1)/mfactor+1         ! Align at index 1
      jmod = MOD((j-1),mfactor)
      jm22 = jm11+MIN(jmod,1)
      !IF (jm22 > ny_m) jm22 = 1

      jjm(j,1) = jm11
      jjm(j,2) = jm22
      jmod     = ABS(jmod)
      DO i=1,nxout

        im11 = (i-1)/mfactor+1
        imod = MOD((i-1),mfactor)
        im22 = im11+MIN(imod,1)
        !IF (im22 > nx_m) im22 = 1

        iim(i,1) = im11
        iim(i,2) = im22
        imod     = ABS(imod)

        wm11(i,j) = (mfactor-imod)*(mfactor-jmod)/denorm
        wm21(i,j) = imod*(mfactor-jmod)/denorm
        wm12(i,j) = (mfactor-imod)*jmod/denorm
        wm22(i,j) = imod*jmod/denorm

        !WRITE(*,'(1x,2(I5,a),4(2(I5,a),F5.3,a))') i,',',j,'-> (',       &
        !                       im11,',',jm11,'-',wm11(i,j),'); (',      &
        !                       im11,',',jm22,'-',wm12(i,j),'); (',      &
        !                       im22,',',jm11,'-',wm21(i,j),'); (',      &
        !                       im22,',',jm22,'-',wm22(i,j),').'
      END DO
    END DO
!-----------------------------------------------------------------------
!
! Open files (message is reused as temporary string)
!
!-----------------------------------------------------------------------

    filemode = 'w'

    IF (fileout .AND. do_open) THEN

      WRITE(fmtstr,'(2(a,I0),a)') '(a,I',numdigits,'.',numdigits,')'

      IF (out_onefile) THEN
        WRITE(message,'(a)') TRIM(filename)
      ELSE
        iproc = (loc_y-1)*nproc_x_out + (loc_x-1)
        WRITE(message,fmt=TRIM(fmtstr)) TRIM(filename)//'_',iproc
      END IF

      WRITE(*,'(6x,a,I0,a)') 'Creating '//TRIM(message)//' for output (',myproc,').'
      CALL open_ncd_wrf_file(message,filemode,fHndl_out,istatus)

    END IF

    IF (istatus < 0) THEN
      WRITE(*,'(1x,3a)')   'File : ', TRIM(message), ' not found.'
      CALL arpsstop('ERROR file open operation',1)
    END IF

  !###################### definition ###################################

    IF (fileout .AND. do_open) THEN

      WRITE(*,'(6x,3a)') 'Output file is based on <',TRIM(tmplfilename),'>.'
      CALL open_ncd_wrf_file(tmplfilename,'r',fHndl_tmp,istatus)

      CALL copy_wrf_definitions(fHndl_tmp,fHndl_out,                    &
               .TRUE.,nproc_x_out, nproc_y_out,loc_x,loc_y,             &
               0,varid_set,0,tmplfilename,filetitle,times,istatus)

      IF (do_copy) THEN
        CALL copy_wrfinput_vars(fHndl_tmp,fHndl_out,                    &
                                .FALSE.,numvar,varid_set,istatus)
      END IF

      CALL close_ncd_wrf_file(fHndl_tmp, istatus)

    END IF

    RETURN
  END SUBROUTINE wrfhybrid_ioinit

  !###################### INTERPOLATION #################################

  SUBROUTINE wrfhybrid_intrp3d ( vm,vmout, istatus )
    IMPLICIT NONE

    REAL,    INTENT(IN)  :: vm(nx,ny,nz)
    REAL,    INTENT(OUT) :: vmout(nxout,nyout,nz)
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i, j,k

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    DO k = 1, nz
      DO j = 1, nyout
        DO i = 1, nxout
          vmout(i,j,k) = vm(iim(i,1),jjm(j,1),k)*wm11(i,j)+             &
                         vm(iim(i,2),jjm(j,1),k)*wm21(i,j)+             &
                         vm(iim(i,1),jjm(j,2),k)*wm12(i,j)+             &
                         vm(iim(i,2),jjm(j,2),k)*wm22(i,j)
        END DO
      END DO
    END DO

    RETURN
  END SUBROUTINE wrfhybrid_intrp3d

  !###################### INTERPOLATION #################################

  SUBROUTINE wrfhybrid_intrp2d ( vm,vmout, istatus )
    IMPLICIT NONE

    REAL,    INTENT(IN)  :: vm(nx,ny)
    REAL,    INTENT(OUT) :: vmout(nxout,nyout)
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i, j

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    DO j = 1, nyout
      DO i = 1, nxout
        vmout(i,j) = vm(iim(i,1),jjm(j,1))*wm11(i,j)+                   &
                     vm(iim(i,2),jjm(j,1))*wm21(i,j)+                   &
                     vm(iim(i,1),jjm(j,2))*wm12(i,j)+                   &
                     vm(iim(i,2),jjm(j,2))*wm22(i,j)
      END DO
    END DO

    RETURN
  END SUBROUTINE wrfhybrid_intrp2d

  !###################### WRITE 3d field ###############################

  SUBROUTINE wrfhybrid_write3d ( fHndl_out, vname,vardta,stagger,varout,istatus )
    IMPLICIT NONE

    INTEGER,          INTENT(IN)  :: fHndl_out
    CHARACTER(LEN=*), INTENT(IN)  :: vname
    REAL,             INTENT(IN)  :: vardta(nxout,nyout,nz)
    REAL,             INTENT(OUT) :: varout(nxfout*nyfout*nz)
    CHARACTER(LEN=1), INTENT(IN)  :: stagger
    INTEGER,          INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j,k, iloc, jloc

    INTEGER :: istart, iend, jstart, jend, kstart, kend

    INTEGER :: kout, outsize, nxfsize, nyfsize

    INTEGER :: outstart(4), outcount(4), srcproc, itag

    REAL, ALLOCATABLE :: dtaout(:)
    INTEGER :: nxpout, nypout, nzpout, kin

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0
    IF (out_onefile) THEN

      outstart(1) = (loc_x-1)*(nxout-1)
      outstart(2) = (loc_y-1)*(nyout-1)
      outstart(3) = 1
      outstart(4) = 1

      kstart = 1; kend = nz-1
      IF (stagger == 'Z') kend = nz

      jstart = 1; jend = nyout-1; nyfsize = nyfout-1
      IF (stagger == 'Y' .AND. loc_y == nproc_y) jend = nyout
      IF (stagger == 'Y')  nyfsize = nyfout

      istart = 1; iend = nxout-1; nxfsize = nxfout-1
      IF (stagger == 'X' .AND. loc_x == nproc_x ) iend = nxout
      IF (stagger == 'X')  nxfsize = nxfout

      nxpout = (iend-istart+1)
      nypout = (jend-jstart+1)
      nzpout = (kend-kstart+1)
      ALLOCATE(dtaout(nxpout*nypout*nzpout), STAT = istatus)

      DO k = kstart, kend
        DO j = jstart, jend
          DO i = istart, iend
            kout = (k-kstart)*nypout*nxpout+(j-jstart)*nxpout+i-istart+1
            dtaout(kout) = vardta(i,j,k)
          END DO
        END DO
      END DO

      outcount(1) = nxpout
      outcount(2) = nypout
      outcount(3) = nzpout
      outcount(4) = 1

      CALL inctag
      itag = gentag

      IF (myproc == 0) THEN
        DO k = kstart, kend
          DO j = jstart, jend
            DO i = istart, iend
              kout = (k-kstart)*nyfsize*nxfsize+(j-jstart+outstart(2))*nxfsize+i-istart+1+outstart(1)
              varout(kout) = vardta(i,j,k)
            END DO
          END DO
        END DO

        DO jloc = 1, nproc_y
          DO iloc = 1, nproc_x
            srcproc = (jloc-1)*nproc_x + iloc-1
            IF (srcproc /= 0) THEN
              CALL mprecvi(outstart,4,srcproc,itag+10,istatus)
              CALL mprecvi(outcount,4,srcproc,itag+20,istatus)
              outsize = outcount(1)*outcount(2)*outcount(3)
              CALL mprecvr(dtaout,outsize,srcproc,itag+30,istatus)

              DO k = 1, outcount(3)
                DO j = 1, outcount(2)
                  DO i = 1, outcount(1)
                    kout = (k-1)*nyfsize*nxfsize+(j-1+outstart(2))*nxfsize+i+outstart(1)
                    kin  = (k-1)*outcount(2)*outcount(1)+(j-1)*outcount(1)+i
                    varout(kout) = dtaout(kin)
                  END DO
                END DO
              END DO

            END IF
          END DO
        END DO

        !
        ! Finally, write the dataset
        !
        outsize = nxfsize*nyfsize*outcount(3)
        outstart(:) = 1
        outcount(1) = nxfsize
        outcount(2) = nyfsize

        IF (debug > 1) WRITE(*,'(1x,3a)') ' Writing ',TRIM(vname),' ......'
        CALL write_wrf_vara_real(fHndl_out,vname,varout,                &
                                 outsize,outstart,outcount,istatus)

      ELSE
        CALL mpsendi(outstart,4,0,itag+10,istatus)
        CALL mpsendi(outcount,4,0,itag+20,istatus)
        outsize = outcount(1)*outcount(2)*outcount(3)
        CALL mpsendr(dtaout,outsize,0,itag+30,istatus)

      END IF

      DEALLOCATE(dtaout)

    ELSE

      kstart = 1; kend = nz-1
      IF (stagger == 'Z') kend = nz

      jstart = 1; jend = nyout-1
      IF (stagger == 'Y' .AND. loc_y == nproc_y) jend = nyout

      istart = 1; iend = nxout-1
      IF (stagger == 'X' .AND. loc_x == nproc_x ) iend = nxout

      DO k = kstart, kend
        DO j = jstart, jend
          DO i = istart, iend
            kout = (k-kstart)*(jend-jstart+1)*(iend-istart+1)+(j-jstart)*(iend-istart+1)+i-istart+1
            varout(kout) = vardta(i,j,k)
          END DO
        END DO
      END DO

      outsize = (kend-kstart+1)*(jend-jstart+1)*(iend-istart+1)
      CALL write_wrf_var_real(fHndl_out,vname,varout,outsize,istatus)

    END IF

    RETURN
  END SUBROUTINE wrfhybrid_write3d

  !###################### WRITE 2d field ###############################

  SUBROUTINE wrfhybrid_write2d ( fHndl_out, vname,vardta,stagger,varout,istatus )
    IMPLICIT NONE

    INTEGER,          INTENT(IN)  :: fHndl_out
    CHARACTER(LEN=*), INTENT(IN)  :: vname
    REAL,             INTENT(IN)  :: vardta(nxout,nyout)
    REAL,             INTENT(OUT) :: varout(nxfout*nyfout)
    CHARACTER(LEN=1), INTENT(IN)  :: stagger
    INTEGER,          INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j,k, iloc, jloc

    INTEGER :: istart, iend, jstart, jend, kstart, kend

    INTEGER :: kout, outsize, nxfsize, nyfsize

    INTEGER :: outstart(4), outcount(4), srcproc, itag

    REAL, ALLOCATABLE :: dtaout(:)
    INTEGER :: nxpout, nypout, nzpout, kin

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0
    IF (out_onefile) THEN

      outstart(1) = (loc_x-1)*(nxout-1)
      outstart(2) = (loc_y-1)*(nyout-1)
      outstart(3) = 1
      outstart(4) = 1

      jstart = 1; jend = nyout-1; nyfsize = nyfout-1
      IF (stagger == 'Y' .AND. loc_y == nproc_y) jend = nyout
      IF (stagger == 'Y')  nyfsize = nyfout

      istart = 1; iend = nxout-1; nxfsize = nxfout-1
      IF (stagger == 'X' .AND. loc_x == nproc_x ) iend = nxout
      IF (stagger == 'X')  nxfsize = nxfout

      nxpout = (iend-istart+1)
      nypout = (jend-jstart+1)
      ALLOCATE(dtaout(nxpout*nypout), STAT = istatus)

      DO j = jstart, jend
        DO i = istart, iend
          kout = (j-jstart)*nxpout+i-istart+1
          dtaout(kout) = vardta(i,j)
        END DO
      END DO

      outcount(1) = nxpout
      outcount(2) = nypout
      outcount(3) = 1

      CALL inctag
      itag = gentag

      IF (myproc == 0) THEN
        DO j = jstart, jend
          DO i = istart, iend
            kout = (j-jstart+outstart(2))*nxfsize+i-istart+1+outstart(1)
            varout(kout) = vardta(i,j)
          END DO
        END DO

        DO jloc = 1, nproc_y
          DO iloc = 1, nproc_x
            srcproc = (jloc-1)*nproc_x + iloc-1
            IF (srcproc /= 0) THEN
              CALL mprecvi(outstart,3,srcproc,itag+10,istatus)
              CALL mprecvi(outcount,3,srcproc,itag+20,istatus)
              outsize = outcount(1)*outcount(2)
              CALL mprecvr(dtaout,outsize,srcproc,itag+30,istatus)

              DO j = 1, outcount(2)
                DO i = 1, outcount(1)
                  kout = (j-1+outstart(2))*nxfsize+i+outstart(1)
                  kin  = (j-1)*outcount(1)+i
                  varout(kout) = dtaout(kin)
                END DO
              END DO

            END IF
          END DO
        END DO

        !
        ! Finally, write the dataset
        !
        outsize = nxfsize*nyfsize
        outstart(:) = 1
        outcount(1) = nxfsize
        outcount(2) = nyfsize

        IF (debug > 1) WRITE(*,'(1x,3a)') ' Writing ',TRIM(vname),' ......'
        CALL write_wrf_vara_real(fHndl_out,vname,varout,                &
                                 outsize,outstart,outcount,istatus)

      ELSE
        CALL mpsendi(outstart,3,0,itag+10,istatus)
        CALL mpsendi(outcount,3,0,itag+20,istatus)
        outsize = outcount(1)*outcount(2)
        CALL mpsendr(dtaout,outsize,0,itag+30,istatus)

      END IF

      DEALLOCATE(dtaout)

    ELSE

      jstart = 1; jend = nyout-1
      IF (stagger == 'Y' .AND. loc_y == nproc_y) jend = nyout

      istart = 1; iend = nxout-1
      IF (stagger == 'X' .AND. loc_x == nproc_x ) iend = nxout

      DO j = jstart, jend
        DO i = istart, iend
          kout = (j-jstart)*(iend-istart+1)+i-istart+1
          varout(kout) = vardta(i,j)
        END DO
      END DO

      outsize = (jend-jstart+1)*(iend-istart+1)
      CALL write_wrf_var_real(fHndl_out,vname,varout,outsize,istatus)

    END IF

    RETURN
  END SUBROUTINE wrfhybrid_write2d

  !###################### finalization #################################

  SUBROUTINE wrfhybrid_iofinal ( fHndl_out, istatus )
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: fHndl_out
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i, j

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (fileout) THEN
       CALL close_ncd_wrf_file(fHndl_out, istatus)
    END IF

    RETURN
  END SUBROUTINE wrfhybrid_iofinal

  !********************* PRIVATE SUBROUTINES ***************************

END MODULE wrfhybrid_ioapi
