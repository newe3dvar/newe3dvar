!
!##################################################################
!##################################################################
!######                                                      ######
!######           MODULE WRF_DAUPDATE_NETCDF_INTERFACE       ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
!-----------------------------------------------------------------------
!
! AUTHOR: Y. Wang (01/29/2014)
!
! MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------
!
MODULE wrf_daupdate_netcdf_interface

  IMPLICIT NONE

  INTEGER, PARAMETER :: STDERR = 0
  INTEGER, PARAMETER :: STDOUT = 6
  INTEGER, PARAMETER :: STDIN  = 5

  INTEGER, PARAMETER :: MAXTIMES = 100

  INCLUDE 'netcdf.inc'

  REAL,   ALLOCATABLE :: vartmp(:)

CONTAINS

  SUBROUTINE init_netcdf_api(nx,ny,nz,nxf,nyf,bdyzone,istatus)

    INTEGER, INTENT(IN)  :: nx, ny, nz, bdyzone
    INTEGER, INTENT(IN)  :: nxf,nyf
    INTEGER, INTENT(OUT) :: istatus
  !---------------------------------------------------------------------

    INTEGER :: nsize, psize
    INTEGER :: n

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    istatus = 0

    IF (bdyzone > 1) THEN
      nsize = MAX(nx,ny)*nz*bdyzone
      psize = nxf*nyf*nz

      nsize = MAX(psize,nsize)

    ELSE
      nsize = nxf*nyf*nz
    END IF

    IF (ALLOCATED(vartmp)) DEALLOCATE(vartmp)

    WRITE(STDOUT,'(1x,a,I0)') 'Allocating vartmp with size ',nsize
    ALLOCATE(vartmp(nsize), STAT = istatus )

  END SUBROUTINE init_netcdf_api

  SUBROUTINE finalize_netcdf_api( istatus )

    INTEGER, INTENT(OUT) :: istatus
  !---------------------------------------------------------------------


  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    istatus = 0

    DEALLOCATE(vartmp, STAT = istatus )

  END SUBROUTINE finalize_netcdf_api

  !#####################################################################

  SUBROUTINE open_wrf_files(files,nfiles,fhandles,mode,debug,istatus)

    INTEGER,            INTENT(IN)  :: nfiles
    CHARACTER(LEN=256), INTENT(IN)  :: files(nfiles)
    INTEGER,            INTENT(OUT) :: fhandles(nfiles)
    INTEGER,            INTENT(IN)  :: mode, debug
    INTEGER,            INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    LOGICAL :: fexists
    INTEGER :: n
    INTEGER :: fmode

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (mode == 0) THEN
      fmode = NF_NOWRITE
    ELSE
      fmode = NF_WRITE
    END IF

    IF (debug > 19) WRITE(STDOUT,'(1x,3a)') 'Opening file ',TRIM(files(1)),' ...'

    DO n = 1, nfiles

      INQUIRE(FILE = files(n), EXIST = fexists)
      IF (fexists) THEN
        istatus = NF_OPEN(TRIM(files(n)),fmode,fhandles(n))
        IF (debug == 21) WRITE(STDOUT,'(1x,3a,I0)') 'File ',TRIM(files(n)),' opened as ',fhandles(n)
        CALL nf_handle_error(istatus,'NF_OPEN in open_wrf_files')
      ELSE
        WRITE(6,'(2a)') 'File not found: ', files(n)
        CALL nf_handle_error(istatus,'NF_OPEN in open_wrf_files')
      ENDIF
    END DO

  END SUBROUTINE open_wrf_files

  !##################################################################

  SUBROUTINE get_wrf_input_dims(fhandles,nfiles,nxd,nyd,nzd,nzs,        &
                            iss,ise,ips,ipe,jss,jse,jps,jpe,istatus)

    INTEGER, INTENT(IN)  :: nfiles
    INTEGER, INTENT(IN)  :: fhandles(nfiles)
    INTEGER, INTENT(OUT) :: nxd, nyd, nzd, nzs
    INTEGER, INTENT(OUT) :: iss(nfiles),ise(nfiles),ips(nfiles),ipe(nfiles)
    INTEGER, INTENT(OUT) :: jss(nfiles),jse(nfiles),jps(nfiles),jpe(nfiles)
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------
    INTEGER :: n, dimid

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    istatus = NF_GET_ATT_INT(fhandles(1),NF_GLOBAL,'WEST-EAST_GRID_DIMENSION',  nxd)
    CALL nf_handle_error(istatus,'WEST-EAST_GRID_DIMENSION in get_wrf_input_dims')
    istatus = NF_GET_ATT_INT(fhandles(1),NF_GLOBAL,'SOUTH-NORTH_GRID_DIMENSION',nyd)
    CALL nf_handle_error(istatus,'SOUTH-NORTH_GRID_DIMENSION in get_wrf_input_dims')
    istatus = NF_GET_ATT_INT(fhandles(1),NF_GLOBAL,'BOTTOM-TOP_GRID_DIMENSION', nzd)
    CALL nf_handle_error(istatus,'BOTTOM-TOP_GRID_DIMENSION in get_wrf_input_dims')

    istatus = NF_INQ_DIMID(fhandles(1),'soil_layers_stag',dimid)
    CALL nf_handle_error(istatus,'soil_layers_stag in get_wrf_input_dims')
    istatus = NF_INQ_DIMLEN(fhandles(1),dimid,nzs)
    CALL nf_handle_error(istatus,'NF_INQ_DIMLEN in get_wrf_input_dims')

    DO n = 1, nfiles
      istatus = NF_GET_ATT_INT(fhandles(n),NF_GLOBAL,'WEST-EAST_PATCH_START_UNSTAG',iss(n))
      CALL nf_handle_error(istatus,'WEST-EAST_PATCH_START_UNSTAG in get_wrf_input_dims')
      istatus = NF_GET_ATT_INT(fhandles(n),NF_GLOBAL,'WEST-EAST_PATCH_END_UNSTAG',  ise(n))
      CALL nf_handle_error(istatus,'WEST-EAST_PATCH_END_UNSTAG in get_wrf_input_dims')
      istatus = NF_GET_ATT_INT(fhandles(n),NF_GLOBAL,'WEST-EAST_PATCH_START_STAG',  ips(n))
      CALL nf_handle_error(istatus,'WEST-EAST_PATCH_START_STAG in get_wrf_input_dims')
      istatus = NF_GET_ATT_INT(fhandles(n),NF_GLOBAL,'WEST-EAST_PATCH_END_STAG',    ipe(n))
      CALL nf_handle_error(istatus,'WEST-EAST_PATCH_END_STAG in get_wrf_input_dims')

      istatus = NF_GET_ATT_INT(fhandles(n),NF_GLOBAL,'SOUTH-NORTH_PATCH_START_UNSTAG',jss(n))
      CALL nf_handle_error(istatus,'SOUTH-NORTH_PATCH_START_UNSTAG in get_wrf_input_dims')
      istatus = NF_GET_ATT_INT(fhandles(n),NF_GLOBAL,'SOUTH-NORTH_PATCH_END_UNSTAG',  jse(n))
      CALL nf_handle_error(istatus,'SOUTH-NORTH_PATCH_END_UNSTAG in get_wrf_input_dims')
      istatus = NF_GET_ATT_INT(fhandles(n),NF_GLOBAL,'SOUTH-NORTH_PATCH_START_STAG',  jps(n))
      CALL nf_handle_error(istatus,'SOUTH-NORTH_PATCH_START_STAG in get_wrf_input_dims')
      istatus = NF_GET_ATT_INT(fhandles(n),NF_GLOBAL,'SOUTH-NORTH_PATCH_END_STAG',    jpe(n))
      CALL nf_handle_error(istatus,'SOUTH-NORTH_PATCH_END_STAG in get_wrf_input_dims')

    END DO

    IF (iss(1) /= 1 .OR. ips(1) /= 1 .OR. ise(nfiles) /= nxd-1 .OR. ipe(nfiles) /= nxd ) THEN
      WRITE(*,'(1x,a,/,1x,a,2(a,I0,a,I7),/,1x,2(a,I3),2(a,I0,a,I7),/)')           &
          'ERROR: Wrong dimension size. '//                                       &
          'Maybe wrong namelist parameter for number of patches in X direction.', &
          '       Expected iss(1) =   1, ips(1) =   1',                           &
                        ', ise(',nfiles,') = ',nxd-1, ', ipe(',nfiles,') = ',nxd, &
          '       Found    iss(1) = ',iss(1),', ips(1) = ',ips(1),                &
                        ', ise(',nfiles,') = ',ise(nfiles), ', ipe(',nfiles,') = ',ipe(nfiles)

      istatus = -1
    END IF

    IF (jss(1) /= 1 .OR. jps(1) /= 1 .OR. jse(nfiles) /= nyd-1 .OR. jpe(nfiles) /= nyd ) THEN
      WRITE(*,'(1x,a,/,1x,a,2(a,I0,a,I7),/,1x,2(a,I3),2(a,I0,a,I7),/)')           &
          'ERROR: Wrong dimension size. '//                                       &
          'Maybe wrong namelist parameter for number of patches in Y direction.', &
          '       Expected jss(1) =   1, jps(1) =   1',                           &
                        ', jse(',nfiles,') = ',nyd-1, ', jpe(',nfiles,') = ',nyd, &
          '       Found    jss(1) = ',jss(1),', jps(1) = ',jps(1),                &
                        ', jse(',nfiles,') = ',jse(nfiles), ', jpe(',nfiles,') = ',jpe(nfiles)

      istatus = -2
    END IF

    RETURN
  END SUBROUTINE get_wrf_input_dims

  !##################################################################

  SUBROUTINE get_wrf_input_time(ncid,da_time,istatus)

    INTEGER,           INTENT(IN)  :: ncid
    CHARACTER(LEN=19), INTENT(OUT) :: da_time
    INTEGER,           INTENT(OUT) :: istatus

  !------------------------------------------------------------------
  !
  !  Misc. local variables
  !
  !------------------------------------------------------------------
    INTEGER :: dimid, varid
    INTEGER :: timelen, strlen, dateStrLen

    INTEGER :: itime

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    itime = 1

    istatus = NF_INQ_DIMID(ncid,'Time',dimid)
    CALL nf_handle_error(istatus,'NF_INQ_DIMID in get_wrf_input_time')
    istatus = NF_INQ_DIMLEN(ncid,dimid,timelen)
    CALL nf_handle_error(istatus,'NF_INQ_DIMLEN in get_wrf_input_time')

    istatus = NF_INQ_DIMID(ncid,'DateStrLen',dimid)
    CALL nf_handle_error(istatus,'NF_INQ_DIMID in get_wrf_input_time')
    istatus = NF_INQ_DIMLEN(ncid,dimid,dateStrLen)
    CALL nf_handle_error(istatus,'NF_INQ_DIMLEN in get_wrf_input_time')

    strlen = LEN(da_time)
    IF(strlen < dateStrLen) THEN
      WRITE(6,'(a)') ' Date string is not long enough to hold Times.'
      STOP 'timestr_too_short'
    END IF

    istatus = NF_INQ_VARID(ncid, 'Times', varid)
    CALL nf_handle_error(istatus,'NF_INQ_VARID in get_wrf_input_time')
    istatus = NF_GET_VARA_TEXT(ncid,varid,(/1,itime/),                  &
                               (/dateStrLen,1/),da_time)
    CALL nf_handle_error(istatus,'NF_GET_VARA_TEXT in get_wrf_input_time')

  END SUBROUTINE get_wrf_input_time

  !##################################################################

  SUBROUTINE get_bdyin_times(ncid,next_time_str,next_itime,itime,istatus)

    INTEGER,           INTENT(IN)  :: ncid
    CHARACTER(LEN=19), INTENT(OUT) :: next_time_str(MAXTIMES)
    INTEGER,           INTENT(OUT) :: next_itime(MAXTIMES), itime
    INTEGER,           INTENT(OUT) :: istatus

  !------------------------------------------------------------------
  !
  !  Misc. local variables
  !
  !------------------------------------------------------------------
    INTEGER :: dimid, varid
    INTEGER :: timelen, dateStrLen
    INTEGER :: i

    INTEGER :: year, month, day, hour, minute, second
    INTEGER :: itime1, itime2
    CHARACTER(LEN=1) :: ach

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    istatus = NF_INQ_DIMID(ncid,'Time',dimid)
    CALL nf_handle_error(istatus,'NF_INQ_DIMID in get_bdyin_times')
    istatus = NF_INQ_DIMLEN(ncid,dimid,timelen)
    CALL nf_handle_error(istatus,'NF_INQ_DIMLEN in get_bdyin_times')

    istatus = NF_INQ_DIMID(ncid,'DateStrLen',dimid)
    CALL nf_handle_error(istatus,'NF_INQ_DIMID in get_bdyin_times')
    istatus = NF_INQ_DIMLEN(ncid,dimid,dateStrLen)
    CALL nf_handle_error(istatus,'NF_INQ_DIMLEN in get_bdyin_times')

    !
    ! Get the first valid time strings in the bdy file
    !
    istatus = NF_INQ_VARID(ncid, 'md___thisbdytimee_x_t_d_o_m_a_i_n_m_e_t_a_data_', varid)
    CALL nf_handle_error(istatus,'NF_INQ_VARID in get_bdyin_times')
    istatus = NF_GET_VARA_TEXT(ncid,varid,(/1,1/),                  &
                               (/dateStrLen,timelen/),next_time_str)
    CALL nf_handle_error(istatus,'NF_GET_VARA_TEXT in get_bdyin_times')

    itime = timelen+1
    istatus = NF_INQ_VARID(ncid, 'md___nextbdytimee_x_t_d_o_m_a_i_n_m_e_t_a_data_', varid)
    CALL nf_handle_error(istatus,'NF_INQ_VARID in get_bdyin_times')
    istatus = NF_GET_VARA_TEXT(ncid,varid,(/1,timelen/),                  &
                               (/dateStrLen,1/),next_time_str(itime))
    CALL nf_handle_error(istatus,'NF_GET_VARA_TEXT in get_bdyin_times')

    !
    ! get bdy frequency in seconds
    !
    !print *, frst_time, last_time
    DO i = 1, itime
      READ(next_time_str(i),'(I4,5(a,I2))') year, ach, month, ach, day,ach,  &
                                            hour,ach,  minute,ach,  second
      CALL ctim2abss( year, month, day, hour, minute, second, next_itime(i) )
    END DO


    !!
    !! Last, get the last valid time in bdy file
    !!
    !itime = timelen
    !istatus = NF_INQ_VARID(ncid, 'md___nextbdytimee_x_t_d_o_m_a_i_n_m_e_t_a_data_', varid)
    !CALL nf_handle_error(istatus,'NF_INQ_VARID in get_bdyin_times')
    !istatus = NF_GET_VARA_TEXT(ncid,varid,(/1,itime/),                  &
    !                           (/dateStrLen,1/),last_time)
    !CALL nf_handle_error(istatus,'NF_GET_VARA_TEXT in get_bdyin_times')
    !
    !itime = timelen+1

    RETURN
  END SUBROUTINE get_bdyin_times

  !##################################################################

  SUBROUTINE close_wrf_files(fhandles,nfiles,istatus)
  !
  !------------------------------------------------------------------
  !
  !  PURPOSE:
  !
  !     Close the WRF file which is opened using open_wrf_files.
  !
  !------------------------------------------------------------------
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nfiles
    INTEGER, INTENT(IN)  :: fhandles(nfiles)
    INTEGER, INTENT(OUT) :: istatus

  !------------------------------------------------------------------
  !
  !  Misc. local variable
  !
  !------------------------------------------------------------------
  !
    INTEGER :: n

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !

    DO n = 1, nfiles
      istatus = NF_CLOSE(fhandles(n))
      CALL nf_handle_error(istatus,'NF_CLOSE in close_wrf_files')
    END DO

    RETURN
  END SUBROUTINE close_wrf_files

  !##################################################################

  SUBROUTINE set_bdyout_mode(da_time,end_time_str,                      &
                         next_time_str,next_itime,ntime,                &
                         bdymode,stime,etime,intnew,intvda,             &
                         debug,istatus)
  !---------------------------------------------------------------------

    INTEGER,           INTENT(IN)  :: ntime
    CHARACTER(LEN=19), INTENT(IN)  :: da_time, end_time_str,next_time_str(ntime)
    INTEGER,           INTENT(IN)  :: next_itime(ntime)
    INTEGER,           INTENT(OUT) :: bdymode
    INTEGER,           INTENT(OUT) :: stime,etime, intnew, intvda
    INTEGER,           INTENT(IN)  :: debug
    INTEGER,           INTENT(OUT) :: istatus
  !---------------------------------------------------------------------

    INTEGER :: year, month, day, hour, minute, second
    INTEGER :: itime1, itime2, itimes, itimee
    CHARACTER(LEN=1) :: ach
    INTEGER :: n, lastn, tbgn,tend
    LOGICAL :: unsupport

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    READ(da_time,'(I4,5(a,I2))') year,ach,  month,ach,  day, ach, hour,ach,  minute, ach, second
    IF (debug > 40) WRITE(*,'(1x,a,I4.4,5(a,I2.2))') 'DA_TIME  = ',     &
                                 year,ach,  month,ach,  day, ach, hour,ach,  minute, ach, second
    CALL ctim2abss( year, month, day, hour, minute, second, itime1 )

    READ(end_time_str,'(I4,5(a,I2))') year,ach, month,ach, day,ach, hour, ach, minute, ach, second
    IF (debug > 40) WRITE(*,'(1x,a,I4.4,5(a,I2.2))') 'END_TIME = ',     &
                                 year,ach,  month, ach, day, ach, hour,ach,  minute, ach, second
    CALL ctim2abss( year, month, day, hour, minute, second, itime2 )

    !READ(frst_time,'(I4,5(a,I2))') year,ach,  month,ach,  day, ach, hour, ach, minute, ach, second
    !IF (debug > 40) WRITE(*,'(1x,a,I4.4,5(a,I2.2))') 'BDY_FRST = ',     &
    !                             year,ach,  month,ach,  day, ach, hour,ach,  minute, ach, second
    !CALL ctim2abss( year, month, day, hour, minute, second, itimes )
    !
    !READ(last_time,'(I4,5(a,I2))') year, ach, month, ach, day,ach,  hour, ach, minute, ach, second
    !IF (debug > 40) WRITE(*,'(1x,a,I4.4,5(a,I2.2))') 'BDY_END  = ',     &
    !                             year,ach,  month,ach,  day, ach, hour,ach,  minute, ach, second
    !CALL ctim2abss( year, month, day, hour, minute, second, itimee )

    unsupport = .TRUE.
    bdymode = 0
    DO n = 1, ntime-1
        IF (itime1 >= next_itime(n) .AND. itime1 < next_itime(n+1)) THEN
            stime   = n
            unsupport = .FALSE.
            EXIT
        END IF
    END DO

    !lastn = (itimee-itimes)/intsec
    !IF (itime1 >= itimes .AND. itime2 > itime1) THEN
    !  DO n = 1, lastn
    !    tbgn = itimes+(n-1)*intsec
    !    tend = itimes+n*intsec
    !    IF (itime1 >= tbgn .AND. itime1 < tend ) THEN
    !      stime = n
    !      IF (itime2 <= tend) THEN
    !        bdymode = 1           ! EnKF cycling
    !        etime   = stime+1
    !        intnew  = tend-itime1
    !        intvda  = itime1-tbgn
    !        unsupport = .FALSE.
    !      ELSE IF (itime2 == itimee .AND. itime1 == tbgn) THEN
    !        bdymode = 2           ! Forecast continued from tbgn
    !        etime   = lastn+1
    !        intnew  = intsec
    !        intvda  = 0
    !        unsupport = .FALSE.
    !      END IF
    !      EXIT
    !    END IF
    !  END DO
    !END IF

    IF ( unsupport ) THEN
      WRITE(STDOUT,'(1x,a,2(a,I12),/,29x,2(a,I12))') 'Unsupported bdy mode because', &
              'DA request: itime1 = ',itime1,', itime2 = ',itime2,                   &
              'BDY   file: itimes = ',next_itime(1),', itimee = ',next_itime(ntime)
      istatus = 1
      bdymode = -1
    ELSE

      etime = ntime
      IF (itime2 > itime1 ) THEN
          DO n = stime+1, ntime
              IF (itime2 <= next_itime(n)) THEN
                  !bdymod = 2
                  etime = n
              END IF
          END DO
      END IF

      intnew  = next_itime(stime+1)-itime1
      intvda  = itime1-next_itime(stime)
    END IF

    RETURN
  END SUBROUTINE set_bdyout_mode

  !##################################################################

  SUBROUTINE create_bdyout_schema(file_out,file_in,fortran_mode,debug,istatus)

    CHARACTER(LEN=256), INTENT(IN)  :: file_in, file_out
    LOGICAL,            INTENT(IN)  :: fortran_mode
    INTEGER,            INTENT(IN)  :: debug
    INTEGER,            INTENT(OUT) :: istatus
  !---------------------------------------------------------------------

    LOGICAL :: fexists
    CHARACTER(256) :: cmdstr

    INTEGER :: fHndl_in, fHndl_out
    INTEGER :: informat, outformat
    CHARACTER(LEN=20) :: nothing(1)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    INQUIRE(FILE = file_out, EXIST = fexists)
    IF (fexists) THEN
      WRITE(STDOUT,'(1x,3a)') '=== Using boundary file schema in ',TRIM(file_out),'.'
      RETURN
    ELSE

      IF (fortran_mode) THEN   ! fortran copying schema

        IF (debug > 0) WRITE(*,'(1x,a)') 'Opening '//TRIM(file_in)//' for read.'

        istatus = NF_OPEN(TRIM(file_in),NF_NOWRITE,fHndl_in)
        CALL nf_handle_error(istatus,'NF_OPEN in create_bdyout_schema')

        istatus = NF_INQ_FORMAT(fHndl_in,informat)
        CALL nf_handle_error(istatus,'NF_INQ_FORMAT in create_bdyout_schema')
        SELECT CASE (informat)
          CASE (NF_FORMAT_CLASSIC)
            outformat = NF_CLOBBER
          CASE (NF_FORMAT_64BIT)
            outformat = NF_64BIT_OFFSET
          CASE (NF_FORMAT_NETCDF4)
            outformat = NF_NETCDF4
          CASE (NF_FORMAT_NETCDF4_CLASSIC)
            outformat = NF_CLASSIC_MODEL
          CASE DEFAULT
            WRITE(STDOUT,'(1x,a,I0,3a)')                                &
              'ERROR: Unsupported input netCDF file format: ',informat, &
              ' in ',TRIM(file_in),'.'
            istatus = -1
            RETURN
        END SELECT

        IF (debug > 0) WRITE(*,'(1x,a)') 'Creating '//TRIM(file_out)//'.'

        istatus = NF_CREATE(TRIM(file_out),outformat,fHndl_out)
        CALL nf_handle_error(istatus,'NF_CREATE in create_bdyout_schema')

        CALL copy_netcdf_definition(fHndl_in,fHndl_out,.FALSE.,1, 1,     &
                                    1,1, 0,nothing,debug,istatus)
        IF (istatus /= 0) CALL nf_handle_error(istatus,'copy_netcdf_definition')

        istatus = NF_CLOSE(fHndl_in)
        CALL nf_handle_error(istatus,'create_bdyout_schema:nf_close')

        istatus = NF_CLOSE(fHndl_out)
        CALL nf_handle_error(istatus,'create_bdyout_schema:nf_close')

      ELSE                                           ! command-line mode
        WRITE(cmdstr,'(5a)') 'ncdump -h ',TRIM(file_in),' | ncgen -x -o ',TRIM(file_out),' --'

        WRITE(STDOUT,'(1x,a)') '=== ********************************* ==='
        WRITE(STDOUT,'(1x,a,/,5x,2a)')                                    &
          '    Trying to run command str: ','  $> ',TRIM(cmdstr)
        WRITE(STDOUT,'(1x,a,/,5x,a)')                                     &
          '    If Fortran "system" function is not implemented on the system,',  &
              'Users can run this command from command line beforehand.'
        WRITE(STDOUT,'(1x,a)') '=== ********************************* ==='

        CALL unixcmd(cmdstr)
      END IF

    END IF

  END SUBROUTINE create_bdyout_schema

  !#####################################################################

  SUBROUTINE find_bdy_datasets(fin,nexcl,exclnames,                     &
                         nmiss,missnames,noutvar,outnames,debug,istatus)

  !---------------------------------------------------------------------
  !
  ! PURPOSE:
  !
  !   Find dataset names in the input boundary file, excluding those
  !   analyed variables
  !
  !---------------------------------------------------------------------
  !
  ! AUTHOR: Y. Wang (03/13/2014)
  !
  !---------------------------------------------------------------------
    INTEGER,           INTENT(IN)  :: fin

    INTEGER,           INTENT(in)  :: nexcl
    CHARACTER(LEN=10), INTENT(in)  :: exclnames(nexcl)

    INTEGER,           INTENT(out)  :: noutvar, nmiss
    CHARACTER(LEN=20), INTENT(out)  :: outnames(100), missnames(100)

    INTEGER,           INTENT(IN)  :: debug
    INTEGER,           INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: varid, nvars

    CHARACTER(LEN=60) :: datname
    CHARACTER(LEN=20) :: varname

    INTEGER :: indx_

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    nmiss = 0
    missnames(:) =  ' '
    noutvar = 0
    outnames(:) = ' '

  !---------------------------------------------------------------------
  !
  ! Check variables one by one
  !
  !---------------------------------------------------------------------

    istatus = NF_INQ_NVARS(fin,nvars)
    IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'find_bdy_datasets:nf_inq_nvars')

    DO varid = 1, nvars
      datname = ' '
      varname = ' '
      istatus = NF_INQ_VARNAME(fin,varid,datname)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'find_bdy_datasets:nf_inq_varname')

      IF (datname(1:2) == 'MU' .OR. TRIM(datname) == 'Times') CYCLE
      IF (INDEX(datname,'bdytime') > 0) CYCLE

      indx_ = INDEX(datname,'_',.TRUE.)
      varname = datname(1:indx_-1)

      IF (.NOT. name_in_set(varname,outnames,noutvar)) THEN
        IF (debug > 0) WRITE(STDOUT,'(1x,3a)') 'Adding ',TRIM(varname),' to copy list ...'
        noutvar = noutvar + 1           ! All other variable except times and MU
        outnames(noutvar)= TRIM(varname)
      END IF

      IF (nexcl > 0 .AND. (name_in_set(varname,exclnames,nexcl)) ) CYCLE

      IF (.NOT. name_in_set(varname,missnames,nmiss)) THEN
        IF (debug > 0) WRITE(STDOUT,'(1x,3a)') 'Adding ',TRIM(varname),' to miss list ...'
        nmiss = nmiss + 1               ! All other variables that are not analyzed
        missnames(nmiss) = TRIM(varname)
      END IF
    END DO

    RETURN
  END SUBROUTINE find_bdy_datasets

  !#####################################################################

  SUBROUTINE get_wrf_da2d(hfiles, nfile,varname, var2d, nx, ny,         &
                          ips,ipe,jps,jpe,debug,istatus)

  !---------------------------------------------------------------------
  ! Purpose: Read a 2D real field from WRF files
  !---------------------------------------------------------------------

    INTEGER,          INTENT(IN)  :: nx, ny, nfile
    INTEGER,          INTENT(IN)  :: hfiles(nfile)
    CHARACTER(LEN=*), INTENT(IN)  :: varname
    REAL,             INTENT(OUT) :: var2d(nx,ny)
    INTEGER,          INTENT(IN)  :: ips(nfile),ipe(nfile)
    INTEGER,          INTENT(IN)  :: jps(nfile),jpe(nfile)
    INTEGER,          INTENT(IN)  :: debug
    INTEGER,          INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: n, varid
    INTEGER :: vartype, ndims
    INTEGER :: vardimids(NF_MAX_DIMS)
    INTEGER :: varnatts, nsizes(3)
    INTEGER :: vdim, dimlen

    INTEGER :: i, j, kout

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


    IF (debug > 39) WRITE(STDOUT,'(3x,3a)') 'Reading ',TRIM(varname),' ...'

    nsizes(3) = 1
    DO n = 1, nfile

      IF (debug == 11) WRITE(STDOUT,'(3x,3a,I5,4I5)') 'Reading ',TRIM(varname),' from file no. ',n, &
                     ips(n),ipe(n),jps(n),jpe(n)
      nsizes(1) = ipe(n)-ips(n)+1
      nsizes(2) = jpe(n)-jps(n)+1
      !
      ! get variable id
      !
      istatus = nf_inq_varid(hfiles(n),TRIM(varname),varid)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'get_wrf_da2d:nf_inq_varid:'//TRIM(varname))

      !
      ! Get more variable information
      !
      istatus = nf_inq_var(hfiles(n),varid,TRIM(varname),vartype,ndims,vardimids,varnatts)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'get_wrf_da2d:nf_inq_var')

      !
      ! Get dimensions
      !
      IF (ndims == 3) THEN
        DO vdim = 1, ndims
          istatus = nf_inq_dimlen(hfiles(n),vardimids(vdim),dimlen)
          CALL nf_handle_error(istatus,'get_wrf_da2d:nf_inq_dim')

          IF (dimlen /= nsizes(vdim)) THEN
            WRITE(STDOUT,'(1x,a,I0,a,I0,a,/,8x,a,I0,a)')                 &
                 'ERROR: Wrong size with dimension ',vdim,' (',dimlen,')', &
                 'Expected ',nsizes(vdim),'.'
            istatus = -1
            CALL nf_handle_error(istatus,'get_wrf_da2d:wrong dimension size')
          END IF
        END DO

        istatus = nf_get_var_real(hfiles(n),varid,vartmp)
        CALL nf_handle_error(istatus,'get_wrf_da2d:nf_get_var_real - '//TRIM(varname))

        DO j = jps(n),jpe(n)
          DO i = ips(n), ipe(n)
             kout = i - ips(n)+1 + (j-jps(n))*nsizes(1)
             !write(0,*) i,j,ips(n),ipe(n),jps(n),jpe(n),kout
             var2d(i,j) = vartmp(kout)
          END DO
        END DO
      ELSE
        WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported variable rank size ',ndims,'.'
        istatus = -1
        CALL nf_handle_error(istatus,'get_wrf_da2d:wrong rank')
      END IF

    END DO

  END SUBROUTINE get_wrf_da2d

  !#####################################################################

  SUBROUTINE get_wrf_da3d(hfiles, nfile,varname, var3d, nx, ny,nz,      &
                          ips,ipe,jps,jpe,debug,istatus)

  !---------------------------------------------------------------------
  ! Purpose: Read a 2D real field from WRF files
  !---------------------------------------------------------------------

    INTEGER,          INTENT(IN)  :: nx, ny, nz, nfile
    INTEGER,          INTENT(IN)  :: hfiles(nfile)
    CHARACTER(LEN=*), INTENT(IN)  :: varname
    REAL,             INTENT(OUT) :: var3d(nx,ny,nz)
    INTEGER,          INTENT(IN)  :: ips(nfile),ipe(nfile)
    INTEGER,          INTENT(IN)  :: jps(nfile),jpe(nfile)
    INTEGER,          INTENT(IN)  :: debug
    INTEGER,          INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: n, varid
    INTEGER :: vartype, ndims
    INTEGER :: vardimids(NF_MAX_DIMS)
    INTEGER :: varnatts, nsizes(4)
    INTEGER :: vdim, dimlen

    INTEGER :: i, j,k, kout

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    IF (debug > 39) WRITE(STDOUT,'(3x,3a)') 'Reading ',TRIM(varname),' ...'

    nsizes(4) = 1
    DO n = 1, nfile

      IF (debug == 11) WRITE(STDOUT,'(3x,3a,I5,4I5)') 'Reading ',TRIM(varname),' from file no. ',n, &
                     ips(n),ipe(n),jps(n),jpe(n)

      nsizes(1) = ipe(n)-ips(n)+1
      nsizes(2) = jpe(n)-jps(n)+1
      nsizes(3) = nz
      !
      ! get variable id
      !
      istatus = nf_inq_varid(hfiles(n),TRIM(varname),varid)
      IF (istatus /= NF_NOERR) THEN
        istatus = -1
        RETURN
      END IF
      !CALL nf_handle_error(istatus,'get_wrf_da3d:nf_inq_varid:'//TRIM(varname))

      !
      ! Get more variable information
      !
      istatus = nf_inq_var(hfiles(n),varid,TRIM(varname),vartype,ndims,vardimids,varnatts)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'get_wrf_da3d:nf_inq_var')

      !
      ! Get dimensions
      !
      IF (ndims == 4) THEN
        DO vdim = 1, ndims
          istatus = nf_inq_dimlen(hfiles(n),vardimids(vdim),dimlen)
          CALL nf_handle_error(istatus,'get_wrf_da3d:nf_inq_dim')

          IF (dimlen /= nsizes(vdim)) THEN
            WRITE(STDOUT,'(1x,a,I0,a,I0,a,/,8x,a,I0,a)')                 &
                 'ERROR: Wrong size with dimension ',vdim,' (',dimlen,')', &
                 'Expected ',nsizes(vdim),'.'
            istatus = -1
            CALL nf_handle_error(istatus,'get_wrf_da3d:wrong dimension size')
          END IF
        END DO

        istatus = nf_get_var_real(hfiles(n),varid,vartmp)
        CALL nf_handle_error(istatus,'get_wrf_da3d:nf_get_var_real - '//TRIM(varname))

        DO k = 1, nz
          DO j = jps(n),jpe(n)
            DO i = ips(n), ipe(n)
               kout = i-ips(n)+1 + (j-jps(n))*nsizes(1) + (k-1)*nsizes(1)*nsizes(2)
               var3d(i,j,k) = vartmp(kout)
            END DO
          END DO
        END DO
      ELSE
        WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported variable rank size ',ndims,'.'
        istatus = -1
        CALL nf_handle_error(istatus,'get_wrf_d3d:wrong rank')
      END IF

    END DO

  END SUBROUTINE get_wrf_da3d

  !#####################################################################

  SUBROUTINE put_wrf_da2d(hfiles, nfile,varname, var2d, nx, ny,         &
                          ips,ipe,jps,jpe,debug,istatus)

  !---------------------------------------------------------------------
  ! Purpose: Read a 2D real field from WRF files
  !---------------------------------------------------------------------

    INTEGER,          INTENT(IN)  :: nx, ny, nfile
    INTEGER,          INTENT(IN)  :: hfiles(nfile)
    CHARACTER(LEN=*), INTENT(IN)  :: varname
    REAL,             INTENT(IN)  :: var2d(nx,ny)
    INTEGER,          INTENT(IN)  :: ips(nfile),ipe(nfile)
    INTEGER,          INTENT(IN)  :: jps(nfile),jpe(nfile)
    INTEGER,          INTENT(IN)  :: debug
    INTEGER,          INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: n, varid
    INTEGER :: vartype, ndims
    INTEGER :: vardimids(NF_MAX_DIMS)
    INTEGER :: varnatts, nsizes(3)
    INTEGER :: vdim, dimlen

    INTEGER :: i, j, kout

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


    nsizes(3) = 1
    DO n = 1, nfile

      IF (debug > 10) WRITE(STDOUT,'(3x,3a,I5,4I5)') 'Writing ',TRIM(varname),' to file no. ',n, &
                     ips(n),ipe(n),jps(n),jpe(n)
      nsizes(1) = ipe(n)-ips(n)+1
      nsizes(2) = jpe(n)-jps(n)+1
      !
      ! get variable id
      !
      istatus = nf_inq_varid(hfiles(n),TRIM(varname),varid)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'put_wrf_da2d:nf_inq_varid:'//TRIM(varname))

      !
      ! Get more variable information
      !
      istatus = nf_inq_var(hfiles(n),varid,TRIM(varname),vartype,ndims,vardimids,varnatts)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'put_wrf_da2d:nf_inq_var')

      !
      ! Get dimensions
      !
      IF (ndims == 3) THEN
        DO vdim = 1, ndims
          istatus = nf_inq_dimlen(hfiles(n),vardimids(vdim),dimlen)
          CALL nf_handle_error(istatus,'get_wrf_da2d:nf_inq_dim')

          IF (dimlen /= nsizes(vdim)) THEN
            WRITE(STDOUT,'(1x,a,I0,a,I0,a,/,8x,a,I0,a)')                 &
                 'ERROR: Wrong size with dimension ',vdim,' (',dimlen,')', &
                 'Expected ',nsizes(vdim),'.'
            istatus = -1
            CALL nf_handle_error(istatus,'put_wrf_da2d:wrong dimension size')
          END IF
        END DO

        DO j = jps(n),jpe(n)
          DO i = ips(n), ipe(n)
             kout = i - ips(n)+1 + (j-jps(n))*nsizes(1)
             !write(0,*) i,j,ips(n),ipe(n),jps(n),jpe(n),kout
             vartmp(kout) = var2d(i,j)
          END DO
        END DO

        istatus = nf_put_var_real(hfiles(n),varid,vartmp)
        CALL nf_handle_error(istatus,'put_wrf_da2d:nf_get_var_real - '//TRIM(varname))

      ELSE
        WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported variable rank size ',ndims,'.'
        istatus = -1
        CALL nf_handle_error(istatus,'put_wrf_da2d:wrong rank')
      END IF

    END DO

  END SUBROUTINE put_wrf_da2d

  !#####################################################################

  SUBROUTINE put_wrf_da3d(hfiles, nfile,varname, var3d, nx, ny,nz,      &
                          ips,ipe,jps,jpe,debug,istatus)

  !---------------------------------------------------------------------
  ! Purpose: Read a 2D real field from WRF files
  !---------------------------------------------------------------------

    INTEGER,          INTENT(IN)  :: nx, ny, nz, nfile
    INTEGER,          INTENT(IN)  :: hfiles(nfile)
    CHARACTER(LEN=*), INTENT(IN)  :: varname
    REAL,             INTENT(IN)  :: var3d(nx,ny,nz)
    INTEGER,          INTENT(IN)  :: ips(nfile),ipe(nfile)
    INTEGER,          INTENT(IN)  :: jps(nfile),jpe(nfile)
    INTEGER,          INTENT(IN)  :: debug
    INTEGER,          INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: n, varid
    INTEGER :: vartype, ndims
    INTEGER :: vardimids(NF_MAX_DIMS)
    INTEGER :: varnatts, nsizes(4)
    INTEGER :: vdim, dimlen

    INTEGER :: i, j,k, kout

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


    nsizes(4) = 1
    DO n = 1, nfile

      nsizes(1) = ipe(n)-ips(n)+1
      nsizes(2) = jpe(n)-jps(n)+1
      nsizes(3) = nz
      !
      ! get variable id
      !
      istatus = nf_inq_varid(hfiles(n),TRIM(varname),varid)
      IF (istatus /= NF_NOERR) THEN
        istatus = -1
        RETURN
      END IF
      !CALL nf_handle_error(istatus,'get_wrf_da3d:nf_inq_varid:'//TRIM(varname))

      !
      ! Get more variable information
      !
      istatus = nf_inq_var(hfiles(n),varid,TRIM(varname),vartype,ndims,vardimids,varnatts)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'put_wrf_da3d:nf_inq_var')

      !
      ! Get dimensions
      !
      IF (ndims == 4) THEN
        DO vdim = 1, ndims
          istatus = nf_inq_dimlen(hfiles(n),vardimids(vdim),dimlen)
          CALL nf_handle_error(istatus,'put_wrf_da3d:nf_inq_dim')

          IF (dimlen /= nsizes(vdim)) THEN
            WRITE(STDOUT,'(1x,a,I0,a,I0,a,/,8x,a,I0,a)')                 &
                 'ERROR: Wrong size with dimension ',vdim,' (',dimlen,')', &
                 'Expected ',nsizes(vdim),'.'
            istatus = -1
            CALL nf_handle_error(istatus,'put_wrf_da3d:wrong dimension size')
          END IF
        END DO

        DO k = 1, nz
          DO j = jps(n),jpe(n)
            DO i = ips(n), ipe(n)
               kout = i-ips(n)+1 + (j-jps(n))*nsizes(1) + (k-1)*nsizes(1)*nsizes(2)
               vartmp(kout) = var3d(i,j,k)
            END DO
          END DO
        END DO

        istatus = nf_put_var_real(hfiles(n),varid,vartmp)
        CALL nf_handle_error(istatus,'put_wrf_da3d:nf_get_var_real - '//TRIM(varname))

      ELSE
        WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported variable rank size ',ndims,'.'
        istatus = -1
        CALL nf_handle_error(istatus,'put_wrf_d3d:wrong rank')
      END IF

    END DO

  END SUBROUTINE put_wrf_da3d

  !#####################################################################

  SUBROUTINE wrf_get_dims_bdy(hfile, varname, dims, ndims, debug,istatus)

    !-------------------------------------------------------------------
    ! Purpose: Get a field dimensions
    !-------------------------------------------------------------------

    IMPLICIT NONE

    INTEGER,           intent(in)  :: hfile
    character(len=*),  intent(in)  :: varname
    INTEGER,           intent(in)  :: debug
    integer,           intent(out) :: dims(4)
    integer,           intent(out) :: ndims
    INTEGER,           INTENT(OUT) :: istatus

  !---------------------------------------------------------------------
    INTEGER :: varid, vartype, varnatts
    INTEGER :: vardimids(NF_MAX_DIMS)

    integer :: vdim

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !
    ! get variable id
    !
    istatus = nf_inq_varid(hfile,TRIM(varname),varid)
    CALL nf_handle_error(istatus,'wrf_get_dims_bdy:nf_inq_varid:'//TRIM(varname))

    !
    ! Get more variable information
    !
    istatus = nf_inq_var(hfile,varid,TRIM(varname),vartype,ndims,vardimids,varnatts)
    IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'wrf_get_dims_bdy:nf_inq_var')

    !
    ! Get dimensions
    !
    IF (ndims /=3 .AND. ndims /= 4) THEN
      WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported variable rank size ',ndims,'.'
      istatus = -1
      CALL nf_handle_error(istatus,'wrf_get_dims_bdy:wrong rank')
    ELSE

      dims(:) = 1
      DO vdim = 1, ndims
        istatus = nf_inq_dimlen(hfile,vardimids(vdim),dims(vdim))
        CALL nf_handle_error(istatus,'wrf_get_dims_bdy:nf_inq_dim')
      END DO

    END IF

  END SUBROUTINE wrf_get_dims_bdy

  !#####################################################################

  SUBROUTINE wrf_get_bdy2d(hfile, varname, var2d, nx, ny,               &
                          itime,debug,istatus)

  !---------------------------------------------------------------------
  ! Purpose: Read a 2D real field from WRF files
  !---------------------------------------------------------------------

    INTEGER,          INTENT(IN)  :: nx, ny
    INTEGER,          INTENT(IN)  :: hfile
    CHARACTER(LEN=*), INTENT(IN)  :: varname
    REAL,             INTENT(OUT) :: var2d(nx,ny)
    INTEGER,          INTENT(IN)  :: itime
    INTEGER,          INTENT(IN)  :: debug
    INTEGER,          INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: n, varid
    INTEGER :: vartype, ndims
    INTEGER :: vardimids(NF_MAX_DIMS)
    INTEGER :: varnatts, nsizes(3)
    INTEGER :: vdim, dimlen

    INTEGER :: i, j,k, kout

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0
    nsizes(1) = nx
    nsizes(2) = ny
    nsizes(3) = 1

    IF (debug > 9) WRITE(STDOUT,'(3x,3a,I0)') 'Reading ',TRIM(varname),' at time  ',itime

    !
    ! get variable id
    !
    istatus = nf_inq_varid(hfile,TRIM(varname),varid)
    CALL nf_handle_error(istatus,'wrf_get_bdy2d:nf_inq_varid:'//TRIM(varname))

    !
    ! Get more variable information
    !
    istatus = nf_inq_var(hfile,varid,TRIM(varname),vartype,ndims,vardimids,varnatts)
    CALL nf_handle_error(istatus,'wrf_get_bdy2d:nf_inq_var')

    !
    ! Get dimensions
    !
    IF (ndims == 3) THEN
      DO vdim = 1, ndims-1
        istatus = nf_inq_dimlen(hfile,vardimids(vdim),dimlen)
        CALL nf_handle_error(istatus,'wrf_get_bdy2d:nf_inq_dim')

        IF (dimlen /= nsizes(vdim)) THEN
          WRITE(STDOUT,'(1x,a,I0,a,I0,a,/,8x,a,I0,a)')                  &
               'ERROR: Wrong size with dimension ',vdim,' (',dimlen,')',&
               'Expected ',nsizes(vdim),'.'
          istatus = -1
          CALL nf_handle_error(istatus,'wrf_get_bdy2d:wrong dimension size')
        END IF
      END DO

      istatus = NF_GET_VARA_REAL(hfile,varid,(/1,1,itime/),nsizes,var2d)
      CALL nf_handle_error(istatus,'wrf_get_bdy3d:nf_get_vara_real - '//TRIM(varname))

    ELSE
      WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported variable rank size ',ndims,'.'
      istatus = -1
      CALL nf_handle_error(istatus,'wrf_get_bdy2d:wrong rank')
    END IF

  END SUBROUTINE wrf_get_bdy2d

  !#####################################################################

  SUBROUTINE wrf_get_bdy3d(hfile, varname, var3d, nx, ny,nz,            &
                          itime,debug,istatus)

  !---------------------------------------------------------------------
  ! Purpose: Read a 2D real field from WRF files
  !---------------------------------------------------------------------

    INTEGER,          INTENT(IN)  :: nx, ny, nz
    INTEGER,          INTENT(IN)  :: hfile
    CHARACTER(LEN=*), INTENT(IN)  :: varname
    REAL,             INTENT(OUT) :: var3d(nx,ny,nz)
    INTEGER,          INTENT(IN)  :: itime
    INTEGER,          INTENT(IN)  :: debug
    INTEGER,          INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: n, varid
    INTEGER :: vartype, ndims
    INTEGER :: vardimids(NF_MAX_DIMS)
    INTEGER :: varnatts, nsizes(4)
    INTEGER :: vdim, dimlen

    INTEGER :: i, j,k, kout

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (debug > 9) WRITE(STDOUT,'(3x,3a,I0)') 'Reading ',TRIM(varname),' at time  ',itime

    nsizes(1) = nx
    nsizes(2) = ny
    nsizes(3) = nz
    nsizes(4) = 1
    !
    ! get variable id
    !
    istatus = nf_inq_varid(hfile,TRIM(varname),varid)
    CALL nf_handle_error(istatus,'wrf_get_bdy3d:nf_inq_varid:'//TRIM(varname))

    !
    ! Get more variable information
    !
    istatus = nf_inq_var(hfile,varid,TRIM(varname),vartype,ndims,vardimids,varnatts)
    CALL nf_handle_error(istatus,'wrf_get_bdy3d:nf_inq_var')

    !
    ! Get dimensions
    !
    IF (ndims == 4) THEN
      DO vdim = 1, ndims-1
        istatus = nf_inq_dimlen(hfile,vardimids(vdim),dimlen)
        CALL nf_handle_error(istatus,'wrf_get_bdy3d:nf_inq_dim')

        IF (dimlen /= nsizes(vdim)) THEN
          WRITE(STDOUT,'(1x,a,I0,a,I0,a,/,8x,a,I0,a)')                  &
               'ERROR: Wrong size with dimension ',vdim,' (',dimlen,')',&
               'Expected ',nsizes(vdim),'.'
          istatus = -1
          CALL nf_handle_error(istatus,'wrf_get_bdy3d:wrong dimension size')
        END IF
      END DO

      istatus = NF_GET_VARA_REAL(hfile,varid,(/1,1,1,itime/),nsizes,var3d)
      CALL nf_handle_error(istatus,'wrf_get_bdy3d:nf_get_vara_real - '//TRIM(varname))

    ELSE
      WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported variable rank size ',ndims,'.'
      istatus = -1
      CALL nf_handle_error(istatus,'wrf_get_bdy3d:wrong rank')
    END IF

  END SUBROUTINE wrf_get_bdy3d

  !#####################################################################

  SUBROUTINE wrf_put_bdy2d(hfile, varname, var2d, nx, ny, itime, debug, istatus)

  !-----------------------------------------------------------------------
  ! Purpose: TBD
  !-----------------------------------------------------------------------

    IMPLICIT NONE

    INTEGER,            INTENT(IN)  :: hfile
    CHARACTER (len=*),  intent(in)  :: varname
    INTEGER,            intent(in)  :: nx, ny, itime
    REAL,               intent(in)  :: var2d(nx,ny)
    INTEGER,            intent(in)  :: debug
    INTEGER,            INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    integer :: varid, vartype, ndims
    INTEGER :: vardimids(NF_MAX_DIMS)
    INTEGER :: varnatts, nsizes(3)
    INTEGER :: vdim, dimlen


  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (debug > 9) WRITE(STDOUT,'(3x,a,a20,a,I0)')                      &
                   'Writing ',TRIM(varname),' at time  ',itime

    nsizes(1) = nx
    nsizes(2) = ny
    nsizes(3) = 1

    !
    ! get variable id
    !
    istatus = nf_inq_varid(hfile,TRIM(varname),varid)
    CALL nf_handle_error(istatus,'wrf_put_bdy2d:nf_inq_varid:'//TRIM(varname))

    !
    ! Get more variable information
    !
    istatus = nf_inq_var(hfile,varid,TRIM(varname),vartype,ndims,vardimids,varnatts)
    CALL nf_handle_error(istatus,'wrf_put_bdy2d:nf_inq_var')

    !
    ! Get dimensions
    !
    IF (ndims == 3) THEN
      DO vdim = 1, ndims-1
        istatus = NF_INQ_DIMLEN(hfile,vardimids(vdim),dimlen)
        CALL nf_handle_error(istatus,'wrf_put_bdy2d:nf_inq_dim')

        IF (dimlen /= nsizes(vdim)) THEN
          WRITE(STDOUT,'(1x,a,I0,a,I0,a,/,8x,a,I0,a)')                  &
               'ERROR: Wrong size with dimension ',vdim,' (',dimlen,')',&
               'Expected ',nsizes(vdim),'.'
          istatus = -1
          CALL nf_handle_error(istatus,'wrf_put_bdy2d:wrong dimension size')
        END IF
      END DO

      istatus = NF_PUT_VARA_REAL(hfile,varid,(/1,1,itime/),nsizes,var2d)
      CALL nf_handle_error(istatus,'wrf_put_bdy2d:nf_put_vara_real - '//TRIM(varname))

    ELSE
      WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported variable rank size ',ndims,'.'
      istatus = -1
      CALL nf_handle_error(istatus,'wrf_put_bdy2d:wrong rank')
    END IF

  END SUBROUTINE wrf_put_bdy2d

  !#####################################################################

  SUBROUTINE wrf_put_bdy3d(hfile, varname, var3d, nx, ny, nz, itime, debug, istatus)

  !-----------------------------------------------------------------------
  ! Purpose: TBD
  !-----------------------------------------------------------------------

    implicit none

    INTEGER,            INTENT(IN)  :: hfile
    CHARACTER (len=*),  intent(in)  :: varname
    integer,            intent(in)  :: nx, ny, nz, itime
    real,               intent(in)  :: var3d(nx,ny,nz)
    INTEGER,            intent(in)  :: debug
    INTEGER,            INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    integer :: varid, vartype, ndims
    INTEGER :: vardimids(NF_MAX_DIMS)
    INTEGER :: varnatts, nsizes(4)
    INTEGER :: vdim, dimlen


  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (debug > 9) WRITE(STDOUT,'(3x,a,a20,a,I0)')                      &
                   'Writing ',TRIM(varname),' at time  ',itime

    nsizes(1) = nx
    nsizes(2) = ny
    nsizes(3) = nz
    nsizes(4) = 1

    !
    ! get variable id
    !
    istatus = nf_inq_varid(hfile,TRIM(varname),varid)
    CALL nf_handle_error(istatus,'wrf_put_bdy3d:nf_inq_varid:'//TRIM(varname))

    !
    ! Get more variable information
    !
    istatus = nf_inq_var(hfile,varid,TRIM(varname),vartype,ndims,vardimids,varnatts)
    CALL nf_handle_error(istatus,'wrf_put_bdy3d:nf_inq_var')

    !
    ! Get dimensions
    !
    IF (ndims == 4) THEN
      DO vdim = 1, ndims-1
        istatus = NF_INQ_DIMLEN(hfile,vardimids(vdim),dimlen)
        CALL nf_handle_error(istatus,'wrf_put_bdy2d:nf_inq_dim')

        IF (dimlen /= nsizes(vdim)) THEN
          WRITE(STDOUT,'(1x,a,I0,a,I0,a,/,8x,a,I0,a)')                  &
               'ERROR: Wrong size with dimension ',vdim,' (',dimlen,')',&
               'Expected ',nsizes(vdim),'.'
          istatus = -1
          CALL nf_handle_error(istatus,'wrf_put_bdy3d:wrong dimension size')
        END IF
      END DO

      istatus = NF_PUT_VARA_REAL(hfile,varid,(/1,1,1,itime/),nsizes,var3d)
      CALL nf_handle_error(istatus,'wrf_put_bdy3d:nf_put_vara_real - '//TRIM(varname))

    ELSE
      WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported variable rank size ',ndims,'.'
      istatus = -1
      CALL nf_handle_error(istatus,'wrf_put_bdy3d:wrong rank')
    END IF

  END SUBROUTINE wrf_put_bdy3d

  !#####################################################################

  SUBROUTINE wrf_copy_bdy(hfilein, itime1, varname, hfileout, itime2, debug, istatus)

  !-----------------------------------------------------------------------
  ! Purpose: TBD
  !-----------------------------------------------------------------------

    implicit none

    INTEGER,            INTENT(in)  :: hfilein, hfileout
    INTEGER,            intent(in)  :: itime1, itime2
    CHARACTER(LEN=*),   intent(in)  :: varname
    INTEGER,            intent(in)  :: debug
    INTEGER,            INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    integer :: varid, vartype, ndims
    INTEGER :: vardimids(NF_MAX_DIMS)
    INTEGER :: varnatts, nsizes(4), nstart(4)
    INTEGER :: vdim, dimlen


  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !
    ! get variable id
    !
    istatus = nf_inq_varid(hfilein,TRIM(varname),varid)
    !IF (istatus /= NF_NOERR) THEN
    !  istatus = -1
    !  RETURN
    !END IF
    CALL nf_handle_error(istatus,'wrf_copy_bdy:nf_inq_varid:'//TRIM(varname))

    !
    ! Get more variable information
    !
    istatus = nf_inq_var(hfilein,varid,TRIM(varname),vartype,ndims,vardimids,varnatts)
    CALL nf_handle_error(istatus,'wrf_put_bdy:nf_inq_var')

    !
    ! Get dimensions
    !
    IF (ndims == 4 .OR. ndims == 3) THEN
      nsizes(:) = 1
      nstart(:) = 1
      DO vdim = 1, ndims-1
        istatus = NF_INQ_DIMLEN(hfilein,vardimids(vdim),dimlen)
        CALL nf_handle_error(istatus,'wrf_put_bdy:nf_inq_dim')

        nsizes(vdim) = dimlen
      END DO

      nstart(ndims) = itime1

      istatus = NF_GET_VARA_REAL(hfilein,varid,nstart,nsizes,vartmp)
      CALL nf_handle_error(istatus,'wrf_copy_bdy:nf_get_vara_real - '//TRIM(varname))

      nstart(ndims) = itime2

      istatus = NF_PUT_VARA_REAL(hfileout,varid,nstart,nsizes,vartmp)
      CALL nf_handle_error(istatus,'wrf_copy_bdy:nf_put_vara_real - '//TRIM(varname))

    ELSE
      WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported variable rank size ',ndims,'.'
      istatus = -1
      CALL nf_handle_error(istatus,'wrf_copy_bdy:wrong rank')
    END IF

  END SUBROUTINE wrf_copy_bdy

  !#####################################################################

  SUBROUTINE wrf_copy_times(hfilein, itime1, hfileout, itime2, lvldbg, istatus)

  !-----------------------------------------------------------------------
  ! Purpose: TBD
  !-----------------------------------------------------------------------

    implicit none

    INTEGER, INTENT(IN)  :: hfilein, hfileout
    INTEGER, intent(in)  :: itime1, itime2
    INTEGER, intent(in)  :: lvldbg
    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    integer :: varid
    INTEGER :: dimids(NF_MAX_DIMS)
    INTEGER :: dimlens(NF_MAX_DIMS)

    CHARACTER(LEN=256) :: varname
    CHARACTER(LEN=19)  :: time_str

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !
    ! Times
    !
    varname = 'Times'
    istatus = nf_inq_varid(hfilein,TRIM(varname),varid)
    CALL nf_handle_error(istatus,'wrf_copy_times:nf_inq_varid:'//TRIM(varname))

    istatus = NF_INQ_VARDIMID(hfilein,varid,dimids)
    CALL nf_handle_error(istatus,'wrf_copy_times:nf_inq_vardimid')

    istatus = NF_INQ_DIMLEN(hfilein,dimids(1),dimlens(1))
    istatus = NF_INQ_DIMLEN(hfilein,dimids(2),dimlens(2))   ! unlimit dimension

    IF( dimlens(1) /= 19) THEN
      WRITE(STDOUT,*) 'Length of Times does not match with the dimension size'
      istatus = -1
      CALL nf_handle_error(istatus,'wrf_copy_times:wrong dimension size')
    END IF

    istatus = NF_GET_VARA_TEXT(hfilein,varid,(/1,itime1/),(/dimlens(1),1/),time_str)
    CALL nf_handle_error(istatus,'NF_GET_VARA_TEXT in wrf_copy_times')

    istatus = NF_PUT_VARA_TEXT(hfileout,varid,(/1,itime2/),(/dimlens(1),1/),time_str)
    CALL nf_handle_error(istatus,'wrf_copy_times:NF_PUT_VARA_TEXT:'//TRIM(varname))

    !
    ! thisbdytime
    !
    varname = 'md___thisbdytimee_x_t_d_o_m_a_i_n_m_e_t_a_data_'
    istatus = nf_inq_varid(hfilein,TRIM(varname),varid)
    CALL nf_handle_error(istatus,'wrf_copy_times:nf_inq_varid:'//TRIM(varname))

    istatus = NF_INQ_VARDIMID(hfilein,varid,dimids)
    CALL nf_handle_error(istatus,'wrf_copy_times:nf_inq_vardimid')

    istatus = NF_INQ_DIMLEN(hfilein,dimids(1),dimlens(1))
    istatus = NF_INQ_DIMLEN(hfilein,dimids(2),dimlens(2))   ! unlimit dimension

    IF( dimlens(1) /= 19) THEN
      WRITE(STDOUT,*) 'Length of Times does not match with the dimension size'
      istatus = -1
      CALL nf_handle_error(istatus,'wrf_copy_times:wrong dimension size')
    END IF

    istatus = NF_GET_VARA_TEXT(hfilein,varid,(/1,itime1/),(/dimlens(1),1/),time_str)
    CALL nf_handle_error(istatus,'NF_GET_VARA_TEXT in wrf_copy_times')

    istatus = NF_PUT_VARA_TEXT(hfileout,varid,(/1,itime2/),(/dimlens(1),1/),time_str)
    CALL nf_handle_error(istatus,'wrf_copy_times:NF_PUT_VARA_TEXT:'//TRIM(varname))


    !
    ! nextbdytime
    !
    varname = 'md___nextbdytimee_x_t_d_o_m_a_i_n_m_e_t_a_data_'
    istatus = nf_inq_varid(hfilein,TRIM(varname),varid)
    CALL nf_handle_error(istatus,'wrf_copy_times:nf_inq_varid:'//TRIM(varname))

    istatus = NF_INQ_VARDIMID(hfilein,varid,dimids)
    CALL nf_handle_error(istatus,'wrf_copy_times:nf_inq_vardimid')

    istatus = NF_INQ_DIMLEN(hfilein,dimids(1),dimlens(1))
    istatus = NF_INQ_DIMLEN(hfilein,dimids(2),dimlens(2))   ! unlimit dimension

    IF( dimlens(1) /= 19) THEN
      WRITE(STDOUT,*) 'Length of Times does not match with the dimension size'
      istatus = -1
      CALL nf_handle_error(istatus,'wrf_copy_times:wrong dimension size')
    END IF

    istatus = NF_GET_VARA_TEXT(hfilein,varid,(/1,itime1/),(/dimlens(1),1/),time_str)
    CALL nf_handle_error(istatus,'NF_GET_VARA_TEXT in wrf_copy_times')

    istatus = NF_PUT_VARA_TEXT(hfileout,varid,(/1,itime2/),(/dimlens(1),1/),time_str)
    CALL nf_handle_error(istatus,'wrf_copy_times:NF_PUT_VARA_TEXT:'//TRIM(varname))


  END SUBROUTINE wrf_copy_times

  !#####################################################################

  SUBROUTINE wrf_fix_times(hfile, init_time, next_time, lvldbg, istatus)

  !-----------------------------------------------------------------------
  ! Purpose: Fix WRF boundary file Time related variables
  !-----------------------------------------------------------------------

    implicit none

    INTEGER,            INTENT(IN)  :: hfile
    CHARACTER(len=19),  intent(in)  :: init_time, next_time
    INTEGER,            intent(in)  :: lvldbg
    INTEGER,            INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    integer :: varid, vartype, ndims
    INTEGER :: dimids(NF_MAX_DIMS)
    INTEGER :: varnatts
    INTEGER :: dimlens(NF_MAX_DIMS)


  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !
    ! Times
    !
    istatus = NF_INQ_VARID(hfile,'Times',varid)
    CALL nf_handle_error(istatus,'NF_INQ_VARID in wrf_fix_times.')

    istatus = NF_INQ_VARDIMID(hfile,varid,dimids)
    CALL nf_handle_error(istatus,'wrf_fix_times:nf_inq_vardimid')

    istatus = NF_INQ_DIMLEN(hfile,dimids(1),dimlens(1))
    istatus = NF_INQ_DIMLEN(hfile,dimids(2),dimlens(2))   ! unlimit dimension

    IF( dimlens(1) /= 19) THEN
      WRITE(STDOUT,*) 'Length of Times does not match with the dimension size'
      istatus = -1
      CALL nf_handle_error(istatus,'wrf_fix_times:wrong dimension size')
    END IF

    istatus = NF_PUT_VARA_TEXT(hfile,varid,(/1,1/),(/dimlens(1),1/),init_time)
    CALL nf_handle_error(istatus,'wrf_fix_times:NF_PUT_VARA_TEXT:Times')

    !
    ! thisbdytime
    !
    istatus = NF_INQ_VARID(hfile,'md___thisbdytimee_x_t_d_o_m_a_i_n_m_e_t_a_data_',varid)
    CALL nf_handle_error(istatus,'NF_INQ_VARID in wrf_fix_times.')

    istatus = NF_INQ_VARDIMID(hfile,varid,dimids)
    CALL nf_handle_error(istatus,'wrf_fix_times:nf_inq_vardimid')

    istatus = NF_INQ_DIMLEN(hfile,dimids(1),dimlens(1))
    istatus = NF_INQ_DIMLEN(hfile,dimids(2),dimlens(2))   ! unlimit dimension

    IF( dimlens(1) /= 19) THEN
      WRITE(STDOUT,*) 'Length of thisbdytime does not match with the dimension size'
      istatus = -1
      CALL nf_handle_error(istatus,'wrf_fix_times:wrong dimension size')
    END IF

    istatus = NF_PUT_VARA_TEXT(hfile,varid,(/1,1/),(/dimlens(1),1/),init_time)
    CALL nf_handle_error(istatus,'wrf_fix_times:NF_PUT_VARA_TEXT:thisbdytime')


    !
    ! nextbdytime
    !
    istatus = NF_INQ_VARID(hfile,'md___nextbdytimee_x_t_d_o_m_a_i_n_m_e_t_a_data_',varid)
    CALL nf_handle_error(istatus,'NF_INQ_VARID in wrf_fix_times.')

    istatus = NF_INQ_VARDIMID(hfile,varid,dimids)
    CALL nf_handle_error(istatus,'wrf_fix_times:nf_inq_vardimid')

    istatus = NF_INQ_DIMLEN(hfile,dimids(1),dimlens(1))
    istatus = NF_INQ_DIMLEN(hfile,dimids(2),dimlens(2))   ! unlimit dimension

    IF( dimlens(1) /= 19) THEN
      WRITE(STDOUT,*) 'Length of thisbdytime does not match with the dimension size'
      istatus = -1
      CALL nf_handle_error(istatus,'wrf_fix_times:wrong dimension size')
    END IF

    istatus = NF_PUT_VARA_TEXT(hfile,varid,(/1,1/),(/dimlens(1),1/),next_time)
    CALL nf_handle_error(istatus,'wrf_fix_times:NF_PUT_VARA_TEXT:thisbdytime')

  END SUBROUTINE wrf_fix_times

  !#####################################################################

  SUBROUTINE fix_wrf_title(hfiles, nfiles, title, init_time, lvldbg, istatus)

  !-----------------------------------------------------------------------
  ! Purpose: TBD
  !-----------------------------------------------------------------------

    IMPLICIT NONE

    INTEGER,            INTENT(IN)  :: nfiles
    INTEGER,            INTENT(IN)  :: hfiles(nfiles)
    CHARACTER(len=*),   intent(in)  :: title, init_time
    INTEGER,            intent(in)  :: lvldbg
    INTEGER,            INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    CHARACTER(LEN=20) :: attname
    integer :: lenstr
    INTEGER :: hfile, n

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    DO n = 1, nfiles
      hfile = hfiles(n)

      istatus = NF_REDEF(hfile)
      CALL nf_handle_error(istatus,'wrf_fix_title')

      attname = 'TITLE'
      lenstr = LEN_TRIM(title)
      istatus = NF_PUT_ATT_TEXT(hfile,NF_GLOBAL,TRIM(attname),lenstr,TRIM(title))
      CALL nf_handle_error(istatus,'wrf_fix_title:'//TRIM(attname))

      attname = 'START_DATE'
      lenstr = LEN_TRIM(init_time)
      istatus = NF_PUT_ATT_TEXT(hfile,NF_GLOBAL,TRIM(attname),lenstr,TRIM(init_time))
      CALL nf_handle_error(istatus,'wrf_fix_title:'//TRIM(attname))

      istatus = NF_ENDDEF(hfile)
      CALL nf_handle_error(istatus,'wrf_fix_title')
    END DO

  END SUBROUTINE fix_wrf_title

  !#####################################################################

  SUBROUTINE fix_wrf_times(hfiles, nfiles, init_time, lvldbg, istatus)

  !-----------------------------------------------------------------------
  ! Purpose: Fix WRF output file Times variable for (wrfinput, wrfout)
  !-----------------------------------------------------------------------

    implicit none

    INTEGER,            INTENT(IN)  :: nfiles, hfiles(nfiles)
    CHARACTER(len=19),  intent(in)  :: init_time
    INTEGER,            intent(in)  :: lvldbg
    INTEGER,            INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    integer :: varid, vartype, ndims
    INTEGER :: dimids(NF_MAX_DIMS)
    INTEGER :: varnatts
    INTEGER :: dimlens(NF_MAX_DIMS)

    INTEGER :: n, hfile

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    !
    ! Times
    !
    DO n = 1, nfiles

      hfile = hfiles(n)

      istatus = NF_INQ_VARID(hfile,'Times',varid)
      CALL nf_handle_error(istatus,'NF_INQ_VARID in wrf_fix_times.')

      istatus = NF_INQ_VARDIMID(hfile,varid,dimids)
      CALL nf_handle_error(istatus,'wrf_fix_times:nf_inq_vardimid')

      istatus = NF_INQ_DIMLEN(hfile,dimids(1),dimlens(1))
      istatus = NF_INQ_DIMLEN(hfile,dimids(2),dimlens(2))   ! unlimit dimension

      IF( dimlens(1) /= 19) THEN
        WRITE(STDOUT,*) 'Length of Times does not match with the dimension size'
        istatus = -1
        CALL nf_handle_error(istatus,'wrf_fix_times:wrong dimension size')
      END IF

      istatus = NF_PUT_VARA_TEXT(hfile,varid,(/1,1/),(/dimlens(1),1/),init_time)
      CALL nf_handle_error(istatus,'wrf_fix_times:NF_PUT_VARA_TEXT:Times')
    END DO

    RETURN
  END SUBROUTINE fix_wrf_times

  SUBROUTINE copy_wrf_file( nfilein,infiles,nfileout,outfiles,          &
                            fortran_mode,npoutx,npouty,                 &
                            nexcl, excludesets,                         &
                            nvarout,outvarsets,outvarstagger,outvardim, &
                            debug,istatus )

    IMPLICIT NONE

    INTEGER,          INTENT(IN)  :: nfilein, nfileout
    CHARACTER(LEN=*), INTENT(IN)  :: infiles(nfilein),outfiles(nfileout)
    LOGICAL,          INTENT(IN)  :: fortran_mode
    INTEGER,          INTENT(IN)  :: npoutx, npouty

    INTEGER,          INTENT(IN)  :: nexcl
    CHARACTER(LEN=20),INTENT(IN)  :: excludesets(nexcl)
    INTEGER,          INTENT(OUT) :: nvarout
    CHARACTER(LEN=20),INTENT(OUT) :: outvarsets(100)
    CHARACTER(LEN=1), INTENT(OUT) :: outvarstagger(100)
    INTEGER,          INTENT(OUT) :: outvardim(4,100)
    INTEGER,          INTENT(IN)  :: debug
    INTEGER,          INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    CHARACTER(LEN=256) :: cmdstr
    INTEGER            :: lenstr

    LOGICAL            :: fexist, alert
    INTEGER            :: n


  !---------------------------------------------------------------------

    INTEGER :: nout , iloc, jloc
    INTEGER :: fHndl_in, fHndl_out
    INTEGER :: informat, outformat
    LOGICAL :: fixdim
    CHARACTER(LEN=20) :: nothing(1)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    nvarout           = 0
    outvarsets(:)     = ' '
    outvarstagger(:)  = ' '
    outvardim(:,:)    = 0

    alert   = .FALSE.
    DO n = 1, nfileout

      !
      ! Check outfilename
      !
      INQUIRE(FILE=TRIM(outfiles(n)),EXIST=fexist)
      IF ( fexist) THEN
        IF (.NOT. alert) THEN
          WRITE(STDOUT,'(1x,3a)') '=== Update existing file ',TRIM(outfiles(n)),' ==='
          alert = .TRUE.
        END IF
        CYCLE
      ELSE

        IF (nfilein /= nfileout .OR. fortran_mode) THEN

          IF (nfilein == nfileout) THEN
            fixdim = .FALSE.
            nout   = n
          ELSE
            fixdim = .TRUE.
            nout   = 1
          END IF

          iloc = MOD(n-1,npoutx)+1
          jloc = (n-1)/npoutx+1
          !
          ! Define output file
          !
          IF (debug > 0) WRITE(*,'(1x,a)') 'Opening '//TRIM(infiles(nout))//' for read.'

          istatus = NF_OPEN(TRIM(infiles(nout)),NF_NOWRITE,fHndl_in)
          CALL nf_handle_error(istatus,'NF_OPEN in copy_wrf_file')

          istatus = NF_INQ_FORMAT(fHndl_in,informat)
          CALL nf_handle_error(istatus,'NF_INQ_FORMAT in create_bdyout_schema')
          SELECT CASE (informat)
            CASE (NF_FORMAT_CLASSIC)
              outformat = NF_CLOBBER
            CASE (NF_FORMAT_64BIT)
              outformat = NF_64BIT_OFFSET
            CASE (NF_FORMAT_NETCDF4)
              outformat = NF_NETCDF4
            CASE (NF_FORMAT_NETCDF4_CLASSIC)
              outformat = NF_CLASSIC_MODEL
            CASE DEFAULT
              WRITE(STDOUT,'(1x,a,I0,3a)')                                &
                'ERROR: Unsupported input netCDF file format: ',informat, &
                ' in ',TRIM(infiles(nout)),'.'
              istatus = -1
              RETURN
          END SELECT

          IF (debug > 0) WRITE(*,'(1x,a)') 'Creating '//TRIM(outfiles(n))//'.'

          !istatus = NF_CREATE(TRIM(outfiles(n)),NF_CLOBBER,fHndl_out)
          istatus = NF_CREATE(TRIM(outfiles(n)),outformat,fHndl_out)
          CALL nf_handle_error(istatus,'NF_CREATE in copy_wrf_file')

          CALL copy_netcdf_definition(fHndl_in,fHndl_out,               &
                          fixdim,npoutx, npouty,iloc,jloc,              &
                          0, nothing,debug,istatus)

          IF (istatus /= 0) CALL nf_handle_error(istatus,'copy_netcdf_definition')

          CALL copy_netcdf_vars ( fHndl_in, fHndl_out, fixdim,          &
                                  nexcl, excludesets,                   &
                                  nvarout,outvarsets, outvarstagger, outvardim,    &
                                  debug,istatus )

          istatus = NF_CLOSE(fHndl_in)
          CALL nf_handle_error(istatus,'copy_wrf_file:nf_close')

          istatus = NF_CLOSE(fHndl_out)
          CALL nf_handle_error(istatus,'copy_wrf_file:nf_close')

        ELSE                    ! command-line copy

          INQUIRE(FILE=TRIM(infiles(n)),EXIST=fexist)
          IF (.NOT. fexist) THEN
            WRITE(6,'(1x,3a)') 'WARNING: inputfile file - ',            &
                      TRIM(infiles(n)),' does not exist.'
            istatus = -1
            RETURN
          END IF

          !
          ! copy file using linux command
          !
          WRITE(cmdstr,'(4a)')  'cp ',TRIM(infiles(n)),' ',TRIM(outfiles(n))

          IF (.NOT. alert) THEN
            WRITE(STDOUT,'(1x,a)') '=== ********************************* ==='
            WRITE(STDOUT,'(1x,a,/,5x,2a)')                                  &
              '    Trying to run command str: ','  $> ',TRIM(cmdstr)
            WRITE(STDOUT,'(1x,a,/,5x,a)')                                   &
              '    If Fortran "system" function is not implemented on the system,',  &
                  'Users can run this command from command line beforehand.'
            WRITE(STDOUT,'(1x,a)') '=== ********************************* ==='
            alert = .TRUE.
          END IF

          CALL unixcmd( TRIM(cmdstr) )
        END IF

      END IF
    END DO

    RETURN
  END SUBROUTINE copy_wrf_file

  !
  !##################################################################
  !##################################################################
  !######                                                      ######
  !######          SUBROUTINE copy_netcdf_definition           ######
  !######                                                      ######
  !######                     Developed by                     ######
  !######     Center for Analysis and Prediction of Storms     ######
  !######                University of Oklahoma                ######
  !######                                                      ######
  !##################################################################
  !##################################################################
  !

  SUBROUTINE copy_netcdf_definition(fin,fout,fixdim,npoutx, npouty,     &
                          iloc,jloc, noutvar, out_names,debug,istatus)

  !---------------------------------------------------------------------
  !
  ! PURPOSE:
  !
  !   Copy a netCDF definition to a new file. This procedure can reduce
  !   output datasets, can also change output patch configuration.
  !
  ! NOTE: each file patch must be called separately
  !
  !---------------------------------------------------------------------
  !
  ! AUTHOR: Y. Wang (03/06/2014)
  !
  !---------------------------------------------------------------------
    INTEGER,           INTENT(IN)  :: fin, fout

    LOGICAL,           INTENT(IN)  :: fixdim
    INTEGER,           INTENT(IN)  :: iloc, jloc, npoutx, npouty

    INTEGER,           INTENT(IN)  :: noutvar
    CHARACTER(LEN=20), INTENT(IN)  :: out_names(noutvar)

    INTEGER,           INTENT(IN)  :: debug
    INTEGER,           INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: dimid, ndims, dimlen, unlimdimid, odimid

    INTEGER :: attnum, ngatts

    INTEGER :: varid, nvars, ovarid, vartype, varndims, varnatts
    INTEGER :: vardimids(NF_MAX_DIMS)
    INTEGER :: varshuffle, vardeflate, vardeflate_level

    CHARACTER(LEN=60) :: dimname, attname, varname

  !---------------------------------------------------------------------

    INTEGER :: nxd, nyd, nxs, nys, nxf, nyf
    INTEGER :: iss, ise, ips, ipe, jss, jse, jps, jpe

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (fixdim) THEN          ! Fix dimensions with different output patch number
      istatus = NF_GET_ATT_INT(fin,NF_GLOBAL,'WEST-EAST_GRID_DIMENSION',  nxd)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:NF_GET_ATT_INT')

      istatus = NF_GET_ATT_INT(fin,NF_GLOBAL,'SOUTH-NORTH_GRID_DIMENSION',nyd)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:NF_GET_ATT_INT')

      IF (MOD(nxd-1,npoutx) /= 0 .OR. MOD(nyd-1,npouty) /= 0) THEN
        istatus = -1
        WRITE(*,'(1x,a,/,2(8x,2(a,I8),/))')                             &
            'ERROR: Dimension size is not divisible by patch size.',    &
            'nxd         = ',nxd,    ', nyd         = ',nyd,            &
            'nproc_x_out = ', npoutx,', nproc_y_out = ',npouty
        CALL nf_handle_error(istatus,'copy_netcdf_definition:wrong dimension size')
      END IF

      nxs = (nxd-1)/npoutx;  nxf = nxs
      nys = (nyd-1)/npouty;  nyf = nys

      IF (iloc == npoutx) nxf = nxf+1
      IF (jloc == npouty) nyf = nyf+1

      iss = (iloc-1)*nxs+1    ; ips = iss
      ise = iss+nxs-1         ; ipe = ips+nxf-1
      jss = (jloc-1)*nys+1    ; jps = jss
      jse = jss+nys-1         ; jpe = jps+nyf-1

    END IF

  !---------------------------------------------------------------------
  !
  ! Copy dimensions definitions
  !
  !---------------------------------------------------------------------

     istatus = NF_INQ_UNLIMDIM(fin,unlimdimid)
     IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:nf_inq_unlimdim')

     istatus = NF_INQ_NDIMS(fin,ndims)
     IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'NF_INQ_NDIMS in copy_netcdf_definition')

     DO dimid = 1, ndims

       istatus = NF_INQ_DIM(fin,dimid,dimname,dimlen)
       IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:nf_inq_dim')

       IF (dimid == unlimdimid) dimlen = NF_UNLIMITED

       IF (fixdim) THEN
         SELECT CASE (TRIM(dimname))
         CASE ('west_east')
           dimlen = nxs
         CASE ('west_east_stag')
           dimlen = nxf
         CASE ('south_north')
           dimlen = nys
         CASE ('south_north_stag')
           dimlen = nyf
         END SELECT
       END IF

       istatus = NF_DEF_DIM(fout,dimname,dimlen,odimid)
       IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:nf_def_dim')

     END DO

  !---------------------------------------------------------------------
  !
  ! Copy global attributes
  !
  !---------------------------------------------------------------------

    istatus = NF_INQ_NATTS(fin,ngatts)
    IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:nf_inq_natts')

    DO attnum = 1, ngatts

      istatus = NF_INQ_ATTNAME(fin,NF_GLOBAL,attnum,attname)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:nf_inq_attname')

      istatus = NF_COPY_ATT(fin,NF_GLOBAL,attname,fout,NF_GLOBAL)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:nf_copy_att')

    END DO

    IF (fixdim) THEN
      istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'WEST-EAST_PATCH_START_UNSTAG', NF_INT,1,iss)
      istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'WEST-EAST_PATCH_END_UNSTAG',   NF_INT,1,ise)
      istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'WEST-EAST_PATCH_START_STAG',   NF_INT,1,ips)
      istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'WEST-EAST_PATCH_END_STAG',     NF_INT,1,ipe)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:NF_PUT_ATT_INT')

      istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'SOUTH-NORTH_PATCH_START_UNSTAG', NF_INT,1,jss)
      istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'SOUTH-NORTH_PATCH_END_UNSTAG',   NF_INT,1,jse)
      istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'SOUTH-NORTH_PATCH_START_STAG',   NF_INT,1,jps)
      istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'SOUTH-NORTH_PATCH_END_STAG',     NF_INT,1,jpe)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:NF_PUT_ATT_INT')
    END IF

  !---------------------------------------------------------------------
  !
  ! Copy variable definitions
  !
  !---------------------------------------------------------------------

    istatus = NF_INQ_NVARS(fin,nvars)
    IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:nf_inq_nvars')

    DO varid = 1, nvars
      istatus = NF_INQ_VAR(fin,varid,varname,vartype,varndims,vardimids,varnatts)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:nf_inq_var')

      IF (noutvar > 0 .AND. (.NOT. name_in_set(varname,out_names,noutvar)) ) CYCLE

      istatus = NF_DEF_VAR(fout,varname,vartype,varndims,vardimids,ovarid)
      IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:nf_def_var')

      !
      ! Copy variable attributes
      !
      DO attnum = 1,varnatts
        istatus = NF_INQ_ATTNAME(fin,varid,attnum,attname)
        IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:nf_inq_attname')

        istatus = NF_COPY_ATT(fin,varid,attname,fout,ovarid)
        IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:nf_copy_att')
      END DO

      !
      ! deflate
      !
      istatus = NF_INQ_VAR_DEFLATE(fin, varid, varshuffle, vardeflate, vardeflate_level)
      IF (istatus == NF_NOERR) THEN
        istatus = NF_DEF_VAR_DEFLATE(fout, ovarid, varshuffle, vardeflate, vardeflate_level)
        IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definitions:nf_def_var_deflate')
      END IF
    END DO

  !---------------------------------------------------------------------
  !
  ! End file definition mode
  !
  !---------------------------------------------------------------------

    istatus = NF_ENDDEF(fout)
    IF (istatus /= NF_NOERR) CALL nf_handle_error(istatus,'copy_netcdf_definition:nf_enddef')

    RETURN
  END SUBROUTINE copy_netcdf_definition

  !############ Copy variables from one file to another file #############

  SUBROUTINE copy_netcdf_vars ( fin, fout, dimdelay, nexcl, excludesets,&
                                nvarout,outvarsets,outvarstagger,outvardim, debug,istatus )

  !---------------------------------------------------------------------
  !
  ! PURPOSE:
  !
  !   Copy dataset from one netCDF file to another file. This procedure can reduce
  !   output datasets.
  !
  ! NOTE: each file patch must be called separately
  !
  !---------------------------------------------------------------------
  !
  ! AUTHOR: Y. Wang (03/06/2014)
  !
  !---------------------------------------------------------------------

    !IMPLICIT NONE

    INTEGER,           INTENT(IN)  :: fin, fout
    LOGICAL,           INTENT(IN)  :: dimdelay
    INTEGER,           INTENT(IN)  :: nexcl
    CHARACTER(LEN=20), INTENT(IN)  :: excludesets(nexcl)
    INTEGER,           INTENT(OUT) :: nvarout
    CHARACTER(LEN=20), INTENT(OUT) :: outvarsets(100)
    CHARACTER(LEN=1),  INTENT(OUT) :: outvarstagger(100)
    INTEGER,           INTENT(OUT) :: outvardim(4,100)
    INTEGER,           INTENT(IN)  :: debug
    INTEGER,           INTENT(OUT) :: istatus

  !-----------------------------------------------------------------------

    !INCLUDE 'netcdf.inc'

    CHARACTER(LEN=32) :: varname, dimname(NF_MAX_DIMS)

    INTEGER :: varid, nvars, idim, ovarid
    INTEGER :: vartype, varndims, varnatts, vardimids(NF_MAX_DIMS)

    INTEGER :: narrsize, narrisizemax, narrasizemax
    INTEGER :: startidx(NF_MAX_DIMS), dimlen(NF_MAX_DIMS)

    INTEGER, ALLOCATABLE :: varari(:)
    REAL,    ALLOCATABLE :: vararr(:)
    CHARACTER(LEN=256)   :: varstr

  !-----------------------------------------------------------------------

    !LOGICAL :: name_in_set

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    nvarout = 0
    !outvarsets(:)  = ''
    !outvardim(3,:) = 0

    istatus = NF_INQ_NVARS(fin,nvars)
    IF (istatus /= NF_NOERR)  CALL nf_handle_error(istatus,'copy_netcdf_vars:nf_inq_nvars')

    narrisizemax = 0
    narrasizemax = 0

    startidx(:)  = 1

    DO varid = 1, nvars

      dimlen(:)  = 1
      narrsize   = 1

      istatus = NF_INQ_VAR(fin,varid,varname,vartype,varndims,vardimids,varnatts)
      IF (istatus /= NF_NOERR)  CALL nf_handle_error(istatus,'copy_netcdf_vars:nf_inq_var')

      IF (nexcl > 0 .AND. name_in_set(varname,excludesets,nexcl)) THEN
        IF (debug > 0) WRITE(*,'(1x,3a)') 'Variable ',TRIM(varname),' in excludesets and skipped.'
        CYCLE
      END IF

      !istatus = nf_copy_var(fin,varid,fout)
      !IF (istatus /= NF_NOERR)  CALL nf_handle_error(istatus,'copy_netcdf_vars:nf_copy_var:'//TRIM(varname))

      DO idim = 1, varndims
        istatus = NF_INQ_DIM(fin,vardimids(idim),dimname(idim),dimlen(idim))
        !istatus = nf_inq_dimlen(fin,vardimids(idim),countidx(idim))
        IF (istatus /= NF_NOERR)  CALL nf_handle_error(istatus,'copy_netcdf_vars:nf_inq_dim'//TRIM(varname))

        narrsize = narrsize*dimlen(idim)
      END DO

      IF (dimdelay) THEN         ! patch configuration changed, so delay copying of this variable
        IF (TRIM(dimname(1)) == 'west_east'   .OR. TRIM(dimname(1)) == 'west_east_stag' .OR. &
            TRIM(dimname(1)) == 'south_north' .OR. TRIM(dimname(1)) == 'south_north_stag' ) THEN
          nvarout = nvarout + 1
          outvarsets(nvarout)  = TRIM(varname)

          istatus = NF_GET_ATT_TEXT (fin,varid,'stagger', outvarstagger(nvarout))
          IF (istatus /= NF_NOERR)  CALL nf_handle_error(istatus,'copy_netcdf_vars:NF_GET_ATT_TEXT:stagger:'//TRIM(varname))

          outvardim(:,nvarout) = dimlen(1:4)

          IF (debug > 0) WRITE(*,'(1x,3a)') 'Variable ',TRIM(varname),' should be copyed later.'
          CYCLE
        END IF
      END IF

      IF (debug > 0) WRITE(*,'(1x,3a)') 'Copying variable ',TRIM(varname),' ...'

      istatus = NF_INQ_VARID(fout,varname,ovarid)
      IF (istatus /= NF_NOERR)  CALL nf_handle_error(istatus,'copy_netcdf_vars:NF_INQ_VARID:'//TRIM(varname))

      SELECT CASE (vartype)

      CASE (NF_INT)
        IF (narrsize > narrisizemax) THEN   ! Allocate input array only when necessary
          IF (ALLOCATED(varari)) DEALLOCATE(varari, STAT = istatus)
          ALLOCATE(varari(narrsize), STAT = istatus)
          narrisizemax = narrsize
        END IF

        istatus = NF_GET_VARA_INT(fin,varid,startidx,dimlen,varari)
        IF (istatus /= NF_NOERR)  CALL nf_handle_error(istatus,'copy_netcdf_vars:NF_GET_VARA_INT:'//TRIM(varname))

        istatus = NF_PUT_VARA_INT(fout,ovarid,startidx,dimlen,varari)
        IF (istatus /= NF_NOERR)  CALL nf_handle_error(istatus,'copy_netcdf_vars:nf_put_vara_INT:'//TRIM(varname))

      CASE (NF_FLOAT)

        IF (narrsize > narrasizemax) THEN   ! Allocate input array only when necessary
          IF (ALLOCATED(vararr)) DEALLOCATE(vararr, STAT = istatus)
          ALLOCATE(vararr(narrsize), STAT = istatus)
          narrasizemax = narrsize
        END IF

        istatus = NF_GET_VARA_REAL(fin,varid,startidx,dimlen,vararr)
        IF (istatus /= NF_NOERR)  CALL nf_handle_error(istatus,'copy_netcdf_vars:NF_GET_VARA_REAL:'//TRIM(varname))

        istatus = NF_PUT_VARA_REAL(fout,ovarid,startidx,dimlen,vararr)
        IF (istatus /= NF_NOERR)  CALL nf_handle_error(istatus,'copy_netcdf_vars:nf_put_vara_REAL:'//TRIM(varname))
      CASE (NF_CHAR)
        WRITE(6,'(1x,2a)') 'INFO: skip variable ',TRIM(varname)
      !  istatus = NF_GET_VAR_TEXT(fin,varid,varstr)
      !  IF (istatus /= NF_NOERR)  CALL nf_handle_error(istatus,'copy_netcdf_vars:NF_GET_VAR_TEXT:'//TRIM(varname))
      !
      !  istatus = NF_PUT_VAR_TEXT(fout,ovarid,varstr)
      !  IF (istatus /= NF_NOERR)  CALL nf_handle_error(istatus,'copy_netcdf_vars:NF_PUT_VAR_TEXT:'//TRIM(varname))
      CASE DEFAULT
        WRITE(6,'(1x,a,I2,2a)') 'ERROR: unsupported variable type = ',vartype,' for variable ',TRIM(varname)
        istatus = -4
      END SELECT

    END DO

    IF (ALLOCATED(varari)) DEALLOCATE(varari)
    IF (ALLOCATED(vararr)) DEALLOCATE(vararr)

    RETURN
  END SUBROUTINE copy_netcdf_vars

  !
  !##################################################################
  !##################################################################
  !######                                                      ######
  !######                 SUBROUTINE nf_handle_error           ######
  !######                                                      ######
  !######                     Developed by                     ######
  !######     Center for Analysis and Prediction of Storms     ######
  !######                University of Oklahoma                ######
  !######                                                      ######
  !##################################################################
  !##################################################################
  !
  SUBROUTINE nf_handle_error(ierr,sub_name)

  !---------------------------------------------------------------------
  !
  ! PURPOSE:
  !   Write error message to the standard output if ierr contains an error
  !
  !---------------------------------------------------------------------
  !
  ! AUTHOR: Y. Wang (01/29/2014)
  !
  !---------------------------------------------------------------------

    IMPLICIT NONE
    INTEGER,          INTENT(IN) :: ierr
    CHARACTER(LEN=*), INTENT(IN) :: sub_name
    CHARACTER(LEN=80) :: errmsg
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
  !  Beginning of executable code...
  !
  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  !
    IF(ierr /= NF_NOERR) THEN
      errmsg = NF_STRERROR(ierr)
      WRITE(6,*) 'NetCDF error: ',errmsg
      WRITE(6,*) 'Program stopped while calling ', sub_name
      STOP
    END IF

    RETURN
  END SUBROUTINE nf_handle_error

  !######################## name_in_set ################################

  LOGICAL FUNCTION name_in_set(vname,chset,nset)

    IMPLICIT NONE
    CHARACTER(LEN=*), INTENT(IN)  :: vname
    INTEGER,          INTENT(IN)  :: nset
    CHARACTER(LEN=*), INTENT(IN)  :: chset(nset)

  !-----------------------------------------------------------------------

    INTEGER :: i

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    name_in_set = .FALSE.

    DO i = 1, nset

      IF (TRIM(vname) == TRIM(chset(i)) ) THEN
        name_in_set = .TRUE.
        EXIT
      END IF

    END DO

    RETURN
  END FUNCTION


  !######################## findname ################################

  INTEGER FUNCTION findname(chset,nset,vname)

    IMPLICIT NONE
    CHARACTER(LEN=*), INTENT(IN)  :: vname
    INTEGER,          INTENT(IN)  :: nset
    CHARACTER(LEN=*), INTENT(IN)  :: chset(nset)

  !-----------------------------------------------------------------------

    INTEGER :: i

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    findname = 0

    DO i = 1, nset

      IF (TRIM(vname) == TRIM(chset(i)) ) THEN
        findname = i
        EXIT
      END IF

    END DO

    RETURN
  END FUNCTION

END MODULE wrf_daupdate_netcdf_interface
