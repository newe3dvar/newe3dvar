#include "geography.h"
#include "dorade.h"

/**********************************************************************
 *
 * inital NEXRAD sweep structure (ref and vel)
 *
 * ********************************************************************/

void init_swp_nexrad(struct Dorade_Sweep *swp,
          char *proj_name,char *radar_name,
          GeoPt radarLoc, float radar_alt_m,int i_vcp,
          struct GeoTime_Jul time,
          int n_parms, char *parmNames[], char *parmDesc[], char *parmUnits[],
          short i_scan, Angle fixed_angle, int i_tilt,
          int n_max_rays, int n_max_gates);

/**********************************************************************
 *
 * inital sweep structure
 *
 * ********************************************************************/

int init_solo(char *proj_name, char *radar_name,
        int i_lat, int i_lon, int i_alt,int i_vcp,
        int iyear,int imon, int iday,int ihr, int imin, double second,
        int i_scan, int i_tilt,int i_angle,
        int n_ref_gates,int n_vel_gates, int n_max_rays);

/**********************************************************************
 *
 * Main Function to fill sweep structure
 *
 * ********************************************************************/

int add_radial_to_sweep(int i_tilt, double elevin, double azimin,
               int ref_ok, int vel_ok,int n_ref_gates,int n_vel_gates,int n_spw_gates,
               float *v_nyq,
               int iyear,int imon, int iday,int ihr, int imin, double second,
               int rfrst_ref,int gsp_ref,int rfrst_vel, int gsp_vel,
               float *ref_ptr,float *vel_ptr,float *spw_ptr);

/**********************************************************************
 *
 * Write a NEXRAD sweep
 *
 * ********************************************************************/

int write_sweep_nexrad(struct Dorade_Sweep *swp);

/**********************************************************************
 *
 * Funciton to write a sweep file
 *
 * ********************************************************************/

int write_sweep();

/**********************************************************************
 *
 * clean up before exit
 *
 * ********************************************************************/

void clean_up(void);
