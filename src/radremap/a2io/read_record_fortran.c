/*
 *	Read_record.c
 *
 *	This is a Fortran interface version.
 *
 */

#pragma ident	"@(#)read_record_fortran.c	5.1	03/01/05	CAPS"

#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <strings.h>

int set_console_messages( int ) ;
int read_record() ;

int read_record_f( irc )
int *irc;
{
	*irc = read_record();
	return 0;
}

int read_record_f_( irc )
int *irc;
{
	read_record_f( irc );
	return 0;
}

int set_console_messages_f( n )
int *n;
{
	set_console_messages( *n );
	return 0;
}

int set_console_messages_f_( n )
int *n;
{
	set_console_messages_f( n );
	return 0;
}
