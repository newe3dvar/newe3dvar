!########################################################################
!########################################################################
!#########                                                      #########
!#########                  PROGRAM RADREMAP                    #########
!#########                                                      #########
!#########                     Developed by                     #########
!#########    Center for Analysis and Prediction of Storms      #########
!#########                University of Oklahoma                #########
!#########                                                      #########
!########################################################################
!########################################################################

PROGRAM radremap

!------------------------------------------------------------------------
!
! PURPOSE:
!
! Reads radar data from NEXRAD 88D data files and remaps data on either
! ARPS or WRF grid to be used for analysis or display.
!
! Fortran version that replicates the function of the original 88d2arps.c,
! developed in the early-90s with updates through 2009.
!
!------------------------------------------------------------------------
!
! AUTHOR:
!
! Keith Brewster (July, 2009)
!
! MODIFICATIONS:
!
! Keith Brewster, February, 2010
! Updated logic to provide balanced load sharing on MPI.
!
! Keith Brewster, July, 2010
! Some clean-up after testing for acceptance in official ARPS release.
!
! Keith Brewster
! Tanslated the following Y. Jung's addition to 88d2arps.c to this version
!   Added wtradtiltcol subroutine to store radar data in the colume on
!   radar tilts. To be used in the EnKF analysis.
!
! Yunheng Wang (04/08/2011)
! Fixed solo support.
!
! Keith Brewster (08/22/2011)
! Added vadsrc to wtwadprf subroutine call.
!
! Y. Wang (05/08/2012)
! Replace the non-standard calls of getarg with the Fortran 2003
! standard call of GET_COMMAND_ARGUMENT.
!
! Keith Brewster (09/07/2012)
! Added processing of dual-pol variables, including new input file
! options to control dual-pol processing and output.
!
!------------------------------------------------------------------------

  IMPLICIT NONE

!------------------------------------------------------------------------
!
! Include files
!
!-----------------------------------------------------------------------

  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'
  INCLUDE 'remapcst.inc'
  INCLUDE 'mp.inc'

!-----------------------------------------------------------------------
!
! Dimensions and parameters
!
!-----------------------------------------------------------------------

  INTEGER, PARAMETER :: maxvar = 10

  INTEGER, PARAMETER :: stdout = 6
  INTEGER, PARAMETER :: iordunf = 2
  INTEGER, PARAMETER :: itime1970 = 315619200

  CHARACTER(LEN=6)  :: velid = 'radv3d'
  CHARACTER(LEN=6)  :: refid = 'refl3d'
  CHARACTER(LEN=6)  :: rhvid = 'rhv_3d'
  CHARACTER(LEN=6)  :: zdrid = 'zdr_3d'
  CHARACTER(LEN=6)  :: kdpid = 'kdp_3d'
  CHARACTER(LEN=20) :: velname = 'RawVelocity'
  CHARACTER(LEN=20) :: refname = 'Reflectivity'
  CHARACTER(LEN=20) :: rhvname = 'Correlation H-V'
  CHARACTER(LEN=20) :: zdrname = 'Zdr'
  CHARACTER(LEN=20) :: kdpname = 'Kdp'
  CHARACTER(LEN=20) :: velunits = 'm/s'
  CHARACTER(LEN=20) :: refunits = 'dBZ'
  CHARACTER(LEN=20) :: rhvunits = ' '
  CHARACTER(LEN=20) :: zdrunits = 'dB'
  CHARACTER(LEN=20) :: kdpunits = 'deg/km'

  REAL, PARAMETER :: misval  = -9999.
!-----------------------------------------------------------------------
!
! Variable Declarations.
!
!-----------------------------------------------------------------------

  INTEGER :: nx, ny, nz, nzsoil, nstyps
  REAL    :: radalt, radlat, radlon
  REAL    :: radarx, radary

!------------------------------------------------------------------------
!
! Radar data dimensions
!
!------------------------------------------------------------------------

  INTEGER :: maxrgate,maxvgate,maxgate,maxazim,maxelev

!
!-----------------------------------------------------------------------
!
! Variables for VAD profiles.
!
!-----------------------------------------------------------------------
!
  INTEGER            :: vadkdim
  CHARACTER(LEN=256) :: vad_fname

  REAL, ALLOCATABLE :: zk(:)
  REAL, ALLOCATABLE :: vadhgt(:), vaddir(:), vadspd(:)
!
!------------------------------------------------------------------------
!
! Variables for remapping routines.
!
!------------------------------------------------------------------------
!
  INTEGER :: nt=1
  !INTEGER, PARAMETER :: exbcbufsz=1
  INTEGER, PARAMETER :: rfropt = 1  ! 4/3rds earth refraction option
  INTEGER, PARAMETER :: nsort = 601
  REAL,    PARAMETER :: sortmin = -150.0
  REAL,    PARAMETER :: sortmax = 150.0
  INTEGER, PARAMETER :: nzsnd = 201
  REAL,    PARAMETER :: dzsnd=100.
  INTEGER :: ktsnd(nzsnd)
  REAL    :: zsnd(nzsnd)             ! hgt levels for refractivity sounding
  REAL    :: usnd(nzsnd)
  REAL    :: vsnd(nzsnd)
  REAL    :: rfrsnd(nzsnd)           ! refractivity sounding
  REAL,    PARAMETER :: rfrconst = (-1./(4.*6371.E03))   ! -1/(4e)

  !REAL,    ALLOCATABLE :: arfdata(:,:) ! attenuated refl (dBZ) data
  !REAL,    ALLOCATABLE :: snstvty(:) ! radar sensitivity
  REAL,    ALLOCATABLE :: rtem(:,:)  ! temporary array for radar data processing
  !INTEGER, ALLOCATABLE :: gcfst(:) ! Ground clutter filter state
  !INTEGER, ALLOCATABLE :: istrgate(:)

  INTEGER :: itimfrst            ! time of first radial in volume
  INTEGER :: rfrst_ref           ! range to first gate (meters)
  INTEGER :: gtspc_ref           ! gate spacing (meters)
  INTEGER :: rfrst_vel           ! range to first gate (meters)
  INTEGER :: gtspc_vel           ! gate spacing (meters)
  INTEGER :: imaxrng

  INTEGER, ALLOCATABLE :: kntrgat(:,:)
  INTEGER, ALLOCATABLE :: kntrazm(:)
  INTEGER :: kntrelv

  INTEGER, ALLOCATABLE :: kntvgat(:,:)
  INTEGER, ALLOCATABLE :: kntvazm(:)
  INTEGER :: kntvelv

  INTEGER, ALLOCATABLE :: timevolr(:,:)
  INTEGER, ALLOCATABLE :: timevolv(:,:)
  REAL,    ALLOCATABLE :: vnyqvol(:,:)

  REAL,    ALLOCATABLE :: rngrvol(:,:)
  REAL,    ALLOCATABLE :: azmrvol(:,:)
  REAL,    ALLOCATABLE :: elvrvol(:,:)
  REAL,    ALLOCATABLE :: elvmnrvol(:)
  REAL,    ALLOCATABLE :: refvol(:,:,:)
  REAL,    ALLOCATABLE :: rhvvol(:,:,:)
  REAL,    ALLOCATABLE :: zdrvol(:,:,:)
  REAL,    ALLOCATABLE :: kdpvol(:,:,:)
  REAL,    ALLOCATABLE :: rngvvol(:,:)
  REAL,    ALLOCATABLE :: azmvvol(:,:)
  REAL,    ALLOCATABLE :: elvvvol(:,:)
  REAL,    ALLOCATABLE :: elvmnvvol(:)
  REAL,    ALLOCATABLE :: velvol(:,:,:)
  REAL,    ALLOCATABLE :: elvmean(:)
  INTEGER, ALLOCATABLE :: kntbin(:)
  INTEGER, ALLOCATABLE :: ngateref(:)
  INTEGER, ALLOCATABLE :: ngatevel(:)

  REAL,    ALLOCATABLE :: rxvol(:,:,:)
  REAL,    ALLOCATABLE :: ryvol(:,:,:)
  REAL,    ALLOCATABLE :: rzvol(:,:,:)

  REAL,    ALLOCATABLE :: zp(:,:,:)
  REAL,    ALLOCATABLE :: xs(:)
  REAL,    ALLOCATABLE :: ys(:)
  REAL,    ALLOCATABLE :: zps(:,:,:)
  !REAL,    ALLOCATABLE :: hterain(:,:)
  !REAL,    ALLOCATABLE :: mapfct(:,:,:) ! Map factors at scalar, u and v points
  !REAL,    ALLOCATABLE :: j1    (:,:,:) ! Coordinate transformation Jacobian defined
  !                                      ! as - d( zp )/d( x ).
  !REAL,    ALLOCATABLE :: j2    (:,:,:) ! Coordinate transformation Jacobian defined
  !                                      ! as - d( zp )/d( y ).
  !REAL,    ALLOCATABLE :: j3    (:,:,:) ! Coordinate transformation Jacobian defined
                                        ! as d( zp )/d( z ).
  INTEGER, ALLOCATABLE :: icolp(:)
  INTEGER, ALLOCATABLE :: jcolp(:)
  REAL,    ALLOCATABLE :: xcolp(:)
  REAL,    ALLOCATABLE :: ycolp(:)
  REAL,    ALLOCATABLE :: zcolp(:,:)
  LOGICAL, ALLOCATABLE :: havdat(:,:)

  REAL, ALLOCATABLE :: colvel(:,:)
  REAL, ALLOCATABLE :: colref(:,:)
  REAL, ALLOCATABLE :: colrhv(:,:)
  REAL, ALLOCATABLE :: colzdr(:,:)
  REAL, ALLOCATABLE :: colkdp(:,:)
  REAL, ALLOCATABLE :: colnyq(:,:)
  REAL, ALLOCATABLE :: coltim(:,:)

  REAL, ALLOCATABLE :: x(:)
  REAL, ALLOCATABLE :: y(:)
  REAL, ALLOCATABLE :: z(:)
  !REAL, ALLOCATABLE :: xslg(:)
  !REAL, ALLOCATABLE :: yslg(:)
  !REAL, ALLOCATABLE :: zpslg(:,:,:)

  !REAL, ALLOCATABLE :: tem1d1(:)
  !REAL, ALLOCATABLE :: tem1d2(:)
  REAL, ALLOCATABLE :: tem2d(:,:)
  !REAL, ALLOCATABLE :: tem2dyz(:,:)
  !REAL, ALLOCATABLE :: tem2dxz(:,:)
  !REAL, ALLOCATABLE :: tem2dns(:,:,:)
  REAL, ALLOCATABLE :: tem3d1(:,:,:)
  REAL, ALLOCATABLE :: tem3d2(:,:,:)
  REAL, ALLOCATABLE :: tem3d3(:,:,:)
  !REAL, ALLOCATABLE :: tem3d4(:,:,:)
  !INTEGER, ALLOCATABLE :: tem2dint(:,:)
  !INTEGER, ALLOCATABLE :: soiltyp(:,:,:)
  !REAL, ALLOCATABLE :: stypfrct(:,:,:)
  !REAL, ALLOCATABLE :: tem3dsoil(:,:,:)
  !REAL, ALLOCATABLE :: tem4dsoilns(:,:,:,:)

  !REAL, ALLOCATABLE :: zpsoil(:,:,:)
  !REAL, ALLOCATABLE :: j3soil(:,:,:)
  !REAL, ALLOCATABLE :: j3soilinv(:,:,:)

  REAL, ALLOCATABLE :: u(:,:,:)
  REAL, ALLOCATABLE :: v(:,:,:)
  REAL, ALLOCATABLE :: qv(:,:,:)
  REAL, ALLOCATABLE :: qscalar(:,:,:,:)
  !REAL, ALLOCATABLE :: ubar(:,:,:)
  !REAL, ALLOCATABLE :: vbar(:,:,:)
  !REAL, ALLOCATABLE :: ptbar(:,:,:)
  REAL, ALLOCATABLE :: ptprt(:,:,:)
  REAL, ALLOCATABLE :: pprt(:,:,:)
  !REAL, ALLOCATABLE :: pbar(:,:,:)
  !REAL, ALLOCATABLE :: qvbar(:,:,:)
  !REAL, ALLOCATABLE :: rhostr(:,:,:)

  !REAL, ALLOCATABLE :: trigs1(:)
  !REAL, ALLOCATABLE :: trigs2(:)
  !INTEGER, ALLOCATABLE :: ifax(:)
  !REAL, ALLOCATABLE :: wsave1(:)
  !REAL, ALLOCATABLE :: wsave2(:)
  !REAL, ALLOCATABLE :: vwork1(:,:)
  !REAL, ALLOCATABLE :: vwork2(:,:)

  !REAL, ALLOCATABLE :: qcumsrc(:,:,:,:)
  !REAL, ALLOCATABLE :: prcrate(:,:,:)
  !REAL, ALLOCATABLE :: exbcbuf(:)

!------------------------------------------------------------------------
!
! horizontal grid and vertial tilt output arrays
!
!------------------------------------------------------------------------

  INTEGER :: grdtiltnumvar

  REAL,    ALLOCATABLE :: grdtiltdata(:,:,:,:)

  REAL,    ALLOCATABLE :: grdtilthighref(:,:,:)
  REAL,    ALLOCATABLE :: grdrangeref(:,:,:)
  REAL,    ALLOCATABLE :: grdslrref(:,:)
  REAL,    ALLOCATABLE :: grdazmref(:,:)
  REAL,    ALLOCATABLE :: elevmeanref(:)

  INTEGER, ALLOCATABLE :: tilttimeref(:)
  INTEGER, ALLOCATABLE :: tilttimevel(:)

  REAL,    ALLOCATABLE :: grdtilthighvel(:,:,:)
  REAL,    ALLOCATABLE :: grdrangevel(:,:,:)
  REAL,    ALLOCATABLE :: grdslrvel(:,:)
  REAL,    ALLOCATABLE :: grdazmvel(:,:)
  REAL,    ALLOCATABLE :: elevmeanvel(:)


!------------------------------------------------------------------------
!
! WRF data file related working arrays
!
!------------------------------------------------------------------------

  CHARACTER (LEN=256) :: wrf_file
  CHARACTER (LEN=19)  :: timestring
  CHARACTER (LEN=1)   :: ach

  INTEGER :: nx_wrf, ny_wrf, nz_wrf, nzsoil_wrf
  INTEGER :: nxlg_wrf, nylg_wrf
  INTEGER :: iproj_wrf

  INTEGER :: ii, jj, kk

  REAL, ALLOCATABLE :: x_wrf(:)
  REAL, ALLOCATABLE :: y_wrf(:)
  REAL, ALLOCATABLE :: zp_wrf(:,:,:)
  REAL, ALLOCATABLE :: xs_wrf(:)
  REAL, ALLOCATABLE :: ys_wrf(:)
  REAL, ALLOCATABLE :: zps_wrf(:,:,:)

!------------------------------------------------------------------------
!
! Misc. variables
!
!------------------------------------------------------------------------

  CHARACTER(LEN=256) :: full_fname
  CHARACTER(LEN=256) :: errmessage

  INTEGER :: i,j,k,kelv,istatus,ilen
  INTEGER :: rdstat, iopt
  INTEGER :: nxny
  INTEGER :: ivcp,ivar
  INTEGER :: varfill,dualpol
  INTEGER :: volflag
  INTEGER :: nprocx_in_cmd,nprocy_in_cmd

  LOGICAL :: velproc
  LOGICAL :: dualpdata,dualpproc
  LOGICAL :: vel2d,vel3d
  LOGICAL :: ref2d,ref3d
  LOGICAL :: rhv2d,rhv3d
  LOGICAL :: zdr2d,zdr3d
  LOGICAL :: kdp2d,kdp3d
  LOGICAL :: unfdiag
  LOGICAL :: qc_on, dealiase
  LOGICAL :: fntime
  LOGICAL :: traditional
  LOGICAL :: rtopts
  INTEGER :: vad
  LOGICAL :: wrttilt,wrtgrdtilt
  LOGICAL :: wrtsolo,wrtsoloqc
  LOGICAL :: remap_on

  LOGICAL :: clrvcp

  INTEGER,           PARAMETER :: iradfmt = 1
  CHARACTER (LEN=8), PARAMETER :: vadsrc = '88D VAD'

  INTEGER :: iyear,imon,iday,ihr,imin,isec
  INTEGER :: kmyear,kmmon,kmday,kmhr,kmmin,kmsec
  INTEGER :: iiyear,iimon,iiday,iihr,iimin,iisec
  INTEGER :: isource,icount
  INTEGER :: iangle
  INTEGER :: nyqset,timeset,vardump

  INTEGER :: ncoltot,ncolp

  REAL :: dsort
  REAL :: refelvmin,refelvmax
  REAL :: refrngmin,refrngmax
  REAL :: maxrng

  REAL :: vnyquist
  REAL :: tmin,tmax

  INTEGER :: iarg,jarg,narg
  INTEGER :: ng_vel

  CHARACTER (LEN=256) :: charg, tempstr
  CHARACTER (LEN=6)   :: varid
  CHARACTER (LEN=20)  :: varname
  CHARACTER (LEN=20)  :: varunits

  INTEGER, PARAMETER :: ROOT = 0
  REAL    :: rdummy

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!------------------------------------------------------------------------
!
! Initializations
!
!------------------------------------------------------------------------
!
  refelvmin = 91.0
  refelvmax = -91.0
  refrngmin = 5.0E03
  refrngmax = 230.0E03
  dmpfmt=1
  hdf4cmpr=0
  fillref=0
  velproc = .TRUE.
  dualpdata = .FALSE.
  dualpproc = .FALSE.
  unfdiag = .FALSE.
  qc_on = .TRUE.
  dealiase  = .FALSE.
  ref2d = .FALSE.
  ref3d = .FALSE.
  vel2d = .FALSE.
  vel3d = .FALSE.
  zdr2d = .FALSE.
  zdr3d = .FALSE.
  kdp2d = .FALSE.
  kdp3d = .FALSE.
  rhv2d = .FALSE.
  rhv3d = .FALSE.
  fntime = .FALSE.
  traditional = .FALSE.
  rtopts = .FALSE.
  vad = 0
  wrttilt = .FALSE.
  wrtgrdtilt = .FALSE.
  wrtsolo = .FALSE.
  wrtsoloqc = .FALSE.
  clrvcp = .FALSE.
  grdtiltver = 2
  grdtiltnumvar = 2

  dsort = (sortmax-sortmin)/float(nsort-1)

  radname = 'DMMY'
  radband = 1
  myproc = 0
  mp_opt = 0
  nprocx_in_cmd = 1
  nprocy_in_cmd = 1

  CALL mpinit_proc(0)

!------------------------------------------------------------------------
!
! Go through the arguments to extract "nprocx_in" and "nprocy_in"
! if they are present.  This must be done before call to initremapopt.
!
!------------------------------------------------------------------------
!
  istatus = 0
  tempstr = ' '
  IF(myproc == ROOT) THEN

    narg = COMMAND_ARGUMENT_COUNT()

    IF(narg > 0) THEN
      iarg = 1
      DO jarg=1,narg
        IF (jarg < iarg) CYCLE

        CALL GET_COMMAND_ARGUMENT(jarg, charg, ilen, istatus )
        IF (charg(1:5) == '-help' .OR. charg(1:6) == '--help') THEN
          CALL GET_COMMAND_ARGUMENT(0, charg, ilen, istatus )
          CALL printHELP(stdout,charg,istatus)
          EXIT
        ELSE IF(charg(1:10) == '-nprocx_in') THEN
          IF(iarg < narg) THEN
            iarg = iarg+1
            CALL GET_COMMAND_ARGUMENT(iarg, charg, ilen, istatus )
            READ(charg,*) nprocx_in_cmd
          ELSE
            WRITE(stdout,'(1x,a)') 'Option "-nprocx_in" needs an argument.'
            istatus = 2
            EXIT
          END IF
          WRITE(stdout,'(a,i4)') ' Number patches of data (X) = ',nprocx_in_cmd
        ELSE IF(charg(1:10) == '-nprocy_in') THEN
          IF(iarg < narg) THEN
            iarg = iarg+1
            CALL GET_COMMAND_ARGUMENT(iarg, charg, ilen, istatus )
            READ(charg,*) nprocy_in_cmd
          ELSE
            WRITE(stdout,'(1x,a)') 'Option "-nprocy_in" needs an argument.'
            istatus = 2
            EXIT
          END IF
          WRITE(stdout,'(a,i4)') ' Number patches of data (Y) = ',nprocy_in_cmd
        ELSE IF(jarg == narg .AND. charg(1:1) /= '-') THEN       ! namelist file
          tempstr = charg
          WRITE(stdout,'(1x,2a)') 'Namelist file from command line - ',TRIM(tempstr)
        END IF
        iarg = iarg+1
      END DO
    END IF ! narg > 0
  END IF ! myproc == 0

  CALL mpupdatei(istatus,1)
  IF (istatus /= 0) CALL arpsstop (' ',istatus)
!
!------------------------------------------------------------------------
!
! Read the input file and set logical variables based on input integers.
!
!------------------------------------------------------------------------
!
  CALL initremapopt(nx,ny,nz,nzsoil,nstyps,                             &
                    nprocx_in_cmd,nprocy_in_cmd,1,                      &
                    tempstr,istatus)

  IF(myproc == ROOT) THEN

    ref2d = (ref2dopt > 0)
    ref3d = (ref3dopt > 0)
    vel2d = (vel2dopt > 0)
    vel3d = (vel3dopt > 0)
    zdr2d = (zdr2dopt > 0)
    zdr3d = (zdr3dopt > 0)
    kdp2d = (kdp2dopt > 0)
    kdp3d = (kdp3dopt > 0)
    rhv2d = (rhv2dopt > 0)
    rhv3d = (rhv3dopt > 0)
    velproc = (velprocopt > 0)
    !IF (dualpout > 0) dualpproc = .TRUE.
    !IF( zdr2d ) dualpproc = .TRUE.
    !IF( zdr3d ) dualpproc = .TRUE.
    !IF( kdp2d ) dualpproc = .TRUE.
    !IF( kdp3d ) dualpproc = .TRUE.
    !IF( rhv2d ) dualpproc = .TRUE.
    !IF( rhv3d ) dualpproc = .TRUE.
    IF (zdr2d .OR. zdr3d .OR. kdp2d .OR. kdp3d .OR. rhv2d .OR. rhv3d .OR. dualpout > 0) dualpproc = .TRUE.

    unfdiag = (unfdiagopt > 0 .AND. unfdiagopt < 2)
    qc_on = (qcopt > 0)
    dealiase = (xu_dealiase > 0)
    traditional = (tradopt > 0)
    vad = vadopt
    wrttilt = (wttiltopt > 0)
    wrtgrdtilt = (wtgrdtiltopt > 0)
    jarg = MOD(wtsoloopt,100)
    wrtsolo   = ( jarg == 1 .OR. jarg == 3 )
    wrtsoloqc = ( jarg == 2 .OR. jarg == 3 )
    fntime = (fntimopt > 0)

    IF (.NOT. dealiase) THEN    ! make sure option for Xu's dealiase consistent
      xu_modelopt = 0
      IF (vad == 4) vad = 0
    END IF

    WRITE(6,'(a,i4,a,i4)') ' Obtained from namelists: dmpfmt  =',dmpfmt,&
                           ', hdf4cmpr=',hdf4cmpr

!-----------------------------------------------------------------------
!
! Process command line
! For backward comptability allow input of radar file names via
! files specified on the command line
!
!-----------------------------------------------------------------------

    IF(narg > 0) THEN

      iarg = 1
      DO jarg=1,narg
        IF (jarg < iarg) CYCLE
        CALL GET_COMMAND_ARGUMENT(jarg, charg, ilen, istatus )
        IF( charg(1:6) == '-radid') THEN
          IF(iarg < narg) THEN
            iarg=iarg+1
            CALL GET_COMMAND_ARGUMENT(iarg, charg, ilen, istatus )
            radname = charg(1:4)
          ELSE
            WRITE(stdout,'(1x,a)') 'Option "-radid" requires an argument.'
            istatus = 2
            EXIT
          END IF
          WRITE(stdout,'(a,a)')  '    radname = ', radname
        ELSE IF(charg(1:6) == '-novel') THEN
          velproc=.FALSE.
        ELSE IF(charg(1:4) == '-hdf') THEN
          dmpfmt=3
          hdf4cmpr=0
          IF(iarg < narg) THEN
            iarg=iarg+1
            CALL GET_COMMAND_ARGUMENT(iarg, charg )
            READ(charg,'(i1)',iostat=istatus) hdf4cmpr
            IF(istatus == 0) THEN
              hdf4cmpr=min(max(hdf4cmpr,0),7)
            ELSE
              WRITE(stdout,'(a,/a,a)')                                  &
                 '-hdf flag requires compression level 0-7 to follow',  &
                 ' found: ',TRIM(charg)
              errmessage = ' Command line argument error'
              istatus = -1
            END IF
          ELSE
            WRITE(stdout,'(1x,a)') 'Option "-hdf" requires an argument for compression level.'
            istatus = 2
            EXIT
          END IF
          WRITE(stdout,'(a,i2)')                                        &
          ' Output in hdf format with compression level: ',hdf4cmpr
        ELSE IF(charg(1:7) == '-binary') THEN
          dmpfmt=1
          hdf4cmpr=0
          WRITE(stdout,'(a)') ' Output in binary format'
        ELSE IF(charg(1:6) == '-ref2d') THEN
          ref2d=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Produce a 2d reflectivity file of lowest tilt'
        ELSE IF(charg(1:6) == '-ref3d') THEN
          ref3d=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Produce a 3d reflectivity file for plotting'
        ELSE IF(charg(1:8) == '-reffile') THEN
          ref3d=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Produce a 3d reflectivity file for plotting'
        ELSE IF(charg(1:6) == '-vel2d') THEN
          vel2d=.TRUE.
          velproc=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Produce a 2d velocity file of lowest tilt'
        ELSE IF(charg(1:6) == '-vel3d') THEN
          vel3d=.TRUE.
          velproc=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Produce a 3d velocity file for plotting'
        ELSE IF(charg(1:8) == '-velfile') THEN
          vel3d=.TRUE.
          velproc=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Produce a 3d velocity file for plotting'
        ELSE IF(charg(1:6) == '-rhv2d') THEN
          rhv2d=.TRUE.
          dualpproc=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Produce a 2d Rho-HV file of lowest tilt'
        ELSE IF(charg(1:6) == '-rhv3d') THEN
          rhv3d=.TRUE.
          dualpproc=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Produce a 3d Rho-HV file for plotting'
        ELSE IF(charg(1:6) == '-zdr2d') THEN
          zdr2d=.TRUE.
          dualpproc=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Produce a 2d Zdr file of lowest tilt'
        ELSE IF(charg(1:6) == '-zdr3d') THEN
          zdr3d=.TRUE.
          dualpproc=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Produce a 3d Zdr file for plotting'
        ELSE IF(charg(1:6) == '-kdp2d') THEN
          kdp2d=.TRUE.
          dualpproc=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Produce a 2d Kdp file of lowest tilt'
        ELSE IF(charg(1:6) == '-kdp3d') THEN
          kdp3d=.TRUE.
          dualpproc=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Produce a 3d Kdp file for plotting'
        ELSE IF(charg(1:8) == '-unfdiag') THEN
          unfdiag=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Write files for velocity unfolding diagnostics'
        ELSE IF(charg(1:5) == '-noqc') THEN
          qc_on=.FALSE.
          WRITE(stdout,'(a)')                                           &
            ' Skip QC steps in processing'
        ELSE IF(charg(1:5) == '-xuqc') THEN
          dealiase=.TRUE.
          WRITE(stdout,'(a)')                                           &
            ' Use Xu''s dealiasing package for processing'
        ELSE IF(charg(1:8) == '-fillref') THEN
          fillref=1
          WRITE(stdout,'(a)')                                           &
            ' Fill-in reflectivity below lowest tilt'
        ELSE IF(charg(1:8) == '-medfilt') THEN
          medfilt=1
          WRITE(stdout,'(a)')                                           &
            ' Apply median filter to az-ran data in despeckle step.'
        ELSE IF(charg(1:10) == '-nprocx_in') THEN
          IF(iarg < narg) THEN
            iarg=iarg+1
            CALL GET_COMMAND_ARGUMENT(iarg, charg )
            READ(charg,*) nprocx_in
          END IF
          WRITE(stdout,'(a,i4)') ' Number patches of data (X) = ',nprocx_in
        ELSE IF(charg(1:10) == '-nprocy_in') THEN
          IF(iarg < narg) THEN
            iarg=iarg+1
            CALL GET_COMMAND_ARGUMENT(iarg, charg )
            READ(charg,*) nprocy_in
          END IF
          WRITE(stdout,'(a,i4)') ' Number patches of data (Y) = ',nprocy_in

        ELSE IF(charg(1:4) == '-dir') THEN
          WRITE(stdout,'(a)') ' Use specified directory for output.'
          IF(iarg < narg) THEN
            iarg=iarg+1
            CALL GET_COMMAND_ARGUMENT(iarg, dirname)
            WRITE(stdout,'(a,a)') '  directory name: ',TRIM(dirname)
          ELSE
            WRITE(stdout,'(a,a,a)') ' Switch ',TRIM(charg), &
                                    ' requires an argument, try again'
            errmessage = ' Command line argument error'
            istatus    = -1
            EXIT
          END IF
        ELSE IF(charg(1:7) == '-fntime') THEN
          fntime=.TRUE.
          WRITE(stdout,'(a)') ' Use time in filename for output filename.'

        ELSE IF(charg(1:7) == '-rtopts') THEN
          rtopts=.TRUE.
          clrvcpopt=1   ! changed to 1 as Kevin's request for 2013 Spring
          medfilt=1
          WRITE(stdout,'(a)') ' Use real-time options.'
          WRITE(stdout,'(a)') ' Do not process any data from clear-air VCPs'
          WRITE(stdout,'(a)') ' Apply median filter.'

        ELSE IF(charg(1:8) == '-noclrv') THEN
          clrvcpopt=1
          WRITE(stdout,'(a)') ' Skipping velocities in clear-air VCP data.'

        ELSE IF(charg(1:8) == '-noclear') THEN
          clrvcpopt=0
          WRITE(stdout,'(a)') ' Skipping all clear-air VCP data.'

        ELSE IF(charg(1:8) == '-radar98') THEN
          rad98opt=1
          WRITE(stdout,'(a)') ' Read using WSR-98D CINRAD radar format.'

        ELSE IF (charg(1:5) == '-solo') THEN
          WRITE(stdout,'(a)')                                           &
            ' Write sweep file in DORADE format for working with SoloII'
          wrtsolo=.TRUE.

        ELSE IF (charg(1:7) == '-soloqc') THEN
          WRITE(stdout,'(a)')                                           &
            ' Write sweep file in DORADE format for working with SoloII after QC'
          wrtsoloqc = .TRUE.

        ELSE IF (charg(1:6) == '-sound') THEN
          WRITE(stdout,'(a)') ' Use single-sounding backgroud file'
          initopt=1
          IF(iarg < narg) THEN
            iarg=iarg+1
            CALL GET_COMMAND_ARGUMENT(iarg, sndfile )
            WRITE(stdout,'(a,a)') ' Using sounding file ',TRIM(sndfile)
          ELSE
            WRITE(stdout,'(a,a,a)') ' Switch ',TRIM(charg),             &
                                    ' requires an argument, try again'
            errmessage = ' Command line argument error'
            istatus=-1
          END IF

        ELSE IF(charg(1:6) == '-tradi') THEN
          traditional = .TRUE.
          WRITE(stdout,'(a)') ' Force use of traditional backgroud processing (initgrdvar).'

        ELSE IF (charg(1:6) == '-vad') THEN
          WRITE(stdout,'(a)') ' VAD wind profile file generated for ADAS'
          vad=1
          IF(iarg < narg) THEN
            iarg=iarg+1
            CALL GET_COMMAND_ARGUMENT(iarg, charg )
            READ(charg,*,iostat=rdstat) vad
          ELSE
            WRITE(stdout,'(a,a,a)') ' Switch "-vad" requires an argument.'
            errmessage = ' Command line argument error'
            istatus=-1
          END IF

        ELSE IF (charg(1:8) == '-wrttilt') THEN
          WRITE(stdout,'(a)') ' Write radar scans in az-ran format.'
          wrttilt =.TRUE.

        ELSE IF (charg(1:12) == '-wrtgridtilt') THEN
          WRITE(stdout,'(a)') ' Write radar scans in grid-tilt format.'
          wrtgrdtilt =.TRUE.
          IF( iarg < narg ) THEN
            CALL GET_COMMAND_ARGUMENT((iarg+1), charg )
            READ(charg,*,iostat=rdstat) iopt
            IF(rdstat == 0 .AND. iopt < 2) THEN
              iarg=iarg+1
              grdtiltver=1
            END IF
          END IF
          IF( grdtiltver == 1 ) THEN
            WRITE(stdout,'(a)') ' Old grid-tilt version for the EnKF application'
            WRITE(stdout,'(a)') ' Data are stored in the entire ARPS domain'
          ELSE
            WRITE(stdout,'(a)') ' Data are stored in updated grid tilt-column format'
            WRITE(stdout,'(a)') ' For old version use -wrtgridtilt 1'
          END IF
        ELSE IF (charg(1:8) == '-radfile') THEN
          IF(iarg < narg) THEN
            iarg=iarg+1
            CALL GET_COMMAND_ARGUMENT(iarg, charg, ilen, istatus )
            radfname=charg
          ELSE
            WRITE(stdout,'(1x,a)') 'Option "-radfile" requires an argument.'
            istatus = 2
            EXIT
          END IF
          WRITE(stdout,'(a,a)')  ' Radar data file name: ',TRIM(radfname)

        END IF
        iarg=iarg+1
      END DO
    END IF ! narg > 0
  END IF ! myproc == 0

  CALL mpupdatei(istatus,1)
  IF (istatus /= 0) CALL arpsstop(TRIM(errmessage),istatus)
!
!------------------------------------------------------------------------
!
! After command line and input variables have been read,
!   communicate updated option parameters with all processors.
!
!------------------------------------------------------------------------
!
  CALL mpupdatec(radname,4)
  !CALL mpupdatel(velproc,1)
  !CALL mpupdatel(dualpproc,1)
  !CALL mpupdatei(dualpout,1)
  CALL mpupdatei(dmpfmt,1)
  CALL mpupdatei(hdf4cmpr,1)
  CALL mpupdatel(ref2d,1)
  CALL mpupdatel(ref3d,1)
  CALL mpupdatel(vel2d,1)
  CALL mpupdatel(vel3d,1)
  CALL mpupdatel(zdr2d,1)
  CALL mpupdatel(zdr3d,1)
  CALL mpupdatel(kdp2d,1)
  CALL mpupdatel(kdp3d,1)
  CALL mpupdatel(rhv2d,1)
  CALL mpupdatel(rhv3d,1)
  CALL mpupdatel(qc_on,1)
  CALL mpupdatel(dealiase,1)
  CALL mpupdatei(fillref,1)
  CALL mpupdatel(fntime,1)
  CALL mpupdatel(traditional,1)
  CALL mpupdatel(wrttilt,1)
  CALL mpupdatel(wrtgrdtilt,1)
  CALL mpupdatei(grdtiltver,1)
  CALL mpupdatel(wrtsolo,1)
  CALL mpupdatel(wrtsoloqc,1)
  CALL mpupdatei(medfilt,1)
  CALL mpupdater(outtime,1)
  CALL mpupdatei(nprocx_in,1)
  CALL mpupdatei(nprocy_in,1)
  CALL mpupdatei(rad98opt,1)

  CALL mpupdatei(wtsoloopt,1)
  remap_on = .TRUE.
  IF (wtsoloopt > 0 .AND. wtsoloopt < 100) remap_on = .FALSE.

  IF (wrtsoloqc .AND. (.NOT. qc_on) ) THEN
    qc_on = .TRUE.
    WRITE(*,'(/,1x,a,L,a,/,10x,a,/)')                                   &
      'WARNING: wrtsoloqc = ',wrtsoloqc,', but qc_on is not on.',       &
      'So the program has turned on qc_on automatically.'
  END IF

!------------------------------------------------------------------------
!
! radar observation file format
!
!------------------------------------------------------------------------

  IF (myproc == ROOT) THEN
    WRITE(*,'(1x,a)') "========================================"
    CALL set_radar98_f(rad98opt)
  END IF

  IF (nlistfil == 1) THEN
    volflag = 0
    full_fname = listfil(1)
    IF (myproc == ROOT) WRITE(*,'(1x,3a)') 'INFO: read list file <',TRIM(full_fname),'>.'
  ELSE
    volflag = 1
    full_fname = radfname
    IF (myproc == ROOT) WRITE(*,'(1x,3a)') 'INFO: read NEXRAD file <',TRIM(full_fname),'>.'
  END IF
!
!------------------------------------------------------------------------
!
! A few more initializations.
! Most of these variables will be reset with info obtained from the data file
!
!------------------------------------------------------------------------
!
  isource = 1
  istatus = 0
  icount  = 0
  gtspc_ref = 1000
  gtspc_vel = 250
  rfrst_ref = 1000
  rfrst_vel = 1000
  maxrgate = 1
  maxvgate = 1
  maxgate = 1
  maxazim = 1
  maxelev = 1
  itimfrst = 0
  refrngmin = rngmin
  refrngmax = rngmax

  IF (myproc == ROOT) THEN

    CALL get_88dinfo(full_fname,volflag,radname,ivcp,itimfrst,          &
                     radlat,radlon,radalt,                              &
                     maxrgate,maxvgate,maxazim,maxelev,istatus)
    WRITE(6,'(1x,a,i0)')     'Back from get_88dinfo. istatus = ',istatus
    IF( istatus /= 0) THEN
      errmessage = 'Bad status opening radar file'
      GOTO 8888
    ELSE
      WRITE(6,'(1x,a,i6)')     'VCP: ',ivcp
      WRITE(6,'(1x,a,3f10.2)') 'radlat,radlon,radalt: ',radlat,radlon,radalt
    END IF

    clrvcp = ( (ivcp == 31) .OR. (ivcp == 32) )
    IF( clrvcp .AND. clrvcpopt < 1 ) THEN
      istatus=-1
      errmessage = 'Clear VCP detected and no clear selected, exiting'
      GOTO 8888
    END IF

    IF(fntime) THEN
      IF (volflag == 0) THEN
        READ(ref_time,'(I4,2I2,a,2I2)') kmyear,kmmon,kmday,ach,kmhr,kmmin
        kmsec = 0
      ELSE
        CALL get_time_from_file_name(full_fname,volflag,radname,        &
                            kmyear,kmmon,kmday,kmhr,kmmin,kmsec,istatus)
        IF (istatus /= 0) THEN
          kmyear=0;  kmmon=0;  kmday=0
          kmhr=0;    kmmin=0;  kmsec=0
        END IF
      END IF
    END IF

    IF( clrvcp .AND. clrvcpopt < 2 ) THEN
      velproc = .FALSE.
      WRITE(6,'(1x,a)') 'Velocity processing not active for Clear VCP'
    END IF
    !CALL get_88draddims(ivcp,maxrgate,maxvgate,maxazim,maxelev,istatus)
    !WRITE(6,'(1x,a)')     'Back from 88draddims...'
    !WRITE(6,'(1x,a,2i9)') 'maxrgate,maxvgate: ',maxrgate,maxvgate
    !WRITE(6,'(1x,a,2i9)') 'maxazim,maxelev: ',maxazim,maxelev
    !WRITE(6,'(1x,a,i6)')  'istatus: ',istatus
    !IF (istatus /= 0) errmessage = 'return from get_88draddims'

  END IF

  8888 CONTINUE

  CALL mpupdatei(istatus, 1)
  IF (istatus /= 0) THEN
    CALL arpsstop(TRIM(errmessage),istatus)
  END IF

  CALL mpupdatei(ivcp,    1)
  CALL mpupdatei(itimfrst,1)
  CALL mpupdater(radlat,  1)
  CALL mpupdater(radlon,  1)
  CALL mpupdater(radalt,  1)

  CALL mpupdatei(maxrgate, 1)
  CALL mpupdatei(maxvgate, 1)
  CALL mpupdatei(maxazim,  1)
  CALL mpupdatei(maxelev,  1)
  CALL mpupdatel(dualpout, 1)
  CALL mpupdatel(clrvcp,   1)
  CALL mpupdatel(velproc,  1)
  CALL mpupdatel(dualpproc,1)

  IF (fntime) THEN
    CALL mpupdatei(kmyear,1)
    CALL mpupdatei(kmmon, 1)
    CALL mpupdatei(kmday, 1)
    CALL mpupdatei(kmhr,  1)
    CALL mpupdatei(kmmin, 1)
    CALL mpupdatei(kmsec, 1)
  END IF

  maxgate=max(maxrgate,maxvgate)

  CALL abss2ctim(itimfrst, iyear, imon, iday, ihr, imin, isec)
  IF (myproc == 0) WRITE(6,'(a,i4.4,5(a,i2.2))') ' itimfrst: ',         &
                   iyear,'-',imon,'-',iday,'_',ihr,':',imin,':',isec

  IF (initopt == 3) THEN
    IF( modelopt == 2) THEN

      READ(ref_time,'(I4,2I2,a,2I2)') iyear,imon,iday,ach,ihr,imin
      isec = 0

      wrf_file = inifile

      IF (nprocx_in > 1 .OR. nprocy_in > 1) THEN
        WRITE(charg,'(2a)') TRIM(inifile),'_0000'
      ELSE
        charg = inifile
      END IF

      WRITE(timestring,'(i4.4,5(a,i2.2))') iyear,'-',imon,'-',iday,       &
                                          '_',ihr,':',imin,':',isec

      IF (myproc == 0) THEN
        CALL get_wrf_domsize(TRIM(charg),timestring,nx_wrf,ny_wrf,        &
                             nz_wrf,nzsoil_wrf,nt,nxlg_wrf,nylg_wrf,istatus)

        WRITE(6,'(/a,2(2(a,I3)))') '  WRF grid dimensions: ',             &
                        'nx = ',nxlg_wrf, ', ny= ',nylg_wrf,', nz= ',nz
        WRITE(6,'(a,I0)') '  WRF Time = ', nt

        CALL get_wrf_grid( TRIM(charg),iproj_wrf,sclfct,trulat1,trulat2,  &
                           trulon,ctrlat,ctrlon,dx,dy,mphyopt,istatus )

        IF(iproj_wrf == 0) THEN        ! NO MAP PROJECTION
          mapproj = 0
        ELSE IF(iproj_wrf == 1) THEN   ! LAMBERT CONFORMAL
          mapproj = 2
        ELSE IF(iproj_wrf == 2) THEN   ! POLAR STEREOGRAPHIC
          mapproj = 1
        ELSE IF(iproj_wrf == 3) THEN   ! MERCATOR
          mapproj = 3
        ELSE
          WRITE(6,*) 'Unknown map projection, ', iproj_wrf
          istatus = -555
        END IF

        nx = nxlg_wrf+2
        ny = nylg_wrf+2
        nz =   nz_wrf+2
      END IF

    ELSE
      IF (myproc == 0) THEN

        CALL get_dims_from_data(inifmt,TRIM(inifile),                     &
                                nx,ny,nz,nzsoil,nstyps, istatus)

        WRITE(*,'(1x,2(a,I0),a)')                                         &
            'INFO: Get global dimensions nx = ',nx,', ny = ',ny,'.'

        IF (MOD(nx-3,nproc_x) /= 0 .OR. MOD(ny-3,nproc_y) /= 0 ) THEN
          WRITE(*,'(1x,a,7x,2(a,I0),/,7x,2(a,I0),a)')                     &
              'ERROR: Inconsistent dimension and process number.',        &
              'nproc_x = ',nproc_x,', nproc_y = ',nproc_y,                &
              'nx      = ',nx,     ', ny      = ',ny,'.'
          istatus = -1
        END IF

        CALL get_arps_grid( inifmt,TRIM(inifile),mapproj,sclfct,trulat1,trulat2,&
                           trulon,ctrlat,ctrlon,dx,dy,mphyopt,istatus )
      END IF

    END IF    ! So far nx/ny are global length
    CALL mpupdatei(istatus,1)
    IF( istatus /= 0 ) CALL arpsstop(                                   &
        'Problem occurred when trying to get dimensions from data.', 1)

    CALL mpupdatei(nx,1)
    CALL mpupdatei(ny,1)
    CALL mpupdatei(nz,1)

    CALL mpupdatei(mapproj,1)
    CALL mpupdater(sclfct,1)
    CALL mpupdater(trulat1,1)
    CALL mpupdater(trulat2,1)
    CALL mpupdater(trulon,1)
    CALL mpupdater(ctrlat,1)
    CALL mpupdater(ctrlon,1)
    CALL mpupdater(dx,1)
    CALL mpupdater(dy,1)

    IF (MOD(nx-3,nproc_x) /= 0) THEN
      WRITE(*,'(1x,a,2(I0,a))') 'ERROR: dimension size in X direction (',nx,') is not a multiple of nproc_x (',nproc_x,').'
      CALL arpsstop('ERROR: dimension problem.',1)
    END IF

    IF (MOD(ny-3,nproc_y) /= 0) THEN
      WRITE(*,'(1x,a,2(I0,a))') 'ERROR: dimension size in Y direction (',ny,') is not a multiple of nproc_x (',nproc_y,').'
      CALL arpsstop('ERROR: dimension problem.',1)
    END IF

    nx = (nx-3)/nproc_x+3
    ny = (ny-3)/nproc_y+3

    dxinv = 1.0/dx
    dyinv = 1.0/dy

    xl = (nx-3)*dx
    yl = (ny-3)*dy

  ELSE
    WRITE(*,'(1x,a,I0)') 'ERROR: unsupported initopt = ',initopt
    CALL arpsstop('ERROR: initopt not supported.',1)
  END IF

  IF (modelopt == 2) THEN
    nx_wrf = nx-2
    ny_wrf = ny-2
    nz_wrf = nz-2
    CALL mpupdatei(nx_wrf,1)
    CALL mpupdatei(ny_wrf,1)
    CALL mpupdatei(nz_wrf,1)
    CALL mpupdatei(nt,1)
  ELSE
    CALL mpupdatei(nzsoil,1)
    CALL mpupdatei(nstyps,1)
  END IF

  ALLOCATE(x(nx),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:x')
  ALLOCATE(y(ny),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:y')
  ALLOCATE(z(nz),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:z')
  ALLOCATE(zp(nx,ny,nz),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:zp')

  ALLOCATE(xs(nx),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:xs')
  ALLOCATE(ys(ny),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:ys')
  ALLOCATE(zps(nx,ny,nz),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:zps')

  ALLOCATE(tem2d(nx,ny),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:tem2d')

  !
  !Forced to read exteranl data set always
  !
  !IF( qc_on .OR. xu_modelopt == 2) THEN
!------------------------------------------------------------------------
!
! Fill refractivity sounding with constant lapse rate
! This is only used when rfropt > 1, but for completeness and
! future upgrade (when an actual sounding made from gridded data
! would be used) this is filled-in.
!
!------------------------------------------------------------------------

    DO k=1,nzsnd
      zsnd(k)=(k-1)*dzsnd
      rfrsnd(k)=325.+rfrconst*zsnd(k)
    END DO

!------------------------------------------------------------------------
!
! ARPS grid array allocations and variable initializations.
!
!------------------------------------------------------------------------

    IF ( modelopt == 2 ) THEN

      ALLOCATE(u    (nx_wrf,ny_wrf,nz_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:u')
      ALLOCATE(v    (nx_wrf,ny_wrf,nz_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:v')
      ALLOCATE(ptprt(nx_wrf,ny_wrf,nz_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:ptprt')
      ALLOCATE(pprt (nx_wrf,ny_wrf,nz_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:pprt')
      ALLOCATE(qv   (nx_wrf,ny_wrf,nz_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:qv')

      ALLOCATE(tem3d1(nx_wrf,ny_wrf,nz_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:tem3d1')
      ALLOCATE(tem3d2(nx_wrf,ny_wrf,nz_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:tem3d2')
      ALLOCATE(tem3d3(nx_wrf,ny_wrf,nz_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:tem3d3')

      ALLOCATE(x_wrf(nx_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:x')
      ALLOCATE(y_wrf(ny_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:y')
      !ALLOCATE(z_wrf(nz_wrf),stat=istatus)
      !CALL check_alloc_status(istatus,'radremap:z')
      ALLOCATE(zp_wrf(nx_wrf,ny_wrf,nz_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:zp')

      ALLOCATE(xs_wrf(nx_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:xs')
      ALLOCATE(ys_wrf(ny_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:ys')
      ALLOCATE(zps_wrf(nx_wrf,ny_wrf,nz_wrf),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:zps')

      CALL readwrfgrd(nprocx_in,nprocy_in,                              &
                      wrf_file,nx_wrf,ny_wrf,nz_wrf,nt,                 &
                      x_wrf,y_wrf,zp_wrf,xs_wrf,ys_wrf,zps_wrf,         &
                      pprt,ptprt,u,v,tem3d3,qv,istatus)

      IF(istatus < 0) THEN
        IF (myproc == ROOT) WRITE(6,'(a,i0,/a)')                        &
                        ' Status from getwrfd = ',istatus,' STOPPING'
        CALL arpsstop('arpsstop called from radremap',1)
      END IF

      CALL lltoxy(1,1,radlat,radlon,radarx,radary)
      IF (myproc == ROOT)  print *, ' Setting radar coordinate based on WRF grid : '

      IF (myproc == ROOT) THEN
        print *, ' Radar x: ',(0.001*radarx),' km'
        print *, ' Radar y: ',(0.001*radary),' km'
        CALL flush(stdout)
      END IF

      CALL extenvprf2(nx_wrf,ny_wrf,nz_wrf,1,nzsnd,                     &
             x_wrf,y_wrf,zp_wrf,xs_wrf,ys_wrf,zps_wrf,                  &
             u,v,ptprt,pprt,qv,tem3d1,tem3d2,tem3d3,                    &
             radarx,radary,radalt,envavgr,rngmax,                       &
             zsnd,ktsnd,usnd,vsnd,rfrsnd,istatus);

      IF (myproc == ROOT) print *, ' Back from extenvprf'

      CALL mpupdatei(istatus, 1)

      IF(istatus < 0) THEN
        IF (myproc == ROOT) WRITE(6,'(a,i0,/a)')                        &
                        ' Status from extenvprf = ',istatus,' STOPPING'
        CALL arpsstop('arpsstop called from radremap',1)
      END IF

      !-----------------------------------------------------------------
      !
      ! Virtual ARPS grid
      !
      !-----------------------------------------------------------------

      !CALL print3dnc_lg(100,'zp_wrf',zp_wrf(:,:,:) ,nx_wrf,ny_wrf,nz_wrf)

      DO i = 1, nx_wrf
        ii = i + 1
        x (ii) = x_wrf(i)
      END DO
      x(1)  = x(2)-dx
      x(nx) = x(nx_wrf)+dx

      DO j = 1, ny_wrf
        jj = j + 1
        y (jj) = y_wrf(j)
      END DO
      y(1)  = y(2)-dy
      y(ny) = y(ny_wrf)+dy

      CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'V',zp_wrf,              &
                          nx,ny,nz,zp,tem3d1,istatus)

      !CALL print3dnc_lg(100,'zp',zp(:,:,:) ,nx,ny,nz)

      !DO k = 1,nz_wrf
      !  kk = k+1
      !  DO j = 1, ny_wrf
      !    jj = j + 1
      !    DO i = 1, nx_wrf
      !      ii = i + 1
      !      zp (ii,jj,kk) = zp_wrf(i,j,k)
      !    END DO
      !  END DO
      !END DO
      !
      !DO j=2,ny-1
      !  DO i=2,nx-1
      !    zp(i,j,1 )=2.0*zp(i,j,2   )-zp(i,j,3   )
      !    zp(i,j,nz)=2.0*zp(i,j,nz-1)-zp(i,j,nz-2)
      !  END DO
      !END DO
      !CALL edgfill(zp,1,nx,2,nx-1,1,ny,2,ny-1, 1,nz,1,nz)

      CALL mapinit(nx,ny)

      IF (myproc == ROOT)  print *, ' Setting radar coordinate based on ARPS grid : '

      CALL radcoord(nx,ny,nz,x,y,z,zp,xs,ys,zps,                        &
                      radlat,radlon,radarx,radary,tem3d1)

      IF (myproc == ROOT) THEN
        print *, ' Radar x: ',(0.001*radarx),' km'
        print *, ' Radar y: ',(0.001*radary),' km'
        CALL flush(stdout)
      END IF

      IF (xu_modelopt == 2) THEN
        CALL xu_dealiase_model_init(xu_modelopt,wrf_file,               &
                             radlat,radlon,rngmin,rngmax,               &
                             nx_wrf,ny_wrf,nz_wrf,x_wrf,y_wrf,u,v,zps_wrf,istatus)
      END IF

      DEALLOCATE(x_wrf, y_wrf, zp_wrf, xs_wrf, ys_wrf, zps_wrf)
      DEALLOCATE(u, v, pprt, ptprt, qv)
      DEALLOCATE(tem3d1, tem3d2, tem3d3)

    !ELSE IF ((.NOT. traditional) .AND. (initopt == 3 .AND. (inifmt == 3 .OR. inifmt == 1)) ) THEN
    ELSE    ! always read needed variables only

      ALLOCATE(u    (nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:u')
      ALLOCATE(v    (nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:v')
      ALLOCATE(ptprt(nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:ptprt')
      ALLOCATE(pprt (nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:pprt')
      ALLOCATE(qv   (nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:qv')

      ALLOCATE(tem3d1(nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:tem3d1')
      ALLOCATE(tem3d2(nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:tem3d2')
      ALLOCATE(tem3d3(nx,ny,nz),stat=istatus)
      CALL check_alloc_status(istatus,'radremap:tem3d3')

      CALL get_gridxyzzp(nx,ny,nz,inigbf,inifmt,nprocx_in,nprocy_in,    &
                         x,y,z,zp,istatus)

      CALL mapinit(nx,ny)

      IF (myproc == ROOT)  print *, ' Setting radar coordinate: '

      CALL radcoord(nx,ny,nz,x,y,z,zp,xs,ys,zps,                        &
                      radlat,radlon,radarx,radary,tem3d1)

      IF (myproc == ROOT) THEN
        print *, ' Radar x: ',(0.001*radarx),' km'
        print *, ' Radar y: ',(0.001*radary),' km'
        CALL flush(stdout)
      END IF

      CALL readuvt(nx,ny,nz,inifile,inifmt,nprocx_in,nprocy_in,         &
                   rdummy,u,v,pprt,ptprt,qv,istatus)

      IF (myproc == ROOT) WRITE(6,'(1x,a,f10.2)')                       &
        'Environmental wind averaging radius: ',envavgr

      CALL extenvprf2(nx,ny,nz,2,nzsnd,x,y,zp,xs,ys,zps,                  &
             u,v,ptprt,pprt,qv,tem3d1,tem3d2,tem3d3,                    &
             radarx,radary,radalt,envavgr,rngmax,                       &
             zsnd,ktsnd,usnd,vsnd,rfrsnd,istatus);

      IF (myproc == ROOT) print *, ' Back from extenvprf'

      CALL mpupdatei(istatus, 1)

      IF(istatus < 0) THEN
        IF (myproc == ROOT) WRITE(6,'(a,i0,/a)')                        &
                        ' Status from extenvprf = ',istatus,' STOPPING'
        CALL arpsstop('arpsstop called from 88d2arps',1)
      END IF

      IF (xu_modelopt == 2) THEN
        CALL xu_dealiase_model_init(xu_modelopt,inigbf,                 &
                                    radlat,radlon,rngmin,rngmax,        &
                                    nx,ny,nz,x,y,u,v,zps,istatus)
      END IF

      DEALLOCATE(u, v, pprt, ptprt, qv)
      DEALLOCATE(tem3d1, tem3d2, tem3d3)

    !ELSE
    !
    !  ALLOCATE(u    (nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:u')
    !  ALLOCATE(v    (nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:v')
    !  ALLOCATE(ptprt(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:ptprt')
    !  ALLOCATE(pprt (nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:pprt')
    !  ALLOCATE(qv   (nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:qv')
    !
    !  ALLOCATE(ubar(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:ubar')
    !  ALLOCATE(vbar(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:vbar')
    !  ALLOCATE(ptbar(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:ptbar')
    !  ALLOCATE(pbar(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:pbar')
    !  ALLOCATE(qvbar(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:qvbar')
    !  ALLOCATE(rhostr(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:rhostr')
    !
    !  ALLOCATE(tem2dyz(ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:tem2dyz')
    !  ALLOCATE(tem2dxz(nx,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:tem2dxz')
    !  ALLOCATE(tem2dns(nx,ny,0:nstyps),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:tem2dns')
    !
    !  ALLOCATE(trigs1(3*(nx-1)/2+1),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:trigs1')
    !  ALLOCATE(trigs2(3*(ny-1)/2+1),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:trigs2')
    !  ALLOCATE(ifax(13),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:ifax')
    !
    !  ALLOCATE(wsave1(3*(ny-1)+15),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:wsave1')
    !  ALLOCATE(wsave2(3*(nx-1)+15),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:wsave2')
    !  ALLOCATE(vwork1(nx+1,ny+1),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:vwork1')
    !  ALLOCATE(vwork2(ny,nx+1),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:vwork2')
    !
    !  ALLOCATE(qcumsrc(nx,ny,nz,5),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:qcsumsrc')
    !  ALLOCATE(prcrate(nx,ny,4),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:prcrate')
    !  ALLOCATE(exbcbuf(exbcbufsz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:exbcbuf')
    !  ALLOCATE(soiltyp(nx,ny,nstyps),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:soiltyp')
    !  ALLOCATE(stypfrct(nx,ny,nstyps),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:stypfrct')
    !  ALLOCATE(tem2dint(nx,ny),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:tem2dint')
    !
    !  ALLOCATE(tem3d1(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:tem3d1')
    !  ALLOCATE(tem3d2(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:tem3d2')
    !  ALLOCATE(tem3d3(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:tem3d3')
    !  ALLOCATE(tem3d4(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:tem3d4')
    !
    !  ALLOCATE(tem3dsoil(nx,ny,nzsoil),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:tem3dsoil')
    !  ALLOCATE(tem4dsoilns(nx,ny,nzsoil,0:nstyps),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:tem4dsoilns')
    !
    !  ALLOCATE(qscalar(nx,ny,nz,nscalar),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:qscalar')
    !
    !  ALLOCATE(j1(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:j1')
    !  ALLOCATE(j2(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:j2')
    !  ALLOCATE(j3(nx,ny,nz),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:j3')
    !  ALLOCATE(hterain(nx,ny),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:hterain')
    !  ALLOCATE(mapfct(nx,ny,8),stat=istatus)
    !  CALL check_alloc_status(istatus,'radremap:mapfct')
    !
    !  CALL set_lbcopt(1)
    !
    !  CALL initgrdvar(nx,ny,nz,nzsoil,nt,nstyps,exbcbufsz,              &
    !            x,y,z,zp,tem3dsoil,hterain,mapfct,                      &
    !            j1,j2,j3,tem3dsoil,tem3d1,tem3d1,tem3d1,tem3d1,tem3dsoil, &
    !            u,v,tem3d1,tem3d1,ptprt,pprt,qv,qscalar,tem3d1,         &
    !            tem2dyz,tem2dyz,tem2dxz,tem2dxz,                        &
    !            tem2dyz,tem2dyz,tem2dxz,tem2dxz,                        &
    !            trigs1,trigs2,ifax,ifax,                                &
    !            wsave1,wsave2,vwork1,vwork2,                            &
    !            ubar,vbar,tem3d1,ptbar,pbar,tem3d1,tem3d1,              &
    !            rhostr,tem3d1,qvbar,tem3d1,tem3d1,                      &
    !            soiltyp,stypfrct,tem2dint,tem2d,tem2d,tem2d,            &
    !            tem4dsoilns,tem4dsoilns,tem2dns,tem2d,tem2dns,          &
    !            tem3d1,qcumsrc,tem3d1,tem2dint,tem2d,                   &
    !            tem2d,tem2d,tem2d,                                      &
    !            tem2d,tem2d,prcrate,exbcbuf,                            &
    !            tem3d1,tem2d,tem2d,tem2d,tem2d,                         &
    !            tem2d,tem2d,tem2d,tem2d,                                &
    !            tem3dsoil,tem3dsoil,tem3dsoil,tem3dsoil,tem3dsoil,      &
    !            tem3d2,tem3d3,tem3d4,tem3d4,tem3d4,                     &
    !            tem3d4,tem3d4,tem3d4,tem3d4)
    !
    !  IF (myproc == ROOT) print *, ' Back from initgrdvar'
    !
    !  DEALLOCATE(trigs1, trigs2,ifax,wsave1,wsave2,vwork1,vwork2,qcumsrc)
    !  DEALLOCATE(prcrate,soiltyp,stypfrct,exbcbuf)
    !  DEALLOCATE(tem2dint,tem2dyz,tem2dxz,tem2dns)
    !  DEALLOCATE(tem3dsoil,tem4dsoilns)
    !  DEALLOCATE(qscalar)
    !
    !  IF (myproc == ROOT) print *, ' Setting radar coordinate: '
    !
    !  CALL radcoord(nx,ny,nz,x,y,z,zp,xs,ys,zps,                        &
    !                radlat,radlon,radarx,radary)
    !
    !  IF (myproc == ROOT) THEN
    !    print *, ' Radar x: ',(0.001*radarx),' km'
    !    print *, ' Radar y: ',(0.001*radary),' km'
    !  END IF
    !
    !  IF (myproc == ROOT)  &
    !    print *, ' Environmental wind averaging radius: ',envavgr
    !
    !
    !  CALL extenvprf(nx,ny,nz,nzsnd,x,y,zp,xs,ys,zps,                   &
    !             u,v,ptprt,pprt,qv,ptbar,pbar,tem3d1,tem3d2,tem3d3,     &
    !             radarx,radary,radalt,envavgr,rngmax,                   &
    !             zsnd,ktsnd,usnd,vsnd,rfrsnd,istatus)
    !
    !  print *, ' Back from extenvprf'
    !
    !  IF(istatus < 0) THEN
    !    IF (myproc == ROOT) WRITE(6,'(a,i0,/a)')                        &
    !         ' Status from extenvprf = ',istatus,' STOPPING'
    !    CALL arpsstop('arpsstop called from 88d2arps',1)
    !  END IF
    !
    !  CALL xu_dealiase_model_init(xu_modelopt,nx,ny,nz,x,y,u,v,zps,istatus)
    !
    !  DEALLOCATE(u, v, pprt, ptprt, qv)
    !  DEALLOCATE(ubar, vbar, pbar, ptbar, qvbar, rhostr)
    !  DEALLOCATE(j1, j2, j3, hterain, mapfct)
    !  DEALLOCATE(tem3d1, tem3d2, tem3d3, tem3d4)

    END IF

  !ELSE
  !
  !  ALLOCATE(j1(nx,ny,nz),stat=istatus)
  !  CALL check_alloc_status(istatus,'radremap:j1')
  !  ALLOCATE(j2(nx,ny,nz),stat=istatus)
  !  CALL check_alloc_status(istatus,'radremap:j2')
  !  ALLOCATE(j3(nx,ny,nz),stat=istatus)
  !  CALL check_alloc_status(istatus,'radremap:j3')
  !  ALLOCATE(hterain(nx,ny),stat=istatus)
  !  CALL check_alloc_status(istatus,'radremap:hterain')
  !  ALLOCATE(mapfct(nx,ny,8),stat=istatus)
  !  CALL check_alloc_status(istatus,'radremap:mapfct')
  !
  !  ALLOCATE(zpsoil(nx,ny,nzsoil),stat=istatus)
  !  CALL check_alloc_status(istatus,'radremap:zpsoil')
  !  ALLOCATE(j3soil(nx,ny,nzsoil),stat=istatus)
  !  CALL check_alloc_status(istatus,'radremap:j3soiloil')
  !  ALLOCATE(j3soilinv(nx,ny,nzsoil),stat=istatus)
  !  CALL check_alloc_status(istatus,'radremap:j3soilinv')
  !
  !  ALLOCATE(tem1d1(nz),stat=istatus)
  !  CALL check_alloc_status(istatus,'radremap:tem1d1')
  !  ALLOCATE(tem1d2(nz),stat=istatus)
  !  CALL check_alloc_status(istatus,'radremap:tem1d2')
  !
  !  ALLOCATE(tem3d1(nx,ny,nz),stat=istatus)
  !  CALL check_alloc_status(istatus,'radremap:tem3d1')
  !
  !  IF (myproc == ROOT) print *, ' Setting ARPS grid coordinates ...'
  !  CALL inigrd(nx,ny,nz,nzsoil,x,y,z,zp,zpsoil,                        &
  !              hterain,mapfct,j1,j2,j3,j3soil,j3soilinv,               &
  !              tem1d1,tem1d2,tem3d1)
  !
  !  IF (myproc == ROOT) print *, ' Setting radar coordinate...'
  !
  !  CALL radcoord(nx,ny,nz,x,y,z,zp,xs,ys,zps,                          &
  !                radlat,radlon,radarx,radary)
  !
  !  DEALLOCATE(zpsoil, j3soil, j3soilinv)
  !  DEALLOCATE(j1, j2, j3, hterain, mapfct)
  !
  !  DEALLOCATE(tem3d1)
  !  DEALLOCATE(tem1d1, tem1d2)
  !
  !END IF

!-----------------------------------------------------------------------
!
! Allocation of analysis arrays
!
!-----------------------------------------------------------------------

  nxny=nx*ny
  ALLOCATE(icolp(nxny),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:icolp')
  ALLOCATE(jcolp(nxny),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:jcolp')
  ALLOCATE(xcolp(nxny),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:xcolp')
  ALLOCATE(ycolp(nxny),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:ycolp')
  ALLOCATE(zcolp(nz,nxny),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:zcolp')
  ALLOCATE(havdat(nx,ny),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:havdat')

  ALLOCATE(kntbin(nsort),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:kntbin')

  ALLOCATE(colref(nz,nxny),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:colref')
  IF (dualpproc) THEN
    ALLOCATE(colrhv(nz,nxny),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:colrhv')
    ALLOCATE(colzdr(nz,nxny),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:colzdr')
    ALLOCATE(colkdp(nz,nxny),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:colkdp')
  END IF
  ALLOCATE(colvel(nz,nxny),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:colvel')
  ALLOCATE(colnyq(nz,nxny),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:colnyq')
  ALLOCATE(coltim(nz,nxny),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:coltim')

!------------------------------------------------------------------------
!
! Radar data array allocations
!
!------------------------------------------------------------------------

  ALLOCATE(rtem(maxgate,maxazim),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:rtem')
  !ALLOCATE(gcfst(maxazim),stat=istatus)
  !CALL check_alloc_status(istatus,'radremap:gcfst')
  !ALLOCATE(istrgate(maxazim),stat=istatus)
  !CALL check_alloc_status(istatus,'radremap:istrgate')

  ALLOCATE(kntrgat(maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:kntrgat')
  ALLOCATE(kntrazm(maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:kntrazm')

  ALLOCATE(kntvgat(maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:kntvgat')
  ALLOCATE(kntvazm(maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:kntvazm')

  ALLOCATE(vnyqvol(maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:vnyqvol')
  ALLOCATE(timevolr(maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:timevolr')
  ALLOCATE(timevolv(maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:timevolv')

  ALLOCATE(rngrvol(maxrgate,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:rngrvol')
  ALLOCATE(azmrvol(maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:azmrvol')
  ALLOCATE(elvrvol(maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:elvrvol')
  ALLOCATE(elvmnrvol(maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:elvmnrvol')
  ALLOCATE(refvol(maxrgate,maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:refvol')
  IF( dualpproc ) THEN
    ALLOCATE(rhvvol(maxrgate,maxazim,maxelev),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:rhvvol')
    ALLOCATE(zdrvol(maxrgate,maxazim,maxelev),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:zdrvol')
    ALLOCATE(kdpvol(maxrgate,maxazim,maxelev),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:kdpvol')
  ELSE
    ALLOCATE(rhvvol(1,1,1),stat=istatus)
    ALLOCATE(zdrvol(1,1,1),stat=istatus)
    ALLOCATE(kdpvol(1,1,1),stat=istatus)
  END IF

  ALLOCATE(rngvvol(maxvgate,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:rngvvol')
  ALLOCATE(azmvvol(maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:azmvvol')
  ALLOCATE(elvvvol(maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:elvvvol')
  ALLOCATE(elvmnvvol(maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:elvmnvvol')
  ALLOCATE(velvol(maxvgate,maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:velvol')
  ALLOCATE(elvmean(maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:elvmean')

  ALLOCATE(ngateref(maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:ngateref')
  ALLOCATE(ngatevel(maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:ngatevel')

  ALLOCATE(rxvol(maxgate,maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:rxvol')
  ALLOCATE(ryvol(maxgate,maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:ryvol')
  ALLOCATE(rzvol(maxgate,maxazim,maxelev),stat=istatus)
  CALL check_alloc_status(istatus,'radremap:rzvol')
!
!------------------------------------------------------------------------
!
!  Allocate arrays for grid-tilt writing for EnKF
!
!------------------------------------------------------------------------
!
  IF( wrtgrdtilt ) THEN
    ALLOCATE(grdtilthighref(nx,ny,maxelev),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:grdtilthighref')
    ALLOCATE(grdrangeref(nx,ny,maxelev),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:grdrangeref')
    ALLOCATE(grdslrref(nx,ny),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:grdslrref')
    ALLOCATE(grdazmref(nx,ny),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:grdazmref')
    ALLOCATE(grdtiltdata(nx,ny,maxelev,grdtiltnumvar),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:grdtiltdata')
    ALLOCATE(elevmeanref(maxelev),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:elevmeanref')

    ALLOCATE(tilttimeref(maxelev),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:tilttimeref')
    ALLOCATE(tilttimevel(maxelev),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:tilttimevel')

    ALLOCATE(grdtilthighvel(nx,ny,maxelev),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:grdtilthighvel')
    ALLOCATE(grdrangevel(nx,ny,maxelev),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:grdrangevel')
    ALLOCATE(grdslrvel(nx,ny),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:grdslrvel')
    ALLOCATE(grdazmvel(nx,ny),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:grdazmvel')
    ALLOCATE(elevmeanvel(maxelev),stat=istatus)
    CALL check_alloc_status(istatus,'radremap:elevmeanvel')
  END IF
!
!------------------------------------------------------------------------
!
!     Initialize 3-d radar data arrays
!
!------------------------------------------------------------------------
!
  !print *, ' calling rmpinit'
  CALL rmpinit(nx,ny,nz,maxrgate,maxvgate,maxazim,maxelev,dualpproc,    &
          kntrgat,kntrazm,kntrelv,                                      &
          kntvgat,kntvazm,kntvelv,                                      &
          vnyqvol,timevolr,timevolv,                                    &
          rngrvol,azmrvol,elvrvol,elvmnrvol,refvol,rhvvol,zdrvol,kdpvol,&
          rngvvol,azmvvol,elvvvol,elvmnvvol,velvol,                     &
          colvel,colref,colnyq,coltim)

  IF (fntime) THEN
    iiyear = kmyear
    iimon  = kmmon
    iiday  = kmday
    iihr   = kmhr
    iimin  = kmmin
    iisec  = kmsec
  ELSE
    iiyear = iyear
    iimon  = imon
    iiday  = iday
    iihr   = ihr
    iimin  = imin
    iisec  = isec
  END IF

  IF (myproc == ROOT) THEN
    CALL read_88d_tilts(full_fname,volflag,radlat,radlon,radalt,ivcp,   &
            iiyear,iimon,iiday,iihr,iimin,iisec,                        &
            dualpproc,dualpdata,qc_on,unfdiag,rfropt,wrtsolo,wrtsoloqc, &
            maxelev,maxrgate,maxvgate,maxazim,itimfrst,                 &
            nzsnd,zsnd,usnd,vsnd,rfrsnd,                                &
            icount,ngateref, ngatevel,                                  &
            rfrst_ref, gtspc_ref, rfrst_vel, gtspc_vel,                 &
            kntrgat,kntrazm,kntrelv,                                    &
            timevolr,rngrvol,azmrvol,elvrvol,elvmnrvol,                 &
            refvol,rhvvol,zdrvol,kdpvol,                                &
            kntvgat,kntvazm,kntvelv,                                    &
            timevolv,rngvvol,azmvvol,elvvvol,elvmnvvol,                 &
            vnyqvol,velvol,                                             &
            misval, rtem, istatus)

    WRITE(6,'(1x,a,i0,a,/)') '==== finished tilt reading, istatus (',istatus, ') ===='


    IF(velproc .OR. vad > 0 .AND. qc_on) THEN
      WRITE(6,'(a)') ' Calling quadunf for velocity'
      CALL quadunf(maxvgate,maxazim,maxelev,nzsnd,nsort,                &
                   velchek,velmedl,veldazl,iordunf,rfropt,              &
                   sortmin,dsort,                                       &
                   kntvgat,kntvazm,kntvelv,                             &
                   radarx,radary,radalt,dazim,                          &
                   rngmin,rngmax,zsnd,rfrsnd,                           &
                   rngvvol,azmvvol,elvvvol,elvmnvvol,vnyqvol,           &
                   kntbin,rxvol,ryvol,rzvol,velvol,                     &
                   istatus)
      IF( unfdiag ) THEN
        varid='rdvelz'
        DO kelv=1,kntvelv
          WRITE(6,'(3a,f7.1)')                                          &
           ' Calling wrtvel varid:',varid,' elev: ',elvmnvvol(kelv)
          iangle=100.*elvmnvvol(kelv)
          gtspc_vel=NINT(rngvvol(2,kelv)-rngvvol(1,kelv))
          rfrst_vel=NINT(rngvvol(1,kelv))
          print *, ' rfrst,gtspace: ',rfrst_vel,gtspc_vel
          ng_vel=kntvgat(1,kelv)
          DO j =1,kntvazm(kelv)
            ng_vel=max(ng_vel,kntvgat(j,kelv))
          END DO
          print *, ' n gates: ',ng_vel
          vnyquist=vnyqvol(1,kelv)
          CALL wrtvel(iangle,kelv,varid,                                &
                    iiyear,iimon,iiday,iihr,iimin,iisec,                &
                    gtspc_vel,rfrst_vel,vnyquist,                       &
                    radname,radlat,radlon,radalt,                       &
                    maxvgate,maxazim,ng_vel,kntvazm(kelv),              &
                    azmvvol(1,kelv),elvvvol(1,kelv),                    &
                    velvol(1,1,kelv))
        END DO
      END IF

    END IF

!-----------------------------------------------------------------------
!
! Anomalous propagation check and clutter check
!
!-----------------------------------------------------------------------

    IF (icount > 0) THEN

      IF( qc_on ) THEN

        write(6, *) ' Calling apdetect ',gtspc_ref,gtspc_vel
        CALL apdetect(maxrgate,maxvgate,maxazim,maxelev,                &
                      kntrgat,kntrazm,kntrelv,                          &
                      kntvgat,kntvazm,kntvelv,                          &
                      refchek,velchek,                                  &
                      gtspc_ref,gtspc_vel,                              &
                      winszrad,winszazim,ivcp,gclopt,gcvrlim,           &
                      rngrvol,azmrvol,elvrvol,                          &
                      rngvvol,azmvvol,elvvvol,                          &
                      refvol,velvol,rtem,                               &
                      istatus)
      END IF

      tempstr = dirname
      CALL get_output_dirname(0,tempstr,0,1,dirname,istatus)
      ldirnam = LEN_TRIM(dirname)

!-----------------------------------------------------------------------
!
! Dealiasing, Dr. Xu and Kang
!
!-----------------------------------------------------------------------

      IF (dealiase) THEN
        IF( unfdiagopt >= 2 ) THEN
          full_fname = ' '
          CALL mkradfnm(101,dirname,ldirnam,radname,iiyear,iimon,iiday, &
                        iihr, iimin, iisec, full_fname, ilen)
          WRITE(stdout,'(1x,2a)') 'Output filename for this volume before dealiasing: ',  &
                                  TRIM(full_fname)
          CALL wrttilts(full_fname,maxvgate,maxrgate,maxazim,maxelev,   &
                     rngvvol,azmvvol,elvvvol,velvol,                    &
                     rngrvol,azmrvol,elvrvol,refvol,                    &
                     radalt,radlat,radlon,                              &
                     iiyear,imon,iiday,iihr,iimin,iisec )
        END IF

        IF (xu_modelopt == 1) THEN
          maxrng = MAXVAL(rngvvol)
          CALL xu_dealiase_model_init(xu_modelopt,xu_modelfile,         &
                     radlat,radlon,rngmin,maxrng,                       &
                     nx,ny,nz,x,y,u,v,zps,istatus)
        END IF

        CALL XU_dealiase_main(radname,radlat,radlon,radalt,ivcp,        &
                              maxrgate,maxvgate,maxazim,maxelev,kntvelv,&
                              rfrst_ref,rfrst_vel,gtspc_ref,gtspc_vel,  &
                              kntrgat,kntvgat,kntrazm,kntvazm,          &
                              azmrvol,azmvvol,elvmnrvol,elvmnvvol,      &
                              timeset, itimfrst,timevolv,               &
                              vnyqvol, rngvvol,                         &
                              refvol,velvol,vadkdim,(unfdiagopt >= 2 ),misval,istatus)

      END IF

      IF (lvldbg == 102) THEN
        DO k = 1,kntvelv
          imaxrng = MAXVAL(kntvgat(:,k))

          CALL XU_WRITE_RADAR_TILT('qc',maxvgate,maxazim,radname,       &
                        radlat,radlon,radalt,rfrst_vel,gtspc_vel,ivcp,  &
                        iiyear,iimon,iiday,iihr, iimin, iisec,          &
                        elvmnvvol(k),kntvazm(k),imaxrng,vnyqvol(1,k),   &
                        azmvvol(:,k),velvol(:,:,k))
        END DO
      END IF

!------------------------------------------------------------------------
!
!   Write tilt data
!
!------------------------------------------------------------------------

      IF( wrttilt ) THEN
        full_fname = ' '
        CALL mkradfnm(102,dirname,ldirnam,radname,iiyear,iimon,iiday,   &
                      iihr, iimin, iisec, full_fname, ilen)
        WRITE(stdout,'(1x,2a)') 'Output filename for this volume after dealising: ',    &
                                TRIM(full_fname)

        CALL wrttilts(full_fname,maxvgate,maxrgate,maxazim,maxelev,     &
                   rngvvol,azmvvol,elvvvol,velvol,                      &
                   rngrvol,azmrvol,elvrvol,refvol,                      &
                   radalt,radlat,radlon,                                &
                   iiyear,imon,iiday,iihr,iimin,iisec )
      END IF

!-----------------------------------------------------------------------
!
!   VAD Wind Processing
!
!-----------------------------------------------------------------------

      IF( vad > 0 ) THEN

        IF (vad == 1) THEN
          ALLOCATE(vadhgt(maxelev),stat=istatus)
          CALL check_alloc_status(istatus,'radremap:vadhgt')
          ALLOCATE(vaddir(maxelev),stat=istatus)
          CALL check_alloc_status(istatus,'radremap:vaddir')
          ALLOCATE(vadspd(maxelev),stat=istatus)
          CALL check_alloc_status(istatus,'radremap:vadspd')

          CALL vadvol(maxvgate,maxazim,maxelev,                         &
                      radalt,velchek,vadradius,vadwidth,vadminknt,      &
                      kntvgat,kntvazm,kntvelv,                          &
                      rngvvol,azmvvol,elvvvol,velvol,                   &
                      vadhgt,vaddir,vadspd)
          vadkdim = maxelev
        ELSE IF (vad == 4) THEN
          ALLOCATE(vadhgt(vadkdim),stat=istatus)
          CALL check_alloc_status(istatus,'radremap:vadhgt')
          ALLOCATE(vaddir(vadkdim),stat=istatus)
          CALL check_alloc_status(istatus,'radremap:vaddir')
          ALLOCATE(vadspd(vadkdim),stat=istatus)
          CALL check_alloc_status(istatus,'radremap:vadspd')

          CALL Xu_dealiase_vadout(vadkdim,vadhgt,vaddir,vadspd,misval,istatus)

        ELSE
          ALLOCATE(vadhgt(nz-2),stat=istatus)
          CALL check_alloc_status(istatus,'radremap:vadhgt')
          ALLOCATE(vaddir(nz-2),stat=istatus)
          CALL check_alloc_status(istatus,'radremap:vaddir')
          ALLOCATE(vadspd(nz-2),stat=istatus)
          CALL check_alloc_status(istatus,'radremap:vadspd')

          ALLOCATE(zk(nz-2),stat=istatus)
          CALL check_alloc_status(istatus,'radremap:zk')
          DO k = 2,nz-1
            zk(k-1) = zp(2,2,k)
          END DO

          CALL gvadvol(vad,maxvgate,maxazim,maxelev,nz-2,zk,            &
                      radalt,velchek,vadradius,vadwidth,vadminknt,      &
                      kntvgat,kntvazm,kntvelv,                          &
                      rngvvol,azmvvol,elvvvol,velvol,                   &
                      vadhgt,vaddir,vadspd,misval,istatus)
          vadkdim = nz-3

          DEALLOCATE(zk)
        END IF

        CALL mkvadfnm(dirname,radname,                                  &
                      iiyear,iimon,iiday,iihr,iimin,iisec,              &
                      vad_fname)
        WRITE(stdout,'(a,a)')                                           &
              ' Filename for VAD output: ',TRIM(vad_fname)
        CALL flush(stdout)

        CALL wtvadprf(vad,vadkdim,vad_fname,radname,                    &
                      vadsrc,radlat,radlon,radalt,vadhgt,vaddir,vadspd)

        DEALLOCATE(vadhgt,vaddir,vadspd)

      END IF

    END IF  ! icount > 0

  END IF ! IF ROOT

!------------------------------------------------------------------------
!
! Create filename for output of remapped radar variables.
!
!------------------------------------------------------------------------

  IF (remap_on) THEN

    CALL mpupdatei(icount,1)
    CALL mpupdatel(dualpdata,1)

    IF (icount > 0) THEN

!------------------------------------------------------------------------
!
!   Call remapping routines
!
!------------------------------------------------------------------------

      nyqset=0
      timeset=1;  IF ( velproc ) timeset=0
      vardump=0;  IF ( ref3d   ) vardump=1
      ivar=1
      curtim = outtime

      CALL mpupdatei(kntrgat,maxazim*maxelev)
      CALL mpupdatei(kntrazm,maxelev)
      CALL mpupdatei(kntrelv,1)

      CALL mpupdater(rngrvol,maxrgate*maxelev)
      CALL mpupdater(azmrvol,maxazim*maxelev)
      CALL mpupdater(elvrvol,maxazim*maxelev)
      CALL mpupdater(refvol,maxrgate*maxazim*maxelev)
      CALL mpupdatei(timevolr,maxazim*maxelev)
      CALL mpupdater(vnyqvol,maxazim*maxelev)

      IF(velproc) THEN
        CALL mpupdatei(kntvgat,maxazim*maxelev)
        CALL mpupdatei(kntvazm,maxelev)
        CALL mpupdatei(kntvelv,1)

        CALL mpupdater(rngvvol,maxvgate*maxelev)
        CALL mpupdater(azmvvol,maxazim*maxelev)
        CALL mpupdater(elvvvol,maxazim*maxelev)
        CALL mpupdater(velvol,maxvgate*maxazim*maxelev)
        CALL mpupdatei(timevolv,maxazim*maxelev)
        CALL mpupdater(vnyqvol,maxazim*maxelev)
      END IF

      IF(dualpproc) THEN
        CALL mpupdater(rhvvol,maxrgate*maxazim*maxelev)
        CALL mpupdater(zdrvol,maxrgate*maxazim*maxelev)
        CALL mpupdater(kdpvol,maxrgate*maxazim*maxelev)
      END IF
!
!     Set-up arrays needed for remapping
!
      IF (myproc == ROOT) WRITE(stdout,'(a)') ' Calling rmpsetup...'
      CALL rmpsetup(maxrgate,maxvgate,maxgate,maxazim,maxelev,          &
                      nx,ny,nxny,nz,nzsnd,                              &
                      rfropt,refchek,velchek,bmwidth,velproc,           &
                      kntrgat,kntrazm,kntrelv,                          &
                      kntvgat,kntvazm,kntvelv,                          &
                      radlat,radlon,radarx,radary,radalt,               &
                      dazim,rngmin,rngmax,                              &
                      rngrvol,azmrvol,elvrvol,                          &
                      rngvvol,azmvvol,elvvvol,                          &
                      refvol,velvol,rxvol,ryvol,rzvol,                  &
                      xs,ys,zps,zsnd,rfrsnd,ncolp,ncoltot,              &
                      havdat,icolp,jcolp,xcolp,ycolp,zcolp,misval,istatus)

      IF (myproc == ROOT) WRITE(stdout,'(a,i12)')                       &
                                ' Back from rmpsetup, ncoltot = ',ncoltot

      WRITE(stdout,'(a,i6,a,i12)')' myproc: ',myproc,', ncolp = ',ncolp

      DEALLOCATE(havdat)

      IF (nprocs > 1 ) THEN
        CALL distrallcol(nxny,nz,ncolp,ncoltot,                         &
                         icolp,jcolp,xcolp,ycolp,zcolp,lvldbg,istatus)
      END IF
!
!  Call remap for reflectivity.
!  Note here that the rxvol,ryvol and rzvol arrays have previously been
!  set for the reflectivity gates.
!
      IF (myproc == ROOT) THEN
        WRITE(stdout,'(a)') ' Calling remap3dcol for reflectivity '
        WRITE(stdout,'(a,i6)') ' Reflectivity tilt count:',kntrelv
      END IF
      varfill=fillref
      CALL remap3dcol(maxrgate,maxgate,maxazim,maxelev,nxny,nz,         &
                    nzsnd,nsort,ncolp,                                  &
                    varfill,ivar,nyqset,timeset,rfropt,                 &
                    refchek,refmiss,bmwidth,refmedl,refdazl,iordref,    &
                    sortmin,dsort,                                      &
                    kntrgat,kntrazm,kntrelv,                            &
                    radlat,radlon,radarx,radary,radalt,dazim,           &
                    rngmin,rngmax,                                      &
                    rngrvol,azmrvol,elvrvol,                            &
                    refvol,timevolr,vnyqvol,rxvol,ryvol,rzvol,          &
                    zsnd,rfrsnd,kntbin,elvmean,                         &
                    xcolp,ycolp,zcolp,                                  &
                    colref,coltim,colnyq,misval,istatus)

      DO kelv=1,kntrelv
        refelvmin=min(elvmean(kelv),refelvmin)
        refelvmax=max(elvmean(kelv),refelvmax)
      END DO

      IF(myproc == ROOT) print *, ' outtime: ',outtime

      IF ( ref3d ) CALL wrtcol2grd(nx,ny,nz,nxny,ncolp,                 &
                    colref,icolp,jcolp,                                 &
                    refid,refname,refunits,refmiss,outtime,runname,     &
                    dirname,dmpfmt,hdfcompr,istatus)

      IF ( dualpdata .AND. dualpproc ) THEN
        IF( rhv3d .OR. dualpout > 0 ) THEN
          IF (myproc == ROOT) WRITE(stdout,'(/a)') ' Calling remap3dcol for Rho-HV'
          ivar=4
          varfill=0
          CALL remap3dcol(maxrgate,maxgate,maxazim,maxelev,nxny,nz,     &
                    nzsnd,nsort,ncolp,                                  &
                    varfill,ivar,nyqset,timeset,rfropt,                 &
                    rhvchek,rhvmiss,bmwidth,rhvmedl,refdazl,iordref,    &
                    sortmin,dsort,                                      &
                    kntrgat,kntrazm,kntrelv,                            &
                    radlat,radlon,radarx,radary,radalt,dazim,           &
                    rngmin,rngmax,                                      &
                    rngrvol,azmrvol,elvrvol,                            &
                    rhvvol,timevolr,vnyqvol,rxvol,ryvol,rzvol,          &
                    zsnd,rfrsnd,kntbin,elvmean,                         &
                    xcolp,ycolp,zcolp,                                  &
                    colrhv,coltim,colnyq,misval,istatus)

          IF ( rhv3d) CALL wrtcol2grd(nx,ny,nz,nxny,ncolp,              &
                        colrhv,icolp,jcolp,                             &
                        rhvid,rhvname,rhvunits,rhvmiss,outtime,runname, &
                        dirname,dmpfmt,hdfcompr,istatus)
        END IF
        IF( zdr3d .OR. dualpout > 0 ) THEN
          IF (myproc == ROOT) WRITE(stdout,'(/a)') ' Calling remap3dcol for Zdr'
          ivar=5
          varfill=0
          CALL remap3dcol(maxrgate,maxgate,maxazim,maxelev,nxny,nz,     &
                    nzsnd,nsort,ncolp,                                  &
                    varfill,ivar,nyqset,timeset,rfropt,                 &
                    zdrchek,zdrmiss,bmwidth,zdrmedl,refdazl,iordref,    &
                    sortmin,dsort,                                      &
                    kntrgat,kntrazm,kntrelv,                            &
                    radlat,radlon,radarx,radary,radalt,dazim,           &
                    rngmin,rngmax,                                      &
                    rngrvol,azmrvol,elvrvol,                            &
                    zdrvol,timevolr,vnyqvol,rxvol,ryvol,rzvol,          &
                    zsnd,rfrsnd,kntbin,elvmean,                         &
                    xcolp,ycolp,zcolp,                                  &
                    colzdr,coltim,colnyq,misval,istatus)

          IF ( zdr3d) CALL wrtcol2grd(nx,ny,nz,nxny,ncolp,              &
                        colzdr,icolp,jcolp,                             &
                        zdrid,zdrname,zdrunits,zdrmiss,outtime,runname, &
                        dirname,dmpfmt,hdfcompr,istatus)
        END IF
        IF( kdp3d .OR. dualpout > 0 ) THEN
          IF (myproc == ROOT) WRITE(stdout,'(/a)') ' Calling remap3dcol for Kdp'
          ivar=7
          varfill=0
          CALL remap3dcol(maxrgate,maxgate,maxazim,maxelev,nxny,nz,     &
                    nzsnd,nsort,ncolp,                                  &
                    varfill,ivar,nyqset,timeset,rfropt,                 &
                    kdpchek,kdpmiss,bmwidth,kdpmedl,refdazl,iordref,    &
                    sortmin,dsort,                                      &
                    kntrgat,kntrazm,kntrelv,                            &
                    radlat,radlon,radarx,radary,radalt,dazim,           &
                    rngmin,rngmax,                                      &
                    rngrvol,azmrvol,elvrvol,                            &
                    kdpvol,timevolr,vnyqvol,rxvol,ryvol,rzvol,          &
                    zsnd,rfrsnd,kntbin,elvmean,                         &
                    xcolp,ycolp,zcolp,                                  &
                    colkdp,coltim,colnyq,misval,istatus)

          IF ( kdp3d) CALL wrtcol2grd(nx,ny,nz,nxny,ncolp,              &
                        colkdp,icolp,jcolp,                             &
                        kdpid,kdpname,kdpunits,kdpmiss,outtime,runname, &
                        dirname,dmpfmt,hdfcompr,istatus)
        END IF
      END IF  ! dualpproc

      IF ( wrtgrdtilt ) THEN
        IF (myproc == ROOT) WRITE(stdout,'(/a)')                        &
                            ' Calling remap2dcts for reflectivity'
        CALL flush(stdout)
        CALL remap2dcts(maxrgate,maxazim,maxelev,nx,ny,nz,nzsnd,        &
                 kntrazm,kntrelv,                                       &
                 radlat,radlon,radarx,radary,radalt,dazim,              &
                 rngmin,rngmax,itimfrst,                                &
                 rngrvol,azmrvol,elvrvol,                               &
                 refvol,timevolr,                                       &
                 xs,ys,zps,zsnd,rfrsnd,                                 &
                 grdtilthighref,grdrangeref,grdslrref,grdazmref,        &
                 grdtiltdata(:,:,:,2),tilttimeref,elevmeanref,ngateref, &
                 misval,istatus)
        IF (myproc == ROOT) WRITE(stdout,'(a)') ' Back from remap2dts'
        CALL flush(stdout)
      END IF

      IF ( ref2d ) THEN
        vardump = 1
        ivar    = 1
        varid   = 'refl2d'
        varname = 'Low-level reflect'
        IF (myproc == ROOT) THEN
          WRITE(stdout,'(/a)') ' Calling remap2d for reflectivity '
        END IF
        CALL remap2d(maxrgate,maxazim,maxelev,nx,ny,nzsnd,nsort,        &
                     vardump,ivar,rfropt,varid,varname,refunits,        &
                     dmpfmt,hdf4cmpr,                                   &
                     refchek,refmiss,refmedl,refdazl,iordref,           &
                     sortmin,dsort,                                     &
                     kntrgat,kntrazm,kntrelv,                           &
                     radlat,radlon,radarx,radary,radalt,dazim,          &
                     rngmin,rngmax,rngrvol,azmrvol,elvrvol,             &
                     refvol,rxvol,ryvol,xs,ys,zsnd,rfrsnd,kntbin,       &
                     tem2d,misval,istatus)
      END IF

      IF ( zdr2d ) THEN
        IF( dualpdata ) THEN
          vardump = 1
          ivar    = 1
          varid   = 'zdr_2d'
          varname = 'Low-level Zdr'
          IF (myproc == ROOT) WRITE(stdout,'(/a)') ' Calling remap2d for Zdr '
          CALL remap2d(maxrgate,maxazim,maxelev,nx,ny,nzsnd,nsort,      &
                     vardump,ivar,rfropt,varid,varname,zdrunits,        &
                     dmpfmt,hdf4cmpr,                                   &
                     zdrchek,zdrmiss,zdrmedl,refdazl,iordref,           &
                     sortmin,dsort,                                     &
                     kntrgat,kntrazm,kntrelv,                           &
                     radlat,radlon,radarx,radary,radalt,dazim,          &
                     rngmin,rngmax,rngrvol,azmrvol,elvrvol,             &
                     zdrvol,rxvol,ryvol,xs,ys,zsnd,rfrsnd,kntbin,       &
                     tem2d,misval,istatus)
        ELSE
          IF (myproc == ROOT) WRITE(stdout,'(1x,a)')                    &
                  'Dual-pol data unavailable, skipping 2D Zdr'
        END IF
      END IF

      IF ( kdp2d ) THEN
        IF( dualpdata ) THEN
          vardump = 1
          ivar    = 1
          varid   = 'kdp_2d'
          varname = 'Low-level Kdp'
          IF (myproc == ROOT) WRITE(stdout,'(/a)') ' Calling remap2d for Kdp '
          CALL remap2d(maxrgate,maxazim,maxelev,nx,ny,nzsnd,nsort,      &
                     vardump,ivar,rfropt,varid,varname,refunits,        &
                     dmpfmt,hdf4cmpr,                                   &
                     refchek,refmiss,refmedl,refdazl,iordref,           &
                     sortmin,dsort,                                     &
                     kntrgat,kntrazm,kntrelv,                           &
                     radlat,radlon,radarx,radary,radalt,dazim,          &
                     rngmin,rngmax,rngrvol,azmrvol,elvrvol,             &
                     kdpvol,rxvol,ryvol,xs,ys,zsnd,rfrsnd,kntbin,       &
                     tem2d,misval,istatus)
        ELSE
          IF (myproc == ROOT) WRITE(stdout,'(1x,a)')                    &
                  'Dual-pol data unavailable, skipping 2D Kdp'
        END IF
      END IF

      IF ( rhv2d ) THEN
        IF( dualpdata ) THEN
          vardump = 1
          ivar    = 1
          varid   = 'rhv_2d'
          varname = 'Low-level correlation'
          IF (myproc == ROOT) WRITE(stdout,'(/a)') ' Calling remap2d for Rho-HV '
          CALL remap2d(maxrgate,maxazim,maxelev,nx,ny,nzsnd,nsort,      &
                     vardump,ivar,rfropt,varid,varname,refunits,        &
                     dmpfmt,hdf4cmpr,                                   &
                     rhvchek,rhvmiss,rhvmedl,refdazl,iordref,           &
                     sortmin,dsort,                                     &
                     kntrgat,kntrazm,kntrelv,                           &
                     radlat,radlon,radarx,radary,radalt,dazim,          &
                     rngmin,rngmax,rngrvol,azmrvol,elvrvol,             &
                     rhvvol,rxvol,ryvol,xs,ys,zsnd,rfrsnd,kntbin,       &
                     tem2d,misval,istatus)
        ELSE
          IF (myproc == ROOT) WRITE(stdout,'(1x,a)')                    &
                  'Dual-pol data unavailable, skipping 2D Rho-HV'
        END IF
      END IF

      IF ( velproc ) THEN
        nyqset  = 1
        timeset = 1
        vardump = 0
        IF(vel3d) vardump = 1
        varfill = 0
        ivar    = 2

        CALL rgatexyz(maxvgate,maxgate,maxazim,maxelev,nzsnd,           &
                      rfropt,lvldbg,                                    &
                      kntvgat,kntvazm,kntvelv,                          &
                      radlat,radlon,radarx,radary,radalt,               &
                      rngvvol,azmvvol,elvvvol,                          &
                      zsnd,rfrsnd,                                      &
                      rxvol,ryvol,rzvol,istatus)

        IF (myproc == ROOT) THEN
          WRITE(stdout,'(/a)') ' Calling remap3dcol for velocity '
          WRITE(stdout,'(a,i6)') ' Velocity tilt count:',kntvelv
        END IF
        CALL remap3dcol(maxvgate,maxgate,maxazim,maxelev,nxny,nz,       &
                    nzsnd,nsort,ncolp,                                  &
                    varfill,ivar,nyqset,timeset,rfropt,                 &
                    velchek,velmiss,bmwidth,velmedl,veldazl,iordvel,    &
                    sortmin,dsort,                                      &
                    kntvgat,kntvazm,kntvelv,                            &
                    radlat,radlon,radarx,radary,radalt,dazim,           &
                    rngmin,rngmax,                                      &
                    rngvvol,azmvvol,elvvvol,                            &
                    velvol,timevolv,vnyqvol,rxvol,ryvol,rzvol,          &
                    zsnd,rfrsnd,kntbin,elvmean,                         &
                    xcolp,ycolp,zcolp,                                  &
                    colvel,coltim,colnyq,misval,istatus)

        IF( vel3d ) CALL wrtcol2grd(nx,ny,nz,nxny,ncolp,                &
                        colvel,icolp,jcolp,                             &
                        velid,velname,velunits,velmiss,outtime,runname, &
                        dirname,dmpfmt,hdfcompr,istatus)

      END IF

      IF ( wrtgrdtilt ) THEN

        IF (myproc == ROOT) WRITE(stdout,'(/a)')                        &
                            ' Calling remap2dcts for velocity'
        CALL flush(stdout)
        CALL remap2dcts(maxvgate,maxazim,maxelev,nx,ny,nz,nzsnd,        &
               kntvazm,kntvelv,                                         &
               radlat,radlon,radarx,radary,radalt,dazim,                &
               rngmin,rngmax,itimfrst,                                  &
               rngvvol,azmvvol,elvvvol,                                 &
               velvol,timevolv,                                         &
               xs,ys,zps,zsnd,rfrsnd,                                   &
               grdtilthighvel,grdrangevel,grdslrvel,grdazmvel,          &
               grdtiltdata(:,:,:,1),tilttimevel,elevmeanvel,ngatevel,   &
               misval,istatus)
        IF (myproc == ROOT) WRITE(stdout,'(a)') ' Back from remap2dts'
        CALL flush(stdout)
      END IF

      IF ( vel2d ) THEN
        vardump  = 1
        ivar     = 2
        varid    = 'radv2d'
        varname  = 'Low-level Velocity'
        varunits = 'm/s'
        IF (myproc == ROOT) THEN
          WRITE(stdout,'(a)') ' Calling remap2d for velocity '
        END IF
        CALL remap2d(maxvgate,maxazim,maxelev,nx,ny,nzsnd,nsort,        &
                     vardump,ivar,rfropt,varid,varname,varunits,        &
                     dmpfmt,hdf4cmpr,                                   &
                     velchek,velmiss,velmedl,veldazl,iordvel,           &
                     sortmin,dsort,                                     &
                     kntvgat,kntvazm,kntvelv,                           &
                     radlat,radlon,radarx,radary,radalt,dazim,          &
                     rngmin,rngmax,rngvvol,azmvvol,elvvvol,             &
                     velvol,rxvol,ryvol,xs,ys,zsnd,rfrsnd,kntbin,       &
                     tem2d,misval,istatus)
      END IF

!------------------------------------------------------------------------
!
!   Create radar column data filename and write file.
!
!------------------------------------------------------------------------

      ldirnam=LEN_TRIM(dirname)
      full_fname = ' '
      CALL mkradfnm(dmpfmt,dirname,ldirnam,radname,iiyear,iimon,iiday,  &
                    iihr, iimin, iisec, full_fname, ilen)
      IF (myproc == ROOT) WRITE(stdout,'(1x,2a)')                       &
          ' Remapped filename for this volume: ', TRIM(full_fname)

      IF(.NOT. wrtgrdtilt) THEN

        WRITE(*,'(1x,a,I3,3(a,I8))') 'myproc:',myproc,                  &
                 ' ncolp:',ncolp,', ncoltot:',ncoltot,', nxny: ',nxny
        dualpol = dualpout
        IF( .NOT. dualpdata) dualpol = 0
        IF( radband == 2 .AND. radname(1:1) == 'T' ) isource = 4    ! 5-cm TDWR
        tmin=1.0E09
        tmax=-1.0E09
        DO i=1,ncolp
          DO k=1,nz
            IF( coltim(k,i) > -900.) THEN
              tmin=min(tmin,coltim(k,i))
              tmax=max(tmax,coltim(k,i))
            END IF
          END DO
        END DO
        !CALL print3dnc_lg(100,'zp ',zp(:,:,:) ,nx,ny,nz)
        !CALL print3dnc_lg(200,'zps',zps(:,:,:) ,nx,ny,nz)

        print *, ' Calling wrtradcol, time_min =',tmin,' time_max =',tmax
        CALL wrtradcol(nx,ny,nxny,nz,ncolp,ncoltot,                     &
                   dmpfmt,iradfmt,hdf4cmpr,dmpzero,dualpol,             &
                   full_fname,radname,radlat,radlon,radalt,             &
                   iiyear,iimon,iiday,iihr,iimin,iisec,ivcp,isource,    &
                   refelvmin,refelvmax,refrngmin,refrngmax,             &
                   icolp,jcolp,xcolp,ycolp,zcolp,                       &
                   colref,colvel,colnyq,coltim,colrhv,colzdr,colkdp,    &
                   istatus)
      END IF

    END IF

!------------------------------------------------------------------------
!
!   Write gridded tilt data
!
!------------------------------------------------------------------------

    IF( wrtgrdtilt ) THEN

       WRITE(full_fname,'(a,a4,a1,i4.4,2(i2.2),a1,3(i2.2))')            &
             dirname(1:ldirnam),radname,'.',iiyear,iimon,iiday,'.',     &
             iihr, iimin, iisec

       IF (myproc == ROOT)                                              &
          WRITE(stdout,'(1x,2a)')                                       &
            'Grid-tilt data filename for this volume: ',TRIM(full_fname)

       CALL wrtgridtilt(full_fname,dmpfmt,fntimopt,                     &
              maxelev,nx,ny,nz,kntrelv,radname,                         &
              radlat,radlon,radarx,radary,radalt,dazim,rngmin,rngmax,   &
              grdtilthighref,grdrangeref,grdslrref,grdazmref,           &
              grdtiltdata,tilttimeref,tilttimevel,itimfrst,elevmeanref, &
              grdtilthighvel,grdrangevel,grdslrvel,grdazmvel,           &
              elevmeanvel,xs,ys,zps,ivcp,isource,grdtiltver,grdtiltnumvar)

    END IF
  END IF  ! remap_on

!------------------------------------------------------------------------
!
! The End.
!
!------------------------------------------------------------------------

  9999 CONTINUE

  IF (myproc == ROOT) THEN
    IF (istatus == 0) THEN
      WRITE(stdout,'(/a/)') '  === Normal termination of RADREMAP === '
    ELSE
      WRITE(stdout,'(/a/)') '  *** Abortion of program RADREMAP *** '
    END IF
  END IF

  CALL mpexit(0)
END PROGRAM radremap

!#######################################################################

SUBROUTINE printHELP(outunit,progname,istatus)

!#######################################################################
!
! PURPOSE: Print help message
!
! NOTE: Should be called by root process only.
!
!#######################################################################

  IMPLICIT NONE
  CHARACTER(LEN=*), INTENT(IN)  :: progname
  INTEGER,          INTENT(IN)  :: outunit
  INTEGER,          INTENT(OUT) :: istatus

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus=1

  WRITE(outunit,'(2(1x,3a,/),1x,a,/,1x,3a,/,48(1x,a,/))')               &
        ' Usage: ',TRIM(progname),' [options] nmlfile',                 &
        '   OR   ',TRIM(progname),' [options] < nmlfile',               &
        ' ',                                                            &
        ' NMLFILE is the name of namelist file for "',TRIM(progname),'"', &
        ' ',                                                            &
        ' OPTIONS:',                                                    &
        ' ',                                                            &
        '     -help                  ',                                 &
        '     --help                 Pint help message and abort',      &
        '     -radid      RADAR_ID   radname',                                                    &
        '     -radfile    fname      Radar data file name',                                       &
        '     -nprocx_in             Number patches of data file in X direction',                 &
        '     -nprocy_in             Number patches of data file in Y direction',                 &
        '     -novel                 Donot process radial velocity',                              &
        '     -dir        dirname    Use specified directory for output',                         &
        '     -hdf        2          Output in hdf format with compression level',                &
        '     -binary                Output in binary format',                                    &
        '     -ref2d                 Produce a 2d reflectivity file of lowest tilt',              &
        '     -ref3d                 Produce a 3d reflectivity file for plotting',                &
        '     -reffile               Produce a 3d reflectivity file for plotting',                &
        '     -vel2d                 Produce a 2d velocity file of lowest tilt',                  &
        '     -vel3d                 Produce a 3d velocity file for plotting',                    &
        '     -velfile               Produce a 3d velocity file for plotting',                    &
        '     -rhv2d                 Produce a 2d Rho-HV file of lowest tilt',                    &
        '     -rhv3d                 Produce a 3d Rho-HV file for plotting',                      &
        '     -zdr2d                 Produce a 2d Zdr file of lowest tilt',                       &
        '     -zdr3d                 Produce a 3d Zdr file for plotting',                         &
        '     -kdp2d                 Produce a 2d Kdp file of lowest tilt',                       &
        '     -kdp3d                 Produce a 3d Kdp file for plotting',                         &
        '     -unfdiag               Write files for velocity unfolding diagnostics',             &
        '     -noqc                  Skip QC steps in processing',                                &
        '     -xuqc                  Use Xu''s QC package',                                       &
        '     -fillref               Fill-in reflectivity below lowest tilt',                     &
        '     -medfilt               Apply median filter to az-ran data in despeckle step',       &
        '     -fntime                Use time in filename for output filename',                   &
        '     -rtopts                Use real-time options',                                      &
        '                             o Do not process any data from clear-air VCPs',             &
        '                             o Apply median filter',                                     &
        '     -radar98               Read using WSR-98D CINRAD radar format',                     &
        '     -noclrv                Skipping velocities in clear-air VCP data',                  &
        '     -noclear               Skipping all clear-air VCP data',                            &
        '     -solo                  Write sweep file in DORADE format for working with SoloII',  &
        '     -soloqc                Write sweep file in DORADE format for working with SoloII after QC', &
        '     -sound                 Use single-sounding backgroud file',                         &
        '     -tradi                 Force use of traditional backgroud processing (initgrdvar)', &
        '     -vad          1        VAD wind profile file generated for ADAS',                   &
        '                            1: Original ADAS formatted VAD',                             &
        '                            2: GVAD',                                                    &
        '     -wrttilt               Write radar scans in az-ran format',                         &
        '     -wrtgridtilt  1        Write radar scans in grid-tilt format',                      &
        '                            1: Old grid-tilt version for the EnKF application',          &
        '                               Data are stored in the entire MODEL domain',              &
        '                            2: Data are stored in updated grid tilt-column format',      &
        ' '


  RETURN
END SUBROUTINE printHELP
