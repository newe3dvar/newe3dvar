!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
      SUBROUTINE search_min_31( ncircle,vrob,phiob,cos_elv,nyqv,rdn  &
                               ,a0,phi0,crit                         &
                               ,test_style,kmin,tmp_cost,tmp_v       &
                               ,vbest,costf,spval,cost_indx )

!     ==================================================================
!     PURPOSE:
!       Using the estimate abs(V) to do parabola regression to get
!       the minimun cost_function and best abs(v).
!     ==================================================================
!
!     Author   : Nai, Kang
!     Date     : Oct. 23, 2007
!     Action   : Created.
!
!     History  :
!     ----------
!
!     ------------------------------------------------------------------
      implicit none

      integer :: ncircle
      real,dimension(1:ncircle) :: vrob
      real,dimension(1:ncircle) :: phiob
      real                      :: cos_elv
      real                      :: nyqv
      real                      :: rdn
      real                      :: a0
      real                      :: phi0
      real     :: crit

      integer  :: test_style,kmin
      real,dimension(13) :: tmp_cost,tmp_v
      real     :: vbest
      real     :: costf
      real     :: spval
      integer  :: cost_indx

      integer  :: i,j,k,kk,kp
      integer  :: ncount,mcount
      real     :: a1,a2,b1,b2,c1,g1,g2,f1,f2,f3
      real     :: dltav1,dltav2,dltav3

! ######################################################################
!     Excuete
! ######################################################################

      IF (kmin <= 1 .OR. kmin >= 13) THEN
        vbest=spval
        RETURN
      END IF

!     Using three points doing  parabola regression to get the
!     minimun cost_function.
!     ----------------------------------------------------------------
      ncount=1
   30 continue
      f3=tmp_v(kmin)
      dltav1=tmp_v(kmin-1)-tmp_v(kmin)
      dltav2=tmp_v(kmin+1)-tmp_v(kmin)
      dltav3=tmp_v(kmin+1)-tmp_v(kmin-1)
      a1=tmp_cost(kmin-1)-tmp_cost(kmin)
      a2=tmp_cost(kmin+1)-tmp_cost(kmin)
      b1=a1*dltav2**2-a2*dltav1**2
      b2=dltav1*dltav2*dltav3
      g1=b1/b2
      if ( g1==0.0 ) g1=g1+0.5
      g2=(a1-g1*dltav1)/dltav1**2
      c1=g1/(2.0*g2)
      f1=tmp_v(kmin)-c1                       ! new v0
      if ( abs(c1)<0.001 ) go to 40

      call cost_function_31( ncircle,vrob,phiob,cos_elv,nyqv,rdn     &
                            ,a0,phi0,crit                            &
                            ,f1,costf,spval )

      if ( costf<tmp_cost(kmin) ) then
        if ( f1>f3 ) then
          f2=tmp_cost(kmin)
          tmp_v(kmin-1)=tmp_v(kmin)
          tmp_cost(kmin-1)=tmp_cost(kmin)
          tmp_v(kmin)=f1
          tmp_cost(kmin)=costf
          if ( abs(f2-tmp_cost(kmin))/f2<0.01 ) go to 40
          if ( abs(f1-tmp_v(kmin-1))/tmp_v(kmin-1)<0.01 ) go to 40
        endif
        if ( f1<f3 ) then
          f2=tmp_cost(kmin)
          tmp_v(kmin+1)=tmp_v(kmin)
          tmp_cost(kmin+1)=tmp_cost(kmin)
          tmp_v(kmin)=f1
          tmp_cost(kmin)=costf
          if ( abs(f2-tmp_cost(kmin))/f2<0.01 ) go to 40
          if ( abs(f1-tmp_v(kmin+1))/tmp_v(kmin-1)<0.01 ) go to 40
        endif
      else
        go to 40
      endif
      ncount=ncount+1
      if ( ncount>20 ) then
        vbest=spval
        RETURN
      endif
      go to 30
   40 continue
      vbest=tmp_v(kmin)

      return

      END SUBROUTINE search_min_31
