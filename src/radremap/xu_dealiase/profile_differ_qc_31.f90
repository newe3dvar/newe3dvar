!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!     ==================================================================
!     PURPOSE:
!        vertical profile differ's check.
!     ==================================================================
!     Author : Kang Nai
!     date   : Jan 9, 2008
!     Action : Created.
!
!     Modify History:
!
!     ==================================================================
      SUBROUTINE PROFILE_DIFFER_QC_31

      use Xu_variables

      implicit none

      integer :: i,ii,j,jj,k,kk,ist,ien,nsum,kst,ken,jp,ibgn
      real    :: a1,a2,a3,b1,b2,b3,c1,c2,c3
      integer,dimension(zlevel) :: prfl_index
!     ------------------------------------------------------------------
!     excuete
!     ------------------------------------------------------------------
      prfl_index=0

      jp=int(1301.0/zstep)                     ! start at 1300 m
      ii=0
      do ilevel=jp,zlevel
        if ( abs(ustor(ilevel))<spval ) then
          ii=ilevel; exit   
        endif
      enddo

      if ( ii==0 ) RETURN         ! no profile above 1300 m

      if ( ii>=zlevel ) RETURN     ! no abrupt layer above 1300.0m

      ken=0
      a1=sqrt(ustor(ii)*ustor(ii)+vstor(ii)*vstor(ii))
      a2=ustor(ii); a3=vstor(ii)
      ist=ii
      do k=ii+1,zlevel
        if ( abs(ustor(k))<spval ) then
          b1=sqrt(ustor(k)*ustor(k)+vstor(k)*vstor(k))
          b2=ustor(k); b3=vstor(k)
          c1=b1-a1; c2=abs(b2-a2); c3=abs(b3-a3)
          if ( (c2>12.0 .or. c3>12.0) .and. c1<-0.1 ) then
            ken=k
            exit
          elseif ( c1<-3.0 ) then          ! the higher, the weak
            if ( b2*a2<0.0 .or. b3*a3<0.0 ) then
              ken=k
              exit
            endif
          else
            if ( abs(c1)<6.0 ) then
              a1=b1
              a2=b2
              a3=b3
              ist=k
            else
              ken=k
              exit
            endif
          endif
        endif
      enddo

      if ( ken==0 ) RETURN       ! only one sequence layer above 1300.0

      ustor(ken)=spval
      vstor(ken)=spval

      ii=ken
      ien=0
      do k=ii+1,zlevel
        if ( abs(ustor(k))<spval ) then
          b1=sqrt(ustor(k)*ustor(k)+vstor(k)*vstor(k))
          b2=ustor(k); b3=vstor(k)
          c1=b1-a1; c2=abs(b2-a2); c3=abs(b3-a3)
          if ( (c2>12.0 .or. c3>12.0) .and. c1<-0.1 ) then
            ien=k
            exit
          elseif ( c1<-3.0 ) then          ! the higher, the weak
            if ( b2*a2<0.0 .or. b3*a3<0.0 ) then
              ien=k
              exit
            endif
          else
            if ( abs(c1)<8.0 ) then
              a1=b1
              a2=b2
              a3=b3
            else
              ien=k
              exit
            endif
          endif
        endif
      enddo
  
      if ( ien==0 ) RETURN

      ustor(ien)=spval
      vstor(ien)=spval

      ii=ien
      ken=zlevel
      do k=ii+1,zlevel
        if ( abs(ustor(k))<spval ) then
          b1=sqrt(ustor(k)*ustor(k)+vstor(k)*vstor(k))
          b2=ustor(k); b3=vstor(k)
          c1=b1-a1; c2=abs(b2-a2); c3=abs(b3-a3)
          if ( (c2>12.0 .or. c3>12.0) .and. c1<-0.1 ) then
            ken=k
            exit
          elseif ( c1<-3.0 ) then          ! the higher, the weak
            if ( b2*a2<0.0 .or. b3*a3<0.0 ) then
              ken=k
              exit
            endif
          else
            if ( abs(c1)<8.0 ) then
              a1=b1
              a2=b2
              a3=b3
            else
              ken=k
              exit
            endif
          endif
        endif
      enddo

      if ( ken==zlevel ) RETURN

!     ------------------------------------------------------------------
!     filling in spval above the ken level
!     ------------------------------------------------------------------
      do i=ken,zlevel
         ustor(i)=spval
         vstor(i)=spval
         cf1stor(i) = spval
         cf2stor(i) = spval
         cf3stor(i) = spval
      enddo
      RETURN

      END SUBROUTINE PROFILE_DIFFER_QC_31
