!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!     ==================================================================
!     PURPOSE:
!        Using the radar data( after QC ) to do the classical VAD to 
!        get the wind profile.
!
!     Author   : Kang Nai
!     Date     : 01/14/2008
!     Action   : created
!
!     ==================================================================
      SUBROUTINE classical_VAD_31

      use Xu_variables

      IMPLICIT NONE

      INTEGER    :: i,j,k,ip,jp,jstep,jref
      integer    :: ii,jj,kk
      real       :: a1,a2,a3,a4
      integer    :: irfstgat,irefgatsp
      real,dimension(1:np) :: tmpphi,tmpvel
      integer    :: dnpt
      real       :: cf1,cf2,cf3,cos_elv
      character(len=80) :: name1,name2,name3
!
      ustor=spval
      vstor=spval
      vad_intp=1

      do ilevel=1, zlevel

!       ----------------------------------------------------------------
!       choice data.
!       ----------------------------------------------------------------
        a1=0.0
        a2=0.0
        a3=0.0
        do k_tilt=1,nthet
          k=k_tilt
          if (select_circ(k,ilevel)>0 ) then
            nbeam=nphi(k)
            nrang = imaxrng(k)
            elvng = thet(k)
            cos_elv=cos(elvng*rdn)

            ii=select_circ(k,ilevel)

            ip=0
            do j=1,nbeam
               if ( abs(vel(ii,j,k))<spval ) then
                    ip=ip+1
                    tmpvel(ip) = -vel(ii,j,k)
                    tmpphi(ip) = 90.0-phi(j,k)
               endif
            enddo
            if ( ip>1 ) then
              call vadlsf(nr,ip,spval,tmpphi,tmpvel,dnpt,cf1,cf2,cf3)
              if ( dnpt .gt. 5 ) then
                 a1=a1-cf2/cos_elv
                 a2=a2+cf3/cos_elv
                 a3=a3+1.0
              endif
            endif
          endif
        enddo

!       ----------------------------------------------------------------
!       do mean job.
!       ----------------------------------------------------------------
        if ( a3>1.0 ) then
          ustor(ilevel)=a1/a3
          vstor(ilevel)=a2/a3
          if ( abs(ustor(ilevel))>100.0 .or.                         &
               abs(vstor(ilevel))>100.0 ) then
            ustor(ilevel)=spval
            vstor(ilevel)=spval
          else
            vad_intp(ilevel)=0
          endif
        endif
      enddo

!     ------------------------------------------------------------------
!     do profile qc and full profile.
!     ------------------------------------------------------------------
      call profile_continue_qc
      call profile_differ_qc_31
      call full_profile_31

!     ------------------------------------------------------------------
!     re-calculate the ivadflag.
!     ------------------------------------------------------------------
      call decide_vadflag

      if ( ivadflag<0 ) then

        no_qualify=.true.
        name1='There is no VAD, no action be made!!!'
        name3='NOVAD.dat'
        open(31,file=name3)
          write(31,'(a37)') name1
        close(31)
        vel=spval
        return

      else

!       ----------------------------------------------------------------
!       output the parameters of the radar.
!       ----------------------------------------------------------------
        IF (outdiag) call write_parameters
!       ----------------------------------------------------------------
!       output the radar united data.
!       ----------------------------------------------------------------
        IF (outdiag) call united_write
!       ----------------------------------------------------------------
!       output the wind profile
!       ----------------------------------------------------------------
IF (outdiag) THEN
        name1=radid(1:4)//'_VAD_profile.keep'
        open(31,file=name1,form='formatted')
          write(31,'(6i6)')iyr(0),imon(0),iday(0),ihr(0),imin(0),isec(0)
          write(31,'(3f8.3)') radlat,radlon,radelv
          do ip=1,zlevel
            if ( abs(ustor(ip))<spval .and. abs(vstor(ip))<spval ) then
              a1 = ustor(ip)*ustor(ip)
              a2 = vstor(ip)*vstor(ip)
              a3 = a1+a2
              if ( a3 .gt. 0.0 ) then
                a4 = sqrt(a3)
              else
                a4 = 0.0
              endif
              a1 = hstor(ip)
              a2 = 0.0
              write(31,'(4e15.5,i8,f5.1)') a1,ustor(ip),vstor(ip)    &
                                          ,a4,ip,a2
            endif
          enddo
        close(31)

        open(31,file='radar_prfl.dat',form='formatted')
          do ip=1,zlevel
            if ( abs(ustor(ip))<spval .and. abs(vstor(ip))<spval ) then
              a1 = ustor(ip)*ustor(ip)
              a2 = vstor(ip)*vstor(ip)
              a3 = a1+a2
              if ( a3 .gt. 0.0 ) then
                a4 = sqrt(a3)
              else
                a4 = 0.0
              endif
              a1 = hstor(ip)
              a2 = 0.0
              write(31,'(4e15.5,i8,f5.1)') a1,ustor(ip),vstor(ip)    &
                                          ,a4,ip,a2
            endif
          enddo
        close(31)
endif

      endif

      return

      END SUBROUTINE classical_VAD_31
