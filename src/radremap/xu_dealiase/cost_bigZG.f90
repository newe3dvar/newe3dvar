!#######################################################################
!#######################################################################
!####                                                               ####
!####                Subroutine cost_bigZG                          ####
!####                                                               ####
!#######################################################################
!#######################################################################

      subroutine cost_bigZG( nvar,x,vrbk,phibk,ZG,kk                 &
                            ,ncircle,vrob,phiob,cos_elv,nyqv,rdn     &
                            ,utri_array,ltri_array,sigma             &
                            ,alfa,indx_real1                         &
                            ,costf,f1,spval )
 
!     ==================================================================
!     PURPOSE:
!       Using the estimate V to get the cost function.
!       The function's equation is:
!       J=sum {KAPA(dltav,nyqv)}**2/sigma**2
!     ==================================================================
!
!     Author :  Kang Nai
!     Date   :  Feb. 24, 2009
!     Action :  Created.
!
!     History:
!     --------
!
!     ------------------------------------------------------------------

      implicit none
!
      integer :: nvar
      real,dimension(1:nvar)        :: x
      real,dimension(1:nvar)        :: vrbk,phibk
      integer                       :: kk
      real,dimension(1:nvar,1:nvar) :: utri_array,ltri_array

      integer :: ncircle
      real, dimension(1:ncircle)   :: vrob
      real, dimension(1:ncircle)   :: phiob
      real,dimension(1:ncircle)    :: ZG
      real                         :: cos_elv
      real                         :: nyqv
      real                         :: rdn
      real                         :: sigma
      real                         :: alfa
      integer,dimension(1:ncircle) :: indx_real1
      real                         :: costf,f1
      real                         :: spval
      real,dimension(1:ncircle)    :: vrin

      integer :: i,ii,j,jj,k,m,mm,n,nn,nsum,knum,i1,i2,jm
      real    :: twonyq,thrths
      real    :: a1,a2,a3,a4,a5,b1,b2,b3,b4,b5

      real, dimension(1:nvar) :: veltmp
      real    :: dlta_v

!     ##################################################################
!     Execute. calculate the cost function.
!     ##################################################################
      do i=1,nvar
        a1=0.0
        do j=1,nvar
          a1=a1+ltri_array(i,j)*x(j)
        enddo
        veltmp(i)=vrbk(i)+a1               ! new Vrref
      enddo
!     ------------------------------------------------------------------
!     circle linear_interpolation
!     ------------------------------------------------------------------
      call circle_linear_int( nvar,veltmp,phibk                      &
                             ,ncircle,vrob,phiob                     &
                             ,vrin,spval )

      b1=0.0
      b3=0.0
      b4=0.0
      b5=0.0
      a5=0.0
      a4=0.0
      twonyq=2.0*nyqv
      thrths=alfa*nyqv

      ZG=0.0
      do j=1,ncircle
        dlta_v=vrob(j)-vrin(j)
        if ( dlta_v>=0.0 ) then
          n=int(dlta_v/twonyq+0.5)
        else
          n=int(dlta_v/twonyq-0.5)
        endif
        b2=(dlta_v-float(n)*twonyq)
        ZG(j)=b2
        b3=b3+b2
        b4=b4+b2*b2
        b5=b5+1.0
      enddo

      if ( b5<1.0 ) then
        costf=spval
        f1=spval
      else
        costf=b4/sigma/sigma                     ! true J
        f1=b4/b5                                 ! average J
      endif

      return
      end subroutine cost_bigZG
