!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
      SUBROUTINE SETUP_WIND_PROFILE

!     ==================================================================
!     PURPOSE:
!       Get the horizontal wind field's profile by using:
!       slope_method;
!       bayes_method;
!       conti_method;
!       covar_method;
!
!     ==================================================================
!
!     Author   : Gong, Jiandong et al.
!     Date     :
!     Action   : Created.
!
!     History  :
!     ----------
!     name     : Nai, Kang
!     date     : Sep. 25, 2007
!     action   : change the method to aliase-robust method.
!
!     ------------------------------------------------------------------
      USE Xu_variables

      IMPLICIT NONE

      real :: a1,a2,a3,a4
      integer :: k

!     ------------------------------------------------------------------
!     0Vr and the tendency method to get the wind profile
!     ------------------------------------------------------------------
      CALL tendency_method

!     ------------------------------------------------------------------
!     combine the profile:
!       first collect is the robust VAD's profile.
!       second collect is the priority robust VAD's profile.
!       third collect is the model's profile.
!     ------------------------------------------------------------------
!     CALL READ_WIND_PROFILE

!     ------------------------------------------------------------------
!     wind profile quality check
!     ------------------------------------------------------------------
      call profile_continue_qc
      call profile_differ_qc

!     ------------------------------------------------------------------
!     full the profile
!     ------------------------------------------------------------------
      call full_profile

!     ------------------------------------------------------------------
!     write out wind profile
!     ------------------------------------------------------------------
      IF (outdiag) open(31,file='aprfl.dat',form='formatted')
      do ilevel=1,zlevel
        if ( abs(ustor(ilevel))<spval .and.                             &
             abs(vstor(ilevel))<spval ) then
          a1 = ustor(ilevel)*ustor(ilevel)
          a2 = vstor(ilevel)*vstor(ilevel)
          a3 = a1+a2
          if ( a3 .gt. 0.0 ) then
               a4 = sqrt(a3)
          else
               a4 = 0.0
          endif
          a1 = hstor(ilevel)
          IF (outdiag) write(31,'(4e15.5,i8)') a1,ustor(ilevel),vstor(ilevel)     &
                                 ,a4,ilevel
          k=ilevel
        else
          a4=spval
        endif
      enddo
      IF (outdiag) close(31)

      nprfl=k
      print*,'nprfl==',nprfl
      print*,' '

      END SUBROUTINE SETUP_WIND_PROFILE
