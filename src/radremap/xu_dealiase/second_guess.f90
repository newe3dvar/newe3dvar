!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
      SUBROUTINE Second_guess( ref_tilt )

!     ==================================================================
!     PURPOSE:
!       Get the Vabs,u0,v0,beta by using the wind profile.
!
!     ==================================================================
!
!     Author   : Kang Nai
!     Date     : 05/28/2009
!     Action   : Created.
!
!     History  :
!     ----------
!
!     ------------------------------------------------------------------
      USE Xu_variables

      IMPLICIT NONE
 
      integer :: i,j,k,i1,i2,j1,j2,npath,ii
      real :: a1,a2,a3,a4,a5
      real :: b1,b2,b3,b4,b5
      integer :: ref_tilt
      character(len=80) :: name1,name2

!     ------------------------------------------------------------------
!     get the lowest tilt's u0ref, v0ref, beta etc.
!     ------------------------------------------------------------------
      k=k_tilt
      do ilevel=1,zlevel
       if ( select_circ(k,ilevel)>0 ) then
         if ( abs(ustor(ilevel))<spval ) then
           vabs(k,ilevel)=sqrt( ustor(ilevel)*ustor(ilevel) +        &
                                vstor(ilevel)*vstor(ilevel) )
           U0ref(k,ilevel)=ustor(ilevel)
           V0ref(k,ilevel)=vstor(ilevel)
           a1=atan2(vstor(ilevel),ustor(ilevel))    ! gradians
           a2=a1/rdn                                ! degree
           if ( a2<0.0 ) a2=a2+360.0                ! 0-360.0
           a3=90.0-a2                               ! azimuth
           if ( a3<0.0 ) a3=a3+360.0
           betaref(k,ilevel)=a3
           Acnjgt(k,ilevel)=1
           nbeam=nphi(k)
           a3=(360.0/mspc)
           do j=1,mspc         ! VAD
             a4=float(j-1)*a3+0.5
             a5=(90.0-a4)*rdn
             a1=ustor(ilevel)*cos(a5)
             a2=vstor(ilevel)*sin(a5)
             analy_back(j,k,ilevel)=a1+a2
           enddo

!          get the proximate guess
!          if ( select_circ(k-ref_tilt,ilevel)>0 ) then
           if ( vabs(k-ref_tilt,ilevel)<spval ) then
             do j=1,mspc
               analy_back(j,k,ilevel)=analy_back(j,k-ref_tilt,ilevel)
             enddo
           endif    ! proximate guess
         endif    ! VAD guess
       endif    ! select_circ>0
      enddo

!     ------------------------------------------------------------------
!     combine the priority background.
!     ------------------------------------------------------------------
!     do ilevel=1,zlevel
!      ii=select_circ(k,ilevel)
!      if ( ii<0 ) then
!       if ( vabs(k,ilevel)<spval ) then
!         select_circ(k,ilevel)=-ii
!       endif
!      endif
!     enddo

      END SUBROUTINE Second_guess
