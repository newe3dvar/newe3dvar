!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!=======================================================================
!     PURPOSE:
!       Get the start tilt's index and the end tilt's index and more.
! ======================================================================
!
!     Author   : Kang Nai
!     Date     : Dec. 13, 2012
!     Action   : Created.
!
!     History  :
!    -----------
!
! ----------------------------------------------------------------------
      SUBROUTINE setup_tilt

      use Xu_variables

      IMPLICIT NONE

      INTEGER    :: i,j,k,iskip,ii,iii
      character(len=50) :: name1,name2
      real       :: a1,a2
      integer    :: low_tilt

!     ------------------------------------------------------------------
!     get the minimun Nyquist velocity.
!     ------------------------------------------------------------------
      a1=100.0
      a2=0.0
      do k=1,nthet
         if ( thet_nyq_vel(k)<a1 ) then
              a1=thet_nyq_vel(k)
         endif
         if ( thet_nyq_vel(k)>a2 ) then
              a2=thet_nyq_vel(k)
         endif
      enddo
      vnyq_min=a1
      vnyq_max=a2
      print*,' min Nyqv==',vnyq_min,' max Nyqv==', vnyq_max

!     ------------------------------------------------------------------
!     get the start_tilt, end_tilt, and step_tilt.
!     ------------------------------------------------------------------
      if ( vcpnum==31 .or. vcpnum==32 ) then
        low_tilt=100
      else
        low_tilt=400
      endif

      start_tilt=nthet
      do k=1,nthet
         ii=int( thet(k)*100 )
         if ( ii>low_tilt ) then
             end_tilt=k
             exit
         endif
      enddo
      step_tilt=-1

      return
      END SUBROUTINE setup_tilt
