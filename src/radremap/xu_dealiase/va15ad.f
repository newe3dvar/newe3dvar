c*********************************************************************** 
c    This subroutine is used to minimize the costfunction with
c    conjugate gradient method. This is a standard subroutine from
c    numerical recipe.
c*********************************************************************** 
      subroutine va15ad(n,m,x,f,g,diagco,diag,iprint,eps,s,y, 
     *          point,w,iflag,ftol)
c 
c 
      real x(n),g(n),s(m*n),y(m*n),diag(n),w(n+2*m) 
      real ftol,gtol,xtol,stpmin,stpmax,stp,f,ys,sq,
     *              yr,beta,one,zero,eps,xnorm,gnorm,yy,ddot,stp1 
c 
      integer bound,lp,iter,nfun,nfev,iprint(2),point,cp,iflag
      logical finish,diagco 
      common /va15dd/mp,lp, gtol
      save  
      data one,zero/1.0e+0,0.0e+0/
c 
c     ------------------------------------------------------------
c     initialize
c     ------------------------------------------------------------
c 
      if(iflag.eq.0) go to 1
      go to (72,10) iflag 
   1  iter= 0 
      if(n.le.0.or.m.le.0) go to 96 
      if(gtol.le.1.e-04) then 
        if(lp.gt.0) write(lp,145) 
        gtol=1.e-02 
      endif 
      nfun= 1 
      point= 0
      finish= .false. 
      if(diagco) then 
         do 3 i=1,n 
 3       if (diag(i).le.zero) go to 95
      else  
         do 4 i=1,n 
 4       diag(i)= 1.0e0 
      endif 
      do 5 i=1,n
 5    s(i)= -g(i)*diag(i) 
      gnorm= sqrt(ddot(n,g,1,g,1)) 
      stp1= one/gnorm 
c 
c     parameters for line search routine  
c     ----------------------------------
      xtol= 1.0e-17 
      stpmin= 1.0e-20 
      stpmax= 1.0e+20 
      maxfev= 200
c 
       call va15bd(iprint,iter,nfun,
     *                     n,m,x,f,g,stp,finish)
c 
c    ------------------------------------------------------------ 
c     main iteration loop 
c    -------------------------------------------------------- 
c 
 8    iter= iter+1
      info=0
      bound=iter-1
      if (iter .gt. m)bound=m 
      if(iter.eq.1) go to 65
c 
c     ------------------------------------------------------------
c     compute -hg and the diagonal scaling matrix in diag 
c     ------------------------------------------------------------
c 
      if(.not.diagco) then
         do 9 i=1,n 
   9     diag(i)= ys/yy 
      else  
         iflag=2
         return 
      endif 
  10  continue
      do 11 i=1,n 
  11  if (diag(i).le.zero) go to 95 
c 
      cp= point 
      if (point.eq.0) cp=m
      w(n+cp)= one/ys 
      do 12 i=1,n 
  12  w(i)= -g(i) 
      cp= point 
      do 25 ii= 1,bound 
         cp=cp-1
         if (cp.eq. -1)cp=m-1 
         sq= ddot(n,s(cp*n+1),1,w,1)
         w(n+m+cp+1)= w(n+cp+1)*sq
         do 20 k=1,n  
  20     w(k)= w(k)-w(n+m+cp+1)*y(cp*n+k) 
  25  continue
c 
      do 30 i=1,n 
  30  w(i)=diag(i)*w(i) 
      do 45 ii=1,bound
         yr= ddot(n,y(cp*n+1),1,w,1)
         beta= w(n+cp+1)*yr 
         do 40 k=1,n  
  40     w(k)= w(k)+s(cp*n+k)*(w(n+m+cp+1)-beta)
         cp=cp+1
         if (cp.eq.m)cp=0 
  45  continue
c 
c     ------------------------------------------------------------
c     store the new direction in s
c     ------------------------------------------------------------
c 
       do 60 j=1,n
  60   s(point*n+j)= w(j) 
c 
c     ------------------------------------------------------------
c     obtain the minimizer of the function along the
c     direction s by using the line search routine of vd05ad  
c     ------------------------------------------------------------
  65  nfev=0
      stp=one 
      if (iter.eq.1) stp=stp1 
      do 70 i=1,n 
  70  w(i)=g(i) 
  72  continue
c 
      call vd05ad(n,x,f,g,s(point*n+1),stp,ftol,gtol, 
     *            xtol,stpmin,stpmax,maxfev,info,nfev,diag) 
c 
      if (info .eq. -1) then
        iflag=1 
        return
      endif 
      if (info .ne. 1) go to 90 
      nfun= nfun + nfev 
c 
c     ------------------------------------------------------------
c     compute the new s and y 
c     ------------------------------------------------------------
c 
      npt=point*n 
      do 75 i=1,n 
      s(npt+i)= stp*s(npt+i)
  75  y(npt+i)= g(i)-w(i) 
      ys= ddot(n,y(npt+1),1,s(npt+1),1) 
      yy= ddot(n,y(npt+1),1,y(npt+1),1) 
      point=point+1 
      if (point.eq.m)point=0
c 
c     ------------------------------------------------------------
c     convergence check 
c     ------------------------------------------------------------
c 
      gnorm= ddot(n,g,1,g,1)
      gnorm=sqrt(gnorm)
      xnorm= ddot(n,x,1,x,1)
      xnorm=sqrt(xnorm)
      xnorm= max1(1.0,xnorm) 
c 
      if (gnorm/xnorm .le. eps) finish=.true. 
c 
      call va15bd(iprint,iter,nfun, 
     *               n,m,x,f,g,stp,finish)
      if (finish) then
         iflag=0
         return 
      endif 
      go to 8 
c 
c     ------------------------------------------------------------
c     end of main iteration loop. error exits.
c     ------------------------------------------------------------
c 
  90  if(lp.le.0) return
c     if (info.eq.0) then 
c          iflag= -1  
c          write(lp,100)iflag 
c     else if (info.eq.2) then  
c          iflag= -2  
c          write(lp,105)iflag 
c     else if (info.eq.3) then  
c          iflag= -3  
c          write(lp,110)iflag 
c     else if (info.eq.4) then  
c          iflag= -4  
c          write(lp,115)iflag 
c     else if (info.eq.5) then  
c          iflag= -5  
c          write(lp,120)iflag 
c     else if (info.eq.6) then  
c          iflag= -6  
c          write(lp,125)iflag 
c     endif 
      return
c 
  95  iflag= -7 
      if(lp.gt.0) write(lp,135)iflag,i
      return
  96  iflag= -8 
      if(lp.gt.0) write(lp,140)iflag
c 
c     ------------------------------------------------------------
c     formats 
c     ------------------------------------------------------------
c 
 100  format(/' iflag= ',i2,/' improper input parameters during', 
     .       ' the line search.') 
 105  format(/' iflag= ',i2,/' relative width of the interval of',
     .       ' uncertainty in the line search'/'is of the order of
     .          machine roundoff.') 
 110  format(/' iflag= ',i2,/' number of calls to function in the', 
     .       ' line search has reached 20.')
 115  format(/' iflag= ',i2,/' the step in the line search is', 
     .       ' too small.') 
 120  format(/' iflag= ',i2,/' the step in the line search is', 
     .       ' too large.') 
 125  format(/' iflag= ',i2,/' rounding errors prevent further',
     .       ' progress in the line search.') 
 135  format(/' iflag= ',i2,/' the',i5,'-th diagonal element of the', 
     .       ' inverse hessian approximation is not positive')
 140  format(/' iflag= ',i2,/' improper input parameters (n or m',
     .       ' are not positive)')
 145  format(/'  gtol is less than or equal to 1.e-04', 
     .       / 'it has been reset to 1.e-02') 
      return
      end 
c 
c 
c 
c 
      subroutine va15bd(iprint,iter,nfun, 
     *                     n,m,x,f,g,stp,finish)
c 
c     --------------------------------------------------------------------- 
c     this routine prints monitoring information. the frequency and amount
c     of output are specified as follows: 
c 
c     iprint(1) < 0 : no output is generated
c     iprint(1) = 0 : output only at first and last iteration
c     iprint(1) > 0 : output every iprint(1) iteration
c     iprint(2) = 0 : iteration count, function value, norm of the gradient
c                     ,number of function calls and step length 
c     iprint(2) = 1 : + vector of variables and gradient vector at the  
c                       initial point 
c     iprint(2) = 2 : + vector of variables
c     iprint(2) = 3 : + gradient vector 
c     --------------------------------------------------------------------- 
c 
      real x(n),g(n),f,gnorm,stp,factor,ddot,gtol 
      integer iprint(2),iter,nfun,prob,lp 
      logical finish  
      common /set/ factor,prob
      common /va15dd/mp,lp, gtol
c 
      if (iprint(1).lt.0)return 
      gnorm= ddot(n,g,1,g,1)
      gnorm= sqrt(gnorm) 
      if (iter.eq.0)then
           write(mp,10) 
           write(mp,20) prob,n,m
           write(mp,30)f,gnorm
                 if (iprint(2).ge.1)then
                     write(mp,40) 
                     write(mp,50) (x(i),i=1,n)
                     write(mp,60) 
                     write(mp,50) (g(i),i=1,n)
                  endif 
           write(mp,10) 
           write(mp,70) 
      else  
          if ((iprint(1).eq.0).and.(iter.ne.1.and..not.finish))return 
              if (iprint(1).ne.0)then 
                   if(mod(iter-1,iprint(1)).eq.0.or.finish)then 
                         write(mp,80)iter,nfun,f,gnorm,stp
                   else 
                         return 
                   endif
              else
                   write(mp,80)iter,nfun,f,gnorm,stp
              endif 
              if (iprint(2).eq.2.or.iprint(2).eq.3)then 
                    if (finish)then 
                        write(mp,90)
                    else
                        write(mp,40)
                    endif 
                      write(mp,50)(x(i),i=1,n)
                  if (iprint(2).eq.3)then 
                      write(mp,60)
                      write(mp,50)(g(i),i=1,n)
                  endif 
              endif 
            if (finish) write(mp,100) 
      endif 
c 
 10   format('*************************************************') 
 20   format(' prob=',i3,'   n=',i5,'   number of corrections=',i2) 
 30   format(' f= ',1pd10.3,'   gnorm= ',1pd10.3) 
 40   format(' vector x= ') 
 50   format(6(2x,1pd10.3)) 
 60   format(' gradient vector g= ')
 70   format(/'   i   nfn',4x,'func',8x,'gnorm',7x,'steplength'/) 
 80   format(2(i4,1x),3x,3(1pd10.3,2x)) 
 90   format(' final point x= ')
 100  format(/' the minimization terminated without detecting errors.', 
     .       /' iflag = 0') 
c 
      return
      end 
c 
c   ----------------------------------------------------------
c   data block
c   ----------------------------------------------------------
c 
      block data va15cd 
      common /va15dd/mp,lp, gtol
      integer lp
      real gtol 
      data mp,lp,gtol/6,6,9.0e-01/
      end 
c 
c   ------------------------------------------------------------- 
c 
      subroutine vd05ad(n,x,f,g,s,stp,ftol,gtol,xtol, 
     *                  stpmin,stpmax,maxfev,info,nfev,wa)
      integer n,maxfev,info,nfev
      real f,stp,ftol,gtol,xtol,stpmin,stpmax 
      real x(n),g(n),s(n),wa(n) 
      save  
c     **********
c 
c     subroutine vd05ad 
c 
c     the purpose of vd05ad is to find a step which satisfies 
c     a sufficient decrease condition and a curvature condition.
c     the user must provide a subroutine which calculates the 
c     function and the gradient.
c 
c     at each stage the subroutine updates an interval of 
c     uncertainty with endpoints stx and sty. the interval of 
c     uncertainty is initially chosen so that it contains a 
c     minimizer of the modified function  
c 
c          f(x+stp*s) - f(x) - ftol*stp*(gradf(x)'s). 
c 
c     if a step is obtained for which the modified function 
c     has a nonpositive function value and nonnegative derivative,
c     then the interval of uncertainty is chosen so that it 
c     contains a minimizer of f(x+stp*s). 
c 
c     the algorithm is designed to find a step which satisfies
c     the sufficient decrease condition 
c 
c           f(x+stp*s) .le. f(x) + ftol*stp*(gradf(x)'s), 
c 
c     and the curvature condition 
c 
c           abs(gradf(x+stp*s)'s)) .le. gtol*abs(gradf(x)'s). 
c 
c     if ftol is less than gtol and if, for example, the function 
c     is bounded below, then there is always a step which satisfies 
c     both conditions. if no step can be found which satisfies both 
c     conditions, then the algorithm usually stops when rounding
c     errors prevent further progress. in this case stp only  
c     satisfies the sufficient decrease condition.
c 
c     the subroutine statement is 
c 
c        subroutine vd05ad(n,x,f,g,s,stp,ftol,gtol,xtol,
c                          stpmin,stpmax,maxfev,info,nfev,wa) 
c     where 
c 
c       n is a positive integer input variable set to the number
c         of variables. 
c 
c       x is an array of length n. on input it must contain the 
c         base point for the line search. on output it contains 
c         x + stp*s.  
c 
c       f is a variable. on input it must contain the value of f
c         at x. on output it contains the value of f at x + stp*s.
c 
c       g is an array of length n. on input it must contain the 
c         gradient of f at x. on output it contains the gradient
c         of f at x + stp*s.
c 
c       s is an input array of length n which specifies the 
c         search direction. 
c 
c       stp is a nonnegative variable. on input stp contains an 
c         initial estimate of a satisfactory step. on output  
c         stp contains the final estimate.
c 
c       ftol and gtol are nonnegative input variables. termination
c         occurs when the sufficient decrease condition and the 
c         directional derivative condition are satisfied. 
c 
c       xtol is a nonnegative input variable. termination occurs
c         when the relative width of the interval of uncertainty
c         is at most xtol.
c 
c       stpmin and stpmax are nonnegative input variables which 
c         specify lower and upper bounds for the step.
c 
c       maxfev is a positive integer input variable. termination
c         occurs when the number of calls to fcn is at least  
c         maxfev by the end of an iteration.
c 
c       info is an integer output variable set as follows:  
c 
c         info = 0  improper input parameters.
c 
c         info =-1  a return is made to compute the function and gradient.
c 
c         info = 1  the sufficient decrease condition and the 
c                   directional derivative condition hold.
c 
c         info = 2  relative width of the interval of uncertainty 
c                   is at most xtol.
c 
c         info = 3  number of calls to fcn has reached maxfev.
c 
c         info = 4  the step is at the lower bound stpmin.
c 
c         info = 5  the step is at the upper bound stpmax.
c 
c         info = 6  rounding errors prevent further progress. 
c                   there may not be a step which satisfies the 
c                   sufficient decrease and curvature conditions. 
c                   tolerances may be too small.
c 
c       nfev is an integer output variable set to the number of 
c         calls to fcn. 
c 
c       wa is a work array of length n. 
c 
c     subprograms called
c 
c       harwell-supplied...vd05bd 
c 
c       fortran-supplied...abs,max,min
c 
c     argonne national laboratory. minpack project. june 1983 
c     jorge j. more', david j. thuente
c 
c     **********
      integer infoc,j 
      logical brackt,stage1 
      real dg,dgm,dginit,dgtest,dgx,dgxm,dgy,dgym,
     *       finit,ftest1,fm,fx,fxm,fy,fym,p5,p66,stx,sty,
     *       stmin,stmax,width,width1,xtrapf,zero 
      data p5,p66,xtrapf,zero /0.5e0,0.66e0,4.0e0,0.0e0/
      if(info.eq.-1) go to 45 
      infoc = 1 
c 
c     check the input parameters for errors.
c 
      if (n .le. 0 .or. stp .le. zero .or. ftol .lt. zero .or.
     *    gtol .lt. zero .or. xtol .lt. zero .or. stpmin .lt. zero
     *    .or. stpmax .lt. stpmin .or. maxfev .le. 0) return  
c 
c     compute the initial gradient in the search direction
c     and check that s is a descent direction.
c 
      dginit = zero 
      do 10 j = 1, n  
         dginit = dginit + g(j)*s(j)
   10    continue 
      if (dginit .ge. zero) return
c 
c     initialize local variables. 
c 
      brackt = .false.
      stage1 = .true. 
      nfev = 0
      finit = f 
      dgtest = ftol*dginit
      width = stpmax - stpmin 
      width1 = width/p5 
      do 20 j = 1, n  
         wa(j) = x(j) 
   20    continue 
c 
c     the variables stx, fx, dgx contain the values of the step,
c     function, and directional derivative at the best step.  
c     the variables sty, fy, dgy contain the value of the step, 
c     function, and derivative at the other endpoint of 
c     the interval of uncertainty.
c     the variables stp, f, dg contain the values of the step,
c     function, and derivative at the current step. 
c 
      stx = zero
      fx = finit
      dgx = dginit
      sty = zero
      fy = finit
      dgy = dginit
c 
c     start of iteration. 
c 
   30 continue
c 
c        set the minimum and maximum steps to correspond
c        to the present interval of uncertainty.
c 
         if (brackt) then 
            stmin = min(stx,sty)
            stmax = max(stx,sty)
         else 
            stmin = stx 
            stmax = stp + xtrapf*(stp - stx)
            end if
c 
c        force the step to be within the bounds stpmax and stpmin.
c 
         stp = max(stp,stpmin)
         stp = min(stp,stpmax)
c 
c        if an unusual termination is to occur then let 
c        stp be the lowest point obtained so far. 
c 
         if ((brackt .and. (stp .le. stmin .or. stp .ge. stmax))
     *      .or. nfev .ge. maxfev-1 .or. infoc .eq. 0 
     *      .or. (brackt .and. stmax-stmin .le. xtol*stmax)) stp = stx  
c 
c        evaluate the function and gradient at stp  
c        and compute the directional derivative.
c 
         do 40 j = 1, n 
            x(j) = wa(j) + stp*s(j) 
   40       continue  
         info=-1
         return 
c 
   45    info=0 
         nfev = nfev + 1
         dg = zero
         do 50 j = 1, n 
            dg = dg + g(j)*s(j) 
   50       continue  
         ftest1 = finit + stp*dgtest
c 
c        test for convergence.
c 
         if ((brackt .and. (stp .le. stmin .or. stp .ge. stmax))
     *      .or. infoc .eq. 0) info = 6 
         if (stp .eq. stpmax .and.
     *       f .le. ftest1 .and. dg .le. dgtest) info = 5 
         if (stp .eq. stpmin .and.
     *       (f .gt. ftest1 .or. dg .ge. dgtest)) info = 4
         if (nfev .ge. maxfev) info = 3 
         if (brackt .and. stmax-stmin .le. xtol*stmax) info = 2 
         if (f .le. ftest1 .and. abs(dg) .le. gtol*(-dginit)) info = 1  
c 
c        check for termination. 
c 
         if (info .ne. 0) return
c 
c        in the first stage we seek a step for which the modified 
c        function has a nonpositive value and nonnegative derivative. 
c 
         if (stage1 .and. f .le. ftest1 .and. 
     *       dg .ge. min(ftol,gtol)*dginit) stage1 = .false.  
c 
c        a modified function is used to predict the step only if
c        we have not obtained a step for which the modified 
c        function has a nonpositive function value and nonnegative
c        derivative, and if a lower function value has been 
c        obtained but the decrease is not sufficient. 
c 
         if (stage1 .and. f .le. fx .and. f .gt. ftest1) then 
c 
c           define the modified function and derivative values. 
c 
            fm = f - stp*dgtest 
            fxm = fx - stx*dgtest 
            fym = fy - sty*dgtest 
            dgm = dg - dgtest 
            dgxm = dgx - dgtest 
            dgym = dgy - dgtest 
c 
c           call cstep to update the interval of uncertainty
c           and to compute the new step.  
c 
            call vd05bd(stx,fxm,dgxm,sty,fym,dgym,stp,fm,dgm, 
     *                 brackt,stmin,stmax,infoc)
c 
c           reset the function and gradient values for f. 
c 
            fx = fxm + stx*dgtest 
            fy = fym + sty*dgtest 
            dgx = dgxm + dgtest 
            dgy = dgym + dgtest 
         else 
c 
c           call vd05bd to update the interval of uncertainty 
c           and to compute the new step.  
c 
            call vd05bd(stx,fx,dgx,sty,fy,dgy,stp,f,dg, 
     *                 brackt,stmin,stmax,infoc)
            end if
c 
c        force a sufficient decrease in the size of the 
c        interval of uncertainty. 
c 
         if (brackt) then 
            if (abs(sty-stx) .ge. p66*width1) 
     *         stp = stx + p5*(sty - stx) 
            width1 = width
            width = abs(sty-stx)
            end if
c 
c        end of iteration.
c 
         go to 30 
c 
c     last card of subroutine vd05ad. 
c 
      end 
c
c
c
c
      subroutine vd05bd(stx,fx,dx,sty,fy,dy,stp,fp,dp,brackt, 
     *                 stpmin,stpmax,info)
      integer info
      real stx,fx,dx,sty,fy,dy,stp,fp,dp,stpmin,stpmax
      logical brackt,bound
c     **********
c 
c     subroutine vd05bd 
c 
c     the purpose of vd05bd is to compute a safeguarded step for
c     a linesearch and to update an interval of uncertainty for 
c     a minimizer of the function.
c 
c     the parameter stx contains the step with the least function 
c     value. the parameter stp contains the current step. it is 
c     assumed that the derivative at stx is negative in the 
c     direction of the step. if brackt is set true then a 
c     minimizer has been bracketed in an interval of uncertainty
c     with endpoints stx and sty. 
c 
c     the subroutine statement is 
c 
c       subroutine vd05bd(stx,fx,dx,sty,fy,dy,stp,fp,dp,brackt, 
c                        stpmin,stpmax,info)
c 
c     where 
c 
c       stx, fx, and dx are variables which specify the step, 
c         the function, and the derivative at the best step obtained
c         so far. the derivative must be negative in the direction
c         of the step, that is, dx and stp-stx must have opposite 
c         signs. on output these parameters are updated appropriately.
c 
c       sty, fy, and dy are variables which specify the step, 
c         the function, and the derivative at the other endpoint of 
c         the interval of uncertainty. on output these parameters are 
c         updated appropriately.
c 
c       stp, fp, and dp are variables which specify the step, 
c         the function, and the derivative at the current step. 
c         if brackt is set true then on input stp must be 
c         between stx and sty. on output stp is set to the new step.
c 
c       brackt is a logical variable which specifies if a minimizer 
c         has been bracketed. if the minimizer has not been bracketed 
c         then on input brackt must be set false. if the minimizer
c         is bracketed then on output brackt is set true. 
c 
c       stpmin and stpmax are input variables which specify lower 
c         and upper bounds for the step.  
c 
c       info is an integer output variable set as follows:  
c         if info = 1,2,3,4,5, then the step has been computed
c         according to one of the five cases below. otherwise 
c         info = 0, and this indicates improper input parameters. 
c 
c     subprograms called
c 
c       fortran-supplied ... abs,max,min,sqrt 
c 
c     argonne national laboratory. minpack project. june 1983 
c     jorge j. more', david j. thuente
c 
c     **********
      real gamma,p,q,r,s,sgnd,stpc,stpf,stpq,theta
      info = 0
c 
c     check the input parameters for errors.
c 
      if ((brackt .and. (stp .le. min(stx,sty) .or. 
     *    stp .ge. max(stx,sty))) .or.
     *    dx*(stp-stx) .ge. 0.0 .or. stpmax .lt. stpmin) return 
c 
c     determine if the derivatives have opposite sign.
c 
      sgnd = dp*(dx/abs(dx))
c 
c     first case. a higher function value.
c     the minimum is bracketed. if the cubic step is closer 
c     to stx than the quadratic step, the cubic step is taken,
c     else the average of the cubic and quadratic steps is taken. 
c 
      if (fp .gt. fx) then
         info = 1 
         bound = .true. 
         theta = 3*(fx - fp)/(stp - stx) + dx + dp  
         s = max(abs(theta),abs(dx),abs(dp))
         gamma = s*sqrt((theta/s)**2 - (dx/s)*(dp/s)) 
         if (stp .lt. stx) gamma = -gamma 
         p = (gamma - dx) + theta 
         q = ((gamma - dx) + gamma) + dp  
         r = p/q
         stpc = stx + r*(stp - stx) 
         stpq = stx + ((dx/((fx-fp)/(stp-stx)+dx))/2)*(stp - stx) 
         if (abs(stpc-stx) .lt. abs(stpq-stx)) then 
            stpf = stpc 
         else 
           stpf = stpc + (stpq - stpc)/2  
           end if 
         brackt = .true.
c 
c     second case. a lower function value and derivatives of  
c     opposite sign. the minimum is bracketed. if the cubic 
c     step is closer to stx than the quadratic (secant) step, 
c     the cubic step is taken, else the quadratic step is taken.
c 
      else if (sgnd .lt. 0.0) then
         info = 2 
         bound = .false.
         theta = 3*(fx - fp)/(stp - stx) + dx + dp  
         s = max(abs(theta),abs(dx),abs(dp))
         gamma = s*sqrt((theta/s)**2 - (dx/s)*(dp/s)) 
         if (stp .gt. stx) gamma = -gamma 
         p = (gamma - dp) + theta 
         q = ((gamma - dp) + gamma) + dx  
         r = p/q
         stpc = stp + r*(stx - stp) 
         stpq = stp + (dp/(dp-dx))*(stx - stp)
         if (abs(stpc-stp) .gt. abs(stpq-stp)) then 
            stpf = stpc 
         else 
            stpf = stpq 
            end if
         brackt = .true.
c 
c     third case. a lower function value, derivatives of the  
c     same sign, and the magnitude of the derivative decreases. 
c     the cubic step is only used if the cubic tends to infinity
c     in the direction of the step or if the minimum of the cubic 
c     is beyond stp. otherwise the cubic step is defined to be
c     either stpmin or stpmax. the quadratic (secant) step is also
c     computed and if the minimum is bracketed then the the step
c     closest to stx is taken, else the step farthest away is taken.
c 
      else if (abs(dp) .lt. abs(dx)) then 
         info = 3 
         bound = .true. 
         theta = 3*(fx - fp)/(stp - stx) + dx + dp  
         s = max(abs(theta),abs(dx),abs(dp))
c 
c        the case gamma = 0 only arises if the cubic does not tend
c        to infinity in the direction of the step.  
c 
         gamma = s*sqrt(max(0.0,(theta/s)**2 - (dx/s)*(dp/s)))
         if (stp .gt. stx) gamma = -gamma 
         p = (gamma - dp) + theta 
         q = (gamma + (dx - dp)) + gamma  
         r = p/q
         if (r .lt. 0.0 .and. gamma .ne. 0.0) then  
            stpc = stp + r*(stx - stp)
         else if (stp .gt. stx) then
            stpc = stpmax 
         else 
            stpc = stpmin 
            end if
         stpq = stp + (dp/(dp-dx))*(stx - stp)
         if (brackt) then 
            if (abs(stp-stpc) .lt. abs(stp-stpq)) then
               stpf = stpc
            else
               stpf = stpq
               end if 
         else 
            if (abs(stp-stpc) .gt. abs(stp-stpq)) then
               stpf = stpc
            else
               stpf = stpq
               end if 
            end if
c 
c     fourth case. a lower function value, derivatives of the 
c     same sign, and the magnitude of the derivative does 
c     not decrease. if the minimum is not bracketed, the step 
c     is either stpmin or stpmax, else the cubic step is taken. 
c 
      else  
         info = 4 
         bound = .false.
         if (brackt) then 
            theta = 3*(fp - fy)/(sty - stp) + dy + dp 
            s = max(abs(theta),abs(dy),abs(dp)) 
            gamma = s*sqrt((theta/s)**2 - (dy/s)*(dp/s))
            if (stp .gt. sty) gamma = -gamma
            p = (gamma - dp) + theta
            q = ((gamma - dp) + gamma) + dy 
            r = p/q 
            stpc = stp + r*(sty - stp)
            stpf = stpc 
         else if (stp .gt. stx) then
            stpf = stpmax 
         else 
            stpf = stpmin 
            end if
         end if 
c 
c     update the interval of uncertainty. this update does not
c     depend on the new step or the case analysis above.
c 
      if (fp .gt. fx) then
         sty = stp
         fy = fp
         dy = dp
      else  
         if (sgnd .lt. 0.0) then
            sty = stx 
            fy = fx 
            dy = dx 
            end if
         stx = stp
         fx = fp
         dx = dp
         end if 
c 
c     compute the new step and safeguard it.
c 
      stpf = min(stpmax,stpf) 
      stpf = max(stpmin,stpf) 
      stp = stpf
      if (brackt .and. bound) then
         if (sty .gt. stx) then 
            stp = min(stx+0.66*(sty-stx),stp) 
         else 
            stp = max(stx+0.66*(sty-stx),stp) 
            end if
         end if 
      return
c 
c     last card of subroutine vd05bd. 
c 
      end 
c
c
c
c
       real function ddot(n,d,i1,s,i2)
c
c      -------------------------------------------------------
c      this function computes the inner product of two vectors
c      -------------------------------------------------------
c
       real d(n),s(n),prod
       integer i1,i2  
c
        prod=0.0e0
        do 10 i=1,n 
 10     prod= prod+d(i)*s(i)
c
        ddot= prod
c
       return 
       end  
c
c


