!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!     ==================================================================
!     PURPOSE:
!       Using the U0ref and V0ref to get the Vref
!     ==================================================================
!
!     Author :  Kang Nai
!     Date   :  Jun. 30, 2011
!     Action :  Created.
!
!     History:
!     --------
!     ------------------------------------------------------------------
      subroutine third_conjugate_31

      use Xu_variables

      implicit none

      integer :: i,ii,j,jj,k,m,n,ip,jp,kp,npath
      real, dimension(1:np) :: veltmp1,veltmp2,veltmp3,veltmp4
      real, dimension(1:np) :: veltmp5,veltmp6,veltmp7,veltmp8
      real    :: azm,a1,a2
      character(len=80) :: name1,name2,name3
      integer :: ma1,ma2

!     variation
      integer,parameter :: maxfn=200
      real,allocatable,dimension(:)  :: x0,x,g
      real                  :: costf,gradtl,err,f1,f2,f4
      real                  :: cos_elv,sigma
      real                  :: sigmabk,twonyq
      real                  :: alfa

      real                  :: a5,dlta_v,b2
      real                  :: sigmabk_threshold

!     ##################################################################
!     Execute. Give some parameters.
!     ##################################################################
!     sigma=2.0
!     sigmabk=5.0
      sigma=sqrt(2.0)
      alfa=0.25
      allocate( x0(mspc) )
      allocate( x(mspc) )
      allocate( g(mspc) )

      k=k_tilt
      f1=spval; f2=-spval
      if ( vabs(k,ilevel)<spval .and. Acnjgt(k,ilevel)>1 ) then

        nbeam = nphi(k)
        elvng = thet(k)
        ii = select_circ(k,ilevel)
        cos_elv=cos(elvng*rdn)
        vnyq=thet_nyq_vel(k)
        twonyq=2.0*vnyq

        do j=1,nbeam
          veltmp1(j)=vel(ii,j,k)
          veltmp2(j)=phi(j,k)
        enddo

!       choice the background
        kp=Acnjgt(k,ilevel)
        do j=1,mspc
          veltmp3(j)=analy_back(j,kp,ilevel)
        enddo

!       variance record
        call circle_linear_int( mspc,veltmp3,phistn                  &
                               ,nbeam,veltmp1,veltmp2                &
                               ,veltmp6,spval )
        a1=0.0
        a5=0.0
        do j=1,nbeam
          if ( abs(veltmp1(j))<spval ) then
            dlta_v=veltmp1(j)-veltmp6(j)
            if ( dlta_v>=0.0 ) then
              n=int(dlta_v/twonyq+0.5)
            else
              n=int(dlta_v/twonyq-0.5)
            endif
            b2=(dlta_v-float(n)*twonyq)
            a1=a1+b2*b2
            a5=a5+1.0
          endif
        enddo
        f4=a1/a5
!       choice the background sigmabk
        sigmabk_threshold=(vnyq/4.0)**2
        sigmabk=f4-sigma*sigma
        sigmabk=max(sigmabk_threshold,sigmabk)
        sigmabk=sqrt(sigmabk)

        x0=0.0        ! Vref=Vbk
        gradtl=1e-15
!       dfpred=0.1
        call lbfgs_bigZG ( mspc,x0,gradtl,maxfn,0.1,x,g,costf,f1     &
                          ,nbeam,veltmp1,veltmp2,cos_elv,vnyq,rdn    &
                          ,veltmp3,phistn,sigmabk                    &
                          ,utri_matrix,ltri_matrix,sigma             &
                          ,veltmp4,spval                             &
                          ,ilevel,k,alfa )

!       interpolate Vr-analyses
        call circle_linear_int( mspc,veltmp4,phistn                  &
                               ,nbeam,veltmp1,veltmp2                &
                               ,veltmp8,spval )

!       variance check
        a1=0.0
        a5=0.0
        do j=1,nbeam
          if ( abs(veltmp1(j))<spval ) then
            dlta_v=veltmp1(j)-veltmp8(j)
            if ( dlta_v>=0.0 ) then
              n=int(dlta_v/twonyq+0.5)
            else
              n=int(dlta_v/twonyq-0.5)
            endif
            b2=(dlta_v-float(n)*twonyq)
            a1=a1+b2*b2
            a5=a5+1.0
          endif
        enddo
        f1=a1/a5

        if ( ilevel<100 ) then
          a1=(vnyq/4.0)**2+sigma**2
          if ( f1<a1 ) then
            do j=1,mspc
              analy_back(j,k,ilevel)=veltmp4(j)
            enddo
            do j=1,nbeam
              velref(ii,j,k)=veltmp8(j)
            enddo
            c_fnct(k,ilevel)=f1
!           write(name1,'(i4.4,i2.2)') ilevel,k
!           name2='adj'//name1(1:6)//'.dat'
!           open(1,file=name2,form='formatted')
!             do j=1,nbeam
!               write(1,'(4f10.3)') veltmp2(j),veltmp1(j)            &
!                                  ,veltmp6(j),veltmp8(j)
!             enddo
!           close(1)
          else
            vabs(k,ilevel)=spval
            U0ref(k,ilevel)=spval
            V0ref(k,ilevel)=spval
            betaref(k,ilevel)=spval
            c_fnct(k,ilevel)=f1
            Acnjgt(k,ilevel)=-3
          endif
        else
          a1=(vnyq/3.0)**2+sigma**2
          if ( f1<a1 .and. f2<a1 ) then
            do j=1,mspc
              analy_back(j,k,ilevel)=veltmp4(j)
            enddo
            do j=1,nbeam
              velref(ii,j,k)=veltmp8(j)
            enddo
            c_fnct(k,ilevel)=f1
!           write(name1,'(i4.4,i2.2)') ilevel,k
!           name2='adj'//name1(1:6)//'.dat'
!           open(1,file=name2,form='formatted')
!             do j=1,nbeam
!               write(1,'(4f10.3)') veltmp2(j),veltmp1(j)            &
!                                  ,veltmp6(j),veltmp8(j)
!             enddo
!           close(1)
          else
            vabs(k,ilevel)=spval
            U0ref(k,ilevel)=spval
            V0ref(k,ilevel)=spval
            betaref(k,ilevel)=spval
            c_fnct(k,ilevel)=f1
            Acnjgt(k,ilevel)=-3
          endif
        endif
      endif      ! endif select circle

      deallocate( x0,x,g )

      return
      end subroutine third_conjugate_31
