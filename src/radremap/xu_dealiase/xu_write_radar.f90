
SUBROUTINE xu_WRITE_RADAR_TILT(name0,nr,np,radid,radlat,radlon,radelv,     &
                  irfstgat,irefgatsp,vcpnum,iyr,imon,iday,ihr,imin,isec,  &
                  thet,nphi,imaxrng,thet_nyq_vel,phi,array)

      IMPLICIT NONE

      CHARACTER(len=*)  :: name0
      INTEGER, INTENT(IN) :: nr,np

      INTEGER               :: nphi, imaxrng,vcpnum
      REAL,DIMENSION(nr,np) :: array
      REAL,DIMENSION(nphi)  :: phi

      CHARACTER(LEN=4) :: radid
      REAL             :: radlat,radlon,radelv
      INTEGER          :: irfstgat,irefgatsp
      INTEGER          :: iyr,imon,iday,ihr,imin,isec
      REAL             :: thet,thet_nyq_vel

      !-----------------------------------------------------------------
      CHARACTER(len=256)  :: name2
      INTEGER    :: i,j
      REAL       :: rfstgat, refgatsp

      i = FLOOR(thet)
      j = NINT((thet-i)*100)
      WRITE(name2,'(a4,3a,I2.2,a,I2.2)') radid,'_',name0,'_',i,'.',j

      rfstgat  = irfstgat
      refgatsp = irefgatsp
      WHERE(array < -700.0) array = 999.0

      open(31,file=name2,form='formatted')
        write(31,'(a4)') radid
        write(31,'(i8)') vcpnum
        write(31,'(6i8)') iyr,imon,iday,ihr,imin,isec
        write(31,'(2f10.3,f10.1)') radlat,radlon,radelv
        write(31,'(2f8.1)') rfstgat,refgatsp
        write(31,'(f8.3)') thet
        write(31,'(2i8)') nphi,imaxrng
        write(31,'(f8.3)') thet_nyq_vel
        write(31,'(15f6.1)') (phi(j),j=1,nphi)
        write(31,'(20f6.1)') ((array(i,j),i=1,imaxrng),j=1,nphi)
      close(31)

END SUBROUTINE xu_WRITE_RADAR_TILT
