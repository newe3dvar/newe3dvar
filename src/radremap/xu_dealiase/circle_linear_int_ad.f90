!#######################################################################
!#######################################################################
!######                                                           ######
!######           subroutine  circle_linear_int_ad                ######
!######                                                           ######
!#######################################################################
!#######################################################################
      SUBROUTINE  circle_linear_int_ad( nvar,vrbk,phibk              &
                                       ,ncircle,vrob,phiob           &
                                       ,spval,avrin,avrbk )
!-----------------------------------------------------------------------
!     PURPOSE:
!       linear interpolation along one circle.
!-----------------------------------------------------------------------
      implicit none

      INTEGER :: nvar
      real,dimension(1:nvar) :: vrbk,phibk       ! back ground
      real,dimension(1:nvar) :: avrbk       ! back ground adjoint

      INTEGER :: ncircle
      real,dimension(1:ncircle) :: vrob,phiob  ! observation

      real,dimension(1:ncircle) :: avrin        ! analyses adjoint
      REAL :: spval

      integer :: i,j,j1,j2
      REAL :: a,c3

!     print*,nvar
!     do i=1,nvar
!       print*,phibk(i),vrbk(i)
!     enddo

      avrbk=0.0
      do i=1,ncircle
        if ( phiob(i)<phibk(1) .or. phiob(i) >phibk(nvar)) then
          j1=nvar
          j2=1
          c3=0.0
          if(phiob(i) < 90.0) c3=360.0
          a=(phiob(i)+c3-phibk(j1))/(phibk(j2)+360.0-phibk(j1))
        else
          do j=1,nvar-1
            if(phiob(i)>= phibk(j) .and. phiob(i)<= phibk(j+1)) then
              j1=j
              j2=j+1
              a=(phiob(i)-phibk(j1))/(phibk(j2)-phibk(j1))
              exit
            endif
          enddo
        endif
        avrbk(j1)=avrbk(j1)+(1.0-a)*avrin(i)
        avrbk(j2)=avrbk(j2)+a*avrin(i)
        avrin(i)=0.0
        
      enddo

      RETURN
      END SUBROUTINE  circle_linear_int_ad
