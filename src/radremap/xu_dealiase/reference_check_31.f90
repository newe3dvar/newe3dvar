!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!     ==================================================================
!     PURPOSE:
!       Using referent radial velocity field to check if the raw radial
!       velocity needs to be unfold. The criterion is 0.5Nyqv. The raw
!       data through the criterion will be seeded.
!     ==================================================================
!
!     Author :  Jiandong Gong
!     Date   :
!     Action :  Created.
!
!     History:
!     --------
!     name   : Kang Nai
!     date   : Nov. 13, 2007
!     action : added a distance limit for the different tilt and adjust
!              the criterion.
!
!     ------------------------------------------------------------------
      SUBROUTINE REFERENCE_CHECK_31(wrkchek,ibgn,iend)

      USE Xu_variables

      IMPLICIT NONE

      real, dimension(1:nr,1:np) :: wrkchek
      integer :: ii,k,iii
      integer :: ibgn,iend
      character(len=40) :: name1,name2
      real    :: limit_range,vrqcthrs

      k=k_tilt

      limit_range=250000.0
      vrqcthrs=0.5*thet_nyq_vel(k)
      ii=int(thet(k)*100.0)
!     ------------------------------------------------------------------
!     get the start gate number and end gate number.
!     ------------------------------------------------------------------
      if ( ii<300 ) then
           ibgn=20
      endif
      if ( ii>=300 ) then
           ibgn=10
      endif

      iend=ibgn
      do ii=nrang,1,-1
        if ( abs(vadvel(ii,10))<spval ) then
          iend=ii
          exit
        endif
      enddo

      CALL dealias_31 ( nr,np,obsvel,wrkvel,nbeam,iminrng,nrang      &
                       ,vadvel,vnyq,spval,index,ipoint               &
                       ,wrkchek,ibgn,iend,vrqcthrs )
 
      CALL vadtilt_check ( nr,np,wrkvel,nbeam,iminrng,nrang          &
                          ,vadvel,vnyq,spval,ran,elvng,index         &
                          ,wrkchek,ibgn,limit_range )

      END SUBROUTINE REFERENCE_CHECK_31
