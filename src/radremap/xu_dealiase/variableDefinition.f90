MODULE XU_VARIABLES
!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!-----------------------------------------------------------------------
!  PURPOSE: Common reference point for constants and arrays
!
!  HISTORY: 04/29/2003 - Creation.                      Gong, Jiandong
!           03/15/2007 - Modify the comments.           Zhang, Pengfei
!           08/14/2007 - introduce tendence method.     Nai, Kang
!           12/13/2012 - re-organized.                  Nai, Kang
!-----------------------------------------------------------------------

   IMPLICIT NONE
!-----------------------------------------------------------------------
!  [1.0]  Fundamental constants
!-----------------------------------------------------------------------

!  Parameter:
   REAL, PARAMETER    :: pi = 3.1415926535897932346
   REAL, PARAMETER    :: rdn = pi/180.0
   REAL, PARAMETER    :: invrdn = 1.0/rdn
   REAL, PARAMETER    :: spval = 999.0        ! no-availble data flag
   LOGICAL            :: outdiag = .FALSE.
!-----------------------------------------------------------------------
!  [2.0]  Parameter for map projector
!-----------------------------------------------------------------------

!  Variable:
   INTEGER            :: mapproj  ! map projection
                                  ! 0:no map projection
                                  ! 1:polar projection
                                  ! 2:Lambert projection
                                  ! 3:Mercator projection
   REAL               :: trulat1  ! trucation of latitude
   REAL               :: trulat2  ! trucation of latitude
   REAL               :: trulon   ! trucation of longtitude
   REAL               :: sclfct   ! 1~meter
   REAL,DIMENSION(2)  :: latnot

   REAL               :: xr0        ! x position
   REAL               :: yr0        ! y position

!-----------------------------------------------------------------------
!  [3.0]  Basic Parameters of the radar
!-----------------------------------------------------------------------

!  Parameter:
   INTEGER :: nr   != 2000 ! gates in one beam of vilocity
   INTEGER :: np   != 800  ! number of beams of one tilt
   INTEGER :: nt   !=  20  ! number of the elevation
   INTEGER :: nr2  != 2000 ! gates in one beam for reflectivity

!  Variable:
   INTEGER            :: ireftim    ! reference time
   INTEGER            :: itime      ! real time

   INTEGER            :: vcpnum     ! Volume Coverage Patten
   CHARACTER(LEN=4)   :: radid      ! Input radar name
   REAL               :: rfstgat    ! 1st gate distance of velocity
   REAL               :: refgatsp   ! radial velocity gate spacing
   REAL               :: rfstgat2   ! 1st gate distance of reflectivity
   REAL               :: refgatsp2  ! reflectivity gate spacing
   INTEGER            :: iminrng    ! first variable gate index of velocity
   INTEGER            :: iminrng2   ! first variable gate index of reflectivity
   INTEGER            :: nrang      ! last variable gate index of velocity
   INTEGER            :: nrang2     ! last variable gate index of reflectivity
   INTEGER            :: nbeam      ! variable beam index along azimuth
   INTEGER            :: nthet      ! variable elevation angle index

   REAL               :: radlat     ! radar position for latitude
   REAL               :: radlon     ! radar position for longtitude
   REAL               :: radelv     ! radar position for vertical height

!  Work Parameter
   REAL               :: azmth      ! azimuth
   REAL               :: elvng      ! elevate angle
   REAL               :: hgtrad     ! radar beam heigh
   REAL               :: sfcrng     ! radar beam surface project
   REAL               :: range      ! distance along beam
   REAL               :: vnyq       ! Nyquist velocity
   REAL               :: vnyq_min   ! minimum Nyquist velocity
   REAL               :: vnyq_max   ! maximum Nyquist velocity
   REAL               :: dhdr       ! dh/dr
   !REAL               :: latgate    ! latitude
   !REAL               :: longate    ! longatitude
   INTEGER            :: n10km      ! 10 km range's gate index.
   INTEGER            :: k_tilt     ! k tilt index
!-----------------------------------------------------------------------
!  [4.0] arrays for radar:
!-----------------------------------------------------------------------

!  Arrays for raw radar data:
   REAL,    ALLOCATABLE :: ran(:)             ! range distance for velocity
   REAL,    ALLOCATABLE :: ran2(:)            ! range distance for reflect
   REAL,    ALLOCATABLE :: phi(:,:)           ! azimuth angle (Degree) of velocity
   REAL,    ALLOCATABLE :: phi2(:,:)          ! azimuth angle (Degree) of reflect
   REAL,    ALLOCATABLE :: thet(:)            ! elevation (Degree) of velocity
   REAL,    ALLOCATABLE :: thet2(:)           ! elevation (Degree) of reflect
   REAL,    ALLOCATABLE :: thet_nyq_vel(:)    ! Nyquist velocity(thet)
   INTEGER, ALLOCATABLE :: nphi(:)            ! number of beams of velocity
   INTEGER, ALLOCATABLE :: nphi2(:)           ! number of beams of reflect
   INTEGER, ALLOCATABLE :: imaxrng(:)         ! max number of range of velocity
   INTEGER, ALLOCATABLE :: imaxrng2(:)        ! max number of range of reflect
   INTEGER, ALLOCATABLE :: iyr(:)             ! Observation year
   INTEGER, ALLOCATABLE :: imon(:)            ! Observation month
   INTEGER, ALLOCATABLE :: iday(:)            ! Observation day
   INTEGER, ALLOCATABLE :: ihr(:)             ! Observation hour
   INTEGER, ALLOCATABLE :: imin(:)            ! Observation minute
   INTEGER, ALLOCATABLE :: isec(:)            ! Observation second

!  Work array
   REAL,    POINTER        :: vel (:,:,:)    ! velocity
   REAL,    POINTER        :: ref (:,:,:)    ! reflectivity
   REAL,    POINTER        :: swg (:,:,:)    ! spectral width

   REAL,    POINTER        :: velref (:,:,:) ! reference radial velocity
   REAL,    POINTER        :: orgvel (:,:,:) ! original radial velocity
   REAL,    POINTER        :: wrkhgt(:,:)    ! radial point's height
   REAL,    POINTER        :: wrksfc(:,:)    ! radial point's hozizonal distance.
   INTEGER, POINTER        :: k_121(:)          ! vcp=121 be chosen level
   REAL,    POINTER        :: obsvel(:,:)
   REAL,    POINTER        :: vadvel(:,:)
   REAL,    POINTER        :: unfvel(:,:)
   REAL,    POINTER        :: wrkvel(:,:)
   INTEGER, POINTER        :: index (:,:)

   REAL,    POINTER        :: mdlvel(:)      ! work array for model check
   REAL                    :: mdlheight      ! tmp height

!------------------------------------------------------------------------------
!  [5.0] parameters to control data stream
!------------------------------------------------------------------------------

!  Variables:
   LOGICAL                 :: no_qualify
   LOGICAL                 :: need_dealiase
   INTEGER                 :: ivadflag=0
   INTEGER                 :: ipoint=0
   INTEGER                 :: nprfl

!------------------------------------------------------------------------------
!  [6.0] parameter for wind profile
!------------------------------------------------------------------------------

!  Variables:
   INTEGER, PARAMETER        :: zlevel = 600 ! max level index (600*25=10000 m)
   REAL,    PARAMETER        :: zstep =25.0  ! dltz (meters)
   REAL,    PARAMETER        :: hstart=250.0 ! start height
   INTEGER                   :: ilevel       ! z level index

   INTEGER                   :: start_tilt   ! Robust VAD's start tilt(high)
   INTEGER                   :: end_tilt     ! Robust VAD's end tilt(low)
   INTEGER                   :: step_tilt    ! Robust VAD's tilt step(-1)
!------------------------------------------------------------------------------
!  [7.0] Array for wind profile
!------------------------------------------------------------------------------

!  Arrays:
   INTEGER, ALLOCATABLE :: select_circ(:,:)  ! selected circle index
   REAL,    ALLOCATABLE :: ustor(:)          ! selected u
   REAL,    ALLOCATABLE :: vstor(:)          ! selected v
   REAL,    ALLOCATABLE :: hstor(:)          ! heigh
   INTEGER, ALLOCATABLE :: vad_intp(:)       ! index of VAD

!  Work array:
   REAL,    ALLOCATABLE :: miss_gap(:,:)     ! max no-data gap in the select_circ
   REAL,    ALLOCATABLE :: vabs    (:,:)     ! Robust VAD's wind speed
   REAL,    ALLOCATABLE :: U0ref   (:,:)     ! guess u
   REAL,    ALLOCATABLE :: V0ref   (:,:)     ! guess v
   INTEGER, ALLOCATABLE :: Acnjgt  (:,:)     ! first borrowed circle index
   INTEGER, ALLOCATABLE :: Bcnjgt  (:,:)     ! second borrowed circle index
   REAL,    ALLOCATABLE :: c_fnct  (:,:)     ! min cost-function
   REAL,    ALLOCATABLE :: betaref (:,:)     ! Robust VAD's beta angle

   REAL,    ALLOCATABLE :: cf1stor(:)
   REAL,    ALLOCATABLE :: cf2stor(:)
   REAL,    ALLOCATABLE :: cf3stor(:)

   REAL,    ALLOCATABLE  :: check_small_phi(:)  ! Robust VAD's fai0
   REAL,    ALLOCATABLE  :: check_large_phi(:)  ! Robust VAD's fai0
   REAL,    ALLOCATABLE  :: check_crit(:)       ! Robust VAD's trent index
   REAL,    ALLOCATABLE  :: cost_f(:)

   INTEGER, ALLOCATABLE :: model_circ(:,:)     ! model selected circle index
   REAL,    ALLOCATABLE :: model_gap(:,:)      ! max no-data gap in the model_circ

!------------------------------------------------------------------------------
!  [8.0] count parameters
!------------------------------------------------------------------------------

!  Arrays:
   INTEGER, ALLOCATABLE :: count_select_circle(:)
   INTEGER, ALLOCATABLE :: count_total_points(:,:)
   INTEGER, ALLOCATABLE :: count_special_points(:,:)

!------------------------------------------------------------------------------
!  [9.0] Var analyses parameters
!------------------------------------------------------------------------------

!  Variables:
   INTEGER                      :: mspc
   INTEGER                      :: itrun

!  Arrays:
   REAL, POINTER                :: phistn(:)
   REAL, POINTER                :: ltri_matrix(:,:)
   REAL, POINTER                :: utri_matrix(:,:)
   REAL, POINTER                :: analy_back(:,:,:)
   REAL, POINTER                :: comb_back(:,:,:)

!------------------------------------------------------------------------------
!  [9.0] RUC model u and v
!------------------------------------------------------------------------------

   INTEGER :: bgrid_model
   integer :: nlon_mdl,nlat_mdl
   INTEGER :: z_model
   REAL,   POINTER   :: x_mdl(:),y_mdl(:)
   REAL,   POINTER   :: ubgrid(:,:,:)
   REAL,   POINTER   :: vbgrid(:,:,:)
   REAL,   POINTER   :: zbgrid(:,:,:)

   LOGICAL           :: model_interpolate

   INTEGER :: iproj_mdl
   REAL    :: scale_mdl, latnt_mdl(2), trlon_mdl
   REAL    :: x0_mdl, y0_mdl

  CONTAINS

    !###################################################################

    SUBROUTINE xu_variable_constants(radname, latrad, lonrad, elvrad,ivcp, &
                            maxrgate,maxvgate,maxazim,maxelev,kntvelv,     &
                            istatus)

      CHARACTER(LEN=4), INTENT(IN) :: radname
      REAL,    INTENT(IN)  :: latrad, lonrad, elvrad
      INTEGER, INTENT(IN)  :: ivcp
      INTEGER, INTENT(IN)  :: maxrgate,maxvgate,maxazim,maxelev
      INTEGER, INTENT(IN)  :: kntvelv
      INTEGER, INTENT(OUT) :: istatus

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

      istatus = 0

      radid     = radname
      radlat    = latrad
      radlon    = lonrad
      radelv    = elvrad
      vcpnum    = ivcp

      nr   = maxvgate         != 2000 ! gates in one beam of vilocity
      np   = maxazim          != 800  ! number of beams of one tilt
      nt   = maxelev          !=  20  ! number of the elevation
      nr2  = maxrgate         != 2000 ! gates in one beam for reflectivity

      nthet = kntvelv

      ALLOCATE( ran         (nr)   , STAT = istatus)  ! range distance for velocity
      ALLOCATE( ran2        (nr2)  , STAT = istatus)  ! range distance for reflect
      ALLOCATE( phi         (np,nt), STAT = istatus)  ! azimuth angle (Degree) of velocity
      ALLOCATE( phi2        (np,nt), STAT = istatus)  ! azimuth angle (Degree) of reflect
      ALLOCATE( thet        (nt)   , STAT = istatus)  ! elevation (Degree) of velocity
      ALLOCATE( thet2       (nt)   , STAT = istatus)  ! elevation (Degree) of reflect
      ALLOCATE( thet_nyq_vel(nt)   , STAT = istatus)  ! Nyquist velocity(thet)
      ALLOCATE( nphi        (nt)   , STAT = istatus)  ! number of beams of velocity
      ALLOCATE( nphi2       (nt)   , STAT = istatus)  ! number of beams of reflect
      ALLOCATE( imaxrng     (nt)   , STAT = istatus)  ! max number of range of velocity
      ALLOCATE( imaxrng2    (nt)   , STAT = istatus)  ! max number of range of reflect
      ALLOCATE( iyr         (0:nt) , STAT = istatus)  ! Observation year
      ALLOCATE( imon        (0:nt) , STAT = istatus)  ! Observation month
      ALLOCATE( iday        (0:nt) , STAT = istatus)  ! Observation day
      ALLOCATE( ihr         (0:nt) , STAT = istatus)  ! Observation hour
      ALLOCATE( imin        (0:nt) , STAT = istatus)  ! Observation minute
      ALLOCATE( isec        (0:nt) , STAT = istatus)  ! Observation second

      ALLOCATE( k_121(nt), STAT = istatus)

      ALLOCATE( select_circ        (nt,zlevel), STAT = istatus)     ! selected circle index
      ALLOCATE( ustor              (zlevel)   , STAT = istatus)     ! selected u
      ALLOCATE( vstor              (zlevel)   , STAT = istatus)     ! selected v
      ALLOCATE( hstor              (zlevel)   , STAT = istatus)     ! heigh
      ALLOCATE( vad_intp           (zlevel)   , STAT = istatus)     ! index of VAD

      ALLOCATE( miss_gap           (nt,zlevel), STAT = istatus)     ! max no-data gap in the select_circ
      ALLOCATE( vabs               (nt,zlevel), STAT = istatus)     ! Robust VAD's wind speed
      ALLOCATE( U0ref              (nt,zlevel), STAT = istatus)     ! guess u
      ALLOCATE( V0ref              (nt,zlevel), STAT = istatus)     ! guess v
      ALLOCATE( Acnjgt             (nt,zlevel), STAT = istatus)     ! first borrowed circle index
      ALLOCATE( Bcnjgt             (nt,zlevel), STAT = istatus)     ! second borrowed circle index
      ALLOCATE( c_fnct             (nt,zlevel), STAT = istatus)     ! min cost-function
      ALLOCATE( betaref            (nt,zlevel), STAT = istatus)     ! Robust VAD's beta angle

      ALLOCATE( cf1stor            (zlevel)   , STAT = istatus)
      ALLOCATE( cf2stor            (zlevel)   , STAT = istatus)
      ALLOCATE( cf3stor            (zlevel)   , STAT = istatus)

      ALLOCATE( check_small_phi    (zlevel)   , STAT = istatus)     ! Robust VAD's fai0
      ALLOCATE( check_large_phi    (zlevel)   , STAT = istatus)     ! Robust VAD's fai0
      ALLOCATE( check_crit         (zlevel)   , STAT = istatus)     ! Robust VAD's trent index
      ALLOCATE( cost_f             (6)        , STAT = istatus)

      ALLOCATE( model_circ         (nt,zlevel), STAT = istatus)     ! model selected circle index
      ALLOCATE( model_gap          (nt,zlevel), STAT = istatus)     ! max no-data gap in the model_circ

      ALLOCATE( count_select_circle (nt)       , STAT = istatus)
      ALLOCATE( count_total_points  (nt,4)     , STAT = istatus)
      ALLOCATE( count_special_points(nt,4)     , STAT = istatus)

      !--------------- Allocate working arrays -------------------------
      ALLOCATE(vel(nr,np,nt), STAT = istatus)
      ALLOCATE(ref(nr2,np,nt), STAT = istatus)
      ALLOCATE(swg(nr,np,nt), STAT = istatus)

      RETURN
    END SUBROUTINE xu_variable_constants

    !###################################################################

    SUBROUTINE xu_variable_init(rfrst_ref,rfrst_vel,gtspc_ref,gtspc_vel,&
                    kntrgat,kntvgat,kntrazm,kntvazm,                    &
                    azmrvol,azmvvol,elvmnrvol,elvmnvvol,                &
                    timeset, itimfrst,timevolv, vnyqvol,                &
                    refvol,velvol,istatus)

      INTEGER, INTENT(IN)  :: rfrst_ref, rfrst_vel, gtspc_ref, gtspc_vel

      INTEGER, INTENT(IN)  :: kntrgat(np,nt),kntvgat(np,nt)
      INTEGER, INTENT(IN)  :: kntrazm(nt),   kntvazm(nt)
      REAL,    INTENT(IN)  :: azmrvol(np,nt), azmvvol(np,nt)
      REAL,    INTENT(IN)  :: elvmnrvol(nt),  elvmnvvol(nt)
      INTEGER, INTENT(IN)  :: timeset, itimfrst
      INTEGER, INTENT(IN)  :: timevolv(np,nt)
      REAL,    INTENT(IN)  :: vnyqvol(np,nt)
      REAL,    INTENT(IN)  :: refvol(nr2,np,nt), velvol(nr,np,nt)

      INTEGER, INTENT(OUT) :: istatus

    !-------------------------------------------------------------------

      INTEGER :: i,j,k
      INTEGER :: ktime
      INTEGER :: kyear, kmon, kday, khr, kmin, ksec
      REAL    :: a1

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

      istatus = 0

      rfstgat  = rfrst_vel
      refgatsp = gtspc_vel
      iminrng  = 1
      DO k = 1, nt
        imaxrng(k)  = MAXVAL(kntvgat(:,k))
      END DO
      nphi(:)  = kntvazm(:)
      phi(:,:) = azmvvol(:,:)
      thet(:)  = elvmnvvol(:)
      thet_nyq_vel(:) = 0.0
      DO k = 1, nt
        DO j = 1, np
          IF (vnyqvol(j,k) > 0.0) THEN
            thet_nyq_vel(k) = vnyqvol(j,k)
            !write(0,*) 'vnyqvol = ',j,k,vnyqvol(j,k)
            EXIT
          END IF
        END DO
      END DO
      DO k = 1, nthet
        WRITE(*,'(1x,a,I3,a,F12.5,a,F12.5)')                            &
         'k = ',k,', thet = ',thet(k),', thet_nyq_vel = ',thet_nyq_vel(k)
      END DO

      rfstgat2  = rfrst_ref
      refgatsp2 = gtspc_ref
      iminrng2  = 1
      DO k = 1, nt
        imaxrng2(k)  = MAXVAL(kntrgat(:,k))
      END DO
      nphi2(:)   = kntrazm(:)
      phi2(:,:)  = azmrvol(:,:)
      thet2(:)   = elvmnrvol(:)

      ref(:,:,:) = refvol(:,:,:)
      vel(:,:,:) = velvol(:,:,:)

      CALL abss2ctim(itimfrst,kyear,kmon,kday,khr,kmin,ksec)
      iyr(0)  = kyear
      imon(0) = kmon
      iday(0) = kday
      ihr(0)  = khr
      imin(0) = kmin
      isec(0) = ksec

      IF (timeset > 0) THEN
        DO k = 1, nt
          ktime = itimfrst + timevolv(1,k)
          CALL abss2ctim(ktime,kyear,kmon,kday,khr,kmin,ksec)
          iyr(k)  = kyear
          imon(k) = kmon
          iday(k) = kday
          ihr(k)  = khr
          imin(k) = kmin
          isec(k) = ksec
        END DO
      ELSE
        DO k = 1, nt
          iyr(k)  = iyr(0)
          imon(k) = imon(0)
          iday(k) = iday(0)
          ihr(k)  = ihr(0)
          imin(k) = imin(0)
          isec(k) = isec(0)
        END DO
      END IF

      WHERE(vel < -700) vel = spval

      !DO k = 1, nt
      !  DO j = 1, np
      !    DO i = 1,nr
      !      IF (vel(i,j,k) > -700.0) THEN
      !        WRITE(100,*) '--',k,j,i,vel(i,j,k)
      !      END IF
      !    END DO
      !  END DO
      !END DO

      RETURN
    END SUBROUTINE xu_variable_init

    !###################################################################

    SUBROUTINE xu_vel_output(rfrstgat_vel,rngvvol,azmvvol,velvol,misval,istatus)

      INTEGER, INTENT(OUT) :: rfrstgat_vel
      REAL,    INTENT(OUT) :: rngvvol(nr,nt), azmvvol(np,nt)
      REAL,    INTENT(OUT) :: velvol(nr,np,nt)
      REAL,    INTENT(IN)  :: misval
      INTEGER, INTENT(OUT) :: istatus

    !-------------------------------------------------------------------

      INTEGER :: i,j,k

    !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

      istatus = 0

      rfrstgat_vel = NINT(rfstgat)

      DO k = 1, nt
        DO i = 1, nr
          rngvvol(i,k) = ran(i)
        END DO
      END DO

      DO k = 1, nt
        DO j = 1, np
          azmvvol(j,k) = phi(j,k)
        END DO
      END DO

      velvol(:,:,:) = vel(:,:,:)

      DO k = 1, nt
        DO j = 1, np
          DO i = 1,nr
            !IF (vel(i,j,k) > -700.0 .and. vel(i,j,k) < 900) THEN
            !  !WRITE(0,*) '===',k,j,i,vel(i,j,k)
            !  !do nothing
            !ELSE    ! convert to ARPS missing value
            !  velvol(i,j,k) = misval
            !END IF
            IF (vel(i,j,k) < -700.0 .OR. vel(i,j,k) > 900) THEN
              velvol(i,j,k) = misval
            END IF

          END DO
        END DO
      END DO

      RETURN
    END SUBROUTINE xu_vel_output

    !###################################################################

    SUBROUTINE use_model_mapproj

      CALL setmapr(iproj_mdl,scale_mdl,latnt_mdl,trlon_mdl)
      CALL setorig(1,x0_mdl,y0_mdl)

      RETURN
    END SUBROUTINE use_model_mapproj

    !###################################################################

    SUBROUTINE use_radar_mapproj

      CALL setmapr(mapproj,sclfct,latnot,trulon)
      CALL setorig(1,xr0,yr0)

      RETURN
    END SUBROUTINE use_radar_mapproj

END MODULE XU_VARIABLES
