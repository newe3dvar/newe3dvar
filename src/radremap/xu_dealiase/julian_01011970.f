!######################################################################
!######################################################################
!     ####                                                         ####
!     ####              SUBROUTINE JULIAN_01011970.F               ####
!     ####                                                         ####
!     #################################################################
      subroutine julian_01011970( iyr,imon,iday,ihr,imin,isec,jultime)
!     =================================================================
!     PURPOSE:
!       calculate the julian time from the 01/01/1970 for writting
!       netCDF style file.
!
!     =================================================================
      implicit none
!
      integer :: iyr,imon,iday,ihr,imin,isec
      integer :: jultime
      integer :: julday70,juldaylast
      integer :: jday
!
      integer :: ii
      integer :: p1,p2,p3,p4
      real    :: a1
!
!     get the julian day
!
      jday=julday70(imon,iday,iyr)
!
!     get the 01/01/1970's julian day
!
      juldaylast=julday70(1,1,1970)
!     ==================================================================
!     jday and juldaylast is the same results but differ method .ie.
!     they both are equelly. Using diff method is just for fun.
!     ==================================================================
!
!     get the julian time from the 01/01/1970
!
      jultime=24*3600*(jday-juldaylast) + 3600*ihr + 60*imin + isec
!
!     get the day from julian day
!
!     jday=juldaylast
!     call caldat(jday,imonth,iday,iyear)
!     print*,'mmddyy=',imonth,iday,iyear
      end subroutine julian_01011970

       FUNCTION julday70(mm,id,iyyy)
       INTEGER julday70,id,iyyy,mm,IGREG
       PARAMETER (IGREG=15+31*(10+12*1582))
       INTEGER ja,jm,jy
       jy=iyyy
       !if (jy.eq.0) pause 'julday: there is no year zero'
       if (jy.lt.0) jy=jy+1
       if (mm.gt.2) then
         jm=mm+1
       else
         jy=jy-1
         jm=mm+13
       endif
       julday70=int(365.25*jy)+int(30.6001*jm)+id+1720995
       if (id+31*(mm+12*iyyy).ge.IGREG) then
        ja=int(0.01*jy)
        julday70=julday70+2-ja+int(0.25*ja)
       endif

       return
       end function julday70

      SUBROUTINE caldat(julian,mm,id,iyyy)
      INTEGER id,iyyy,julian,mm,IGREG
      PARAMETER (IGREG=2299161)
      INTEGER ja,jalpha,jb,jc,jd,je
      if(julian.ge.IGREG)then
        jalpha=int(((julian-1867216)-0.25)/36524.25)
        ja=julian+1+jalpha-int(0.25*jalpha)
      else
        ja=julian
      endif
      jb=ja+1524
      jc=int(6680.+((jb-2439870)-122.1)/365.25)
      jd=365*jc+int(0.25*jc)
      je=int((jb-jd)/30.6001)
      id=jb-jd-int(30.6001*je)
      mm=je-1
                                                                                
      if(mm.gt.12)mm=mm-12
      iyyy=jc-4715
      if(mm.gt.2)iyyy=iyyy-1
      if(iyyy.le.0)iyyy=iyyy-1

      return
      END subroutine caldat
