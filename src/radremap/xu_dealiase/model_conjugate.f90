!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!     ==================================================================
!     PURPOSE:
!       Using the U0ref and V0ref to get the Vref
!     ==================================================================
!
!     Author :  Kang Nai
!     Date   :  Mar. 20, 2009
!     Action :  Created.
!
!     History:
!     --------
!     ------------------------------------------------------------------
      subroutine model_conjugate

      use Xu_variables

      implicit none

      integer :: i,ii,j,jj,k,m,n,ip,jp,kp,npath
      real, dimension(1:np) :: veltmp1,veltmp2,veltmp3,veltmp4
      real, dimension(1:np) :: veltmp5,veltmp6,veltmp7,veltmp8
      real    :: azm,a1,a2
      character(len=80) :: name1,name2,name3
      integer :: ma1,ma2

!     variation
      integer,parameter :: maxfn=200
      real,allocatable,dimension(:)  :: x0,x,g
      real                  :: costf,gradtl,err,f1,f2
      real                  :: cos_elv,sigma
      real                  :: sigmabk,twonyq
      real                  :: alfa

!     ##################################################################
!     Execute. Give some parameters.
!     ##################################################################
      sigma=2.0
      sigmabk=5.0
!     alfa=0.25
      alfa=0.5
      allocate( x0(mspc) )
      allocate( x(mspc) )
      allocate( g(mspc) )

      k=k_tilt
      f1=spval
      a1=spval; a2=spval; f2=0.0

      if ( model_circ(k,ilevel)>0 ) then
        nbeam = nphi(k)
        elvng = thet(k)
        ii = model_circ(k,ilevel)
        cos_elv=cos(elvng*rdn)
        vnyq=thet_nyq_vel(k)
        twonyq=2.0*vnyq

        ma1=0
        do j=1,nbeam
          veltmp1(j)=vel(ii,j,k)
          if ( abs(veltmp1(j))<spval ) then
            ma1=ma1+1
          endif
          veltmp2(j)=phi(j,k)
        enddo

!       choice the background
        veltmp3=spval
        do j=1,mspc
          if ( abs(analy_back(j,k,ilevel))<spval ) then
            veltmp3(j)=analy_back(j,k,ilevel)
          endif
        enddo


        x0=0.0        ! Vref=Vbk
        gradtl=1e-15
!       dfpred=0.1
        call lbfgs_bigZG ( mspc,x0,gradtl,maxfn,0.1,x,g,costf,f1     &
                          ,nbeam,veltmp1,veltmp2,cos_elv,vnyq,rdn    &
                          ,veltmp3,phistn,sigmabk                    &
                          ,utri_matrix,ltri_matrix,sigma             &
                          ,veltmp4,spval                             &
                          ,ilevel,k,alfa )

!       interpolate Vr-analyses
        call circle_linear_int( mspc,veltmp4,phistn                  &
                               ,nbeam,veltmp1,veltmp2                &
                               ,veltmp8,spval )
!       interpolate Vr-ref
        call circle_linear_int( mspc,veltmp3,phistn                  &
                               ,nbeam,veltmp1,veltmp2                &
                               ,veltmp7,spval )

!       write(name1,'(i2.2,i4.4)') k,ilevel
!       name2='chsn'//name1(1:6)//'.dat'
!       open(1,file=name2)
!        do j=1,nbeam
!         write(1,'(i4,4f8.1)') j,veltmp2(j),veltmp1(j)              &
!                              ,veltmp8(j),veltmp7(j)
!        enddo
!       close(1)
!       name2='base'//name1(1:6)//'.dat'
!       open(1,file=name2)
!        do j=1,mspc
!         write(1,'(i4,3f8.1)') j,phistn(j),veltmp4(j),veltmp3(j)
!        enddo
!       close(1)

        if ( ilevel<100 ) then
          if ( f1<15.0 ) then
            do j=1,mspc
              analy_back(j,k,ilevel)=veltmp4(j)
            enddo
            do j=1,nbeam
              velref(ii,j,k)=veltmp8(j)
            enddo
          else
            do j=1,mspc
              analy_back(j,k,ilevel)=spval
            enddo
          endif
        elseif ( model_gap(k,ilevel)<280.0 ) then
          if ( f1<10.0 ) then
            do j=1,mspc
              analy_back(j,k,ilevel)=veltmp4(j)
            enddo
            do j=1,nbeam
              velref(ii,j,k)=veltmp8(j)
              mdlvel(j)=veltmp8(j)
            enddo
            mdlheight=wrkhgt(ii,k)
          elseif ( f1<15.0 ) then
            a1=wrkhgt(ii,k)
            a2=mdlheight
            if ( abs(a1-a2)<1000.0 ) then
              do j=1,mspc
                analy_back(j,k,ilevel)=veltmp4(j)
              enddo
              do j=1,nbeam
                velref(ii,j,k)=veltmp8(j)
                mdlvel(j)=veltmp8(j)
              enddo
              mdlheight=wrkhgt(ii,k)
            else
              f2=0.0
              do j=1,nbeam
                f2=f2+(veltmp8(j)-mdlvel(j))**2
              enddo
              f2=sqrt(f2/float(nbeam))/vnyq    ! standard variance
              if ( f2<0.25 ) then
                do j=1,mspc
                  analy_back(j,k,ilevel)=veltmp4(j)
                enddo
                do j=1,nbeam
                  velref(ii,j,k)=veltmp8(j)
                  mdlvel(j)=veltmp8(j)
                enddo
                mdlheight=a1
              else
                do j=1,mspc
                  analy_back(j,k,ilevel)=spval
                enddo
              endif
            endif
          else
            a1=wrkhgt(ii,k)
            a2=mdlheight
            f2=0.0
            do j=1,nbeam
              f2=f2+(veltmp8(j)-mdlvel(j))**2
            enddo
            f2=sqrt(f2/float(nbeam))/vnyq    ! standard variance
            if ( ma1<30 ) then
              if ( f2<0.15 .and. abs(a1-a2)<2000.0 ) then
                do j=1,mspc
                  analy_back(j,k,ilevel)=veltmp4(j)
                enddo
                do j=1,nbeam
                  velref(ii,j,k)=veltmp8(j)
                  mdlvel(j)=veltmp8(j)
                enddo
                mdlheight=a1
              else
                do j=1,mspc
                  analy_back(j,k,ilevel)=spval
                enddo
              endif
            else
              if ( f2<0.25 .and. abs(a1-a2)<2000.0 ) then
                do j=1,mspc
                  analy_back(j,k,ilevel)=veltmp4(j)
                enddo
                do j=1,nbeam
                  velref(ii,j,k)=veltmp8(j)
                  mdlvel(j)=veltmp8(j)
                enddo
                mdlheight=a1
              else
                do j=1,mspc
                  analy_back(j,k,ilevel)=spval
                enddo
              endif
            endif
          endif
        else
          if ( f1<7.5 ) then
            do j=1,mspc
              analy_back(j,k,ilevel)=veltmp4(j)
            enddo
            do j=1,nbeam
              velref(ii,j,k)=veltmp8(j)
              mdlvel(j)=veltmp8(j)
            enddo
            mdlheight=wrkhgt(ii,k)
!           write(name1,'(i2.2,i4.4)') k,ilevel
!           name2='iter'//name1(1:6)//'.dat'
!           open(1,file=name2)
!            do j=1,nbeam
!             write(1,'(i4,3f8.1)') j,veltmp2(j),veltmp8(j),veltmp7(j)
!            enddo
!           close(1)
!           name2='anly'//name1(1:6)//'.dat'
!           open(1,file=name2)
!            do j=1,mspc
!             write(1,'(i4,3f8.1)') j,phistn(j),veltmp4(j),veltmp3(j)
!            enddo
!           close(1)
!           write(name1,'(i4.4,i2.2)') ilevel,k
!           name2='comb'//name1(1:6)//'.dat'
!           open(1,file=name2)
!             write(1,'(2i6,e15.5)') ilevel,k,f1
!           close(1)
          else
            a1=wrkhgt(ii,k)
            a2=mdlheight
            f2=0.0
            do j=1,nbeam
              f2=f2+(veltmp8(j)-mdlvel(j))**2
            enddo
            f2=sqrt(f2/float(nbeam))/vnyq    ! standard variance
            if ( ma1<30 ) then
              if ( f2<0.15 .and. abs(a1-a2)<2000.0 ) then
                do j=1,mspc
                  analy_back(j,k,ilevel)=veltmp4(j)
                enddo
                do j=1,nbeam
                  velref(ii,j,k)=veltmp8(j)
                  mdlvel(j)=veltmp8(j)
                enddo
                mdlheight=a1
              else
                do j=1,mspc
                  analy_back(j,k,ilevel)=spval
                enddo
              endif
            else
              if ( f2<1.0/3.0 .and. abs(a1-a2)<2000.0 ) then
                do j=1,mspc
                  analy_back(j,k,ilevel)=veltmp4(j)
                enddo
                do j=1,nbeam
                  velref(ii,j,k)=veltmp8(j)
                  mdlvel(j)=veltmp8(j)
                enddo
                mdlheight=a1
              else
                do j=1,mspc
                  analy_back(j,k,ilevel)=spval
                enddo
              endif
            endif
          endif
        endif
!       npath=600+k
!       write(npath,'(2i4,7f10.3,i6)') ilevel,k,costf,f1             &
!                                     ,model_gap(k,ilevel),f2        &
!                                     ,a2,a1,mdlheight,ma1
      endif

      deallocate( x0,x,g )

      return
      end subroutine model_conjugate
