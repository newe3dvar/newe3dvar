!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
      SUBROUTINE struct_covar( garma )
!
! ======================================================================
!     PURPOSE:
!       get the structure of the covariance of the background.
! ======================================================================
!
!     Author   : Nai, Kang
!     Date     : May 27, 2009
!     Action   : Created.
!
!     History  :
!    -----------
!
! ----------------------------------------------------------------------

      USE Xu_variables

      implicit none

      real    :: garma

      integer :: i,j,k
      real    :: a1,a2,a3

!     ==================================================================
!     singular value decomposition.
!     ==================================================================
      a2=(360.0/mspc)    ! degree
      do j=1,mspc
        a1=float(j-1)*a2+0.5
        phistn(j)=a1
      enddo
!     radiu                        ! distance from radar to points.
!     L_hoz                        ! horizonal influence radiu
!     garma=L_hoz/radiu

      call covariance( mspc,itrun                                    &
                      ,phistn,utri_matrix,ltri_matrix                &
                      ,garma,rdn )

      END SUBROUTINE struct_covar
