!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
      subroutine rough_min( ncircle,vrob,phiob                       &
                           ,cos_elv,nyqv,rdn                         &
                           ,dvrdazm,phi0                             &
                           ,alfa,rr                                  &
                           ,costf,a0,vbest,beta                      &
                           ,spval )
 
!     ==================================================================
!     PURPOSE:
!       searching the minimun cost-function to get the u0, v0, w0.
!     ==================================================================
!
!     Author :  Kang Nai
!     Date   :  Feb. 13, 2009
!     Action :  Created.
!
!     History:
!     --------
!
!     ------------------------------------------------------------------

      implicit none
!
      integer :: ncircle
      real, dimension(1:ncircle) :: vrob
      real, dimension(1:ncircle) :: phiob
      real                       :: cos_elv  ! cos(elevate angle)
      real                       :: nyqv     ! Nyquist Velocity
      real                       :: rdn      ! 3.14159/180.0
      real                       :: dvrdazm  ! d(vr)/d(azimuth)
      real                       :: phi0     ! azimuth of 0Vr
      real                       :: alfa     ! constant
      real                       :: rr       ! correlation coefficient
      real                       :: costf
      real                       :: a0
      real                       :: vbest
      real                       :: beta
      real                       :: spval

      real,dimension(13) :: tmp_cost,tmp_v,test_v
      real               :: crit

      integer :: i,j,k,kk,kmin
      real    :: a1,a2,a3,a4,a5
      integer :: test_style

!     ##################################################################
!     Execute. Give some parameters.
!     ##################################################################
!     Note:
!       Vr_ref=a0+Vbest*cos(elavation angle)*cos(azimuth-beta).
!       d(Vr_ref)/d(azimuth)=Vbest*cos(elavation angle)*(-sin(a90))
!            if dvr/dazm>0, a90=-pi/2
!            if dvr/dazm<0, a90=pi/2
!       Vbest=abs(d(Vr_ref)/d(azimuth))/cos(elavation angle)
!       this Vbest is the first guess vbest
!       the first guess a0=0.0. 
!       the first guess beta=phi0-90.0 if dvrdazm<0
!                       beta=phi0+90.0 if dvrdazm>0
!     ------------------------------------------------------------------

!     x(1)=abs(dvrdazm/cos_elv)

!     if ( dvrdazm>0.0 ) then
!       x(2)=phi0+90.0
!     else
!       x(2)=phi0-90.0
!     endif
!     if ( x(2)>=360.0 ) x(2)=x(2)-360.0
!     if ( x(2)<0.0 ) x(2)=x(2)+360.0

!     x(3)=0.0

!     numb=3
!     maxfn = 400
!     gradtl = 0.00000000100
!     err = 0.00000000100

!     CALL vmcgg( 3,400,0.00000000100,0.00000000100                  &
!                ,x,g,cost_function                                  &
!                ,nbeam,veltmp1,cos_elv,nyqv ) 
!     vbest=x(1)
!     beta=x(2)
!     a0=x(3)
!     print*,ncircle,dvrdazm,phi0,alfa,rr
!     print*,cos_elv,nyqv,rdn
!     do i=1,ncircle
!       print*,i,phiob(i),vrob(i)
!     enddo

      a0=0.0
      crit=1.0
      if ( dvrdazm<0.0 ) then
        crit=-1.0
      endif
! ----------------------------------------------------------------------
!     design the first guess best V.
! ----------------------------------------------------------------------
      test_v(1)=1.0
      test_v(2)=2.0
      test_v(3)=5.0
      test_v(4)=10.0
      test_v(5)=20.0
      test_v(6)=30.0
      test_v(7)=40.0
      test_v(8)=50.0
      test_v(9)=60.0
      test_v(10)=70.0
      test_v(11)=80.0
      test_v(12)=90.0
      test_v(13)=100.0

      a1=1.0E+10
      kmin=0
      do k=1,13
        tmp_v(k)=test_v(k)
        call cost_function_31( ncircle,vrob,phiob,cos_elv,nyqv,rdn   &
                              ,a0,phi0,crit                          &
                              ,test_v(k),costf,spval )
        tmp_cost(k)=costf
        !write(999,'(2i4,2f20.3)') n,k,test_v(k),costf
        if ( costf<a1 ) then
          a1=costf
          kmin=k
        endif
      enddo
!     ------------------------------------------------------------------
!     choice the parabola regression style:
!     test_style=1;  using 0Vr gradient to do parabola regression.
!     test_style=2;  using three points to do parabola regression.
!     ------------------------------------------------------------------
      test_style=2
      call search_min_31( ncircle,vrob,phiob,cos_elv,nyqv,rdn        &
                         ,a0,phi0,crit                               &
                         ,test_style,kmin,tmp_cost,tmp_v             &
                         ,vbest,costf,spval )

      if ( vbest<spval ) then
        if ( dvrdazm<0.0 ) then
          beta=phi0-90.0
        else
          beta=phi0+90.0
        endif
        if ( beta<0.0 ) beta=360.0+beta
        if ( beta>=360.0 ) beta=beta-360.0
      else
        costf=spval
      endif

      return
      end subroutine rough_min
