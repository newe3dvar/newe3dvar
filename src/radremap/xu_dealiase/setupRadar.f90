!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!=======================================================================
!     PURPOSE:
!       1) shift radar data in the radial direction to make the first
!          gate distance is positive.
!       2) set the radar position as the origin points.
!     ==================================================================
!
!     Author :  Jiandong Gong
!     Date   :
!     Action :  Created.
!
!     History:
!     --------
!
!=======================================================================
      SUBROUTINE SETUP_RADAR

      IMPLICIT NONE

! --------------------------------------------
!     origin radar position
! --------------------------------------------

      CALL ORIG_RADAR

! --------------------------------------------
!     shift radar data if needed.
! --------------------------------------------

      CALL PREP_RADAR

      END SUBROUTINE SETUP_RADAR

! ======================================================================

      SUBROUTINE ORIG_RADAR

      use xu_variables

      IMPLICIT NONE

      trulon=radlon
      latnot(1)=trulat1
      latnot(2)=trulat2

      call setmapr(mapproj,sclfct,latnot,trulon)
      call lltoxy(1,1,radlat,radlon,xr0,yr0)

      call setorig(1,xr0,yr0)
      call lltoxy(1,1,radlat,radlon,xr0,yr0)

      print*
      print*,'lat,lon = ',radlat,radlon
      print*,'xr0,yr0 = ',xr0,yr0
      print*

      END SUBROUTINE ORIG_RADAR
