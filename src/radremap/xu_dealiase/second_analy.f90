!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
      SUBROUTINE Second_analy
!
! ======================================================================
!     PURPOSE:
!       Using selected background cycle data and the original data to
!       do the conjugate job to get the best fitting analysis data.
! ======================================================================
!
!     Author   : Nai, Kang
!     Date     : May. 28, 2009
!     Action   : Created.
!
!     History  :
!    -----------
!
! ----------------------------------------------------------------------

      use Xu_variables

      implicit none

      integer :: start_level,end_level
      integer :: k,jm,jn,klevel,jk
      integer :: ii,j,i1,i2,npath,n
      logical :: first_check,tmp_check
      real    :: beta_tmp,a1

      real,dimension(1:np) :: veltmp1,veltmp2,veltmp3
      real,dimension(1:np) :: veltmp4,veltmp5
      character(len=80)    :: name1,name2

! ######################################################################
!     Execute.
! ######################################################################

      k=k_tilt

      start_level=int((250.0+1.0)/zstep)    ! the lowest level is 250 m
      end_level=zlevel                      ! the toppest height

      first_check=.false.
      do ilevel=start_level,end_level
!       ================================================================
!       get the available top level
!       ================================================================
        if ( vabs(k,ilevel)<spval ) then
          first_check=.true.
          jm=ilevel
          exit
        endif
      enddo

      if ( first_check ) then
!       ================================================================
!       using variation method to get the refvel.
!       ================================================================       
        do ilevel=jm,end_level
          call first_conjugate
        enddo
      endif

      END SUBROUTINE Second_analy
