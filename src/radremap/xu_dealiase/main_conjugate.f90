!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!=======================================================================
!     PURPOSE:
!       Using the VAD profile or priority wind profile as the background
!       and do variational analyses to get the best analysis Vr on the
!       selected cycle.
!=======================================================================
!
!     Author   : Kang Nai
!     Date     : Jun. 08, 2009
!     Action   : Created.
!
!     History  :
!     ----------
!
!-----------------------------------------------------------------------
      SUBROUTINE main_conjugate

      USE Xu_variables

      IMPLICIT NONE

      real,allocatable,dimension(:,:) :: refvel
      real,allocatable,dimension(:,:) :: chkvel

      integer           :: start_level, end_level
      integer :: i,j,k,ref_tilt
!-----------------------------------------------------------------------
      !WRITE (0, '(/,10X,A,/)') '*** Dealiasing Starting ***'

!-----------------------------------------------------------------------
!     [1.0] get the first reference tilt
!-----------------------------------------------------------------------
      end_tilt=1

!-----------------------------------------------------------------------
!     [2.0] dealiasing the available lowest tilt.
!-----------------------------------------------------------------------
      k_tilt=end_tilt

!-----------------------------------------------------------------------
!       [2.1] get the Select cycles first referece vr
!-----------------------------------------------------------------------
        CALL first_guess

!-----------------------------------------------------------------------
!       [2.3] get the selected circle's referent radial velocity
!-----------------------------------------------------------------------
        CALL first_analy

!-----------------------------------------------------------------------
!     [3.0] Do higher tilts.
!-----------------------------------------------------------------------
      ref_tilt=1
      do k_tilt=end_tilt+1,nthet

!-----------------------------------------------------------------------
!       [3.1] get the Select cycles first referece vr
!-----------------------------------------------------------------------
        CALL second_guess(ref_tilt)

!-----------------------------------------------------------------------
!       [3.2] get the selected circle's referent radial velocity
!-----------------------------------------------------------------------
        CALL second_analy

      enddo

!-----------------------------------------------------------------------
!     [4.0] Do model compare with the velref
!-----------------------------------------------------------------------
      IF (bgrid_model > 0) CALL model_compare

      if ( model_interpolate ) then
!-----------------------------------------------------------------------
!       [5.0] Do model based analyses.
!-----------------------------------------------------------------------
        CALL model_analy
      endif

!-----------------------------------------------------------------------
!     [6.0] continue check by using the analysis results.
!-----------------------------------------------------------------------
      CALL main_check_driver

!-----------------------------------------------------------------------
!     [7.0] clean the high elevation angle's data.
!-----------------------------------------------------------------------
      allocate ( refvel(1:nr,1:np) )
      allocate ( chkvel(1:nr,1:np) )

      do k_tilt=1,nthet
        k=k_tilt
        nbeam = nphi(k)
        nrang = imaxrng(k)
        elvng = thet(k)

        chkvel=spval
        do j=1,nbeam; do i=iminrng,nrang
          chkvel(i,j)=vel(i,j,k)
        enddo; enddo
        refvel=spval
        do j=1,nbeam; do i=iminrng,nrang
          refvel(i,j)=ref(i,j,k)
        enddo; enddo
        if ( elvng>15.0 ) then
          do j=1,nbeam; do i=iminrng,nrang
            if ( wrkhgt(i,k)>=15000.0 ) then
              if ( refvel(i,j)<20.0 ) then
                chkvel(i,j)=spval
              endif
            endif
          enddo; enddo
          do j=1,nbeam; do i=iminrng,nrang
            vel(i,j,k)=chkvel(i,j)
          enddo; enddo
        endif
      enddo

      deallocate( refvel,chkvel )

      END SUBROUTINE main_conjugate
