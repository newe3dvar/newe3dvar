!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
      SUBROUTINE READ_WIND_PROFILE

!     ==================================================================
!     PURPOSE:
!       Get horizontal wind field's profile by using the priority VAD.
!
!     ==================================================================
!
!     Author   : Kang Nai
!     Date     : 05/06/2009
!     Action   : Created.
!
!     History  :
!     ----------
!
!     name     : Kang Nai
!     date     : July 17, 2009
!     action   : loan the lower profile from priority profile if needed.
!     ------------------------------------------------------------------
      USE Xu_variables

      IMPLICIT NONE
 
      integer :: i,j,k,i1,ii
      real :: a1,a2,a3,a4,a5
      integer :: prio_yr,prio_mon,prio_day,prio_hr,prio_min,prio_sec
      integer :: curr_yr,curr_mon,curr_day,curr_hr,curr_min,curr_sec
      integer :: curr_time,prio_time
      character(len=80) :: name1,name2,name3

      real,allocatable,dimension(:) :: utmp,vtmp,htmp
      logical :: exst

!     ******************************************************************
!     Execute
!     ******************************************************************
!     searching the priority wind profile data.
!     ------------------------------------------------------------------
      name1=radid(1:4)//'_VAD_profile.keep'
      inquire(file=name1,exist=exst)
      if ( .not. exst ) RETURN         ! no priority wind profile

      allocate( utmp(zlevel) )
      allocate( vtmp(zlevel) )
      allocate( htmp(zlevel) )
      utmp=spval
      vtmp=spval
!     ------------------------------------------------------------------
!     read in the profile data
!     ------------------------------------------------------------------
      open(1,file=name1,status='old')
      read(1,'(6i6)')prio_yr,prio_mon,prio_day,prio_hr,prio_min,prio_sec
      read(1,'(3f8.3)') a1,a2,a3
   10 continue
        read(1,'(4e15.5,i8,f5.1)',end=100) a1,a2,a3,a4,i1,a5
        i=int( (a1+1.0)/zstep )
        htmp(i)=a1
        utmp(i)=a2
        vtmp(i)=a3
!       print*,i,htmp(i),utmp(i),vtmp(i)
        go to 10
  100 continue
      close(1)
      ii=i1
      print*,' '
      print*,'read in the priority profile',ii

!     ------------------------------------------------------------------
!     calculate the diff of the time.
!     ------------------------------------------------------------------
      curr_yr=iyr(0)
      curr_mon=imon(0)
      curr_day=iday(0)
      curr_hr=ihr(0)
      curr_min=imin(0)
      curr_sec=isec(0)

      call julian_01011970( prio_yr,prio_mon,prio_day                &
                           ,prio_hr,prio_min,prio_sec,prio_time )
      call julian_01011970( curr_yr,curr_mon,curr_day                &
                           ,curr_hr,curr_min,curr_sec,curr_time )

      if ( vcpnum==32 ) then
        if ( (curr_time-prio_time)<1800 ) then     ! half hour
          do k=1,zlevel
            if ( abs(ustor(k))>100.0 .and. abs(utmp(k))<spval ) then
              ustor(k)=utmp(k)
              vstor(k)=vtmp(k)
!             hstor(k)=htmp(k)
            endif
          enddo
        endif
      else
        if ( (curr_time-prio_time)<900 ) then     ! 15 minutes
          do k=1,zlevel
            if ( abs(ustor(k))>100.0 .and. abs(utmp(k))<spval ) then
              ustor(k)=utmp(k)
              vstor(k)=vtmp(k)
!             hstor(k)=htmp(k)
            endif
          enddo
        endif
      endif

      deallocate( utmp,vtmp,htmp )

!     ------------------------------------------------------------------
!     wind profile quality check
!     ------------------------------------------------------------------
      call profile_continue_qc
      call profile_differ_qc

!     ------------------------------------------------------------------
!     full the profile
!     ------------------------------------------------------------------
      call full_profile

      do k=zlevel,1,-1
        if ( abs(ustor(k))<spval ) then
          nprfl=k
          exit
        endif
      enddo
      print*,' '
      print*,'after read-in level==',nprfl
      print*,' '

      return
      END SUBROUTINE READ_WIND_PROFILE
