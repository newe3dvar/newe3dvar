!#######################################################################
!#######################################################################
!####                                                               ####
!####                Subroutine choice_bigZG                        ####
!####                                                               ####
!#######################################################################
!#######################################################################

      subroutine choice_bigZG( nvar,x,vrbk,phibk,ZG,ichoice          &
                              ,nsum,vrno,phino,cos_elv,nyqv,rdn      &
                              ,nreal,realvr,realphi                  &
                              ,utri_array,ltri_array                 &
                              ,alfa,indx_real1                       &
                              ,spval )
 
!     ==================================================================
!     PURPOSE:
!       select some datas
!     ==================================================================
!
!     Author :  Kang Nai
!     Date   :  Mar. 17, 2009
!     Action :  Created.
!
!     History:
!     --------
!
!     ------------------------------------------------------------------

      implicit none
!
      integer                   :: nvar
      real,dimension(1:nvar)    :: x
      real,dimension(1:nvar)    :: vrbk,phibk
      integer                   :: ichoice

      integer                   :: nsum
      real,dimension(1:nsum)    :: ZG
      real,dimension(1:nsum)    :: vrno
      real,dimension(1:nsum)    :: phino
      real                      :: cos_elv
      real                      :: nyqv
      real                      :: rdn

      integer                   :: nreal
      real,dimension(1:nsum)    :: realvr,realphi

      real,dimension(1:nvar,1:nvar) :: utri_array,ltri_array

      real                      :: alfa
      integer,dimension(1:nsum) :: indx_real1,indx_real2

      real                      :: spval

      real,dimension(1:nsum)    :: vrin

      integer :: i,j,k,m,n,i1,i2,jm
      real    :: twonyq,thrths
      real    :: a1,a2,a3,a4,a5,b1,b2,b3,b4,b5

      real, dimension(1:nvar)   :: veltmp
      real                      :: dlta_v
      character(len=80)         :: name1,name2,name3
      real,dimension(1:nsum)    :: ZGout

!     ##################################################################
!     Execute. calculate the cost function.
!     ##################################################################
      do i=1,nvar
        a1=0.0
        do j=1,nvar
          a1=a1+ltri_array(i,j)*x(j)
        enddo
        veltmp(i)=vrbk(i)+a1               ! new Vrref
      enddo
!     ------------------------------------------------------------------
!     circle linear_interpolation
!     ------------------------------------------------------------------
      call circle_linear_int( nvar,veltmp,phibk                      &
                             ,nsum,vrno,phino                        &
                             ,vrin,spval )

      b1=0.0
      b3=0.0
      b4=0.0
      b5=0.0
      a5=0.0
      a4=0.0
      twonyq=2.0*nyqv
      thrths=alfa*nyqv

      ZGout=0.0
      do j=1,nsum
        dlta_v=vrno(j)-vrin(j)
        if ( dlta_v>=0.0 ) then
          n=int(dlta_v/twonyq+0.5)
        else
          n=int(dlta_v/twonyq-0.5)
        endif
        b2=(dlta_v-float(n)*twonyq)
        ZGout(j)=b2
      enddo

      indx_real2=0
      do j=1,nsum
        if ( abs(ZGout(j))<=thrths ) then
          indx_real2(j)=1
        endif
      enddo

      i1=0
      i2=0
      do i=1,nsum
        i1=i1+indx_real1(i)
        i2=i2+indx_real2(i)
      enddo
!     print*,'i1==',i1,' i2==',i2
      if ( i2<nsum ) then
        if ( (i2-i1)<=30 ) then
          m=30-(i2-i1)
          do k=1,m
            a1=nyqv
            do j=1,nsum
              if ( indx_real2(j)==0 ) then
                if ( abs(ZGout(j))<a1 ) then
                  a1=abs(ZGout(j))
                  jm=j
                endif
              endif
            enddo
!           alfa=abs(ZGout(jm))/nyqv
!           indx_real2(jm)=1

            b5=abs(ZGout(jm))/nyqv
            if ( b5<=0.75 ) then
              alfa=abs(ZGout(jm))/nyqv
              indx_real2(jm)=1
            endif

          enddo
        endif
      endif

      nreal=0
      do j=1,nsum
        if ( indx_real2(j)==1 ) then
          nreal=nreal+1
          b2=ZGout(j)
          realvr(nreal)=vrno(j)
          realphi(nreal)=phino(j)
          ZG(nreal)=b2
        endif
      enddo

      indx_real1=indx_real2

!     write(name1,'(i2.2)') ichoice
!     name2='realcase'//name1(1:2)//'.dat'
!     open(1,file=name2,form='formatted')
!       do i=1,nreal
!         write(1,'(3f10.3)') realphi(i),realvr(i),ZG(i)
!       enddo
!     close(1)
!     name2='bkcase'//name1(1:2)//'.dat'
!     open(1,file=name2,form='formatted')
!       do i=1,nvar
!         write(1,'(2f10.3)') phibk(i),veltmp(i)
!       enddo
!     close(1)

      return
      end subroutine choice_bigZG
