!#######################################################################
!#######################################################################
!####                                                               ####
!####                Subroutine fcn_bigZG                           ####
!####                                                               ####
!#######################################################################
!#######################################################################

      subroutine fcn_bigZG( nvar,x,costf,f1,kk,maxfn,keep_doing      &
                           ,ncircle,vrob,phiob,cos_elv,nyqv,rdn      &
                           ,vrbk,phibk,ZG,sigmabk                    &
                           ,utri_array,ltri_array,sigma              &
                           ,alfa,indx_real1                          &
                           ,spval )
 
!     ==================================================================
!     PURPOSE:
!       Using the estimate V to get the cost function.
!       The function's equation is:
!       J=(x**2/sigmabk**2 + 
!         sum{KAPA(dltav,nyqv)}**2/sigma**2
!     ==================================================================
!
!     Author :  Kang Nai
!     Date   :  Feb. 24, 2009
!     Action :  Created.
!
!     History:
!     --------
!
!     ------------------------------------------------------------------

      implicit none
!
      integer                       :: nvar
      real,dimension(1:nvar)        :: x
      real,dimension(1:nvar)        :: vrbk,phibk
      real                          :: costf,f1
      integer                       :: kk
      integer                       :: maxfn
      logical                       :: keep_doing
      real,dimension(1:nvar,1:nvar) :: utri_array,ltri_array

      integer                   :: ncircle
      real,dimension(1:ncircle) :: vrob
      real,dimension(1:ncircle) :: phiob
      real,dimension(1:ncircle) :: ZG
      real                      :: cos_elv
      real                      :: nyqv
      real                      :: rdn
      real                      :: sigmabk
      real                      :: sigma
      real                      :: alfa
      real,dimension(1:ncircle) :: indx_real1
      real                      :: spval

      integer :: i
      real    :: a1,b1,b2
      real    :: cost1,cost2

!     ##################################################################
!     Execute. calculate the cost function.
!     ##################################################################
      keep_doing=.true.

      cost1=0.0
      do i=1,nvar
        cost1=cost1+ x(i)*x(i)/sigmabk/sigmabk
      enddo

      call cost_bigZG( nvar,x,vrbk,phibk,ZG,kk                       &
                      ,ncircle,vrob,phiob,cos_elv,nyqv,rdn           &
                      ,utri_array,ltri_array,sigma                   &
                      ,alfa,indx_real1                               &
                      ,cost2,f1,spval )

      costf=cost1+cost2

      if ( kk>=maxfn ) then
        keep_doing=.false.
      endif

      return
      end subroutine fcn_bigZG
