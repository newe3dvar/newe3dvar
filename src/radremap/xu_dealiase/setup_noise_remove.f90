!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
! ======================================================================
!     PURPOSE:
!       remove the noise data in the raw data.
! ======================================================================
!     Author   : Gong, Jiandong et al.
!     Date     :
!     Action   : Created. Based on Gu, Wei's method.
!
!     History  :
!    -----------
!     Aug.20, 2007  Kang Nai modified it to remove soled one or two beam
!                   data.
!
!     Nov.20, 2007  Kang Nai added to remove the 0Vr noise which shows
!                   near the 150 km at lower the 2.5 tilts, and some
!                   time on the higher tilts in the calm weather day.
!
!     Feb.08, 2008  Kang Nai added to remove the bulk 0Vr area.
!
!     Jan.09, 2013  Kang Nai added start available gate number.
! ----------------------------------------------------------------------
      SUBROUTINE SETUP_NOISE_REMOVE

      use Xu_variables

      IMPLICIT NONE

      INTEGER :: i,j,k,kp
      integer :: ii,ispecial,iii
      real,dimension(1:np) :: veltmp
      real    :: a1,a2
      character(len=40) :: name1,name2,name3
      real,dimension(nr,np) :: tmp1,tmp2

! ----------------------------------------------------------------------
!     put the flag value to the very near area(5 km) which elevation
!     angle is below the 3.0 degree.
! ----------------------------------------------------------------------
      do k=1,nthet
        nbeam = nphi(k)
        nrang = imaxrng(k)
        a1=thet_nyq_vel(k)
        ii=int(thet(k)*100.0)
        if ( ii<300 ) then
          iii=10
        endif
        if ( ii>=300 ) then
          iii=1
        endif
        do j=1,nbeam
        do i=iminrng,nrang
          if ( i<iii ) then
            vel(i,j,k)=spval
          elseif ( abs(vel(i,j,k))>a1 ) then
            vel(i,j,k)=spval
!         else
!           count_total_points(k,3)=count_total_points(k,3)+1
          endif
        enddo
        enddo
      enddo

! ----------------------------------------------------------------------
!     noise_remove
! ----------------------------------------------------------------------
      do k=1,nthet
        nbeam = nphi(k)
        nrang = imaxrng(k)
        call noise_rm( nr,np,vel(1,1,k),nbeam,iminrng,nrang,spval)
      enddo

! ----------------------------------------------------------------------
!     make a bulk 0Vr area remove.
! ----------------------------------------------------------------------
      do k=1,nthet
        nbeam = nphi(k)
        nrang = imaxrng(k)
        call area0vr_rm( nr,np,vel(1,1,k),nbeam,iminrng,nrang,spval   &
                        ,tmp1,tmp2)
      enddo

! ----------------------------------------------------------------------
!     remove the 0vr noise which usually happened about 150km.
! ----------------------------------------------------------------------
      do k=1,nthet
        nbeam = nphi(k)
        nrang = imaxrng(k)
        do i=1,nrang
          veltmp=spval
          do j=1,nbeam
            veltmp(j)=vel(i,j,k)
          enddo
          a1=0.0
          a2=0.0
          do j=1,nbeam
            if ( abs(veltmp(j))<0.05 ) then
              a1=a1+1.0
              a2=a2+1.0
            else
              a2=0.0
            endif
            if ( a2>=10 ) then   ! 10 or more 0Vr points connexion.
              veltmp=spval
              exit
            endif
            if ( a1>20 ) then    ! total 0Vr points > 20
              veltmp=spval
              exit
            endif
          enddo
          do j=1,nbeam
            vel(i,j,k)=veltmp(j)
          enddo
        enddo
      enddo

      print*, 'finished noise remove'
      print*,' '

!     do k_tilt=1,nthet
!       k=k_tilt
!       nbeam = nphi(k)
!       nrang = imaxrng(k)
!       elvng = thet(k)
!       kp=int( thet(k)*100.0 )
!       write(name1,'(i4.4)') kp
!       tmp1=spval
!       do j=1,nbeam
!       do i=1,nrang
!         tmp1(i,j)=vel(i,j,k)
!       enddo
!       enddo
!       name2='vadb'//name1(1:4)//'.dat'
!       call write_radar(name2,tmp1)
!     enddo

      END SUBROUTINE SETUP_NOISE_REMOVE
