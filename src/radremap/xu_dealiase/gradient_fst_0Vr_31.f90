!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!     ==================================================================
!     PURPOSE:
!       Using the gradient of the small Vr to decide if the 0Vr is
!       the real one.
!     ==================================================================
!
!     Author :  Kang Nai
!     Date   :  Oct. 15, 2007
!     Action :  Created.
!
!     History:
!     --------
!     name   : Nai,Kang
!     date   : Feb.06, 2009
!     action : Give a reliablity check for the united 0Vr.
!     ------------------------------------------------------------------
      subroutine gradient_fst_0Vr_31

      use Xu_variables

      implicit none

      integer, dimension(np)  :: indx_minvr
      real,    dimension(np)  :: phi_minvr
      integer                 :: guessp
      real,    dimension(20)  :: vpr_gradient,ppr_gradient
      real,    dimension(20)  :: cp_gradient,rp_gradient
      real,    dimension(20)  :: cost_ps,a0ps,vpsbest,betaps
      integer                 :: goodp
      real,    dimension(20)  :: vp_gradient,pp_gradient
      real,    dimension(20)  :: cost_pr,a0pr,vprbest,betapr
      real,    dimension(20)  :: ratp

      integer                 :: guessm
      real,    dimension(20)  :: vmr_gradient,pmr_gradient
      real,    dimension(20)  :: cm_gradient,rm_gradient
      real,    dimension(20)  :: cost_ms,a0ms,vmsbest,betams
      integer                 :: goodm
      real,    dimension(20)  :: vm_gradient,pm_gradient
      real,    dimension(20)  :: cost_mr,a0mr,vmrbest,betamr
      real,    dimension(20)  :: ratm

      integer :: msum
      real    :: cos_elv
      real    :: small_phi,large_phi,crit

      integer :: jm,ncount,mcount,klevel
      real    :: a1,a2,a3,a4,a5,a6
      real    :: b1,b2,b3,b4,b5
      real    :: c1,c2,c3

      integer :: i,ii,j,jj,k,m,mm,n,nn,ip,jp,npath,iip,jjp
      real, dimension(1:np) :: veltmp1,veltmp2,veltmp3,veltmp4

      real, dimension(20)   :: tmp_wrk
      real, dimension(500)  :: vel_small,phi_small
      real :: a_regression,b_regression,rr

      integer :: shield_count
      real    :: shield_value,shield_angle,residual,shield_line
      real    :: rough_v1,beta1,dlta_thet1
      real    :: cost_adj_pr,cost_adj_mr,cost_adj_ps,cost_adj_ms
      real    :: vabsbest,U0,V0,beta,cbest
      integer :: mbest
      real    :: vprmax

!     ##################################################################
!     Execute. Give some parameters.
!     ##################################################################
      small_phi=spval
      large_phi=spval

      shield_count=15
      shield_value=2.0
      shield_line=15.0
      if ( vcpnum==32 ) then
        shield_angle=15.0
      else
        shield_angle=7.5
      endif

!     ==================================================================
!     do tilt loop, from higher to lower. which may be better to get
!     the 0 Vr points.
!     ==================================================================
      do k_tilt=2,nthet
        k=k_tilt
        if ( select_circ(k,ilevel) > 0 ) then       ! if select_circ>0

          nbeam = nphi(k)
          elvng = thet(k)
          ii = select_circ(k,ilevel)
          cos_elv=cos(elvng*rdn)
          vnyq=thet_nyq_vel(k)

          do j=1,nbeam
            veltmp1(j)=vel(ii,j,k)
            veltmp2(j)=phi(j,k)
            veltmp3(j)=veltmp1(j)
          enddo

          vpr_gradient=spval; ppr_gradient=spval
          cp_gradient=spval; rp_gradient=spval
          cost_ps=spval; a0ps=spval; vpsbest=spval; betaps=spval
          cost_pr=spval; a0pr=spval; vprbest=spval; betapr=spval
          vmr_gradient=spval; pmr_gradient=spval
          cm_gradient=spval; rm_gradient=spval
          cost_ms=spval; a0ms=spval; vmsbest=spval; betams=spval
          cost_mr=spval; a0mr=spval; vmrbest=spval; betamr=spval
!         ==============================================================
!         choise all the minimum abs(vr).
!         ==============================================================
          ncount=0
   10     continue
          a1=100.0
          do jm=1, nbeam
            if ( abs(veltmp1(jm)).le.a1 ) then
              a1 = abs(veltmp1(jm))
              m=jm
            endif
          enddo

          if ( a1 > shield_value ) go to 20    !!! exit gate (need more test)

          ncount=ncount+1
          phi_minvr(ncount)=phi(m,k)
          indx_minvr(ncount)=m

!         ==============================================================
!         filling in the miss value around the min Vr point to avoid
!         second chosen in searching process.
!         ==============================================================
          do jm =-7,7
            j=m+jm
            if ( j<=0 ) j=nbeam+j
            if ( j>nbeam) j=j-nbeam
              veltmp1(j)=spval
          enddo
          go to 10
   20     continue     ! out of searching, do next step.

          if ( ncount>1 .and. ncount<=20 ) then   ! 0Vr number is OK
!           ============================================================
!           calculate the gradient at the 0Vr points.
!           ============================================================
            guessp=0
            guessm=0
            do n=1, ncount
              m=indx_minvr(n)
              msum=0
              a3=0.0
              do jm=-shield_count,shield_count
                a3=a3+1.0
                j=m+jm
                if ( j>0 .and. j<nbeam ) then
                  if ( abs(veltmp3(j))<spval ) then
                    msum=msum+1
                    vel_small(msum)=veltmp3(j)
                    phi_small(msum)=veltmp2(j)
                  endif
                elseif ( j<=0 ) then
                  j=j+nbeam
                  if ( abs(veltmp3(j))<spval ) then
                    msum=msum+1
                    vel_small(msum)=veltmp3(j)
                    phi_small(msum)=veltmp2(j)-360.0
                  endif
                elseif ( j>nbeam ) then
                  j=j-nbeam
                  if ( abs(veltmp3(j))<spval ) then
                    msum=msum+1
                    vel_small(msum)=veltmp3(j)
                    phi_small(msum)=veltmp2(j)+360.0
                  endif
                endif
              enddo
!             ==========================================================
!             sign check.
!             ==========================================================
              a1=0.0
              a2=0.0
              do i=1,msum
                if ( vel_small(i)>=0.0 ) then
                  a1=a1+1.0
                else
                  a2=a2+1.0
                endif
              enddo

              if ( a1/a3<0.8 .and. a2/a3<0.8 ) then

                if ( msum>10 ) then
                  call line_regression( msum,vel_small,phi_small     &
                                       ,small_phi,spval              &
                                       ,vnyq                         &
                                       ,a_regression,b_regression,rr )
                  if ( abs(small_phi)<spval ) then
                    if ( b_regression>=0.1 ) then
                      guessp=guessp+1
                      vpr_gradient(guessp)=b_regression
                      ppr_gradient(guessp)=small_phi
                    endif
                    if ( b_regression<=-0.1 ) then
                      guessm=guessm+1
                      vmr_gradient(guessm)=b_regression
                      pmr_gradient(guessm)=small_phi
                    endif
                  endif
                endif
              endif
            enddo

            IF ( guessp>0 .and. guessm>0 ) THEN
!             ==========================================================
!             filling the same phi0 points in the positive group.
!             ==========================================================
              rp_gradient=ppr_gradient
              cp_gradient=vpr_gradient
              do m=1,guessp
                if ( rp_gradient(m)<spval ) then
                  a2=rp_gradient(m)
                  do n=1,guessp
                    if ( n/=m ) then
                      a1=( ppr_gradient(n)-a2 )
                      if ( a1>=0.0 ) then
                        nn=int(a1/360.0+0.5)
                      else
                        nn=int(a1/360.0-0.5)
                      endif
                      a3=abs(a1-float(nn)*360.0)
                      if ( a3<=10.0 ) then
                        if ( vpr_gradient(n)>vpr_gradient(m) ) then
                          cp_gradient(m)=vpr_gradient(n)
                          rp_gradient(m)=ppr_gradient(n)
                        endif
                        cp_gradient(n)=spval
                        rp_gradient(n)=spval
                      endif
                    endif
                  enddo
                endif
              enddo

              vpr_gradient=spval
              ppr_gradient=spval
              m=0
              do n=1,guessp
                if ( cp_gradient(n)<spval ) then
                  m=m+1
                  vpr_gradient(m)=-cp_gradient(n)    ! for sort
                  ppr_gradient(m)=rp_gradient(n)
                endif
              enddo
              guessp=m

!             ==========================================================
!             filling the same phi0 points in the negative group.
!             ==========================================================
              rm_gradient=pmr_gradient
              cm_gradient=vmr_gradient
              do m=1,guessm
                if ( rm_gradient(m)<spval ) then
                  a2=rm_gradient(m)
                  do n=1,guessm
                    if ( n/=m ) then
                      a1=( pmr_gradient(n)-a2 )
                      if ( a1>=0.0 ) then
                        nn=int(a1/360.0+0.5)
                      else
                        nn=int(a1/360.0-0.5)
                      endif
                      a3=abs(a1-float(nn)*360.0)
                      if ( a3<=10 ) then
                        if ( vmr_gradient(n)<vmr_gradient(m) ) then
                          cm_gradient(m)=vmr_gradient(n)
                          rm_gradient(m)=pmr_gradient(n)
                        endif
                        cm_gradient(n)=spval
                        rm_gradient(n)=spval
                      endif
                    endif
                  enddo
                endif
              enddo

              vmr_gradient=spval
              pmr_gradient=spval
              m=0
              do n=1,guessm
                if ( cm_gradient(n)<spval ) then
                  m=m+1
                  vmr_gradient(m)=cm_gradient(n)
                  pmr_gradient(m)=rm_gradient(n)
                endif
              enddo
              guessm=m

!             ==========================================================
!             sort following vp_gradient and vm_gradient.
!             ==========================================================
              tmp_wrk=vpr_gradient
              call sort2(guessp,tmp_wrk,ppr_gradient)
              vpr_gradient=tmp_wrk

              tmp_wrk=vmr_gradient
              call sort2(guessm,tmp_wrk,pmr_gradient)
              vmr_gradient=tmp_wrk

              do n=1,guessp                    ! contrary
                vpr_gradient(n)=-vpr_gradient(n)
              enddo

              !do n=1,guessp
              !print*,ilevel,k,vpr_gradient(n),ppr_gradient(n),rp_gradient(n)
              !enddo
              !print*,' '
              !do n=1,guessm
              !print*,ilevel,k,vmr_gradient(n),pmr_gradient(n),rm_gradient(n)
              !enddo
              !print*,' '

!             ==========================================================
!             searching the positive minimum cost-function points.
!             ==========================================================
              goodp=0
              do n=1,guessp
                call rough_min( nbeam,veltmp3,veltmp2                &
                               ,cos_elv,vnyq,rdn                     &
                               ,vpr_gradient(n),ppr_gradient(n)      &
                               ,cp_gradient(n),rp_gradient(n)        &
                               ,cost_ps(n),a0ps(n),vpsbest(n)        &
                               ,betaps(n),spval )
                !print*,n,cost_ps(n),a0ps(n),vpsbest(n),betaps(n)
!               convariance check
                if ( cost_ps(n)<(vnyq/3.0)**2 ) then
                  goodp=goodp+1
                  vp_gradient(goodp)=vpr_gradient(n)
                  pp_gradient(goodp)=ppr_gradient(n)
                  cost_pr(goodp)=cost_ps(n)
                  a0pr(goodp)=a0ps(n)
                  vprbest(goodp)=vpsbest(n)
                  betapr(goodp)=betaps(n)
                endif
              enddo
!             ==========================================================
!             searching the negative minimum cost-function points.
!             ==========================================================
              goodm=0
              do n=1,guessm
                call rough_min( nbeam,veltmp3,veltmp2                &
                               ,cos_elv,vnyq,rdn                     &
                               ,vmr_gradient(n),pmr_gradient(n)      &
                               ,cm_gradient(n),rm_gradient(n)        &
                               ,cost_ms(n),a0ms(n),vmsbest(n)        &
                               ,betams(n),spval )
                !print*,n,cost_ms(n),a0ms(n),vmsbest(n),betams(n)
!               convariance check
                if ( cost_ms(n)<(vnyq/3.0)**2 ) then
                  goodm=goodm+1
                  vm_gradient(goodm)=vmr_gradient(n)
                  pm_gradient(goodm)=pmr_gradient(n)
                  cost_mr(goodm)=cost_ms(n)
                  a0mr(goodm)=a0ms(n)
                  vmrbest(goodm)=vmsbest(n)
                  betamr(goodm)=betams(n)
                endif
              enddo
            ELSE
              goodp=0; goodm=0
            ENDIF     ! ENDIF guessp>0 and guessm>0

            !print*,'goodp==',goodp,'goodm==',goodm
            IF ( goodp>0 .and. goodm>0 ) THEN
!             ==========================================================
!             comparing the positive minimum cost-function points.
!             ==========================================================
              a1=1.0E+10     ! min cost function searching
              do n=1,goodp
                if ( cost_pr(n)<a1 ) then
                  a1=cost_pr(n)
                  ip=n
                endif
              enddo
              !print*,'ip==',ip,'cost==',a1

              a1=0.0         ! max gragient searching
              do n=1,goodp
                if ( vp_gradient(n)>a1 ) then
                  a1=vp_gradient(n)
                  iip=n
                endif
              enddo
              !print*,'iip==',iip,'grid==',a1

!             mix rate calculate
              do n=1,goodp
                ratp(n)=0.0
                a1=vp_gradient(n)/vp_gradient(iip)
                a2=sqrt( cost_pr(ip)/cost_pr(n) )
                ratp(n)=a1+a2
                !print*,n,a1,a2,ratp(n)
              enddo

              a1=0.0          ! max mix rate searching
              do n=1,goodp
                if ( ratp(n)>a1 ) then
                  a1=ratp(n)
                  ip=n
                endif
              enddo
              !print*,'ip==',ip,'rate==',a1

!             max Vpr searching
              a1=vprbest(ip)
              nn=min(goodp,3)
              do n=1,nn
                a2=cost_pr(n)/cost_pr(ip)
                if ( a2<2.0*cost_pr(ip) .and. a2<(0.4*vnyq)**2 ) then
                  a1=max(vprbest(n),a1)
                endif
              enddo
              vprmax=a1
              !print*,'ip==',ip,'vprp==',vprmax

              call cost_function_mean( nbeam,veltmp3,veltmp2         &
                                      ,cos_elv,vnyq,rdn              &
                                      ,a0pr(ip),pp_gradient(ip)      &
                                      ,1.0                           &
                                      ,vprbest(ip),cost_adj_pr,spval )

!             ==========================================================
!             comparing the negative minimum cost-function points.
!             ==========================================================
              a1=1.0E+10        ! min cost fuction searching
              do n=1,goodm
                if ( cost_mr(n)<a1 ) then
                  a1=cost_mr(n)
                  jp=n
                endif
              enddo
              !print*,'jp==',jp,'cost==',a1

              a1=0.0            ! max gradient searching
              do n=1,goodm
                if ( vm_gradient(n)<a1 ) then
                  a1=vm_gradient(n)
                  jjp=n
                endif
              enddo
              !print*,'jjp==',jjp,'grid==',a1

!             calculate the mix rate
              do n=1,goodm
                a1=vm_gradient(n)/vm_gradient(jjp)
                a2=sqrt( cost_mr(jp)/cost_mr(n) )
                ratm(n)=a1+a2
                !print*,n,a1,a2,ratm(n)
              enddo

              a1=0.0       ! max rate searching
              do n=1,goodm
                if ( ratm(n)>a1 ) then
                  a1=ratm(n)
                  jp=n
                endif
              enddo
              !print*,'jp==',jp,'rate==',a1

!             max Vpm searching
              a1=vmrbest(jp)
              nn=min(goodm,3)
              do n=1,nn
                a2=cost_mr(n)/cost_mr(jp)
                if ( a2<2.0*cost_mr(jp) .and. a2<(0.4*vnyq)**2 ) then
                  a1=max(vmrbest(n),a1)
                endif
              enddo
              !print*,'jp==',jp,'vrpm==',a1

!             max vr represent
              vprmax=max(a1,vprmax)
              !print*,'vprmax==',vprmax

              call cost_function_mean( nbeam,veltmp3,veltmp2         &
                                      ,cos_elv,vnyq,rdn              &
                                      ,a0mr(jp),pm_gradient(jp)      &
                                      ,-1.0                          &
                                      ,vmrbest(jp),cost_adj_mr,spval )

!             if ( vprmax>4.0*vnyq ) then
!               a2=vprmax*sqrt( 1.0-(4.0*vnyq/vprmax)**2 )/10.0*rdn
!             elseif ( vprmax>2.0*vnyq ) then
!               a2=vprmax*sqrt( 1.0-(2.0*vnyq/vprmax)**2 )/10.0*rdn
!             endif
!             if ( vprmax>2.0*vnyq ) then
!               nn=0
!               do n=1,goodp
!                 if ( vp_gradient(n)>a2 ) then
!                   nn=nn+1
!                 endif
!               enddo
!               goodp=nn

!               nn=0
!               do n=1,goodm
!                 if ( abs(vm_gradient(n))>a2 ) then
!                   nn=nn+1
!                 endif
!               enddo
!               goodm=nn
!             endif
!             ==========================================================
!             re-check if the goodp or goodm =1 only
!             ==========================================================
              if ( vprmax>2.2*vnyq ) then
                !print*, 'goodp = ',goodp, goodm,k,ilevel,select_circ(k,ilevel)
                !print*,ip,vprbest(ip),jp,vmrbest(jp),vprmax
                if ( goodp==1 .and. goodm==1 ) then
!                 a1=pp_gradient(ip)-pm_gradient(jp)
!                 if ( a1>=0.0 ) then
!                   n=int(a1/360.0+0.5)
!                 else
!                   n=int(a1/360.0-0.5)
!                 endif
!                 a3=abs(a1-float(n)*360.0)-180.0
!                 if ( abs(a3)>=7.5 ) then
!                 if ( abs(a3)>=15.0 ) then
!                   a0pr(ip)=spval
!                   vprbest(ip)=spval
!                   betapr(ip)=spval
!                   cost_pr(ip)=spval
!                   cost_adj_pr=spval
!                 endif
                  a0pr(ip)=spval
                  vprbest(ip)=spval
                  betapr(ip)=spval
                  cost_pr(ip)=spval
                  cost_adj_pr=spval
                  a0mr(jp)=spval
                  vmrbest(jp)=spval
                  betamr(jp)=spval
                  cost_mr(jp)=spval
                  cost_adj_mr=spval
                else
                  if ( goodp==1 ) then
                    a1=pp_gradient(ip)-pm_gradient(jp)
                    if ( a1>=0.0 ) then
                      n=int(a1/360.0+0.5)
                    else
                      n=int(a1/360.0-0.5)
                    endif
                    a3=abs(a1-float(n)*360.0)-180.0
!                   if ( abs(a3)>=7.5 ) then
                    if ( abs(a3)>=15.0 ) then
                      a0pr(ip)=spval
                      vprbest(ip)=spval
                      betapr(ip)=spval
                      cost_pr(ip)=spval
                      cost_adj_pr=spval
                    endif
                  elseif ( goodp==2 ) then
                    if ( abs(vprbest(ip))>=2.0*vnyq ) then
                    a2=asin(2.0*vnyq/vprbest(ip))/rdn    ! degree
                    else
                      a2=asin(2.0*vnyq/vprmax)/rdn
                    endif
                    a1=pp_gradient(1)-pp_gradient(2)
                    if ( a1>=0.0 ) then
                      n=int(a1/360.0+0.5)
                    else
                      n=int(a1/360.0-0.5)
                    endif
                    a3=abs( (a1-float(n)*360.0) )
!                   if ( a3>1.2*a2 ) then
                    if ( a3>1.5*a2 ) then
                      a0pr(ip)=spval
                      vprbest(ip)=spval
                      betapr(ip)=spval
                      cost_pr(ip)=spval
                      cost_adj_pr=spval
                    endif
                  elseif ( goodp==3 ) then
                    if ( abs(vprbest(ip))>=2.0*vnyq ) then
                    a6=asin(2.0*vnyq/vprbest(ip))/rdn    ! degree
                    else
                      a6=asin(2.0*vnyq/vprmax)/rdn
                    endif
                    b1=1.0
                    a1=pp_gradient(1)-pp_gradient(ip)
                    if ( a1>=0.0 ) then
                      nn=int(a1/360.0+0.5)
                    else
                      nn=int(a1/360.0-0.5)
                    endif
                    a1=a1-float(nn)*360.0
                    if ( abs(a1)<0.01 ) then
                      a1=1.0
                    else
                      if ( abs(a1)>1.5*a6 ) then
                        b1=0.0
                      endif
                    endif

                    b2=1.0
                    a2=pp_gradient(2)-pp_gradient(ip)
                    if ( a2>=0.0 ) then
                      nn=int(a2/360.0+0.5)
                    else
                      nn=int(a2/360.0-0.5)
                    endif
                    a2=a2-float(nn)*360.0
                    if ( abs(a2)<0.01 ) then
                      a2=1.0
                    else
                      if ( abs(a2)>1.5*a6 ) then
                        b2=0.0
                      endif
                    endif

                    b3=1.0
                    a3=pp_gradient(3)-pp_gradient(ip)
                    if ( a3>=0.0 ) then
                      nn=int(a3/360.0+0.5)
                    else
                      nn=int(a3/360.0-0.5)
                    endif
                    a3=a3-float(nn)*360.0
                    if ( abs(a3)<0.01 ) then
                      a3=1.0
                    else
                      if ( abs(a3)>1.5*a6 ) then
                        b3=0.0
                      endif
                    endif

                    a4=a1*a2*a3
                    b4=b1*b2*b3
                    if ( a4>0.0 ) then
                      a0pr(ip)=spval
                      vprbest(ip)=spval
                      betapr(ip)=spval
                      cost_pr(ip)=spval
                      cost_adj_pr=spval
                    else
                      if ( b4<0.1 ) then
                        a1=pp_gradient(ip)-pm_gradient(jp)
                        if ( a1>=0.0 ) then
                          n=int(a1/360.0+0.5)
                        else
                          n=int(a1/360.0-0.5)
                        endif
                        a3=abs(a1-float(n)*360.0)-180.0
!                       if ( abs(a3)>=7.5 ) then
                        if ( abs(a3)>=15.0 ) then
                          a0pr(ip)=spval
                          vprbest(ip)=spval
                          betapr(ip)=spval
                          cost_pr(ip)=spval
                          cost_adj_pr=spval
                        endif
                      endif   ! endif b4<0.1
                    endif   ! endif a4>0.0
                  endif   ! endif goop number

                  if ( goodm==1 ) then
                    a1=pp_gradient(ip)-pm_gradient(jp)
                    if ( a1>=0.0 ) then
                      n=int(a1/360.0+0.5)
                    else
                      n=int(a1/360.0-0.5)
                    endif
                    a3=abs(a1-float(n)*360.0)-180.0
!                   if ( abs(a3)>=7.5 ) then
                    if ( abs(a3)>=15.0 ) then
                      a0mr(jp)=spval
                      vmrbest(jp)=spval
                      betamr(jp)=spval
                      cost_mr(jp)=spval
                      cost_adj_mr=spval
                    endif
                  elseif ( goodm==2 ) then
                    if ( abs(vmrbest(jp))>=2.0*vnyq ) then
                    a2=asin(2.0*vnyq/vmrbest(jp))/rdn    ! degree
                    else
                      a2=asin(2.0*vnyq/vprmax)/rdn
                    endif
                    a1=pm_gradient(1)-pm_gradient(2)
                    if ( a1>=0.0 ) then
                      n=int(a1/360.0+0.5)
                    else
                      n=int(a1/360.0-0.5)
                    endif
                    a3=abs( (a1-float(n)*360.0) )
!                   if ( a3>1.2*a2 ) then
                    if ( a3>1.5*a2 ) then
                      a0mr(jp)=spval
                      vmrbest(jp)=spval
                      betamr(jp)=spval
                      cost_mr(jp)=spval
                      cost_adj_mr=spval
                    endif
                  elseif ( goodm==3 ) then
                    if ( abs(vmrbest(jp))>=2.0*vnyq ) then
                    a6=asin(2.0*vnyq/vmrbest(jp))/rdn    ! degree
                    else
                      a6=asin(2.0*vnyq/vprmax)/rdn
                    endif
                    b1=1.0
                    a1=pm_gradient(1)-pm_gradient(jp)
                    if ( a1>=0.0 ) then
                      nn=int(a1/360.0+0.5)
                    else
                      nn=int(a1/360.0-0.5)
                    endif
                    a1=a1-float(nn)*360.0
                    if ( abs(a1)<0.01 ) then
                      a1=1.0
                    else
                      if ( abs(a1)>1.5*a6 ) then
                        b1=0.0
                      endif
                    endif

                    b2=1.0
                    a2=pm_gradient(2)-pm_gradient(jp)
                    if ( a2>=0.0 ) then
                      nn=int(a2/360.0+0.5)
                    else
                      nn=int(a2/360.0-0.5)
                    endif
                    a2=a2-float(nn)*360.0
                    if ( abs(a2)<0.01 ) then
                      a2=1.0
                    else
                      if ( abs(a2)>1.5*a6 ) then
                        b2=0.0
                      endif
                    endif

                    b3=1.0
                    a3=pm_gradient(3)-pm_gradient(jp)
                    if ( a3>=0.0 ) then
                      nn=int(a3/360.0+0.5)
                    else
                      nn=int(a3/360.0-0.5)
                    endif
                    a3=a3-float(nn)*360.0
                    if ( abs(a3)<0.01 ) then
                      a3=1.0
                    else
                      if ( abs(a3)>1.5*a6 ) then
                        b3=0.0
                      endif
                    endif

                    a4=a1*a2*a3
                    b4=b1*b2*b3
                    if ( a4>0.0 ) then
                      a0mr(jp)=spval
                      vmrbest(jp)=spval
                      betamr(jp)=spval
                      cost_mr(jp)=spval
                      cost_adj_mr=spval
                    else
                      if ( b4<0.1 ) then
                        a1=pp_gradient(ip)-pm_gradient(jp)
                        if ( a1>=0.0 ) then
                          n=int(a1/360.0+0.5)
                        else
                          n=int(a1/360.0-0.5)
                        endif
                        a3=abs(a1-float(n)*360.0)-180.0
!                       if ( abs(a3)>=7.5 ) then
                        if ( abs(a3)>=15.0 ) then
                          a0mr(jp)=spval
                          vmrbest(jp)=spval
                          betamr(jp)=spval
                          cost_mr(jp)=spval
                          cost_adj_mr=spval
                        endif
                      endif   ! endif b4<0.1
                    endif   ! endif a4>0.0
                  endif   ! endif goodm number
                endif   ! endif goodp==1 and goodm==1
              endif   ! endif vbr>2.0vnyq or vmr>2.0vnyq

              if ( goodp>=4 ) then
                if ( goodm==1 .or. goodm>=4 ) then
                  a0pr(ip)=spval
                  vprbest(ip)=spval
                  betapr(ip)=spval
                  cost_pr(ip)=spval
                  cost_adj_pr=spval
                else
                  a1=pp_gradient(ip)-pm_gradient(jp)
                  if ( a1>=0.0 ) then
                    n=int(a1/360.0+0.5)
                  else
                    n=int(a1/360.0-0.5)
                  endif
                  a3=abs(a1-float(n)*360.0)-180.0
!                 if ( abs(a3)>=7.5 ) then
                  if ( abs(a3)>=15.0 ) then
                    a0pr(ip)=spval
                    vprbest(ip)=spval
                    betapr(ip)=spval
                    cost_pr(ip)=spval
                    cost_adj_pr=spval
                  endif
                endif
              endif
              if ( goodm>=4 ) then
                if ( goodp==1 .or. goodp>=4 ) then
                  a0mr(jp)=spval
                  vmrbest(jp)=spval
                  betamr(jp)=spval
                  cost_mr(jp)=spval
                  cost_adj_mr=spval
                else
                  a1=pp_gradient(ip)-pm_gradient(jp)
                  if ( a1>=0.0 ) then
                    n=int(a1/360.0+0.5)
                  else
                    n=int(a1/360.0-0.5)
                  endif
                  a3=abs(a1-float(n)*360.0)-180.0
!                 if ( abs(a3)>=7.5 ) then
                  if ( abs(a3)>=15.0 ) then
                    a0mr(jp)=spval
                    vmrbest(jp)=spval
                    betamr(jp)=spval
                    cost_mr(jp)=spval
                    cost_adj_mr=spval
                  endif
                endif
              endif
              if ( vprmax>3.2*vnyq ) then
                if ( goodp==3 .and. goodm==3 ) then
                  a1=pp_gradient(ip)-pm_gradient(jp)
                  if ( a1>=0.0 ) then
                    n=int(a1/360.0+0.5)
                  else
                    n=int(a1/360.0-0.5)
                  endif
                  a3=abs(a1-float(n)*360.0)-180.0
!                 if ( abs(a3)>=7.5 ) then
                  if ( abs(a3)>=15.0 ) then
                    a0pr(ip)=spval
                    vprbest(ip)=spval
                    betapr(ip)=spval
                    cost_pr(ip)=spval
                    cost_adj_pr=spval
                  endif
                endif
                if ( goodp==2 .and. goodm==2 ) then
                  a1=pp_gradient(ip)-pm_gradient(jp)
                  if ( a1>=0.0 ) then
                    n=int(a1/360.0+0.5)
                  else
                    n=int(a1/360.0-0.5)
                  endif
                  a3=abs(a1-float(n)*360.0)-180.0
!                 if ( abs(a3)>=7.5 ) then
                  if ( abs(a3)>=15.0 ) then
                    a0pr(ip)=spval
                    vprbest(ip)=spval
                    betapr(ip)=spval
                    cost_pr(ip)=spval
                    cost_adj_pr=spval
                  endif
                endif
              endif

              vabsbest=spval;U0=spval;V0=spval;beta=spval;cbest=spval
              mbest=0
              call get_adjust_VAD( nbeam,veltmp3,veltmp2,veltmp4     &
                                  ,cos_elv,vnyq,rdn                  &
                                  ,a0pr(ip),vprbest(ip),betapr(ip)   &
                                  ,cost_pr(ip),cost_adj_pr           &
                                  ,a0mr(jp),vmrbest(jp),betamr(jp)   &
                                  ,cost_mr(jp),cost_adj_mr           &
                                  ,k,ilevel,spval,3                  &
                                  ,vabsbest,U0,V0,beta,cbest,mbest )

              if ( mbest>0 ) then
                vabs(k,ilevel)=vabsbest
                U0ref(k,ilevel)=U0
                V0ref(k,ilevel)=V0
                c_fnct(k,ilevel)=cbest
                betaref(k,ilevel)=beta
                Acnjgt(k,ilevel)=mbest
                do jj=1,mspc
                  a1=phistn(jj)
                  a2=360.0
                  do j=1,nbeam
                    a3=veltmp2(j)
                    a4=abs(a3-a1)
                    if ( a4<a2 ) then
                      a2=a4
                      comb_back(jj,k,ilevel)=veltmp4(j)
                    endif
                  enddo
                enddo
              endif

            ELSE       ! goodp==0 or goodm==0, no semi-circle
              select_circ(k,ilevel)=0
            ENDIF      !  goodp>0 doogm>0

          endif      ! endif the suitable ncount
        endif      ! endif select circle
      enddo     ! enddo k_tilt

      return
      end subroutine gradient_fst_0Vr_31
