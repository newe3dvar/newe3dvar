!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
      SUBROUTINE first_analy
!
! ======================================================================
!     PURPOSE:
!       Using selected background cycle data and the original data to
!       do the conjugate job to get the best fitting analysis data.
! ======================================================================
!
!     Author   : Nai, Kang
!     Date     : May. 28, 2009
!     Action   : Created.
!
!     History  :
!    -----------
!
! ----------------------------------------------------------------------

      use Xu_variables

      implicit none

      integer :: start_level,end_level
      integer :: k,jm,jn,klevel,jk
      integer :: ii,j,i1,i2,npath,n
      logical :: first_check

! ######################################################################
!     Execute.
! ######################################################################

      k=k_tilt

      start_level=int((hstart+1.0)/zstep)   ! the lowest level is 250 m
      end_level=nprfl                       ! the toppest height

      first_check=.false.
      do ilevel=start_level,end_level
!       ================================================================
!       get the available top level
!       ================================================================
        if ( vabs(k,ilevel)<spval ) then
          first_check=.true.
          jm=ilevel
          exit
        endif
      enddo

      if ( first_check ) then
!       ================================================================
!       using variation method to get the refvel.
!       ================================================================       
        do ilevel=jm,end_level
          call first_conjugate
        enddo
      endif

      END SUBROUTINE first_analy
