
      SUBROUTINE WRITE_parameters

      use Xu_variables

      IMPLICIT NONE

      character(len=40) :: name2
      REAL,DIMENSION(nr,np) :: array
      INTEGER    :: i,j,k

      open(31,file='Lambert_projection.dat')
        write(31,'(i4)') mapproj
        write(31,'(f6.1)') sclfct
        write(31,'(f20.6)') trulon
        write(31,'(2f20.6)') trulat1,trulat2
      close(31)

!     ------------------------------------------------------------------
      open(31,file='radar_info.dat')
        write(31,'(a4)') radid
        write(31,'(3i8)') iyr(0),imon(0),iday(0)
        write(31,'(3i8)') ihr(0),imin(0),isec(0)
        write(31,'(3f10.3)') radlat,radlon,radelv
      close(31)
 
      END SUBROUTINE WRITE_parameters
