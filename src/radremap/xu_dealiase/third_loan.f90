!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!     ==================================================================
!     PURPOSE:
!       get the background Vref from the adjacent tilt.
!     ==================================================================
!
!     Author :  Kang Nai
!     Date   :  Mar. 20, 2009
!     Action :  Created.
!
!     History:
!     Author :  Kang Nai
!     Date   :  Feb. 22, 2012
!     Action :  borrow from the 1.5 tilt
!     ------------------------------------------------------------------
      subroutine third_loan

      use Xu_variables

      implicit none
 
      real    :: cos_elv,twonyq,variance_threshold
      integer :: i,ii,j,jj,k,m,n,npath,kprio
      real    :: a1,a2,a3,a4,a5

      real    :: vabsbest,U0,V0
      logical :: check_job
      integer,dimension(40) :: step_indx
      real,allocatable,dimension(:) :: veltmp1,veltmp2,veltmp3
      real,allocatable,dimension(:) :: veltmp4,veltmp5,veltmp6

!     ##################################################################
!     Execute. Give some parameters.
!     ##################################################################
      step_indx(1)=-1; step_indx(2)=1; step_indx(3)=-2; step_indx(4)=2
      step_indx(5)=-3; step_indx(6)=3; step_indx(7)=-4; step_indx(8)=4
      step_indx(9)=-5; step_indx(10)=5
      step_indx(11)=-6; step_indx(12)=6
      step_indx(13)=-7; step_indx(14)=7
      step_indx(15)=-8; step_indx(16)=8
      step_indx(17)=-9; step_indx(18)=9
      step_indx(19)=-10; step_indx(20)=10
      step_indx(21)=-11; step_indx(22)=11
      step_indx(23)=-12; step_indx(24)=12
      step_indx(25)=-13; step_indx(26)=13
      step_indx(27)=-14; step_indx(28)=14
      step_indx(29)=-15; step_indx(30)=15
      step_indx(31)=-16; step_indx(32)=16
      step_indx(33)=-17; step_indx(34)=17
      step_indx(35)=-18; step_indx(36)=18
      step_indx(37)=-19; step_indx(38)=19
      step_indx(39)=-20; step_indx(40)=20

      k=1
      if ( select_circ(k,ilevel) > 0 ) then       ! if select_circ>0

        nbeam = nphi(k)
        elvng = thet(k)
        cos_elv=cos(elvng*rdn)
        vnyq=thet_nyq_vel(k)
        ii=select_circ(k,ilevel)
        twonyq=2.0*vnyq
        if ( ilevel<=100 ) then
          variance_threshold=(vnyq/4.0)**2+2
        else
          variance_threshold=(vnyq/3.0)**2+2
        endif

        if ( vabs(2,ilevel)<spval ) then
!         --------------------------------------------------------------
!         variance check.
!         --------------------------------------------------------------
          allocate( veltmp1(nbeam) )
          allocate( veltmp2(nbeam) )
          allocate( veltmp3(mspc) )
          allocate( veltmp4(mspc) )
          allocate( veltmp5(nbeam) )
          do j=1,nbeam
            veltmp1(j)=vel(ii,j,k)
            veltmp2(j)=phi(j,k)
          enddo
          do j=1,mspc
            veltmp3(j)=analy_back(j,2,ilevel)
            veltmp4(j)=phistn(j)
          enddo
          call circle_linear_int( mspc,veltmp3,veltmp4               &
                                 ,nbeam,veltmp1,veltmp2              &
                                 ,veltmp5,spval )
          a1=0.0
          a5=0.0
          do j=1,nbeam
            if ( abs(veltmp1(j))<spval ) then
              a2=veltmp1(j)-veltmp5(j)
              if ( a2>=0.0 ) then
                n=int(a2/twonyq+0.5)
              else
                n=int(a2/twonyq-0.5)
              endif
              a3=(a2-float(n)*twonyq)
              a1=a1+a3*a3
              a5=a5+1.0
            endif
          enddo
          a4=a1/a5

          deallocate ( veltmp1,veltmp2,veltmp3,veltmp4,veltmp5 )

          if ( a4<variance_threshold ) then
            Acnjgt(k,ilevel)=2
            vabs(k,ilevel)=vabs(2,ilevel)
            U0ref(k,ilevel)=U0ref(2,ilevel)
            V0ref(k,ilevel)=V0ref(2,ilevel)
            c_fnct(k,ilevel)=c_fnct(2,ilevel)
            betaref(k,ilevel)=betaref(2,ilevel)
          endif
        endif     ! endif vabs<spval
      endif     ! endif select_circ>0

      return
      end subroutine third_loan
