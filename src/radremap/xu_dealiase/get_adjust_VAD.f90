!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
      subroutine get_adjust_VAD( nbeam,vrob,phiob,vrref              &
                                ,cos_elv,nyqv,rdn                    &
                                ,a01r,v1r,beta1r,cost1r,cost_adj1r   &
                                ,a02r,v2r,beta2r,cost2r,cost_adj2r   &
                                ,mcount,ilevel,spval,indx            &
                                ,vabs,U0,V0,beta,cbest,mbest )
 
!     ==================================================================
!     PURPOSE:
!       Using two estimate V to get the weighted referent vr.
!     ==================================================================
!
!     Author :  Kang Nai
!     Date   :  Oct. 24, 2007
!     Action :  Created.
!
!     History:
!     --------
!     name   :  Kang Nai
!     date   :  Feb. 13, 2009
!     action :  change to one select circle data.
!     ------------------------------------------------------------------

      implicit none
!
      integer :: nbeam
      real,dimension(1:nbeam) :: vrob
      real,dimension(1:nbeam) :: phiob
      real,dimension(1:nbeam) :: vrref
      real                    :: cos_elv
      real                    :: nyqv
      real                    :: rdn
      real    :: a01r,v1r,beta1r,cost1r,cost_adj1r  ! dvr/dazm>0
      real    :: a02r,v2r,beta2r,cost2r,cost_adj2r  ! dvr/dazm<0
      integer :: mcount
      integer :: ilevel
      real    :: spval
      integer :: indx
      real    :: vabs,U0,V0,beta,cbest
      integer :: mbest

      integer :: i,ii,j,jj,k,kk,m,mm,n,nn,nsum,knum,npath
      real    :: twonyq,thrths,thcrit
      real    :: a1,a2,a3,a4,a5,a6,b1,b2
      real    :: cstr,cplus,cmins
      real    :: alfa_p0,alfa_m0

      real    :: tmp_angle1,tmp_angle2,dlta_v
      real    :: vr_sign,sigma
      character(len=40) :: name1,name2

      logical :: f_guess,s_guess,t_guess,q_guess
      real,dimension(1:nbeam) :: veltmp1,veltmp2
      real    :: variance_threshold
!     ##################################################################
!     Execute.
!     ##################################################################

      twonyq=2.0*nyqv
      thrths=0.5*twonyq

      vrref=spval

      if ( ilevel<100 ) then
        variance_threshold=(nyqv/4.0)**2+2.0
      else
        variance_threshold=(nyqv/3.0)**2+2.0
      endif

!     if ( indx==1 ) then                  ! only one negative point
!       if ( cost2s<10.0 ) then
!         do j=1,nbeam
!           tmp_angle2=(phiob(j)-beta2s)*rdn
!           tmp_v(j)= a02s+v2s*cos_elv*cos(tmp_angle2)
!         enddo
!         vrref=tmp_v
!         RETURN
!       else
!         RETURN
!       endif
!     endif

!     if ( indx==2 ) then                 ! only one positive point
!       if ( cost1s<10.0 ) then
!         do j=1,nbeam
!           tmp_angle1=(phiob(j)-beta1s)*rdn
!           tmp_v(j)= a01s+v1s*cos_elv*cos(tmp_angle1)
!         enddo
!         vrref=tmp_v
!         RETURN
!       else
!         RETURN
!       endif
!     endif

!     get the VAD ref
      do j=1,nbeam
        tmp_angle1=(phiob(j)-beta1r)*rdn
        veltmp1(j)=a01r+v1r*cos_elv*cos(tmp_angle1)
        tmp_angle2=(phiob(j)-beta2r)*rdn
        veltmp2(j)= a02r+v2r*cos_elv*cos(tmp_angle2)
      enddo
      alfa_p0=beta1r-90.0     ! dvr/dazm>0
      if ( alfa_p0<0.0 ) alfa_p0=alfa_p0+360.0  ! a0p
      alfa_m0=beta2r+90.0     ! dvr/dazm<0
      if ( alfa_m0>360.0 ) alfa_m0=alfa_m0-360.0  !a0m
!     print*,'alfa_p0==',alfa_p0,'alfa_m0==',alfa_m0

      if ( cost1r>variance_threshold .and.                           &
           cost2r>variance_threshold ) RETURN

      if ( cost1r>variance_threshold ) then
!       if ( cost1r<variance_threshold ) then
!         if ( cost_adj1r<25.0 ) then
!           f_guess=.true.
!         else
!           f_guess=.false.
!         endif
!       else
          f_guess=.false.
!       endif
      else
!       if ( cost_adj1r<25.0 ) then
          f_guess=.true.
!       else
!         f_guess=.false.
!       endif
      endif

      if ( cost2r>variance_threshold ) then
!       if ( cost2r<25.0 ) then
!         if ( cost_adj2r<25.0 ) then
!           s_guess=.true.
!         else
!           s_guess=.false.
!         endif
!       else
          s_guess=.false.
!       endif
      else
!       if ( cost_adj2r<25.0 ) then
          s_guess=.true.
!       else
!         s_guess=.false.
!       endif
      endif
!
!     if ( f_guess ) then
!       if ( cost_adj1r>cost1r ) then
!         write(101,'(2f20.1)') cost1r,cost_adj1r
!       endif
!     endif
!     if ( s_guess) then
!       if ( cost_adj2r>cost2r ) then
!         write(102,'(2f20.1)') cost2r,cost_adj2r
!       endif
!     endif

      t_guess=f_guess
      q_guess=s_guess

      if ( f_guess ) then
        if ( .not. s_guess ) then
          if ( cost1r<=16.0 ) then
            t_guess=.true.
          else
            t_guess=.false.
          endif
        else
          t_guess=.true.
        endif
      endif

      if ( s_guess ) then
        if ( .not. f_guess ) then
          if ( cost2r<=16.0 ) then
            q_guess=.true.
          else
            q_guess=.false.
          endif
        else
          q_guess=.true.
        endif
      endif

      if ( t_guess .and. q_guess ) then
        if ( abs(beta2r-beta1r)>30.0 ) RETURN
      endif

      if ( t_guess .and. q_guess ) then
        if ( abs(v2r-v1r)>5.0 ) RETURN
      endif

!     if ( cost2r>cost1r ) then
!       if ( cost2r>10.0 .and. cost1r<10.0 ) then
!         a1=cost2r/cost1r
!         if ( a1>3.0 ) RETURN
!       endif
!     else
!       if ( cost1r>10.0 .and. cost2r<10.0 ) then
!         a1=cost1r/cost2r
!         if ( a1>3.0 ) RETURN
!       endif
!     endif

      if ( t_guess .and. q_guess ) then

!       combine the two refvel
        if ( alfa_m0>alfa_p0 ) then
          do j=1,nbeam
            if ( phiob(j)>=alfa_p0 .and. phiob(j)<alfa_m0 ) then
              cstr=90.0/(alfa_m0-alfa_p0)
              b1=(phiob(j)-alfa_p0)*cstr*rdn
              cplus=cos(b1)*cos(b1)
              b2=(phiob(j)-alfa_m0)*cstr*rdn
              cmins=cos(b2)*cos(b2)
              vrref(j)=(cplus*veltmp1(j)+cmins*veltmp2(j))/(cplus+cmins)
            else
              cstr=90.0/(alfa_p0-alfa_m0+360.0)
              if ( phiob(j)<alfa_p0 ) then
                b1=(phiob(j)+360.0-alfa_m0)*cstr*rdn
                b2=(phiob(j)-alfa_p0)*cstr*rdn
              endif
              if ( phiob(j)>=alfa_m0 ) then
                b1=(phiob(j)-alfa_m0)*cstr*rdn
                b2=(phiob(j)-(alfa_p0+360.0))*cstr*rdn
              endif
              cmins=cos(b1)*cos(b1)
              cplus=cos(b2)*cos(b2)
              vrref(j)=(cmins*veltmp2(j)+cplus*veltmp1(j))/(cplus+cmins)
            endif
          enddo
        else
          do j=1,nbeam
            if ( phiob(j)>=alfa_m0 .and. phiob(j)<alfa_p0 ) then
              cstr=90.0/(alfa_p0-alfa_m0)
              b1=(phiob(j)-alfa_m0)*cstr*rdn
              cmins=cos(b1)*cos(b1)
              b2=(phiob(j)-alfa_p0)*cstr*rdn
              cplus=cos(b2)*cos(b2)
              vrref(j)=(cmins*veltmp2(j)+cplus*veltmp1(j))/(cplus+cmins)
            else
              cstr=90.0/(alfa_m0-alfa_p0+360.0)
              if ( phiob(j)<alfa_m0 ) then
                b1=(phiob(j)+360.0-alfa_p0)*cstr*rdn
                b2=(phiob(j)-alfa_m0)*cstr*rdn
              endif
              if ( phiob(j)>=alfa_p0 ) then
                b1=(phiob(j)-alfa_p0)*cstr*rdn
                b2=(phiob(j)-(alfa_m0+360.0))*cstr*rdn
              endif
              cplus=cos(b1)*cos(b1)
              cmins=cos(b2)*cos(b2)
              vrref(j)=(cplus*veltmp1(j)+cmins*veltmp2(j))/(cplus+cmins)
            endif
          enddo
        endif

!       variance check
        a1=0.0
        a5=0.0
        do j=1,nbeam
          if ( abs(vrob(j))<spval ) then
            dlta_v=vrob(j)-vrref(j)
            if ( dlta_v>=0.0 ) then
              n=int(dlta_v/twonyq+0.5)
            else
              n=int(dlta_v/twonyq-0.5)
            endif
            b2=(dlta_v-float(n)*twonyq)
            a1=a1+b2*b2
            a5=a5+1.0
          endif
        enddo
        a1=a1/a5

!       variance_threshold
        if ( ilevel<=60 ) then
          variance_threshold=(nyqv/4.0)**2
          if ( a1>variance_threshold ) then
            vabs=spval
            U0=spval
            V0=spval
            beta=spval
            cbest=spval
            vrref=spval
          else
            mbest=mcount
            if ( cost2r<cost1r ) then
              vabs=v2r
              U0=vabs*cos((90.0-beta2r)*rdn)
              V0=vabs*sin((90.0-beta2r)*rdn)
              beta=beta2r
              cbest=cost2r
            else
              vabs=v1r
              U0=vabs*cos((90.0-beta1r)*rdn)
              V0=vabs*sin((90.0-beta1r)*rdn)
              beta=beta1r
              cbest=cost1r
            endif
          endif
        else
          variance_threshold=(nyqv/3.0)**2
          if ( a1>variance_threshold ) then
            vabs=spval
            U0=spval
            V0=spval
            beta=spval
            cbest=spval
            vrref=spval
          else
            mbest=mcount
            if ( cost2r<cost1r ) then
              vabs=v2r
              U0=vabs*cos((90.0-beta2r)*rdn)
              V0=vabs*sin((90.0-beta2r)*rdn)
              beta=beta2r
!             cbest=cost2r
              cbest=a1
            else
              vabs=v1r
              U0=vabs*cos((90.0-beta1r)*rdn)
              V0=vabs*sin((90.0-beta1r)*rdn)
              beta=beta1r
!             cbest=cost1r
              cbest=a1
            endif
          endif
        endif

!       npath=400+mcount
!       write(npath,'(2i4,3f10.3)') ilevel,mcount,cbest,cost1r,cost2r

!       write(name1,'(i4.4,i2.2)') ilevel,mcount
!       name2='bdj'//name1(1:6)//'.dat'
!       open(1,file=name2,form='formatted')
!         do j=1,nbeam
!           write(1,'(5f10.3)') phiob(j),vrob(j)                     &
!                              ,veltmp1(j),veltmp2(j)                &
!                              ,vrref(j)
!         enddo
!       close(1)

      endif

      return
      end subroutine get_adjust_VAD
