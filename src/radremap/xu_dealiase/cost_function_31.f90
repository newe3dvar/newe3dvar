!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
      subroutine cost_function_31( ncircle,vrob,phiob,cos_elv,nyqv,rdn &
                                  ,a0,phi0,crit                        &
                                  ,vmax,costf,spval )
 
!     ==================================================================
!     PURPOSE:
!       Using the estimate V to get the cost function.
!       The function's equation is:
!       J=sum {KAPA(dltav,nyqv)}**2
!     ==================================================================
!
!     Author :  Kang Nai
!     Date   :  Oct. 9, 2007
!     Action :  Created.
!
!     History:
!     --------
!     name   :  Kang Nai
!     Date   :  Jan. 28, 2012
!     Action :  Do half-circle cost function.
!     ------------------------------------------------------------------

      implicit none
!
      integer :: ncircle
      real, dimension(1:ncircle) :: vrob
      real, dimension(1:ncircle) :: phiob
      real                       :: cos_elv
      real                       :: nyqv
      real                       :: rdn
      real                       :: a0
      real                       :: phi0
      real                       :: crit
      real                       :: vmax
      real                       :: costf
      real                       :: spval

      integer :: i,ii,j,jj,k,kk,m,mm,n,nn,nsum,knum
      real    :: twonyq,thrths
      real    :: a1,a2,a3,a4,a5,b1,b2,b3,b4,b5
      character(len=80) :: name1,name2
      real    :: c1,c2

      real, dimension(1:ncircle) :: veltmp2,veltmp3,veltmp4
      real    :: alfa0,alfa1,beta
      real    :: tmp_angle1,tmp_angle2,dlta_v
      real    :: vr_sign

!     ##################################################################
!     Execute. calculate the cost function.
!     ##################################################################
!     print*,ncircle,cos_elv,nyqv,rdn
!     print*,a0,phi0,crit
!     vr_sign=1.0
!     if ( vmax*crit<0.0 ) then
!          vr_sign=-1.0
!     endif

      if ( crit>0.0 ) then
        if ( phi0>=180.0 ) then
          alfa0=phi0-180.0
          alfa1=phi0
          vr_sign=-1.0
        else
          alfa0=phi0
          alfa1=phi0+180.0
          vr_sign=1.0
        endif
      else
        if ( phi0>=180.0 ) then
          alfa0=phi0-180.0
          alfa1=phi0
          vr_sign=1.0
        else
          alfa0=phi0
          alfa1=phi0+180.0
          vr_sign=-1.0
        endif
      endif
      beta=0.5*(alfa1+alfa0-180.0)
          
      b1=0.0
      b3=0.0
      b4=0.0
      b5=0.0
      a5=0.0
      a4=0.0
      twonyq=2.0*nyqv
      thrths=0.5*twonyq

      do j=1,ncircle
        if ( abs(vrob(j))<spval ) then
          if ( phi0>=90.0 .and. phi0<=270.0 ) then
            c1=phi0-90.0
            c2=phi0+90.0
            if ( phiob(j)>=c1 .and. phiob(j)<=c2 ) then
              tmp_angle1=(phiob(j)-beta)*rdn
              a1= sin(tmp_angle1)
              a2=vr_sign*vmax*a1*cos_elv
              dlta_v=vrob(j)-(a0+a2)
              if ( dlta_v>=0.0 ) then
                n=int(dlta_v/twonyq+0.5)
              else
                n=int(dlta_v/twonyq-0.5)
              endif
              b2=(dlta_v-float(n)*twonyq)
              b3=b3+b2
              b4=b4+b2*b2
              b5=b5+1.0
            endif
          elseif ( phi0<90.0 ) then
            c1=phi0-90.0+360.0
            c2=phi0+90.0
            if ( phiob(j)>=c1 .or. phiob(j)<=c2 ) then
              tmp_angle1=(phiob(j)-beta)*rdn
              a1= sin(tmp_angle1)
              a2=vr_sign*vmax*a1*cos_elv
              dlta_v=vrob(j)-(a0+a2)
              if ( dlta_v>=0.0 ) then
                n=int(dlta_v/twonyq+0.5)
              else
                n=int(dlta_v/twonyq-0.5)
              endif
              b2=(dlta_v-float(n)*twonyq)
              b3=b3+b2
              b4=b4+b2*b2
              b5=b5+1.0
            endif
          elseif ( phi0>270.0 ) then
            c1=phi0-90.0
            c2=phi0+90.0-360.0
            if ( phiob(j)>=c1 .or. phiob(j)<=c2 ) then
              tmp_angle1=(phiob(j)-beta)*rdn
              a1= sin(tmp_angle1)
              a2=vr_sign*vmax*a1*cos_elv
              dlta_v=vrob(j)-(a0+a2)
              if ( dlta_v>=0.0 ) then
                n=int(dlta_v/twonyq+0.5)
              else
                n=int(dlta_v/twonyq-0.5)
              endif
              b2=(dlta_v-float(n)*twonyq)
              b3=b3+b2
              b4=b4+b2*b2
              b5=b5+1.0
            endif
          endif   ! phi0
        endif  ! vr<spval
      enddo
      if ( b5<1.0 ) then
        costf=spval
      else
        costf=b4/b5                  ! average J
      endif

      return
      end subroutine cost_function_31
