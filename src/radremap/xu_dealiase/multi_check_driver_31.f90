!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
! ======================================================================
!     PURPOSE:
!       Using the wind profile to unfolding the data if it needed.
! ======================================================================
!
!     Author   : Gong, Jiandong et al.
!     Date     :
!     Action   : Created.
!
!     History  :
!     ----------
!     name     : Nai, Kang
!     date     : Aug. 15, 2007
!     action   : Modified it to fit the new two step dealiase agorithm.
!
!     name     : Nai, Kang
!     date     : Aug. 15, 2007
!     action   : Modified it to fit the single 0Vr method.
!
! ----------------------------------------------------------------------
      SUBROUTINE Multi_check_driver_31

      use Xu_variables

      implicit none

      integer  :: i,j,k,kp,kpp
      character(len=40) :: name1,name2,name3

      real, dimension(1:nr,1:np) :: wrkchek
      real, dimension(1:nr,1:np) :: chekwrk
      integer  :: need_unfold_check
      integer  :: ii,ibgn,iend

      allocate ( obsvel( 1:nr, 1:np ) )
      allocate ( vadvel( 1:nr, 1:np ) )
      allocate ( wrkvel( 1:nr, 1:np ) )
      allocate ( index ( 1:nr, 1:np ) )
      allocate ( unfvel( 1:nr, 1:np ) )

      do k_tilt=1,nthet
        k=k_tilt
        nbeam = nphi(k)
        nrang = imaxrng(k)
        elvng = thet(k)
        vnyq  = thet_nyq_vel(k)
        kp=int( thet(k)*100.0 )
        write(name1,10) kp
   10   format(i4.4,'.dat')

!       ----------------------------------------------------------------
!       filling in the missing value in the tmp work array.
!       ----------------------------------------------------------------
        obsvel=spval
        vadvel=spval
        wrkvel=spval
        index=0
        unfvel=spval

!       ----------------------------------------------------------------
!       put the fold field to a tmp work field.
!       ----------------------------------------------------------------
        do j=1,nbeam; do i=iminrng,nrang
          obsvel(i,j)=orgvel(i,j,k)
        enddo; enddo

!       if ( vcpnum==32 ) then
!         do j=1,nbeam; do i=121,nrang                 ! 30km
!           if ( abs(obsvel(i,j))<0.6 ) obsvel(i,j)=spval
!         enddo; enddo
!       endif

!       ----------------------------------------------------------------
!       calculate reference radial velocity.
!       ----------------------------------------------------------------
        
        call full_field_31

!       ----------------------------------------------------------------
!       output the VAD field if needed.
!       ----------------------------------------------------------------
!       name2='vadr'//name1(1:8)
!       call write_radar(name2,vadvel)
 
!       ----------------------------------------------------------------
!       reference check; do <80 km unfold and make a index array.
!       ----------------------------------------------------------------
        call reference_check_31(wrkchek,ibgn,iend)

!       ----------------------------------------------------------------
!       output the middle results if needed.
!       ----------------------------------------------------------------
!       chekwrk=spval
!       do j=1,nbeam; do i=iminrng,nrang
!         chekwrk(i,j)=wrkvel(i,j)
!         if ( index(i,j) == 10 ) then
!           chekwrk(i,j) = spval
!         endif
!       enddo; enddo
!       name2='vadf'//name1(1:8)
!       call write_radar(name2,chekwrk)
!       name2='vadn'//name1(1:8)
!       call write_radar(name2,wrkchek)

!       ----------------------------------------------------------------
!       whole body check if the wind needs to be unfold.
!       which based on the vref field.
!       ----------------------------------------------------------------
        call continue_check_31(ibgn,iend)

!       name2='vadz'//name1(1:8)
!       call write_radar(name2,unfvel)
!       ----------------------------------------------------------------
!       Vr continue check. need test !!!
!       ----------------------------------------------------------------
!       call body_continue_check

!       ----------------------------------------------------------------
!       replace the fold filed by the unfold field.
!       ----------------------------------------------------------------
        ii=int(thet(k)*100.0)
        if ( ii<300 ) then
          ibgn=20
        endif
        if ( ii>=300 ) then
          ibgn=10
        endif

        do j=1,nbeam
        do i=iminrng,nrang
          if ( i<=ibgn ) then
            vel(i,j,k)=spval
          else
            vel(i,j,k)=unfvel(i,j)
          endif
          if ( abs(vel(i,j,k))<spval ) then
            count_total_points(k,2)=count_total_points(k,2)+1
          endif
        enddo
        enddo

      enddo

      deallocate ( obsvel )
      deallocate ( vadvel )
      deallocate ( unfvel )
      deallocate ( wrkvel )
      deallocate ( index  )

      END SUBROUTINE Multi_check_driver_31
