!#######################################################################
!#######################################################################
!####                                                               ####
!####                Subroutine grds_bigZG                          ####
!####                                                               ####
!#######################################################################
!#######################################################################

      subroutine grds_bigZG( nvar,x,g,kk,maxfn,keep_doing            &
                            ,nreal,vrob,phiob,cos_elv,nyqv,rdn       &
                            ,vrbk,phibk,ZG,sigmabk                   &
                            ,utri_array,ltri_array,sigma             &
                            ,spval )
 
!     ==================================================================
!     PURPOSE:
!       Using the estimate V to get the cost function's gredient.
!       The cost function's equation is:
!       J=(x**2/sigmabk**2 + 
!         sum{KAPA(dltav,nyqv)}**2/sigma**2
!     ==================================================================
!
!     Author :  Kang Nai
!     Date   :  Feb. 24, 2009
!     Action :  Created.
!
!     History:
!     --------
!
!     ------------------------------------------------------------------

      implicit none
!
      integer :: nvar
      real,dimension(1:nvar)        :: x
      real,dimension(1:nvar)        :: g
      integer                       :: kk
      integer                       :: maxfn
      logical                       :: keep_doing
      real,dimension(1:nvar)        :: vrbk
      real,dimension(1:nvar)        :: phibk
      real,dimension(1:nvar)        :: avrbk
      real,dimension(1:nvar,1:nvar) :: utri_array,ltri_array

      integer :: nreal
      real, dimension(1:nreal)      :: vrob
      real, dimension(1:nreal)      :: phiob
      real,dimension(1:nreal)       :: ZG
      real                          :: cos_elv
      real                          :: nyqv
      real                          :: rdn
      real                          :: sigmabk
      real                          :: sigma
      real                          :: spval

      integer :: i,j,k
      real    :: a1,b1,b2
      real,dimension(nvar)       :: grd
      character(len=80)          :: name1,name2,name3

!     ##################################################################
!     Execute. calculate the cost function.
!     ##################################################################
      keep_doing=.true.
!     write(name1,'(i2.2)') kk
!     name2='grdtcase'//name1(1:2)//'.dat'
!     open(1,file=name2)
!       do i=1,nreal
!         write(1,'(3f10.3)') phiob(i),vrob(i),ZG(i)
!       enddo
!     close(1)

      call circle_linear_int_ad( nvar,vrbk,phibk                     &
                                ,nreal,vrob,phiob                    &
                                ,spval,ZG,avrbk )

      g=0.0
      do i=1,nvar
        a1=x(i)/sigmabk/sigmabk
        b1=0.0
        do j=1,nvar
          b1=b1-utri_array(i,j)*avrbk(j)/sigma/sigma
        enddo
        g(i)=2.0*(a1+b1)
      enddo

!     name2='grdt'//name1(1:2)//'.dat'
!     open(1,file=name2)
!       do i=1,nvar
!         write(1,'(4f10.3)') phibk(i),vrbk(i),avrbk(i),g(i)
!       enddo
!     close(1)

!     call grads_bigZG( nreal,vrob,phiob,cos_elv,nyqv,rdn          &
!                      ,ua,va,sigma                                  &
!                      ,grd,spval )

!     g(1)=g(1)+grd(1)
!     g(2)=g(2)+grd(2)

      if ( kk>=maxfn ) then
        keep_doing=.false.
      endif

      return
      end subroutine grds_bigZG
