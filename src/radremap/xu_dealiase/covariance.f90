!#######################################################################
!#######################################################################
!####                                                               ####
!####                Subroutine covariance                          ####
!####                                                               ####
!#######################################################################
!#######################################################################

      subroutine covariance( nvar,itrun                              &
                            ,phibk,utri_matrice,ltri_matrice         &
                            ,garma,rdn )
 
!     ==================================================================
!     PURPOSE:
!       Calculating the covariance of the background field.
!       Cb=sigmabk**2*cos(dlta(phi))*exp(-Rc**2/(2*L**2))
!         Rc=2*r*sin(dlta(phi)/2)
!     ==================================================================
!
!     Author :  Kang Nai
!     Date   :  Mar. 11, 2009
!     Action :  Created.
!
!     History:
!     --------
!
!     ------------------------------------------------------------------

      implicit none
!
      integer                       :: nvar     ! total sample number
      integer                       :: itrun    ! truncate number
      real,dimension(1:nvar)        :: phibk
      real                          :: garma
      real                          :: rdn
      real,dimension(1:nvar,1:nvar) :: utri_matrice
      real,dimension(1:nvar,1:nvar) :: ltri_matrice

      integer :: i,j,k,i1,j1,k1
      real    :: a1,a2,a3,a4,a5
      real,dimension(1:nvar,1:nvar) :: Crr
      real,dimension(1:nvar)        :: p
      real                          :: phi1,phi2,dltaphi,dltax
      real,dimension(1:nvar)        :: sig_trun

      integer :: nx2,lwork,ierr
      real,allocatable,dimension(:,:) :: U0,VT0,S0
      real,allocatable,dimension(:)   :: sig
      real,allocatable,dimension(:)   :: work
      character(len=80)               :: name1,name2

!     ##################################################################
!     Execute. calculate the cost function.
!     ##################################################################
      
      Crr=0.0
      do i=1,nvar
        phi1=phibk(i)                          ! degree
        do j=1,nvar
          phi2=phibk(j)                        ! degree
          dltaphi=(phi2-phi1)*rdn              ! radians
!         dltax=2.0*radiu*sin(dltaphi/2.0)
!         a1=dltax**2/(2.0*L_hoz**2)*(-1.0)
          a1=2.0*(sin(dltaphi/2.0))**2
          a2=garma**2
          a3=a1/a2*(-1.0)
          a4=cos(dltaphi)*exp(a3)
          Crr(i,j)=a4
        enddo
      enddo

!     singular value decomposition
      nx2=nvar
      allocate(U0(nx2,nx2),sig(nx2),VT0(nx2,nx2))
      allocate(S0(nx2,nx2))
      sig=0.0
!     ierr=0
!     S0=Q
      S0=Crr
      lwork=2*max(3*min(nx2,nx2)+max(nx2,nx2),5*min(nx2,nx2))
!     print*,'lwork==',lwork
      allocate(work(lwork))
      call sgesvd('A','A',nx2,nx2,S0,nx2,sig,U0,nx2,VT0,nx2,work,lwork,ierr)
      print*,'ierr==',ierr
      deallocate(work)
!     open(1,file='singlular_36.dat')
!       a1=0.0
!       a2=0.0
!       do i=1,nx2
!         a1=a1+sig(i)
!         if ( i<=12 ) then
!           a2=a2+sig(i)
!         endif
!         write(1,'(i4,e15.5)') i,sig(i)
!       enddo
!       print*,a1,a2,a2/a1
!     close(1)

!     truncate
!     write(name1,'(i2.2)') itrun

      do i=1,nx2
        if ( i<=itrun ) then
          sig_trun(i)=sqrt(sig(i))
        else
          sig_trun(i)=0.0
        endif
      enddo

      ltri_matrice=0.0
      do j=1,nvar
        do i=1,nvar
          ltri_matrice(i,j)=sig_trun(j)*U0(i,j)
        enddo
      enddo

      utri_matrice=0.0
      do i=1,nvar
      do j=1,nvar
        utri_matrice(j,i)=ltri_matrice(i,j)
      enddo
      enddo

      deallocate( U0,sig,VT0,S0 )

      return
      end subroutine covariance
