
      SUBROUTINE WRITE_RADAR(name2,array)

      use Xu_variables

      IMPLICIT NONE

      character(len=40) :: name2
      REAL,DIMENSION(nr,np) :: array
      INTEGER    :: i,j,k
 
      k=k_tilt
      open(31,file=name2,form='formatted')
        write(31,'(a4)') radid
        write(31,'(i8)') vcpnum
        write(31,'(6i8)') iyr(k),imon(k),iday(k),ihr(k),imin(k),isec(k)
        write(31,'(2f10.3,f10.1)') radlat,radlon,radelv
        write(31,'(2f8.1)') rfstgat,refgatsp
        write(31,'(f8.3)') thet(k)
        write(31,'(2i8)') nphi(k),imaxrng(k)
        write(31,'(f8.3)') thet_nyq_vel(k)
        write(31,'(15f6.1)') (phi(j,k),j=1,nphi(k))
        write(31,'(20f6.1)') ((array(i,j),i=1,imaxrng(k)),j=1,nphi(k))
      close(31)

      END SUBROUTINE WRITE_RADAR
