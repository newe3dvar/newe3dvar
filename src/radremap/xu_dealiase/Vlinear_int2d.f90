!
!##################################################################
!##################################################################
!######                                                      ######
!######            subroutine  linearint_2d                  ######
!######                                                      ######
!##################################################################
!##################################################################
!
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  linear interpolation in 2D. 
!
!-----------------------------------------------------------------------
!
SUBROUTINE  linearint_2df(nx,ny,vbl2,pxx,pyy,pval,spval)
! 
! 
  INTEGER :: nx, ny
  DIMENSION vbl2(nx,ny)
  REAL :: pxx, pyy
  REAL :: pval
  REAL :: spval

! 
! 
  i = IFIX(pxx)
  j = IFIX(pyy)
!
!    print*,'ij=',i,j,' pxx=',pxx,pyy
!
  IF((0 < i) .AND. (i < nx)  .AND. (0 < j) .AND. (j < ny)) THEN
!
!
    deltadx = pxx - FLOAT (i)
    deltady = pyy - FLOAT (j)
!
    deltadxm= 1. - deltadx
    deltadym= 1. - deltady
!
!
!   IF(vbl2(i,j).lt.spval.and.vbl2(i+1,j  ).lt.spval.and.   &
!      vbl2(i,j+1).lt.spval.and.vbl2(i+1,j+1).lt.spval)THEN
!     pval =   deltadxm*( deltadym * vbl2(i,  j  )                      &
!                  +deltady  * vbl2(i+1,j  ))                           &
!           + deltadx *(deltadym * vbl2(i,  j+1)                        &
!           + deltady  * vbl2(i+1,j+1))
!   ELSE
!     pval = spval
!   ENDIF

!   i for x.  j for y      Kang Nai changed May 18 2013
    IF(vbl2(i,j).lt.spval.and.vbl2(i+1,j  ).lt.spval.and.   &
       vbl2(i,j+1).lt.spval.and.vbl2(i+1,j+1).lt.spval)THEN
      pval =   deltadxm*( deltadym * vbl2(i,  j  )                      &
                   +deltady  * vbl2(i,j+1  ))                           &
            + deltadx *(deltadym * vbl2(i+1,  j)                        &
            + deltady  * vbl2(i+1,j+1))
    ELSE
      pval = spval
    ENDIF
!
!
  ELSE
!
!
     pval = spval
    WRITE (0,'(2(a,f10.2))') ' pxx = ',pxx,' pyy = ',pyy
    WRITE (0,'(a,/)')        ' no interpolation was performed'
    STOP                     ' in intplin_2df.f'
!
  END IF
!
  RETURN
END SUBROUTINE  linearint_2df
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######               subroutine alinearint_2d               ######
!######                                                      ######
!##################################################################
!##################################################################
!
!
!

SUBROUTINE alinearint_2df(nx, ny, vbl2, pxx, pyy, pval)
!c
!c
  INTEGER :: nx, ny
  DIMENSION vbl2 (nx,ny)
  REAL :: pxx, pyy
  REAL :: pval
!
  i = IFIX (pxx)
  j = IFIX (pyy)
!
!
  IF ((0 < i) .AND. (i < nx)  .AND. (0 < j) .AND. (j < ny)) THEN
!
    deltadx = pxx - FLOAT(i)
    deltady = pyy - FLOAT(j)
!
    deltadxm= 1.  - deltadx
    deltadym= 1.  - deltady
!
    vbl2(i+1,j+1)=vbl2(i+1,j+1) + deltadx*deltady *pval
    vbl2(i  ,j+1)=vbl2(i  ,j+1) + deltadx*deltadym*pval
    vbl2(i+1,j  )=vbl2(i+1,j  ) + deltadxm*deltady*pval
    vbl2(i  ,j  )=vbl2(i  ,j  ) + deltadxm*deltadym*pval
    pval = 0.

  ELSE
!
    WRITE (0,*) ' no interpolation was performed'
    STOP        ' in intplin_2df.f'
!
  END IF
!
  RETURN
END SUBROUTINE alinearint_2df
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######               subroutine  map_to_mod2                ######
!######                                                      ######
!##################################################################
!##################################################################
!
!

SUBROUTINE map_to_mod2(nx,ny,pgx,pgy,px,py,pxx,pyy)
!
!
  INTEGER :: nx, ny
  DIMENSION pgx(nx)
  DIMENSION pgy(ny)
  REAL :: px, py
  REAL :: pxx,pyy
!
  pxx = 0.
  pyy = 0.
!
  DO j=1,ny-1
    DO i=1,nx-1
      IF(     (px  >= pgx(i  )) .AND. (px  < pgx(i+1))                  &
            .AND. (py  >= pgy(j  ))                                     &
            .AND. (py  < pgy(j+1)) ) THEN
!
        pxx = FLOAT(i)+ ( px-pgx(i) )/( pgx(i+1)-pgx(i) )
        pyy = FLOAT(j)+ ( py-pgy(j) )/( pgy(j+1)-pgy(j) )
!
!
        RETURN
!
      END IF
!
    END DO
  END DO
!
  RETURN
END SUBROUTINE map_to_mod2
