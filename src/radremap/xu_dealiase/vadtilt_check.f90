!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
! ======================================================================
!     PURPOSE:
!       Mark the unsuitable points.
! ======================================================================
!
!     Author   : Gong, Jiandong et al.
!     Date     :
!     Action   : Created.
!
!     History  :
!     ----------
!
! ----------------------------------------------------------------------

      subroutine vadtilt_check( nr,np,wrk,nbeam,iminrng,nrang        &
                               ,vadvel,vnyq,spval,ran,elvng,index    &
                               ,wrkchek,ibgn,limit_range )

      implicit none

      integer nr,np
      real,   dimension(nr,np) :: wrk
      integer                  :: nbeam,iminrng,nrang
      real,   dimension(nr,np) :: vadvel
      real                     :: vnyq, spval
      real,   dimension(nr)    :: ran
      real                     :: elvng
      integer,dimension(nr,np) :: index
      real,   dimension(nr,np) :: wrkchek
      integer :: ibgn
      real                     :: limit_range

      integer,dimension(:,:),allocatable :: iindex
      real    :: vel2,velmean,velvar
      real    :: range,hgtrad,sfcrng

      integer :: i,j,ip,jp,ims,inr,inp,ipst,ipen
      integer :: nycor,ipoint
      integer :: npts,iii,jjj,i0,j0
      real    :: a1,a2,refvel
      integer :: j1,j2

!     ------------------------------------------------------------------
!     This step is a point-mark process with a 3x3 window.
!     ------------------------------------------------------------------

      allocate ( iindex(1:nr,1:np) )

      iindex=0
      ipoint=0

      do j=1,nbeam
      do i=ibgn+1,nrang
        if ( abs(wrk(i,j))<spval ) then

          npts  = 0
          velmean=0.0
          do jjj=-1,1
            j0=j + jjj
            if ( j0<=0 ) j0=j0+nbeam
            if ( j0>nbeam) j0=j0-nbeam
            do iii=-1,1
              i0 = i + iii
              i0=max(1,i0)
              i0=min(i0,nrang)
              if ( abs(wrk(i0,j0))<spval .and. index(i0,j0)==0 ) then
                npts  =  npts + 1
                velmean=velmean+wrk(i0,j0)
              end if
            end do
          end do

          if ( npts >= 5 ) then
            velmean = velmean/npts
            if( abs(wrk(i,j)-velmean) > 5.0 ) iindex(i,j)=10
          else
            iindex(i,j)=10
          endif

        endif
      enddo
      enddo

      do j=1,nbeam
      do i=ibgn+1,nrang
        if( index(i,j) == 0 .and. iindex(i,j).eq.10 ) then
          index(i,j)=10
          wrkchek(i,j)=10.0
          ipoint=ipoint+1
        endif
      enddo
      enddo

      deallocate ( iindex )

!     ------------------------------------------------------------------
!     distance limit check.
!     ------------------------------------------------------------------
      do j=1,nbeam
      do i=1,nrang
        range = ran(i)
        if( range>limit_range ) then
          index(i,j) = 10
          wrkchek(i,j) =10.0
        endif
      enddo
      enddo

!     ------------------------------------------------------------------
!     tendency mark.
!     ------------------------------------------------------------------
      do j=1,nbeam
      do i=ibgn+1,nrang
        if ( index(i,j)==0 ) then
          refvel=vadvel(i,j)
          if ( wrk(i,j)*refvel<0.0 ) then
            index(i,j)=10
            wrkchek(i,j) =10.0
          endif
        endif
      enddo
      enddo

!     ------------------------------------------------------------------
!     smoth mark.
!     ------------------------------------------------------------------
      do j=1,nbeam
       do i=ibgn+1,nrang
         if ( wrkchek(i,j)<-20 ) then
           a1=0.0
           do i0=1,10
             iii=i-i0
             if ( wrkchek(iii,j)>0.0 ) then
               a1=a1+1.0
             endif
           enddo
           a2=0.0
           do i0=1,10
             iii=i+i0
             iii=min(nrang,iii)
             if ( wrkchek(iii,j)>0.0 ) then
               a2=a2+1.0
             endif
           enddo
           if ( a1>1.0 .and. a2>1.0 ) then
             wrkchek(i,j) =10.0
             index(i,j)=10
           endif
         endif
       enddo
      enddo

      return
      end subroutine vadtilt_check
