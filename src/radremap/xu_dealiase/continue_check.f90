!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
! ======================================================================
!     PURPOSE:
!       Body check to unfold the point which is the fold point.
! ======================================================================
!
!     Author  : Gong, Jiandong et al.
!     Date    :
!     Action  : Created.
!
!     History :
!     ---------
!     name    : Kang Nai and Qin Xu
!     date    : Sep. 20, 2007
!     action  : changed the simple unfold mode.
!
! ----------------------------------------------------------------------
      SUBROUTINE CONTINUE_CHECK

      USE Xu_variables

      IMPLICIT NONE

      integer :: special_points,pass_points
      integer :: i,j,k,ii
      character(len=80) :: name1,name2,name3

      real,dimension(:),allocatable   :: tmp3,tmp4

      allocate ( tmp3(nr) )
      allocate ( tmp4(np) )
 
      k=k_tilt
      do i=1,nrang
        tmp3(i)=wrkhgt(i,k)
      enddo
      do j=1,nbeam
        tmp4(j)=phi(j,k)
      enddo

      write(name1,'(i4.4,5i2.2)') iyr(k),imon(k),iday(k)             &
                                 ,ihr(k),imin(k),isec(k)
      ii=int(100.0*thet(k))
      write(name2,'(i4.4)') ii

      if ( ii<200 ) then     ! up tilt
       CALL GO_block_low( nr,np,nrang,nbeam,n10km                    &
                         ,obsvel,vnyq,thet(k),vcpnum,ran,tmp4        &
                         ,wrkvel,index,unfvel                        &
                         ,tmp3,spval                                 &
                         ,iyr(k),imon(k),iday(k)                     &
                         ,ihr(k),imin(k),isec(k)                     &
                         ,special_points,pass_points )
       count_special_points(k,1)=special_points
       count_total_points(k,4)=pass_points

       CALL Back_block_unfold( nr,np,nrang,nbeam,n10km               &
                              ,obsvel,vnyq,thet(k),vcpnum,ran,tmp4   &
                              ,wrkvel,index,unfvel                   &
                              ,tmp3,spval                            &
                              ,iyr(k),imon(k),iday(k)                &
                              ,ihr(k),imin(k),isec(k)                &
                              ,special_points )
       count_special_points(k,2)=special_points

       CALL GO_cyclone_unfold( nr,np,nrang,nbeam,n10km               &
                              ,obsvel,vnyq,thet(k),vcpnum,ran,tmp4   &
                              ,wrkvel,index,unfvel                   &
                              ,tmp3,spval                            &
                              ,iyr(k),imon(k),iday(k)                &
                              ,ihr(k),imin(k),isec(k)                &
                              ,special_points )
       count_special_points(k,3)=special_points

       CALL Back_cyclone_unfold( nr,np,nrang,nbeam,n10km             &
                                ,obsvel,vnyq,thet(k),vcpnum,ran,tmp4 &
                                ,wrkvel,index,unfvel                 &
                                ,tmp3,spval                          &
                                ,iyr(k),imon(k),iday(k)              &
                                ,ihr(k),imin(k),isec(k)              &
                                ,special_points )

       CALL TORNADO_forw( nr,np,nrang,nbeam,n10km                    &
                         ,obsvel,vnyq,thet(k),vcpnum,ran,tmp4        &
                         ,wrkvel,index,unfvel                        &
                         ,tmp3,spval                                 &
                         ,iyr(k),imon(k),iday(k)                     &
                         ,ihr(k),imin(k),isec(k)                     &
                         ,special_points )

       CALL TORNADO_back( nr,np,nrang,nbeam,n10km                    &
                         ,obsvel,vnyq,thet(k),vcpnum,ran,tmp4        &
                         ,wrkvel,index,unfvel                        &
                         ,tmp3,spval                                 &
                         ,iyr(k),imon(k),iday(k)                     &
                         ,ihr(k),imin(k),isec(k)                     &
                         ,special_points )
       count_special_points(k,4)=special_points

      else
       CALL GO_block_up( nr,np,nrang,nbeam,n10km                     &
                          ,obsvel,vnyq,thet(k),vcpnum,ran,tmp4       &
                          ,wrkvel,index,unfvel                       &
                          ,tmp3,spval                                &
                          ,iyr(k),imon(k),iday(k)                    &
                          ,ihr(k),imin(k),isec(k)                    &
                          ,special_points,pass_points )
       count_special_points(k,1)=special_points
       count_total_points(k,4)=pass_points

       CALL Back_block_unfold( nr,np,nrang,nbeam,n10km               &
                              ,obsvel,vnyq,thet(k),vcpnum,ran,tmp4   &
                              ,wrkvel,index,unfvel                   &
                              ,tmp3,spval                            &
                              ,iyr(k),imon(k),iday(k)                &
                              ,ihr(k),imin(k),isec(k)                &
                              ,special_points )
       count_special_points(k,2)=special_points

       CALL GO_cyclone_unfold( nr,np,nrang,nbeam,n10km               &
                              ,obsvel,vnyq,thet(k),vcpnum,ran,tmp4   &
                              ,wrkvel,index,unfvel                   &
                              ,tmp3,spval                            &
                              ,iyr(k),imon(k),iday(k)                &
                              ,ihr(k),imin(k),isec(k)                &
                              ,special_points )
       count_special_points(k,3)=special_points

       CALL Back_cyclone_unfold( nr,np,nrang,nbeam,n10km             &
                                ,obsvel,vnyq,thet(k),vcpnum,ran,tmp4 &
                                ,wrkvel,index,unfvel                 &
                                ,tmp3,spval                          &
                                ,iyr(k),imon(k),iday(k)              &
                                ,ihr(k),imin(k),isec(k)              &
                                ,special_points )

       CALL TORNADO_forw( nr,np,nrang,nbeam,n10km                    &
                         ,obsvel,vnyq,thet(k),vcpnum,ran,tmp4        &
                         ,wrkvel,index,unfvel                        &
                         ,tmp3,spval                                 &
                         ,iyr(k),imon(k),iday(k)                     &
                         ,ihr(k),imin(k),isec(k)                     &
                         ,special_points )
       count_special_points(k,4)=special_points

      endif

      deallocate ( tmp3,tmp4 )

      END SUBROUTINE CONTINUE_CHECK
