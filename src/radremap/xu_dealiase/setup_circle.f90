!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!     ==================================================================
!     PURPOSE:
!       choice the circle gate number which fitted the minimun available
!       data number in the circle at the standard height.
!     ==================================================================
!
!     Author   :  Gong, Jiandong et al.
!     Date     :
!     Action   :  Created.
!
!     History  :
!     ----------
!     Aug. 24, 2007: Nai,Kang change the minimum available value number
!                    from 0.6 to 0.4. Added height differ critical value.
!
!     Sep. 25, 2007: Nai,Kang added qualify criterion number(3).
!                    Which is the bottom level's qualify circles number
!                    between the 250 to 800 meters height.
!
!     Feb. 18, 2009  Nai,Kang removed the qualify criterion from 31 mode
!
!     Dec. 13, 2012  Nai,Kang added un-qualify circle's selection.
!
! ----------------------------------------------------------------------
      SUBROUTINE Setup_circle

      use Xu_variables

      implicit none

      integer :: i,j,k,ii,jj,npts,nd,nc,npath,i1,i2,i0
      logical :: found,refound
      real    :: tmp1,tmp2,a1,a2
      character(len=50) :: name1,name2

      integer :: jbg,jend,start_level
      real,allocatable,dimension(:) :: veltmp1,phitmp1
      logical :: gap_job

      real    :: height_diff

! ----------------------------------------------------------------------
!     setup high for wind profile
! ----------------------------------------------------------------------
      do ilevel=1,zlevel
        hstor(ilevel) = zstep + (ilevel-1) * zstep
      enddo

! ----------------------------------------------------------------------
!     setup standard cycles
! ----------------------------------------------------------------------
      select_circ=0
      model_circ=0
      start_level=int((hstart+1.0)/zstep)

      do k=1,nthet
        nbeam = nphi(k)
        nrang = imaxrng(k)
        elvng = thet(k)
        tmp2 =10000.0
        if ( elvng<2.0 ) then
          height_diff=zstep/2.0
        elseif ( elvng<5.0 ) then
          height_diff=zstep
        elseif ( elvng<10.0 ) then
          height_diff=2.0*zstep
        else
          height_diff=4.0*zstep
        endif

        do ilevel=start_level,zlevel
          tmp2 =10000.0
          do i=iminrng,nrang
            tmp1=abs(wrkhgt(i,k)-hstor(ilevel))
            if ( tmp1<tmp2 ) then
              tmp2=tmp1
              ii=i
            endif
          enddo
          if ( tmp2<height_diff ) then
            select_circ(k,ilevel) = ii
          endif
        enddo
      enddo

      do k=1,nthet
        do ilevel=start_level,zlevel
          i1=select_circ(k,ilevel)
          do nd=ilevel+1,zlevel
            i2=select_circ(k,nd)
            if ( i2==i1 ) then
              select_circ(k,nd)=0
            endif
          enddo
        enddo
      enddo
! =====================================================================
!     select cycle
! =====================================================================
      do ilevel=start_level,zlevel
        do k=1,nthet
          nbeam = nphi(k)
          nrang = imaxrng(k)

          ii=select_circ(k,ilevel)
          if ( ii>0 ) then
            found=.false.
            do i=ii,ii-2,-1
              npts=0
              do j=1,nbeam
                if ( abs(vel(i,j,k))<spval ) npts=npts+1
              enddo
!              write(0,*) ilevel, k, i, npts,nbeam
              if ( real(npts)/nbeam >= 0.40 ) then
              !write(0,*) ilevel, k, i, npts,nbeam
                select_circ(k,ilevel)=i
                found=.true.
                exit
              endif
            enddo

            if ( .not. found ) then
              do i=ii+1,ii+2
                i0=min(i,nrang)
                npts=0
                do j=1,nbeam
                  if ( abs(vel(i0,j,k))<spval ) npts=npts+1
                enddo
                if ( real(npts)/nbeam >= 0.40 ) then
                  select_circ(k,ilevel)=i0
                  found=.true.
                  exit
                endif
              enddo
            endif

            if ( .not. found ) then
              select_circ(k,ilevel)=0
!             ==========================================================
!             special cycle for the priority use.
!             ==========================================================
              refound=.false.
              do i=ii,ii-2,-1
                npts=0
                do j=1,nbeam
                  if ( abs(vel(i,j,k))<spval ) npts=npts+1
                enddo
                if ( npts>=20 ) then
                  model_circ(k,ilevel)=i   ! un-qualify circle
                  refound=.true.
                  exit
                endif
              enddo

              if ( .not. refound ) then
                do i=ii+1,ii+2
                  i0=min(i,nrang)
                  npts=0
                  do j=1,nbeam
                    if ( abs(vel(i0,j,k))<spval ) npts=npts+1
                  enddo
                  if ( npts >= 20 ) then
                    model_circ(k,ilevel)=i0  ! un-qualify circle
                    refound=.true.
                    exit
                  endif
                enddo
              endif   ! endif refound
            endif   ! endif not found
          endif   ! endif ii>0

        enddo
      enddo

! ======================================================================
!     count the selected circle's largest missing data gap for VCP 31.
! ======================================================================
      if ( vcpnum==31 ) then
        do ii=start_level,zlevel
        do k=1,nthet
          allocate( veltmp1(1:2*nphi(k)) )
          allocate( phitmp1(1:2*nphi(k)) )
          i=select_circ(k,ii)
          if ( i>0 ) then
            do j=1,nphi(k)
              veltmp1(j)=vel(i,j,k)
              veltmp1(nphi(k)+j)=vel(i,j,k)
              phitmp1(j)=phi(j,k)
              phitmp1(nphi(k)+j)=phi(j,k)+360.0
            enddo
            a1=0.0
            miss_gap(k,ii)=0.0
   10       continue
            gap_job=.false.
            do j=1,2*nphi(k)
              if ( veltmp1(j)>900.0 ) then
                jbg=j
                gap_job=.true.
                exit
              endif
            enddo
            if ( .not. gap_job ) go to 20
            jend=2*nphi(k)
            do j=jbg,2*nphi(k)
              if ( veltmp1(j)<spval ) then
                jend=j
                exit
              endif
            enddo
            a2=jend-jbg
            if ( a2>a1 ) then
              a1=a2
              miss_gap(k,ii)=phitmp1(jend)-phitmp1(jbg)
            endif
            do j=jbg,jend
              veltmp1(j)=888.0
            enddo
            go to 10
   20       continue
            if ( k>1 .and. miss_gap(k,ii)>=90.0 ) then
              select_circ(k,ii)=0
            endif
          endif
          deallocate( veltmp1,phitmp1 )
        enddo
        enddo
      endif

! ======================================================================
!     count the model circle's largest missing data gap.
! ======================================================================
      do ii=start_level,zlevel
      do k=1,nthet
        allocate( veltmp1(1:2*nphi(k)) )
        allocate( phitmp1(1:2*nphi(k)) )
        i=model_circ(k,ii)
        if ( i>0 ) then
          do j=1,nphi(k)
            veltmp1(j)=vel(i,j,k)
            veltmp1(nphi(k)+j)=vel(i,j,k)
            phitmp1(j)=phi(j,k)
            phitmp1(nphi(k)+j)=phi(j,k)+360.0
          enddo
          a1=0.0
          model_gap(k,ii)=0.0
   30     continue
          gap_job=.false.
          do j=1,2*nphi(k)
            if ( veltmp1(j)>900.0 ) then
              jbg=j
              gap_job=.true.
              exit
            endif
          enddo
          if ( .not. gap_job ) go to 40
          jend=2*nphi(k)
          do j=jbg,2*nphi(k)
            if ( veltmp1(j)<spval ) then
              jend=j
              exit
            endif
          enddo
          a2=jend-jbg
          if ( a2>a1 ) then
            a1=a2
            model_gap(k,ii)=phitmp1(jend)-phitmp1(jbg)
          endif
          do j=jbg,jend
            veltmp1(j)=888.0
          enddo
          go to 30
   40     continue
        endif
        deallocate( veltmp1,phitmp1 )
      enddo
      enddo

! ======================================================================
!     get the max height of the select circ.
! ======================================================================
      a1=0.0
      do ii=1,zlevel
        do k=1,nthet
          i=select_circ(k,ii)
          if ( i>0 ) then
            if ( wrkhgt(i,k)>a1 ) then
              a1=wrkhgt(i,k)
            endif
          endif
        enddo
      enddo

      a2=0.0
      do ii=1,zlevel
        do k=1,nthet
          i=model_circ(k,ii)
          if ( i>0 ) then
            if ( wrkhgt(i,k)>a2 ) then
              a2=wrkhgt(i,k)
            endif
          endif
        enddo
      enddo

      if ( a1<1.0 ) then
        no_qualify=.true.
        print*,' '
        print*,' This volume radar data is not qualify to dealise.'
        print*,' '
        name1='this volume data is not qualify to do dealiase'
        open(31,file='no_select_circ.dat')
          write(31,'(a46)') name1
        close(31)
      endif
      print*,' '
      print*,' Max height ==',max(a1,a2)
      print*,' '
      IF (outdiag) THEN
      open(31,file='selected_circ.dat')
        do ii=1,zlevel
          write(31,'(i4,20i6)') ii,(select_circ(k,ii),k=1,nthet)
        enddo
      close(31)
      open(31,file='model_circ.dat')
        do ii=1,zlevel
          write(31,'(i4,20i6)') ii,(model_circ(k,ii),k=1,nthet)
        enddo
      close(31)
      END IF

      RETURN
      END SUBROUTINE Setup_circle
