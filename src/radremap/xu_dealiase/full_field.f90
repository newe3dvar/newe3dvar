!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!     ==================================================================
!     PURPOSE:
!       get the whole reference radial velocity field of one tilt.
!     ==================================================================
!
!     Author :  Kang Nai
!     Date   :  Feb. 18, 2009
!     Action :  Created.
!
!     History:
!     --------
!
!     ==================================================================
      SUBROUTINE full_field

      use Xu_variables

      implicit none

      integer :: first_gate_number
      real    :: first_height
      integer :: ibgn,iend
      real    :: bgn_height,end_height

      integer :: i,ii,j,k,m,n
      real    :: p,urstar

!     ------------------------------------------------------------------
!     Calculate horizontal wind in the tilt
!     ------------------------------------------------------------------
      k=k_tilt

      do j=1,nbeam
!       ----------------------------------------------------------------
!       searching the available points.
!       ----------------------------------------------------------------
        do i=1,nrang
          if ( abs(velref(i,j,k))<spval ) then
            ii=i
            exit
          endif
        enddo
        first_gate_number=ii

        if ( i>=nrang ) go to 30

        first_height=wrkhgt(first_gate_number,k)
!       if ( first_height<1000.0 ) then
        if ( first_height<2000.0 ) then
          urstar=velref(ii,j,k)*0.4/log(first_height)
          do i=ii,1,-1
            hgtrad=wrkhgt(i,k)
            if ( hgtrad>200.0 ) then
              vadvel(i,j)=velref(ii,j,k)
            else
              vadvel(i,j)=urstar/0.4*log( hgtrad )
            endif
          enddo
        endif

        ibgn=first_gate_number
        bgn_height=first_height
   10   continue
        do i=ibgn+1,nrang
          if ( abs(velref(i,j,k))<spval ) then
            iend=i
            exit
          endif
        enddo

        if ( i>=nrang ) go to 20

        end_height=wrkhgt(iend,k)

        if ( bgn_height<5000.0 ) then
          if ( (end_height-bgn_height)>2500.0 ) then
            bgn_height=end_height
            ibgn=iend
          else
            do i=ibgn,iend
              p=ran(iend)-ran(ibgn)
              vadvel(i,j)= velref(ibgn,j,k)*(ran(iend)-ran(i))/p     &
                          +velref(iend,j,k)*(ran(i)-ran(ibgn))/p
            enddo
            ibgn=iend
            bgn_height=end_height
          endif
        else
          do i=ibgn,iend
            p=ran(iend)-ran(ibgn)
            vadvel(i,j)= velref(ibgn,j,k)*(ran(iend)-ran(i))/p       &
                        +velref(iend,j,k)*(ran(i)-ran(ibgn))/p
          enddo
          ibgn=iend
          bgn_height=end_height
        endif
        go to 10
   20   continue
!       iend=ibgn+10
!       iend=min(iend,nrang)
!       do i=ibgn,iend
!         vadvel(i,j)=velref(ibgn,j,k)
!       enddo
   30   continue
      enddo

      END SUBROUTINE full_field
