c***********************************************************************
c    This subroutine is used to find minimum value of costfunction 
c    that could be reached.
c***********************************************************************
      subroutine lbfgs_bigZG ( n,x0,eps,maxin,ftol,x,g,f,f1
     &                        ,ncircle,vrob,phiob,cos_elv,nyqv,rdn
     &                        ,vrbk,phibk,sigmabk
     &                        ,utri_array,ltri_array,sigma
     &                        ,vref,spval
     &                        ,ilevel,k,alfa )
c
      implicit none

c     ------------------------------------------------------------------
c     what for ?
c     ------------------------------------------------------------------
      integer m,ndim,nxm,nwork
      parameter(ndim=1900,m=5,nxm=ndim*m,nwork=ndim+2*m)
      real s(nxm),y(nxm),diag(ndim),w(nwork)
c
      integer n
      real x0(n)
      real eps
      real ftol
      integer maxin
      real x(n),g(n)
      real f,f1
      real,dimension(1:n) :: vrbk,phibk,vref
      real,dimension(1:n,1:n) :: utri_array,ltri_array

      integer :: ncircle
      real, dimension(1:ncircle) :: vrob
      real, dimension(1:ncircle) :: phiob
      real,dimension(1:ncircle)  :: ZG
      real                       :: cos_elv
      real                       :: nyqv
      real                       :: rdn
      real                       :: sigmabk
      real                       :: sigma
      real                       :: spval
      logical                    :: keep_doing
      integer                    :: ilevel,k
      real                       :: alfa
c
      real point,gtol,t1,t2 
c
      integer ioread,lp,iprint(2),iflag 
      logical diagco  
c     external va15cd, fcn,grd
c
      integer mp
      common /va15dd/mp,lp, gtol
c
      integer icall, i, j, ichoice,icheck
c     no missing
      integer :: nsum
      real,dimension(1:ncircle) :: vrno,phino
c
c     ichoice
      integer ::  nreal
      real, dimension(1:ncircle) :: realvr,realphi
      integer,dimension(1:ncircle) :: indx_real1

      character(len =80) :: name1,name2,name3
c
      if ( n.gt.ndim ) then
        write(*,*) '  stop,  n>> ndim=1978000'
        stop
      end if 
      do i=1,n
        x(i)=x0(i)
      end do

c     no mission
      nsum=0
      do j=1,ncircle
        if ( abs(vrob(j))<spval ) then
          nsum=nsum+1
          vrno(nsum)=vrob(j)
          phino(nsum)=phiob(j)
        endif
      enddo

!     write(name1,'(i4.4,i2.2)') ilevel,k
!     name2='bigZG_iter'//name1(1:6)//'.dat'
!     open(32,file=name2,form='formatted')
!     print*,'ilevel==',ilevel,' k_tilt==',k,alfa

      indx_real1=0
      ichoice=0
  100 continue
        iprint(1)= -1
        iprint(2)= 0
        diagco= .false.
        icall=0
        iflag=0
c       ----------------------------------------------------------------
c       choise data
c       ----------------------------------------------------------------
        call choice_bigZG( n,x,vrbk,phibk,ZG,ichoice
     &                    ,nsum,vrno,phino,cos_elv,nyqv,rdn
     &                    ,nreal,realvr,realphi
     &                    ,utri_array,ltri_array
     &                    ,alfa,indx_real1
     &                    ,spval )


   20   continue
c 
c       ****************************************************************
c       calculate the value of costfunction and its gradient with repect
c       to x.
c       ****************************************************************
c
        call fcn_bigZG( n,x,f,f1,icall,maxin,keep_doing
     &                 ,nreal,realvr,realphi,cos_elv,nyqv,rdn
     &                 ,vrbk,phibk,ZG,sigmabk
     &                 ,utri_array,ltri_array,sigma
     &                 ,alfa,indx_real1
     &                 ,spval )

        call grds_bigZG( n,x,g,icall,maxin,keep_doing
     &                  ,nreal,realvr,realphi,cos_elv,nyqv,rdn
     &                  ,vrbk,phibk,ZG,sigmabk
     &                  ,utri_array,ltri_array,sigma
     &                  ,spval )

        call va15ad( n,m,x,f,g,diagco,diag,iprint,eps,s,y
     *              ,point,w,iflag,ftol )

c       write(32,'(i4,e15.5,i4,f10.3)') icall,f,iflag,f1
 
        if ( iflag.le.0 ) go to 50
        icall=icall + 1 
        if ( icall.gt.maxin ) go to 50
        go to 20
  50    continue
c       write(32,'(2i4,2f20.3)') ichoice,icall,f,f1

        icheck=nint(0.95*nsum)
        if ( nreal>=icheck ) go to 200
!       if ( nreal>=nsum ) go to 200
          ichoice=ichoice+1
        if ( ichoice>5 ) go to 200
        go to 100

  200 continue

c     close(32)
c
c     calculate the final vrref
      do i=1,n
        t1=0.0
        do j=1,n
          t1=t1+ltri_array(i,j)*x(j)
        enddo
        vref(i)=vrbk(i)+t1               ! new Vref
      enddo

c     open(33,file='bigZG_ZG_fnl.dat')
c       do i=1,n
c         write(33,'(2f20.3)') phiob(i),ZG(i)
c       enddo
c     close(33)

      end subroutine lbfgs_bigZG
