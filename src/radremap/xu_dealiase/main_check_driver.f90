!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
! ======================================================================
!     PURPOSE:
!       Check driver.
! ======================================================================
!
!     Author   : Kang Nai
!     Date     : May 20,2009
!     Action   : Created.
!
!     History  :
!     ----------
!
! ----------------------------------------------------------------------
      SUBROUTINE main_check_driver

      use Xu_variables

      implicit none

      integer  :: i,j,k,kp,kpp,ii
      character(len=40) :: name1,name2,name3

      allocate ( obsvel( 1:nr, 1:np ) )
      allocate ( vadvel( 1:nr, 1:np ) )
      allocate ( wrkvel( 1:nr, 1:np ) )
      allocate ( index ( 1:nr, 1:np ) )
      allocate ( unfvel( 1:nr, 1:np ) )

      do k_tilt=1,nthet
        k=k_tilt
        nbeam = nphi(k)
        nrang = imaxrng(k)
        elvng = thet(k)
        vnyq  = thet_nyq_vel(k)
        write(name1,'(i4.4,5i2.2)') iyr(k),imon(k),iday(k)           &
                                   ,ihr(k),imin(k),isec(k)
        ii=int(thet(k)*100.0)
        write(name2,'(i4.4)') ii

!       ----------------------------------------------------------------
!       put the fold field to a tmp work field.
!       ----------------------------------------------------------------
        obsvel=spval
        vadvel=spval
        wrkvel=spval
        index=0
        unfvel=spval

        do j=1,nbeam; do i=iminrng,nrang
          obsvel(i,j)=orgvel(i,j,k)
          if ( abs(obsvel(i,j))>700.0 ) then
            index(i,j)=20
          endif
        enddo; enddo
!       ----------------------------------------------------------------
!       calculate reference radial velocity.
!       ----------------------------------------------------------------
        call full_field

      IF (outdiag) THEN
        name3='vadr'//name1(1:14)//'_'//name2(1:4)//'.dat'
        call write_radar( name3,vadvel )
      END IF
!       ----------------------------------------------------------------
!       simple unfolding
!       ----------------------------------------------------------------
        call multi_check_driver

      enddo   ! enddo k_tilt

      deallocate( obsvel )
      deallocate( vadvel )
      deallocate( unfvel )
      deallocate( wrkvel )
      deallocate( index  )

      END SUBROUTINE main_check_driver
