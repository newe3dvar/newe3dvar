!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
! ======================================================================
!     PURPOSE:
!       Using the wind profile to unfolding the data if it needed.
! ======================================================================
!
!     Author   : Gong, Jiandong et al.
!     Date     :
!     Action   : Created.
!
!     History  :
!     ----------
!     name     : Nai, Kang
!     date     : Aug. 15, 2007
!     action   : Modified it to fit the new two step dealiase agorithm.
!
!     name     : Nai, Kang
!     date     : Aug. 15, 2007
!     action   : Modified it to fit the single 0Vr method.
!
! ----------------------------------------------------------------------
      SUBROUTINE Multi_check_driver

      use Xu_variables

      implicit none

      integer  :: i,j,k,kp,kpp
      character(len=40) :: name1,name2,name3

      real, dimension(1:nr,1:np) :: wrkchek
      real, dimension(1:nr,1:np) :: chekwrk
      integer  :: need_unfold_check
      integer  :: ii,iend

      k=k_tilt
      nbeam = nphi(k)
      nrang = imaxrng(k)
      elvng = thet(k)
      vnyq  = thet_nyq_vel(k)

      
      write(name1,'(i4.4,5i2.2)') iyr(k),imon(k),iday(k)             &
                                 ,ihr(k),imin(k),isec(k)
      kp=int( thet(k)*100.0 )
      write(name2,'(i4.4)') kp

!     ------------------------------------------------------------------
!     filling in the missing value in the tmp work array.
!     ------------------------------------------------------------------
!     wrkvel=spval
!     index=0
!     unfvel=spval

!     ------------------------------------------------------------------
!     put the fold field to a tmp work field.
!     ------------------------------------------------------------------
!     do j=1,nbeam; do i=iminrng,nrang
!       obsvel(i,j)=chkvel(i,j)
!     enddo; enddo

!     if ( vcpnum==32 ) then
!       do j=1,nbeam; do i=121,nrang                 ! 30km
!         if ( abs(obsvel(i,j))<0.6 ) obsvel(i,j)=spval
!       enddo; enddo
!     endif

!     ------------------------------------------------------------------
!     reference check; do <80 km unfold and make a index array.
!     ------------------------------------------------------------------
      call reference_check(wrkchek)

!     ------------------------------------------------------------------
!     output the middle results if needed.
!     ------------------------------------------------------------------
      chekwrk=spval
      do j=1,nbeam; do i=iminrng,nrang
        chekwrk(i,j)=wrkvel(i,j)
        if ( index(i,j) == 10 ) then
          chekwrk(i,j) = spval
        endif
        if ( abs(chekwrk(i,j))<spval ) then
          count_total_points(k,3)=count_total_points(k,3)+1
        endif
      enddo; enddo
      IF (outdiag) THEN
      name3='vadf'//name1(1:14)//'_'//name2(1:4)//'.dat'
      call write_radar(name3,chekwrk)
      END IF
!     name3='vadi'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,wrkchek)

!     ------------------------------------------------------------------
!     whole body check if the wind needs to be unfold.
!     which based on the refvel field.
!     ------------------------------------------------------------------
      call continue_check

!     ------------------------------------------------------------------
!     replace the fold filed by the unfold field.
!     ------------------------------------------------------------------
      do j=1,nbeam
      do i=iminrng,nrang
        vel(i,j,k)=unfvel(i,j)
        if ( abs(vel(i,j,k))<spval ) then
          count_total_points(k,2)=count_total_points(k,2)+1
        endif
      enddo
      enddo

      END SUBROUTINE Multi_check_driver
