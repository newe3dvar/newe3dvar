!     ##################################################################
!     ##################################################################
!     ####                                                          ####
!     ####              MAIN PROGRAM RUC2RADAR_analyses             ####
!     ####                                                          ####
!     ####             Developed by Cooperative Institute           ####
!     ####            for Mesoscale Meteorological Studies          ####
!     ####                  University of Oklahoma                  ####
!     ####                                                          ####
!     ##################################################################
      subroutine model_compare

!-----------------------------------------------------------------------
!
!     PURPOSE:
!       read in the RUC model U, V background field and interpolate to
!       the radial velocity at the available observational points.
!
!-----------------------------------------------------------------------
!
!     AUTHOR: Kang Nai
!     Date  : 01/07/2013
!     Action: created
!
!-----------------------------------------------------------------------
      use Xu_variables

      IMPLICIT NONE
!-----------------------------------------------------------------------
!     parameter for the RUC field.
!-----------------------------------------------------------------------
      integer                           :: nz_mdl
      real                              :: lat_mdl,lon_mdl
      real                              :: dlon_mdl,dlat_mdl
      integer                           :: mdl_jultime
      real,allocatable,dimension(:)     :: P_mdl
      real,allocatable,dimension(:,:,:) :: U_mdl,V_mdl,T_mdl
      real,allocatable,dimension(:,:)   :: Ps_mdl,Ts_mdl,Us_mdl,Vs_mdl
      !real,allocatable,dimension(:)     :: x_mdl,y_mdl
      real                              :: Ps_hgt,Ts_hgt,Us_hgt,Vs_hgt

!-----------------------------------------------------------------------
!     parameter for the local
!-----------------------------------------------------------------------
      integer                           :: i,i1,i2,ii,j,j1,j2,jj,k,k1,k2
      real                              :: a1,a2,a3,a4,a5,a6,a7,a8
      real                              :: b1,b2,c1,c2,d1,d2
      character(len=80)                 :: name1,name2,name3,name4
      real                              :: lxx,lyy,lxy
      integer                           :: k11,k12,k21,k22
      integer                           :: mlat_mdl

!
      integer                           :: mz_mdl
      real,allocatable,dimension(:)     :: p11,t11,u11,v11
      real,allocatable,dimension(:)     :: z11

!
      real                              :: xr1,yr1
      integer                           :: ix,jy
      real                              :: rlat,rlon
      real                              :: ubkgd,vbkgd,belt,vr
      integer                           :: iflag

      integer                           :: n1,n2,n3,n4
      logical                           :: interp

      REAL               :: latgate, ygate    ! latitude
      REAL               :: longate, xgate    ! longatitude
      REAL               :: z_model_step

!***********************************************************************
!     Execute
!***********************************************************************
      print*,' '
      print*,'In the model compare process'
      print*,' '

      model_interpolate=.false.

      latnot(1)=trulat1
      latnot(2)=trulat2

      IF (bgrid_model == 1) THEN

        z_model=70                  ! 60*250=17500 meter
        z_model_step=250.0          ! meter

!     ------------------------------------------------------------------
!     read in the surface Ps field
!     ------------------------------------------------------------------
      open(1,file='Ps_mdl_grid.model',status='old')
        read(1,'(2i8)') nlon_mdl,nlat_mdl

        allocate( Ps_mdl(1:nlon_mdl,1:nlat_mdl) )

        read(1,'(3f10.3)') lat_mdl,lon_mdl,Ps_hgt
        read(1,'(2f20.8)') dlon_mdl,dlat_mdl
        read(1,'(i20)') mdl_jultime
        read(1,'(10f10.1)') ((Ps_mdl(i,j),i=1,nlon_mdl),j=1,nlat_mdl)
      close(1)
!     ------------------------------------------------------------------
!     read in the surface Ts field
!     ------------------------------------------------------------------
      open(1,file='Ts_mdl_grid.model',status='old')
        read(1,'(2i8)') nlon_mdl,nlat_mdl

        allocate( Ts_mdl(1:nlon_mdl,1:nlat_mdl) )

        read(1,'(3f10.3)') lat_mdl,lon_mdl,Ts_hgt
        read(1,'(2f20.8)') dlon_mdl,dlat_mdl
        read(1,'(i20)') mdl_jultime
        read(1,'(10f10.1)') ((Ts_mdl(i,j),i=1,nlon_mdl),j=1,nlat_mdl)
      close(1)
!     ------------------------------------------------------------------
!     read in the surface Us field
!     ------------------------------------------------------------------
      open(1,file='Us_mdl_grid.model',status='old')
        read(1,'(2i8)') nlon_mdl,nlat_mdl

        allocate( Us_mdl(1:nlon_mdl,1:nlat_mdl) )

        read(1,'(3f10.3)') lat_mdl,lon_mdl,Us_hgt
        read(1,'(2f20.8)') dlon_mdl,dlat_mdl
        read(1,'(i20)') mdl_jultime
        read(1,'(10f10.1)') ((Us_mdl(i,j),i=1,nlon_mdl),j=1,nlat_mdl)
      close(1)
!     ------------------------------------------------------------------
!     read in the surface Vs field
!     ------------------------------------------------------------------
      open(1,file='Vs_mdl_grid.model',status='old')
        read(1,'(2i8)') nlon_mdl,nlat_mdl

        allocate( Vs_mdl(1:nlon_mdl,1:nlat_mdl) )

        read(1,'(3f10.3)') lat_mdl,lon_mdl,Vs_hgt
        read(1,'(2f20.8)') dlon_mdl,dlat_mdl
        read(1,'(i20)') mdl_jultime
        read(1,'(10f10.1)') ((Vs_mdl(i,j),i=1,nlon_mdl),j=1,nlat_mdl)
      close(1)
!     ------------------------------------------------------------------
!     read in the RUC U field
!     ------------------------------------------------------------------
      open(1,file='U_mdl_grid.model',form='unformatted'              &
            ,status='old')
       read(1) nlon_mdl,nlat_mdl,nz_mdl

       allocate( U_mdl(1:nlon_mdl,1:nlat_mdl,1:nz_mdl) )
       allocate( P_mdl(1:nz_mdl) )
       allocate( x_mdl(1:nlon_mdl) )
       allocate( y_mdl(1:nlat_mdl) )

       read(1) lat_mdl,lon_mdl
       read(1) dlon_mdl,dlat_mdl
       read(1) mdl_jultime
       read(1) (P_mdl(k),k=1,nz_mdl)
       read(1) (((U_mdl(i,j,k),i=1,nlon_mdl),j=1,nlat_mdl),k=1,nz_mdl)
      close(1)
!     ------------------------------------------------------------------
!     read in the RUC V field
!     ------------------------------------------------------------------
      open(1,file='V_mdl_grid.model',form='unformatted'              &
            ,status='old')
       read(1) nlon_mdl,nlat_mdl,nz_mdl

       allocate( V_mdl(1:nlon_mdl,1:nlat_mdl,1:nz_mdl) )

       read(1) lat_mdl,lon_mdl
       read(1) dlon_mdl,dlat_mdl
       read(1) mdl_jultime
       read(1) (P_mdl(k),k=1,nz_mdl)
       read(1) (((V_mdl(i,j,k),i=1,nlon_mdl),j=1,nlat_mdl),k=1,nz_mdl)
      close(1)
!     ------------------------------------------------------------------
!     read in the RUC T field
!     ------------------------------------------------------------------
      open(1,file='T_mdl_grid.model',form='unformatted'              &
            ,status='old')
       read(1) nlon_mdl,nlat_mdl,nz_mdl

       allocate( T_mdl(1:nlon_mdl,1:nlat_mdl,1:nz_mdl) )

       read(1) lat_mdl,lon_mdl
       read(1) dlon_mdl,dlat_mdl
       read(1) mdl_jultime
       read(1) (P_mdl(k),k=1,nz_mdl)
       read(1) (((T_mdl(i,j,k),i=1,nlon_mdl),j=1,nlat_mdl),k=1,nz_mdl)
      close(1)
!     ------------------------------------------------------------------
!     calculate the points location.
!     ------------------------------------------------------------------
      do j=1,nlat_mdl
        jj=nlat_mdl-j+1
        y_mdl(jj)=lat_mdl-float(j-1)*dlat_mdl
      enddo
      do i=1,nlon_mdl
        x_mdl(i)= lon_mdl+float(i-1)*dlon_mdl
      enddo

!     ------------------------------------------------------------------
!     get the local array from the P to Z
!     ------------------------------------------------------------------
      allocate( ubgrid(1:nlon_mdl,1:nlat_mdl,1:z_model) )
      allocate( vbgrid(1:nlon_mdl,1:nlat_mdl,1:z_model) )
      allocate( zbgrid(1:nlon_mdl,1:nlat_mdl,1:z_model) )
      ubgrid=spval
      vbgrid=spval
      zbgrid=spval

      allocate( p11(1:nz_mdl+1) )
      allocate( t11(1:nz_mdl+1) )
      allocate( z11(1:nz_mdl+1) )
      allocate( u11(1:nz_mdl+1) )
      allocate( v11(1:nz_mdl+1) )

      do j=1,nlat_mdl
      do i=1,nlon_mdl
        if ( Ps_mdl(i,j)<0.0 ) cycle

        p11(1)=Ps_mdl(i,j)
        t11(1)=Ts_mdl(i,j)
        u11(1)=Us_mdl(i,j)
        v11(1)=Vs_mdl(i,j)

        if ( P_mdl(1)<Ps_mdl(i,j) ) then
          k1=1
          do k=1,nz_mdl
            k1=k1+1
            p11(k1)=P_mdl(k)
            t11(k1)=T_mdl(i,j,k)
            u11(k1)=U_mdl(i,j,k)
            v11(k1)=V_mdl(i,j,k)
          enddo
          mz_mdl=k1
        else
          do k=1,nz_mdl-1
            if ( P_mdl(k)>=Ps_mdl(i,j) .and.                         &
                 P_mdl(k+1)<=Ps_mdl(i,j) ) then
              k11=k+1
            endif
          enddo
          k1=1
          do k=k11,nz_mdl
            k1=k1+1
            p11(k1)=P_mdl(k)
            t11(k1)=T_mdl(i,j,k)
            u11(k1)=U_mdl(i,j,k)
            v11(k1)=V_mdl(i,j,k)
          enddo
          mz_mdl=k1
        endif

        z11(1)=0.0
        do k=1,mz_mdl-1
          a1=-14.6355*(t11(k+1)+t11(k))*alog(p11(k+1)/p11(k))
          z11(k+1)=z11(k)+a1
        enddo
        z11(1)=10.0    ! us,vs at 10.0

!       interpolate at vertical
        do k=1,z_model
          a1=float(k)*z_model_step
          zbgrid(i,j,k)=a1
          do k1=1,mz_mdl-1
            if ( z11(k1+1)>=a1 .and. z11(k1)<=a1 ) then
              b1=(u11(k1+1)-u11(k1))/(z11(k1+1)-z11(k1))
              b2=u11(k1)+b1*(a1-z11(k1))
              c1=(v11(k1+1)-v11(k1))/(z11(k1+1)-z11(k1))
              c2=v11(k1)+c1*(a1-z11(k1))
            endif
          enddo
          ubgrid(i,j,k)=b2
          vbgrid(i,j,k)=c2
        enddo
      enddo
      enddo

!     release some memory
      deallocate ( U_mdl,V_mdl,P_mdl,T_mdl )
      deallocate ( Ps_mdl,Ts_mdl,Us_mdl,Vs_mdl )
      deallocate ( p11,t11,z11,u11,v11 )

!*    set the center point.
!     CALL setmapr(mapproj,sclfct,latnot,trulon)
!     CALL lltoxy(1,1,radlat,radlon,xr0,yr0)
!
!     PRINT*
!     PRINT*,'center lat,lon = ',radlat,radlon
!     PRINT*,'xr0,yr0 = ',xr0,yr0
!
!     CALL setorig(1,xr0,yr0)
!     CALL lltoxy(1,1,radlat,radlon,xr0,yr0)
!
!     PRINT*
!     PRINT*,'lat,lon = ',radlat,radlon
!     PRINT*,'xr0,yr0 = ',xr0,yr0
!     PRINT*
      ELSE
        CALL use_model_mapproj
      END IF
!     ------------------------------------------------------------------
!     ****    get the radar radial velocity by using grid data      ****
!     ------------------------------------------------------------------
      d1=0.0
      d2=0.0
      do k=1,nthet
       nbeam = nphi(k)
       nrang = imaxrng(k)
       vnyq=thet_nyq_vel(k)
       n1=0;n2=0;n3=0;n4=0
       do i=nrang,1,-1
        if ( abs(velref(i,1,k))<spval ) then
          if ( wrkhgt(i,k)>5000.0 ) then
            n1=n1+1
            if ( n1<=15 ) then
              interp=.true.
            else
              interp=.false.
            endif
          endif
          if ( wrkhgt(i,k)>1500.0 .and. wrkhgt(i,k)<=5000.0 ) then
            n2=n2+1
            if ( n2<=10 ) then
              interp=.true.
            else
              interp=.false.
            endif
          endif
          if ( wrkhgt(i,k)>900.0 .and. wrkhgt(i,k)<=1500.0 ) then
            n3=n3+1
            if ( n3<=5 ) then
              interp=.true.
            else
              interp=.false.
            endif
          endif
          if ( wrkhgt(i,k)<=900.0 ) then
            n4=n4+1
            if ( n4<=5 ) then
              interp=.true.
            else
              interp=.false.
            endif
          endif
        else
          interp=.false.
        endif

        if ( interp ) then
          a1=0.0
          a2=0.0
          do j=1,nbeam
            azmth=phi(j,k)
            range=ran(i)
            elvng=thet(k)
            call beamhgt( elvng,range,hgtrad,sfcrng )
            call gcircle( radlat,radlon,azmth,sfcrng,latgate,longate )
            IF (bgrid_model == 2) THEN
              CALL lltoxy(1,1,latgate,longate,xgate,ygate)
            ELSE
              xgate = longate
              ygate = latgate
            END IF
            belt=(90.0-azmth)*rdn
            if ( hgtrad>0.0 .and. hgtrad<zstep*zlevel ) then
              call linearint_3d ( nlon_mdl,nlat_mdl,z_model          &
                                 ,ubgrid,x_mdl,y_mdl                 &
                                 ,zbgrid,1,xgate,ygate,hgtrad        &
                                 ,ubkgd,iflag,range,elvng )
              call linearint_3d ( nlon_mdl,nlat_mdl,z_model          &
                                 ,vbgrid,x_mdl,y_mdl                 &
                                 ,zbgrid,1,xgate,ygate,hgtrad        &
                                 ,vbkgd,iflag,range,elvng )
            endif
            vr=spval
            if ( abs(ubkgd)<spval .and. abs(vbkgd)<spval ) then
              vr=ubkgd*cos(belt)+vbkgd*sin(belt)
            endif
            if ( abs(vel(i,j,k))<spval ) then
              a1=a1+(vr-velref(i,j,k))**2
              a2=a2+1.0
            endif
          enddo
          if ( a2>1.0 ) then
            d1=d1+sqrt(a1/a2)/vnyq
            d2=d2+1.0
          endif
        endif
       enddo
       print*,' '
       print*,'Total available compare lines==',n1,n2,n3,n4
       print*,' '
      enddo

      if ( d2>1.0 .and. (d1/d2)<(1.0/3.0) ) then
        model_interpolate=.true.
!       ----------------------------------------------------------------
!       ****    get the radar radial velocity by using grid data    ****
!       ----------------------------------------------------------------
        do k=1,nthet
          do ilevel=1,zlevel
            if ( model_circ(k,ilevel) > 0 ) then
              i=model_circ(k,ilevel)
              a1=(360.0/mspc)
              do j=1,mspc
                azmth=float(j-1)*a1+0.5
                range=ran(i)
                elvng=thet(k)
                call beamhgt( elvng,range,hgtrad,sfcrng )
                call gcircle( radlat,radlon,azmth,sfcrng,latgate,longate )
                IF (bgrid_model == 2) THEN
                  CALL lltoxy(1,1,latgate,longate,xgate,ygate)
                ELSE
                  xgate = longate
                  ygate = latgate
                END IF
                belt=(90.0-azmth)*rdn
                call linearint_3d ( nlon_mdl,nlat_mdl,z_model        &
                                   ,ubgrid,x_mdl,y_mdl               &
                                   ,zbgrid,1,xgate,ygate,hgtrad      &
                                   ,ubkgd,iflag,range,elvng )
                call linearint_3d ( nlon_mdl,nlat_mdl,z_model        &
                                   ,vbgrid,x_mdl,y_mdl               &
                                   ,zbgrid,1,xgate,ygate,hgtrad      &
                                   ,vbkgd,iflag,range,elvng )
                if ( abs(ubkgd)<spval .and. abs(vbkgd)<spval ) then
                  analy_back(j,k,ilevel)=ubkgd*cos(belt)+vbkgd*sin(belt)
                endif
              enddo
            endif
          enddo
        enddo
      endif

      deallocate ( x_mdl,y_mdl )
      deallocate ( ubgrid,vbgrid,zbgrid )
!
      IF (bgrid_model == 2) THEN
        CALL use_radar_mapproj
      END IF

      RETURN
      end subroutine model_compare
