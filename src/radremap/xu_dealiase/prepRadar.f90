!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!=======================================================================
!     PURPOSE:
!       shift the first gate distance if this distance is negative
!       originally.
! ======================================================================
!
!     Author   : Gong, Jiandong etal
!     Date     :
!     Action   : Created.
!
!     History  :
!    -----------
!     Aug. 14, 2007  Nai, Kang re-arrange the radar data to clockwise
!                    beginning by the northest beam.
!
! ----------------------------------------------------------------------
      SUBROUTINE PREP_RADAR

      use xu_variables

      IMPLICIT NONE

      INTEGER    :: i,j,k,iskip,ii,iii,kp
      real, allocatable, dimension(:) :: vel1cyc,ref1cyc,wrksum,wrktmp
      character(len=50) :: name1,name2
      real       :: a1,a2
      integer    :: low_tilt

      real,allocatable,dimension(:,:) :: tmpvel

! ----------------------------------------------------------------------
!     shift radial velocity and the spectrum width.
! ----------------------------------------------------------------------
      if ( rfstgat.lt.0.0 ) then
        iskip=0
        do i=1,nr
          rfstgat=rfstgat+refgatsp
          if (rfstgat.gt.1.0) exit
        enddo
        iskip=i

        do k=1,nthet
          nbeam = nphi(k)
          nrang = imaxrng(k)
          do j=1,nbeam
          do i=iminrng,nrang-iskip
            vel(i,j,k)=vel(i+iskip,j,k)
            swg(i,j,k)=swg(i+iskip,j,k)
          enddo
          enddo
          imaxrng(k)=imaxrng(k)-iskip
        enddo
      endif

! ----------------------------------------------------------------------
!     get the range distance(meter)
! ----------------------------------------------------------------------
      do i=1,nr
        ran(i) = rfstgat + float(i-1)*refgatsp
        if ( ran(i)<10000.0 ) then
          n10km=i
        endif
      enddo

      n10km=n10km+1

! ----------------------------------------------------------------------
!     get the vertical height and surface distance(meter)
! ----------------------------------------------------------------------
      do k=1,nthet
        nbeam = nphi(k)
        nrang = imaxrng(k)
        elvng = thet(k)
        do i=1,nr
          range=ran(i)
          call beamhgt(elvng,range,hgtrad,sfcrng)
          wrkhgt(i,k)=hgtrad
          wrksfc(i,k)=sfcrng
        enddo
      enddo

! ----------------------------------------------------------------------
!     shift reflectivity
! ----------------------------------------------------------------------
      if ( rfstgat2.lt.0.0 ) then
        iskip=0
        do i=1,nr2
          rfstgat2=rfstgat2+refgatsp2
          if (rfstgat2.gt.1.0) exit
        enddo
        iskip=i

        do i=1,nr2
          ran2(i) = rfstgat2 + float(i-1)*refgatsp2
        enddo

        do k=1,nthet
          nbeam = nphi2(k)
          nrang2= imaxrng2(k)
          do j=1,nbeam
          do i=iminrng2,nrang2-iskip
            ref(i,j,k)=ref(i+iskip,j,k)
          enddo
          enddo
          imaxrng2(k)=imaxrng2(k)-iskip
        enddo
      endif

! ----------------------------------------------------------------------
!     get the range distance(meter) of the reflictivity.
!     it is differ at the old lagecy data.
! ----------------------------------------------------------------------
      do i=1,nr2
        ran2(i) = rfstgat2 + float(i-1)*refgatsp2
      enddo

! ----------------------------------------------------------------------
!     sort the velocity data along the azimuth begin with the north points.
! ----------------------------------------------------------------------
      allocate( vel1cyc(1:np) )
      allocate( ref1cyc(1:np) )
      allocate( wrksum(1:np) )
      allocate( wrktmp(1:np) )

      do k=1,nthet
        nbeam = nphi(k)
        nrang = imaxrng(k)
        elvng=thet(k)
        wrksum=spval
        do j=1,nbeam
          wrksum(j)=phi(j,k)
        enddo
        do i=iminrng,nrang
          vel1cyc=spval
          do j=1,nbeam
            vel1cyc(j)=vel(i,j,k)
          enddo
          wrktmp=wrksum; call sort2(nbeam,wrktmp,vel1cyc)
          do j=1,nbeam
            vel(i,j,k)=vel1cyc(j)
          enddo

          vel1cyc=spval
          do j=1,nbeam
            vel1cyc(j)=swg(i,j,k)
          enddo
          wrktmp=wrksum; call sort2(nbeam,wrktmp,vel1cyc)
          do j=1,nbeam
            swg(i,j,k)=vel1cyc(j)
          enddo
        enddo
        do j=1,nbeam
          phi(j,k)=wrktmp(j)
        enddo
      enddo

! ----------------------------------------------------------------------
!     sort the reflictivity data along the azimuth begin with the north points.
! ----------------------------------------------------------------------
      do k=1,nthet
        nbeam = nphi2(k)
        nrang = imaxrng2(k)
        wrksum=spval
        do j=1,nbeam
          wrksum(j)=phi2(j,k)
        enddo
        do i=iminrng2,imaxrng2(k)
          ref1cyc=spval
          do j=1,nbeam
            ref1cyc(j)=ref(i,j,k)
          enddo
          wrktmp=wrksum; call sort2(nbeam,wrktmp,ref1cyc)
          do j=1,nbeam
            ref(i,j,k)=ref1cyc(j)
          enddo
        enddo
        do j=1,nbeam
          phi2(j,k)=wrktmp(j)
        enddo
      enddo

! ----------------------------------------------------------------------
!     count the available data.
! ----------------------------------------------------------------------
      do k=1,nthet
        nbeam = nphi(k)
        nrang = imaxrng(k)
        do j=1,nbeam
        do i=1,nrang
          orgvel(i,j,k)=vel(i,j,k)
          if ( abs(orgvel(i,j,k))<spval ) then
            count_total_points(k,1)=count_total_points(k,1)+1
          endif
        enddo
        enddo
      enddo

! ----------------------------------------------------------------------
!     output the velocity data.
! ----------------------------------------------------------------------
!     allocate( tmpvel(1:nr,1:np) )
!     do k_tilt=1,nthet
!       k=k_tilt
!       nbeam = nphi(k)
!       nrang = imaxrng(k)
!       elvng=thet(k)
!       tmpvel=spval
!       do j=1,nbeam
!       do i=iminrng,nrang
!         tmpvel(i,j)=orgvel(i,j,k)
!       enddo
!       enddo
!       kp=int( thet(k)*100.0 )
!       write(name1,'(i4.4)') kp
!       name2='vada'//name1(1:4)//'.dat'
!       call write_radar(name2,tmpvel)
!     enddo
!     deallocate( tmpvel )

      deallocate( vel1cyc,ref1cyc,wrksum,wrktmp )

      return
      END SUBROUTINE PREP_RADAR

      SUBROUTINE sort2(n,arr,brr)
      INTEGER n,M,NSTACK
      REAL arr(n),brr(n)
      PARAMETER (M=7,NSTACK=50)
      INTEGER i,ir,j,jstack,k,l,istack(NSTACK)
      REAL a,b,temp
      jstack=0
      l=1
      ir=n
1     if(ir-l.lt.M)then
        do 12 j=l+1,ir
          a=arr(j)
          b=brr(j)
          do 11 i=j-1,l,-1
            if(arr(i).le.a)goto 2
            arr(i+1)=arr(i)
            brr(i+1)=brr(i)
11        continue
          i=l-1
2         arr(i+1)=a
          brr(i+1)=b
12      continue
        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        temp=arr(k)

        arr(k)=arr(l+1)
        arr(l+1)=temp
        temp=brr(k)
        brr(k)=brr(l+1)
        brr(l+1)=temp
        if(arr(l).gt.arr(ir))then
          temp=arr(l)
          arr(l)=arr(ir)
          arr(ir)=temp
          temp=brr(l)
          brr(l)=brr(ir)
          brr(ir)=temp
        endif
        if(arr(l+1).gt.arr(ir))then
          temp=arr(l+1)
          arr(l+1)=arr(ir)
          arr(ir)=temp
          temp=brr(l+1)
          brr(l+1)=brr(ir)
          brr(ir)=temp
        endif
        if(arr(l).gt.arr(l+1))then
          temp=arr(l)
          arr(l)=arr(l+1)
          arr(l+1)=temp

          temp=brr(l)
          brr(l)=brr(l+1)
          brr(l+1)=temp
        endif
        i=l+1
        j=ir
        a=arr(l+1)
        b=brr(l+1)
3       continue
          i=i+1
        if(arr(i).lt.a)goto 3
4       continue
          j=j-1
        if(arr(j).gt.a)goto 4
        if(j.lt.i)goto 5
        temp=arr(i)
        arr(i)=arr(j)
        arr(j)=temp
        temp=brr(i)
        brr(i)=brr(j)
        brr(j)=temp
        goto 3
5       arr(l+1)=arr(j)
        arr(j)=a
        brr(l+1)=brr(j)
        brr(j)=b

        jstack=jstack+2
        if(jstack.gt.NSTACK) stop 'NSTACK too small in sort2'
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif
      goto 1
      END
