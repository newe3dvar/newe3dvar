!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
      SUBROUTINE model_analy
!
! ======================================================================
!     PURPOSE:
!       Using selected model background cycle data and the observed data
!       to do the conjugate job to get the best fitting analysis data.
! ======================================================================
!
!     Author   : Nai, Kang
!     Date     : Jan. 08, 2013
!     Action   : Created.
!
!     History  :
!    -----------
!
! ----------------------------------------------------------------------

      use Xu_variables

      implicit none

      integer :: start_level,end_level
      integer :: k,jm,jn,klevel,jk
      integer :: ii,j,i1,i2,npath,n
      logical :: first_check

! ######################################################################
!     Execute.
! ######################################################################
      print*,' '
      print*,'In the model analyses process'
      print*,' '

      start_level=int((hstart+1.0)/zstep)   ! the lowest level is 250 m

      do k_tilt=1,nthet
        first_check=.false.
        do ilevel=start_level,zlevel
!         ==============================================================
!         get the available bottom level
!         ==============================================================
          if ( model_circ(k_tilt,ilevel)>0 ) then
            first_check=.true.
            jm=ilevel
            exit
          endif
        enddo
        if ( first_check ) then
          do ilevel=zlevel,start_level,-1
!           ============================================================
!           get the available top level
!           ============================================================
            if ( model_circ(k_tilt,ilevel)>0 ) then
              jn=ilevel
              exit
            endif
          enddo
!         ==============================================================
!         using variation method to get the refvel.
!         ==============================================================
          mdlvel=spval
          mdlheight=-spval
          do ilevel=jm,jn
            call model_conjugate
          enddo
        endif
      enddo

      END SUBROUTINE model_analy
