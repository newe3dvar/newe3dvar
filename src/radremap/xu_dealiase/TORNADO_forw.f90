!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!     ==================================================================
!     PURPOSE:
!       Check the small special area and to unfold the data follow the
!       sine or cosine curve if needed.
!     ==================================================================
!
!     Author   : Nai, Kang
!     Date     : Mar. 08, 2012
!     Action   : Created.
!
!     History  :
!     ----------
!
!     ------------------------------------------------------------------
!
!     INPUT :
!
!       maxgate  Maximum number of gates in a radial
!       maxazim  Maximum number of radials in a tilt
!       ngate    Number of gates in radial
!       nazim    Number of radials
!       rvel     Doppler radial velocity
!       ovel     Doppler radial velocity
!       vnyq     Nyquist velocity
!       enangle  tilt
! 
!     WORK ARRAYS:
!     
!       tmp_forw
!       indx_forw     
!       tmp_back
!       indx_back
!
!     ------------------------------------------------------------------
      SUBROUTINE TORNADO_forw( maxgate,maxazim,ngate,nazim,n10km     &
                              ,ovel,vnyq,enangle,vcpnum,ran,phi      &
                              ,rvel,index,unfvel                     &
                              ,tmp3,spval                            &
                              ,iyr,imon,iday                         &
                              ,ihr,imin,isec                         &
                              ,special_points )
 
      IMPLICIT NONE
 
      INTEGER, INTENT(IN) :: maxgate
      INTEGER, INTENT(IN) :: maxazim
      INTEGER, INTENT(IN) :: ngate
      INTEGER, INTENT(IN) :: nazim
!     INTEGER, INTENT(IN) :: n10km
      INTEGER             :: n10km

      REAL, INTENT(IN)    :: ovel(maxgate,maxazim)
      REAL, INTENT(IN)    :: vnyq
      REAL, INTENT(IN)    :: enangle
      INTEGER, INTENT(IN) :: vcpnum
      REAL, INTENT(IN)    :: ran(maxgate)
      REAL, INTENT(IN)    :: phi(maxazim)

      REAL                :: rvel(maxgate,maxazim)
      INTEGER             :: index(maxgate,maxazim)
      REAL, INTENT(OUT)   :: unfvel(maxgate,maxazim)

      REAL, INTENT(IN)    :: tmp3(maxgate)
      REAL, INTENT(IN)    :: spval

      INTEGER :: iyr,imon,iday,ihr,imin,isec
      INTEGER :: special_points
  
      INTEGER :: igate,iray
      REAL    :: twonyq,inv2nyq,thrpri
      REAL    :: refvel,tstdev,tstvel,thresh

      integer :: i,ii,j,jj,k,m,n
      real    :: a1,a2,a3,a4,a5
      real    :: b1,b2,b3,b4,b5
      character(len=80) :: name1,name2,name3

      real,allocatable,dimension(:,:)    :: cyclone
      real,allocatable,dimension(:,:)    :: tmp_forw,dif_forw
      real,allocatable,dimension(:,:)    :: tmp_back,dif_back
      integer,allocatable,dimension(:,:) :: indx_forw,indx_back
      integer :: ip,ipst,ipen,ippp,kp,k1
      integer,dimension(:),allocatable   :: kgoodnum
      integer,dimension(:,:),allocatable :: kgoodbgn,kgoodend
      integer :: step_step(11)
      logical :: cont_tmp

!     @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!     Beginning of executable code...
!     @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      twonyq = 2.0*vnyq
      inv2nyq = 1./twonyq
      thrpri = 0.65*vnyq
      thresh=vnyq/3.0

      step_step(1)=0;step_step(2)=-1;step_step(3)=1
      step_step(4)=-2;step_step(5)=2
      step_step(6)=-3;step_step(7)=3
      step_step(8)=-4;step_step(9)=4
      step_step(10)=-5;step_step(11)=5

      write(name1,'(i4.4,5i2.2)') iyr,imon,iday,ihr,imin,isec
      ii=int( enangle*100.0 )
      write(name2,'(i4.4)') ii

      allocate( tmp_forw(maxgate,maxazim) )
      allocate( indx_forw(maxgate,maxazim) )
      allocate( dif_forw(maxgate,maxazim) )
      allocate( tmp_back(maxgate,maxazim) )
      allocate( indx_back(maxgate,maxazim) )
      allocate( dif_back(maxgate,maxazim) )

      allocate( cyclone(maxgate,maxazim) )
!     ------------------------------------------------------------------
!     Set up tmp_forward and tmp_back to be quality arrays. 
!     set up indx_forward, indx_back; -20=good 10=bad/missing.
!     ------------------------------------------------------------------
      tmp_forw=spval
      tmp_back=spval
      DO iray=1,nazim
        DO igate=1,ngate
          if(abs(rvel(igate,iray))<spval .and. index(igate,iray)==0)then
            tmp_forw(igate,iray)=rvel(igate,iray)
            tmp_back(igate,iray)=rvel(igate,iray)
            indx_forw(igate,iray)=-20
            indx_back(igate,iray)=-20
          elseif ( index(igate,iray)==50 ) then
            indx_forw(igate,iray)=50
            indx_back(igate,iray)=50
          else
            indx_forw(igate,iray)=10
            indx_back(igate,iray)=10
          endif
        END DO
      END DO

      n10km=2
 
!     special point re-check
      Do igate=n10km,ngate        ! from radar to outside
      DO iray=1,nazim
        if ( indx_forw(igate,iray)==-20 ) then
          b1=0.0
          do ii=1,5
            i=igate-ii
            if ( i<1 ) cycle
            if ( indx_forw(i,iray)==50 ) then
              b1=b1+1.0
            endif
          enddo
          b2=0.0
          do ii=1,5
            i=igate+ii
            if ( i>ngate ) cycle
            if ( indx_forw(i,iray)==50 ) then
              b2=b2+1.0
            endif
          enddo
          IF ( b1>1.0 .and. b2>1.0 ) then
            indx_forw(igate,iray)=50
            tmp_forw(igate,iray)=spval
          ENDIF
        endif
      ENDDO
      ENDDO

!     name3='forw'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='back'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)

!     ------------------------------------------------------------------
!     beam special area check.
!     ------------------------------------------------------------------
      allocate ( kgoodnum(1:nazim) )
      allocate ( kgoodbgn(1:ngate,1:nazim) )
      allocate ( kgoodend(1:ngate,1:nazim) )
      kgoodbgn=0
      kgoodend=0
      kgoodnum=0
      DO iray=1,nazim
        ippp=0
        ip=36
   10   continue
        ipst=0
        if ( ip<ngate ) then
          do i=ip,ngate
            if ( abs(ovel(i,iray))<spval .and.                        &
                 indx_forw(i,iray)==50 ) then
              ipst=i ; exit
            endif
          enddo
          if ( ipst>0 .and. ipst<ngate ) then
            ipen=ngate+1
            do i=ipst+1,ngate
              if ( indx_forw(i,iray)==-20 ) then
                ipen=i-1 ; exit
              endif
            enddo
            if ( ipen<ngate ) then
              if ( (ipen-ipst) >=2 ) then
                ippp=ippp+1
                kgoodnum(iray)=ippp
                kgoodbgn(ippp,iray)=ipst
                kgoodend(ippp,iray)=ipen
              else
                do i=ipst,ipen
                  indx_forw(i,iray)=10
                enddo
              endif
            endif
          endif
          ip=i
          go to 10
        endif
      ENDDO
!     ------------------------------------------------------------------
!     beam special area continue unfolding.
!     ------------------------------------------------------------------
      DO iray=1,nazim
       kp=kgoodnum(iray)
       if ( kp==0 ) cycle
       do k=1,kp
        ipst=kgoodbgn(k,iray)
        ipen=kgoodend(k,iray)
        IF ( (ipen-ipst+1)>6 ) THEN
          refvel= tmp_forw(ipst-1,iray)
          ip=ipst
!         print*,iray,ipst,ipen,ip,refvel
          if ( abs(refvel)<100.0 ) then          ! no ref
            cont_tmp=.false.
            k1=0
            do i=ipst,ipen
             IF ( abs(ovel(i,iray))<spval ) THEN
               tstdev=twonyq*NINT((ovel(i,iray)-refvel)*inv2nyq)
               tstvel=ovel(i,iray)-tstdev
!              write(999,'(4i6,3f10.1)') iray,i,ip,k1                &
!                                       ,ovel(i,iray),refvel,tstvel
               if ( abs(tstvel-refvel)<thrpri ) then
                 IF ( tstvel*refvel>0.0 .and.                        &
                      abs(tstvel-refvel)<=10.0 ) THEN
                   if (abs(refvel)>8.5 ) then
                     do jj=2,5
                      j=iray+step_step(jj)
                      if ( j<0 ) j=j+nazim
                      if ( j>nazim ) j=j-nazim
                      if ( abs(tmp_forw(i,j))<spval ) then
                        a1=tmp_forw(i,j)*tstvel
                        if ( a1>0.0 ) then
                          tmp_forw(i,iray)=tstvel
                          indx_forw(i,iray)=-10
                          refvel= tstvel
                          k1=i
                          cont_tmp=.true.
                          exit
                        else
                          ip=i
                          cont_tmp=.false.
                          exit
                        endif
                      endif
                     enddo
                     IF ( k1==0 ) THEN
                       ip=ipst
                       cont_tmp=.false.
                     ENDIF
                   else
                     ip=i
                     cont_tmp=.false.
                   endif
                 ELSE
                   ip=i
                   cont_tmp=.false.
                 ENDIF
               else
                 cont_tmp=.true.
               endif
             ELSE
               cont_tmp=.true.
             ENDIF

             IF ( .not. cont_tmp ) EXIT

            enddo
          endif
          if ( k1<ipen ) then
            refvel= tmp_forw(ipen+1,iray)
!           print*,iray,ip,refvel
            do i=ipen,ip,-1
             IF ( abs(ovel(i,iray))<spval ) THEN
               tstdev=twonyq*NINT((ovel(i,iray)-refvel)*inv2nyq)
               tstvel=ovel(i,iray)-tstdev
               a1=refvel
               if ( abs(a1)<60.0 ) then
                 IF ( abs(tstvel-refvel)<thrpri ) THEN
                   if ( abs(refvel)<3.0 ) then
                     tmp_forw(i,iray)=tstvel
                     indx_forw(i,iray)=-10
                     refvel= tstvel
                   elseif ( tstvel*refvel>0.0 .and.                  &
                            abs(tstvel-refvel)<=10.0 ) then
                     tmp_forw(i,iray)=tstvel
                     indx_forw(i,iray)=-10
                     refvel= tstvel
                   elseif ( tstvel*refvel>0.0 .and.                  &
                            abs(tstvel-refvel)>10.0 ) then
                     IF ( tstvel>0.0 ) THEN
                       a2=tstvel-twonyq
                       if ( a2*refvel>0.0 ) then
                         tmp_forw(i,iray)=tstvel-twonyq
                         indx_forw(i,iray)=-10
                         refvel= tstvel-twonyq
                       endif
                     ELSE
                       a2=tstvel+twonyq
                       if ( a2*refvel>0.0 ) then
                         tmp_forw(i,iray)=tstvel+twonyq
                         indx_forw(i,iray)=-10
                         refvel= tstvel+twonyq
                       endif
                     ENDIF
                   endif
                 ENDIF
               else
                 IF ( a1>0.0 ) THEN
                   tmp_forw(i,iray)=tstvel-twonyq
                   indx_forw(i,iray)=-10
                   refvel= tstvel-twonyq
                 ELSE
                   tmp_forw(i,iray)=tstvel+twonyq
                   indx_forw(i,iray)=-10
                   refvel= tstvel+twonyq
                 ENDIF
               endif
             ENDIF
!            write(998,'(2i4,4f10.1)') iray,i,ovel(i,iray),a1          &
!                                     ,refvel,tmp_forw(i,iray)
            enddo
          endif
        ELSE
          refvel= tmp_forw(ipen+1,iray)
          do i=ipen,ipst,-1
           tstdev=twonyq*NINT((ovel(i,iray)-refvel)*inv2nyq)
           tstvel=ovel(i,iray)-tstdev
           if ( abs(tstvel-refvel)<thrpri ) then
             if ( tstvel*refvel>0.0 ) then
               tmp_forw(i,iray)=tstvel
               indx_forw(i,iray)=-10
               refvel= tstvel
             endif
           endif
          enddo
        ENDIF
       enddo
      ENDDO
!     name3='vadf'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='vadi'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)
!     ------------------------------------------------------------------
!     beam special area environment unfolding.
!     ------------------------------------------------------------------
      DO iray=1,nazim
        kp=kgoodnum(iray)
        if ( kp==0 ) cycle
        do k=1,kp
          ipst=kgoodbgn(k,iray)
          ipen=kgoodend(k,iray)
          do i=ipen,ipst,-1
            if ( indx_forw(i,iray)==50 ) then
!             3X3 window check
              a1=0.0
              a2=0.0
              do jj=-1,1
                j=iray+jj
                if ( j<0 ) j=nazim+j
                if ( j>nazim ) j=j-nazim
                do ii=-1,1
                  ip=i+ii
                  if ( indx_forw(ip,j)<=-10 ) then
                    a1=a1+tmp_forw(ip,j)
                    a2=a2+1.0
                  endif
                enddo
              enddo
              IF ( a2>1.0 ) THEN
                refvel=a1/a2
                if ( abs(ovel(i,iray)-refvel)<thrpri ) then
                  if ( ovel(i,iray)*refvel>0.0 ) then
                    tmp_forw(i,iray)=ovel(i,iray)
                    indx_forw(i,iray)=-10
                  endif
                else
                  tstdev=twonyq*NINT((ovel(i,iray)-refvel)*inv2nyq)
                  IF ( abs(tstdev)>1.0 ) THEN
                    tstvel=ovel(i,iray)-tstdev
                    if ( abs(tstvel-refvel )<thrpri ) then
                      IF ( abs(tstvel)<70.0 ) THEN
                        tmp_forw(i,iray)=tstvel
                        indx_forw(i,iray)=-10
                      ENDIF
                    endif
                  ENDIF
                endif
              ENDIF     ! endif a2>1.0
            endif    ! endif indx_forw(i,iray)==50
          enddo   ! enddo i
        enddo   ! enddo kp
      ENDDO

!     name3='vadf'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='vadi'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)

!     ------------------------------------------------------------------
!     eliminate the rough jump along the radial.
!     ------------------------------------------------------------------
      DO iray=1,nazim
      DO igate=n10km,ngate
       if ( indx_forw(igate,iray)<=-10 ) then
         IF ( abs(tmp_forw(igate,iray))>60.0 .and.                   &
              abs(tmp_forw(igate,iray))<spval ) THEN
           refvel=spval
           do ii=1,20
            i=igate-ii
            if ( i<1 ) cycle
            if ( indx_forw(i,iray)<=-10 ) then
              refvel=tmp_forw(i,iray)
              exit
            endif
           enddo
           IF ( abs(tmp_forw(igate,iray)-refvel)>thrpri ) THEN
             tstdev=twonyq*NINT((tmp_forw(igate,iray)-refvel)*inv2nyq)
             if ( abs(tstdev)>1.0 ) then
               tstvel=tmp_forw(igate,iray)-tstdev
               IF ( abs(tstvel-refvel )<thrpri ) THEN
                 tmp_forw(igate,iray)=tstvel
               ELSE
                 tmp_forw(igate,iray)=spval
                 indx_forw(igate,iray)=10
               ENDIF
             endif
           ENDIF
         ENDIF
       endif
      ENDDO
      ENDDO

!     name3='vadf'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='vadi'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)

!     ------------------------------------------------------------------
!     combine the check results.
!     ------------------------------------------------------------------
      rvel=spval
      unfvel=spval
      index=10
      special_points=0
      DO iray=1,nazim
      DO igate=1,ngate
        IF ( indx_forw(igate,iray)<=-10 ) THEN
          rvel(igate,iray)=tmp_forw(igate,iray)
          unfvel(igate,iray)=tmp_forw(igate,iray)
          index(igate,iray)=0
        ELSEIF ( indx_forw(igate,iray)==50 ) THEN
          index(igate,iray)=50
          special_points=special_points+1
          unfvel(igate,iray)=888.8
        ENDIF
      ENDDO
      ENDDO

      deallocate ( tmp_forw,indx_forw )
      deallocate ( tmp_back,indx_back )
      deallocate ( cyclone )
      deallocate ( kgoodnum,kgoodbgn,kgoodend )
 
      RETURN
      END SUBROUTINE TORNADO_forw
