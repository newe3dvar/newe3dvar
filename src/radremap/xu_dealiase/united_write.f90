      SUBROUTINE UNITED_WRITE

      use Xu_variables

      IMPLICIT NONE

      character(len=40) :: name1
      INTEGER    :: i,j,k,kt

!-----------------------------------------------------------------------
!     clean the unavailable points.
!-----------------------------------------------------------------------
      do k=1,nthet
        do j=1,nphi(k)
        do i=1,imaxrng(k)
          if ( abs(vel(i,j,k))>=100.0 ) then
            vel(i,j,k)=spval
          endif
        enddo
        enddo
      enddo

      j=0
      do k=1,nthet
        elvng=thet(k)
        i=nint(elvng*100.0 )
        if ( i<500 ) then
!       if ( i<200 ) then
          j=j+1
        endif
      enddo
      kt=j

      name1=radid(1:4)//'_united_obsv_scan.dat'
      open(31,file=name1,form='unformatted')
        write(31) nr,np,kt
        write(31) radid
        write(31) vcpnum
        write(31) iyr(0),imon(0),iday(0),ihr(0),imin(0),isec(0)
        write(31) radlat,radlon,radelv
        write(31) rfstgat,refgatsp
        write(31) (thet(k),k=1,kt)
        write(31) (nphi(k),k=1,kt)
        write(31) (imaxrng(k),k=1,kt)
        write(31) (thet_nyq_vel(k),k=1,kt)
        write(31) ((phi(j,k),j=1,nphi(k)),k=1,kt)
        write(31) (((vel(i,j,k),i=1,imaxrng(k)),j=1,nphi(k)),k=1,kt)
      close(31)

      END SUBROUTINE UNITED_WRITE
