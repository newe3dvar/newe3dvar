!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
! ======================================================================
!     PURPOSE:
!       Using 0Vr line and the tendance around the 0Vr line to unfold
!       the select circle data to get the wind profile.
! ======================================================================
!
!     Author   : Nai, Kang
!     Date     : Aug. 15, 2007
!     Action   : Created.
!
!     History  :
!    -----------
!     name     : Nai,Kang
!     date     : Oct.16, 2007
!     action   : Using line_regression to get the united 0Vr.
!
!     name     : Nai,Kang
!     date     : Feb.06, 2009
!     action   : Give a reliablity check for the united 0Vr.
!
!     name     : Nai,Kang
!     date     : Feb.09, 2009
!     action   : added a adjust cost-function check.
!                (sum(Zg(dltav,nyqv))/n)/(sum {ZG(dltav,nyqv)}**2)<0.01
!
!     name     : Nai,Kang
!     date     : Feb.09, 2009
!     action   : using one circle one 0Vr method to replace the old one.
!
!     name     : Nai, Kang
!     date     : Mar. 20,2009
!     action   : added the variation method to get the one circle's
!                reference Vr.
!
!     name     : Nai, Kang
!     date     : Mar. 20,2009
!     action   : read-in the singular value decomposition matrix
!
!     name     : Nai, Kang
!     date     : Feb. 28,2012
!     action   : introduce the half-circle cost-function method.
!
! ----------------------------------------------------------------------
      SUBROUTINE tendency_method_31

      use Xu_variables

      implicit none

      integer :: start_level,end_level
      integer :: k,jm,jn,klevel,jk
      integer :: ii,j,i1,i2,npath,n
      logical :: first_check,tmp_check
      real    :: beta_tmp,a1

      real,dimension(1:np) :: veltmp1,veltmp2,veltmp3
      real,dimension(1:np) :: veltmp4,veltmp5
      character(len=80)    :: name1,name2

! ######################################################################
!     Execute.
! ######################################################################
      print*,' '
      print*,'wind profile by tendence method'
      print*,' '

      start_level=int((hstart+1.0)/zstep)   ! the lowest level is 250 m
      end_level=zlevel                      ! the toppest height

!     ==================================================================
!     get the tilt's 0Vr azimuth which has a remarkable 0Vr.
!     ==================================================================
      do ilevel=start_level,end_level
        call gradient_fst_0Vr_31
      enddo

      print*,' VAD and combined VAD have been done, go to descent.'
      print*,' '

!     ==================================================================
!     combine the priority background to the new background.
!     ==================================================================
      do ilevel=start_level,end_level
        do k=1,nthet
          if ( vabs(k,ilevel)<spval ) then
            ii=select_circ(k,ilevel)
            if ( ii<0 ) then
              select_circ(k,ilevel)=-ii
            endif
            if ( ii==0 ) then
              vabs(k,ilevel)=spval
              U0ref(k,ilevel)=spval
              V0ref(k,ilevel)=spval
              betaref(k,ilevel)=spval
              c_fnct(k,ilevel)=spval
              do j=1,mspc
               comb_back(j,k,ilevel)=spval
              enddo
            endif
          endif
        enddo
      enddo

      first_check=.false.
      do ilevel=start_level,end_level
        do k_tilt=nthet,2,-1
          k=k_tilt
!         ==============================================================
!         get the available top level
!         ==============================================================
          if ( vabs(k,ilevel)<spval ) then
            first_check=.true.
            jm=ilevel
            exit
          endif
        enddo
        if ( first_check ) exit
      enddo

      if ( first_check ) then
!       ================================================================
!         Choice the nearest tilt's available Vref as the backgroud.
!       ================================================================
        do ilevel=jm,end_level
          call first_loan
        enddo

!       do ilevel=jm,end_level
!       do k=nthet,2,-1
!         npath=100+k
!         write(npath,'(4i6,4f10.3)') ilevel,k                       &
!                                    ,Acnjgt(k,ilevel)               &
!                                    ,Bcnjgt(k,ilevel)               &
!                                    ,vabs(k,ilevel)                 &
!                                    ,betaref(k,ilevel)              &
!                                    ,c_fnct(k,ilevel)               &
!                                    ,miss_gap(k,ilevel)
!       enddo
!       enddo
!       ================================================================
!       Vertical check the Vref.
!       ================================================================
        do k=nthet,2,-1
        do ilevel=jm+1,end_level
          if ( vabs(k,ilevel)<spval ) then
            if ( ilevel<=60 ) then              ! 1500 meters
              a1=spval
              do n=ilevel-1,jm,-1
                if ( vabs(k,n)<spval ) then
                  a1=vabs(k,n)
                  exit
                endif
              enddo
              if ( abs(a1)<spval ) then
!               if ( (ilevel-n)<=10 ) then     ! height diff<250 meters
                if ( (ilevel-n)<=16 ) then     ! height diff<400 meters
                  if ( vabs(k,ilevel)-a1<-6.0 ) then
                    vabs(k,ilevel)=spval
                    U0ref(k,ilevel)=spval
                    V0ref(k,ilevel)=spval
                    betaref(k,ilevel)=spval
                    c_fnct(k,ilevel)=spval
                    Acnjgt(k,ilevel)=-3
                  endif
                else
                  vabs(k,ilevel)=spval
                  U0ref(k,ilevel)=spval
                  V0ref(k,ilevel)=spval
                  betaref(k,ilevel)=spval
                  c_fnct(k,ilevel)=spval
                  Acnjgt(k,ilevel)=-3
                endif
              endif
            else
              jn=int((1500.0+1)/zstep)
              a1=-spval
              do n=ilevel-1,jn,-1
                if ( vabs(k,n)<spval ) then
                  a1=vabs(k,n)
                  exit
                endif
              enddo
              if ( vabs(k,ilevel)-a1<-6.0 ) then
                vabs(k,ilevel)=spval
                U0ref(k,ilevel)=spval
                V0ref(k,ilevel)=spval
                betaref(k,ilevel)=spval
                c_fnct(k,ilevel)=spval
                Acnjgt(k,ilevel)=-3
              endif
            endif
          endif
        enddo
        enddo

!       do k=nthet,2,-1
!         jn=int((1500.0+1.0)/zstep)
!         do ilevel=jn,end_level
!           if ( vabs(k,ilevel)<spval ) then
!             beta_tmp=-spval
!             tmp_check=.false.
!             do n=1,40                          ! 1000 meter
!               klevel=ilevel-n
!               if ( vabs(k,klevel)<spval ) then
!                 beta_tmp=betaref(k,klevel)
!                 tmp_check=.true.
!                 exit
!               endif
!             enddo
!             if ( .not. tmp_check ) then
!               do n=1,10                          ! 250 meter
!                 klevel=ilevel+n
!                 if ( vabs(k,klevel)<spval ) then
!                   beta_tmp=betaref(k,klevel)
!                   tmp_check=.true.
!                   exit
!                 endif
!               enddo
!             endif

!             if ( abs(beta_tmp-betaref(k,ilevel))>20.0 ) then
!             if ( abs(beta_tmp-betaref(k,ilevel))>30.0 ) then
!               vabs(k,ilevel)=spval
!               U0ref(k,ilevel)=spval
!               V0ref(k,ilevel)=spval
!               betaref(k,ilevel)=spval
!               c_fnct(k,ilevel)=spval
!               Acnjgt(k,ilevel)=-2
!             endif
!           endif
!         enddo
!       enddo

!       do ilevel=jm,end_level
!       do k=nthet,2,-1
!         npath=200+k
!         write(npath,'(4i6,4f10.3)') ilevel,k                       &
!                                    ,Acnjgt(k,ilevel)               &
!                                    ,Bcnjgt(k,ilevel)               &
!                                    ,vabs(k,ilevel)                 &
!                                    ,betaref(k,ilevel)              &
!                                    ,c_fnct(k,ilevel)               &
!                                    ,miss_gap(k,ilevel)
!       enddo
!       enddo
!       ================================================================
!       using variation method to get the vref.
!       ================================================================
        do ilevel=jm,end_level
          call first_conjugate_31
        enddo

!       do ilevel=jm,end_level
!       do k=nthet,2,-1
!         npath=300+k
!         write(npath,'(4i6,4f10.3)') ilevel,k                       &
!                                    ,Acnjgt(k,ilevel)               &
!                                    ,Bcnjgt(k,ilevel)               &
!                                    ,vabs(k,ilevel)                 &
!                                    ,betaref(k,ilevel)              &
!                                    ,c_fnct(k,ilevel)               &
!                                    ,miss_gap(k,ilevel)
!       enddo
!       enddo
        print*,' finashed the combine ref descent process, go to next.'
        print*,' '
!       ================================================================
!       choice the background Vref to suitable cycle.
!       ================================================================
        do ilevel=jm,end_level
          call second_loan
        enddo

!       do ilevel=jm,end_level
!       do k=nthet,2,-1
!         npath=400+k
!         write(npath,'(4i6,4f10.3)') ilevel,k                       &
!                                    ,Acnjgt(k,ilevel)               &
!                                    ,Bcnjgt(k,ilevel)               &
!                                    ,vabs(k,ilevel)                 &
!                                    ,betaref(k,ilevel)              &
!                                    ,c_fnct(k,ilevel)               &
!                                    ,miss_gap(k,ilevel)
!       enddo
!       enddo

!       ================================================================
!       Vertical check the Vref.
!       ================================================================
!       do k=nthet,2,-1
!       do ilevel=jm+1,end_level
!         if ( vabs(k,ilevel)<spval ) then
!           if ( ilevel<=60 ) then              ! 1500 meters
!             a1=spval
!             do n=ilevel-1,jm,-1
!               if ( vabs(k,n)<spval ) then
!                 a1=vabs(k,n)
!                 exit
!               endif
!             enddo
!             if ( abs(a1)<spval ) then
!               if ( (ilevel-n)<=10 ) then     ! height diff<250 meters
!               if ( (ilevel-n)<=16 ) then     ! height diff<400 meters
!                 if ( vabs(k,ilevel)-a1<-6.0 ) then
!                   vabs(k,ilevel)=spval
!                   U0ref(k,ilevel)=spval
!                   V0ref(k,ilevel)=spval
!                   betaref(k,ilevel)=spval
!                   c_fnct(k,ilevel)=spval
!                   Bcnjgt(k,ilevel)=-3
!                 endif
!               else
!                 vabs(k,ilevel)=spval
!                 U0ref(k,ilevel)=spval
!                 V0ref(k,ilevel)=spval
!                 betaref(k,ilevel)=spval
!                 c_fnct(k,ilevel)=spval
!                 Bcnjgt(k,ilevel)=-3
!               endif
!             endif
!           else
!             jn=int((1500.0+1)/zstep)
!             a1=spval
!             do n=ilevel-1,jn,-1
!               if ( vabs(k,n)<spval ) then
!                 a1=vabs(k,n)
!                 exit
!               endif
!             enddo
!             if ( abs(a1)<spval ) then
!               if ( vabs(k,ilevel)-a1<-6.0 ) then
!                 vabs(k,ilevel)=spval
!                 U0ref(k,ilevel)=spval
!                 V0ref(k,ilevel)=spval
!                 betaref(k,ilevel)=spval
!                 c_fnct(k,ilevel)=spval
!                 Bcnjgt(k,ilevel)=-3
!               endif
!             endif
!           endif
!         endif
!       enddo
!       enddo

!       do k=nthet,2,-1
!         jn=int((2500.0+1.0)/zstep)
!         do ilevel=jn,end_level
!           if ( vabs(k,ilevel)<spval ) then
!             beta_tmp=-spval
!             tmp_check=.false.
!             do n=1,40                          ! 1000 meter
!               klevel=ilevel-n
!               if ( vabs(k,klevel)<spval ) then
!                 beta_tmp=betaref(k,klevel)
!                 tmp_check=.true.
!                 exit
!               endif
!             enddo
!             if ( .not. tmp_check ) then
!               do n=1,10                          ! 250 meter
!                 klevel=ilevel+n
!                 if ( vabs(k,klevel)<spval ) then
!                   beta_tmp=betaref(k,klevel)
!                   tmp_check=.true.
!                   exit
!                 endif
!               enddo
!             endif

!             if ( abs(beta_tmp-betaref(k,ilevel))>20.0 ) then
!               vabs(k,ilevel)=spval
!               U0ref(k,ilevel)=spval
!               V0ref(k,ilevel)=spval
!               betaref(k,ilevel)=spval
!               c_fnct(k,ilevel)=spval
!               Bcnjgt(k,ilevel)=-2
!             endif
!           endif
!         enddo
!       enddo

!       do ilevel=jm,end_level
!       do k=nthet,2,-1
!         npath=500+k
!         write(npath,'(4i6,4f10.3)') ilevel,k                       &
!                                    ,Acnjgt(k,ilevel)               &
!                                    ,Bcnjgt(k,ilevel)               &
!                                    ,vabs(k,ilevel)                 &
!                                    ,betaref(k,ilevel)              &
!                                    ,c_fnct(k,ilevel)               &
!                                    ,miss_gap(k,ilevel)
!       enddo
!       enddo
!       ================================================================
!       using variation method to get the vref.
!       ================================================================
        do ilevel=jm,end_level
          call second_conjugate_31
        enddo

!       do ilevel=jm,end_level
!       do k=nthet,2,-1
!         npath=600+k
!         write(npath,'(4i6,4f10.3)') ilevel,k                       &
!                                    ,Acnjgt(k,ilevel)               &
!                                    ,Bcnjgt(k,ilevel)               &
!                                    ,vabs(k,ilevel)                 &
!                                    ,betaref(k,ilevel)              &
!                                    ,c_fnct(k,ilevel)               &
!                                    ,miss_gap(k,ilevel)
!       enddo
!       enddo
!       ================================================================
!       check again the refvel
!       ================================================================
        do ilevel=jm,end_level
        do k=nthet,2,-1
          if ( vabs(k,ilevel)>900.0 ) then
            nbeam = nphi(k)
            ii = select_circ(k,ilevel)
            IF (ii > 0) THEN
              do j=1,nbeam
                velref(ii,j,k)=spval
              enddo
              do j=1,mspc
                analy_back(j,k,ilevel)=spval
              enddo
            END IF
          endif
        enddo
        enddo

!       do ilevel=jm,end_level
!       do k=nthet,2,-1
!         npath=700+k
!         write(npath,'(4i6,4f10.3)') ilevel,k                       &
!                                    ,Acnjgt(k,ilevel)               &
!                                    ,Bcnjgt(k,ilevel)               &
!                                    ,vabs(k,ilevel)                 &
!                                    ,betaref(k,ilevel)              &
!                                    ,c_fnct(k,ilevel)               &
!                                    ,miss_gap(k,ilevel)
!       enddo
!       enddo

        print*,' go to 0.5 descent process.'
        print*,' '
!       ================================================================
!       using 1.5 tilt's vref as 0.5 tilt's vref.
!       ================================================================
        k_tilt=1
        do ilevel=jm,end_level
          call third_loan
        enddo

!       ================================================================
!       using variation method to get the 0.5 tilt's vref.
!       ================================================================
        do ilevel=jm,end_level
          call third_conjugate_31
        enddo

!       k=k_tilt
!       do ilevel=jm,end_level
!         npath=901
!         write(npath,'(4i6,4f10.3)') ilevel,k                       &
!                                    ,Acnjgt(k,ilevel)               &
!                                    ,Bcnjgt(k,ilevel)               &
!                                    ,vabs(k,ilevel)                 &
!                                    ,betaref(k,ilevel)              &
!                                    ,c_fnct(k,ilevel)               &
!                                    ,miss_gap(k,ilevel)
!       enddo

      endif

      do k=nthet,1,-1
      do ilevel=start_level,end_level
        if ( vabs(k,ilevel)<spval ) then
          count_select_circle(k)=count_select_circle(k)+1
        endif
      enddo
      enddo

      END SUBROUTINE tendency_method_31
