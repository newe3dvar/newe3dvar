!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!=======================================================================
!     PURPOSE:
!       Main_Driver program is used to drive dealiasing process by using
!       the one volumn of radar's radial velocity data.
!=======================================================================
!
!     Author   : Gong, Jiandong
!     Date     :
!     Action   : Created.
!
!     History  :
!     ----------
!     Mar. 27, 2007: Pengfei change it to a subroutine "Dealiase_Main"
!                    for QC package.
!
!     Sep. 25, 2007: Qin Xu and kang Nai introduced two step dialiasing
!                    agorithm to QC the Doppler radar radial velocity
!                    data. Which used the non_line Kapa function to get
!                    the reference field and do unfolding job.
!
!     Nov. 17, 2008: Kang Nai added a detecting code to monitor if the
!                    data needs to dealiasing.
!
!     Feb. 02, 2009: Kang Nai changed the z-step from 50m to 25m, which
!                    will fit the strong wind direction and speed shear
!                    in the vertical height.
!
!     Feb. 06, 2009: Kang Nai terminated the multi_check_drive process.
!
!     Feb. 17, 2009: Kang Nai using the one circle one 0Vr method to
!                    replaced the whole volume two 0Vr method. This
!                    can treat more cases then the old one.
!
!     Jun. 05, 2009: Kang Nai added using priority profile when can not
!                    get the VAD profile.
!
!     Feb. 28, 2012: Kang Nai introduced the half-circle robust-VAD.
!
!     Dec. 13, 2012: Kang Nai introduced the model profile check.
!
!     Jan. 09, 2013:  Kang Nai introduced the model circle background.
!
!-----------------------------------------------------------------------

SUBROUTINE Xu_Dealiase_main(radname,latrad,lonrad,elvrad,ivcp,          &
                            maxrgate,maxvgate,maxazim,maxelev,kntvelv,  &
                            rfrst_ref,rfrst_vel,gtspc_ref,gtspc_vel,    &
                            kntrgat,kntvgat,kntrazm,kntvazm,            &
                            azmrvol,azmvvol,elvmnrvol,elvmnvvol,        &
                            timeset, itimfrst,timevolv,                 &
                            vnyqvol,rngvvol,                            &
                            refvol,velvol,vadkdim,unfdiag,misval,istatus)

      USE XU_variables

      IMPLICIT NONE

      CHARACTER(LEN=4), INTENT(IN) :: radname
      REAL,    INTENT(IN)    :: latrad,lonrad,elvrad
      INTEGER, INTENT(IN)    :: ivcp
      INTEGER, INTENT(IN)    :: maxrgate,maxvgate,maxazim,maxelev
      INTEGER, INTENT(IN)    :: kntvelv

      INTEGER, INTENT(IN)    :: timeset, itimfrst

      INTEGER, INTENT(IN)    :: rfrst_ref, gtspc_ref, gtspc_vel
      INTEGER, INTENT(INOUT) :: rfrst_vel
      INTEGER, INTENT(IN)    :: kntrgat(maxazim,maxelev),kntvgat(maxazim,maxelev)
      INTEGER, INTENT(IN)    :: kntrazm(maxelev),        kntvazm(maxelev)
      REAL,    INTENT(IN)    :: azmrvol(maxazim,maxelev)
      REAL,    INTENT(INOUT) :: azmvvol(maxazim,maxelev)
      REAL,    INTENT(IN)    :: elvmnrvol(maxelev),  elvmnvvol(maxelev)
      INTEGER, INTENT(IN)    :: timevolv(maxazim,maxelev)
      REAL,    INTENT(IN)    :: vnyqvol(maxazim,maxelev)
      REAL,    INTENT(IN)    :: refvol(maxrgate,maxazim,maxelev)
      REAL,    INTENT(OUT)   :: rngvvol(maxvgate,maxelev)
      REAL,    INTENT(INOUT) :: velvol(maxvgate,maxazim,maxelev)

      INTEGER, INTENT(OUT)   :: vadkdim

      LOGICAL, INTENT(IN)    :: unfdiag

      REAL,    INTENT(IN)    :: misval

      INTEGER, INTENT(OUT)   :: istatus

!-----------------------------------------------------------------------
      integer :: i,j,k
      character(len=80) :: name1,name2,name3

   INTEGER :: iproj_org
   REAL    :: scale_org, latnt_org(2), trlon_org
   REAL    :: x0_org, y0_org

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

      istatus = 0

!-----------------------------------------------------------------------
!
!     Save original map projection
!
!-----------------------------------------------------------------------

      CALL getmapr(iproj_org,scale_org,latnt_org,trlon_org,x0_org,y0_org)

!-----------------------------------------------------------------------
!
!     Initialize package variables from RADREMAP variables
!
!-----------------------------------------------------------------------

      CALL xu_variable_constants(radname,latrad,lonrad,elvrad,ivcp,     &
                                 maxrgate,maxvgate,maxazim,maxelev,     &
                                 kntvelv,istatus)

      CALL xu_variable_init(rfrst_ref,rfrst_vel,gtspc_ref,gtspc_vel,    &
                            kntrgat,kntvgat, kntrazm,kntvazm,           &
                            azmrvol,azmvvol,elvmnrvol,elvmnvvol,        &
                            timeset, itimfrst,timevolv, vnyqvol,        &
                            refvol,velvol,istatus)

      outdiag = unfdiag    ! diagnostic output

      !IF(modelgrid > 0) THEN
      !  CALL xu_model_init(nx,ny,nz,x,y,ugrid,vgrid,zps,istatus)
      !ELSE
      !  bgrid_model = 0
      !END IF

!-----------------------------------------------------------------------
!
!     Original QC package code
!
!-----------------------------------------------------------------------

!     ******** put map parameters ********
      mapproj = 2
      sclfct = 1.0
      trulon = radlon
      i=(int((radlat+5.0)/10.0))*10
      trulat1 = i-15.0
      trulat2 = i+15.0
      !open(1,file='Lambert_projection.dat')
      !  write(1,'(i4)') mapproj
      !  write(1,'(f6.1)') sclfct
      !  write(1,'(f20.6)') trulon
      !  write(1,'(2f20.6)') trulat1,trulat2
      !close(1)

!-----------------------------------------------------------------------

      WRITE (*, '(/,10X,A,/)') '*** Dealiasing Starting ***'

      count_select_circle=0
      count_total_points=0

      allocate ( orgvel(1:nr,1:np,1:nthet) )
      allocate ( wrkhgt(1:nr,1:nthet) )
      allocate ( wrksfc(1:nr,1:nthet) )

      orgvel=spval
      ustor=spval
      vstor=spval

      vabs=spval
      U0ref=spval
      V0ref=spval
      betaref=spval
      c_fnct=spval
      Acnjgt=0
      Bcnjgt=0
      miss_gap=spval

      allocate ( mdlvel(1:np) )
      model_gap=spval

!-----------------------------------------------------------------------
!     [1.0] Set up original point and shift data if need.
!-----------------------------------------------------------------------
      CALL Setup_radar

!-----------------------------------------------------------------------
!     [2.0] Set up noise remove.
!-----------------------------------------------------------------------
      CALL Setup_noise_remove

!-----------------------------------------------------------------------
!     [3.0] set up the start tilt and the end tilt.
!-----------------------------------------------------------------------
      CALL setup_tilt

!-----------------------------------------------------------------------
!     [4.0] Select cycles at the standard height from the ground.
!-----------------------------------------------------------------------
      CALL setup_circle
      if ( no_qualify ) then
        WRITE (*, '(/,10X,A,/)') '*** Dealiasing no qualify ***'
        deallocate ( orgvel,wrkhgt,wrksfc )
        RETURN
      endif

!-----------------------------------------------------------------------
!     [5.0] Design the singular value decomposition array.
!-----------------------------------------------------------------------
!     make a structure matrix
      mspc=90           ! background points.
      itrun=9           ! truncate number.
      allocate ( phistn(1:mspc) )
      allocate ( ltri_matrix(1:mspc,1:mspc) )
      allocate ( utri_matrix(1:mspc,1:mspc) )

!-----------------------------------------------------------------------
!     [6.0] Readin the priority analysis circles data
!-----------------------------------------------------------------------
      allocate ( analy_back(1:mspc,1:nt,1:zlevel) )
      allocate ( comb_back(1:mspc,1:nt,1:zlevel) )
      analy_back=spval
      comb_back=spval

!-----------------------------------------------------------------------
!     [7.0] Do the unfolding process.
!-----------------------------------------------------------------------
      allocate ( velref(1:nr,1:np,1:nthet) )
      velref=spval

      if ( vcpnum==31 ) then
!       ----------------------------------------------------------------
!       [7.01] get the structure array.
!       ----------------------------------------------------------------
        CALL struct_covar( 1.0 )

!       ----------------------------------------------------------------
!       [7.02] get the selected circle's referent radial velocity.
!       ----------------------------------------------------------------
        CALL tendency_method_31

!       ----------------------------------------------------------------
!       [7.03] unfolding the aliased data.
!       ----------------------------------------------------------------
        CALL Multi_check_driver_31

!       ----------------------------------------------------------------
!       [7.04] get the VAD
!       ----------------------------------------------------------------
        CALL classical_VAD_31

!       ----------------------------------------------------------------
!       [7.05] clean the unavailable points.
!       ----------------------------------------------------------------
        do k=1,nthet
          do j=1,nphi(k)
          do i=1,imaxrng(k)
            if ( abs(vel(i,j,k))>=100.0 ) then
              vel(i,j,k)=spval
            endif
          enddo
          enddo
        enddo

!       ----------------------------------------------------------------
!       [7.06] output the count results.
!       ----------------------------------------------------------------
IF (outdiag) THEN
        write(name1,'(i4.4,5i2.2)') iyr(0),imon(0),iday(0)           &
                                   ,ihr(0),imin(0),isec(0)
        name2='count'//name1(1:14)//'.dat'
        open(41,file=name2)
        do k=1,nthet
         write(41,'(f8.3,3i20)') thet(k),(count_total_points(k,j),j=1,3)
        enddo
        close(41)
END IF

      else
!-----------------------------------------------------------------------
!       [7.11] get the structure array
!-----------------------------------------------------------------------
        CALL struct_covar( 0.5 )

!-----------------------------------------------------------------------
!       [7.12] get the selected circle's first guess radial velocity
!-----------------------------------------------------------------------
        CALL setup_wind_profile

!-----------------------------------------------------------------------
!       [7.13] get the priority wind profile.
!-----------------------------------------------------------------------
        CALL read_wind_profile

!-----------------------------------------------------------------------
!       [7.13] get the ivadflag.
!-----------------------------------------------------------------------
        call decide_vadflag

        if ( ivadflag<0 ) then

          no_qualify=.true.
          name1='There is no VAD, no action be made!!!'
          name3='NOVAD.dat'
          open(31,file=name3)
            write(31,'(a37)') name1
          close(31)
          vel=spval

        else

!-----------------------------------------------------------------------
!         [7.14] get the selected circle's referent radial velocity
!-----------------------------------------------------------------------
          CALL main_conjugate

!-----------------------------------------------------------------------
!         [7.15] do final VAD
!-----------------------------------------------------------------------
          CALL classical_VAD

        endif

!-----------------------------------------------------------------------
!       [7.17] clean the unavailable points.
!-----------------------------------------------------------------------
        do k=1,nthet
          do j=1,nphi(k)
          do i=1,imaxrng(k)
            if ( abs(vel(i,j,k))>=100.0 ) then
              vel(i,j,k)=spval
            endif
          enddo
          enddo
        enddo

!-----------------------------------------------------------------------
!       [7.18] output the count results.
!-----------------------------------------------------------------------
IF (outdiag) THEN
        write(name1,'(i4.4,5i2.2)') iyr(0),imon(0),iday(0)           &
                                   ,ihr(0),imin(0),isec(0)
        name2='count'//name1(1:14)//'.dat'
        open(41,file=name2)
        do k=1,nthet
         write(41,'(f8.3,4i20)') thet(k),(count_total_points(k,j),j=1,4)
        enddo
        close(41)

        name2='specl'//name1(1:14)//'.dat'
        open(41,file=name2)
        do k=1,nthet
         write(41,'(f10.2,4i20)') thet(k)                            &
                                 ,count_special_points(k,1)          &
                                 ,count_special_points(k,2)          &
                                 ,count_special_points(k,3)          &
                                 ,count_special_points(k,4)
        enddo
        close(41)
END IF
      endif

      deallocate ( orgvel,wrkhgt,wrksfc )
      deallocate ( phistn,ltri_matrix,utri_matrix )
      deallocate ( analy_back,comb_back )
      deallocate ( velref )
      deallocate ( mdlvel )

      WRITE (*, '(/,10X,A,/)') '*** Dealiasing Ending ***'

!-----------------------------------------------------------------------
!
!     Map QC package variables to RADREMAP variables
!
!-----------------------------------------------------------------------

     CALL xu_vel_output(rfrst_vel,rngvvol,azmvvol,velvol,misval,istatus)

     vadkdim = zlevel
     !IF (vadopt > 0) THEN
     !  CALL xu_output_vad(istatus)
     !END IF

!-----------------------------------------------------------------------
!
!     Restore original map projection
!
!-----------------------------------------------------------------------
      CALL setmapr(iproj_org,scale_org,latnt_org,trlon_org)
      CALL setorig(1,x0_org,y0_org)

      RETURN
END SUBROUTINE XU_Dealiase_Main

!#######################################################################

SUBROUTINE xu_dealiase_model_init(modelgrid,gridfile,                   &
                     latrad,lonrad,rngmin,rngmax,                       &
                     nx,ny,nz,x,y,u,v,zps,istatus)

  USE XU_variables

  IMPLICIT NONE

  INTEGER,            INTENT(IN)  :: modelgrid
  CHARACTER(LEN=256), INTENT(IN)  :: gridfile
  REAL,               INTENT(IN)  :: latrad,lonrad,rngmin,rngmax
  INTEGER,            INTENT(IN)  :: nx, ny, nz
  REAL,               INTENT(IN)  :: x(nx), y(ny)
  REAL,               INTENT(IN)  :: u(nx,ny,nz), v(nx,ny,nz)
  REAL,               INTENT(IN)  :: zps(nx,ny,nz)
  INTEGER,            INTENT(OUT) :: istatus
!-----------------------------------------------------------------------

  INTEGER :: nx_ext, ny_ext, nz_ext
  INTEGER :: ix1, ix2, jy1, jy2

  INTEGER :: iproj_grb,nx_grb,ny_grb,uvearth
  REAL    :: dx_grb,dy_grb,latsw,lonsw,lattru1,lattru2,lontrue

  !REAL,    PARAMETER :: gravity = 9.81

  INTEGER, PARAMETER :: n3dvs = 3
  INTEGER, PARAMETER :: GPH = 1        ! Geopotential Height
  INTEGER, PARAMETER :: UWD = 2        ! U-component of Wind
  INTEGER, PARAMETER :: VWD = 3        ! V-component of Wind

  INTEGER, PARAMETER :: maxvar = n3dvs

  INTEGER, PARAMETER :: varids(4,maxvar) = RESHAPE(  & ! order is IMPORTANT
                    ! Discipline  Category  Parameter Layer/Level_Type
                         (/   0,   3,   5, 100,    & !  1 GPH
                              0,   2,   2, 100,    & !  2 UWD
                              0,   2,   3, 100     & !  3 VWD
                          /), (/4,maxvar/) )

  REAL,   PARAMETER :: var3dlvl(40) = (/ 101320., 100000., 97500., 95000., 92500.,   &
              90000., 87500., 85000., 82500., 80000., 77500., 75000., 72500., 70000., 67500.,   &
              65000., 62500., 60000., 57500., 55000., 52500., 50000., 47500., 45000., 42500.,   &
              40000., 37500., 35000., 32500., 30000., 27500., 25000., 22500., 20000., 17500.,   &
              15000., 12500., 10000.,  7500.,  5000. /)

  INTEGER, PARAMETER :: var3dindx(maxvar) = (/ GPH, UWD, VWD /)

  REAL,    ALLOCATABLE :: var_grb3d(:,:,:,:)

!-----------------------------------------------------------------------

  INTEGER :: lvldbg

  INTEGER :: i,j,k

  INTEGER :: iproj_org
  REAL    :: scale_org, latnt_org(2), trlon_org
  REAL    :: x0_org, y0_org
  REAL    :: x1, y1, x2, y2, xrad, yrad

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  bgrid_model = 2

  IF (modelgrid == 1) THEN

    CALL getgrb2dims(gridfile,nx_grb,ny_grb,dx_grb,dy_grb,              &
                     iproj_grb,lattru1,lattru2,lontrue,latsw,lonsw,     &
                     istatus)
    nz_ext = 40

!-----------------------------------------------------------------------
!
!     Save original map projection
!
!-----------------------------------------------------------------------

    CALL getmapr(iproj_org,scale_org,latnt_org,trlon_org,x0_org,y0_org)


    iproj_mdl = iproj_grb
    scale_mdl = 1.0
    latnt_mdl(1) = lattru1
    latnt_mdl(2) = lattru2
    trlon_mdl = lontrue

    CALL setmapr(iproj_mdl,scale_mdl,latnt_mdl,trlon_mdl)
    CALL lltoxy(1,1,latsw,lonsw,x1,y1)
    x0_mdl    = x1 + dx_grb
    y0_mdl    = y1 + dy_grb
    CALL setorig(1,x0_mdl,y0_mdl)

    CALL lltoxy(1,1,latrad,lonrad,xrad,yrad)
    x1 = xrad-rngmax - dx_grb
    y1 = yrad-rngmax - dy_grb
    x2 = xrad+rngmax + dx_grb
    y2 = yrad+rngmax + dy_grb
    ix1 = x1/dx_grb + 2
    ix2 = x2/dx_grb + 2
    jy1 = y1/dy_grb + 2
    jy2 = y2/dy_grb + 2
    WRITE(*,'(1x,2(a,I0),2(a,F8.2),a)') 'Grib file size     : nx_grb = ',nx_grb, &
      ', ny_grb = ',ny_grb,', dx_grb = ',dx_grb,', dy_grb = ',dy_grb,'.'
    WRITE(*,'(1x,a,I0,3(a,F8.2),/,37x,2(a,F8.2),a)')    'Grib map projection: iproj_grb = ',iproj_grb, &
                  ', lattru1 = ',lattru1,                               &
                  ', lattru2 = ',lattru2,', lontrue = ',lontrue,        &
                  '  latsw = ',latsw,',   lonsw = ',lonsw,'.'
    WRITE(*,'(1x,3(a,F8.2),a)') 'Radar location     : radlat = ',latrad,', radlon = ',lonrad,', rngmax = ',rngmax/1000.,' km.'
    WRITE(*,'(1x,4(a,I0),a)')   'Reading section    : ix1 - ix2 = ',ix1, ' -- ',ix2,'; jy1 - jy2 = ',jy1,' -- ',jy2,'.'
    !
    ! Determine grib file sub-domain to be read
    !
    nx_ext = ix2-ix1+1
    ny_ext = jy2-jy1+1
    WRITE(*,'(1x,2(a,I0),a)')  'Reading size       : nx_ext = ',nx_ext, ', ny_ext = ',ny_ext,'.'

    !IF (iboxe > iboxs .AND. jboxe > jboxs) THEN
    !  ix1 = iboxs
    !  ix2 = iboxe
    !  jy1 = jboxs
    !  jy2 = jboxe
    !ELSE
    !  ix1 = 1
    !  ix2 = nx_ext
    !  jy1 = 1
    !  jy2 = ny_ext
    !END IF

    ALLOCATE(var_grb3d(nx_ext,ny_ext,nz_ext,n3dvs), STAT = istatus)
    var_grb3d = 0.0

    lvldbg = 0

    CALL rdgrb2bkg(nx_ext,ny_ext,nz_ext,gridfile,                       &
           ix1,ix2,jy1,jy2,                                             &
           iproj_grb,nx_grb,ny_grb,dx_grb,dy_grb,                       &
           latsw,lonsw,lattru1,lattru2,lontrue,uvearth,                 &
           n3dvs, maxvar, varids, var3dindx, var3dlvl, var_grb3d,       &
           lvldbg, istatus)


    nlon_mdl = nx_ext
    nlat_mdl = ny_ext
    z_model  = nz_ext

    ALLOCATE(x_mdl(nlon_mdl), STAT = istatus)
    ALLOCATE(y_mdl(nlat_mdl), STAT = istatus)
    !
    ! Relocate latsw/lonsw for the sub-domain
    !
    !CALL lltoxy(1,1,latsw,lonsw,x1,y1)
    x0_mdl    = x1 + dx_grb
    y0_mdl    = y1 + dy_grb
    CALL setorig(1,x0_mdl,y0_mdl)

    DO i = 1, nlon_mdl
      x_mdl(i) = (i-2)* dx_grb
    END DO

    DO j = 1, nlat_mdl
      y_mdl(j) = (j-2)* dy_grb
    END DO

    ALLOCATE(ubgrid(nlon_mdl,nlat_mdl,z_model), STAT = istatus)
    ALLOCATE(vbgrid(nlon_mdl,nlat_mdl,z_model), STAT = istatus)
    ALLOCATE(zbgrid(nlon_mdl,nlat_mdl,z_model), STAT = istatus)

    DO k = 1, z_model
      DO j = 1, nlat_mdl
        DO i = 1, nlon_mdl
          ubgrid(i,j,k) = var_grb3d(i,j,k,UWD)
          vbgrid(i,j,k) = var_grb3d(i,j,k,VWD)
          zbgrid(i,j,k) = var_grb3d(i,j,k,GPH)
        END DO
      END DO
    END DO

    DO k = 1, z_model
      WRITE(*,*) k, zbgrid(2,2,k),ubgrid(2,2,k),vbgrid(2,2,k)
    END DO

!-----------------------------------------------------------------------
!
!     Restore original map projection
!
!-----------------------------------------------------------------------
    CALL setmapr(iproj_org,scale_org,latnt_org,trlon_org)
    CALL setorig(1,x0_org,y0_org)

    DEALLOCATE( var_grb3d )

  ELSE IF (modelgrid == 2) THEN
    nlon_mdl = nx
    nlat_mdl = ny
    z_model  = nz

    ALLOCATE(x_mdl(nx), STAT = istatus)
    ALLOCATE(y_mdl(ny), STAT = istatus)

    x_mdl(:) = x(:)
    y_mdl(:) = y(:)

    ALLOCATE(ubgrid(nx,ny,nz), STAT = istatus)
    ALLOCATE(vbgrid(nx,ny,nz), STAT = istatus)
    ALLOCATE(zbgrid(nx,ny,nz), STAT = istatus)

    ubgrid(:,:,:) = u(:,:,:)
    vbgrid(:,:,:) = v(:,:,:)
    zbgrid(:,:,:) = zps(:,:,:)

    CALL getmapr(iproj_mdl,scale_mdl,latnt_mdl,trlon_mdl,x0_mdl,y0_mdl)
  ELSE
    WRITE(*,'(1x,a,I0)') 'ERROR: unsupported modelgrid option: ',modelgrid
    istatus = -1
    RETURN
  END IF

  RETURN
END SUBROUTINE xu_dealiase_model_init

!#######################################################################

SUBROUTINE Xu_dealiase_vadout(vadkdim,vadhgt,vaddir,vadspd,misval,istatus)

  USE XU_variables

  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: vadkdim

  REAL,    INTENT(OUT) :: vadhgt(vadkdim)
  REAL,    INTENT(OUT) :: vaddir(vadkdim)
  REAL,    INTENT(OUT) :: vadspd(vadkdim)
  REAL,    INTENT(IN)  :: misval
  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INTEGER :: k
  REAL    :: mischk

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  DO k = 1, zlevel
    IF (ABS(ustor(k)) > 900) ustor(k) = misval
    IF (ABS(vstor(k)) > 900) vstor(k) = misval
    vadhgt(k) = hstor(k)
  END DO

  mischk = misval + 9000
  CALL uvrotdd1d(vadkdim,ustor,vstor,vaddir,vadspd,                     &
                 misval, mischk,istatus)

  RETURN
END SUBROUTINE Xu_dealiase_vadout
