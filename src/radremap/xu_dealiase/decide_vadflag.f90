!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!     ==================================================================
!     PURPOSE:
!       get the unfolding flag.
!     ==================================================================
!
!     Author :  Jiandong Gong
!     Date   :
!     Action :  Created.
!
!     History:
!     --------
!     ------------------------------------------------------------------
      SUBROUTINE DECIDE_VADFLAG

       use Xu_variables

       implicit none

       integer :: i,j
       character(len=50) :: name1

       ivadflag=-10
       do ilevel = 1,zlevel
         if( abs(ustor(ilevel))<spval ) then
           ivadflag=10
           exit
         endif
       enddo
       print*,' '
       if (ivadflag==-10) then
         print*,'VADflag==',ivadflag,' There is no VAD, no acting of dealising'
       elseif (ivadflag==10) then
         print*,'VADflag==',ivadflag,' dealiasing method will be SIMPLE_METHOD'
       endif
       print*,' '

       END SUBROUTINE DECIDE_VADFLAG
