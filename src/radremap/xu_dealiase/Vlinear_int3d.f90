!)
!##################################################################
!##################################################################
!######                                                      ######
!######       The Tri_linear Interpolation subroutine        ######
!######                                                      ######
!##################################################################
!##################################################################
!
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Linear Interpolation in 3D.                         
!
!-----------------------------------------------------------------------
!
SUBROUTINE  linearint_3d(nx,ny,nz,vbl3,pgx,pgy,pgz,ivar,                &
           px,py,pz,pval,iflag,range,elvng )
! 
! 
  INTEGER :: nx, ny, nz
  REAL :: vbl3, pgx, pgy, pgz
  DIMENSION vbl3(nx,ny,nz)
  DIMENSION pgx(nx)
  DIMENSION pgy(ny)
  DIMENSION pgz(nx,ny,nz)
!
  REAL :: px,  py,  pz
  REAL :: pval
  REAL :: spval1,spval2
  real :: range,elvng
!
  iflag=1
  spval1 = 99999.0
  spval2 = 999.0
!
  CALL map_to_mod2( nx,ny,pgx,pgy,px,py,pxx,pyy )
!
  IF( (pxx <= 0.).OR.(pyy <= 0.) ) THEN
    iflag = 0
    pval  = spval2
!   print*,'out of model horizontal domain, not interpolate!'
!   print*,'px=',px,' py=',py,pval
    RETURN
  END IF
!c
!c
  CALL linearint_2df (nx,ny,pgz(1,1,nz),pxx,pyy,pzz,spval1)
  IF( pz > pzz ) THEN
!      print*,'out of model top, not interpolate!'
!      print*,'pz==',pz,'pzz==',pzz
    iflag = 0
    pval  = spval2
    RETURN
  END IF
!c
!c
  CALL  linearint_3df(nx,ny,nz,vbl3,pgz(1,1,1),ivar,                    &
                                    pxx,pyy,pz,pval,spval1,spval2)
!
  RETURN
END SUBROUTINE  linearint_3d
!
!##################################################################
!##################################################################
!######                                                      ######
!######       The Tri_linear Interpolation subroutine        ######
!######                                                      ######
!##################################################################
!##################################################################
!
!
!

SUBROUTINE linearint_3df(nx,ny,nz,vbl3,pgz,ivar,                &
           pxx,pyy,pz,pval,spval1,spval2)
!
  INTEGER :: nx, ny, nz
  REAL :: vbl3, pgz
  DIMENSION vbl3(nx,ny,nz)
  DIMENSION  pgz(nx,ny,nz)
  REAL :: pxx, pyy
  REAL :: pval
  REAL :: spval1,spval2
!
  ik = 0
!
  DO k = 1, nz-1
    CALL linearint_2df(nx,ny,pgz(1,1,k  ),pxx,pyy,z1,spval1)
    CALL linearint_2df(nx,ny,pgz(1,1,k+1),pxx,pyy,z2,spval1)
!
    IF((pz > z1) .AND. (pz <= z2)) THEN
      ik = k
      EXIT
    END IF
  END DO
!
  IF(ik > 0) THEN
!
!
    zdz2 =  (pz - z1)/(z2  - z1)
    zdz1 = -(pz - z2)/(z2  - z1)
!
    CALL linearint_2df(nx,ny,vbl3(1,1,ik  ),pxx,pyy,zv1,spval1)
    CALL linearint_2df(nx,ny,vbl3(1,1,ik+1),pxx,pyy,zv2,spval1)
!
!
! knai added
    if ( abs(zv1)<spval1 .and. abs(zv2)<spval1 ) then
      pval = zdz1 * zv1 + zdz2 * zv2
    else
      pval = spval1
    endif
!
  ELSE

    CALL linearint_2df(nx,ny,vbl3(1,1,ik+1),pxx,pyy,zv2,spval1)

    IF( (ivar == 1).OR.(ivar == 2).OR.(ivar == 5)                       &
                                    .OR.(ivar == 6) ) THEN
      pval = zv2
    ELSE IF( ivar == 4 ) THEN
      pval = zv2 + 0.0068*(z2  - z1)
    ELSE IF(ivar == 3 ) THEN
      pval = zv2 + 1.145E-4*(z2  - z1)
    ELSE
      PRINT*,' stop, the analysis variable is not exist!'
      STOP
    END IF
  END IF

!
  RETURN
END SUBROUTINE linearint_3df
!
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######       The Tri_linear Interpolation subroutine        ######
!######                                                      ######
!##################################################################
!##################################################################
!
!

SUBROUTINE alinearint_3d(nx,ny,nz,vbl3,pgx,pgy,pgz,ivar,                &
           px,py,pz,pval,iflag)
!c
!c
  INTEGER :: nx, ny, nz
  REAL :: vbl3, pgx, pgy, pgz
  DIMENSION vbl3 (nx,ny,nz)
  DIMENSION pgx (nx)
  DIMENSION pgy (ny)
  DIMENSION pgz (nx,ny,nz)
  REAL :: px,  py,  pz
  REAL :: pxx, pyy
  REAL :: pval
  REAL :: spval
!
  iflag=1
  spval=999.0
!
!
  CALL map_to_mod2(nx,ny,pgx(1),pgy(1),px,py,pxx,pyy)
!
!
  IF ((pxx <= 0.) .OR. (pyy <= 0.)) THEN
 
     print*,'out of model horizontal domain, not interpolate!' &
       , 'px=',px,' py=',py

    iflag = 0
    pval  = spval
    RETURN
!
  END IF
!
  CALL linearint_2df (nx,ny,pgz(1,1,nz),pxx,pyy,pzz)
!
!
  IF ( pz > pzz )  THEN
    print*,'out of model top, not interpolate!'
    iflag = 0
    pval  = spval
    RETURN
  END IF
!
  CALL alinearint_3df(nx,ny,nz,vbl3,pgz(1,1,1),ivar,                    &
                                    pxx,pyy,pz,pval)
!
!
  RETURN
END SUBROUTINE alinearint_3d
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######       The Tri_linear Interpolation subroutine        ######
!######                                                      ######
!##################################################################
!##################################################################
!
!

SUBROUTINE alinearint_3df(nx, ny, nz, vbl3, pgz, ivar,                  &
           pxx, pyy, pz, pval)
!
  INTEGER :: nx, ny, nz
  REAL :: vbl3, pgz
  DIMENSION vbl3(nx,ny,nz)
  DIMENSION  pgz(nx,ny,nz)
  REAL :: pxx, pyy
  REAL :: pval
!
!
  ik = 0
!
!
  DO k = 1, nz - 1
    CALL linearint_2df(nx,ny,pgz(1,1,k  ),pxx,pyy,z1)
    CALL linearint_2df(nx,ny,pgz(1,1,k+1),pxx,pyy,z2)
!
    IF( (pz > z1).AND.(pz <= z2) ) THEN
      ik = k
      EXIT
    END IF
!
  END DO
!
!
  IF(ik > 0) THEN
!
    zdz2 = (pz - z1)/(z2 - z1)
    zdz1 =-(pz - z2)/(z2 - z1)
!
!
    zv1 = 0.
    zv2 = 0.
!
    zv2 = zv2 + zdz2 * pval
    zv1 = zv1 + zdz1 * pval
    pval = 0.
!
    CALL alinearint_2df(nx,ny,vbl3(1,1,ik  ),pxx,pyy,zv1)
!
    CALL alinearint_2df(nx,ny,vbl3(1,1,ik+1),pxx,pyy,zv2)
!
  ELSE

    IF( (ivar == 1).OR.(ivar == 2).OR.(ivar == 5)                       &
                                    .OR.(ivar == 6) ) THEN
      zv2 = 0.0
      zv2 = zv2 + pval
      pval = 0.0

    ELSE IF( ivar == 4 ) THEN
!c          pval = zv2 + 0.0068*(z2  - z1)
      zv2 = 0.0
      zv2 = zv2 + pval
      pval=0.0

    ELSE IF(ivar == 3 ) THEN
!c            pval = zv2 + 1.145e-4*(z2  - z1)
      zv2 = 0.0
      zv2 = zv2 + pval
      pval=0.0

    ELSE
      PRINT*,' stop, the analysis variable is not exist!'
      STOP
    END IF

    CALL alinearint_2df(nx,ny,vbl3(1,1,ik+1),pxx,pyy,zv2)

  END IF

  RETURN
END SUBROUTINE alinearint_3df
