!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
! ======================================================================
!     PURPOSE:
!       Unfold and Quality Control Doppler radial velocities.
! ======================================================================
!
!     Author   : Nai, Kang
!     Date     : Sep. 20, 2007
!     Action   : Created.( used some code copyed from Gong's code)
!
!     History  :
!     ----------
!     name      : Nai, Kang
!     date      : Nov.26, 2007
!     action    : Added final noise remove process.
!
!     name      : Nai, Kang
!     date      : Jun. 9, 2008
!     action    : Modified the check process.
!
!-----------------------------------------------------------------------
!     INPUT :
!
!       nr        Maximum number of gates in a radial
!       np        Maximum number of radials in a tilt
!       ibgn      first available gate index
!       iend      last available gates index
!       nbeam     Number of radials
!       rvel      Doppler radial velocity
!       ovel      Doppler radial velocity
!       vnyq      Nyquist velocity
!       enangle   tilt
! 
!     WORK ARRAYS:
!     
!       tmp_forw
!       indx_forw     
!       tmp_back
!       indx_back
!
!-----------------------------------------------------------------------
      SUBROUTINE unfold_simple_31( nr,np,ibgn,iend,nbeam,vnyq        &
                                  ,index,rvel,ovel,vad,unfvel        &
                                  ,ivadflag,enangle,tmp3,spval,k,ran )

      IMPLICIT NONE

      INTEGER, INTENT(IN)  :: nr
      INTEGER, INTENT(IN)  :: np
      INTEGER, INTENT(IN)  :: ibgn
      INTEGER, INTENT(IN)  :: iend
      INTEGER, INTENT(IN)  :: nbeam
      REAL,    INTENT(IN)  :: vnyq

      INTEGER, INTENT(IN)  :: index(nr,np)
      REAL,    INTENT(IN)  :: rvel(nr,np)
      REAL,    INTENT(IN)  :: ovel(nr,np)
      REAL,    INTENT(IN)  :: vad(nr,np)
      REAL,    INTENT(OUT) :: unfvel(nr,np)

      INTEGER, INTENT(IN)  :: ivadflag
      REAL,    INTENT(IN)  :: enangle
      REAL,    INTENT(IN)  :: tmp3(nr)
      REAL,    INTENT(IN)  :: spval
      INTEGER, INTENT(IN)  :: k
      REAL,    INTENT(IN)  :: ran(nr)

      INTEGER :: igate,iray
      REAL    :: twonyq,inv2nyq,thrpri
      REAL    :: sum,sum2,vavg,tstvel,sum1
      REAL    :: refvel,tstdev,thresh
      integer :: i,ii,j,jj,ip,ik
      integer :: step_step(11)
      logical :: back_check,fwrd_check,near_check
      real    :: a1,a2,a3,a4,a5
      real,allocatable,dimension(:,:) :: tmp_forw,tmp_back
      real,allocatable,dimension(:,:) :: tmp1,tmp2
      integer,allocatable,dimension(:,:) :: indx_forw,indx_back
      real    :: near_thp,back_thp,shield_bottom,limit_number
      real    :: range,hgtrad,sfcrng
      character(len=80) :: name1,name2

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!     Beginning of executable code...
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      twonyq = 2.0*vnyq
      inv2nyq = 1./twonyq
      thrpri = 0.5*vnyq

      near_thp=vnyq/3.0
      near_thp=min(6.0,near_thp)
      back_thp=vnyq/4.0
      back_thp=min(5.0,back_thp)
      shield_bottom=2.6
      step_step(1)=0;step_step(2)=-1;step_step(3)=1
      step_step(4)=-2;step_step(5)=2
      step_step(6)=-3;step_step(7)=3
      step_step(8)=-4;step_step(9)=4
      step_step(10)=-5;step_step(11)=5

      ii=int( enangle*100.0 )
      write(name1,'(i4.4)') ii

      allocate( tmp_forw(nr,np) )
      allocate( tmp_back(nr,np) )
      allocate( indx_forw(nr,np) )
      allocate( indx_back(nr,np) )
!     allocate( tmp1(nr,np) )
!     allocate( tmp2(nr,np) )
!-----------------------------------------------------------------------
!     Set up tmp_forward and tmp_back to be quality arrays. 
!     set up indx_forward, indx_back; -20=good 10=bad/missing.
!-----------------------------------------------------------------------
      tmp_forw=spval
      tmp_back=spval
      DO iray=1,nbeam
      DO igate=ibgn,iend
        if ( index(igate,iray)==100 ) then
          indx_forw(igate,iray)=100
          indx_back(igate,iray)=100
        elseif ( index(igate,iray)==0 ) then
          if (abs(rvel(igate,iray))<spval ) then
            tmp_forw(igate,iray)=rvel(igate,iray)
            indx_forw(igate,iray)=-20
            tmp_back(igate,iray)=rvel(igate,iray)
            indx_back(igate,iray)=-20
          endif
        elseif ( index(igate,iray)==10 ) then
          if (abs(rvel(igate,iray))<spval ) then
            indx_forw(igate,iray)=10
            indx_back(igate,iray)=10
          endif
        endif
      END DO
      END DO
 
!-----------------------------------------------------------------------
!     outside to inside, clockwise check.
!-----------------------------------------------------------------------
      do igate=iend,ibgn,-1
      do iray=1,nbeam               ! clockwise
        if ( abs(ovel(igate,iray))<spval .and.                       &
             indx_forw(igate,iray)==10 ) then
!         --------------------------------------------------------------
!         get the area mean.
!         --------------------------------------------------------------
          a1=0.0
          a2=0.0
          do i=igate-20,igate+20
            if ( i>iend ) cycle
            if ( i<10 ) cycle
            do jj=1,11
              j=iray+step_step(jj)
              if ( j<=0 ) j=nbeam+j
              if ( j>nbeam ) j=j-nbeam
              if ( abs(tmp_forw(i,j))<spval .and. indx_forw(i,j)<0   &
                   .and. abs(tmp3(igate)-tmp3(i))<500.0 ) then
                a1=a1+tmp_forw(i,j)
                a2=a2+1.0
              endif
            enddo
          enddo

          limit_number=40.0
          if ( igate<80 ) limit_number=20.0

!         --------------------------------------------------------------
!         using the mean as a reference to unfold the raw points and
!         put it as seeded points.
!         --------------------------------------------------------------
          IF ( a2<limit_number ) THEN
            tmp_forw(igate,iray)=spval
          ELSE
            refvel=a1/a2
            if ( abs(ovel(igate,iray)-refvel) < thrpri ) then
              tmp_forw(igate,iray)=ovel(igate,iray)
              indx_forw(igate,iray)=-20
            else
              tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
              tstvel=ovel(igate,iray)-tstdev
              IF ( abs(tstvel-refvel) < thrpri ) THEN
                tmp_forw(igate,iray)=tstvel
                indx_forw(igate,iray)=-20
              ENDIF
            endif

            if ( abs(refvel)>near_thp .and.                          &
                 abs(tmp_forw(igate,iray))<=shield_bottom ) then
              tmp_forw(igate,iray)=spval
              indx_forw(igate,iray)=10
            endif

            if ( abs(refvel)>near_thp .and.                          &
                 (refvel*tmp_forw(igate,iray))<=0.0 ) then
              tmp_forw(igate,iray)=spval
              indx_forw(igate,iray)=10
            endif
          ENDIF    ! ENDIF a2<limit_number
        endif    ! endif index=10
      enddo
      enddo
!-----------------------------------------------------------------------
!     inside to outside, clockwise check.
!-----------------------------------------------------------------------
      do igate=ibgn,iend
      do iray=1,nbeam
        if ( abs(ovel(igate,iray))<spval .and.                       &
             indx_back(igate,iray)==10 ) then
!         --------------------------------------------------------------
!         get the area mean.
!         --------------------------------------------------------------
          a1=0.0
          a2=0.0
          do i=igate-20,igate+20
            if ( i>iend ) cycle
            if ( i<10 ) cycle
            do jj=1,11
              j=iray+step_step(jj)
              if ( j<=0 ) j=nbeam+j
              if ( j>nbeam ) j=j-nbeam
              if ( abs(tmp_back(i,j))<spval .and. indx_back(i,j)<0   &
                   .and. abs(tmp3(igate)-tmp3(i))<500.0 ) then
                a1=a1+tmp_back(i,j)
                a2=a2+1.0
              endif
            enddo
          enddo

          limit_number=40.0
          if ( igate<80 ) limit_number=20.0

!         --------------------------------------------------------------
!         using the mean as reference to unfolding the raw data.
!         --------------------------------------------------------------
          IF ( a2<limit_number ) THEN
            tmp_back(igate,iray)=spval
          ELSE
            refvel=a1/a2
            if ( abs(ovel(igate,iray) - refvel) < thrpri ) then
              tmp_back(igate,iray)=ovel(igate,iray)
              indx_back(igate,iray)=-20
            else
              tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
              tstvel=ovel(igate,iray)-tstdev
              IF ( abs(tstvel-refvel ) < thrpri ) THEN
                tmp_back(igate,iray)=tstvel
                indx_back(igate,iray)=-20
              ENDIF
            endif

            if ( abs(refvel)>near_thp .and.                          &
                 abs(tmp_back(igate,iray))<=shield_bottom ) then
              tmp_back(igate,iray)=spval
              indx_back(igate,iray)=10
            endif

            if ( abs(refvel)>near_thp .and.                          &
                 (refvel*tmp_back(igate,iray))<=0.0 ) then
              tmp_back(igate,iray)=spval
              indx_back(igate,iray)=10
            endif
          ENDIF    ! ENDIF a2<limit_number
        endif    ! endif index=10
      enddo
      enddo

!     ------------------------------------------------------------------
!     combine the check results.
!     ------------------------------------------------------------------
      unfvel=spval
      DO iray=1,nbeam
      DO igate=ibgn,iend
        IF ( indx_forw(igate,iray)==-20 ) then
          if ( indx_back(igate,iray)==-20 ) then
            a1=tmp_forw(igate,iray)-tmp_back(igate,iray)
            if ( abs(a1)<0.1 ) then
              unfvel(igate,iray)=tmp_forw(igate,iray)
            endif
          endif
        ENDIF
      ENDDO
      ENDDO

      deallocate ( tmp_forw,tmp_back,indx_forw,indx_back )
!     deallocate ( tmp1,tmp2 )
 
      RETURN
      END SUBROUTINE unfold_simple_31
