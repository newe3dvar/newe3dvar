!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!     ==================================================================
!     PURPOSE:
!       Check the cyclone shear area and to unfold the data if needed
!       and started backword from outside to radar.
!     ==================================================================
!
!     Author   : Nai, Kang
!     Date     : Jun. 16, 2011
!     Action   : Created.
!
!     History  :
!     ----------
!
!     ------------------------------------------------------------------
!
!     INPUT :
!
!       maxgate  Maximum number of gates in a radial
!       maxazim  Maximum number of radials in a tilt
!       ngate    Number of gates in radial
!       nazim    Number of radials
!       rvel     Doppler radial velocity
!       ovel     Doppler radial velocity
!       vnyq     Nyquist velocity
!       enangle  tilt
!
!     WORK ARRAYS:
!
!       tmp_forw
!       indx_forw
!       tmp_back
!       indx_back
!
!     ------------------------------------------------------------------
      SUBROUTINE BACK_cyclone_unfold( maxgate,maxazim,ngate,nazim,n10km&
                                     ,ovel,vnyq,enangle,vcpnum,ran,phi &
                                     ,rvel,index,unfvel                &
                                     ,tmp3,spval                       &
                                     ,iyr,imon,iday                    &
                                     ,ihr,imin,isec                    &
                                     ,special_points )

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: maxgate
      INTEGER, INTENT(IN) :: maxazim
      INTEGER, INTENT(IN) :: ngate
      INTEGER, INTENT(IN) :: nazim
!     INTEGER, INTENT(IN) :: n10km
      INTEGER             :: n10km

      REAL, INTENT(IN)    :: ovel(maxgate,maxazim)
      REAL, INTENT(IN)    :: vnyq
      REAL, INTENT(IN)    :: enangle
      INTEGER, INTENT(IN) :: vcpnum
      REAL, INTENT(IN)    :: ran(maxgate)
      REAL, INTENT(IN)    :: phi(maxazim)

      REAL                :: rvel(maxgate,maxazim)
      INTEGER             :: index(maxgate,maxazim)
      REAL, INTENT(OUT)   :: unfvel(maxgate,maxazim)

      REAL, INTENT(IN)    :: tmp3(maxgate)
      REAL, INTENT(IN)    :: spval

      INTEGER :: iyr,imon,iday,ihr,imin,isec
      INTEGER :: special_points

      INTEGER :: igate,iray
      REAL    :: twonyq,inv2nyq,thrpri
      REAL    :: refvel,tstdev,tstvel,thresh
      integer :: i,ii,j,jj,ip,ik
      integer :: step_step(41)
      logical :: back_check,fwrd_check,near_check
      real    :: a1,a2,a3,a4,a5
      real    :: b1,b2,b3,b4,b5
      real    :: c1,c2,c3,c4,c5
      real    :: d1,d2,d3,d4,d5
      real    :: e1,e2,e3,e4,e5

      real    :: athet,amean,anumber
      real    :: bthet,bmean,bnumber
      real    :: catmin,catmax,catmidian
      real,allocatable,dimension(:,:)    :: cyclone
      integer,allocatable,dimension(:,:) :: cyclone_indx
      integer,allocatable,dimension(:,:) :: converg_indx
      real    :: special_thrpri
      logical :: special_ref
      integer :: iimin,iimax,i_center
      real    :: phimax,phimin,phi_center
      real    :: first_below_midian,second_below_midian
      real    :: third_below_midian,fourth_below_midian
      real    :: first_large_midian,second_large_midian
      real    :: third_large_midian,fourth_large_midian
      real    :: first_below_mean,second_below_mean
      real    :: third_below_mean,fourth_below_mean
      real    :: first_large_mean,second_large_mean
      real    :: third_large_mean,fourth_large_mean

      real,allocatable,dimension(:,:) :: tmp_forw,tmp_back
      integer,allocatable,dimension(:,:) :: indx_forw,indx_back
      real,allocatable,dimension(:,:) :: tmp_anclk,tmp_clock
      integer,allocatable,dimension(:,:) :: indx_anclk,indx_clock
      real    :: near_thp,back_thp,shield_bottom,limit_number
      character(len=80) :: name1,name2,name3
      integer :: iter_check

!     @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!     Beginning of executable code...
!     @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      twonyq = 2.0*vnyq
      inv2nyq = 1./twonyq
      thrpri = 0.5*vnyq
      thresh=vnyq/3.0

      near_thp=vnyq/3.0
      near_thp=min(6.0,near_thp)
      back_thp=vnyq/4.0
      back_thp=min(5.0,back_thp)
      shield_bottom=2.6
      step_step(1)=0;step_step(2)=-1;step_step(3)=1
      step_step(4)=-2;step_step(5)=2
      step_step(6)=-3;step_step(7)=3
      step_step(8)=-4;step_step(9)=4
      step_step(10)=-5;step_step(11)=5
      step_step(12)=-6;step_step(13)=6
      step_step(14)=-7;step_step(15)=7
      step_step(16)=-8;step_step(17)=8
      step_step(18)=-9;step_step(19)=9
      step_step(20)=-10;step_step(21)=10
      step_step(22)=-11;step_step(23)=11
      step_step(24)=-12;step_step(25)=12
      step_step(26)=-13;step_step(27)=13
      step_step(28)=-14;step_step(29)=14
      step_step(30)=-15;step_step(31)=15
      step_step(32)=-16;step_step(33)=16
      step_step(34)=-17;step_step(35)=17
      step_step(36)=-18;step_step(37)=18
      step_step(38)=-19;step_step(39)=19
      step_step(40)=-20;step_step(41)=20

      write(name1,'(i4.4,5i2.2)') iyr,imon,iday,ihr,imin,isec
      ii=int( enangle*100.0 )
      write(name2,'(i4.4)') ii
      if ( ii>250 ) then
        special_thrpri=0.5*vnyq
      else
        special_thrpri=0.75*vnyq
      endif

      allocate( tmp_forw(maxgate,maxazim) )
      allocate( indx_forw(maxgate,maxazim) )
      allocate( tmp_back(maxgate,maxazim) )
      allocate( indx_back(maxgate,maxazim) )
      allocate( tmp_clock(maxgate,maxazim) )
      allocate( indx_clock(maxgate,maxazim) )
      allocate( tmp_anclk(maxgate,maxazim) )
      allocate( indx_anclk(maxgate,maxazim) )
      allocate( cyclone(maxgate,maxazim) )
      allocate( cyclone_indx(maxgate,maxazim) )
      allocate( converg_indx(maxgate,maxazim) )
!     ------------------------------------------------------------------
!     Set up tmp_forward and tmp_back to be quality arrays.
!     set up indx_forward, indx_back; -20=good 10=bad/missing.
!     ------------------------------------------------------------------
      tmp_forw=spval
      tmp_back=spval
      tmp_clock=spval
      tmp_anclk=spval
      DO iray=1,nazim
        DO igate=1,ngate
          if(abs(rvel(igate,iray))<spval .and. index(igate,iray)==0)then
            tmp_forw(igate,iray)=rvel(igate,iray)
            indx_forw(igate,iray)=-20
            tmp_back(igate,iray)=rvel(igate,iray)
            indx_back(igate,iray)=-20
            tmp_clock(igate,iray)=rvel(igate,iray)
            indx_clock(igate,iray)=-20
            tmp_anclk(igate,iray)=rvel(igate,iray)
            indx_anclk(igate,iray)=-20
          elseif ( index(igate,iray)==50 ) then
            indx_forw(igate,iray)=50
            indx_back(igate,iray)=50
            indx_clock(igate,iray)=50
            indx_anclk(igate,iray)=50
          else
            indx_forw(igate,iray)=10
            indx_back(igate,iray)=10
            indx_clock(igate,iray)=10
            indx_anclk(igate,iray)=10
          endif
        END DO
      END DO

      n10km=2

!     special point re-check
      Do igate=n10km,ngate        ! from radar to outside
      DO iray=1,nazim
        if ( indx_forw(igate,iray)==-20 ) then
          b1=0.0
          do ii=1,10
            i=igate-ii
            if ( i<1 ) cycle
            if ( indx_forw(i,iray)==50 ) then
              b1=b1+1.0
            endif
          enddo
          b2=0.0
          do ii=1,10
            i=igate+ii
            if ( i>ngate ) cycle
            if ( indx_forw(i,iray)==50 ) then
              b2=b2+1.0
            endif
          enddo
          IF ( b1>1.0 .and. b2>1.0 ) then
            indx_forw(igate,iray)=50
            tmp_forw(igate,iray)=spval
            indx_back(igate,iray)=50
            tmp_back(igate,iray)=spval
            indx_clock(igate,iray)=50
            tmp_clock(igate,iray)=spval
            indx_anclk(igate,iray)=50
            tmp_anclk(igate,iray)=spval
          ENDIF
        endif
      ENDDO
      ENDDO

!     name3='forw'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='back'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)

      iter_check=0
  100 continue
!     ------------------------------------------------------------------
!     very smoth check.
!     ------------------------------------------------------------------
      Do igate=n10km,ngate        ! from radar to outside
      DO iray=1,nazim
       if ( abs(ovel(igate,iray))<spval .and.                        &
            indx_forw(igate,iray)==50 ) then
         refvel=spval
         special_ref=.false.
         a1=0.0
         a2=0.0
         do jj=-1,1
          j=iray+jj
          i=igate-1
          if ( j<=0 ) j=j+nazim
          if ( indx_forw(i,j)==-20 ) then
            a1=a1+tmp_forw(i,j)
            a2=a2+1.0
          endif
         enddo
         if ( a2>1.0 ) then
           refvel=a1/a2
           IF ( abs(ovel(igate,iray)-refvel)<thrpri ) then
             tmp_forw(igate,iray)=ovel(igate,iray)
             indx_forw(igate,iray)=-20
           ENDIF
         endif
       endif    ! endif special points check
      ENDDO
      ENDDO

      DO igate=ngate-1,n10km,-1       ! out side to inside
      DO iray=1,nazim
       if ( abs(ovel(igate,iray))<spval .and.                        &
            indx_back(igate,iray)==50 ) then
         refvel=spval
         special_ref=.false.
         a1=0.0
         a2=0.0
         do jj=-1,1
          j=iray+jj
          i=igate+1
          if ( j<=0 ) j=j+nazim
          if ( indx_back(i,j)==-20 ) then
            a1=a1+tmp_back(i,j)
            a2=a2+1.0
          endif
         enddo
         if ( a2>1.0 ) then
           refvel=a1/a2
           IF ( abs(ovel(igate,iray)-refvel)<thrpri ) then
             tmp_back(igate,iray)=ovel(igate,iray)
             indx_back(igate,iray)=-20
           ENDIF
         endif
       endif    ! endif special points check
      ENDDO
      ENDDO

      DO iray=1,nazim            ! clockwise
      DO igate=n10km,ngate-1
       if ( abs(ovel(igate,iray))<spval .and.                        &
            indx_clock(igate,iray)==50 ) then
         refvel=spval
         special_ref=.false.
         a1=0.0
         a2=0.0
         do ii=-1,1
          j=iray-1
          if ( j<=0 ) j=j+nazim
          i=igate+ii
          if ( indx_clock(i,j)==-20 ) then
            a1=a1+tmp_clock(i,j)
            a2=a2+1.0
          endif
         enddo
         if ( a2>1.0 ) then
           refvel=a1/a2
           IF ( abs(ovel(igate,iray)-refvel)<thrpri ) then
             tmp_clock(igate,iray)=ovel(igate,iray)
             indx_clock(igate,iray)=-20
           ENDIF
         endif
       endif    ! endif special points check
      ENDDO
      ENDDO

      DO iray=nazim,1,-1            ! counterclockwise
      DO igate=n10km,ngate-1
       if ( abs(ovel(igate,iray))<spval .and.                        &
            indx_anclk(igate,iray)==50 ) then
         refvel=spval
         special_ref=.false.
         a1=0.0
         a2=0.0
         do ii=-1,1
          j=iray+1
          if ( j>nazim ) j=j-nazim
          i=igate+ii
          if ( indx_anclk(i,j)==-20 ) then
            a1=a1+tmp_anclk(i,j)
            a2=a2+1.0
          endif
         enddo
         if ( a2>1.0 ) then
           refvel=a1/a2
           IF ( abs(ovel(igate,iray)-refvel)<thrpri ) then
             tmp_anclk(igate,iray)=ovel(igate,iray)
             indx_anclk(igate,iray)=-20
           ENDIF
         endif
       endif    ! endif special points check
      ENDDO
      ENDDO

!     ------------------------------------------------------------------
!     combine the four checking results.
!     ------------------------------------------------------------------
      DO igate=n10km,ngate        ! inside to outside
      DO iray=1,nazim
       if ( index(igate,iray)==50 ) then
         IF ( indx_forw(igate,iray)==-20 ) THEN
           if ( indx_clock(igate,iray)==-20 ) then
             a1=tmp_forw(igate,iray)-tmp_clock(igate,iray)
             IF ( abs(a1)<0.1 ) THEN
               index(igate,iray)=0
               rvel(igate,iray)=tmp_forw(igate,iray)
             ENDIF
           elseif ( indx_anclk(igate,iray)==-20 ) then
             a1=tmp_forw(igate,iray)-tmp_anclk(igate,iray)
             IF ( abs(a1)<0.1 ) THEN
               index(igate,iray)=0
               rvel(igate,iray)=tmp_forw(igate,iray)
             ENDIF
           endif
         ELSEIF( indx_back(igate,iray)==-20 )THEN
           if ( indx_clock(igate,iray)==-20 ) then
             a1=tmp_back(igate,iray)-tmp_clock(igate,iray)
             IF ( abs(a1)<0.1 ) THEN
               index(igate,iray)=0
               rvel(igate,iray)=tmp_back(igate,iray)
             ENDIF
           elseif ( indx_anclk(igate,iray)==-20 ) then
             a1=tmp_back(igate,iray)-tmp_anclk(igate,iray)
             IF ( abs(a1)<0.1 ) THEN
               index(igate,iray)=0
               rvel(igate,iray)=tmp_back(igate,iray)
             ENDIF
           endif
         ENDIF
       endif
      ENDDO
      ENDDO

!     ------------------------------------------------------------------
!     re-filling the tmp-array
!     ------------------------------------------------------------------
      tmp_forw=spval
      tmp_back=spval
      tmp_clock=spval
      tmp_anclk=spval
      DO iray=1,nazim
        DO igate=1,ngate
          if ( index(igate,iray)==0 )then
            tmp_forw(igate,iray)=rvel(igate,iray)
            indx_forw(igate,iray)=-20
            tmp_back(igate,iray)=rvel(igate,iray)
            indx_back(igate,iray)=-20
            tmp_clock(igate,iray)=rvel(igate,iray)
            indx_clock(igate,iray)=-20
            tmp_anclk(igate,iray)=rvel(igate,iray)
            indx_anclk(igate,iray)=-20
          elseif ( index(igate,iray)==50 ) then
            indx_forw(igate,iray)=50
            indx_back(igate,iray)=50
            indx_clock(igate,iray)=50
            indx_anclk(igate,iray)=50
          else
            indx_forw(igate,iray)=10
            indx_back(igate,iray)=10
            indx_clock(igate,iray)=10
            indx_anclk(igate,iray)=10
          endif
        END DO
      END DO

!     ------------------------------------------------------------------
!     a little rough check.
!     ------------------------------------------------------------------
      Do igate=n10km,ngate        ! from radar to outside
      DO iray=1,nazim
       if ( abs(ovel(igate,iray))<spval .and.                        &
            indx_forw(igate,iray)==50 ) then
         refvel=spval
         special_ref=.false.
         do ii=1,5
          i=igate-ii
          if ( i<1 ) cycle
          if ( indx_forw(i,iray)==-20 ) then
            refvel=tmp_forw(i,iray)
            special_ref=.true.
            exit
          endif
         enddo
         IF ( special_ref ) THEN
           if ( abs(ovel(igate,iray)-refvel)<thrpri ) then
             tmp_forw(igate,iray)=ovel(igate,iray)
             indx_forw(igate,iray)=-20
           else
             tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
             IF ( abs(tstdev)>1.0 ) THEN
               tstvel=ovel(igate,iray)-tstdev
               if ( abs(tstvel-refvel )<thrpri ) then
                 IF ( abs(tstvel)<70.0 ) THEN
                   tmp_forw(igate,iray)=tstvel
                   indx_forw(igate,iray)=-20
                 ENDIF
               endif
             ENDIF
           endif
         ENDIF
       endif    ! endif special points check
      ENDDO
      ENDDO

      Do igate=ngate,n10km,-1        ! from outside to radar
      DO iray=1,nazim
       if ( abs(ovel(igate,iray))<spval .and.                        &
            indx_forw(igate,iray)==50 ) then
         refvel=spval
         special_ref=.false.
         do ii=1,5
          i=igate+ii
          if ( i>ngate ) cycle
          if ( indx_forw(i,iray)==-20 ) then
            refvel=tmp_forw(i,iray)
            special_ref=.true.
            exit
          endif
         enddo
         IF ( special_ref ) THEN
           if ( abs(ovel(igate,iray)-refvel)<thrpri ) then
             tmp_back(igate,iray)=ovel(igate,iray)
             indx_back(igate,iray)=-20
           else
             tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
             IF ( abs(tstdev)>1.0 ) THEN
               tstvel=ovel(igate,iray)-tstdev
               if ( abs(tstvel-refvel )<thrpri ) then
                 IF ( abs(tstvel)<70.0 ) THEN
                   tmp_back(igate,iray)=tstvel
                   indx_back(igate,iray)=-20
                 ENDIF
               endif
             ENDIF
           endif
         ENDIF
       endif    ! endif special points check
      ENDDO
      ENDDO

      DO iray=1,nazim             ! clockwise
      DO igate=n10km,ngate
       if ( abs(ovel(igate,iray))<spval .and.                        &
            indx_clock(igate,iray)==50 ) then
         refvel=spval
         special_ref=.false.
         do jj=1,2
          j=iray-jj
          if ( j<=0 ) j=j+nazim
          if ( indx_clock(igate,j)==-20 ) then
            refvel=tmp_clock(igate,j)
            special_ref=.true.
            exit
          endif
         enddo
         IF ( special_ref ) THEN
           if ( abs(ovel(igate,iray)-refvel)<thrpri ) then
             tmp_clock(igate,iray)=ovel(igate,iray)
             indx_clock(igate,iray)=-20
           else
             tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
             IF ( abs(tstdev)>1.0 ) THEN
               tstvel=ovel(igate,iray)-tstdev
               if ( abs(tstvel-refvel )<thrpri ) then
                 IF ( abs(tstvel)<70.0 ) THEN
                   tmp_clock(igate,iray)=tstvel
                   indx_clock(igate,iray)=-20
                 ENDIF
               endif
             ENDIF
           endif
         ENDIF
       endif    ! endif special points check
      ENDDO
      ENDDO

      DO iray=nazim,1,-1             ! counterclockwise
      DO igate=n10km,ngate
       if ( abs(ovel(igate,iray))<spval .and.                        &
            indx_anclk(igate,iray)==50 ) then
         refvel=spval
         special_ref=.false.
         do jj=1,2
          j=iray+jj
          if ( j>nazim ) j=j-nazim
          if ( indx_anclk(igate,j)==-20 ) then
            refvel=tmp_anclk(igate,j)
            special_ref=.true.
            exit
          endif
         enddo
         IF ( special_ref ) THEN
           if ( abs(ovel(igate,iray)-refvel)<thrpri ) then
             tmp_anclk(igate,iray)=ovel(igate,iray)
             indx_anclk(igate,iray)=-20
           else
             tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
             IF ( abs(tstdev)>1.0 ) THEN
               tstvel=ovel(igate,iray)-tstdev
               if ( abs(tstvel-refvel )<thrpri ) then
                 IF ( abs(tstvel)<70.0 ) THEN
                   tmp_anclk(igate,iray)=tstvel
                   indx_anclk(igate,iray)=-20
                 ENDIF
               endif
             ENDIF
           endif
         ENDIF
       endif    ! endif special points check
      ENDDO
      ENDDO

!     ------------------------------------------------------------------
!     combine the four checking results.
!     ------------------------------------------------------------------
      DO igate=n10km,ngate        ! inside to outside
      DO iray=1,nazim
       if ( index(igate,iray)==50 ) then
         IF ( indx_forw(igate,iray)==-20 ) THEN
           if ( indx_clock(igate,iray)==-20 ) then
             a1=tmp_forw(igate,iray)-tmp_clock(igate,iray)
             IF ( abs(a1)<0.1 ) THEN
               index(igate,iray)=0
               rvel(igate,iray)=tmp_forw(igate,iray)
             ENDIF
           elseif ( indx_anclk(igate,iray)==-20 ) then
             a1=tmp_forw(igate,iray)-tmp_anclk(igate,iray)
             IF ( abs(a1)<0.1 ) THEN
               index(igate,iray)=0
               rvel(igate,iray)=tmp_forw(igate,iray)
             ENDIF
           endif
         ELSEIF( indx_back(igate,iray)==-20 )THEN
           if ( indx_clock(igate,iray)==-20 ) then
             a1=tmp_back(igate,iray)-tmp_clock(igate,iray)
             IF ( abs(a1)<0.1 ) THEN
               index(igate,iray)=0
               rvel(igate,iray)=tmp_back(igate,iray)
             ENDIF
           elseif ( indx_anclk(igate,iray)==-20 ) then
             a1=tmp_back(igate,iray)-tmp_anclk(igate,iray)
             IF ( abs(a1)<0.1 ) THEN
               index(igate,iray)=0
               rvel(igate,iray)=tmp_back(igate,iray)
             ENDIF
           endif
         ENDIF
       endif
      ENDDO
      ENDDO

!     ------------------------------------------------------------------
!     re-filling the tmp-array
!     ------------------------------------------------------------------
      tmp_forw=spval
      tmp_back=spval
      tmp_clock=spval
      tmp_anclk=spval
      DO iray=1,nazim
        DO igate=1,ngate
          if ( index(igate,iray)==0 )then
            tmp_forw(igate,iray)=rvel(igate,iray)
            indx_forw(igate,iray)=-20
            tmp_back(igate,iray)=rvel(igate,iray)
            indx_back(igate,iray)=-20
            tmp_clock(igate,iray)=rvel(igate,iray)
            indx_clock(igate,iray)=-20
            tmp_anclk(igate,iray)=rvel(igate,iray)
            indx_anclk(igate,iray)=-20
          elseif ( index(igate,iray)==50 ) then
            indx_forw(igate,iray)=50
            indx_back(igate,iray)=50
            indx_clock(igate,iray)=50
            indx_anclk(igate,iray)=50
          else
            indx_forw(igate,iray)=10
            indx_back(igate,iray)=10
            indx_clock(igate,iray)=10
            indx_anclk(igate,iray)=10
          endif
        END DO
      END DO

      iter_check=iter_check+1
      if ( iter_check>=3 ) go to 200
      go to 100

  200 continue

!     ------------------------------------------------------------------
!     re-filling the tmp-array
!     ------------------------------------------------------------------
      tmp_forw=spval
      tmp_back=spval
      DO iray=1,nazim
        DO igate=1,ngate
          if ( index(igate,iray)==0 )then
            tmp_forw(igate,iray)=rvel(igate,iray)
            indx_forw(igate,iray)=-20
            tmp_back(igate,iray)=rvel(igate,iray)
            indx_back(igate,iray)=-20
          elseif ( index(igate,iray)==50 ) then
            indx_forw(igate,iray)=50
            indx_back(igate,iray)=50
          else
            indx_forw(igate,iray)=10
            indx_back(igate,iray)=10
          endif
        END DO
      END DO

!     name3='vadc'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='back'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)

!     ------------------------------------------------------------------
!     regular common check for no-special area.
!     ------------------------------------------------------------------
      Do igate=ngate-1,n10km,-1     ! from outside to radar
       DO iray=1,nazim         ! clockwise
        if ( abs(ovel(igate,iray))<spval .and.                       &
             indx_forw(igate,iray)==10 ) then
          a1=0.0
          a2=0.0
          catmin=100.0
          catmax=-100.0
          do i=igate-20,igate+20
           if ( i<1 ) cycle
           if ( i>ngate ) cycle
           do jj=1,11
            j=iray+step_step(jj)
            if ( j<=0 ) j=nazim+j
            if ( j>nazim ) j=j-nazim
            if ( abs(tmp_forw(i,j))<spval .and.                      &
                 indx_forw(i,j)<0 .and.                              &
                 (tmp3(igate)-tmp3(i))<500.0 ) then
              a1=a1+tmp_forw(i,j)
              a2=a2+1.0
              if ( tmp_forw(i,j)<catmin ) then
                catmin=tmp_forw(i,j)
              endif
              if ( tmp_forw(i,j)>catmax ) then
                catmax=tmp_forw(i,j)
              endif
            endif
           enddo
          enddo
          catmidian=0.5*(catmax+catmin)       ! get the midian value

          limit_number=40.0
          if ( igate<80 ) limit_number=20.0

          IF ( a2<limit_number ) THEN     ! T-check
            near_check=.false.
            back_check=.false.
            fwrd_check=.false.

            a3=spval
            do ii=1,10        !  backward to outside to get the seed
             i=igate+ii
             if ( i>ngate ) cycle
             if ( abs(tmp_forw(i,iray))<spval ) then
               a3=tmp_forw(i,iray)
               exit
             endif
            enddo
            if ( abs(a3)<spval ) then
              refvel=a3
              tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
              tstvel=ovel(igate,iray)-tstdev
              IF ( abs(tstvel-refvel ) < thrpri ) THEN
                tmp_forw(igate,iray)=tstvel
              ENDIF
              if (abs(refvel-tmp_forw(igate,iray))<near_thp ) then
                near_check=.true.
              endif
            endif

            a4=spval
            do jj=1,5         ! counter-clock to the circle to get the seed
             j=iray-jj
             i=igate
             if ( j<=0 ) then
               j=nazim+j
               i=igate+1
             endif
             if ( abs(tmp_forw(i,j))<spval) then
               a4=tmp_forw(i,j)
               exit
             endif
            enddo
            if ( abs(a4)<spval ) then
              refvel=a4
              tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
              tstvel=ovel(igate,iray)-tstdev
              IF ( abs(tstvel-refvel ) < thrpri ) THEN
                tmp_forw(igate,iray)=tstvel
              ENDIF
              if ( abs(refvel-tmp_forw(igate,iray))<back_thp ) then
                back_check=.true.
              endif
            endif

            a5=spval
            do jj=1,5        ! clockwise to the circle to get the seed
             j=iray+jj
             i=igate+1
             if ( j>nazim ) then
               j=j-nazim
               i=igate
             endif
             if ( abs(tmp_forw(i,j))<spval) then
               a5=tmp_forw(i,j)
               exit
             endif
            enddo
            if ( abs(a5)<spval ) then
              refvel=a5
              tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
              tstvel=ovel(igate,iray)-tstdev
              IF ( abs(tstvel-refvel ) < thrpri ) THEN
                tmp_forw(igate,iray)=tstvel
              ENDIF
              if ( abs(refvel-tmp_forw(igate,iray))<back_thp ) then
                fwrd_check=.true.
              endif
            endif

            if ( near_check ) then
              if ( back_check ) then
                indx_forw(igate,iray)=-20
              else
                if ( fwrd_check ) then
                  indx_forw(igate,iray)=-20
                elseif ( indx_forw(igate+1,iray)==50 ) then
                  indx_forw(igate,iray)=50
                  tmp_forw(igate,iray)=spval
                else
                  indx_forw(igate,iray)=10
                  tmp_forw(igate,iray)=spval
                endif
              endif
            else
              if ( back_check .and. fwrd_check ) then
                indx_forw(igate,iray)=-20
              elseif ( indx_forw(igate+1,iray)==50 ) then
                indx_forw(igate,iray)=50
                tmp_forw(igate,iray)=spval
              else
                indx_forw(igate,iray)=10
                tmp_forw(igate,iray)=spval
              endif
            endif
          ENDIF    ! endif T-check

          IF ( a2>=limit_number ) THEN     ! block check
            refvel=a1/a2
            if ( abs(ovel(igate,iray) - refvel) < thrpri ) then
              tmp_forw(igate,iray)=ovel(igate,iray)
              indx_forw(igate,iray)=-20
            else
              tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
              tstvel=ovel(igate,iray)-tstdev
              IF ( abs(tstvel-refvel ) < thresh ) THEN
                tmp_forw(igate,iray)=tstvel
                indx_forw(igate,iray)=-20
              ELSEIF ( abs(tstvel-refvel ) < thrpri ) THEN
                tmp_forw(igate,iray)=tstvel
                indx_forw(igate,iray)=0    ! need more T-check
              ELSE
                indx_forw(igate,iray)=50   ! special area index
              ENDIF
            endif

            if ( abs(refvel)>near_thp .and.                        &
                 abs(tmp_forw(igate,iray))<=shield_bottom ) then
              tmp_forw(igate,iray)=spval
              indx_forw(igate,iray)=50
            endif

            if ( abs(refvel)>near_thp .and.                        &
               (refvel*tmp_forw(igate,iray))<=0.0 ) then
              tmp_forw(igate,iray)=spval
              indx_forw(igate,iray)=50
            endif
          ENDIF    ! endif block check

          iF ( indx_forw(igate,iray)==0 ) THEN     ! more T-check
            near_check=.false.
            back_check=.false.
            fwrd_check=.false.

            a3=spval
            do ii=1,10        !  backward to outside to get the seed
             i=igate+ii
             if ( i>ngate ) cycle
             if ( abs(tmp_forw(i,iray))<spval ) then
               a3=tmp_forw(i,iray)
               exit
             endif
            enddo
            if ( abs(a3)<spval ) then
              if (abs(a3-tmp_forw(igate,iray))<near_thp ) then
                near_check=.true.
              endif
            endif

            a4=spval
            do jj=1,5        ! counter-clock to the circle to get the seed
             j=iray-jj
             i=igate
             if ( j<=0 ) then
               j=nazim+j
               i=igate+1
             endif
             if ( abs(tmp_forw(i,j))<spval) then
               a4=tmp_forw(i,j)
               exit
             endif
            enddo
            if ( abs(a4)<spval ) then
              if ( abs(a4-tmp_forw(igate,iray))<back_thp ) then
                back_check=.true.
              endif
            endif

            a5=spval
            do jj=1,5        ! clockwise to the circle to get the seed
             j=iray+jj
             i=igate+1
             if ( j>nazim ) then
               j=j-nazim
               i=igate
             endif
             if ( abs(tmp_forw(i,j))<spval) then
               a5=tmp_forw(i,j)
               exit
             endif
            enddo
            if ( abs(a5)<spval ) then
              if ( abs(a5-tmp_forw(igate,iray))<back_thp ) then
                fwrd_check=.true.
              endif
            endif

            if ( near_check ) then
              if ( back_check ) then
                indx_forw(igate,iray)=-20
              else
                if ( fwrd_check ) then
                  indx_forw(igate,iray)=-20
                else
                  indx_forw(igate,iray)=50
                  tmp_forw(igate,iray)=spval
                endif
              endif
            else
              if ( back_check .and. fwrd_check ) then
                indx_forw(igate,iray)=-20
              else
                indx_forw(igate,iray)=50
                tmp_forw(igate,iray)=spval
              endif
            endif
          ENDIF    ! endif more T-check
        endif   ! endif flag data
       ENDDO  ! enddo irat

       DO iray=nazim,1,-1      ! counter-clockwise
        if ( abs(ovel(igate,iray))<spval .and.                       &
             indx_back(igate,iray)==10 ) then
          a1=0.0
          a2=0.0
          catmin=100.0
          catmax=-100.0
          do i=igate-20,igate+20
           if ( i<1 ) cycle
           if ( i>ngate ) cycle
           do jj=1,11
            j=iray+step_step(jj)
            if ( j<=0 ) j=nazim+j
            if ( j>nazim ) j=j-nazim
            if ( tmp_back(i,j)<spval .and.                           &
                 indx_back(i,j)<0 .and.                              &
                 (tmp3(igate)-tmp3(i))<500.0 ) then
              a1=a1+tmp_back(i,j)
              a2=a2+1.0
              if ( tmp_back(i,j)<catmin ) then
                catmin=tmp_back(i,j)
              endif
              if ( tmp_back(i,j)>catmax ) then
                catmax=tmp_back(i,j)
              endif
            endif
           enddo
          enddo
          catmidian=0.5*(catmax+catmin)       ! get the midian value

          limit_number=40.0
          if ( igate<80 ) limit_number=20.0

          IF ( a2<limit_number ) THEN   ! T-check
            near_check=.false.
            back_check=.false.
            fwrd_check=.false.

            a3=spval
            do ii=1,10        ! backward to outside to get the seed
             i=igate+ii
             if ( i>ngate ) cycle
             if ( abs(tmp_back(i,iray))<spval ) then
               a3=tmp_back(i,iray)
               exit
             endif
            enddo
            if ( abs(a3)<spval ) then
              refvel=a3
              tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
              tstvel=ovel(igate,iray)-tstdev
              IF ( abs(tstvel-refvel ) < thrpri ) THEN
                tmp_back(igate,iray)=tstvel
              ENDIF
              if (abs(refvel-tmp_back(igate,iray))<near_thp ) then
                near_check=.true.
              endif
            endif

            a4=spval
            do jj=1,5         ! clockwise to the circle get the seed
             j=iray+jj
             i=igate
             if ( j>nazim ) then
               j=j-nazim
               i=igate+1
             endif
             if ( abs(tmp_back(i,j))<spval) then
               a4=tmp_back(i,j)
               exit
             endif
            enddo
            if ( abs(a4)<spval ) then
              refvel=a4
              tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
              tstvel=ovel(igate,iray)-tstdev
              IF ( abs(tstvel-refvel ) < thrpri ) THEN
                tmp_back(igate,iray)=tstvel
              ENDIF
              if ( abs(refvel-tmp_back(igate,iray))<back_thp ) then
                back_check=.true.
              endif
            endif

            a5=spval
            do jj=1,5         ! counter-clock to the circle get the seed
             j=iray-jj
             i=igate+1
             if ( j<=0) then
               j=j+nazim
               i=igate
             endif
             if ( abs(tmp_back(i,j))<spval) then
               a5=tmp_back(i,j)
               exit
             endif
            enddo
            if ( abs(a5)<spval ) then
              refvel=a5
              tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
              tstvel=ovel(igate,iray)-tstdev
              IF ( abs(tstvel-refvel ) < thrpri ) THEN
                tmp_back(igate,iray)=tstvel
              ENDIF
              if ( abs(refvel-tmp_back(igate,iray))<back_thp ) then
                fwrd_check=.true.
              endif
            endif

            if ( near_check ) then
              if ( back_check ) then
                indx_back(igate,iray)=-20
              else
                if ( fwrd_check ) then
                  indx_back(igate,iray)=-20
                elseif ( indx_back(igate+1,iray)==50 ) then
                  indx_back(igate,iray)=50
                  tmp_back(igate,iray)=spval
                else
                  indx_back(igate,iray)=10
                  tmp_back(igate,iray)=spval
                endif
              endif
            else
              if ( back_check .and. fwrd_check ) then
                indx_back(igate,iray)=-20
              elseif ( indx_back(igate+1,iray)==50 ) then
                indx_back(igate,iray)=50
                tmp_back(igate,iray)=spval
              else
                indx_back(igate,iray)=10
                tmp_back(igate,iray)=spval
              endif
            endif
          ENDIF   ! endif T-check

          IF ( a2>=limit_number ) THEN       ! block check
            refvel=a1/a2
            if ( abs(ovel(igate,iray) - refvel) < thrpri ) then
              tmp_back(igate,iray)=ovel(igate,iray)
              indx_back(igate,iray)=-20
            else
              tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
              tstvel=ovel(igate,iray)-tstdev
              IF ( abs(tstvel-refvel ) < thresh ) THEN
                tmp_back(igate,iray)=tstvel
                indx_back(igate,iray)=-20
              ELSEIF ( abs(tstvel-refvel ) < thrpri ) THEN
                tmp_back(igate,iray)=tstvel
                indx_back(igate,iray)=0      ! need more T-check
              ELSE
                indx_back(igate,iray)=50     ! special area index
              ENDIF
            endif

            if ( abs(refvel)>near_thp .and.                        &
                 abs(tmp_back(igate,iray))<=shield_bottom ) then
              tmp_back(igate,iray)=spval
              indx_back(igate,iray)=50
            endif

            if ( abs(refvel)>near_thp .and.                        &
                 (refvel*tmp_back(igate,iray))<=0.0 ) then
              tmp_back(igate,iray)=spval
              indx_back(igate,iray)=50
            endif
          ENDIF     ! endif block check

          IF ( indx_back(igate,iray)==0 ) THEN    ! more T-check
            near_check=.false.
            back_check=.false.
            fwrd_check=.false.

            a3=spval
            do ii=1,10        ! backward to outside to get the seed
             i=igate+ii
             if ( i>ngate ) cycle
             if ( abs(tmp_back(i,iray))<spval ) then
               a3=tmp_back(i,iray)
               exit
             endif
            enddo
            if ( abs(a3)<spval ) then
              if (abs(a3-tmp_back(igate,iray))<near_thp ) then
                near_check=.true.
              endif
            endif

            a4=spval
            do jj=1,5         ! clockwise to the circle get the seed
             j=iray+jj
             i=igate
             if ( j>nazim ) then
               j=j-nazim
               i=igate+1
             endif
             if ( abs(tmp_back(i,j))<spval) then
               a4=tmp_back(i,j)
               exit
             endif
            enddo
            if ( abs(a4)<spval ) then
              if ( abs(a4-tmp_back(igate,iray))<back_thp ) then
                back_check=.true.
              endif
            endif

            a5=spval
            do jj=1,5         ! counter-clock to the circle get the seed
             j=iray-jj
             i=igate+1
             if ( j<=0) then
               j=j+nazim
               i=igate
             endif
             if ( abs(tmp_back(i,j))<spval) then
               a5=tmp_back(i,j)
               exit
             endif
            enddo
            if ( abs(a5)<spval ) then
              if ( abs(a5-tmp_back(igate,iray))<back_thp ) then
                fwrd_check=.true.
              endif
            endif

            if ( near_check ) then
              if ( back_check ) then
                indx_back(igate,iray)=-20
              else
                if ( fwrd_check ) then
                  indx_back(igate,iray)=-20
                else
                  indx_back(igate,iray)=50
                  tmp_back(igate,iray)=spval
                endif
              endif
            else
              if ( back_check .and. fwrd_check ) then
                indx_back(igate,iray)=-20
              else
                indx_back(igate,iray)=50
                tmp_back(igate,iray)=spval
              endif
            endif
          ENDIF    ! endif more T-check
        endif    ! endif flag data
       ENDDO   ! enddo iray

!      clockwise check, counter-clockwise check combine
       DO iray=1,nazim
        IF ( indx_forw(igate,iray)==-20 ) THEN
          if ( indx_back(igate,iray)==-20 ) then
            a1=tmp_forw(igate,iray)-tmp_back(igate,iray)
            if ( abs(a1)>0.01 ) then
              tmp_forw(igate,iray)=spval
              indx_forw(igate,iray)=10
              tmp_back(igate,iray)=spval
              indx_back(igate,iray)=10
            endif
          else    ! indx_back>0
            indx_back(igate,iray)=indx_forw(igate,iray)
            tmp_back(igate,iray)=tmp_forw(igate,iray)
          endif
        ELSE       ! indx_forw>0
          tmp_forw(igate,iray)=tmp_back(igate,iray)
          indx_forw(igate,iray)=indx_back(igate,iray)
        ENDIF     ! endif indx_forw
       ENDDO    ! enddo combine
      ENDDO   ! enddo igate

      cyclone=spval
      special_points=0
      DO iray=1,nazim
      DO igate=1,ngate
        cyclone(igate,iray)=tmp_forw(igate,iray)
        if ( indx_forw(igate,iray)==50 ) then
          special_points=special_points+1
          cyclone(igate,iray)=888.8
        endif
      ENDDO
      ENDDO
!     name3='vadd'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)


!     special point re-check
      Do igate=n10km,ngate        ! from radar to outside
      DO iray=1,nazim
        if ( indx_forw(igate,iray)==-20 ) then
          b1=0.0
          do ii=1,10
            i=igate-ii
            if ( i<1 ) cycle
            if ( indx_forw(i,iray)==50 ) then
              b1=b1+1.0
            endif
          enddo
          b2=0.0
          do ii=1,35
            i=igate+ii
            if ( i>ngate ) cycle
            if ( indx_forw(i,iray)==50 ) then
              b2=b2+1.0
            endif
          enddo
          IF ( b1>1.0 .and. b2>1.0 ) then
            indx_forw(igate,iray)=50
            tmp_forw(igate,iray)=spval
            indx_back(igate,iray)=50
            tmp_back(igate,iray)=spval
          ENDIF
        endif
        if ( indx_forw(igate,iray)==-20 ) then
          b1=0.0
          do jj=1,2
            j=iray-jj
            if ( j<=0 ) j=j+nazim
            if ( indx_forw(igate,j)==50 ) then
              b1=b1+1.0
            endif
          enddo
          b2=0.0
          do jj=1,2
            j=iray+jj
            if ( j>nazim ) j=j-nazim
            if ( indx_forw(igate,j)==50 ) then
              b2=b2+1.0
            endif
          enddo
          IF ( b1>0.0 .and. b2>0.0 ) then
            indx_forw(igate,iray)=50
            tmp_forw(igate,iray)=spval
            indx_back(igate,iray)=50
            tmp_back(igate,iray)=spval
          ENDIF
        endif
      ENDDO
      ENDDO

!     name3='vadf'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='vadi'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     name3='back'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)
!     ------------------------------------------------------------------
!     failed alarm check
!     ------------------------------------------------------------------
      cyclone_indx=-20
      DO igate=n10km,ngate
      DO iray=1,nazim
       if ( indx_forw(igate,iray)==50 ) then
!        get a midian value
         a1=0.0
         a2=0.0
         catmin=100.0
         catmax=-100.0
         do i=igate-20,igate+20
          if ( i<1 ) cycle
          if ( i>ngate ) cycle
          do jj=1,11
           j=iray+step_step(jj)
           if ( j<=0 ) j=nazim+j
           if ( j>nazim ) j=j-nazim
           if ( abs(tmp_forw(i,j))<spval .and.                      &
                indx_forw(i,j)<0 ) then
             a1=a1+tmp_forw(i,j)
             a2=a2+1.0
             if ( tmp_forw(i,j)<catmin ) then
               catmin=tmp_forw(i,j)
               phimin=phi(j)
               iimin=i
             endif
             if ( tmp_forw(i,j)>catmax ) then
               catmax=tmp_forw(i,j)
               iimax=i
               phimax=phi(j)
             endif
           endif
          enddo
         enddo
         catmidian=0.5*(catmax+catmin)

!        ---------------------------------------------------------------
!        design four phase.
!
!           first phase    !   fourth phase
!           ------------------------------    !^gate
!           second phase   !   third phase
!                         > azmuth
!
!                         radar
!
!        ---------------------------------------------------------------
         i_center=iimin+(iimax-iimin)/2
         phi_center=(phimax-360.0*NINT((phimax-phimin)/360.0))-phimin
         phi_center=phimin+phi_center/2.0

         IF ( a2>10.0 .and. abs(catmax-catmin)>vnyq ) THEN  ! cyclone shear
           athet=0.0
           amean=0.0
           anumber=0.0
           bthet=0.0
           bmean=0.0
           bnumber=0.0
           first_below_midian=0.0
           first_large_midian=0.0
           second_below_midian=0.0
           second_large_midian=0.0
           third_below_midian=0.0
           third_large_midian=0.0
           fourth_below_midian=0.0
           fourth_large_midian=0.0
           do i=igate-20,igate+20
            if ( i<1 ) cycle
            if ( i>ngate ) cycle
            do jj=1,11
             j=iray+step_step(jj)
             if ( j<=0 ) j=nazim+j
             if ( j>nazim ) j=j-nazim
             if ( abs(tmp_forw(i,j))<spval .and.                &
                  indx_forw(i,j)<0 ) then
               d1=phi(iray)
               d2=360.0*NINT((phi(j)-d1)/360.0)
               if ( tmp_forw(i,j)<=catmidian ) then
                 athet=athet+((phi(j)-d2)-d1)     ! dalte(azimuth)
                 amean=amean+tmp_forw(i,j)
                 anumber=anumber+1.0
               else
                 bthet=bthet+((phi(j)-d2)-d1)     ! dalte(azimuth)
                 bmean=bmean+tmp_forw(i,j)
                 bnumber=bnumber+1.0
               endif

!              phase counter
               d1=phi(j)-360.0*NINT((phi(j)-phimin)/360.0)
               if ( tmp_forw(i,j)<=catmidian ) then

                 IF ( d1>phi_center ) THEN   ! phase 3 or phase 4
                   if ( i>i_center ) then   ! phase 4
                     fourth_below_midian=fourth_below_midian+1.0
                   else      ! phase 3
                     third_below_midian=third_below_midian+1.0
                   endif
                 ELSE       ! phase 1 or phase 2
                   if ( i>i_center ) then   ! phase 1
                     first_below_midian=first_below_midian+1.0
                   else   ! phase 2
                     second_below_midian=second_below_midian+1.0
                   endif
                 ENDIF

               else

                 IF ( d1>phi_center ) THEN   ! phase 3 or phase 4
                   if ( i>i_center ) then   ! phase 4
                     fourth_large_midian=fourth_large_midian+1.0
                   else      ! phase 3
                     third_large_midian=third_large_midian+1.0
                   endif
                 ELSE       ! phase 1 or phase 2
                   if ( i>i_center ) then   ! phase 1
                     first_large_midian=first_large_midian+1.0
                   else   ! phase 2
                     second_large_midian=second_large_midian+1.0
                   endif
                 ENDIF

               endif     ! endif midian check
             endif     ! endif available point
            enddo
           enddo
           athet=athet/anumber
           amean=amean/anumber
           bthet=bthet/bnumber
           bmean=bmean/bnumber

           if ( abs(bmean-amean)>0.5*vnyq ) then ! cyclone area
             IF ( athet<bthet ) THEN ! cyclone flow
               b1=second_below_midian+first_below_midian
               b2=third_below_midian+fourth_below_midian
               b3=second_large_midian+first_large_midian
               b4=third_large_midian+fourth_large_midian
               if ( b1>b3 .and. b1>10.0 ) then   ! cyclone buck
                 c1=b3/b1
               else
                 c1=spval
               endif
               if ( b2<b4 .and. b4>10.0 ) then   ! cyclone buck
                 c2=b2/b4
               else
                 c2=spval
               endif
               if ( b1>b2 ) then   ! cyclone tendence
                 c3=b2/b1
               else
                 c3=spval
               endif
               if ( b3<b4 ) then   ! cyclone tendence
                 c4=b3/b4
               else
                 c4=spval
               endif
               if ( c1<0.5 .and. c2<0.5 ) then
                 if ( c3<0.5 .or. c4<0.5 ) then
                   cyclone_indx(igate,iray)=50    ! cyclone
                 else
                   cyclone_indx(igate,iray)=0     ! noise
                 endif
               else
                 cyclone_indx(igate,iray)=0       ! cyclone tendence
               endif
             ELSE
               cyclone_indx(igate,iray)=0         ! cyclone flow
             ENDIF    ! endif cyclone flow
           else
             cyclone_indx(igate,iray)=0           ! cyclone area
           endif     ! endif cyclone area
         ELSE
           cyclone_indx(igate,iray)=0             ! cyclone shear
         ENDIF     ! endif cyclone shear
       endif    ! endif alarm point
      ENDDO
      ENDDO

!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=cyclone_indx(igate,iray)
!     ENDDO
!     ENDDO
!     name3='back'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)
!     ------------------------------------------------------------------
!     convergence check.
!     ------------------------------------------------------------------
      converg_indx=-20
      DO igate=n10km,ngate
      DO iray=1,nazim
       if ( indx_forw(igate,iray)==50 ) then
!        get the midian value
         a1=0.0
         a2=0.0
         catmin=100.0
         catmax=-100.0
         do i=igate-20,igate+20
          if ( i<1 ) cycle
          if ( i>ngate ) cycle
          do jj=1,11
           j=iray+step_step(jj)
           if ( j<=0 ) j=nazim+j
           if ( j>nazim ) j=j-nazim
           if ( abs(tmp_forw(i,j))<spval .and.                      &
                indx_forw(i,j)<0 ) then
             a1=a1+tmp_forw(i,j)
             a2=a2+1.0
             if ( tmp_forw(i,j)<catmin ) then
               catmin=tmp_forw(i,j)
               phimin=phi(j)
               iimin=i
             endif
             if ( tmp_forw(i,j)>catmax ) then
               catmax=tmp_forw(i,j)
               iimax=i
               phimax=phi(j)
             endif
           endif
          enddo
         enddo
         catmidian=0.5*(catmax+catmin)

!        ---------------------------------------------------------------
!        design four phase.
!
!           first phase    !   fourth phase
!           ------------------------------    !^gate
!           second phase   !   third phase
!                         > azmuth
!
!                         radar
!
!        ---------------------------------------------------------------
         i_center=iimin+(iimax-iimin)/2
         phi_center=(phimax-360.0*NINT((phimax-phimin)/360.0))-phimin
         phi_center=phimin+phi_center/2.0

         IF ( a2>10.0 .and.  abs(catmax-catmin)>vnyq ) then
           amean=0.0
           anumber=0.0
           bmean=0.0
           bnumber=0.0
           first_below_midian=0.0
           first_below_mean=0.0
           first_large_midian=0.0
           first_large_mean=0.0
           second_below_midian=0.0
           second_below_mean=0.0
           second_large_midian=0.0
           second_large_mean=0.0
           third_below_midian=0.0
           third_below_mean=0.0
           third_large_midian=0.0
           third_large_mean=0.0
           fourth_below_midian=0.0
           fourth_below_mean=0.0
           fourth_large_midian=0.0
           fourth_large_mean=0.0
           do i=igate-20,igate+20
            if ( i<1 ) cycle
            if ( i>ngate ) cycle
            do jj=1,11
             j=iray+step_step(jj)
             if ( j<=0 ) j=nazim+j
             if ( j>nazim ) j=j-nazim
             if ( abs(tmp_forw(i,j))<spval .and.                &
                  indx_forw(i,j)<0 ) then
               if ( tmp_forw(i,j)<=catmidian ) then
                 amean=amean+tmp_forw(i,j)
                 anumber=anumber+1.0
               else
                 bmean=bmean+tmp_forw(i,j)
                 bnumber=bnumber+1.0
               endif

!              phase counter
               d1=phi(j)-360.0*NINT((phi(j)-phimin)/360.0)
               if ( tmp_forw(i,j)<=catmidian ) then

                 IF ( d1>phi_center ) THEN   ! phase 3 or phase 4
                   if ( i>i_center ) then   ! phase 4
                     fourth_below_midian=fourth_below_midian+1.0
                     fourth_below_mean=fourth_below_mean+tmp_forw(i,j)
                   else      ! phase 3
                     third_below_midian=third_below_midian+1.0
                     third_below_mean=third_below_mean+tmp_forw(i,j)
                   endif
                 ELSE       ! phase 1 or phase 2
                   if ( i>i_center ) then   ! phase 1
                     first_below_midian=first_below_midian+1.0
                     first_below_mean=first_below_mean+tmp_forw(i,j)
                   else   ! phase 2
                     second_below_midian=second_below_midian+1.0
                     second_below_mean=second_below_mean+tmp_forw(i,j)
                   endif
                 ENDIF

               else

                 IF ( d1>phi_center ) THEN   ! phase 3 or phase 4
                   if ( i>i_center ) then   ! phase 4
                     fourth_large_midian=fourth_large_midian+1.0
                     fourth_large_mean=fourth_large_mean+tmp_forw(i,j)
                   else      ! phase 3
                     third_large_midian=third_large_midian+1.0
                     third_large_mean=third_large_mean+tmp_forw(i,j)
                   endif
                 ELSE       ! phase 1 or phase 2
                   if ( i>i_center ) then   ! phase 1
                     first_large_midian=first_large_midian+1.0
                     first_large_mean=first_large_mean+tmp_forw(i,j)
                   else   ! phase 2
                     second_large_midian=second_large_midian+1.0
                     second_large_mean=second_large_mean+tmp_forw(i,j)
                   endif
                 ENDIF

               endif     ! endif midian check
             endif     ! endif available point
            enddo
           enddo

           amean=amean/anumber
           bmean=bmean/bnumber

           if ( first_below_midian>5 ) then
             first_below_mean=first_below_mean/first_below_midian
           else
             first_below_mean=0.0
           endif
           if ( second_below_midian>5 ) then
             second_below_mean=second_below_mean/second_below_midian
           else
             second_below_mean=0.0
           endif
           if ( third_below_midian>5 ) then
             third_below_mean=third_below_mean/third_below_midian
           else
             third_below_mean=0.0
           endif
           if ( fourth_below_midian>5 ) then
             fourth_below_mean=fourth_below_mean/fourth_below_midian
           else
             fourth_below_mean=0.0
           endif
           if ( first_large_midian>5 ) then
             first_large_mean=first_large_mean/first_large_midian
           else
             first_large_mean=0.0
           endif
           if ( second_large_midian>5 ) then
             second_large_mean=second_large_mean/second_large_midian
           else
             second_large_mean=0.0
           endif
           if ( third_large_midian>5 ) then
             third_large_mean=third_large_mean/third_large_midian
           else
             third_large_mean=0.0
           endif
           if ( fourth_large_midian>5 ) then
             fourth_large_mean=fourth_large_mean/fourth_large_midian
           else
             fourth_large_mean=0.0
           endif

           if ( abs(bmean-amean)>0.5*vnyq .and. bmean*amean<0.0 ) then ! cyclone area
             b1=first_below_mean+fourth_below_mean
             b2=first_large_mean+fourth_large_mean
             b3=second_below_mean+third_below_mean
             b4=second_large_mean+third_large_mean
!            IF ( b1>b2 ) THEN   ! negative buck
!              c1=b2/b1
!              if ( b3<b4 ) then ! positive buck
!                c2=b3/b4
!              else
!                c2=spval
!              endif
!            ELSE                ! positive buck
!              c1=b1/b2
!              if ( b3<b4 ) then ! positive buck
!                c2=spval
!              else
!                c2=b4/b3
!              endif
!            ENDIF
!            write(999,'(2i6,7f10.1)') iray,igate,phi(iray)          &
!                                     ,b1,b2,b3,b4                   &
!                                     ,bmean,amean
             c1=abs(b1+b3)
             c2=abs(b2+b4)
             IF ( c1>vnyq .and. c2>vnyq ) THEN
               converg_indx(igate,iray)=50    ! convergance
             ELSE
               converg_indx(igate,iray)=40    ! convergence flow
             ENDIF    ! endif cyclone flow
           else
             converg_indx(igate,iray)=0       ! convergence area
           endif     ! endif cyclone area
         ELSE
           converg_indx(igate,iray)=0         ! covergence tendence
         ENDIF     ! endif cyclone shear
       endif    ! endif alarm point
      ENDDO
      ENDDO

!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=converg_indx(igate,iray)
!     ENDDO
!     ENDDO
!     name3='vadf'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)
!     ------------------------------------------------------------------
!     combine the convergence and cyclone check.
!     ------------------------------------------------------------------
      DO igate=n10km,ngate
      DO iray=1,nazim
        if ( converg_indx(igate,iray)==50 ) then
          indx_forw(igate,iray)=50
        else
          if ( cyclone_indx(igate,iray)==50 ) then
            a1=0.0
            do i=igate-20,igate+20
              if ( i<1 ) cycle
              if ( i>ngate ) cycle
              do jj=1,11
                j=iray+step_step(jj)
                if ( j<=0 ) j=nazim+j
                if ( j>nazim ) j=j-nazim
                if ( converg_indx(i,j)>=40 ) then
                  a1=a1+1.0
                endif
              enddo
            enddo
            IF ( a1>10.0 ) THEN
              indx_forw(igate,iray)=50
            ELSE
              indx_forw(igate,iray)=10
            ENDIF
          elseif ( cyclone_indx(igate,iray)==0 ) then
            indx_forw(igate,iray)=10
          endif
        endif
      ENDDO
      ENDDO

!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='vadi'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)
!     ------------------------------------------------------------------
!     isolate check.
!     ------------------------------------------------------------------
      DO igate=n10km-1,ngate
      DO iray=1,nazim
        if ( indx_forw(igate,iray)==50 ) then
          a1=0.0
          do jj=-1,1
            j=iray+jj
            if ( j<=0 ) j=nazim+j
            if ( j>nazim ) j=j-nazim
            do ii=-1,1
              i=igate+ii
              if ( i>ngate ) cycle
              if ( indx_forw(i,j)==50 ) then
                a1=a1+1.0
              endif
            enddo
          enddo
          if ( a1<2.0 ) then
            indx_forw(igate,iray)=10
          endif
        endif
      ENDDO
      ENDDO

!     name3='forw'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='back'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)
!     ------------------------------------------------------------------
!     Re-alarm the cyclone area.
!     ------------------------------------------------------------------
      DO igate=n10km,ngate
      DO iray=1,nazim
       if ( indx_forw(igate,iray)==50 ) then
         do ii=-20,20
          i=igate+ii
          if ( i<n10km ) cycle
          if ( i>ngate ) cycle
          do jj=-10,10
           j=iray+jj
           if ( j<=0 ) j=j+nazim
           if ( j>nazim ) j=j-nazim
           if ( indx_forw(i,j)>=10 .and. abs(ovel(i,j))<spval ) then
             indx_forw(i,j)=50
           endif
          enddo
         enddo
       endif
      ENDDO
      ENDDO

!     name3='vadf'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='vadi'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)
!     ------------------------------------------------------------------
!     Re-found the cyclone area.
!     ------------------------------------------------------------------
      DO igate=n10km,ngate
      DO iray=1,nazim
       if ( indx_forw(igate,iray)==50 ) then
         do i=igate,n10km,-1
           if ( indx_forw(i,iray)==10 .and.                          &
                abs(ovel(i,iray))<spval ) then
             indx_forw(i,iray)=50
           elseif ( indx_forw(i,iray)<0 ) then
             exit
           endif
         enddo
       endif
      ENDDO
      ENDDO
!     name3='vadf'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='vadi'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)
!     ------------------------------------------------------------------
!     mark the special area.
!     ------------------------------------------------------------------
      DO igate=n10km,ngate
      DO iray=1,nazim
        IF ( indx_forw(igate,iray)==-20 ) THEN
          b1=0.0
          do ii=1,20
           i=igate-ii
           if ( i<1 ) cycle
           if ( indx_forw(i,iray)==50 ) then
             b1=b1+1.0
           endif
          enddo
          b2=0.0
          do ii=1,10
           i=igate+ii
           if ( i>ngate ) cycle
           if ( indx_forw(i,iray)==50 ) then
             b2=b2+1.0
           endif
          enddo
          if ( b1>=6.0 .and. b2>=1.0 ) then
            indx_forw(igate,iray)=50
            tmp_forw(igate,iray)=spval
          elseif ( b1>=2.0 .and. b2>=2.0 ) then
            indx_forw(igate,iray)=50
            tmp_forw(igate,iray)=spval
          endif
        ENDIF
      ENDDO
      ENDDO

!     name3='forw'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='back'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)
!     ------------------------------------------------------------------
!     special area check.
!     ------------------------------------------------------------------
      indx_back=indx_forw
      tmp_back=tmp_forw
      special_thrpri=vnyq/3.0
      DO igate=n10km,ngate-1      ! to outside
      DO iray=1,nazim
       if ( indx_forw(igate,iray)==50 ) then
!        get the reference
         refvel=spval
         special_ref=.false.
!        3X3 window check
         a1=0.0
         a2=0.0
         do jj=-1,1
           j=iray+jj
           if ( j<=0 ) j=nazim+j
           if ( j>nazim ) j=j-nazim
           do ii=-1,1
             i=igate+ii
             if ( indx_forw(i,j)<=-10 ) then
               a1=a1+tmp_forw(i,j)
               a2=a2+1.0
             endif
           enddo
         enddo
         IF ( a2>1.0 ) THEN
           refvel=a1/a2
           if ( abs(ovel(igate,iray)-refvel)<special_thrpri ) then
             tmp_forw(igate,iray)=ovel(igate,iray)
             indx_forw(igate,iray)=-10
             special_ref=.true.
           else
             tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
             IF ( abs(tstdev)>1.0 ) THEN
               tstvel=ovel(igate,iray)-tstdev
               if ( abs(tstvel-refvel )<special_thrpri ) then
                 IF ( abs(tstvel)<70.0 ) THEN
                   tmp_forw(igate,iray)=tstvel
                   indx_forw(igate,iray)=-10
                   special_ref=.true.
                 ENDIF
               endif
             ENDIF
           endif
         ENDIF
!        beam continuity check
         IF ( .not. special_ref ) THEN
           a1=0.0
           a2=0.0
           do ii=1,5
            i=igate-ii
            if ( i<1 ) cycle
            if ( indx_forw(i,iray)<=-10 ) then
              a1=a1+tmp_forw(i,iray)
              a2=a2+1.0
            endif
           enddo
           if ( a2>1.0 ) then
             refvel=a1/a2
             IF ( abs(ovel(igate,iray)-refvel)<special_thrpri ) THEN
               tmp_forw(igate,iray)=ovel(igate,iray)
               indx_forw(igate,iray)=-10
             ELSE
               tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
               if ( abs(tstdev)>1.0 ) then
                 tstvel=ovel(igate,iray)-tstdev
                 IF ( abs(tstvel-refvel )<special_thrpri ) THEN
                   if ( abs(tstvel)<70.0 ) then
                     tmp_forw(igate,iray)=tstvel
                     indx_forw(igate,iray)=-10
                   endif
                 ENDIF
               endif
             ENDIF
           endif
         ENDIF    ! endif not special_ref
       endif
      ENDDO
      ENDDO

      DO igate=ngate-1,n10km,-1       ! to inside
      DO iray=1,nazim
       if ( indx_back(igate,iray)==50 ) then
!        get the reference
         refvel=spval
         special_ref=.false.
!        3X3 window check
         a1=0.0
         a2=0.0
         do jj=-1,1
           j=iray+jj
           if ( j<=0 ) j=nazim+j
           if ( j>nazim ) j=j-nazim
           do ii=-1,1
             i=igate+ii
             if ( indx_back(i,j)<=-10 ) then
               a1=a1+tmp_back(i,j)
               a2=a2+1.0
             endif
           enddo
         enddo
         IF ( a2>1.0 ) THEN
           refvel=a1/a2
           if ( abs(ovel(igate,iray)-refvel)<special_thrpri ) then
             tmp_back(igate,iray)=ovel(igate,iray)
             indx_back(igate,iray)=-10
             special_ref=.true.
           else
             tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
             IF ( abs(tstdev)>1.0 ) THEN
               tstvel=ovel(igate,iray)-tstdev
               if ( abs(tstvel-refvel )<special_thrpri ) then
                 IF ( abs(tstvel)<70.0 ) THEN
                   tmp_back(igate,iray)=tstvel
                   indx_back(igate,iray)=-10
                   special_ref=.true.
                 ENDIF
               endif
             ENDIF
           endif
         ENDIF
!        beam continuity check
         IF ( .not. special_ref ) THEN
           a1=0.0
           a2=0.0
           do ii=1,5
            i=igate+ii
            if ( i>ngate ) cycle
            if ( indx_back(i,iray)<=-10 ) then
              a1=a1+tmp_back(i,iray)
              a2=a2+1.0
            endif
           enddo
           if ( a2>1.0 ) then
             refvel=a1/a2
             IF ( abs(ovel(igate,iray)-refvel)<special_thrpri ) THEN
               tmp_back(igate,iray)=ovel(igate,iray)
               indx_back(igate,iray)=-10
             ELSE
               tstdev=twonyq*NINT((ovel(igate,iray)-refvel)*inv2nyq)
               if ( abs(tstdev)>1.0 ) then
                 tstvel=ovel(igate,iray)-tstdev
                 IF ( abs(tstvel-refvel )<special_thrpri ) THEN
                   if ( abs(tstvel)<70.0 ) then
                     tmp_back(igate,iray)=tstvel
                     indx_back(igate,iray)=-10
                   endif
                 ENDIF
               endif
             ENDIF
           endif
         ENDIF    ! endif not special_ref
       endif
      ENDDO
      ENDDO

!     name3='forw'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='back'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)

!     name3='vadf'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_back)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_back(igate,iray)
!     ENDDO
!     ENDDO
!     name3='vadi'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)

!     combine the special area
      DO igate=n10km,ngate-1       ! to outside
      DO iray=1,nazim
        if ( indx_back(igate,iray)==-10 ) then
          if ( abs(tmp_back(igate,iray))>0.8*vnyq ) then
            if ( indx_forw(igate,iray)==-10 ) then
              a1=tmp_forw(igate,iray)-tmp_back(igate,iray)
              if ( abs(a1)>0.1 ) then
                indx_forw(igate,iray)=50
                tmp_forw(igate,iray)=spval
                indx_back(igate,iray)=50
                tmp_back(igate,iray)=spval
              endif
            else
              indx_forw(igate,iray)=50
              tmp_forw(igate,iray)=spval
              indx_back(igate,iray)=50
              tmp_back(igate,iray)=spval
            endif
          else
            tmp_forw(igate,iray)=tmp_back(igate,iray)
            indx_forw(igate,iray)=indx_back(igate,iray)
          endif
        else
          tmp_forw(igate,iray)=tmp_back(igate,iray)
          indx_forw(igate,iray)=indx_back(igate,iray)
        endif
      ENDDO
      ENDDO

!     name3='forw'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='back'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)
!     ------------------------------------------------------------------
!     eliminate the rough jump along the radial.
!     ------------------------------------------------------------------
      DO iray=1,nazim
      DO igate=n10km,ngate
       if ( indx_forw(igate,iray)<=-10 ) then
         IF ( abs(tmp_forw(igate,iray))>70.0 .and.                   &
              abs(tmp_forw(igate,iray))<spval ) THEN
           refvel=spval
           do ii=1,20
            i=igate-ii
            if ( i<1 ) cycle
            if ( indx_forw(i,iray)<=-10 ) then
              refvel=tmp_forw(i,iray)
              exit
            endif
           enddo
           IF ( abs(tmp_forw(igate,iray)-refvel)>thrpri ) THEN
             tstdev=twonyq*NINT((tmp_forw(igate,iray)-refvel)*inv2nyq)
             if ( abs(tstdev)>1.0 ) then
               tstvel=tmp_forw(igate,iray)-tstdev
               IF ( abs(tstvel-refvel )<thrpri ) THEN
                 tmp_forw(igate,iray)=tstvel
               ELSE
                 tmp_forw(igate,iray)=spval
                 indx_forw(igate,iray)=10
               ENDIF
             endif
           ENDIF
         ENDIF
       endif
      ENDDO
      ENDDO

!     name3='vadf'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,tmp_forw)
!     cyclone=spval
!     DO iray=1,nazim
!     DO igate=1,ngate
!       cyclone(igate,iray)=indx_forw(igate,iray)
!     ENDDO
!     ENDDO
!     name3='vadi'//name1(1:14)//'_'//name2(1:4)//'.dat'
!     call write_radar(name3,cyclone)
!     ------------------------------------------------------------------
!     combine the check results.
!     ------------------------------------------------------------------
      rvel=spval
      unfvel=spval
      index=10
      DO iray=1,nazim
      DO igate=1,ngate
        IF ( indx_forw(igate,iray)<=-10 ) THEN
          rvel(igate,iray)=tmp_forw(igate,iray)
          unfvel(igate,iray)=tmp_forw(igate,iray)
          index(igate,iray)=0
        ELSEIF ( indx_forw(igate,iray)==50 ) THEN
          index(igate,iray)=50
        ENDIF
      ENDDO
      ENDDO

      deallocate ( tmp_forw,tmp_back,indx_forw,indx_back )
      deallocate ( tmp_clock,tmp_anclk,indx_clock,indx_anclk )
      deallocate ( cyclone )
      deallocate ( cyclone_indx, converg_indx )

      RETURN
      END SUBROUTINE BACK_cyclone_unfold
