!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
!     ==================================================================
!     PURPOSE:
!       Using referent radial velocity field to check if the raw radial
!       velocity needs to be unfold. The criterion is 0.5Nyqv. The raw
!       data through the criterion will be seeded.
!     ==================================================================
!
!     Author :  Jiandong Gong
!     Date   :
!     Action :  Created.
!
!     History:
!     --------
!     name   : Kang Nai
!     date   : Nov. 13, 2007
!     action : added a distance limit for the different tilt and adjust
!              the criterion.
!
!     ------------------------------------------------------------------
      SUBROUTINE REFERENCE_CHECK(wrkchek)

      USE Xu_variables

      IMPLICIT NONE

      real, dimension(1:nr,1:np) :: wrkchek
      integer :: ii,k,iii
      integer :: iend
      character(len=40) :: name1,name2
      real    :: limit_range,vrqcthrs

      k=k_tilt

      limit_range=300000.0
!     vrqcthrs=0.5*thet_nyq_vel(k)       ! test 1
      vrqcthrs=0.25*thet_nyq_vel(k)
      ii=int(thet(k)*100.0)
!     if ( ii<110 ) then
!          limit_range=30000.0
!     endif
!     if ( ii>110 .and. ii<300 ) then
!          limit_range=50000.0
!     endif
!     if ( vcpnum==21 .or. vcpnum==121 .or. vcpnum==221 ) then
!       if ( ii<110 ) then
!         limit_range=30000.0
!       endif
!       if ( ii>110 .and. ii<300 ) then
!         limit_range=50000.0
!       endif
!     endif
!     ------------------------------------------------------------------
!     get the start gate number and end gate number.
!     ------------------------------------------------------------------
      iend=0

      CALL dealias ( nr,np,obsvel,wrkvel,nbeam,iminrng,nrang         &
                    ,vadvel,vnyq,spval,index,ipoint                  &
                    ,wrkchek,iend,vrqcthrs )
 
      CALL vadtilt_check ( nr,np,wrkvel,nbeam,iminrng,nrang          &
                          ,vadvel,vnyq,spval,ran,elvng,index         &
                          ,wrkchek,iend,limit_range )

      END SUBROUTINE REFERENCE_CHECK
