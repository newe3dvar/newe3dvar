!
!     ##################################################################
!     ##################################################################
!     ######                                                      ######
!     ######        Radar Data Quality Control System.            ######
!     ######                                                      ######
!     ######              Copyright (c) 2009                      ######
!     ######             Cooperative Institute                    ######
!     ######       for Mesoscale Meteorological Studies           ######
!     ######    University of Oklahoma.  All rights reserved.     ######
!     ######                                                      ######
!     ##################################################################
!     ##################################################################
!
! ======================================================================
!     PURPOSE:
!       Body check to unfold the point which is the fold point.
! ======================================================================
!
!     Author  : Gong, Jiandong et al.
!     Date    :
!     Action  : Created.
!
!     History :
!     ---------
!     name    : Kang Nai and Qin Xu
!     date    : Sep. 20, 2007
!     action  : changed the simple unfold mode.
!
! ----------------------------------------------------------------------
      SUBROUTINE CONTINUE_CHECK_31(ibgn,iend)

      USE Xu_variables

      IMPLICIT NONE

      integer :: ibgn,iend
      integer :: i,j,k,ii
      character(len=80) :: name1,name2

      real,dimension(:),allocatable   :: tmp3

      allocate ( tmp3(nr) )
 
      k=k_tilt
      do i=1,nrang
        tmp3(i)=wrkhgt(i,k)
      enddo

      CALL unfold_simple_31( nr,np,ibgn,iend,nbeam,vnyq              &
                            ,index,wrkvel,obsvel,vadvel,unfvel       &
                            ,ivadflag,thet(k),tmp3,spval,k,ran )

      deallocate ( tmp3 )

      END SUBROUTINE CONTINUE_CHECK_31
