!#######################################################################
!#######################################################################
!#######                                                          ######
!#######               SUBROUTINE READ_88D_TILTS                  ######
!#######                                                          ######
!#######################################################################
!#######################################################################

SUBROUTINE read_88d_tilts(infile,volflag,radlat,radlon,radalt,ivcp,     &
            iiyear,iimon,iiday,iihr,iimin,iisec,                        &
            dualpproc,dualpdata,qc_on,unfdiag,rfropt,wrtsolo,wrtsoloqc, &
            maxelev,maxrgate,maxvgate,maxazim,itimfrst,                 &
            nzsnd,zsnd,usnd,vsnd,rfrsnd,                                &
            icount,ngateref, ngatevel,                                  &
            rfrst_ref, gtspc_ref, rfrst_vel, gtspc_vel,                 &
            kntrgat,kntrazm,kntrelv,                                    &
            timevolr,rngrvol,azmrvol,elvrvol,elvmnrvol,                 &
            refvol,rhvvol,zdrvol,kdpvol,                                &
            kntvgat,kntvazm,kntvelv,                                    &
            timevolv,rngvvol,azmvvol,elvvvol,elvmnvvol,                 &
            vnyqvol,velvol,                                             &
            misval, rtem, istatus)

  IMPLICIT NONE

!-----------------------------------------------------------------------
!
! PURPOSE: read radar observation tilt by tilt and do quality control
!          then output the total volume in 3D radar arrays
!
!-----------------------------------------------------------------------
!
  CHARACTER(LEN=256), INTENT(IN) :: infile
  INTEGER, INTENT(IN)  :: volflag  ! = 0, infile is a list of tilt files
                                   ! = 1, infile contains full volume data

  REAL,    INTENT(IN)  :: radalt, radlat, radlon
  INTEGER, INTENT(IN)  :: maxelev, itimfrst
  INTEGER, INTENT(IN)  :: maxrgate,maxvgate,maxazim
  LOGICAL, INTENT(IN)  :: dualpproc, qc_on, unfdiag, wrtsolo, wrtsoloqc
  INTEGER, INTENT(IN)  :: iiyear,iimon,iiday,iihr,iimin,iisec
  INTEGER, INTENT(IN)  :: rfropt, ivcp

  INTEGER, INTENT(IN)  :: nzsnd
  REAL,    INTENT(IN)  :: zsnd(nzsnd)             ! hgt levels for refractivity sounding
  REAL,    INTENT(IN)  :: usnd(nzsnd)
  REAL,    INTENT(IN)  :: vsnd(nzsnd)
  REAL,    INTENT(IN)  :: rfrsnd(nzsnd)           ! refractivity sounding

  INTEGER, INTENT(OUT) :: rfrst_ref, gtspc_ref, rfrst_vel, gtspc_vel
  INTEGER, INTENT(OUT) :: ngateref(maxelev), ngatevel(maxelev)
  INTEGER, INTENT(OUT) :: icount
  LOGICAL, INTENT(INOUT) :: dualpdata

  INTEGER, INTENT(OUT)   :: kntrgat(maxazim,maxelev)
  INTEGER, INTENT(OUT)   :: kntrazm(maxelev),kntrelv
  INTEGER, INTENT(OUT)   :: timevolr(maxazim,maxelev)
  REAL,    INTENT(OUT)   :: rngrvol(maxrgate,maxelev)
  REAL,    INTENT(OUT)   :: azmrvol(maxazim,maxelev)
  REAL,    INTENT(OUT)   :: elvrvol(maxazim,maxelev)
  REAL,    INTENT(OUT)   :: elvmnrvol(maxelev)
  REAL,    INTENT(OUT)   :: refvol(maxrgate,maxazim,maxelev)
  REAL,    INTENT(OUT)   :: rhvvol(maxrgate,maxazim,maxelev)
  REAL,    INTENT(OUT)   :: zdrvol(maxrgate,maxazim,maxelev)
  REAL,    INTENT(OUT)   :: kdpvol(maxrgate,maxazim,maxelev)

  INTEGER, INTENT(OUT)   :: kntvgat(maxazim,maxelev)
  INTEGER, INTENT(OUT)   :: kntvazm(maxelev)
  INTEGER, INTENT(OUT)   :: kntvelv
  REAL,    INTENT(OUT)   :: vnyqvol(maxazim,maxelev)
  REAL,    INTENT(OUT)   :: timevolv(maxazim,maxelev)
  REAL,    INTENT(OUT)   :: rngvvol(maxvgate,maxelev)
  REAL,    INTENT(OUT)   :: azmvvol(maxazim,maxelev)
  REAL,    INTENT(OUT)   :: elvvvol(maxazim,maxelev)
  REAL,    INTENT(OUT)   :: elvmnvvol(maxelev)
  REAL,    INTENT(OUT)   :: velvol(maxvgate,maxazim,maxelev)

  REAL,    INTENT(OUT)   :: rtem(MAX(maxrgate,maxvgate),maxazim)
  REAL,    INTENT(IN)    :: misval

  INTEGER, INTENT(OUT) :: istatus

!------------------------------------------------------------------------

  REAL,    ALLOCATABLE :: azim(:)    ! azimuth angle for each radial(degree)
  REAL,    ALLOCATABLE :: elev(:)    ! elevation angle for each radial (degree)
  REAL,    ALLOCATABLE :: vnyq(:)    ! Nyquist velocities (meters/sec)
  INTEGER, ALLOCATABLE :: time(:)    ! time offset from itimfrst

  REAL,    ALLOCATABLE :: refdata(:,:) ! refl (dBZ) data
  REAL,    ALLOCATABLE :: rhvdata(:,:) ! Rho-HV data
  REAL,    ALLOCATABLE :: zdrdata(:,:) ! Zdr data
  REAL,    ALLOCATABLE :: phidata(:,:) ! Phi data
  REAL,    ALLOCATABLE :: kdpdata(:,:) ! Kdp data
  REAL,    ALLOCATABLE :: veldata(:,:) ! velocity data (m/s)
  REAL,    ALLOCATABLE :: spwdata(:,:)
  REAL,    ALLOCATABLE :: refrange(:)   ! reflectivity range (km)

  REAL,    ALLOCATABLE :: unfvdata(:,:) ! unfolded velocity data (m/s)
  REAL,    ALLOCATABLE :: bkgvel(:,:)
  INTEGER, ALLOCATABLE :: bgate(:)
  INTEGER, ALLOCATABLE :: egate(:)

!------------------------------------------------------------------------

  INCLUDE 'remapcst.inc'

  INTEGER, PARAMETER :: stdout = 6

  INTEGER, PARAMETER :: bkgopt=1
  INTEGER, PARAMETER :: shropt=1
  REAL,    PARAMETER :: anrflag = -800.
  REAL,    PARAMETER :: phichek = -400.
  REAL,    PARAMETER :: missval = -999.
  REAL,    PARAMETER :: smallval = 1.0E-5

  REAL    :: radaltadj
  INTEGER :: dlpmedfilt
  REAL    :: vnyquist
  INTEGER :: timeset, nyqset
  INTEGER :: iscan,iangle,ktime
  INTEGER :: kyear,kmon,kday,khr,kmin,ksec
  CHARACTER (LEN=6)   :: varid
  CHARACTER (LEN=256) :: filename(maxelev)

  CHARACTER(LEN=20) :: pname
  INTEGER           :: plen

  INTEGER :: ng_ref,ng_vel, nazim

  INTEGER :: refstat,rhvstat,dlpstat,velstat,eofstat
  INTEGER :: itilt, i,j, igate,jazim
  INTEGER :: kfn,kkfn, kref, kvel
  REAL    :: rmin, rmax
  CHARACTER(LEN=9)   :: timestr
  INTEGER            :: ihr, imin, isec, iyear, imon, iday


!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  ALLOCATE(azim(maxazim),  stat=istatus)
  CALL check_alloc_status(istatus,'read_88d_tilts:azim')
  ALLOCATE(elev(maxazim),  stat=istatus)
  CALL check_alloc_status(istatus,'read_88d_tilts:elev')
  ALLOCATE(vnyq(maxazim),  stat=istatus)
  CALL check_alloc_status(istatus,'read_88d_tilts:vnyq')
  ALLOCATE(time(maxazim),  stat=istatus)
  CALL check_alloc_status(istatus,'read_88d_tilts:time')

  ALLOCATE(refdata(maxrgate,maxazim),stat=istatus)
  CALL check_alloc_status(istatus,'read_88d_tilts:refdata')
  ALLOCATE(refrange(maxrgate),stat=istatus)
  CALL check_alloc_status(istatus,'read_88d_tilts:refrange')
  ALLOCATE(veldata(maxvgate,maxazim),stat=istatus)
  CALL check_alloc_status(istatus,'read_88d_tilts:veldata')
  ALLOCATE(spwdata(maxvgate,maxazim),stat=istatus)
  CALL check_alloc_status(istatus,'read_88d_tilts:spwdata')
  ALLOCATE(rhvdata(maxrgate,maxazim),stat=istatus)
  CALL check_alloc_status(istatus,'read_88d_tilts:rhvdata')
  refdata  = missval
  refrange = missval
  veldata  = missval
  spwdata  = missval
  rhvdata  = missval  ! it is possible rhvstat = 1 but dualpproc = .FALSE.

  IF(dualpproc) THEN
    ALLOCATE(zdrdata(maxrgate,maxazim),stat=istatus)
    CALL check_alloc_status(istatus,'read_88d_tilts:zdrdata')
    ALLOCATE(phidata(maxrgate,maxazim),stat=istatus)
    CALL check_alloc_status(istatus,'read_88d_tilts:phidata')
    ALLOCATE(kdpdata(maxrgate,maxazim),stat=istatus)
    CALL check_alloc_status(istatus,'read_88d_tilts:kdpdata')
  ELSE
    ALLOCATE(zdrdata(1,1),stat=istatus)
    ALLOCATE(phidata(1,1),stat=istatus)
    ALLOCATE(kdpdata(1,1),stat=istatus)
  END IF
  zdrdata  = missval
  phidata  = missval
  kdpdata  = missval

  IF(qc_on) THEN
    ALLOCATE(unfvdata(maxvgate,maxazim),stat=istatus)
    CALL check_alloc_status(istatus,'read_88d_tilts:unfvdata')
    ALLOCATE(bkgvel(maxvgate,maxazim),stat=istatus)
    CALL check_alloc_status(istatus,'read_88d_tilts:bkgvel')
    ALLOCATE(bgate(maxazim),stat=istatus)
    CALL check_alloc_status(istatus,'read_88d_tilts:bgate')
    ALLOCATE(egate(maxazim),stat=istatus)
    CALL check_alloc_status(istatus,'read_88d_tilts:egate')
  END IF

!------------------------------------------------------------------------

  IF (volflag /= 1) THEN
    OPEN(45,FILE=TRIM(infile))
    DO  itilt = 1, maxelev
      READ(45,'(A)') filename(itilt)
    END DO
    CLOSE(45)
  END IF

  kref = 0
  kvel = 0

  dlpmedfilt = 0

  DO itilt=1,maxelev
    IF (volflag == 1) THEN
      WRITE(6,'(1x,a,I2,2a)') '=== Reading tilt - ',itilt,' of 88d radar data from: ', TRIM(infile)

      CALL rdtilt88d(maxrgate,maxvgate,maxazim,dualpproc,infile,        &
                   refvarname,rhvvarname,zdrvarname,kdpvarname,         &
                   velvarname,spwvarname,itimfrst,                      &
                   ng_ref,rfrst_ref,gtspc_ref,                          &
                   ng_vel,rfrst_vel,gtspc_vel,                          &
                   nazim,azim,elev,time,vnyq,                           &
                   refdata,rhvdata,zdrdata,phidata,veldata,spwdata,     &
                   refstat,rhvstat,dlpstat,velstat,eofstat,istatus)
    ELSE
      refstat = 0
      rhvstat = 0
      dlpstat = 0
      eofstat = 0
      ng_ref  = 0

      WRITE(6,'(1x,a,I2,2a)') '=== Reading tilt - ',itilt,' from: ', TRIM(filename(itilt))

      CALL read_nc4_radial(filename(itilt),maxvgate,maxazim,velvarname, &
                           itimfrst,ng_vel,rfrst_vel,gtspc_vel,         &
                           nazim,azim,elev,time,vnyq,                   &
                           veldata,velstat,missval,istatus)
    END IF
    IF(istatus /= 0) EXIT

!------------------------------------------------------------------------
!
!  Correct altitude, where necessary.
!  Correct true north, for example from TDWR data given in mag north.
!
!------------------------------------------------------------------------

    radaltadj=radalt+altoffset
    IF( ABS(northazim) > smallval ) THEN     ! /= 0.0
      DO j = 1, nazim
        azim(j) = azim(j)-northazim
        IF(azim(j) < 0.0) azim(j)=azim(j)+360.0
      END DO
    END IF
!
!------------------------------------------------------------------------
!
!   Update Nyquist Velocity with input values, if needed.
!
!------------------------------------------------------------------------
!
    IF( nfixed_nyqv > 0 ) THEN
      DO j=1, nazim
        kfn=0
        DO kkfn=nfixed_nyqv,1,-1
          IF( elev(j) <= elevlim_fnyqv(kkfn) ) kfn=kkfn
        END DO
        IF( kfn > 0 ) THEN
          IF(j==1)                                                      &
            WRITE(6,'(2(a,f6.2),a)') ' Resetting Nyquist at elev: ',    &
                        elev(j),' to ',fixed_nyqv(kfn),' m/s.'
          vnyq(j)=fixed_nyqv(kfn)
        END IF
      END DO
    END IF

!------------------------------------------------------------------------
!
!   Process reflectivity data.
!
!------------------------------------------------------------------------

    WRITE(*,'(1x,2(a,I0))') 'Back from rdtilt88d/read_nc4_radial, istatus = ',istatus,', eofstat = ',eofstat
    WRITE(*,'(1x,4(a,I2))') 'refstat = ',refstat,', velstat = ',velstat,', rhvstat = ',rhvstat,', dlpstat = ',dlpstat

    IF (dlpstat > 0) dualpdata = .TRUE.
    ngateref(itilt)=ng_ref
    ngatevel(itilt)=ng_vel

    IF( refstat == 1 ) THEN
      kref=kref+1
      icount = icount + 1
      WRITE(stdout,'(a,i2,a)')                                          &
      ' ----------------- ',icount,': base reflectivity -----------------'

      WRITE(*,'(1x,a,I6,a,F10.5)') 'nazim  = ',nazim,  ', elev(1)   = ',elev(1)
      WRITE(*,'(1x,2(a,I6))')      'ng_ref = ',ng_ref, ', gtspc_ref = ',gtspc_ref

      WRITE(stdout,*) 'Transferring ',nazim,' reflectivity radials.'

      WRITE(stdout,'(a)')                                               &
            '  =Ref=  j     dtime      azim      elev    refmin    refmax'
      DO j = 1, nazim, 20
        rmin=999.
        rmax=-999.
        DO i=1,ng_ref
          IF(refdata(i,j) > refchek) THEN
            rmin=min(refdata(i,j),rmin)
            rmax=max(refdata(i,j),rmax)
!            IF(refdata(i,j) > 40.0) THEN
!              WRITE(stdout,'(a,2i6,2f10.2)')' BIG-R i,j,azim,refl:',   &
!                    i,j,azim(j),refdata(i,j)
!            END IF
          END IF
        END DO

        CALL abss2ctim(itimfrst+time(j), iyear, imon, iday, ihr, imin, isec)
        WRITE(timestr,'(3(a,i2.2))') ' ',ihr,':',imin,':',isec

        WRITE(stdout,'(5x,i5,1x,a,4f10.2)') j,timestr,azim(j),elev(j),rmin,rmax
      END DO

      IF( qc_on) THEN
        write(6,'(1x,a)') 'Calling anomradial '
        CALL anomradial(maxrgate,maxazim,ng_ref,nazim,                  &
                    rfrst_ref,gtspc_ref,refchek,anrflag,azim,refdata)

        IF( rhvstat == 1 ) THEN
          write(6,'(1x,a)') 'Calling RhoHV screen'
          CALL rhohvchk(maxrgate,maxvgate,maxazim,ng_ref,ng_vel,nazim,  &
               gtspc_ref,gtspc_vel,refchek,velchek,rhohvthr,            &
               refdata,veldata,rhvdata)
        END IF

        write(6,'(1x,a)') 'Calling despekl '
        CALL despekl(maxrgate,maxazim,ng_ref,maxazim,refchek,medfilt,   &
                     refdata,rtem)
      END IF

      IF( dlpstat == 1 ) THEN
        print *, ' rfrst_ref: ',rfrst_ref,'  gtscpc_ref: ',gtspc_ref
        DO igate=1,ng_ref
          refrange(igate)=0.001*(rfrst_ref+(igate-1)*gtspc_ref)
        END DO

        WRITE(stdout,'(1x,a,i4)')                                       &
        ' ----- Processing base dual-pol variables in tilt: ',icount

        CALL phi2kdp(maxrgate,maxazim,ng_ref,nazim,                     &
                     refrange,refdata,rhvdata,phidata,kdpdata,rtem)
        WRITE(stdout,'(a)')                                             &
            '=Rho-HV= j     azim     elev   rhvmin   rhvmax'
        DO j = 1, nazim, 20
          rmin=999.
          rmax=-999.
          DO i=1,ng_ref
            IF(rhvdata(i,j) > rhvchek) THEN
              rmin=min(rhvdata(i,j),rmin)
              rmax=max(rhvdata(i,j),rmax)
            END IF
          END DO
          WRITE(stdout,'(5x,i5,2f9.1,2f9.4)')                           &
               j,azim(j),elev(j),rmin,rmax
        END DO
        IF( qc_on) THEN
          write(6, *) ' calling despekl '
          CALL despekl(maxrgate,maxazim,ng_ref,maxazim,rhvchek,         &
                       dlpmedfilt,rhvdata,rtem)
        END IF

        WRITE(stdout,'(a)')                                             &
            ' =Zdr=   j     azim     elev   Zdrmin   Zdrmax'
        DO j = 1, nazim, 20
          rmin=999.
          rmax=-999.
          DO i=1,ng_ref
            IF(zdrdata(i,j) > zdrchek) THEN
              rmin=min(zdrdata(i,j),rmin)
              rmax=max(zdrdata(i,j),rmax)
            END IF
          END DO
          WRITE(stdout,'(5x,i5,2f9.1,2f9.4)')                           &
               j,azim(j),elev(j),rmin,rmax
        END DO
        IF( qc_on) THEN
          write(6, *) ' calling despekl '
          CALL despekl(maxrgate,maxazim,ng_ref,maxazim,zdrchek,         &
                       dlpmedfilt,zdrdata,rtem)
        END IF

        WRITE(stdout,'(a)')                                             &
            '  =Phi= j     azim     elev   phimin   phimax'
        DO j = 1, nazim, 20
          rmin=999.
          rmax=-999.
          DO i=1,ng_ref
            IF(phidata(i,j) > phichek) THEN
              rmin=min(phidata(i,j),rmin)
              rmax=max(phidata(i,j),rmax)
            END IF
          END DO
          WRITE(stdout,'(5x,i5,2f9.1,2f9.4)')                           &
               j,azim(j),elev(j),rmin,rmax
        END DO

        WRITE(stdout,'(a)')                                             &
            '  =Kdp= j     azim     elev   kdpmin   kdpmax'
        DO j = 1, nazim, 20
          rmin=999.
          rmax=-999.
          DO i=1,ng_ref
            IF(kdpdata(i,j) > kdpchek) THEN
              rmin=min(kdpdata(i,j),rmin)
              rmax=max(kdpdata(i,j),rmax)
            END IF
          END DO
          WRITE(stdout,'(5x,i5,2f9.1,2f9.4)')                           &
               j,azim(j),elev(j),rmin,rmax
        END DO
        IF( qc_on) THEN
          write(6, *) ' calling despekl '
          CALL despekl(maxrgate,maxazim,ng_ref,maxazim,kdpchek,         &
                       dlpmedfilt,kdpdata,rtem)
        END IF

        write(6, *) ' calling volbuilddlp for dual-pol data'
        write(6, *) '   elev=',elev(1),' nazim =',nazim,' ng_ref=',ng_ref
        nyqset  = 0
        timeset = 1
        CALL volbuilddlp(maxrgate,maxazim,maxelev,ng_ref,nazim,         &
                nyqset,timeset,                                         &
                kntrgat,kntrazm,kntrelv,                                &
                gtspc_ref,rfrst_ref,refchek,                            &
                vnyq,time,                                              &
                azim,elev,refdata,rhvdata,zdrdata,kdpdata,              &
                vnyqvol,timevolr,                                       &
                rngrvol,azmrvol,elvrvol,elvmnrvol,                      &
                refvol,rhvvol,zdrvol,kdpvol,misval)

      ELSE  ! no dual-pol data processing

        write(6, '(1x,a)') 'calling volbuild reflectivity'
        !write(6, *) '   elev=',elev(1),' nazim =',nazim,' ng_ref=',ng_ref
        CALL volbuild(maxrgate,maxazim,maxelev,ng_ref,nazim,            &
                nyqset,timeset,                                         &
                kntrgat,kntrazm,kntrelv,                                &
                gtspc_ref,rfrst_ref,refchek,                            &
                vnyq,time,                                              &
                azim,elev,refdata,                                      &
                vnyqvol,timevolr,                                       &
                rngrvol,azmrvol,elvrvol,elvmnrvol,refvol,misval)

      END IF ! dlpstat
    END IF   ! refstat

!------------------------------------------------------------------------
!
!   Process velocity data.
!
!------------------------------------------------------------------------

    IF(velstat == 1) THEN
      kvel=kvel+1
      icount = icount + 1

      WRITE(stdout,'(a,i4,a)')                                          &
      ' ----------------- ',icount,': base velocity -----------------'

      WRITE(*,'(1x,a,I6,a,F10.5)') 'nazim  = ',nazim,  ', elev(1)   = ',elev(1)
      WRITE(*,'(1x,2(a,I6))')      'ng_vel = ',ng_vel, ', gtspc_vel = ',gtspc_vel

      WRITE(stdout,'(a,i6,a)')' Transferring',nazim,' velocity radials.'

      vnyquist=vnyq(1)
      WRITE(stdout,'(a,f10.2)') ' Nyquist velocity: ',vnyquist

      WRITE(stdout,'(a)')                                               &
          '  =Vel=   j     dtime      azim      elev      vr min    vr max'
      DO j = 1, nazim, 20
        rmin=999.
        rmax=-999.
        DO i=1,ng_vel
          IF(veldata(i,j) > velchek) THEN
            rmin=min(veldata(i,j),rmin)
            rmax=max(veldata(i,j),rmax)
          END IF
        END DO

        CALL abss2ctim(itimfrst+time(j), iyear, imon, iday, ihr, imin, isec)
        WRITE(timestr,'(3(a,i2.2))') ' ',ihr,':',imin,':',isec

        WRITE(stdout,'(6x,i5,1x,a,4f10.2)') j,timestr,azim(j),elev(j),rmin,rmax
      END DO

!------------------------------------------------------------------------
!
!   Call quality control and radar volume builder.
!
!------------------------------------------------------------------------

      nyqset=1
      timeset=1
      iangle=NINT(100.*elev(1))

      IF( qc_on ) THEN
        IF( unfdiag ) THEN
          varid='rdvela'
          WRITE(6,'(a,a6)') ' Calling wrtvel varid:',varid
          CALL wrtvel(iangle,itilt,varid,                               &
                      iiyear,iimon,iiday,iihr,iimin,iisec,              &
                      gtspc_vel,rfrst_vel,vnyquist,                     &
                      radname,radlat,radlon,radaltadj,                  &
                      maxvgate,maxazim,ng_vel,nazim,                    &
                      azim,elev,veldata)
        END IF

        WRITE(6,'(a)') ' Calling despekl '
        CALL despekl(maxvgate,maxazim,ng_vel,maxazim,velchek,medfilt,   &
                     veldata,rtem)

        IF( unfdiag ) THEN
          varid='rdvelb'
          WRITE(6,'(a,a6)') ' Calling wrtvel varid:',varid
          CALL wrtvel(iangle,itilt,varid,                               &
                      iiyear,iimon,iiday,iihr,iimin,iisec,              &
                      gtspc_vel,rfrst_vel,vnyquist,                     &
                      radname,radlat,radlon,radaltadj,                  &
                      maxvgate,maxazim,ng_vel,nazim,                    &
                      azim,elev,veldata)
        END IF

        WRITE(6,'(a)') ' Calling unfoldnqc'
        CALL unfoldnqc(maxvgate,maxazim,ng_vel,nazim,                   &
                       nzsnd,zsnd,usnd,vsnd,rfrsnd,                     &
                       bkgopt,shropt,rfropt,                            &
                       gtspc_vel,rfrst_vel,iangle,itilt,                &
                       iiyear,iimon,iiday,iihr,iimin,iisec,             &
                       radlat,radlon,radaltadj,                         &
                       veldata,spwdata,elev,azim,vnyq,                  &
                       unfvdata,bkgvel,bgate,egate,rtem)

        IF( unfdiag ) THEN
          varid='rdvelu'
          WRITE(6,'(a,a6)') ' Calling wrtvel varid:',varid
          CALL wrtvel(iangle,itilt,varid,                               &
                      iiyear,iimon,iiday,iihr,iimin,iisec,              &
                      gtspc_vel,rfrst_vel,vnyquist,                     &
                      radname,radlat,radlon,radaltadj,                  &
                      maxvgate,maxazim,ng_vel,nazim,                    &
                      azim,elev,unfvdata)
        END IF

        print *, ' calling volbuild velocity '
        print *, '    maxvgate=',maxvgate,'  ng_vel=',ng_vel
        print *, '    elv=',elev(1),' nazim =',nazim
        CALL volbuild(maxvgate,maxazim,maxelev,ng_vel,nazim,            &
                 nyqset,timeset,                                        &
                 kntvgat,kntvazm,kntvelv,                               &
                 gtspc_vel,rfrst_vel,velchek,                           &
                 vnyq,time,                                             &
                 azim,elev,unfvdata,                                    &
                 vnyqvol,timevolv,                                      &
                 rngvvol,azmvvol,elvvvol,elvmnvvol,velvol,misval)
      ELSE

        print *, ' calling volbuild velocity '
        print *, '    maxvgate=',maxvgate,'  ng_vel=',ng_vel
        print *, '    elv=',elev(1),' nazim =',nazim
        CALL volbuild(maxvgate,maxazim,maxelev,ng_vel,nazim,            &
                 nyqset,timeset,                                        &
                 kntvgat,kntvazm,kntvelv,                               &
                 gtspc_vel,rfrst_vel,velchek,                           &
                 vnyq,time,                                             &
                 azim,elev,veldata,                                     &
                 vnyqvol,timevolv,                                      &
                 rngvvol,azmvvol,elvvvol,elvmnvvol,velvol,misval)
      END IF

    END IF ! velstat

!------------------------------------------------------------------------
!
!  Write SOLO-II data
!
!------------------------------------------------------------------------
!
    IF( wrtsolo ) THEN
      WRITE(pname,'(a,I0)') 'SOLO, vcp=',ivcp
      plen = LEN_TRIM(pname)

      IF( refstat == 1 ) THEN
        iangle=NINT(100.*elev(1))
        ktime=itimfrst+time(1)
        CALL abss2ctim(ktime,kyear,kmon,kday,khr,kmin,ksec)
        CALL init_solo_f(pname,plen,radname,4,                          &
                 radlat,radlon,radaltadj,ivcp,                          &
                 kyear,kmon,kday,khr,kmin,ksec,                         &
                 iscan,kref,iangle,                                     &
                 maxrgate,maxvgate,maxazim,istatus)
        DO jazim=1,nazim
          ktime=itimfrst+time(jazim)
          CALL abss2ctim(ktime,kyear,kmon,kday,khr,kmin,ksec)
          CALL add_radial_to_sweep_f(kref,elev(jazim),azim(jazim),      &
               1,0,ng_ref,ng_vel,ng_vel,vnyq,                           &
               kyear,kmon,kday,khr,kmin,ksec,                           &
               rfrst_ref,gtspc_ref,rfrst_vel,gtspc_vel,                 &
               refdata(1,jazim),veldata(1,jazim),spwdata(1,jazim),      &
               istatus)
        END DO
        CALL write_sweep_f(istatus)
      END IF ! refstat

      IF( velstat == 1 ) THEN
        iangle=NINT(100.*elev(1))
        ktime=itimfrst+time(1)
        CALL abss2ctim(ktime,kyear,kmon,kday,khr,kmin,ksec)
        CALL init_solo_f(pname,plen,radname,4,                          &
                 radlat,radlon,radaltadj,ivcp,                          &
                 kyear,kmon,kday,khr,kmin,ksec,                         &
                 iscan,kvel,iangle,                                     &
                 maxrgate,maxvgate,maxazim,istatus)
        DO jazim=1,nazim
          ktime=itimfrst+time(jazim)
          CALL abss2ctim(ktime,kyear,kmon,kday,khr,kmin,ksec)
          CALL add_radial_to_sweep_f(kvel,elev(jazim),azim(jazim),      &
               0,1,ng_ref,ng_vel,ng_vel,vnyq,                           &
               kyear,kmon,kday,khr,kmin,ksec,                           &
               rfrst_ref,gtspc_ref,rfrst_vel,gtspc_vel,                 &
               refdata(1,jazim),veldata(1,jazim),spwdata(1,jazim),      &
               istatus)
        END DO
        CALL write_sweep_f(istatus)
      END IF ! refstat
    END IF ! wrtsolo

    IF( wrtsoloqc ) THEN  ! write "unfvdata" instead of "veldata"
      WRITE(pname,'(a,I0)') 'SOLOQC, vcp=',ivcp
      plen = LEN_TRIM(pname)

      IF( refstat == 1 ) THEN
        iangle=NINT(100.*elev(1))
        ktime=itimfrst+time(1)
        CALL abss2ctim(ktime,kyear,kmon,kday,khr,kmin,ksec)
        CALL init_solo_f(pname,plen,radname,4,                          &
                 radlat,radlon,radaltadj,ivcp,                          &
                 kyear,kmon,kday,khr,kmin,ksec,                         &
                 iscan,kref,iangle,                                     &
                 maxrgate,maxvgate,maxazim,istatus)
        DO jazim=1,nazim
          ktime=itimfrst+time(jazim)
          CALL abss2ctim(ktime,kyear,kmon,kday,khr,kmin,ksec)
          CALL add_radial_to_sweep_f(kref,elev(jazim),azim(jazim),      &
               1,0,ng_ref,ng_vel,ng_vel,vnyq,                           &
               kyear,kmon,kday,khr,kmin,ksec,                           &
               rfrst_ref,gtspc_ref,rfrst_vel,gtspc_vel,                 &
               refdata(1,jazim),veldata(1,jazim),spwdata(1,jazim),      &
               istatus)
        END DO
        CALL write_sweep_f(istatus)
      END IF ! refstat

      IF( velstat == 1 ) THEN
        iangle=NINT(100.*elev(1))
        ktime=itimfrst+time(1)
        CALL abss2ctim(ktime,kyear,kmon,kday,khr,kmin,ksec)
        CALL init_solo_f(pname,plen,radname,4,                          &
                 radlat,radlon,radaltadj,ivcp,                          &
                 kyear,kmon,kday,khr,kmin,ksec,                         &
                 iscan,kvel,iangle,                                     &
                 maxrgate,maxvgate,maxazim,istatus)
        DO jazim=1,nazim
          ktime=itimfrst+time(jazim)
          CALL abss2ctim(ktime,kyear,kmon,kday,khr,kmin,ksec)
          CALL add_radial_to_sweep_f(kvel,elev(jazim),azim(jazim),      &
               0,1,ng_ref,ng_vel,ng_vel,vnyq,                           &
               kyear,kmon,kday,khr,kmin,ksec,                           &
               rfrst_ref,gtspc_ref,rfrst_vel,gtspc_vel,                 &
               refdata(1,jazim),unfvdata(1,jazim),spwdata(1,jazim),     &
               istatus)
        END DO
        CALL write_sweep_f(istatus)
      END IF ! refstat
    END IF ! wrtsoloqc

    IF(eofstat == 1) EXIT

  END DO  ! Tilt loop

!------------------------------------------------------------------------
!
!  Deallocate working arrays before return
!
!------------------------------------------------------------------------
!
  DEALLOCATE(azim,       stat=istatus)
  DEALLOCATE(elev,       stat=istatus)
  DEALLOCATE(vnyq,       stat=istatus)
  DEALLOCATE(time,       stat=istatus)

  DEALLOCATE(refdata,    stat=istatus)
  DEALLOCATE(refrange,   stat=istatus)
  DEALLOCATE(veldata,    stat=istatus)
  DEALLOCATE(spwdata,    stat=istatus)
  DEALLOCATE(rhvdata,    stat=istatus)

  IF ( ALLOCATED(zdrdata) ) THEN
    DEALLOCATE(zdrdata,  stat=istatus)
    DEALLOCATE(phidata,  stat=istatus)
    DEALLOCATE(kdpdata,  stat=istatus)
  END IF

  IF( ALLOCATED(unfvdata) ) THEN
    DEALLOCATE(unfvdata, stat=istatus)
    DEALLOCATE(bkgvel,   stat=istatus)
    DEALLOCATE(bgate,    stat=istatus)
    DEALLOCATE(egate,    stat=istatus)
  END IF

  RETURN
END SUBROUTINE read_88d_tilts

!#######################################################################
!#######################################################################
!#######                                                          ######
!#######               SUBROUTINE READ_NC4_RADIAL                 ######
!#######                                                          ######
!#######################################################################
!#######################################################################

SUBROUTINE read_nc4_radial(filename,maxvgate,maxazim,velvarname,        &
                   itimfrst,ng_vel,rfrst_vel,gtspc_vel,                 &
                   nazim,azim,elev,time,vnyq,                           &
                   veldata,velstat,misval,istatus)

  USE model_precision

  IMPLICIT NONE

  CHARACTER(LEN=256), INTENT(IN) :: filename
  CHARACTER(LEN=80 ), INTENT(IN) :: velvarname

  INTEGER, INTENT(IN)  :: maxvgate,maxazim
  INTEGER, INTENT(IN)  :: itimfrst
  INTEGER, INTENT(OUT) :: ng_vel, nazim
  INTEGER, INTENT(OUT) :: rfrst_vel, gtspc_vel

  REAL,    INTENT(OUT) :: azim(maxazim),elev(maxazim),vnyq(maxazim)
  INTEGER, INTENT(OUT) :: time(maxazim)
  REAL,    INTENT(OUT) :: veldata(maxvgate,maxazim)
  INTEGER, INTENT(OUT) :: velstat

  REAL,    INTENT(IN)  :: misval
  INTEGER, INTENT(OUT) :: istatus

!------------------------------------------------------------------------

  INTEGER :: i,j,k,n,n0, nn
  !INCLUDE 'netcdf.inc'

  INTEGER, PARAMETER :: abstimoffset = 315619200    ! this system starts from 1960 01 01

  INTEGER :: ncid
  INTEGER :: azimin, gatein
  REAL(SP):: elevation, rfrst_gate
  INTEGER :: timein, abstsec
  REAL(SP):: timefract

  REAL(SP), ALLOCATABLE :: veldatain(:,:)
  REAL(SP), ALLOCATABLE :: gatewidth(:)
  INTEGER :: pixelen
  INTEGER,  ALLOCATABLE :: pixel_x(:), pixel_y(:),pixel_count(:)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  ng_vel = 0
  rfrst_vel = -999
  gtspc_vel = -999
  azim      = misval
  elev      = misval
  time      = misval
  vnyq      = misval
  veldata   = misval
  velstat   = -1

  CALL netopen(filename,'R',ncid)

  CALL netreaddim(ncid, 'Azimuth', azimin,istatus)
  CALL netreaddim(ncid, 'Gate',    gatein,istatus)
  IF (azimin > maxazim .OR. gatein > maxvgate) THEN
    WRITE(*,'(1x,a)') 'ERROR: array dimension sizes are not large enough.'
    istatus = -1
    RETURN
  END IF
  ng_vel = gatein
  nazim  = azimin

  CALL netreadattr(ncid,'RangeToFirstGate',rfrst_gate,istatus)
  rfrst_vel = INT(rfrst_gate)

  CALL netreadatti(ncid,'Time',            timein,   istatus)
  !CALL netreadattr(ncid,'FractionalTime',timefract,istatus)
  abstsec = timein + abstimoffset
  !write(0,*) itimfrst, 'abstsec = ', abstsec
  DO j = 1, nazim
    time(j) = abstsec - itimfrst
  END DO

  CALL netreadattr(ncid,'Elevation',      elevation, istatus)
  DO j = 1, nazim
    elev(j) = elevation
  END DO

  CALL netread1d(ncid,0,0,'Azimuth',        azimin,azim)
  CALL netread1d(ncid,0,0,'NyquistVelocity',azimin,vnyq)

  ALLOCATE(gatewidth(azimin),        STAT = istatus)

  CALL netread1d(ncid,0,0,'GateWidth',      azimin,gatewidth)
  gtspc_vel = INT(gatewidth(1))

  CALL netreaddim(ncid, 'pixel', pixelen,istatus)
  IF (istatus /= 0) THEN  ! may be new netCDF 4 formate
    ALLOCATE(veldatain(gatein,azimin), STAT = istatus)
    CALL netread2d(ncid,0,0,'AliasedVelocityDPQC',gatein,azimin,veldatain)

    DO j = 1, azimin
      DO i = 1, gatein
        veldata(i,j) = veldatain(i,j)
      END DO
    END DO

    DEALLOCATE(veldatain)

  ELSE

    IF (pixelen > 0) THEN

      ALLOCATE(pixel_x(pixelen),     STAT = istatus)
      ALLOCATE(pixel_y(pixelen),     STAT = istatus)
      ALLOCATE(pixel_count(pixelen), STAT = istatus)
      ALLOCATE(veldatain(pixelen,1), STAT = istatus)

      CALL netread1di(ncid,0,0,'pixel_x',    pixelen,pixel_x)
      CALL netread1di(ncid,0,0,'pixel_y',    pixelen,pixel_y)
      CALL netread1di(ncid,0,0,'pixel_count',pixelen,pixel_count)

      CALL netread1d (ncid,0,0,'AliasedVelocityDPQC',pixelen, veldatain)

      DO n = 1, pixelen
        n0 = pixel_x(n)*gatein + pixel_y(n)
        DO k = 0, pixel_count(n)-1
          nn = n0 + k
          j = nn/gatein + 1
          i = mod(nn,gatein) + 1
          !WRITE(0,'(1x,7I5)') n, pixel_x(n),j,pixel_y(n),i,k,pixel_count(n)
          !IF (i < 1 .OR. i > gatein) THEN
          !  WRITE(0,*) 'ERROR: index i = ',i,' > ',gatein,' in read_nc4_radial.'
          !  istatus = -1
          !END IF
          !IF (j < 1 .OR. j > azimin) THEN
          !  WRITE(0,*) 'ERROR: index j = ',j,' > ',azimin,' in read_nc4_radial.'
          !  istatus = -2
          !END IF
          veldata(i,j) = veldatain(n,1)
        END DO
      END DO
    !ELSE    ! use misval above
    !CALL arpsstop('',0)
      DEALLOCATE(pixel_x,pixel_y,pixel_count)
      DEALLOCATE(veldatain)
    END IF
  END IF

  CALL netclose(ncid)

  velstat = 1

  RETURN
END SUBROUTINE read_nc4_radial

!#######################################################################
!#######################################################################
!#######                                                          ######
!#######               SUBROUTINE READ_NC4_RADIAL                 ######
!#######                                                          ######
!#######################################################################
!#######################################################################

SUBROUTINE get_time_from_file_name(infilename,volflag,radname,          &
                            kmyear,kmmon,kmday,kmhr,kmmin,kmsec,istatus)

  IMPLICIT NONE

  CHARACTER(LEN=256), INTENT(IN) :: infilename
  CHARACTER(LEN=4),   INTENT(IN) :: radname
  INTEGER, INTENT(IN)  :: volflag

  INTEGER, INTENT(OUT) :: kmyear
  INTEGER, INTENT(OUT) :: kmmon
  INTEGER, INTENT(OUT) :: kmday
  INTEGER, INTENT(OUT) :: kmhr
  INTEGER, INTENT(OUT) :: kmmin
  INTEGER, INTENT(OUT) :: kmsec
  INTEGER, INTENT(OUT) :: istatus

!------------------------------------------------------------------------

  CHARACTER(LEN=256) :: radfname
  CHARACTER(LEN=4)   :: filehead

  INTEGER :: lenfn,iloc,jloc,kloc
  LOGICAL :: back = .TRUE.


!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
! Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  IF (volflag == 1) THEN
    radfname = infilename
    filehead = radname
  ELSE
    OPEN(45,FILE=TRIM(infilename))
    READ(45,'(A)') radfname
    CLOSE(45)
    filehead = ''
  END IF

  lenfn=LEN_TRIM(radfname)
  iloc=index(radfname(1:lenfn),'/',back)
  IF (filehead == '') THEN
    jloc = iloc
    kloc = 1
  ELSE
    kloc = index(radfname((iloc+1):lenfn),filehead)
    jloc=iloc+kloc+3
  END IF
  IF(kloc > 0) THEN
    WRITE(6,'(3a)') ' Getting time from filename substring: "',         &
                   radfname((jloc+1):(jloc+15)),'"'
    IF(radfname((jloc+9):(jloc+9)) == '_' .OR.                        &
       radfname((jloc+9):(jloc+9)) == '-') THEN
      READ(radfname((jloc+1):(jloc+15)),'(i4,2i2,1x,3i2)')              &
            kmyear,kmmon,kmday,kmhr,kmmin,kmsec
    ELSE
      READ(radfname((jloc+1):(jloc+12)),'(i4,4i2)')                     &
            kmyear,kmmon,kmday,kmhr,kmmin
      kmsec=0
    END IF
    WRITE(*,'(1x,a,6I4)') 'Filename time variables: ',kmyear,kmmon,kmday,kmhr,kmmin,kmsec
  ELSE
    istatus = 1
  END IF

  RETURN
END SUBROUTINE get_time_from_file_name
