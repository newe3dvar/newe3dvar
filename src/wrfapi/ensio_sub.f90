!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DTAREAD4WRF                ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE dtaread4wrf(nx,ny,nz,nscalar,qnames,file_name, numdigit,userho,&
                       u ,v ,w ,pt, pp, qv, qscalar, rhobar, zpm,istatus )

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Coordinate the reading of WRF history data of various formats.
!  Composed following the subroutine dtaread
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Shizhang Wang
!    2/28/2011.
!
!  MODIFICATION HISTORY:
!
!  10/24/2012 (Y. Wang)
!  Added MPI capability and improved the interface for handling multiple
!  possible situations.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx,ny,nz The dimension of data arrays
!
!  DATA ARRAYS READ IN:
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!
!    uprt     x component of perturbation velocity (m/s)
!    vprt     y component of perturbation velocity (m/s)
!    wprt     vertical component of perturbation velocity
!             in Cartesian coordinates (m/s).
!
!    ptprt    perturbation potential temperature (K)
!    pprt     perturbation pressure (Pascal)
!    qvprt    perturbation water vapor mixing ratio (kg/kg)
!
!    qc       Cloud water mixing ratio (kg/kg)
!    qr       Rainwater mixing ratio (kg/kg)
!    qi       Cloud ice mixing ratio (kg/kg)
!    qs       Snow mixing ratio (kg/kg)
!    qh       Hail mixing ratio (kg/kg)
!
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhobar   Base state air density (kg/m**3)
!    qvbar    Base state water vapor mixing ratio (kg/kg)
!
!  OUTPUT:
!
!    time     The time of the input data (s)
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!
!    uprt     x component of perturbation velocity (m/s)
!    vprt     y component of perturbation velocity (m/s)
!    wprt     vertical component of perturbation velocity in
!             Cartesian coordinates (m/s).
!
!    ptprt    perturbation potential temperature (K)
!    pprt     perturbation pressure (Pascal)
!
!    qvprt    perturbation water vapor mixing ratio (kg/kg)
!    qc       Cloud water mixing ratio (kg/kg)
!    qr       Rainwater mixing ratio (kg/kg)
!    qi       Cloud ice mixing ratio (kg/kg)
!    qs       Snow mixing ratio (kg/kg)
!    qh       Hail mixing ratio (kg/kg)
!
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhobar   Base state air density (kg/m**3)
!    qvbar    Base state water vapor mixing ratio (kg/kg)
!
!    ireturn  Return status indicator
!
!-----------------------------------------------------------------------

  USE model_precision

  IMPLICIT NONE

!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!  Z is not initialized (?) - WYH
!-----------------------------------------------------------------------
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
  INTEGER :: nscalar

  CHARACTER(LEN=256) :: file_name

! REAL :: x     (nx)           ! x-coord. of the physical and compu
                               ! -tational grid. Defined at u-point(m).
! REAL :: y     (ny)           ! y-coord. of the physical and compu
                               ! -tational grid. Defined at v-point(m).
! REAL :: z     (nz)           ! z-coord. of the computational grid.
                               ! Defined at w-point on the staggered
                               ! grid(m).

  REAL, INTENT(OUT) :: u (nx,ny,nz)         ! x component of perturbation velocity
                               ! (m/s)
  REAL, INTENT(OUT) :: v (nx,ny,nz)         ! y component of perturbation velocity
                               ! (m/s)
  REAL, INTENT(OUT) :: w (nx,ny,nz)         ! vertical component of perturbation
                               ! velocity in Cartesian coordinates
                               ! (m/s).
  REAL, INTENT(OUT) :: pt (nx,ny,nz)        ! perturbation potential temperature (K)
  REAL, INTENT(OUT) :: pp (nx,ny,nz)        ! perturbation pressure (Pascal)

  REAL, INTENT(OUT) :: qv(nx,ny,nz)         ! perturbation water vapor mixing ratio
                               ! (kg/kg)
  REAL, INTENT(OUT) :: qscalar(nx,ny,nz,nscalar)
  REAL, INTENT(OUT) :: rhobar(nx,ny,nz)
  REAL, INTENT(OUT) :: zpm(nx,ny,nz)

  CHARACTER(LEN=*), INTENT(IN) :: qnames(nscalar)
  INTEGER,          INTENT(IN) :: numdigit
  LOGICAL,          INTENT(IN) :: userho

  INTEGER :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'mp.inc'

  REAL, PARAMETER :: gravity = 9.81

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INTEGER :: nx_wrf, ny_wrf, nz_wrf
  INTEGER :: nxlg_wrf, nylg_wrf, nxd_wrf, nyd_wrf

  INTEGER :: ncmprx, ncmpry
  INTEGER :: i,j,k,nq

  LOGICAL :: multifile, patch_split

  CHARACTER(LEN=40)  :: qnames_wrf
  CHARACTER(LEN=256) :: message
  CHARACTER(LEN=19 ) :: datestr
  INTEGER            :: itime, io_form

  INTEGER, ALLOCATABLE :: fHndl(:,:)
  REAL(SP), ALLOCATABLE :: temd(:,:,:), var_wrf1d(:)
  REAL(P),  ALLOCATABLE :: var_wrf(:,:,:)
  REAL(P),  ALLOCATABLE :: qv_wrf(:,:,:), pt_wrf(:,:,:)
  REAL(P),  ALLOCATABLE :: pb_wrf(:,:,:), rb_wrf(:,:,:)
  REAL(P),  ALLOCATABLE :: mub_wrf(:,:), mu_wrf(:,:)
  REAL(P),  ALLOCATABLE :: znu_wrf(:), rdnw_wrf(:)
  REAL(P),  ALLOCATABLE :: ph_wrf(:,:,:)

  REAL(SP) :: ptopin
  REAL(P)  :: p_top

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

!-----------------------------------------------------------------------
!
! This subroutine handles 6 possible readings
!
! 1. One WRF file in no-mpi mode (just read);                         - multifile = .FALSE., patch_split = .FALSE.
! 2. One WRF file in mpi mode (split);                                - multifile = .FALSE., patch_split = .TRUE.
!
! 3. Split WRF files in no-mpi mode (join);                           + multifile = .TRUE., patch_split = .FALSE.
! 4. Split WRF files with same number of processes (just read);       + multifile = .TRUE., patch_split = .FALSE.
! 5. Split WRF files with less number of processes (read and join);   + multifile = .TRUE., patch_split = .FALSE.
! 6. Split WRF files with more number of processes (read and split);  + multifile = .TRUE., patch_split = .TRUE.
!
!-----------------------------------------------------------------------

  IF (nproc_x_in == 1 .AND. nproc_y_in == 1) THEN
    multifile = .FALSE.
    ncmprx = 1
    ncmprx = 1
  ELSE
    multifile = .TRUE.
  END IF

  IF (nproc_x_in >= nproc_x .AND. nproc_y_in >= nproc_y) THEN
    patch_split = .FALSE.
    IF (mod(nproc_x_in,nproc_x) /= 0 .OR. mod(nproc_y_in,nproc_y) /= 0) THEN
      message = 'ERROR: nproc_x_in (nproc_y_in) must be a multiple of nproc_x (nproc_y).'
      istatus = -1
    ELSE
      ncmprx = nproc_x_in / nproc_x
      ncmpry = nproc_y_in / nproc_y
    END IF
  ELSE IF (nproc_x_in < nproc_x .AND. nproc_y_in < nproc_y) THEN
    patch_split = .TRUE.
    IF (mod(nproc_x,nproc_x_in) /= 0 .OR. mod(nproc_y,nproc_y_in) /= 0) THEN
      message = 'ERROR: nproc_x_in (nproc_y_in) must be a factor of nproc_x (nproc_y).'
      istatus = -2
    ELSE
      ncmprx = nproc_x / nproc_x_in
      ncmpry = nproc_y / nproc_y_in
    END IF
  ELSE
    message = 'ERROR: unsupported combination of nproc_x_in and nproc_x, or nproc_y_in and nproc_y.'
    istatus = -3
  END IF

  9999 CONTINUE

  IF (istatus /= 0) THEN
    WRITE(*,'(1x,a)')       TRIM(message)
    WRITE(*,'(7x,2(a,I4))') ' nproc_x_in = ',nproc_x_in, ', nproc_x = ', nproc_x
    WRITE(*,'(7x,2(a,I4))') ' nproc_y_in = ',nproc_y_in, ', nproc_y = ', nproc_y
    istatus = -1
    !RETURN
    CALL arpsstop('ERROR mpi parameters',1)
  END IF

!-----------------------------------------------------------------------
!
! Dimension maps
!
!-----------------------------------------------------------------------
  nx_wrf = nx-2       ! staggered dimensions
  ny_wrf = ny-2
  nz_wrf = nz-2

  nxlg_wrf = (nx_wrf-1)*nproc_x + 1     ! WRF domain indices
  nylg_wrf = (ny_wrf-1)*nproc_y + 1

  IF (multifile) THEN
    nxd_wrf = (nxlg_wrf-1) / nproc_x_in + 1
    nyd_wrf = (nylg_wrf-1) / nproc_y_in + 1
  ELSE
    nxd_wrf = nxlg_wrf    ! dataset size in the file, staggered
    nyd_wrf = nylg_wrf
  END IF

  IF (patch_split) THEN
    !
    ! It is required that (nxd_wrf-1) is dividable with ncmprx
    !
    IF (MOD(nxd_wrf-1,ncmprx) /= 0 .OR. MOD(nyd_wrf-1,ncmpry) /= 0) THEN
      istatus = -4
      message = 'ERROR: dimension sizes does not fit with the number of processes.'
      GOTO 9999
    END IF

  END IF

  ALLOCATE(fHndl(ncmprx,ncmpry), STAT = istatus)

  ALLOCATE(var_wrf (nx_wrf,ny_wrf,nz_wrf),   STAT = istatus)
  ALLOCATE(temd(nxd_wrf,nyd_wrf,nz_wrf),     STAT = istatus)
  ALLOCATE(var_wrf1d (nz_wrf),               STAT = istatus)

  ALLOCATE(qv_wrf(nx_wrf,ny_wrf,nz_wrf),     STAT = istatus)
  ALLOCATE(pt_wrf(nx_wrf,ny_wrf,nz_wrf),     STAT = istatus)
  ALLOCATE(pb_wrf(nx_wrf,ny_wrf,nz_wrf),     STAT = istatus)
  ALLOCATE(ph_wrf(nx_wrf,ny_wrf,nz_wrf),     STAT = istatus)

  var_wrf   = 0.0
  temd      = 0.0
  var_wrf1d = 0.0

  qv_wrf    = 0.0
  pt_wrf    = 0.0
  pb_wrf    = 0.0
  ph_wrf    = 0.0

  io_form  = 7  ! hard-coded for 7 at present
  CALL open_wrf_files(file_name,io_form,patch_split,multifile,1,        &
                     ncmprx,ncmpry,numdigit,fHndl)

!-----------------------------------------------------------------------
! Initialize data array
!-----------------------------------------------------------------------

! variables used for analysis
  u  = 0.0
  v  = 0.0
  w  = 0.0
  pt = 0.0
  pp = 0.0
  qv = 0.0

  qscalar = 0.0

  datestr = '2012-10-25_16:22:00'
  itime   = 1

!-----------------------------------------------------------------------
!
!  Horizontal wind
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'U','X',                                &
                  nx_wrf,ny_wrf,nz_wrf,var_wrf,                         &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'X',var_wrf,                 &
                      nx,ny,nz,u,temd,istatus)

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'V','Y',                                &
                  nx_wrf,ny_wrf,nz_wrf,var_wrf,                         &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'Y',var_wrf,                 &
                      nx,ny,nz,v,temd,istatus)

!-----------------------------------------------------------------------
!
!  Vertical wind
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'W','Z',                                &
                  nx_wrf,ny_wrf,nz_wrf,var_wrf,                         &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'Z',var_wrf,                 &
                      nx,ny,nz,w,temd,istatus)


!-----------------------------------------------------------------------
!
! Read potential temperature
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'T','0',                                &
                  nx_wrf,ny_wrf,nz_wrf,pt_wrf,                          &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)
! var_wrf(:,:,:) = pt_wrf(:,:,:) + 300.0
!
! CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',var_wrf,                 &
!                     nx,ny,nz,pt,temd,istatus)

! ptbar = 0.0

!-----------------------------------------------------------------------
!
! Pressure
!
!-----------------------------------------------------------------------

   CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                   datestr,itime,'P','0',                                &
                   nx_wrf,ny_wrf,nz_wrf,var_wrf,                         &
                   nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

   CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                   datestr,itime,'PB','0',                               &
                   nx_wrf,ny_wrf,nz_wrf,pb_wrf,                          &
                   nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

   var_wrf(:,:,:)=var_wrf(:,:,:)+pb_wrf(:,:,:)
   CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',var_wrf,                 &
                       nx,ny,nz,pp,temd,istatus)
!  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',pb_wrf,                  &
!                      nx,ny,nz,pbar,temd,istatus)

!-----------------------------------------------------------------------
!
! Read water vapor mixing ratio and Convert to specific humidity
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'QVAPOR','0',                           &
                  nx_wrf,ny_wrf,nz_wrf,qv_wrf,                          &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

!-----------------------------------------------------------------------
!
! Base state of air density
!
!-----------------------------------------------------------------------

  IF (userho) THEN

    ALLOCATE(rb_wrf(nx_wrf,ny_wrf,nz_wrf),     STAT = istatus)
    ALLOCATE(mub_wrf (nx_wrf,ny_wrf),          STAT = istatus)
    ALLOCATE(mu_wrf  (nx_wrf,ny_wrf),          STAT = istatus)
    ALLOCATE(znu_wrf (nz_wrf-1),               STAT = istatus)
    ALLOCATE(rdnw_wrf(nz_wrf-1),               STAT = istatus)

    rb_wrf    = 0.0
    mub_wrf   = 0.0
    mu_wrf    = 0.0
    znu_wrf   = 0.0
    rdnw_wrf  = 0.0

    CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,  &
                    datestr,itime,'MUB','0',                            &
                    nx_wrf,ny_wrf,mub_wrf,                              &
                    nxd_wrf,nyd_wrf,temd,istatus)

    CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,  &
                    datestr,itime,'MU','0',                             &
                    nx_wrf,ny_wrf,mu_wrf,                               &
                    nxd_wrf,nyd_wrf,temd,istatus)

    CALL get_wrf_1d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,  &
                    datestr,itime,'RDNW',                               &
                    nz_wrf,var_wrf1d,nz_wrf-1,istatus)
    DO k = 1, nz_wrf-1
      rdnw_wrf(k) = var_wrf1d(k)
    END DO

    CALL get_wrf_s(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,   &
                    datestr,itime,'P_TOP',ptopin,istatus)
    p_top = ptopin

    CALL get_wrf_1d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,  &
                    datestr,itime,'ZNU',                                &
                    nz_wrf,var_wrf1d,nz_wrf-1,istatus)
    DO k = 1, nz_wrf-1
      znu_wrf(k) = var_wrf1d(k)
    END DO

    CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,  &
                    datestr,itime,'PH','Z',                             &
                    nx_wrf,ny_wrf,nz_wrf,ph_wrf,                        &
                    nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

    CALL cal_rho_p(nx_wrf,ny_wrf,nz_wrf,mub_wrf,mu_wrf,rdnw_wrf,        &
                   p_top,znu_wrf,qv_wrf,ph_wrf,pt_wrf,                  &
                   rb_wrf,istatus)

    CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',rb_wrf,                &
                        nx,ny,nz,rhobar,temd,istatus)

    DEALLOCATE ( rb_wrf )
    DEALLOCATE ( mub_wrf, mu_wrf, znu_wrf, rdnw_wrf )

  ELSE
    rhobar(:,:,:) = 0.0
  END IF

!-----------------------------------------------------------------------
!
! Specific humidity/Potential temperature Conversion
!
!-----------------------------------------------------------------------

  var_wrf(:,:,:) = pt_wrf(:,:,:) + 300.0

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',var_wrf,                 &
                      nx,ny,nz,pt,temd,istatus)

  var_wrf(:,:,:) = qv_wrf(:,:,:)/(1+qv_wrf(:,:,:))

  WHERE(var_wrf < 0.0) var_wrf = 0.0
  WHERE(var_wrf > 1.0) var_wrf = 1.0

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',var_wrf,                 &
                      nx,ny,nz,qv,temd,istatus)

!-----------------------------------------------------------------------
!
! Vertical coordinates variables
!
!-----------------------------------------------------------------------

   CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,   &
                   datestr,itime,'PH','Z',                              &
                   nx_wrf,ny_wrf,nz_wrf,ph_wrf,                         &
                   nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

   CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,   &
                   datestr,itime,'PHB','Z',                             &
                   nx_wrf,ny_wrf,nz_wrf,var_wrf,                        &
                   nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

   ph_wrf(:,:,:)=(var_wrf(:,:,:)+ph_wrf(:,:,:))/gravity

   CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'V',ph_wrf,                 &
                       nx,ny,nz,zpm,temd,istatus)

!-----------------------------------------------------------------------
!
! hydrometeors
!
!-----------------------------------------------------------------------

! DO nq = 1, nscalar
!   CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,  &
!                   datestr,itime,qnames(nq),'0',                       &
!                   nx_wrf,ny_wrf,nz_wrf,var_wrf,                       &
!                   nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)
!
!   WHERE(var_wrf < 0.0) var_wrf = 0.0
!   WHERE(var_wrf > 1.0) var_wrf = 1.0
!
!   CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',var_wrf,               &
!                       nx,ny,nz,qscalar(:,:,:,nq),temd,istatus)
! END DO

  DO nq = 1, nscalar

    SELECT CASE ( TRIM(qnames(nq)) )
    CASE ('qc')
      qnames_wrf = 'QCLOUD'
    CASE ('qr')
      qnames_wrf = 'QRAIN'
    CASE ('qi')
      qnames_wrf = 'QICE'
    CASE ('qs')
      qnames_wrf = 'QSNOW'
    CASE ('qg')
      qnames_wrf = 'QGRAUP'
    CASE ('qh')
      qnames_wrf = 'QHAIL'
      !qnames_wrf = 'QGRAUP'
    CASE DEFAULT
      WRITE(*,'(1x,3a)') 'ERROR: support of ',TRIM(qnames(nq)),' is still not added.'
      CONTINUE
    END SELECT

    CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,  &
                    datestr,itime,qnames_wrf,'0',                       &
                    nx_wrf,ny_wrf,nz_wrf,var_wrf,                       &
                    nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

    IF (istatus == 0) THEN

      WHERE(var_wrf < 0.0) var_wrf = 0.0
      WHERE(var_wrf > 1.0) var_wrf = 1.0

      CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',var_wrf,             &
                          nx,ny,nz,qscalar(:,:,:,nq),temd,istatus)


    END IF
  END DO

!-----------------------------------------------------------------------
!
! Set ARPS grid for this WRF grid based on namelist parameters
! Z is not initialized here (?).
!-----------------------------------------------------------------------

! CALL setgrd(nx,ny,x,y)

!-----------------------------------------------------------------------
! Finishing
!-----------------------------------------------------------------------

  CALL close_wrf_files(fHndl,io_form,patch_split,1,ncmprx,ncmpry)

  DEALLOCATE ( fHndl )
  DEALLOCATE ( qv_wrf, pt_wrf, ph_wrf, pb_wrf )
  DEALLOCATE ( var_wrf, temd, var_wrf1d )

  RETURN
END SUBROUTINE dtaread4wrf
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DTAREAD4WRF                ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE dtareadPH(nx,ny,nz,file_name, numdigit,zpm, istatus )

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Read PH and PHB, and map them to the toal verical grid heights used in
!  the ARPS grid (zp)
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Y. Wang (03/24/2017)
!
!  MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------

  USE model_precision

  IMPLICIT NONE

!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  INTEGER,            INTENT(IN)  :: nx,ny,nz          ! Number of grid points in 3 directions

  CHARACTER(LEN=256), INTENT(IN)  :: file_name
  INTEGER,            INTENT(IN)  :: numdigit

  REAL,               INTENT(OUT) :: zpm (nx,ny,nz)
  INTEGER,            INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'mp.inc'

  REAL, PARAMETER :: gravity = 9.81

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INTEGER :: nx_wrf,   ny_wrf,   nz_wrf
  INTEGER :: nxlg_wrf, nylg_wrf, nxd_wrf, nyd_wrf

  INTEGER :: ncmprx, ncmpry
  INTEGER :: i,j,k

  LOGICAL :: multifile, patch_split

  CHARACTER(LEN=256) :: message
  CHARACTER(LEN=19 ) :: datestr
  INTEGER            :: itime, io_form

  INTEGER,  ALLOCATABLE :: fHndl(:,:)
  REAL(SP), ALLOCATABLE :: temd(:,:,:)

  REAL(P),  ALLOCATABLE :: ph_wrf(:,:,:), phb_wrf(:,:,:)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

!-----------------------------------------------------------------------
!
! This subroutine handles 6 possible readings
!
! 1. One WRF file in no-mpi mode (just read);                         - multifile = .FALSE., patch_split = .FALSE.
! 2. One WRF file in mpi mode (split);                                - multifile = .FALSE., patch_split = .TRUE.
!
! 3. Split WRF files in no-mpi mode (join);                           + multifile = .TRUE., patch_split = .FALSE.
! 4. Split WRF files with same number of processes (just read);       + multifile = .TRUE., patch_split = .FALSE.
! 5. Split WRF files with less number of processes (read and join);   + multifile = .TRUE., patch_split = .FALSE.
! 6. Split WRF files with more number of processes (read and split);  + multifile = .TRUE., patch_split = .TRUE.
!
!-----------------------------------------------------------------------

  IF (nproc_x_in == 1 .AND. nproc_y_in == 1) THEN
    multifile = .FALSE.
    ncmprx = 1
    ncmprx = 1
  ELSE
    multifile = .TRUE.
  END IF

  IF (nproc_x_in >= nproc_x .AND. nproc_y_in >= nproc_y) THEN
    patch_split = .FALSE.
    IF (mod(nproc_x_in,nproc_x) /= 0 .OR. mod(nproc_y_in,nproc_y) /= 0) THEN
      message = 'ERROR: nproc_x_in (nproc_y_in) must be a multiple of nproc_x (nproc_y).'
      istatus = -1
    ELSE
      ncmprx = nproc_x_in / nproc_x
      ncmpry = nproc_y_in / nproc_y
    END IF
  ELSE IF (nproc_x_in < nproc_x .AND. nproc_y_in < nproc_y) THEN
    patch_split = .TRUE.
    IF (mod(nproc_x,nproc_x_in) /= 0 .OR. mod(nproc_y,nproc_y_in) /= 0) THEN
      message = 'ERROR: nproc_x_in (nproc_y_in) must be a factor of nproc_x (nproc_y).'
      istatus = -2
    ELSE
      ncmprx = nproc_x / nproc_x_in
      ncmpry = nproc_y / nproc_y_in
    END IF
  ELSE
    message = 'ERROR: unsupported combination of nproc_x_in and nproc_x, or nproc_y_in and nproc_y.'
    istatus = -3
  END IF

  9999 CONTINUE

  IF (istatus /= 0) THEN
    WRITE(*,'(1x,a)')       TRIM(message)
    WRITE(*,'(7x,2(a,I4))') ' nproc_x_in = ',nproc_x_in, ', nproc_x = ', nproc_x
    WRITE(*,'(7x,2(a,I4))') ' nproc_y_in = ',nproc_y_in, ', nproc_y = ', nproc_y
    istatus = -1
    !RETURN
    CALL arpsstop('ERROR mpi parameters',1)
  END IF

!-----------------------------------------------------------------------
!
! Dimension maps
!
!-----------------------------------------------------------------------
  nx_wrf = nx-2       ! staggered dimensions
  ny_wrf = ny-2
  nz_wrf = nz-2

  nxlg_wrf = (nx_wrf-1)*nproc_x + 1     ! WRF domain indices
  nylg_wrf = (ny_wrf-1)*nproc_y + 1

  IF (multifile) THEN
    nxd_wrf = (nxlg_wrf-1) / nproc_x_in + 1
    nyd_wrf = (nylg_wrf-1) / nproc_y_in + 1
  ELSE
    nxd_wrf = nxlg_wrf    ! dataset size in the file, staggered
    nyd_wrf = nylg_wrf
  END IF

  IF (patch_split) THEN
    !
    ! It is required that (nxd_wrf-1) is dividable with ncmprx
    !
    IF (MOD(nxd_wrf-1,ncmprx) /= 0 .OR. MOD(nyd_wrf-1,ncmpry) /= 0) THEN
      istatus = -4
      message = 'ERROR: dimension sizes does not fit with the number of processes.'
      GOTO 9999
    END IF

  END IF

  ALLOCATE(fHndl(ncmprx,ncmpry),             STAT = istatus)

  ALLOCATE(temd    (nxd_wrf,nyd_wrf,nz_wrf), STAT = istatus)

  ALLOCATE(ph_wrf  (nx_wrf,ny_wrf,nz_wrf),   STAT = istatus)
  ALLOCATE(phb_wrf (nx_wrf,ny_wrf,nz_wrf),   STAT = istatus)

  io_form  = 7  ! hard-coded for 7 at present
  CALL open_wrf_files(file_name,io_form,patch_split,multifile,1,        &
                     ncmprx,ncmpry,numdigit,fHndl)

!-----------------------------------------------------------------------
! Initialize data array
!-----------------------------------------------------------------------

  datestr = '2012-10-25_16:22:00'
  itime   = 1

!-----------------------------------------------------------------------
!
! Vertical coordinates variables
!
!-----------------------------------------------------------------------

   CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,   &
                   datestr,itime,'PH','Z',                              &
                   nx_wrf,ny_wrf,nz_wrf,ph_wrf,                         &
                   nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

   CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,   &
                   datestr,itime,'PHB','Z',                             &
                   nx_wrf,ny_wrf,nz_wrf,phb_wrf,                        &
                   nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

   ph_wrf(:,:,:)=(phb_wrf(:,:,:)+ph_wrf(:,:,:))/gravity

   CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'V',ph_wrf,                 &
                       nx,ny,nz,zpm,temd,istatus)

!-----------------------------------------------------------------------
! Finishing
!-----------------------------------------------------------------------

  CALL close_wrf_files(fHndl,io_form,patch_split,1,ncmprx,ncmpry)

  DEALLOCATE ( fHndl )
  DEALLOCATE ( phb_wrf, ph_wrf )
  DEALLOCATE ( temd )

  RETURN
END SUBROUTINE dtareadPH
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE dtadump4wrf                ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE dtadump4wrf(wrfoutopt,indirtd,nx,ny,nz,nzsoil,nstyps,        &
                       outdir,outfile,numdigits,                        &
                       x,y,z,zp,zpsoil,                                 &
                       uprt ,vprt ,wprt ,ptprt, pprt ,                  &
                       qvprt, qscalar, tke, kmh,kmv,                    &
                       ubar, vbar, wbar, ptbar, pbar, rhobar, qvbar,    &
                       ph, mu, istatus)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Coordinate the writing of WRF history data in netCDF format.
!  Composed following the subroutine dtadump.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Shizhang Wang
!    3/3/2011.
!
!  MODIFICATION HISTORY:
!
!  11/13/2012 (Y. Wang)
!  Rewrote for flexibility and reusability.
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx,ny,nz The dimension of data arrays
!    nzsoil   The dimension of data arrays
!
!  DATA ARRAYS READ IN:
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       z coordinate of grid points in physical space (m)
!    zpsoil   z coordinate of grid points in the soil (m)
!
!    uprt     x component of perturbation velocity (m/s)
!    vprt     y component of perturbation velocity (m/s)
!    wprt     vertical component of perturbation velocity
!             in Cartesian coordinates (m/s).
!
!    ptprt    perturbation potential temperature (K)
!    pprt     perturbation pressure (Pascal)
!    qvprt    perturbation water vapor mixing ratio (kg/kg)
!
!    qc       Cloud water mixing ratio (kg/kg)
!    qr       Rainwater mixing ratio (kg/kg)
!    qi       Cloud ice mixing ratio (kg/kg)
!    qs       Snow mixing ratio (kg/kg)
!    qh       Hail mixing ratio (kg/kg)
!    tke      Turbulent Kinetic Energy ((m/s)**2)
!
!    kmh      Horizontal turb. mixing coef. for momentum ( m**2/s )
!    kmv      Vertical turb. mixing coef. for momentum ( m**2/s )
!
!    ubar     Base state x velocity component (m/s)
!    vbar     Base state y velocity component (m/s)
!    wbar     Base state z velocity component (m/s)
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhobar   Base state air density (kg/m**3)
!    qvbar    Base state water vapor mixing ratio (kg/kg)
!
!    soiltyp  Soil type
!    stypfrct  Soil type fraction
!    vegtyp   Vegetation type
!    lai      Leaf Area Index
!    roufns   Surface roughness
!    veg      Vegetation fraction
!
!    tsoil    Soil temperature (K)
!    qsoil    Soil moisture (m**3/m**3)
!    wetcanp  Canopy water amount
!
!    raing    Grid supersaturation rain
!    rainc    Cumulus convective rain
!    prcrate  Precipitation rates
!
!    radfrc   Radiation forcing (K/s)
!    radsw    Solar radiation reaching the surface
!    rnflx    Net radiation flux absorbed by surface
!    radswnet Net shortwave radiation, SWin - SWout
!    radlwin  Incoming longwave radiation
!
!    usflx    Surface flux of u-momentum (kg/(m*s**2))
!    vsflx    Surface flux of v-momentum (kg/(m*s**2))
!    ptsflx   Surface heat flux (K*kg/(m**2 * s ))
!    qvsflx   Surface moisture flux of (kg/(m**2 * s))
!
!    ireturn  Return status indicator
!
!-----------------------------------------------------------------------

  USE wrfio_api

  IMPLICIT NONE

!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'indtflg.inc'
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'          ! Grid & map parameters.
  INCLUDE 'mp.inc'            ! mpi parameters.
  INCLUDE 'phycst.inc'

!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
  INTEGER :: nzsoil            ! Number of grid points in the soil
  INTEGER :: nstyps            ! Number of soil type

  CHARACTER(LEN=256)  :: outfile, outdir, indirtd
  INTEGER, INTENT(IN) :: numdigits, wrfoutopt

  REAL :: x     (nx)           ! x-coord. of the physical and compu
                               ! -tational grid. Defined at u-point(m).
  REAL :: y     (ny)           ! y-coord. of the physical and compu
                               ! -tational grid. Defined at v-point(m).
  REAL :: z     (nz)           ! z-coord. of the computational grid.
                               ! Defined at w-point on the staggered
                               ! grid(m).
  REAL :: zp    (nx,ny,nz)     ! Physical height coordinate defined at
                               ! w-point of the staggered grid(m).
  REAL :: zpsoil (nx,ny,nzsoil)      ! Physical height coordinate defined at
                               ! w-point of the soil (m).

  REAL :: uprt (nx,ny,nz)      ! x component of perturbation velocity
                               ! (m/s)
  REAL :: vprt (nx,ny,nz)      ! y component of perturbation velocity
                               ! (m/s)
  REAL :: wprt (nx,ny,nz)      ! vertical component of perturbation
                               ! velocity in Cartesian coordinates
                               ! (m/s).
  REAL :: ptprt(nx,ny,nz)      ! perturbation potential temperature (K)
  REAL :: pprt (nx,ny,nz)      ! perturbation pressure (Pascal)

  REAL :: ph (nx,ny,nz)      ! Perturbation geopotential for WRF
  REAL :: mu(nx,ny)            ! perturbation dry air mass in column (Pa)

  REAL :: qvprt(nx,ny,nz)      ! perturbation water vapor mixing ratio
                               ! (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalar)

  REAL :: tke  (nx,ny,nz)      ! Turbulent Kinetic Energy ((m/s)**2)

  REAL :: kmh   (nx,ny,nz)     ! Horizontal turb. mixing coef. for
                               ! momentum. ( m**2/s )
  REAL :: kmv   (nx,ny,nz)     ! Vertical turb. mixing coef. for
                               ! momentum. ( m**2/s )
  REAL :: ubar  (nx,ny,nz)     ! Base state x velocity component (m/s)
  REAL :: vbar  (nx,ny,nz)     ! Base state y velocity component (m/s)
  REAL :: wbar  (nx,ny,nz)     ! Base state z velocity component (m/s)
  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: rhobar(nx,ny,nz)     ! Base state air density (kg/m**3)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal)
  REAL :: qvbar (nx,ny,nz)     ! Base state water vapor mixing ratio
                               ! (kg/kg)
  INTEGER :: istatus           ! Return status indicator

!-----------------------------------------------------------------------

  INTEGER :: nx_wrf, ny_wrf, nz_wrf, nq

  CHARACTER(LEN=40) :: qnames_wrf
  CHARACTER(LEN=19) :: timestr

  LOGICAL :: hard_copy = .TRUE.

  CHARACTER(LEN=256), PARAMETER :: tmpldir     = './wrftmpl/'
  CHARACTER(LEN=256) :: tmplfile
  !CHARACTER(LEN=256), PARAMETER :: enkfoutfile = 'wrfinput_d01'

  CHARACTER(LEN=80),  PARAMETER :: wrftitle    = 'OUTPUT FROM ARPS EnKF V5.3 for WRFV3.5.1'

  REAL, ALLOCATABLE :: var_wrf(:,:,:)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  WRITE(timestr,'(I4.4,5(a,I2.2))') year,'-',month,'-',day,'_',hour, ':', minute, ':', second

!-----------------------------------------------------------------------
!
! First, set parameters for house keeping
!
!-----------------------------------------------------------------------

  nx_wrf = nx-2
  ny_wrf = ny-2
  nz_wrf = nz-2

  ALLOCATE( var_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus )

  IF (wrfoutopt == 0) THEN
    hard_copy = .FALSE.
    WRITE(tmplfile,'(3a)') TRIM(indirtd),'wrfout_d01_',TRIM(timestr)
  ELSE IF (wrfoutopt == 1) THEN
    WRITE(tmplfile,'(2a)') TRIM(indirtd),'wrfinput_d01'
  ELSE IF (wrfoutopt == 2) THEN
    WRITE(tmplfile,'(3a)') TRIM(indirtd),'wrfout_d01_',TRIM(timestr)
  END IF


  IF (hard_copy) THEN
    IF (myproc == 0) THEN
      CALL wrf_file_hard_copy( nproc_x_out,nproc_y_out,numdigits,       &
                               TRIM(tmplfile),outdir,outfile,istatus )
    END IF
    CALL mpbarrier
  END IF

  CALL wrf_io_init( wrfoutopt,nx_wrf,ny_wrf,nz_wrf,hard_copy,numdigits, &
                    outdir,outfile,istatus )

!-----------------------------------------------------------------------
!
! 1. Copy existing fields to WRF input file, exluding the analyzed fields
!
!-----------------------------------------------------------------------


  CALL wrf_file_define( tmplfile, numdigits, wrftitle, timestr, 1, istatus )

  !IF (wrfoutopt /= 0) THEN
  !  ! copy time-dependent fields from WRF history file
  !  CALL wrf_file_copy( indirtd, numdigits, timestr, istatus )
  !END IF

!-----------------------------------------------------------------------
!
! 2. Write out analyzed fields
!
!-----------------------------------------------------------------------

  ! analysis variables

  CALL array_arps2wrf(nx,ny,nz,uprt,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'U', var_wrf, 'X', istatus )

  CALL array_arps2wrf(nx,ny,nz,vprt,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'V', var_wrf, 'Y', istatus )

  CALL array_arps2wrf(nx,ny,nz,wprt,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'W', var_wrf, 'Z', istatus )

  CALL array_arps2wrf(nx,ny,nz,ph,  nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'PH',var_wrf, 'Z', istatus )

  CALL array_arps2wrf(nx,ny,nz,ptprt,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'T',     var_wrf, ' ', istatus )

  CALL array_arps2wrf(nx,ny,nz,qvprt,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'QVAPOR',var_wrf, ' ', istatus )

  ! microphysical variables
  DO nq = 1, nscalar
    !SELECT CASE ( qnames(nq) )
    !CASE ('qc')
    !  qnames_wrf = 'QCLOUD'
    !CASE ('qr')
    !  qnames_wrf = 'QRAIN'
    !CASE ('qi')
    !  qnames_wrf = 'QICE'
    !CASE ('qs')
    !  qnames_wrf = 'QSNOW'
    !CASE ('qg')
    !  qnames_wrf = 'QGRAUP'
    !CASE ('qh')
    !  qnames_wrf = 'QHAIL'
    !CASE DEFAULT
    !  WRITE(*,'(1x,3a)') 'ERROR: support of ',TRIM(qnames(nq)),' is still not added.'
    !  CONTINUE
    !END SELECT

    CALL array_arps2wrf(nx,ny,nz,qscalar(:,:,:,nq),nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
    CALL wrf_file_write3d ( qnames(nq), var_wrf, ' ', istatus )
  END DO

  CALL array_arps2wrf_2d(nx,ny,mu,nx_wrf,ny_wrf,var_wrf,istatus)
  CALL wrf_file_write2d ( 'MU',var_wrf(:,:,1), ' ', istatus )
  ! for dump ensemble mean, because the value of this variable in each member is not the same

!-----------------------------------------------------------------------
!
! 3. Close opened files
!
!-----------------------------------------------------------------------

  CALL wrf_io_final( istatus )

  DEALLOCATE( var_wrf )

  RETURN
END SUBROUTINE dtadump4wrf

!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DTAREADWRFINONE            ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE dtareadwrfinone(nx_en,ny_en,nz_en,nx_m,ny_m,nz_m,file_name,userho,    &
                           interpopt,iim,jjm,wm11,wm12,wm21,wm22,       &
                           u ,v ,w ,pt, pp, qv, qscalar, rhobar, zpm,   &
                           istatus )

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Coordinate the reading of WRF history data in one file and then
!  broadcast and process in the memory.
!
!  It combines the capability of dtaread4wrf & dtareadPH for speedy
!  processing when node has plenty of memory.
!
!  It can only handles One WRF file in no-mpi mode  or mpi mode.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!    5/05/2017.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx,ny,nz       The dimension of out arrays
!    nx_m,ny_m,nz_m The dimension of data arrays
!
!  DATA ARRAYS READ IN:
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!
!    uprt     x component of perturbation velocity (m/s)
!    vprt     y component of perturbation velocity (m/s)
!    wprt     vertical component of perturbation velocity
!             in Cartesian coordinates (m/s).
!
!    ptprt    perturbation potential temperature (K)
!    pprt     perturbation pressure (Pascal)
!    qvprt    perturbation water vapor mixing ratio (kg/kg)
!
!    qc       Cloud water mixing ratio (kg/kg)
!    qr       Rainwater mixing ratio (kg/kg)
!    qi       Cloud ice mixing ratio (kg/kg)
!    qs       Snow mixing ratio (kg/kg)
!    qh       Hail mixing ratio (kg/kg)
!
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhobar   Base state air density (kg/m**3)
!    qvbar    Base state water vapor mixing ratio (kg/kg)
!
!  OUTPUT:
!
!    time     The time of the input data (s)
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!
!    uprt     x component of perturbation velocity (m/s)
!    vprt     y component of perturbation velocity (m/s)
!    wprt     vertical component of perturbation velocity in
!             Cartesian coordinates (m/s).
!
!    ptprt    perturbation potential temperature (K)
!    pprt     perturbation pressure (Pascal)
!
!    qvprt    perturbation water vapor mixing ratio (kg/kg)
!    qc       Cloud water mixing ratio (kg/kg)
!    qr       Rainwater mixing ratio (kg/kg)
!    qi       Cloud ice mixing ratio (kg/kg)
!    qs       Snow mixing ratio (kg/kg)
!    qh       Hail mixing ratio (kg/kg)
!
!    ptbar    Base state potential temperature (K)
!    pbar     Base state pressure (Pascal)
!    rhobar   Base state air density (kg/m**3)
!    qvbar    Base state water vapor mixing ratio (kg/kg)
!
!    ireturn  Return status indicator
!
!-----------------------------------------------------------------------

  USE model_precision

  IMPLICIT NONE

  INCLUDE 'mp.inc'
  INCLUDE 'globcst.inc'

!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!  Z is not initialized (?) - WYH
!-----------------------------------------------------------------------
!
  INTEGER, INTENT(IN) :: nx_en,ny_en,nz_en          ! Number of grid points in 3 directions
  INTEGER, INTENT(IN) :: nx_m,ny_m,nz_m

  INTEGER, INTENT(IN) :: interpopt

  CHARACTER(LEN=256)  :: file_name
  LOGICAL, INTENT(IN) :: userho

  INTEGER, INTENT(IN) :: iim(nx_en,2), jjm(ny_en,2)
  REAL,    INTENT(IN) :: wm11(nx_en,ny_en), wm12(nx_en,ny_en)
  REAL,    INTENT(IN) :: wm21(nx_en,ny_en), wm22(nx_en,ny_en)

! REAL :: x     (nx)           ! x-coord. of the physical and compu
                               ! -tational grid. Defined at u-point(m).
! REAL :: y     (ny)           ! y-coord. of the physical and compu
                               ! -tational grid. Defined at v-point(m).
! REAL :: z     (nz)           ! z-coord. of the computational grid.
                               ! Defined at w-point on the staggered
                               ! grid(m).

  REAL, INTENT(OUT) :: u (nx_en,ny_en,nz_en)         ! x component of perturbation velocity
                               ! (m/s)
  REAL, INTENT(OUT) :: v (nx_en,ny_en,nz_en)         ! y component of perturbation velocity
                               ! (m/s)
  REAL, INTENT(OUT) :: w (nx_en,ny_en,nz_en)         ! vertical component of perturbation
                               ! velocity in Cartesian coordinates
                               ! (m/s).
  REAL, INTENT(OUT) :: pt (nx_en,ny_en,nz_en)        ! perturbation potential temperature (K)
  REAL, INTENT(OUT) :: pp (nx_en,ny_en,nz_en)        ! perturbation pressure (Pascal)

  REAL, INTENT(OUT) :: qv(nx_en,ny_en,nz_en)         ! perturbation water vapor mixing ratio
                               ! (kg/kg)
  REAL, INTENT(OUT) :: qscalar(nx_en,ny_en,nz_en,nscalarq)
  REAL, INTENT(OUT) :: rhobar(nx_en,ny_en,nz_en)
  REAL, INTENT(OUT) :: zpm(nx_en,ny_en,nz_en)

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  REAL, PARAMETER :: gravity = 9.81

!-----------------------------------------------------------------------
!
! Local working variables
!
!-----------------------------------------------------------------------

  INTEGER :: numsvar, num1dvar, num2dvar
  CHARACTER(LEN=40) :: namessvar(1), names1dvar(2),names2dvar(2)
  INTEGER           :: size1dvar(2), maxsize1d

  INTEGER :: num3dvar, numqbase
  CHARACTER(LEN=40) :: names3dvar(20)
  CHARACTER(LEN=1)  :: stag3dvar(20)

  INTEGER :: PTR_U, PTR_V, PTR_W, PTR_T, PTR_P, PTR_PB, PTR_PH, PTR_PHB
  INTEGER :: PTR_QV, PTR_QC, PTR_QR, PTR_QI, PTR_QS, PTR_QG, PTR_QH

  REAL(P)  :: p_top
  REAL(P),  ALLOCATABLE :: qv_wrf(:,:,:), pt_wrf(:,:,:), ph_wrf(:,:,:)
  REAL(P),  ALLOCATABLE :: mub_wrf(:,:), mu_wrf(:,:)
  REAL(P),  ALLOCATABLE :: znu_wrf(:), rdnw_wrf(:)
  REAL(P),  ALLOCATABLE :: rb_wrf(:,:,:)

  REAL(P),  ALLOCATABLE :: vars_wrf(:), var1d_wrf(:,:), var2d_wrf(:,:,:)
  REAL(P),  ALLOCATABLE :: var3d_wrf(:,:,:,:)

  REAL(P),  ALLOCATABLE :: rhotmp(:,:,:)
!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  LOGICAL :: fexists

  INTEGER :: nx_wrf, ny_wrf, nz_wrf
  INTEGER :: nxlg_wrf, nylg_wrf

  INTEGER :: i,j,k,nq
  INTEGER :: ii1, ii2, jj1, jj2, kk

  INTEGER :: dim0dvar, dim1dvar, dim2dvar, dim3dvar
  REAL    :: ptval, ppval, pbval, qvval, phval, phbval

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  IF (nproc_x_in /= 1 .OR. nproc_y_in /= 1) THEN
    WRITE(*,'(1x,a)')       'ERROR: unsupported process configuration.'
    WRITE(*,'(7x,2(a,I4))') ' nproc_x_in = ',nproc_x_in, ', nproc_x = ', nproc_x
    WRITE(*,'(7x,2(a,I4))') ' nproc_y_in = ',nproc_y_in, ', nproc_y = ', nproc_y
    istatus = -1
    !RETURN
    CALL arpsstop('ERROR mpi parameters',1)
  END IF

  INQUIRE(FILE = file_name, EXIST = fexists)
  IF (.NOT. fexists) THEN
    istatus = -1
    WRITE(6,'(2a)') 'ERROR: File not found: ', file_name
    RETURN
  ENDIF

!-----------------------------------------------------------------------
!
! Dimension sizes
!
!-----------------------------------------------------------------------
  nx_wrf = nx_m-2       ! staggered dimensions
  ny_wrf = ny_m-2
  nz_wrf = nz_m-2

  nxlg_wrf = (nx_wrf-1)*nproc_x + 1     ! WRF domain indices
  nylg_wrf = (ny_wrf-1)*nproc_y + 1

!-----------------------------------------------------------------------
!
! Variable names
!
!-----------------------------------------------------------------------

  numsvar = 1
  namessvar(1) = 'P_TOP'

  num1dvar = 2
  names1dvar(1) = 'RDNW'; size1dvar(1) = nz_wrf-1
  names1dvar(2) = 'ZNU';  size1dvar(2) = nz_wrf-1

  num2dvar = 2
  names2dvar(1) = 'MUB'
  names2dvar(2) = 'MU'

  IF (.NOT. userho) THEN   ! Do not read scalar/1d/2d variables
    numsvar  = 0
    num1dvar = 0
    num2dvar = 0
  END IF

  names3dvar(1) = 'U';      stag3dvar(1) = 'X'; PTR_U   = 1
  names3dvar(2) = 'V';      stag3dvar(2) = 'Y'; PTR_V   = 2
  names3dvar(3) = 'W';      stag3dvar(3) = 'Z'; PTR_W   = 3
  names3dvar(4) = 'T';      stag3dvar(4) = '0'; PTR_T   = 4
  names3dvar(5) = 'P';      stag3dvar(5) = '0'; PTR_P   = 5
  names3dvar(6) = 'PB';     stag3dvar(6) = '0'; PTR_PB  = 6
  names3dvar(7) = 'PH';     stag3dvar(7) = 'V'; PTR_PH  = 7
  names3dvar(8) = 'PHB';    stag3dvar(8) = 'V'; PTR_PHB = 8
  names3dvar(9) = 'QVAPOR'; stag3dvar(9) = '0'; PTR_QV  = 9
  numqbase = 9
  num3dvar = numqbase+nscalarq

  DO nq = 1, nscalarq

    SELECT CASE ( TRIM(qnames(nq)) )
    CASE ('qc')
      names3dvar(numqbase+nq) = 'QCLOUD';  PTR_QC  = numqbase+nq
    CASE ('qr')
      names3dvar(numqbase+nq) = 'QRAIN';   PTR_QR  = numqbase+nq
    CASE ('qi')
      names3dvar(numqbase+nq) ='QICE';     PTR_QI  = numqbase+nq
    CASE ('qs')
      names3dvar(numqbase+nq) = 'QSNOW';   PTR_QS  = numqbase+nq
    CASE ('qg')
      names3dvar(numqbase+nq) = 'QGRAUP';  PTR_QG  = numqbase+nq
    CASE ('qh')
      names3dvar(numqbase+nq) ='QHAIL';    PTR_QH  = numqbase+nq
    CASE DEFAULT
      WRITE(*,'(1x,3a)') 'ERROR: support of ',TRIM(qnames(nq)),' is still not added.'
      CONTINUE
    END SELECT
    stag3dvar(numqbase+nq) = '0'

  END DO

  dim0dvar = MAX(numsvar, 1)   ! To avoid 0 dimension arrays
  dim1dvar = MAX(num1dvar,1)
  dim2dvar = MAX(num2dvar,1)
  dim3dvar = num3dvar

!-----------------------------------------------------------------------
!
! Allocate data arrays and read
!
!-----------------------------------------------------------------------

  maxsize1d = MAXVAL(size1dvar)
  ALLOCATE(var3d_wrf(0:nx_wrf+1,0:ny_wrf+1,0:nz_wrf+1,dim3dvar), STAT = istatus)
  ALLOCATE(var2d_wrf(0:nx_wrf+1,0:ny_wrf+1,           dim2dvar), STAT = istatus)
  ALLOCATE(var1d_wrf(maxsize1d,                       dim1dvar), STAT = istatus)
  ALLOCATE(vars_wrf (                                 dim0dvar),  STAT = istatus)

  var3d_wrf = 0.0
  var2d_wrf = 0.0
  var1d_wrf = 0.0
  vars_wrf  = 0.0

  CALL readwrfinonefile(file_name,nx_wrf,ny_wrf,nz_wrf,nxlg_wrf,nylg_wrf, &
                        dim0dvar, dim1dvar, dim2dvar, dim3dvar,           &
                        numsvar,namessvar,vars_wrf,                       &
                        num1dvar,names1dvar,size1dvar,maxsize1d,var1d_wrf,&
                        num2dvar,names2dvar,var2d_wrf,                    &
                        num3dvar,names3dvar,stag3dvar,var3d_wrf,          &
                        istatus)

!-----------------------------------------------------------------------
!
!  Transfer to the ARPS grid
!
!  Do interpolation, nx_en = nx > nx_em
!
!-----------------------------------------------------------------------

  IF (interpopt == 2) THEN    ! interpolate to nx,ny,nz, nx_en == nx
    DO k = 1,nz_en
      kk = k-1
      DO j = 1, ny_en
        jj1 = jjm(j,1)-1; jj2 = jjm(j,2)-1
        DO i = 1, nx_en
          ii1 = iim(i,1)-1; ii2 = iim(i,2)-1

          u(i,j,k) = var3d_wrf(ii1,jj1,kk,PTR_U)*wm11(i,j)+             &
                     var3d_wrf(ii2,jj1,kk,PTR_U)*wm21(i,j)+             &
                     var3d_wrf(ii1,jj2,kk,PTR_U)*wm12(i,j)+             &
                     var3d_wrf(ii2,jj2,kk,PTR_U)*wm22(i,j)
          v(i,j,k) = var3d_wrf(ii1,jj1,kk,PTR_V)*wm11(i,j)+             &
                     var3d_wrf(ii2,jj1,kk,PTR_V)*wm21(i,j)+             &
                     var3d_wrf(ii1,jj2,kk,PTR_V)*wm12(i,j)+             &
                     var3d_wrf(ii2,jj2,kk,PTR_V)*wm22(i,j)
          w(i,j,k) = var3d_wrf(ii1,jj1,kk,PTR_W)*wm11(i,j)+             &
                     var3d_wrf(ii2,jj1,kk,PTR_W)*wm21(i,j)+             &
                     var3d_wrf(ii1,jj2,kk,PTR_W)*wm12(i,j)+             &
                     var3d_wrf(ii2,jj2,kk,PTR_W)*wm22(i,j)
          ptval = var3d_wrf(ii1,jj1,kk,PTR_T)*wm11(i,j)+                &
                  var3d_wrf(ii2,jj1,kk,PTR_T)*wm21(i,j)+                &
                  var3d_wrf(ii1,jj2,kk,PTR_T)*wm12(i,j)+                &
                  var3d_wrf(ii2,jj2,kk,PTR_T)*wm22(i,j)
          pt(i,j,k) = ptval + 300.0
          ppval = var3d_wrf(ii1,jj1,kk,PTR_P)*wm11(i,j)+                &
                  var3d_wrf(ii2,jj1,kk,PTR_P)*wm21(i,j)+                &
                  var3d_wrf(ii1,jj2,kk,PTR_P)*wm12(i,j)+                &
                  var3d_wrf(ii2,jj2,kk,PTR_P)*wm22(i,j)
          pbval = var3d_wrf(ii1,jj1,kk,PTR_PB)*wm11(i,j)+               &
                  var3d_wrf(ii2,jj1,kk,PTR_PB)*wm21(i,j)+               &
                  var3d_wrf(ii1,jj2,kk,PTR_PB)*wm12(i,j)+               &
                  var3d_wrf(ii2,jj2,kk,PTR_PB)*wm22(i,j)
          pp(i,j,k) = ppval + pbval

          qvval = var3d_wrf(ii1,jj1,kk,PTR_QV)*wm11(i,j)+               &
                  var3d_wrf(ii2,jj1,kk,PTR_QV)*wm21(i,j)+               &
                  var3d_wrf(ii1,jj2,kk,PTR_QV)*wm12(i,j)+               &
                  var3d_wrf(ii2,jj2,kk,PTR_QV)*wm22(i,j)
          qv(i,j,k) =  qvval/(1.0+qvval)
        END DO
      END DO
    END DO

    WHERE(qv < 0.0) qv = 0.0
    WHERE(qv > 1.0) qv = 1.0

    !
    ! hydrometeors
    !
    IF (P_QC > 0) THEN
      DO k = 1,nz_en
        kk = k-1
        DO j = 1, ny_en
          jj1 = jjm(j,1)-1; jj2 = jjm(j,2)-1
          DO i = 1, nx_en
            ii1 = iim(i,1)-1; ii2 = iim(i,2)-1
            qscalar(i,j,k,P_QC) = var3d_wrf(ii1,jj1,kk,PTR_QC)*wm11(i,j)+ &
                                  var3d_wrf(ii2,jj1,kk,PTR_QC)*wm21(i,j)+ &
                                  var3d_wrf(ii1,jj2,kk,PTR_QC)*wm12(i,j)+ &
                                  var3d_wrf(ii2,jj2,kk,PTR_QC)*wm22(i,j)
          END DO
        END DO
      END DO
    END IF

    IF (P_QR > 0) THEN
      DO k = 1,nz_en
        kk = k-1
        DO j = 1, ny_en
          jj1 = jjm(j,1)-1; jj2 = jjm(j,2)-1
          DO i = 1, nx_en
            ii1 = iim(i,1)-1; ii2 = iim(i,2)-1
            qscalar(i,j,k,P_QR) = var3d_wrf(ii1,jj1,kk,PTR_QR)*wm11(i,j)+ &
                                  var3d_wrf(ii2,jj1,kk,PTR_QR)*wm21(i,j)+ &
                                  var3d_wrf(ii1,jj2,kk,PTR_QR)*wm12(i,j)+ &
                                  var3d_wrf(ii2,jj2,kk,PTR_QR)*wm22(i,j)
          END DO
        END DO
      END DO
    END IF

    IF (P_QI > 0) THEN
      DO k = 1,nz_en
        kk = k-1
        DO j = 1, ny_en
          jj1 = jjm(j,1)-1; jj2 = jjm(j,2)-1
          DO i = 1, nx_en
            ii1 = iim(i,1)-1; ii2 = iim(i,2)-1
            qscalar(i,j,k,P_QI) = var3d_wrf(ii1,jj1,kk,PTR_QI)*wm11(i,j)+ &
                                  var3d_wrf(ii2,jj1,kk,PTR_QI)*wm21(i,j)+ &
                                  var3d_wrf(ii1,jj2,kk,PTR_QI)*wm12(i,j)+ &
                                  var3d_wrf(ii2,jj2,kk,PTR_QI)*wm22(i,j)
          END DO
        END DO
      END DO
    END IF

    IF (P_QS > 0) THEN
      DO k = 1,nz_en
        kk = k-1
        DO j = 1, ny_en
          jj1 = jjm(j,1)-1; jj2 = jjm(j,2)-1
          DO i = 1, nx_en
            ii1 = iim(i,1)-1; ii2 = iim(i,2)-1
            qscalar(i,j,k,P_QS) = var3d_wrf(ii1,jj1,kk,PTR_QS)*wm11(i,j)+ &
                                  var3d_wrf(ii2,jj1,kk,PTR_QS)*wm21(i,j)+ &
                                  var3d_wrf(ii1,jj2,kk,PTR_QS)*wm12(i,j)+ &
                                  var3d_wrf(ii2,jj2,kk,PTR_QS)*wm22(i,j)
          END DO
        END DO
      END DO
    END IF

    IF (P_QG > 0) THEN
      DO k = 1,nz_en
        kk = k-1
        DO j = 1, ny_en
          jj1 = jjm(j,1)-1; jj2 = jjm(j,2)-1
          DO i = 1, nx_en
            ii1 = iim(i,1)-1; ii2 = iim(i,2)-1
            qscalar(i,j,k,P_QG) = var3d_wrf(ii1,jj1,kk,PTR_QG)*wm11(i,j)+ &
                                  var3d_wrf(ii2,jj1,kk,PTR_QG)*wm21(i,j)+ &
                                  var3d_wrf(ii1,jj2,kk,PTR_QG)*wm12(i,j)+ &
                                  var3d_wrf(ii2,jj2,kk,PTR_QG)*wm22(i,j)
          END DO
        END DO
      END DO
    END IF

    IF (P_QH > 0) THEN
      DO k = 1,nz_en
        kk = k-1
        DO j = 1, ny_en
          jj1 = jjm(j,1)-1; jj2 = jjm(j,2)-1
          DO i = 1, nx_en
            ii1 = iim(i,1)-1; ii2 = iim(i,2)-1
            qscalar(i,j,k,P_QH) = var3d_wrf(ii1,jj1,kk,PTR_QH)*wm11(i,j)+ &
                                  var3d_wrf(ii2,jj1,kk,PTR_QH)*wm21(i,j)+ &
                                  var3d_wrf(ii1,jj2,kk,PTR_QH)*wm12(i,j)+ &
                                  var3d_wrf(ii2,jj2,kk,PTR_QH)*wm22(i,j)
          END DO
        END DO
      END DO
    END IF

    !WHERE(qscalar < 0.0) qscalar = 0.0
    !WHERE(qscalar > 1.0) qscalar = 1.0

    !
    ! Vertical coordinates variables
    !
    DO k = 1,nz_en
      kk = k-1
      DO j = 1, ny_en
        jj1 = jjm(j,1)-1; jj2 = jjm(j,2)-1
        DO i = 1, nx_en
          ii1 = iim(i,1)-1; ii2 = iim(i,2)-1

          phval = var3d_wrf(ii1,jj1,kk,PTR_PH )*wm11(i,j)+      &
                  var3d_wrf(ii2,jj1,kk,PTR_PH )*wm21(i,j)+      &
                  var3d_wrf(ii1,jj2,kk,PTR_PH )*wm12(i,j)+      &
                  var3d_wrf(ii2,jj2,kk,PTR_PH )*wm22(i,j)
          phbval= var3d_wrf(ii1,jj1,kk,PTR_PHB)*wm11(i,j)+      &
                  var3d_wrf(ii2,jj1,kk,PTR_PHB)*wm21(i,j)+      &
                  var3d_wrf(ii1,jj2,kk,PTR_PHB)*wm12(i,j)+      &
                  var3d_wrf(ii2,jj2,kk,PTR_PHB)*wm22(i,j)

          zpm(i,j,k) = (phval+phbval)/gravity

        END DO
      END DO
    END DO

  ELSE             ! nx_m == nx_en

!-----------------------------------------------------------------------
!
! no interpolation nx_m = nx_en
! nx is nx_en here
!
!-----------------------------------------------------------------------

    DO k = 1,nz_en
      DO j = 1, ny_en
        DO i = 1, nx_en
          u(i,j,k) = var3d_wrf(i-1,j-1,k-1,PTR_U)
          v(i,j,k) = var3d_wrf(i-1,j-1,k-1,PTR_V)
          w(i,j,k) = var3d_wrf(i-1,j-1,k-1,PTR_W)
          pt(i,j,k) = var3d_wrf(i-1,j-1,k-1,PTR_T)+ 300.0
          pp(i,j,k) = var3d_wrf(i-1,j-1,k-1,PTR_P)+ var3d_wrf(i-1,j-1,k-1,PTR_PB)
          qv(i,j,k) = var3d_wrf(i-1,j-1,k-1,PTR_QV)/(1.0+var3d_wrf(i-1,j-1,k-1,PTR_QV))
        END DO
      END DO
    END DO

    WHERE(qv < 0.0) qv = 0.0
    WHERE(qv > 1.0) qv = 1.0

    !
    ! hydrometeors
    !
    IF (P_QC > 0) THEN
      DO k = 1,nz_en
        DO j = 1, ny_en
          DO i = 1, nx_en
            qscalar(i,j,k,P_QC) = var3d_wrf(i-1,j-1,k-1,PTR_QC)
          END DO
        END DO
      END DO
    END IF

    IF (P_QR > 0) THEN
      DO k = 1,nz_en
        DO j = 1, ny_en
          DO i = 1, nx_en
            qscalar(i,j,k,P_QR) = var3d_wrf(i-1,j-1,k-1,PTR_QR)
          END DO
        END DO
      END DO
    END IF

    IF (P_QI > 0) THEN
      DO k = 1,nz_en
        DO j = 1, ny_en
          DO i = 1, nx_en
            qscalar(i,j,k,P_QI) = var3d_wrf(i-1,j-1,k-1,PTR_QI)
          END DO
        END DO
      END DO
    END IF

    IF (P_QS > 0) THEN
      DO k = 1,nz_en
        DO j = 1, ny_en
          DO i = 1, nx_en
            qscalar(i,j,k,P_QS) = var3d_wrf(i-1,j-1,k-1,PTR_QS)
          END DO
        END DO
      END DO
    END IF

    IF (P_QG > 0) THEN
      DO k = 1,nz_en
        DO j = 1, ny_en
          DO i = 1, nx_en
            qscalar(i,j,k,P_QG) = var3d_wrf(i-1,j-1,k-1,PTR_QG)
          END DO
        END DO
      END DO
    END IF

    IF (P_QH > 0) THEN
      DO k = 1,nz_en
        DO j = 1, ny_en
          DO i = 1, nx_en
            qscalar(i,j,k,P_QH) = var3d_wrf(i-1,j-1,k-1,PTR_QH)
          END DO
        END DO
      END DO
    END IF

    !WHERE(qscalar < 0.0) qscalar = 0.0
    !WHERE(qscalar > 1.0) qscalar = 1.0

    !
    ! Vertical coordinates variables
    !
    DO k = 1,nz_en
      DO j = 1, ny_en
        DO i = 1, nx_en
          zpm(i,j,k) = (var3d_wrf(i-1,j-1,k-1,PTR_PH)+var3d_wrf(i-1,j-1,k-1,PTR_PHB))/gravity
        END DO
      END DO
    END DO

  END IF

!-----------------------------------------------------------------------
!
! Calcultate rhobar
!
!-----------------------------------------------------------------------

  IF (userho) THEN

    ALLOCATE(rhotmp(nx_m,ny_m,nz_m), STAT = istatus)


    ALLOCATE(qv_wrf(nx_wrf,ny_wrf,nz_wrf),     STAT = istatus)
    ALLOCATE(pt_wrf(nx_wrf,ny_wrf,nz_wrf),     STAT = istatus)
    ALLOCATE(ph_wrf(nx_wrf,ny_wrf,nz_wrf),     STAT = istatus)
    ALLOCATE(rb_wrf(nx_wrf,ny_wrf,nz_wrf),     STAT = istatus)
    qv_wrf  = 0.0
    pt_wrf  = 0.0
    ph_wrf  = 0.0
    rb_wrf  = 0.0

    ALLOCATE(mub_wrf (nx_wrf,ny_wrf),          STAT = istatus)
    ALLOCATE(mu_wrf  (nx_wrf,ny_wrf),          STAT = istatus)
    ALLOCATE(znu_wrf (nz_wrf-1),               STAT = istatus)
    ALLOCATE(rdnw_wrf(nz_wrf-1),               STAT = istatus)
    mub_wrf  = 0.0
    mu_wrf   = 0.0
    znu_wrf  = 0.0
    rdnw_wrf = 0.0

    DO j = 1,ny_wrf
      DO i = 1, nx_wrf
        mub_wrf(i,j) = var2d_wrf(i,j,1)
      END DO
    END DO

    DO j = 1,ny_wrf
      DO i = 1, nx_wrf
        mu_wrf(i,j) = var2d_wrf(i,j,2)
      END DO
    END DO

    DO k =1,nz_wrf-1
      rdnw_wrf(k) = var1d_wrf(k,1)
    END DO

    DO k = 1, nz_wrf-1
      znu_wrf(k) = var1d_wrf(k,2)
    END DO

    p_top = vars_wrf(1)

    DO k = 1,nz_wrf
      DO j = 1, ny_wrf
        DO i = 1, nx_wrf
          qv_wrf(i,j,k) = var3d_wrf(i,j,k,PTR_QV)
        END DO
      END DO
    END DO

    DO k = 1,nz_wrf
      DO j = 1, ny_wrf
        DO i = 1, nx_wrf
          ph_wrf(i,j,k) = var3d_wrf(i,j,k,PTR_PH)
        END DO
      END DO
    END DO

    DO k = 1,nz_wrf
      DO j = 1, ny_wrf
        DO i = 1, nx_wrf
          pt_wrf(i,j,k) = var3d_wrf(i,j,k,PTR_T)
        END DO
      END DO
    END DO

    CALL cal_rho_p(nx_wrf,ny_wrf,nz_wrf,mub_wrf,mu_wrf,rdnw_wrf,        &
                   p_top,znu_wrf,qv_wrf,ph_wrf,pt_wrf,                  &
                   rb_wrf,istatus)

    CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',rb_wrf,                &
                        nx_m,ny_m,nz_m,rhotmp,qv_wrf,istatus) !qv_wrf used as a working array

    IF (interpopt == 2) THEN

        DO k = 1,nz_en
          DO j = 1, ny_en
            jj1 = jjm(j,1); jj2 = jjm(j,2)
            DO i = 1, nx_en
              ii1 = iim(i,1); ii2 = iim(i,2)

              rhobar(i,j,k) = rhotmp(ii1,jj1,k)*wm11(i,j)+       &
                              rhotmp(ii2,jj1,k)*wm21(i,j)+       &
                              rhotmp(ii1,jj2,k)*wm12(i,j)+       &
                              rhotmp(ii2,jj2,k)*wm22(i,j)

            END DO
          END DO
        END DO

    ELSE

      rhobar(:,:,:) = rhotmp(:,:,:)

    END IF

    DEALLOCATE ( rhotmp )
    DEALLOCATE ( qv_wrf, pt_wrf , ph_wrf, rb_wrf )
    DEALLOCATE ( mub_wrf, mu_wrf, znu_wrf, rdnw_wrf)
  ELSE
    rhobar(:,:,:) = 0.0
  END IF

!-----------------------------------------------------------------------
! Finishing
!-----------------------------------------------------------------------

  DEALLOCATE ( vars_wrf, var1d_wrf, var2d_wrf, var3d_wrf)

  RETURN
END SUBROUTINE dtareadwrfinone
