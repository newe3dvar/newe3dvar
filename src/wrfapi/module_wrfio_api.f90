MODULE wrfio_api
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! This module handles reading and writing of WRF data.
!
! At present, it will read WRF history files and write out WRF input files.
! It will handle WRF files in netCDF format only.
!
! This module may handle all 9 combinations of the following scenarios
!
!      IN (m), Running (p), OUT (n)
!
! 1.    m = p            a. p = n
! 2.    m > p (join)     b. p < n (one process to multiple files)
! 3.    m < p (split)    c. p > n (multiple processes to one file)
!
! At present, only this three scenarios will be handled
!
! 1. m=n=1    ! p = 1 or p > 1
! 2. m>=p=n   ! p > n is difficult because multiple processes to write one file
! 3. m< p=n   ! p < n is usually not necessary for EnKF system
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  USE model_precision

  PRIVATE

  LOGICAL :: filein, fileout
  INTEGER :: nfx_in, nfy_in, nfx_out, nfy_out
  INTEGER, ALLOCATABLE :: fHndl_in(:,:), fHndl_out(:,:)

  INTEGER :: nxlg, nylg, nx, ny, nz    ! staggered sizes
  INTEGER :: isx, iex, isy, iey        ! in start(end) x(y)   - relative in subdomain
  INTEGER :: osx, oex, osy, oey        ! out start(end) x(y)  - relative in subdomain

  INTEGER :: nxfin,  nyfin
  INTEGER :: nxfout, nyfout

  LOGICAL :: in_split, in_onefile, out_onefile

  REAL(SP), ALLOCATABLE :: varout(:)    ! Working arrays
  REAL(P),  ALLOCATABLE :: var(:,:,:)

  LOGICAL :: hard_copy
  INTEGER :: wrfoutopt, debug = 0

  !---------------------------------------------------------------------
  !
  ! WRF input file specification
  !
  !---------------------------------------------------------------------

  INTEGER, PARAMETER :: nvar_copy = 16, nvar_anal = 14, nvar_save = 0

  CHARACTER(20) :: var_copy_names(nvar_copy) =(/                        &
                     'XLAT                ', 'XLONG               ',    &
                     'XLAT_U              ', 'XLONG_U             ',    &
                     'XLAT_V              ', 'XLONG_V             ',    &
                     'MUB                 ', 'HGT                 ',    &
                     'P                   ', 'PB                  ',    &
                     'PHB                 ', 'MAPFAC_M            ',    &
                     'MAPFAC_U            ', 'MAPFAC_V            ',    &
                     'F                   ', 'E                   '  /)
                     ! MU & PSFC will be replaced with analysis results

  !CHARACTER(20) :: var_copy_names(nvar_copy) =(/                        &
  !                   'P                   ', 'PB                  ',    &
  !                   'PHB                 ', 'MU                  ',    &
  !                   'Q2                  ', 'T2                  ',    &
  !                   'TH2                 ', 'PSFC                ',    &
  !                   'TSLB                ', 'SMOIS               ',    &
  !                   'SH2O                ', 'MUB                 ',    &
  !                   'SEAICE              ', 'SNOW                ',    &
  !                   'SNOWH               ', 'CANWAT              ',    &
  !                   'TSK                 ', 'SNOWC               ',    &
  !                   'SR                  ', 'SST                 ' /)
  !                   ! 'SMCREL              ',


  !CHARACTER(20) :: var_save_names(nvar_save) =(/                        &
  !CHARACTER(20) :: var_save_names(1        ) =(/                        &
  !                   'MU                  ' /)
                     !, 'MUB                 ',    &
                     !'MAPFAC_U            ', 'MAPFAC_V            ',    &
                     !'MAPFAC_M            '  /)

  CHARACTER(20) :: var_analysis_names(nvar_anal) =(/                    &
                     'U                   ', 'V                   ',    &
                     'W                   ', 'PH                  ',    &
                     'T                   ', 'QVAPOR              ',    &
                     'QCLOUD              ', 'QRAIN               ',    &
                     'QICE                ', 'QGRAUP              ',    &
                     'QHAIL               ', 'QSNOW               ',    &
                     'MU                  ', 'PSFC                ' /)

  !*********************************************************************
  PUBLIC :: wrf_io_init, wrf_io_final, wrf_get_dimensions, wrf_fix_titles
  PUBLIC :: wrf_file_define, wrf_file_copy, wrf_file_hard_copy,         &
            wrf_file_write2d, wrf_file_write3d, wrf_file_ready

  INCLUDE 'mp.inc'

  CONTAINS

  !###################### initialization ###############################

  SUBROUTINE wrf_io_init ( outopt,nx_wrf,ny_wrf,nz_wrf,hcopy,numdigits, &
                           dirname, filename, istatus )
    IMPLICIT NONE

    INTEGER,            INTENT(IN)  :: outopt
    INTEGER,            INTENT(IN)  :: nx_wrf, ny_wrf, nz_wrf
    CHARACTER(LEN=256), INTENT(IN)  :: dirname,filename
    LOGICAL,            INTENT(IN)  :: hcopy
    INTEGER,            INTENT(IN)  :: numdigits
    INTEGER,            INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: m, n, i, j
    CHARACTER(LEN=256) :: message

    INTEGER :: iproc, jproc
    INTEGER :: nxsize, nysize
    CHARACTER(LEN=1) :: filemode
    CHARACTER(LEN=20):: fmtstr

    INTEGER :: outsize

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0
    wrfoutopt = outopt
    hard_copy = hcopy

    nx = nx_wrf; ny = ny_wrf; nz = nz_wrf
    nxlg = (nx-1)*nproc_x + 1
    nylg = (ny-1)*nproc_y + 1

    filein  = .FALSE.
    fileout = .FALSE.

    m = nproc_x_in * nproc_y_in
    n = nproc_x_out * nproc_y_out

    in_onefile  = .FALSE.
    out_onefile = .FALSE.
    in_split = .FALSE.
    !
    ! It only supports the following
    !
    !  1. Process 0 read and write
    !  2. All processes read and join patches, and then write out in one patch
    !  3. All processes read and extract patches, and then write out in one patch
    !
    IF (m == 1 .AND. n == 1) THEN
      IF (myproc == 0) THEN
        filein  = .TRUE.
        fileout = .TRUE.
      END IF
      nfx_in  = 1;      nfy_in  = 1
      nfx_out = 1;      nfy_out = 1
      isx = 1; iex = nxlg;  isy = 1; iey = nylg
      osx = 1; oex = nxlg;  osy = 1; oey = nylg
      nxfin  = nxlg; nyfin  = nylg
      nxfout = nxlg; nyfout = nylg
      in_split    = .TRUE.
      in_onefile  = .TRUE.
      out_onefile = .TRUE.
    ELSE IF ( nproc_x_out == nproc_x .AND. nproc_y_out == nproc_y ) THEN
      filein  = .TRUE.
      fileout = .TRUE.
      osx = 1; oex = nx;  osy = 1; oey = ny
      nfx_out = 1; nfy_out = 1
      nxfout = nx; nyfout = ny    ! dataset size
      IF (nproc_x_in >= nproc_x .AND. nproc_y_in >= nproc_y ) THEN
        isx = 1; iex = nx; isy = 1; iey = ny
        nfx_in = nproc_x_in / nproc_x;  nfy_in = nproc_y_in / nproc_y

        nxfin = (nxlg-1)/nproc_x_in + 1
        nyfin = (nylg-1)/nproc_y_in + 1

      ELSE IF (nproc_x_in <= nproc_x .AND. nproc_y_in <= nproc_y ) THEN

        nfx_in = nproc_x / nproc_x_in
        nfy_in = nproc_y / nproc_y_in

        nxfin = (nxlg-1)/nproc_x_in + 1
        nxsize = (nxfin-1)/nfx_in
        iproc  = MOD(loc_x,nfx_in)
        IF (iproc == 0) iproc = nfx_in
        isx = (iproc-1)*nxsize + 1
        iex = (iproc)*nxsize+1

        nyfin = (nylg-1)/nproc_y_in + 1
        nysize = (nyfin-1)/nfy_in
        jproc  = MOD(loc_y,nfy_in)
        IF (jproc == 0) jproc = nfy_in
        isy = (jproc-1)*nysize + 1
        iey = (jproc)*nysize+1

        in_split = .TRUE.
        nfx_in = 1; nfy_in = 1
      ELSE
        istatus = -1
        message = 'ERROR: unsupported combination of nproc_x_in and nproc_x, or nproc_y_in and nproc_y.'
      END IF

    ELSE IF ( n == 1 ) THEN
      filein      = .TRUE.
      IF (myproc == 0) fileout = .TRUE.
      out_onefile = .TRUE.
      osx = 1; oex = nxlg;  osy = 1; oey = nylg
      nfx_out = 1; nfy_out = 1
      nxfout = nxlg; nyfout = nylg    ! dataset size
      IF (nproc_x_in >= nproc_x .AND. nproc_y_in >= nproc_y ) THEN
        isx = 1; iex = nx; isy = 1; iey = ny
        nfx_in = nproc_x_in / nproc_x;  nfy_in = nproc_y_in / nproc_y

        nxfin = (nxlg-1)/nproc_x_in + 1
        nyfin = (nylg-1)/nproc_y_in + 1

      ELSE IF (nproc_x_in <= nproc_x .AND. nproc_y_in <= nproc_y ) THEN

        nfx_in = nproc_x / nproc_x_in
        nfy_in = nproc_y / nproc_y_in

        nxfin  = (nxlg-1)/nproc_x_in + 1
        nxsize = (nxfin-1)/nfx_in
        iproc  = MOD(loc_x,nfx_in)
        IF (iproc == 0) iproc = nfx_in
        isx = (iproc-1)*nxsize + 1
        iex = (iproc)*nxsize+1

        nyfin = (nylg-1)/nproc_y_in + 1
        nysize = (nyfin-1)/nfy_in
        jproc  = MOD(loc_y,nfy_in)
        IF (jproc == 0) jproc = nfy_in
        isy = (jproc-1)*nysize + 1
        iey = (jproc)*nysize+1

        in_split = .TRUE.
        nfx_in = 1; nfy_in = 1
      ELSE
        istatus = -1
        message = 'ERROR: unsupported combination of nproc_x_in and nproc_x, or nproc_y_in and nproc_y.'
      END IF
    ELSE
      istatus = -2
      message = 'ERROR: unsupported combination of nproc_x_out and nproc_x, or nproc_y_out and nproc_y.'
    END IF

    IF (istatus < 0) THEN
      WRITE(*,'(1x,a,I4)')     TRIM(message),istatus
      WRITE(*,'(7x,3(a,I4))') ' nproc_x_in = ',nproc_x_in, ', nproc_x = ', nproc_x, ', nproc_x_out = ',nproc_x_out
      WRITE(*,'(7x,3(a,I4))') ' nproc_y_in = ',nproc_y_in, ', nproc_y = ', nproc_y, ', nproc_y_out = ',nproc_y_out
      CALL arpsstop('ERROR mpi parameters in module_wrfio_api',1)
    END IF

!-----------------------------------------------------------------------
!
! Open files (message is reused as temporary string)
!
!-----------------------------------------------------------------------

    outsize = nxfout*nyfout*nz

    ALLOCATE(fHndl_out(nfx_out,nfy_out),      STAT = istatus)

    filemode = 'w'
    IF (hard_copy) THEN
      filemode = 'm'
    END IF

    IF (fileout) THEN

      WRITE(fmtstr,'(2(a,I0),a)') '(2a,I',numdigits,'.',numdigits,')'

      DO j = 1, nfy_out       ! output file, should in separate directories for each member
        DO i = 1, nfx_out
          IF (out_onefile) THEN
            WRITE(message,'(2a)') TRIM(dirname), TRIM(filename)
          ELSE
            !iproc = (j-1)*nproc_x_out + i-1
            iproc = ((loc_y-1)*nfy_out+j-1)*nproc_x_out + (loc_x-1)*nfx_out+i - 1
            WRITE(message,fmt=TRIM(fmtstr)) TRIM(dirname), TRIM(filename)//'_',iproc
          END IF

          IF (debug > 0) WRITE(*,'(1x,a,I0,a)') 'Opening '//TRIM(message)//' for output (',myproc,').'
          CALL open_ncd_wrf_file(message,filemode,fHndl_out(i,j),istatus)

        END DO
      END DO

    END IF

    IF (istatus < 0) THEN
      WRITE(*,'(1x,3a)')   'File : ', TRIM(message), ' not found.'
      CALL arpsstop('ERROR file open operation',1)
    END IF


!-----------------------------------------------------------------------
!
! Allocate working arrays
!
!-----------------------------------------------------------------------


    ALLOCATE( varout(outsize), STAT = istatus )
    ALLOCATE( var(nx,ny,nz),   STAT = istatus )

    RETURN
  END SUBROUTINE wrf_io_init

  !###################### READYFL ###################################

  SUBROUTINE wrf_file_ready(dirname,filename,readyflag,istatus)

    INTEGER,            INTENT(IN)  :: readyflag
    CHARACTER(LEN=256), INTENT(IN)  :: dirname,filename
    INTEGER,            INTENT(OUT) :: istatus

  !---------------------------------------------------------------------
    INTEGER            :: nchout
    CHARACTER(LEN=256) :: readyfl, temchar

    INTEGER           :: m,n

    CHARACTER(LEN=8)  :: cdate
    CHARACTER(LEN=10) :: ctime
    CHARACTER(LEN=5)  :: czone
    INTEGER           :: ival(8)

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (readyflag == 1) CALL mpbarrier    ! Wait all processes and write out one ready file

    IF (fileout .AND. readyflag > 0) THEN


      m = INDEX(filename,'out_d',  .TRUE.)
      n = INDEX(filename,'input_d',.TRUE.)

      IF ( m > 0) THEN
        WRITE(temchar,'(4a)') TRIM(dirname), filename(1:m-1),'outReady_d',  TRIM(filename(m+5:))
      ELSE IF (n > 0) THEN
        WRITE(temchar,'(4a)') TRIM(dirname), filename(1:n-1),'inputReady_d',TRIM(filename(n+7:))
      ELSE
        WRITE(temchar,'(3a)') TRIM(dirname), TRIM(filename),'_Ready'
      END IF

      IF (readyflag == 1) THEN       ! Wait all processes and write out one ready file
        WRITE(readyfl,'(1a)') TRIM(temchar)
        WRITE(temchar,'(1a)') TRIM(filename)
      ELSE                          ! All processes write its own ready file
        WRITE(readyfl,'(2a,2I3.3)') TRIM(temchar),'_',loc_x,loc_y
        WRITE(temchar,'(2a,2I3.3)') TRIM(filename),'_',loc_x,loc_y
      END IF

      IF ( myproc == 0 .OR. readyflag > 1 ) THEN

        CALL date_and_time(cdate,ctime,czone,ival)

        CALL getunit( nchout )

        OPEN(UNIT=nchout,FILE=TRIM(readyfl),ACTION ='WRITE',FORM='FORMATTED',STATUS='UNKNOWN')
        WRITE(nchout,'(1x,7a)') TRIM(temchar), ' is ready on ',cdate,'_',ctime,' ',czone
        CLOSE(UNIT=nchout)
        CALL retunit ( nchout )

      END IF

    END IF

    RETURN
  END SUBROUTINE wrf_file_ready

  !###################### definition ###################################

  SUBROUTINE wrf_file_define ( tmplfile, numdigits, filetitle, times,   &
                               refopt, istatus )
    IMPLICIT NONE

    CHARACTER(LEN=*),   INTENT(IN)  :: filetitle
    CHARACTER(LEN=*),   INTENT(IN)  :: tmplfile
    INTEGER,            INTENT(IN)  :: numdigits
    CHARACTER(LEN=19),  INTENT(IN)  :: times
    INTEGER,            INTENT(IN)  :: refopt
    INTEGER,            INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i, j
    INTEGER :: nvars, nvar
    !CHARACTER(LEN=20), ALLOCATABLE :: var_exclude_set(:)
    CHARACTER(LEN=20), ALLOCATABLE :: var_keep_set(:)

    CHARACTER(LEN=20) :: var_addition_set(1,3)
    INTEGER           :: naddition

    INTEGER              :: fHndl_tmp
    CHARACTER(LEN=256)   :: filename, tdfilename
    LOGICAL              :: fix_dim
    CHARACTER(LEN=20)    :: fmtstr
    INTEGER              :: iproc, ilocx, jlocy
    INTEGER              :: ncmpx, ncmpy

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0
    fix_dim = .TRUE.

    IF (hard_copy) RETURN   ! Already have a hard copy, do nothing

    !
    ! CSM3DVAR ask only copyed and analyzed variables
    !

    IF ( MOD(wrfoutopt,100) == 0) THEN   ! define only var_keep_set and copy only var_copy_names
      ! = 0,   only Analyzed fields
      ! = 100, copy a few extra fields

      nvars = nvar_anal+nvar_copy+1
      ALLOCATE( var_keep_set(nvars), STAT = istatus )

      nvar = 0
      DO i = 1, nvar_anal
        nvar = nvar+1
        var_keep_set(nvar) = var_analysis_names(i)
      END DO

      IF (wrfoutopt == 100) THEN
        DO i = 1, nvar_copy
          nvar = nvar+1
          var_keep_set(nvar) = var_copy_names(i)
        END DO
      END IF

      nvar = nvar+1
      var_keep_set(nvar) = 'Times'

    ELSE                       ! define all from input and copy all except for analysis fields

      nvars = nvar_anal+nvar_save+1
      ALLOCATE( var_keep_set(nvars), STAT = istatus )

      nvar = 0
      DO i = 1, nvar_anal
        nvar = nvar+1
        var_keep_set(nvar) = var_analysis_names(i)
      END DO

      !DO i = 1, nvar_save
      !  nvar = nvar+1
      !  var_keep_set(nvar) = var_save_names(i)
      !END DO

      nvar = nvar+1
      var_keep_set(nvar) = 'Times'

    END IF

    naddition = 0
    var_addition_set(:,:) = ' '

    IF (refopt > 0) THEN
      naddition = 1
      var_addition_set(1,1) = 'REFMOSAIC3D'
      var_addition_set(1,2) = 'Radar mosaic'
      var_addition_set(1,3) = 'dBZ'
    END IF

  !---------------------------------------------------------------------
  !
  ! Copy fields from wrfinput templates
  !
  !---------------------------------------------------------------------

    IF (wrfoutopt /= 0) THEN

      IF (fileout) THEN

        WRITE(fmtstr,'(2(a,I0),a)') '(2a,I',numdigits,'.',numdigits,')'

        DO j = 1, nfy_out        ! WRF input template, must in the current working directory "wrfinput_d01"
          DO i = 1, nfx_out
            IF (out_onefile) THEN
              filename = tmplfile
            ELSE
              iproc = ((loc_y-1)*nfy_out+j-1)*nproc_x_out + (loc_x-1)*nfx_out+i - 1
              WRITE(filename,fmt=TRIM(fmtstr)) TRIM(tmplfile),'_',iproc
            END IF

            IF (debug > 0) WRITE(*,'(1x,a,I0,a)') 'Opening '//TRIM(filename)//' for template (',myproc,').'

            CALL open_ncd_wrf_file(filename,'r',fHndl_tmp,istatus)

            IF (wrfoutopt == 100) THEN ! define only var_keep_set and copy only var_copy_names

              CALL copy_wrf_definitions(fHndl_tmp,fHndl_out(i,j),       &
                      .FALSE.,1,1,1,1,nvars,var_keep_set,               &
                      naddition,var_addition_set,                       &
                      filetitle,times,istatus)

              CALL copy_wrfinput_vars(fHndl_tmp,fHndl_out(i,j),.TRUE.,  &
                                     nvar_copy,var_copy_names, istatus)

            ELSE                      ! define all from input and copy all except for analysis fields

              CALL copy_wrf_definitions(fHndl_tmp,fHndl_out(i,j),       &
                      .FALSE.,1,1,1,1,0,var_analysis_names,             &
                      naddition,var_addition_set,                       &
                      filetitle,times,istatus)

              CALL copy_wrfinput_vars(fHndl_tmp,fHndl_out(i,j),.FALSE., &
                                     nvars,var_keep_set, istatus)

            END IF

            CALL close_ncd_wrf_file(fHndl_tmp, istatus)

          END DO
        END DO
      END IF

  !---------------------------------------------------------------------
  !
  ! Copy file definition from input background
  !
  !---------------------------------------------------------------------

    ELSE

      WRITE(fmtstr,'(2(a,I0),a)') '(2a,I',numdigits,'.',numdigits,')'

      ncmpx = MAX(nproc_x/nproc_x_in,1)
      ncmpy = MAX(nproc_y/nproc_y_in,1)

      ! define output file based on tdfilename
      IF (fileout) THEN

        IF (in_onefile) THEN
          tdfilename = tmplfile
        ELSE
          iproc = ((loc_y-1)*nfy_in/ncmpy)*nproc_x_in + (loc_x-1)*nfx_in/ncmpx
          !write(0,'(1x,2I4,a,I4,a,4I4)') loc_x,loc_y,' --> ',iproc,' with ',nfx_in,nfy_in,ncmpx,ncmpy
          WRITE(tdfilename,fmt=TRIM(fmtstr)) TRIM(tmplfile),'_',iproc
        END IF

        IF (nproc_x_in == nproc_x_out .AND. nproc_y_in == nproc_y_out)  &
          fix_dim = .FALSE.

        DO j = 1, nfy_out
          DO i = 1, nfx_out

            IF (debug > 0) WRITE(*,'(1x,a,I0,a)') 'Opening '//TRIM(tdfilename)//' for template (',myproc,').'

            CALL open_ncd_wrf_file(tdfilename,'r',fHndl_tmp,istatus)

            ilocx = (loc_x-1)*nfx_out+i
            jlocy = (loc_y-1)*nfy_out+j

            CALL copy_wrf_definitions(fHndl_tmp,fHndl_out(i,j),         &
                     fix_dim,nproc_x_out, nproc_y_out,ilocx,jlocy,      &
                     nvar,var_keep_set, naddition,var_addition_set,     &
                     filetitle,times,istatus)

            CALL close_ncd_wrf_file(fHndl_tmp, istatus)

          END DO
        END DO
      END IF

    END IF

    DEALLOCATE( var_keep_set )

    RETURN
  END SUBROUTINE wrf_file_define

  !###################### Copy existing file #########################

  SUBROUTINE wrf_file_hard_copy ( nprocx,nprocy,numdigits,              &
                                  infile,outdir,outfile,istatus )
    IMPLICIT NONE

    INTEGER,          INTENT(IN)    :: nprocx,nprocy,numdigits
    CHARACTER(LEN=*), INTENT(IN)    :: infile
    CHARACTER(LEN=*), INTENT(INOUT) :: outdir, outfile
    INTEGER,          INTENT(OUT)   :: istatus

  !---------------------------------------------------------------------

    CHARACTER(LEN=256) :: infilename, outfilename
    INTEGER            :: lenstr

    CHARACTER(LEN=256) :: cmdstr, tmpstr

    LOGICAL            :: fexist, alert
    INTEGER            :: n, nprocs
    CHARACTER(LEN=20)  :: fmtstr

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0
    alert   = .FALSE.

    !IF (outdir == './' .OR. outdir == '.' .OR. outdir == '') THEN
    !  outdir = './output/'
    !END IF
    IF (outdir == '.' .OR. outdir == '') THEN
      outdir = './'
    END IF

    CALL inquiredir(TRIM(outdir),fexist)

    IF( .NOT. fexist ) THEN
      CALL makedir(outdir,istatus)
      WRITE(6,'(5x,a,2(/5x,a))') 'Required output directory <'          &
        //TRIM(outdir)//'> not found.', 'It was created by the program.'
    END IF

    lenstr = LEN_TRIM(outdir)
    IF (outdir(lenstr:lenstr) /= '/') THEN
      WRITE(tmpstr,'(2a)') TRIM(outdir),'/'
    ELSE
      WRITE(tmpstr,'(a)' ) TRIM(outdir)
    END IF

    WRITE(fmtstr,'(2(a,I0),a)') '(2a,I',numdigits,'.',numdigits,')'

    nprocs = nprocx*nprocy
    DO n = 0, nprocs-1
      !
      ! Get infilename
      !
      IF (nprocs > 1) THEN
        WRITE(infilename,fmt=TRIM(fmtstr)) TRIM(infile),'_',n
      ELSE
        infilename = infile
      END IF

      INQUIRE(FILE=TRIM(infilename),EXIST=fexist)
      IF (.NOT. fexist) THEN
        WRITE(6,'(1x,3a)') 'WARNING: inputfile file - ',                  &
                  TRIM(infilename),' does not exist.'
        istatus = -1
        RETURN
      END IF

      !
      ! Get outfilename
      !
      IF (nprocs > 1) THEN
        WRITE(outfilename,fmt=TRIM(fmtstr)) TRIM(tmpstr),TRIM(outfile)//'_',n
      ELSE
        WRITE(outfilename,'(2a)') TRIM(tmpstr), TRIM(outfile)
      END IF

      INQUIRE(FILE=TRIM(outfilename),EXIST=fexist)
      IF (.NOT. fexist) THEN
        !
        ! copy file using linux command
        !
        WRITE(cmdstr,'(4a)')  'cp ',TRIM(infilename),' ',TRIM(outfilename)

        IF (.NOT. alert) THEN

          WRITE(*,'(1x,a)') '=== ********************************* ==='
          WRITE(*,'(1x,a,/,5x,2a)')                                     &
            '    Trying to run command str: ','  $> ',TRIM(cmdstr)
          WRITE(*,'(1x,a,/,5x,a)')                                      &
            '    If Fortran "system" function is not implemented on the system,',  &
                'Users can run this command from command line beforehand.'
          WRITE(*,'(1x,a)') '=== ********************************* ==='
          alert = .TRUE.
        END IF

        IF (debug > 0) WRITE(*,'(1x,a)') 'Copying '//TRIM(infilename)//' to '//TRIM(outfilename)
        CALL unixcmd( TRIM(cmdstr) )
      ELSE
        IF (debug > 0) WRITE(*,'(1x,a)') 'File '//TRIM(infilename)//' exists.'
      END IF
    END DO

    !outfile = outfilename

    RETURN
  END SUBROUTINE wrf_file_hard_copy

  !###################### Fix title in files - OBSOLETE ################

  SUBROUTINE wrf_fix_titles ( filetitle, times, istatus )
    IMPLICIT NONE

    CHARACTER(LEN=*),  INTENT(IN)  :: filetitle
    CHARACTER(LEN=19), INTENT(IN)  :: times
    INTEGER,           INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i, j

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (fileout) THEN
      DO j = 1, nfy_out
        DO i = 1,nfx_out

          CALL wrf_fix_title(fHndl_out(i,j),filetitle,times,istatus)

        END DO
      END DO

    END IF

    RETURN
  END SUBROUTINE wrf_fix_titles

  !###################### Copy existing fields - OBSOLETE ##############

  SUBROUTINE wrf_file_copy ( indirtd, numdigits, timestr, istatus )
    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN)  :: indirtd, timestr
    INTEGER,          INTENT(IN)  :: numdigits
    INTEGER,          INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: ifi, jfi, ifo, jfo
    INTEGER :: i, j, k, n
    INTEGER :: ia, ja, kin,kout
    CHARACTER(LEN=20) :: vname

    INTEGER :: nsize(4),ndims
    CHARACTER(LEN=1) :: stagger

    INTEGER :: istart, iend, jstart, jend, outsize

    INTEGER, ALLOCATABLE :: fHndl_in(:,:)
    REAL,    ALLOCATABLE :: varin(:)
    INTEGER :: iproc
    CHARACTER(LEN=256) :: infilename
    CHARACTER(LEN=20)  :: fmtstr

    INTEGER :: insize

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (filein) THEN

      insize  = nxfin*nyfin*nz

      ALLOCATE( varin(insize),           STAT = istatus )
      ALLOCATE( fHndl_in(nfx_in,nfy_in), STAT = istatus )

      WRITE(fmtstr,'(2(a,I0),a)') '(4a,I',numdigits,'.',numdigits,')'

      DO j = 1, nfy_in            ! WRF history files, should in separate directory for each member
        DO i = 1, nfx_in
          IF (in_onefile) THEN
            WRITE(infilename,'(3a)') TRIM(indirtd),'wrfout_d01_',timestr
          ELSE
            !iproc = (j-1)*nproc_x_out + i-1
            iproc = ((loc_y-1)*nfy_in+j-1)*nproc_x_in + (loc_x-1)*nfx_out+i - 1
            WRITE(infilename,fmt=TRIM(fmtstr)) TRIM(indirtd),'wrfout_d01_',timestr,'_',iproc
          END IF

          IF (debug > 0) WRITE(*,'(1x,a)') 'Opening '//TRIM(infilename)//' for read.'

          CALL open_ncd_wrf_file(infilename,'r',fHndl_in(i,j),istatus)

          IF (istatus < 0) THEN
            WRITE(*,'(1x,3a)')   'File : ', TRIM(infilename), ' not found.'
            CALL arpsstop('ERROR file open operation',1)
          END IF

        END DO
      END DO

    END IF

    DO n = 1, nvar_copy
      vname = var_copy_names(n)  ! Assume all variables are "float", otherwise,
                                 ! should write separate code for "int".

      IF (in_onefile .AND. out_onefile) THEN
        IF (fileout) THEN
          nsize(4) = nxfin*nyfin*nz
          CALL read_wrf_var_real(fHndl_in(1,1),vname,varin,             &
                                 nsize,ndims,stagger,istatus)

          outsize = nsize(3)*nsize(2)*nsize(1)

          CALL write_wrf_var_real(fHndl_out(1,1),vname,varin,outsize,istatus)
        END IF
      ELSE

        IF (filein) THEN
          DO jfi = 1, nfy_in
            DO ifi = 1,nfx_in

              nsize(4) = nxfin*nyfin*nz

              CALL read_wrf_var_real(fHndl_in(ifi,jfi),vname,varin,     &
                                     nsize,ndims,stagger,istatus)

              ! Convert varin to var
              IF (in_split) THEN

                DO k = 1, nsize(3)      ! nfx_in & nfy_in must be 1
                  DO j = isy, MIN(iey,nsize(2))
                    DO i = isx, MIN(iex,nsize(1))
                      kin = i + (j-1)*nsize(1)+(k-1)*nsize(1)*nsize(2)
                      var(i,j,k) = varin(kin)
                    END DO
                  END DO
                END DO

              ELSE

                DO k = 1, nsize(3)
                  DO j = 1, nsize(2)
                    DO i = 1, nsize(1)
                      kin = i + (j-1)*nsize(1)+(k-1)*nsize(1)*nsize(2)
                      ia  = i + (ifi-1)*(nxfin-1)     ! assume nx_wrf is evenly distributed
                      ja  = j + (jfi-1)*(nyfin-1)     ! among patches
                      var(ia,ja,k) = varin(kin)
                    END DO
                  END DO
                END DO

              END IF

            END DO
          END DO
        END IF

        IF (fileout) THEN

          DO jfo = 1, nfy_out
            DO ifo = 1,nfx_out

              istart = osx; iend = oex-1
              jstart = osy; jend = oey-1
              IF (stagger == 'X' .AND. loc_x == nproc_x ) THEN    ! assume nfx_out = nfy_out = 1
                iend = oex
              END IF

              IF (stagger == 'Y' .AND. loc_y == nproc_y ) THEN    ! assume nfx_out = nfy_out = 1
                jend = oey
              END IF

              DO k = 1, nsize(3)
                DO j = jstart, jend
                  DO i = istart, iend
                    kout = (k-1)*(jend-jstart+1)*(iend-istart+1)+(j-1)*(iend-istart+1)+i
                    varout(kout) = var(i,j,k)
                  END DO
                END DO
              END DO

              outsize = nsize(3)*(jend-jstart+1)*(iend-istart+1)

              CALL write_wrf_var_real(fHndl_out(ifo,jfo),vname,varout,outsize,istatus)
            END DO
          END DO

        END IF

      END IF
    END DO

    IF (filein) THEN
      DO j = 1, nfy_in
        DO i = 1, nfx_in
          CALL close_ncd_wrf_file(fHndl_in(i,j), istatus)
        END DO
      END DO

      DEALLOCATE( varin )
      DEALLOCATE( fHndl_in )

    END IF

    RETURN
  END SUBROUTINE wrf_file_copy

  !###################### WRITE 3d field ###############################

  SUBROUTINE wrf_file_write3d ( vname,vardta,stagger,istatus )
    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN)  :: vname
    REAL,             INTENT(IN)  :: vardta(nx,ny,nz)
    CHARACTER(LEN=1), INTENT(IN)  :: stagger
    INTEGER,          INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j,k, iloc, jloc

    INTEGER :: istart, iend, jstart, jend, kstart, kend

    INTEGER :: kout, outsize, nxfsize, nyfsize

    INTEGER :: outstart(4), outcount(4), srcproc, itag

    REAL, ALLOCATABLE :: dtaout(:)
    INTEGER :: nxpout, nypout, nzpout, kin

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (out_onefile) THEN
      !kstart = 1; kend = nz-1
      !IF (stagger == 'Z') kend = nz
      !
      !jstart = 1; jend = ny-1
      !IF (stagger == 'Y' .AND. loc_y == nproc_y ) jend = ny
      !
      !istart = 1; iend = nx-1
      !IF (stagger == 'X' .AND. loc_x == nproc_x ) iend = nx
      !
      !DO k = kstart, kend
      !  DO j = jstart, jend
      !    DO i = istart, iend
      !      kout = (k-kstart)*(jend-jstart+1)*(iend-istart+1)+(j-jstart)*(iend-istart+1)+i-istart+1
      !      varout(kout) = vardta(i,j,k)
      !    END DO
      !  END DO
      !END DO
      !
      !outcount(1) = iend-istart+1
      !outcount(2) = jend-jstart+1
      !outcount(3) = kend-kstart+1
      !outcount(4) = 1
      !
      !outstart(1) = (loc_x-1)*(nx-1)+1
      !outstart(2) = (loc_y-1)*(ny-1)+1
      !outstart(3) = 1
      !outstart(4) = 1
      !
      !CALL inctag
      !itag = gentag
      !
      !IF (myproc == 0) THEN
      !
      !  outsize = outcount(1)*outcount(2)*outcount(3)
      !  CALL write_wrf_vara_real(fHndl_out(1,1),vname,varout,           &
      !                           outsize,outstart,outcount,istatus)
      !
      !  DO j = 1, nproc_y
      !    DO i = 1, nproc_x
      !      srcproc = (j-1)*nproc_x + i-1
      !      IF (srcproc /= 0) THEN
      !        CALL mprecvi(outstart,4,srcproc,itag+10,istatus)
      !        CALL mprecvi(outcount,4,srcproc,itag+20,istatus)
      !        outsize = outcount(1)*outcount(2)*outcount(3)
      !        CALL mprecvr(varout,outsize,srcproc,itag+30,istatus)
      !
      !        CALL write_wrf_vara_real(fHndl_out(1,1),vname,varout,     &
      !                                 outsize,outstart,outcount,istatus)
      !      END IF
      !    END DO
      !  END DO
      !
      !ELSE
      !
      !  CALL mpsendi(outstart,4,0,itag+10,istatus)
      !  CALL mpsendi(outcount,4,0,itag+20,istatus)
      !  outsize = outcount(1)*outcount(2)*outcount(3)
      !  CALL mpsendr(varout,outsize,0,itag+30,istatus)
      !
      !END IF

      outstart(1) = (loc_x-1)*(nx-1)
      outstart(2) = (loc_y-1)*(ny-1)
      outstart(3) = 1
      outstart(4) = 1

      kstart = 1; kend = nz-1
      IF (stagger == 'Z') kend = nz

      jstart = 1; jend = ny-1; nyfsize = nyfout-1
      IF (stagger == 'Y' .AND. loc_y == nproc_y) jend = ny
      IF (stagger == 'Y')  nyfsize = nyfout

      istart = 1; iend = nx-1; nxfsize = nxfout-1
      IF (stagger == 'X' .AND. loc_x == nproc_x ) iend = nx
      IF (stagger == 'X')  nxfsize = nxfout


      nxpout = (iend-istart+1)
      nypout = (jend-jstart+1)
      nzpout = (kend-kstart+1)
      ALLOCATE(dtaout(nx*ny*nz), STAT = istatus)

      DO k = kstart, kend
        DO j = jstart, jend
          DO i = istart, iend
            kout = (k-kstart)*nypout*nxpout+(j-jstart)*nxpout+i-istart+1
            dtaout(kout) = vardta(i,j,k)
          END DO
        END DO
      END DO

      outcount(1) = nxpout
      outcount(2) = nypout
      outcount(3) = nzpout
      outcount(4) = 1

      CALL inctag
      itag = gentag

      IF (myproc == 0) THEN

        DO k = kstart, kend
          DO j = jstart, jend
            DO i = istart, iend
              kout = (k-kstart)*nyfsize*nxfsize+(j-jstart+outstart(2))*nxfsize+i-istart+1+outstart(1)
              varout(kout) = vardta(i,j,k)
              !if (kout == 451 .OR. kout == 452) write(*,*) TRIM(vname),0,i,j,k,' -> ',kout, nxfsize,nyfsize
            END DO
          END DO
        END DO

        DO jloc = 1, nproc_y
          DO iloc = 1, nproc_x
            srcproc = (jloc-1)*nproc_x + iloc-1
            IF (srcproc /= 0) THEN
              CALL mprecvi(outstart,4,srcproc,itag+10,istatus)
              CALL mprecvi(outcount,4,srcproc,itag+20,istatus)
              outsize = outcount(1)*outcount(2)*outcount(3)
              CALL mprecvr(dtaout,outsize,srcproc,itag+30,istatus)

              !write(0,*) 'Filling ',srcproc,' with ',outcount(1),outcount(2),' expects'
              DO k = 1, outcount(3)
                DO j = 1, outcount(2)
                  DO i = 1, outcount(1)
                    kout = (k-1)*nyfsize*nxfsize+(j-1+outstart(2))*nxfsize+i+outstart(1)
                    kin  = (k-1)*outcount(2)*outcount(1)+(j-1)*outcount(1)+i
                    varout(kout) = dtaout(kin)
                    !if (kout == 451 .OR. kout == 452) write(*,*) TRIM(vname),srcproc,kin,' -> ',kout,outstart(1:2),outcount(1:2)
                  END DO
                END DO
              END DO

            END IF
          END DO
        END DO

        !
        ! Finally, write the dataset
        !
        outsize = nxfsize*nyfsize*outcount(3)
        outstart(:) = 1
        outcount(1) = nxfsize
        outcount(2) = nyfsize

        WRITE(*,'(1x,3a)') 'Writing ',TRIM(vname),' ......'
        CALL write_wrf_vara_real(fHndl_out(1,1),vname,varout,           &
                                 outsize,outstart,outcount,istatus)

      ELSE

        CALL mpsendi(outstart,4,0,itag+10,istatus)
        CALL mpsendi(outcount,4,0,itag+20,istatus)
        outsize = outcount(1)*outcount(2)*outcount(3)
        CALL mpsendr(dtaout,outsize,0,itag+30,istatus)

      END IF

      DEALLOCATE(dtaout)

    ELSE

      kstart = 1; kend = nz-1
      IF (stagger == 'Z') kend = nz

      jstart = 1; jend = ny-1
      IF (stagger == 'Y' .AND. loc_y == nproc_y) jend = ny

      istart = 1; iend = nx-1
      IF (stagger == 'X' .AND. loc_x == nproc_x ) iend = nx

      DO k = kstart, kend
        DO j = jstart, jend
          DO i = istart, iend
            kout = (k-kstart)*(jend-jstart+1)*(iend-istart+1)+(j-jstart)*(iend-istart+1)+i-istart+1
            varout(kout) = vardta(i,j,k)
          END DO
        END DO
      END DO

      outsize = (kend-kstart+1)*(jend-jstart+1)*(iend-istart+1)
      CALL write_wrf_var_real(fHndl_out(1,1),vname,varout,outsize,istatus)

    END IF

    RETURN
  END SUBROUTINE wrf_file_write3d

  !###################### WRITE 3d field ###############################
  SUBROUTINE wrf_file_write2d ( vname,vardta,stagger,istatus )
    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN)  :: vname
    REAL,             INTENT(IN)  :: vardta(nx,ny)
    CHARACTER(LEN=1), INTENT(IN)  :: stagger
    INTEGER,          INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i,j,iloc,jloc

    INTEGER :: istart, iend, jstart, jend

    INTEGER :: kout, outsize, nxfsize, nyfsize

    INTEGER :: outstart(4), outcount(4), srcproc, itag

    REAL, ALLOCATABLE :: dtaout(:)
    INTEGER :: nxpout, nypout, nzpout, kin

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (out_onefile) THEN

      !jstart = 1; jend = ny-1
      !IF (stagger == 'Y' .AND. loc_y == nproc_y ) jend = ny
      !
      !istart = 1; iend = nx-1
      !IF (stagger == 'X' .AND. loc_x == nproc_x ) iend = nx
      !
      !DO j = jstart, jend
      !  DO i = istart, iend
      !    kout = (j-jstart)*(iend-istart+1)+i-istart+1
      !    varout(kout) = vardta(i,j)
      !  END DO
      !END DO
      !
      !outcount(1) = iend-istart+1
      !outcount(2) = jend-jstart+1
      !outcount(3) = 1
      !outcount(4) = 1
      !
      !outstart(1) = (loc_x-1)*(nx-1)+1
      !outstart(2) = (loc_y-1)*(ny-1)+1
      !outstart(3) = 1
      !outstart(4) = 1
      !
      !CALL inctag
      !itag = gentag
      !
      !IF (myproc == 0) THEN
      !
      !  outsize = outcount(1)*outcount(2)*outcount(3)
      !  CALL write_wrf_vara_real(fHndl_out(1,1),vname,varout,           &
      !                           outsize,outstart,outcount,istatus)
      !
      !  DO j = 1, nproc_y
      !    DO i = 1, nproc_x
      !      srcproc = (j-1)*nproc_x + i-1
      !      IF (srcproc /= 0) THEN
      !        CALL mprecvi(outstart,4,srcproc,itag+10,istatus)
      !        CALL mprecvi(outcount,4,srcproc,itag+20,istatus)
      !        outsize = outcount(1)*outcount(2)*outcount(3)
      !        CALL mprecvr(varout,outsize,srcproc,itag+30,istatus)
      !
      !        CALL write_wrf_vara_real(fHndl_out(1,1),vname,varout,     &
      !                                 outsize,outstart,outcount,istatus)
      !      END IF
      !    END DO
      !  END DO
      !
      !ELSE
      !
      !  CALL mpsendi(outstart,4,0,itag+10,istatus)
      !  CALL mpsendi(outcount,4,0,itag+20,istatus)
      !  outsize = outcount(1)*outcount(2)*outcount(3)
      !  CALL mpsendr(varout,outsize,0,itag+30,istatus)
      !
      !END IF

      outstart(1) = (loc_x-1)*(nx-1)
      outstart(2) = (loc_y-1)*(ny-1)
      outstart(3) = 1
      outstart(4) = 1

      jstart = 1; jend = ny-1; nyfsize = nyfout-1
      IF (stagger == 'Y' .AND. loc_y == nproc_y ) jend = ny
      IF (stagger == 'Y')  nyfsize = nyfout

      istart = 1; iend = nx-1; nxfsize = nxfout-1
      IF (stagger == 'X' .AND. loc_x == nproc_x ) iend = nx
      IF (stagger == 'X' ) nxfsize = nxfout


      nxpout = (iend-istart+1)
      nypout = (jend-jstart+1)
      nzpout = 1
      ALLOCATE(dtaout(nx*ny*nz), STAT = istatus)

      DO j = jstart, jend
        DO i = istart, iend
          kout = (j-jstart)*nxpout+i-istart+1
          dtaout(kout) = vardta(i,j)
        END DO
      END DO

      outcount(1) = iend-istart+1
      outcount(2) = jend-jstart+1
      outcount(3) = 1
      outcount(4) = 1


      CALL inctag
      itag = gentag

      IF (myproc == 0) THEN

        DO j = jstart, jend
          DO i = istart, iend
            kout = (j-jstart+outstart(2))*nxfsize+i-istart+1+outstart(1)
            varout(kout) = vardta(i,j)
          END DO
        END DO

        DO jloc = 1, nproc_y
          DO iloc = 1, nproc_x
            srcproc = (jloc-1)*nproc_x + iloc-1
            IF (srcproc /= 0) THEN
              CALL mprecvi(outstart,4,srcproc,itag+10,istatus)
              CALL mprecvi(outcount,4,srcproc,itag+20,istatus)
              outsize = outcount(1)*outcount(2)*outcount(3)
              CALL mprecvr(dtaout,outsize,srcproc,itag+30,istatus)

              DO j = 1, outcount(2)
                DO i = 1, outcount(1)
                  kout = (j-1+outstart(2))*nxfsize+ i+outstart(1)
                  kin  = (j-1)*outcount(1)+i
                  varout(kout) = dtaout(kin)
                END DO
              END DO

            END IF
          END DO
        END DO

        !
        ! Finally, write the dataset
        !
        outsize = nxfsize*nyfsize*outcount(3)
        outstart(:) = 1
        outcount(1) = nxfsize
        outcount(2) = nyfsize

        WRITE(*,'(1x,3a)') 'Writing ',TRIM(vname),' ......'
        CALL write_wrf_vara_real(fHndl_out(1,1),vname,varout,           &
                                 outsize,outstart,outcount,istatus)


      ELSE

        CALL mpsendi(outstart,4,0,itag+10,istatus)
        CALL mpsendi(outcount,4,0,itag+20,istatus)
        outsize = outcount(1)*outcount(2)*outcount(3)
        CALL mpsendr(dtaout,outsize,0,itag+30,istatus)

      END IF

      DEALLOCATE(dtaout)

    ELSE

      jstart = 1; jend = ny-1
      IF (stagger == 'Y' .AND. loc_y == nproc_y) jend = ny

      istart = 1; iend = nx-1
      IF (stagger == 'X' .AND. loc_x == nproc_x) iend = nx

      DO j = jstart, jend
        DO i = istart, iend
          kout = (j-jstart)*(iend-istart+1)+i-istart+1
          varout(kout) = vardta(i,j)
        END DO
      END DO

      outsize = (jend-jstart+1)*(iend-istart+1)
      CALL write_wrf_var_real(fHndl_out(1,1),vname,varout,outsize,istatus)

    END IF

    RETURN
  END SUBROUTINE wrf_file_write2d

  !###################### finalization #################################

  SUBROUTINE wrf_io_final ( istatus )
    IMPLICIT NONE

    INTEGER, INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    INTEGER :: i, j

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    IF (fileout) THEN

      DO j = 1, nfy_out
        DO i = 1, nfx_out
          CALL close_ncd_wrf_file(fHndl_out(i,j), istatus)
        END DO
      END DO

    END IF

    DEALLOCATE( fHndl_out )
    DEALLOCATE( var, varout)

    RETURN
  END SUBROUTINE wrf_io_final

  SUBROUTINE wrf_get_dimensions( filein,numdigits,nx,ny,nz,nzsoil,nstyps,nscalar, &
                             P_QC,P_QR,P_QI,P_QS,P_QG,P_QH,             &
                             P_NC,P_NR,P_NI,P_NS,P_NG,P_NH,             &
                                  P_ZR,P_ZI,P_ZS,P_ZG,P_ZH,qnames,      &
                             mapproj,ctrlat,ctrlon,trulat1,trulat2,trulon,&
                             dx,dy,istatus )
    IMPLICIT NONE

    CHARACTER(LEN=256),INTENT(IN)  :: filein
    INTEGER,           INTENT(IN)  :: numdigits
    INTEGER,           INTENT(OUT) :: nx, ny, nz, nzsoil, nstyps, nscalar
    INTEGER,           INTENT(OUT) :: P_QC,P_QR,P_QI,P_QS,P_QG,P_QH,     &
                                      P_NC,P_NR,P_NI,P_NS,P_NG,P_NH,     &
                                      P_ZR,P_ZI,P_ZS,P_ZG,P_ZH
    CHARACTER(LEN=40), INTENT(OUT) :: qnames(20)

    INTEGER,           INTENT(OUT) :: mapproj
    REAL,              INTENT(OUT) :: ctrlat, ctrlon, trulat1, trulat2, trulon
    REAL,              INTENT(OUT) :: dx, dy
    INTEGER,           INTENT(OUT) :: istatus

  !---------------------------------------------------------------------

    CHARACTER(LEN=256) :: filename
    INTEGER :: P_NCCN
    INTEGER :: ncid
    INTEGER :: nxin, nyin, nzin, iproj

    CHARACTER(LEN=20)  :: fmtstr

  !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    istatus = 0

    WRITE(fmtstr,'(2(a,I0),a)') '(2a,I',numdigits,'.',numdigits,')'

    IF (nproc_x_in == 1 .AND. nproc_y_in == 1) THEN
      filename = filein
    ELSE
      WRITE(filename,fmt=TRIM(fmtstr)) TRIM(filein),'_',nproc_x_in*nproc_y_in-1
    END IF

    CALL open_ncd_wrf_file(filename, 'r', ncid, istatus)

    CALL get_wrf_dimensions( ncid, nxin,nyin,nzin,nzsoil,nstyps,nscalar,&
                             P_QC,P_QR,P_QI,P_QS,P_QG,P_QH,             &
                             P_NC,P_NR,P_NI,P_NS,P_NG,P_NH,             &
                                  P_ZR,P_ZI,P_ZS,P_ZG,P_ZH,             &
                             P_NCCN,qnames,                             &
                             iproj,ctrlat,ctrlon,trulat1,trulat2,trulon,&
                             dx,dy, istatus )

    ! map projection, dx dy etc.
    IF(iproj == 0) THEN        ! No projection
      mapproj = 0
    ELSE IF(iproj == 1) THEN   ! LAMBERT CONFORMAL
      mapproj = 2
    ELSE IF(iproj == 2) THEN   ! POLAR STEREOGRAPHIC
      mapproj = 1
    ELSE IF(iproj == 3) THEN   ! MERCATOR
      mapproj = 3
    ELSE
      WRITE(6,*) 'ERROR: Unknown map projection, ', iproj
      istatus = -555
      RETURN
    END IF

    IF (nproc_x_in == 1 .AND. nproc_y_in == 1) THEN
      nx = nxin + 2
      ny = nyin + 2
      nz = nzin + 2
    ELSE
      nx = (nxin-1) * nproc_x_in + 1 + 2
      ny = (nyin-1) * nproc_y_in + 1 + 2
      nz = nzin + 2
    END IF

    nx = (nx-3)/nproc_x + 3   ! return local sizes
    ny = (ny-3)/nproc_y + 3

    CALL close_ncd_wrf_file(ncid,istatus)

    RETURN
  END SUBROUTINE wrf_get_dimensions

  !********************* PRIVATE SUBROUTINES ***************************

END MODULE wrfio_api
