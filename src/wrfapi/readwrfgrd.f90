!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE READWRFGRD                 ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE readwrfgrd(nprocx_in,nprocy_in,                              &
                    wrf_file,nx_wrf,ny_wrf,nz_wrf,nt,                   &
                    x_wrf,y_wrf,zp_wrf,xs_wrf,ys_wrf,zps_wrf,           &
                    p_wrf,pt_wrf,u_wrf,v_wrf,w_wrf,qv_wrf,istatus)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Read in WRF data array for radremap
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  05/06/2014.
!
!  MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------
!
!
!  INPUT:
!
!    wrf_file WRF file name
!    nx_wrf   Number of grid points in the x-direction (east/west)
!    ny_wrf   Number of grid points in the y-direction (north/south)
!    nz_wrf   Number of grid points in the vertical
!
!  OUTPUT:
!
!    x_wrf    x coordinate of grid points in physical/comp. space (m)
!    y_wrf    y coordinate of grid points in physical/comp. space (m)
!    zp_wrf   Vertical coordinate of grid points in physical space(m)
!    xs_wrf   x coordinate of grid points in physical/comp. space (m)
!    ys_wrf   y coordinate of grid points in physical/comp. space (m)
!    zps_wrf  Vertical coordinate of grid points in physical space
!
!    u_wrf    x component of velocity at all time levels (m/s)
!    v_wrf    y component of velocity at all time levels (m/s)
!    w_wrf    Vertical component of Cartesian velocity
!             at all time levels (m/s)
!    wcont    Contravariant vertical velocity (m/s)
!    pt_wrf   Perturbation potential temperature at all time levels
!             (K)
!    p_wrf    Perturbation pressure at all time levels (Pascal)
!    qv_wrf   Water vapor specific humidity at all time levels
!             (kg/kg)
!
!    istatus
!
!  WORK ARRAYS:
!
!-----------------------------------------------------------------------
!
  USE model_precision

  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'mp.inc'            ! Message passing parameters.
  INCLUDE 'grid.inc'
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  INTEGER, INTENT(IN) :: nprocx_in, nprocy_in

  CHARACTER(LEN=256) :: wrf_file

  INTEGER :: nx_wrf,ny_wrf,nz_wrf  ! The number of grid points in 3
                                   ! directions
  INTEGER :: nt

  REAL :: x_wrf(nx_wrf)            ! x-coord. of the physical and compu-
                                   ! tational grid. Defined at u-point.
  REAL :: y_wrf(ny_wrf)            ! y-coord. of the physical and compu-
                                   ! tational grid. Defined at v-point.
  REAL :: zp_wrf(nx_wrf,ny_wrf,nz_wrf)
  REAL :: xs_wrf(nx_wrf)
  REAL :: ys_wrf(ny_wrf)
  REAL :: zps_wrf(nx_wrf,ny_wrf,nz_wrf)     ! Physical height coordinate defined at
                                            ! w-point of the staggered grid.

  REAL :: u_wrf (nx_wrf,ny_wrf,nz_wrf) ! Total u-velocity (m/s)
  REAL :: v_wrf (nx_wrf,ny_wrf,nz_wrf) ! Total v-velocity (m/s)
  REAL :: w_wrf (nx_wrf,ny_wrf,nz_wrf) ! Total w-velocity (m/s)
  REAL :: pt_wrf(nx_wrf,ny_wrf,nz_wrf) ! Perturbation potential temperature (K)
  REAL :: p_wrf (nx_wrf,ny_wrf,nz_wrf) ! Perturbation pressure (Pascal)

  REAL :: qv_wrf(nx_wrf,ny_wrf,nz_wrf) ! Water vapor specific humidity (kg/kg)

  INTEGER, INTENT(OUT) :: istatus
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k

  INTEGER :: iproj_wrf
  REAL    :: scale_wrf,latnot_wrf(2),trlon_wrf
  REAL    :: ctrlat_wrf,ctrlon_wrf,dx_wrf,dy_wrf

  REAL :: x0_wrf, y0_wrf, xctr, yctr
  REAL :: xsub0,ysub0

  INTEGER :: nxlg_wrf, nylg_wrf, nxd_wrf, nyd_wrf
  LOGICAL :: multifile, patch_split

  INTEGER :: ncmprx, ncmpry

  CHARACTER(LEN=256) :: message
  CHARACTER(LEN=19 ) :: datestr
  INTEGER            :: itime, io_form, numdigit

  INTEGER,  ALLOCATABLE :: fHndl(:,:)
  REAL(SP), ALLOCATABLE :: temd(:,:,:)
  REAL(P),  ALLOCATABLE :: var_wrf(:,:,:)
  REAL(P),  ALLOCATABLE :: lat_wrf(:,:), lon_wrf(:,:)
  REAL(P),  ALLOCATABLE :: tem2d1(:,:), tem2d2(:,:)

  REAL(P),  PARAMETER  :: epsl = 10E-3
  REAL(P),  PARAMETER  :: grav = 9.81

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
  istatus = 0

!-----------------------------------------------------------------------
!
! This subroutine handles 6 possible readings
!
! 1. One WRF file in no-mpi mode (just read);                         - multifile = .FALSE., patch_split = .FALSE.
! 2. One WRF file in mpi mode (split);                                - multifile = .FALSE., patch_split = .TRUE.
!
! 3. Split WRF files in no-mpi mode (join);                           + multifile = .TRUE., patch_split = .FALSE.
! 4. Split WRF files with same number of processes (just read);       + multifile = .TRUE., patch_split = .FALSE.
! 5. Split WRF files with less number of processes (read and join);   + multifile = .TRUE., patch_split = .FALSE.
! 6. Split WRF files with more number of processes (read and split);  + multifile = .TRUE., patch_split = .TRUE.
!
!-----------------------------------------------------------------------

  nproc_x_in = nprocx_in
  nproc_y_in = nprocy_in
  IF (nproc_x_in == 1 .AND. nproc_y_in == 1) THEN
    multifile = .FALSE.
    ncmprx = 1
    ncmprx = 1
  ELSE
    multifile = .TRUE.
  END IF

  IF (nproc_x_in >= nproc_x .AND. nproc_y_in >= nproc_y) THEN
    patch_split = .FALSE.
    IF (mod(nproc_x_in,nproc_x) /= 0 .OR. mod(nproc_y_in,nproc_y) /= 0) THEN
      message = 'ERROR: nproc_x_in (nproc_y_in) must be a multiple of nproc_x (nproc_y).'
      istatus = -1
    ELSE
      ncmprx = nproc_x_in / nproc_x
      ncmpry = nproc_y_in / nproc_y
    END IF
  ELSE IF (nproc_x_in <= nproc_x .AND. nproc_y_in <= nproc_y) THEN
    patch_split = .TRUE.
    IF (mod(nproc_x,nproc_x_in) /= 0 .OR. mod(nproc_y,nproc_y_in) /= 0) THEN
      message = 'ERROR: nproc_x_in (nproc_y_in) must be a factor of nproc_x (nproc_y).'
      istatus = -2
    ELSE
      ncmprx = nproc_x / nproc_x_in
      ncmpry = nproc_y / nproc_y_in
    END IF
  ELSE
    message = 'ERROR: unsupported combination of nproc_x_in and nproc_x, or nproc_y_in and nproc_y.'
    istatus = -3
  END IF

  9999 CONTINUE

  IF (istatus /= 0) THEN
    WRITE(*,'(1x,a,I4)')       TRIM(message),istatus
    WRITE(*,'(7x,2(a,I4))') ' nproc_x_in = ',nproc_x_in, ', nproc_x = ', nproc_x
    WRITE(*,'(7x,2(a,I4))') ' nproc_y_in = ',nproc_y_in, ', nproc_y = ', nproc_y
    istatus = -1
    !RETURN
    CALL arpsstop('ERROR mpi parameters in Vwrfgrdvar',1)
  END IF

!-----------------------------------------------------------------------
!
! Dimension maps
!
!-----------------------------------------------------------------------

  nxlg_wrf = (nx_wrf-1)*nproc_x + 1     ! WRF domain sizes
  nylg_wrf = (ny_wrf-1)*nproc_y + 1

  IF (multifile) THEN
    nxd_wrf = (nxlg_wrf-1) / nproc_x_in + 1
    nyd_wrf = (nylg_wrf-1) / nproc_y_in + 1
  ELSE
    nxd_wrf = nxlg_wrf    ! dataset size in the file, staggered
    nyd_wrf = nylg_wrf
  END IF

  IF (patch_split) THEN
    !
    ! It is required that (nxd_wrf-1) is dividable with ncmprx
    !
    IF (MOD(nxd_wrf-1,ncmprx) /= 0 .OR. MOD(nyd_wrf-1,ncmpry) /= 0) THEN
      istatus = -4
      message = 'ERROR: dimension sizes does not fit with the number of processes.'
      GOTO 9999
    END IF

  END IF

  ALLOCATE(fHndl(ncmprx,ncmpry), STAT = istatus)

  ALLOCATE(var_wrf (nx_wrf, ny_wrf, nz_wrf), STAT = istatus)
  ALLOCATE(temd    (nxd_wrf,nyd_wrf,nz_wrf), STAT = istatus)

  ALLOCATE(lat_wrf (nx_wrf,ny_wrf),    STAT = istatus)
  ALLOCATE(lon_wrf (nx_wrf,ny_wrf),    STAT = istatus)
  ALLOCATE(tem2d1  (nx_wrf,ny_wrf),    STAT = istatus)
  ALLOCATE(tem2d2  (nx_wrf,ny_wrf),    STAT = istatus)

  io_form = 7   ! hard-coded for 7 at present
  numdigit = 4  ! hard-coded for 4 at present
  CALL open_wrf_files(wrf_file,io_form,patch_split,multifile,1,         &
                     ncmprx,ncmpry,numdigit,fHndl)


!-----------------------------------------------------------------------
! Initialize data array
!-----------------------------------------------------------------------

  datestr = '2012-10-25_16:22:00'
  itime   = nt

!-----------------------------------------------------------------------
!
!  Horizontal wind
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'U','X',                                &
                  nx_wrf,ny_wrf,nz_wrf,u_wrf,                           &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'V','Y',                                &
                  nx_wrf,ny_wrf,nz_wrf,v_wrf,                           &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

!-----------------------------------------------------------------------
!
!  Vertical wind
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'W','Z',                                &
                  nx_wrf,ny_wrf,nz_wrf,w_wrf,                           &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)


!-----------------------------------------------------------------------
!
! Read potential temperature
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'T','0',                                &
                  nx_wrf,ny_wrf,nz_wrf,pt_wrf,                          &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  pt_wrf = pt_wrf+300.0

!-----------------------------------------------------------------------
!
! Pressure
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'P','0',                                &
                  nx_wrf,ny_wrf,nz_wrf,p_wrf,                           &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'PB','0',                               &
                  nx_wrf,ny_wrf,nz_wrf,var_wrf,                         &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  p_wrf(:,:,:) = p_wrf(:,:,:) + var_wrf(:,:,:)

!-----------------------------------------------------------------------
!
! Physical height (save for setting zp, Perturbation only here)
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'PH','Z',                               &
                  nx_wrf,ny_wrf,nz_wrf,zp_wrf,                          &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  ! Geopotential Height
  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'PHB','Z',                              &
                  nx_wrf,ny_wrf,nz_wrf,var_wrf,                         &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  zp_wrf(:,:,:) = (var_wrf(:,:,:) + zp_wrf(:,:,:)) / grav

  !
  !  Move zp_ext field onto the scalar grid.
  !
  DO k=1,nz_wrf-1
    DO j=1,ny_wrf
      DO i=1,nx_wrf
        zps_wrf(i,j,k)=0.5*(zp_wrf(i,j,k)+zp_wrf(i,j,k+1))
      END DO
    END DO
  END DO
  DO j=1,ny_wrf
    DO i=1,nx_wrf
      zps_wrf(i,j,nz_wrf)=(2.*zps_wrf(i,j,nz_wrf-1))                    &
                             -zps_wrf(i,j,nz_wrf-2)
    END DO
  END DO

!-----------------------------------------------------------------------
!
! Read water vapor mixing ratio
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'QVAPOR','0',                           &
                  nx_wrf,ny_wrf,nz_wrf,qv_wrf,                          &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  ! Convert QV to specific humidity
  qv_wrf(:,:,:) = qv_wrf(:,:,:)/(1+qv_wrf(:,:,:))

  WHERE(qv_wrf < 0.0) qv_wrf = 0.0
  WHERE(qv_wrf > 1.0) qv_wrf = 1.0

!-----------------------------------------------------------------------
!
! Get WRF attributes
!
!-----------------------------------------------------------------------

  CALL get_wrf_att( fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,  &
                    iproj_wrf,scale_wrf,trulat1,trulat2,trlon_wrf,        &
                    ctrlat_wrf,ctrlon_wrf,dx_wrf,dy_wrf,istatus )

  latnot_wrf(1) = trulat1
  latnot_wrf(2) = trulat2

  IF(iproj_wrf == 1) THEN        ! LAMBERT CONFORMAL
    mapproj = 2
  ELSE IF(iproj_wrf == 2) THEN   ! POLAR STEREOGRAPHIC
    mapproj = 1
  ELSE IF(iproj_wrf == 3) THEN   ! MERCATOR
    mapproj = 3
  ELSE
    WRITE(6,*) 'Unknown map projection, ', iproj_wrf
    istatus = -555
    RETURN
    !STOP
  END IF

  !
  ! Set ARPS grid variable in grid.inc'
  !
  !mapproj
  sclfct  = scale_wrf
  !trulat1
  !trulat2
  trulon  = trlon_wrf
  ctrlat  = ctrlat_wrf
  ctrlon  = ctrlon_wrf
  dx      = dx_wrf
  dy      = dy_wrf

!-----------------------------------------------------------------------
!
! Set up WRF map projection
!
!-----------------------------------------------------------------------

  CALL setmapr(mapproj,scale_wrf,latnot_wrf,trlon_wrf)
  CALL lltoxy(1,1,ctrlat_wrf,ctrlon_wrf,xctr,yctr)
  x0_wrf=xctr - 0.5*(nxlg_wrf-1)*dx_wrf
  y0_wrf=yctr - 0.5*(nylg_wrf-1)*dy_wrf
  CALL setorig(1,x0_wrf,y0_wrf)

  xsub0 = dx_wrf * (nx_wrf-1) * (loc_x-1)
  ysub0 = dy_wrf * (ny_wrf-1) * (loc_y-1)

  DO i=1,nx_wrf
    x_wrf(i)=(i-1)*dx_wrf+xsub0
  END DO
  DO j=1,ny_wrf
    y_wrf(j)=(j-1)*dy_wrf+ysub0
  END DO

  DO i=1,nx_wrf-1
    xs_wrf(i)=0.5*(x_wrf(i)+x_wrf(i+1))
  END DO
  xs_wrf(nx_wrf)=2.*xs_wrf(nx_wrf-1)-xs_wrf(nx_wrf-2)
  DO j=1,ny_wrf-1
    ys_wrf(j)=0.5*(y_wrf(j)+y_wrf(j+1))
  END DO
  ys_wrf(ny_wrf)=2.*ys_wrf(ny_wrf-1)-ys_wrf(ny_wrf-2)

  CALL xytoll(nx_wrf,ny_wrf,xs_wrf,ys_wrf,lat_wrf,lon_wrf)

!-----------------------------------------------------------------------
!
!  Check whether the latitude and longitude are consistent with the data
!
!-----------------------------------------------------------------------

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                datestr,itime,'XLAT','0',                               &
                nx_wrf,ny_wrf,tem2d1,                                   &
                nxd_wrf,nyd_wrf,temd,istatus)

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                datestr,itime,'XLONG','0',                              &
                nx_wrf,ny_wrf,tem2d2,                                   &
                nxd_wrf,nyd_wrf,temd,istatus)

  DO j = 1,ny_wrf-1
    DO i = 1,nx_wrf -1
      IF( ABS(lat_wrf(i,j)-tem2d1(i,j)) > epsl .OR.                     &
          ABS(lon_wrf(i,j)-tem2d2(i,j)) > epsl ) THEN
        WRITE(6,'(2A,2(A,I3),A)') 'The derivated lat. & long. ',        &
                      'are not consistent with the data at grid point', &
                      ' i = ',i,' j = ',j,'.'
        WRITE(6,'(2(A,F9.3))') 'The determined latitude = ',            &
                      lat_wrf(i,j), ' longitude = ', lon_wrf(i,j)
        WRITE(6,'(2(A,F9.3))') 'The data in file are latitude = ',      &
                      tem2d1(i,j), ' longitude = ', tem2d2(i,j)
        istatus = -666
        RETURN
      END IF
    END DO
  END DO

!-----------------------------------------------------------------------
! Finish reading
!-----------------------------------------------------------------------

  CALL close_wrf_files(fHndl,io_form,patch_split,1,ncmprx,ncmpry)

!-----------------------------------------------------------------------
!
! Clear working arrays
!
!-----------------------------------------------------------------------

  DEALLOCATE( fHndl )
  DEALLOCATE( lat_wrf, lon_wrf )
  DEALLOCATE( tem2d1, tem2d2 )
  DEALLOCATE( var_wrf, temd )

  RETURN
END SUBROUTINE readwrfgrd
