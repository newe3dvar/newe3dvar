!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%                                                          %%%%
!%%%% NOTE: Before calling subroutines in this file, it is     %%%%
!%%%%       assumed that the program is in MPI multifile mode. %%%%
!%%%%       If ncompressx = ncompressy = 1, the subroutines    %%%%
!%%%%       do not need to join smaller tiles, otherwiser,     %%%%
!%%%%       join smaller WRF tiles to get a local ARPS patch.  %%%%
!%%%%                                                          %%%%
!%%%%       All subroutines in this file do not have to        %%%%
!%%%%       support PHDF5 format because PHDF5 file does       %%%%
!%%%%       not in split files.                                %%%%
!%%%%                                                          %%%%
!%%%%       All file IDs are size of (ncompressx,ncompressy)   %%%%
!%%%%       arrays, except the following attribute related     %%%%
!%%%%       subroutines:                                       %%%%
!%%%%          get_wrf_meta_from_multi_files                   %%%%
!%%%%          get_dom_ti_integer                              %%%%
!%%%%          get_dom_ti_real                                 %%%%
!%%%%          get_dom_ti_char                                 %%%%
!%%%%                                                          %%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE open_wrf_file              ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE open_wrf_multi_files(filename,io_form,multifile,file_mode,   &
                                ncompressx,ncompressy,numdigits,        &
                                nidout,istatus)
!
!------------------------------------------------------------------
!
!  PURPOSE:
!
!    Open a WRF file and return NetCDF file handler. Each processor
!    may open multifiles depends on ncompressx/ncompressy. However,
!    for_meta_only is .TRUE., only one file will be opened.
!
!    This mode only supports binary and NetCDF format so far.
!
!    NOTE: it is required to call close_wrf_multi_files explicitly to close
!          the opened file in your calling program.
!
!------------------------------------------------------------------

  IMPLICIT NONE
  CHARACTER(LEN=*), INTENT(IN)  :: filename
  INTEGER,          INTENT(IN)  :: io_form, file_mode
  LOGICAL,          INTENT(IN)  :: multifile
  INTEGER,          INTENT(IN)  :: ncompressx, ncompressy,numdigits
  INTEGER,          INTENT(OUT) :: nidout(ncompressx,ncompressy)
  INTEGER,          INTENT(OUT) :: istatus

!------------------------------------------------------------------
!
!  Misc. local variable
!
!------------------------------------------------------------------
  CHARACTER(LEN=256), ALLOCATABLE :: tmpstr(:,:)
  LOGICAL                         :: fexists

  CHARACTER(LEN=80)  :: sysdepinfo
  LOGICAL, SAVE      :: initialized = .FALSE.

  INCLUDE 'mp.inc'

  INTEGER  :: loc_proc, iloc, jloc
  INTEGER  :: iloc_x, jloc_y, nprocx_in

  CHARACTER(LEN=20)  :: fmtstr
  LOGICAL            :: for_meta_only

  CHARACTER(LEN=1), PARAMETER :: cmode(0:3) = (/'r','r','w','m'/)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Begining of executable code
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  for_meta_only = (file_mode == 0)

  iloc_x = (loc_x-1)*ncompressx    ! column of processors
  jloc_y = (loc_y-1)*ncompressy    ! rows of processors
  nprocx_in = ncompressx*nproc_x

  ALLOCATE(tmpstr(ncompressx,ncompressy), STAT = istatus)

  WRITE(fmtstr,'(a,I1,a,I1,a)') '(2a,I',numdigits,'.',numdigits,')'

  DO jloc = 1, ncompressy
    DO iloc = 1, ncompressx

      loc_proc = (jloc_y+jloc-1)*nprocx_in + iloc_x+(iloc-1)
      IF (multifile) THEN
        WRITE(tmpstr(iloc,jloc),FMT=TRIM(fmtstr)) TRIM(filename),'_',loc_proc
      ELSE
        WRITE(tmpstr(iloc,jloc),FMT='(a)')  TRIM(filename)
      END IF

      INQUIRE(FILE = TRIM(tmpstr(iloc,jloc)), EXIST = fexists)
      IF ( .NOT. fexists ) THEN
        WRITE(6,'(3a)') 'File not found: ',tmpstr(iloc,jloc),' in open_wrf_file'
        CALL arpsstop('WRF file not exist.',1)
      ENDIF

    END DO
  END DO

  sysdepinfo = 'DATASET=HISTORY'
  nidout(:,:) = -1

  IF (io_form == 7) THEN              ! not initialize needed

    IF ( for_meta_only )  THEN
      CALL open_ncd_wrf_file ( tmpstr(1,1), cmode(file_mode), nidout(1,1), istatus )
    ELSE
      DO jloc = 1,ncompressy
        DO iloc = 1,ncompressx
          CALL open_ncd_wrf_file ( tmpstr(iloc,jloc), cmode(file_mode), nidout(iloc,jloc), istatus )
        END DO
      END DO
    END IF

  ELSE
    WRITE(0,*) 'Unsupported IO format - ',io_form
    CAlL arpsstop('Unsupported IO format.',1)
  END IF

  initialized = .TRUE.

  DEALLOCATE(tmpstr)

  RETURN
END SUBROUTINE open_wrf_multi_files
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE close_wrf_file               ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE close_wrf_multi_files(nch,io_form,file_mode,                 &
                                 ncompressx,ncompressy,istatus)
!
!------------------------------------------------------------------
!
!  PURPOSE:
!
!     Close the WRF file which is opened using open_wrf_multi_file.
!
!------------------------------------------------------------------
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: io_form
  INTEGER, INTENT(IN) :: file_mode
  INTEGER, INTENT(IN) :: ncompressx, ncompressy
  INTEGER, INTENT(IN) :: nch(ncompressx,ncompressy)
  INTEGER, INTENT(OUT):: istatus

!------------------------------------------------------------------
!
!  Misc. local variable
!
!------------------------------------------------------------------
!
  INTEGER :: iloc, jloc
  LOGICAL :: for_meta_only

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  istatus = 0

  for_meta_only = (file_mode == 0)

  IF(io_form == 7) THEN
    IF (for_meta_only) THEN
      CALL close_ncd_wrf_file ( nch(1,1), istatus )
    ELSE
      DO jloc = 1, ncompressy
        DO iloc = 1, ncompressx
          CALL close_ncd_wrf_file ( nch(iloc,jloc), istatus )
        END DO
      END DO
    END IF
  !ELSE IF (io_form == 1) THEN
  !  IF (for_meta_only) THEN
  !    CALL ext_int_ioclose(nch(1,1),iStatus)
  !  ELSE
  !    DO jloc = 1, ncompressy
  !      DO iloc = 1, ncompressx
  !        CALL ext_int_ioclose(nch(iloc,jloc),iStatus)
  !      END DO
  !    END DO
  !  END IF
  END IF

  IF (istatus /= 0) THEN
    WRITE(0,'(1x,2a)') 'ERROR: closing file handler ',nch
    CALL arpsstop('Error in close_wrf_multi_files',1)
  END IF

  RETURN
END SUBROUTINE close_wrf_multi_files
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE get_wrf_2d                   ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_join_wrf_2d(nfid,io_form,ncompressx,ncompressy,          &
                      datestr,itime,varname,stagger,                    &
                      !stagger,dimname1,dimname2,dimname3,
                      nx,ny,var2d,                                      &
                      nxdin,nydin,temtd,istatus)

!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Read in a 2D array from the WRF NetCDF file
!
!-----------------------------------------------------------------------

  USE model_precision
  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: io_form
  INTEGER,          INTENT(IN)  :: ncompressx,ncompressy
  INTEGER,          INTENT(IN)  :: nfid(ncompressx,ncompressy)
  CHARACTER(LEN=*), INTENT(IN)  :: datestr
  INTEGER,          INTENT(IN)  :: itime
  CHARACTER(LEN=*), INTENT(IN)  :: varname
  CHARACTER(LEN=*), INTENT(IN)  :: stagger
  !CHARACTER(LEN=*), INTENT(IN)  :: dimname1
  !CHARACTER(LEN=*), INTENT(IN)  :: dimname2
  !CHARACTER(LEN=*), INTENT(IN)  :: dimname3
  INTEGER,          INTENT(IN)  :: nx              ! local index
  INTEGER,          INTENT(IN)  :: ny
  REAL(P),          INTENT(OUT) :: var2d(nx,ny)
  INTEGER,          INTENT(IN)  :: nxdin,nydin             ! Data index
  REAL(SP),         INTENT(OUT) :: temtd(nxdin*nydin)      ! domain array
  INTEGER,          INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INTEGER, PARAMETER :: VAR_NOTEXIST = -1
  INTEGER, PARAMETER :: WRF_REAL     = 104
  INTEGER, PARAMETER :: WRF_INTEGER  = 106

  INCLUDE 'mp.inc'

  INTEGER            :: i, j
  INTEGER            :: i1,j1

  !INTEGER,          INTENT(IN)  :: fzone

  !CHARACTER(80) :: DimNames(3)
  !INTEGER       :: DomainStart(3), DomainEnd(3)
  !INTEGER       :: MemoryStart(3), MemoryEnd(3)
  !INTEGER       :: PatchStart(3),  PatchEnd(3)
  !
  !INTEGER       :: ilocs,iloce,jlocs,jloce

!-----------------------------------------------------------------------
!
! Dimensions:
!
!   nxdim, nydim  --  Whole domain size, stagger or unstagger
!   nx,    ny     --  ARPS patch stagger size, (IN)
!   nxp,   nyp    --  ARPS patch size contains acutal data from WRF files
!   nxdin, nydin  --  WRF data patch size, stagger (IN)
!   nxt,   nyt    --  WRF data patch size, unstagger
!   nxd,   nyd    --  WRF actual data size in files, stagger or unstagger
!
!-----------------------------------------------------------------------

  INTEGER       :: nxt, nyt       ! unstagger WRF patch size
  INTEGER       :: nxd, nyd       ! WRF data patch size
                                  ! maybe stagger or unstagger
  !INTEGER       :: nxp, nyp
  !INTEGER       :: nxdim, nydim

  INTEGER       :: iloc, jloc
  INTEGER       :: ia, ja, kin
  INTEGER       :: ilocs_t, jlocs_t

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  IF ( myproc == 0 )   &
     WRITE(6,FMT='(2x,a,a10)',ADVANCE='NO') 'Reading 2D variable ', TRIM(varname)

  !nxdim = (nx - 1)*nproc_x    ! domain size
  !nydim = (ny - 1)*nproc_y
  !IF (Stagger == 'X') nxdim = nxdim + 1
  !IF (Stagger == 'Y') nydim = nydim + 1
  !
  !nxp   = nx-1                        ! ARPS patch size
  !nyp   = ny-1
  !IF (Stagger == 'X' .AND. loc_x == nproc_x) nxp = nxp+1
  !IF (Stagger == 'Y' .AND. loc_y == nproc_y) nyp = nyp+1

  nxt = (nx-1)/ncompressx             ! WRF patch size
  nyt = (ny-1)/ncompressy

!-----------------------------------------------------------------------
! Do some check, should be commented out before releasing.
!-----------------------------------------------------------------------

  IF (nxt /= nxdin .AND. nxt /= nxdin-1) THEN
    WRITE(6,*) 'Wrong in WRF patch size, nxdin = ',nxdin,', nxt = ',nxt
    istatus = -2
    RETURN
  END IF
  IF (nyt /= nydin .AND. nyt /= nydin-1) THEN
    WRITE(6,*) 'Wrong in WRF patch size, nydin = ',nydin,', nyt = ',nyt
    istatus = -2
    RETURN
  END IF

  DO jloc = 1,ncompressy
    DO iloc = 1,ncompressx

      nxd = nxt                   ! Actually data size
      nyd = nyt
      IF (stagger == 'X' .AND. loc_x == nproc_x .AND. iloc == ncompressx)  &
         nxd = nxt+1
      IF (stagger == 'Y' .AND. loc_y == nproc_y .AND. jloc == ncompressy)  &
         nyd = nyt+1

      ilocs_t = (iloc-1)*nxt   ! WRF tile start index within ARPS patch
      jlocs_t = (jloc-1)*nyt

      IF (io_form == 7) THEN

        CALL get_ncd_2d(nfid(iloc,jloc),itime,varname,nxd,nyd,temtd,istatus)

        DO j = 1,nyd
          j1 = (j-1)*nxd
          DO i = 1,nxd
            kin = i + j1
            ia  = ilocs_t + i
            ja  = jlocs_t + j
            var2d(ia,ja) = temtd(kin)
          END DO
        END DO

      ELSE
        istatus = -1
        WRITE(0,*) 'ERROR: unsupported io_form = ',io_form
        RETURN
      END IF

    END DO     ! iloc
  END DO       ! jloc

  IF ( myproc == 0 ) THEN
    IF (istatus == 0) THEN
      WRITE(6,'(a)') '         ...  DONE.'
    ELSE
      WRITE(6,'(a)') '         ...  ERROR.'
    END IF
  END IF

  RETURN
END SUBROUTINE get_join_wrf_2d
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE get_wrf_3d                   ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_join_wrf_3d(nfid,io_form,ncompressx,ncompressy,          &
                      datestr,itime,varname,stagger,                    &
                      !stagger,dimname1,dimname2,dimname3,
                      nx,ny,nz,var3d,                                   &
                      nxdin,nydin,nzin,temtd,istatus)

!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Read in a 3D array from the WRF NetCDF file
!
!-----------------------------------------------------------------------

  USE model_precision

  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: io_form
  INTEGER,          INTENT(IN)  :: ncompressx,ncompressy
  INTEGER,          INTENT(IN)  :: nfid(ncompressx,ncompressy)
  CHARACTER(LEN=*), INTENT(IN)  :: datestr
  INTEGER,          INTENT(IN)  :: itime
  CHARACTER(LEN=*), INTENT(IN)  :: varname
  CHARACTER(LEN=*), INTENT(IN)  :: stagger
  !CHARACTER(LEN=*), INTENT(IN)  :: dimname1
  !CHARACTER(LEN=*), INTENT(IN)  :: dimname2
  !CHARACTER(LEN=*), INTENT(IN)  :: dimname3
  INTEGER,          INTENT(IN)  :: nx              ! local index
  INTEGER,          INTENT(IN)  :: ny
  INTEGER,          INTENT(IN)  :: nz
  REAL(P),          INTENT(OUT) :: var3d(nx,ny,nz)
  INTEGER,          INTENT(IN)  :: nxdin,nydin,nzin             ! Data index
  REAL(SP),         INTENT(OUT) :: temtd(nxdin*nydin*nzin)      ! domain array
  INTEGER,          INTENT(INOUT) :: istatus

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INTEGER, PARAMETER :: VAR_NOTEXIST = -1
  INTEGER, PARAMETER :: WRF_REAL     = 104
  INTEGER, PARAMETER :: WRF_INTEGER  = 106

  INCLUDE 'mp.inc'

  INTEGER            :: i, j, k
  INTEGER            :: i1,j1,k1

  !INTEGER,          INTENT(IN)  :: fzone

  !CHARACTER(80) :: DimNames(3)
  !INTEGER       :: DomainStart(3), DomainEnd(3)
  !INTEGER       :: MemoryStart(3), MemoryEnd(3)
  !INTEGER       :: PatchStart(3),  PatchEnd(3)
  !
  !INTEGER       :: ilocs,iloce,jlocs,jloce

!-----------------------------------------------------------------------
!
! Dimensions:
!
!   nxdim, nydim  --  Whole domain size, stagger or unstagger
!   nx,    ny     --  ARPS patch stagger size, (IN)
!   nxp,   nyp    --  ARPS patch size contains acutal data from WRF files
!   nxdin, nydin  --  WRF data patch size, stagger (IN)
!   nxt,   nyt    --  WRF data patch size, unstagger
!   nxd,   nyd    --  WRF actual data size in files, stagger or unstagger
!
!-----------------------------------------------------------------------

  INTEGER       :: nxt, nyt       ! unstagger WRF patch size
  INTEGER       :: nxd, nyd, nzd  ! WRF data patch size
                                  ! maybe stagger or unstagger
  !INTEGER       :: nxp, nyp
  !INTEGER       :: nxdim, nydim

  INTEGER       :: iloc, jloc
  INTEGER       :: ia, ja, kin
  INTEGER       :: ilocs_t, jlocs_t

  INTEGER       :: lvldbg

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  lvldbg = istatus

  IF ( myproc == 0 )   &
     WRITE(6,FMT='(2x,a,a10)',ADVANCE='NO') 'Reading 3D variable ', TRIM(varname)

  !nxdim = (nx - 1)*nproc_x    ! domain size
  !nydim = (ny - 1)*nproc_y
  !IF (Stagger == 'X') nxdim = nxdim + 1
  !IF (Stagger == 'Y') nydim = nydim + 1
  !
  !nxp   = nx-1                        ! ARPS patch size
  !nyp   = ny-1
  !IF (Stagger == 'X' .AND. loc_x == nproc_x) nxp = nxp+1
  !IF (Stagger == 'Y' .AND. loc_y == nproc_y) nyp = nyp+1

  nxt = (nx-1)/ncompressx             ! WRF patch size
  nyt = (ny-1)/ncompressy

!-----------------------------------------------------------------------
! Do some check, should be commented out before releasing.
!-----------------------------------------------------------------------

  IF (nxt /= nxdin .AND. nxt /= nxdin-1) THEN
    WRITE(6,*) 'Wrong in WRF patch size, nxdin = ',nxdin,', nxt = ',nxt
    istatus = -2
    RETURN
  END IF
  IF (nyt /= nydin .AND. nyt /= nydin-1) THEN
    WRITE(6,*) 'Wrong in WRF patch size, nydin = ',nydin,', nyt = ',nyt
    istatus = -2
    RETURN
  END IF

  IF (stagger == 'Z') THEN
    nzd = nzin
  ELSE
    nzd = nzin -1
  END IF

  DO jloc = 1,ncompressy
    DO iloc = 1,ncompressx

      nxd = nxt                   ! Actually data size
      nyd = nyt
      IF (stagger == 'X' .AND. loc_x == nproc_x .AND. iloc == ncompressx)  &
         nxd = nxt+1
      IF (stagger == 'Y' .AND. loc_y == nproc_y .AND. jloc == ncompressy)  &
         nyd = nyt+1

      ilocs_t = (iloc-1)*nxt   ! WRF tile start index within ARPS patch
      jlocs_t = (jloc-1)*nyt

      IF (io_form == 7) THEN

        CALL get_ncd_3d(nfid(iloc,jloc),itime,varname,nxd,nyd,nzd,temtd,istatus)

        DO k = 1,nzd
          k1 = (k-1)*nxd*nyd
          DO j = 1,nyd
            j1 = (j-1)*nxd
            DO i = 1,nxd
              kin = i + j1 + k1
              ia  = ilocs_t + i
              ja  = jlocs_t + j
              var3d(ia,ja,k) = temtd(kin)
            END DO
          END DO
        END DO

      ELSE
        istatus = -1
        WRITE(0,*) 'ERROR: unsupported io_form = ',io_form
        RETURN
      END IF

    END DO     ! iloc
  END DO       ! jloc

  IF ( myproc == 0 ) THEN
    IF (istatus == 0) THEN
      WRITE(6,'(a)') '         ...  DONE.'
    ELSE
      WRITE(6,'(a)') '         ...  ERROR.'
    END IF
  END IF

  RETURN
END SUBROUTINE get_join_wrf_3d
