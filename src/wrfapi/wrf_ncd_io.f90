!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%
!%%%% The followings are defined for WRITE to WRF input file.
!%%%%
!%%%% NOTE: They may be merged later with subroutines in the next section
!%%%        of this file.
!%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


!###################### Open a WRF file ###############################
SUBROUTINE open_ncd_wrf_file ( filename, filemode, ncid, istatus )

  IMPLICIT NONE

  CHARACTER(LEN=*), INTENT(IN)  :: filename
  CHARACTER(LEN=1), INTENT(IN)  :: filemode
  INTEGER,          INTENT(OUT) :: ncid, istatus

!------------------------------------------------------------------

  LOGICAL :: fexists
  INTEGER :: file_mode

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  IF (filemode == 'r') THEN
    INQUIRE(FILE = filename, EXIST = fexists)
    IF (fexists) THEN
      file_mode = NF_NOWRITE
    ELSE
      istatus = -1
      WRITE(6,'(2a)') 'ERROR: File not found: ', filename
      RETURN
    ENDIF

    istatus = NF_OPEN(TRIM(filename),file_mode,ncid)
    CALL handle_ncd_error(istatus,'NF_OPEN("'//TRIM(filename)//'") in open_wrf_file.',.FALSE.)

  ELSE IF (filemode == 'w') THEN
    !file_mode = IOR(NF_CLOBBER,NF_64BIT_OFFSET)
    file_mode =  IOR(IOR(NF_CLOBBER,NF_NETCDF4),NF_CLASSIC_MODEL)

    istatus = NF_CREATE(TRIM(filename),file_mode,ncid)
    CALL handle_ncd_error(istatus,'NF_CREATE('//TRIM(filename)//') in open_wrf_file.',.TRUE.)

  ELSE IF (filemode == 'm') THEN
    INQUIRE(FILE = filename, EXIST = fexists)
    IF (fexists) THEN
      file_mode = NF_WRITE !NF_SHARE  !NF_WRITE
    ELSE
      istatus = -1
      WRITE(6,'(2a)') 'ERROR: File not found: ', filename
      RETURN
    ENDIF

    istatus = NF_OPEN(TRIM(filename),file_mode,ncid)
    CALL handle_ncd_error(istatus,'NF_OPEN('//TRIM(filename)//' in open_wrf_file.',.TRUE.)

  ELSE
    istatus = -2
    WRITE(6,'(2a)') 'ERROR: Unsupported file opening mode : ', filemode
  END IF

  !IF (istatus < 0) STOP  ! or call arpsstop here

  RETURN
END SUBROUTINE open_ncd_wrf_file

!###################### Close an opened WRF file ######################
SUBROUTINE close_ncd_wrf_file ( ncid, istatus )
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: ncid
  INTEGER, INTENT(OUT) :: istatus

!------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  istatus = NF_CLOSE(ncid)
  CALL handle_ncd_error(istatus,'NF_CLOSE in close_wrf_file',.TRUE.)

  RETURN
END SUBROUTINE close_ncd_wrf_file

!###################### Close an opened WRF file ######################

SUBROUTINE get_wrf_dimensions( ncid, nx,ny,nz,nzsoil,nstyps,nscalar,    &
                             P_QC,P_QR,P_QI,P_QS,P_QG,P_QH,             &
                             P_NC,P_NR,P_NI,P_NS,P_NG,P_NH,             &
                                  P_ZR,P_ZI,P_ZS,P_ZG,P_ZH,             &
                             P_NCCN,qnames,                             &
                             iproj,ctrlat,ctrlon,trulat1,trulat2,trulon,&
                             dx,dy, istatus )
  IMPLICIT NONE

  INTEGER,           INTENT(IN)  :: ncid
  INTEGER,           INTENT(OUT) :: nx, ny, nz, nzsoil, nstyps, nscalar
  INTEGER,           INTENT(OUT) :: P_QC,P_QR,P_QI,P_QS,P_QG,P_QH,    &
                                    P_NC,P_NR,P_NI,P_NS,P_NG,P_NH,     &
                                    P_ZR,P_ZI,P_ZS,P_ZG,P_ZH
  INTEGER,           INTENT(OUT) :: P_NCCN
  CHARACTER(LEN=40), INTENT(OUT) :: qnames(30)

  INTEGER,           INTENT(OUT) :: iproj
  REAL,              INTENT(OUT) :: ctrlat, ctrlon, trulat1, trulat2, trulon
  REAL,              INTENT(OUT) :: dx, dy
  INTEGER,           INTENT(OUT) :: istatus

!------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER, PARAMETER :: ntotal = 13
  INTEGER, PARAMETER :: I_QC = 1, I_QR = 2,  I_QI = 3,  I_QS = 4,  I_QG = 5,  I_QH = 6,  &
                        I_NC = 7, I_NR = 8,  I_NI = 9,  I_NS = 10, I_NG = 11, I_NH = 12, &
                        I_NCCN = 13,                                                     &
                                  I_ZR = 14, I_ZI = 15, I_ZS = 16, I_ZG = 17, I_ZH = 18
                ! denotes the order of names in qnames_wrf

  CHARACTER(LEN=40), PARAMETER :: qnames_wrf(ntotal) = (/ 'QCLOUD    ', &
                'QRAIN     ', 'QICE      ', 'QSNOW     ', 'QGRAUP    ', &
                'QHAIL     ', 'QNCLOUD   ', 'QNRAIN    ', 'QNICE     ', &
                'QNSNOW    ', 'QNGRAUPEL ', 'QNHAIL    ', 'QNCCN     '  /)

  INTEGER :: P_ARR(20)   ! remember the order of qnames_wrf and map to the variable P_??.

  INTEGER :: dimid, varid
  INTEGER :: nq

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  istatus = nf_inq_dimid(ncid,'west_east_stag',dimid)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:nf_inq_dimid(west_east_stag)',.TRUE.)
  istatus = nf_inq_dimlen(ncid,dimid,nx)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:nf_inq_dimlen(nx)',.TRUE.)

  istatus = nf_inq_dimid(ncid,'south_north_stag',dimid)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:nf_inq_dimid(south_north_stag)',.TRUE.)
  istatus = nf_inq_dimlen(ncid,dimid,ny)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:nf_inq_dimlen(ny)',.TRUE.)

  istatus = nf_inq_dimid(ncid,'bottom_top_stag',dimid)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:nf_inq_dimid(bottom_top_stag)',.TRUE.)
  istatus = nf_inq_dimlen(ncid,dimid,nz)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:nf_inq_dimlen(nz)',.TRUE.)

  istatus = nf_inq_dimid(ncid,'soil_layers_stag',dimid)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:nf_inq_dimid(soil_layers_stag)',.TRUE.)
  istatus = nf_inq_dimlen(ncid,dimid,nzsoil)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:nf_inq_dimlen(nzsoil)',.TRUE.)

  nstyps = 1

  istatus = NF_GET_ATT_INT(ncid,NF_GLOBAL,'MAP_PROJ',iproj)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:NF_GET_ATT_INT(MAP_PROJ)',.TRUE.)

  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'CEN_LAT',ctrlat)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:NF_GET_ATT_REAL(CEN_LAT)',.TRUE.)

  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'CEN_LON',ctrlon)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:NF_GET_ATT_REAL(CEN_LON)',.TRUE.)

  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'TRUELAT1',trulat1)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:NF_GET_ATT_REAL(TRUELAT1)',.TRUE.)

  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'TRUELAT2',trulat2)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:NF_GET_ATT_REAL(TRUELAT2)',.TRUE.)

  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'STAND_LON',trulon)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:NF_GET_ATT_REAL(STAND_LON)',.TRUE.)

  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'DX',dx)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:NF_GET_ATT_REAL(DX)',.TRUE.)

  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'DY',dy)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_dimensions:NF_GET_ATT_REAL(DY)',.TRUE.)

  nscalar  = 0
  P_ARR(:) = 0
  DO nq = 1, ntotal
    istatus = NF_INQ_VARID(ncid,qnames_wrf(nq),varid)
    IF (istatus == NF_NOERR) THEN
      nscalar = nscalar + 1
      P_ARR(nq) = nscalar
      qnames(nscalar) = qnames_wrf(nq)
    END IF
  END DO

  P_QC = P_ARR(I_QC);  P_NC = P_ARR(I_NC)
  P_QR = P_ARR(I_QR);  P_NR = P_ARR(I_NR);  P_ZR = P_ARR(I_ZR)
  P_QI = P_ARR(I_QI);  P_NI = P_ARR(I_NI);  P_ZI = P_ARR(I_ZI)
  P_QS = P_ARR(I_QS);  P_NS = P_ARR(I_NS);  P_ZS = P_ARR(I_ZS)
  P_QG = P_ARR(I_QG);  P_NG = P_ARR(I_NG);  P_ZG = P_ARR(I_ZG)
  P_QH = P_ARR(I_QH);  P_NH = P_ARR(I_NH);  P_ZH = P_ARR(I_ZH)
  P_NCCN = P_ARR(I_NCCN)

  RETURN
END SUBROUTINE get_wrf_dimensions

!###################### Read a variable from an opened WRF file ########

SUBROUTINE read_wrf_var_real ( fin,varname, varout,nsizes, ndims, stagger,istatus )

  USE model_precision

  IMPLICIT NONE

  INTEGER,          INTENT(IN)    :: fin
  CHARACTER(LEN=*), INTENT(IN)    :: varname
  INTEGER,          INTENT(OUT)   :: ndims
  INTEGER,          INTENT(INOUT) :: nsizes(4)
                                  ! nsizes(4) - IN  Maximum array size
                                  !           - OUT Actuall array size
                                  ! nsizes(1:3) - Array sizes with each dimension
  REAL(SP),         INTENT(OUT)   :: varout(nsizes(4))
  CHARACTER(LEN=1), INTENT(OUT)   :: stagger

  INTEGER,          INTENT(OUT)   :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER :: varid
  INTEGER :: vartype,varnatts
  INTEGER :: vardimids(NF_MAX_DIMS)

  INTEGER :: vdim, dimlen, unlimdimid, nxid, nyid
  CHARACTER(LEN=32) :: dimname

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  !
  ! get variable id
  !
  istatus = nf_inq_varid(fin,varname,varid)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'read_wrf_var_real:nf_inq_varid:'//TRIM(varname),.TRUE.)

  !
  ! Get more variable information
  !
  istatus = nf_inq_var(fin,varid,varname,vartype,ndims,vardimids,varnatts)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'read_wrf_var_real:nf_inq_var',.TRUE.)

  !
  ! Get dimensions
  !
  istatus = nf_inq_unlimdim(fin,unlimdimid)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'read_wrf_var_real:nf_inq_unlimdim',.TRUE.)

  istatus = nf_inq_dimid(fin,'west_east_stag',nxid)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'read_wrf_var_real:nf_inq_dimid',.TRUE.)

  istatus = nf_inq_dimid(fin,'south_north_stag',nyid)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'read_wrf_var_real:nf_inq_dimid',.TRUE.)

  stagger = ' '

  IF (ndims >= 1 .AND. ndims <= 4) THEN
    nsizes(:) = 1
    DO vdim = 1, ndims
      istatus = nf_inq_dim(fin,vardimids(vdim),dimname,dimlen)
      IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'read_wrf_var_real:nf_inq_dim',.TRUE.)

      IF (vardimids(vdim) /= unlimdimid ) THEN
        nsizes(vdim) = dimlen
        nsizes(4) = nsizes(4)*dimlen

        IF (vardimids(vdim) == nxid) THEN
          stagger = 'X'
        ELSE IF (vardimids(vdim) == nyid) THEN
          stagger = 'Y'
        END IF
      END IF

    END DO
    ndims = ndims - 1    ! Do not count the unlimited dimension

    istatus = nf_get_var_real(fin,varid,varout)
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'read_wrf_var_real:nf_get_var_real - '//TRIM(varname),.TRUE.)
  ELSE
    WRITE(*,'(1x,a,I0,a)') 'ERROR: unsupported variable rank size ',ndims,'.'
    istatus = -1
  END IF

  RETURN
END SUBROUTINE read_wrf_var_real

!################### Write a variable to an opened WRF file ############

SUBROUTINE write_wrf_var_real(fout,vname,varin,nsize,istatus )

  USE model_precision

  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: fout
  CHARACTER(LEN=*), INTENT(IN)  :: vname
  INTEGER,          INTENT(IN)  :: nsize

  REAL(SP),         INTENT(IN)  :: varin(nsize)

  INTEGER,          INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER :: varid, vdim
  INTEGER :: vartype, ndims
  INTEGER :: vardimids(NF_MAX_DIMS)
  INTEGER :: varnatts, nsizes, dimlen

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  !
  ! get variable id
  !
  istatus = nf_inq_varid(fout,vname,varid)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'write_wrf_var_real:nf_inq_varid',.TRUE.)

  !
  ! Get more variable information
  !
  istatus = nf_inq_var(fout,varid,TRIM(vname),vartype,ndims,vardimids,varnatts)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'write_wrf_var_real:nf_inq_var',.FALSE.)

  nsizes = 1
  DO vdim = 1, ndims
    istatus = nf_inq_dimlen(fout,vardimids(vdim),dimlen)
    CALL handle_ncd_error(istatus,'write_wrf_var_real:nf_inq_dim',.FALSE.)
    nsizes = nsizes*dimlen
  END DO

  IF (nsizes /= nsize) THEN
    WRITE(*,'(1x,a,/,8x,2(a,I0))')                                      &
         'ERROR: Array size does not match for '//TRIM(vname),          &
         'Passed in ',nsize,', expected ',nsizes
    istatus = -1
    CALL handle_ncd_error(istatus,'write_wrf_var_real:wrong array size',.TRUE.)
  END IF

  istatus = nf_put_var_real(fout,varid,varin)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'write_wrf_var_real:nf_put_var_real',.TRUE.)

  RETURN
END SUBROUTINE write_wrf_var_real

!################### Write a variable to an opened WRF file ############

SUBROUTINE write_wrf_vara_real(fout,vname,varin,nsize,istart,icount,istatus )

  USE model_precision

  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: fout
  CHARACTER(LEN=*), INTENT(IN)  :: vname
  INTEGER,          INTENT(IN)  :: nsize
  INTEGER,          INTENT(IN)  :: istart(4), icount(4)

  REAL(SP),         INTENT(IN)  :: varin(nsize)

  INTEGER,          INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INTEGER :: varid

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  !
  ! get variable id
  !
  istatus = nf_inq_varid(fout,vname,varid)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'write_wrf_vara_real:nf_inq_varid:'//TRIM(vname),.TRUE.)

  istatus = nf_put_vara_real(fout,varid,istart,icount,varin)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'write_wrf_vara_real:nf_put_vara_real',.TRUE.)

  RETURN
END SUBROUTINE write_wrf_vara_real

!######### Copy file definition from one file to another file #########

SUBROUTINE copy_wrf_definitions ( fin, fout,                            &
                  fixdim,nproc_x_out, nproc_y_out, iloc,jloc,           &
                  nvar,var_names,naddition,var_add,                     &
                  filetitle, datestr, istatus  )

  IMPLICIT NONE

  INTEGER,           INTENT(IN)  :: fin, fout
  LOGICAL,           INTENT(IN)  :: fixdim
  INTEGER,           INTENT(IN)  :: nproc_x_out, nproc_y_out
  INTEGER,           INTENT(IN)  :: iloc, jloc
  INTEGER,           INTENT(IN)  :: nvar
  CHARACTER(LEN=20), INTENT(IN)  :: var_names(nvar)
  INTEGER,           INTENT(IN)  :: naddition
  CHARACTER(LEN=20), INTENT(IN)  :: var_add(naddition,3)
  CHARACTER(LEN=* ), INTENT(IN)  :: filetitle
  CHARACTER(LEN=19), INTENT(IN)  :: datestr
  INTEGER,           INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  CHARACTER(LEN=32) :: dimname, attname, varname

  INTEGER :: ndims, dimid, dimlen, odimid, unlimdimid

  INTEGER :: ngatts, attnum

  INTEGER :: varid, nvars, ovarid, varnum
  INTEGER :: vartype, varndims, varnatts, vardimids(NF_MAX_DIMS)
  INTEGER :: varshuffle, vardeflate, vardeflate_level

  INTEGER :: lenstr

  INTEGER :: nxd, nyd, nxf, nyf, nxs,nys
  INTEGER :: ips,ipe,jps,jpe
  INTEGER :: iss,ise,jss,jse

  LOGICAL :: name_in_set

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  IF (fixdim) THEN
    istatus = NF_GET_ATT_INT(fin,NF_GLOBAL,'WEST-EAST_GRID_DIMENSION',  nxd)
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:NF_GET_ATT_INT',.TRUE.)

    istatus = NF_GET_ATT_INT(fin,NF_GLOBAL,'SOUTH-NORTH_GRID_DIMENSION',nyd)
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:NF_GET_ATT_INT',.TRUE.)

    IF (MOD(nxd-1,nproc_x_out) /= 0 .OR. MOD(nyd-1,nproc_y_out) /= 0) THEN
      istatus = -1
      WRITE(*,'(1x,a,/,2(8x,2(a,I8),/))')                               &
          'ERROR: Dimension size is not divisible by patch size.',      &
          'nxd         = ',nxd,         ', nyd         = ',nyd,         &
          'nproc_x_out = ', nproc_x_out,', nproc_y_out = ',nproc_y_out
      CALL handle_ncd_error(istatus,'copy_wrf_definitions:wrong dimension size',.TRUE.)
    END IF

    nxs = (nxd-1)/nproc_x_out;  nxf = nxs
    nys = (nyd-1)/nproc_y_out;  nyf = nys

    IF (iloc == nproc_x_out) nxf = nxf+1
    IF (jloc == nproc_y_out) nyf = nyf+1

    iss = (iloc-1)*nxs+1    ; ips = iss
    ise = iss+nxs-1         ; ipe = ips+nxf-1
    jss = (jloc-1)*nys+1    ; jps = jss
    jse = jss+nys-1         ; jpe = jps+nyf-1

  END IF

!-----------------------------------------------------------------------
!
! Copy dimensions definitions
!
!-----------------------------------------------------------------------

  istatus = nf_inq_unlimdim(fin,unlimdimid)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_inq_unlimdim',.TRUE.)

  istatus = nf_inq_ndims(fin,ndims)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_inq_ndims',.TRUE.)

  DO dimid = 1, ndims

    istatus = nf_inq_dim(fin,dimid,dimname,dimlen)
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_inq_dim',.TRUE.)

    IF (dimid == unlimdimid) dimlen = NF_UNLIMITED

    IF (fixdim) THEN
      SELECT CASE (TRIM(dimname))
      CASE ('west_east')
        dimlen = nxs
      CASE ('west_east_stag')
        dimlen = nxf
      CASE ('south_north')
        dimlen = nys
      CASE ('south_north_stag')
        dimlen = nyf
      END SELECT
    END IF

    istatus = nf_def_dim(fout,dimname,dimlen,odimid)
    !write(*,*) 'dimlen = ',odimid, dimlen
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_def_dim',.TRUE.)

  END DO

!-----------------------------------------------------------------------
!
! Copy global attributes
!
!-----------------------------------------------------------------------

  istatus = nf_inq_natts(fin,ngatts)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_inq_natts',.TRUE.)

  DO attnum = 1, ngatts

    istatus = nf_inq_attname(fin,NF_GLOBAL,attnum,attname)
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_inq_attname',.TRUE.)

    istatus = nf_copy_att(fin,NF_GLOBAL,attname,fout,NF_GLOBAL)
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_copy_att',.TRUE.)

  END DO

  IF (fixdim) THEN
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'WEST-EAST_PATCH_START_UNSTAG', NF_INT,1,iss)
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'WEST-EAST_PATCH_END_UNSTAG',   NF_INT,1,ise)
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'WEST-EAST_PATCH_START_STAG',   NF_INT,1,ips)
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'WEST-EAST_PATCH_END_STAG',     NF_INT,1,ipe)
    CALL handle_ncd_error(istatus,'copy_wrf_definitions:NF_PUT_ATT_INT',.TRUE.)

    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'SOUTH-NORTH_PATCH_START_UNSTAG', NF_INT,1,jss)
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'SOUTH-NORTH_PATCH_END_UNSTAG',   NF_INT,1,jse)
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'SOUTH-NORTH_PATCH_START_STAG',   NF_INT,1,jps)
    istatus = NF_PUT_ATT_INT (fout,NF_GLOBAL,'SOUTH-NORTH_PATCH_END_STAG',     NF_INT,1,jpe)
    CALL handle_ncd_error(istatus,'copy_wrf_definitions:NF_PUT_ATT_INT',.TRUE.)
  END IF

  lenstr = LEN_TRIM(filetitle)
  istatus = NF_PUT_ATT_TEXT(fout,NF_GLOBAL,'TITLE',lenstr,TRIM(filetitle))
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:NF_PUT_ATT_TEXT',.TRUE.)

  lenstr = LEN_TRIM(DateStr)
  istatus = NF_PUT_ATT_TEXT(fout,NF_GLOBAL,'START_DATE',lenstr,TRIM(DateStr))
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:NF_PUT_ATT_TEXT',.TRUE.)

!-----------------------------------------------------------------------
!
! Copy variable definitions
!
!-----------------------------------------------------------------------

  istatus = nf_inq_nvars(fin,nvars)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_inq_nvars',.TRUE.)

  DO varid = 1, nvars
    istatus = nf_inq_var(fin,varid,varname,vartype,varndims,vardimids,varnatts)
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_inq_var',.TRUE.)

    IF (nvar > 0 .AND. (.NOT. name_in_set(varname,var_names,nvar)) ) CYCLE

    istatus = nf_def_var(fout,varname,vartype,varndims,vardimids,ovarid)
    IF (istatus /= NF_NOERR) &
      CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_def_var'//TRIM(varname),.TRUE.)

    !IF (trim(varname) == 'PHB') write(*,*) 'dimids = ',vardimids
    !
    ! Copy variable attributes
    !
    DO attnum = 1,varnatts
      istatus = nf_inq_attname(fin,varid,attnum,attname)
      IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_inq_attname',.TRUE.)

      istatus = nf_copy_att(fin,varid,attname,fout,ovarid)
      IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_copy_att',.TRUE.)
    END DO

    !
    ! deflate
    !
    istatus = NF_INQ_VAR_DEFLATE(fin, varid, varshuffle, vardeflate, vardeflate_level)
    IF (istatus == NF_NOERR) THEN
      istatus = NF_DEF_VAR_DEFLATE(fout, ovarid, varshuffle, vardeflate, vardeflate_level)
      IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_def_var_deflate',.FALSE.)
    END IF

  END DO

!-----------------------------------------------------------------------
!
! Add additional variable definitions
!
!-----------------------------------------------------------------------

  IF (naddition > 0 ) THEN

    !
    ! Use QVAPOR as template variable
    !
    istatus = nf_inq_varid(fin,'QVAPOR',varid)
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_inq_varid',.TRUE.)

    istatus = nf_inq_var(fin,varid,varname,vartype,varndims,vardimids,varnatts)
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_inq_var',.TRUE.)

    DO varnum = 1, naddition

      varname = var_add(varnum,1)

      istatus = nf_def_var(fout,varname,vartype,varndims,vardimids,ovarid)
      IF (istatus /= NF_NOERR) &
        CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_def_var'//TRIM(varname),.TRUE.)

      !
      ! Copy variable attributes from template variable
      !
      DO attnum = 1,varnatts
        istatus = nf_inq_attname(fin,varid,attnum,attname)
        IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_inq_attname',.TRUE.)

        IF (TRIM(attname) == 'description') THEN
          istatus = nf_put_att_text(fout,ovarid,attname,                &
                       LEN_TRIM(var_add(varnum,2)),TRIM(var_add(varnum,2)))
          IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_put_att_text:description',.TRUE.)
        ELSE IF (TRIM(attname) == 'units') THEN
          istatus = nf_put_att_text(fout,ovarid,attname,                &
                       LEN_TRIM(var_add(varnum,3)),TRIM(var_add(varnum,3)))
          IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_put_att_text:units',.TRUE.)
        ELSE
          istatus = nf_copy_att(fin,varid,attname,fout,ovarid)
          IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_copy_att',.TRUE.)
        END IF
      END DO

      !
      ! deflate
      !
      istatus = NF_INQ_VAR_DEFLATE(fin, varid, varshuffle, vardeflate, vardeflate_level)
      IF (istatus == NF_NOERR) THEN
        istatus = NF_DEF_VAR_DEFLATE(fout, ovarid, varshuffle, vardeflate, vardeflate_level)
        IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_def_var_deflate',.FALSE.)
      END IF

      !
      ! Add an extra attrible "_FillValue"
      !
      attname = '_FillValue'
      istatus = nf_put_att_real(fout,ovarid,attname,NF_FLOAT,1,-9999.0_4)
      IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_put_att_real:_FillValue',.TRUE.)

    END DO

  END IF

!-----------------------------------------------------------------------
!
! End file definition mode
!
!-----------------------------------------------------------------------

  istatus = nf_enddef(fout)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:nf_enddef',.TRUE.)

!-----------------------------------------------------------------------
!
! Write time string (it needs an updated value).
!
!-----------------------------------------------------------------------

  istatus = nf_inq_varid(fout,'Times',ovarid)

  istatus = NF_PUT_VARA_TEXT(fout,ovarid,(/1,1/),(/19,1/),DateStr)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrf_definitions:NF_PUT_VARA_TEXT',.TRUE.)

  RETURN
END SUBROUTINE copy_wrf_definitions

!############ Copy variables from one file to another file #############

SUBROUTINE copy_wrfinput_vars ( fin, fout, keep_excl,nexcl, excludesets,istatus )

  USE model_precision

  IMPLICIT NONE

  INTEGER,           INTENT(IN)  :: fin, fout
  INTEGER,           INTENT(IN)  :: nexcl
  LOGICAL,           INTENT(IN)  :: keep_excl  ! .TRUE.,  Keep "excludesets"
                                               ! .FALSE., Exclude "excludesets"
  CHARACTER(LEN=20), INTENT(IN)  :: excludesets(nexcl)
  INTEGER,           INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  CHARACTER(LEN=32) :: varname

  INTEGER :: varid, nvars, idim, ovarid
  INTEGER :: vartype, varndims, varnatts, vardimids(NF_MAX_DIMS)

  INTEGER :: narrsize, narrisizemax, narrasizemax
  INTEGER :: startidx(NF_MAX_DIMS), countidx(NF_MAX_DIMS)

  INTEGER,  ALLOCATABLE :: varari(:)
  REAL(SP), ALLOCATABLE :: vararr(:)

!-----------------------------------------------------------------------

  LOGICAL :: name_in_set
  INTEGER :: i,j,k

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  istatus = nf_inq_nvars(fin,nvars)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrfinput_vars:nf_inq_nvars',.TRUE.)

  narrisizemax = 0
  narrasizemax = 0

  startidx(:)  = 1

  DO varid = 1, nvars

    countidx(:)  = 1
    narrsize     = 1

    istatus = nf_inq_var(fin,varid,varname,vartype,varndims,vardimids,varnatts)
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrfinput_vars:nf_inq_var',.TRUE.)

    IF (keep_excl) THEN  ! Keep
      IF (.NOT. name_in_set(varname,excludesets,nexcl)) CYCLE
    ELSE  ! exclude
      IF ( name_in_set(varname,excludesets,nexcl)) CYCLE
    END IF

    !istatus = nf_copy_var(fin,varid,fout)
    !IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrfinput_vars:nf_copy_var:'//TRIM(varname),.TRUE.)

    DO idim = 1, varndims
      istatus = nf_inq_dimlen(fin,vardimids(idim),countidx(idim))
      IF (istatus /= NF_NOERR)   &
        CALL handle_ncd_error(istatus,'copy_wrfinput_vars:nf_inq_dimlen'//TRIM(varname),.TRUE.)

      narrsize = narrsize*countidx(idim)
    END DO

    !
    ! Get output variable id
    !
    istatus = nf_inq_varid(fout,varname,ovarid)
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'copy_wrfinput_vars:nf_inq_varid',.TRUE.)

    SELECT CASE (vartype)

    CASE (NF_INT)
      IF (narrsize > narrisizemax) THEN   ! Allocate input array only when necessary
        IF (ALLOCATED(varari)) DEALLOCATE(varari, STAT = istatus)
        ALLOCATE(varari(narrsize), STAT = istatus)
        narrisizemax = narrsize
      END IF

      istatus = NF_GET_VARA_INT(fin,varid,startidx,countidx,varari)
      IF (istatus /= NF_NOERR)  &
        CALL handle_ncd_error(istatus,'copy_wrfinput_vars:NF_GET_VARA_INT:'//TRIM(varname),.TRUE.)

      istatus = NF_PUT_VARA_INT(fout,ovarid,startidx,countidx,varari)
      IF (istatus /= NF_NOERR)  &
        CALL handle_ncd_error(istatus,'copy_wrfinput_vars:nf_put_vara_INT:'//TRIM(varname),.TRUE.)

    CASE (NF_FLOAT)

      IF (narrsize > narrasizemax) THEN   ! Allocate input array only when necessary
        IF (ALLOCATED(vararr)) DEALLOCATE(vararr, STAT = istatus)
        ALLOCATE(vararr(narrsize), STAT = istatus)
        narrasizemax = narrsize
      END IF

      istatus = NF_GET_VARA_REAL(fin,varid,startidx,countidx,vararr)
      IF (istatus /= NF_NOERR)  &
        CALL handle_ncd_error(istatus,'copy_wrfinput_vars:NF_GET_VARA_REAL:'//TRIM(varname),.TRUE.)

      istatus = NF_PUT_VARA_REAL(fout,ovarid,startidx,countidx,vararr)
      IF (istatus /= NF_NOERR)  &
        CALL handle_ncd_error(istatus,'copy_wrfinput_vars:nf_put_vara_REAL:'//TRIM(varname),.TRUE.)
    !CASE (NF_CHAR)
    CASE DEFAULT
      WRITE(6,'(1x,a,I2,2a)') 'ERROR: unsupported variable type = ',vartype,' for variable ',TRIM(varname)
      istatus = -4
    END SELECT

  END DO

  IF (ALLOCATED(varari)) DEALLOCATE(varari)
  IF (ALLOCATED(vararr)) DEALLOCATE(vararr)

  RETURN
END SUBROUTINE copy_wrfinput_vars

SUBROUTINE wrf_fix_title( fout, filetitle, DateStr, istatus)

  IMPLICIT NONE

  INTEGER,           INTENT(IN)  :: fout
  CHARACTER(LEN=*),  INTENT(IN)  :: filetitle
  CHARACTER(LEN=19), INTENT(IN)  :: DateStr
  INTEGER,           INTENT(OUT) :: istatus

  INCLUDE 'netcdf.inc'

!-----------------------------------------------------------------------

  INTEGER :: lenstr
  CHARACTER(LEN=20) :: attname

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  attname = 'TITLE'
  lenstr = LEN_TRIM(filetitle)
  istatus = NF_PUT_ATT_TEXT(fout,NF_GLOBAL,TRIM(attname),lenstr,TRIM(filetitle))
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'wrf_fix_title:NF_PUT_ATT_TEXT',.TRUE.)

  attname = 'START_DATE'
  lenstr = LEN_TRIM(DateStr)
  istatus = NF_PUT_ATT_TEXT(fout,NF_GLOBAL,TRIM(attname),lenstr,TRIM(DateStr))
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'wrf_fix_title:NF_PUT_ATT_TEXT',.TRUE.)

  RETURN
END SUBROUTINE wrf_fix_title


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%
!%%%% The following was copied from WRF2ARPS. Use for reading WRF files.
!%%%%
!%%%% NOTE: They should be merged with above later
!%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!!!***!
!!!***!##################################################################
!!!***!##################################################################
!!!***!######                                                      ######
!!!***!######               SUBROUTINE open_ncd_file               ######
!!!***!######                                                      ######
!!!***!######                     Developed by                     ######
!!!***!######     Center for Analysis and Prediction of Storms     ######
!!!***!######                University of Oklahoma                ######
!!!***!######                                                      ######
!!!***!##################################################################
!!!***!##################################################################
!!!***!
!!!***SUBROUTINE open_ncd_file(filename,ncidout)
!!!***!
!!!***!------------------------------------------------------------------
!!!***!
!!!***!  PURPOSE:
!!!***!
!!!***!    Open a WRF file and return NetCDF file handler.
!!!***!    NOTE: it is required to call close_wrf_file explicitly to close
!!!***!          the opened file in your calling program.
!!!***!
!!!***!------------------------------------------------------------------
!!!***
!!!***  IMPLICIT NONE
!!!***  CHARACTER(LEN=*), INTENT(IN)  :: filename
!!!***  INTEGER,          INTENT(OUT) :: ncidout
!!!***
!!!***!------------------------------------------------------------------
!!!***!
!!!***!  Misc. local variable
!!!***!
!!!***!------------------------------------------------------------------
!!!***  INTEGER :: istatus
!!!***  LOGICAL :: fexists
!!!***
!!!***  INCLUDE 'netcdf.inc'
!!!***
!!!***!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!***!
!!!***!  Begining of executable code
!!!***!
!!!***!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!***
!!!***  INQUIRE(FILE = filename, EXIST = fexists)
!!!***  IF (fexists) THEN
!!!***    istatus = NF_OPEN(TRIM(filename),NF_NOWRITE,ncidout)
!!!***    CALL handle_ncd_error(istatus,'NF_OPEN in open_wrf_file',.FALSE.)
!!!***  ELSE
!!!***    WRITE(6,'(2a)') 'File not found: ', filename
!!!***    STOP 'open_wrf_file'
!!!***  ENDIF
!!!***
!!!***  RETURN
!!!***
!!!***END SUBROUTINE open_ncd_file
!!!***!
!!!***!##################################################################
!!!***!##################################################################
!!!***!######                                                      ######
!!!***!######              SUBROUTINE close_ncd_file               ######
!!!***!######                                                      ######
!!!***!######                     Developed by                     ######
!!!***!######     Center for Analysis and Prediction of Storms     ######
!!!***!######                University of Oklahoma                ######
!!!***!######                                                      ######
!!!***!##################################################################
!!!***!##################################################################
!!!***!
!!!***SUBROUTINE close_ncd_file(ncid)
!!!***!
!!!***!------------------------------------------------------------------
!!!***!
!!!***!  PURPOSE:
!!!***!
!!!***!     Close the WRF file which is opened using open_wrf_file.
!!!***!
!!!***!------------------------------------------------------------------
!!!***  IMPLICIT NONE
!!!***
!!!***  INTEGER, INTENT(IN) :: ncid
!!!***
!!!***!------------------------------------------------------------------
!!!***!
!!!***!  Misc. local variable
!!!***!
!!!***!------------------------------------------------------------------
!!!***!
!!!***  INTEGER :: istatus
!!!***
!!!***  INCLUDE 'netcdf.inc'
!!!***
!!!***!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!***!
!!!***!  Beginning of executable code...
!!!***!
!!!***!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!***!
!!!***
!!!***  istatus = NF_CLOSE(ncid)
!!!***  CALL handle_ncd_error(istatus,'NF_CLOSE in close_wrf_file',.FALSE.)
!!!***
!!!***  RETURN
!!!***END SUBROUTINE close_ncd_file
!
!##################################################################
!##################################################################
!######                                                      ######
!######            SUBROUTINE get_ncd_frames_per_outfile     ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_ncd_frames_per_outfile(filename,numframes,istatus)

!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Get the size of unlimitted dimension in the file
!
!-----------------------------------------------------------------------
!
!  AUTHOR:
!  Yunheng Wang (03/26/2007)
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------

  IMPLICIT NONE

  CHARACTER(LEN=*), INTENT(IN)  :: filename
  INTEGER,          INTENT(OUT) :: numframes
  INTEGER,          INTENT(OUT) :: istatus

!------------------------------------------------------------------
!
!  Misc. local variables
!
!------------------------------------------------------------------

  INTEGER :: ncid
  INTEGER :: dimid
  INTEGER :: timelen

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus   = 0
  numframes = 0

  CALL open_ncd_wrf_file ( TRIM(filename), 'r', ncid, istatus )

  istatus = NF_INQ_DIMID(ncid,'Time',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_ncd_frames_per_outfile',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,timelen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_ncd_frames_per_outfile',.FALSE.)
  IF( timelen < 1) THEN
    WRITE(6,'(1x,3a,I2)') 'ERROR: The unlimited dimension in the file ',  &
                          TRIM(filename),' is bad, timelen = ',timelen, '.'
    istatus = -1
  ELSE
    numframes = timelen
  END IF

  CALL close_ncd_wrf_file ( ncid, istatus )

  RETURN
END SUBROUTINE get_ncd_frames_per_outfile
!
!##################################################################
!##################################################################
!######                                                      ######
!######            SUBROUTINE get_ncd_next_times             ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_ncd_next_time(ncid,itime,timestr,istatus)
!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Read the the Date String in the WRF outputs at specified time
!
!-----------------------------------------------------------------------
  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: ncid     ! NetCDF file handler
  INTEGER, INTENT(IN)  :: itime    ! Time dimension value
                                   ! this is the unlimited dimension
  CHARACTER(LEN=*), INTENT(OUT) :: timestr
  INTEGER,          INTENT(OUT) :: istatus

!------------------------------------------------------------------
!
!  Misc. local variables
!
!------------------------------------------------------------------
  INTEGER :: dimid, varid
  INTEGER :: timelen, strlen, dateStrLen

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = NF_INQ_DIMID(ncid,'Time',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_wrf_Times',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,timelen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_Times',.FALSE.)
  IF(itime > timelen .OR. itime < 1) THEN
    WRITE(6,'(a,I2,a,I2)') ' The unlimited dimension in the file is ',  &
                           timelen, ' itime is ',itime
    STOP 'Time_out_of_bound'
  END IF

  istatus = NF_INQ_DIMID(ncid,'DateStrLen',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_wrf_Times',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,dateStrLen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_Times',.FALSE.)

  strlen = LEN(timestr)
  IF(strlen < dateStrLen) THEN
    WRITE(6,'(a)') ' Date string is not long enough to hold Times.'
    STOP 'timestr_too_short'
  END IF

  istatus = NF_INQ_VARID(ncid, 'Times', varid)
  CALL handle_ncd_error(istatus,'NF_INQ_VARID in get_wrf_Times',.FALSE.)
  istatus = NF_GET_VARA_TEXT(ncid,varid,(/1,itime/),                    &
                             (/dateStrLen,1/),timestr)
  CALL handle_ncd_error(istatus,'NF_GET_VARA_TEXT in get_wrf_Times',.FALSE.)

  RETURN
END SUBROUTINE get_ncd_next_time
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE get_wrf_att                ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_ncd_dom_ti_integer(ncid,element,val,ireturn)
!-----------------------------------------------------------------------
!
! PURPOSE
!
!   Retieve WRF grib information from the NetCDF file which are stored
!   as Global attributes.
!
!-----------------------------------------------------------------------
  IMPLICIT NONE

  INTEGER,      INTENT(IN)  :: ncid
  CHARACTER(*), INTENT(IN)  :: element
  INTEGER,      INTENT(OUT) :: val
  INTEGER,      INTENT(OUT) :: ireturn

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  ireturn = NF_GET_ATT_INT(ncid,NF_GLOBAL,element,val)
  CALL handle_ncd_error(ireturn,'NF_GET_ATT_INT ('//TRIM(element)//') in get_ncd_dom_ti_integer',.FALSE.)

  RETURN
END SUBROUTINE get_ncd_dom_ti_integer

SUBROUTINE get_ncd_dom_ti_real(ncid,element,val,ireturn)
!-----------------------------------------------------------------------
!
! PURPOSE
!
!   Retieve WRF grib information from the NetCDF file which are stored
!   as Global attributes.
!
!-----------------------------------------------------------------------
  IMPLICIT NONE

  INTEGER,      INTENT(IN)  :: ncid
  CHARACTER(*), INTENT(IN)  :: element
  REAL,         INTENT(OUT) :: val
  INTEGER,      INTENT(OUT) :: ireturn

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  ireturn = NF_GET_ATT_REAL(ncid,NF_GLOBAL,element,val)
  CALL handle_ncd_error(ireturn,'NF_GET_ATT_REAL in get_ncd_dom_ti_real',.FALSE.)

  RETURN
END SUBROUTINE get_ncd_dom_ti_real

SUBROUTINE get_ncd_dom_ti_char(ncid,element,val,ireturn)
!-----------------------------------------------------------------------
!
! PURPOSE
!
!   Retieve WRF grib information from the NetCDF file which are stored
!   as Global attributes.
!
!-----------------------------------------------------------------------
  IMPLICIT NONE

  INTEGER,      INTENT(IN)  :: ncid
  CHARACTER(*), INTENT(IN)  :: element
  CHARACTER(*), INTENT(OUT) :: val
  INTEGER,      INTENT(OUT) :: ireturn

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  ireturn = NF_GET_ATT_TEXT(ncid,NF_GLOBAL,element,val)
  CALL handle_ncd_error(ireturn,'NF_GET_ATT_TEXT in get_ncd_dom_ti_char.',.FALSE.)

  RETURN
END SUBROUTINE get_ncd_dom_ti_char
!
!##################################################################
!##################################################################
!######                                                      ######
!######             SUBROUTINE get_ncd_1d                    ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_ncd_1d(ncid,itime,varname,nx,var1d,istatus)
!-----------------------------------------------------------------------
!
! PURPOSE:
!
!   Read in a 1D array from the WRF NetCDF file.
!
!-----------------------------------------------------------------------
  USE model_precision

  IMPLICIT NONE
  INTEGER,          INTENT(IN)  :: ncid
  INTEGER,          INTENT(IN)  :: itime
  CHARACTER(LEN=*), INTENT(IN)  :: varname
  INTEGER,          INTENT(IN)  :: nx
  REAL(SP),         INTENT(OUT) :: var1d(nx)
  INTEGER,          INTENT(OUT) :: istatus
!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER                    :: varid
  CHARACTER(LEN=NF_MAX_NAME) :: namein
  INTEGER                    :: vartype, ndims,natts,dimlen
  INTEGER                    :: dimids(NF_MAX_VAR_DIMS)


!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = NF_INQ_VARID(ncid,varname,varid)
  CALL handle_ncd_error(istatus,'NF_INQ_VARID ('//TRIM(varname)//') in get_wrf_1d',.FALSE.)

  istatus = NF_INQ_VAR(ncid,varid,namein,vartype,ndims,dimids,natts)
  CALL handle_ncd_error(istatus,'NF_INQ_VAR ('//TRIM(varname)//') in get_wrf_1d',.FALSE.)

  IF(vartype /= NF_FLOAT) THEN
    WRITE(6,'(3a)') 'Variable ',varname, ' is not REAL.'
    STOP 'WRONG_VAR_TYPE'
  END IF

  IF(ndims /= 2) THEN
    WRITE(6,'(3a)') 'Variable ', varname, ' is not a 1D array.'
    STOP 'WRONG_VAR_DIMENSIONS'
  END IF

  istatus = NF_INQ_DIMLEN(ncid,dimids(1),dimlen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN ('//TRIM(varname)//') in get_wrf_1d',.FALSE.)
  IF(dimlen /= nx) THEN
    WRITE(6,'(3a,I3,a,I0)') 'The dimension of variable ', varname,    &
                    ' is ',dimlen, ' and it should be ',nx
    STOP 'WRONG_DIM_length'
  END IF

  istatus = NF_INQ_DIMLEN(ncid,dimids(2),dimlen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN ('//TRIM(varname)//') in get_wrf_1d',.FALSE.)
  IF(dimlen < itime) THEN
    WRITE(6,'(a,I3,a,I3)') 'The total records number is ', dimlen,     &
                    ' however, the required time level is ',itime
    STOP 'itime_tool_large'
  END IF

  istatus = NF_GET_VARA_REAL(ncid,varid,(/1,itime/),(/nx,1/),var1d)
  CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL ('//TRIM(varname)//') in get_wrf_1d',.FALSE.)

  RETURN
END SUBROUTINE get_ncd_1d
!
!##################################################################
!##################################################################
!######                                                      ######
!######             SUBROUTINE get_ncd_scalar                ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_ncd_scalar(ncid,itime,varname,var,istatus)
!-----------------------------------------------------------------------
!
! PURPOSE:
!
!   Read in a scalar from the WRF NetCDF file.
!
!-----------------------------------------------------------------------

  USE model_precision

  IMPLICIT NONE
  INTEGER,          INTENT(IN)  :: ncid
  INTEGER,          INTENT(IN)  :: itime
  CHARACTER(LEN=*), INTENT(IN)  :: varname
  REAL(SP),         INTENT(OUT) :: var
  INTEGER,          INTENT(OUT) :: istatus
!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER                    :: varid
  CHARACTER(LEN=NF_MAX_NAME) :: namein
  INTEGER                    :: vartype, ndims,natts,dimlen
  INTEGER                    :: dimids(NF_MAX_VAR_DIMS)

  !REAL :: varin(1)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = NF_INQ_VARID(ncid,varname,varid)
  CALL handle_ncd_error(istatus,'NF_INQ_VARID ('//TRIM(varname)//') in get_ncd_scalar',.FALSE.)

  istatus = NF_INQ_VAR(ncid,varid,namein,vartype,ndims,dimids,natts)
  CALL handle_ncd_error(istatus,'NF_INQ_VAR ('//TRIM(varname)//') in get_ncd_scalar',.FALSE.)

  IF(vartype /= NF_FLOAT) THEN
    WRITE(6,'(3a)') 'Variable ',varname, ' is not REAL.'
    STOP 'WRONG_VAR_TYPE'
  END IF

  IF(ndims /= 1) THEN
    WRITE(6,'(3a)') 'Variable ', varname, ' is not a scalar.'
    STOP 'WRONG_VAR_DIMENSIONS'
  END IF

  istatus = NF_INQ_DIMLEN(ncid,dimids(1),dimlen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_1d',.FALSE.)
  IF(dimlen < itime) THEN
    WRITE(6,'(a,I3,a,I3)') 'The total records number is ', dimlen,     &
                    ' however, the required time level is ',itime
    STOP 'itime_tool_large'
  END IF

  istatus = NF_GET_VARA_REAL(ncid,varid,(/itime/),(/1/),var)
  !istatus = NF_GET_VAR_REAL(ncid,varid,varin)
  CALL handle_ncd_error(istatus,'NF_GET_VAR_REAL in get_wrf_scalar',.FALSE.)

  RETURN
END SUBROUTINE get_ncd_scalar
!
!##################################################################
!##################################################################
!######                                                      ######
!######             SUBROUTINE get_ncd_2d                    ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_ncd_2d(ncid,itime,varname,nx,ny,var2d,istatus)
!-----------------------------------------------------------------------
!
! PURPOSE:
!
!   Read in a 2D array from the WRF NetCDF file.
!
!-----------------------------------------------------------------------
  USE model_precision

  IMPLICIT NONE
  INTEGER,          INTENT(IN)  :: ncid
  INTEGER,          INTENT(IN)  :: itime
  CHARACTER(LEN=*), INTENT(IN)  :: varname
  INTEGER,          INTENT(IN)  :: nx
  INTEGER,          INTENT(IN)  :: ny
  REAL(SP),         INTENT(OUT) :: var2d(nx,ny)
  INTEGER,          INTENT(OUT) :: istatus
!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER                    :: varid
  CHARACTER(LEN=NF_MAX_NAME) :: namein
  INTEGER                    :: vartype, ndims,natts,dimlen
  INTEGER                    :: dimids(NF_MAX_VAR_DIMS)

  INTEGER, PARAMETER         :: VAR_NOTEXIST = -1

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = NF_INQ_VARID(ncid,varname,varid)
  IF(istatus == NF_ENOTVAR) THEN
     WRITE(6,'(3a)') ' WARNING: variable ',TRIM(varname),' does not exist.'
     var2d(:,:) = -9999.0
     istatus = VAR_NOTEXIST
     RETURN
  END IF
  CALL handle_ncd_error(istatus,'NF_INQ_VARID in get_ncd_2d with '//TRIM(varname),.FALSE.)

  istatus = NF_INQ_VAR(ncid,varid,namein,vartype,ndims,dimids,natts)
  CALL handle_ncd_error(istatus,'NF_INQ_VAR in get_ncd_2d',.FALSE.)

  IF(vartype /= NF_FLOAT) THEN
    WRITE(6,'(3a)') 'Variable ',varname, ' is not REAL.'
    STOP 'WRONG_VAR_TYPE'
  END IF

  IF(ndims /= 3) THEN
    WRITE(6,'(3a)') 'Variable ', varname, ' is not a 2D array.'
    STOP 'WRONG_VAR_DIMENSIONS'
  END IF

  istatus = NF_INQ_DIMLEN(ncid,dimids(1),dimlen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_2d',.FALSE.)
  IF(dimlen /= nx) THEN
    WRITE(6,'(3a,I3,a,I0)') 'First dimension of variable ', varname,    &
                    ' is ',dimlen, ' and it should be ',nx
    STOP 'WRONG_DIM_length'
  END IF

  istatus = NF_INQ_DIMLEN(ncid,dimids(2),dimlen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_2d',.FALSE.)
  IF(dimlen /= ny) THEN
    WRITE(6,'(3a,I3,a,I0)') 'Second dimension of variable ', varname,   &
                    ' is ',dimlen, ' and it should be ',ny
    STOP 'WRONG_DIM_length'
  END IF

  istatus = NF_INQ_DIMLEN(ncid,dimids(3),dimlen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_2d',.FALSE.)
  IF(dimlen < itime) THEN
    WRITE(6,'(a,I3,a,I3)') 'The total records number is ', dimlen,     &
                    ' however, the required time level is ',itime
    STOP 'itime_tool_large'
  END IF

  istatus = NF_GET_VARA_REAL(ncid,varid,(/1,1,itime/),(/nx,ny,1/),var2d)
  CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in get_wrf_2d',.FALSE.)

  RETURN
END SUBROUTINE get_ncd_2d
!
!##################################################################
!##################################################################
!######                                                      ######
!######             SUBROUTINE get_ncd_2di                   ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_ncd_2di(ncid,itime,varname,nx,ny,var2d,istatus)
!-----------------------------------------------------------------------
!
! PURPOSE:
!
!   Read in a 2D INTEGER array from the WRF NetCDF file.
!
!-----------------------------------------------------------------------

  IMPLICIT NONE
  INTEGER,          INTENT(IN)  :: ncid
  INTEGER,          INTENT(IN)  :: itime
  CHARACTER(LEN=*), INTENT(IN)  :: varname
  INTEGER,          INTENT(IN)  :: nx
  INTEGER,          INTENT(IN)  :: ny
  INTEGER,          INTENT(OUT) :: var2d(nx,ny)
  INTEGER,          INTENT(OUT) :: istatus
!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER                    :: varid
  CHARACTER(LEN=NF_MAX_NAME) :: namein
  INTEGER                    :: vartype, ndims,natts,dimlen
  INTEGER                    :: dimids(NF_MAX_VAR_DIMS)


!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = NF_INQ_VARID(ncid,varname,varid)
  CALL handle_ncd_error(istatus,'NF_INQ_VARID in get_wrf_2di',.FALSE.)

  istatus = NF_INQ_VAR(ncid,varid,namein,vartype,ndims,dimids,natts)
  CALL handle_ncd_error(istatus,'NF_INQ_VAR in get_wrf_2di',.FALSE.)

  IF(vartype /= NF_INT) THEN
    WRITE(6,'(3a)') 'Variable ',varname, ' is not INTEGER.'
    STOP 'WRONG_VAR_TYPE'
  END IF

  IF(ndims /= 3) THEN
    WRITE(6,'(3a)') 'Variable ', varname, ' is not a 2D array.'
    STOP 'WRONG_VAR_DIMENSIONS'
  END IF

  istatus = NF_INQ_DIMLEN(ncid,dimids(1),dimlen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_2di',.FALSE.)
  IF(dimlen /= nx) THEN
    WRITE(6,'(3a,I3,a,I0)') 'First dimension of variable ', varname,    &
                    ' is ',dimlen, ' and it should be ',nx
    STOP 'WRONG_DIM_length'
  END IF

  istatus = NF_INQ_DIMLEN(ncid,dimids(2),dimlen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_2di',.FALSE.)
  IF(dimlen /= ny) THEN
    WRITE(6,'(3a,I3,a,I0)') 'Second dimension of variable ', varname,   &
                    ' is ',dimlen, ' and it should be ',ny
    STOP 'WRONG_DIM_length'
  END IF

  istatus = NF_INQ_DIMLEN(ncid,dimids(3),dimlen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_2di',.FALSE.)
  IF(dimlen < itime) THEN
    WRITE(6,'(a,I3,a,I0)') 'The total records number is ', dimlen,     &
                    ' however, the required time level is ',itime
    STOP 'itime_tool_large'
  END IF

  istatus = NF_GET_VARA_INT(ncid,varid,(/1,1,itime/),(/nx,ny,1/),var2d)
  CALL handle_ncd_error(istatus,'NF_GET_VARA_INT in get_wrf_2di',.FALSE.)

  RETURN
END SUBROUTINE get_ncd_2di
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE get_ncd_3d                   ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_ncd_3d(ncid,itime,varname,nx,ny,nz,var3d,istatus)
!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Read in a 3D array from the WRF NetCDF file
!
!-----------------------------------------------------------------------
  USE model_precision

  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: ncid
  INTEGER,          INTENT(IN)  :: itime
  CHARACTER(LEN=*), INTENT(IN)  :: varname
  INTEGER,          INTENT(IN)  :: nx
  INTEGER,          INTENT(IN)  :: ny
  INTEGER,          INTENT(IN)  :: nz
  REAL(SP),         INTENT(OUT) :: var3d(nx,ny,nz)
  INTEGER,          INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------
  INCLUDE 'netcdf.inc'

  INTEGER                    :: varid
  CHARACTER(LEN=NF_MAX_NAME) :: namein
  INTEGER                    :: vartype, ndims,natts,dimlen
  INTEGER                    :: dimids(NF_MAX_VAR_DIMS)

  INTEGER, PARAMETER         :: VAR_NOTEXIST = -1

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = NF_INQ_VARID(ncid,varname,varid)
  IF(istatus == NF_ENOTVAR) THEN
     WRITE(6,'(3a)') ' WARNING: variable ',TRIM(varname),' does not exist.'
     var3d(:,:,:) = -999.0
     istatus = VAR_NOTEXIST
     RETURN
  END IF
  CALL handle_ncd_error(istatus,'NF_INQ_VARID in get_ncd_3d',.FALSE.)

  istatus = NF_INQ_VAR(ncid,varid,namein,vartype,ndims,dimids,natts)
  CALL handle_ncd_error(istatus,'NF_INQ_VAR in get_ncd_3d',.FALSE.)

  IF(vartype /= NF_FLOAT) THEN
    WRITE(6,'(3a)') 'Variable ',TRIM(varname), ' is not REAL.'
    STOP 'WRONG_VAR_TYPE'
  END IF

  IF(ndims /= 4) THEN
    WRITE(6,'(3a)') 'Variable ', TRIM(varname), ' is not a 3D array.'
    STOP 'WRONG_VAR_DIMENSIONS'
  END IF

  istatus = NF_INQ_DIMLEN(ncid,dimids(1),dimlen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_ncd_3d',.FALSE.)
  IF(dimlen /= nx) THEN
    WRITE(6,'(3a,I3,a,I3)') 'First dimension of variable ', TRIM(varname),    &
                    ' is ',dimlen, ' and it should be ',nx
    STOP 'WRONG_DIM_length'
  END IF

  istatus = NF_INQ_DIMLEN(ncid,dimids(2),dimlen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_ncd_3d',.FALSE.)
  IF(dimlen /= ny) THEN
    WRITE(6,'(3a,I3,a,I3)') 'Second dimension of variable ', TRIM(varname),   &
                    ' is ',dimlen, ' and it should be ',ny
    STOP 'WRONG_DIM_length'
  END IF

  istatus = NF_INQ_DIMLEN(ncid,dimids(3),dimlen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_ncd_3d',.FALSE.)
  IF(dimlen /= nz) THEN
    WRITE(6,'(3a,I3,a,I3)') 'Third dimension of variable ', TRIM(varname),   &
                    ' is ',dimlen, ' and it should be ',nz
    STOP 'WRONG_DIM_length'
  END IF

  istatus = NF_INQ_DIMLEN(ncid,dimids(4),dimlen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_ncd_3d',.FALSE.)
  IF(dimlen < itime) THEN
    WRITE(6,'(a,I3,a,I3)') 'The total records number is ', dimlen,     &
                    ' however, the required time level is ',itime
    STOP 'itime_tool_large'
  END IF

  istatus = NF_GET_VARA_REAL(ncid,varid,(/1,1,1,itime/),               &
                             (/nx,ny,nz,1/),var3d)
  IF(istatus /= NF_NOERR .OR. istatus /= NF_EEXIST) THEN
    CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in get_ncd_3d:'//varname,.FALSE.)
  END IF

  RETURN
END SUBROUTINE get_ncd_3d
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE get_ncd_dimension            ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_ncd_dimension(ncid,nx_ext,ny_ext,nz_ext,nzsoil_ext,istatus)
!
!------------------------------------------------------------------------
!
! PURPOSE:
!
!   Read dimension parameters and the first time string from
!   WRF output file.
!
!   Please note this subroutine will open a file to read and then close
!   it. So it does not leave any opened handler for NetCDF file.
!
!------------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: ncid
  INTEGER,          INTENT(OUT) :: nx_ext
  INTEGER,          INTENT(OUT) :: ny_ext
  INTEGER,          INTENT(OUT) :: nz_ext
  INTEGER,          INTENT(OUT) :: nzsoil_ext
  INTEGER,          INTENT(OUT) :: istatus

!------------------------------------------------------------------------
!
!  Misc. Local variables
!
!------------------------------------------------------------------------

  INTEGER :: dimid

  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Begining of executable code
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

!-----------------------------------------------------------------------
!
! Get WRF dimensions
!
!-----------------------------------------------------------------------

  istatus = NF_INQ_DIMID(ncid,'west_east_stag',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_ncd_dimension',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,nx_ext)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_ncd_dimension',.FALSE.)

  istatus = NF_INQ_DIMID(ncid,'south_north_stag',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_ncd_dimension',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,ny_ext)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_ncd_dimension',.FALSE.)

  istatus = NF_INQ_DIMID(ncid,'bottom_top_stag',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_ncd_dimension',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,nz_ext)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_ncd_dimension',.FALSE.)

  istatus = NF_INQ_DIMID(ncid,'soil_layers_stag',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_ncd_dimension',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,nzsoil_ext)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_ncd_dimension',.FALSE.)

  RETURN
END SUBROUTINE get_ncd_dimension

!############ Get variable list from one netcdf file #############

SUBROUTINE get_ncd_3dvars ( fin, maxnvar, varset, varstag, nvars, istatus )

  IMPLICIT NONE

  INTEGER,           INTENT(IN)  :: fin
  INTEGER,           INTENT(IN)  :: maxnvar
  CHARACTER(LEN=20), INTENT(OUT) :: varset(maxnvar)
  CHARACTER(LEN=1),  INTENT(OUT) :: varstag(maxnvar)
  INTEGER,           INTENT(OUT) :: nvars
  INTEGER,           INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  CHARACTER(LEN=32) :: varname, stagval

  INTEGER :: varid, num3dvar
  INTEGER :: vartype, varndims, varnatts, vardimids(NF_MAX_DIMS)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  istatus = nf_inq_nvars(fin,nvars)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_ncd_vars:nf_inq_nvars',.TRUE.)

  num3dvar = 0
  DO varid = 1, nvars

    varname = '                               '

    istatus = nf_inq_var(fin,varid,varname,vartype,varndims,vardimids,varnatts)
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_ncd_vars:nf_inq_var',.TRUE.)

    IF (varndims == 4) THEN    ! only fetch 3D variables

      num3dvar = num3dvar+1
      varset(num3dvar) = varname(1:20)

      istatus = NF_GET_ATT_TEXT(fin,varid,'stagger',stagval)
      IF (istatus == NF_NOERR) THEN
        varstag(num3dvar) = stagval(1:1)
      ELSE
        WRITE(*,'(1x,2a)') 'WARNING: att "stagger" not exist for varialbe: ',varname
      END IF

      !print *,num3dvar, varset(num3dvar), ',', varstag(num3dvar)

    END IF

  END DO

  nvars = num3dvar

  RETURN
END SUBROUTINE get_ncd_3dvars

!############ Get variable list from one netcdf file #############

SUBROUTINE get_ncd_2dvars ( fin, maxnvar, varset, varstag, nvars, istatus )

  IMPLICIT NONE

  INTEGER,           INTENT(IN)  :: fin
  INTEGER,           INTENT(IN)  :: maxnvar
  CHARACTER(LEN=20), INTENT(OUT) :: varset(maxnvar)
  CHARACTER(LEN=1),  INTENT(OUT) :: varstag(maxnvar)
  INTEGER,           INTENT(OUT) :: nvars
  INTEGER,           INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  CHARACTER(LEN=32) :: varname, stagval

  INTEGER :: varid, num2dvar
  INTEGER :: vartype, varndims, varnatts, vardimids(NF_MAX_DIMS)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  istatus = nf_inq_nvars(fin,nvars)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_ncd_vars:nf_inq_nvars',.TRUE.)

  num2dvar = 0
  DO varid = 1, nvars

    varname = '                               '

    istatus = nf_inq_var(fin,varid,varname,vartype,varndims,vardimids,varnatts)
    IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_ncd_vars:nf_inq_var',.TRUE.)

    IF (varndims == 3) THEN    ! only fetch 3D variables

      num2dvar = num2dvar+1
      varset(num2dvar) = varname(1:20)

      istatus = NF_GET_ATT_TEXT(fin,varid,'stagger',stagval)
      IF (istatus == NF_NOERR) THEN
        varstag(num2dvar) = stagval(1:1)
      ELSE
        WRITE(*,'(1x,2a)') 'WARNING: att "stagger" not exist for varialbe: ',varname
      END IF

      !print *,num3dvar, varset(num3dvar), ',', varstag(num3dvar)

    END IF

  END DO

  nvars = num2dvar

  RETURN
END SUBROUTINE get_ncd_2dvars
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE get_wrf_domsize            ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_wrf_domsize(filename,timestr,nx_ext,ny_ext,nz_ext,       &
                           nzsoil_ext,nt_ext,nxlg_ext,nylg_ext,istatus)

!------------------------------------------------------------------------
!
! PURPOSE:
!   Read dimension parameters and the time level which match the time
!   string from the parameters.
!
!   Please note this subroutine will open a file to read and then close
!   it. So it does not leave any opened handler for NetCDF file.
!
!------------------------------------------------------------------------
!
  IMPLICIT NONE
  CHARACTER(LEN=*), INTENT(IN)  :: filename
  INTEGER,          INTENT(OUT) :: nx_ext
  INTEGER,          INTENT(OUT) :: ny_ext
  INTEGER,          INTENT(OUT) :: nz_ext, nzsoil_ext
  INTEGER,          INTENT(INOUT) :: nt_ext
  INTEGER,          INTENT(OUT) :: nxlg_ext,nylg_ext
  CHARACTER(LEN=*), INTENT(IN)  :: timestr

  INTEGER,          INTENT(OUT) :: istatus

!------------------------------------------------------------------------
!
!  Misc. Local variables
!
!------------------------------------------------------------------------

  INTEGER :: ncid
  LOGICAL :: fexists
  INTEGER :: dimid,varid
  INTEGER :: dateStrLen, strlen, ntime

  CHARACTER(LEN=19) :: temstr

  INCLUDE 'netcdf.inc'

  INTEGER :: i

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Begining of executable code
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  INQUIRE(FILE = filename, EXIST = fexists)
  IF (fexists) THEN
    istatus = NF_OPEN(TRIM(filename),NF_NOWRITE,ncid)
    CALL handle_ncd_error(istatus,'NF_OPEN in get_wrf_domsize',.TRUE.)
  ELSE
    WRITE(6,'(2a)') 'File not found: ', filename
    STOP 'open_wrfd_file'
  ENDIF

!-----------------------------------------------------------------------
!
! Get WRF dimensions
!
!-----------------------------------------------------------------------

  istatus = NF_INQ_DIMID(ncid,'west_east_stag',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_wrf_domsize',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,nx_ext)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_domsize',.FALSE.)

  istatus = NF_INQ_DIMID(ncid,'south_north_stag',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_wrf_domsize',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,ny_ext)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_domsize',.FALSE.)

  istatus = NF_INQ_DIMID(ncid,'bottom_top_stag',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_wrf_domsize',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,nz_ext)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_domsize',.FALSE.)

  istatus = NF_INQ_DIMID(ncid,'soil_layers_stag',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_wrf_domsize',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,nzsoil_ext)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_domsize',.FALSE.)

  istatus = NF_INQ_DIMID(ncid,'DateStrLen',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_wrf_domsize',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,dateStrLen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_domsize',.FALSE.)

  istatus = NF_INQ_DIMID(ncid,'Time',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_wrf_domsize',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,ntime)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_domsize',.FALSE.)

!-----------------------------------------------------------------------
!
! Get WRF global dimension size
!
!-----------------------------------------------------------------------

  istatus = NF_GET_ATT_INT(ncid,NF_GLOBAL,'WEST-EAST_GRID_DIMENSION',  nxlg_ext)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_domsize:NF_GET_ATT_INT(WEST-EAST_GRID_DIMENSION)',.FALSE.)

  istatus = NF_GET_ATT_INT(ncid,NF_GLOBAL,'SOUTH-NORTH_GRID_DIMENSION',nylg_ext)
  IF (istatus /= NF_NOERR) CALL handle_ncd_error(istatus,'get_wrf_domsize:NF_GET_ATT_INT(SOUTH-NORTH_GRID_DIMENSION)',.FALSE.)

!-----------------------------------------------------------------------
!
! Get WRF date string
!
!-----------------------------------------------------------------------

  IF (nt_ext /= -99) THEN

    strlen = LEN(timestr)
    IF(strlen < dateStrLen) THEN
      WRITE(6,'(a)') ' Date string is not long enough to hold Times.'
      STOP 'timestr_too_short'
    END IF

    DO i = 1, ntime
      istatus = NF_INQ_VARID(ncid, 'Times', varid)
      CALL handle_ncd_error(istatus,'NF_INQ_VARID in get_wrf_domsize',.FALSE.)
      istatus = NF_GET_VARA_TEXT(ncid,varid,(/1,1/),(/dateStrLen,1/),temstr)
      CALL handle_ncd_error(istatus,'NF_GET_VAR_TEXT in get_wrf_domsize',.FALSE.)
      IF(temstr(1:dateStrlen) .EQ. timestr(1:dateStrlen)) THEN
        nt_ext = i
        EXIT
      END IF
    END DO

    IF(i > ntime) THEN
      WRITE(6,*) 'Time string: ',timestr,' not found in the data set - ',filename
      STOP
    END IF

  !ELSE   ! do not care about time string
  END IF

  istatus = NF_CLOSE(ncid)
  CALL handle_ncd_error(istatus,'NF_CLOSE in get_wrf_domsize',.FALSE.)

  RETURN

END SUBROUTINE get_wrf_domsize

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE get_wrf_grid               ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_wrf_grid(filename,mapproj,sclfct,trlat1,trlat2,trlon,    &
                       ctrlat,ctrlon,dx,dy,mphyopt,istatus)
!-----------------------------------------------------------------------
!
! PURPOSE
!
!   Retieve WRF grib information from the NetCDF file which are stored
!   as Global attributes.
!
!-----------------------------------------------------------------------

  USE model_precision

  IMPLICIT NONE

  CHARACTER(LEN=*), INTENT(IN)  :: filename
  INTEGER, INTENT(OUT) :: mapproj
  REAL(P), INTENT(OUT) :: sclfct
  REAL(P), INTENT(OUT) :: trlat1
  REAL(P), INTENT(OUT) :: trlat2
  REAL(P), INTENT(OUT) :: trlon
  REAL(P), INTENT(OUT) :: ctrlat
  REAL(P), INTENT(OUT) :: ctrlon
  REAL(P), INTENT(OUT) :: dx
  REAL(P), INTENT(OUT) :: dy
  INTEGER, INTENT(OUT) :: mphyopt

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER  :: ncid

  REAL(SP) :: trlat1in
  REAL(SP) :: trlat2in
  REAL(SP) :: trlonin
  REAL(SP) :: ctrlatin
  REAL(SP) :: ctrlonin
  REAL(SP) :: dxin
  REAL(SP) :: dyin

  LOGICAL  :: fexists

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  INQUIRE(FILE = filename, EXIST = fexists)
  IF (fexists) THEN
    istatus = NF_OPEN(TRIM(filename),NF_NOWRITE,ncid)
    CALL handle_ncd_error(istatus,'NF_OPEN in get_wrf_dimension',.FALSE.)
  ELSE
    WRITE(6,'(2a)') 'File not found: ', filename
    istatus = -1
    RETURN
  ENDIF

  ! Get Map projection attributes from NetCDF file
  istatus = NF_GET_ATT_INT(ncid,NF_GLOBAL,'MAP_PROJ',mapproj)
  CALL handle_ncd_error(istatus,'NF_GET_ATT_INT in get_wrf_grid',.FALSE.)
  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'TRUELAT1',trlat1in)
  CALL handle_ncd_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid',.FALSE.)
  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'TRUELAT2',trlat2in)
  CALL handle_ncd_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid',.FALSE.)
  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'STAND_LON',trlonin)  ! ????
  CALL handle_ncd_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid',.FALSE.)

  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'CEN_LAT',ctrlatin)
  CALL handle_ncd_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid',.FALSE.)
  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'CEN_LON',ctrlonin)
  CALL handle_ncd_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid',.FALSE.)

  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'DX',dxin)
  CALL handle_ncd_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid',.FALSE.)
  istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'DY',dyin)
  CALL handle_ncd_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid',.FALSE.)

  istatus = NF_GET_ATT_INT(ncid,NF_GLOBAL,'MP_PHYSICS',mphyopt)
  CALL handle_ncd_error(istatus,'NF_GET_ATT_REAL in get_wrf_grid',.FALSE.)

  istatus = NF_CLOSE(ncid)
  CALL handle_ncd_error(istatus,'NF_CLOSE in get_wrf_dimension',.FALSE.)

  sclfct = 1.0

  trlat1 = trlat1in
  trlat2 = trlat2in
  trlon  = trlonin
  ctrlat = ctrlatin
  ctrlon = ctrlonin
  dx     = dxin
  dy     = dyin

  RETURN
END SUBROUTINE get_wrf_grid

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE get_wrf_domtime            ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_wrf_domtime(filename,nt,timestring,istatus)
!-----------------------------------------------------------------------
!
! PURPOSE
!
!   Retieve WRF time string from the NetCDF file .
!
!-----------------------------------------------------------------------
  IMPLICIT NONE

  CHARACTER(LEN=*),  INTENT(IN)  :: filename
  INTEGER,           INTENT(IN)  :: nt
  CHARACTER(LEN=19), INTENT(OUT) :: timestring

  INTEGER,           INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER  :: ncid, dimid, varid

  INTEGER  :: dateStrLen

  LOGICAL  :: fexists

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  INQUIRE(FILE = filename, EXIST = fexists)
  IF (fexists) THEN
    istatus = NF_OPEN(TRIM(filename),NF_NOWRITE,ncid)
    CALL handle_ncd_error(istatus,'NF_OPEN in get_wrf_dimension',.FALSE.)
  ELSE
    WRITE(6,'(2a)') 'File not found: ', filename
    istatus = -1
    RETURN
  ENDIF

  istatus = NF_INQ_DIMID(ncid,'DateStrLen',dimid)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMID in get_wrf_dimension',.FALSE.)
  istatus = NF_INQ_DIMLEN(ncid,dimid,dateStrLen)
  CALL handle_ncd_error(istatus,'NF_INQ_DIMLEN in get_wrf_dimension',.FALSE.)

  istatus = NF_INQ_VARID(ncid, 'Times', varid)
  CALL handle_ncd_error(istatus,'NF_INQ_VARID in get_wrf_dimension',.FALSE.)
  istatus = NF_GET_VARA_TEXT(ncid,varid,(/1,nt/),(/dateStrLen,1/),timestring)
  CALL handle_ncd_error(istatus,'NF_GET_VAR_TEXT in get_wrf_dimension',.FALSE.)

  istatus = NF_CLOSE(ncid)
  CALL handle_ncd_error(istatus,'NF_CLOSE in get_wrf_dimension',.FALSE.)

  RETURN
END SUBROUTINE get_wrf_domtime

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE get_wrf_domscalar          ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_wrf_domscalar(filename,nscalar,nqscalar,qnames,          &
                           P_QC,P_QR,P_QI,P_QS,P_QG,P_QH,               &
                           P_NC,P_NR,P_NI,P_NS,P_NG,P_NH,               &
                           P_ZR,P_ZI,P_ZS,P_ZG,P_ZH,                    &
                           istatus)
!-----------------------------------------------------------------------
!
! PURPOSE
!
!   Retieve WRF hydrometeor scalar dimensions from the NetCDF file .
!
!-----------------------------------------------------------------------
  IMPLICIT NONE

  CHARACTER(LEN=*),  INTENT(IN)  :: filename

  INTEGER,           INTENT(OUT) :: nscalar, nqscalar
  INTEGER,           INTENT(OUT) :: P_QC,P_QR,P_QI,P_QS,P_QG,P_QH,      &
                                    P_NC,P_NR,P_NI,P_NS,P_NG,P_NH,      &
                                    P_ZR,P_ZI,P_ZS,P_ZG,P_ZH
  CHARACTER(LEN=40), INTENT(OUT) :: qnames(30)

  INTEGER,           INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'netcdf.inc'

  INTEGER  :: ncid

  LOGICAL  :: fexists

  INTEGER, PARAMETER :: ntotal = 12
  INTEGER, PARAMETER :: I_QC = 1, I_QR = 2,  I_QI = 3,  I_QS = 4,  I_QG = 5,  I_QH = 6,  &
                        I_NC = 7, I_NR = 8,  I_NI = 9,  I_NS = 10, I_NG = 11, I_NH = 12, &
                                  I_ZR = 13, I_ZI = 14, I_ZS = 15, I_ZG = 16, I_ZH = 17
                ! denotes the order of names in qnames_wrf

  CHARACTER(LEN=40), PARAMETER :: qnames_wrf(ntotal) = (/ 'QCLOUD    ', &
                'QRAIN     ', 'QICE      ', 'QSNOW     ', 'QGRAUP    ', &
                'QHAIL     ', 'QNCLOUD   ', 'QNRAIN    ', 'QNICE     ', &
                'QNSNOW    ', 'QNGRAUPEL ', 'QNHAIL    ' /)

  INTEGER :: P_ARR(20)   ! remember the order of qnames_wrf and map to the variable P_??.

  INTEGER :: varid
  INTEGER :: nq

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  INQUIRE(FILE = filename, EXIST = fexists)
  IF (fexists) THEN
    istatus = NF_OPEN(TRIM(filename),NF_NOWRITE,ncid)
    CALL handle_ncd_error(istatus,'NF_OPEN in get_wrf_dimension',.FALSE.)
  ELSE
    WRITE(6,'(2a)') 'File not found: ', filename
    istatus = -1
    RETURN
  ENDIF

  nscalar  = 0
  nqscalar = 0
  P_ARR(:) = 0
  DO nq = 1, ntotal
    istatus = NF_INQ_VARID(ncid,qnames_wrf(nq),varid)
    IF (istatus == NF_NOERR) THEN
      nscalar = nscalar + 1
      IF (nq < 7) nqscalar = nqscalar + 1
      P_ARR(nq) = nscalar
      qnames(nscalar) = qnames_wrf(nq)
    END IF
  END DO

  P_QC = P_ARR(I_QC);  P_NC = P_ARR(I_NC)
  P_QR = P_ARR(I_QR);  P_NR = P_ARR(I_NR);  P_ZR = P_ARR(I_ZR)
  P_QI = P_ARR(I_QI);  P_NI = P_ARR(I_NI);  P_ZI = P_ARR(I_ZI)
  P_QS = P_ARR(I_QS);  P_NS = P_ARR(I_NS);  P_ZS = P_ARR(I_ZS)
  P_QG = P_ARR(I_QG);  P_NG = P_ARR(I_NG);  P_ZG = P_ARR(I_ZG)
  P_QH = P_ARR(I_QH);  P_NH = P_ARR(I_NH);  P_ZH = P_ARR(I_ZH)

  IF (P_NC < 1) THEN   ! If QNC not present, check QNDROP
    istatus = NF_INQ_VARID(ncid,'QNDROP',varid)
    IF (istatus == NF_NOERR) THEN
      nscalar = nscalar + 1
      qnames(nscalar) = 'QNDROP'
      P_NC = nscalar
    END IF
  END IF

  istatus = NF_CLOSE(ncid)
  CALL handle_ncd_error(istatus,'NF_CLOSE in get_wrf_dimension',.FALSE.)

  RETURN
END SUBROUTINE get_wrf_domscalar

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%
!%%%% Generalized support procedures
!%%%%
!%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE handle_ncd_error           ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE handle_ncd_error(ierr,sub_name,progstop)

!-----------------------------------------------------------------------
!
! PURPOSE:
!   Write error message to the standard output if ierr contains an error
!
!-----------------------------------------------------------------------

  IMPLICIT NONE
  INTEGER,          INTENT(IN) :: ierr
  CHARACTER(LEN=*), INTENT(IN) :: sub_name
  LOGICAL,          INTENT(IN) :: progstop
!-----------------------------------------------------------------------

  CHARACTER(LEN=80) :: errmsg

  INCLUDE 'netcdf.inc'
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF(ierr /= NF_NOERR) THEN
    errmsg = NF_STRERROR(ierr)
    WRITE(6,'(1x,2a)') 'NetCDF error: ',errmsg
    WRITE(6,'(1x,3a)') 'ERROR while calling <', sub_name,'>.'
    IF (progstop) CALL arpsSTOP(1,'Aborted due to NetCDF ERROR.')
  END IF

  RETURN
END SUBROUTINE handle_ncd_error

LOGICAL FUNCTION name_in_set(vname,chset,nset)

  IMPLICIT NONE
  CHARACTER(LEN=*), INTENT(IN)  :: vname
  INTEGER,          INTENT(IN)  :: nset
  CHARACTER(LEN=*), INTENT(IN)  :: chset(nset)

!-----------------------------------------------------------------------

  INTEGER :: i
  CHARACTER(LEN=40) :: varname, elename
  CHARACTER(LEN=40) :: upvarnm, upelenm

!-----------------------------------------------------------------------

  CHARACTER(LEN=40) :: upcase

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  name_in_set = .FALSE.

  DO i = 1, nset

    WRITE(varname,'(a)')  TRIM(vname)
    WRITE(elename,'(a)')  TRIM(chset(i))

    upvarnm = upcase(varname)
    upelenm = upcase(elename)

    IF (upvarnm == upelenm ) THEN
      name_in_set = .TRUE.
      EXIT
    END IF

  END DO

  RETURN
END FUNCTION name_in_set
