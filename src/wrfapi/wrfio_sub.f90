!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE open_wrf_file              ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE open_wrf_files(filename,io_form,patch_split,multifile,file_mode, &
                         ncmprx,ncmpry,numdigits,nidout)
!
!------------------------------------------------------------------
!
!  PURPOSE:
!
!    Open a WRF file and return NetCDF file handler.
!    It will call open_wrf_one_file or open_wrf_multi_files depends
!    on the pass-in parameters
!
!    NOTE: it is required to call close_wrf_file explicitly to close
!          the opened file in your calling program.
!
!------------------------------------------------------------------

  IMPLICIT NONE
  CHARACTER(LEN=*), INTENT(IN)  :: filename
  INTEGER,          INTENT(IN)  :: io_form
  LOGICAL,          INTENT(IN)  :: patch_split, multifile
  INTEGER,          INTENT(IN)  :: file_mode
  INTEGER,          INTENT(IN)  :: ncmprx, ncmpry,numdigits
  INTEGER,          INTENT(OUT) :: nidout(ncmprx,ncmpry)

!------------------------------------------------------------------
!
!  Misc. local variable
!
!------------------------------------------------------------------
  INTEGER            :: istatus
  !INCLUDE 'mp.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Begining of executable code
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  IF (patch_split) THEN
    !IF (myproc == 0) THEN
    CALL open_wrf_one_file(filename,io_form,multifile,file_mode,        &
                           ncmprx,ncmpry,numdigits,nidout(1,1),istatus)
    !END IF
    !CALL mpupdatei(istatus,1)
  ELSE
    CALL open_wrf_multi_files(filename,io_form,multifile,file_mode,     &
                              ncmprx,ncmpry,numdigits,nidout,istatus)
  END IF

  IF (istatus /= 0) THEN
    WRITE(0,'(1x,2a)') 'ERROR: Opening file ',filename
    CALL arpsstop('Open WRF file error.',1)
  END IF

  RETURN
END SUBROUTINE open_wrf_files
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE close_wrf_file               ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE close_wrf_files(nch,io_form,patch_split,file_mode,           &
                          ncompressx,ncompressy)
!
!------------------------------------------------------------------
!
!  PURPOSE:
!
!     Close the WRF file which is opened using open_wrf_file.
!
!------------------------------------------------------------------
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: io_form
  LOGICAL, INTENT(IN) :: patch_split
  INTEGER, INTENT(IN) :: file_mode
  INTEGER, INTENT(IN) :: ncompressx, ncompressy
  INTEGER, INTENT(IN) :: nch(ncompressx,ncompressy)

!------------------------------------------------------------------
!
!  Misc. local variable
!
!------------------------------------------------------------------
!
  INTEGER :: istatus

  INCLUDE 'mp.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  istatus = 0

  IF(patch_split) THEN
    CALL close_wrf_one_file(nch(1,1),io_form,istatus)
  ELSE
    CALL close_wrf_multi_files(nch,io_form,file_mode,                   &
                               ncompressx,ncompressy,istatus)
  END IF

  IF (istatus /= 0) THEN
    WRITE(0,'(1x,2a)') 'ERROR: closing file handler ',nch
    CALL mpexit(1)
  END IF

  RETURN
END SUBROUTINE close_wrf_files
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE get_wrf_att                  ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_wrf_att(nfid,io_form,multifile,patch_split,              &
                      ncompressx,ncompressy,                            &
                      mapproj,sclfct,trlat1,trlat2,trlon,               &
                      ctrlat,ctrlon,dx,dy,istatus)

!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Read in a scalar variable from the WRF files. Call directly the
!    netCDF IO API for WRF.
!
!-----------------------------------------------------------------------
  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: ncompressx,ncompressy
  INTEGER,          INTENT(IN)  :: nfid(ncompressx,ncompressy)
  INTEGER,          INTENT(IN)  :: io_form
  LOGICAL,          INTENT(IN)  :: multifile, patch_split
  INTEGER, INTENT(OUT) :: mapproj
  REAL,    INTENT(OUT) :: sclfct
  REAL,    INTENT(OUT) :: trlat1
  REAL,    INTENT(OUT) :: trlat2
  REAL,    INTENT(OUT) :: trlon
  REAL,    INTENT(OUT) :: ctrlat
  REAL,    INTENT(OUT) :: ctrlon
  REAL,    INTENT(OUT) :: dx
  REAL,    INTENT(OUT) :: dy
  INTEGER,          INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INTEGER :: ncid

  INCLUDE 'mp.inc'
  INCLUDE 'netcdf.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  IF ( myproc == 0 ) THEN
    ncid = nfid(1,1)

    WRITE(6,FMT='(2a)',ADVANCE='NO') '  Reading WRF attributes '

    IF (io_form == 7) THEN
      ! Get Map projection attributes from NetCDF file
      istatus = NF_GET_ATT_INT(ncid,NF_GLOBAL,'MAP_PROJ',mapproj)
      CALL handle_ncd_error(istatus,'MAP_PROJ in NF_GET_ATT_INT',.TRUE.)
      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'TRUELAT1',trlat1)
      CALL handle_ncd_error(istatus,'TRUELAT1 in NF_GET_ATT_REAL',.TRUE.)
      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'TRUELAT2',trlat2)
      CALL handle_ncd_error(istatus,'TRUELAT2 in NF_GET_ATT_REAL',.TRUE.)
      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'STAND_LON',trlon)
      CALL handle_ncd_error(istatus,'STAND_LON in NF_GET_ATT_REAL',.TRUE.)

      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'CEN_LAT',ctrlat)
      CALL handle_ncd_error(istatus,'CEN_LAT in NF_GET_ATT_REAL',.TRUE.)
      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'CEN_LON',ctrlon)
      CALL handle_ncd_error(istatus,'CEN_LON in NF_GET_ATT_REAL',.TRUE.)

      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'DX',dx)
      CALL handle_ncd_error(istatus,'DX in NF_GET_ATT_REAL',.TRUE.)
      istatus = NF_GET_ATT_REAL(ncid,NF_GLOBAL,'DY',dy)
      CALL handle_ncd_error(istatus,'DY in NF_GET_ATT_REAL',.TRUE.)

    END IF

    IF ( istatus == 0 ) THEN
      WRITE(6,'(a)') '         ...  DONE.'
    ELSE
      WRITE(6,'(a)') '         ...  ERROR.'
    END IF
  END IF

  sclfct = 1.0
  CALL mpupdatei(mapproj,1)
  CALL mpupdater(trlat1,1)
  CALL mpupdater(trlat2,1)
  CALL mpupdater(trlon,1)
  CALL mpupdater(ctrlat,1)
  CALL mpupdater(ctrlon,1)
  CALL mpupdater(dx,1)
  CALL mpupdater(dy,1)

  RETURN
END SUBROUTINE get_wrf_att
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE get_wrf_s                    ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_wrf_s(nfid,io_form,multifile,patch_split,                &
                      ncompressx,ncompressy,                            &
                      datestr,itime,varname,var,istatus)

!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Read in a scalar variable from the WRF files. Call directly the
!    netCDF IO API for WRF.
!
!-----------------------------------------------------------------------

  USE model_precision

  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: ncompressx,ncompressy
  INTEGER,          INTENT(IN)  :: nfid(ncompressx,ncompressy)
  INTEGER,          INTENT(IN)  :: io_form
  LOGICAL,          INTENT(IN)  :: multifile, patch_split
  CHARACTER(LEN=*), INTENT(IN)  :: datestr
  INTEGER,          INTENT(IN)  :: itime
  CHARACTER(LEN=*), INTENT(IN)  :: varname
  REAL(P),          INTENT(OUT) :: var
  INTEGER,          INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INCLUDE 'mp.inc'

  REAL(SP) :: varin

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  IF ( myproc == 0 )   &
    WRITE(6,FMT='(2x,a,5x,a10)',ADVANCE='NO') 'Reading scalar ', varname

  IF (io_form == 7) THEN
    !IF (myproc == 0) THEN
    CALL get_ncd_scalar(nfid(1,1),itime,varname,varin,istatus)
    !END IF
    !CALL mpupdater(varin,1)
    var = varin
  END IF

  IF ( myproc == 0 ) THEN
    IF ( istatus == 0 ) THEN
      WRITE(6,'(a)') '         ...  DONE.'
    ELSE
      WRITE(6,'(a)') '         ...  ERROR.'
    END IF
  END IF

  RETURN
END SUBROUTINE get_wrf_s
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE get_wrf_1d                   ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_wrf_1d(nfid,io_form,multifile,patch_split,               &
                      ncompressx,ncompressy,                            &
                      datestr,itime,varname,nz,var1d,nzd,istatus)

!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Read in a 1D array from the WRF files.Call directly the
!    netCDF IO API for WRF due it does not care about spliting or
!    joining of the data array.
!
!-----------------------------------------------------------------------

  USE model_precision

  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: ncompressx,ncompressy
  INTEGER,          INTENT(IN)  :: nfid(ncompressx,ncompressy)
  INTEGER,          INTENT(IN)  :: io_form
  LOGICAL,          INTENT(IN)  :: multifile, patch_split
  CHARACTER(LEN=*), INTENT(IN)  :: datestr
  INTEGER,          INTENT(IN)  :: itime
  CHARACTER(LEN=*), INTENT(IN)  :: varname
  INTEGER,          INTENT(IN)  :: nz
  REAL(SP),         INTENT(OUT) :: var1d(nz)
  INTEGER,          INTENT(IN)  :: nzd
  INTEGER,          INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INCLUDE 'mp.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  IF ( myproc == 0 )   &
    WRITE(6,FMT='(2x,a,a10)',ADVANCE='NO') 'Reading 1D variable ', varname

  IF (io_form == 7) THEN                       ! NetCDF format
    !IF (myproc == 0) THEN
    CALL get_ncd_1d(nfid(1,1),itime,varname,nzd,var1d,istatus)
    !END IF
    !CALL mpbcastra(var1d,nzd,0)
  END IF

  IF ( myproc == 0 ) THEN
    IF ( istatus == 0 ) THEN
      WRITE(6,'(a)') '         ...  DONE.'
    ELSE
      WRITE(6,'(a)') '         ...  ERROR.'
    END IF
  END IF

  RETURN
END SUBROUTINE get_wrf_1d
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE get_wrf_2d                   ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_wrf_2d(nfid,io_form,multifile,patch_split,               &
                      ncompressx,ncompressy,                            &
                      datestr,itime,varname,stagger,                    &
                      nx,ny,var2d,nxd,nyd,temtd,istatus)

!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Read in a 2D array from the WRF files
!
!-----------------------------------------------------------------------
  USE model_precision

  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: ncompressx,ncompressy
  INTEGER,          INTENT(IN)  :: nfid(ncompressx,ncompressy)
  INTEGER,          INTENT(IN)  :: io_form
  LOGICAL,          INTENT(IN)  :: multifile, patch_split
  CHARACTER(LEN=*), INTENT(IN)  :: datestr
  INTEGER,          INTENT(IN)  :: itime
  CHARACTER(LEN=*), INTENT(IN)  :: varname
  CHARACTER(LEN=*), INTENT(IN)  :: stagger
  INTEGER,          INTENT(IN)  :: nx, ny              ! local index
  REAL(P),          INTENT(OUT) :: var2d(nx,ny)
  INTEGER,          INTENT(IN)  :: nxd,nyd             ! Data index
  REAL(SP),         INTENT(OUT) :: temtd(nxd*nyd)      ! domain array
  INTEGER,          INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INTEGER :: i,j

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  IF (patch_split) THEN  ! handle situations 2, 6
    CALL get_split_wrf_2d(nfid(1,1),io_form,ncompressx,ncompressy,      &
                          datestr,itime,varname,stagger,                &
                          nx,ny,var2d,                                  &
                          nxd,nyd,temtd,istatus)
  ELSE                   ! handle situations 1, 3, 4, 5
    CALL get_join_wrf_2d(nfid,io_form,ncompressx,ncompressy,            &
                        datestr,itime,varname,stagger,                  &
                        nx,ny,var2d,                                    &
                        nxd,nyd,temtd,istatus)
  END IF

  ! Assumed nxd*nyd > ny (and nx), so temtd is used as a temporary array again below
  !IF (stagger /= 'X') THEN       ! set nx column for X staggered variable
  !  DO j = 1, ny
  !      var2d(nx,j) = var2d(nx-1,j)
  !  END DO
  !
  !  CALL wrf_mpsendrecv1de(var2d,nx,ny,temtd)
  !END IF
  !
  !IF (stagger /= 'Y') THEN      ! set ny row for Y staggered variable
  !  DO i = 1, nx
  !    var2d(i,ny) = var2d(i,ny-1)
  !  END DO
  !
  !  CALL wrf_mpsendrecv1dn(var2d,nx,ny,temtd)
  !END IF

  RETURN
END SUBROUTINE get_wrf_2d
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE get_wrf_3d                   ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_wrf_3d(nfid,io_form,multifile,patch_split,               &
                      ncompressx,ncompressy,                            &
                      datestr,itime,varname,stagger,                    &
                      nx,ny,nz,var3d,                                   &
                      nxd,nyd,nzd,temtd,istatus)

!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Read in a 3D array from the WRF files
!
!-----------------------------------------------------------------------
  USE model_precision

  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: ncompressx,ncompressy
  INTEGER,          INTENT(IN)  :: nfid(ncompressx,ncompressy)
  INTEGER,          INTENT(IN)  :: io_form
  LOGICAL,          INTENT(IN)  :: multifile, patch_split
  CHARACTER(LEN=*), INTENT(IN)  :: datestr
  INTEGER,          INTENT(IN)  :: itime
  CHARACTER(LEN=*), INTENT(IN)  :: varname
  CHARACTER(LEN=*), INTENT(IN)  :: stagger
  INTEGER,          INTENT(IN)  :: nx, ny, nz              ! local index
  REAL(P),          INTENT(OUT) :: var3d(nx,ny,nz)
  INTEGER,          INTENT(IN)  :: nxd,nyd,nzd             ! Data index
  REAL(SP),         INTENT(OUT) :: temtd(nxd*nyd*nzd)      ! domain array
  INTEGER,          INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INTEGER :: i,j,k
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


  IF (patch_split) THEN  ! handle situations 2, 6
    CALL get_split_wrf_3d(nfid(1,1),io_form,ncompressx,ncompressy,      &
                          datestr,itime,varname,stagger,                &
                          nx,ny,nz,var3d,                               &
                          nxd,nyd,nzd,temtd,istatus)
  ELSE                   ! handle situations 1, 3, 4, 5
    CALL get_join_wrf_3d(nfid,io_form,ncompressx,ncompressy,            &
                        datestr,itime,varname,stagger,                  &
                        nx,ny,nz,var3d,                                 &
                        nxd,nyd,nzd,temtd,istatus)
  END IF

  !! Assumed nxd*nyd > ny (and nx), so temtd is used as a temporary array again below
  !IF (stagger == 'X') THEN       ! set nx column for X staggered variable
  !  CALL wrf_mpsendrecv2de(var3d,nx,ny,nz,temtd)
  !ELSE
  !IF (stagger /= 'X') then
  !  DO k = 1, nz
  !    DO j = 1, ny
  !      var3d(nx,j,k) = var3d(nx-1,j,k)
  !    END DO
  !  END DO
  !
  !  CALL wrf_mpsendrecv2de(var3d,nx,ny,nz,temtd)
  !END IF
  !
  !IF (stagger /= 'Y') THEN      ! set ny row for Y staggered variable
  !  DO k = 1, nz
  !    DO i = 1, nx
  !      var3d(i,ny,k) = var3d(i,ny-1,k)
  !    END DO
  !  END DO
  !
  !  CALL wrf_mpsendrecv2dn(var3d,nx,ny,nz,temtd)
  !END IF

  RETURN
END SUBROUTINE get_wrf_3d

!#######################################################################

SUBROUTINE array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,stagger,var_wrf,         &
                          nx_arps,ny_arps,nz_arps,var_arps,tem1,istatus)

!-----------------------------------------------------------------------
!
! Copy WRF data to ARPS array and fill the boudary zone of the array
!
!-----------------------------------------------------------------------

  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: nx_wrf,ny_wrf,nz_wrf
  INTEGER, INTENT(IN)  :: nx_arps,ny_arps,nz_arps
  REAL,    INTENT(IN)  :: var_wrf(nx_wrf,ny_wrf,nz_wrf)
  REAL,    INTENT(OUT) :: var_arps(nx_arps,ny_arps,nz_arps)

  CHARACTER(*), INTENT(IN) :: stagger

  REAL,    INTENT(OUT) :: tem1(nx_arps,nz_arps)

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INTEGER :: i,j,k, ii, jj, kk
  INTEGER :: nxd, nyd, flag_stagger
  INTEGER :: ips, ipe, jps, jpe, kps, kpe

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  nxd = nx_wrf - 1
  nyd = ny_wrf - 1

  IF (stagger == 'X') THEN
    nxd = nx_wrf
  ELSE IF (stagger == 'Y') THEN
    nyd = ny_wrf
  END IF

  DO k = 1,nz_wrf
    kk = k+1
    DO j = 1, nyd
      jj = j + 1
      DO i = 1, nxd
        ii = i + 1
        var_arps (ii,jj,kk) = var_wrf(i,j,k)
      END DO
    END DO
  END DO

  ips = 2; ipe = nx_arps-2
  jps = 2; jpe = ny_arps-2
  kps = 2; kpe = nz_arps-2

  flag_stagger = 0
  IF (stagger == 'X') THEN
    flag_stagger = 1
    ipe = ipe + 1
  ELSE IF (stagger == 'Y') THEN
    flag_stagger = 2
    jpe = jpe + 1
  ELSE IF (stagger == 'Z') THEN
    kpe = kpe + 1
  ELSE IF (stagger == 'V') THEN  ! Vertical extension

    DO j = 1, nyd
      jj = j + 1
      DO i = 1, nxd
        ii = i + 1
        var_arps(ii,jj,1)       = 2*var_arps(ii,jj,2)-var_arps(ii,jj,3)
        var_arps(ii,jj,nz_arps) = 2*var_arps(ii,jj,nz_arps-1)-var_arps(ii,jj,nz_arps-2)
      END DO
    END DO

    kps = 1
    kpe = nz_arps
  END IF

  CALL edgfill(var_arps,1,nx_arps,ips,ipe, 1,ny_arps,jps,jpe, 1,nz_arps,kps,kpe)

  CALL mpsendrecv2dew(var_arps,nx_arps,ny_arps,nz_arps,0,0,flag_stagger,tem1)
  CALL mpsendrecv2dns(var_arps,nx_arps,ny_arps,nz_arps,0,0,flag_stagger,tem1)

  !IF (loc_x > 1)       ips = ips - 1
  !IF (loc_x < nproc_x) ipe = ipe + 1
  !IF (loc_y > 1)       jps = jps - 1
  !IF (loc_y < nproc_y) jpe = jpe + 1

  RETURN
END SUBROUTINE array_wrf2arps

!#######################################################################

SUBROUTINE array_wrf2arps_2d(nx_wrf,ny_wrf,stagger,var_wrf,             &
                             nx_arps,ny_arps,var_arps,tem1,istatus)

!-----------------------------------------------------------------------
!
! Copy WRF data to ARPS array and fill the boudary zone of the array
!
!-----------------------------------------------------------------------

  IMPLICIT NONE
  INTEGER, INTENT(IN) :: nx_wrf,ny_wrf
  INTEGER, INTENT(IN) :: nx_arps,ny_arps
  CHARACTER(*), INTENT(IN) :: stagger
  REAL,    INTENT(IN)  :: var_wrf(nx_wrf,ny_wrf)
  REAL,    INTENT(OUT) :: var_arps(nx_arps,ny_arps)

  REAL,    INTENT(OUT) :: tem1(nx_arps)

  INTEGER, INTENT(OUT) :: istatus
!-----------------------------------------------------------------------

  INTEGER :: i,j, ii, jj
  INTEGER :: nxd, nyd, flag_stagger
  INTEGER :: ips, ipe, jps, jpe

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  nxd = nx_wrf - 1
  nyd = ny_wrf - 1

  IF (stagger == 'X') THEN
    nxd = nx_wrf
  ELSE IF (stagger == 'Y') THEN
    nyd = ny_wrf
  END IF

  DO j = 1, nyd
    jj = j + 1
    DO i = 1, nxd
      ii = i + 1
      var_arps (ii,jj) = var_wrf(i,j)
    END DO
  END DO

  ips = 2; ipe = nx_arps-2
  jps = 2; jpe = ny_arps-2

  flag_stagger = 0
  IF (stagger == 'X') THEN
    flag_stagger = 1
    ipe = ipe + 1
  ELSE IF (stagger == 'Y') THEN
    flag_stagger = 2
    jpe = jpe + 1
  END IF

  CALL edgfill(var_arps,1,nx_arps,ips,ipe, 1,ny_arps,jps,jpe, 1,1,1,1)

  CALL mpsendrecv1dew(var_arps,nx_arps,ny_arps,0,0,flag_stagger,tem1)
  CALL mpsendrecv1dns(var_arps,nx_arps,ny_arps,0,0,flag_stagger,tem1)

  !IF (loc_x > 1)       ips = ips - 1
  !IF (loc_x < nproc_x) ipe = ipe + 1
  !IF (loc_y > 1)       jps = jps - 1
  !IF (loc_y < nproc_y) jpe = jpe + 1

  RETURN
END SUBROUTINE array_wrf2arps_2d


!#######################################################################

SUBROUTINE array_arps2wrf(nx_arps,ny_arps,nz_arps,var_arps,             &
                          nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)

!-----------------------------------------------------------------------
!
! Extract WRF data from ARPS array
!
!-----------------------------------------------------------------------

  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: nx_wrf,ny_wrf,nz_wrf
  INTEGER, INTENT(IN)  :: nx_arps,ny_arps,nz_arps
  REAL,    INTENT(OUT) :: var_wrf(nx_wrf,ny_wrf,nz_wrf)
  REAL,    INTENT(IN)  :: var_arps(nx_arps,ny_arps,nz_arps)

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INTEGER :: i,j,k, ii, jj, kk

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  DO k = 1,nz_wrf
    kk = k+1
    DO j = 1, ny_wrf
      jj = j + 1
      DO i = 1, nx_wrf
        ii = i + 1
        var_wrf(i,j,k) = var_arps (ii,jj,kk)
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE array_arps2wrf

!#######################################################################

SUBROUTINE array_arps2wrf_2d(nx_arps,ny_arps,var_arps,                  &
                             nx_wrf,ny_wrf,var_wrf,istatus)

!-----------------------------------------------------------------------
!
! Extract WRF data from ARPS array
!
!-----------------------------------------------------------------------

  IMPLICIT NONE
  INTEGER, INTENT(IN)  :: nx_wrf,ny_wrf
  INTEGER, INTENT(IN)  :: nx_arps,ny_arps
  REAL,    INTENT(OUT) :: var_wrf(nx_wrf,ny_wrf)
  REAL,    INTENT(IN)  :: var_arps(nx_arps,ny_arps)

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INTEGER :: i,j, ii, jj

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  DO j = 1, ny_wrf
    jj = j + 1
    DO i = 1, nx_wrf
      ii = i + 1
      var_wrf(i,j) = var_arps (ii,jj)
    END DO
  END DO

  RETURN
END SUBROUTINE array_arps2wrf_2d

!#######################################################################

SUBROUTINE readwrfinonefile(file_name,nx_wrf,ny_wrf,nz_wrf,nxlg_wrf,nylg_wrf,&
                        dim0dvar, dim1dvar, dim2dvar, dim3dvar,         &
                        numsvar,namessvar,vars_wrf,                     &
                        num1dvar,names1dvar,size1dvar,maxsize1d,var1d_wrf, &
                        num2dvar,names2dvar,var2d_wrf,                  &
                        num3dvar,names3dvar,stag3dvar,var3d_wrf,        &
                        istatus)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Read WRF variables in one file and then broadcast for further
!  processing of each process separately.
!
!  It can only handles One WRF file in no-mpi mode  or mpi mode.
!
!  Note that the output array size starting from 0 and ending with one
!  more extra grid.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!    5/05/2017.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
  USE model_precision

  IMPLICIT NONE

  CHARACTER(LEN=256)   :: file_name
  INTEGER, INTENT(IN)  :: nx_wrf, ny_wrf, nz_wrf
  INTEGER, INTENT(IN)  :: nxlg_wrf, nylg_wrf
  INTEGER, INTENT(IN)  :: dim0dvar, dim1dvar, dim2dvar, dim3dvar
  INTEGER, INTENT(IN)  :: numsvar,  num1dvar, num2dvar, num3dvar
  CHARACTER(LEN=40), INTENT(IN) :: namessvar (dim0dvar)
  CHARACTER(LEN=40), INTENT(IN) :: names1dvar(dim1dvar)
  CHARACTER(LEN=40), INTENT(IN) :: names2dvar(dim2dvar)
  CHARACTER(LEN=40), INTENT(IN) :: names3dvar(dim3dvar)
  INTEGER,           INTENT(IN) :: size1dvar(dim1dvar),maxsize1d
  CHARACTER(LEN=1),  INTENT(IN) :: stag3dvar(dim3dvar)

  REAL(P), INTENT(OUT) :: vars_wrf(dim0dvar)
  REAL(P), INTENT(OUT) :: var1d_wrf(maxsize1d,                       dim1dvar)
  REAL(P), INTENT(OUT) :: var2d_wrf(0:nx_wrf+1,0:ny_wrf+1,           dim2dvar)
  REAL(P), INTENT(OUT) :: var3d_wrf(0:nx_wrf+1,0:ny_wrf+1,0:nz_wrf+1,dim3dvar)

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'mp.inc'
  INCLUDE 'netcdf.inc'

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INTEGER :: fhndl, varid
  INTEGER :: num3d, num3dx, num3dy, num3dz
  INTEGER :: size3d, size3dx, size3dy, size3dz
  INTEGER :: size2d

  INTEGER :: i,j,k,n
  INTEGER :: ii, jj
  INTEGER :: ips, ipe, jps, jpe, kps, kpe
  INTEGER :: ipeu, jpev, kpew, ibgn, jbgn

  REAL(SP), ALLOCATABLE :: dta3d(:,:,:,:), dta3dx(:,:,:,:), dta3dy(:,:,:,:), dta3dz(:,:,:,:)
  REAL(SP), ALLOCATABLE :: dta2d(:,:,:), dta1d(:,:), dtas(:)

  REAL(P),  ALLOCATABLE :: var3d(:,:,:), var3dx(:,:,:), var3dy(:,:,:), var3dz(:,:,:)
  REAL(P),  ALLOCATABLE :: var2d(:,:), var1d(:)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

!-----------------------------------------------------------------------
! Prepare for reading
!-----------------------------------------------------------------------
  num3dx = 0
  num3dy = 0
  num3dz = 0

  DO n = 1, num3dvar
    IF (stag3dvar(n) == 'X') num3dx = num3dx + 1
    IF (stag3dvar(n) == 'Y') num3dy = num3dy + 1
    IF (stag3dvar(n) == 'Z' .OR. stag3dvar(n) == 'V') num3dz = num3dz + 1
  END DO

  num3d = num3dvar-num3dx-num3dy-num3dz

  IF (num3d  > 0) ALLOCATE(var3d (nxlg_wrf-1,nylg_wrf-1,nz_wrf-1), STAT = istatus)
  IF (num3dx > 0) ALLOCATE(var3dx(nxlg_wrf,  nylg_wrf-1,nz_wrf-1), STAT = istatus)
  IF (num3dy > 0) ALLOCATE(var3dy(nxlg_wrf-1,nylg_wrf,  nz_wrf-1), STAT = istatus)
  IF (num3dz > 0) ALLOCATE(var3dz(nxlg_wrf-1,nylg_wrf-1,nz_wrf),   STAT = istatus)

  IF (num2dvar > 0) ALLOCATE(var2d(nxlg_wrf-1,nylg_wrf-1), STAT = istatus)
  IF (num1dvar > 0) ALLOCATE(var1d(maxsize1d),             STAT = istatus)

!-----------------------------------------------------------------------
!
! Do reading
!
!-----------------------------------------------------------------------

  IF (myproc == 0) THEN

    IF (num3d  > 0) ALLOCATE(dta3d (nxlg_wrf-1,nylg_wrf-1,nz_wrf-1, num3d),  STAT = istatus)
    IF (num3dx > 0) ALLOCATE(dta3dx(nxlg_wrf,  nylg_wrf-1,nz_wrf-1, num3dx), STAT = istatus)
    IF (num3dy > 0) ALLOCATE(dta3dy(nxlg_wrf-1,nylg_wrf,  nz_wrf-1, num3dy), STAT = istatus)
    IF (num3dz > 0) ALLOCATE(dta3dz(nxlg_wrf-1,nylg_wrf-1,nz_wrf,   num3dz), STAT = istatus)

    IF (num2dvar > 0) ALLOCATE(dta2d(nxlg_wrf-1,nylg_wrf-1,         num2dvar), STAT = istatus)
    IF (num1dvar > 0) ALLOCATE(dta1d(maxsize1d,                     num1dvar), STAT = istatus)
    IF (numsvar > 0)  ALLOCATE(dtas(                                numsvar),  STAT = istatus)

    istatus = NF_OPEN(TRIM(file_name),NF_NOWRITE,fhndl)
    CALL handle_ncd_error(istatus,'NF_OPEN in readwrfinonefile.',.TRUE.)

    !
    ! Read 3D vars
    !
    num3dx = 0
    num3dy = 0
    num3dz = 0
    num3d  = 0
    DO n = 1, num3dvar
      istatus = NF_INQ_VARID(fhndl,names3dvar(n),varid)
      CALL handle_ncd_error(istatus,'NF_INQ_VARID in readwrfinonefile.',.TRUE.)

      IF (stag3dvar(n) == 'X') THEN
        num3dx = num3dx+1
        istatus = NF_GET_VARA_REAL(fhndl,varid,(/1,1,1,1/),             &
              (/nxlg_wrf,  nylg_wrf-1,nz_wrf-1,1/),dta3dx(:,:,:,num3dx))
      ELSE IF (stag3dvar(n) == 'Y') THEN
        num3dy = num3dy+1
        istatus = NF_GET_VARA_REAL(fhndl,varid,(/1,1,1,1/),             &
              (/nxlg_wrf-1,nylg_wrf,  nz_wrf-1,1/),dta3dy(:,:,:,num3dy))
      ELSE IF (stag3dvar(n) == 'Z' .OR. stag3dvar(n) == 'V') THEN
        num3dz = num3dz+1
        istatus = NF_GET_VARA_REAL(fhndl,varid,(/1,1,1,1/),             &
              (/nxlg_wrf-1,nylg_wrf-1,nz_wrf,  1/),dta3dz(:,:,:,num3dz))
      ELSE
        num3d = num3d+1
        istatus = NF_GET_VARA_REAL(fhndl,varid,(/1,1,1,1/),             &
              (/nxlg_wrf-1,nylg_wrf-1,nz_wrf-1,1/),dta3d(:,:,:,num3d))
      END IF

      IF(istatus /= NF_NOERR) THEN
        CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in readwrfinonefile:'//names3dvar(n),.TRUE.)
      END IF

    END DO

    !
    ! Read 2D vars
    !
    DO n = 1, num2dvar
      istatus = NF_INQ_VARID(fhndl,names2dvar(n),varid)
      CALL handle_ncd_error(istatus,'NF_INQ_VARID in readwrfinonefile.',.TRUE.)

      istatus = NF_GET_VARA_REAL(fhndl,varid,(/1,1,1/),                 &
                               (/nxlg_wrf-1,nylg_wrf-1,1/),dta2d(:,:,n))
      IF(istatus /= NF_NOERR) THEN
        CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in readwrfinonefile:'//names2dvar(n),.TRUE.)
      END IF

    END DO

    !
    ! Read 1D vars
    !
    DO n = 1, num1dvar
      istatus = NF_INQ_VARID(fhndl,names1dvar(n),varid)
      CALL handle_ncd_error(istatus,'NF_INQ_VARID in readwrfinonefile.',.TRUE.)

      istatus = NF_GET_VARA_REAL(fhndl,varid,(/1,1/),                   &
                                          (/size1dvar(n),1/),dta1d(:,n))
      IF(istatus /= NF_NOERR) THEN
        CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in readwrfinonefile:'//names1dvar(n),.TRUE.)
      END IF

    END DO

    !
    ! Read scalar variables
    !
    DO n = 1, numsvar
      istatus = NF_INQ_VARID(fhndl,namessvar(n),varid)
      CALL handle_ncd_error(istatus,'NF_INQ_VARID in readwrfinonefile.',.TRUE.)

      istatus = NF_GET_VARA_REAL(fhndl,varid,(/1/),(/1/),dtas(n))
      IF(istatus /= NF_NOERR) THEN
        CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in readwrfinonefile:'//namessvar(n),.TRUE.)
      END IF

    END DO

    istatus = NF_CLOSE(fhndl)
    CALL handle_ncd_error(istatus,'NF_CLOSE in readwrfinonefile',.TRUE.)

  END IF

!-----------------------------------------------------------------------
!
! Do processing
!
!-----------------------------------------------------------------------

  !
  ! Prepare extracting dimensions
  !
  IF (loc_x == 1) THEN
    ips  = 1
    ibgn = 0
  ELSE
    ips  = (loc_x-1)*(nx_wrf-1)
    ibgn = ips
  END IF

  IF (loc_x == nproc_x) THEN
    ipe  = nxlg_wrf-1
    ipeu = nxlg_wrf
  ELSE
    ipe  = loc_x*(nx_wrf-1)+2      ! extract 2 extra points, one for staggered U, one reserved for the ARPS grid, nx
    ipeu = ipe
  END IF

  IF (loc_y == 1) THEN
    jps  = 1
    jbgn = 0
  ELSE
    jps  = (loc_y-1)*(ny_wrf-1)
    jbgn = jps
  END IF

  IF (loc_y == nproc_y) THEN
    jpe  = nylg_wrf-1
    jpev = nylg_wrf
  ELSE
    jpe  = loc_y*(ny_wrf-1)+2      ! extract 2 extra points, one for staggered V, one reserved for the ARPS grid, ny
    jpev = jpe
  END IF

  kps  = 1
  kpe  = nz_wrf-1
  kpew = nz_wrf

  !
  ! Process 3d Variables
  !
  size3dx = (nxlg_wrf)  *(nylg_wrf-1)*(nz_wrf-1)
  size3dy = (nxlg_wrf-1)*(nylg_wrf)  *(nz_wrf-1)
  size3dz = (nxlg_wrf-1)*(nylg_wrf-1)*(nz_wrf)
  size3d  = (nxlg_wrf-1)*(nylg_wrf-1)*(nz_wrf-1)

  num3dx = 0
  num3dy = 0
  num3dz = 0
  num3d  = 0
  DO n = 1, num3dvar
    IF (stag3dvar(n) == 'X') THEN
      num3dx = num3dx+1
      IF (myproc == 0) var3dx(:,:,:) = dta3dx(:,:,:,num3dx)
      CALL mpbcastra(var3dx,size3dx,0)

      DO k = kps,kpe
        DO j = jps, jpe
          jj = j-jbgn
          DO i = ips, ipeu
            ii = i-ibgn
            var3d_wrf(ii,jj,k,n) = var3dx(i,j,k)
          END DO
        END DO
      END DO
!write(0,*) 'X0:', myproc,nx_wrf,ny_wrf,nz_wrf
!write(0,*) 'X1:', myproc,ips,ipeu,jps,jpe,ibgn,jbgn,kps,kpe
      CALL edgfill(var3d_wrf(:,:,:,n),0,nx_wrf+1,ips-ibgn,ipeu-ibgn,    &
                                      0,ny_wrf+1,jps-jbgn,jpe-jbgn,     &
                                      0,nz_wrf+1,kps,kpe)

    ELSE IF (stag3dvar(n) == 'Y') THEN
      num3dy = num3dy+1
      IF (myproc == 0) var3dy(:,:,:) = dta3dy(:,:,:,num3dy)
      CALL mpbcastra(var3dy,size3dy,0)

      DO k = kps,kpe
        DO j = jps, jpev
          jj = j-jbgn
          DO i = ips, ipe
            ii = i-ibgn
            var3d_wrf(ii,jj,k,n) = var3dy(i,j,k)
          END DO
        END DO
      END DO

!write(0,*) 'Y0:', myproc,nx_wrf,ny_wrf,nz_wrf
!write(0,*) 'Y1:', myproc,ips,ipe,jps,jpev,ibgn,jbgn,kps,kpe
      CALL edgfill(var3d_wrf(:,:,:,n),0,nx_wrf+1,ips-ibgn,ipe-ibgn,     &
                                      0,ny_wrf+1,jps-jbgn,jpev-jbgn,    &
                                      0,nz_wrf+1,kps,kpe)

    ELSE IF (stag3dvar(n) == 'Z') THEN
      num3dz = num3dz+1
      IF (myproc == 0) var3dz(:,:,:) = dta3dz(:,:,:,num3dz)
      CALL mpbcastra(var3dz,size3dz,0)

      DO k = kps,kpew
        DO j = jps, jpe
          jj = j-jbgn
          DO i = ips, ipe
            ii = i-ibgn
            var3d_wrf(ii,jj,k,n) = var3dz(i,j,k)
          END DO
        END DO
      END DO

!write(0,*) 'Z0:', myproc,nx_wrf,ny_wrf,nz_wrf
!write(0,*) 'Z1:', myproc,ips,ipe,jps,jpe,ibgn,jbgn,kps,kpew
      CALL edgfill(var3d_wrf(:,:,:,n),0,nx_wrf+1,ips-ibgn,ipe-ibgn,     &
                                      0,ny_wrf+1,jps-jbgn,jpe-jbgn,     &
                                      0,nz_wrf+1,kps,kpew)

    ELSE IF (stag3dvar(n) == 'V') THEN   ! Vertical extension
      num3dz = num3dz+1
      IF (myproc == 0) var3dz(:,:,:) = dta3dz(:,:,:,num3dz)
      CALL mpbcastra(var3dz,size3dz,0)

      DO k = kps,kpew
        DO j = jps, jpe
          jj = j-jbgn
          DO i = ips, ipe
            ii = i-ibgn
            var3d_wrf(ii,jj,k,n) = var3dz(i,j,k)
          END DO
        END DO
      END DO

!write(0,*) 'V0:', myproc,nx_wrf,ny_wrf,nz_wrf
!write(0,*) 'V1:', myproc,ips,ipe,jps,jpe,ibgn,jbgn,kps,kpew
      CALL edgfill(var3d_wrf(:,:,:,n),0,nx_wrf+1,ips-ibgn,ipe-ibgn,     &
                                      0,ny_wrf+1,jps-jbgn,jpe-jbgn,     &
                                      0,nz_wrf+1,kps,kpew)

      DO j = 0, ny_wrf+1
        DO i = 0, nx_wrf+1
          var3d_wrf(i,j,0,n)        = 2*var3d_wrf(i,j,1,n)      - var3d_wrf(ii,jj,2,n)
          var3d_wrf(i,j,nz_wrf+1,n) = 2*var3d_wrf(i,j,nz_wrf,n) - var3d_wrf(ii,jj,nz_wrf-1,n)
        END DO
      END DO

    ELSE
      num3d = num3d+1
      IF (myproc == 0) var3d(:,:,:) = dta3d(:,:,:,num3d)
      CALL mpbcastra(var3d,size3d,0)

      DO k = kps,kpe
        DO j = jps, jpe
          jj = j-jbgn
          DO i = ips, ipe
            ii = i-ibgn
            var3d_wrf(ii,jj,k,n) = var3d(i,j,k)
          END DO
        END DO
      END DO

!write(0,*) 'M0:', myproc,nx_wrf,ny_wrf,nz_wrf
!write(0,*) 'M1:', myproc,ips,ipe,jps,jpe,ibgn,jbgn,kps,kpe
      CALL edgfill(var3d_wrf(:,:,:,n),0,nx_wrf+1,ips-ibgn,ipe-ibgn,     &
                                      0,ny_wrf+1,jps-jbgn,jpe-jbgn,     &
                                      0,nz_wrf+1,kps,kpe)

    END IF
  END DO

  !
  ! Process 2d Variables
  !
  size2d  = (nxlg_wrf-1)*(nylg_wrf-1)

  DO n = 1, num2dvar

    IF (myproc == 0) var2d(:,:) = dta2d(:,:,n)
    CALL mpbcastra(var2d,size2d,0)

    DO j = jps, jpe
       jj = j-jbgn
       DO i = ips, ipe
         ii = i-ibgn
         var2d_wrf(ii,jj,n) = var2d(i,j)
       END DO
     END DO

     CALL edgfill(var2d_wrf(:,:,n),0,nx_wrf+1,ips-ibgn,ipe-ibgn,        &
                                   0,ny_wrf+1,jps-jbgn,jpe-jbgn,        &
                                   1,1,1,1)

  END DO

 !
 ! Process 1d Variables
 !
 var1d_wrf = 0.0
 DO n = 1, num1dvar

   IF (myproc == 0) var1d(:) = dta1d(:,n)
   CALL mpbcastra(var1d,maxsize1d,0)

   DO k = 1, size1dvar(n)
     var1d_wrf(k,n) = var1d(k)
   END DO

 END DO

 !
 ! Process scalar
 !
 var1d_wrf = 0.0
 DO n = 1, numsvar
   IF (myproc == 0) vars_wrf(n) = dtas(n)
   CALL mpbcastr(vars_wrf(n),0)
 END DO

!-----------------------------------------------------------------------
! Finishing
!-----------------------------------------------------------------------

  IF (myproc == 0) THEN
    IF (ALLOCATED(dta3d  )) DEALLOCATE(dta3d  )
    IF (ALLOCATED(dta3dx )) DEALLOCATE(dta3dx )
    IF (ALLOCATED(dta3dy )) DEALLOCATE(dta3dy )
    IF (ALLOCATED(dta3dz )) DEALLOCATE(dta3dz )

    IF (ALLOCATED(dta2d))   DEALLOCATE(dta2d)
    IF (ALLOCATED(dta1d))   DEALLOCATE(dta1d)
    IF (ALLOCATED(dtas) )   DEALLOCATE(dtas )
  ENDIF

  IF (ALLOCATED(var3d) ) DEALLOCATE(var3d  )
  IF (ALLOCATED(var3dx)) DEALLOCATE(var3dx )
  IF (ALLOCATED(var3dy)) DEALLOCATE(var3dy )
  IF (ALLOCATED(var3dz)) DEALLOCATE(var3dz )

  IF (ALLOCATED(var2d) ) DEALLOCATE(var2d)
  IF (ALLOCATED(var1d) ) DEALLOCATE(var1d)

  RETURN
END SUBROUTINE readwrfinonefile

!#######################################################################

SUBROUTINE readwrfens(ensdirname,ensfileopt,ensreadready,nensemble,timestrin,iensemble,&
                      nx_wrf,ny_wrf,nz_wrf,nxlg_wrf,nylg_wrf,           &
                      dim0dvar, dim1dvar, dim2dvar, dim3dvar,           &
                      numsvar,namessvar,vars_wrf,                       &
                      num1dvar,names1dvar,size1dvar,maxsize1d,var1d_wrf, &
                      num2dvar,names2dvar,var2d_wrf,                    &
                      num3dvar,names3dvar,stag3dvar,var3d_wrf,          &
                      istatus)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Read WRF variables in one file and then broadcast for further
!  processing of each process separately.
!
!  It can only handles One WRF file in no-mpi mode  or mpi mode.
!
!  Note that the output array size starts from 0 and ends with one
!  more extra grid.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!    5/05/2017.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
  USE model_precision

  IMPLICIT NONE

  CHARACTER(LEN=256), INTENT(IN) :: ensdirname
  CHARACTER(LEN=19),  INTENT(IN) :: timestrin
  INTEGER, INTENT(IN)  :: ensfileopt, ensreadready
  INTEGER, INTENT(IN)  :: nensemble, iensemble

  INTEGER, INTENT(IN)  :: nx_wrf, ny_wrf, nz_wrf
  INTEGER, INTENT(IN)  :: nxlg_wrf, nylg_wrf
  INTEGER, INTENT(IN)  :: dim0dvar, dim1dvar, dim2dvar, dim3dvar
  INTEGER, INTENT(IN)  :: numsvar,  num1dvar, num2dvar, num3dvar
  CHARACTER(LEN=40), INTENT(IN) :: namessvar (dim0dvar)
  CHARACTER(LEN=40), INTENT(IN) :: names1dvar(dim1dvar)
  CHARACTER(LEN=40), INTENT(IN) :: names2dvar(dim2dvar)
  CHARACTER(LEN=40), INTENT(IN) :: names3dvar(dim3dvar)
  INTEGER,           INTENT(IN) :: size1dvar(dim1dvar),maxsize1d
  CHARACTER(LEN=1),  INTENT(IN) :: stag3dvar(dim3dvar)

  REAL(P), INTENT(OUT) :: vars_wrf(dim0dvar,nensemble)
  REAL(P), INTENT(OUT) :: var1d_wrf(maxsize1d,                       dim1dvar,nensemble)
  REAL(P), INTENT(OUT) :: var2d_wrf(0:nx_wrf+1,0:ny_wrf+1,           dim2dvar,nensemble)
  REAL(P), INTENT(OUT) :: var3d_wrf(0:nx_wrf+1,0:ny_wrf+1,0:nz_wrf+1,dim3dvar,nensemble)

  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'mp.inc'
  INCLUDE 'netcdf.inc'

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  CHARACTER(LEN=256)   :: dirfn, file_name, readyfilename
  LOGICAL :: fexists

  INTEGER :: fhndl, varid
  INTEGER :: num3d, num3dx, num3dy, num3dz
  INTEGER :: size3d, size3dx, size3dy, size3dz
  INTEGER :: size2d

  INTEGER :: i,j,k,n,l
  INTEGER :: ii, jj
  INTEGER :: ips, ipe, jps, jpe, kps, kpe
  INTEGER :: ipeu, jpev, kpew, ibgn, jbgn

  REAL(SP), ALLOCATABLE :: dta3d(:,:,:,:), dta3dx(:,:,:,:), dta3dy(:,:,:,:), dta3dz(:,:,:,:)
  REAL(SP), ALLOCATABLE :: dta2d(:,:,:), dta1d(:,:), dtas(:)

  REAL(P),  ALLOCATABLE :: var3d(:,:,:), var3dx(:,:,:), var3dy(:,:,:), var3dz(:,:,:)
  REAL(P),  ALLOCATABLE :: var2d(:,:), var1d(:)

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

!-----------------------------------------------------------------------
! Prepare for reading
!-----------------------------------------------------------------------
  num3dx = 0
  num3dy = 0
  num3dz = 0

  DO n = 1, num3dvar
    IF (stag3dvar(n) == 'X') num3dx = num3dx + 1
    IF (stag3dvar(n) == 'Y') num3dy = num3dy + 1
    IF (stag3dvar(n) == 'Z' .OR. stag3dvar(n) == 'V') num3dz = num3dz + 1
  END DO

  num3d = num3dvar-num3dx-num3dy-num3dz

  IF (num3d  > 0) ALLOCATE(var3d (nxlg_wrf-1,nylg_wrf-1,nz_wrf-1), STAT = istatus)
  IF (num3dx > 0) ALLOCATE(var3dx(nxlg_wrf,  nylg_wrf-1,nz_wrf-1), STAT = istatus)
  IF (num3dy > 0) ALLOCATE(var3dy(nxlg_wrf-1,nylg_wrf,  nz_wrf-1), STAT = istatus)
  IF (num3dz > 0) ALLOCATE(var3dz(nxlg_wrf-1,nylg_wrf-1,nz_wrf),   STAT = istatus)

  IF (num2dvar > 0) ALLOCATE(var2d(nxlg_wrf-1,nylg_wrf-1), STAT = istatus)
  IF (num1dvar > 0) ALLOCATE(var1d(maxsize1d),             STAT = istatus)

!-----------------------------------------------------------------------
!
! Do reading
!
!-----------------------------------------------------------------------

  DO l = nensemble, 1, -1

    IF (myproc == l) THEN

      CALL get_input_dirname(ensdirname,l,dirfn)

      IF (ensfileopt == 1) THEN      ! WRF INPUT
        WRITE(file_name,'(2a)') TRIM(dirfn),'wrfinput_d01'
      ELSE IF (ensfileopt == 2) THEN      ! WRF OUTPUT
        WRITE(file_name,'(3a)')     TRIM(dirfn),'wrfout_d01_', timestrin
        IF (ensreadready > 0) THEN
          WRITE(readyfilename,'(3a)') TRIM(dirfn),'wrfoutReady_d01_', timestrin
          CALL wait_for_readyfile(readyfilename,istatus)
          IF (istatus /= 0) CALL arpsstop('ERROR: readyfile not ready -'//TRIM(readyfilename))
        END IF

      ELSE IF (ensfileopt == 3) THEN      ! WRF INPUT
        WRITE(file_name,'(2a)') TRIM(dirfn),'wrfinput_d01_ic'
      ELSE IF (ensfileopt == 4) THEN ! WRF Forecast from NEWS-e
        WRITE(file_name,'(4a,I0)') TRIM(dirfn),'wrffcst_d01_',timestrin,'_',l
      ELSE  ! WRF OUTPUT
        WRITE(file_name,'(4a,I0)') TRIM(dirfn),'wrfout_d01_', timestrin,'_',l
        IF (ensreadready > 0) THEN
          WRITE(readyfilename,'(4a,I0)') TRIM(dirfn),'wrfoutReady_d01_', timestrin,'_',l
          CALL wait_for_readyfile(readyfilename,istatus)
          IF (istatus /= 0) CALL arpsstop('ERROR: readyfile not ready -'//TRIM(readyfilename))
        END IF
      END IF

      INQUIRE(FILE = file_name, EXIST = fexists)
      IF (.NOT. fexists) THEN
        istatus = -1
        WRITE(6,'(2a)') 'ERROR: File not found: ', file_name
        RETURN
      ENDIF

      !WRITE(*,'(1x,a,I2,3a,I2)') 'Proc:',myproc,' Reading Background from file <',TRIM(file_name),'> for member ',l


      IF (num3d  > 0) ALLOCATE(dta3d (nxlg_wrf-1,nylg_wrf-1,nz_wrf-1, num3d),  STAT = istatus)
      IF (num3dx > 0) ALLOCATE(dta3dx(nxlg_wrf,  nylg_wrf-1,nz_wrf-1, num3dx), STAT = istatus)
      IF (num3dy > 0) ALLOCATE(dta3dy(nxlg_wrf-1,nylg_wrf,  nz_wrf-1, num3dy), STAT = istatus)
      IF (num3dz > 0) ALLOCATE(dta3dz(nxlg_wrf-1,nylg_wrf-1,nz_wrf,   num3dz), STAT = istatus)

      IF (num2dvar > 0) ALLOCATE(dta2d(nxlg_wrf-1,nylg_wrf-1,         num2dvar), STAT = istatus)
      IF (num1dvar > 0) ALLOCATE(dta1d(maxsize1d,                     num1dvar), STAT = istatus)
      IF (numsvar > 0)  ALLOCATE(dtas(                                numsvar),  STAT = istatus)

      istatus = NF_OPEN(TRIM(file_name),NF_NOWRITE,fhndl)
      CALL handle_ncd_error(istatus,'NF_OPEN in readwrfinonefile.',.TRUE.)

      !
      ! Read 3D vars
      !
      num3dx = 0
      num3dy = 0
      num3dz = 0
      num3d  = 0
      DO n = 1, num3dvar
        istatus = NF_INQ_VARID(fhndl,names3dvar(n),varid)
        CALL handle_ncd_error(istatus,'NF_INQ_VARID in readwrfinonefile.',.TRUE.)

        IF (stag3dvar(n) == 'X') THEN
          num3dx = num3dx+1
          istatus = NF_GET_VARA_REAL(fhndl,varid,(/1,1,1,1/),             &
                (/nxlg_wrf,  nylg_wrf-1,nz_wrf-1,1/),dta3dx(:,:,:,num3dx))
        ELSE IF (stag3dvar(n) == 'Y') THEN
          num3dy = num3dy+1
          istatus = NF_GET_VARA_REAL(fhndl,varid,(/1,1,1,1/),             &
                (/nxlg_wrf-1,nylg_wrf,  nz_wrf-1,1/),dta3dy(:,:,:,num3dy))
        ELSE IF (stag3dvar(n) == 'Z' .OR. stag3dvar(n) == 'V') THEN
          num3dz = num3dz+1
          istatus = NF_GET_VARA_REAL(fhndl,varid,(/1,1,1,1/),             &
                (/nxlg_wrf-1,nylg_wrf-1,nz_wrf,  1/),dta3dz(:,:,:,num3dz))
        ELSE
          num3d = num3d+1
          istatus = NF_GET_VARA_REAL(fhndl,varid,(/1,1,1,1/),             &
                (/nxlg_wrf-1,nylg_wrf-1,nz_wrf-1,1/),dta3d(:,:,:,num3d))
        END IF

        IF(istatus /= NF_NOERR) THEN
          CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in readwrfinonefile:'//names3dvar(n),.TRUE.)
        END IF

      END DO

      !
      ! Read 2D vars
      !
      DO n = 1, num2dvar
        istatus = NF_INQ_VARID(fhndl,names2dvar(n),varid)
        CALL handle_ncd_error(istatus,'NF_INQ_VARID in readwrfinonefile.',.TRUE.)

        istatus = NF_GET_VARA_REAL(fhndl,varid,(/1,1,1/),                 &
                                 (/nxlg_wrf-1,nylg_wrf-1,1/),dta2d(:,:,n))
        IF(istatus /= NF_NOERR) THEN
          CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in readwrfinonefile:'//names2dvar(n),.TRUE.)
        END IF

      END DO

      !
      ! Read 1D vars
      !
      DO n = 1, num1dvar
        istatus = NF_INQ_VARID(fhndl,names1dvar(n),varid)
        CALL handle_ncd_error(istatus,'NF_INQ_VARID in readwrfinonefile.',.TRUE.)

        istatus = NF_GET_VARA_REAL(fhndl,varid,(/1,1/),                   &
                                            (/size1dvar(n),1/),dta1d(:,n))
        IF(istatus /= NF_NOERR) THEN
          CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in readwrfinonefile:'//names1dvar(n),.TRUE.)
        END IF

      END DO

      !
      ! Read scalar variables
      !
      DO n = 1, numsvar
        istatus = NF_INQ_VARID(fhndl,namessvar(n),varid)
        CALL handle_ncd_error(istatus,'NF_INQ_VARID in readwrfinonefile.',.TRUE.)

        istatus = NF_GET_VARA_REAL(fhndl,varid,(/1/),(/1/),dtas(n))
        IF(istatus /= NF_NOERR) THEN
          CALL handle_ncd_error(istatus,'NF_GET_VARA_REAL in readwrfinonefile:'//namessvar(n),.TRUE.)
        END IF

      END DO

      istatus = NF_CLOSE(fhndl)
      CALL handle_ncd_error(istatus,'NF_CLOSE in readwrfinonefile',.TRUE.)

    END IF

  END DO
  CALL mpbarrier

!-----------------------------------------------------------------------
!
! Do processing
!
!-----------------------------------------------------------------------

  !
  ! Prepare extracting dimensions
  !
  IF (loc_x == 1) THEN
    ips  = 1
    ibgn = 0
  ELSE
    ips  = (loc_x-1)*(nx_wrf-1)
    ibgn = ips
  END IF

  IF (loc_x == nproc_x) THEN
    ipe  = nxlg_wrf-1
    ipeu = nxlg_wrf
  ELSE
    ipe  = loc_x*(nx_wrf-1)+2      ! extract 2 extra points, one for staggered U, one reserved for the ARPS grid, nx
    ipeu = ipe
  END IF

  IF (loc_y == 1) THEN
    jps  = 1
    jbgn = 0
  ELSE
    jps  = (loc_y-1)*(ny_wrf-1)
    jbgn = jps
  END IF

  IF (loc_y == nproc_y) THEN
    jpe  = nylg_wrf-1
    jpev = nylg_wrf
  ELSE
    jpe  = loc_y*(ny_wrf-1)+2      ! extract 2 extra points, one for staggered V, one reserved for the ARPS grid, ny
    jpev = jpe
  END IF

  kps  = 1
  kpe  = nz_wrf-1
  kpew = nz_wrf

  !
  ! Process 3d Variables
  !
  size3dx = (nxlg_wrf)  *(nylg_wrf-1)*(nz_wrf-1)
  size3dy = (nxlg_wrf-1)*(nylg_wrf)  *(nz_wrf-1)
  size3dz = (nxlg_wrf-1)*(nylg_wrf-1)*(nz_wrf)
  size3d  = (nxlg_wrf-1)*(nylg_wrf-1)*(nz_wrf-1)

  DO l = 1, nensemble

    IF (l == iensemble) CYCLE   ! skip one ensemble member as requested

    num3dx = 0
    num3dy = 0
    num3dz = 0
    num3d  = 0
    DO n = 1, num3dvar
      IF (stag3dvar(n) == 'X') THEN
        num3dx = num3dx+1
        IF (myproc == l) var3dx(:,:,:) = dta3dx(:,:,:,num3dx)
        CALL mpbcastra(var3dx,size3dx,l)

        DO k = kps,kpe
          DO j = jps, jpe
            jj = j-jbgn
            DO i = ips, ipeu
              ii = i-ibgn
              var3d_wrf(ii,jj,k,n,l) = var3dx(i,j,k)
            END DO
          END DO
        END DO
  !write(0,*) 'X0:', myproc,nx_wrf,ny_wrf,nz_wrf
  !write(0,*) 'X1:', myproc,ips,ipeu,jps,jpe,ibgn,jbgn,kps,kpe
        CALL edgfill(var3d_wrf(:,:,:,n,l),0,nx_wrf+1,ips-ibgn,ipeu-ibgn, &
                                          0,ny_wrf+1,jps-jbgn,jpe-jbgn,  &
                                          0,nz_wrf+1,kps,kpe)

      ELSE IF (stag3dvar(n) == 'Y') THEN
        num3dy = num3dy+1
        IF (myproc == l) var3dy(:,:,:) = dta3dy(:,:,:,num3dy)
        CALL mpbcastra(var3dy,size3dy,l)

        DO k = kps,kpe
          DO j = jps, jpev
            jj = j-jbgn
            DO i = ips, ipe
              ii = i-ibgn
              var3d_wrf(ii,jj,k,n,l) = var3dy(i,j,k)
            END DO
          END DO
        END DO

  !write(0,*) 'Y0:', myproc,nx_wrf,ny_wrf,nz_wrf
  !write(0,*) 'Y1:', myproc,ips,ipe,jps,jpev,ibgn,jbgn,kps,kpe
        CALL edgfill(var3d_wrf(:,:,:,n,l),0,nx_wrf+1,ips-ibgn,ipe-ibgn,   &
                                          0,ny_wrf+1,jps-jbgn,jpev-jbgn,  &
                                          0,nz_wrf+1,kps,kpe)

      ELSE IF (stag3dvar(n) == 'Z') THEN
        num3dz = num3dz+1
        IF (myproc == l) var3dz(:,:,:) = dta3dz(:,:,:,num3dz)
        CALL mpbcastra(var3dz,size3dz,l)

        DO k = kps,kpew
          DO j = jps, jpe
            jj = j-jbgn
            DO i = ips, ipe
              ii = i-ibgn
              var3d_wrf(ii,jj,k,n,l) = var3dz(i,j,k)
            END DO
          END DO
        END DO

  !write(0,*) 'Z0:', myproc,nx_wrf,ny_wrf,nz_wrf
  !write(0,*) 'Z1:', myproc,ips,ipe,jps,jpe,ibgn,jbgn,kps,kpew
        CALL edgfill(var3d_wrf(:,:,:,n,l),0,nx_wrf+1,ips-ibgn,ipe-ibgn,   &
                                          0,ny_wrf+1,jps-jbgn,jpe-jbgn,   &
                                          0,nz_wrf+1,kps,kpew)

      ELSE IF (stag3dvar(n) == 'V') THEN   ! Vertical extension
        num3dz = num3dz+1
        IF (myproc == l) var3dz(:,:,:) = dta3dz(:,:,:,num3dz)
        CALL mpbcastra(var3dz,size3dz,l)

        DO k = kps,kpew
          DO j = jps, jpe
            jj = j-jbgn
            DO i = ips, ipe
              ii = i-ibgn
              var3d_wrf(ii,jj,k,n,l) = var3dz(i,j,k)
            END DO
          END DO
        END DO

  !write(0,*) 'V0:', myproc,nx_wrf,ny_wrf,nz_wrf
  !write(0,*) 'V1:', myproc,ips,ipe,jps,jpe,ibgn,jbgn,kps,kpew
        CALL edgfill(var3d_wrf(:,:,:,n,l),0,nx_wrf+1,ips-ibgn,ipe-ibgn,   &
                                          0,ny_wrf+1,jps-jbgn,jpe-jbgn,   &
                                          0,nz_wrf+1,kps,kpew)

        DO j = 0, ny_wrf+1
          DO i = 0, nx_wrf+1
            var3d_wrf(i,j,0,n,l)        = 2*var3d_wrf(i,j,1,n,l)      - var3d_wrf(ii,jj,2,n,l)
            var3d_wrf(i,j,nz_wrf+1,n,l) = 2*var3d_wrf(i,j,nz_wrf,n,l) - var3d_wrf(ii,jj,nz_wrf-1,n,l)
          END DO
        END DO

      ELSE
        num3d = num3d+1
        IF (myproc == l) var3d(:,:,:) = dta3d(:,:,:,num3d)
        CALL mpbcastra(var3d,size3d,l)

        DO k = kps,kpe
          DO j = jps, jpe
            jj = j-jbgn
            DO i = ips, ipe
              ii = i-ibgn
              var3d_wrf(ii,jj,k,n,l) = var3d(i,j,k)
            END DO
          END DO
        END DO

  !write(0,*) 'M0:', myproc,nx_wrf,ny_wrf,nz_wrf
  !write(0,*) 'M1:', myproc,ips,ipe,jps,jpe,ibgn,jbgn,kps,kpe
        CALL edgfill(var3d_wrf(:,:,:,n,l),0,nx_wrf+1,ips-ibgn,ipe-ibgn,   &
                                          0,ny_wrf+1,jps-jbgn,jpe-jbgn,   &
                                          0,nz_wrf+1,kps,kpe)

      END IF

      !CALL mpbarrier
    END DO

  END DO

  !
  ! Process 2d Variables
  !
  size2d  = (nxlg_wrf-1)*(nylg_wrf-1)
  DO l = 1, nensemble

    IF (l == iensemble) CYCLE

    DO n = 1, num2dvar

      IF (myproc == l) var2d(:,:) = dta2d(:,:,n)
      CALL mpbcastra(var2d,size2d,l)

      DO j = jps, jpe
         jj = j-jbgn
         DO i = ips, ipe
           ii = i-ibgn
           var2d_wrf(ii,jj,n,l) = var2d(i,j)
         END DO
       END DO

       CALL edgfill(var2d_wrf(:,:,n,l),0,nx_wrf+1,ips-ibgn,ipe-ibgn,      &
                                       0,ny_wrf+1,jps-jbgn,jpe-jbgn,      &
                                       1,1,1,1)
       !CALL mpbarrier
    END DO


 !
 ! Process 1d Variables
 !
   var1d_wrf = 0.0
   DO n = 1, num1dvar

     IF (l == iensemble) CYCLE

     IF (myproc == l) var1d(:) = dta1d(:,n)
     CALL mpbcastra(var1d,maxsize1d,l)

     DO k = 1, size1dvar(n)
       var1d_wrf(k,n,l) = var1d(k)
     END DO

   END DO

 !
 ! Process scalar
 !
   var1d_wrf = 0.0
   DO n = 1, numsvar
     IF (myproc == l) vars_wrf(n,l) = dtas(n)
     CALL mpbcastr(vars_wrf(n,l),l)
   END DO

 END DO


!-----------------------------------------------------------------------
! Finishing
!-----------------------------------------------------------------------

!  IF (myproc == 0) THEN
    IF (ALLOCATED(dta3d  )) DEALLOCATE(dta3d  )
    IF (ALLOCATED(dta3dx )) DEALLOCATE(dta3dx )
    IF (ALLOCATED(dta3dy )) DEALLOCATE(dta3dy )
    IF (ALLOCATED(dta3dz )) DEALLOCATE(dta3dz )

    IF (ALLOCATED(dta2d))   DEALLOCATE(dta2d)
    IF (ALLOCATED(dta1d))   DEALLOCATE(dta1d)
    IF (ALLOCATED(dtas) )   DEALLOCATE(dtas )
!  ENDIF

  IF (ALLOCATED(var3d) ) DEALLOCATE(var3d  )
  IF (ALLOCATED(var3dx)) DEALLOCATE(var3dx )
  IF (ALLOCATED(var3dy)) DEALLOCATE(var3dy )
  IF (ALLOCATED(var3dz)) DEALLOCATE(var3dz )

  IF (ALLOCATED(var2d) ) DEALLOCATE(var2d)
  IF (ALLOCATED(var1d) ) DEALLOCATE(var1d)

  RETURN
END SUBROUTINE readwrfens

SUBROUTINE wait_for_readyfile(readyfilename,istatus)
  IMPLICIT NONE
  CHARACTER(LEN=256)   :: readyfilename
  INTEGER, INTENT(OUT) :: istatus

!---------------------------------------------------------------------

  LOGICAL :: fexists
  INTEGER :: waitseconds

  INTEGER, PARAMETER :: maxwait = 1000

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  INQUIRE(FILE=readyfilename, EXIST = fexists )

  waitseconds = 0
  DO WHILE (.NOT. fexists .AND. waitseconds < maxwait)
     WRITE(*,'(1x,3a,I0)') 'Waiting for ',TRIM(readyfilename),', waitseconds = ', waitseconds
     CALL SLEEP(10)
     waitseconds = waitseconds + 10
     INQUIRE(FILE=readyfilename, EXIST = fexists )
  END DO

  IF (.NOT. fexists) THEN
    WRITE(0,'(1x,3a)') 'ERROR: file <',TRIM(readyfilename),'> cannot be located.'
    istatus = -1
  END IF

  RETURN
END SUBROUTINE wait_for_readyfile
