!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE open_wrf_file              ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE open_wrf_one_file(filenamein,io_form,multifile,file_mode,    &
                             ncompressx,ncompressy,numdigits, nidout,istatus)
!
!------------------------------------------------------------------
!
!  PURPOSE:
!
!    Open a WRF file and return NetCDF file handler. The file handler
!    will only be returned to root processor (myproc == 0). It should
!    be used in no-mpi mode or mpi mode reading joined WRF file.
!
!    NOTE: it is required to call close_wrf_file explicitly to close
!          the opened file in your calling program.
!
!------------------------------------------------------------------

  IMPLICIT NONE
  CHARACTER(LEN=*), INTENT(IN)  :: filenamein
  INTEGER,          INTENT(IN)  :: io_form
  LOGICAL,          INTENT(IN)  :: multifile
  INTEGER,          INTENT(IN)  :: file_mode
  INTEGER,          INTENT(IN)  :: ncompressx, ncompressy
  INTEGER,          INTENT(IN)  :: numdigits
  INTEGER,          INTENT(OUT) :: nidout
  INTEGER,          INTENT(OUT) :: istatus

!------------------------------------------------------------------
!
!  Misc. local variable
!
!------------------------------------------------------------------
  LOGICAL            :: fexists

  CHARACTER(LEN=80)  :: sysdepinfo
  LOGICAL, SAVE      :: initialized = .FALSE.
  CHARACTER(LEN=256) :: filename

  INTEGER :: iloc_x, jloc_y, loc_proc
  CHARACTER(LEN=20)  :: fmtstr

  CHARACTER(LEN=1), PARAMETER :: cmode(0:3) = (/'r','r','w','m'/)

  INCLUDE 'mp.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Begining of executable code
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  IF (multifile) THEN

    iloc_x = (loc_x-1)/ncompressx    ! column of patch
    jloc_y = (loc_y-1)/ncompressy    ! rows of patch
    loc_proc = jloc_y*nproc_x_in + iloc_x + 1

    WRITE(fmtstr,'(a,I1,a,I1,a)') '(2a,I',numdigits,'.',numdigits,')'
    WRITE(filename,FMT=TRIM(fmtstr)) TRIM(filenamein),'_',loc_proc

  ELSE
    filename = filenamein
  END IF

  INQUIRE(FILE = TRIM(filename), EXIST = fexists)
  IF ( .NOT. fexists ) THEN
    WRITE(6,'(3a)') 'File not found: ',filename,' in open_wrf_one_file'
    CALL arpsstop('WRF file not exist.',1)
  ENDIF

  sysdepinfo = 'DATASET=HISTORY'

  IF (io_form == 7) THEN           ! no initialization needed

    !IF (myproc == 0) THEN
    CALL open_ncd_wrf_file ( TRIM(filename), cmode(file_mode), nidout, istatus )
    !END IF

  ELSE
    WRITE(0,*) 'Unsupported IO format - ',io_form,'.'
    CAlL arpsstop('Unsupported IO format.',1)
  END IF

  initialized = .TRUE.

  RETURN
END SUBROUTINE open_wrf_one_file
!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE close_wrf_file               ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE close_wrf_one_file(nch,io_form,istatus)
!
!------------------------------------------------------------------
!
!  PURPOSE:
!
!     Close the WRF file which is opened using open_wrf_one_file.
!     Only root processor do the job.
!
!------------------------------------------------------------------
  IMPLICIT NONE

  INTEGER, INTENT(IN) :: nch
  INTEGER, INTENT(IN) :: io_form
  INTEGER, INTENT(OUT):: istatus

!------------------------------------------------------------------
!
!  Misc. local variable
!
!------------------------------------------------------------------
!
  INCLUDE 'mp.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  istatus = 0

  !IF (io_form == 1) THEN
  !  IF (myproc == 0) CALL ext_int_ioclose(nch,iStatus)
  !ELSE IF (io_form == 5) THEN
  !  CALL close_phdf5_for_read(nch,istatus)

  IF(io_form == 7) THEN
    !IF (myproc == 0) THEN
    CALL close_ncd_wrf_file ( nch, istatus )
    !END IF
  END IF

  IF (istatus /= 0) THEN
    WRITE(0,'(1x,2a)') 'ERROR: closing file handler ',nch
    CALL arpsstop('Error in close_wrf_one_file.',1)
  END IF

  RETURN
END SUBROUTINE close_wrf_one_file
!
!##################################################################
!##################################################################
!######                                                      ######
!######             SUBROUTINE get_wrf_2d                    ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_split_wrf_2d(nfid,io_form,ncompressx,ncompressy,         &
                      datestr,itime,varname,stagger,                    &
                      !dimname1,dimname2,dimname3,              &
                      nx,ny,var2d,                                      &
                      nxdin,nydin,temdm,istatus)

!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Read in a 3D array from the WRF NetCDF file and fetch the section
!    needed for this process.
!
!-----------------------------------------------------------------------

  USE model_precision

  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: nfid
  INTEGER,          INTENT(IN)  :: io_form
  INTEGER,          INTENT(IN)  :: ncompressx,ncompressy
  CHARACTER(LEN=*), INTENT(IN)  :: datestr
  INTEGER,          INTENT(IN)  :: itime

  CHARACTER(LEN=*), INTENT(IN)  :: varname
  CHARACTER(LEN=*), INTENT(IN)  :: stagger
  !CHARACTER(LEN=*), INTENT(IN)  :: dimname1
  !CHARACTER(LEN=*), INTENT(IN)  :: dimname2
  !CHARACTER(LEN=*), INTENT(IN)  :: dimname3
  INTEGER,          INTENT(IN)  :: nx, ny              ! stagggered local index
  REAL(P),          INTENT(OUT) :: var2d(nx,ny)
  INTEGER,          INTENT(IN)  :: nxdin,nydin             ! unstaggered data size
  REAL(SP),         INTENT(OUT) :: temdm(nxdin*nydin)      !
  INTEGER,          INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INTEGER, PARAMETER :: VAR_NOTEXIST = -1
  INTEGER, PARAMETER :: WRF_REAL     = 104
  INTEGER, PARAMETER :: WRF_INTEGER  = 106

  INCLUDE 'mp.inc'

  INTEGER            :: i, j, k, ii, jj
  INTEGER            :: i1,j1,k1

  INTEGER            :: ips, ipe, jps, jpe      ! unstaggered start and end indices
  INTEGER            :: nxd, nyd
  INTEGER            :: iproc, jproc, nxsize, nysize

  !INTEGER,          INTENT(IN)  :: fzone
  !CHARACTER(80) :: DimNames(3)
  !INTEGER       :: DomainStart(3), DomainEnd(3)
  !INTEGER       :: MemoryStart(3), MemoryEnd(3)
  !INTEGER       :: PatchStart(3),  PatchEnd(3)
  !
  !INTEGER       :: nxdim, nydim, nzdim
  !INTEGER       :: ilocs,iloce,jlocs,jloce

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  IF ( myproc == 0 )   &
     WRITE(6,FMT='(2x,a,a10)',ADVANCE='NO') 'Reading 2D variable ', TRIM(varname)

  !nxdim = nxlg - 1    ! domain size
  !nydim = nylg - 1
  !nzdim = nzlg - 1
  !IF (Stagger == 'X') nxdim = nxlg
  !IF (Stagger == 'Y') nydim = nylg
  !IF (Stagger == 'Z') nzdim = nzlg
  !
  !ilocs = (nx-fzone)*(loc_x-1)+fzone
  !jlocs = (ny-fzone)*(loc_y-1)+fzone
  !iloce = (nx-fzone)*(loc_x)+fzone
  !jloce = (ny-fzone)*(loc_y)+fzone

  nxsize = (nxdin-1)/ncompressx
  iproc  = MOD(loc_x,ncompressx)
  IF (iproc == 0) iproc = ncompressx
  ips = (iproc-1)*nxsize + 1
  ipe = (iproc)*nxsize+1

  nysize = (nydin-1)/ncompressy
  jproc  = MOD(loc_y,ncompressy)
  IF (jproc == 0) jproc = ncompressy
  jps = (jproc-1)*nysize + 1
  jpe = (jproc)*nysize+1

  nxd = nxdin-1
  IF ( stagger == 'X' .AND. (loc_x-1)/ncompressx+1 == nproc_x_in ) THEN
    nxd = nxdin
  END IF

  nyd = nydin-1
  IF ( stagger == 'Y' .AND. (loc_y-1)/ncompressy+1 == nproc_y_in ) THEN
    nyd = nydin
  END IF

  IF (io_form == 7) THEN
    !IF (myproc == 0) THEN
    CALL get_ncd_2d(nfid,itime,varname,nxd,nyd,temdm,istatus)
    !END IF
    !CALL mpbcastra(temdm,nxd*nyd,0)

    DO j = jps,MIN(jpe,nyd)
      j1 = (j-1)*nxd
      jj = j-jps+1
      DO i = ips,MIN(ipe,nxd)
        i1 = i + j1
        ii = i-ips+1
        var2d(ii,jj) = temdm(i1)
      END DO
    END DO

  ELSE
    istatus = -1
    RETURN
  END IF

  IF ( myproc == 0 ) THEN
    IF (istatus == 0) THEN
      WRITE(6,'(a)') '         ...  DONE.'
    ELSE
      WRITE(6,'(a)') '         ...  ERROR.'
    END IF
  END IF

  RETURN
END SUBROUTINE get_split_wrf_2d

!
!##################################################################
!##################################################################
!######                                                      ######
!######              SUBROUTINE get_split_wrf_3d             ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE get_split_wrf_3d(nfid,io_form,ncompressx,ncompressy,         &
                      datestr,itime,varname,stagger,                    &
                      !dimname1,dimname2,dimname3,              &
                      nx,ny,nz,var3d,                                   &
                      nxdin,nydin,nzin,temdm,istatus)

!-----------------------------------------------------------------------
!
!  PURPOSE:
!    Read in a 3D array from the WRF NetCDF file and fetch the section
!    needed for this process.
!
!-----------------------------------------------------------------------

  USE model_precision

  IMPLICIT NONE

  INTEGER,          INTENT(IN)  :: nfid
  INTEGER,          INTENT(IN)  :: io_form
  INTEGER,          INTENT(IN)  :: ncompressx,ncompressy
  CHARACTER(LEN=*), INTENT(IN)  :: datestr
  INTEGER,          INTENT(IN)  :: itime

  CHARACTER(LEN=*), INTENT(IN)  :: varname
  CHARACTER(LEN=*), INTENT(IN)  :: stagger
  !CHARACTER(LEN=*), INTENT(IN)  :: dimname1
  !CHARACTER(LEN=*), INTENT(IN)  :: dimname2
  !CHARACTER(LEN=*), INTENT(IN)  :: dimname3
  INTEGER,          INTENT(IN)  :: nx, ny, nz              ! stagggered local index
  REAL(P),          INTENT(OUT) :: var3d(nx,ny,nz)
  INTEGER,          INTENT(IN)  :: nxdin,nydin,nzin             ! unstaggered data size
  REAL(SP),         INTENT(OUT) :: temdm(nxdin*nydin*nzin)      !
  INTEGER,          INTENT(INOUT) :: istatus

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INTEGER, PARAMETER :: VAR_NOTEXIST = -1
  INTEGER, PARAMETER :: WRF_REAL     = 104
  INTEGER, PARAMETER :: WRF_INTEGER  = 106

  INCLUDE 'mp.inc'

  INTEGER            :: i, j, k, ii,jj
  INTEGER            :: i1,j1,k1

  INTEGER            :: ips, ipe, jps, jpe      ! unstaggered start and end indices
  INTEGER            :: nxd, nyd, nzd
  INTEGER            :: iproc, jproc, nxsize, nysize

  INTEGER            :: lvldbg

  !INTEGER,          INTENT(IN)  :: fzone
  !CHARACTER(80) :: DimNames(3)
  !INTEGER       :: DomainStart(3), DomainEnd(3)
  !INTEGER       :: MemoryStart(3), MemoryEnd(3)
  !INTEGER       :: PatchStart(3),  PatchEnd(3)
  !
  !INTEGER       :: nxdim, nydim, nzdim
  !INTEGER       :: ilocs,iloce,jlocs,jloce

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  lvldbg = istatus

  IF ( myproc == 0 )   &
     WRITE(6,FMT='(2x,a,a10)',ADVANCE='NO') 'Reading 3D variable ', TRIM(varname)

  istatus = 0

  !nxdim = nxlg - 1    ! domain size
  !nydim = nylg - 1
  !nzdim = nzlg - 1
  !IF (Stagger == 'X') nxdim = nxlg
  !IF (Stagger == 'Y') nydim = nylg
  !IF (Stagger == 'Z') nzdim = nzlg
  !
  !ilocs = (nx-fzone)*(loc_x-1)+fzone
  !jlocs = (ny-fzone)*(loc_y-1)+fzone
  !iloce = (nx-fzone)*(loc_x)+fzone
  !jloce = (ny-fzone)*(loc_y)+fzone

  nxsize = (nxdin-1)/ncompressx
  iproc  = MOD(loc_x,ncompressx)
  IF (iproc == 0) iproc = ncompressx
  ips = (iproc-1)*nxsize + 1
  ipe = (iproc)*nxsize+1

  nysize = (nydin-1)/ncompressy
  jproc  = MOD(loc_y,ncompressy)
  IF (jproc == 0) jproc = ncompressy
  jps = (jproc-1)*nysize + 1
  jpe = (jproc)*nysize+1

  nxd = nxdin-1
  IF ( stagger == 'X' .AND. (loc_x-1)/ncompressx+1 == nproc_x_in ) THEN
    nxd = nxdin
  END IF

  nyd = nydin-1
  IF ( stagger == 'Y' .AND. (loc_y-1)/ncompressy+1 == nproc_y_in ) THEN
    nyd = nydin
  END IF

  IF (stagger == 'Z') THEN
    nzd = nzin
  ELSE
    nzd = nzin -1
  END IF

  IF (io_form == 7) THEN

     !IF (myproc == 0) THEN
     CALL get_ncd_3d(nfid,itime,varname,nxd,nyd,nzd,temdm,istatus)
     !END IF
     !CALL mpbcastra(temdm,nxd*nyd*nzd,0)

     DO k = 1,nz
       k1 = (k-1)*nxd*nyd
       DO j = jps,MIN(jpe,nyd)
         j1 = (j-1)*nxd
         jj = j-jps+1
         DO i = ips,MIN(ipe,nxd)
           i1 = i + j1 + k1
           ii = i-ips+1
           var3d(ii,jj,k) = temdm(i1)
         END DO
       END DO
     END DO

  ELSE
    istatus = -1
    RETURN
  END IF

  IF ( myproc == 0 ) THEN
    IF (istatus == 0) THEN
      WRITE(6,'(a)') '         ...  DONE.'
    ELSE
      WRITE(6,'(a)') '         ...  ERROR.'
    END IF
  END IF

  RETURN
END SUBROUTINE get_split_wrf_3d
