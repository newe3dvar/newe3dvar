!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE INITWRFGRD                 ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE initwrfgrd(file_name,nx,ny,nz,nzsoil,                        &
                    x,y,z,zp,zpsoil,hterain,mapfct,xlat,xlon,           &
                    j1,j2,j3,aj3x,aj3y,aj3z,j3inv,                      &
                    u,v,w,pt,pres,phprt,qv,qscalar,                     &
                    qvbar,rhobar,phb,                                   &
                    nk, qnames_out,wmixr, p_top, psfc, varsfc,          &
                    mu, mub, dnw, dn, znw, znu,                         &
                    use_arps_metrics,fnm,fnp,cf1,cf2,cf3,               &
                    tem1, tem2, tem3, tem4, tem5, istatus)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Initialize the model array variables from WRF history files
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  08/05/2013.
!
!  MODIFICATION HISTORY:
!  09/28/2016 (Y. Wang)
!  1. Note that only pass out j1, j2, j3, and j3inv which are used in divergence
!     constraint.
!            j1 -> zx
!            j2 -> zy
!            j3 -> dzw
!            j3inv -> rdzw
!
!  2. Note that mapfct array is defined differently as that in the ARPS grid.
!
!            mapfct(:,:,1) -> MAPFAC_M
!            mapfct(:,:,2) -> MAPFAC_U
!            mapfct(:,:,3) -> MAPFAC_V
!            mapfct(:,:,4) -> MAPFAC_MX
!            mapfct(:,:,5) -> MAPFAC_MY
!            mapfct(:,:,6) -> MAPFAC_UY
!            mapfct(:,:,7) -> MAPFAC_VX
!            mapfct(:,:,8) -> UNDEFINED
!
!  3. Note that dnw, dn, fnm, fnp etc. are dimensioned with nz, but there
!     is not value beyond the valid WRF grid.
!
!-----------------------------------------------------------------------
!
!
!  INPUT:
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zp       Vertical coordinate of grid points in physical space
!             (m)
!    zpsoil   Vertical coordinate of grid points in the soil model
!             in physical space (m).
!    hterain  The height of terrain (m)
!
!    mapfct   Map factors at scalar, u and v points
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!    aj3x     Avgx of the coordinate transformation Jacobian  d(zp)/dz
!    aj3y     Avgy of the coordinate transformation Jacobian  d(zp)/dz
!    aj3z     Avgz of the coordinate transformation Jacobian  d(zp)/dz
!
!  Note that j3, aj3x,aj3y,aj3z etc. is only used with use_arps_metrics = .TRUE.
!       and the definitions for j1,j2, j3, j3inv, and mapfct differ for use_arps_metrics.
!
!  OUTPUT:
!
!    u        x component of velocity at all time levels (m/s)
!    v        y component of velocity at all time levels (m/s)
!    w        Vertical component of Cartesian velocity
!             at all time levels (m/s)
!    wcont    Contravariant vertical velocity (m/s)
!    pt       Potential temperature at all time levels (K)
!    pres     Pressure at all time levels (Pascal)
!    qv       Water vapor specific humidity at all time levels
!             (kg/kg)
!    qc       Cloud water mixing ratio at all time levels (kg/kg)
!    qr       Rainwater mixing ratio at all time levels (kg/kg)
!    qi       Cloud ice mixing ratio at all time levels (kg/kg)
!    qs       Snow mixing ratio at all time levels (kg/kg)
!    qh       Hail mixing ratio at all time levels (kg/kg)
!
!    rhobar   Base state density
!
!
!  WORK ARRAYS:
!
!-----------------------------------------------------------------------
!
  USE model_precision

  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'          ! Grid & map parameters.
  INCLUDE 'bndry.inc'
  INCLUDE 'phycst.inc'
  INCLUDE 'mp.inc'            ! Message passing parameters.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!

  CHARACTER(LEN=256) :: file_name

  INTEGER :: nx,ny,nz          ! The number of grid points in 3
                               ! directions
  INTEGER :: nzsoil            ! Number of grid points in the -z direction

  REAL :: x     (nx)           ! x-coord. of the physical and compu-
                               ! tational grid. Defined at u-point.
  REAL :: y     (ny)           ! y-coord. of the physical and compu-
                               ! tational grid. Defined at v-point.
  REAL :: z     (nz)           ! z-coord. of the computational grid.
                               ! Defined at w-point on the staggered
                               ! grid.
  REAL :: zp    (nx,ny,nz)     ! Physical height coordinate defined at
                               ! w-point of the staggered grid.
  REAL :: zpsoil(nx,ny,nzsoil) ! The physical height coordinate defined
                               ! at the center of a soil layer(m).
  REAL :: hterain(nx,ny)       ! The height of the terrain.

  REAL :: mapfct(nx,ny,8)      ! Map factors at scalar, u and v points

  REAL :: xlat(nx,ny)          ! The latitude of grid
  REAL :: xlon(nx,ny)          ! The longitude of grid

  REAL :: j1    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! defined as - d( zp )/d( x ).
  REAL :: j2    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! defined as - d( zp )/d( y ).
  REAL :: j3    (nx,ny,nz)     ! Coordinate transformation Jacobian
                               ! defined as d( zp )/d( z ).

  REAL :: aj3x  (nx,ny,nz)     ! Coordinate transformation Jacobian defined
                               ! as d( zp )/d( z ) AVERAGED IN THE X-DIR.
  REAL :: aj3y  (nx,ny,nz)     ! Coordinate transformation Jacobian defined
                               ! as d( zp )/d( z ) AVERAGED IN THE Y-DIR.
  REAL :: aj3z  (nx,ny,nz)     ! Coordinate transformation Jacobian defined
                               ! as d( zp )/d( z ) AVERAGED IN THE Z-DIR.
  REAL :: j3inv (nx,ny,nz)     ! Inverse of j3

  REAL :: u     (nx,ny,nz) ! Total u-velocity (m/s)
  REAL :: v     (nx,ny,nz) ! Total v-velocity (m/s)
  REAL :: w     (nx,ny,nz) ! Total w-velocity (m/s)
  REAL :: wcont (nx,ny,nz) ! Contravariant vertical velocity (m/s)
  REAL :: pt    (nx,ny,nz) ! Perturbation potential temperature (K)
  REAL :: pres  (nx,ny,nz) ! Perturbation pressure (Pascal)
  REAL :: phprt (nx,ny,nz) ! Geopotential (m**2/s**2)

  REAL :: qv    (nx,ny,nz) ! Water vapor specific humidity (kg/kg)
  REAL :: qscalar(nx,ny,nz,nscalar)

  REAL :: qvbar    (nx,ny,nz)
  REAL :: phb      (nx,ny,nz)

  REAL :: rhobar(nx,ny,nz)     ! Base state density rhobar times j3.

  REAL :: prcrate(nx,ny,4)     ! precipitation rate (kg/(m**2*s))
                               ! prcrate(1,1,1) = total precipitation rate
                               ! prcrate(1,1,2) = grid scale precip. rate
                               ! prcrate(1,1,3) = cumulus precip. rate
                               ! prcrate(1,1,4) = microphysics precip. rate

  CHARACTER(LEN=40), INTENT(OUT) :: qnames_out(40)
  INTEGER, INTENT(OUT) :: nk

  LOGICAL, INTENT(IN)  :: use_arps_metrics

  REAL, INTENT(OUT) :: wmixr(nx,ny,nz)
  REAL, INTENT(OUT) :: psfc(nx,ny), varsfc(nx, ny, 4)
  REAL, INTENT(OUT) :: p_top, cf1, cf2, cf3
  REAL, INTENT(OUT) :: mu(nx,ny), mub(nx,ny)
  REAL, INTENT(OUT) :: dnw(nz), dn(nz), znw(nz), znu(nz)
  REAL, INTENT(OUT) :: fnm(nz), fnp(nz)

  REAL, INTENT(INOUT)  :: tem1(nx,ny,nz), tem2(nx,ny,nz), tem3(nx,ny,nz)
  REAL, INTENT(INOUT)  :: tem4(nx,ny,nz), tem5(nx,ny,nz)

  INTEGER, INTENT(OUT) :: istatus
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k, nq
  INTEGER :: iskip
  REAL :: temp

  REAL :: alatpro(2)
  REAL :: sclf
  REAL :: dxscl             ! Model x-direction grid spacing
                            ! normalized by the map scale
                            ! dxscl=dx/sclf
  REAL :: dyscl             ! Model y-direction grid spacing
                            ! normalized by the map scale
                            ! dyscl=dy/sclf
  REAL :: xs,ys, swx,swy, ctrx, ctry

  REAL :: rmin,rmax

  INTEGER :: nx_wrf, ny_wrf, nz_wrf
  INTEGER :: nxlg_wrf, nylg_wrf, nxd_wrf, nyd_wrf
  LOGICAL :: multifile, patch_split

  INTEGER :: ncmprx, ncmpry

  CHARACTER(LEN=256) :: message
  CHARACTER(LEN=19 ) :: datestr
  CHARACTER(LEN=40 ) :: qnames_wrf
  INTEGER            :: itime, io_form, numdigit

  INTEGER,  ALLOCATABLE :: fHndl(:,:)
  REAL(SP), ALLOCATABLE :: temd(:,:,:), var_wrf1d(:)
                           ! note that var_wrf1d is explicit single precision
  REAL(P),  ALLOCATABLE :: var_wrf(:,:,:), var_wrf2d(:,:)
  REAL(P),  ALLOCATABLE :: qv_wrf(:,:,:), ph_wrf(:,:,:), pt_wrf(:,:,:)
  REAL(P),  ALLOCATABLE :: pb_wrf(:,:,:), rb_wrf(:,:,:)
  REAL(P),  ALLOCATABLE :: u_wrf(:,:,:),  v_wrf(:,:,:)
  REAL(P),  ALLOCATABLE :: mub_wrf(:,:), mu_wrf(:,:), hgt_wrf(:,:)
  REAL(P),  ALLOCATABLE :: znu_wrf(:), rdnw_wrf(:)

  REAL(P),  ALLOCATABLE :: psnd(:), ptsnd(:), qvsnd(:), usnd(:), vsnd(:)

  REAL(P) :: zsoil(nzsoil)
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
  istatus = 0

!-----------------------------------------------------------------------
!
! This subroutine handles 6 possible readings
!
! 1. One WRF file in no-mpi mode (just read);                         - multifile = .FALSE., patch_split = .FALSE.
! 2. One WRF file in mpi mode (split);                                - multifile = .FALSE., patch_split = .TRUE.
!
! 3. Split WRF files in no-mpi mode (join);                           + multifile = .TRUE., patch_split = .FALSE.
! 4. Split WRF files with same number of processes (just read);       + multifile = .TRUE., patch_split = .FALSE.
! 5. Split WRF files with less number of processes (read and join);   + multifile = .TRUE., patch_split = .FALSE.
! 6. Split WRF files with more number of processes (read and split);  + multifile = .TRUE., patch_split = .TRUE.
!
!-----------------------------------------------------------------------

  IF (nproc_x_in == 1 .AND. nproc_y_in == 1) THEN
    multifile = .FALSE.
    ncmprx = 1
    ncmpry = 1
  ELSE
    multifile = .TRUE.
  END IF

  IF (nproc_x_in >= nproc_x .AND. nproc_y_in >= nproc_y) THEN
    patch_split = .FALSE.
    IF (mod(nproc_x_in,nproc_x) /= 0 .OR. mod(nproc_y_in,nproc_y) /= 0) THEN
      message = 'ERROR: nproc_x_in (nproc_y_in) must be a multiple of nproc_x (nproc_y).'
      istatus = -1
    ELSE
      ncmprx = nproc_x_in / nproc_x
      ncmpry = nproc_y_in / nproc_y
    END IF
  ELSE IF (nproc_x_in <= nproc_x .AND. nproc_y_in <= nproc_y) THEN
    patch_split = .TRUE.
    IF (mod(nproc_x,nproc_x_in) /= 0 .OR. mod(nproc_y,nproc_y_in) /= 0) THEN
      message = 'ERROR: nproc_x_in (nproc_y_in) must be a factor of nproc_x (nproc_y).'
      istatus = -2
    ELSE
      ncmprx = nproc_x / nproc_x_in
      ncmpry = nproc_y / nproc_y_in
    END IF
  ELSE
    message = 'ERROR: unsupported combination of nproc_x_in and nproc_x, or nproc_y_in and nproc_y.'
    istatus = -3
  END IF

  9999 CONTINUE

  IF (istatus /= 0) THEN
    WRITE(*,'(1x,a,I4)')       TRIM(message),istatus
    WRITE(*,'(7x,2(a,I4))') ' nproc_x_in = ',nproc_x_in, ', nproc_x = ', nproc_x
    WRITE(*,'(7x,2(a,I4))') ' nproc_y_in = ',nproc_y_in, ', nproc_y = ', nproc_y
    istatus = -1
    !RETURN
    CALL arpsstop('ERROR mpi parameters in initwrfgrd',1)
  END IF


!-----------------------------------------------------------------------
!
! Dimension maps
!
!-----------------------------------------------------------------------
  nx_wrf = nx-2       ! staggered dimensions
  ny_wrf = ny-2
  nz_wrf = nz-2

  nxlg_wrf = (nx_wrf-1)*nproc_x + 1     ! WRF domain indices
  nylg_wrf = (ny_wrf-1)*nproc_y + 1

  IF (multifile) THEN
    nxd_wrf = (nxlg_wrf-1) / nproc_x_in + 1
    nyd_wrf = (nylg_wrf-1) / nproc_y_in + 1
  ELSE
    nxd_wrf = nxlg_wrf    ! dataset size in the file, staggered
    nyd_wrf = nylg_wrf
  END IF

  IF (patch_split) THEN
    !
    ! It is required that (nxd_wrf-1) is dividable with ncmprx
    !
    IF (MOD(nxd_wrf-1,ncmprx) /= 0 .OR. MOD(nyd_wrf-1,ncmpry) /= 0) THEN
      istatus = -4
      message = 'ERROR: dimension sizes does not fit with the number of processes.'
      GOTO 9999
    END IF

  END IF

  ALLOCATE(fHndl(ncmprx,ncmpry), STAT = istatus)

  ALLOCATE(var_wrf   (nx_wrf,ny_wrf,nz_wrf), STAT = istatus)
  ALLOCATE(var_wrf2d (nx_wrf,ny_wrf),        STAT = istatus)
  ALLOCATE(var_wrf1d (nz_wrf),               STAT = istatus)

  ALLOCATE(temd (nxd_wrf,nyd_wrf,nz_wrf), STAT = istatus)

  ALLOCATE(ph_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus)
  ALLOCATE(qv_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus)
  ALLOCATE(pt_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus)
  ALLOCATE(pb_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus)
  ALLOCATE(rb_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus)
  ALLOCATE(u_wrf(nx_wrf,ny_wrf,nz_wrf),  STAT = istatus)
  ALLOCATE(v_wrf(nx_wrf,ny_wrf,nz_wrf),  STAT = istatus)

  ALLOCATE(mub_wrf (nx_wrf,ny_wrf),    STAT = istatus)
  ALLOCATE(mu_wrf  (nx_wrf,ny_wrf),    STAT = istatus)
  ALLOCATE(hgt_wrf (nx_wrf,ny_wrf),    STAT = istatus)
  ALLOCATE(znu_wrf (nz_wrf-1),         STAT = istatus)
  ALLOCATE(rdnw_wrf(nz_wrf-1),         STAT = istatus)

  io_form = 7   ! hard-coded for 7 at present
  numdigit = 4  ! hard-coded for 4 at present
  CALL open_wrf_files(file_name,io_form,patch_split,multifile,1,        &
                     ncmprx,ncmpry,numdigit,fHndl)

!-----------------------------------------------------------------------
! Initialize data array
!-----------------------------------------------------------------------

  ! variables used for analysis
  u  = 0.0
  v  = 0.0
  w  = 0.0
  pt = 0.0
  pres  = 0.0
  qv = 0.0

  qscalar = 0.0

  datestr = '2012-10-25_16:22:00'
  itime   = 1

!-----------------------------------------------------------------------
!
!  Horizontal wind
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'U','X',                                &
                  nx_wrf,ny_wrf,nz_wrf,u_wrf,                           &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'X',u_wrf,                   &
                      nx,ny,nz,u,tem5,istatus)

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'V','Y',                                &
                  nx_wrf,ny_wrf,nz_wrf,v_wrf,                           &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'Y',v_wrf,                   &
                      nx,ny,nz,v,tem5,istatus)

!-----------------------------------------------------------------------
!
!  Vertical wind
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'W','Z',                                &
                  nx_wrf,ny_wrf,nz_wrf,var_wrf,                         &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'Z',var_wrf,                 &
                      nx,ny,nz,w,tem5,istatus)


!-----------------------------------------------------------------------
!
! Read potential temperature
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'T','0',                                &
                  nx_wrf,ny_wrf,nz_wrf,pt_wrf,                          &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

!-----------------------------------------------------------------------
!
! Pressure
!
!-----------------------------------------------------------------------

   !CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
   !                datestr,itime,'P','0',                                &
   !                nx_wrf,ny_wrf,nz_wrf,var_wrf,                         &
   !                nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

   CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                   datestr,itime,'PB','0',                               &
                   nx_wrf,ny_wrf,nz_wrf,pb_wrf,                          &
                   nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

!-----------------------------------------------------------------------
!
! Physical height (save for setting zp, Perturbation only here)
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'PH','Z',                               &
                  nx_wrf,ny_wrf,nz_wrf,ph_wrf,                          &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'V',ph_wrf,                  &
                      nx,ny,nz,phprt,tem5,istatus)  ! phprt - original perturbation of geopotential

!-----------------------------------------------------------------------
!
! Read water vapor mixing ratio
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'QVAPOR','0',                           &
                  nx_wrf,ny_wrf,nz_wrf,qv_wrf,                          &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',qv_wrf,                  &
                      nx,ny,nz,wmixr,tem5,istatus)

!-----------------------------------------------------------------------
!
! Vertical coordinates variables
!
!-----------------------------------------------------------------------

  IF (.NOT. use_arps_metrics) THEN

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'MAPFAC_M','0',                         &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'0',var_wrf2d,                   &
                         nx,ny,mapfct(:,:,1),tem5,istatus)

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'MAPFAC_U','X',                         &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'X',var_wrf2d,                   &
                         nx,ny,mapfct(:,:,2),tem5,istatus)

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'MAPFAC_V','Y',                         &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'Y',var_wrf2d,                   &
                         nx,ny,mapfct(:,:,3),tem5,istatus)

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'MAPFAC_MX','0',                        &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'0',var_wrf2d,                   &
                         nx,ny,mapfct(:,:,4),tem5,istatus)

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'MAPFAC_MY','0',                        &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'0',var_wrf2d,                   &
                         nx,ny,mapfct(:,:,5),tem5,istatus)

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'MAPFAC_UY','X',                        &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'X',var_wrf2d,                   &
                         nx,ny,mapfct(:,:,6),tem5,istatus)

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'MAPFAC_VX','Y',                        &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'Y',var_wrf2d,                   &
                         nx,ny,mapfct(:,:,7),tem5,istatus)

  END IF  ! mapfct
  !
  ! mub_wrf should be kept from now on
  !
  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'MUB','0',                              &
                  nx_wrf,ny_wrf,mub_wrf,                                &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'0',mub_wrf,                     &
                         nx,ny,mub,tem5,istatus)

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'MU','0',                               &
                  nx_wrf,ny_wrf,mu_wrf,                                 &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'0',mu_wrf,                      &
                         nx,ny,mu,tem5,istatus)

  CALL get_wrf_1d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'ZNU',                                  &
                  nz_wrf,var_wrf1d,nz_wrf-1,istatus)
  DO k = 1, nz_wrf-1
    znu_wrf(k) = var_wrf1d(k)
    znu(k+1)   = var_wrf1d(k)
  END DO

  CALL get_wrf_1d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'ZNW',                                  &
                  nz_wrf,var_wrf1d,nz_wrf,istatus)
  DO k = 1, nz_wrf
    znw(k+1) = var_wrf1d(k)
  END DO

  CALL get_wrf_1d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'DNW',                                  &
                  nz_wrf,var_wrf1d,nz_wrf-1,istatus)
  DO k = 1, nz_wrf-1
    dnw(k+1) = var_wrf1d(k)
  END DO

  CALL get_wrf_1d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'DN',                                   &
                  nz_wrf,var_wrf1d,nz_wrf-1,istatus)
  DO k = 1, nz_wrf-1
    dn(k+1) = var_wrf1d(k)
  END DO

  CALL get_wrf_1d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'FNM',                                  &
                  nz_wrf,var_wrf1d,nz_wrf-1,istatus)
  DO k = 1, nz_wrf-1
    fnm(k+1) = var_wrf1d(k)
  END DO

  CALL get_wrf_1d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'FNP',                                  &
                  nz_wrf,var_wrf1d,nz_wrf-1,istatus)
  DO k = 1, nz_wrf-1
    fnp(k+1) = var_wrf1d(k)
  END DO

  CALL get_wrf_1d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'RDNW',                                 &
                  nz_wrf,var_wrf1d,nz_wrf-1,istatus)
  DO k = 1, nz_wrf-1
    rdnw_wrf(k) = var_wrf1d(k)
  END DO

  CALL get_wrf_s(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,     &
                  datestr,itime,'P_TOP',p_top,istatus)

  CALL get_wrf_s(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,     &
                  datestr,itime,'CF1',cf1,istatus)
  CALL get_wrf_s(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,     &
                  datestr,itime,'CF2',cf2,istatus)
  CALL get_wrf_s(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,     &
                  datestr,itime,'CF3',cf3,istatus)

!-----------------------------------------------------------------------
!
! Soil layers depth
!
!-----------------------------------------------------------------------

  CALL get_wrf_1d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'ZS',                                   &
                  nzsoil,var_wrf1d,nzsoil,istatus) ! nzsoil_wrf = nzsoil
  DO k = 1, nzsoil
    zsoil(k) = var_wrf1d(k)
  END DO

!-----------------------------------------------------------------------
!
! !!!! transform ph to pbar and pprt !!!!!
! Calculate rhobar
!
!-----------------------------------------------------------------------

  CALL cal_rho_p(nx_wrf,ny_wrf,nz_wrf,mub_wrf,mu_wrf,rdnw_wrf,          &
                 p_top,znu_wrf,qv_wrf,ph_wrf,pt_wrf,                    &
                 rb_wrf,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',rb_wrf,                  &
                      nx,ny,nz,rhobar,tem5,istatus)

!-----------------------------------------------------------------------
!
! Get the total atmospheric states in the WRF grid
!
!-----------------------------------------------------------------------

  ! Potential temperature
  pt_wrf = pt_wrf + 300.0

  ! Convert QV to specific humidity
  DO k = 1, nz_wrf-1
    DO j = 1, ny_wrf-1
      DO i = 1, nx_wrf-1
        qv_wrf(i,j,k) = qv_wrf(i,j,k)/(1+qv_wrf(i,j,k))
      END DO
    END DO
  END DO
  !WHERE(qv_wrf < 0.0) qv_wrf = 0.0
  !WHERE(qv_wrf > 1.0) qv_wrf = 1.0

  !qvbar   = 0.0

  ! Total Geopotential Height
  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'PHB','Z',                              &
                  nx_wrf,ny_wrf,nz_wrf,var_wrf,                         &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'Z',var_wrf,                 &
                      nx,ny,nz,phb,tem5,istatus)         ! phb original geopotential base

  ph_wrf(:,:,:) = (var_wrf(:,:,:) + ph_wrf(:,:,:)) / g   ! save total physical height for later use

  ! Total WRF pressure
  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                   datestr,itime,'P','0',                               &
                   nx_wrf,ny_wrf,nz_wrf,var_wrf,                        &
                   nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  pb_wrf(:,:,:) = pb_wrf(:,:,:) + var_wrf(:,:,:)

!-----------------------------------------------------------------------
!
! Get base arrays based on total variables
!
! pb_wrf is total pressure now
!
!-----------------------------------------------------------------------

  ALLOCATE(psnd(0:nz-1),  STAT = istatus)
  ALLOCATE(ptsnd(0:nz-1), STAT = istatus)
  ALLOCATE(qvsnd(0:nz-1), STAT = istatus)
  ALLOCATE(usnd(0:nz-1),  STAT = istatus)
  ALLOCATE(vsnd(0:nz-1),  STAT = istatus)

  CALL extwrfsnd(nx_wrf,ny_wrf,nz_wrf,ph_wrf,pb_wrf,pt_wrf,qv_wrf,      &
                 u_wrf,v_wrf,psnd,ptsnd,qvsnd,usnd,vsnd, istatus )

  DO k= 1, nz
    DO j = 1, ny
      DO i = 1, nx
        qvbar(i,j,k) = qvsnd(k-1)
      END DO
    END DO
  END DO

  DEALLOCATE( psnd, ptsnd, qvsnd, usnd, vsnd )

!-----------------------------------------------------------------------
!
! Get the final ARPS arrays
!
!-----------------------------------------------------------------------

   CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',qv_wrf,                 &
                       nx,ny,nz,qv,tem5,istatus)

   CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',pb_wrf,                 &
                       nx,ny,nz,pres,temd,istatus)

   CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',pt_wrf,                 &
                      nx,ny,nz,pt,tem5,istatus)

!-----------------------------------------------------------------------
!
! hydrometeors
!
!-----------------------------------------------------------------------

  nk = 0
  DO nq = 1, nscalar

    IF ( ANY ( qnames=="QCLOUD" ) ) THEN
       qnames_wrf = qnames(nq)
    ELSE
      SELECT CASE ( TRIM(qnames(nq)) )
      CASE ('qc')
        qnames_wrf = 'QCLOUD'
      CASE ('qr')
        qnames_wrf = 'QRAIN'
      CASE ('qi')
        qnames_wrf = 'QICE'
      CASE ('qs')
        qnames_wrf = 'QSNOW'
      CASE ('qg')
        qnames_wrf = 'QGRAUP'
      CASE ('qh')
        qnames_wrf = 'QHAIL'
        !qnames_wrf = 'QGRAUP'
      CASE DEFAULT
        WRITE(*,'(1x,3a)') 'ERROR: support of ',TRIM(qnames(nq)),' is still not added.'
        CONTINUE
      END SELECT
    END IF

    CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,  &
                    datestr,itime,qnames_wrf,'0',                       &
                    nx_wrf,ny_wrf,nz_wrf,var_wrf,                       &
                    nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

    WHERE(var_wrf < 1.0E-30 ) var_wrf = 0.0  ! sometimes get very small values for examples 6.2872527E-37 from WRF
                                             ! which may cause float overflow when the values appears as a denominator

    IF (istatus == 0) THEN
      IF (myproc == 0) WRITE(*,'(2x,51x,a,I2,a,a10,a)')                 &
        'nq = ',nq,' -> <',qnames(nq),'>.'

      nk = nk + 1
      !IF (nq < nqscalar) THEN
      !  WHERE(var_wrf < 0.0) var_wrf = 0.0
      !  WHERE(var_wrf > 1.0) var_wrf = 1.0
      !END IF

      CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',var_wrf,             &
                          nx,ny,nz,qscalar(:,:,:,nq),tem5,istatus)

      qnames_out(nk) = qnames_wrf

    END IF
  END DO

!-----------------------------------------------------------------------
! Set WRF terrain heights
!-----------------------------------------------------------------------

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'HGT','0',                              &
                  nx_wrf,ny_wrf,hgt_wrf,                                &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'0',hgt_wrf,                     &
                         nx,ny,hterain,tem5,istatus)

!-----------------------------------------------------------------------
! Set model grid lat/lon
!-----------------------------------------------------------------------

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'XLAT','0',                             &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'0',var_wrf2d,                   &
                         nx,ny,xlat,tem5,istatus)

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'XLONG','0',                             &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'0',var_wrf2d,                   &
                         nx,ny,xlon,tem5,istatus)

!-----------------------------------------------------------------------
! Get surface pressure
!-----------------------------------------------------------------------

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'PSFC','0',                             &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'0',var_wrf2d,                   &
                         nx,ny,psfc,tem5,istatus)

!-----------------------------------------------------------------------
! Get surface temperature
!-----------------------------------------------------------------------

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'T2','0',                               &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'0',var_wrf2d,                   &
                         nx,ny,varsfc(:,:,1),tem5,istatus)

!-----------------------------------------------------------------------
! Get surface water vapor mixing ratio
!-----------------------------------------------------------------------

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'Q2','0',                               &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'0',var_wrf2d,                   &
                         nx,ny,varsfc(:,:,2),tem5,istatus)

!-----------------------------------------------------------------------
! Get surface U-wind
!-----------------------------------------------------------------------

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'U10','0',                              &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'0',var_wrf2d,                   &
                         nx,ny,varsfc(:,:,3),tem5,istatus)

!-----------------------------------------------------------------------
! Get surface V-wind
!-----------------------------------------------------------------------

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'V10','0',                              &
                  nx_wrf,ny_wrf,var_wrf2d,                              &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL array_wrf2arps_2d(nx_wrf,ny_wrf,'0',var_wrf2d,                   &
                         nx,ny,varsfc(:,:,4),tem5,istatus)

!-----------------------------------------------------------------------
! Finish reading
!-----------------------------------------------------------------------

  CALL close_wrf_files(fHndl,io_form,patch_split,1,ncmprx,ncmpry)

!-----------------------------------------------------------------------
!
! Initilize the grid metrics for divergence computation
!
!-----------------------------------------------------------------------

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'V',ph_wrf,                  &
                      nx,ny,nz,zp,tem5,istatus)

  IF (.NOT. use_arps_metrics) THEN

    CALL inigrdwrf(nx,ny,nz,nzsoil,x,y,z,zsoil,zpsoil,hterain,istatus)

    !USEWRF
    !CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'V',ph_wrf,                &
    !                    nx,ny,nz,zp,tem5,istatus)
    !USEWRF

    CALL compute_diff_metrics( nx,ny,nz, dxinv, dyinv, zp,              &
                             j1,j2,j3,j3inv,tem1,istatus )

  ELSE

    ! USEARPS
    CALL inigrdarps(nx,ny,nz,nzsoil,x,y,z,zp,zpsoil,                    &
                hterain,mapfct,j1,j2,j3,tem1,                           &
                tem2,tem3,tem4,tem5)

    !CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'V',ph_wrf,                &
    !                    nx,ny,nz,zp,tem5,istatus)
    !USEARPS
!-----------------------------------------------------------------------
!
! Initilize the reverse of Jacobian arrays
!
!-----------------------------------------------------------------------

    ebc = 4
    wbc = 4
    nbc = 4
    sbc = 4

    !USEARPS
    DO k=1,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          j3inv(i,j,k)=1.0/j3(i,j,k)
        END DO
      END DO
    END DO

    !DO k=1,nzsoil
    !  DO j=1,ny-1
    !    DO i=1,nx-1
    !      j3soilinv(i,j,k)=1.0/j3soil(i,j,k)
    !    END DO
    !  END DO
    !END DO

    DO k=1,nz-1
      DO j=1,ny-1
        DO i=2,nx-1
          aj3x(i,j,k)=0.5*(j3(i,j,k)+j3(i-1,j,k))
        END DO
      END DO
    END DO

    IF (mp_opt > 0) THEN
      CALL acct_interrupt(mp_acct)
      CALL mpsendrecv2dew(aj3x,nx,ny,nz,ebc,wbc,1,tem2)
      !CALL mpsend2dns(aj3x,nx,ny,nz,1,mptag,tem2)
      !CALL mprecv2dns(aj3x,nx,ny,nz,1,mptag,tem2)
    END IF
    CALL acct_interrupt(bc_acct)
    CALL bcsu(nx,ny,nz,1,ny-1,1,nz-1,ebc,wbc,aj3x)
    CALL acct_stop_inter

    DO k=1,nz-1
      DO j=2,ny-1
        DO i=1,nx-1
          aj3y(i,j,k)=0.5*(j3(i,j,k)+j3(i,j-1,k))
        END DO
      END DO
    END DO

    IF (mp_opt > 0) THEN
      CALL acct_interrupt(mp_acct)
      !CALL mpsend2dew(aj3y,nx,ny,nz,2,mptag,tem2)
      !CALL mprecv2dew(aj3y,nx,ny,nz,2,mptag,tem2)
      CALL mpsendrecv2dns(aj3y,nx,ny,nz,nbc,sbc,2,tem2)
    END IF
    CALL acct_interrupt(bc_acct)
    CALL bcsv(nx,ny,nz,1,nx-1,1,nz-1,nbc,sbc,aj3y)
    CALL acct_stop_inter

    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          aj3z(i,j,k)=0.5*(j3(i,j,k)+j3(i,j,k-1))
        END DO
      END DO
    END DO

    CALL acct_interrupt(bc_acct)
    CALL bcsw(nx,ny,nz,1,nx-1,1,ny-1,tbc,bbc,aj3z)
    CALL acct_stop_inter

    !USEARPS

  END IF

!-----------------------------------------------------------------------
!
! Clear working arrays
!
!-----------------------------------------------------------------------

  DEALLOCATE ( fHndl )
  DEALLOCATE ( ph_wrf, qv_wrf, pt_wrf, pb_wrf, rb_wrf, u_wrf, v_wrf )
  DEALLOCATE ( mub_wrf, mu_wrf, hgt_wrf, znu_wrf, rdnw_wrf )
  DEALLOCATE ( var_wrf, var_wrf2d, var_wrf1d )
  DEALLOCATE ( temd )

  RETURN
END SUBROUTINE initwrfgrd

!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE dtadumpwrfinput            ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE dtadumpwrf(infilename,outdir,filename,refflag,nx,ny,nz,zp,   &
                      u, v, w, pt, mu, ph, qv, qscalar,                 &
                      phb, psfc,ref3d,tem1, istatus)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Write the analyzed variables to wrfinput_d01.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!    8/30/2013.
!
!  MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!    nx,ny,nz The dimension of data arrays
!
!  DATA ARRAYS READ IN:
!
!    zp       z coordinate of grid points in physical space (m)
!
!    u        x component of velocity (m/s)
!    v        y component of velocity (m/s)
!    w        vertical component of velocity
!             in Cartesian coordinates (m/s).
!
!    pt       potential temperature (K)
!    pp       pressure (Pascal)
!    qv       water vapor mixing ratio (kg/kg)
!
!    qscalar
!    qc       Cloud water mixing ratio (kg/kg)
!    qr       Rainwater mixing ratio (kg/kg)
!    qi       Cloud ice mixing ratio (kg/kg)
!    qs       Snow mixing ratio (kg/kg)
!    qh       Hail mixing ratio (kg/kg)
!
!    istatus  Return status indicator
!
!-----------------------------------------------------------------------

  USE wrfio_api

  IMPLICIT NONE

!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  !INCLUDE 'indtflg.inc'
  INCLUDE 'globcst.inc'
  !INCLUDE 'grid.inc'          ! Grid & map parameters.
  INCLUDE 'mp.inc'            ! mpi parameters.
  INCLUDE 'phycst.inc'

!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
  INTEGER :: nzsoil            ! Number of grid points in the soil

  CHARACTER(LEN=256) :: infilename
  CHARACTER(LEN=256) :: filename, outdir

  INTEGER, INTENT(IN) :: refflag

  REAL :: zp    (nx,ny,nz)     ! Physical height coordinate defined at
                               ! w-point of the staggered grid(m).

  REAL :: u (nx,ny,nz)      ! x component of velocity (m/s)
  REAL :: v (nx,ny,nz)      ! y component of velocity (m/s)
  REAL :: w (nx,ny,nz)      ! vertical component of velocity in Cartesian coordinates (m/s).
  REAL :: pt(nx,ny,nz)      ! potential temperature (K)
! REAL :: pp(nx,ny,nz)      ! pressure (Pascal)
  REAL :: mu(nx,ny)         ! mass of dry air (Pascal)
  REAL :: ph(nx,ny,nz)      ! geopotential (m**2/s**2)
  REAL :: qv(nx,ny,nz)      ! Mixing ratio
  REAL :: phb(nx,ny,nz)
  REAL :: psfc(nx,ny)
  REAL :: ref3d(nx,ny,nz)

  REAL :: qscalar(nx,ny,nz,nscalar)

  REAL :: tem1(nx,ny,nz)

  INTEGER :: istatus           ! Return status indicator

!-----------------------------------------------------------------------

  INTEGER :: nx_wrf, ny_wrf, nz_wrf, nq

  CHARACTER(LEN=40) :: qnames_wrf
  CHARACTER(LEN=19) :: timestr

  REAL, ALLOCATABLE :: var_wrf(:,:,:)

  !CHARACTER(LEN=256) :: infilename, indir
  LOGICAL :: hard_copy = .FALSE.
  CHARACTER(LEN=80),  PARAMETER :: wrftitle = 'OUTPUT FROM CSM3DVAR for WRFV3.7.1'

  INTEGER :: wrfoutopt
  INTEGER :: i,j

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  !indir = './'
  !infilename = 'wrfinput_d01'

  IF (hard_copy) THEN
    IF (myproc == 0) THEN
      CALL wrf_file_hard_copy( 1,1,4,                                   &
                               infilename,outdir,filename,istatus )
    END IF
    CALL mpbarrier
  END IF

!-----------------------------------------------------------------------
!
! First, set parameters for house keeping
!
!-----------------------------------------------------------------------

  WRITE(timestr,'(I4.4,5(a,I2.2))') year,'-',month,'-',day,'_',hour, ':', minute, ':', second

  nx_wrf = nx-2
  ny_wrf = ny-2
  nz_wrf = nz-2

  ALLOCATE( var_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus )

  IF (hdmpfmt == 0 .OR. hdmpfmt == 100 .OR. hdmpfmt == 1) THEN
    wrfoutopt = hdmpfmt
  ELSE
    wrfoutopt = 1
  END IF

  CALL wrf_io_init( wrfoutopt,nx_wrf,ny_wrf,nz_wrf,hard_copy,4,         &
                    outdir,filename,istatus )

!-----------------------------------------------------------------------
!
! 1. Copy existing fields to WRF input file, exluding the analyzed fields
!
!-----------------------------------------------------------------------

  CALL wrf_file_define( infilename, 4, wrftitle, timestr, 1, istatus )
  ! define wrfinput file and copy static fields

!-----------------------------------------------------------------------
!
! 2. Write out analyzed fields
!
!-----------------------------------------------------------------------

  ! analysis variables

  CALL array_arps2wrf(nx,ny,nz,u,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'U', var_wrf, 'X', istatus )

  CALL array_arps2wrf(nx,ny,nz,v,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'V', var_wrf, 'Y', istatus )

  CALL array_arps2wrf(nx,ny,nz,w,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'W', var_wrf, 'Z', istatus )

! CALL array_arps2wrf(nx,ny,nz,pp,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
! CALL wrf_file_write3d ( 'P', var_wrf, ' ', istatus )
! tem1(:,:,:) = zp(:,:,:)*g-phb(:,:,:)
! CALL array_arps2wrf(nx,ny,nz,tem1,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)

  CALL array_arps2wrf_2d(nx,ny,mu,nx_wrf,ny_wrf,var_wrf,istatus)
  CALL wrf_file_write2d( 'MU',var_wrf(:,:,1), '0', istatus )

  CALL array_arps2wrf(nx,ny,nz,ph,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'PH',var_wrf, 'Z', istatus )

  CALL array_arps2wrf(nx,ny,nz,pt,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  var_wrf = var_wrf-300.0
  CALL wrf_file_write3d ( 'T',    var_wrf, ' ', istatus )

  CALL array_arps2wrf(nx,ny,nz,qv,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  var_wrf(:,:,:) = var_wrf(:,:,:)/(1-var_wrf(:,:,:))
  CALL wrf_file_write3d ( 'QVAPOR',var_wrf, ' ', istatus )

  ! microphysical variables (TOBEDONE)
  DO nq = 1, nscalar

    IF ( ANY ( qnames=="QCLOUD" ) ) THEN
       qnames_wrf = qnames(nq)
    ELSE
      SELECT CASE ( TRIM(qnames(nq)) )
      CASE ('qc')
        qnames_wrf = 'QCLOUD'
      CASE ('qr')
        qnames_wrf = 'QRAIN'
      CASE ('qi')
        qnames_wrf = 'QICE'
      CASE ('qs')
        qnames_wrf = 'QSNOW'
      CASE ('qg')
        qnames_wrf = 'QGRAUP'
      CASE ('qh')
        qnames_wrf = 'QHAIL'
        !qnames_wrf = 'QGRAUP'
      CASE DEFAULT
        WRITE(*,'(1x,3a)') 'ERROR: support of ',TRIM(qnames(nq)),' is still not added.'
        CONTINUE
      END SELECT
    END IF

    CALL array_arps2wrf(nx,ny,nz,qscalar(:,:,:,nq),nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
    CALL wrf_file_write3d ( qnames_wrf, var_wrf, ' ', istatus )

  END DO

  CALL array_arps2wrf_2d(nx,ny,psfc,nx_wrf,ny_wrf,var_wrf,istatus)
  CALL wrf_file_write2d( 'PSFC',var_wrf(:,:,1), '0', istatus )

!-----------------------------------------------------------------------
!
! 2. Write out additional fields
!
!-----------------------------------------------------------------------

  IF (.NOT. hard_copy .AND. refflag >= 1) THEN
    CALL array_arps2wrf(nx,ny,nz,ref3d,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
    CALL wrf_file_write3d ( 'REFMOSAIC3D', var_wrf, ' ', istatus )
  END IF

!-----------------------------------------------------------------------
!
! 3. Close opened files
!
!-----------------------------------------------------------------------

  CALL wrf_io_final( istatus )

  DEALLOCATE( var_wrf )

  IF (readyfl > 0) CALL wrf_file_ready(outdir,filename,readyfl,istatus)

  RETURN
END SUBROUTINE dtadumpwrf
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DIAG_PH_MU                 ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE diag_ph_mu(nx,ny,nz,pxb,pxa,ptxa,qvxb,qvxa,                  &
                      wmixr, phb, p_top, psfc,                          &
                      mub, hgt, dnw, znw, znu,                          &
                      mu,phprt)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Diagnose perturbation geopotential and perturbation dry-air mass
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Sijie Pan
!  08/05/2013.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!

  CHARACTER(LEN=256) :: file_name

  INTEGER :: nx,ny,nz

  REAL, DIMENSION(nx,ny,nz), INTENT(IN)     ::  pxb,pxa,ptxa,qvxb,qvxa
  REAL, DIMENSION(nx,ny,nz), INTENT(IN)     ::  wmixr, phb
  REAL, DIMENSION(nx,ny),    INTENT(IN)     ::  hgt, dnw, znw, znu, mub
  REAL,                      INTENT(IN)     ::  p_top
  REAL, DIMENSION(nx,ny),    INTENT(IN)     ::  psfc
  REAL, DIMENSION(nx,ny,nz), INTENT(OUT)    ::  phprt
  REAL, DIMENSION(nx,ny),    INTENT(INOUT)  ::  mu

  INTEGER :: istatus

!-----------------------------------------------------------------------

  INTEGER :: i,j,k
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
  istatus = 0

  CALL cal_ph_mu(nx,ny,nz,phb,mub,znu,znw,dnw,hgt,p_top, psfc,          &
                 pxb,pxa,ptxa,qvxb,qvxa,                                &
                 wmixr,mu,phprt)


  RETURN
END SUBROUTINE diag_ph_mu
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE cal_rho_p                  ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE cal_rho_p(nx,ny,nz,mub,mu,rdnw,p_top,znu,qv,ph,pt,rhobar,istatus)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate air density and pressure from WRF variables for ARPSEnKF
!  The formulas are referred to WRF3DVAR da_transfer_wrftoxb Y.R. Guo(2004)
!-----------------------------------------------------------------------
!
!  AUTHOR: Shizhang Wang
!    3/1/2011.
!
!  MODIFICATION:
!
!  10/26/2012 (Y. Wang)
!  Rewrote for clarity and efficiency. Note that here nx,ny,nz are WRF
!  dimension sizes.
!
!-----------------------------------------------------------------------
  IMPLICIT NONE
  ! input varibles
  integer , INTENT(IN) ::  nx,ny,nz
  real,     INTENT(IN) ::  mub(nx,ny),mu(nx,ny)
  REAL,     INTENT(IN) ::  qv(nx,ny,nz)
  REAL,     INTENT(IN) ::  ph(nx,ny,nz),pt(nx,ny,nz)  ! should be perturbations
  REAL,     INTENT(IN) ::  rdnw(nz-1),znu(nz-1)
  real,     INTENT(IN) ::  p_top

  ! output varibles
  !real,     INTENT(OUT) ::  pp(nx,ny,nz)     ! total pressure
  !real,     INTENT(OUT) ::  pbar(nx,ny,nz)

  real,     INTENT(OUT) :: rhobar(nx,ny,nz)
  INTEGER,  INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
  ! local varibles
  REAL, PARAMETER :: pi = 3.1415926535897932346
  REAL, PARAMETER :: gas_constant = 287.     ! Value used in WRF.
  REAL, PARAMETER :: gas_constant_v = 461.6  ! Value used in WRF.
  REAL, PARAMETER :: cp = 7.*gas_constant/2. ! Value used in WRF.
  REAL, PARAMETER :: t_kelvin = 273.15
  REAL, PARAMETER :: kappa = gas_constant / cp
  REAL, PARAMETER :: rd_over_rv = gas_constant / gas_constant_v
  REAL, PARAMETER :: gravity = 9.81        ! m/s - value used in MM5.

!-----------------------------------------------------------------------

  real    :: albn,aln,ppb,ttb,qvf1
  real    :: cvpm,cpovcv,ps0,ts0,tis0,tlp
  real    :: iso_temp, tlp_strat, ps_strat
  real    :: rho, p
  real    :: temp

  INTEGER :: i,j,k

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  ps0  = 100000.0    ! Base sea level pressure
  ts0  = 300.0       ! Base potential temperature for all levels.
  tis0 = 290.0       ! Base sea level temperature
  tlp  = 50.0        ! temperature difference from 1000mb to 300mb

  iso_temp = 200.0   ! isothermal temperature in statosphere
  tlp_strat = -11.0  ! base state lapse rate (dT/d(lnP)) in stratosphere
  ps_strat = 55      ! base state pressure (Pa) at bottom of the stratosphere, US Standard atmosphere 55 hPa.

  cvpm =  - (1. - gas_constant/cp)
  cpovcv = cp / (cp - gas_constant)

  DO k = 1, nz-1
    DO j = 1, ny-1
      DO i = 1, nx-1
        ppb  = znu(k) * mub(i,j) + p_top
        temp = MAX( iso_temp, tis0+tlp*log(ppb/ps0) )
        IF ( ppb < ps_strat ) then
          temp = iso_temp + tlp_strat*log(ppb/ps_strat)
        END IF
        ttb  = temp * (ps0/ppb)**kappa
        albn = (gas_constant/ps0) * ttb * (ppb/ps0)**cvpm

        qvf1 = 1. + qv(i,j,k) / rd_over_rv
        aln  = -1. / (mub(i,j)+mu(i,j)) * ( albn*mu(i,j) + rdnw(k) *(ph(i,j,k+1) - ph(i,j,k)) )

        !  total pressure and density
        !WYH p = ps0 * ( (gas_constant*(ts0+pt(i,j,k))*qvf1) / &
        !WYH             (ps0*(aln+albn)) )**cpovcv
        rho = 1.0 / (albn+aln)
        !  perturbation pressure
        !WYH pp(i,j,k) = p  ! -ppb   ! we need total pressure
        !  base-state pressure
        !WYH pbar(i,j,k) = ppb
        !  base-state density
        !WTH rhobar(i,j,k) = 1.0 / albn
        rhobar(i,j,k) = rho
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE cal_rho_p

!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE cal_ph_mu                  ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE cal_ph_mu(nx,ny,nz,phb,mub,                                  &
                     znu,znw,dnw,hgt,p_top,psfc,                        &
                     pxb,pxa,ptxa,qvxb,qvxa,                            &
                     moist,mu,phprt)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Calculate geopotential and mass of dry air from analysis variables
!  The formulas are referred to WRF3DVAR da_transfer_xatowrf Syed RH Rizvi(2008)
!  and Formulation Document by Y.R. Guo(2004)
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Sijie Pan
!    8/25/2015.
!
!-----------------------------------------------------------------------
  IMPLICIT NONE
  ! input variables
  INTEGER , INTENT(IN)                     ::  nx,ny,nz
  REAL, DIMENSION(nx,ny,nz), INTENT(IN)    ::  pxb,pxa,ptxa,qvxb,       &
                                               phb,moist,qvxa
  REAL, DIMENSION(nx,ny),    INTENT(IN)    ::  hgt,mub, psfc
  REAL, DIMENSION(nz),       INTENT(IN)    ::  znu,znw,dnw
  REAL, INTENT(IN)                         ::  p_top
  ! output variables
  REAL, INTENT(INOUT)                      ::  mu(nx,ny)
  REAL, INTENT(OUT)                        ::  phprt(nx,ny,nz)

!-----------------------------------------------------------------------
  ! local varibles
  REAL, PARAMETER :: pi = 3.1415926535897932346
  REAL, PARAMETER :: gas_constant = 287.     ! Value used in WRF.
  REAL, PARAMETER :: gas_constant_v = 461.6  ! Value used in WRF.
  REAL, PARAMETER :: cp = 7.*gas_constant/2. ! Value used in WRF.
  REAL, PARAMETER :: t_kelvin = 273.15
  REAL, PARAMETER :: kappa = gas_constant / cp
  REAL, PARAMETER :: rd_over_rv = gas_constant / gas_constant_v
  REAL, PARAMETER :: gravity = 9.81        ! m/s - value used in MM5.
  REAL, PARAMETER :: ps0 = 100000.

!-----------------------------------------------------------------------

  REAL, DIMENSION(nx,ny)                        ::  mumid
! REAL, DIMENSION(nx,ny,nz)                     ::  phb, mub
  REAL, DIMENSION(nx,ny,nz)                     ::  qvtmp,pinc
  REAL                                          ::  tmp1,tmp2,cvpm,     &
                                                    thetam,pfu,pfd,     &
                                                    phm
  REAL :: mu_full, p_full, q_full
  REAL, DIMENSION(nz)                           ::  ald, ph

!-----------------------------------------------------------------------
 !test
  REAL, DIMENSION(nz)                           ::  aldt
  REAL                                          ::  thetamt

!-----------------------------------------------------------------------

  INTEGER               :: i, j, k

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  mumid=0.0
  qvtmp=0.0

  ! [Step 1. Calculate increments of mixing ratio]
  DO k=1,nz-1
    DO j=1,ny-1
      DO i=1,nx-1
        ! convert to increments of mixing ratio from increments of specific humidity
        ! dqv=dsh*(dqv/dsh)=dsh*(d(sh/1-sh)/dsh)=dsh/(1-sh)**2
        ! increments of specific humidity (sh) calculated by qvxa-qvxb, if xa<0 set it equals 0
        IF (qvxa(i,j,k) < 0.0) THEN
          qvtmp(i,j,k) =  (0.0-qvxb(i,j,k))/(1. - qvxb(i,j,k))**2
        ELSE
          qvtmp(i,j,k) =  (qvxa(i,j,k)-qvxb(i,j,k))/(1. - qvxb(i,j,k))**2
        END IF
      END DO
    END DO
  END DO

  ! [Step 2. Calculate increments of dry-column air mass per unit area]
  DO j=1,ny-1
    DO i=1,nx-1
      tmp1=0.0
      tmp2=0.0
      DO k=2,nz-2
        tmp1=tmp1+qvtmp(i,j,k)*dnw(k)
        tmp2=tmp2+(1.0+moist(i,j,k))*dnw(k)
      END DO

      mumid(i,j)=-(pxa(i,j,2)-pxb(i,j,2)+(mub(i,j)+mu(i,j))*tmp1)/tmp2

    END DO
  END DO

  ! [Step 3. Calculate geopotential height]
  cvpm =  - (1. - gas_constant/cp)

  DO j=1,ny-1
    DO i=1,nx-1
      mu(i,j) = mu(i,j)+mumid(i,j)

      ! total dry air density
      mu_full = mub(i,j)+mu(i,j)  !+mumid(i,j)
      ph      = 0.0
      ! compute geopotential (using dry inverse density and dry pressure)
      ph(2)   = hgt(i,j) * gravity
      DO k = 2, nz-2     ! To work on WRF grid, i.e. ARPS physical grid
        p_full = pxa(i,j,k)
        q_full = moist(i,j,k)+qvtmp(i,j,k)
        ! Compute virtual potential temperature
        ! We use potential temperature as a control varialble.
        ! Conversion between pt and t is not necessary.
        thetam = ptxa(i,j,k)*(1+q_full/rd_over_rv)
        ald(k) = (gas_constant/ps0)*thetam*(p_full/ps0)**cvpm
        pfu = mu_full*znw(k+1)  + p_top
        pfd = mu_full*znw(k)    + p_top
        phm = mu_full*znu(k)    + p_top
        ph(k+1) = ph(k) + ald(k)*phm*LOG(pfd/pfu)
        phprt(i,j,k+1) = ph(k+1) - phb(i,j,k+1)
      END DO
    END DO
  END DO

  !DO j=1,ny-1
  !  DO i=1,nx-1
  !    mu(i,j)=mu(i,j)+mumid(i,j)
  !  END DO
  !END DO

RETURN

END SUBROUTINE cal_ph_mu

!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE INIGRD                     ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE inigrdarps(nx,ny,nz,nzsoil,x,y,z,zp,zpsoil,                  &
                  hterain,mapfct,j1,j2,j3,j3soil,j3soilinv,             &
                  zp1d,dzp1d,tem1)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Initialize the model grid variables.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang (03/27/2017)
!  Minor modification from "inigrd" in the ARPS package. hterain/zp should
!  be passed in and make no more change.
!
!  MODIFICATION HISTORY:
!
!
!-----------------------------------------------------------------------
!
!  INPUT :
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    nzsoil   Number of grid points in the soil model in the -z-direction
!    hterain  Terrain height (m)
!    zp       Vertical coordinate of grid points in physical space
!             (m)
!
!  OUTPUT:
!
!    x        x coordinate of grid points in physical/comp. space (m)
!    y        y coordinate of grid points in physical/comp. space (m)
!    z        z coordinate of grid points in computational space (m)
!    zpsoil   Vertical coordinate of grid points in the soil model
!             in physical space (m).
!    mapfct   Map factors at scalar, u and v points
!
!    j1       Coordinate transformation Jacobian -d(zp)/dx
!    j2       Coordinate transformation Jacobian -d(zp)/dy
!    j3       Coordinate transformation Jacobian  d(zp)/dz
!    j3soil   Soil coordinate transformation Jacobian  d(zpsoil)/dz
!    j3soilinv Inverse of the soil coordinate transformation j3soil
!
!  WORK ARRAY:
!
!    zp1d     Temporary work array.
!    dzp1d    Temporary work array.
!    tem1     Temporary work array.
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER :: nx,ny,nz     ! The number of grid points in 3 directions
  INTEGER :: nzsoil            ! Number of grid points in the -z-direction

  REAL :: x(nx)           ! x-coord. of the physical and
                          ! computational grid. Defined at u-point.
  REAL :: y(ny)           ! y-coord. of the physical and
                          ! computational grid. Defined at v-point.
  REAL :: z(nz)           ! z-coord. of the computational grid.
                          ! Defined at w-point on the staggered grid.
  REAL :: zsoil(nzsoil)   ! z-coord. of the soil model computational grid.
                          ! Defined at the center of a soil layer.
  REAL, INTENT(IN) :: zp(nx,ny,nz)    ! Physical height coordinate defined at
                          ! w-point of the staggered grid.
  REAL :: zpsoil (nx,ny,nzsoil) ! The physical height coordinate defined
                          ! at the center of a soil layer(m).

  REAL :: j1(nx,ny,nz)    ! Coordinate transformation Jacobian
                          ! -d(zp)/dx.
  REAL :: j2(nx,ny,nz)    ! Coordinate transformation Jacobian
                          ! -d(zp)/dy.
  REAL :: j3(nx,ny,nz)    ! Coordinate transformation Jacobian
                          ! d(zp)/dz.
  REAL :: j3soil (nx,ny,nzsoil) ! Coordinate transformation Jacobian
                          ! defined as d( zpsoil )/d( zsoil ).
  REAL :: j3soilinv(nx,ny,nzsoil) ! Inverse of J3soil.


  REAL, INTENT(IN) :: hterain(nx,ny)  ! Terrain height.

  REAL :: mapfct(nx,ny,8)      ! Map factors at scalar, u and v points

  REAL :: zp1d (nz)       ! Temporary array
  REAL :: dzp1d(nz)       ! Temporary array
  REAL :: tem1(nx,ny,nz)  ! Temporary array
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL :: xs, ys, zs, radnd, pi2
  REAL :: zflat1,z1,z2

  REAL :: zpmax
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'          ! Grid & map parameters.
  INCLUDE 'phycst.inc'
  INCLUDE 'bndry.inc'
  INCLUDE 'mp.inc'            ! Message passing parameters.

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
!-----------------------------------------------------------------------
!
!  Define a uniform model grid.
!
!-----------------------------------------------------------------------
!
  CALL setgrd ( nx,ny, x,y )
!
!-----------------------------------------------------------------------
!
!  Set map factor at scalar, u, and v points
!
!-----------------------------------------------------------------------
!
  IF ( mpfctopt /= 0 ) THEN

    DO j=1,ny-1
      DO i=1,nx-1
        xs = 0.5*(x(i)+x(i+1))
        ys = 0.5*(y(j)+y(j+1))
        CALL xytomf( 1,1,xs,ys,mapfct(i,j,1) )
        IF(maptest == 1)THEN   ! set up a symmetric field...
          mapfct(i,j,1) = 1.0 + ABS((xs-0.5*(x(1)+x(nx)))               &
                                   /(x(nx)-x(1))                        &
                                   +(ys-0.5*(y(1)+y(ny)))               &
                                   /(y(ny)-y(1)))
        END IF
        mapfct(i,j,4) = 1.0/mapfct(i,j,1)
        mapfct(i,j,7) = mapfct(i,j,1)*mapfct(i,j,1)
        mapfct(i,j,8) = 0.25*mapfct(i,j,1)  ! for use in sovlwpim
                                            ! and wcontra...
      END DO
    END DO

    DO j=1,ny-1
      DO i=1,nx
        ys = 0.5*(y(j)+y(j+1))
        CALL xytomf( 1,1,x(i),ys,mapfct(i,j,2) )
        IF(maptest == 1)THEN   ! set up a symmetric field...
          mapfct(i,j,2) = 1.0 + ABS((x(i)-0.5*(x(1)+x(nx)))             &
                                   /(x(nx)-x(1))                        &
                                   +(ys-0.5*(y(1)+y(ny)))               &
                                   /(y(ny)-y(1)))
        END IF
        mapfct(i,j,5) = 1.0/mapfct(i,j,2)
      END DO
    END DO

    DO j=1,ny
      DO i=1,nx-1
        xs = 0.5*(x(i)+x(i+1))
        CALL xytomf( 1,1,xs,y(j),mapfct(i,j,3) )
        IF(maptest == 1)THEN   ! set up a symmetric field...
          mapfct(i,j,3) = 1.0 + ABS((xs-0.5*(x(1)+x(nx)))               &
                                   /(x(nx)-x(1))                        &
                                   +(y(j)-0.5*(y(1)+y(ny)))             &
                                   /(y(ny)-y(1)))
        END IF
        mapfct(i,j,6) = 1.0/mapfct(i,j,3)
      END DO
    END DO

  ELSE

    DO k=1,7
      DO j=1,ny
        DO i=1,nx
          mapfct(i,j,k) = 1.0
          mapfct(i,j,8) = 0.25
        END DO
      END DO
    END DO

  END IF
!
!-----------------------------------------------------------------------
!
!  Print map factor at scalar, u, and v points for the purpose of
!  debug
!
!-----------------------------------------------------------------------
!
!   CALL getunit( mpunit )
!   CALL asnctl ('NEWLOCAL', 1, ierr)
!   CALL asnfile('mapfactor.data', '-F f77 -N ieee', ierr)
!
!   OPEN (mpunit,file='mapfactor.data',form='unformatted')
!
!   CALL edgfill(mapfct(1,1,1),1,nx,1,nx-1, 1,ny,1,ny-1, 1,1,1,1)
!   write(mpunit) ((mapfct(i,j,1),i=1,nx),j=1,ny)
!
!   CALL edgfill(mapfct(1,1,2),1,nx,1,nx,   1,ny,1,ny-1, 1,1,1,1)
!   write(mpunit) ((mapfct(i,j,2),i=1,nx),j=1,ny)
!
!   CALL edgfill(mapfct(1,1,3),1,nx,1,nx-1, 1,ny,1,ny,   1,1,1,1)
!   write(mpunit) ((mapfct(i,j,3),i=1,nx),j=1,ny)
!
!   CLOSE( mpunit )
!   CALL retunit( mpunit )
!
!-----------------------------------------------------------------------
!
!  End of debug print.
!
!-----------------------------------------------------------------------
!
  DO k=1,nz
    z(k) = zrefsfc + (k-2) * dz
  END DO
!!
!!-----------------------------------------------------------------------
!!
!!  Specify the terrain
!!
!!-----------------------------------------------------------------------
!!
!  IF( ternopt == 0 ) THEN     ! No terrain, the ground is flat
!
!    DO j=1,ny-1
!      DO i=1,nx-1
!        hterain(i,j) = zrefsfc
!      END DO
!    END DO
!
!  ELSE IF( ternopt == 1 ) THEN  ! Bell-shaped mountain
!
!    IF (mntopt == 1) THEN
!!
!!-----------------------------------------------------------------------
!!
!!  Define the bell-shaped mountain
!!
!!-----------------------------------------------------------------------
!!
!    pi2 = 1.5707963267949
!
!    DO j=1,ny-1
!      DO i=1,nx-1
!        xs = (x(i)+x(i+1))*0.5
!        ys = (y(j)+y(j+1))*0.5
!        zs = z(2)
!
!        IF( mntwidy < 0.0 .OR. runmod == 2 ) THEN ! 2-d terrain in x-z plane.
!
!          radnd = 1.0+((xs-mntctrx)/mntwidx)**2
!
!        ELSE IF( mntwidx < 0.0 .OR. runmod == 3 ) THEN ! 2-d terrain in y-z plane.
!
!          radnd = 1.0+((ys-mntctry)/mntwidy)**2
!
!        ELSE                             ! 3-d terrain
!
!          radnd = 1.0+((xs-mntctrx)/mntwidx)**2                         &
!                 +((ys-mntctry)/mntwidy)**2
!        END IF
!
!        hterain(i,j) = zrefsfc + hmount/radnd
!
!      END DO
!    END DO
!
!    ELSE  ! mntopt == 2 or others
!
!      WRITE(6,'(1x,a,/)') ' Please supply your own terrain function in subroutine inigrd'
!      CALL arpsstop('',1)
!    END IF
!!
!!-----------------------------------------------------------------------
!!
!!  Make sure that the terrain satisfies the specified boundary
!!  conditions.
!!
!!-----------------------------------------------------------------------
!!
!    IF (mp_opt > 0) THEN
!      CALL acct_interrupt(mp_acct)
!      CALL mpsendrecv1dew(hterain,nx,ny,ebc,wbc,0,tem1)
!      CALL mpsendrecv1dns(hterain,nx,ny,nbc,sbc,0,tem1)
!    END IF
!    CALL acct_interrupt(bc_acct)
!    CALL bcs2d(nx,ny,hterain, ebc,wbc,nbc,sbc)
!    CALL acct_stop_inter
!
!  ELSE IF( ternopt == 2 ) THEN          ! Read from terrain data base
!
!!
!!-----------------------------------------------------------------------
!!
!!    Read in the terrain data.
!!
!!-----------------------------------------------------------------------
!!
!!  blocking inserted for ordering i/o for message passing
!!   DO i=0,nprocs-1,readstride
!!     IF(myproc >= i.AND.myproc <= i+readstride-1)THEN
!
!!       IF (mp_opt > 0 .AND. readsplit(FINDX_T) > 0) THEN
!
!!       CALL readsplittrn( nx,ny,dx,dy, terndta,                        &
!!              mapproj,trulat1,trulat2,trulon,sclfct,ctrlat,ctrlon,     &
!!              hterain )
!
!!       ELSE
!
!!       CALL readtrn( nx,ny,dx,dy, terndta,                             &
!!              mapproj,trulat1,trulat2,trulon,sclfct,ctrlat,ctrlon,     &
!!              hterain )
!
!!       END IF
!
!!     END IF
!
!!     IF (mp_opt > 0) CALL mpbarrier
!!   END DO
!
!  END IF

!!!
!!!-----------------------------------------------------------------------
!!!
!!!  Set up a stretched vertical grid.
!!!
!!!  For strhopt=1, function y = a+b*x**3 is used to specify dz as a
!!!                              function of k.
!!!  For strhopt=2, function y = c + a*tanh(b*x) is used to specify dz
!!!                              as a function of k.
!!!
!!!-----------------------------------------------------------------------
!!!
!!  IF ( strhopt == 0 ) THEN
!!
!!    DO k=1,nz
!!      zp1d(k) = z(k)
!!    END DO
!!
!!  ELSE IF ( strhopt == 1 .OR.strhopt == 2 ) THEN
!!
!!    z1 = zrefsfc + MAX(0.0, MIN(dlayer1, z(nz-2)-zrefsfc ))
!!    z2 = z1      + MAX(0.0, MIN(dlayer2, z(nz-1)-z1      ))
!!
!!    IF( dlayer1 >= (nz-3)*dzmin ) THEN
!!      WRITE(6,'(/1x,a,f13.3,/a,f13.3,a,a)')                             &
!!          'Can not setup a vertical grid with uniform dz=',dzmin,       &
!!          ' over the depth of ',dlayer1,' please specify a smaller ',   &
!!          'value of dlayer1. Program stopped INIGRD.'
!!      CALL arpsstop('arpsstop called from INIGRD with ther vertical grid ' &
!!            ,1)
!!    END IF
!!
!!    CALL strhgrd(nz,strhopt,zrefsfc,z1,z2,z(nz-1),                      &
!!                 dzmin,strhtune, zp1d,dzp1d)
!!
!!  ELSE
!!
!!    WRITE(6,'(1x,a,i3,a/)')                                             &
!!        'Invalid vertical grid stretching option, strhopt was ',strhopt, &
!!        '. Program stopped in INIGRD.'
!!      CALL arpsstop('arpsstop called from INIGRD with stretching ' ,1)
!!
!!  END IF
!!!
!!!-----------------------------------------------------------------------
!!!
!!!  Physical height of computational grid defined as
!!!
!!!  Zp=(z-zrefsfc)*(Zm-hterain)/(Zm-zrefsfc)+hterain for z=<Zm.
!!!  ZP=z for z>Zm
!!!
!!!  where Zm the height at which the grid levels becomes flat.
!!!  Hm < Zm =< Ztop, hm is the height of mountain and Ztop the height
!!!  of model top.
!!!
!!!-----------------------------------------------------------------------
!!!
!!  DO k=nz-1,2,-1
!!    IF(zp1d(k) <= zflat) THEN
!!      zflat1 = zp1d(k)
!!      EXIT
!!    END IF
!!  END DO
!!  zflat1=MAX(MIN(z(nz-1),zflat1),zrefsfc)
!!
!!  DO k=2,nz-1
!!
!!    IF(zp1d(k) > zflat1) THEN
!!      DO j=1,ny-1
!!        DO i=1,nx-1
!!          zp(i,j,k)=zp1d(k)
!!        END DO
!!      END DO
!!    ELSE
!!      DO j=1,ny-1
!!        DO i=1,nx-1
!!          zp(i,j,k)=(zp1d(k)-zrefsfc)*(zflat1-hterain(i,j))             &
!!                     /(zflat1-zrefsfc)+hterain(i,j)
!!        END DO
!!      END DO
!!    END IF
!!
!!  END DO
!!
!!  DO j=1,ny-1
!!    DO i=1,nx-1
!!      zp(i,j,2)=hterain(i,j)
!!      zp(i,j,1)=2.0*zp(i,j,2)-zp(i,j,3)
!!      zp(i,j,nz)=2.0*zp(i,j,nz-1)-zp(i,j,nz-2)
!!    END DO
!!  END DO
!
!-----------------------------------------------------------------------
!
!  Calculate transformation Jacobians J1, J2 and J3.
!
!-----------------------------------------------------------------------
!
  CALL jacob(nx,ny,nz,x,y,z,zp,j1,j2,j3,tem1)

  CALL inisoilgrd(nx,ny,nzsoil,hterain,zpsoil,j3soil,j3soilinv)


  RETURN
END SUBROUTINE inigrdarps
!
!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE INIGRDWRF                  ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE inigrdwrf(nx,ny,nz,nzsoil,x,y,z,zsoil,zpsoil,hterain,istatus)

!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Initialize an ARPS similar model grid variables from WRF input variables
!  To imitate "inigrd" in the ARPS package.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang
!  03/04/2016.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INTEGER, INTENT(IN)  :: nx,ny,nz     ! The number of grid points in 3 directions, imitates ARPS grid
  INTEGER, INTENT(IN)  :: nzsoil       ! Number of grid points in the z-direction for soil layers

  REAL,    INTENT(OUT) :: x(nx)        ! x-coord. of the physical and
                                       ! computational grid. Defined at u-point.
  REAL,    INTENT(OUT) :: y(ny)        ! y-coord. of the physical and
                                       ! computational grid. Defined at v-point.
  REAL,    INTENT(OUT) :: z(nz)        ! Dummy array, should not be used
  REAL,    INTENT(IN)  :: zsoil(nzsoil)! z-coord. of the soil model computational grid.
                                       ! Defined at the center of a soil layer.

!  REAL,    INTENT(IN)  :: zp(nx,ny,nz)    ! Physical height coordinate defined at
!                                          ! w-point of the staggered grid.
  REAL,    INTENT(OUT) :: zpsoil (nx,ny,nzsoil) ! The physical height coordinate defined
                          ! at the center of a soil layer(m).

!  REAL,    INTENT(OUT) :: j1(nx,ny,nz)    ! Coordinate transformation Jacobian
!                          ! -d(zp)/dx.
!  REAL,    INTENT(OUT) :: j2(nx,ny,nz)    ! Coordinate transformation Jacobian
!                          ! -d(zp)/dy.
!  REAL,    INTENT(OUT) :: j3(nx,ny,nz)    ! Coordinate transformation Jacobian
!                          ! d(zp)/dz. UNIFORMLY 1.0
!  REAL,    INTENT(OUT) :: j3inv(nx,ny,nz) ! UNIFORMLY 1.0
!  REAL,    INTENT(OUT) :: aj3x (nx,ny,nz)
!  REAL,    INTENT(OUT) :: aj3y (nx,ny,nz)
!  REAL,    INTENT(OUT) :: aj3z (nx,ny,nz)
!
!  REAL,    INTENT(OUT) :: j3soil (nx,ny,nzsoil) ! Coordinate transformation Jacobian
!                          ! defined as d( zpsoil )/d( zsoil ). UNIFORMLY 1.0
!  REAL,    INTENT(OUT) :: j3soilinv(nx,ny,nzsoil) ! Inverse of J3soil. UNIFORMLY 1.0


  REAL,    INTENT(IN)  :: hterain(nx,ny)  ! Terrain height.

!  REAL,    INTENT(OUT) :: mapfct(nx,ny,8)      ! Map factors at scalar, u and v points
!
!  REAL :: tem1(nx,ny,nz)  ! Temporary array
!
  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL :: xs, ys, zs, radnd, pi2
  REAL :: zflat1,z1,z2

  REAL :: zpmax
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  INCLUDE 'globcst.inc'
  INCLUDE 'grid.inc'          ! Grid & map parameters.
  INCLUDE 'phycst.inc'
  INCLUDE 'bndry.inc'
  INCLUDE 'mp.inc'            ! Message passing parameters.

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!
  istatus = 0

!-----------------------------------------------------------------------
!
!  Define a uniform model grid.
!
!-----------------------------------------------------------------------
!
  CALL setgrd ( nx,ny, x,y )    ! Call ARPS grid directly
!
!-----------------------------------------------------------------------
!
!  Set map factor at scalar, u, and v points
!
!-----------------------------------------------------------------------
!
!  IF ( mpfctopt /= 0 ) THEN
!
!    DO j=1,ny-1
!      DO i=1,nx-1
!        xs = 0.5*(x(i)+x(i+1))
!        ys = 0.5*(y(j)+y(j+1))
!        CALL xytomf( 1,1,xs,ys,mapfct(i,j,1) )
!        IF(maptest == 1)THEN   ! set up a symmetric field...
!          mapfct(i,j,1) = 1.0 + ABS((xs-0.5*(x(1)+x(nx)))               &
!                                   /(x(nx)-x(1))                        &
!                                   +(ys-0.5*(y(1)+y(ny)))               &
!                                   /(y(ny)-y(1)))
!        END IF
!        mapfct(i,j,4) = 1.0/mapfct(i,j,1)
!        mapfct(i,j,7) = mapfct(i,j,1)*mapfct(i,j,1)
!        mapfct(i,j,8) = 0.25*mapfct(i,j,1)  ! for use in sovlwpim
!                                            ! and wcontra...
!      END DO
!    END DO
!
!    DO j=1,ny-1
!      DO i=1,nx
!        ys = 0.5*(y(j)+y(j+1))
!        CALL xytomf( 1,1,x(i),ys,mapfct(i,j,2) )
!        IF(maptest == 1)THEN   ! set up a symmetric field...
!          mapfct(i,j,2) = 1.0 + ABS((x(i)-0.5*(x(1)+x(nx)))             &
!                                   /(x(nx)-x(1))                        &
!                                   +(ys-0.5*(y(1)+y(ny)))               &
!                                   /(y(ny)-y(1)))
!        END IF
!        mapfct(i,j,5) = 1.0/mapfct(i,j,2)
!      END DO
!    END DO
!
!    DO j=1,ny
!      DO i=1,nx-1
!        xs = 0.5*(x(i)+x(i+1))
!        CALL xytomf( 1,1,xs,y(j),mapfct(i,j,3) )
!        IF(maptest == 1)THEN   ! set up a symmetric field...
!          mapfct(i,j,3) = 1.0 + ABS((xs-0.5*(x(1)+x(nx)))               &
!                                   /(x(nx)-x(1))                        &
!                                   +(y(j)-0.5*(y(1)+y(ny)))             &
!                                   /(y(ny)-y(1)))
!        END IF
!        mapfct(i,j,6) = 1.0/mapfct(i,j,3)
!      END DO
!    END DO
!
!  ELSE   ! mpfctopt == 0
!
!    DO k=1,7
!      mapfct(:,:,k) = 1.0
!    END DO
!    mapfct(:,:,8) = 0.25
!
!  END IF
!
!-----------------------------------------------------------------------
!
!  Print map factor at scalar, u, and v points for the purpose of
!  debug
!
!-----------------------------------------------------------------------
!
!   CALL getunit( mpunit )
!   CALL asnctl ('NEWLOCAL', 1, ierr)
!   CALL asnfile('mapfactor.data', '-F f77 -N ieee', ierr)
!
!   OPEN (mpunit,file='mapfactor.data',form='unformatted')
!
!   CALL edgfill(mapfct(1,1,1),1,nx,1,nx-1, 1,ny,1,ny-1, 1,1,1,1)
!   write(mpunit) ((mapfct(i,j,1),i=1,nx),j=1,ny)
!
!   CALL edgfill(mapfct(1,1,2),1,nx,1,nx,   1,ny,1,ny-1, 1,1,1,1)
!   write(mpunit) ((mapfct(i,j,2),i=1,nx),j=1,ny)
!
!   CALL edgfill(mapfct(1,1,3),1,nx,1,nx-1, 1,ny,1,ny,   1,1,1,1)
!   write(mpunit) ((mapfct(i,j,3),i=1,nx),j=1,ny)
!
!   CLOSE( mpunit )
!   CALL retunit( mpunit )
!
!-----------------------------------------------------------------------
!
!  End of debug print.
!
!-----------------------------------------------------------------------
!
  !DO k=1,nz
  !  z(k) = zrefsfc + (k-2) * dz
  !END DO
  z(:) = -9999.9
!
!-----------------------------------------------------------------------
!
!  Calculate transformation Jacobians J1, J2 and J3.
!
!-----------------------------------------------------------------------
!
  !CALL jacob(nx,ny,nz,x,y,z,zp,j1,j2,j3,tem1)

!!-----------------------------------------------------------------------
!!
!!  Calculate transformation Jacobian J1 defined as -del(zp)/del(x)
!!
!!-----------------------------------------------------------------------
!!
!  DO k=1,nz
!    DO j=1,ny-1
!      DO i=2,nx-1
!        j1(i,j,k) = -2 * (zp(i,j,k)-zp(i-1,j,k)) / (x(i+1)-x(i-1))
!      END DO
!    END DO
!  END DO
!
!!
!!-----------------------------------------------------------------------
!!
!!  X - boundary conditions of j1
!!
!!-----------------------------------------------------------------------
!!
!
!  IF (mp_opt > 0) THEN
!    CALL acct_interrupt(mp_acct)
!    CALL mpsendrecv2dew(j1,nx,ny,nz,ebc,wbc,1,tem1)
!  END IF
!
!  CALL acct_interrupt(bc_acct)
!
!  IF(wbc == 1) THEN            ! Rigid wall boundary condition
!
!    DO k=1,nz
!      DO j=1,ny-1
!        j1( 1,j,k)=j1( 3 ,j,k)
!      END DO
!    END DO
!
!  ELSE IF(wbc == 2) THEN        ! Periodic boundary condition.
!
!    IF(mp_opt == 0) THEN
!      DO k=1,nz
!      DO j=1,ny-1
!        j1( 1,j,k)=j1(nx-2,j,k)
!      END DO
!      END DO
!    END IF
!
!  ELSE IF(wbc /= 0) THEN
!
!    DO k=1,nz
!      DO j=1,ny-1
!        j1( 1,j,k)=j1( 2 ,j,k)
!      END DO
!    END DO
!
!  END IF
!
!  IF(ebc == 1) THEN             ! Rigid wall boundary condition
!
!    DO k=1,nz
!      DO j=1,ny-1
!        j1(nx,j,k)=j1(nx-2,j,k)
!      END DO
!    END DO
!
!  ELSE IF(ebc == 2) THEN         ! Periodic boundary condition.
!
!    IF(mp_opt == 0) THEN
!      DO k=1,nz
!      DO j=1,ny-1
!        j1(nx,j,k)=j1( 3 ,j,k)
!      END DO
!      END DO
!    END IF
!
!  ELSE IF(ebc /= 0) THEN
!
!    DO k=1,nz
!      DO j=1,ny-1
!        j1(nx,j,k)=j1(nx-1,j,k)
!      END DO
!    END DO
!
!  END IF
!
!  DO k=1,nz
!    DO i=1,nx
!      j1(i,ny,k) = j1(i,ny-1,k)
!    END DO
!  END DO
!
!  CALL acct_stop_inter
!
!!
!!-----------------------------------------------------------------------
!!
!!  Calculate transformation Jacobian J2 defined as -del(zp)/del(y)
!!
!!-----------------------------------------------------------------------
!!
!  DO k=1,nz
!    DO i=1,nx-1
!      DO j=2,ny-1
!        j2(i,j,k) = -2 * (zp(i,j,k)-zp(i,j-1,k)) / (y(j+1)-y(j-1))
!      END DO
!    END DO
!  END DO
!!
!!-----------------------------------------------------------------------
!!
!!  Y - boundary conditions of j2
!!
!!-----------------------------------------------------------------------
!!
!
!  IF (mp_opt > 0) THEN
!    CALL acct_interrupt(mp_acct)
!    CALL mpsendrecv2dns(j2,nx,ny,nz,nbc,sbc,2,tem1)
!  END IF
!
!  CALL acct_interrupt(bc_acct)
!
!  IF(sbc == 1) THEN            ! Rigid wall boundary condition
!
!    DO k=1,nz
!      DO i=1,nx-1
!        j2(i, 1,k)=j2(i, 3 ,k)
!      END DO
!    END DO
!
!  ELSE IF(sbc == 2) THEN        ! Periodic boundary condition.
!
!    IF (mp_opt == 0) THEN
!      DO k=1,nz
!      DO i=1,nx-1
!        j2(i, 1,k)=j2(i,ny-2,k)
!      END DO
!      END DO
!    END IF
!
!  ELSE IF(sbc /= 0) THEN
!
!    DO k=1,nz
!      DO i=1,nx-1
!        j2(i, 1,k)=j2(i, 2 ,k)
!      END DO
!    END DO
!
!  END IF
!
!  IF(nbc == 1) THEN             ! Rigid wall boundary condition
!
!    DO k=1,nz
!      DO i=1,nx-1
!        j2(i,ny,k)=j2(i,ny-2,k)
!      END DO
!    END DO
!
!  ELSE IF(nbc == 2) THEN         ! Periodic boundary condition.
!
!    IF (mp_opt == 0) THEN
!      DO k=1,nz
!      DO i=1,nx-1
!        j2(i,ny,k)=j2(i, 3 ,k)
!      END DO
!      END DO
!    END IF
!
!  ELSE IF(nbc /= 0) THEN
!
!    DO k=1,nz
!      DO i=1,nx-1
!        j2(i,ny,k)=j2(i,ny-1,k)
!      END DO
!    END DO
!
!  END IF
!
!  DO k=1,nz
!    DO j=1,ny
!      j2(nx,j,k) = j2(nx-1,j,k)
!    END DO
!  END DO
!
!  CALL acct_stop_inter
!
!!
!!-----------------------------------------------------------------------
!!
!!  Calculate transformation Jacobian J3 defined as del(zp)/del(z)
!!
!!-----------------------------------------------------------------------
!!
!  j3(:,:,:) = 1.0
!  !DO k=1,nz-1
!  !  DO j=1,ny-1
!  !    DO i=1,nx-1
!  !      j3(i,j,k) =(zp(i,j,k+1)-zp(i,j,k)
!  !    END DO
!  !  END DO
!  !END DO
!  !
!  !DO k=1,nz-1
!  !  DO j=1,ny-1
!  !    j3(nx,j,k) = j3(nx-1,j,k)
!  !  END DO
!  !END DO
!  !
!  !DO k=1,nz-1
!  !  DO i=1,nx
!  !    j3(i,ny,k) = j3(i,ny-1,k)
!  !  END DO
!  !END DO
!  !
!  !DO j=1,ny
!  !  DO i=1,nx
!  !    j3(i,j,nz) = j3(i,j,nz-1)
!  !  END DO
!  !END DO
!
!!-----------------------------------------------------------------------
!!
!! Initilize the reverse of Jacobian arrays
!!
!!-----------------------------------------------------------------------
!
!  j3inv(:,:,:) = 1.0
!
!  !DO k=1,nz-1
!  !  DO j=1,ny-1
!  !    DO i=1,nx-1
!  !      j3inv(i,j,k)=1.0/j3(i,j,k)
!  !    END DO
!  !  END DO
!  !END DO
!
!!-----------------------------------------------------------------------
!!
!! Initilize the reverse of Jacobian arrays, aj3x, aj3y, aj3z
!!
!!-----------------------------------------------------------------------
!
!  aj3x(:,:,:) = 1.0
!  aj3y(:,:,:) = 1.0
!  aj3z(:,:,:) = 1.0
!
!  !DO k=1,nz-1
!  !  DO j=1,ny-1
!  !    DO i=2,nx-1
!  !      aj3x(i,j,k)=0.5*(j3(i,j,k)+j3(i-1,j,k))
!  !    END DO
!  !  END DO
!  !END DO
!  !
!  !IF (mp_opt > 0) THEN
!  !  CALL acct_interrupt(mp_acct)
!  !  CALL mpsendrecv2dew(aj3x,nx,ny,nz,ebc,wbc,1,tem2)
!  !  !CALL mpsend2dns(aj3x,nx,ny,nz,1,mptag,tem2)
!  !  !CALL mprecv2dns(aj3x,nx,ny,nz,1,mptag,tem2)
!  !END IF
!  !CALL acct_interrupt(bc_acct)
!  !CALL bcsu(nx,ny,nz,1,ny-1,1,nz-1,ebc,wbc,aj3x)
!  !CALL acct_stop_inter
!  !
!  !DO k=1,nz-1
!  !  DO j=2,ny-1
!  !    DO i=1,nx-1
!  !      aj3y(i,j,k)=0.5*(j3(i,j,k)+j3(i,j-1,k))
!  !    END DO
!  !  END DO
!  !END DO
!  !
!  !IF (mp_opt > 0) THEN
!  !  CALL acct_interrupt(mp_acct)
!  !  !CALL mpsend2dew(aj3y,nx,ny,nz,2,mptag,tem2)
!  !  !CALL mprecv2dew(aj3y,nx,ny,nz,2,mptag,tem2)
!  !  CALL mpsendrecv2dns(aj3y,nx,ny,nz,nbc,sbc,2,tem2)
!  !END IF
!  !CALL acct_interrupt(bc_acct)
!  !CALL bcsv(nx,ny,nz,1,nx-1,1,nz-1,nbc,sbc,aj3y)
!  !CALL acct_stop_inter
!  !
!  !DO k=2,nz-1
!  !  DO j=1,ny-1
!  !    DO i=1,nx-1
!  !      aj3z(i,j,k)=0.5*(j3(i,j,k)+j3(i,j,k-1))
!  !    END DO
!  !  END DO
!  !END DO
!  !
!  !CALL acct_interrupt(bc_acct)
!  !CALL bcsw(nx,ny,nz,1,nx-1,1,ny-1,tbc,bbc,aj3z)
!  !CALL acct_stop_inter
!
!
!-----------------------------------------------------------------------
!
!  Calculate the soil model grid variables:
!
!                  zsoil,zpsoil,j3soil,j3soilinv.
!
!-----------------------------------------------------------------------

  !CALL inisoilgrd(nx,ny,nzsoil,hterain,zpsoil,j3soil,j3soilinv)

  DO k = 1,nzsoil
    DO j = 1,ny
      DO i = 1,nx
        zpsoil(i,j,k) = hterain(i,j) - zsoil(k)
!        j3soil(i,j,k)    = 1.0
!        j3soilinv(i,j,k) = 1.0
      END DO
    END DO
  END DO

  RETURN
END SUBROUTINE inigrdwrf

!
!##################################################################
!##################################################################
!######                                                      ######
!######               SUBROUTINE EXTWRFSND                   ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE extwrfsnd(nx,ny,nz,hgt,pp,pt,qv,u,v,                         &
                     psnd,ptsnd,qvsnd,usnd,vsnd, istatus)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Finds a mean sounding of all external data within the WRF/External
!  domain.  This is used to define the model base state.
!  The data are averaged in the horizontal.
!  The final sounding is hydrostatic.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Y. Wang
!  October, 2016  Based on "extmnsnd" in the ARPS package.
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Sizing variables
!
!-----------------------------------------------------------------------
!
  INTEGER, INTENT(IN) :: nx,ny,nz   ! on WRF/external grid

!
!-----------------------------------------------------------------------
!
!  External grid variables
!
!-----------------------------------------------------------------------
!
  REAL, INTENT(IN) :: hgt(nx,ny,nz)          ! Height MSL on W levels
  REAL, INTENT(IN) :: pp(nx,ny,nz)           ! Total pressure (Pa)
  REAL, INTENT(IN) :: pt(nx,ny,nz)           ! Potential Temperature (K)
  REAL, INTENT(IN) :: qv(nx,ny,nz)           ! Total specific humidity (kg/kg)
  REAL, INTENT(IN) :: u(nx,ny,nz)            ! u wind component
  REAL, INTENT(IN) :: v(nx,ny,nz)            ! v wind component

!
!-----------------------------------------------------------------------
!
!  Output sounding variables
!
!-----------------------------------------------------------------------
!

  REAL,    INTENT(OUT) :: psnd(0:nz+1),ptsnd(0:nz+1),qvsnd(0:nz+1),     &
                          usnd(0:nz+1),vsnd(0:nz+1)

  INTEGER, INTENT(OUT) :: istatus
!
!-----------------------------------------------------------------------
!
!  Limits of calculations
!
!-----------------------------------------------------------------------
!
  REAL :: zsnd(0:nz+1)
  REAL :: plsnd(0:nz+1),tsnd(0:nz+1),rhssnd(0:nz+1),rhosnd(0:nz+1)

  REAL, ALLOCATABLE :: pln(:,:,:)         ! ln of total pressure
  REAL, ALLOCATABLE :: rhs(:,:,:)         ! RHstar=SQRT(1.-RH)
  REAL, ALLOCATABLE :: ttt(:,:,:)         ! temperature (K)

!
!-----------------------------------------------------------------------
!
!  Output sounding variables, double precision version
!
!  Running a sum on lots of small numbers ends up with a precision
!  problem.  MPI and non-MPI runs will NEVER have the same answers.
!  Non-MPI runs will not even have the same answers in the numbers are
!  computed in a different order!
!
!-----------------------------------------------------------------------
!
  DOUBLE PRECISION :: zsnd_dbl  (nz)
  !DOUBLE PRECISION :: ptsnd_dbl (nz)
  DOUBLE PRECISION :: plsnd_dbl (nz)
  DOUBLE PRECISION :: tsnd_dbl  (nz)
  DOUBLE PRECISION :: rhssnd_dbl(nz)
  DOUBLE PRECISION :: usnd_dbl  (nz)
  DOUBLE PRECISION :: vsnd_dbl  (nz)
!
!-----------------------------------------------------------------------
!
!  Misc local variables
!
!-----------------------------------------------------------------------
!
  INTEGER :: ibgn, jbgn, iend, jend, iendu, jendv, ktop,kbot
  INTEGER :: kntm,kntu,kntv
!
!-----------------------------------------------------------------------
!
!  Include files
!
!-----------------------------------------------------------------------
!
  INCLUDE 'phycst.inc'
  INCLUDE 'mp.inc'
!
!-----------------------------------------------------------------------
!
!  Lapse rate and a constant for hydrostatic integration
!
!-----------------------------------------------------------------------
!
  REAL, PARAMETER :: gamma  = 0.0068         ! 6.8 degrees per km
  REAL, PARAMETER :: pconst = (g/rd)
!
!-----------------------------------------------------------------------
!
!  Misc internal variables
!
!-----------------------------------------------------------------------
!
  REAL, PARAMETER :: rhmin=0.05
  REAL, PARAMETER :: rhmax=1.0

  INTEGER :: i,j,k
  REAL    :: c1,c2,pres,qvsat,qvbot,qvtop,rh,tvbar

!-----------------------------------------------------------------------
!
!  Function f_qvsat and inline directive for Cray PVP
!
!-----------------------------------------------------------------------
!
  REAL :: f_qvsat

!fpp$ expand (f_qvsat)
!!dir$ inline always f_qvsat
!*$*  inline routine (f_qvsat)

!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  istatus = 0

!
!  Make sure we do the computations only once for each subdomain
!
  ibgn = 1
  jbgn = 1
  iend = nx-1; iendu = nx-1
  jend = ny-1; jendv = ny-1
  IF (loc_x == nproc_x) iendu = nx
  IF (loc_y == nproc_y) jendv = ny
  kbot = 0
  ktop = nz+1

!
!-----------------------------------------------------------------------
!
!  Zero-out all sounding arrays
!  ptsnd array is used temporarily to store a count.
!
!-----------------------------------------------------------------------
!
  DO k=kbot,ktop
    zsnd(k)  = 0.
    ptsnd(k) = 0.
    plsnd(k) = 0.
    tsnd(k)  = 0.
    rhssnd(k)= 0.
    qvsnd(k) = 0.
    usnd(k)  = 0.
    vsnd(k)  = 0.
    psnd(k)  = 0.
    rhosnd(k) = 0.
  END DO

!
!-----------------------------------------------------------------------
!
!  Computer derive state variables
!
!  Calculate log of pressure for external grid.
!  Change qv to RHstar   RHStar=sqrt(1.-relative humidity)
!  Convert potential temperature to temperature
!-----------------------------------------------------------------------
!
  ALLOCATE(pln(nx,ny,nz), STAT = istatus)
  ALLOCATE(rhs(nx,ny,nz), STAT = istatus)
  ALLOCATE(ttt(nx,ny,nz),   STAT = istatus)

  DO k = 1, nz-1
    DO j = 1, ny-1
      DO i = 1, nx-1
        pln(i,j,k) = ALOG(pp(i,j,k))
        ttt(i,j,k) = pt(i,j,k) * ((pp(i,j,k)/p0)**rddcp)
        qvsat=f_qvsat( pp(i,j,k), ttt(i,j,k) )
        rhs(i,j,k)=SQRT(AMAX1(0.,(rhmax-(qv(i,j,k)/qvsat))))
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Look at all external points possibly within domain
!
!-----------------------------------------------------------------------
!
  DO k=1,nz
    zsnd_dbl(k)   = 0.
    plsnd_dbl(k)  = 0.
    tsnd_dbl(k)   = 0.
    rhssnd_dbl(k) = 0.
    usnd_dbl(k)   = 0.
    vsnd_dbl(k)   = 0.
  END DO

  kntm = 0; kntu = 0; kntv = 0

  DO j = jbgn,jend
    DO i = ibgn,iend

      kntm=kntm+1
      DO k=1,nz-1
        zsnd_dbl(k)  = zsnd_dbl(k)  + 0.5*(hgt(i,j,k)+hgt(i,j,k+1))  ! hgt is staggered on W levels
        plsnd_dbl(k) = plsnd_dbl(k) + pln(i,j,k)
        tsnd_dbl(k)  = tsnd_dbl(k)  + ttt(i,j,k)
        rhssnd_dbl(k)= rhssnd_dbl(k)+ rhs(i,j,k)
      END DO
    END DO
  END DO

!
!-----------------------------------------------------------------------
!
!  Look at all external points for U stagger
!
!-----------------------------------------------------------------------
!

  DO j = jbgn,jend
    DO i = ibgn,iendu

      kntu=kntu+1
      DO k=1,nz-1
        usnd_dbl(k)=usnd_dbl(k)+u(i,j,k)
      END DO
    END DO
  END DO

!
!-----------------------------------------------------------------------
!
!  Look at all external points for V stagger
!
!-----------------------------------------------------------------------
!

  DO j = jbgn,jendv
    DO i = ibgn,iend

      kntv=kntv+1
      DO k=1,nz-1
        vsnd_dbl(k)=vsnd_dbl(k)+v(i,j,k)
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
! mpi sum  ptsnd(k), knt, kntu, kntv
!          plsnd(k),tsnd(k),rhssnd(k),usnd(k),vsnd(k)
!
!-----------------------------------------------------------------------
!
  IF(mp_opt > 0) THEN
    CALL mptotali(kntm)
    CALL mptotali(kntu)
    CALL mptotali(kntv)

    CALL mpsumdp(tsnd_dbl,  nz)
    CALL mpsumdp(zsnd_dbl,  nz)
    CALL mpsumdp(plsnd_dbl, nz)
    CALL mpsumdp(rhssnd_dbl,nz)
    CALL mpsumdp(usnd_dbl,  nz)
    CALL mpsumdp(vsnd_dbl,  nz)
  END IF

!
!-----------------------------------------------------------------------
!
!  Divide through to find average and Return to single precision.
!
!
! NOTE: plsnd, tsnd, etc should be exactly the same as
!       non-mpi mode because of the problem variables were made
!       double precision, which get around the round-off error.
!       The variables summed had sufficient precision.
!
!-----------------------------------------------------------------------
!
  DO k=1,nz-1
    zsnd(k)   = zsnd_dbl(k)  / kntm
    tsnd(k)   = tsnd_dbl(k)  / kntm
    plsnd(k)  = plsnd_dbl(k) / kntm
    rhssnd(k) = rhssnd_dbl(k)/ kntm
    usnd(k)   = usnd_dbl(k)  / kntu
    vsnd(k)   = vsnd_dbl(k)  / kntv
  END DO

  zsnd(0)    = zsnd(1)   - (zsnd(2) -zsnd(1))
  zsnd(nz)   = zsnd(nz-1)+ (zsnd(nz-1)-zsnd(nz-2))
  zsnd(nz+1) = zsnd(nz)  + (zsnd(nz)-zsnd(nz-1))

  IF(myproc == 0) THEN
    WRITE(*,'(1x,a,3(i0,a),a/)')                                        &
       ' EXTWRFSND found ',kntm,'(S), ',kntu,'(U), ',kntv,'(V) points', &
       ' within the WRF domain horizontally.'

    WRITE(*,'(1x,a,2(f15.2,a)/)')                                       &
               ' Height of external data for mean spans from ',         &
                 zsnd(kbot),' to ',zsnd(ktop),' meters.'
  END IF

!-----------------------------------------------------------------------
!
!  Set variables "below-ground"
!  Use a constant lapse rate, gamma.
!  plsnd is a sort of first-guess log(pressure), needed for qv
!  calculation from rhstar.
!
!-----------------------------------------------------------------------
!
  pres = EXP(plsnd(1))
  rh=MAX(rhmin,rhmax-(rhssnd(1)*rhssnd(1)))
  qvsat = f_qvsat( pres, tsnd(1) )
  qvbot=rh*qvsat
  c1=g/(rd*gamma)
  DO k=0,kbot,-1
    tsnd(k)=tsnd(1)-gamma*(zsnd(k)-zsnd(1))
    plsnd(k)=plsnd(1)+c1*ALOG( tsnd(k)/tsnd(1) )
    psnd(k) = EXP(plsnd(k))
    qvsat = f_qvsat( psnd(k), tsnd(k) )
    rh=qvbot/qvsat
    rhssnd(k)=SQRT(MAX(0.,(rhmax-rh)))
    usnd(k)=usnd(1)
    vsnd(k)=vsnd(1)
  END DO
!
!-----------------------------------------------------------------------
!
!  Set variables "above-top"
!  Use a constant temperature. We're assuming stratosphere here.
!
!-----------------------------------------------------------------------
!
  pres = EXP(plsnd(nz-1))
  rh=MAX(rhmin,rhmax-(rhssnd(nz-1)*rhssnd(nz-1)))
  qvsat = f_qvsat( pres, tsnd(nz-1) )
  qvtop=rh*qvsat
  c2=g/rd
  DO k=nz,ktop
    tsnd(k)=tsnd(nz-1)
    plsnd(k)=plsnd(nz-1)-c2*(zsnd(k)-zsnd(nz-1))/tsnd(nz-1)
    psnd(k) = EXP(plsnd(k))
    qvsat = f_qvsat( psnd(k), tsnd(k) )
    rh=qvtop/qvsat
    rhssnd(k)=SQRT(MAX(0.,(rhmax-rh)))
    usnd(k)=usnd(nz-1)
    vsnd(k)=vsnd(nz-1)
  END DO

!-----------------------------------------------------------------------
!
!  Calculate qv profile from RH-star.
!  Temporarily use rhosnd to store virtual temperature
!
!-----------------------------------------------------------------------
!
  DO k=kbot,ktop
    pres = EXP(plsnd(k))
    rh=MAX(rhmin,rhmax-(rhssnd(k)*rhssnd(k)))
    qvsat = f_qvsat( pres, tsnd(k) )
    qvsnd(k)=rh*qvsat
    rhosnd(k) = tsnd(k)*(1.0+rvdrd*qvsnd(k))/(1.0+qvsnd(k))
  END DO
!
!-----------------------------------------------------------------------
!
!  Make sure that the sounding is hydrostatic by integrating
!  from 1. rhosnd is really virtual temperature here.
!
!-----------------------------------------------------------------------
!
  psnd(1)=EXP(plsnd(1))

  DO k=0,kbot,-1
    tvbar=0.5*(rhosnd(k+1)+rhosnd(k))
    psnd(k)=psnd(k+1)*EXP(pconst*(zsnd(k+1)-zsnd(k))/tvbar)
  END DO

  DO k=2,ktop
    tvbar=0.5*(rhosnd(k-1)+rhosnd(k))
    psnd(k)=psnd(k-1)*EXP(pconst*(zsnd(k-1)-zsnd(k))/tvbar)
  END DO
!
!-----------------------------------------------------------------------
!
!  Derived variable calculations
!  Compute density from virtual temperature.
!  Compute potential temperature.
!  Compute log of pressure.
!
!-----------------------------------------------------------------------
!
  DO k=kbot,ktop
    rhosnd(k)=psnd(k)/(rd*rhosnd(k))
    ptsnd(k)=tsnd(k)*((p0/psnd(k))**rddcp)
!    pres=exp(plsnd(k))
!    print *,'psnd old, psnd new: ',pres,psnd(k),(psnd(k)-pres)
    plsnd(k)=ALOG(psnd(k))
  END DO

!-----------------------------------------------------------------------
!
! Before return
!
!-----------------------------------------------------------------------

  DEALLOCATE(pln, rhs, ttt)

  RETURN
END SUBROUTINE extwrfsnd

!#####################################################################

SUBROUTINE compute_diff_metrics( nx,ny, nz, rdx, rdy, z_at_w,           &
                                 zx,zy,dzw,rdzw,tem1,istatus )

!---------------------------------------------------------------------
!
!  PURPOSE:
!
!  Compute metrics required by cal_div3_wrf & cal_adjdiv3_wrf.
!
!---------------------------------------------------------------------
!
!  AUTHOR:
!
!  Y. Wang (09/28/2016)
!  Based on dyn_em/module_diffusion_em.f90 in WRFV3.7.1.
!
!  MODIFICATION HISTORY:
!
!---------------------------------------------------------------------

  IMPLICIT NONE

  INTEGER, INTENT( IN )  :: nx, ny, nz
  REAL,    INTENT( IN )  :: rdx, rdy

  !REAL, DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( IN )    :: ph, phb
  REAL, DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( IN )    :: z_at_w
  REAL, DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( OUT )   :: dzw,rdzw, zx, zy
  REAL, DIMENSION( 1:nx, 1:ny, 1:nz ), INTENT( INOUT ) :: tem1

  INTEGER, INTENT(OUT)  :: istatus

!---------------------------------------------------------------------

  INTEGER :: kte
  INTEGER :: i, j, k, i_start, i_end, j_start, j_end, ktf

  INCLUDE 'mp.inc'

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  kte = nz

  ktf = nz-1

  j_start = 1
  j_end   = ny

  i_start = 1
  i_end   = nx

  !z_at_w(:,:,:) = ( ph(:,:,:) + phb(:,:,:) ) / g
  dzw  = 1.0
  rdzw = 1.0

  DO j = j_start, j_end

    DO k = 1, ktf
      DO i = i_start, i_end
        dzw(i,j,k)  = z_at_w(i,j,k+1) - z_at_w(i,j,k)
        rdzw(i,j,k) = 1.0 / ( z_at_w(i,j,k+1) - z_at_w(i,j,k) )
      END DO
    END DO

    !DO k = 2, ktf
    !  DO i = i_start, i_end
    !    rdz(i,j,k) = 2.0 / ( z_at_w(i,j,k+1) - z_at_w(i,j,k-1) )
    !  END DO
    !END DO
    !
    !DO i = i_start, i_end
    !  rdz(i,j,1) = 2./(z_at_w(i,j,2)-z_at_w(i,j,1))
    !END DO

  END DO

  i_start = 1
  i_end   = nx
  j_start = 1
  j_end   = ny

  zx(:,:,:) = 0.0
  zy(:,:,:) = 0.0
  DO k = 1, kte
    DO j = j_start, j_end
    DO i = 2, i_end
      zx(i,j,k) = rdx * ( z_at_w(i,j,k) - z_at_w(i-1,j,k) )
    END DO
    END DO
  END DO

  IF (mp_opt > 0) THEN
    !CALL acct_interrupt(mp_acct)
    CALL mpsendrecv2dew(zx,nx,ny,nz,0,0,1,tem1)
  END IF

  DO k = 1, kte
    DO j = 2, j_end
    DO i = i_start, i_end
      zy(i,j,k) = rdy * ( z_at_w(i,j,k) - z_at_w(i,j-1,k) )
    END DO
    END DO
  END DO

  IF (mp_opt > 0) THEN
    !CALL acct_interrupt(mp_acct)
    CALL mpsendrecv2dns(zy,nx,ny,nz,0,0,2,tem1)
  END IF

  RETURN
END SUBROUTINE compute_diff_metrics
