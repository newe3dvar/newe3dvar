!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE READWRFVAR                 ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE readwrfvar(nx,ny,nz,nscalar,qnames,file_name,     &
                      u, v, w, pt, ptbar, pp, pbar, ph,      &
                      rhobar, qv, full3d, istatus,           &
                      idealopt, refmos3d)

!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Read wrf variable from wrf output in wrf array
!
!-----------------------------------------------------------------------

  USE model_precision

  IMPLICIT NONE

  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions
  INTEGER :: nscalar

  CHARACTER(LEN=256) :: file_name

  REAL :: u (nx,ny,nz)         ! x component of perturbation velocity
                                     ! (m/s)
  REAL :: v (nx,ny,nz)         ! y component of perturbation velocity
                                     ! (m/s)
  REAL :: w (nx,ny,nz)         ! vertical component of perturbation
                                     ! velocity in Cartesian coordinates
                                     ! (m/s).
  REAL :: pt (nx,ny,nz)        ! perturbation potential temperature (K)

  REAL :: ph (nx,ny,nz)        ! Mass variale, geopotential for WRF
! REAL :: mu (nx,ny)                 ! perturbation dry air mass in column (Pa)

  REAL :: qv(nx,ny,nz)         ! perturbation water vapor mixing ratio
                                     ! (kg/kg)
  REAL :: full3d(nx,ny,nz,nscalar)
! REAL :: qscalar(nx,ny,nz,nscalar)

  REAL :: ptbar (nx,ny,nz)     ! Base state potential temperature (K)
  REAL :: rhobar(nx,ny,nz)     ! Base state air density (kg/m**3)
  REAL :: pp    (nx,ny,nz)     ! perturbation pressure (Pascal)
  REAL :: pbar  (nx,ny,nz)     ! Base state pressure (Pascal)

  INTEGER, OPTIONAL, INTENT(IN)         :: idealopt
  REAL,    OPTIONAL, INTENT(OUT)        :: refmos3d (nx,ny,nz)

  CHARACTER(LEN=*), INTENT(IN) :: qnames(nscalar)

  INTEGER :: istatus

!-----------------------------------------------------------------------

  INCLUDE 'mp.inc'

  REAL, PARAMETER :: gravity = 9.81

!-----------------------------------------------------------------------
!
! Misc. Local variables
!
!-----------------------------------------------------------------------

  INTEGER :: nx_wrf, ny_wrf, nz_wrf
  INTEGER :: nxlg_wrf, nylg_wrf, nxd_wrf, nyd_wrf

  INTEGER :: ncmprx, ncmpry
  INTEGER :: i,j,k,nq,numdigit

  INTEGER :: refmosaic

  LOGICAL :: multifile, patch_split

  CHARACTER(LEN=40)  :: qnames_wrf
  CHARACTER(LEN=256) :: message
  CHARACTER(LEN=19 ) :: datestr
  INTEGER            :: itime, io_form

  INTEGER,  ALLOCATABLE :: fHndl(:,:)
  REAL(SP), ALLOCATABLE :: temd(:,:,:), var_wrf1d(:)
  REAL(P),  ALLOCATABLE :: var_wrf(:,:,:)!,var_wrf1(:,:,:)
  REAL(P),  ALLOCATABLE :: qv_wrf(:,:,:), ph_wrf(:,:,:), pt_wrf(:,:,:)
  REAL(P),  ALLOCATABLE :: pb_wrf(:,:,:), rb_wrf(:,:,:), ref_wrf(:,:,:)
  REAL(P),  ALLOCATABLE :: mub_wrf(:,:), mu_wrf(:,:), znu_wrf(:), rdnw_wrf(:)

  REAL(P),  ALLOCATABLE :: tem1(:,:,:)

  REAL(P) :: p_top

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  nproc_x_in = 1
  nproc_y_in = 1
! IF (nproc_x_in == 1 .AND. nproc_y_in == 1) THEN
    multifile = .FALSE.
    ncmprx = 1
    ncmprx = 1
! ELSE
!   multifile = .TRUE.
! END IF

  refmosaic = 0
  IF (PRESENT(idealopt)) THEN
    IF (idealopt == 0) THEN    ! not ideal experiments
      refmosaic=1
    END IF
  END IF

  IF (nproc_x_in >= nproc_x .AND. nproc_y_in >= nproc_y) THEN
    patch_split = .FALSE.
    IF (mod(nproc_x_in,nproc_x) /= 0 .OR. mod(nproc_y_in,nproc_y) /= 0) THEN
      message = 'ERROR: nproc_x_in (nproc_y_in) must be a multiple of nproc_x (nproc_y).'
      istatus = -1
    ELSE
      ncmprx = nproc_x_in / nproc_x
      ncmpry = nproc_y_in / nproc_y
    END IF
  ELSE IF (nproc_x_in < nproc_x .AND. nproc_y_in < nproc_y) THEN
    patch_split = .TRUE.
    IF (mod(nproc_x,nproc_x_in) /= 0 .OR. mod(nproc_y,nproc_y_in) /= 0) THEN
      message = 'ERROR: nproc_x_in (nproc_y_in) must be a factor of nproc_x (nproc_y).'
      istatus = -2
    ELSE
      ncmprx = nproc_x / nproc_x_in
      ncmpry = nproc_y / nproc_y_in
    END IF
  ELSE
    message = 'ERROR: unsupported combination of nproc_x_in and nproc_x, or nproc_y_in and nproc_y.'
    istatus = -3
  END IF

  9999 CONTINUE

  IF (istatus /= 0) THEN
    WRITE(*,'(1x,a)')       TRIM(message)
    WRITE(*,'(7x,2(a,I4))') ' nproc_x_in = ',nproc_x_in, ', nproc_x = ', nproc_x
    WRITE(*,'(7x,2(a,I4))') ' nproc_y_in = ',nproc_y_in, ', nproc_y = ', nproc_y
    istatus = -1
    !RETURN
    CALL arpsstop('ERROR mpi parameters',1)
  END IF

!-----------------------------------------------------------------------
!
! Dimension maps
!
!-----------------------------------------------------------------------
  nx_wrf = nx-2       ! staggered dimensions
  ny_wrf = ny-2
  nz_wrf = nz-2

  nxlg_wrf = (nx_wrf-1)*nproc_x + 1     ! WRF domain indices
  nylg_wrf = (ny_wrf-1)*nproc_y + 1

  IF (multifile) THEN
    nxd_wrf = (nxlg_wrf-1) / nproc_x_in + 1
    nyd_wrf = (nylg_wrf-1) / nproc_y_in + 1
  ELSE
    nxd_wrf = nxlg_wrf    ! dataset size in the file, staggered
    nyd_wrf = nylg_wrf
  END IF

  IF (patch_split) THEN
    !
    ! It is required that (nxd_wrf-1) is dividable with ncmprx
    !
    IF (MOD(nxd_wrf-1,ncmprx) /= 0 .OR. MOD(nyd_wrf-1,ncmpry) /= 0) THEN
      istatus = -4
      message = 'ERROR: dimension sizes does not fit with the number of processes.'
      GOTO 9999
    END IF

  END IF

  ALLOCATE(fHndl(ncmprx,ncmpry), STAT = istatus)
  ALLOCATE(var_wrf (nx_wrf, ny_wrf, nz_wrf), STAT = istatus)
! ALLOCATE(var_wrf1(nx_wrf, ny_wrf, nz_wrf), STAT = istatus)
  ALLOCATE(temd    (nxd_wrf,nyd_wrf,nz_wrf), STAT = istatus)
  ALLOCATE(var_wrf1d(nz_wrf),                STAT = istatus)

  ALLOCATE(ph_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus)
  ALLOCATE(qv_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus)
  ALLOCATE(pt_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus)
  ALLOCATE(pb_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus)
  ALLOCATE(rb_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus)
  IF (refmosaic==1) ALLOCATE(ref_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus)

  ALLOCATE(mub_wrf (nx_wrf,ny_wrf),    STAT = istatus)
  ALLOCATE(mu_wrf  (nx_wrf,ny_wrf),    STAT = istatus)
  ALLOCATE(znu_wrf (nz_wrf-1),         STAT = istatus)
  ALLOCATE(rdnw_wrf(nz_wrf-1),         STAT = istatus)

  ALLOCATE(tem1    (nx,ny,nz),         STAT = istatus)

  io_form  = 7  ! hard-coded for 7 at present
  numdigit = 4
  CALL open_wrf_files(file_name,io_form,patch_split,multifile,1,        &
                     ncmprx,ncmpry,numdigit,fHndl)

!-----------------------------------------------------------------------
! Initialize data array
!-----------------------------------------------------------------------

! variables used for analysis
  u  = 0.0
  v  = 0.0
  w  = 0.0
  pt = 0.0
  pp = 0.0
  ph = 0.0
  full3d = 0.0

  datestr = '2012-10-25_16:22:00'
  itime   = 1

!-----------------------------------------------------------------------
!
!  Horizontal wind
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'U','X',                                &
                  nx_wrf,ny_wrf,nz_wrf,var_wrf,                         &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'X',var_wrf,                 &
                      nx,ny,nz,u,tem1,istatus)

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'V','Y',                                &
                  nx_wrf,ny_wrf,nz_wrf,var_wrf,                         &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'Y',var_wrf,                 &
                      nx,ny,nz,v,tem1,istatus)

! ubar    = 0.0
! vbar    = 0.0

!-----------------------------------------------------------------------
!
!  Vertical wind
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'W','Z',                                &
                  nx_wrf,ny_wrf,nz_wrf,var_wrf,                         &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'Z',var_wrf,                 &
                      nx,ny,nz,w,tem1,istatus)

!-----------------------------------------------------------------------
!
! Read potential temperature
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'T','0',                                &
                  nx_wrf,ny_wrf,nz_wrf,pt_wrf,                          &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

! pt=pt_wrf(:,:,:)+300.0
! ptbar=0.0

!-----------------------------------------------------------------------
!
! Read Air density
!
!-----------------------------------------------------------------------

! CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
!                 datestr,itime,'ALT','0',                                &
!                 nx_wrf,ny_wrf,nz_wrf,var_wrf,                          &
!                 nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

! DO k=1,nz_wrf
! DO j=1,ny_wrf
! DO i=1,nx_wrf

!    IF (var_wrf(i,j,k)/=0) rhobar(i,j,k)=1/var_wrf(i,j,k)

! END DO
! END DO
! END DO

!-----------------------------------------------------------------------
!
! Read Pressure
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'P','0',                                &
                  nx_wrf,ny_wrf,nz_wrf,var_wrf,                         &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'PB','0',                               &
                  nx_wrf,ny_wrf,nz_wrf,pb_wrf,                          &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

! var_wrf(:,:,:) = pb_wrf(:,:,:) + var_wrf(:,:,:)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',var_wrf,                 &
                      nx,ny,nz,pp,tem1,istatus)
  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',pb_wrf,                  &
                      nx,ny,nz,pbar,tem1,istatus)
! pbar=0.0

!-----------------------------------------------------------------------
!
! Physical height
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'PH','Z',                               &
                  nx_wrf,ny_wrf,nz_wrf,ph_wrf,                          &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'Z',ph_wrf,                  &
                      nx,ny,nz,ph,tem1,istatus)

!-----------------------------------------------------------------------
!
! Read water vapor mixing ratio and Convert to specific humidity
!
!-----------------------------------------------------------------------

  CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'QVAPOR','0',                           &
                  nx_wrf,ny_wrf,nz_wrf,qv_wrf,                          &
                  nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',qv_wrf,                  &
                      nx,ny,nz,qv,tem1,istatus)

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'MUB','0',                              &
                  nx_wrf,ny_wrf,mub_wrf,                                &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL get_wrf_2d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'MU','0',                               &
                  nx_wrf,ny_wrf,mu_wrf,                                 &
                  nxd_wrf,nyd_wrf,temd,istatus)

  CALL get_wrf_1d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'ZNU',                                  &
                  nz_wrf,var_wrf1d,nz_wrf-1,istatus)

  DO k = 1, nz_wrf-1
    znu_wrf(k) = var_wrf1d(k)
  END DO

  CALL get_wrf_1d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,    &
                  datestr,itime,'RDNW',                                 &
                  nz_wrf,var_wrf1d,nz_wrf-1,istatus)

  DO k = 1, nz_wrf-1
    rdnw_wrf(k) = var_wrf1d(k)
  END DO

  CALL get_wrf_s(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,     &
                  datestr,itime,'P_TOP',p_top,istatus)

  CALL cal_rho_p(nx_wrf,ny_wrf,nz_wrf,mub_wrf,mu_wrf,rdnw_wrf,          &
                 p_top,znu_wrf,qv_wrf,ph_wrf,pt_wrf,                    &
                 rb_wrf,istatus)

  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',rb_wrf,                  &
                      nx,ny,nz,rhobar,tem1,istatus)

  pt_wrf = pt_wrf + 300.0
  CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',pt_wrf,                  &
                      nx,ny,nz,pt,tem1,istatus)
  ptbar = 0.0

  DO nq = 1, nscalar

    IF ( ANY ( qnames=="QCLOUD" ) ) THEN
       qnames_wrf = qnames(nq)
    ELSE
      SELECT CASE ( TRIM(qnames(nq)) )
      CASE ('qc')
        qnames_wrf = 'QCLOUD'
      CASE ('qr')
        qnames_wrf = 'QRAIN'
      CASE ('qi')
        qnames_wrf = 'QICE'
      CASE ('qs')
        qnames_wrf = 'QSNOW'
      CASE ('qg')
        qnames_wrf = 'QGRAUP'
      CASE ('qh')
        qnames_wrf = 'QHAIL'
      CASE DEFAULT
        WRITE(*,'(1x,3a)') 'ERROR: support of ',TRIM(qnames(nq)),' is still not added.'
        CONTINUE
      END SELECT
    END IF

    CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,  &
                    datestr,itime,qnames_wrf,'0',                       &
                    nx_wrf,ny_wrf,nz_wrf,var_wrf,                       &
                    nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

    IF (istatus == 0) THEN

      SELECT CASE (qnames_wrf)
      CASE ('QCLOUD','QRAIN','QICE','QSNOW','QGRAUP','QHAIL')
        WHERE(var_wrf < 0.0) var_wrf = 0.0
        WHERE(var_wrf > 1.0) var_wrf = 1.0
      END SELECT

      CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',var_wrf,             &
                          nx,ny,nz,full3d(:,:,:,nq),tem1,istatus)

    END IF

!   DO k=1,nz_wrf-1
!     DO j=1,ny_wrf-1
!       DO i=1,nx_wrf-1

!       full3d(:,:,:,nq)=var_wrf(:,:,:)

!       END DO
!     END DO
!   END DO

  END DO

!-----------------------------------------------------------------------
!
! Read 3D Reflectivity Mosaic
!
!-----------------------------------------------------------------------

  IF (refmosaic == 1) THEN

    CALL get_wrf_3d(fHndl,io_form,multifile,patch_split,ncmprx,ncmpry,  &
                    datestr,itime,'REFMOSAIC3D','0',                    &
                    nx_wrf,ny_wrf,nz_wrf,ref_wrf,                       &
                    nxd_wrf,nyd_wrf,nz_wrf,temd,istatus)

    CALL array_wrf2arps(nx_wrf,ny_wrf,nz_wrf,'0',ref_wrf,               &
                        nx,ny,nz,refmos3d,tem1,istatus)

  END IF

!-----------------------------------------------------------------------
! Finishing
!-----------------------------------------------------------------------

  CALL close_wrf_files(fHndl,io_form,patch_split,1,ncmprx,ncmpry)

  DEALLOCATE ( fHndl ,temd, var_wrf, var_wrf1d )
  DEALLOCATE ( ph_wrf, qv_wrf, pt_wrf, pb_wrf, rb_wrf )
  IF (refmosaic==1) DEALLOCATE(ref_wrf)
  DEALLOCATE ( mub_wrf, mu_wrf, znu_wrf, rdnw_wrf)
  DEALLOCATE ( tem1 )

  RETURN
END SUBROUTINE readwrfvar

!
!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE DUMPWRFVAR                 ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE dumpwrfvar(infilename,outdir,filename,nx,ny,nz, u ,v ,w , &
                      pt, ph, qv, qscalar, istatus)



  USE wrfio_api

  IMPLICIT NONE

!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------
!
  !INCLUDE 'indtflg.inc'
  INCLUDE 'globcst.inc'
  !INCLUDE 'grid.inc'          ! Grid & map parameters.
  INCLUDE 'mp.inc'            ! mpi parameters.
  INCLUDE 'phycst.inc'

!-----------------------------------------------------------------------
!
!  Variable Declarations.
!
!-----------------------------------------------------------------------
!
  INTEGER :: nx,ny,nz          ! Number of grid points in 3 directions

  CHARACTER(LEN=256) :: infilename
  CHARACTER(LEN=256) :: outdir, filename

  REAL :: u (nx,ny,nz)      ! x component of velocity (m/s)
  REAL :: v (nx,ny,nz)      ! y component of velocity (m/s)
  REAL :: w (nx,ny,nz)      ! vertical component of velocity
                                  ! in Cartesian coordinates (m/s).
  REAL :: pt(nx,ny,nz)      ! potential temperature (K)
! REAL :: p(nx,ny,nz)       ! Pressure (Pascal)
  REAL :: ph(nx,ny,nz)      ! Geopotential Height (m**2/s**2)
  REAL :: qv(nx,ny,nz)      ! Mixing ratio

  REAL :: qscalar(nx,ny,nz,nscalar)

  INTEGER :: istatus           ! Return status indicator

!-----------------------------------------------------------------------

  INTEGER :: nx_wrf, ny_wrf, nz_wrf, nq

  CHARACTER(LEN=40) :: qnames_wrf
  CHARACTER(LEN=19) :: timestr

  REAL, ALLOCATABLE :: var_wrf(:,:,:)

  !CHARACTER(LEN=256) :: infilename, indir
  LOGICAL :: hard_copy = .FALSE.
  CHARACTER(LEN=80),  PARAMETER :: wrftitle    = 'OUTPUT FROM CSM3DVAR for WRFV3.5.1'

  INTEGER :: wrfoutopt

!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  !indir = './'
  !infilename = 'wrfinput_d01'

  IF (hard_copy) THEN
    IF (myproc == 0) THEN
      CALL wrf_file_hard_copy( 1,1,4,                                   &
                               infilename,outdir,filename,istatus )
    END IF
    CALL mpbarrier
  END IF

!-----------------------------------------------------------------------
!
! First, set parameters for house keeping
!
!-----------------------------------------------------------------------

  WRITE(timestr,'(I4.4,5(a,I2.2))') year,'-',month,'-',day,'_',hour,':',minute,':', second

  nx_wrf = nx-2
  ny_wrf = ny-2
  nz_wrf = nz-2

  ALLOCATE( var_wrf(nx_wrf,ny_wrf,nz_wrf), STAT = istatus )

  IF (hdmpfmt == 0 .OR. hdmpfmt == 100 .OR. hdmpfmt == 1) THEN
    wrfoutopt = hdmpfmt
  ELSE
    wrfoutopt = 1
  END IF

  CALL wrf_io_init( wrfoutopt,nx_wrf,ny_wrf,nz_wrf,hard_copy,4, &
                    outdir,filename,istatus )

!-----------------------------------------------------------------------
!
! 1. Copy existing fields to WRF input file, exluding the analyzed fields
!
!-----------------------------------------------------------------------

  CALL wrf_file_define( infilename, 4, wrftitle, timestr, -1, istatus )
  ! define wrfinput file and copy static fields

!-----------------------------------------------------------------------
!
! 2. Write out analyzed fields
!
!-----------------------------------------------------------------------

  ! analysis variables

  CALL array_arps2wrf(nx,ny,nz,u,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'U', var_wrf, 'X', istatus )

  CALL array_arps2wrf(nx,ny,nz,v,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'V', var_wrf, 'Y', istatus )

  CALL array_arps2wrf(nx,ny,nz,w,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'W', var_wrf, 'Z', istatus )

! CALL array_arps2wrf(nx,ny,nz,p,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
! CALL wrf_file_write3d ( 'P', var_wrf,' ', istatus )

  CALL array_arps2wrf(nx,ny,nz,ph,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'PH', var_wrf, 'Z', istatus )

  CALL array_arps2wrf(nx,ny,nz,pt,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  var_wrf = var_wrf - 300.0
  CALL wrf_file_write3d ( 'T', var_wrf, ' ', istatus )

  CALL array_arps2wrf(nx,ny,nz,qv,nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
  CALL wrf_file_write3d ( 'QVAPOR', var_wrf, ' ', istatus )

  ! microphysical variables (TOBEDONE)
  DO nq = 1, nscalar

    IF ( ANY ( qnames=="QCLOUD" ) ) THEN
       qnames_wrf = qnames(nq)
    ELSE
      SELECT CASE ( TRIM(qnames(nq)) )
      CASE ('qc')
        qnames_wrf = 'QCLOUD'
      CASE ('qr')
        qnames_wrf = 'QRAIN'
      CASE ('qi')
        qnames_wrf = 'QICE'
      CASE ('qs')
        qnames_wrf = 'QSNOW'
      CASE ('qg')
        qnames_wrf = 'QGRAUP'
      CASE ('qh')
        qnames_wrf = 'QHAIL'
        !qnames_wrf = 'QGRAUP'
      CASE DEFAULT
        WRITE(*,'(1x,3a)') 'ERROR: support of ',TRIM(qnames(nq)),' is still not added.'
        CONTINUE
      END SELECT
    END IF

    CALL array_arps2wrf(nx,ny,nz,qscalar(:,:,:,nq),nx_wrf,ny_wrf,nz_wrf,var_wrf,istatus)
    CALL wrf_file_write3d ( qnames_wrf, var_wrf, ' ', istatus )

  END DO

!-----------------------------------------------------------------------
!
! 3. Close opened files
!
!-----------------------------------------------------------------------

  CALL wrf_io_final( istatus )

  DEALLOCATE( var_wrf )
  RETURN
END SUBROUTINE dumpwrfvar
