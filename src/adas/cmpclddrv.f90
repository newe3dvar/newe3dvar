SUBROUTINE cmpclddrv(nx,ny,nz,nxlg,nylg,i4timanx,                       &
           xs,ys,xslg,yslg,zs,j3,hterain,latgr,longr,                   &
           pprt,ptprt,qv,qscalar,w,w_index,                             &
           pbar,ptbar,qvbar,rhobar,                                     &
           nobsng,indexsng,stnsng,isrcsng,csrcsng,xsng,ysng,            &
           timesng,latsng,lonsng,hgtsng,                                &
           kloud,store_amt,store_hgt,                                   &
           stnrad,isrcrad,latrad,lonrad,elvrad,                         &
           istat_radar,ref_mos_3d,zdr_mos_3d,kdp_mos_3d,rhv_mos_3d,     &
           qscalar_cld,clouds_3d,ctmp_3d,w_cld,cloud_ceiling,vil,       &
           cldpcp_type_3d,icing_index_3d,l_mask_pcptype,                &
           p_3d,t_3d,rh_3d,qvprt_old,qw_old,                            &
           pseudoQobs_opt,pttem,qvtem,qstem,tem1,tem2,tem3)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Driver for complex cloud analysis code.
!  This subroutine serves as an interface between the ADAS
!  code and the cloud code.
!
!-----------------------------------------------------------------------
!
!  AUTHOR:
!  Keith Brewster, CAPS
!  Based on code originally installed directly into adas.f
!  by Jian Zhang, CAPS
!  February, 1998
!
!  MODIFICATION HISTORY:
!  05/05/1998 Jian Zhang
!  Abandoned the cloud grid, using the ARPS grid instead.
!
!  08/30/2000 D.H. Wang
!  Added option for Ferrier formulation of cloud water.
!
!  11/02/2000 K. Brewster
!  Modifications to allow tiny, insignificant differences in
!  mapping parameters between radar and ADAS.  Minor clean-up.
!
!  02/05/2001 Richard Carpenter, WDT
!  Precipitate determined from radar enhances, rather than replaces, the
!  background field.
!
!-----------------------------------------------------------------------
!  July, 2001 (K. Brewster)
!  Changed wrtvar calls to wrtvar1 to match new routine for writing
!  3-D arrays to be read by arpsplt as an arbitary array.
!
!  Some clean-up of comments and lines that had been commented-out.
!
!  Added new latent heating options, cldptopt = 4 and 5.
!  cldptop = 4 replaces temperature with cloud parcel temperature
!  (with entrainment) everywhere there is cloud.
!  cldptopt =5 replaces adusts temperature to cloud parcel temperature
!  beginning where w=-0.2 m/s ramping to full application of cloud parcel
!  temperature where w>=0.
!
!  11/01/2001 (K. Brewster)
!  Restructured the building of the reflectivity mosaic and moved
!  it into new subroutine, refmosaic, replacing rad_patch_fill.
!
!  02/07/2006 (K. W. Thomas)
!  MPI update.
!
!  04/27/2010 (K. Brewster)
!  Restructured handling of ref_mos_3d to make it an input to this
!  routine, avoid the necessity of re-reading the radar files.
!
!  04/22/2016 (K. Brewster)
!  Added cldqvopt = 2 processing for qv adjustment dependency on
!  on w and temperature following the work of ChongChi Tong.
!
!-----------------------------------------------------------------------
!
  USE my3mom_fncs_mod, ONLY : gammaDP
  USE module_PseudoQobs
  USE module_radarobs, ONLY : mx_rad

  IMPLICIT NONE
  INCLUDE 'adas.inc'
  INCLUDE 'bndry.inc'
  INCLUDE 'mp.inc'
  INCLUDE 'globcst.inc'
!
!-----------------------------------------------------------------------
!
!  Grid variables
!
!-----------------------------------------------------------------------
!
  INTEGER, INTENT(IN) :: nx,ny,nz
  INTEGER, INTENT(IN) :: nxlg, nylg
  INTEGER, INTENT(IN) :: i4timanx   ! analysis time in seconds from 00:00 UTC
                                    ! Jan. 1, 1960
!
  REAL, INTENT(IN) :: xs(nx)
  REAL, INTENT(IN) :: ys(ny)
  REAL, INTENT(IN) :: xslg(nxlg)
  REAL, INTENT(IN) :: yslg(nylg)
  REAL, INTENT(IN) :: zs(nx,ny,nz)
  REAL, INTENT(IN) :: j3(nx,ny,nz)
  REAL, INTENT(IN) :: hterain(nx,ny)
  REAL, INTENT(IN) :: latgr(nx,ny)
  REAL, INTENT(IN) :: longr(nx,ny)
  INTEGER, INTENT(IN) :: w_index(nx,ny)

  REAL, INTENT(IN)    :: pprt(nx,ny,nz)
  REAL, INTENT(INOUT) :: ptprt(nx,ny,nz)
  REAL, INTENT(INOUT) :: qv(nx,ny,nz)
  REAL, INTENT(INOUT) :: qscalar(nx,ny,nz,nscalar)
  REAL, INTENT(INOUT) :: w(nx,ny,nz)
  REAL, INTENT(IN)    :: pbar(nx,ny,nz)
  REAL, INTENT(IN)    :: ptbar(nx,ny,nz)
  REAL, INTENT(IN)    :: qvbar(nx,ny,nz)
  REAL, INTENT(IN)    :: rhobar(nx,ny,nz)

!
!-----------------------------------------------------------------------
!
!  Single-level data
!
!-----------------------------------------------------------------------
!
  INTEGER,           INTENT(IN) :: nobsng
  INTEGER,           INTENT(IN) :: indexsng(nobsng)
  CHARACTER (LEN=5), INTENT(IN) :: stnsng(mx_sng)
  INTEGER,           INTENT(IN) :: isrcsng(mx_sng)
  CHARACTER (LEN=8), INTENT(IN) :: csrcsng(mx_sng)
  REAL,              INTENT(IN) :: xsng(mx_sng)
  REAL,              INTENT(IN) :: ysng(mx_sng)
  INTEGER,           INTENT(IN) :: timesng(mx_sng)
  REAL,              INTENT(IN) :: latsng(mx_sng)
  REAL,              INTENT(IN) :: lonsng(mx_sng)
  REAL,              INTENT(IN) :: hgtsng(mx_sng)
  INTEGER,           INTENT(IN) :: kloud(mx_sng)
  CHARACTER (LEN=4), INTENT(IN) :: store_amt(mx_sng,5)
  REAL,              INTENT(IN) :: store_hgt(mx_sng,5)
!
!-----------------------------------------------------------------------
!
!  Radar site variables
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=5), INTENT(IN) :: stnrad(mx_rad)
  INTEGER,           INTENT(IN) :: isrcrad(0:mx_rad)
  REAL,              INTENT(IN) :: latrad(mx_rad)
  REAL,              INTENT(IN) :: lonrad(mx_rad)
  REAL,              INTENT(IN) :: elvrad(mx_rad)
  !INTEGER,           INTENT(IN) :: ixrad(mx_rad)
  !INTEGER,           INTENT(IN) :: jyrad(mx_rad)
!
!-----------------------------------------------------------------------
!
!  3D radar array
!
!-----------------------------------------------------------------------
!
  INTEGER, INTENT(IN)    :: istat_radar
  REAL,    INTENT(IN)    :: ref_mos_3d(nx,ny,nz)
  REAL,    INTENT(INOUT) :: zdr_mos_3d(nx,ny,nz)
  REAL,    INTENT(IN)    :: kdp_mos_3d(nx,ny,nz)
  REAL,    INTENT(IN)    :: rhv_mos_3d(nx,ny,nz)

  REAL                   :: lkdp(nx,ny,nz)

!
!-----------------------------------------------------------------------
!
!  Output variables
!
!-----------------------------------------------------------------------
!
  REAL,    INTENT(OUT) :: qscalar_cld(nx,ny,nz,nscalar)
  REAL,    INTENT(OUT) :: clouds_3d (nx,ny,nz)  ! final 3D fractnl cloud cover analysis.
  REAL,    INTENT(OUT) :: ctmp_3d (nx,ny,nz)    ! cloud temperature (K)
  REAL,    INTENT(OUT) :: w_cld (nx,ny,nz)      ! vertical velocity (m/s) in clouds

  REAL,    INTENT(OUT) :: cloud_ceiling (nx,ny) ! cloud ceiling heights( m AGL)
  REAL,    INTENT(OUT) :: vil (nx,ny) ! vertically intregrated cloud liquid/ice water

  INTEGER, INTENT(OUT) :: cldpcp_type_3d(nx,ny,nz) ! cloud/precip type field
  INTEGER, INTENT(OUT) :: icing_index_3d(nx,ny,nz) ! icing severity index
  LOGICAL, INTENT(OUT) :: l_mask_pcptype(nx,ny)    ! analyze precip type using simulated
                                      ! radar data?
!
!-----------------------------------------------------------------------
!
!  Temporary arrays
!
!-----------------------------------------------------------------------
!
  REAL, INTENT(OUT) :: p_3d     (nx,ny,nz)
  REAL, INTENT(OUT) :: t_3d     (nx,ny,nz)
  REAL, INTENT(OUT) :: rh_3d    (nx,ny,nz)
  REAL, INTENT(OUT) :: qvprt_old(nx,ny,nz)
  REAL, INTENT(OUT) :: qw_old   (nx,ny,nz)
  REAL, INTENT(OUT) :: tem1     (nx,ny,nz)
  REAL, INTENT(OUT) :: tem2     (nx,ny,nz)
  REAL, INTENT(OUT) :: tem3     (nx,ny,nz)

  INTEGER, INTENT(IN) :: pseudoQobs_opt

  REAL, INTENT(OUT) :: pttem(nx,ny,nz), qvtem(nx,ny,nz)
  REAL, INTENT(OUT) :: qstem(nx,ny,nz,nscalar)
!
!-----------------------------------------------------------------------
!
!  Misc local variables
!
!-----------------------------------------------------------------------
!
  REAL :: f_qvsat
  REAL, PARAMETER :: rhsubsat=0.950
  REAL, PARAMETER :: smfct1=0.5

  REAL, PARAMETER :: epsdif=0.0001

!
  CHARACTER (LEN=6) :: varid
  CHARACTER (LEN=20) :: varname
  CHARACTER (LEN=20) :: varunits
  INTEGER :: i,j,k,ii,jj,kk,nq,irad
  INTEGER :: istatus_cld,istatus_lwc,istatus_pcp,istatwrt,wrfopt
  REAL :: xrad,yrad,qvsat,radri,radrj,p0inv,ppi,qpcp,rh2qv,wgt
  REAL :: tgrid,dqv_prt,dqw,qw_new,arg,rh,wratio,ptdiff,ptcld
!
  INTEGER :: strhopt_rad                  ! streching option
  INTEGER :: mapproj_rad                  ! map projection indicator
  REAL :: dx_rad,dy_rad,dz_rad,dzmin_rad  ! grid spcngs
  REAL :: ctrlat_rad,ctrlon_rad           ! central lat and lon
  REAL :: tlat1_rad,tlat2_rad,tlon_rad    ! true lat and lon
  REAL :: scale_rad                       ! map scale factor
  REAL :: max_pt_adj,extrnz,extrnz1,extr1,relh,frac
  REAL :: min_pt_adj
  REAL :: cldpcpfrc,onemcpf,cldtot,pcptot,cldlim,qsum
  CHARACTER (LEN=72) :: warn_string

! constant in equation for radar reflectivity
  REAL :: Gr, Gi, Gs, Gg, Gh

  REAL, PARAMETER :: pi = 3.14159265

! cldptopt == 10, work arrays
  INTEGER  :: zdr_col_depth(nx,ny)
!  REAL    :: zdr_col_depth(nx,ny)
  INTEGER :: zdr_col_wgt
  INTEGER :: zdr_col_wgt_diag
  INTEGER :: crit_zdr
  INTEGER :: crit_rhv,crit_bkg
  REAL, DIMENSION(-1:1, -1:1) :: maskedarray_zdr
  REAL, DIMENSION(-1:1, -1:1) :: maskedarray_rhv,maskedarray_rhv_bkg
  REAL, DIMENSION(-1:1, -1:1, -1:1) :: maskedarray_refobs

  REAL    :: rdummy
  INTEGER :: i1, j1
  ! ---- add by anwei lai
  INTEGER :: rh100_flag
  INTEGER :: rh100_flag_bkg  ! indicate the background is convective or not
  INTEGER :: rh_thresh
  REAL    :: dwpt  !function
  REAL, PARAMETER :: gamma_d = 9.8/1004.0     ! dry adiabatic lapse rate (K/m)
  INTEGER :: rh100_flag_top_lvl,lcl_model_lvl
  REAL    :: t_model_surf_c,td_model_surf_c,rh_model_surf_c
  REAL    :: z_lcl(nx,ny)

  REAL, PARAMETER :: vil_4d7_index =4.0/7.0
  REAL    :: lw,db,ZE
  REAL    :: lw_bkg,db_bkg,ZE_bkg
  REAL    :: vil_from_dbz(nx,ny)
  REAL    :: vil_from_bkg(nx,ny)
  REAL    :: ref_bkg(nx,ny,nz)
  REAL    :: max_refobs,min_refobs
  INTEGER :: num_norain,num_miss
  !===== add by anwei lai
  INTEGER :: istatus
!
  !===== add by chq
  REAL    :: melting_height,zdr_base,melting_z
  !REAL    :: zdrtmp(nx,ny)
  REAL, ALLOCATABLE  :: ref_large_3d(:,:,:)
!-----------------------------------------------------------------------
!
!  Include file
!
!-----------------------------------------------------------------------
!
!  INCLUDE 'globcst.inc'
  INCLUDE 'phycst.inc'
  INCLUDE 'grid.inc'        ! Grid parameters
!
!-----------------------------------------------------------------------
!
!  Specify the scheme and products wanted.
!
!-----------------------------------------------------------------------
!
  INTEGER :: iflag_slwc
  LOGICAL :: l_flag_incld_w ,l_flag_cldtyp,l_flag_icing
!
!-----------------------------------------------------------------------
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of the cloud analysis procedure.
!  Some initializations.
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  iflag_slwc = 13 ! New Smith-Fedds model for Liquid Water Content
  l_flag_incld_w = .true.  ! Analyze in-cloud w-field
  l_flag_cldtyp = .true.   ! Diagnose cloud type field
  l_flag_icing = .true.  ! Analyzing icing severity
!
!-----------------------------------------------------------------------
!
!  Compute state variables on grid including k=1, nz-1, nz, via
!  linear extrapolation, except RH is taken to be constant.
!  For simplicity use dry air density.
!  Save the old buoyancy field.
!
!-----------------------------------------------------------------------
!
! OpenMP changed loop order to j,k,i:
!$OMP PARALLEL DO PRIVATE(i,j,k,qvsat)
  DO j=1,ny-1
    DO k=2,nz-1
      DO i=1,nx-1
        p_3d(i,j,k) = pprt(i,j,k)+pbar(i,j,k)
        t_3d(i,j,k) = (ptprt(i,j,k)+ptbar(i,j,k))*                      &
                      ((pprt(i,j,k)+pbar(i,j,k))/p0)**rddcp
        !rhobar(i,j,k)=rhostr(i,j,k)/j3(i,j,k)
        qvsat=f_qvsat(p_3d(i,j,k),t_3d(i,j,k))
        rh_3d(i,j,k)=100.*MAX(0.,(qv(i,j,k)/qvsat)) !eidted by hq
        !rh_3d(i,j,k)=100.*MIN(1.,MAX(0.,(qv(i,j,k)/qvsat)))
        qvprt_old(i,j,k) = qv(i,j,k) - qvbar(i,j,k)

        qw_old(i,j,k) = 0.0
        DO nq=1,nscalarq
          qw_old(i,j,k) = qw_old(i,j,k) + qscalar(i,j,k,nq)
        END DO
      END DO
    END DO
  END DO

! OpenMP:
!$OMP PARALLEL DO PRIVATE(j,i,extrnz1,extrnz,extr1)
  DO j = 1, ny-1
    DO i = 1, nx-1
      extrnz1 = (zs(i,j,nz-1)-zs(i,j,nz-3))                             &
             / (zs(i,j,nz-2)-zs(i,j,nz-3))
      t_3d(i,j,nz-1) = t_3d(i,j,nz-2)                                   &
                       + (t_3d(i,j,nz-2)-t_3d(i,j,nz-3))*extrnz1
      p_3d(i,j,nz-1) = p_3d(i,j,nz-2)                                   &
                       + (p_3d(i,j,nz-2)-p_3d(i,j,nz-3))*extrnz1
      rh_3d(i,j,nz-1) = rh_3d(i,j,nz-2)

      extrnz = (zs(i,j,nz)-zs(i,j,nz-3))                                &
             / (zs(i,j,nz-2)-zs(i,j,nz-3))
      t_3d(i,j,nz) = t_3d(i,j,nz-3)                                     &
                       + (t_3d(i,j,nz-2)-t_3d(i,j,nz-3))*extrnz
      p_3d(i,j,nz) = p_3d(i,j,nz-3)                                     &
                       + (p_3d(i,j,nz-2)-p_3d(i,j,nz-3))*extrnz
      rh_3d(i,j,nz) = rh_3d(i,j,nz-2)

      extr1 = (zs(i,j,1)-zs(i,j,3))                                     &
             / (zs(i,j,2)-zs(i,j,3))
      t_3d(i,j,1) = t_3d(i,j,3)                                         &
                       + (t_3d(i,j,2)-t_3d(i,j,3))*extr1
      p_3d(i,j,1) = p_3d(i,j,3)                                         &
                       + (p_3d(i,j,2)-p_3d(i,j,3))*extr1
      rh_3d(i,j,1) = rh_3d(i,j,2)
    END DO
  END DO

  CALL edgfill( t_3d ,1,nx,1,nx-1, 1,ny,1,ny-1, 1,nz,1,nz)
  CALL edgfill( p_3d ,1,nx,1,nx-1, 1,ny,1,ny-1, 1,nz,1,nz)
  CALL edgfill(rh_3d ,1,nx,1,nx-1, 1,ny,1,ny-1, 1,nz,1,nz)
  CALL edgfill(   zs ,1,nx,1,nx-1, 1,ny,1,ny-1, 1,nz,1,nz)
!
!-----------------------------------------------------------------------
!
!  Initialize the cloud/precip array using the background values
!
!-----------------------------------------------------------------------
!

  qscalar_cld=qscalar

!
! Do not overwrite multi-moment fields
! This will be handled in the individual liquid water assignments
!
!
!-----------------------------------------------------------------------
!
!  Analyze 3D fractional cloud cover field by using
!  SAO, radar reflectivity, IR and VIS satellite data.
!
!-----------------------------------------------------------------------
!
  IF (myproc == 0) WRITE(6,*) ' Calling Cloud Cover Analysis: cloud_cv'
!
  clouds_3d = 0.0
  CALL cloud_cv (nx,ny,nz,i4timanx,curtim,dirname,runname(1:lfnkey),    &
            hdmpfmt,hdfcompr,lvldbg,                                    &
            xs,ys,zs,dx,dy,hterain,latgr,longr,                         &
            nxlg,nylg,xslg,yslg,                                        &
            p_3d,t_3d,rh_3d,                                            &
            nobsng,indexsng,stnsng,isrcsng,csrcsng,xsng,ysng,           &
            timesng,latsng,lonsng,hgtsng,                               &
            kloud,store_amt,store_hgt,                                  &
            ref_mos_3d,istat_radar,                                     &
            clouds_3d,cloud_ceiling,tem1,tem2,                          &
            istatus_cld)

  IF(istatus_cld /= 1) THEN
    WRITE(6,*) 'Bad status returned from cloud_cv, Aborting...'
    GO TO 999
  END IF

  IF (mp_opt > 0) THEN    ! Since clouds_3d will be used later
    CALL acct_interrupt(mp_acct)
    CALL mpsendrecv2dew(clouds_3d,nx,ny,nz,ebc,wbc,0,tem1)
    CALL mpsendrecv2dns(clouds_3d,nx,ny,nz,nbc,sbc,0,tem1)
  END IF

!
!-----------------------------------------------------------------------
!
!  Analyze 3D cloud liquid/ice water field.
!
!-----------------------------------------------------------------------
!
  IF (myproc == 0) WRITE(6,*) ' Calculating cloud liquid/ice water.'
!
!-----------------------------------------------------------------------
!
!  Analyzing cloud liquid water field
!
!-----------------------------------------------------------------------
!
  CALL cloud_lwc (nx,ny,nz,curtim,dirname,runname,                     &
                  clouds_3d,t_3d,rh_3d,p_3d,zs,                        &
                  istat_radar,ref_mos_3d,                              &
                  iflag_slwc,qscalar_cld(:,:,:,P_QC),                  &
                  qscalar_cld(:,:,:,P_QI),ctmp_3d,                     &
                  l_flag_incld_w,w_cld,                                &
                  l_mask_pcptype,l_flag_cldtyp,cldpcp_type_3d,         &
                  l_flag_icing,icing_index_3d,tem1,                    &
                  istatus_lwc)

  IF(istatus_lwc /= 1) THEN
    IF (myproc == 0) WRITE(6,*)' Bad status returned from cloud_lwc'
    GO TO 999
  END IF

  IF (mp_opt > 0) THEN    ! Note the computation in subroutine cloud_type_qc
    CALL acct_interrupt(mp_acct)
    CALL mpsendrecv2dew(clouds_3d,nx,ny,nz,ebc,wbc,0,tem1)
    CALL mpsendrecv2dns(clouds_3d,nx,ny,nz,nbc,sbc,0,tem1)
  END IF
!
!-----------------------------------------------------------------------
!
!  Calculate 3D Precipitation mixing ratio, now in kg/kg.
!
!-----------------------------------------------------------------------
!
  IF(istat_radar == 1) THEN

!    IF (cldqropt == 1) THEN
!
! Kessler's scheme
!
!     IF (myproc == 0) THEN
!       WRITE(6,'(a)') ' Computing Precip mixing ratio.'
!       WRITE(6,'(a)') ' Using Kessler radar reflectivity equations...'
!     END IF
!     CALL pcp_mxr (nx,ny,nz,t_3d,p_3d,zs,hterain,ref_mos_3d,           &
!                   cldpcp_type_3d,                                     &
!                   qr_cld,qs_cld,qh_cld,tem1,                          &
!                   cloudopt,refthr1,refthr2,hgtrefthr,istatus_pcp)

!   ELSE IF (cldqropt == 2) THEN
!
! Ferrier's scheme
!
!     IF (myproc == 0) THEN
!       WRITE(6,'(a)') ' Computing Precip mixing ratio.'
!       WRITE(6,'(a)') ' Using Ferrier radar reflectivity equations...'
!     END IF
!     CALL pcp_mxr_ferrier (nx,ny,nz,t_3d,p_3d,zs,hterain,ref_mos_3d,   &
!                  cldpcp_type_3d,                                      &
!                  qr_cld,qs_cld,qh_cld,tem1,                           &
!                  cloudopt,refthr1,refthr2,hgtrefthr,istatus_pcp)
!   END IF   !cldqropt=1 or 2


    IF (cldqropt < 100) THEN  !  ARPS microphysical schemes

      SELECT CASE (cldqropt)
        CASE (1)     ! Warm rain schemes
          CALL reflec2q_wrf(nx,ny,nz,zs,hterain,                        &
                            p_3d,t_3d,qv,ref_mos_3d,cldpcp_type_3d,     &
                            hgtrefthr,refthr1,refthr2,0,cloudopt,       &
                            qscalar_cld(1,1,1,P_QR),tem1,tem2,tem3)
          !CALL pcp_mxr (nx,ny,nz,t_3d,p_3d,zs,hterain,ref_mos_3d,           &
          !              cldpcp_type_3d,                                     &
          !              qscalar_cld(1,1,1,P_QR),qscalar_cld(1,1,1,P_QS),qscalar_cld(1,1,1,P_QG), tem1,&
          !              !qr_cld,qs_cld,qh_cld,                               &
          !              cloudopt,refthr1,refthr2,hgtrefthr,istatus_pcp)

        CASE (2:7)   ! Lin,Schultz,Straka Lin,or WSM6 schemes
          !CALL reflec(nx,ny,nz, rhobar, qscalar(:,:,:,P_QR),&
          !                                     qscalar(:,:,:,P_QS),&
          !                                     tem1(:,:,:),        &
          !                                     tem9 )
          !CALL pcp_mxr (nx,ny,nz,t_3d,p_3d,zs,hterain,                 &
          !          ref_mos_3d,cldpcp_type_3d,                         &
          !          qscalar_cld(1,1,1,P_QR),qscalar_cld(1,1,1,P_QS),   &
          !          qscalar_cld(1,1,1,P_QH),tem1,                      &
          !          cloudopt,refthr1,refthr2,hgtrefthr,istatus_pcp)
          IF( P_QH > 0 ) THEN
            CALL reflec2q_wrf(nx,ny,nz,zs,hterain,                      &
                            p_3d,t_3d,qv,ref_mos_3d,cldpcp_type_3d,     &
                            hgtrefthr,refthr1,refthr2,1,cloudopt,       &
                            qscalar_cld(1,1,1,P_QR),                    &
                            qscalar_cld(1,1,1,P_QS),                    &
                            qscalar_cld(1,1,1,P_QH),                    &
                            tem2)
          ELSE
            CALL reflec2q_wrf(nx,ny,nz,zs,hterain,                      &
                            p_3d,t_3d,qv,ref_mos_3d,cldpcp_type_3d,     &
                            hgtrefthr,refthr1,refthr2,1,cloudopt,       &
                            qscalar_cld(1,1,1,P_QR),                    &
                            qscalar_cld(1,1,1,P_QS),                    &
                            qscalar_cld(1,1,1,P_QG),                    &
                            tem2)
          END IF
        CASE (8:12)  ! Milbrandt and Yau 2- or 3-moment scheme
          !CALL reflec_MM(nx,ny,nz,rhobar,qscalar,tz,tem9)
          CALL reflec2q_MM(nx,ny,nz,zs,hterain,ref_mos_3d,rhobar,       &
                   t_3d,cldpcp_type_3d,hgtrefthr,refthr1,refthr2,       &
                   cloudopt,qscalar_cld,tem2)
        CASE (99)
           !CALL reflec_ferrier(nx,ny,nz, rhobar, qscalar, tz, tem9)
           !CALL pcp_mxr_ferrier (nx,ny,nz,t_3d,p_3d,zs,hterain,         &
           !        ref_mos_3d,cldpcp_type_3d,qscalar_cld(1,1,1,P_QR),   &
           !        qscalar_cld(1,1,1,P_QS),qscalar_cld(1,1,1,P_QH),     &
           !        tem1,cloudopt,refthr1,refthr2,hgtrefthr,istatus_pcp)
           WRITE(6,'(a)') 'Calling pcp_mxr, original formulation'
           qscalar_cld(:,:,:,P_QR)=1000.*qscalar_cld(:,:,:,P_QR)
           qscalar_cld(:,:,:,P_QS)=1000.*qscalar_cld(:,:,:,P_QS)
           qscalar_cld(:,:,:,P_QH)=1000.*qscalar_cld(:,:,:,P_QH)
           CALL pcp_mxr (nx,ny,nz,t_3d,p_3d,zs,hterain,                 &
                   ref_mos_3d,cldpcp_type_3d,                           &
                   qscalar_cld(1,1,1,P_QR),qscalar_cld(1,1,1,P_QS),     &
                   qscalar_cld(1,1,1,P_QH),tem1,                        &
                   cloudopt,refthr1,refthr2,hgtrefthr,istatus_pcp)
           qscalar_cld(:,:,:,P_QR)=0.001*qscalar_cld(:,:,:,P_QR)
           qscalar_cld(:,:,:,P_QS)=0.001*qscalar_cld(:,:,:,P_QS)
           qscalar_cld(:,:,:,P_QH)=0.001*qscalar_cld(:,:,:,P_QH)
           !CALL pcp_mxr2(nx,ny,nz,t_3d,p_3d,zs,hterain,               &
           !        ref_mos_3d,cldpcp_type_3d,                         &
           !        qscalar_cld(1,1,1,P_QR),qscalar_cld(1,1,1,P_QS),   &
           !        qscalar_cld(1,1,1,P_QH),tem1,                      &
           !        cloudopt,refthr1,refthr2,hgtrefthr,istatus_pcp)
      END SELECT
    ELSE IF (cldqropt == 100) THEN  !to use reflec_g2 formula

      !CALL reflec_g2(nx,ny,nz,rhobar, qscalar, tz, tem9)
      !CALL reflec2q_g2(nx,ny,nz,rhobar, t_3d, ref_mos_3d, qscalar_cld)A
      WRITE(6,'(a,i4,a)') ' Option: ',cldqropt,' not supported. Try again.'
      CALL arpsstop("Invalid cldqropt",1)

    ELSE IF (cldqropt > 100 .AND. cldqropt < 200) THEN

      wrfopt = MOD(cldqropt,100)
      SELECT CASE (wrfopt)

        CASE (1)     ! Kessler, warm rain
          CALL reflec2q_wrf(nx,ny,nz,zs,hterain,                        &
                            p_3d,t_3d,qv,ref_mos_3d,cldpcp_type_3d,     &
                            hgtrefthr,refthr1,refthr2,0,cloudopt,       &
                            qscalar_cld(1,1,1,P_QR),tem1,tem2,tem3)
        CASE (3,4)     ! Kessler, WSM 3-class
          CALL pcp_mxr (nx,ny,nz,t_3d,p_3d,zs,hterain,                  &
                  ref_mos_3d,cldpcp_type_3d,                            &
                  qscalar_cld(1,1,1,P_QR),qscalar_cld(1,1,1,P_QS),      &
                  qscalar_cld(1,1,1,P_QH),tem1,                         &
                  cloudopt,refthr1,refthr2,hgtrefthr,istatus_pcp)
        CASE (2,6,10,16) ! Lin, WSM 5- and 6- classes)
          IF(myproc == 0) WRITE(6,'(a)') ' Calling reflec2q_wrf ...'
          !CALL reflec_wrf(nx,ny,nz,qv,qscalar(:,:,:,P_QR),  &
          !                            qscalar(:,:,:,P_QS),  &
          !                            qscalar(:,:,:,P_QG),  &
          !                            1,tem1,tz,tem9)
          CALL reflec2q_wrf(nx,ny,nz,zs,hterain,                        &
                            p_3d,t_3d,qv,ref_mos_3d,cldpcp_type_3d,     &
                            hgtrefthr,refthr1,refthr2,1,cloudopt,       &
                            qscalar_cld(1,1,1,P_QR),                    &
                            qscalar_cld(1,1,1,P_QS),                    &
                            qscalar_cld(1,1,1,P_QH),                    &
                            tem2)
        CASE (5)     ! Ferrier microphysics scheme (as in WRF_POST)
          IF(myproc == 0) WRITE(6,'(a)') 'Calling reflec2q_ferrier_wrf ...'
          tem2(:,:,:) = 2.0
          !CALL reflec_ferrier_wrf(nx,ny,nz,qv,qscalar(:,:,:,P_QC), &
          !                                    qscalar(:,:,:,P_QR), &
          !                                    qscalar(:,:,:,P_QS), &
          !                                    tem1,tz,tem9,tem8,tem2)
          !CALL reflec2q_ferrier_wrf(nx,ny,nz,p_3d,t_3d,ref_mos_3d,qv,  &
          !                  qscalar_cld(1,1,1,P_QC),                   &
          !                  qscalar_cld(1,1,1,P_QR),                   &
          !                  qscalar_cld(1,1,1,P_QS),                   &
          !                  qscalar_cld(1,1,1,P_QH))
          WRITE(6,'(a,i4,a)') ' Option: ',cldqropt,' not supported. Try again.'
          CALL arpsstop("Invalid cldqropt",1)

        CASE (8) ! Thompson microphysics scheme (old version)
          IF(myproc == 0) WRITE(6,'(1x,a)') 'Calling reflec2q_9s ...'
            !CALL CALREF9s(nx,ny,nz,qscalar(:,:,:,P_QR),  &
            !                           qscalar(:,:,:,P_QS),  &
            !                           qscalar(:,:,:,P_QG),  &
            !                  tem1,tz,tem9)
            CALL reflec2q_9s(nx,ny,nz,zs,hterain,                       &
                            p_3d,t_3d,ref_mos_3d,                       &
                            cldpcp_type_3d,                             &
                            hgtrefthr,refthr1,refthr2,cloudopt,         &
                            qscalar_cld(1,1,1,P_QR),                    &
                            qscalar_cld(1,1,1,P_QS),                    &
                            qscalar_cld(1,1,1,P_QG),                    &
                            tem2)
        CASE (11) ! background dependent retrieval
          IF(myproc == 0) WRITE(6,'(1x,a)') 'Calling reflec2q_bg ...'
            !CALL CALREF9s(nx,ny,nz,qscalar(:,:,:,P_QR),  &
            !                           qscalar(:,:,:,P_QS),  &
            !                           qscalar(:,:,:,P_QG),  &
            !                  tem1,tz,tem9)
            CALL reflec2q_bg(nx,ny,nz,zs,hterain,                        &
                             p_3d,t_3d,ref_mos_3d,                       &
                             cldpcp_type_3d,                             &
                             hgtrefthr,refthr1,refthr2,cloudopt,         &
                             qscalar_cld(1,1,1,P_QR),                    &
                             qscalar_cld(1,1,1,P_QS),                    &
                             qscalar_cld(1,1,1,P_QG),                    &
                             tem2)
            !CALL print3dnc_lg(100,'qr_bg',qscalar(:,:,:,P_QR),nx,ny,nz) !print rain in bg
            !CALL print3dnc_lg(100,'qs_bg',qscalar(:,:,:,P_QS),nx,ny,nz)
            !CALL print3dnc_lg(100,'qg_bg',qscalar(:,:,:,P_QG),nx,ny,nz)
            !CALL print3dnc_lg(100,'qr_rtv',qscalar_cld(:,:,:,P_QR),nx,ny,nz)!print retrieved rain in bg
            !CALL print3dnc_lg(100,'qs_rtv',qscalar_cld(:,:,:,P_QS),nx,ny,nz)
            !CALL print3dnc_lg(100,'qg_rtv',qscalar_cld(:,:,:,P_QG),nx,ny,nz)
          !  CALL print3dnc_lg(100,'reflectivity',ref_mos_3d(:,:,:),nx,ny,nz)
        CASE (12) ! background dependent retrieval NSSL
          IF(myproc == 0) WRITE(6,'(1x,a)') 'Calling reflec2q_bg_nssl ...'
            !CALL CALREF9s(nx,ny,nz,qscalar(:,:,:,P_QR),  &
            !                           qscalar(:,:,:,P_QS),  &
            !                           qscalar(:,:,:,P_QG),  &
            !                  tem1,tz,tem9)
            lkdp = 0
            !DO j=1,ny-1
            !   DO i=1,nx-1
            !     DO k=2,nz-1
            !        if (kdp_mos_3d (i,j,k) .le. 1e-3) THEN
            !                lkdp(i,j,k)=-30
            !        else
            !                lkdp(i,j,k)=10*LOG10(kdp_mos_3d (i,j,k))
            !        end if
            !     END DO
            !   END DO
            !END DO
           ! CALL print3dnc_lg(100,'lkdp',lkdp(:,:,:),nx,ny,nz)

            CALL reflec2q_bg_nssl(nx,ny,nz,zs,hterain,                   &
                             p_3d,t_3d,ref_mos_3d,  lkdp,                &
                             cldpcp_type_3d,                             &
                             hgtrefthr,refthr1,refthr2,cloudopt,         &
                             qscalar_cld(1,1,1,P_QR),                    &
                             qscalar_cld(1,1,1,P_QS),                    &
                             qscalar_cld(1,1,1,P_QG),                    &
                             qscalar_cld(1,1,1,P_QH),                    &
                             tem2)

            !CALL print3dnc_lg(100,'qr_rtv',qscalar_cld(:,:,:,P_QR),nx,ny,nz)
            !CALL print3dnc_lg(100,'qs_rtv',qscalar_cld(:,:,:,P_QS),nx,ny,nz)
            !CALL print3dnc_lg(100,'qg_rtv',qscalar_cld(:,:,:,P_QG),nx,ny,nz)
            !CALL print3dnc_lg(100,'qh_rtv',qscalar_cld(:,:,:,P_QH),nx,ny,nz)
            !CALL print3dnc_lg(100,'reflectivity',ref_mos_3d(:,:,:),nx,ny,nz)
            !CALL print3dnc_lg(101,'kdp',kdp_mos_3d(:,:,:),nx,ny,nz)

        CASE (9,17)
          IF(myproc == 0) WRITE(6,'(a)')  'Calling reflec2q_wrf ...'
          CALL reflec2q_wrf(nx,ny,nz,zs,hterain,                        &
                            p_3d,t_3d,qv,ref_mos_3d,cldpcp_type_3d,     &
                            hgtrefthr,refthr1,refthr2,1,cloudopt,       &
                            qscalar_cld(1,1,1,P_QR),                    &
                            qscalar_cld(1,1,1,P_QS),                    &
                            qscalar_cld(1,1,1,P_QH),                    &
                            tem2)
        CASE DEFAULT
          WRITE(6,'(1x,a,I2,a)') 'ERROR: Unknown cldqropt = ',cldqropt,'.'
          CALL arpsstop('ERROR: Unsupported mphopt option',1)
      END SELECT

    ELSE IF (cldqropt > 200) THEN ! COAMPS original scheme

      SELECT CASE (cldqropt)
        CASE (201)
          IF( myproc == 0) WRITE(6,'(a,i6,a)') &
          'WARNING: COAMPS cldqropt option,',cldqropt,' not yet supported.'
          CALL arpsstop("Invalid cldqropt",1)
          !CALL coamps_rrf(tz,tem1,qscalar(:,:,:,P_QR),      &
          !                        qscalar(:,:,:,p_QS),      &
          !                        qscalar(:,:,:,P_QI),      &
          !                        qscalar(:,:,:,P_QC),      &
          !                        qscalar(:,:,:,P_QG),      &
          !                        qv,nx,ny,nz,tem9)
        CASE (204)  ! Thompson scheme
          !CALL CALREF9s(nx,ny,nz,qscalar(:,:,:,P_QR),       &
          !                       qscalar(:,:,:,P_QS),       &
          !                       qscalar(:,:,:,P_QG),       &
          !                       tem1,tz,tem9)
            CALL reflec2q_9s(nx,ny,nz,zs,hterain,                       &
                            p_3d,t_3d,ref_mos_3d,                       &
                            cldpcp_type_3d,                             &
                            hgtrefthr,refthr1,refthr2,cloudopt,         &
                            qscalar_cld(1,1,1,P_QR),                    &
                            qscalar_cld(1,1,1,P_QS),                    &
                            qscalar_cld(1,1,1,P_QG),                    &
                            tem2)

        CASE (205:209) ! MY scheme
          !CALL reflec_MM(nx,ny,nz,rhobar,qscalar,tz,tem9)
          CALL reflec2q_MM(nx,ny,nz,zs,hterain,ref_mos_3d,rhobar,       &
                       t_3d,cldpcp_type_3d,                             &
                       hgtrefthr,refthr1,refthr2,cloudopt,              &
                       qscalar_cld,tem2)
      END SELECT

    ELSE
      IF( myproc == 0) WRITE(6,'(a,i6,a)') &
        'WARNING: Invalid cldqropt option,',cldqropt,' stopping.'
      CALL arpsstop("Invalid cldqropt",1)
    END IF

  END IF
!
!-----------------------------------------------------------------------
!
!  Calculate the vertically integrated condensates
!  (in units of kg/m**2)
!
!-----------------------------------------------------------------------
!
  istatus = 0
  vil=0.0
! OpenMP:
!$OMP PARALLEL DO PRIVATE(j,i,k,qsum,arg)
  DO j=1,ny-1
    DO i=1,nx-1
      DO k=2,nz-1
        qsum = 0.0
        DO nq=1,nscalarq
          qsum = qsum + qscalar_cld(i,j,k,nq)
        END DO
        IF(rhobar(i,j,k) == 0.) THEN
          PRINT *, ' rhobar 0 at ',i,j,k
          istatus = -1
        END IF
        arg = 0.5*(zs(i,j,k+1)-zs(i,j,k-1))*qsum/rhobar(i,j,k)  ! kg/m**2
        vil(i,j) = vil(i,j) +arg
      END DO
    END DO
  END DO

  CALL mpmini(istatus)
  IF (istatus /= 0) CALL arpsstop('ERROR: Found 0 in rhobar.',1)

  IF(cld_files == 1) THEN
    varid='colvil'
    varname='Cloud VIL'
    varunits='kg/m**2'
    CALL wrtvar1(nx,ny,1,vil,varid,varname,varunits,                    &
                curtim,runname(1:lfnkey),dirname,istatwrt)
  END IF

  IF (myproc == 0) THEN
    WRITE(6,'(a)')' Cloud options: '
    WRITE(6,'(a,i3)') ' cldqvopt=',cldqvopt
    WRITE(6,'(a,i3)') ' cldqcopt=',cldqcopt
    WRITE(6,'(a,i3)') ' cldqropt=',cldqropt
    WRITE(6,'(a,i3)') ' cldptopt=',cldptopt
    WRITE(6,'(a,i3)') ' cldwopt =',cldwopt
  END IF
!-----------------------------------------------------------------------
!
! To avoid overwritting ptprt, qv & qscalar directly
! Added with PSEUDOQOBS options
!
!-----------------------------------------------------------------------

  DO nq = 1, nscalar
    qstem(:,:,:,nq) = qscalar(:,:,:,nq)
  END DO
  pttem(:,:,:) = ptprt(:,:,:)
  qvtem(:,:,:) = qv(:,:,:)

!
!-----------------------------------------------------------------------
!
!  Enhance the cloud liquid/ice water mixing ratio fields (convert
!   g/kg to kg/kg) and THEN DO smoothing.
!
!-----------------------------------------------------------------------
!
  IF (cldqcopt == 1) THEN
    IF (myproc == 0) WRITE(6,'(a)')' Enhancing qc and qi-fields'
    IF(P_QC > 0) qstem(:,:,:,P_QC)=0.0
    IF(P_QI > 0) qstem(:,:,:,P_QI)=0.0
    IF (mphyopt < 1000) THEN
      DO k=2,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            IF(P_QC > 0) qstem(i,j,k,P_QC)=max(0.0,qscalar_cld(i,j,k,P_QC))
            IF(P_QI > 0) qstem(i,j,k,P_QI)=max(0.0,qscalar_cld(i,j,k,P_QI))
            ! Limit total cloud water plus ice to local qvsat.
            qvsat=f_qvsat( p_3d(i,j,k), t_3d(i,j,k))
            qvsat = qvslimit_2_qc *qvsat
            qsum = 0.0
            IF(P_QC > 0) qsum = qsum + qstem(i,j,k,P_QC)
            IF(P_QI > 0) qsum = qsum + qstem(i,j,k,P_QI)
            IF(qsum > 1.0E-10 .AND. qsum > qvsat) THEN
              relh = qstem(i,j,k,P_QC)/qsum
              IF (P_QC > 0) qstem(i,j,k,P_QC) = relh*qvsat
              IF (P_QI > 0) qstem(i,j,k,P_QI) = (1.0-relh)*qvsat
            END IF
          END DO
        END DO
      END DO
    ELSE  ! QI is handled with refl for these schemes don't include in reduction
      DO k=2,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            qstem(i,j,k,P_QC)=max(0.0,qscalar_cld(i,j,k,P_QC))
            ! Limit total cloud water to local qvsat.
            qvsat=f_qvsat( p_3d(i,j,k), t_3d(i,j,k))
            qvsat = qvslimit_2_qc *qvsat
            qstem(i,j,k,P_QC)=MIN(qstem(i,j,k,P_QC),qvsat)
            qstem(i,j,k,P_QI)=max(0.0,qscalar_cld(i,j,k,P_QI))
          END DO
        END DO
      END DO
    END IF

    IF (mp_opt > 0) THEN  ! Before smoothing, the arrays must be MPI valid
      CALL acct_interrupt(mp_acct)
      IF (P_QC > 0) THEN
        CALL mpsendrecv2dew(qstem(1,1,1,P_QC),nx,ny,nz,ebc,wbc,0,tem1)
        CALL mpsendrecv2dns(qstem(1,1,1,P_QC),nx,ny,nz,nbc,sbc,0,tem1)
      END IF
      IF (P_QI > 0) THEN
        CALL mpsendrecv2dew(qstem(1,1,1,P_QI),nx,ny,nz,ebc,wbc,0,tem1)
        CALL mpsendrecv2dns(qstem(1,1,1,P_QI),nx,ny,nz,nbc,sbc,0,tem1)
      END IF
    END IF

    IF (smth_opt == 1) THEN
      DO k=2,nz-1
        IF (P_QC > 0) CALL smooth9p(qstem(1,1,k,P_QC),               &
                              nx,ny,1,nx-1,1,ny-1,0,tem1(1,1,1))
        IF (P_QI > 0) CALL smooth9p(qstem(1,1,k,P_QI),               &
                              nx,ny,1,nx-1,1,ny-1,0,tem1(1,1,1))
      END DO
    ELSE IF(smth_opt == 2) THEN
      IF (P_QC > 0) CALL smooth3d(nx,ny,nz, 1,nx-1,1,ny-1,1,nz-1,      &
                          0,smfct1,zs,  &
                          qstem(1,1,1,P_QC),tem1,qstem(1,1,1,P_QC))
      IF (P_QI > 0) CALL smooth3d(nx,ny,nz, 1,nx-1,1,ny-1,1,nz-1,      &
                          0,smfct1,zs,  &
                          qstem(1,1,1,P_QI),tem1,qstem(1,1,1,P_QI))
    END IF
  END IF
!
!-----------------------------------------------------------------------
!
!  Enhance and then smooth the hydrometeor mixing ratio fields.
!
!-----------------------------------------------------------------------
!
  IF (cldqropt > 0) THEN
    IF (myproc == 0) WRITE(6,'(a)')' Enhancing qr, qs, and qh-fields'

    SELECT CASE ( mphyopt )
    CASE ( 1 )
      DO k=2,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            qstem(i,j,k,P_QR)=MIN(qscalar_cld(i,j,k,P_QR),qrlimit)
          END DO
        END DO
      END DO
    CASE ( 2:4 )
      DO k=2,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            qstem(i,j,k,P_QR)=MIN(qscalar_cld(i,j,k,P_QR),qrlimit)
            qstem(i,j,k,P_QS)=MIN(qscalar_cld(i,j,k,P_QS),qrlimit)
            qstem(i,j,k,P_QH)=MIN(qscalar_cld(i,j,k,P_QH),qrlimit)
          END DO
        END DO
      END DO
    CASE ( 5:7 )
      DO k=2,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            qstem(i,j,k,P_QR)=MIN(qscalar_cld(i,j,k,P_QR),qrlimit)
            qstem(i,j,k,P_QS)=MIN(qscalar_cld(i,j,k,P_QS),qrlimit)
            qstem(i,j,k,P_QG)=MIN(qscalar_cld(i,j,k,P_QG),qrlimit)
          END DO
        END DO
      END DO
    CASE ( 8 )
      DO k=2,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            qstem(i,j,k,P_QR)=MIN(qscalar_cld(i,j,k,P_QR),qrlimit)
            qstem(i,j,k,P_QI)=MIN(qscalar_cld(i,j,k,P_QI),qrlimit)
            qstem(i,j,k,P_QS)=MIN(qscalar_cld(i,j,k,P_QS),qrlimit)
            qstem(i,j,k,P_QG)=MIN(qscalar_cld(i,j,k,P_QG),qrlimit)
            qstem(i,j,k,P_QH)=MIN(qscalar_cld(i,j,k,P_QH),qrlimit)
          END DO
        END DO
      END DO
    CASE ( 9, 10, 12 )
      DO k=2,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            qstem(i,j,k,P_QR) = MIN(qscalar_cld(i,j,k,P_QR),qrlimit)
            qstem(i,j,k,P_NR) = qscalar_cld(i,j,k,P_NR)

            qstem(i,j,k,P_QI) = MIN(qscalar_cld(i,j,k,P_QI),qrlimit)
            qstem(i,j,k,P_NI) = qscalar_cld(i,j,k,P_NI)

            qstem(i,j,k,P_QS) = MIN(qscalar_cld(i,j,k,P_QS),qrlimit)
            qstem(i,j,k,P_NS) = qscalar_cld(i,j,k,P_NS)

            qstem(i,j,k,P_QG) = MIN(qscalar_cld(i,j,k,P_QG),qrlimit)
            qstem(i,j,k,P_NG) = qscalar_cld(i,j,k,P_NG)

            qstem(i,j,k,P_QH) = MIN(qscalar_cld(i,j,k,P_QH),qrlimit)
            qstem(i,j,k,P_NH) = qscalar_cld(i,j,k,P_NH)
          END DO
        END DO
      END DO
    CASE ( 11 )
      DO k=2,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            qstem(i,j,k,P_QR) = MIN(qscalar_cld(i,j,k,P_QR),qrlimit)
            qstem(i,j,k,P_NR) = qscalar_cld(i,j,k,P_NR)
            qstem(i,j,k,P_ZR) = qscalar_cld(i,j,k,P_ZR)

            qstem(i,j,k,P_QI) = MIN(qscalar_cld(i,j,k,P_QI),qrlimit)
            qstem(i,j,k,P_NI) = qscalar_cld(i,j,k,P_NI)

            qstem(i,j,k,P_QS) = MIN(qscalar_cld(i,j,k,P_QS),qrlimit)
            qstem(i,j,k,P_NS) = qscalar_cld(i,j,k,P_NS)
            qstem(i,j,k,P_ZS) = qscalar_cld(i,j,k,P_ZS)

            qstem(i,j,k,P_QG) = MIN(qscalar_cld(i,j,k,P_QG),qrlimit)
            qstem(i,j,k,P_NG) = qscalar_cld(i,j,k,P_NG)
            qstem(i,j,k,P_ZG) = qscalar_cld(i,j,k,P_ZG)

            qstem(i,j,k,P_QH) = MIN(qscalar_cld(i,j,k,P_QH),qrlimit)
            qstem(i,j,k,P_NH) = qscalar_cld(i,j,k,P_NH)
            qstem(i,j,k,P_ZH) = qscalar_cld(i,j,k,P_ZH)
          END DO
        END DO
      END DO
    CASE DEFAULT
      WRITE(6,'(a,i6,a)') ' Unsupported mphyopt: ',mphyopt,' exiting.'
      STOP
    END SELECT

    IF (mp_opt > 0) THEN  ! Before smoothing, the arrays must be MPI valid
      CALL acct_interrupt(mp_acct)
      IF(P_QR > 0) CALL mpsendrecv2dew(qstem(1,1,1,P_QR),nx,ny,nz,ebc,wbc,0,tem1)
      IF(P_QR > 0) CALL mpsendrecv2dns(qstem(1,1,1,P_QR),nx,ny,nz,nbc,sbc,0,tem1)
      IF(P_QS > 0) CALL mpsendrecv2dew(qstem(1,1,1,P_QS),nx,ny,nz,ebc,wbc,0,tem1)
      IF(P_QS > 0) CALL mpsendrecv2dns(qstem(1,1,1,P_QS),nx,ny,nz,nbc,sbc,0,tem1)
      IF(P_QG > 0) CALL mpsendrecv2dew(qstem(1,1,1,P_QG),nx,ny,nz,ebc,wbc,0,tem1)
      IF(P_QG > 0) CALL mpsendrecv2dns(qstem(1,1,1,P_QG),nx,ny,nz,nbc,sbc,0,tem1)
      IF(P_QH > 0) CALL mpsendrecv2dew(qstem(1,1,1,P_QH),nx,ny,nz,ebc,wbc,0,tem1)
      IF(P_QH > 0) CALL mpsendrecv2dns(qstem(1,1,1,P_QH),nx,ny,nz,nbc,sbc,0,tem1)
    END IF

    IF (smth_opt == 1) THEN
      IF(P_QR > 0) THEN
        DO k=2,nz-1
          CALL smooth9p(qstem(1,1,k,P_QR),nx,ny,1,nx-1,1,ny-1,0,tem1)
        END DO
      END IF
      IF(P_QS > 0) THEN
        DO k=2,nz-1
          CALL smooth9p(qstem(1,1,k,P_QS),nx,ny,1,nx-1,1,ny-1,0,tem1)
        END DO
      END IF
      IF(P_QG > 0) THEN
        DO k=2,nz-1
          CALL smooth9p(qstem(1,1,k,P_QG),nx,ny,1,nx-1,1,ny-1,0,tem1)
        END DO
      END IF
      IF(P_QH > 0) THEN
        DO k=2,nz-1
          CALL smooth9p(qstem(1,1,k,P_QH),nx,ny,1,nx-1,1,ny-1,0,tem1)
        END DO
      END IF
    ELSE IF(smth_opt == 2) THEN
      IF(P_QR > 0) CALL smooth3d(nx,ny,nz, 1,nx-1,1,ny-1,2,nz-1,0,smfct1,zs,  &
                    qstem(1,1,1,P_QR),tem1,qstem(1,1,1,P_QR))
      IF(P_QS > 0) CALL smooth3d(nx,ny,nz, 1,nx-1,1,ny-1,2,nz-1,0,smfct1,zs,  &
                    qstem(1,1,1,P_QS),tem1,qstem(1,1,1,P_QS))
      IF(P_QG > 0) CALL smooth3d(nx,ny,nz, 1,nx-1,1,ny-1,2,nz-1,0,smfct1,zs,  &
                    qstem(1,1,1,P_QG),tem1,qstem(1,1,1,P_QG))
      IF(P_QH > 0) CALL smooth3d(nx,ny,nz, 1,nx-1,1,ny-1,2,nz-1,0,smfct1,zs,  &
                    qstem(1,1,1,P_QH),tem1,qstem(1,1,1,P_QH))
    END IF


    IF( frac_qr_2_qc > 0.0) THEN
      frac = 1.0-frac_qr_2_qc
      DO k=2,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            IF (P_QC > 0 .AND. P_QR > 0) THEN
              qstem(i,j,k,P_QC) = qstem(i,j,k,P_QC) +              &
                          frac_qr_2_qc*qstem(i,j,k,P_QR)
              qstem(i,j,k,P_QR) = MAX( 0.0, frac*qstem(i,j,k,P_QR))
            END IF
            IF (P_QI > 0) THEN
              IF( P_QH > 0) THEN
                qstem(i,j,k,P_QI) = qstem(i,j,k,P_QI) +            &
                   frac_qr_2_qc*(qstem(i,j,k,P_QS)+qstem(i,j,k,P_QH))
                qstem(i,j,k,P_QS) = MAX( 0.0, frac*qstem(i,j,k,P_QS))
                qstem(i,j,k,P_QH) = MAX( 0.0, frac*qstem(i,j,k,P_QH))
              ELSE IF ( P_QG > 0) THEN
                qstem(i,j,k,P_QI) = qstem(i,j,k,P_QI) +            &
                   frac_qr_2_qc*(qstem(i,j,k,P_QS)+qstem(i,j,k,P_QG))
                qstem(i,j,k,P_QS) = MAX( 0.0, frac*qstem(i,j,k,P_QS))
                qstem(i,j,k,P_QG) = MAX( 0.0, frac*qstem(i,j,k,P_QG))
              END IF
            END IF
          END DO
        END DO
      END DO
    END IF
  END IF   ! cldqropt.eq.1?

!-----------------------------------------------------------------------
!
!  Adjust the cloud water fields for possible double counting from
!  the combination of the Smith-Feddes cloud scheme and the
!  reflectivity-to-precipitation algorithm.
!
!  For now a simple approach is taken.  If clouds and precipation
!  are present, limit cloud water to 5% of the precipitation value.
!
!  Note possible conflict with frac_qr_2_qc above, so for best
!  results set frac_qr_2_qc to ZERO.
!
!-----------------------------------------------------------------------

  cldpcpfrc=0.05
  IF( mphyopt < 8 ) THEN
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          cldtot = 0.0
          pcptot = 0.0
          IF (P_QC > 0) cldtot = cldtot + qstem(i,j,k,P_QC)
          IF (P_QI > 0) cldtot = cldtot + qstem(i,j,k,P_QI)
          IF (P_QR > 0) pcptot = pcptot + qstem(i,j,k,P_QR)
          IF (P_QS > 0) pcptot = pcptot + qstem(i,j,k,P_QS)
          IF (P_QG > 0) pcptot = pcptot + qstem(i,j,k,P_QG)
          IF (P_QH > 0) pcptot = pcptot + qstem(i,j,k,P_QH)
          IF(cldtot > 0. .AND. pcptot > 0.) THEN
            cldlim=cldpcpfrc*pcptot
            IF(cldtot > cldlim) THEN
              IF (P_QC > 0) qstem(i,j,k,P_QC)=qstem(i,j,k,P_QC)*(cldlim/cldtot)
              IF (P_QI > 0) qstem(i,j,k,P_QI)=qstem(i,j,k,P_QI)*(cldlim/cldtot)
            END IF
          END IF
        END DO
      END DO
    END DO
  ELSE
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          cldtot = qstem(i,j,k,P_QC)
          pcptot = 0.0
          pcptot = pcptot + qstem(i,j,k,P_QR)
          pcptot = pcptot + qstem(i,j,k,P_QI)
          pcptot = pcptot + qstem(i,j,k,P_QS)
          IF (P_QG > 0) pcptot = pcptot + qstem(i,j,k,P_QG)
          IF (P_QH > 0) pcptot = pcptot + qstem(i,j,k,P_QH)
          IF(cldtot > 0. .AND. pcptot > 0.) THEN
            cldlim=cldpcpfrc*pcptot
            IF(cldtot > cldlim) THEN
              IF (P_QC > 0) qstem(i,j,k,P_QC)=qstem(i,j,k,P_QC)*cldlim/cldtot
            END IF
          END IF
        END DO
      END DO
    END DO
  END IF


!-----------------------------------------------------------------------
!
!  Adjust the purterbation potential temperature field to account
!  for the latent heating release.
!
!  Corrected by D.H. Wang for ppi term in temperature adjustment
!  Adjustment is to temperature then converted to potential
!  temperature.
!
!-----------------------------------------------------------------------
!
  p0inv=1./p0
  SELECT CASE ( cldptopt)
  CASE ( 3 )

    IF (myproc == 0) THEN
      WRITE(6,'(a)')' Adjusting ptprt to account for latent heating.'
      WRITE(6,'(a,f10.4,a,f10.4)')                                      &
         ' frac of qc:',frac_qc_2_lh,' adj_lim:',max_lh_2_pt
    END IF
    max_pt_adj = 0.0
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          ppi = ((pbar(i,j,k)+pprt(i,j,k))*p0inv) ** rddcp
          !arg = lathv*frac_qc_2_lh*(qc(i,j,k)+qi(i,j,k))/(cp*ppi)
          arg = 0.0
          IF (P_QC > 0) arg = arg + qstem(i,j,k,P_QC)
          IF (P_QI > 0) arg = arg + qstem(i,j,k,P_QI)
          arg = lathv*frac_qc_2_lh*arg/(cp*ppi)
          max_pt_adj = MAX(max_pt_adj,arg)
          pttem(i,j,k) = ptprt(i,j,k) + MIN(arg,max_lh_2_pt)
          max_pt_adj = MAX(max_pt_adj,arg)
        END DO
      END DO
    END DO
!
!    In this call calls below, "min_pt_adj" is just a dummy argument, as the
!    subroutine called returns a max and a min.
!
    min_pt_adj = 0
    CALL mpmax0(max_pt_adj,min_pt_adj)
    IF (myproc == 0) PRINT*,'max_adj=',max_pt_adj

  CASE ( 4 )

    IF (myproc == 0) THEN
      WRITE(6,'(a)')' Adjusting ptprt to account for latent heating in w.'
      PRINT*,'frac of qc:',frac_qc_2_lh,' adj_lim:',max_lh_2_pt
    END IF
    max_pt_adj = 0.0
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          !
          ! Ningzhu's change on 3/23/2011
          !
          !IF(w(i,j,k) > 0. ) THEN
          !  wratio=1.0
          !  ptcld=ctmp_3d(i,j,k)*(p0/p_3d(i,j,k))**rddcp
          !  ptdiff=ptcld-(ptbar(i,j,k)+ptprt(i,j,k))
          !  IF(ptdiff > 0.) THEN
          !    arg = frac_qc_2_lh*wratio*ptdiff
          !    ptprt(i,j,k) = ptprt(i,j,k) + MIN(arg,max_lh_2_pt)
          !    max_pt_adj = MAX(max_pt_adj,arg)
          !  END IF
          !END IF
          IF(w(i,j,k) > 0. ) THEN
            ppi = ((pbar(i,j,k)+pprt(i,j,k))*p0inv) ** rddcp
            !arg = lathv*frac_qc_2_lh*(qc(i,j,k)+qi(i,j,k))/(cp*ppi)
            arg = 0.0
            IF (P_QC > 0) arg = arg + qstem(i,j,k,P_QC)
            IF (P_QI > 0) arg = arg + qstem(i,j,k,P_QI)
            arg = lathv*frac_qc_2_lh*arg/(cp*ppi)
            pttem(i,j,k) = ptprt(i,j,k) + MIN(arg,max_lh_2_pt)
            max_pt_adj = MAX(max_pt_adj,arg)
          END IF
        END DO
      END DO
    END DO
    min_pt_adj = 0
    CALL mpmax0(max_pt_adj,min_pt_adj)
    IF (myproc == 0) PRINT*,'max_adj=',max_pt_adj

  CASE ( 5 )

    IF (myproc == 0) THEN
      WRITE(6,'(a)')' Adjusting ptprt to moist-adiab cloud temp for w>-0.2'
      PRINT*,'frac of qc:',frac_qc_2_lh,' adj_lim:',max_lh_2_pt
    END IF
    max_pt_adj = 0.0
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          wratio=min(max(0.,(5.0*(w(i,j,k)+0.2))),1.0)
          !wratio = 1.0
          ptcld=ctmp_3d(i,j,k)*(p0/p_3d(i,j,k))**rddcp
          ptdiff=ptcld-(ptbar(i,j,k)+ptprt(i,j,k))
          IF(ptdiff > 0.) THEN
            arg = frac_qc_2_lh*wratio*ptdiff
            pttem(i,j,k) = ptprt(i,j,k) + MIN(arg,max_lh_2_pt)
            max_pt_adj = MAX(max_pt_adj,arg)
          END IF
        END DO
      END DO
    END DO
    min_pt_adj = 0
    CALL mpmax0(max_pt_adj,min_pt_adj)
    IF (myproc == 0) PRINT*,'max_adj=',max_pt_adj

  CASE ( 6 )

    IF (myproc == 0) THEN
      WRITE(6,'(a)')' Adjusting ptprt to moist-adiab cloud temp for w>0.0'
      PRINT*,'frac of qc:',frac_qc_2_lh,' adj_lim:',max_lh_2_pt
    END IF
    max_pt_adj = 0.0
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          IF(w(i,j,k) > 0.) THEN
            ptcld=ctmp_3d(i,j,k)*(p0/p_3d(i,j,k))**rddcp
            ptdiff=ptcld-(ptbar(i,j,k)+ptprt(i,j,k))
            IF(ptdiff > 0.) THEN
              arg = frac_qc_2_lh*ptdiff
              pttem(i,j,k) = ptprt(i,j,k) + MIN(arg,max_lh_2_pt)
              max_pt_adj = MAX(max_pt_adj,arg)
            END IF
          END IF
        END DO
      END DO
    END DO
    min_pt_adj = 0
    CALL mpmax0(max_pt_adj,min_pt_adj)
    IF (myproc == 0) PRINT*,'max_adj=',max_pt_adj

  CASE ( 7 )

    IF (myproc == 0) THEN
      WRITE(6,'(a)')' Adjusting ptprt based on concept bouble model'
      PRINT*,'frac of qc:',frac_qc_2_lh,' adj_lim:',max_lh_2_pt
    END IF
    max_pt_adj = 0.0
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
!          IF( abs(w(i,j,k)) > 1.0) THEN
          IF( w_index(i,j) ==  1) THEN
            ptcld=ctmp_3d(i,j,k)*(p0/p_3d(i,j,k))**rddcp
            ptdiff=w(i,j,k)*(w(i,j,k)-w(i,j,k-1))/(zs(i,j,k)-zs(i,j,k-1))      &
                    /0.66/9.8                                                  &
                 + (qvprt_old(i,j,k)+qstem(i,j,k,P_QR)+qstem(i,j,k,P_QS)   &
                                    +qstem(i,j,k,P_QC)+qstem(i,j,k,P_QI)   &
                                    +qstem(i,j,k,P_QH))/(1.0+qvbar(i,j,k))
            pttem(i,j,k) =3*ptdiff*ptcld
            !ptdiff =3*ptdiff*ptcld + ptcld    ! now ptdiff is total theta
            !ptprt(i,j,k)= ptdiff-ptbar(i,j,k)
              END IF
        END DO
      END DO
    END DO
    min_pt_adj = 0
    CALL mpmax0(max_pt_adj,min_pt_adj)
    IF (myproc == 0) PRINT*,'max_adj=',max_pt_adj

  CASE ( 8 )
    IF (myproc == 0) THEN
      WRITE(6,'(a)')' Adjusting ptprt based on concept bouble model'
      PRINT*,'frac of qc:',frac_qc_2_lh,' adj_lim:',max_lh_2_pt
            END IF
    max_pt_adj = 0.0
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
!          IF( abs(w(i,j,k)) > 1.0) THEN
          IF( w_index(i,j) ==  1) THEN
            ptcld=ctmp_3d(i,j,k)*(p0/p_3d(i,j,k))**rddcp
            ptdiff=w(i,j,k)*(w(i,j,k)-w(i,j,k-1))/(zs(i,j,k)-zs(i,j,k-1))      &
                   /9.8 + w(i,j,k)*w(i,j,k)/9.8/5000.0                         &
                 + (qvprt_old(i,j,k)+qstem(i,j,k,P_QR)+qstem(i,j,k,P_QS)   &
                                    +qstem(i,j,k,P_QC)+qstem(i,j,k,P_QI)   &
                                    +qstem(i,j,k,P_QH))/(1.0+qvbar(i,j,k))
            pttem(i,j,k)=2*ptdiff*ptcld
          END IF
        END DO
      END DO
    END DO
    min_pt_adj = 0
    CALL mpmax0(max_pt_adj,min_pt_adj)
    IF (myproc == 0) PRINT*,'max_adj=',max_pt_adj

  CASE ( 9 )

    IF (myproc == 0) THEN
      WRITE(6,'(a)')' Adjusting ptprt based on concept bouble model'
      PRINT*,'frac of qc:',frac_qc_2_lh,' adj_lim:',max_lh_2_pt
    END IF
    max_pt_adj = 0.0
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
!          IF( abs(w(i,j,k)) > 1.0) THEN
          IF( w_index(i,j) ==  1) THEN
            ptcld=ctmp_3d(i,j,k)*(p0/p_3d(i,j,k))**rddcp
            ptdiff=w(i,j,k)*(w(i,j,k)-w(i,j,k-1))/(zs(i,j,k)-zs(i,j,k-1))      &
                    /0.66/9.8 + w(i,j,k)*w(i,j,k)/0.66/9.8/5000.0              &
                 + (qvprt_old(i,j,k)+qstem(i,j,k,P_QR)+qstem(i,j,k,P_QS)   &
                                    +qstem(i,j,k,P_QC)+qstem(i,j,k,P_QI)   &
                                    +qstem(i,j,k,P_QH))/(1.0+qvbar(i,j,k))
            pttem(i,j,k) = 2*ptdiff*ptcld
            !ptdiff = 2*ptdiff*ptcld + ptcld
            !ptprt(i,j,k)= ptdiff-ptbar(i,j,k)
            END IF
          END DO
      END DO
    END DO
    min_pt_adj = 0
    CALL mpmax0(max_pt_adj,min_pt_adj)
    IF (myproc == 0) PRINT*,'max_adj=',max_pt_adj

  CASE ( 10 )

    IF (myproc == 0) THEN
      WRITE(6,'(a)')' Adjusting ptprt to moist-adiab cloud temp for ZDR col'
      PRINT*,'frac of qc:',frac_qc_2_lh,' adj_lim:',max_lh_2_pt
    END IF
    max_pt_adj = 0.0
    DO j=2,ny-1
      DO i=2,nx-1
        zdr_col_depth(i,j) = 0.0
        DO k=2,nz-1
          IF (ctmp_3d(i,j,k)  < 273.15 .AND. ctmp_3d(i,j,k) > 258.15  .AND. &
              ref_mos_3d(i,j,k) >= 10  .AND. rhv_mos_3d(i,j,k) > 0.85 .AND. &
              zdr_mos_3d(i,j,k) >= 1.0 .AND. zdr_mos_3d(i,j,k-1) >= 1.0) THEN
            maskedarray_rhv = rhv_mos_3d(i-1:i+1,j-1:j+1,k)
            crit_rhv = COUNT(maskedarray_rhv>0.88)
            IF (crit_rhv >= 5) THEN
              maskedarray_zdr = zdr_mos_3d(i-1:i+1,j-1:j+1,k)
              crit_zdr = COUNT(maskedarray_zdr>=1.0 .AND. maskedarray_zdr<5.0)
              IF (crit_zdr >= 5) THEN
                zdr_col_depth(i,j) = zdr_col_depth(i,j) + 1.0
              END IF
            END IF
          END IF
        END DO
        IF (zdr_col_depth(i,j) > 1.0) THEN
          DO k=2,nz-1
            ptcld=ctmp_3d(i,j,k)*(p0/p_3d(i,j,k))**rddcp
            ptdiff=ptcld-(ptbar(i,j,k)+ptprt(i,j,k))
            IF(ptdiff > 0.) THEN
              arg = frac_qc_2_lh*ptdiff
              rdummy = ptprt(i,j,k) + MIN(arg,max_lh_2_pt)
              DO j1 = j-1,j+1
                DO i1 = i-1,i+1
                  pttem(i1,j1,k) = rdummy
                END DO
              END DO
              !pttem(i-1:i+1,j-1:j+1,k) = ptprt(i,j,k) + MIN(arg,max_lh_2_pt)
!               ptprt(i,j,k) = ptprt(i,j,k) + MIN(arg,max_lh_2_pt)
              max_pt_adj = MAX(max_pt_adj,arg)
            END IF
          END DO
        END IF
      END DO
    END DO
    min_pt_adj = 0
    CALL mpmax0(max_pt_adj,min_pt_adj)
    IF (myproc == 0) PRINT*,'max_adj=',max_pt_adj

  END SELECT
!
!-----------------------------------------------------------------------
!
!  Enhance rh*-field in the cloudy area with the cloud cover
!  greater than thresh_cvr and then smooth the enhanced field.
!
!-----------------------------------------------------------------------
!
  IF (cldqvopt > 0) THEN
    IF (myproc == 0) WRITE(6,'(a,f5.2,a,f5.2,a,f5.1,a,f5.1,a)')         &
            ' Enhancing RH-field for cldcvr between'                    &
             ,cvr2rh_thr1,' and ', cvr2rh_thr2                          &
            ,' with linear ramp from ',rh_thr1*100.0                    &
             ,'% to ',rh_thr2*100.0,'%'

    SELECT CASE ( cldqvopt)
      CASE ( 1 )
        DO k=2,nz-1
          DO j=1,ny-1
            DO i=1,nx-1
              IF(clouds_3d(i,j,k) > cvr2rh_thr1) THEN
                rh = (clouds_3d(i,j,k)-cvr2rh_thr1)*(rh_thr2-rh_thr1)       &
                        /(cvr2rh_thr2-cvr2rh_thr1) + rh_thr1
                rh = MIN(rh,rh_thr2)
                rh_3d(i,j,k) = MAX((100.*rh),rh_3d(i,j,k))
              END IF
            END DO
          END DO
        END DO
      CASE ( 2 )
        DO k=2,nz-1
          DO j=1,ny-1
            DO i=1,nx-1
              IF(clouds_3d(i,j,k) > cvr2rh_thr1) THEN
                rh = (clouds_3d(i,j,k)-cvr2rh_thr1)*(rh_thr2-rh_thr1)       &
                        /(cvr2rh_thr2-cvr2rh_thr1) + rh_thr1
                rh = 100.*MIN(rh,rh_thr2)
                IF( w(i,j,k) > 0.0) THEN              ! updraft
                  rh_3d(i,j,k) = MAX(rh,rh_3d(i,j,k))
                ELSE IF ((w(i,j,k) > -0.2) ) THEN     ! weak downdraft
                  IF (rh_3d(i,j,k) < rh) THEN
                    wgt=-5.0*w(i,j,k)
                    rh_3d(i,j,k)=wgt*rh_3d(i,j,k) + (1.0-wgt)*rh
                  END IF
                ELSE                                  ! downdraft
                  IF( t_3d(i,j,k) < 273.15 ) THEN
                    rh=MIN(rh,90.)
                    rh_3d(i,j,k) = MAX(rh,rh_3d(i,j,k))
                  END IF
                END IF
              END IF
            END DO
          END DO
        END DO
      CASE ( 3 )
        CALL print3dnc_lg(101,'bgRH',rh_3d,nx,ny,nz)
        !DO k=3,nz-1
        !  CALL despekl(nx,ny,nx,ny,-3,1,zdr_mos_3d(:,:,k),zdrtmp) !despeckle ZDR < -3 dB, median_filter=1
        !end do
        !write(6,*) "ref_mos_3d",ref_mos_3d(600,600,50)
        DO j=2,ny-2
          DO i=2,nx-2
            ! find the height of freezing level, chq
            melting_height=0

            DO k = 10,40
              IF (ctmp_3d(i,j,k) < 273.15) THEN
                melting_height=zs(i,j,k)
                exit
              END IF
            END DO
            !to find the zdr column base height, chq
            ALLOCATE(ref_large_3d(0:nx+1,0:ny+1,1:nz), STAT = istatus)
            CALL mpfillextarray3d(ref_mos_3d,nx,ny,nz,1,1, ref_large_3d,&
                      !ebc,wbc,sbc,nbc,stagdim,istatus)
                      0,0,0,0,0,istatus)

            zdr_base=0
            DO k=2,nz-1
              IF (ctmp_3d(i,j,k-1) < 273.15 .AND. &
                  rhv_mos_3d(i,j,k) > 0.85 .AND. (MAXVAL(ref_large_3d(i-2:i+2,j-2:j+2,2:nz-10))> 30) .AND. &
                  zdr_mos_3d(i,j,k) >= 1.0 .AND. zdr_mos_3d(i,j,k-1) >= 1.0  .AND. &
                  (zs(i,j,k-1)-melting_height).lt. 1000) THEN
                !write(6,*) ref_mos_3d(i,j,k),MAXVAL(ref_mos_3d(i-2:i+2,j-2:j+2,2:nz-10))
                zdr_base=zs(i,j,k-1)
                exit
              END IF
            END DO
!                        If (zdr_base .gt. 0) write(6,*) "i,j,melting_height,zdr_base:",i,j,melting_height,zdr_base
            zdr_col_depth(i,j) = 0
            DO k=2,nz-1
              !if(zdr_mos_3d(i,j,k-1)>0) write(6,*)zdr_mos_3d(i,j,k-1),zdr_mos_3d(i,j,k),"zdr"
              IF (ctmp_3d(i,j,k) < 273.15 .AND. ctmp_3d(i,j,k) > 253.15 .AND. &
                  (MAXVAL(ref_large_3d(i-2:i+2,j-2:j+2,2:nz-10))> 30) .AND. rhv_mos_3d(i,j,k) > 0.85 .AND. &
                  zdr_mos_3d(i,j,k) >= 1.0 .AND. zdr_mos_3d(i,j,k-1) >= 1.0 .AND. &
                  (zdr_base-melting_height) .lt. 1000) THEN
                !zdr_col_depth(i,j) = zdr_col_depth(i,j) + 1
                maskedarray_rhv = rhv_mos_3d(i-1:i+1,j-1:j+1,k)
                crit_rhv = COUNT(maskedarray_rhv>0.88)
                IF (crit_rhv >= 1) THEN
                  maskedarray_zdr = zdr_mos_3d(i-1:i+1,j-1:j+1,k)
                  crit_zdr = COUNT(maskedarray_zdr>=1.0 .AND. maskedarray_zdr<6.0)
                  IF (crit_zdr >= 1) THEN
                    zdr_col_depth(i,j) = zdr_col_depth(i,j) + 1
                  END IF
                END IF
              END IF
            END DO
 !           if(zdr_col_depth(i,j)>0)       write(6,*)zdr_col_depth(i,j),"zdrdepth",i,j
          END DO
        END DO

        DO j=2, ny-1
          DO i=2, nx-1
            zdr_mos_3d(i,j,nz-1)=zdr_col_depth(i,j)
          END DO
        END DO
        CALL print3dnc_lg(101,'zdr_after',zdr_mos_3d,nx,ny,nz)

        DO j=2, ny-1
          DO i=2, nx-1
            melting_z=0
            DO k = 10,40
              IF (ctmp_3d(i,j,k) < 273.15) THEN
                melting_z = k
                exit
              END IF
            END DO

            DO k=2, nz-1
              zdr_col_wgt_diag = 0
              zdr_col_wgt      = 0
              !IF(clouds_3d(i,j,k) > cvr2rh_thr1) THEN
              IF (ref_mos_3d(i,j,k) >= 10. .AND. rh_3d(i,j,k) > 80. .AND. &
                  zdr_col_depth(i,j) == 0.0 .AND. ctmp_3d(i,j,k) < 273.15 .AND. &
                  MOD(i,2)==0 .AND. MOD(j,2)==0) THEN

                rh_3d(i,j,k) = rh_3d(i,j,k) ! - 0.3*(rh_3d(i,j,k) - 80)

              ELSE IF (zdr_col_depth(i,j) >= 1 .AND. rh_3d(i,j,k) < 100 ) THEN
               ! WRITE(6,'(a)')'Detected ZDR Column grid cell. Saturating column. '
                IF (clouds_3d(i,j,k) > cvr2rh_thr1) THEN
                  ! zdr_col_wgt_diag = FLOOR(zdr_col_depth(i,j)/2)
                  zdr_col_wgt_diag = zdr_col_depth(i,j)/2
                  zdr_col_wgt  = zdr_col_depth(i,j)
                  !  zdr_col_wgt  = FLOOR(zdr_col_depth(i,j))
                  !write(6,*)i,j,zdr_col_wgt,zdr_col_depth(i,j),zdr_col_wgt_diag,nx,ny
                  !rh_3d(i,j,k) = 100.
                  !rh_3d(i,j,13:38) = 100.
                  !rh_3d(i:i+zdr_col_wgt,j,13:38) = 100.
                  !rh_3d(i-zdr_col_wgt:i,j,13:38) = 100.
                  !rh_3d(i,j:j+zdr_col_wgt,13:38) = 100.
                  !rh_3d(i,j-zdr_col_wgt:j,13:38) = 100.
                  !rh_3d(i:i+zdr_col_wgt_diag,j:j+zdr_col_wgt_diag,13:38) = 100.
                  !rh_3d(i:i+zdr_col_wgt_diag,j-zdr_col_wgt_diag:j,13:38) = 100.
                  !rh_3d(i-zdr_col_wgt_diag:i,j:j+zdr_col_wgt_diag,13:38) = 100.
                  !rh_3d(i-zdr_col_wgt_diag:i,j-zdr_col_wgt_diag:j,13:38) = 100.
                  !
                  ! need extra buffer for message passing
                  !if (zdr_col_depth(i,j) >= 3) then
                  ! if (zdr_col_depth(i,j) <= 1 .and. rh_3d(i,j,k) .le. 100.) then
                  !       IF (k .gt. melting_z-3 .and. k .lt. melting_z+10) THEN
                  !         rh_3d(i,j,k) =  100.
                  !       END IF
                  IF  (zdr_col_depth(i,j) >= 2 .AND. rh_3d(i,j,k) .LE. 100.) THEN
                    IF (k .GT. 5 .and. k .LT. nz-5) THEN
                      rh_3d(i,j,k) =  100.
                    END IF
                  END IF
                  !IF (i-zdr_col_wgt >= 1 .AND. i+zdr_col_wgt <= nx-1 .AND. &
                  !    j-zdr_col_Wgt >= 1 .AND. j+zdr_col_wgt <= ny-1) THEN
                  !  DO i1=i-zdr_col_wgt,i+zdr_col_wgt
                  !    DO j1=j-zdr_col_wgt,j+zdr_col_wgt
                  !      rh_3d(i1,j1,k) = 100.
                  !    END DO
                  !  END DO
                  !END IF

                END IF
              END IF

            END DO
          END DO
        END DO

        CALL mpsendrecv2dew(rh_3d,nx,ny,nz,0,0,0,tem1)
        CALL mpsendrecv2dns(rh_3d,nx,ny,nz,0,0,0,tem1)

        DEALLOCATE(ref_large_3d)

        CALL print3dnc_lg(101,'retrievedRH',rh_3d,nx,ny,nz)

        !IF (myproc == 0) WRITE(6,*) "what is going on1 "

      CASE ( 4 ) ! test by anwei lai 2017/10/09

        DO j=1,ny-1
          DO i=1,nx-1
            rh100_flag = 0
            DO k=2,nz-1
               IF(ref_mos_3d(i,j,k) >= 40.)THEN
                  rh100_flag = 1
                  exit
               END IF
            END DO

            IF (rh100_flag == 1) THEN
              rh_3d(i,j,13:38) = 100.
            END IF
            DO k=2,nz-1
               IF(ref_mos_3d(i,j,k) <=0.and.ref_mos_3d(i,j,k) >-90.)THEN
                !  IF (myproc == 0)WRITE(6,*)i,j,k,ref_mos_3d(i,j,k)
                  IF(rh_3d(i,j,k)>90.)THEN
                     rh_3d(i,j,k) = rh_3d(i,j,k)*0.7
                  ELSE IF (rh_3d(i,j,k)>80.)THEN
                      rh_3d(i,j,k) = rh_3d(i,j,k)*0.8
                  END IF
              END IF
            END DO
          END DO
        END DO

      CASE ( 5 ) ! test by anwei lai 2017/10/09
        DO j=1,ny-1
           DO i=1,nx-1
              rh100_flag = 0
              DO k=2,nz-1
                 IF(ref_mos_3d(i,j,k) >= 40.)THEN
                    rh100_flag = rh100_flag + 1
                 END IF
              END DO
!!!!!!!!!!!!!!!!!!find the top of radar echo top lvl
              rh100_flag_top_lvl = 1
              lcl_model_lvl      = 13

              IF (rh100_flag >=1)THEN
                 DO k=2,nz-1  !k is small at the surface
               !  write(6,*)"ref_mos",ref_mos_3d(i,j,k),k,nz-1
                   IF(ref_mos_3d(i,j,k) >= 18.5.and.ref_mos_3d(i,j,k+1)<18.5)THEN
                     rh100_flag_top_lvl = k
                   END IF
                 END DO
!!!!!!!!!!!!!!!!!find the LCL for the module level
                t_model_surf_c  = 2.*t_3d(i,j,2)-t_3d(i,j,3)-273.15
                rh_model_surf_c = 2.*rh_3d(i,j,2)-rh_3d(i,j,3)
                td_model_surf_c = dwpt(t_model_surf_c,rh_model_surf_c)
                z_lcl(i,j)= (t_model_surf_c-td_model_surf_c)/gamma_d
                DO k=1,nz-1
                   IF(z_lcl(i,j)>=zs(i,j,k).and.z_lcl(i,j)<zs(i,j,k+1))THEN
                      lcl_model_lvl = k
                      DO kk=1,13
                        IF(ref_mos_3d(i,j,lcl_model_lvl) < 0.)THEN
                             lcl_model_lvl =lcl_model_lvl + 1
                        ELSE
                           exit
                        ENDIF
                      ENDDO

                      WRITE(6,*)z_lcl(i,j),zs(i,j,k),zs(i,j,k+1),k
                      !exit
                  END IF
                END DO
                !WRITE(6,*),'top lvl,bottomlvl',&
                !    rh100_flag_top_lvl, lcl_model_lvl,i,j

                IF(rh100_flag_top_lvl >= lcl_model_lvl)  &
                    rh_3d(i,j,lcl_model_lvl:rh100_flag_top_lvl) = 100.

              ELSE
                DO k=2,nz-1   ! no precipitaion echo
                   IF(ref_mos_3d(i,j,k) <=0.and.ref_mos_3d(i,j,k)>-90. &
                     .and.rh_3d(i,j,k)>=90..and.t_3d(i,j,k)<273.15)THEN
                       rh_3d(i,j,k) = rh_3d(i,j,k)!-(rh_3d(i,j,k)-80)*0.5
                 !  IF(ref_mos_3d(i,j,k) <=0.and.ref_mos_3d(i,j,k)>-90. &
                 !    .and.rh_3d(i,j,k)>=80.)THEN
                 !      rh_3d(i,j,k) = rh_3d(i,j,k)-(rh_3d(i,j,k)-80)*0.5
                 !  IF(ref_mos_3d(i,j,k) <=0.and.ref_mos_3d(i,j,k)>-90. &
                 !    )THEN
                 !      rh_3d(i,j,k) = rh_3d(i,j,k)-(rh_3d(i,j,k)-80)*0.5
                   END IF
                END DO
              END IF

           END DO
         END DO

      CASE (6)
        ref_bkg = 0.0
        !not considered the hail
        CALL reflec_g(nx,ny,nz,ref_mos_3d,10.,0.,rhobar,                &
                      qscalar(1,1,1,P_QR),qscalar(1,1,1,P_QS),          &
                      qscalar(1,1,1,P_QG), t_3d, ref_bkg)

         DO  j=1,ny
           DO i=1,nx
             lw = 0.0
             DB = 0.0
             ZE = 0.0
             vil_from_dbz(i,j)= 0.0
             DO k=2,nz-1
                IF (ref_mos_3d(i,j,k) >10.0)THEN
                  ZE = 10**(0.1*ref_mos_3d(i,j,k))
                  lw = 0.00344*ZE**vil_4d7_index !(4.0/7.0)
                  DB = 0.0005*(zs(i,j,k+1)-zs(i,j,k-1))
                  vil_from_dbz(i,j) = vil_from_dbz(i,j) + lw*DB
                END IF
             END DO

             ! calculated the background VIL

             vil_from_bkg(i,j) = 0.0

             DO k=2,nz-1
                IF(ref_bkg(i,j,k) > 10.0 ) THEN
                  ZE_bkg = 10**(0.1*ref_bkg(i,j,k))
                  lw_bkg = 0.00344*ZE_bkg**vil_4d7_index !(4.0/7.0)
                  DB_bkg = 0.0005*(zs(i,j,k+1)-zs(i,j,k-1))
                  vil_from_bkg(i,j) = vil_from_bkg(i,j) + lw_bkg*DB_bkg
                END IF
             END DO
           END DO
         END DO

         DO  j=2,ny-2
           DO i=2,nx-2
              maskedarray_rhv = 0.0
              DO jj=-1,1,1
                DO ii=-1,1,1
                   maskedarray_rhv(ii,jj) = vil_from_dbz(i+ii,j+jj)
                END DO
              END DO
              crit_rhv =0
              crit_rhv = COUNT(maskedarray_rhv>=6.5)
              rh100_flag = 0
              IF(crit_rhv>=1 .and. maxval(ref_mos_3d(i,j,:))>=35.)THEN
                 rh100_flag = 1
                 !print*,"vil,",vil_from_dbz(i,j),maxval(ref_mos_3d(i,j,:)),i,j
              END IF

              maskedarray_rhv_bkg  = 0.0
              crit_bkg         = 0
              DO jj=-1,1,1
                DO ii=-1,1,1
                   maskedarray_rhv_bkg (ii,jj)  = vil_from_bkg(i+ii,j+jj)
                END DO
              END DO
              crit_bkg       = COUNT(maskedarray_rhv_bkg >= 6.5 )
              rh100_flag_bkg = 0
              IF(crit_bkg >=1) THEN
                rh100_flag_bkg = 1   !0--background not convective !1--convective
                !print*,'bg vil',vil_from_bkg(i,j),maxval(ref_bkg(i,j,:)),i,j
              ENDIF

!!!!!!!!!!!!!!!!!!find the top of radar echo top lvl
              rh100_flag_top_lvl = 1
              lcl_model_lvl      = 13
!!!!!!!!!!!!!!!!!find the LCL for the module level
              t_model_surf_c  = 2.*t_3d(i,j,2)-t_3d(i,j,3)-273.15
              rh_model_surf_c = 2.*rh_3d(i,j,2)-rh_3d(i,j,3)
              td_model_surf_c = dwpt(t_model_surf_c,rh_model_surf_c)

              z_lcl(i,j)= (t_model_surf_c-td_model_surf_c)/gamma_d
              DO k=1,nz-1
                 IF (z_lcl(i,j)>=zs(i,j,k) .and. z_lcl(i,j)<zs(i,j,k+1)) THEN
                   lcl_model_lvl = k
                 END IF
              END DO
              lcl_model_lvl = MAX(lcl_model_lvl,2)
!              IF (rh100_flag >=1)THEN
              IF (rh100_flag >=1 .and. rh100_flag_bkg == 0)THEN
                 DO k=2,nz-1  !k is small at the surface
                   IF(ref_mos_3d(i,j,k) >=18.5.and.ref_mos_3d(i,j,k+1)<18.5)THEN
                     rh100_flag_top_lvl = k
                   END IF
                 END DO
!                print*,"echo toplvl,",rh100_flag_top_lvl,&
!                 ref_mos_3d(i,j,rh100_flag_top_lvl),zs(i,j,rh100_flag_top_lvl)

                 IF(rh100_flag_top_lvl >= lcl_model_lvl)  &
                    rh_3d(i,j,lcl_model_lvl:rh100_flag_top_lvl) = 100.

              ELSE IF(rh100_flag <1 .and. rh100_flag_bkg == 1) THEN
                 DO k=lcl_model_lvl,31
                    rh_3d(i,j,k) = rh_3d(i,j,k)*0.95
                 END DO
              ELSE IF(rh100_flag <1 .and. rh100_flag_bkg == 0) THEN
  !              DO k=1,nz-1
                DO k=lcl_model_lvl,31
                    IF(zs(i,j,k)<600)THEN
                      rh_thresh = 95.
                    ELSE IF(zs(i,j,k)>=600 .and. zs(i,j,k)<1500) THEN
                      rh_thresh = 90.
                    ELSE IF (zs(i,j,k)>=1500 .and. zs(i,j,k)<2500) THEN
                      rh_thresh = 85.
                    ELSE IF(zs(i,j,k)>=2500)THEN
                      rh_thresh = 75.
                    END IF
                    num_norain = 0
                    num_miss   = 0

                    maskedarray_refobs = ref_mos_3d(i-1:i+1,j-1:j+1,k-1:k+1)
                    max_refobs = maxval(maskedarray_refobs)- &
                                 minval(maskedarray_refobs)
                    num_norain = COUNT(maskedarray_refobs <5.)
                    num_miss = COUNT(maskedarray_refobs <0.)
                    IF((num_norain-num_miss) >18 .and. max_refobs <30 .and. &
                       ref_bkg(i,j,k)>=40 .and. rh_3d(i,j,k)>rh_thresh)THEN
                       rh_3d(i,j,k) =rh_3d(i,j,k) -0.5*(rh_3d(i,j,k)-rh_thresh)
                    END IF
                 END DO
  !                ELSE
  !                    DO k=lcl_model_lvl,nz-1
 !                      IF(ref_mos_3d(i,j,k) <=5.and.ref_mos_3d(i,j,k)>-90.and.&
 !                         ref_bkg(i,j,k)>20.and.rh_3d(i,j,k)>rh_thresh)THEN
 !
 !                           print*,rh_3d(i,j,k)
 !                           rh_3d(i,j,k) =rh_3d(i,j,k) - 0.5*(rh_3d(i,j,k)-rh_thresh)
 !                           print*,rh_3d(i,j,k),ref_mos_3d(i,j,k),ref_bkg(i,j,k)
 !           !                rh_3d(i,j,k) =rh_3d(i,j,k) - 0.5*(rh_3d(i,j,k)-rh_thresh)
            !                exit
 !                      ENDIF
  !                    END DO
  !               END IF
 !               END DO
  !               print*,ref_bkg(i,j,k),ref_mos_3d(i,j,k)
              END IF
          END DO
        END DO
      CASE DEFAULT
        IF (myproc == 0) WRITE(6,'(a,i4,a)')  &
          ' WARNING cldqvopt =',cldqvopt,' not supported, skipping...'
    END SELECT

    IF (mp_opt > 0) THEN  ! Before smoothing, the arrays must be MPI valid
      CALL acct_interrupt(mp_acct)
      CALL mpsendrecv2dew(rh_3d,nx,ny,nz,ebc,wbc,0,tem1)
      CALL mpsendrecv2dns(rh_3d,nx,ny,nz,nbc,sbc,0,tem1)
    END IF

    IF (smth_opt == 1) THEN
      DO k=2,nz-1
        CALL smooth9p(rh_3d(1,1,k),nx,ny,1,nx-1,1,ny-1,0,tem1(1,1,1))
      END DO
    ELSE IF(smth_opt == 2) THEN
      CALL smooth3d(nx,ny,nz, 1,nx-1,1,ny-1,2,nz-1,0,smfct1,zs,         &
                    rh_3d,tem1,rh_3d)

    END IF
!
!-----------------------------------------------------------------------
!
!  Convert the rh* field back into qv-field
!  Where precip was removed, set qv according to rhsubsat
!
!-----------------------------------------------------------------------
!
!    IF (myproc == 0) print *, 'Here QV adjustment loop 1'
!   DO k=2,nz-1
    DO k=2,31
      DO j=1,ny-1
        DO i=1,nx-1
!         tgrid =(ptprt(i,j,k)+ptbar(i,j,k))*((p_3d(i,j,k)/p0)**rddcp) ! TONG
!         qvsat=f_qvsat( p_3d(i,j,k), t)                           ! TONG
          qvsat=f_qvsat( p_3d(i,j,k), t_3d(i,j,k))
          rh2qv=0.01*rh_3d(i,j,k)*qvsat
          qpcp=0.0
          IF(P_QR > 0) qpcp=qpcp+qstem(i,j,k,P_QR)
          IF(P_QS > 0) qpcp=qpcp+qstem(i,j,k,P_QS)
          IF(P_QG > 0) qpcp=qpcp+qstem(i,j,k,P_QG)
          IF(P_QH > 0) qpcp=qpcp+qstem(i,j,k,P_QH)
          !comment by chq
          !IF ( qpcp == 0.0 ) THEN  ! no precip here
          !  qw_new = 0.0
          !  DO nq=1,nscalarq
          !    qw_new = qw_new + qstem(i,j,k,nq)
          !  END DO
          !  dqw = qw_new - qw_old(i,j,k)
          !  IF( dqw < 0.0 ) THEN   ! total water was reduced
          !    qvtem(i,j,k)=MIN(qv(i,j,k),rh2qv)
          !    qvtem(i,j,k)=MIN(qvtem(i,j,k),(rhsubsat*qvsat))
          !  ELSE
          !    qvtem(i,j,k)=MAX(qvtem(i,j,k),rh2qv)
          !  END IF
          ! ELSE
          !   IF( w(i,j,k) > -0.2) THEN
                qvtem(i,j,k)=rh2qv
          !   ELSE
          !     qvtem(i,j,k)=MIN(qv(i,j,k),rh2qv)
          !   END IF
          ! END IF
        END DO
      END DO
    END DO

  END IF          ! cldqvopt=1
!
!-----------------------------------------------------------------------
!
!  Adjust the perturbation potential temperature field to preserve
!  the previous buoyancy field.
!
!-----------------------------------------------------------------------
!
  IF (cldptopt > 0 .AND. cldptopt < 3) THEN
    IF (myproc == 0) THEN
      WRITE(6,'(a)')' Adjusting ptprt-field to preserve buoyancy'
      PRINT*,'frac of qw:',frac_qw_2_pt
    END IF
    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          qw_new = 0.0
          DO nq=1,nscalarq
            qw_new = qw_new + qstem(i,j,k,nq)
          END DO
          dqw = qw_new - qw_old(i,j,k)
          IF (cldptopt == 1) THEN
            arg =  dqw/(1.0+qvbar(i,j,k))
          ELSE
            dqv_prt = qvtem(i,j,k) - qvbar(i,j,k) - qvprt_old(i,j,k)
            arg = (dqv_prt + dqw)/(1.0+qvbar(i,j,k))                    &
                    - dqv_prt/(0.622+qvbar(i,j,k))
          END IF
          pttem(i,j,k) = pttem(i,j,k) + ptbar(i,j,k)*arg*frac_qw_2_pt
        END DO
      END DO
    END DO
!
!-----------------------------------------------------------------------
!
!  Re-adjust the qv field to account for the temp. adjustment
!  Where the water fields were reduced, limit rh to rhsubsat
!
!-----------------------------------------------------------------------
!
    IF (cldqvopt == 1) THEN

      print *, ' Here QV adjustment loop 2'

      DO k=2,nz-1
        DO j=1,ny-1
          DO i=1,nx-1
            tgrid =(pttem(i,j,k)+ptbar(i,j,k))*((p_3d(i,j,k)/p0)**rddcp)
            qvsat=f_qvsat( p_3d(i,j,k), tgrid )
            rh2qv=0.01*rh_3d(i,j,k)*qvsat
            qpcp=0.0
            IF(P_QR > 0) qpcp=qpcp+qstem(i,j,k,P_QR)
            IF(P_QS > 0) qpcp=qpcp+qstem(i,j,k,P_QS)
            IF(P_QG > 0) qpcp=qpcp+qstem(i,j,k,P_QG)
            IF(P_QH > 0) qpcp=qpcp+qstem(i,j,k,P_QH)
            IF ( qpcp == 0.0 ) THEN  ! no precip here
              qw_new = 0.0
              DO nq=1,nscalarq
                qw_new = qw_new + qstem(i,j,k,nq)
              END DO
              dqw = qw_new - qw_old(i,j,k)
              IF( dqw < 0.0 ) THEN   ! total water was reduced
                qvtem(i,j,k)=MIN(qvtem(i,j,k),rh2qv)
                qvtem(i,j,k)=MIN(qvtem(i,j,k),(rhsubsat*qvsat))
              ELSE
                qvtem(i,j,k)=MAX(qvtem(i,j,k),rh2qv)
              END IF
            ELSE
              qvtem(i,j,k)=MAX(qvtem(i,j,k),rh2qv)
            END IF
          END DO
        END DO
      END DO

    END IF          ! cldqvopt=1
  END IF     ! cldptopt.eq.1?
!
!
!-----------------------------------------------------------------------
!
!  Enhance and THEN smooth w-field using analyzed in-cloud w-field.
!
!-----------------------------------------------------------------------
!
  IF (cldwopt == 1) THEN
    IF (myproc == 0) WRITE(6,'(a,f8.4)')  &
      ' Enhancing w-field for areas w/ cldcvr >', thresh_cvr

    DO k=2,nz-1
      DO j=1,ny-1
        DO i=1,nx-1
          IF(w_cld(i,j,k) > w(i,j,k)) THEN
            w(i,j,k) = w_cld(i,j,k)
          END IF
        END DO
      END DO
    END DO

    IF (mp_opt > 0) THEN  ! Before smoothing, the arrays must be MPI valid
      CALL acct_interrupt(mp_acct)
      CALL mpsendrecv2dew(w,nx,ny,nz,ebc,wbc,3,tem1)
      CALL mpsendrecv2dns(w,nx,ny,nz,nbc,sbc,3,tem1)
    END IF

    IF (smth_opt == 1) THEN
      DO k=2,nz-1
        CALL smooth9p(w(1,1,k),nx,ny,1,nx-1,1,ny-1,0,tem1(1,1,1))
      END DO
    ELSE IF(smth_opt == 2) THEN
      CALL smooth3d(nx,ny,nz, 1,nx-1,1,ny-1,2,nz-1,3,smfct1,zs,         &
                    w(1,1,1),tem1,w(1,1,1))
    END IF

  END IF   ! cldwopt = 1

!-----------------------------------------------------------------------
!
! PSEUDOQOBS options
!
!-----------------------------------------------------------------------

  IF (pseudoQobs_opt == 0) THEN
    DO nq = 1, nscalar
      qscalar(:,:,:,nq) = qstem(:,:,:,nq)
    END DO
    ptprt(:,:,:) = pttem(:,:,:)
    qv(:,:,:)    = qvtem(:,:,:)
  ELSE
    CALL pseudoQ_init(nx,ny,nz,nscalarq,zs,ref_mos_3d,ptbar,               &
                      pttem,qvtem,qstem,istatus)
  END IF

!
!-----------------------------------------------------------------------
!
  999   CONTINUE

  RETURN
END SUBROUTINE cmpclddrv
!
!##################################################################
!##################################################################
!######                                                      ######
!######             SUBROUTINE INICLDGRD                     ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!
!

SUBROUTINE inicldgrd (nx,ny,nz,zs,                                      &
           bgqcopt,default_clear_cover,                                 &
           topo,t_3d,p_3d,rh_3d,cf_modelfg,t_sfc_k,psfc_pa,             &
           cldcv,wtcldcv,z_ref_lcl,z_lcl,rh_modelfg,                    &
           r_missing,tem1,istatus)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Create background temperature and cloud fractional
!  cover fields on the cloud analysis grid (which has the same
!  horizontal grid as the ADAS, but the vertical levels are
!  different from ADAS).
!
!  This should probably be free of very small scale horizontal
!  structures for best results when combining with the satellite
!  data (cloud analysis uses surface observations, radar echo,
!  and satellite imagery data).
!
!-----------------------------------------------------------------------
!
!  AUTHOR: (Jian Zhang)
!  03/1996
!
!  MODIFICATION HISTORY
!
!  03/14/97  J. Zhang
!            Cleaning up the code and implemented for the official
!            arps4.2.4 version
!  09/10/97  J. Zhang
!            Using a quadratic relationship for deriving cloud
!            fractional cover field from gridded relative humidity
!            analysis. Added calculation for lifting condensation
!            levels.
!  09/15/97  J. Zhang
!            Fixed a bug when calling function rh_to_cldcv
!  05/06/98  J. Zhang
!            Abandoned the cloud grid, using the ARPS grid instead.
!
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!
!  INPUT:
!
!  nx, ny, nz              ! ADAS grid size.
!
!c Analysis variables
!
!  zs (nx,ny,nz)           ! The physical height coordinate
                           ! defined at w-point of staggered grid.
!  t_3d (nx,ny,nz)         ! Temperature field
!  p_3d (nx,ny,nz)         ! Pressure field
!  rh_3d(nx,ny,nz)         ! Relative humidity field
!
!  OUTPUT:
!
!   REAL*4 cf_modelfg(nx,ny,nz)  ! model first guess for cld cv.
!   REAL*4 rh_modelfg(nx,ny,nz)  ! model first guess for RH
!
!-----------------------------------------------------------------------
!
  IMPLICIT NONE

  INCLUDE 'mp.inc'
  INCLUDE 'bndry.inc'
!
!-----------------------------------------------------------------------
!
!  Variables declaration
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
  INTEGER :: nx,ny,nz              ! ADAS grid size
!
  INTEGER :: bgqcopt
  REAL :: z_ref_lcl                ! ref. level for computing LCL
  REAL :: default_clear_cover      ! default value for clear sky
  REAL :: r_missing                ! bad or missing data flag
!
  REAL :: zs (nx,ny,nz)            ! Hgts of each ADAS gird pt.
  REAL :: topo (nx,ny)             ! Terrain height
  REAL :: t_3d (nx,ny,nz)          ! Temperature field (K)
  REAL :: p_3d (nx,ny,nz)          ! Pressure field (pa)
  REAL :: rh_3d (nx,ny,nz)         ! relative humidity (0-100) field
!
!  OUTPUT:
!
  REAL :: cf_modelfg(nx,ny,nz)     ! Output, 1st guess of cloud
                                 ! cover on cloud height grid
  REAL :: rh_modelfg(nx,ny,nz)     ! Output, 1st guess of relative
                                 ! humidity on cloud height grid
  REAL :: t_sfc_k(nx,ny)           ! Air temp. at sfc
  REAL :: psfc_pa(nx,ny)           ! pressure (Pascal) at sfc
  REAL :: z_lcl(nx,ny)             ! lifting condensatn lvl (MSL)
!
  REAL :: cldcv (nx,ny,nz)         ! 3D gridded frac cld cv analysis.
  REAL :: wtcldcv (nx,ny,nz)       ! wgt assigned to cld cvr analysis
!
  INTEGER :: istatus               ! flag indicate process status
!
!   WORK variable:
!
  REAL :: tem1 (nx,ny,nz)          ! MPI temporary variable
!
!  FUNCTIONS:
  REAL :: rh_to_cldcv,dwpt
!
!  CONSTANTS:
  REAL :: gamma_d   ! dry adiabatic lapse rate (K/m)
  PARAMETER (gamma_d = 9.8/1004.0)
!
!-----------------------------------------------------------------------
!
!  Misc. local variables:
!
!-----------------------------------------------------------------------
!
  INTEGER :: i,j,k
  REAL :: arg,frac_z
  REAL :: t_ref_k,t_ref_c,rh_ref,td_ref_c,z_ref
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
  IF (myproc == 0) WRITE(6,*) 'Initializing the cloud analysis grid'
  istatus = 0
!
!-----------------------------------------------------------------------
!
!  Initialize surface temperature fields with missing flag
!  Initialize background cloud cover and the weight arrays.
!  Initialize model first guess fields with default values
!
!-----------------------------------------------------------------------
!
  DO j=1,ny
    DO i=1,nx
      t_sfc_k(i,j) = r_missing
    END DO
  END DO

  DO k=1,nz
    DO j=1,ny
      DO i=1,nx
        cldcv(i,j,k) = r_missing
        wtcldcv(i,j,k) = r_missing
        cf_modelfg(i,j,k) = default_clear_cover
        rh_modelfg(i,j,k) = 0.0
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Calculate the surface temperature using standard atmosphere
!  profile.
!
!-----------------------------------------------------------------------
!
  DO j = 1, ny-1
    DO i = 1, nx-1
      psfc_pa(i,j) = 2.*p_3d(i,j,2)-p_3d(i,j,3)
      t_sfc_k(i,j) = 2.*t_3d(i,j,2)-t_3d(i,j,3)
    END DO
  END DO
  DO i = 1, nx-1
    psfc_pa(i,ny) = psfc_pa(i,ny-1)
    t_sfc_k(i,ny) = t_sfc_k(i,ny-1)
  END DO
  DO j = 1, ny
    psfc_pa(nx,j) = psfc_pa(nx-1,j)
    t_sfc_k(nx,j) = t_sfc_k(nx-1,j)
  END DO
!
!-----------------------------------------------------------------------
!
!  Find the lifting condensation level
!
!-----------------------------------------------------------------------
!
  DO j = 1,ny-1
    DO i = 1,nx-1
      z_ref = z_ref_lcl + topo(i,j)
      z_ref = min(max(z_ref,(zs(i,j,2)+1.0)),(zs(i,j,nz-1)-1.0))
!
!-----------------------------------------------------------------------
!
!  Find the model temperature and dewpoint at the reference level.
!
!-----------------------------------------------------------------------
!
      DO k = 3,nz-1
        IF (z_ref >= zs(i,j,k-1)) THEN
          frac_z = (z_ref-zs(i,j,k-1))/(zs(i,j,k)-zs(i,j,k-1))
          t_ref_k = t_3d(i,j,k-1)                                       &
                     + frac_z*(t_3d(i,j,k)-t_3d(i,j,k-1))
          t_ref_c = t_ref_k - 273.15
!
          rh_ref = rh_3d(i,j,k-1)                                       &
                 + frac_z*(rh_3d(i,j,k)-rh_3d(i,j,k-1))
          td_ref_c = dwpt(t_ref_c,rh_ref)
        END IF
      END DO  ! k = 2,nz-1
!
      z_lcl(i,j) = z_ref + (t_ref_c - td_ref_c)/gamma_d
      z_lcl(i,j) = min(zs(i,j,nz-1),max(z_lcl(i,j),zs(i,j,2)))

    END DO  ! I
  END DO  ! J
  DO j = 1, ny-1
    z_lcl(nx,j) = z_lcl(nx-1,j)
  END DO
  DO i = 1, nx
    z_lcl(i,ny) = z_lcl(i,ny-1)
  END DO
!
!-----------------------------------------------------------------------
!
!  Remap model temperature and rh fields to cloud height grid
!  and convert model rh to cloud cover
!
!-----------------------------------------------------------------------
!
  IF (myproc == 0) WRITE(6,*)
!
  DO k = 2,nz-1
    DO j = 1,ny-1
      DO i = 1,nx-1
        rh_modelfg(i,j,k) = 0.01*rh_3d(i,j,k)
        IF (zs(i,j,k) >= z_lcl(i,j) .AND. bgqcopt > 0) THEN
          arg = zs(i,j,k) - topo(i,j)
          cf_modelfg(i,j,k) = rh_to_cldcv(rh_modelfg(i,j,k),arg)
        END IF
      END DO  ! I
    END DO  ! J
  END DO  ! K

  DO i = 1, nx-1
    DO j = 1, ny-1
      rh_modelfg(i,j,1) = rh_modelfg(i,j,2)
      rh_modelfg(i,j,nz) = rh_modelfg(i,j,nz-1)
    END DO
  END DO

  DO k = 1,nz
    DO j = 1,ny-1
      rh_modelfg(nx,j,k) = rh_modelfg(nx-1,j,k)
      cf_modelfg(nx,j,k) = cf_modelfg(nx-1,j,k)
    END DO  ! J
    DO i = 1,nx
      rh_modelfg(i,ny,k) = rh_modelfg(i,ny-1,k)
      cf_modelfg(i,ny,k) = cf_modelfg(i,ny-1,k)
    END DO  ! I

  END DO  ! K

  IF (mp_opt > 0) THEN
    CALL mpsendrecv2dew(cf_modelfg,nx,ny,nz,ebc,wbc,1,tem1)
    CALL mpsendrecv2dns(cf_modelfg,nx,ny,nz,nbc,sbc,2,tem1)
  END IF

  istatus=1

  RETURN
END SUBROUTINE inicldgrd

!##################################################################
!##################################################################
!######                                                      ######
!######                SUBROUTINE READRAD_JZ                 ######
!######                                                      ######
!######                     Developed by                     ######
!######     Center for Analysis and Prediction of Storms     ######
!######                University of Oklahoma                ######
!######                                                      ######
!##################################################################
!##################################################################
!

SUBROUTINE readrad_jz(nx,ny,nz,isrcrad,stnrad                           &
           ,latrad,lonrad,elvrad                                        &
           ,gridvel,gridref,gridnyq,gridtim                             &
           ,istatus)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!  Reads radar data remapped on the ARPS grid.
!  This routine requires the remapping to occur on the same grid
!  as the analysis.
!
!-----------------------------------------------------------------------
!
!  AUTHOR:  Jian Zhang
!  05/1996  Read the remapped radar data which was written by the
!           corresponding output routine "wrtrad" in remaplib.f.
!
!  MODIFICATION HISTORY:
!  03/19/97  J. Zhang
!            Added a line of error message when there is trouble
!            reading a radar file.
!  04/03/97  J. Zhang
!            Added the option of reading the data file created
!            from "WTRADCOL".  Added output for the remapping
!            parameters in the radar file (e.g., strhopt,mapproj,
!            dx,dy,dz,dzmin,ctrlat,ctrlon,tlat1,tlat2,tlon,scale)
!  04/07/97  J. Zhang
!            Added  the QC for the case when i,j,k outside the model
!            domain
!  04/09/97  J. Zhang
!            Added the Initializations for gridref, girdvel...
!  04/11/97  J. Zhang
!            Include dims.inc for nx,ny,nz
!  04/14/97  J. Zhang
!            Added message output for the case when actual # of
!            radar files exceeds the maximum allowed number in the
!            ADAS include file.  When that happens, the program will
!            stop.
!  08/06/97  J. Zhang
!            Change adascld24.inc to adascld25.inc.
!  09/11/97  J. Zhang
!            Change adascld25.inc to adascld26.inc.
!
!-----------------------------------------------------------------------
!
!  INCLUDE:  (from dims.inc and adas.inc)
!
!    nx       Number of grid points in the x-direction (east/west)
!    ny       Number of grid points in the y-direction (north/south)
!    nz       Number of grid points in the vertical
!    mx_rad     maximum number of radars
!
!    nradfil    number of radar files
!    fradname   file name for radar datasets
!
!  OUTPUT:
!
!    isrcrad  index of radar source
!    stnrad   radar site name    character*4
!    latrad   latitude of radar  (degrees N)
!    lonrad   longitude of radar (degrees E)
!    elvrad   elevation of feed horn of radar (m MSL)
!
!    gridvel  radial velocity on ARPS grid
!    gridref  reflectivity on ARPS grid
!    gridnyq  nyquist velocity on ARPS grid
!    gridtim  observation time at ARPS grid
!
!    istatus  status indicator
!
!    tem1     Temporary work array.
!
!-----------------------------------------------------------------------
!
!  Variable Declarations:
!
!-----------------------------------------------------------------------
!
  USE module_radarobs, ONLY : mx_rad, nradfil, radfname

  IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
!  Include files:
!
!-----------------------------------------------------------------------

  INCLUDE 'adas.inc'     ! ADAS parameters
!
!-----------------------------------------------------------------------
!
!  INPUT:
!
!-----------------------------------------------------------------------

  INTEGER :: nx,ny,nz    ! the ARPS grid size
!
!  INCLUDE:   (from adas.inc)
!  integer mx_rad
!  INPUT:   (from namelist in the .input file)
!  integer nradfil
!  character*132 fradname(mx_rad)
!
!  LOCAL:
  REAL :: readk(nz)
  REAL :: readhgt(nz)
  REAL :: readref(nz)
  REAL :: readvel(nz)
  REAL :: readnyq(nz)
  REAL :: readtim(nz)
!
  INTEGER :: kntref(nz)
  INTEGER :: kntvel(nz)
  INTEGER :: iradvr
  INTEGER :: nradvr
!
  INTEGER :: iopt_wrtrad
  PARAMETER (iopt_wrtrad=2)
!
!  OUTPUT:
  INTEGER :: istatus
!
!  OUTPUT:  ARPS radar arrays
  REAL :: gridvel(nx,ny,nz,mx_rad)
  REAL :: gridref(nx,ny,nz,mx_rad)
  REAL :: gridnyq(nx,ny,nz,mx_rad)
  REAL :: gridtim(nx,ny,nz,mx_rad)
!
!  OUTPUT:  Radar site variables
  INTEGER :: isrcrad(0:mx_rad)
  CHARACTER (LEN=5) :: stnrad(mx_rad)
  REAL :: latrad(mx_rad)
  REAL :: lonrad(mx_rad)
  REAL :: elvrad(mx_rad)
!
!-----------------------------------------------------------------------
!
!  Temporary working array
!
!-----------------------------------------------------------------------
!
  REAL :: tem1(nx,ny,nz)
!
!-----------------------------------------------------------------------
!
!  Misc. local variables
!
!-----------------------------------------------------------------------
!
  CHARACTER (LEN=4)   :: stn
  CHARACTER (LEN=6)   :: runname
  CHARACTER (LEN=256) :: fname
  INTEGER :: ireftim,itime,vcpnum,idummy
  INTEGER :: hdmpfmt,strhopt,mapprin
  INTEGER :: nchanl,ierr
  INTEGER :: iyr, imon, idy, ihr, imin, isec
  INTEGER :: i,j,k,krad,kk,ipt,klev

  REAL :: dxin,dyin,dzin,dzminin,ctrlatin
  REAL :: ctrlonin,tlat1in,tlat2in,tlonin,scalin,rdummy
  REAL :: xrd,yrd,gridlat,gridlon,elev
!
!-----------------------------------------------------------------------
!
!  Common block that stores remapping parameters for the radar
!  data file.
!
!-----------------------------------------------------------------------
!
  COMMON/remapfactrs_rad/strhopt,mapprin
  COMMON/remapfactrs_rad2/dxin,dyin,dzin,dzminin,                       &
           ctrlatin,ctrlonin,tlat1in,tlat2in,tlonin,scalin
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!  Beginning of executable code...
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!
!-----------------------------------------------------------------------
!
  istatus=0
  PRINT*,'Reading radar data from',nradfil,' sites.'
  IF (nradfil > mx_rad) THEN
    WRITE(6,'(a,i3,a,i3/a)')                                            &
        ' ERROR: nradfil ',nradfil,' exceeds mx_rad dimension',         &
        mx_rad,' please increase MX_RAD in the .inc file'
    PRINT*,' ABORTING from READRAD......'
    CALL arpsstop("MX_RAD array too small",1)
  END IF
  IF(nradfil < 1) THEN
    WRITE(6,*) 'No radar data available. Returning from READRAD...'
    RETURN
  END IF
!
!-----------------------------------------------------------------------
!
!  Initializations
!
!-----------------------------------------------------------------------
!
  DO krad = 1, mx_rad
    DO k=1,nz
      DO j=1,ny
        DO i=1,nx
          gridref(i,j,k,krad)=-9999.
          gridvel(i,j,k,krad)=-9999.
          gridnyq(i,j,k,krad)=-9999.
          gridtim(i,j,k,krad)=-9999.
        END DO
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!
!  Loop through all radars
!
!-----------------------------------------------------------------------
!
  DO krad = 1, nradfil

    fname=radfname(krad)
    CALL asnctl ('NEWLOCAL', 1, ierr)
    CALL asnfile(fname, '-F f77 -N ieee', ierr)

    CALL getunit( nchanl )
    OPEN(UNIT=nchanl,FILE=trim(fname),ERR=399,                          &
         FORM='unformatted',STATUS='old')
!
!-----------------------------------------------------------------------
!
!  Read radar description variables
!
!-----------------------------------------------------------------------
!
    istatus=1
    isrcrad(krad)=1
!
    READ(nchanl) stn
    stnrad(krad)=stn
    PRINT*,'Reading ',stnrad(krad),' radar data from ',                 &
              radfname(krad)
    READ(nchanl) ireftim,itime,vcpnum,idummy,idummy,                    &
               idummy,idummy,idummy,idummy,idummy
!
    CALL abss2ctim(itime, iyr, imon, idy, ihr, imin, isec )
    iyr=MOD(iyr,100)
    WRITE(6,815) imon,idy,iyr,ihr,imin
    815   FORMAT(i2.2,'/',i2.2,'/',i2.2,1X,i2.2,':',i2.2,' UTC')
!
    READ(nchanl) runname
    READ(nchanl) hdmpfmt,strhopt,mapprin,idummy,idummy,                 &
               idummy,idummy,idummy,idummy,idummy

    READ(nchanl) dxin,dyin,dzin,dzminin,ctrlatin,                       &
             ctrlonin,tlat1in,tlat2in,tlonin,scalin,                    &
             latrad(krad),lonrad(krad),elvrad(krad),                    &
             rdummy,rdummy
!
    IF (iopt_wrtrad == 2) THEN
!
!-----------------------------------------------------------------------
!
!  Read the data file created from subroutine "WRTRAD"
!
!-----------------------------------------------------------------------
!
      READ(nchanl) tem1    ! Reflectivity
      DO i=1,nx
        DO j=1,ny
          DO k=1,nz
            gridref(i,j,k,krad) = tem1(i,j,k)
          END DO
        END DO
      END DO
!
      READ(nchanl) tem1    ! Radial Velocity
      DO i=1,nx
        DO j=1,ny
          DO k=1,nz
            gridvel(i,j,k,krad) = tem1(i,j,k)
          END DO
        END DO
      END DO

      READ(nchanl) tem1    ! Nyquist Velocity
      DO i=1,nx
        DO j=1,ny
          DO k=1,nz
            gridnyq(i,j,k,krad) = tem1(i,j,k)
          END DO
        END DO
      END DO

      READ(nchanl) tem1    ! Time (scnds) from the reference time
      DO i=1,nx
        DO j=1,ny
          DO k=1,nz
            gridtim(i,j,k,krad) = tem1(i,j,k)
          END DO
        END DO
      END DO
!
    ELSE IF (iopt_wrtrad == 1) THEN
!
!-----------------------------------------------------------------------
!
!  Read the data file created from subroutine "WTRADCOL"
!
!-----------------------------------------------------------------------
!
      DO k=1,nz
        kntref(k) = 0
        kntvel(k) = 0
      END DO

      READ(nchanl) nradvr,iradvr
!
      DO ipt=1,(nx*ny)

        READ(nchanl,END=51) i,j,xrd,yrd,                                &
                       gridlat,gridlon,elev,klev
        READ(nchanl,END=52) (readk(kk),kk=1,klev)
        READ(nchanl,END=52) (readhgt(kk),kk=1,klev)
        READ(nchanl,END=52) (readref(kk),kk=1,klev)
        READ(nchanl,END=52) (readvel(kk),kk=1,klev)
        READ(nchanl,END=52) (readnyq(kk),kk=1,klev)
        READ(nchanl,END=52) (readtim(kk),kk=1,klev)

        IF(i <= nx.AND.i >= 1 .AND. j <= ny.AND.j >= 1) THEN
          DO kk=1,klev
            k=nint(readk(kk))
            IF(k <= nz.AND.k >= 1) THEN
              gridref(i,j,k,krad)=readref(kk)
              gridvel(i,j,k,krad)=readvel(kk)
              gridnyq(i,j,k,krad)=readnyq(kk)
              gridtim(i,j,k,krad)=readtim(kk)
              IF (gridref(i,j,k,krad) > -200.                           &
                  .AND. gridref(i,j,k,krad) < 200.)                     &
                  kntref(k)=kntref(k)+1
              IF (gridvel(i,j,k,krad) > -200.                           &
                  .AND. gridvel(i,j,k,krad) < 200.)                     &
                  kntvel(k)=kntvel(k)+1
            END IF  ! 1 < k < nz
          END DO  ! kk = 1, klev
        END IF  ! 1 < i < nx  & 1 < j < ny

      END DO  ! ipt = 1, nx*ny

      51        CONTINUE
      ipt=ipt-1
      WRITE(6,'(a,i6,a)') ' End of file reached after reading',         &
                         ipt,' columns'
      GO TO 55
      52        CONTINUE
      WRITE(6,'(a,i6,a)') ' End of file reached while reading',         &
                         ipt,' column'
      55        CONTINUE
!
!-----------------------------------------------------------------------
!
!  Write statistics
!
!-----------------------------------------------------------------------
!
      WRITE(6,'(a)') '  k   n ref    n vel'
      DO k=1,nz
        WRITE(6,'(i3,2i10)') k,kntref(k),kntvel(k)
      END DO
!
      CLOSE(nchanl)
      CALL retunit( nchanl )

    END IF  ! iopt_wrtrad
    GO TO 400

    399     CONTINUE
    PRINT*,'Error reading the radar file:',fname

    400     CONTINUE
  END DO  ! KRAD = 1, nradfil

  RETURN
END SUBROUTINE readrad_jz

!SUBROUTINE despekl(maxgate,maxazim,                                     &
!                   ngate,nazim,varchek,medfilt,rdata,tmp)
!!
!!
!!-----------------------------------------------------------------------
!!
!!  PURPOSE:
!!
!!  Despeckle radar data.
!!  Can be used for reflectivity or velocity.
!!
!!
!!  AUTHOR: Keith Brewster
!!
!!  MODIFICATION HISTORY:
!!
!!
!!-----------------------------------------------------------------------
!!
!!  INPUT:
!!    maxgate  Maximum number of gates in a radial
!!    maxazim  Maximum number of radials in a tilt
!!    ngate    Number of gates in radial
!!    nazim    Number of radials
!!    varchek  Threshold to determine good data vs. flagged
!!    medfilt  Median filter option switch
!!    rdata    Doppler radial velocity
!!
!!  OUTPUT:
!!    rdata
!!
!!  WORK ARRAYS:
!!
!!    tmp
!!
!!-----------------------------------------------------------------------
!!
!!  Variable Declarations.
!!
!!-----------------------------------------------------------------------
!!
!
!  IMPLICIT NONE
!  INTEGER, INTENT(IN) :: maxgate
!  INTEGER, INTENT(IN) :: maxazim
!  INTEGER, INTENT(IN) :: ngate
!  INTEGER, INTENT(IN) :: nazim
!
!  REAL,    INTENT(IN) :: varchek
!  INTEGER, INTENT(IN) :: medfilt
!
!  REAL, INTENT(INOUT) :: rdata(maxgate,maxazim)
!  REAL, INTENT(OUT)   :: tmp(maxgate,maxazim)
!
!!
!!-----------------------------------------------------------------------
!!
!!  Misc. local variables
!!
!!-----------------------------------------------------------------------
!!
!  INTEGER :: i,j
!  INTEGER :: NN,m,n,loc,is
!  INTEGER :: kntgd,kntdsp
!  INTEGER :: nprint
!!
!  REAL :: sortdata(9)
!  REAL :: swp
!!
!  REAL :: sum,pctdsp
!  REAL, PARAMETER :: dspmiss = -991.
!!
!!
!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!
!!  Beginning of executable code...
!!
!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!
!!
!!-----------------------------------------------------------------------
!!
!! Create temporary array where tmp=1 if non-missing tmp=0 if missing
!!
!!-----------------------------------------------------------------------
!!
!!
!  tmp=0.
!  DO j=1,nazim
!    DO i=1,ngate
!      IF ( rdata(i,j) > varchek ) tmp(i,j)=1.
!    END DO
!  END DO
!!
!!-----------------------------------------------------------------------
!!
!! If datum has fewer than 3 non-missing neighbors in a 3x3
!! square template set it to missing
!!
!!-----------------------------------------------------------------------
!!
!  kntgd=0
!  kntdsp=0
!  DO j=2,nazim-1
!    DO i=2,ngate-1
!      IF(rdata(i,j) > varchek ) THEN
!        kntgd=kntgd+1
!        sum=tmp(i-1,j+1)+tmp(i,j+1)+tmp(i+1,j+1)          &
!           +tmp(i-1,j  )+           tmp(i+1,j)            &
!           +tmp(i-1,j-1)+tmp(i,j-1)+tmp(i+1,j-1)
!        IF( sum < 3. ) THEN
!          kntdsp=kntdsp+1
!          rdata(i,j) = dspmiss
!        END IF
!      END IF
!    END DO
!  END DO
!  IF(kntgd > 0 ) THEN
!    pctdsp=100.*(float(kntdsp)/float(kntgd))
!  ELSE
!    pctdsp=0.
!  END IF
!
!  write(6,'(a,i8,a,i8,a,f6.1,a)') &
!    ' Despeckled ',kntdsp,' of ',kntgd,' data =',pctdsp,' percent.'
!
!!----------------------------------------------------------------------------
!!
!!  Zhao Kun added a median filter to smooth the reflectivity and velocity
!!  Recoded by Keith Brewster to make it optional and streamline a bit.
!!
!!----------------------------------------------------------------------------
!
!  IF(medfilt > 0) THEN
!!   nprint=0
!    DO j=1,nazim
!      DO i=1,ngate
!        tmp(i,j)=rdata(i,j)
!      END DO
!    END DO
!
!    DO j=2,nazim-1
!      DO i=2,ngate-1
!        NN=0
!        DO m=-1,1
!          DO n=-1,1
!            swp=tmp(i+m,j+n)
!            IF(swp > varchek) THEN
!              NN=NN+1
!              IF(NN == 1) THEN ! first
!                sortdata(NN)=swp
!              ELSE
!                DO loc=1,NN-1
!                  IF(swp < sortdata(loc)) THEN
!                    DO is=NN,loc+1,-1
!                      sortdata(is)=sortdata(is-1)
!                    END DO
!                    EXIT
!                  END IF
!                END DO
!                sortdata(loc)=swp
!              END IF
!            END IF
!          END DO
!        END DO
!
!        IF (NN > 6) THEN
!!         IF(nprint < 100) THEN
!!           print *, ' NN: ',NN
!!           print *, ' sortdata: ',(sortdata(m),m=1,NN)
!!           print*,'median',sortdata((NN+1)/2),rdata(i,j),tmp(i,j)
!!           nprint=nprint+1
!!         END IF
!          rdata(i,j)=sortdata((NN+1)/2)
!
!        END IF
!
!      END DO
!    END DO
!!
!!  First radial
!!
!    j=1
!    DO i=2,ngate-1
!      NN=0
!      DO m=-1,1
!        DO n=0,2
!          swp=tmp(i+m,j+n)
!          IF(swp > varchek) THEN
!            NN=NN+1
!            IF(NN == 1) THEN ! first
!              sortdata(NN)=swp
!            ELSE
!              DO loc=1,NN-1
!                IF(swp < sortdata(loc)) THEN
!                  DO is=NN,loc+1,-1
!                    sortdata(is)=sortdata(is-1)
!                  END DO
!                  EXIT
!                END IF
!              END DO
!              sortdata(loc)=swp
!            END IF
!          END IF
!        END DO
!      END DO
!
!      IF (NN > 6) THEN
!
!!       DO m=1,NN
!!         print*,'NN',m,sortdata(m)
!!       ENDDO
!!       print*,'mean',sortdata((NN+1)/2),rdata(i,j),tmp(i,j)
!        rdata(i,j)=sortdata((NN+1)/2)
!!       stop
!
!      END IF
!
!    END DO
!!
!!  Last radial
!!
!    j=nazim
!    DO i=2,ngate-1
!      NN=0
!      DO m=-1,1
!        DO n=-2,0
!          swp=tmp(i+m,j+n)
!          IF(swp > varchek) THEN
!            NN=NN+1
!            IF(NN == 1) THEN ! first
!              sortdata(NN)=swp
!            ELSE
!              DO loc=1,NN-1
!                IF(swp < sortdata(loc)) THEN
!                  DO is=NN,loc+1,-1
!                    sortdata(is)=sortdata(is-1)
!                  END DO
!                  EXIT
!                END IF
!              END DO
!              sortdata(loc)=swp
!            END IF
!          END IF
!        END DO
!      END DO
!
!      IF (NN > 6) THEN
!
!!       DO m=1,NN
!!         print*,'NN',m,sortdata(m)
!!       ENDDO
!!       print*,'mean',sortdata((NN+1)/2),rdata(i,j),tmp(i,j)
!        rdata(i,j)=sortdata((NN+1)/2)
!!       stop
!
!      END IF
!
!    END DO
!  END IF ! median filter
!
!  RETURN
!END SUBROUTINE despekl
