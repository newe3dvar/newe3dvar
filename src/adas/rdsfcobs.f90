SUBROUTINE read_surface_obs(infile,blackfile,                          &
           maxsta,istart,atime,n_meso,n_sao,n_sfcobs,                  &
           stn,obstype,lat,lon,elev,wx,                                &
           t,td,dd,ff,ddg,ffg,pstn,pmsl,alt,                           &
           kloud,ceil,lowcld,cover,rad,                                &
           idp3,store_emv,store_amt,store_hgt,vis,obstime,istatus)
!
!*******************************************************************************
!
!    Routine to read mesonet and SAO data for LAPS that has been written
!    into the .LSO file by the 'get_surface_obs' routine.
!
!    Changes:
!            P. Stamus       12-30-92  Original version.
!                            01-07-93  Add/change obs counters.
!                            01-08-93  Add read_header entry.
!            K. Brewster     12-20-99  Moved the initialization of
!                                      cloud amts to missing to
!                                      inside of station read loop
!            K. Brewster     09-10-13  Updated to allow for modified
!                                      format and to adapt to future format
!                                      changes.
!                                      Removed old counters useful only to
!                                      the old LAPS programs.
!
!    Input/Output:
!
!     Variable        Var type   I/O   Description
!    ----------      ---------- ----- -------------
!     infile            A*80      I    Directory where LSO file is.
!     maxsta             I        I    Max Number of stations allowed.
!     atime             A*24      O    Data time in dd-mmm-yyyy hh:mm
!     n_meso             I        O    Number mesonet observations
!     n_sao              I        O    Number SAO and METAR observations
!     n_sfcobs           I        O    Total number of surface obs
!     lat                RA       O    Station latitude (deg)
!     lon                RA       O    Station longitude (deg)
!     elev               RA       O    Station elevation (m)
!     obstype           A*8 A     O    Observation type (SA, SP, ASOS, etc)
!     obstime            IA       O    Time of observation (hhmm)
!     wx                A*8 A     O    Observed weather
!     t                  RA       O    Temperature (F)
!     td                 RA       O    Dewpoint (F)
!     dd                 RA       O    Wind direction (deg)
!     ff                 RA       O    Wind speed (kt)
!     ddg                RA       O    Gust wind direction (deg)
!     ffg                RA       O    Gust wind speed (kt)
!     pstn               RA       O    Station pressure (mb)
!     pmsl               RA       O    MSL pressure (mb)
!     alt                RA       O    Altimeter setting (mb)
!     kloud              IA       O    Number of cloud layers...max of 5.
!     ceil               RA       O    Ceiling height (m)
!     lowcld             RA       O    Height lowest cloud (m)
!     cover              RA       O    Cloud cover (tenths)
!     vis                RA       O    Visibility (miles)
!     rad                RA       O    Solar radiation.
!     idp3               IA       O    3-h coded pressure change (e.g.,608)
!     store_emv         A*1 A     O    Cloud descriptors: ea. layer, ea. stn
!     store_amt         A*4 A     O    Cloud layer coverage.
!     store_hgt          RA       O    Height of each cloud layer.
!     istatus            I        O    Status flag: 1 = normal
!                                                  -1 = file not found
!                                                  -2 = Arrays too small
!
!    User Notes:
!
!    1.  Arrays should be dimensioned 'maxsta' in the calling program,
!        with maxsta *at least* 120 (for CO domain).
!
!    2.  Pressures are stored as reported, except that altimeters are
!        converted to millibars.
!
!    3.  The 'kloud' variable tells whether there are clouds and how
!        many layers if there are:
!            a) kloud = 0    means   No cloud DATA (but NOT "no clouds").
!            b) kloud = 1    means   CLR or 1 cloud layer.  A height is
!                                    given for CLR which is the maximum valid
!                                    height of the observation (automatic
!                                    stations have limited valid heights).
!            c) kloud = 2-5  means   Two to five cloud layers.
!
!    4.  Thin obscured (-X) is a cloud layer and is given a 'badflag'
!        height, since it is not supposed to have a height (you're supposed
!        to be able to see other clouds and/or sky).
!
!*******************************************************************************
!
  IMPLICIT NONE
  CHARACTER (LEN=*), INTENT(IN)  :: infile
  CHARACTER (LEN=*), INTENT(IN)  :: blackfile
  INTEGER, INTENT(IN) :: maxsta
  INTEGER, INTENT(IN) :: istart
  CHARACTER (LEN=24), INTENT(OUT) :: atime
  INTEGER, INTENT(OUT) :: n_meso
  INTEGER, INTENT(OUT) :: n_sao
  INTEGER, INTENT(OUT) :: n_sfcobs

  CHARACTER (LEN=5), INTENT(OUT)  :: stn(maxsta)
  CHARACTER (LEN=8), INTENT(OUT)  :: obstype(maxsta)
  REAL, INTENT(OUT) :: lat(maxsta)
  REAL, INTENT(OUT) :: lon(maxsta)
  REAL, INTENT(OUT) :: elev(maxsta)

  CHARACTER (LEN=8), INTENT(OUT)  :: wx(maxsta)
  REAL, INTENT(OUT) :: t(maxsta)
  REAL, INTENT(OUT) :: td(maxsta)
  REAL, INTENT(OUT) :: dd(maxsta)
  REAL, INTENT(OUT) :: ff(maxsta)
  REAL, INTENT(OUT) :: ddg(maxsta)
  REAL, INTENT(OUT) :: ffg(maxsta)
  REAL, INTENT(OUT) :: pstn(maxsta)
  REAL, INTENT(OUT) :: pmsl(maxsta)
  REAL, INTENT(OUT) :: alt(maxsta)
  INTEGER, INTENT(OUT) :: kloud(maxsta)
  REAL, INTENT(OUT) :: ceil(maxsta)
  REAL, INTENT(OUT) :: lowcld(maxsta)
  REAL, INTENT(OUT) :: cover(maxsta)
  REAL, INTENT(OUT) :: rad(maxsta)
  INTEGER, INTENT(OUT) :: idp3(maxsta)
  CHARACTER (LEN=1), INTENT(OUT)  :: store_emv(maxsta,5)
  CHARACTER (LEN=4), INTENT(OUT)  :: store_amt(maxsta,5)
  REAL, INTENT(OUT) :: store_hgt(maxsta,5)
  REAL, INTENT(OUT) :: vis(maxsta)
  INTEGER, INTENT(OUT) :: obstime(maxsta)
  INTEGER, INTENT(OUT) :: istatus
!
! Misc Local Variables
!
  INTEGER, PARAMETER :: misval = 999
  REAL, PARAMETER :: badflag = -99.9
  INTEGER, PARAMETER :: inunit = 21
  CHARACTER(LEN=128) :: inline
  INTEGER :: iclose,ifmtver
  INTEGER :: iostatus
  INTEGER :: i,k,ii
!
!.....  Read the surface header
!
  iclose=0
  CALL read_surface_header(infile,inunit,iclose,ifmtver,               &
                atime,n_meso,n_sao,n_sfcobs,istatus)
!
  IF((n_sfcobs+istart-1) > maxsta) THEN
    WRITE(6,'(a,i9,/a,i9,a,/2a)')                                      &
           ' +++ ERROR in READ_SURFACE_OBS: maxsta =',maxsta,          &
           '     but there will be',(n_sfcobs+istart-1),' stations ',  &
           '     after reading file',TRIM(infile)
    WRITE(6,'(/a/)') '   Increase the value of "maxsta" and try again.'
    istatus = -2
    RETURN
  END IF
!
!.....  Now read the station data.
!
  IF( ifmtver < 2 ) THEN    ! legacy LSO format
    DO i=1,n_sfcobs
      k=i+istart-1
      READ(inunit,'(1X,a5,f6.2,1X,f7.2,1X,f5.0,1X,a8,1X,i4,1X,a8)',    &
           iostat=iostatus) stn(k),lat(k),lon(k),elev(k),obstype(k),   &
           obstime(k),wx(k)
      IF(iostatus /= 0) EXIT
      READ(inunit,*) t(k),td(k),dd(k),ff(k),ddg(k),ffg(k),             &
                   pstn(k),pmsl(k),alt(k)
!     READ(inunit,*) kloud(k),ceil(k),lowcld(k),cover(k),              &
!                  vis(k),rad(k),idp3(k)
      READ(inunit,*,iostat=iostatus) kloud(k),ceil(k),lowcld(k),       &
                                     cover(k),vis(k),rad(k),idp3(k)
      IF(iostatus /=0) THEN
        write(*,*) "Error occured at surface obs ",i," of ",n_sfcobs
        write(*,*) "Rewriting it as missing value"
        kloud(k)=0
        ceil(k)=90000.0
        lowcld(k)=90000.0
        cover(k)=0.0
        vis(k)=-99.900
        rad(k)=-99.9
        idp3(k)=-99
      END IF
!
!.....  Read the cloud data if we have any.
!
      DO ii=1,5
        store_emv(k,ii) = ' '
        store_amt(k,ii) = '    '
        store_hgt(k,ii) = badflag
      END DO ! ii
      IF(kloud(k) > 0) THEN
        DO ii=1,kloud(k)
          IF (ii < 6) THEN
            READ(inunit,'(5X,a1,1X,a4,1X,f7.1)')                       &
                 store_emv(k,ii),store_amt(k,ii),store_hgt(k,ii)
          ELSE
            READ(inunit,*)
            WRITE(6,'(2a)')  'SKIPPING extra cloud layers for station ',stn(k)
          END IF
        END DO !ii
      END IF
!
    END DO !i
  ELSE     ! Format 2 format
    DO i=1,n_sfcobs
      k=i+istart-1
      READ(inunit,'(1x,a5,1x,a8,1x,a8)',iostat=iostatus)               &
           stn(k),obstype(k),wx(k)
      IF(iostatus /= 0) EXIT
      READ(inunit,*) lat(k),lon(k),elev(k),obstime(k)
      READ(inunit,*) t(k),td(k),dd(k),ff(k),ddg(k),ffg(k),             &
                   pstn(k),pmsl(k),alt(k)
      READ(inunit,*) kloud(k),ceil(k),lowcld(k),cover(k),              &
                   vis(k),rad(k),idp3(k)
!
!.....  Read the cloud data if we have any.
!
      DO ii=1,5
        store_emv(k,ii) = ' '
        store_amt(k,ii) = '    '
        store_hgt(k,ii) = badflag
      END DO ! ii
      IF(kloud(k) > 0) THEN
        DO ii=1,kloud(k)
          IF (ii < 6) THEN
            READ(inunit,'(5X,a1,1X,a4,1X,f7.1)')                       &
                 store_emv(k,ii),store_amt(k,ii),store_hgt(k,ii)
          ELSE
            READ(inunit,*)
            WRITE(6,'(2a)')  'SKIPPING extra cloud layers for station ',stn(k)
          END IF
        END DO !ii
      END IF
!
    END DO !i
  END IF
!
  CLOSE(inunit)
!
  !CALL blklistsfc(blackfile,maxsta,istart,n_sfcobs,misval,badflag,     &
  !    stn,obstype,wx,                                                  &
  !    t,td,dd,ff,ddg,ffg,pstn,pmsl,alt,kloud,ceil,lowcld,cover,rad,    &
  !    idp3,store_emv,store_amt,store_hgt,vis,istatus)
!
!..... End of data gathering. Let's go home...
!
  istatus = 1             ! everything's ok...
  PRINT *, ' Normal completion of READ_SURFACE_OBS'
!
  RETURN
END SUBROUTINE read_surface_obs
!
!
SUBROUTINE read_surface_header(infile,inunit,iclose,ifmtver,           &
                atime,n_meso,n_sao,n_sfcobs,istatus)
!
! Determine format and read data counts from surface observation file.
!
! Ketth Brewster
!
  IMPLICIT NONE
  CHARACTER(LEN=*) :: infile
  INTEGER, INTENT(IN) :: inunit
  INTEGER, INTENT(IN) :: iclose
  INTEGER, INTENT(OUT) :: ifmtver
  CHARACTER(LEN=24) :: atime
  INTEGER, INTENT(OUT) :: n_meso
  INTEGER, INTENT(OUT) :: n_sao
  INTEGER, INTENT(OUT) :: n_sfcobs
  INTEGER, INTENT(OUT) :: istatus
!
! Misc local variables
!
  INTEGER, PARAMETER :: maxfmtver = 2
  LOGICAL, PARAMETER :: back = .TRUE.
  CHARACTER(LEN=128) :: inline
!
  INTEGER :: iostatus
  INTEGER :: locmark,ibgn,iend
  INTEGER :: n_meso_pos,n_sao_g,n_sao_pos_g,n_sao_pos_b
  INTEGER :: n_obs_g,n_obs_pos_g,n_obs_pos_b
!
! Initializations
!
  istatus=0
  ifmtver=0
  n_sao=0
  n_meso=0
  n_sfcobs=0
!
!.....  Open the file.  Check for a 'file not found' or other problem.
!
  OPEN(inunit,IOSTAT=iostatus,FILE=trim(infile),STATUS='old',          &
       ACCESS='sequential',FORM='formatted',ACTION='READ')
  IF(iostatus /= 0) THEN     ! error during open
    istatus = -1
    WRITE(6,'(3a)') ' +++ ERROR opening: ',TRIM(infile),' +++'
    WRITE(6,'(a,i5)') '    IOS code = ',iostatus
    RETURN
  END IF
!
!.....  File open...first read the header (line(s))
!       Determine the format version
!
  READ(inunit,'(a)') inline
  ifmtver=1
  IF( inline(1:7) == '#Format' ) THEN
    locmark=INDEX(TRIM(inline),'=',back)
    ibgn=locmark+1
    iend=LEN_TRIM(inline)
    READ(inline(ibgn:iend),*,iostat=iostatus) ifmtver
    IF(iostatus /= 0) THEN
      istatus=-2
      WRITE(6,'(a)') ' +++ ERROR reading format version from first line: +++'
      WRITE(6,'(2a)') '     Surface observation file: ',TRIM(infile)
      WRITE(6,'(2a)') '     First line: ',TRIM(inline)
      WRITE(6,'(a,i5)') '     IOS code = ',iostatus
      RETURN
    ELSE IF( ifmtver < 1 .OR. ifmtver > maxfmtver ) THEN
      istatus=-2
      WRITE(6,'(a)') ' +++ ERROR reading format version from first line: +++'
      WRITE(6,'(2a)') '     Surface observation file: ',TRIM(infile)
      WRITE(6,'(2a)') '     First line: ',TRIM(inline)
      WRITE(6,'(a,i5)') '     Unknown format version = ',ifmtver
      RETURN
    END IF
  END IF
!
  IF(ifmtver < 2) THEN
    READ(inline(1:25),'(1x,a24)') atime
    ibgn=26
    iend=LEN_TRIM(inline)
    READ(inline(ibgn:iend),*) &
           n_meso,          & ! # of mesonet stations
           n_meso_pos,        & ! total # mesonet stations possible
           n_sao_g,           & ! # of saos in the laps grid
           n_sao_pos_g,       & ! total # of saos possible in laps grid
           n_sao,           & ! # of saos in the box
           n_sao_pos_b,       & ! total # of saos possible in the box
           n_obs_g,           & ! # of obs in the laps grid
           n_obs_pos_g,       & ! total # of obs psbl in the laps grid
           n_sfcobs,           & ! # of obs in the box
           n_obs_pos_b        ! total # of obs possible in the box
  ELSE IF (ifmtver == 2) THEN
    READ(inunit,'(1x,a24)') atime
    READ(inunit,*) &
           n_meso,          & ! # of mesonet stations
           n_sao,           & ! # of saos in the laps grid
           n_sfcobs,        & ! # of obs in the box
           n_obs_pos_b        ! total # of obs possible in the box
  END IF

  IF(iclose > 0) CLOSE(inunit)
  RETURN
END SUBROUTINE read_surface_header

SUBROUTINE blklistsfc(blkfile,maxsta,istart,nobs,misval,rmisval,        &
           stn,obstype,wx,                                              &
           t,td,dd,ff,ddg,ffg,pstn,pmsl,alt,kloud,ceil,lowcld,cover,rad, &
           idp3,store_emv,store_amt,store_hgt,vis,istatus)
!
!  Reads list of stations and variables from a file
!  to force variables known to be chronically wrong to "missing".
!
!  Keith Brewster, CAPS
!  May, 1995
!
  IMPLICIT NONE
!
!  Arguments
!
  CHARACTER (LEN=*) :: blkfile
  INTEGER           :: maxsta,istart,nobs,misval
  REAL              :: rmisval
  CHARACTER (LEN=5) :: stn(maxsta)
  REAL :: t(maxsta),td(maxsta)
  REAL :: dd(maxsta),ff(maxsta),ddg(maxsta),ffg(maxsta)
  REAL :: pstn(maxsta),pmsl(maxsta),alt(maxsta)
  REAL :: store_hgt(maxsta,5)
  REAL :: ceil(maxsta),lowcld(maxsta),cover(maxsta)
  REAL :: vis(maxsta),rad(maxsta)
!
  INTEGER :: kloud(maxsta),idp3(maxsta)
!
  CHARACTER (LEN=8) :: wx(maxsta)
  CHARACTER (LEN=8) :: obstype(maxsta)
  CHARACTER (LEN=1) :: store_emv(maxsta,5)
  CHARACTER (LEN=4) :: store_amt(maxsta,5)
  INTEGER :: istatus,iostatus
!
!  Some internal parameters
!
  INTEGER :: maxblk
  PARAMETER (maxblk=10)
  CHARACTER (LEN=40) :: BLANK
  PARAMETER(BLANK='                                        ')
!
!  Misc internal variables
!
  CHARACTER (LEN=5) :: blkstn
  INTEGER :: blkvar(maxblk)
  INTEGER :: i,iunit,ista,ivar,nblack
  LOGICAL :: found
!
  CALL getunit(iunit)
  OPEN(iunit,FILE=trim(blkfile),STATUS='old')
  DO
    READ(iunit,'(a5,i5)',IOSTAT=iostatus) blkstn,nblack
    IF(iostatus < 0 ) EXIT
    IF(iostatus > 0 ) THEN
      WRITE(6,'(a,a)') ' Error reading blacklist file ',trim(blkfile)
      EXIT
    END IF
    DO i=1,maxblk
      blkvar(i)=-99
    END DO
    READ(iunit,*) (blkvar(i),i=1,nblack)
    found=.false.
    DO i=1,nobs
      ista=i+istart-1
      IF(stn(ista) == blkstn) THEN
        found=.true.
        DO ivar=1,nblack
          SELECT CASE (blkvar(ivar))
          CASE(1)
            WRITE(6,'(a,i4,a,a)') ' Blacklisting Temperature',    &
              blkvar(ivar),' at ',blkstn
            t(ista)=rmisval
          CASE(2)
            WRITE(6,'(a,i4,a,a)') ' Blacklisting Dew Point',      &
              blkvar(ivar),' at ',blkstn
            td(ista)=rmisval
          CASE(3)
            WRITE(6,'(a,i4,a,a)') ' Blacklisting Wind Variables', &
              blkvar(ivar),' at ',blkstn
            dd(ista)=rmisval
            ff(ista)=rmisval
            ddg(ista)=rmisval
            ffg(ista)=rmisval
          CASE(4)
            WRITE(6,'(a,i4,a,a)') ' Blacklisting Press Variables',&
              blkvar(ivar),' at ',blkstn
            pstn(ista)=rmisval
            pmsl(ista)=rmisval
            alt(ista)=rmisval
          CASE(5)
            WRITE(6,'(a,i4,a,a)') ' Blacklisting Cloud variables',&
              blkvar(ivar),' at ',blkstn
            kloud(ista)=0
            ceil(ista)=rmisval
            lowcld(ista)=rmisval
            cover(ista)=rmisval
          CASE(6)
            WRITE(6,'(a,i4,a,a)') ' Blacklisting Visibility',     &
              blkvar(ivar),' at ',blkstn
            vis(ista)=rmisval
          CASE(7)
            WRITE(6,'(a,i4,a,a)') ' Blacklisting Solar Radiation',&
              blkvar(ivar),' at ',blkstn
            rad(ista)=rmisval
          CASE DEFAULT
            WRITE(6,'(a,i4,a,a)') ' Invalid variable number ',    &
              blkvar(ivar),' at ',blkstn
          END SELECT
        END DO
        CYCLE
      END IF
    END DO
    IF(.NOT. found) WRITE(6,'(a,a,a)')                       &
      ' Blacklisted station ',blkstn,' not found in dataset'
  END DO
  CLOSE (iunit)
  CALL retunit(iunit)
  RETURN
END SUBROUTINE blklistsfc

!
!##################################################################
!##################################################################
!######                                                      ######
!######                  SUBROUTINE READ_METAR_OBS           ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE read_metar_obs(infile,blackfile,maxsta,istart,atime,n_stn,   &
           stn,obstype,lat,lon,elev,wx,                                 &
           t,td,dd,ff,ddg,ffg,pstn,pmsl,alt,                            &
           idp3,vis,obstime,istatus)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!    Routine to read METAR in netCDF format.
!
!     Variable        Var type   I/O   Description
!    ----------      ---------- ----- -------------
!     infile            A*256     I    file name including directory
!     maxsta            I         I    Max Number of stations allowed.
!     atime             A*24      O    Data time in dd-mmm-yyyy hh:mm
!     n_stn             I         O    Number observations records/stations
!     lat               RA        O    Station latitude (deg)
!     lon               RA        O    Station longitude (deg)
!     elev              RA        O    Station elevation (m)
!     obstype           A*8 A     O    Observation type (SA, SP, ASOS, etc)
!     obstime           IA        O    Time of observation (hhmm)
!     wx                A*8 A     O    Observed weather
!     obs(1)  - t       RA        O    Temperature (K)
!     obs(2)  - td      RA        O    Dewpoint (K)
!     obs(3)  - dd      RA        O    Wind direction (deg)
!     obs(4)  - ff      RA        O    Wind speed (m/s)
!     obs(5)  - ddg     RA        O    Gust wind direction (deg)
!     obs(6)  - ffg     RA        O    Gust wind speed (m/s)
!     obs(7)  - pstn    RA        O    Station pressure (Pascal)
!     obs(8)  - pmsl    RA        O    MSL pressure (Pascal)
!     obs(9)  - alt     RA        O    Altimeter setting (Pascal)
!     obs(10) -         RA        O    NO USE, Ceiling height (m)
!     obs(11) -         RA        O    NO USE, Height lowest cloud (m)
!     obs(12) -         RA        O    NO USE, Cloud cover (tenths)
!     obs(13) -         RA        O    NO USE, Solar radiation.
!     obs(14) - vis     RA        O    Visibility (miles)
!     idp3              IA        O    3-h coded pressure change (e.g.,608)
!     istatus           I         O    Status flag: 0 = normal
!                                                  -1 = file not found
!                                                  -2 = Arrays too small
!
!    User Notes:
!
!    1.  Arrays should be dimensioned 'maxsta' in the calling program,
!        with maxsta *at least* 120 (for CO domain).
!
!    2.  Pressures are stored as reported, except that altimeters are
!        converted to millibars.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang (04/28/2018)
!  Based on prepsfcobs
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!
  USE model_precision

  IMPLICIT NONE
  CHARACTER (LEN=*),  INTENT(IN)  :: infile
  CHARACTER (LEN=*),  INTENT(IN)  :: blackfile
  INTEGER,            INTENT(IN)  :: maxsta
  INTEGER,            INTENT(IN)  :: istart
  CHARACTER (LEN=24), INTENT(OUT) :: atime
  INTEGER,            INTENT(OUT) :: n_stn

  CHARACTER (LEN=5), INTENT(OUT)  :: stn(maxsta)
  CHARACTER (LEN=8), INTENT(OUT)  :: obstype(maxsta)
  REAL,              INTENT(OUT)  :: lat(maxsta)
  REAL,              INTENT(OUT)  :: lon(maxsta)
  REAL,              INTENT(OUT)  :: elev(maxsta)
  CHARACTER (LEN=8), INTENT(OUT)  :: wx(maxsta)

  REAL,    INTENT(OUT) :: t(maxsta)
  REAL,    INTENT(OUT) :: td(maxsta)
  REAL,    INTENT(OUT) :: dd(maxsta)
  REAL,    INTENT(OUT) :: ff(maxsta)
  REAL,    INTENT(OUT) :: ddg(maxsta)
  REAL,    INTENT(OUT) :: ffg(maxsta)
  REAL,    INTENT(OUT) :: pstn(maxsta)
  REAL,    INTENT(OUT) :: pmsl(maxsta)
  REAL,    INTENT(OUT) :: alt(maxsta)
  INTEGER, INTENT(OUT) :: idp3(maxsta)
  REAL,    INTENT(OUT) :: vis(maxsta)

  INTEGER, INTENT(OUT) :: obstime(maxsta)
  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
!
! Misc Local Variables
!
!-----------------------------------------------------------------------

  INTEGER, PARAMETER :: misval  = 999
  REAL,    PARAMETER :: badflag = -99.9
  INTEGER :: i,k,ii

  INTEGER, PARAMETER :: abstimoffset = 315619200    ! this system starts from 1960 01 01
  INTEGER :: abstsec, iyear, imon, iday, ihr, imin, isec

  INTEGER :: ncid
  CHARACTER(LEN=25), ALLOCATABLE :: obsweather(:)
  CHARACTER(LEN=6),  ALLOCATABLE :: obsreport(:)
  REAL(DP),          ALLOCATABLE :: timein(:)
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  CALL netopen(infile,'R',ncid)
  IF (ncid < 0) THEN
    istatus = -1
    RETURN        ! Open error, do nothing
  END IF

!.....  Read the surface header
!
  CALL netreaddim(ncid, 'recNum', n_stn, istatus)

  IF((n_stn+istart-1) > maxsta) THEN
    WRITE(6,'(a,i9,/a,i9,a,/2a)')                                      &
           ' +++ ERROR in READ_METAR_OBS: maxsta =',maxsta,            &
           '     but there will be',(n_stn+istart-1),' stations ',     &
           '     after reading file',TRIM(infile)
    WRITE(6,'(/a/)') '   Increase the value of "maxsta" and try again.'
    istatus = -2
    RETURN
  END IF

  ALLOCATE(obsweather(n_stn), STAT = istatus)
  ALLOCATE(obsreport(n_stn),  STAT = istatus)
  ALLOCATE(timein(n_stn),     STAT = istatus)
!
!.....  Now read the station data.
!

  CALL netreadattstr(ncid,'endTime',atime,istatus)

  CALL netread2dc(ncid,0,0,'stationName',n_stn,5,stn(istart))

  CALL netread2dc(ncid,0,0,'presWeather',n_stn,25,obsweather)
  CALL netread2dc(ncid,0,0,'reportType', n_stn,6, obsreport)
  CALL netread1dd(ncid,0,0,'timeObs',    n_stn,   timein)
  DO i = 0, n_stn-1
    stn(i+istart)(5:5) = ' '
    !obstype(i+istart) = obsreport(i+1)    ! 'METAR' or 'SPECI'
    obstype(i+istart) = 'SA      '
    wx(i+istart) = obsweather(i+1)(1:8)

    !write(0,*)i, istart, timein(i+1)
    abstsec = INT(timein(i+1)) + abstimoffset
    CALL abss2ctim(abstsec, iyear, imon, iday, ihr, imin, isec)
    obstime(i+istart) =  ihr*100+imin
  END DO

  CALL netread1d(ncid,0,0,'latitude', n_stn,lat (istart))
  CALL netread1d(ncid,0,0,'longitude',n_stn,lon (istart))
  CALL netread1d(ncid,0,0,'elevation',n_stn,elev(istart))

  CALL netread1d(ncid,0,0,'temperature',n_stn,t(istart))     ! K
  CALL netread1d(ncid,0,0,'dewpoint',   n_stn,td(istart))
  CALL netread1d(ncid,0,0,'windDir',    n_stn,dd(istart))
  CALL netread1d(ncid,0,0,'windSpeed',  n_stn,ff(istart))    ! m/s
  !CALL netread1d(ncid,0,0,'elevation',n_stn,ddg(istart))
  ddg(istart:istart+n_stn-1) = badflag
  !CALL netread1d(ncid,0,0,'windGust',   n_stn,ffg(istart))   ! m/s
  !CALL netread1d(ncid,0,0,'elevation',n_stn,pstn(istart))
  pstn(istart:istart+n_stn-1) = badflag  ! misval
  CALL netread1d(ncid,0,0,'seaLevelPress',n_stn,pmsl(istart))  ! Pascal
  CALL netread1d(ncid,0,0,'altimeter',    n_stn,alt(istart))   ! Pascal

  CALL netread1d(ncid,0,0,'visibility',      n_stn,vis(istart))  ! meter
  CALL netread1d(ncid,0,0,'pressChange3Hour',n_stn,idp3(istart)) ! Pascal

  DEALLOCATE(obsweather)
  DEALLOCATE(obsreport)
  DEALLOCATE(timein)

  CALL netclose(ncid)

  DO i = istart, istart+n_stn-1
    IF (t(   i) > 1.0E+30 ) t   (i) = badflag     ! K
    IF (td(  i) > 1.0E+30 ) td  (i) = badflag
    IF (dd(  i) > 1.0E+30 ) dd  (i) = badflag
    IF (ff(  i) > 1.0E+30 ) ff  (i) = badflag    ! m/s
    IF (pmsl(i) > 1.0E+30 ) pmsl(i) = badflag    ! Pascal
    IF (alt( i) > 1.0E+30 ) alt (i) = badflag    ! Pascal
    IF (vis( i) > 1.0E+30 ) vis (i) = badflag    ! meter
    IF (idp3(i) > 1.0E+30 ) idp3(i) = badflag    ! Pascal
  END DO
  !WHERE(t(   istart:istart+n_stn-1) > 1.0E+30 ) t    = badflag     ! K
  !WHERE(td(  istart:istart+n_stn-1) > 1.0E+30 ) td   = badflag
  !WHERE(dd(  istart:istart+n_stn-1) > 1.0E+30 ) dd   = badflag
  !WHERE(ff(  istart:istart+n_stn-1) > 1.0E+30 ) ff   = badflag    ! m/s
  !WHERE(pmsl(istart:istart+n_stn-1) > 1.0E+30 ) pmsl = badflag    ! Pascal
  !WHERE(alt( istart:istart+n_stn-1) > 1.0E+30 ) alt  = badflag    ! Pascal
  !WHERE(vis( istart:istart+n_stn-1) > 1.0E+30 ) vis  = badflag    ! meter
  !WHERE(idp3(istart:istart+n_stn-1) > 1.0E+30 ) idp3 = badflag    ! Pascal

!-----------------------------------------------------------------------
!
! Black list know bad observations
!
!  Maybe no necessary for METAR?
!
!-----------------------------------------------------------------------
!
  !CALL blklistsfc(blackfile,maxsta,istart,n_stn,misval,badflag,        &
  !    stn,obstype,wx,                                                  &
  !    t,td,dd,ff,ddg,ffg,pstn,pmsl,alt,kloud,ceil,lowcld,cover,rad,    &
  !    idp3,store_emv,store_amt,store_hgt,vis,istatus)
!
!-----------------------------------------------------------------------
!
!..... End of data gathering. Let's go home...
!
!-----------------------------------------------------------------------

  PRINT *, ' Normal completion of READ_METAR_OBS'
!

  RETURN
END SUBROUTINE read_metar_obs

!
!##################################################################
!##################################################################
!######                                                      ######
!######                  SUBROUTINE READ_METAR_OBS           ######
!######                                                      ######
!######                     Developed by                     ######
!######       National Severe Storm Laboratory, NOAA         ######
!######                                                      ######
!##################################################################
!##################################################################
!
SUBROUTINE read_prepbufr_obs(infile,blackfile,maxsta,istart,rmiss,      &
           atime,n_stn,stn,obstype,lat,lon,elev,wx,                     &
           t,q,u,v,pstn,alt,obstime,istatus)
!
!-----------------------------------------------------------------------
!
!  PURPOSE:
!
!    Routine to read METAR in netCDF format.
!
!     Variable        Var type   I/O   Description
!    ----------      ---------- ----- -------------
!     infile            A*256     I    file name including directory
!     maxsta            I         I    Max Number of stations allowed.
!
!     atime             A*24      O    Data time in dd-mmm-yyyy hh:mm
!     n_stn             I         O    Number observations records/stations
!     lat               RA        O    Station latitude (deg)
!     lon               RA        O    Station longitude (deg)
!     elev              RA        O    Station elevation (m)
!     obstype           A*8 A     O    Observation type (SA, SP, ASOS, etc)
!     obstime           IA        O    Time of observation (hhmm)
!     wx                A*8 A     O    Observed weather
!     obs(1)  - t       RA        O    Temperature (K)
!     obs(2)  - Q       RA        O    Specific humidity (kg/kg)
!     obs(5)  - u       RA        O    Gust wind direction (deg)
!     obs(6)  - v       RA        O    Gust wind speed (m/s)
!     obs(7)  - pstn    RA        O    Station pressure (Pascal)
!     obs(9)  - alt     RA        O    Altimeter setting (Pascal)
!     istatus           I         O    Status flag: 0 = normal
!                                                  -1 = file not found
!                                                  -2 = Arrays too small
!
!    User Notes:
!
!    1.  Arrays should be dimensioned 'maxsta' in the calling program,
!        with maxsta *at least* 120 (for CO domain).
!
!    2.  Pressures are stored as reported, except that altimeters are
!        converted to millibars.
!
!-----------------------------------------------------------------------
!
!  AUTHOR: Yunheng Wang (02/20/2019)
!  Based on prepsfcobs
!
!  MODIFICATION HISTORY:
!
!-----------------------------------------------------------------------
!
!
  USE model_precision
  USE obs_prepbufr

  IMPLICIT NONE
  CHARACTER (LEN=*),  INTENT(IN)  :: infile
  CHARACTER (LEN=*),  INTENT(IN)  :: blackfile
  INTEGER,            INTENT(IN)  :: maxsta
  INTEGER,            INTENT(IN)  :: istart
  REAL,               INTENT(IN)  :: rmiss
  CHARACTER (LEN=24), INTENT(OUT) :: atime
  INTEGER,            INTENT(OUT) :: n_stn

  CHARACTER (LEN=5), INTENT(OUT)  :: stn(maxsta)
  CHARACTER (LEN=8), INTENT(OUT)  :: obstype(maxsta)
  REAL,              INTENT(OUT)  :: lat(maxsta)
  REAL,              INTENT(OUT)  :: lon(maxsta)
  REAL,              INTENT(OUT)  :: elev(maxsta)
  CHARACTER (LEN=8), INTENT(OUT)  :: wx(maxsta)

  REAL,    INTENT(OUT) :: t(maxsta)
  REAL,    INTENT(OUT) :: q(maxsta)
  REAL,    INTENT(OUT) :: u(maxsta)
  REAL,    INTENT(OUT) :: v(maxsta)
  REAL,    INTENT(OUT) :: pstn(maxsta)
  REAL,    INTENT(OUT) :: alt(maxsta)

  INTEGER, INTENT(OUT) :: obstime(maxsta)
  INTEGER, INTENT(OUT) :: istatus

!-----------------------------------------------------------------------
!
! Misc Local Variables
!
!-----------------------------------------------------------------------

  CHARACTER(8)  :: sfcsets(1) = (/'ADPSFC' /)
  CHARACTER(8)  :: dflsrcs(1) = (/'SA'    /)

  CHARACTER(8) :: subset
  CHARACTER(8) :: sid
  INTEGER      :: unit_in
  INTEGER      :: idate,iret,i

  INTEGER :: abstsec, iyear, imon, iday, ihr, imin, isec
  INTEGER :: mabstsec, myear, mmon, mday, mhr, mmin, msec

  INTEGER :: iset,num_subset, typ, ihmtime
  REAL    :: qmark_stn, pdhr, mindhr,minrpt

  LOGICAL :: fexist
!
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  istatus = 0

  INQUIRE(FILE=TRIM(infile),EXIST=fexist)
  IF (.NOT. fexist) THEN
    istatus = -1
    RETURN
  END IF

  CALL getunit(unit_in)
  OPEN(unit_in,FILE=TRIM(infile),ACTION='READ',FORM='UNFORMATTED')
  CALL openbf(unit_in,'IN',unit_in)
  !CALL dxdump(unit_in,unit_table)
  CALL datelen(10)     ! YYYYMMDDHH

  !.....  Read the prepbufr messages
  n_stn = 0
  num_subset = 0

  CALL READPB(unit_in,.TRUE.,sfcsets,1,subset,idate,iret )

  minrpt = 0.0
  mindhr = 1.0
  DO WHILE (iret >= 0)

    iset = in_subsets(subset,sfcsets,1)
    qmark_stn = MAX(evns(2,1,1,KTOB), evns(2,1,1,KQOB), evns(2,1,1,KUOB),   &
                    evns(2,1,1,KVOB), evns(2,1,1,KPOB))
    IF (nlev == 1 .AND. qmark_stn <= 2) THEN   ! read SFC
      num_subset = num_subset + 1

      myear = idate/1000000
      mmon  = MOD(idate,1000000)/10000
      mday  = MOD(idate,10000)/100
      mhr   = MOD(idate,100)
      CALL ctim2abss(myear,mmon,mday,0,0,0,mabstsec)    ! no mhr, only date

      WRITE(sid,'(a5)') hdr(1)

      IF ( n_stn > 0 ) THEN
        IF ( sid == stn(n_stn+istart) ) THEN
          IF ( hdr(5) > pdhr ) THEN
            n_stn = n_stn
          ELSE
            GOTO 800
          END IF
        ELSE
          n_stn = n_stn+1
        END IF
      ELSE
        n_stn = n_stn+1
      END IF

      pdhr  = hdr(5)
      IF (ABS(hdr(5)) < mindhr) THEN  ! Find cycle time hour
        mindhr = ABS(hdr(5))
        minrpt = hdr(6)
      END IF
      stn(n_stn+istart)      = sid
      wx(n_stn+istart)       = subset
      obstype(n_stn+istart)  = dflsrcs(iset)
      abstsec = mabstsec + hdr(6)*3600  !RPT
      CALL abss2ctim(abstsec, iyear, imon, iday, ihr, imin, isec)
      ihmtime = ihr*100+imin
      obstime(n_stn+istart)  = ihmtime
      lat(n_stn+istart)  = hdr(2)
      lon(n_stn+istart)  = hdr(3)
      elev(n_stn+istart) = hdr(4)
      typ = nint(hdr(8))

      t(n_stn+istart)     = rmiss
      q(n_stn+istart)     = rmiss
      u(n_stn+istart)     = rmiss
      v(n_stn+istart)     = rmiss
      pstn(n_stn+istart)  = rmiss
      alt(n_stn+istart)   = rmiss
      IF ( KTOB >= indxvr1(typ) .AND. KTOB <=indxvr2(typ) ) THEN
        IF (evns(1,1,1,KTOB) < R8BFMS) t(n_stn+istart) = evns(1,1,1,KTOB) + 273.15
      END IF
      IF ( KQOB >= indxvr1(typ) .AND. KQOB <=indxvr2(typ) ) THEN
        IF (evns(1,1,1,KQOB) < R8BFMS) q(n_stn+istart) = evns(1,1,1,KQOB) * 1.0E-6
      END IF
      IF ( KUOB >= indxvr1(typ) .AND. KUOB <=indxvr2(typ) ) THEN
        IF (evns(1,1,1,KUOB) < R8BFMS) u(n_stn+istart) = evns(1,1,1,KUOB)
      END IF
      IF ( KVOB >= indxvr1(typ) .AND. KVOB <=indxvr2(typ) ) THEN
        IF (evns(1,1,1,KVOB) < R8BFMS) v(n_stn+istart) = evns(1,1,1,KVOB)
      END IF
      IF ( KPOB >= indxvr1(typ) .AND. KPOB <=indxvr2(typ) ) THEN
        IF (evns(1,1,1,KPOB) < R8BFMS) pstn(n_stn+istart) = evns(1,1,1,KPOB)*100.
      END IF

    END IF

    800 CONTINUE

    IF (iret /= 0) EXIT
    CALL READPB(unit_in,.TRUE.,sfcsets,1,subset,idate,iret )
  END DO

  CALL closbf(unit_in)
  CALL retunit(unit_in)

  abstsec = mabstsec + minrpt*3600  !RPT
  CALL abss2ctim(abstsec, myear, mmon, mday, mhr, mmin, msec)
  WRITE(atime,'(I2.2,a,I3.3,a,I4.4,a,I2.2,a,I2.2)') mday,'-',mmon,'-',myear,' ',mhr,':',mmin
  !WRITE(*,'(1x,I0,2x,3a)') num_subset, subset,', atime = ', atime

!
!-----------------------------------------------------------------------
!
!..... End of data gathering. Let's go home...
!
!-----------------------------------------------------------------------

  !WRITE(*, '(1x,a)') 'Normal completion of READ_PREPBUFR_OBS'

  RETURN
END SUBROUTINE read_prepbufr_obs

