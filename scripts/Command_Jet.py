#!/usr/bin/env python
## ---------------------------------------------------------------------
##
## This is a python library to submit program to PBS
##
## ---------------------------------------------------------------------
##
## HISTORY:
##   Yunheng Wang (06/11/2012)
##   Initial version.
##
##   04/22/2018
##   Modified for Jet.
##
########################################################################
##
## Requirements:
##
########################################################################

import os, sys, time, re
import subprocess

from configBase import *

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class configurator (baseConfigurator) :
  #classbegin
  '''
  This is a specific computer-dependent configurations for Jet
  uses the Torque/PBS batch system.
  '''

  ##----------------------- Initialization      -----------------------
  def __init__(self,wrkdir,norun,debug) :
    baseConfigurator.__init__(self,wrkdir,norun,debug)
    self.defaultQueue = 'batch'
    self.hostname = 'Jet'
    self.schjobext = 'slurm'
    self.needcopy  = False
  #enddef

  ##----------------------- Ending of the module -----------------------
  def finalize(self) :
    baseConfigurator.finalize(self)
  #enddef

  ######################################################################

  def run_a_program(self,executable,nmlfile,outfile,wrkdir,jobconf,inarg=None):
    '''
      Generate job script for a task and submit it

      nmlfile : namelist file can be None
      outfile : must provide a output file name
      wrkdir  : the program can be run in a directory different from the
                instance attribute of wrkdir
      NOTE: For array jobs, it is a general directory without ensemble number appended
    '''

    if self.cmdmutex.acquire():
      self.serieno += 1
      myserieno = self.serieno
      self.cmdmutex.release()

    #cwdsaved = os.getcwd()
    #os.chdir(wrkdir)

    cmdstr = executable
    if nmlfile :
      if inarg is None:
        cmdstr += " %s" % nmlfile
      elif inarg == 'STDIN':
        cmdstr += " < %s" % nmlfile
      else:
        cmdstr += " %s %s" % (inarg,nmlfile)

    if outfile :
      (jobname,ext) = os.path.splitext(outfile)
      jobname = jobname
      cmdstr += " > %s" % outfile
    else :
      jobname = os.path.basename(executable)

    if jobconf.mpi :

      claimncpn = jobconf.claimncpn
      #nprocs = jobconf.nproc_x*jobconf.nproc_y
      nprocs = jobconf.ntotal
      if jobconf.shell:
        cmdstr = 'mpiexec -n %d %s' % (nprocs, cmdstr)
      else:
        cmdstr = 'srun -n %d %s' % (nprocs, cmdstr)

      (nodes,extra) = divmod(nprocs,claimncpn)
      if extra > 0 :
        nodes += 1

    else :
      nprocs = 1
      claimncpn = 1

      #cmdstr = 'mpiexec -n %d %s' % (nprocs, cmdstr)
      nodes  = 1

    if jobconf.shell:

      if jobconf.numens is None:
        self.addlog(0,self.hostname, 'In <%s>, executing:\n    $> %s\n' % (wrkdir,cmdstr))
        job = JobID(jobname,wrkdir,self,myserieno)
        job['status'] = subprocess.call(cmdstr,shell=True,cwd=wrkdir)

        if job['status'] == 0: subprocess.call('touch done.%s.%d' %(jobname,myserieno),shell=True,cwd=wrkdir)
        else:                  subprocess.call('touch error.%s.%d'%(jobname,myserieno),shell=True,cwd=wrkdir)
      else:
        for noens in range(0,jobconf.numens+1):
          jobdir = "%s_%d" % (wrkdir, noens)
          self.addlog(0,self.hostname, 'In <%s>, executing:\n    $> %s\n' % (jobdir,cmdstr))
          job = JobID(jobname,jobdir,self,myserieno,size=jobconf.numens)
          job['status'] = subprocess.call(cmdstr,shell=True,cwd=jobdir)

          if job['status'] == 0: subprocess.call('touch done.%s.%d_%d' %(jobname,myserieno,noens),shell=True,cwd=jobdir)
          else:                  subprocess.call('touch error.%s.%d_%d'%(jobname,myserieno,noens),shell=True,cwd=jobdir)

    else:

      if jobconf.numens is None:  # single job
        scptdir   = wrkdir
        jobwrkdir = wrkdir
        append    = "${SLURM_JOBID}"
        logpre    = "%s/%s_%%j"%(wrkdir,jobname)
      else: # job array
        scptdir   = "%s_0" % wrkdir
        append    = "${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}"
        jobwrkdir = "%s_${SLURM_ARRAY_TASK_ID}"%wrkdir
        logpre    = "%s_%%a/%s_%%a_%%j"%(wrkdir,jobname)

      (hour,minute) = divmod(jobconf.claimmin,60)
      timestr = '%02d:%02d:00' % (hour,minute)

      scriptstr = '''#!/bin/sh -l
          #SBATCH -A hpc-wof1
          #SBATCH --partition=vjet,xjet,sjet
          #SBATCH -J %(jobname)s
          #SBATCH --ntasks=%(nprocs)d
          #SBATCH -t %(claimtime)s
          #SBATCH --exclusive
          #SBATCH -o %(logpre)s.out
          #SBATCH -e %(logpre)s.err

          time1=$(date '+%%s')
          echo "Job Started: $(date). Job Id:   $SLURM_JOBID"
          echo " "

          cd %(wrkdir)s

          rm -rf %(outfile)s

          set echo on

          %(cmdstr)s

          if [[ $? -eq 0 ]]; then
            touch done.%(jobname)s.%(append)s
          else
            touch error.%(jobname)s.%(append)s
          fi

          set echo off

          time2=$(date '+%%s')

          let diff=time2-time1
          let hour=diff/3600
          let diff=diff%%3600
          let min=diff/60
          let sec=diff%%60

          echo -n "Job   Ended: $(date). "
          printf 'Job run time:  %%02d:%%02d:%%02d' $hour $min $sec
          echo " "

          ''' %{ 'wrkdir' : jobwrkdir, 'logpre' : logpre, 'jobname' : jobname,
                 'cmdstr' : cmdstr,    'outfile' : outfile,
                 'claimtime'  : timestr, 'nprocs' : nprocs,
                 #'nodes' : nodes, 'claimncpn' : claimncpn,
                 'append' : append
                }

      scriptfile = '%s.%s' % (jobname,self.schjobext)
      fullscript = os.path.join(scptdir,scriptfile)

      batchFile = open(fullscript,'w')
      batchFile.write(self.trim(scriptstr))
      batchFile.close()

      self.addlog(0,self.hostname,'''- %02d - Jobscript "%s" generated.''' %(
                        myserieno,scriptfile )  )

      if self.showonly:
        self.addlog(0,self.hostname, 'Preparing job script "%s" in %s.' % (scriptfile,wrkdir))
        job = JobID(jobname,wrkdir,self)
        job['status'] = 0
      else :
        job = self.submit_a_job(jobname,scptdir,numarray=jobconf.numens,maxtry=jobconf.numtry)
        job.update_status()

    #os.chdir(cwdsaved)

    return job
  #enddef run_a_program

  ######################################################################

  def run_ncl_plt(self,executable,nmlfile,field,wrkdir,fminu,tmpldir,
                  jobconf,transfer=False):
    '''
      Plot using NCL

      nmlfile : namelist file can be None
      outfile : must provide a output file name
      wrkdir  : the program can be run in a directory different from the
                instance attribute of wrkdir
    '''

    if self.cmdmutex.acquire():
      self.serieno += 1
      myserieno = self.serieno
      self.cmdmutex.release()

    #cwdsaved = os.getcwd()
    #os.chdir(wrkdir)

    cmdstr = executable
    if nmlfile :
      cmdstr += " %s" % nmlfile

    (jobname,ext) = os.path.splitext(nmlfile)
    jobname = os.path.basename(jobname)

    if jobconf.mpi :

      claimncpn = jobconf.claimncpn
      nprocs = jobconf.nproc_x*jobconf.nproc_y

      (nodes,extra) = divmod(nprocs,claimncpn)
      if extra > 0 :
        nodes += 1

    else :
      nprocs = 1
      claimncpn = 1
      nodes  = 1

    #cmdstr = 'srun -n %d %s' % (nprocs, cmdstr)

    if jobconf.shell:

      self.addlog(0,self.hostname, 'In <%s>, executing:\n    $> %s\n' % (wrkdir,cmdstr))
      job = JobID(jobname,wrkdir,self,myserieno)
      job['status'] = subprocess.call(cmdstr,shell=True,cwd=wrkdir)

      if job['status'] == 0: subprocess.call('touch done.%s.%d' %(jobname,myserieno),shell=True,cwd=wrkdir)
      else:                  subprocess.call('touch error.%s.%d'%(jobname,myserieno),shell=True,cwd=wrkdir)

    else:

      if jobconf.numens is None:  # single job
        scptdir   = wrkdir
        jobwrkdir = wrkdir
        append    = "${SLURM_JOBID}"
        logpre    = "%s/%s_%%j"%(wrkdir,jobname)
      else: # job array
        scptdir   = "%s_1" % wrkdir
        append    = "${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}"
        jobwrkdir = "%s_${SLURM_ARRAY_TASK_ID}"%wrkdir
        logpre    = "%s_%%a/%s_%%a_%%j"%(wrkdir,jobname)

      (hour,minute) = divmod(jobconf.claimmin,60)
      timestr = '%02d:%02d:00' % (hour,minute)

      scriptstr = '''#!/bin/sh -l
          #SBATCH -A hpc-wof1
          #SBATCH --partition=vjet,xjet,sjet
          #SBATCH -J %(jobname)s
          #SBATCH --ntasks=%(nprocs)d
          #SBATCH --exclusive
          #SBATCH -t %(claimtime)s
          #SBATCH -o %(logpre)s.out
          #SBATCH -e %(logpre)s.err

          time1=$(date '+%%s')
          echo "Job Started: $(date). Job Id:  $SLURM_JOBID"
          echo " "

          cd %(wrkdir)s

          module load ncl

          set echo on

          %(cmdstr)s

          if [[ $? -eq 0 ]]; then
            touch done.%(jobname)s.%(append)s
          else
            touch error.%(jobname)s.%(append)s
          fi

          set echo off

          time2=$(date '+%%s')

          let diff=time2-time1
          let hour=diff/3600
          let diff=diff%%3600
          let min=diff/60
          let sec=diff%%60

          echo -n "Job   Ended: $(date). "
          printf 'Job run time:  %%02d:%%02d:%%02d' $hour $min $sec
          echo " "
          ''' %{ 'wrkdir' : jobwrkdir, 'jobname'     : jobname,
                 'cmdstr' : cmdstr,    'claimtime'   : timestr,
                 'nprocs' : nprocs,    'logpre'      : logpre,
                 #'nodes'  : nodes,     'claimncpn'   : claimncpn,
                 'append' : append
                }

      scriptfile = '%s.%s' % (jobname,self.schjobext)
      scriptfull = os.path.join(scptdir,scriptfile)

      batchFile = open(scriptfull,'w')
      batchFile.write(self.trim(scriptstr))
      batchFile.close()

      self.addlog(0,self.hostname,'''- %02d -  Jobscript "%s" generated.''' %(
                        myserieno,scriptfile )  )

      if self.showonly :
        self.addlog(0,"jet",'Preparing job script "%s" in %s.' % (scriptfile,wrkdir))
        job = JobID(jobname,wrkdir,self)
        job['status'] = 0

      else :
        job = self.submit_a_job(jobname,scptdir,numarray=jobconf.numens)
        job.update_status()

    #os.chdir(cwdsaved)

    return job
  #enddef run_ncl_plt

  ##====================================================================

  def submit_a_job(self,jobname,wrkdir,numarray,maxtry=1) :
    '''
    submit a job to PBS on Jet
    '''

    jobfile = os.path.join(wrkdir,'%s.%s'%(jobname,self.schjobext))

    #if djobs :
    #  bjoblst = ['sbatch', '-d', 'afterok:%s' % ':'.join(djobs)]
    #else :
    bjoblst = ['sbatch']

    if numarray is not None:
      bjoblst.extend(['-a','0-%d'%numarray])

    bjoblst.append(jobfile)

    ##retout = subprocess.check_output(bjobstr,stdin=None,stderr=subprocess.STDOUT,cwd=wrkdir)
    retout = subprocess.Popen(bjoblst, stdout=subprocess.PIPE,cwd=wrkdir).communicate()[0]
    retout = retout.decode(encoding='utf-8', errors='strict')
    #print retout

    ## get jobid here
    #Submitted batch job 10800507
    jobidre = re.compile(r'Submitted batch job (\d+)')
    jobidmatch = jobidre.match(retout)
    if jobidmatch :
      jobid = jobidmatch.group(1)

      job = JobID(jobname,wrkdir,self,jobid,maxtry,numarray)

      self.addlog(  0,"jet",'Submitted %s.' % job  )
      self.addlog(999,"jet",'    $ %s' % ( ' '.join(bjoblst) )  )
      self.addlog(999,"jet",'    > %s' % ( retout )  )

      return job

    else :
      print('Something is wrong with job (%s) submitting? Get: "%s".'%(jobname,retout),file=sys.stderr)
      self.addlog(1,self.hostname,'  Job <%s> is not submitted correctly.' % (jobname,)  )
      self.addlog(-1,self.hostname,'    $ %s\n    > "%s"' % ( ' '.join(bjoblst), retout )  )
      #return False
      return None

  #enddef submit_a_job

  ##====================================================================

  def get_job_status(self,jobid) :
    '''
    Check job status
    '''

    if jobid is None :
      return 0

    retout = subprocess.Popen(['qstat', jobid], stdout=subprocess.PIPE).communicate()[0]
    #print retout

    retlist = retout.splitlines()
    i=0
    for retl in retlist:
      if re.match('[- ]+',retl):
        break
      i = i + 1

    if i > 0 and i < len(retlist):
      jobstats = {}
      statpos = [k+1 for k, ltr in enumerate(retlist[i]) if ltr.isspace()]
      j = 0
      for k in statpos:
        #print i,j,k
        head = (retlist[i-1][j:k]).strip()
        value = (retlist[i+1][j:k]).strip()
        jobstats[(retlist[i-1][j:k]).strip()] = (retlist[i+1][j:k]).strip()
        j = k

      #for key in jobstats.keys():
      #  print key,'->',jobstats[key]

      #-  the job state:
      #     C -     Job is completed after having run/
      #     E -  Job is exiting after having run.
      #     H -  Job is held.
      #     Q -  job is queued, eligible to run or routed.
      #     R -  job is running.
      #     T -  job is being moved to new location.
      #     W -  job is waiting for its execution time
      #          (-a option) to be reached.
      #     S -  (Unicos only) job is suspend.

      jobstatus = { 'C' : 0, 'Q': 1, 'R': 2, 'E' : 6, 'H' : 6, 'T': 6, 'W': 6, 'S': 6 }

      #self.addlog(999,"jet","jobstats (%s): %s -> %d." % (jobid,jobstats['S'],jobstatus[jobstats['S']]))
      return jobstatus[jobstats['S']]

      #status = jobstats['S']
      #if status == "C" :
      #  jobstatus = 0
      #elif status == "Q" :
      #  jobstatus = 1
      #elif status == "R" :
      #  jobstatus = 2
      #else :
      #  jobstatus = 6
      #
      #print "jobstats = ", jobstatus
      #return jobstatus

    else:
      #print >> sys.stderr, errmsg
      errmsg = 'Job id <%s> gets wrong status "%s".' % (jobid,retout)
      self.addlog(-1,"jet", '\n  *** %s ***\n' % errmsg  )
      return False

    ### get job status here
    #retlist = retout.split('\n')
    #jobstatre = re.compile(r'%s.jetbqs3 +[^ ]+ +%s +[\d:]+ (\w) \w+'%(jobid,os.getenv('USER')))
    ##print "%s - %s" % (jobstatre.pattern, jobid)
    #jobstatmatch = jobstatre.match(retlist[2])
    ##print retlist[2]
    #if jobstatmatch :
    #  status = jobstatmatch.group(1)
    #  if status == "C" :
    #    jobstatus = 0
    #  elif status == "Q" :
    #    jobstatus = 1
    #  elif status == "R" :
    #    jobstatus = 2
    #  else :
    #    jobstatus = 6
    #
    #  return jobstatus
    #else :
    #  errmsg = 'Job id %s is not found.' % jobid
    #  #print >> sys.stderr, errmsg
    #  self.addlog(-1,"jet", '\n  *** %s ***\n' % errmsg  )
    #  return False

  #enddef get_job_status

  ######################################################################

  @staticmethod
  def fetchDefaultWorkingDir() :

      return '/lfs3/projects/hpc-wof1/afierro/EN3DVAR_TEST'
      

  #enddef fetchDefaultWorkingDir

  ######################################################################

  @staticmethod
  def fetchNCARGRoot() :

      import os

      ncargpath = os.environ.get('NCARG_ROOT')

      if ncargpath is None:
        #ncargpath = '/apps/ncl/6.1.2/intel.bin/ncl'     # Jet
        ncargpath = '/apps/ncl/CentOS7.5_64bit_gnu485'

      return ncargpath

  #enddef fetchDefaultWorkingDir

#endclass configurator

if __name__ == "__main__":
  #
  # Test get job status
  #
  statusmap = { 0: 'COMPLETED', 1: 'PENDING', 2: 'RUNNING', 9: 'UNKNOWN'}

  cmd=configurator("./",False,False)
  a = cmd.get_job_status(sys.argv[1])
  print ("%d -> %s"%(a,statusmap[a]))
