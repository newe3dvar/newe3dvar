#!/usr/bin/env python3
##
##----------------------------------------------------------------------
##
## This file defines a forecast/analysis workflow and running steps for
## each program in the workflow.
##
## ---------------------------------------------------------------------
##
## HISTORY:
##   Yunheng Wang (12/11/2015)
##   Initial version based on early works.
##
##
########################################################################
##
## Requirements:
##
##   o Python 3.6 or above
##
########################################################################

import sys, os, re, glob
import shutil,gzip
#import bz2

import subprocess, threading
from datetime import datetime, timedelta
import time, math

import namelist, prep_okmeso

from configBase import ExtMData

class NSSLCase(threading.Thread):
    #classbegin
    '''
       This is to initialize a forecast/analysis workflow over one domain at
       a specific time on a specific date. It contains

       __init__  : initialize its instance
       run       : to run this case as a separate thread
       run_xxx   : running step for each program containing in this workflow
    '''

    ####################################################################
    ##
    ## Case intialization
    ##
    ####################################################################

    def __init__(self, caseconf, domainin, cmdconfig, bucket, programin=None):
        '''
           Initialize NSSL 3dvar case
        '''

        self.runcase = caseconf
        self.command = cmdconfig
        self.domain  = domainin

        ## directory settings
        self.runDirs = self.runcase.rundirs

        ##
        ##---------------  Run-time arguments for programs --------------##
        ##
        self.runConfig = self.runcase.getRuntimeConfig()

        programtorun = self.runcase.programs
        if programin is None:
            self.programs = programtorun
        elif programin in  programtorun:
            i = programtorun.index(programin)
            self.programs = programtorun[i:]
        else:
            self.programs = [programin]

        ##---------------  Observations ---------------------------------##
        ##
        ## Radar names to be analyzed
        self.radars = self.runcase.radars    # initially comes from configuration
                                             # if empty, will be determined based on domain
        ##---------------- looking for radar within domain --------------
        if len(self.radars) < 1:
             self.radars = [radar.name for radar in self.domain['radars']]

        self.procradars = 0

        ## Observations to be analyzed
        self.obstypes = self.runcase.getCaseObsType()

        ##
        ##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        ##

        self.mutex = threading.Lock()
        self.kill_received = False
        self.bucket = bucket

        threading.Thread.__init__(self)

        self.name = 'case-%s_%02d' % (caseconf.caseNo, self.domain.id)
    #enddef

    ####################################################################
    ##
    ## Work flow
    ##
    ####################################################################

    def run(self):

        try:
            self.caseDir = self.create_dirs()

            ## do analysis only
            if self.runcase.caseNo == 1:      # Analysis starting with external dataset

              extdat = self.check_link_extm(self.runcase.extsrcs)
              if extdat is None :
                self.command.addlog(-1,"cntl.run","ERROR: did not find external data source.")

              if self.domain.isroot and self.check_job('ungrib') :
                 self.run_ungrib(extdat,False)

              if self.check_job('geogrid'): self.run_geogrid(extdat,True)

              if self.check_job('metgrid'): self.run_metgrid(extdat,True)

              if self.check_job('tinterp'): self.run_tinterp(extdat,True)

              (wrfstat,bkgfiles) = self.run_real(extdat,wait=True,outmet='tinterp')

              if self.check_job('wrf'):
                  (dumstat,dumbkg,wrffhr) = self.run_wrf(extdat,False)
                  # (wrfstat,bkgfile,wrffhr) - will use for bakground

              validobs = self.check_wait_obs(self.obstypes)  ## prepare observation data

              if self.check_job('radremap') and len(validobs['radar']) > 0:
                self.run_radar_remaps(validobs['radar'],bkgfiles[0],wrfstat)

              self.command.addlog(999,"domains",self.domain)
              ##
              ## run news3dvar
              ##
              anafiles = self.run_news3dvar(validobs,bkgfiles,wrfstat,True)

              ##
              ## Post-processing
              ##
              if self.check_job('nclplt') or self.check_job('unipost'):
                  self.run_post(afiles=anafiles)

            ## WRF forecast after data analysis
            elif self.runcase.caseNo == 2:    # Forecast starting from an analysis

                extdat = self.decode_ungribrun()

                if self.check_job('metgrid'): self.run_metgrid(extdat,True,condition=None)

                if self.check_job('real') : self.run_real(extdat,wait=True,outmet='metgrid1')

                if self.check_job('wrfdaupdate') : self.run_wrfdaupdate(False,wait=True)

                (wrfstat,bkgfiles,wrffhr) = self.run_wrf(extdat,False)

                if self.check_job('nclplt') or self.check_job('unipost') or self.check_job('nclplt2d'):
                    self.run_post(afiles=[os.path.dirname(bkgfile) for bkgfile in bkgfiles])

            ## do analysis and forecast
            elif self.runcase.caseNo == 3:    # Analysis and Forecast based on external dataset

              extdat = self.check_link_extm(self.runcase.extsrcs)
              if extdat is None :
                self.command.addlog(-1,"cntl.run","ERROR: did not find external data source.")

              if self.domain.isroot and self.check_job('ungrib') :
                 self.run_ungrib(extdat,False)

              if self.check_job('geogrid'): self.run_geogrid(extdat,True)

              if self.check_job('metgrid'): self.run_metgrid(extdat,True)

              if self.check_job('tinterp'): self.run_tinterp(extdat,True)  # careful for error

              (wrfstat,bkgfiles) = self.run_real(extdat,wait=True,outmet='tinterp')

              validobs = self.check_wait_obs(self.obstypes)  ## prepare observation data

              if self.check_job('radremap') and len(validobs['radar']) > 0:
                self.run_radar_remaps(validobs['radar'],bkgfiles[0],wrfstat)

              ##
              ## run news3dvar
              ##
              anafile = self.run_news3dvar(validobs,bkgfiles,wrfstat,True)

              if self.check_job('wrfdaupdate') : self.run_wrfdaupdate(False,wait=True)

              (wrfstat,bkgfiles,wrffhr) = self.run_wrf(extdat,False)

              if self.check_job('nclplt') or self.check_job('unipost') or self.check_job('nclplt'):
                  self.run_post(afiles=[os.path.dirname(bkgfile) for bkgfile in bkgfiles])

            elif self.runcase.caseNo == 4:    # Analysis starting from early analysis cycle

              wrfstat = (False,1,1)

              bkgfiles = self.get_cycle_bkgfile(True,4)    # NEWS-e member = 4
              if bkgfiles is None: return 1

              validobs = self.check_wait_obs(self.obstypes)  ## prepare observation data

              if self.check_job('radremap') and len(validobs['radar']) > 0:
                self.run_radar_remaps(validobs['radar'],bkgfiles[0],wrfstat)

              self.command.addlog(999,"domains",self.domain)

              ##
              ## run news3dvar
              ##
              anafiles = self.run_news3dvar(validobs,bkgfiles,wrfstat,True)

              ##
              ## Post-processing
              ##
              if self.check_job('nclplt') or self.check_job('unipost'):
                  self.run_post(afiles=anafiles)

            elif self.runcase.caseNo == 5:    # Forecast starting from an early cycle

               if self.domain.cycle_num < 0:
                 if self.check_job('wrfdaupdate') : self.run_wrfdaupdate(True,wait=True,memid=4)
               else:
                 extdat = self.decode_ungribrun()

                 if self.runcase.endTime > extdat.endTime:

                     extdat = self.check_link_extm(self.runcase.extsrcs)

                     if extdat is None :
                       self.command.addlog(-1,"cntl.run","ERROR: did not find external data source.")

                     if self.domain.isroot and self.check_job('ungrib') :
                        self.run_ungrib(extdat,False)

                     if self.check_job('metgrid'): self.run_metgrid(extdat,True)

                     (wrfstat,bkgfiles) = self.run_real(extdat,wait=True,outmet='metgrid4')

                     if self.check_job('wrfdaupdate') : self.run_wrfdaupdate(False,wait=True)

                 else:
                     if self.check_job('wrfdaupdate') : self.run_wrfdaupdate(True,wait=True)

               (wrfstat,bkgfiles,wrffhr) = self.run_wrf(extdat,False)

               if self.check_job('nclplt') or self.check_job('unipost') or self.check_job('nclplt2d'):
                   self.run_post(afiles=[os.path.dirname(bkgfile) for bkgfile in bkgfiles])

            elif self.runcase.caseNo in (6,7):    # Forecast lauched after cycles of anlysises

              if self.domain.cycle_num >= 0:  # prepare boundary file

                  extdat = self.check_link_extm(self.runcase.extsrcs)

                  if extdat is None :
                    self.command.addlog(-1,"cntl.run","ERROR: did not find external data source.")

                  if self.domain.isroot and self.check_job('ungrib') :
                     self.run_ungrib(extdat,False)

                  if self.check_job('metgrid'): self.run_metgrid(extdat,True)

                  (wrfstat,bkgfiles) = self.run_real(extdat,wait=True,outmet='metgrid5')

              if self.check_job('wrfdaupdate') : self.run_wrfdaupdate(False,wait=True,memid=4)

              (wrfstat,bkgfiles,wrffhr) = self.run_wrf(extdat,False)

              if self.check_job('nclplt') or self.check_job('unipost') or self.check_job('nclplt2d'):
                  self.run_post(afiles=[os.path.dirname(bkgfile) for bkgfile in bkgfiles])

            elif self.runcase.caseNo == 27:    # Hybrid gain with NEWS-e

              if self.check_job('wrfhybrid'):
                  bkgfile = self.run_wrfhybrid(0,True)
              wrfstat = (False,1,1)

              validobs = self.check_wait_obs(self.obstypes)  ## prepare observation data

              if self.check_job('radremap') and len(validobs['radar']) > 0:
                self.run_radar_remaps(validobs['radar'],bkgfiles[0],wrfstat)

              ##
              ## run news3dvar
              ##
              anafiles = self.run_news3dvar(validobs,bkgfile,wrfstat,True)

              if self.check_job('wrfhybrid'):
                 bkgfile = self.run_wrfhybrid(1,True)

              ##
              ## Post-processing
              ##
              if self.check_job('nclplt') or self.check_job('unipost'):
                  self.run_post(afiles=anafiles)

        except Exception as ex:
            self.bucket.put(ex)
            self.kill_received = True

        return
    #enddef run

    ################## Post-processing #################################

    def check_job(self,jobname):
      ''' Check whether we should run this job '''

      if self.kill_received : return False

      if jobname in self.programs :
        return True
      else :
        return False

    #enddef check_job

    ################## run_radar_remaps ################################

    def run_radar_remaps(self,radars,bkgfile,wrfstat):
       '''
           call run_radremap one by one or all in different threads

           must block the workflow untill all processing are finished.

           write a ready file at last
       '''
       mpiconfig = self.runConfig['radremap']
       if mpiconfig.shell:   # run_radremap one by one
         for radar in radars: self.run_radremap(radar,bkgfile,wrfstat,True)
       else:

         th88ds=[]
         for radar in radars :
           radThread = threading.Thread(target=self.run_radremap,
                                        args=(radar,bkgfile,wrfstat,True))
           radThread.start()
           th88ds.append(radThread)

         for radThread in th88ds :
           radThread.join()

       th88fl = open(os.path.join(self.caseDir,'radremap','radremap_Ready'),'w')
       th88fl.write('Process radars done on %s.' %( time.strftime('%m-%d %H:%M:%s') ) )
       th88fl.close()

    #enddef run_radar_remaps

    ################## Post-processing #################################

    def run_post(self,afiles) :
      '''
         run post processing as requested
      '''

      #------------- Wait for file ready --------------------------------
      afile = afiles[0]

      if afile is not None:
        if os.path.isfile(afile):     # afile is passed-in

          if self.check_job('nclplt'):
                self.run_nclplt(afiles,self.runcase.startTime,False)

          if self.check_job('unipost'):
                self.run_unipost(afile,0,False)

          # ##
          # ## extract soundings
          # ##
          # if self.check_job('wrfextsnd') and rematch:
          #     self.run_wrfextsnd(anafile,False)

        elif os.path.isdir(afile):        # WRF forecasting

          endsecs     = self.runcase.fcstSeconds
          outputintvl = self.runcase.fcstOutput

          for itime in range(0,endsecs+outputintvl,outputintvl):
              currtime = self.runcase.startTime + timedelta(seconds=itime)
              outfiles = [os.path.join(wrfdir,'wrfout_d01_%s'%(currtime.strftime('%Y-%m-%d_%H:%M:%S'))) for wrfdir in afiles]
              wrfmin = itime//60

              if self.kill_received : break

              if self.check_job('nclplt'):
                  self.run_nclplt(outfiles,currtime,False)

              #if self.check_job('unipost'):
              #    self.run_unipost(outfile,wrfmin,False)
              #
              #if self.check_job('nclplt2d'):
              #    if itime == 0:
              #        out2dfile = os.path.join(wrfdir,
              #                  'wrfo2d_d01_%s'%(currtime.strftime('%Y-%m-%d_%H:%M:%S')))
              #        self.run_nclplt(afile=outfile,atime=currtime,wait=False,
              #                        bfile=out2dfile,pltfields=['vor2d','ref2d'] )
              #    else:
              #        kstime = itime-outputintvl+300
              #        ketime = itime + 300
              #        for jtime in range(kstime,ketime,300):
              #            time2d = self.runcase.startTime + timedelta(seconds=jtime)
              #            out2dfile = os.path.join(wrfdir,
              #                      'wrfo2d_d01_%s'%(time2d.strftime('%Y-%m-%d_%H:%M:%S')))
              #            self.run_nclplt(afile=outfile,atime=time2d,wait=False,
              #                            bfile=out2dfile,pltfields=['vor2d','ref2d'] )

      return
    #enddef run_post

    ####################################################################
    ##
    ## Internal fucntions
    ##
    ####################################################################

    ####----------------------- case directory -----------------------##
    def create_dirs(self):
      caseroot,caserel,casedom = self.runcase.getCaseDir(self.domain.id)

      casedir = os.path.join(self.command.wrkdir,caseroot)
      while not os.path.lexists(casedir) and self.domain.isroot:        # date
        self.command.addlog(0,"cntl.dir","Making case dir <%s>."%casedir)
        try:
          os.mkdir(casedir)
        except Exception:
          time.sleep(2)
          pass

      timedir = os.path.join(casedir,caserel)
      while not os.path.lexists(timedir) and self.domain.isroot:        # time
        self.command.addlog(0,"cntl.dir","Making case time dir <%s>."%timedir)
        try:
          os.mkdir(timedir)
        except Exception:
          time.sleep(2)
          pass

      domDir = os.path.join(timedir,casedom)
      while not os.path.lexists(domDir):      # domain no
        self.command.addlog(0,"cntl.dir","Making domain dir <%s>."%domDir)
        try:
          os.mkdir(domDir)
        except Exception:
          time.sleep(2)
          pass

      return domDir

    ####--------------------- get_cycle_bkgfile ----------------------##

    def get_cycle_bkgfile(self,wait,dartmbr=4):
        '''
        Find early forecast to use as background for this case.
        It is used for cycling anlysis.
        '''

        timestr = self.runcase.startTime.strftime('%Y-%m-%d_%H:%M:%S')

        if self.domain.cycle_num == -1:  # hybrid variance runs
            bkgdir = os.path.join(self.runDirs['dartdir'],'WRFOUT')
            bkgfile = os.path.join(bkgdir,'wrffcst_d01_%s_%d'%(timestr,dartmbr))
            if self.command.wait_for_a_file('get_cycle_bkgfile',bkgfile,600,600,5,skipread=False):
                return bkgfile
            else:
                self.command.addlog(-1,'cntl.cycle_bkgfile',f'Time out for {bkgfile}.')

        if self.domain.cycle_num == 1:  #based on dom0x, special for the first cycle of runCase 4
            earlydomid = self.domain.id%10
        else:
            earlydomid = self.domain.id
        wrfdirs = ('wrf4','wrf1','wrf2','wrf5')

        earlyTime = self.runcase.startTime - timedelta(seconds=self.runcase.cyclelength)
        caseroot,caserel,casedom = self.runcase.getCaseDir(earlydomid,earlyTime)
        earlydomdir = os.path.join(self.command.wrkdir,caseroot,caserel,casedom)

        if self.runConfig.wrf.numens is not None:
            numens = self.runConfig.wrf.numens+1
            append = True
        else:
            numens = 1
            append = False
        #
        # Wait for WRF directory from early forecast cycle
        #
        maxwaittime = 10800
        waittime = 0
        foundtmpl = False
        while waittime < maxwaittime:
          for wrfdir in wrfdirs:
            if append:
              earlydir = os.path.join(earlydomdir,"%s_0"%wrfdir)
            else:
              earlydir = os.path.join(earlydomdir,wrfdir)

            self.command.addlog(0,'cntl.cycle_bkgfile','Checking WRF directory <%s>.' % (earlydir))
            if os.path.lexists(earlydir):
              foundtmpl = True
              break

          if foundtmpl:
            self.command.addlog(0,'cntl.cycle_bkgfile','Use WRF directory <%s> for cycled analysis background.' % (earlydir))
            break
          else:
            waittime += 10
            time.sleep(10)
        else:
            self.command.addlog(-1,"cntl.cycle_bkgfile","Early cycle dir <%s> do not exist."%earlydir)

        bkgfiles = []
        for iens in range(0,numens):
          if append:
            dirbas = re.sub(r"_\d{1,3}$","",earlydir)
            earlymemdir = "%s_%d"%(dirbas,iens)
          else:
            earlymemdir = earlydir

          bkgfile = os.path.join(earlymemdir,'wrfout_d01_%s'%timestr)
          bkgready = os.path.join(earlymemdir,'wrfoutReady_d01_%s'%timestr)
          self.command.addlog(0,"cntl.cycle_bkgfile",f"Waiting for {bkgready} ...")
          if self.command.wait_for_a_file('get_cycle_bkgfile',bkgready,7200,600,5,skipread=True):
              bkgfiles.append(bkgfile)
          else:
              self.command.addlog(-1,"cntl.cycle_bkgfile","Timeout for <%s>."%bkgready)

        return bkgfiles

    ####----------------------- decode early ungrib run --------------##

    def decode_ungribrun(self):
       '''
          Should be called only by runcase = 2 & 5

          only decode control member (0)
       '''

       if self.runcase.caseNo == 5:
           ungrib_base = self.domain.cycle_base
       else:
           ungrib_base = os.path.dirname(self.caseDir)

       ensaffix=""
       if self.runConfig.ungrib.numens is not None:
           ensaffix="_0"

       maxwaittime = 10800
       waittime = 0
       foundungrib = False
       while waittime < maxwaittime:
         for ungrbdir in ('ungrib2','ungrib4','ungrib5','ungrib0'):
           ungrib_path   = os.path.join(ungrib_base,"%s%s"%(ungrbdir,ensaffix))
           self.command.addlog(0,'DECODE_UNGRIBRUN','Checking ungrib directory <%s>.' % (ungrib_path))
           if os.path.lexists(ungrib_path):
             foundungrib = True
             break
         if foundungrib:
           self.command.addlog(0,'DECODE_UNGRIBRUN','Use ungrib date from directory <%s>.' % (ungrib_path))
           break
         else:
           waittime += 10
           time.sleep(10)

       #if not os.path.lexists(ungrib_path):
       if not self.command.wait_for_a_file('decode_ungribrun',ungrib_path,7200,600,5,skipread=True):
         self.command.addlog(-1,'DECODE_UNGRIBRUN','Cannot find ungrib path <%s>.' % ungrib_path)

       namelist_file = os.path.join(ungrib_path,'namelist.wps')

       self.command.addlog(999,"cntl.ungrb",'<decode_ungribrun> waiting for %s ...' %namelist_file)
       self.command.wait_for_a_file('decode_ungribrun',namelist_file,10800,600,5,skipread=True)

       nmlgrp = namelist.decode_namelist_file(namelist_file)

       extdname = nmlgrp['ungrib'].prefix
       extconf  = self.runcase.getExtConfig(extdname)
       extdat   = ExtMData(extdname,extconf)

       extdat['startTime'] = datetime.strptime((nmlgrp['share'].start_date)[0],'%Y-%m-%d_%H:%M:%S')
       extdat['endTime']   = datetime.strptime((nmlgrp['share'].end_date)[0],  '%Y-%m-%d_%H:%M:%S')

       return extdat
    #enddef decode_ungribrun

    ##========================== UNGRIB   ==============================

    def run_ungrib(self,extsrc,wait) :
      ''' run ungrib.exe
      wait : whether to wait for the job to be finished
      '''

      tmplinput = self.runcase.getNamelistTemplate('ungrib')
      mpiconfig = self.runConfig['ungrib']

      if mpiconfig.mpi:
        executable = os.path.join(self.runDirs.wpsdir,'ungrib','ungribp.exe')
      else:
        executable = os.path.join(self.runDirs.wpsdir,'ungrib','ungrib.exe')

      wrkbas,wrkdirs = self.runcase.getwrkdir(self.caseDir,"ungrib",numens=mpiconfig.numens)
      #os.path.join(os.path.dirname(self.caseDir),'ungrib%d'%(self.runcase.caseNo-1))

      nmlfiles = []
      for wrkdir in wrkdirs:
        if not os.path.lexists(wrkdir) :
          os.mkdir(wrkdir)

        nmlfiles.append(os.path.join(wrkdir,'namelist.wps'))

      ##-------------- make namelist file -------------------------------

      inittimestr = extsrc.startTime.strftime('%Y-%m-%d_%H:%M:%S')
      endtimestr  = extsrc.endTime.strftime('%Y-%m-%d_%H:%M:%S')

      nmlin = { 'start_date' : [inittimestr,inittimestr],
                'end_date'   : [endtimestr, endtimestr],
                'interval_seconds' : extsrc.hourIntvl*3600,
                'prefix'           : extsrc.extapp
               }

      nmlgrp = namelist.decode_namelist_file(tmplinput)
      nmlgrp.merge(nmlin)
      for nmlfile in nmlfiles:
        nmlgrp.writeToFile(nmlfile)
        ##print "write to %s ..." % nmlfile

      ##----------- run the program from command line ----------------
      hhmmstr = self.runcase.startTime.strftime('%H%M')
      outfile = 'ungrb%d_%s.output'% (self.runcase.caseNo,hhmmstr)
      jobid = self.command.run_a_program(executable,None,outfile,wrkbas,mpiconfig)

      ##-------------- wait for it to be done -------------------
      if not self.command.wait_job('ungrib',jobid,wait) :
          self.command.addlog(-1,"UNGRIB",f'job failed: {jobid}' )
          #raise SystemExit()

      return 0
    #enddef run_ungrib

    ##========================= GEOGRID  ===============================

    def run_geogrid(self,extsrc,wait) :
      ''' run geogrid.exe '''

      executable = os.path.join(self.runDirs.wpsdir,'geogrid','geogrid.exe')
      tmplinput  = self.runcase.getNamelistTemplate('geogrid')

      mpiconfig = self.runConfig['geogrid']

      wrkbas,wrkdirs = self.runcase.getwrkdir(self.caseDir,'geogrid',False,mpiconfig.numens)
      #os.path.join(self.caseDir,'geogrid')

      wrkdir = wrkdirs[0]
      if not os.path.lexists(wrkdir) :
        os.mkdir(wrkdir)

      nmlfile = os.path.join(wrkdir,'namelist.wps')

      ##---------------------- copy working files ------------------------

      geogrid_path = os.path.join(self.caseDir,'geogrid')

      geotbl = os.path.join(wrkdir,'GEOGRID.TBL')
      if not os.path.lexists(geotbl) :
        absfl = os.path.join(self.runDirs.wpsdir,'geogrid','GEOGRID.TBL.ARW')
        self.command.copyfile(absfl,geotbl,hardcopy=False)

      ##-------------- make namelist file --------------------------------

      inittimestr = extsrc.startTime.strftime('%Y-%m-%d_%H:%M:%S')
      endtimestr  = extsrc.endTime.strftime('%Y-%m-%d_%H:%M:%S')

      nmlin = {  'start_date' : [inittimestr,inittimestr],
                 'end_date'   : [endtimestr, endtimestr],
                 'interval_seconds' : extsrc.hourIntvl*3600,
                 'opt_metgrid_tbl_path'         : './',
                 'opt_output_from_geogrid_path' : './',
                 'e_we'    : self.domain.nx,
                 'e_sn'    : self.domain.ny,
                 'dx'      : self.domain.dx,
                 'dy'      : self.domain.dy,
                 'ref_lat' : self.domain.ctrlat,
                 'ref_lon' : self.domain.ctrlon,
                 'map_proj'  : self.domain.map_proj,
                 'truelat1'  : self.domain.truelat1,
                 'truelat2'  : self.domain.truelat2,
                 'stand_lon' : self.domain.standlon,
                 'opt_geogrid_tbl_path' : './',
               }

      nmlgrp = namelist.decode_namelist_file(tmplinput)
      nmlgrp.merge(nmlin)
      nmlgrp.writeToFile(nmlfile)

      ##------------ run the program from command line -------------------
      outfile = 'geo%02d.output'%self.domain.id
      jobid = self.command.run_a_program(executable,None,outfile,wrkdir,mpiconfig)

      ##-------------- wait for it to be done -------------------
      if not self.command.wait_job('geogrid',jobid,wait) :
        self.command.addlog(-1,"GEOGRID",f'job failed: {jobid}' )
        #raise SystemExit()

      return
    #enddef run_geogrid

    ##========================= METGRID  ===============================

    def run_metgrid(self,extsrc,wait,condition=None) :
      ''' run metgrid.exe '''

      executable = os.path.join(self.runDirs.wpsdir,'metgrid','metgrid.exe')
      tmplinput  = self.runcase.getNamelistTemplate('metgrid')

      mpiconfig   = self.runConfig.metgrid

      wrkbas,wrkdirs = self.runcase.getwrkdir(self.caseDir,'metgrid',numens=mpiconfig.numens)

      nmlfiles=[]
      for wrkdir in wrkdirs:
        if not os.path.lexists(wrkdir) :
          os.mkdir(wrkdir)

        nmlfiles.append(os.path.join(wrkdir,'namelist.wps'))

      ##---------------------- copy working files -----------------------

        mettbl = os.path.join(wrkdir,'METGRID.TBL')
        if not os.path.lexists(mettbl) :
          absfl = os.path.join(self.runDirs.wpsdir,'metgrid','METGRID.TBL.ARW.%s'%extsrc.extapp)
          self.command.copyfile(absfl,mettbl,hardcopy=False)

      ##------------------- wait for GEOGRID files ----------------------
      if self.runcase.caseNo in (1,2,3):
          geogrid_path = os.path.join(self.caseDir,'geogrid')
      else:
          geogrid_path = os.path.join(self.domain.cycle_base,'dom00','geogrid')

      geonmlfl = os.path.join(geogrid_path,'namelist.wps')
      self.command.wait_for_a_file('run_metgrid',geonmlfl,7200,600,5)
      nmlgrp = namelist.decode_namelist_file(geonmlfl)
      io_form_geogrid = nmlgrp['share'].io_form_geogrid
      if io_form_geogrid > 100 and mpiconfig.mpi:
         geofls = []
         for i in range(mpiconfig.ntotal):
            geofl1 = os.path.join(geogrid_path,'geo_em.d01.nc_%04d'%i)
            geofls.append(geofl1)
      else:
          geofl1 = os.path.join(geogrid_path,'geo_em.d01.nc')
          geofls = [geofl1]

      ##------------------- Wait for ungrib ready ----------------------

      fgnames = [extsrc.extapp]
      ungrbconfig = self.runConfig.ungrib

      if self.runcase.caseNo == 2:
          nothing,ungrib_paths = self.runcase.getwrkdir(self.caseDir,'ungrib0',False,numens=ungrbconfig.numens)
      else:
          nothing,ungrib_paths = self.runcase.getwrkdir(self.caseDir,'ungrib',numens=ungrbconfig.numens)

      iens = 0
      for wrkdir in wrkdirs:
        ix = extsrc.hourIntvl*3600
        if self.runcase.caseNo == 1:
            endtime = extsrc.startTime + timedelta(seconds=ix)
            ie = ix
        else:
            endtime = extsrc.endTime
            ie = int( (extsrc.endTime-extsrc.startTime).total_seconds() )

        ungrib_path = ungrib_paths[iens]
        ungribfiles = []
        for ia in range(0,ie+100,ix):  # wait for all times in case it is processed parallel
            currtime = extsrc.startTime + timedelta(seconds=ia)
            endtimestr  = currtime.strftime('%Y-%m-%d_%H')
            for fgname in fgnames:
                endfile = os.path.join(ungrib_path,'%s:%s'%(fgname,endtimestr))
                ungribfiles.append(endfile)

        self.command.addlog(999,"cntl.met","Waiting for:")
        for extfile in ungribfiles:
            self.command.addlog(999,"cntl.met",extfile)

        if self.command.wait_for_files('run_metgrid',ungribfiles,7200,600,5) == len(ungribfiles):
            for srcfl in ungribfiles :
                fl    = os.path.basename(srcfl)
                relfl = os.path.join(wrkdir,fl)
                if not os.path.lexists(relfl) :
                    os.symlink(srcfl,relfl)
        else :
            self.command.addlog(-2,"cntl.met",'Path "%s" is not found' % ungrib_path)

        iens += 1

      ##--------------- make sure condition file is availabe ------------

      if condition is not None:
          self.command.addlog(999,"cntl.met","<run_metgrid> waiting for %s ..."%condition)
          self.command.wait_for_a_file('run_metgrid',condition,3600,300,10,skipread=True)

      ##-------------- make namelist file --------------------------------

      inittimestr = extsrc.startTime.strftime('%Y-%m-%d_%H:%M:%S')
      endtimestr  = endtime.strftime('%Y-%m-%d_%H:%M:%S')

      nmlin = { 'start_date'                   : inittimestr,
                'end_date'                     : endtimestr,
                'interval_seconds'             : extsrc.hourIntvl*3600,
                'opt_metgrid_tbl_path'         : './',
                'opt_output_from_geogrid_path' : geogrid_path,
                'opt_output_from_metgrid_path' : './',
                'fg_name'                      : fgnames
               }

      nmlgrp = namelist.decode_namelist_file(tmplinput)
      nmlgrp.merge(nmlin)
      for nmlfile in nmlfiles:
        nmlgrp.writeToFile(nmlfile)

      ##------------ run the program from command line ------------------
      hhmmstr = self.runcase.startTime.strftime('%H%M')
      outfile = 'met%d_%s.output'%(self.runcase.caseNo,hhmmstr)
      jobid = self.command.run_a_program(executable,None,outfile,wrkbas,mpiconfig)

      ##-------------- wait for it to be done -------------------
      if not self.command.wait_job('metgrid',jobid,wait) :
        self.command.addlog(-1,"METGRID",f'job failed: {jobid}' )
        #raise SystemExit()

    #enddef run_metgrid

    ##========================= TINTERP ================================

    def run_tinterp(self, extsrc, wait) :
      '''
        Run or submit TINTERP job from METGRID outputs

        NOTE: works with runCase 1 only. Support the forecast length is 5
              minutes and the starting time in 5-minute intervals.
      '''

      executable = os.path.join(self.runDirs.vardir,'bin','tinterp')
      tmplinput  = self.runcase.getNamelistTemplate('tinterp')

      mpiconfig = self.runConfig.tinterp
      if mpiconfig.mpi : executable += '_mpi'

      wrkbas,wrkdirs = self.runcase.getwrkdir(self.caseDir,'tinterp',False,mpiconfig.numens)
      for wrkdir in wrkdirs:
        #os.path.join(self.caseDir,'tinterp')
        if not os.path.lexists(wrkdir) :
          os.mkdir(wrkdir)

      ##---------------- loop all external files ------------------------

      # Datetime objects of external two continous data files
      extTime1 = extsrc.startTime
      extTime2 = extsrc.startTime + timedelta(hours=extsrc.hourIntvl)

      file1 = 'met_em.d01.%s.nc' % extTime1.strftime('%Y-%m-%d_%H:%M:%S')
      file2 = 'met_em.d01.%s.nc' % extTime2.strftime('%Y-%m-%d_%H:%M:%S')

      # Expected datatime objects for this analysis/forecast

      expTime1 = self.runcase.startTime
      expTime2 = self.runcase.startTime + timedelta(seconds=self.runcase.fcstSeconds)

      if extTime1 == expTime1 and extTime2 == expTime2:
          # No time interpolation is ncessary, just link the original metgrid files
          skiptinterp = True
      else:
          skiptinterp = False
      #
      # NOTE: works with runCase 1 only. Support the forecast length is
      #      within [self.extmInitTime,+self.runcase.getCaseExtHours(extsrc,1)]
      #

      ## Target domain time interval in seconds
      nouttime = 2
      outsecd1 = (expTime1 - extTime1).total_seconds()
      outsecd2 = (expTime2 - extTime1).total_seconds()
      outtime = [outsecd1, outsecd2]

      if extsrc.extapp.startswith('NAM') or extsrc.extapp in ('GFS', 'GEFS'):
          varlist  = ['PRES','SM','ST','RH','VV','UU','TT',
                     'SM100200','SM040100','SM010040','SM000010',
                     'ST100200','ST040100','ST010040','ST000010',
                     'SKINTEMP','PSFC','SNOALB','SOILTEMP']
      elif extsrc.extapp.startswith('RAP'):
          #varlist  = ['PRES','RH','VV','UU','TT', 'SOILT','SOILM',
          #           'SOILM300','SOILM160','SOILM100','SOILM060','SOILM030',
          #           'SOILM010','SOILM004','SOILM001','SOILM000',
          #           'SOILT300','SOILT160','SOILT100','SOILT060','SOILT030',
          #           'SOILT010','SOILT004','SOILT001','SOILT000',
          #           'SKINTEMP','PSFC','SNOALB','SOILTEMP']
          varlist  = ['PRES','RH','VV','UU','TT', 'SOILT','SOILM',
                     'SOILM300','SOILM160',
                     'SOILT300','SOILT160',
                     'SKINTEMP','PSFC','SNOALB','SOILTEMP']
      elif extsrc.extapp.startswith('HRRR'):
          #varlist  = ['PRES','RH','VV','UU','TT', 'SOILT','SOILM',
          #           'SOILM300','SOILM160','SOILM100','SOILM060','SOILM030',
          #           'SOILM010','SOILM004','SOILM001','SOILM000',
          #           'SOILT300','SOILT160','SOILT100','SOILT060','SOILT030',
          #           'SOILT010','SOILT004','SOILT001','SOILT000',
          #           'SKINTEMP','PSFC','SNOALB','SOILTEMP']
          varlist  = ['PRES','RH','VV','UU','TT', 'SOILT','SOILM',
                     'SOILM000','SOILM001','SOILM004','SOILM010','SOILM030',
                     'SOILM060','SOILM100','SOILM160','SOILM300',
                     'SOILT000','SOILT001','SOILT004','SOILT010','SOILT030',
                     'SOILT060','SOILT100','SOILT160','SOILM300',
                     'SKINTEMP','PSFC','SNOALB','SOILTEMP']

      metconfig = self.runConfig.metgrid
      nothing,metdirs = self.runcase.getwrkdir(self.caseDir,'metgrid',numens=metconfig.numens)

      nmlgrp  = namelist.decode_namelist_file(tmplinput)
      nmlfile = 'ttrp-%02d.input' % self.domain.id

      iens = 0
      for wrkdir in wrkdirs:

        absfile1 = os.path.join(metdirs[iens],file1)
        absfile2 = os.path.join(metdirs[iens],file2)
        absnmlfile = os.path.join(wrkdir,nmlfile)

        nmlin = {'hisfile'   :  [absfile1, absfile2],
                 'hisfmt'    :  1,
                 'nvarlist'  : len(varlist),
                 'varlist'   : varlist,
                 'outdir'    :  './',
                 'outfmt'    :  202,
                 'nouttime'  :  nouttime,
                 'outtime'   :  outtime,
                 'nproc_x'   :  mpiconfig.nproc_x,
                 'nproc_y'   :  mpiconfig.nproc_y,
                }
        nmlgrp.merge(nmlin)
        nmlgrp.writeToFile(absnmlfile)
        if skiptinterp:  # Do not run tinterp, but just link the original metgrid files
            relfl1 = os.path.join(wrkdir,file1)
            relfl2 = os.path.join(wrkdir,file2)
            self.command.copyfile(absfile1,relfl1,hardcopy=False)
            self.command.copyfile(absfile1,relfl2,hardcopy=False)
        iens += 1

      ##-------------- run the program --------------------------
      if not skiptinterp:
          hhmmstr = self.runcase.startTime.strftime('%H%M')
          outfile = 'ttrp%d_%s.output' % (self.runcase.caseNo,hhmmstr)

          jobid = self.command.run_a_program(executable,nmlfile,outfile,wrkbas,mpiconfig)

      ##-------------- wait for it to be done -------------------

          if not self.command.wait_job('tinterp',jobid,wait) :
             self.command.addlog(-1,"TINTERP",f'job failed: {jobid}' )
             #raise SystemExit()

    #enddef run_tinterp

    ##========================= REAL.exe ===============================

    def run_real(self, extsrc, wait, outmet='outmet',condition=None) :
      '''
        Run or submit real.exe job
      '''

      executable = os.path.join(self.runDirs.wrfdir,'main','real.exe')

      mpiconfig = self.runConfig['real']

      wrkbas,wrkdirs = self.runcase.getwrkdir(self.caseDir,'real',numens=mpiconfig.numens)

      nmlfiles = []
      for wrkdir in wrkdirs:
        if not os.path.lexists(wrkdir) :
          os.mkdir(wrkdir)

        ## clean rsl.*
        #subprocess.call('rm -rf rsl.*',shell=True,cwd=wrkdir)

        nmlfiles.append(os.path.join(wrkdir,'namelist.input'))

      ##---------------------- copy working files ------------------------

      if mpiconfig.numens is None:
          metdirs = [os.path.join(self.caseDir,outmet)]
      else:
          metdirs = [os.path.join(self.caseDir,"%s_%d"%(outmet,iens))
                                 for iens in range(0,mpiconfig.numens+1)]

      if self.runcase.caseNo == 1:
        realstart = self.runcase.startTime
        realend   = self.runcase.endTime
        extint    = self.runcase.fcstSeconds
      elif self.runcase.caseNo == 3:        # should be checked again, especially when time sampling is on
        realstart = self.runcase.startTime
        realend   = self.runcase.endTime
        extint    = self.runcase.fcstOutput
      else:
        realstart = extsrc.startTime
        realend   = extsrc.endTime
        extint    = extsrc.hourIntvl*3600

      fcstlength = int( (realend-realstart).total_seconds() )
      iens = 0
      for wrkdir in wrkdirs:
        metdir = metdirs[iens]
        for i in range(0,fcstlength+60,extint):
            ftime = realstart+timedelta(seconds=i)
            timestr = ftime.strftime('%Y-%m-%d_%H:%M:%S')
            fl = 'met_em.d01.%s.nc' % timestr

            basfl = os.path.join(metdir,fl )
            #li = basfl.rsplit('met_em.d')
            #readyfl = 'met_emReady.d'.join(li)
            #if self.command.wait_for_a_file('run_real',readyfl,1800,300,10,skipread=True):
            for absfl in glob.iglob('%s*' % basfl):
                relfl = os.path.join(wrkdir,os.path.basename(absfl))
                if not os.path.lexists(relfl) :
                    self.command.copyfile(absfl,relfl,hardcopy=False)
        iens += 1
      ##--------------- make sure condition file is availabe ------------

      if condition is not None:
          #print "waiting for %s ..." %(condition)
          self.command.wait_for_a_file('run_real',condition,1800,300,10,skipread=True)

      ##---------------- make namelist file ----------------------------

      (runday,runhr) = divmod(fcstlength, 24*3600)
      (runhr, runmn) = divmod(runhr, 3600)
      (runmn, runsc) = divmod(runmn, 60)

      nmlin = {'run_days'              :  runday,
               'run_hours'             :  runhr,
               'run_minutes'           :  runmn,
               'run_seconds'           :  runsc,
               'start_year'            :  realstart.year,
               'start_month'           :  realstart.month,
               'start_day'             :  realstart.day,
               'start_hour'            :  realstart.hour,
               'start_minute'          :  realstart.minute,
               'start_second'          :  realstart.second,
               'end_year'              :  realend.year,
               'end_month'             :  realend.month,
               'end_day'               :  realend.day,
               'end_hour'              :  realend.hour,
               'end_minute'            :  realend.minute,
               'end_second'            :  realend.second,
               'nproc_x'               :  mpiconfig.nproc_x,
               'nproc_y'               :  mpiconfig.nproc_y,
               'interval_seconds'      :  extint,
               'auxinput1_inname'      :  'met_em.d<domain>.<date>',
               'num_metgrid_levels'      : extsrc.nz,
               'num_metgrid_soil_levels' : extsrc.nzsoil,
               'e_we'                  :  self.domain.nx,
               'e_sn'                  :  self.domain.ny,
               'dx'                    :  self.domain.dx,
               'dy'                    :  self.domain.dy,
              }
      iens = 0
      for nmlfile in nmlfiles:
        tmplinput  = self.runcase.getNamelistTemplate('real',iens)
        nmlgrp = namelist.decode_namelist_file(tmplinput)

        if extsrc.extname == 'HRRR':
            nmlin["p_top_requested"] = 5000
        elif extsrc.extname in ('HRRRX','HRRRE'):
            nmlin["p_top_requested"] = 2000

        nmlgrp.merge(nmlin)
        nmlgrp.writeToFile(nmlfile)
        iens += 1

      ##-------------- run the program --------------------------

      hhmmstr = self.runcase.startTime.strftime('%H%M')
      outfile = 'real%d_%s.output'%(self.runcase.caseNo,hhmmstr)

      if self.check_job('real'):
        jobid = self.command.run_a_program(executable,None,outfile,wrkbas,mpiconfig)

        ##-------------- wait for it to be done -------------------
        ##print 'checking real with ', status,jobid,wait
        if not self.command.wait_job('real',jobid,wait) :
          self.command.addlog(-1,"REAL",f'job failed: {jobid}' )
          #raise SystemExit()

      real_io_form = nmlgrp['time_control'].io_form_input
      outstat = (False,1,1)
      if real_io_form > 100 :
        outstat = self.runConfig['real'].getmpi()

      ##-------------- Pack output file for analysis background ---------

      wrfinputf = [os.path.join(wrkdir,'wrfinput_d01' ) for wrkdir in wrkdirs]

      return (outstat,wrfinputf)
    #enddef run_real

    ##%%%%%%%%%%%%%%%%%  Link 9 km real from early run  %%%%%%%%%%%%%%%%

    def link_wait_real(self,wrfdir,iens) :
      '''find valid real output from an early run'''

      ##
      ##--------------------- find a valid real run ----------------

      if self.command.showonly : return True

      realconfig = self.runConfig.real
      nothing,realdirs = self.runcase.getwrkdir(self.caseDir,'real',numens=realconfig.numens)
      #os.path.join(self.caseDir,'real%d'%(self.runcase.caseNo-1))

      realdir = realdirs[iens]
      if not os.path.lexists(realdir) :
        self.command.addlog(-1,"LINK",f'Directory {realdir} does not exist!' )
        #raise SystemExit()

      maxwaitime = 30*60
      waitime = 0
      realdone = False
      while not realdone and waitime < maxwaitime :
        self.command.addlog(0,"cntl.wreal", 'Waiting for real rsl.error.0000. \n')
        filename = os.path.join(realdir,'rsl.error.0000')
        if os.path.lexists(filename) :
          fh = open(filename,'r')
          for line in fh:
            if line.find('real_em: SUCCESS COMPLETE REAL_EM INIT') >= 0 :
              realdone = True
              break
        time.sleep(10)
        waitime += 10

      if waitime >= maxwaitime :
        self.command.addlog(-1,"cntl.wreal",
           'Waiting for real (rsl.out.0000 in %s) excceeded %d seconds.\n' % (
           realdir,waitime) )

      ##--------------------- Link wrfinput and wrfbdy  ----------------

      ##if self.runcase.maxdom == 2  :
      ##  realfiles = ['wrfinput_d01','wrfinput_d02','wrfbdy_d01']
      ##else :
      realfiles = ['wrfinput_d01','wrfbdy_d01']

      for fl in realfiles :
        relfl = os.path.join(wrfdir,fl)
        if not os.path.lexists(relfl) :
          absfl = os.path.join(realdir,fl)
          os.symlink(absfl,relfl)

      return True
    #enddef link_wait_real

    ##========================= WRF.exe ================================

    def run_wrf(self,extsrc,wait) :
      '''
        Run or submit wrf.exe job
      '''

      executable = os.path.join(self.runDirs.wrfdir,'main','wrf.exe')

      mpiconfig = self.runConfig['wrf']

      ##---------------- make preparation for a WRF run -----------------


      #runfiles = ['CAM_ABS_DATA',    'RRTM_DATA', 'ETAMPNEW_DATA.expanded_rain',
      #            'CAM_AEROPT_DATA', 'RRTM_DATA_DBL', 'URBPARM.TBL',
      #            'co2_trans',       'RRTMG_LW_DATA', 'VEGPARM.TBL',
      #            'ETAMPNEW_DATA',   'RRTMG_LW_DATA_DBL', 'ETAMPNEW_DATA_DBL',
      #            'ozone.formatted', 'RRTMG_SW_DATA',     'GENPARM.TBL',
      #            'ozone_lat.formatted',  'RRTMG_SW_DATA_DBL',  'grib2map.tbl',
      #            'ozone_plev.formatted', 'SOILPARM.TBL', 'gribmap.txt',
      #            'LANDUSE.TBL',      'tr67t85', 'tr49t67', 'tr49t85' ]
      #            'freezeH2O.dat',    'qr_acr_qg.dat',  'qr_acr_qs.dat' ]
      runfiles = ['aerosol.formatted', 'aerosol_lat.formatted', 'aerosol_lon.formatted',
                  'aerosol_plev.formatted', 'CCN_ACTIVATE.BIN', 'ETAMPNEW_DATA',
                  'GENPARM.TBL', 'gribmap.txt', 'LANDUSE.TBL',
                  'ozone.formatted', 'ozone_lat.formatted', 'ozone_plev.formatted',
                  'RRTM_DATA', 'RRTMG_LW_DATA',
                  'RRTMG_SW_DATA', 'SOILPARM.TBL', 'tr49t67', 'tr49t85', 'tr67t85',
                  'VEGPARM.TBL' ]
                  #'freezeH2O.bin','qr_acr_qg.bin', 'qr_acr_qs.bin',
      #
      #            #'CAM_ABS_DATA',    'ETAMPNEW_DATA.expanded_rain',
      #            #'CAM_AEROPT_DATA', 'RRTM_DATA_DBL', 'URBPARM.TBL',
      #            #'co2_trans',
      #            #'RRTMG_LW_DATA_DBL', 'ETAMPNEW_DATA_DBL',
      #            #'RRTMG_SW_DATA_DBL',  'grib2map.tbl',

      wrkbas,wrkdirs = self.runcase.getwrkdir(self.caseDir,'wrf',numens=mpiconfig.numens)
      iens = 0
      nmlfiles = []
      for wrkdir in wrkdirs:
        if not os.path.lexists(wrkdir) :
          os.mkdir(wrkdir)

        if self.runcase.caseNo == 1:
            status = self.link_wait_real(wrkdir,iens)
            if not status : return None
        iens += 1

        ##---------------- make output directory --------------------------

        ## clean rsl.*
        subprocess.call('rm -rf rsl.*',shell=True,cwd=wrkdir)

        ##------------------------ copy working files ---------------------

        for fl in runfiles:
          relfl = os.path.join(wrkdir,fl)
          if not os.path.lexists(relfl) :
            absfl = os.path.join(self.runDirs.wrfdir,'run',fl)
            self.command.copyfile(absfl,relfl,hardcopy=False)

      ##--------------- make namelist file ------------------------------

        nmlfiles.append( os.path.join(wrkdir,'namelist.input'))

      history_interval = self.runcase.fcstOutput//60
      history_begin    = 0
      history_end      = self.runcase.fcstSeconds//60
      numsample        = self.runcase.ntimesample//2
      if self.runcase.caseNo in (2,5):              # forward forecast only the last output is needed
        history_interval = self.runcase.timesamin   # good as long as not 0
        history_begin    = self.runcase.fcstSeconds//60 - 2*numsample*self.runcase.timesamin
        history_end      = self.runcase.fcstSeconds//60

      endTime  = self.runcase.endTime
      fcstleng = self.runcase.fcstSeconds
      (runday,runhr) = divmod(fcstleng, 24*3600)
      (runhr, runmn) = divmod(runhr, 3600)
      (runmn, runsc) = divmod(runmn, 60)

      nmlin = {'run_days'           :  runday,
               'run_hours'          :  runhr,
               'run_minutes'        :  runmn,
               'run_seconds'        :  runsc,
               'start_year'         :  self.runcase.startTime.year,
               'start_month'        :  self.runcase.startTime.month,
               'start_day'          :  self.runcase.startTime.day,
               'start_hour'         :  self.runcase.startTime.hour,
               'start_minute'       :  self.runcase.startTime.minute,
               'start_second'       :  self.runcase.startTime.second,
               'end_year'           :  endTime.year,
               'end_month'          :  endTime.month,
               'end_day'            :  endTime.day,
               'end_hour'           :  endTime.hour,
               'end_minute'         :  endTime.minute,
               'end_second'         :  endTime.second,
               'interval_seconds'   :  self.runcase.fcstSeconds,
               'nproc_x'            :  mpiconfig.nproc_x,
               'nproc_y'            :  mpiconfig.nproc_y,
               #'history_outname'    :  'wrfout_d<domain>_<date>',
               'history_begin'      :  history_begin,
               'history_end'        :  history_end,
               'history_interval'   :  history_interval,
               'e_we'               :  self.domain.nx,
               'e_sn'               :  self.domain.ny,
               'dx'                 :  self.domain.dx,
               'dy'                 :  self.domain.dy,
              }

      iens = 0
      for nmlfile in nmlfiles:
          tmplinput  = self.runcase.getNamelistTemplate('wrf',iens)
          nmlgrp = namelist.decode_namelist_file(tmplinput)

          if extsrc.extname == 'HRRR':
              nmlin["p_top_requested"] = 5000
          elif extsrc.extname in ('HRRRX','HRRRE'):
              nmlin["p_top_requested"] = 2000

          nmlgrp.merge(nmlin)

          nmlgrp.writeToFile(nmlfile)
          iens += 1

      ##-------------- run the program from command line -------------------

      if self.check_job('wrf'):
          hhmmstr = self.runcase.startTime.strftime('%H%M')
          outfile = 'wrf%d_%s.output'%(self.runcase.caseNo,hhmmstr)
          jobid = self.command.run_a_program(executable,None,outfile,wrkbas,mpiconfig)

          ##-------------- wait for it to be done -------------------
          if not self.command.wait_job('wrf',jobid,wait,12*3600) :
              self.command.addlog(-1,"WRF",f'job failed: {jobid}' )
              #raise SystemExit()

      wrf_io_form = nmlgrp['time_control'].io_form_history
      outstat = (False,1,1)
      if wrf_io_form > 100 :
        outstat = self.runConfig['wrf'].getmpi()

      ##-------------- wait for it to be done -------------------
      wrfhr = 0
      filetime = self.runcase.startTime.strftime('%Y-%m-%d_%H:%M:%S')
      wrfifile = [os.path.join(wrkdir,'wrfout_d01_%s'%filetime ) for wrkdir in wrkdirs]

      return (outstat,wrfifile,wrfhr)

    #enddef run_wrf

    ##======================== radremap   ==============================

    def run_radremap(self,radname,bkgfile,wrfstat,wait) :
        ''' run radremap.exe
        wait : whether to wait for the job to be finished

        It uses background file bkgfile
        '''

        executable = os.path.join(self.runDirs.vardir,'bin','radremap')
        tmplinput  = self.runcase.getNamelistTemplate('radremap')

        mpiconfig = self.runConfig['radremap']
        if mpiconfig.mpi : executable += '_mpi'

        wrkbas,wrkdirs = self.runcase.getwrkdir(self.caseDir,'radremap',False,None)
        #os.path.join(self.caseDir,'radremap')
        wrkdir = wrkdirs[0]
        radinfo = os.path.join(wrkdir,'radarinfo.dat')

        radconf = self.runcase.getObsConfig('radar')

        if self.mutex.acquire() :
          if not os.path.lexists(wrkdir) :
            os.mkdir(wrkdir)

          ##-------------- Copy radarinfo.dat ---------------------------

          if not os.path.lexists(radinfo) :
              infosrc=os.path.join(self.runDirs.inputdir,radconf.radarinfo)
              self.command.copyfile(infosrc, radinfo,hardcopy=False )
          self.mutex.release()

        ##-------------- Perpare for data files -------------------------

        hhmmstr = self.runcase.startTime.strftime('%H%M')
        runname = 'rad%d%s_%s' % ( self.runcase.caseNo,radname,hhmmstr )
        nmlfile = os.path.join(wrkdir,'%s.input' % runname)

        timstr  = self.runcase.startTime.strftime('%Y%m%d%H%M')

        ##-------------- make namelist file ------------------------------

        inittimestr = self.runcase.startTime.strftime('%Y-%m-%d.%H:%M:%S')
        timstr      = self.runcase.startTime.strftime('%Y%m%d%H%M%S')
        reftime     = self.runcase.startTime.strftime('%Y%m%d_%H%M')

        nmlgrp = namelist.decode_namelist_file(tmplinput)
        if nmlgrp["radremapopt"].nlistfil == 1:
            radlist = 'RADR_%s%s.list' % (radname,timstr)
            radfile = 'xxxx'

            if not os.path.lexists(os.path.join(wrkdir,radlist)) :
              ##print radfile
              self.command.addlog(-1,"cntl.radar",'Radar list for %s is not found.'% radname)
        else:
            radlist = 'xxxx'
            radfile = 'RADR_%s%s.raw' % (radname,timstr)

            if not os.path.lexists(os.path.join(wrkdir,radfile)) :
              ##print radfile
              self.command.addlog(-1,"cntl.radar",'Radar file for %s is not found.'% radname)

        radar98 = 0

        nmlin = { 'initime'  : inittimestr,
                  'inifile'  : bkgfile,
                  'inigbf'   : 'xxxxxx',
                  'radname'  : radname,
                  'radfname' : './%s'%radfile,
                  'listfil'  : ['./%s'%radlist],
                  'rad98opt' : radar98,
                  'ref_time' : reftime,
                  'runname'  : runname,
                  'nproc_x'  :  mpiconfig.nproc_x,
                  'nproc_y'  :  mpiconfig.nproc_y,
                  'max_fopen'   : mpiconfig.nproc_x*mpiconfig.nproc_y,
                  'nprocx_in': wrfstat[1],
                  'nprocy_in': wrfstat[2]
                 }

        nmlgrp.merge(nmlin)
        nmlgrp.writeToFile(nmlfile)

        ##------------ Wait for backgrond file ready -----------------

        li = bkgfile.rsplit('wrfout_d')
        bkgready = 'wrfoutReady_d'.join(li)   # if bkgfile contain "wrfout_d", it will
        # be replaced with "wrfoutReady_d". Otherwise, bkgready is the same as bkgfile

        if not self.command.wait_for_a_file('run_radremap',bkgready,600,20,5,skipread=True):
            self.command.addlog(-1,"run_radremap",f"{bkgready} not ready in 10 minutes.")


        ##------------ run the program from command line -----------------
        outfile = '%s.output' % runname
        jobid = self.command.run_a_program(executable,nmlfile,outfile,wrkdir,mpiconfig,inarg='-input')

        ##-------------- wait for it to be done -------------------
        if not self.command.wait_job('radremap_%s'%radname,jobid,wait) :
          #raise SystemExit()
          pass

        if self.mutex.acquire() :
            self.procradars += 1
            self.mutex.release()

    #enddef run_radremap

    ##========================== news3dvar =============================

    def run_news3dvar(self,obsfiles,bkgfiles,wrfstat,wait) :
      '''
        Run or submit news3dvar job
      '''

      executable = os.path.join(self.runDirs.vardir,'bin','news3dvar')
      tmplinput  = self.runcase.getNamelistTemplate('news3dvar')

      mpiconfig = self.runConfig['news3dvar']
      if mpiconfig.mpi : executable += '_mpi'

      ##---------------- Check files and directory -----------------------

      wrkbas,wrkdirs = self.runcase.getwrkdir(self.caseDir,'news3dvar',False,mpiconfig.numens)
      nmlfiles = []
      nmlfile = 'news3dvar.input'
      for wrkdir in wrkdirs:
        if not os.path.lexists(wrkdir) :
          os.mkdir(wrkdir)

        calpath = os.path.join(wrkdir,'calib')
        if not os.path.lexists(calpath) :
          calbas = os.path.join(self.runDirs.inputdir,'calib')
          self.command.copyfile(calbas,calpath,hardcopy=False)

        nmlfiles.append(os.path.join(wrkdir,nmlfile))

      timstr   = self.runcase.startTime.strftime('%Y%m%d%H%M' )

      ##---------------- get event date ------------------------------

      if self.runcase.startTime.hour < self.runcase.wofstarthour:
         eventdt = self.runcase.startTime - timedelta(days=1)
      else:
         eventdt = self.runcase.startTime

      eventdate = eventdt.strftime('%Y%m%d')

      ##---------------- Waiting for DART files -------------------------

      if mpiconfig.numens is not None:

        cov_factor = self.runcase.hybrid
        bkgdir = re.sub(r"_\d{1,3}$","",os.path.dirname(bkgfiles[0]))
        ensdirname = '%s_%%0N'%bkgdir
        nensemble  = mpiconfig.numens

        if self.domain.cycle_num > 0:
          ensfileopt = 2        # WRF output file
        else:
          ensfileopt = 1        # WRF input file

      elif self.runcase.hybrid > 0:

          failfl = os.path.join(os.path.dirname(os.path.dirname(self.caseDir)),
                                '%s_NEWSe.fail'%eventdate)
          time_wrf = self.runcase.startTime.strftime('%Y-%m-%d_%H:%M:%S' )

          if time_wrf == f"{eventdt:%Y-%m-%d}_{self.runcase.wofstarthour:02d}:00:00":
              inittime = True
              ensdirname = 'ic%0N'   # use in news3dvar.input
              ensfileopt = 3
              waittime   = 7200      # initial time wait longer for files
              waitready  = 600
          else:
              inittime = False
              subdirt  = (self.runcase.startTime-timedelta(minutes=15)).strftime('%Y%m%d%H%M')
              ensfileopt = 4
              ensdirname = subdirt
              waittime   = 3600
              waitready  = 300


          failwof = False
          if not os.path.lexists(failfl):
            nen = 36
            flagdir = os.path.join(self.runDirs['dartdir'],eventdate)
            afiles = []
            for n in range(1,nen+1):
                if inittime:
                    afile = os.path.join(flagdir,f'ic{n:d}','wrfinput_d01_ic')
                    afiles.append(afile)
                else:
                    afile = os.path.join(flagdir,subdirt,f'wrffcst_d01_{time_wrf}_{n:d}')
                    afiles.append(afile)

            self.command.addlog(0,"cntl.3dvar",'Looking for ensemble files in %s ...'%os.path.dirname(afiles[0]))
            #self.command.addlog(0,"cntl.3dvar",'%s'%('\n'.join([os.path.basename(afile) for afile in afiles])) )
            if self.command.wait_for_files('run_news3dvar',afiles,waittime,waitready,5,skipread=False) < len(afiles):
                failwof = True
                self.command.addlog(1,"cntl.3dvar",'Generating <%s> ....'%failfl)
                with open(failfl, 'w') as f:
                    f.write(f"First for {time_wrf} at {datetime.now():%Y-%m-%d.%H:%M:%S}." )
          else:
            self.command.addlog(1,"cntl.3dvar",'Found  <%s> ....'%failfl)
            failwof = True

          ensdirname = os.path.join(self.runDirs['dartdir'],eventdate,ensdirname)
          nensemble  = 36

          cov_factor = self.runcase.hybrid
          if failwof:
            cov_factor = 0.0
      else:
        cov_factor = 0
        ensdirname = 'None'
        nensemble  = 1
        ensfileopt = 1        # Will not use, can be anything

      ##---------------- Set observation files  ---------------------

      nradfile  = 0; radfnames = []
      nsngfile  = 0; sngfname  = []; sngtmchk = [];
      nconvfil  = 0; convfname = []
      nuafile   = 0; uafnames  = []
      ncwpfile  = 0; cwpfname  = []
      nlgtfile1 = 0; lgtfname  = []
      nlgtfile2 = 0;

      raddir = os.path.join(self.caseDir,'radremap')

      for (obskey,files) in obsfiles.items() :
         if obskey == 'radar' :
           datstr   = self.runcase.startTime.strftime('%Y%m%d.%H%M')
           for radname in files:
             radfname = os.path.join(raddir,'%s.%s.nc' % (radname,datstr))
             if os.path.lexists(radfname): radfnames.append(radfname)
           nradfile = len(radfnames)
           self.command.addlog(0,"cntl.3dvar",'Analysis will use %d radar data files.'%nradfile)
         elif obskey == 'ua' :
           nuafile = len(files)
           uafnames = files
         elif obskey == 'sng' :
           nsngfile = len(files)
           for filelst in files :
             sngfname.append(filelst[0])
             sngtmchk.append(filelst[1])
         elif obskey == 'conv' :
           nconvfil = len(files)
           for filelst in files:
             convfname.append(filelst)
         elif obskey == 'cwp':
           ncwpfile = len(files)
           cwpfname = files
         elif obskey == 'lightning1':
           nlgtfile1 = len(files)
           if nlgtfile1 > 0 :
              lgtfname += files
         elif obskey == 'lightning2':
           nlgtfile2 = len(files)
           if nlgtfile2 > 0 :
              lgtfname += files

      ##---------------- VAD checking  ---------------------
      if '88vad' in self.obstypes and 'radar' in obsfiles:
          datstr   = self.runcase.startTime.strftime('%y%m%d.%H%M')
          vadfiles = []
          for radname in obsfiles['radar']:
            vadfile = os.path.join(raddir,'%s.%s.vad'%(radname,datstr))
            if os.path.lexists(vadfile): vadfiles.append(vadfile)

          uafnames += vadfiles
          nuafile += len(vadfiles)
          self.command.addlog(0,"cntl.3dvar",'Analysis will use %d VAD data files.'%len(vadfiles))

      skip3dvar = False
      if nradfile+nuafile+nsngfile+nconvfil+ncwpfile+nlgtfile1+nlgtfile2 == 0 :
          self.command.addlog(1,"cntl.3dvar",'No observation was found. news3dvar skipped.')
          skip3dvar = True
          #return None

      if nradfile <= 0 : radfnames = ['xxxx']
      if nuafile  <= 0 : uafnames  = ['xxxx']
      if ncwpfile <= 0 : cwpfname  = ['xxxx']
      if nsngfile <= 0 :
         sngfname = ['xxxx']
         sngtmchk = ['none']
      if nconvfil <= 0 :
         convfname = ['xxxx']
      nlgtfile = nlgtfile1+nlgtfile2
      if nlgtfile <= 0 : lgtfname = ['xxxx']

      ##---------------- make namelist file ------------------------------

      #if 'unipost' in self.programs:
      #  hdmpfmt = 1
      #elif 'nclplt' in self.programs:
      #  hdmpfmt = 100
      #else:
      #  hdmpfmt = 0

      if self.runcase.startTime.hour < 10:
        ncwpfile = 0

      nmlgrp = namelist.decode_namelist_file(tmplinput)

      if nmlgrp["obs_ref"].refsrc == 1:
          obsconf   = self.runcase.getObsConfig('mrms')
          mrmsfiles = self.check_wait_mrms(self.runcase.startTime,obsconf,wrkdirs[0])
          #assert(len(mrmsfiles) == 33)
      else:
          mrmsfiles = ['xxxx']

      nmlin  = {'initime'     : self.runcase.startTime.strftime('%Y-%m-%d.%H:%M:00')
               ,'modelopt'    : 2
               ,'inigbf'      : 'xxxxxx'
               #,'hdmpfmt'     : hdmpfmt
               ,'runname'     : '%s_%s' % (self.runcase.runname,timstr)
               ,'refile'      : mrmsfiles[0]
               ,'nradfil'     : nradfile
               ,'radfname'    : radfnames
               ,'nsngfil'     : nsngfile
               ,'sngfname'    : sngfname
               ,'sngtmchk'    : sngtmchk
               ,'nconvfil'    : nconvfil
               ,'convfname'   : convfname
               ,'nuafil'      : nuafile
               ,'uafname'     : uafnames
               ,'ncwpfil'     : ncwpfile
               ,'cwpfname'    : cwpfname
               ,'nlgtfil'     : nlgtfile
               ,'lightning_files': lgtfname
               ,'dirname'     : './'
               ,'nproc_x'     : mpiconfig.nproc_x
               ,'nproc_y'     : mpiconfig.nproc_y
               ,'max_fopen'   : mpiconfig.nproc_x*mpiconfig.nproc_y
               ,'ensdirname'  : ensdirname
               ,'ensfileopt'  : ensfileopt
               ,'nensmbl'     : nensemble
               ,'ntimesample' : self.runcase.ntimesample
               ,'stimesample' : self.runcase.timesamin*60
               ,'cov_factor'  : cov_factor
               ,'icycle'      : self.domain.cycle_num
               }

      ##
      ## Trim run-time namelist with npass
      ##
      npass = nmlgrp['adas_const'].npass

      ###################################################################
      ##
      ## Given values in namelist.py string format and get the real values
      ## in Fortran format.
      ##
      ###################################################################

      def getvaluelst(values,vartype) :
          retlst = []
          prep0  = False
          for val in values :
            if type(val) is list :
              retlst.append(getvaluelst(val,vartype))
            else :
              if vartype == 'int0' :
                retlst.append(int(val))
                prep0 = True
              else :
                retlst.append(vartype(val))
          if prep0 :
            retlst.insert(0,0)

          return retlst
      #enddef getvaluelst

      ###################################################################

      vardpass = {'adas_radar'  : { 'iuserad' : 'int0' }
                 ,'adas_sng'    : { 'iusesng' : 'int0' }
                 ,'adas_ua'     : { 'iuseua'  : 'int0' }
                 ,'adas_cwp'    : { 'iusecwp' : 'int0' }
                 ,'adas_tpw'    : { 'iusetpw' : 'int0' }
                 ,'var_const'   : { 'maxin'           : int
                                   ,'vrob_opt'        : int
                                   ,'cldfirst_opt'    : int
                                   ,'qobsrad_bdyzone' : int }
                 ,'var_refil'   : { 'ipass_filt' : int
                                   ,'hradius'    : float
                                   ,'vradius'    : float
                                   ,'vradius_opt' : int
                                  }
                 ,'var_diverge' : { 'wgt_div_h'  : float
                                   ,'wgt_div_v'  : float
                                  }
                 ,'var_smth'    : { 'wgt_smth'   : float }
                 ,'var_thermo'  : { 'wgt_thermo' : float }
                 ,'var_mslp'    : { 'mslp_err' : float }
                 ,'var_consdiv' : { 'wgt_dpec' : float,
                                    'wgt_hbal' : float
                                  }
                 ,'var_ens'     : { 'vradius_opt_ens' : int,
                                    'hradius_ens'     : float,
                                    'vradius_ens'     : float
                                  }
                 ,'var_reflec'  : { 'ref_opt' : int,
                                    'wgt_ref' : float,
                                    'hradius_ref'     : float,
                                    'vradius_opt_ref' : int,
                                    'vradius_ref'     : float
                                  }
                 ,'var_lightning' : { 'iuselgt' : int
                                     }
                 ,'var_aeri'      : { 'iuseaeri' : 'int0'
                                     }
                }

      for nmlblk,nmlvars in vardpass.items() :
        for nmlvar,nmltype in nmlvars.items() :
          nmlvalues = nmlgrp[nmlblk][nmlvar]
          if len(nmlvalues) > npass :     ## Check sizes
            del nmlvalues[npass:]
          elif len(nmlvalues) < npass :
            self.command.addlog(-1,"cntl.3dvar",'ERROR: No enough values for variable <%s>.' % nmlvar)

          nmlin[nmlvar] = getvaluelst(nmlvalues,nmltype)

      ##nmlin.update(useobs)

      nmlgrp.merge(nmlin)
      iens = 0
      for absnmlfile in nmlfiles:
        nmlgrp["initialization"].inifile = bkgfiles[iens]
        nmlgrp["var_ens"].iensmbl = iens
        #if iens < 1: nmlgrp["var_ens"].ntimesample = 1    # control member always uses 1
        #else:        nmlgrp["var_ens"].ntimesample = self.runcase.ntimesample
        nmlgrp.writeToFile(absnmlfile)
        iens += 1

      ##-------------- run the program from command line ----------------
      if self.check_job('news3dvar') and not skip3dvar:
          hhmmstr = self.runcase.startTime.strftime('%H%M')
          outfile = 'n3dvar%d_%s.output'%(self.runcase.caseNo,hhmmstr)
          jobid = self.command.run_a_program(executable,nmlfile,
                                                outfile,wrkbas,mpiconfig)

      ##-------------- wait for it to be done -------------------
          if not self.command.wait_job('news3dvar',jobid,wait) :
              self.command.addlog(-1,"3DVAR",f'job failed: {jobid}' )
              #raise SystemExit()

      anafiles = []
      for iens in range(0,len(wrkdirs)):
        anafile = os.path.join(wrkdirs[iens],os.path.basename(bkgfiles[iens]))
        anafiles.append(anafile)

        if skip3dvar:
          if anafile.rfind('wrfout_d') > 0:
            li = anafile.rsplit('wrfout_d')
            anaready = 'wrfoutReady_d'.join(li)   # if afile contain "wrfout_d", it will
          else:
            li = anafile.rsplit('wrfinput_d')
            anaready = 'wrfinputReady_d'.join(li)   # if afile contain "wrfout_d", it will
          self.command.copyfile(bkgfiles[iens],anafile,False)
          self.command.copyfile(bkgfiles[iens],anaready,False)

      return anafiles
    #enddef run_news3dvar

    ##========================== NCLPLT ================================

    def run_nclplt(self,afiles,atime,wait,bfile=None,pltfields=None) :

      executable = os.path.join(self.command.fetchNCARGRoot(),'bin','ncl')
      jconf = self.runConfig['nclplt']

      ##---------------- make working directory -------------------------

      wrkbas,wrkdirs = self.runcase.getwrkdir(self.caseDir,'nclscripts',numens=jconf.numens)
      #os.path.join(self.caseDir,'nclscripts%d'%(self.runcase.caseNo-1))
      iens = 0
      for wrkdir in wrkdirs:
        if not os.path.lexists(wrkdir) :
          os.mkdir(wrkdir)

        afile = afiles[iens]
        li = afile.rsplit('wrfout_d')
        flready = 'wrfoutReady_d'.join(li)   # if afile contain "wrfout_d", it will
            # be replaced with "wrfoutReady_d". Otherwise, flready is the same as afile

        self.command.wait_for_a_file('run_nclplt',flready,3600,1800,10,skipread=True)

        #self.command.wait_for_a_file('run_nclplt',afiles[iens],60,300,5,skipread=True)
        iens += 1

      fminu = int ( (atime - self.runcase.startTime).total_seconds() )//60

      #outdir = os.path.join(wrkdir,'out%03d' % fminu)
      #if not os.path.lexists(outdir) :
      #  os.mkdir(outdir)


      ##---------------- Plot each field --------------------------------

      #LOCTime    = atime + timedelta(hours=-6.0)
      #LOCtimestr = LOCTime.strftime('%Y-%m-%d_%H:%M:%S')
      LOCtimestr = atime.strftime('%Y-%m-%d_%H:%M:%S')

      ncargroot = self.command.fetchNCARGRoot()

      if pltfields is None:
          pltfields = self.runcase.fields

      for field in pltfields:
        fmatch = re.match('agl([hDZv])([0-9\.]{3,4})km',field)
        if fmatch:
          ncltmpl = os.path.join(self.runDirs.inputdir,'nclscripts',
                                 'plt_agl%s3km.ncl' %fmatch.group(1) )
          hghtln  = "heights = (/ %f /)" % float(fmatch.group(2))
          fieldshort = "%s%s" %(fmatch.group(1),fmatch.group(2)[:1] )
        else:
          ncltmpl = os.path.join(self.runDirs.inputdir,'nclscripts','plt_%s.ncl'%field)
          hghtln  = " "
          fieldshort = field[:2]
        timeln = 'timescst = "%s"' % LOCtimestr
        hhmmstr = self.runcase.startTime.strftime('%H%M')

        if bfile is not None:
            bfileline = 'b = addfile("%s.nc","r")' % bfile
        else:
            bfileline = ' '

        if fminu > 0:
          forastr = '''
             jobtype    = "Forecast"
             dbzvarn    = "dbzp"
             smooth_opt = 3
          '''
        else:
          forastr = '''
             jobtype    = "Analysis"
             dbzvarn    = "dbz"
             smooth_opt = 0
          '''

        nclscpt = 'p%d%s_%s-%03d.ncl'%(self.runcase.caseNo,fieldshort,hhmmstr,fminu)
        iens = 0
        for wrkdir in wrkdirs:
          nclfile = os.path.join(wrkdir,nclscpt)
          nclhndl = open(nclfile,'w')
          nclstr  = '''
             load "%(NCARG_ROOT)s/lib/ncarg/nclscripts/csm/gsn_code.ncl"
             ;load "%(NCARG_ROOT)s/lib/ncarg/nclscripts/wrf/WRFUserARW.ncl"
             load "%(CUMTmpl)s/nclscripts/WRFUserARW.ncl"

             ;external DIV "%(CUMTmpl)s/nclscripts/wrfdivergence.so"
             ;external VOR "%(CUMTmpl)s/nclscripts/wrfvorticity.so"

             begin
               a = addfile("%(wrffile)s.nc","r")
               %(bfileline)s
               wks_type = "%(outtype)s"
               wks_type@wkWidth  = 1224
               wks_type@wkHeight = 1584
               wks = gsn_open_wks(wks_type,"%(field)s")

               %(timeline)s
               %(heightline)s
               %(forastr)s
          ''' % { 'wrffile' : afiles[iens], 'bfileline' : bfileline,
                  'outtype' : 'png',  'forastr'  : forastr,
                  'field'   : '%s-%03d'%(field,fminu), 'heightline' : hghtln, 'timeline': timeln,
                  'NCARG_ROOT' : ncargroot,
                  'CUMTmpl'   : self.runDirs.inputdir
                }

          nclhndl.write(self.command.trim(nclstr))
          with open(ncltmpl,'r') as ncltmph:
            for line in ncltmph :
              if line.startswith(';;;'): continue
              ##line = line.replace('CUMTmpl',self.runDirs.inputdir)
              nclhndl.write(line)

          ##
          ## Attach special map outlines as needed
          ##
          ##flag = 0
          ##if not field.endswith('ew') and not field.endswith('ns'):
          ##  ## Check NCARG version
          ##  ncargver = subprocess.check_output("%s/bin/ncargversion"%ncargroot,
          ##             stdin=None,stderr=subprocess.STDOUT,shell=None,cwd=wrkdir)
          ##  vermatch = re.search(r'Version (\d).(\d).\d',ncargver)
          ##  if vermatch:
          ##    majorver = int(vermatch.group(1))
          ##    minorver = int(vermatch.group(2))
          ##    ##print majorver, minorver
          ##    if majorver >= 6:
          ##       if minorver >= 1: flag = 2
          ##       else:             flag = 1
          ##
          ##if flag > 0:
          ##  attachlines = """     cityplot = attach_citys(wks,plot,"%s/somewhere_shp",%d)
          ##
          ##                """ % (self.runDirs.inputdir,flag)
          ##  nclhndl.write(attachlines)

          nclhndl.write(
                self.command.trim(""";--- draw the map and the shapefile outlines ---
                                               draw(plot)
                                               frame(wks)
                                             end do
                                          end
                                   """)
                        )

          nclhndl.close()

          iens += 1

        ## Now execute the ncl script
        jobid = self.command.run_ncl_plt(executable,nclscpt,field,
                              wrkbas,fminu,self.runDirs.inputdir,jconf,
                              transfer=False )

        if not self.command.wait_job('nclplt',jobid,wait) :
           self.command.addlog(-1,"NCL",f'job failed: {jobid}' )
           #raise SystemExit()

    #enddef run_nclplt

    ##========================== UNIPOST ===============================

    def run_unipost(self,afile,wrfmin,wait) :

      executable = os.path.join(self.runDirs.uppdir,'bin','unipost.exe')
      tmplinput  = self.runcase.getNamelistTemplate('unipost')

      jconf = self.runConfig['unipost']

      ##---------------- make working directory -------------------------

      wrkbas,wrkupdir = self.runcase.getwrkdir(self.caseDir,'postprd')
      #os.path.join(self.caseDir,'postprd%d'%(self.runcase.caseNo-1))
      if not os.path.lexists(wrkupdir) :
        os.mkdir(wrkupdir)

      wrkdir = os.path.join(wrkupdir,'fcst-%03d'%wrfmin)
      if not os.path.lexists(wrkdir) :
        os.mkdir(wrkdir)

      timestr = self.runcase.startTime.strftime('%Y-%m-%d_%H:%M:%S')

      ##---------------- Link working files ----------------------------

      srcfiles = [os.path.join(self.runDirs.wrfdir,"run","ETAMPNEW_DATA.expanded_rain"),
                  os.path.join(self.runDirs.wrfdir,"run","ETAMPNEW_DATA"),
                  os.path.join(self.runDirs.uppdir,"src","lib","g2tmpl","params_grib2_tbl_new"),
                  os.path.join(self.runDirs.uppdir,"parm","post_avblflds.xml"),
                  os.path.join(self.runDirs.uppdir,"parm","postcntrl.xml")
                  ]
      runfiles = ["hires_micro_lookup.dat","nam_micro_lookup.dat","params_grib2_tbl_new",
                  "post_avblflds.xml","postcntrl.xml"
                  ]

      for i in range(0,len(runfiles)):
        relfl = os.path.join(wrkdir,runfiles[i])
        if not os.path.lexists(relfl) :
          self.command.copyfile(srcfiles[i],relfl)

      ##--------------- make namelist file ------------------------------

      prdcntl = os.path.join(wrkdir,'fort.14')
      if os.path.lexists(prdcntl):
         os.unlink(prdcntl)

      self.command.copyfile(tmplinput,prdcntl,hardcopy=True)

      stdnml = os.path.join(wrkdir,'itag')

      nmlfile = open(stdnml,'w')
      nmlstr  = '''
                %(wrffile)s
                netcdf
                %(ndate)s
                NCAR
                ''' % { 'wrffile' : afile,
                        'ndate'   : timestr
                       }

      nmlfile.write(self.command.trim(nmlstr))
      nmlfile.close()

      ## Now execute the unipost script
      outfile = os.path.join(wrkdir,'unipost.output')
      jobid = self.command.run_unipost(executable,'',outfile,
                wrkdir,os.path.join(self.runDirs.uppdir,'bin'),
                timestr,wrfmin,jconf)

      if not self.command.wait_job('unipost',jobid,wait) :
         self.command.addlog(-1,"UPP",f'job failed: {jobid}' )
         #raise SystemExit()

    #enddef run_unipost

    ##========================== WRFEXTSND =============================

    def run_wrfextsnd(self,afile,wait) :

      executable = os.path.join(self.runDirs.vardir,'bin','wrfextsnd')
      tmplinput  = self.runcase.getNamelistTemplate('wrfextsnd')

      jconf = self.runConfig['wrfextsnd']

      ##---------------- make working directory -------------------------

      wrkdir = os.path.join(self.caseDir,'extsnd')
      if not os.path.lexists(wrkdir) :
        os.mkdir(wrkdir)

      timestr = self.runcase.startTime.strftime('%Y-%m-%d_%H:%M:%S')

      ##--------------- make namelist file ------------------------------

      nmlfile  = os.path.join(wrkdir,'wrfextsnd.input')

      nmlgrp = namelist.decode_namelist_file(tmplinput)
      nmlin  = {'dir_extd'          : os.path.dirname(afile)
               ,'init_time_str'     : timestr
               ,'io_form'           : 7
               ,'grid_id'           : 2
               ,'start_time_str'    : timestr
               ,'history_interval'  : '00_01:00:00'
               ,'end_time_str'      : timestr
               ,'locopt'            : 1
               ,'nsnd'              : 1
               ,'stid'              : [  'GUNKNOWN' ]
               ,'istnm'             : [    83538 ]
               ,'slat'              : [    22.69 ]
               ,'slon'              : [   114.34 ]
               ,'selev'             : [      0.0 ]
               ,'dir_output'        : wrkdir
               ,'outsnd'            : 2
               ,'outsfc'            : 1
               ,'nproc_x'           : jconf.nproc_x
               ,'nproc_y'           : jconf.nproc_y
               ,'max_fopen'         : jconf.nproc_x*jconf.nproc_y
               }

      nmlgrp.merge(nmlin)
      nmlgrp.writeToFile(nmlfile)

      ##-------------- run the program from command line ----------------

      outfile = 'wrfextsnd.output'
      jobid = self.command.run_a_program(executable,nmlfile,
                                                outfile,wrkdir,jconf)

      ##-------------- wait for it to be done -------------------
      if not self.command.wait_job('wrfextsnd',jobid,wait) :
          self.command.addlog(-1,"SND",f'job failed: {jobid}' )
          #raise SystemExit()

    #enddef run_wrfextsnd

    ##========================== WRFHYBRID =============================

    def run_wrfhybrid(self,runmode,wait) :

      executable = os.path.join(self.runDirs.vardir,'bin','wrfhybrid')
      tmplinput  = self.runcase.getNamelistTemplate('wrfhybrid')

      jconf = self.runConfig['wrfhybrid']
      if jconf.mpi : executable += '_mpi'

      ##---------------- make working directory -------------------------

      wrkdir = os.path.join(self.caseDir,'wrfhybrid%d'%runmode)
      if not os.path.lexists(wrkdir) :
        os.mkdir(wrkdir)

      if runmode == 0:
          outdir = './'
          outbkg = '.TRUE.'
          outrct = '.FALSE.'
          outnew = '.FALSE.'
      else:
          outdir = './dart_mem%5N/'
          outbkg = '.FALSE.'
          outrct = '.FALSE.'
          outnew = '.FALSE.'   # to be .FALSE.

      timestr = self.runcase.startTime.strftime('%Y-%m-%d_%H:%M:%S')
      datestr = self.runcase.startTime.strftime('%Y%m%d')

      ##---------------- Waiting for DART files -------------------------
      nen = 36
      flagdir = os.path.join(self.runDirs['dartdir'],'flags')
      if runmode == 0:
          afiles = []
          for n in range(1,nen+1):
              afile = os.path.join(flagdir,'wrf_done%d'%n)
              afiles.append(afile)

          if self.command.wait_for_files('run_wrfhybrid',afiles,1800,300,5,skipread=True) < len(afiles):
              self.command.addlog(-1,"WRFHYBRID","Time out waiting for wrf_done.")

      else:
          flagfile = os.path.join(flagdir,'filter.done')
          self.command.wait_for_a_file('run_wrfhybrid',flagfile,3600,300,5,skipread=True)

      ##--------------- make namelist file ------------------------------

      nmlfile  = os.path.join(wrkdir,'wrfhybrid.input')

      nmlgrp = namelist.decode_namelist_file(tmplinput)
      nmlin  = { 'program_mode' : runmode,
                 'alpha'        : 0.5,
                 'nen'          : nen,
                 'dir_extm'     : os.path.join(self.runDirs['dartdir'],'advance_temp%0N'),
                 'filename_convention_m'  : 2,
                 'init_time_str_m'        : timestr,

                 'start_time_str_m'       : timestr,
                 'end_time_str_m'         : timestr,
                 'outdir'            : outdir,

                 'dir_extd'          : '../news3dvar',

                 'out_bkg_mean'      : outbkg,
                 'out_rct_mean'      : outrct,
                 'out_memb_new'      : outnew
               }

      nmlgrp.merge(nmlin)
      nmlgrp.writeToFile(nmlfile)

      ##-------------- run the program from command line ----------------

      outfile = 'wrfhybrid.output'
      jobid = self.command.run_a_program(executable,nmlfile,
                                                outfile,wrkdir,jconf)

      ##-------------- wait for it to be done -------------------
      if not self.command.wait_job('wrfhybrid',jobid,wait) :
          self.command.addlog(-1,"HYBRID",f'job failed: {jobid}' )
          #raise SystemExit()

      if runmode == 0:
          retfile = 'wrfinput_d01'
          bfile = os.path.join(flagdir,'NEWSVAR.run')
          with open(bfile,'w') as f: f.write('NEWSVAR is running\n')
      else:
          retfile = 'dart_memamean/wrfinput_d01'
          bfile = os.path.join(flagdir,'NEWSVAR.done')
          with open(bfile,'w') as f: f.write('NEWSVAR is done\n')

      return os.path.join(wrkdir,retfile)
    #enddef run_wrfhybrid

    ##========================  run_wrfdaupdate ========================

    def run_wrfdaupdate(self,use_base,wait,memid=0) :
         '''
           Run WRFDAUPDATE to prepare for a WRF forecast
           use_base : use background from earlier base run
         '''

         executable = os.path.join(self.runDirs.vardir,'bin','wrfdaupdate')
         tmplinput  = self.runcase.getNamelistTemplate('wrfdaupdate')

         mpiconfig = self.runConfig.wrfdaupdate

         wrkbas,wrkdirs = self.runcase.getwrkdir(self.caseDir,'wrf',numens=mpiconfig.numens)
         # background directory
         nothing,tmpldirs = self.runcase.getwrkdir(self.caseDir,'real',numens=self.runConfig.real.numens)
         # var directory
         nothing,dadirs = self.runcase.getwrkdir(self.caseDir,'news3dvar',False,self.runcase.nens)

         nmlfiles  = []

         da_files  = []
         bkgfiles  = []
         tmplwbdys = []
         tmplwinps = []

         nmlfile = 'wrfdaupdate.input'
         iens = 0
         for wrkdir in wrkdirs:
           if not os.path.lexists(wrkdir) :
             os.mkdir(wrkdir)

           nmlfiles.append(os.path.join(wrkdir,nmlfile ))

         ##-------------- Waiting for pre-requisites -------------------

         #wrffile = 'wrfout_d01_%s' % self.runcase.startTime.strftime('%Y-%m-%d_%H:%M:%S')

           if self.runConfig.real.numens is None:
             realdirs = ['dom20/real4','dom20/real5','dom00/real1']
           else:
             realdirs = ['dom20/real4_%d'%iens,'dom20/real5_%d'%iens,
                         'dom00/real1_%d'%iens,'dom10/real2_%d'%iens]

           #----
           # decode bkgfile from var namelist, and set bkgfiles
           #----
           dadir   = dadirs[iens]
           danml = os.path.join(dadir,'news3dvar.input' )

           self.command.addlog(0,"cntl.daup",f"Waiting for {danml} ...")
           if not self.command.wait_for_a_file('cntl.daup',danml,7200,600,5):
              self.command.addlog(-1,"cntl.daup",f"Cannot find {danml} after {7200/3600} hours")
           nmlgrp = namelist.decode_namelist_file(danml)
           bkgfile = nmlgrp['initialization'].inifile
           bkgfiles.append(bkgfile)

           hdmpfmt = nmlgrp['history_dump'].hdmpfmt

           #----
           # wait var to finish, and set da_files
           #----
           dafile = os.path.join( dadir,os.path.basename(bkgfile) )
           if bkgfile.rfind('wrfout_d') >= 0:
               flsep = 'wrfout_d'
               flglue = 'wrfoutReady_d'
           else:
               flsep = 'wrfinput_d'
               flglue = 'wrfinputReady_d'
           daready = flglue.join( dafile.rsplit(flsep) )
           self.command.addlog(0,"cntl.daup",f"Waiting for {daready} ...")
           if not self.command.wait_for_a_file('cntl.daup',daready,3600,600,5,skipread=True):
               self.command.addlog(-1,"cntl.daup",f"Cannot find {daready} after {3600/60} minutes")
           da_files.append(dafile)

           #----
           # check and wait real to finish, and set tmplwinps & tmplwbdys
           #----
           if use_base:
               maxwaittime = 7200
               waittime = 0
               foundtmpl = False
               while waittime < maxwaittime:
                 for realdir in realdirs:
                   tmpldir = os.path.join(self.domain['cycle_base'],realdir)
                   self.command.addlog(0,'cntl.daup','Checking template directory <%s>.' % (tmpldir))
                   if os.path.lexists(tmpldir):
                     foundtmpl = True
                     break

                 if foundtmpl:
                   self.command.addlog(0,'cntl.daup','Use WRF file in directory <%s> for templates.' % (tmpldir))
                   break
                 else:
                   waittime += 10
                   time.sleep(10)

               if not os.path.lexists(tmpldir):
                 self.command.addlog(-1,'cntl.daup','template directory <%s> do not exist.' % (tmpldir))
           else:
              tmpldir = tmpldirs[iens]

           ## wait for real to be done
           #self.command.addlog(0,"cntl.daup",f"Checking for <rsl.error.0000> in tmpldir ...")
           #maxwaitime = 30*60
           #waitime = 0
           #realdone = False
           #while not realdone and waitime < maxwaitime :
           #  filename = os.path.join(tmpldir,'rsl.error.0000')
           #  if os.path.lexists(filename) :
           #    fh = open(filename,'r')
           #    for line in fh:
           #      if line.find('real_em: SUCCESS COMPLETE REAL_EM INIT') >= 0 :
           #        realdone = True
           #        break
           #  time.sleep(10)
           #  waitime += 10

           tmplwinp  = os.path.join(tmpldir,'wrfinput_d01')
           tmplwbdy  = os.path.join(tmpldir,'wrfbdy_d01')

           self.command.addlog(0,"cntl.daup",f"Waiting for {tmplwbdy} ...")
           if not self.command.wait_for_a_file('run_wrfdaudpate',tmplwbdy,600,300,5):
               self.command.addlog(-1,"cntl.daup",f"Cannot find {tmplwbdy} after {600/60} minutes")

           tmplwinps.append(tmplwinp)
           tmplwbdys.append(tmplwbdy)

           iens += 1

         ##---------------- Check skip Conditions ------------------------
         skipdaupdate = False
         if self.runcase.skipwrfdaupdate:  # try to skip wrfdaupdate as possible
           if hdmpfmt == 1:  skipdaupdate = True

         ##---------------- make namelist file ------------------------

         nextime = self.runcase.startTime+timedelta(seconds=self.runcase.fcstSeconds)
         endtstr = nextime.strftime('%Y-%m-%d_%H:%M:%S')

         nmlgrp = namelist.decode_namelist_file(tmplinput)
         perturb_ic  = nmlgrp['perturb'].perturb_ic
         perturb_lbc = nmlgrp['perturb'].perturb_lbc

         nmlin = {     'end_time_str' : endtstr,

                       'update_lateral_bdy'      : '.true.',
                       'update_input'            : '.true.',
                       'update_input_copy_td'    : '.true.',

                       'define_bdy'   : '.TRUE.',
                       'copy_input'   : '.TRUE.',

                       'outdir'       : './'
                 }

         nmlgrp.merge(nmlin)
         iens = 0
         for absnmlfile in nmlfiles:
           nmlgrp['input'].wrf_bdy_in   = tmplwbdys[iens]
           nmlgrp['input'].wrf_input_in = tmplwinps[iens]
           nmlgrp['input'].da_file      = da_files[iens]
           nmlgrp['input'].wrf_td_in    = bkgfiles[iens]

           if iens == 0:  # To prevent control member from perturbing
             nmlgrp['perturb'].perturb_ic  = 0
             nmlgrp['perturb'].perturb_lbc = 0
           else:          # recover the value from namelist template
             nmlgrp['perturb'].perturb_ic  = perturb_ic
             nmlgrp['perturb'].perturb_lbc = perturb_lbc

           nmlgrp.writeToFile(absnmlfile)
           if skipdaupdate:
               inputfile = os.path.join(wrkdirs[iens],'wrfinput_d01')
               bdyfile   = os.path.join(wrkdirs[iens],'wrfbdy_d01')
               self.command.copyfile(da_files[iens], inputfile,hardcopy=False)
               self.command.copyfile(tmplwbdys[iens],bdyfile,  hardcopy=False)
           iens +=1

         ##-------------- run the program --------------------------
         if not skipdaupdate:
             hhmmstr = self.runcase.startTime.strftime('%H%M')
             outfile = 'daup%d_%s.output'%(self.runcase.caseNo,hhmmstr)

             jobid = self.command.run_a_program(executable,nmlfile,outfile,wrkbas,mpiconfig)

         ##-------------- wait for it to be done -------------------
             if not self.command.wait_job('daupdate%02d'%self.domain.id,jobid,wait) :
                 self.command.addlog(-1,"DAUP",f'job failed: {jobid}' )
                 #raise SystemExit()

    #enddef run_wrfdaupdate

    ##%%%%%%%%%%%%%%%%%%%%%%  link extm  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    def check_link_extm(self,extdnames,tile=None) :
      ''' Link external data set'''

      # whether we want to do the actual work
      dowrk = self.domain.isroot and not self.command.showonly

      nowaitfiles = not self.check_job('ungrib')

      mpiconfig = self.runConfig.ungrib
      nothing,extmdirs = self.runcase.getwrkdir(self.caseDir,'ungrib',numens=mpiconfig.numens)
      #os.path.join(os.path.dirname(self.caseDir),'ungrib%d'%(self.runcase.caseNo-1))
      if dowrk:
          for extmdir in extmdirs:
              if not os.path.lexists(extmdir) :
                  os.mkdir(extmdir)

      appext = None
      sttime = time.time()
      ## link external grib files
      seclength = self.runcase.fcstSeconds
      if self.runcase.caseNo == 1:
          seclength = max(self.runcase._cfg.fcstlngth[0],self.runcase._cfg.fcstlngth[1],
                          self.runcase._cfg.fcstlngth[3],self.runcase._cfg.fcstlngth[4] )

      for extdname in extdnames :

        extconf = self.runcase.getExtConfig(extdname)
        srcHour,extHour = extconf.extintvals    # forecast frequency and forecast file interval

        extsrc = ExtMData(extdname,extconf)

        if 'srcTime' not in extsrc.keys():
          time1 = self.runcase.startTime
          if extdname == 'HRRRE' :
            initHour = 12 #time1.hour//srcHour*srcHour
            time2    = datetime.strptime(f"{time1:%Y%m%d}_{initHour:02d}:00:00",'%Y%m%d_%H:%M:%S')
          else:
            time2 = self.runcase.startTime-timedelta(hours=extconf.offset)    # realtime use only data 2 hours ago
            initHour = time2.hour//srcHour*srcHour

          #print extHour, srcHour, initHour
          earlyday = 0
          if time1.hour < self.runcase.wofstarthour:
            earlyday = 1

          exttime   = datetime.strptime(f"{time2:%Y%m%d}_{initHour:02d}:00:00",'%Y%m%d_%H:%M:%S')
          evtTime   = exttime - timedelta(days=earlyday)
          expHour   = int( (time1-exttime).total_seconds() //3600 )     # forecast starts from here
          startHour = expHour//extHour*extHour                          # find the earliest available external model forecast

          expHour   = expHour-startHour                                 # offset to be added back from startHour
          extra     = math.ceil((seclength/3600+expHour)/extHour)
          endHour   = startHour + extra*extHour

        else:
          exttime   = extsrc.srcTime
          startHour = int((extsrc.startTime - extsrc.srcTime).total_seconds()//3600)
          endHour   = int((extsrc.endTime   - extsrc.srcTime).total_seconds()//3600)

        n1total  = int(endHour-startHour)//extHour+1
        ntotal   = n1total
        if self.runConfig.ungrib.numens is not None:
          ntotal *= (mpiconfig.numens+1)

        maxwaittime = extconf.waitparms.exist
        if not self.check_job('ungrib'):           # just to check available external files
            maxwaittime = extconf.waitparms.tick   # wait one tick to speed up checking

        numtry = 0
        while numtry < extconf.numtry:
            numtry += 1
            extdat = os.path.join(extconf.extdir, extconf.extsubdir.format(exttime))
            #extdat = os.path.join(extconf.extdir, extconf.extsubdir.format(evtTime))

            itotal = 0
            extmfiles = []    # all time need for all ensemble members
            allextfil = []
            mid=0
            for extmdir in extmdirs:
                extfiles = []
                for nj in range(startHour,endHour+1,extHour) :
                    ij = nj - startHour
                    fl = extconf.filext.format(exttime,no=nj,iens=mid)
                    absfl = os.path.join(extdat,fl)
                    extfiles.append(absfl)
                    allextfil.append(absfl)

                mid += 1
                extmfiles.append(extfiles)

            itotal = self.command.wait_for_files('link_extm',allextfil,
                     maxwaittime,extconf.waitparms.ready,extconf.waitparms.tick,
                     expectSize=extconf.waitparms.size)

            if  itotal < ntotal:
                self.command.addlog(0,"cntl.extm",
                  f"= Found {itotal}/{ntotal} files from {extdname}:{exttime:%Y%m%d_%H:%M:%S} in {extdat}" )
                exttime   = exttime - timedelta(hours=srcHour)
                startHour = startHour + srcHour
                endHour   = startHour + (n1total-1)*extHour
                continue   ## next numtry
            else:
                self.command.addlog(0,"cntl.extm",
                  f"= Try {numtry} - Found all {ntotal} files from {extdname}:{exttime:%Y%m%d_%H:%M:%S} in {extdat}" )
                appext = extconf.appext
                extsrc['srcTime']   = exttime
                extsrc['startTime'] = exttime + timedelta(hours=startHour)
                extsrc['endTime']   = exttime + timedelta(hours=endHour)
                break     ## break out numtry
        else:
            continue  ## next extdname
        break

      #
      # Finally, find one set of files.
      # Link them into ungrib directory
      #
      if itotal == ntotal:
          for mid, extmdir in enumerate(extmdirs):
              extfiles = extmfiles[mid]
              self.command.addlog(0,"EXTM",f"EXTM files for memid = {mid}")
              for idx,filepath in enumerate(extfiles):
                  self.command.addlog(0,"EXTM",f"{idx:03d}: {filepath}")

          if dowrk:
              aord = ord('A')
              for mid, extmdir in enumerate(extmdirs):
                  extfiles = extmfiles[mid]
                  for ij, filepath in enumerate(extfiles):
                      (ii,i1) = divmod(ij,26)
                      (i3,i2) = divmod(ii,26)
                      if i3 >= aord+26: raise ValueError('RAN OUT OF GRIB FILE SUFFIXES!')
                      flname = os.path.join(extmdir,'GRIBFILE.%s%s%s' % (chr(i3+aord),chr(i2+aord),chr(i1+aord)))
                      ##print flname
                      if os.path.lexists(flname) : os.unlink(flname)
                      self.command.copyfile(filepath,flname)
      else:
          self.command.addlog(-1,"EXTM","Cannot find valid GRIB files.")

      #
      # For HRRRE 2020, use analysis at 1400Z
      #
      if self.runcase.caseNo == 1 and appext == "HRRRE":
        extconf = self.runcase.getExtConfig('HRRRE')
        absfl = os.path.join(extconf.extdir, f'{exttime:%Y%m%d}/1400/postprd_mem0000',
                             'wrfnat_hrrre_newse_mem0000_01.grib2')
        #absfl = os.path.join(extconf.extdir, f'{exttime:%Y%m%d}/1400/postprd_mem0004',
        #                     'wrfnat_hrrre_newse_mem0004_01.grib2')

        if not self.command.wait_for_a_file('link_extm',absfl,7200,60,1) :
            self.command.addlog(-1,"EXTM","Cannot find %s" % (absfl))

        if os.path.lexists(absfl):
          flname = os.path.join(extmdir,'GRIBFILE.AAA')
          if os.path.lexists(flname) : os.unlink(flname)
          self.command.copyfile(absfl,flname)

      ##
      ## link Vtable
      ##
      if appext is not None and dowrk :
        absfl = os.path.join(self.runDirs.wpsdir,'ungrib','Variable_Tables','Vtable.'+appext)
        for extmdir in extmdirs:
          vtable = os.path.join(extmdir,'Vtable')
          if os.path.lexists(vtable) : os.unlink(vtable)
          self.command.copyfile(absfl,vtable,hardcopy=False)

      if appext is not None:
          return extsrc
      else:
          return None

    #enddef check_link_extm

    ##%%%%%%%%%%%%%%%%%  Preprocess Observation data %%%%%%%%%%%%%%%%%%%

    def check_wait_obs(self,obses) :
        ''' Preprocess observations '''

        nothing,wrkdirs = self.runcase.getwrkdir(self.caseDir,'news3dvar',False,self.runConfig.news3dvar.numens)
        #os.path.join(self.caseDir,'news3dvar')
        for wrkdir in wrkdirs:
          if not os.path.lexists(wrkdir) :
              os.mkdir(wrkdir)

        ### get time strings
        startime   = self.runcase.startTime

        ## --------------- find each observations -----------------------

        retobs = {'radar' : [], 'ua' : [], 'sng' : [], 'conv': [], 'cwp': [],
                  'lightning1' : [], 'lightning2' : [] }

        for obstype in obses :
            obsconf = self.runcase.getObsConfig(obstype)

            if obstype == 'radar':
              retobs['radar'] = self.check_wait_radar(startime,obsconf)
              #retobs['radar'] = self.check_wait_radar_radial(startime,obsconf.datdir)
              continue

            if obstype == '88vad': continue

            if obstype == "lightning1":
              retobs['lightning1'] = self.check_wait_ENTLN(obstype,startime,obsconf,wrkdirs[0])
              continue

            if obstype == "lightning2":
              retobs['lightning2'] = self.check_wait_GLM(obstype,startime,obsconf,wrkdirs[0])
              continue

            if obsconf.typeof in ('ua', 'cwp', 'conv'):
              retobs[obsconf.typeof] = self.check_wait_datfile(obstype,startime,obsconf,wrkdirs[0])
            elif obsconf.typeof == 'sng':
              retobs['sng'] = self.check_wait_sng(obstype,startime,obsconf,wrkdirs[0])
            else :
              self.command.addlog(-1,"cntl.obs",'    Error: unsupport obs type %s ... ' % (obsconf.typeof))

        ##
        ## Log the observation files
        ##
        validobs = {}
        for (obskey,files) in retobs.items() :
          if len(files) > 0 :
             if isinstance(files[0],list) :
               filelst = [fl[0] for fl in files]
             else :
               filelst = files
             self.command.addlog(0,"cntl.obs",'Found %d "%s" files and they are:' % (len(files),obskey) )
             for fn in filelst:
               self.command.addlog(0,"cntl.obs",'        %s' % fn )

             validobs[obskey] = files
          else :
             self.command.addlog(0,"cntl.obs",'Found 0 "%s" files.' %( obskey ) )
             validobs[obskey] = []

        return validobs
    #enddef check_wait_obs

    ##%%%%%%%%%%%%%%%%%  Wait for radar data %%%%%%%%%%%%%%%%%%%%%%%%%%%

    def check_wait_radar(self,startime,obsconf) :
        ''' Wait for radar data'''

        nothing,wrkdirs = self.runcase.getwrkdir(self.caseDir,'radremap',False,None)
        wrkdir = wrkdirs[0]
        if not os.path.lexists(wrkdir) :
            os.mkdir(wrkdir)

        timstr = startime.strftime('%Y%m%d%H%M%S')  # fixed file name for easy process

        ##---------------- looking for radar files ----------------------
        radars = []
        waittime = 0
        while waittime < obsconf.maxwait :  ## wait 5 minutes for complete radar set
          radarsToCheck = set(self.radars).difference(set(radars))
          for radname in  radarsToCheck :
            radfound = False
            radfile = os.path.join(wrkdir,'RADR_%s%s.raw' % (radname,timstr))
            if os.path.lexists(radfile) :
                self.command.addlog(0,"cntl.radar",'Found radar data file %s ... ' % radfile)
                radfound = True
            else :
              ##
              ## look for radar data files
              ##
              for timemin in obsconf.timerange:    ## in minutes, find in (-10, 10) range
                 currTime = startime+timedelta(minutes=timemin)
                 if currTime.hour < self.runcase.wofstarthour-1:
                   evtTime = (currTime-timedelta(days=1))
                 else:
                   evtTime = currTime
                 datfile1  = os.path.join(obsconf.datdir,
                             obsconf.subdir.format(radar=radname,time=currTime),
                             obsconf.filename.format(radar=radname,time=currTime) )

                 self.command.addlog(999,"cntl.radar",'Looking for radar data file %s ... ' % datfile1)

                 datafls = glob.glob(datfile1)
                 #assert(len(datafls) <= 1)
                 if len(datafls)>=1:
                     self.command.addlog(0,"cntl.radar",'Found radar data file %s ... ' % datafls[0])
                     if not self.command.wait_for_a_file("check_wait_radar",datafls[0],10,180,5):
                       baddatfl = True
                       break
                     self.command.copyfile(datafls[0],radfile,hardcopy=True)
                     radfound = True
                     break   ## timemin loop

            if radfound : radars.append(radname)

          if len(radars) < len(self.radars) and (datetime.utcnow()-startime).total_seconds() < 3600:
            time.sleep(10)
            waittime += 10
          else :
            break

        self.command.addlog(0,"cntl.radar",'Found %d radar files after %d minutes and they are [%s] ' %(
                              len(radars),waittime/60,', '.join(radars) ) )
        #self.runcase.setCaseRadars(self.domID,radars)
        self.domain['usedradars']  = radars
        self.domain['search_done'] = True

        return radars
    #enddef check_wait_radar

    ##%%%%%%%%%%%%%%%%%  Wait for radar radial velocity data %%%%%%%%%%%

    def check_wait_radar_radial(self,startime,obsdir) :
        ''' Wait for radar data'''

        import getradial

        nothing,wrkdirs = self.runcase.getwrkdir(self.caseDir,'radremap',False,None)
        wrkdir = wrkdirs[0]
        if not os.path.lexists(wrkdir) :
            os.mkdir(wrkdir)

        timstr = startime.strftime('%Y%m%d%H%M%S')  # fixed file name for easy process

        maxwaittime = 120    ## wait 5 minutes for complete radar set
        timerange   = [0,-1,-2,-3,-4,-5,-6,-7,-8,-9,-10]    ## in minutes, find in (-10, 10) range

        ##---------------- looking for radar within domain --------------

        #filelist = getradial.get_radial_list(startime,obsdir,self.radars)
        #
        #for radar in filelist.keys():
        #   if len(filelist[radar]) > 0:
        #       radlistfile = os.path.join(wrkdir,"RADR_%s%s.list" % (radar,timstr))
        #       raddir = os.path.join(wrkdir,radar)
        #       if not os.path.lexists(raddir): os.makedirs(raddir)
        #       with open(radlistfile,'w') as lfile:
        #            for fl in filelist[radar]:
        #                (radrelpath,radfname) = os.path.split(fl)
        #                flpaths = radrelpath.split(os.sep)
        #                (radfhead,radfext) = os.path.splitext(radfname)
        #                radfname = "%s_%s%s" %(radfhead,flpaths[2],radfext)
        #                absfile = os.path.join(obsdir,fl)
        #                wrkfile = os.path.join(raddir,radfname)
        #                if not os.path.lexists(wrkfile):
        #                    self.command.copyfile(absfile,wrkfile,hardcopy=True)
        #                print >> lfile, wrkfile
        #   else:
        #       filelist.pop(radar,None)
        #radars = filelist.keys()

        #
        # Run from backup
        #
        #filelist = getradial.get_radial_list(startime,obsdir,self.radars)
        #
        datestr = startime.strftime('%Y%m%d')
        radars=[]
        for radar in self.radars:
           filelist = os.path.join(obsdir,datestr,"RADR_%s%s.list" % (radar,timstr))
           if os.path.lexists(filelist):
             wrklist = os.path.join(wrkdir,"RADR_%s%s.list" % (radar,timstr))
             self.command.copyfile(filelist,wrklist,hardcopy=True)

             raddir = os.path.join(wrkdir,radar)
             if not os.path.lexists(raddir): os.makedirs(raddir)

             lines = [line.rstrip('\n') for line in open(filelist)]

             for fl in lines:
               (radrelpath,radfname) = os.path.split(fl)
               wrkfile = os.path.join(raddir,radfname)
               self.command.copyfile(fl,wrkfile,hardcopy=True)
             radars.append(radar)

        self.command.addlog(0,"cntl.radar",'Found %d radar files and they are [%s] ' %(
                              len(radars),', '.join(radars) ) )
        #self.runcase.setCaseRadars(self.domID,radars)
        self.domain['usedradars']  = radars
        self.domain['search_done'] = True

        return radars
    #enddef check_wait_radar_radial

    ##%%%%%%%%%%%%%%%%%  Wait for satellite %%%%%%%%%%%%%%%%%%%%%%%%%%%%

    def check_wait_datfile(self,obstype,startime,obsconf,wrkdir):
      '''Prepare satellite CWP, Bufr, sounding (ua) data file
      '''

      obsfound  = False
      baddatfl  = False
      retobsfl  = []

      if obstype == "satcwp":
        currTime  = startime-timedelta(minutes=(startime.minute%15))
      elif obstype == "bufr":
        currTime = startime.replace(microsecond=0,second=0,minute=0) # round to whole hour
      elif obstype == "ua":
        currTime = startime
      else:
        self.command.addlog(-1,"cntl.datfile","ERROR:unsupported obstype: %s"%obstype)

      ##---------- looking for original observation files -----------
      waittime = 0
      while waittime < obsconf.maxwait:    ## wait 10 seconds for each data set
         obsfileRe = obsconf.filename.format(currTime)
         obsfileAb = os.path.join(wrkdir,obsfileRe)

         if os.path.lexists(obsfileAb) :
             self.command.addlog(0,"cntl.datfile",'Found data file for %s as %s ... ' % (obstype,obsfileAb))
             obsfound = True
         else :
             for minrng in obsconf.timerange:
                 currTime   = startime + timedelta(minutes=minrng)
                 obsfileRe = obsconf.filename.format(currTime)
                 datfile  = os.path.join(obsconf.datdir,
                               obsconf.subdir.format(currTime),
                               obsfileRe )
                 self.command.addlog(0,"cntl.datfile",'Looking for data file %s ... ' % (datfile))

                 ## -------- Preprocessing each datfile to get obsfile ---
                 if os.path.lexists(datfile) :
                    obsfileAb = os.path.join(wrkdir,obsfileRe)
                    if os.path.lexists(obsfileAb) : os.unlink(obsfileAb)
                    #
                    # Wait dat file for it to be ready
                    #
                    if not self.command.wait_for_a_file('wait_datfile',datfile,60,60,2,expectSize=obsconf.expsize):
                      baddatfl = True
                      break
                    #
                    # Everything is good now
                    #
                    self.command.copyfile(datfile,obsfileAb,hardcopy=True)
                    self.command.addlog(0,"cntl.datfile",'Found data file %s ... ' % (datfile))
                    obsfound = True
                    break

         ## Continue waiting or exit
         if obsfound :
               retobsfl.append(obsfileAb)
               break  ## waittime loop
         elif baddatfl:
               break
         elif (datetime.utcnow()-startime).total_seconds() > 3600:  ## Not a real-time run
               break
         else :                              ## real-time run wait for maxwaittime seconds
               time.sleep(10)
               waittime += 10

      return retobsfl
    #enddef check_wait_datfile

    ##%%%%%%%%%%%%%%%%%  Wait for surface obs %%%%%%%%%%%%%%%%%%%%%%%%%%

    def check_wait_sng(self,obstype,startime,obsconf,wrkdir):
      ''' Prepare surface observations'''

      maxwaittime = obsconf.maxwait    ## wait 10 seconds for each data set

      ##---------- looking for original observation files -----------
      obsfound  = False
      retobsfl  = []

      if obstype == 'surf':

        waittime = 0
        while waittime < maxwaittime  :

          #if startime.minute >= 15:
          #    break    # waittime, break for next obstype

          currTime   = startime.replace(microsecond=0,second=0,minute=0) # round to whole hour

          obsfileRe = obsconf.filename.format(currTime)
          obsfileAb = os.path.join(wrkdir,obsfileRe)

          currTime   = startime - timedelta(hours=1)   # 1 hour ealier
          obsfile1hrAb = os.path.join(wrkdir,obsconf.filename.format(currTime))

          if os.path.lexists(obsfileAb) and os.path.lexists(obsfile1hrAb):
              self.command.addlog(0,"cntl.sng",'Found data file for %s as %s ... ' % (obstype,obsfileAb))
              obsfound = True
          else :
              for minrng in obsconf.timerange:
                currTime   = startime + timedelta(minutes=minrng)

                obsfileRe = obsconf.filename.format(currTime)
                datfile   = os.path.join(obsconf.datdir,
                                 obsconf.subdir.format(currTime),
                                 obsfileRe )
                self.command.addlog(0,"cntl.sng",'Looking for data file %s ... ' % (datfile))

                ## -------- Preprocessing each datfile to get obsfile ---
                if os.path.lexists(datfile) :
                   obsfileAb = os.path.join(wrkdir,obsfileRe )
                   if not self.command.wait_for_a_file('wait_sng',datfile,60,60,2,expectSize=obsconf.expsize):
                         break
                   self.command.copyfile(datfile,obsfileAb,hardcopy=True)
                   self.command.addlog(0,"cntl.sng",'Found data file %s ... ' % (obsfileAb))
                   obsfound = True
                   break

              ## SNG needs 1 hour early obseration for time check
              if obsfound :
                currTime= currTime - timedelta(hours=1)

                datfile1hrRe = obsconf.filename.format(currTime)
                datfile1hr = os.path.join(obsconf.datdir,
                                obsconf.subdir.format(currTime),
                                datfile1hrRe)
                self.command.addlog(0,"cntl.sng",'Looking for data file %s ... ' % (datfile1hr))

                if os.path.lexists(datfile1hr) :
                   obsfile1hrAb = os.path.join(wrkdir,datfile1hrRe)
                   self.command.copyfile(datfile1hr,obsfile1hrAb,hardcopy=True)
                   self.command.addlog(0,"cntl.sng",'Found data file %s ... ' % (obsfile1hrAb))
                   obsfound = True
                else :
                  obsfile1hrAb = 'None'

          ## Continue waiting or exit
          if obsfound :
            #retobsfl.append([obsfileAb,obsfile1hrAb])
            retobsfl.append([obsfileAb," "])
            break  # waittime loop
          elif (datetime.utcnow()-startime).total_seconds() > 3600  :
            break
          else :
            time.sleep(10)
            waittime += 10

      elif obstype == 'auto':    # Convert OK mesonet to LSO format

        stnfilename = os.path.join(self.runDirs.inputdir,obsconf.stnfile)
        lonmin, latmin, lonmax, latmax = prep_okmeso.findRange(stnfilename)

        if self.domain.checkRange(latmin,latmax,lonmin,lonmax,100):
          waittime = 0
          while waittime < maxwaittime  :

            currTime  = startime
            obsfileRe = obsconf.filename.format(currTime)
            obsfileAb = os.path.join(wrkdir,"%s.lso"%obsfileRe)

            time5min   = startime.minute%5
            mystartime = startime - timedelta(minutes=time5min)   # round to 5 minutes

            currTime     = mystartime - timedelta(hours=1)           # round to 5 minutes
            obsfile1hrRe = obsconf.filename.format(currTime)
            obsfile1hrAb = os.path.join(wrkdir,"%s.lso"%obsfile1hrRe)

            if os.path.lexists(obsfileAb): ##and os.path.lexists(obsfile1hrAb):
                self.command.addlog(0,"cntl.auto",'Found data file for %s as %s and %s ... ' % (obstype,obsfileAb,obsfile1hrAb))
                obsfound = True
            else :
                for minrng in obsconf.timerange:
                  currTime   = mystartime + timedelta(minutes=minrng)

                  obsfileRe = obsconf.filename.format(currTime)
                  datfile   = os.path.join(obsconf.datdir,
                                 obsconf.subdir.format(currTime),
                                 '%s.mdf'%obsfileRe )
                  self.command.addlog(0,"cntl.auto",'Looking for data file %s ... ' % (datfile))

                  ## -------- Preprocessing each datfile to get obsfile ---
                  if os.path.lexists(datfile) :
                     obsfileAb = os.path.join(wrkdir,'%s.lso'%(obsfileRe) )
                     if not self.command.wait_for_a_file('wait_sng',datfile,60,60,2,expectSize=obsconf.expsize):
                           break
                     #self.command.copyfile(datfile,obsfileAb,hardcopy=True)
                     prep_okmeso.meso2lso(datfile,stnfilename,obsfileAb)
                     self.command.addlog(0,"cntl.auto",'Found data file %s ... ' % (obsfileAb))
                     obsfound = True
                     break

                ## SNG needs 1 hour early obseration for time check
                if obsfound :
                  currTime = currTime - timedelta(hours=1)

                  datfile1hrRe = obsconf.filename.format(currTime)
                  datfile1hr= os.path.join(obsconf.datdir,
                                 obsconf.subdir.format(currTime),
                                 '%s.mdf'%datfile1hrRe )
                  self.command.addlog(0,"cntl.auto",'Looking for data file %s ... ' % (datfile1hr))

                  if os.path.lexists(datfile1hr) :
                     obsfile1hrAb = os.path.join(wrkdir,'%s.lso'%(datfile1hrRe) )
                     #self.command.copyfile(datfile1hr,obsfile1hrAb,hardcopy=True)
                     prep_okmeso.meso2lso(datfile1hr,stnfilename,obsfile1hrAb)
                     obsfound = True
                  else :
                    obsfile1hrAb = 'None'

            ## Continue waiting or exit
            if obsfound :
              retobsfl.append([obsfileAb,obsfile1hrAb])
              break  # waittime loop
            elif (datetime.utcnow()-startime).total_seconds() > 3600  :
              break
            else :
              time.sleep(10)
              waittime += 10
        else:
            self.command.addlog(0,"cntl.sng","WARNING: OK mesonet is outside of the domain. Skipping ...")
      else:
        self.command.addlog(-1,"cntl.sng",'Error: unsupport obs type %s ... ' % (obskey))

      return retobsfl
    #enddef check_wait_sng

    ##%%%%%%%%%%%%%%%%%  Wait for lightning file %%%%%%%%%%%%%%%%%%%%%%%

    def check_wait_ENTLN(self,obstype,startime,obsconf,wrkdir):
      ''' Prepare ENTLN lightning observations'''

      maxwaittime = obsconf.maxwait    ## wait 10 seconds for each data set

      ##---------- looking for original observation files -----------
      obsfound  = False
      retobsfl  = []

      obsdir = obsconf.datdir

      waittime = 0
      while waittime < maxwaittime  :
          currTime   = startime-timedelta(minutes=(startime.minute%5))
          timstr     = currTime.strftime('%y%j')
          obsfileRe  = '%s_entln.nc' % (timstr)
          obsfileAb  = os.path.join(wrkdir,obsfileRe)
          if os.path.lexists(obsfileAb) :
              self.command.addlog(0,"cntl.lgt",'Found ENTLN data file for %s as %s ... ' % (obstype,obsfileAb))
              obsfound = True
          else :
            for minrng in obsconf.timerange :
              currTime   = currTime + timedelta(minutes=minrng)
              timesrc1d  = currTime - timedelta(days=1)
              timstr1day = timesrc1d.strftime('%y%j')
              timstrsrc  = currTime.strftime('%y%j')
              timstrsrc2 = currTime.strftime('%y%j')
              obsfileRe = '%s_entln.nc' % (timstrsrc)
              datfile   = os.path.join(obsdir,obsfileRe )
              self.command.addlog(0,"cntl.lgt",'Looking for ENTLN data file %s ... ' % (datfile))
              ## -------- Preprocessing each datfile to get obsfile ---
              if self.runDirs['obsvar'][obstype]['prepro']:
                subprocess.call('ncrcat %(obsdir)s/%(yd)s17050005r %(obsdir)s/%(yd)s17[1-5][05]0005r %(obsdir)s/%(yd)s18000005r %(outfile)s'% {'obsdir':obsdir,'outfile':obsfileRe,'yd':timstrsrc2, 'yd2' : timstr1day}, shell=True,cwd=wrkdir)

                obsfileAb = os.path.join(wrkdir,obsfileRe)
              if os.path.lexists(obsfileAb) :
                 obsfound = True
                 break
              elif os.path.lexists(datfile) :
                 if os.path.lexists(obsfileAb) : os.unlink(obsfileAb)
                 self.command.copyfile(datfile,obsfileAb,hardcopy=True)
                 obsfound = True
                 break

          ## Continue waiting or exit
          if obsfound :
            retobsfl.append(obsfileAb)
            break  ## waittime loop
          elif (datetime.utcnow()-startime).total_seconds() > 3600:  ## Not a real-time run
            break
          else :                              ## real-time run wait for maxwaittime seconds
            time.sleep(10)
            waittime += 10

      return retobsfl
    #enddef check_wait_ENTLN

    ##%%%%%%%%%%%%%%%%%  Wait for lightning file (GLM)%%%%%%%%%%%%%%%%%%

    def check_wait_GLM(self,obstype,startime,obsconf,wrkdir):
      ''' Prepare GLM lightning observations'''

      #ncopath= "/scratch/software/Odin/python/anaconda2/bin"

      maxwaittime = obsconf.maxwait    ## wait 10 seconds for each data set

      ##---------- looking for original observation files -----------
      obsfound  = False
      retobsfl  = []

      waittime = 0
      while waittime < maxwaittime  :
          currTime  = startime-timedelta(minutes=(startime.minute%5))
          obsfileRe = obsconf.filename.format(currTime)
          obsfileAb = os.path.join(wrkdir,obsfileRe)

          if os.path.lexists(obsfileAb) :
              self.command.addlog(0,"cntl.lgt",'Found GLM data file for %s as %s ... ' % (obstype,obsfileAb))
              obsfound = True
          else :
            for minrng in obsconf.timerange :
              currTime  = currTime + timedelta(minutes=minrng)
              if currTime.hour < self.runcase.wofstarthour-1:
                evtTime = (currTime-timedelta(days=1))
              else:
                evtTime = currTime

              obsfileRe = obsconf.filename.format(currTime)
              datfile   = os.path.join(obsconf.datdir,
                             obsconf.subdir.format(evtTime),
                             obsfileRe )

              self.command.addlog(0,"cntl.lgt",'Looking for GLM data file %s ... ' % (datfile))

              if os.path.lexists(datfile) :
                 obsfileAb = os.path.join(wrkdir,obsfileRe)
                 if not self.command.wait_for_a_file('wait_lightning',datfile,60,60,2):
                       break
                 self.command.copyfile(datfile,obsfileAb,hardcopy=True)
                 self.command.addlog(0,"cntl.GLM",'Found data file %s ... ' % (obsfileAb))
                 obsfound = True
                 break

          ## Continue waiting or exit
          if obsfound :
            retobsfl.append(obsfileAb)
            break  ## waittime loop
          elif (datetime.utcnow()-startime).total_seconds() > 3600:  ## Not a real-time run
            break
          else :                              ## real-time run wait for maxwaittime seconds
            time.sleep(10)
            waittime += 10

      return retobsfl
    #enddef check_wait_GLM

    ##%%%%%%%%%%%%%%%%%  Wait for MRMS files %%%%%%%%%%%%%%%%%%%%%%%%

    def check_wait_mrms(self,startime,obsconf,wrkdir):
      ''' Prepare mrms data in grib2 format'''

      mrmslvls = ['00.50', '00.75', '01.00', '01.25', '01.50',
                  '01.75', '02.00', '02.25', '02.50', '02.75', '03.00', '03.50',
                  '04.00', '04.50', '05.00', '05.50', '06.00', '06.50', '07.00',
                  '07.50', '08.00', '08.50', '09.00', '10.00', '11.00', '12.00',
                  '13.00', '14.00', '15.00', '16.00', '17.00', '18.00', '19.00' ]

      maxwaittime = obsconf.maxwait    ## wait 10 seconds for each data set

      ##---------- looking for original observation files -----------
      obsfound  = False
      foundfiles = []

      currTime  = startime
      obsfileRe = [obsconf.filename.format(currTime,level=lvl) for lvl in mrmslvls]
      obsfileAb = [os.path.join(wrkdir,fileRe) for fileRe in obsfileRe]

      obsfound = True
      for fileAb in obsfileAb:
          if os.path.lexists(fileAb):
              foundfiles.append(fileAb)
          else:
              obsfound = False
              foundfiles = []
              break

      if obsfound:
          self.command.addlog(0,"cntl.mrms",'Found all MRMS files at %s ... ' % currTime)
      else:      # search for file in earlier time

          waittime = 0
          while waittime < maxwaittime  :
              for minrng in obsconf.timerange:
                currTime  = startime + timedelta(minutes=minrng)
                timstrsrc = currTime.strftime('%Y%m%d-%H%M')
                ldmdir    = obsconf.subdir.format(currTime)

                obsfileRe = [obsconf.filename.format(currtime,level=lvl) for lvl in mrmslvls]
                obsfileAb = [os.path.join(wrkdir,fileRe) for fileRe in obsfileRe]

                obsfileSr = ['MRMS_MergedReflectivityQC_%s_%s??.grib2.gz' % (lvl,timstrsrc) for lvl in mrmslvls]
                datfiles  = [os.path.join(obsconf.datdir,ldmdir,fileSr) for fileSr in obsfileSr]
                self.command.addlog(0,"cntl.mrms",'Looking for %s ... ' % (datfiles[0]))

                ## -------- Preprocessing each datfile to get obsfile ---
                i = 0
                for datfile in datfiles:     # each level
                    datafls   = glob.glob(datfile)
                    #assert(len(datafls) <= 1)
                    if len(datafls)>=1 :
                       if not self.command.wait_for_a_file('wait_mrms',datafls[0],60,120,2,expectSize=obsconf.expsize):
                           break
                       with gzip.open(datafls[0], 'rb') as f_in:
                           with open(obsfileAb[i], 'wb') as f_out:
                               shutil.copyfileobj(f_in, f_out)
                       #self.command.copyfile(srcfile,obsfileAb[i],hardcopy=False)
                       self.command.addlog(0,"cntl.mrms",'Found data file %s ... ' %datafls[0])
                       foundfiles.append(obsfileAb[i])
                    else:
                       foundfiles = []
                       break
                    i += 1

                if len(foundfiles) == len(mrmslvls):
                    obsfound = True
                    break

              ## Continue waiting or exit
              if obsfound :
                break  # waittime loop
              elif (datetime.utcnow()-startime).total_seconds() > 3600  :
                break
              else :
                time.sleep(10)
                waittime += 10

      return foundfiles
    #enddef check_wait_mrms

#endclass
