#!/usr/bin/env python3
##
##----------------------------------------------------------------------
##
## This file defines a forecast/analysis workflow and running steps for
## each program in the workflow.
##
## ---------------------------------------------------------------------
##
## HISTORY:
##   Yunheng Wang (05/25/2018)
##   Initial version based on early works.
##
##
########################################################################
##
## Requirements:
##
##   o Python 2.7 or above
##
########################################################################

import sys, os, re, glob
import subprocess, threading, bz2
from datetime import datetime, timedelta
import time
import logging, argparse

import namelist

##========================== run_interp ==============================

def run_interp(cmd,rootdir,fcst_time,fcstfiles,mrmsfiles,wrktimes):

    executable = os.path.join(rootdir.srcdir,'bin','news3dvar')
    tmplinput  = os.path.join(rootdir.srcdir,'NSSLVAR','input','mrmsinterp.input')

    #                  ##    MPI   nx ny Queue    Min NCPN Exclusive
    jconf = Command.MPIConf(True,  2,  2,None, 30,4, False)
    if jconf.mpi : executable += '_mpi'

    ##---------------- make working directory -------------------------

    wrkdir = cmd.wrkdir
    #if not os.path.lexists(wrkdir) :
    #  os.mkdir(wrkdir)
    if rootdir.field == 'ref':
      refsrc = 1
    else:
      refsrc = 3

    calpath = os.path.join(wrkdir,'calib')
    if not os.path.lexists(calpath) :
      calbas = os.path.join(rootdir.srcdir,'NSSLVAR','calib')
      #calbas = '/scratch/ywang/NEWSVAR/news3dvar.git/data/adas'
      cmd.copyfile(calbas,calpath,True)

    timstr   = fcst_time.strftime('%Y%m%d%H%M' )

    obs_files = []
    for fcstfile,mrmsfile,wrktime in zip(fcstfiles,mrmsfiles,wrktimes):

      ##---------------- Set observation files  ---------------------

      nradfile = 0;  radfname = ['xxx']
      nsngfile = 0;  sngfname = ['xxx'];   sngtmchk = ['xxx']
      nuafile  = 0;  uafname  = ['xxx']
      ncwpfile = 0;  cwpfname = ['xxx']
      nlghtfile = 0; lghtfname = ['xxxx']; srclgt = ['xxxx']

      ##---------------- make namelist file ------------------------------

      hhmmstr = wrktime.strftime('%d%H%M')

      nmlfile  = os.path.join(wrkdir,'mintrp_%s_%s.input'%(rootdir.outname,hhmmstr))
      nmlgrp = namelist.decode_namelist_file(tmplinput)

      nmlin  = {'initime'     : wrktime.strftime('%Y-%m-%d.%H:%M:00')
               ,'modelopt'    : 2
               ,'inifile'     : fcstfile
               ,'inigbf'      : 'xxxxxx'
               ,'runname'     : 'mrmsinterp_%s' % (timstr)
               ,'refsrc'      : refsrc
               ,'reffmt'      : 202
               ,'refile'      : mrmsfile
               ,'nradfil'     : 0,  'radfname' : ['xxx']
               ,'nsngfil'     : 0,  'sngfname' : ['xxx'],  'sngtmchk' : ['xxx']
               ,'nuafil'      : 0,  'uafname'  : ['xxx']
               ,'ncwpfil'     : 0,  'cwpfname' : ['xxx']
               ,'nlgtfil'     : 0,  'lightning_files': ['xxx']
               ,'dirname'     : './'
               ,'refout'      : 207
               ,'nproc_x'     : jconf.nproc_x
               ,'nproc_y'     : jconf.nproc_y
               ,'max_fopen'   : jconf.nproc_x*jconf.nproc_y
               ,'cov_factor'  : 0.0
               }

      nmlgrp.merge(nmlin)
      nmlgrp.writeToFile(nmlfile)

      ##-------------- run the program from command line ----------------
      outfile = os.path.join(wrkdir,'mintrp_%s_%s.output'%(rootdir.outname,hhmmstr) )
      jobid = cmd.run_a_program(executable,nmlfile,outfile,wrkdir,jconf)

      ##-------------- wait for it to be done -------------------
      if not cmd.wait_job('mintrp_%s'%hhmmstr,jobid,False) :
          raise SystemExit()

      anafile = os.path.join(wrkdir,'%s_%s'%(rootdir.intrpname[rootdir.field],
                                    wrktime.strftime('%Y-%m-%d_%H:%M:%S.nc')))
      obs_files.append(anafile)

    return obs_files
#enddef run_interp

##========================== FSS ==============================

def run_fss(cmd,rootdir,timestr,fcst_files,obs_files,obsfmt) :

  executable = os.path.join(rootdir.srcdir,'bin','fss')
  tmplinput  = os.path.join(rootdir.srcdir,'NSSLVAR','input','neighbor_fss.input')

  #                  ##    MPI   nx ny Queue    Min NCPN Exclusive
  jconf = Command.MPIConf(True,  4,  6,None,    30,12,   False,   shell=True)
  if jconf.mpi : executable += '_mpi'

  ##---------------- make working directory -------------------------

  wrkdir = cmd.wrkdir
  #if not os.path.lexists(wrkdir) :
  #  os.mkdir(wrkdir)

  if rootdir.field == 'ref':
    vfield = 1
  else:
    vfield = 2

  txtfile = os.path.join(wrkdir,'vscore_%s_%s.fss'%(rootdir.outname,timestr))

  ##--------------- make namelist file ------------------------------

  nmlfile  = os.path.join(wrkdir,'neighbor_%s_fss.input'%rootdir.outname)

  nmlgrp = namelist.decode_namelist_file(tmplinput)

  nmlin  = { 'fcst_files' : fcst_files,
             'obsfmt'     : obsfmt,
             'obs_files'  : obs_files,
             'ntime'      : len(fcst_files),

             'vfield'     : vfield,
             'radius'     : rootdir.hradius[rootdir.field],
             'nradius'    : len(rootdir.hradius[rootdir.field]),
             'thres'      : rootdir.thres[rootdir.field],
             'nthres'     : len(rootdir.thres[rootdir.field]),

             'outfile'    : txtfile,
             'nproc_x'    : jconf.nproc_x,
             'nproc_y'    : jconf.nproc_y
           }

  nmlgrp.merge(nmlin)
  nmlgrp.writeToFile(nmlfile)

  ##-------------- run the program from command line ----------------

  outfile = os.path.join(wrkdir,'fss_%s_%s.output'%(rootdir.outname,timestr))
  jobid = cmd.run_a_program(executable,nmlfile,outfile,wrkdir,jconf)

  #executable= os.path.join('/lfs3/projects/hpc-wof1/afierro/news3dvar_verif/bin','fss')
  #retcode = subprocess.call('%s  %s'% (executable,nmlfile),cwd=wrkdir,shell=True)

  ##-------------- wait for it to be done -------------------
  #if not cmd.wait_job('run_fss',jobid,False) :
  #    raise SystemExit()

  return txtfile
#enddef run_fss

##========================== ETS ==============================

def run_ets(cmd,rootdir,timestr,fcst_files,obs_files,obsfmt) :

  executable = os.path.join(rootdir.srcdir,'bin','ets')
  tmplinput  = os.path.join(rootdir.srcdir,'NSSLVAR','input','neighbor_ets.input')

  #                  ##    MPI   nx ny Queue    Min NCPN Exclusive
  jconf = Command.MPIConf(True,  4,  6,None,    30,12,   False,   shell=True)
  if jconf.mpi : executable += '_mpi'

  ##---------------- make working directory -------------------------

  wrkdir = cmd.wrkdir
  #if not os.path.lexists(wrkdir) :
  #  os.mkdir(wrkdir)

  if rootdir.field == 'ref':
    vfield = 1
  else:
    vfield = 2

  txtfile1 = os.path.join(wrkdir,'vscore_%s_%s.ets'%(rootdir.outname,timestr))
  txtfile2 = os.path.join(wrkdir,'vscore_%s_%s.bias'%(rootdir.outname,timestr))
  txtfile3 = os.path.join(wrkdir,'contingency_%s_%s.txt'%(rootdir.outname,timestr))

  ##--------------- make namelist file ------------------------------

  nmlfile  = os.path.join(wrkdir,'neighbor_%s_ets.input'%rootdir.outname)

  nmlgrp = namelist.decode_namelist_file(tmplinput)

  nmlin  = { 'fcst_files' : fcst_files,
             'obsfmt'     : obsfmt,
             'obs_files'  : obs_files,
             'ntime'      : len(fcst_files),

             'vfield'     : vfield,
             'radius'     : rootdir.hradius[rootdir.field],
             'vradius'    : rootdir.vradius[rootdir.field],
             'nradius'    : len(rootdir.hradius[rootdir.field]),
             'thres'      : rootdir.thres[rootdir.field],
             'nthres'     : len(rootdir.thres[rootdir.field]),

             'outfile'    : [txtfile1,txtfile2],
             'outcontg'   : 1,
             'contgfile'  : txtfile3,
             'nproc_x'    : jconf.nproc_x,
             'nproc_y'    : jconf.nproc_y
           }

  nmlgrp.merge(nmlin)
  nmlgrp.writeToFile(nmlfile)

  ##-------------- run the program from command line ----------------

  outfile = os.path.join(wrkdir,'ets_%s_%s.output'%(rootdir.outname,timestr))
  jobid = cmd.run_a_program(executable,nmlfile,outfile,wrkdir,jconf)

  #executable= os.path.join('/lfs3/projects/hpc-wof1/afierro/news3dvar_verif/bin','ets')
  #retcode = subprocess.call('%s  %s'% (executable,nmlfile),cwd=wrkdir,shell=True)


  ##-------------- wait for it to be done -------------------
  #if not cmd.wait_job('run_ets',jobid,False) :
  #    raise SystemExit()

  return txtfile1,txtfile2
#enddef run_ets

##========================== PLT_FSS =================================

def plt_fss(cmd,rootdir,fcstTime,txtfile) :

  inputdir   = os.path.join(rootdir.srcdir,'NSSLVAR')
  executable = os.path.join(rootdir.ncldir,'ncl')

  ncltmpl = os.path.join(inputdir,'nclscripts','fss.ncl' )

  #                  ##    MPI   nx ny Queue    Min NCPN Exclusive
  jconf = Command.MPIConf(False,  1,  1,None, 30,1, False,   shell=True)

  ##---------------- make working directory -------------------------

  wrkdir = cmd.wrkdir
  #if not os.path.lexists(wrkdir) :
  #  os.mkdir(wrkdir)

  if not cmd.wait_for_a_file('plt_fss',txtfile,3600,300,10):
    raise RuntimeError("FSS file not found.")

  ##---------------- Plot each field --------------------------------

  timestr = fcstTime.strftime('%Y%m%d_%H%M')

  timeline = 'timecst = "%sZ"'%timestr
  unitsline = 'units = "%s"'%rootdir.units[rootdir.field]
  fcsttime  = 'fcstintvl = "%s"'%rootdir.timeunits[rootdir.field]

  nclscpt = os.path.join(wrkdir,'plt_fss_%s_%s.ncl'%(rootdir.outname,timestr))

  nclfile = open(nclscpt,'w')
  nclstr  = '''
       ; ***********************************************
       ; These files are loaded by default in NCL V6.2.0 and newer
       ; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
       ; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
       ; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
       ;************************************************
       begin

         filename = "%(filename)s"

         wks_type = "png"
         wks_type@wkWidth  = 1224
         wks_type@wkHeight = 1584
         wks   = gsn_open_wks (wks_type,"fss_%(varname)s_%(timestr)s.png")

         %(timeline)s
         %(unitsline)s
         %(fcsttime)s
       ''' % { 'filename' : txtfile, 'timestr' : timestr,
               'timeline' : timeline,'varname' : rootdir.outname,
               'unitsline': unitsline, 'fcsttime' : fcsttime
              }

  nclfile.write(cmd.trim(nclstr))
  for line in file(ncltmpl) :
      if line.startswith(';;;'): continue
      nclfile.write(line)

  nclfile.close()

  ## Now execute the ncl script
  jobid = cmd.run_ncl_plt(executable,nclscpt,'fss',
                          wrkdir,0,inputdir,jconf,transfer=False )

  #retcode = subprocess.call('ncl %s'% nclscpt,cwd=wrkdir,shell=True)

  if not cmd.wait_job('plt_fss',jobid,True) :
       raise RuntimeError("Job plt_fss failed")

#enddef plt_fss

##========================== PLT_ETS =================================

def plt_ets(cmd,rootdir,fcstTime,txtfiles) :

  inputdir   = os.path.join(rootdir.srcdir,'NSSLVAR')
  executable = os.path.join(rootdir.ncldir,'ncl')

  #                  ##    MPI   nx ny Queue    Min NCPN Exclusive
  jconf = Command.MPIConf(False,  1,  1,None, 30,1, False,   shell=True)

  ##---------------- make working directory -------------------------

  wrkdir = cmd.wrkdir
  #if not os.path.lexists(wrkdir) :
  #  os.mkdir(wrkdir)

  if not cmd.wait_for_a_file('plt_ets',txtfiles[0],3600,300,10):
    raise RuntimeError("ETS file not found.")

  ##---------------- Plot each field --------------------------------

  timestr = fcstTime.strftime('%Y%m%d_%H%M')

  timeline  = 'timecst = "%sZ"'%timestr
  unitsline = 'units = "%s"'%rootdir.units[rootdir.field]
  fcsttime  = 'fcstintvl = "%s"'%rootdir.timeunits[rootdir.field]

  i = 0
  for field in ("ets","bias"):
    ncltmpl = os.path.join(inputdir,'nclscripts','%s.ncl'%field )
    nclscpt = os.path.join(wrkdir,'plt_%s_%s_%s.ncl'%(field,rootdir.outname,timestr))

    nclfile = open(nclscpt,'w')
    nclstr  = '''
         ; ***********************************************
         ; These files are loaded by default in NCL V6.2.0 and newer
         ; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
         ; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
         ; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
         ;************************************************
         begin

           filename = "%(filename)s"

           wks_type = "png"
           wks_type@wkWidth  = 1224
           wks_type@wkHeight = 1584
           wks   = gsn_open_wks (wks_type,"%(field)s_%(varname)s_%(timestr)s.png")

           %(timeline)s
           %(unitsline)s
           %(fcsttime)s
      ''' % { 'filename' : txtfiles[i], 'timestr' : timestr,
              'timeline' : timeline, 'field' : field, 'varname':rootdir.outname,
              'unitsline': unitsline, 'fcsttime' : fcsttime
            }

    nclfile.write(cmd.trim(nclstr))
    for line in file(ncltmpl) :
        if line.startswith(';;;'): continue
        nclfile.write(line)

    nclfile.close()
    i += 1

    ## Now execute the ncl script
    jobid = cmd.run_ncl_plt(executable,nclscpt,field,
                            wrkdir,0,inputdir,jconf,transfer=False )

    #retcode = subprocess.call('ncl %s'% nclscpt,cwd=wrkdir,shell=True)


    if not cmd.wait_job('plt_ets',jobid,True) :
         raise RuntimeError("Job plt_ets failed")

#enddef plt_ets

################# Retrieve forecast files #############################

def get_files(fcst_time,fcst_interval,fcst_len,rootdir,srcobs):

  fcst_files = []
  obs_files  = []
  wrk_times  = []
  basedir = fcst_time.strftime('%Y%m%d')
  if fcst_time.hour < 16:
    basedir = (fcst_time-timedelta(hours=24)).strftime('%Y%m%d')

  for dtime in range(0,fcst_len+fcst_interval,fcst_interval):
    wrkTime = fcst_time + timedelta(minutes=dtime)
    timestr = wrkTime.strftime('%Y-%m-%d_%H:%M:%S')
    wrk_times.append(wrkTime)

    wrfdir  = fcst_time.strftime('%H%MZ')
    wrffile = os.path.join(rootdir.fcstdir,wrfdir,'dom20/wrf5','wrfout_d01_%s'%timestr)
    wrfready = os.path.join(rootdir.fcstdir,wrfdir,'dom20/wrf5','wrfoutReady_d01_%s'%timestr)
    if not os.path.lexists(wrfready):
      if not wait_for_a_file(wrfready,3600,waittick=10):
        raise RuntimeError("Forecast file not found at %s."%timestr)
    fcst_files.append(wrffile)

    if srcobs == 1:   #'ANALYSIS':    # find 3D var analysis files
      vardir  = wrkTime.strftime('%H%MZ')
      varfile = os.path.join(rootdir.fcstdir,basedir,vardir,'dom20/news3dvar','wrfout_d01_%s'%timestr)
      varready = os.path.join(rootdir.fcstdir,basedir,vardir,'dom20/news3dvar','wrfoutReady_d01_%s'%timestr)
      if not os.path.lexists(varready):
        if not wait_for_a_file(varready,3600,waittick=10):
          raise RuntimeError("Analysis file not found at %s."%timestr)
      obs_files.append(varfile)

    elif srcobs == 0:  # find MRMS binary files

      mrmstime = "%s.%s"%(rootdir.mrmsname[rootdir.field],
                          wrkTime.strftime('%Y%m%d.%H%M%S'))
      mrmsfile = os.path.join(rootdir.mrmsdir,mrmstime)
      #mrmsfilegz = os.path.join(rootdir.mrmsdir,'%s.gz'%mrmstime)
      if not wait_for_a_file(mrmsfile,3600,waittick=10):
          raise RuntimeError("MRMS file not found at %s."%timestr)
      obs_files.append(mrmsfile)

    elif srcobs == 202:  #'MRMSBIN', Interpolated netCDF files
      creftime = "%s_%s"%(rootdir.intrpname[rootdir.field],
                          wrkTime.strftime('%Y-%m-%d_%H:%M:%S'))
      #creffile = os.path.join(rootdir.obsdir,basedir,wrfdir,'%s.nc'%creftime)
      creffile = os.path.join(rootdir.obsdir,basedir,'%s.nc'%creftime)
      if not os.path.lexists(creffile):
        if not wait_for_a_file(creffile,3600,waittick=10):
          raise RuntimeError("MRMS %s file not found at %s."%(rootdir.intrpname[rootdir.field],timestr))
      obs_files.append(creffile)

    else:
      raise RuntimeError("Unsupported obs. source: %s."%srcobs)

  return fcst_files, obs_files, wrk_times

##========================== PLT_COMREF =================================

def plt_comref(cmd,rootdir,field,obsfiles,wrktimes) :

  inputdir   = os.path.join(rootdir.srcdir,'NSSLVAR')
  executable = os.path.join(rootdir.ncldir,'ncl')

  #                  ##    MPI   nx ny Queue    Min NCPN Exclusive
  jconf = Command.MPIConf(False,  1,  1,'radarq', 30,1, False,   shell=True)

  ##---------------- make working directory -------------------------

  wrkdir = cmd.wrkdir
  #if not os.path.lexists(wrkdir) :
  #  os.mkdir(wrkdir)

  do_sub = False
  if field == 'obsref':
     refname = 'REFMOSAIC3D'
     tmplfile = 'comref.ncl'
     title = 'MRMS observation'
  elif field == 'fcstref':
     refname = 'REFL_10CM'
     tmplfile = 'comref.ncl'
     title = 'WRF forecast'
  elif field == 'obspcp':
     refname = 'HOURLYPRCP'
     tmplfile = 'hrlypcp.ncl'
     title = 'Stage IV observation'
  elif field == 'fcstpcp':
     refname = 'RAINNC'
     tmplfile = 'hrlypcp.ncl'
     title = 'WRF forecast'
     do_sub  = True

  prevfile = obsfiles[0]
  for obsfile,wrktime in zip(obsfiles,wrktimes):

    if not cmd.wait_for_a_file('plt_%s'%field,obsfile,3600,300,10):
      raise RuntimeError("COMREF file not found.")

  ##---------------- Plot each field --------------------------------

    timestr = wrktime.strftime('%Y%m%d_%H%M')

    timeline = 'timescst = "%sZ"'%timestr
    titleline = 'title = "%s"' % title

    ncltmpl = os.path.join(inputdir,'nclscripts',tmplfile)
    nclscpt = os.path.join(wrkdir,'plt_%s_%s.ncl'%(field,timestr))

    nclfile = open(nclscpt,'w')
    nclstr  = '''
         ; ***********************************************
         ; These files are loaded by default in NCL V6.2.0 and newer
         load "%(ncargroot)s/lib/ncarg/nclscripts/csm/gsn_code.ncl"
         ;load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
         ;load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
         ;************************************************
         load "%(ncargroot)s/lib/ncarg/nclscripts/wrf/WRFUserARW.ncl"
         begin

           filename = "%(filename)s"
           field    = "%(refname)s"

           wks_type = "png"
           wks_type@wkWidth  = 1224
           wks_type@wkHeight = 1584
           wks   = gsn_open_wks (wks_type,"%(field)s_%(timestr)s.png")

           %(timeline)s
           %(titleline)s
      ''' % { 'filename' : obsfile, 'timestr' : timestr,
              'timeline' : timeline, 'field' : field,
              'refname'  : refname,  'titleline': titleline,
              'ncargroot': os.path.dirname(rootdir.ncldir)
            }

    nclfile.write(cmd.trim(nclstr))

    if do_sub:
      nclfile.write('''  do_sub = True \n''')
      nclfile.write('''  filename0 = "%s"\n'''%prevfile)

      prevfile = obsfile
    else:
      nclfile.write('''  do_sub = False \n''')

    for line in file(ncltmpl) :
        if line.startswith(';;;'): continue
        nclfile.write(line)

    nclfile.close()

    ## Now execute the ncl script
    jobid = cmd.run_ncl_plt(executable,nclscpt,field,
                            wrkdir,0,inputdir,jconf,transfer=False )

    #retcode = subprocess.call('ncl %s'% nclscpt,cwd=wrkdir,shell=True)
                        #stdin=None,stdout=subprocess.STDOUT,stderr=stdout)

    if not cmd.wait_job('plt_comref',jobid,True) :
         raise RuntimeError("Job plt_comref failed")

#enddef plt_comref

################# Waiting for file ready #############################

def wait_for_a_file(filepath,maxwaitexist=10800,waittick=10):
  """
     Checks if a file is ready for reading/copying.

     For a file to be ready it must exist and is not writing by
     any other processe.
  """

  ##
  ## First, make sure file exists
  ##
  #  If the file doesn't exist, wait waittick seconds and try again
  #  until it's found.
  #

  wait_time = 0
  while wait_time < maxwaitexist :
    if os.path.exists(filepath):
      return True
      break
    else :
      logging.info("<%s> hasn't arrived after %s seconds." % (filepath, wait_time) )
      time.sleep(waittick)
      wait_time += waittick
  else:   ## We have waitted too long
      logging.error('Waiting for file <%s> excceeded %d seconds.\n' % \
                     (filepath,maxwaitexist))
      return False

########################################################################

class Dirs(object):

  def __init__(self,args):
    self.srcdir  = args.src_dir
    self.fcstdir = args.fcst_dir
    self.wrkdir  = args.wrk_dir
    self.mrmsdir = args.mrms_dir
    self.obsdir  = args.obs_dir
    self.ncldir  = '/apps/ncl/6.1.2/intel.bin'

    self.field   = args.field

    if args.field == 'ref':
      self.outname = 'CREF'
    else:
      self.outname = 'HPCP'

    self.mrmsname  = {'ref': 'CREF',
                      'pcp': '1HR_STAGE4' }

    self.intrpname = {'ref': 'MRMS_CREF',
                      'pcp': 'MRMS_HPRCP' }

    self.units     = {'ref': 'dBZ',
                      'pcp': 'mm'  }

    self.timeunits = {'ref': 'minutes',
                      'pcp': 'hours'  }

    self.thres   = {'ref': [20.0,30.0,40.0],
                    'pcp': [1.0,2.0,5.0,10.0] }
    self.hradius = {'ref': [9.0,12.0],
                    'pcp': [9.0,12.00] }

    self.vradius = {'ref': [  1,  1,  1,   1,   1,   1],
                    'pcp': [  1,  1,  1,   1,   1,   1] }


    self.field   = args.field


########################################################################

class CustomFormatter(argparse.RawTextHelpFormatter, argparse.ArgumentDefaultsHelpFormatter):
    pass

def parse_command_line():

  parser = argparse.ArgumentParser(description='Run verificaiton scores for NEWSVAR forecast',
                                   epilog='''        \t\t---- Yunheng Wang (2018-05-25).
                                          ''',
                                   formatter_class=CustomFormatter)

  parser.add_argument('fcst_date',
                      help='date (yyyymmddHHMM)')

  parser.add_argument('field',
                      help='field (one [ref,pcp])')

  parser.add_argument('-s','--src_dir',
                      help='Program source directory (find "bin","scripts","NSSLVAR/input" etc.)',
                      default="/lfs3/projects/hpc-wof1/ywang/news3dvar")

  parser.add_argument('-d','--fcst_dir',
                      help='WRF forecast directory',default="/lfs3/projects/hpc-wof1/ywang/Alex/fcsts/wrfctrl")

  parser.add_argument('-b','--obs_dir',
                      help='Observation directory',default="/lfs3/projects/hpc-wof1/ywang/Alex/mrms_data")

  parser.add_argument('-m','--mrms_dir',
                      help='MRMS obs. directory',default="/lfs3/projects/hpc-wof1/ywang/Alex/mrms_data")

  parser.add_argument('-w','--wrk_dir',
                      help='Working directory',default="/lfs3/projects/hpc-wof1/ywang/Alex/verif/wrfctrl")

  parser.add_argument('-p','--program',
                      help='Program to run, one of [minterp,plt_fcst,plt_obs,run_fss,run_ets,plt_fss,plt_ets]',default=None)

  parser.add_argument('-r','--run',action='store_true',
                      help='MRMS was interpolated to model grid by separated NEWS3DVAR run',default=None)

  parser.add_argument('-l','--fcst_len',default=720,type=int,
                    help='Forecast length in minutes')
  parser.add_argument('-i','--interval',default=60,type=int,
                    help='Verification interval in minutes')

  args = parser.parse_args()

  return args

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if __name__ == "__main__":

  _debug = True

  import Command_Odin as Command
  #import Command_Jet as Command

  stime = time.time()

  args = parse_command_line()

  rootdir = Dirs(args)

  fcst_time = datetime.strptime(args.fcst_date,'%Y%m%d%H%M')

  timstr  = fcst_time.strftime('%Y%m%d_%H%M')
  if fcst_time.hour < 16:
    eventdate = (fcst_time-timedelta(days=1)).strftime('%Y%m%d')
  else:
    eventdate = fcst_time.strftime('%Y%m%d')

  wrkdir = os.path.join(args.wrk_dir,eventdate)
  if not os.path.lexists(wrkdir):
    os.mkdir(wrkdir)

  if 'minterp' != args.program:
    caseDir = fcst_time.strftime('%H%MZ')
    wrkdir = os.path.join(wrkdir,caseDir)
    if not os.path.lexists(wrkdir):
      os.mkdir(wrkdir)

  cmdconfig = Command.configurator(wrkdir,False,_debug)
  cmdconfig.setuplog(wrkdir,'verif',timstr,_debug)

  #thres=[40.0]
  #hradius=[4.0]
  #vradius=[1]

  #flens = { '19' : 360, '20' : 300, '21' : 240 }
  #hour = fcst_time.strftime('%H')
  #fcstlen = flen[hour]

  #---------------------------------------------------------------------
  #
  # Prepare forecast file and observation files
  #
  #---------------------------------------------------------------------

  srcobs = 1                                 # 'ANALYSIS'
  if args.run: srcobs = 202                  # 'MRMSBIN', Interpolated netCDF files
  if 'minterp' == args.program: srcobs = 0   # Find MRMS binary file

  fcst_files,obs_files,wrk_times = get_files(fcst_time,args.interval,args.fcst_len,rootdir,srcobs)

  #---------------------------------------------------------------------
  #
  # Run the programs for score and plot
  #
  #---------------------------------------------------------------------

  if args.program is None:
      fssfile = run_fss(cmdconfig,rootdir,timstr,fcst_files,obs_files,srcobs)
      etsfiles = run_ets(cmdconfig,rootdir,timstr,fcst_files,obs_files,srcobs)

      plt_fss(cmdconfig,rootdir,fcst_time,fssfile)
      plt_ets(cmdconfig,rootdir,fcst_time,etsfiles)
  else:
      if  "minterp" == args.program:
        obs_files = run_interp(cmdconfig,rootdir,fcst_time,fcst_files,obs_files,wrk_times)

      if  "plt_obs" == args.program:
        plt_comref(cmdconfig,rootdir,'obs%s'%args.field,obs_files,wrk_times)

      if  "plt_fcst" == args.program:
        plt_comref(cmdconfig,rootdir,'fcst%s'%args.field,fcst_files,wrk_times)

      if  "run_fss" == args.program:
        fssfile = run_fss(cmdconfig,rootdir,timstr,fcst_files,obs_files,srcobs)

      if  "run_ets" == args.program:
        etsfiles = run_ets(cmdconfig,rootdir,timstr,fcst_files,obs_files,srcobs)

      if  "plt_fss" == args.program:
        fssfile = os.path.join(wrkdir,'vscore_%s_%s.fss'%(rootdir.outname,timstr))
        plt_fss(cmdconfig,rootdir,fcst_time,fssfile)

      if  "plt_ets" == args.program:
        etsfiles = [os.path.join(wrkdir,'vscore_%s_%s.ets' %(rootdir.outname,timstr)),
                    os.path.join(wrkdir,'vscore_%s_%s.bias'%(rootdir.outname,timstr)) ]
        plt_ets(cmdconfig,rootdir,fcst_time,etsfiles)

  etime = time.time()-stime
  fm = (etime % 3600 )//60
  fs =  etime % 3600 - fm*60
  print ("Job finished and used %02dm %02ds." % ( fm,fs))
