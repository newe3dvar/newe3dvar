#!/usr/bin/env python
## ---------------------------------------------------------------------
## This software is in the public domain, furnished "as is", without
## technical support, and with no warranty, express or implied, as to
## its usefulness for any purpose.
## ---------------------------------------------------------------------
##
## This is a python library to run program from command line
##
## ---------------------------------------------------------------------
##
## HISTORY:
##   Yunheng Wang (05/18/2012)
##   Initial version.
##
##
########################################################################
##
## Requirements:
##
########################################################################

import os, sys, time, re
import subprocess

from configBase import *

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class configurator (baseConfigurator) :
  #classbegin
  '''
  this is a container for running programs from command lines
  '''

  ##----------------------- Initialization      -----------------------
  def __init__(self,wrkdir,norun,debug) :
    baseConfigurator.__init__(self,wrkdir,norun,debug)
    self.hostname = 'Shell'
  #enddef

  ##----------------------- Ending of the module -----------------------
  def finalize(self) :
    baseConfigurator.finalize(self)
  #enddef

  ######################################################################

  def run_a_program(self,executable,nmlfile,outfile,wrkdir,jobconf,inarg=None):
    '''
    run a program through the shell

      nmlfile : namelist file can be None
      outfile : must provide a output file name
      wrkdir  : the program can be run in a directory different from the
                instance attribute of wrkdir
    '''

    if self.cmdmutex.acquire():
      self.serieno += 1
      myserieno = self.serieno
      self.cmdmutex.release()

    #cwdsaved = os.getcwd()
    #os.chdir(wrkdir)

    cmdstr = ' '
    stdinput = None
    if nmlfile :
      cmdstr += "< %s" % nmlfile

    stdoutput = None
    if outfile :
      if not self.showonly : stdoutput = file(outfile,'w')
      cmdstr += ">! %s" % outfile

    if jobconf.mpi :

      #nprocs = jobconf.nproc_x*jobconf.nproc_y
      nprocs = jobconf.ntotal

      cmdlist = [ 'mpirun', '-np %d'% nprocs, executable]

    else :
      cmdlist = [executable]

    if inarg is None:
      cmdlist.append(nmlfile)
    elif inarg == 'STDIN':
      if not self.showonly : stdinput = file(nmlfile,'r')
    else:
      cmdlist.append(" %s %s" % (inarg,nmlfile))

    self.addlog(1,self.hostname,'''%s = %02d =\n  Launching "%s".''' %(
                      time.strftime('%Y-%m-%d %I:%M:%S %p'),myserieno,
                      ' '.join(cmdlist)+cmdstr
                     )
                )

    job = JobID(jobname,wrkdir,self,myserieno)

    if self.showonly :
      self.addlog(0,self.hostname, '%s' % ' '.join(cmdlist)+cmdstr)
      job['status']  = 0

    else :

      #if self.wait :
        retP = subprocess.Popen(cmdlist,stdin=stdinput,stdout=stdoutput,
                                stderr=subprocess.STDOUT,cwd=wrkdir)
        retP.wait()
        retcode = retP.returncode

        job['status'] = retcode

        if job['status'] == 0: subprocess.call('touch done.%s.%d' %(jobname,myserieno),shell=True,cwd=wrkdir)
        else:                  subprocess.call('touch error.%s.%d'%(jobname,myserieno),shell=True,cwd=wrkdir)

      #else :
      #  cmdlist.insert(0,'nohup')
      #  retP = subprocess.Popen(cmdlist,stdin=stdinput,stdout=stdoutput,
      #                          stderr=subprocess.STDOUT,cwd=wrkdir)
      #  job['status'] = 9


    ##retcode = subprocess.call(executable,stdin=nmlfile,stdout=file(outfile,'w'),
    ##                  stderr=subprocess.STDOUT,cwd=wrkdir)
    ##subprocess.check_call()

    if stdinput  :  stdinput.close()
    if stdoutput :  stdoutput.close()

    #os.chdir(cwdsaved)

    return job

  #enddef

  def run_ncl_plt(self,executable,nmlfile,field,
                  wrkdir,fminu,tmpldir,jobconf,transfer=False):
    '''
      Plot using NCL and convert to PNG file

      nmlfile : namelist file can be None
      outfile : must provide a output file name
      wrkdir  : the program can be run in a directory different from the
                instance attribute of wrkdir
    '''
    gs      = "/usr/bin/gs"
    gsopt   = "-dSAFER -dBATCH -dNOPAUSE -sDEVICE=png16m -dGraphicsAlphaBits=4"
    psfile  = "%s.ps"  % field

    self.serieno += 1

    cwdsaved = os.getcwd()
    os.chdir(wrkdir)

    cmdstr = executable
    if nmlfile :
      cmdstr += " < %s" % nmlfile

    (jobname,ext) = os.path.splitext(nmlfile)
    jobname = os.path.basename(jobname)

    if jobconf.mpi :

      claimncpn = jobconf.claimncpn
      nprocs = jobconf.nproc_x*jobconf.nproc_y
      cmdstr = 'mpirun -np %d %s' % (nprocs, cmdstr)

      (nodes,extra) = divmod(nprocs,claimncpn)
      if extra > 0 :
        nodes += 1

      nprocs = nodes*self.hardncpn

    else :
      nprocs = 1
      claimncpn = 1
      nodes  = 1

    if field.startswith('agl'):
      outfile = r"agl%02d.png"
      pngrenamesub = """for fn in agl??.png
        do
          seq=${fn:3:2}
          seq=${seq#0}
          (( skm = seq+2 ))
          #skm=$(echo $seq+2.5 | bc -q)
          mv $fn %(outdir)s/agl${skm}km.png
        done
        """ % {'outdir' : outdir }
    else :
      outfile = os.path.join(outdir,"%s.png" % field)
      pngrenamesub = "ls -l %s" % outfile

    (hour,minute) = divmod(jobconf.claimmin,60)
    timestr = '%02d:%02d' % (hour,minute)

    scriptstr = '''#!/bin/bash

        export NCARG_RANGS=%(SZMBTmpl)s/NCLRANGS

        cd %(wrkdir)s

        time1=$(date '+%%s')
        echo "Job Started: $(date). Job Id:  $PBS_JOBID"
        echo " "

        set echo on

        %(cmdstr)s

        %(gs)s %(gsopt)s -sOutputFile=%(outfile)s %(psfile)s

        %(pngrename)s

        set echo off

        time2=$(date '+%%s')

        let diff=time2-time1
        let hour=diff/3600
        let diff=diff%%3600
        let min=diff/60
        let sec=diff%%60

        echo -n "Job   Ended: $(date). "
        printf 'Job run time:  %%02d:%%02d:%%02d' $hour $min $sec
        echo " "

        ''' %{ 'wrkdir'  : wrkdir,   'cmdstr'    : cmdstr,
               'gs'       : gs,      'gsopt'     : gsopt,
               'outfile'  : outfile, 'psfile'    : psfile,
               'SZMBTmpl' : tmpldir, 'pngrename' : pngrenamesub
        	    }

    scriptfile = '%s.sh' % jobname

    batchFile = open(scriptfile,'w')
    batchFile.write(self.trim(scriptstr))
    batchFile.close()

    self.addlog(1,self.hostname,'''%s - %02d -\n  Jobscript "%s" generated.''' %(
                      time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
                      scriptfile )
                )

    if self.showonly :
      self.addlog(1,self.hostname, 'Preparing job script "%s" in %s.' % (scriptfile,wrkdir))

      job = JobID(jobname,wrkdir,self,myserieno)
      job['status'] = 0

    else :
      job = self.submit_a_job(jobname,wrkdir)

    #os.chdir(cwdsaved)

    return job
  #enddef run_ncl_plt

  def submit_a_job(self,jobname,wrkdir,djobs=None):

    retcode = subprocess.call(['bash', '%s.sh' % jobname],cwd=wrkdir,shell=True,
                        stdin=None,stdout=subprocess.STDOUT,stderr=stdout)

    if retcode == 0: subprocess.call('touch done.%s.%d' %(jobname,self.serieno),shell=True,cwd=wrkdir)
    else:            subprocess.call('touch error.%s.%d'%(jobname,self.serieno),shell=True,cwd=wrkdir)

    job = JobID(jobname,wrkdir,self,self.serieno)
    job['status'] = retcode

    return job
  #enddef

  ######################################################################

  @staticmethod
  def fetchDefaultWorkingDir(tests=False) :

    if tests :
      return '/scratch/ywang/test_runs'
    else :
      return '/scratch/ywang/real_runs'

  #enddef fetchDefaultWorkingDir

  ######################################################################

  @staticmethod
  def fetchNCARGRoot() :

      import os

      ncargpath = os.environ.get('NCARG_ROOT')

      if ncargpath is None:
        #ncargpath = '/scratch/software/NCL/default'    # Odin
        ncargpath = '/apps/ncl/6.1.2/intel.bin/ncl'     # Jet

      return ncargpath

  #enddef fetchDefaultWorkingDir

#endclass
