#!/bin/bash


source /etc/profile.d/modules.sh
module load contrib anaconda


origdir="/lfs3/projects/hpc-wof1/afierro/news3dvar"
wrkdir="/lfs3/projects/hpc-wof1/ywang/Alex"
srcdir="/lfs3/projects/hpc-wof1/ywang/news3dvar"

cases=(wrfctrl  wrf_GLM  wrf_RAD  wrf_RAD_GLM)
#cases=(wrfctrl  wrf_GLM_LCL2km  wrf_RAD_REFOPT1  wrf_RAD_GLM)
dates=(20180531)
#dates=(20180530 20180531)
fields=(ref pcp)

#
# 1. Prepare WRF forecast directory and files
#
if [[ $1 -eq 1 ]]; then
  for fcase in ${cases[@]}; do
    for date in ${dates[@]}; do

      cd $wrkdir/fcsts
      #if [[ -r $fcase ]]; then
      #  rm -rf $fcase
      #fi
      mkdir -p $fcase/$date/0000Z/dom20/wrf5
      cd $fcase/$date/0000Z/dom20/wrf5

      for filename in $origdir/$date/0000Z/dom10/$fcase/wrfout_d01*; do
        ln -s $filename .
        filen=$(basename $filename)
        touch ${filen/wrfout/wrfoutReady}
      done
      
    done
  done
fi

#
# 2. Interpolate MRMS to model grid
#
if [[ $1 -eq 2 ]]; then
  for date in ${dates[@]}; do
    cd ${srcdir}/scripts
    for field in ${fields[@]}; do
      ./verif.py -p minterp -s ${srcdir} -d $wrkdir/fcsts/${cases[0]} -w $wrkdir/mrms_data -m $wrkdir/mrms_data ${date}0000 $field
    done
  done
fi

#
# 3. Compute and plot verification scores
#
if [[ $1 -eq 3 ]]; then
  for fcase in ${cases[@]}; do
    for date in ${dates[@]}; do

      cd $wrkdir/verif
      if [[ ! -r $fcase ]]; then
        mkdir -p $fcase
      fi

      #ls $origdir/$date/0000Z/dom10/$fcase
      cd ${srcdir}/scripts
      for field in ${fields[@]}; do
        ./verif.py -r -s ${srcdir} -d $wrkdir/fcsts/$fcase -b $wrkdir/mrms_data -w $wrkdir/verif/$fcase ${date}0000 $field
      done

    done
  done
fi

#
# 4. plot obs comref
#
if [[ $1 -eq 4 ]]; then
  for date in ${dates[@]}; do
    cd ${srcdir}/scripts
    for field in ${fields[@]}; do
      ./verif.py -r -p plt_obs -s ${srcdir} -d $wrkdir/fcsts/${cases[0]} -w $wrkdir/mrms_data -m $wrkdir/mrms_data ${date}0000 $field
    done
  done
fi

#
# 5. Plot fcst comref
#
if [[ $1 -eq 5 ]]; then
  for fcase in ${cases[@]}; do
    for date in ${dates[@]}; do

      #ls $origdir/$date/0000Z/dom10/$fcase
      cd ${srcdir}/scripts
      for field in ${fields[@]}; do
        ./verif.py -r -p plt_fcst -s ${srcdir} -d $wrkdir/fcsts/$fcase -b $wrkdir/mrms_data -w $wrkdir/verif/$fcase ${date}0000 $field
      done

    done
  done
fi


#
# 6. tar all the plots
#
if [[ $1 -eq 6 ]]; then
cd ${wrkdir}
  for fcase in ${cases[@]}; do
    for date in ${dates[@]}; do
    
    dir=${date}0000Z
    if [ ! -e $dir ]; then
    mkdir ${date}0000Z
    fi
    mkdir ${date}0000Z/$fcase
    cp -rf ${wrkdir}/verif/$fcase/$date/0000Z/*png ${date}0000Z/$fcase 
    

    done
  done
   
# obs:
   for date in ${dates[@]}; do
   mkdir ${date}0000Z/OBS
   cp -rf ${wrkdir}/mrms_data/$date/0000Z/*png ${date}0000Z/OBS
   done

tar -zcvf ${date}0000.tgz ${date}0000Z  

fi


module unload contrib anaconda 
