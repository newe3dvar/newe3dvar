#!/usr/bin/env python
## ---------------------------------------------------------------------
##
## This is a python library to submit jobs to SLURM scheduler
##
## ---------------------------------------------------------------------
##
## HISTORY:
##   Yunheng Wang (07/12/2016)
##   Initial version.
##
##
########################################################################
##
## Requirements:
##
########################################################################

import os, sys, time, re
import subprocess

from configBase import *

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class configurator (baseConfigurator) :
  #classbegin
  '''
  this is a specific computer-dependent configuration for Schooner
  A supercomputer hosted at OSCER of OU.
  '''

  ##----------------------- Initialization      -----------------------
  def __init__(self,wrkdir,norun,debug) :
    baseConfigurator.__init__(self,wrkdir,norun,debug)
    self.hardncpn  = 20    ## constant hardward number cores per node
    self.defaultQueue = 'normal'
    #self.jobstatuscmd = self.get_job_status
    self.hostname  = 'SCHOONER'
    self.schjobext = 'slurm'
    #self.needcopy = True
  #enddef

  ##----------------------- Ending of the module -----------------------
  def finalize(self) :
    baseConfigurator.finalize(self)
  #enddef

  ######################################################################

  def run_a_program(self,executable,nmlfile,outfile,wrkdir,jobconf,inarg=None):
    '''
      Generate job script for a task and submit it

      nmlfile : namelist file can be None
      outfile : must provide a output file name
      wrkdir  : the program can be run in a directory different from the
                instance attribute of wrkdir
    '''

    if self.cmdmutex.acquire():
      self.serieno += 1
      myserieno = self.serieno
      self.cmdmutex.release()

    #cwdsaved = os.getcwd()
    #os.chdir(wrkdir)

    cmdstr = executable
    if nmlfile :
      if inarg is None:
        cmdstr += " %s" % nmlfile
      elif inarg == 'STDIN':
        cmdstr += " < %s" % nmlfile
      else:
         cmdstr += " %s %s" % (inarg,nmlfile)

    if outfile :
      (jobname,ext) = os.path.splitext(outfile)
      jobname = os.path.basename(jobname)
      cmdstr += " > %s" % outfile
    else :
      jobname = os.path.basename(executable)

    if jobconf.mpi :

      claimncpn = min(jobconf.claimncpn,self.hardncpn)
      nprocs = jobconf.nproc_x*jobconf.nproc_y
      cmdstr = 'mpirun -n %d %s' % (nprocs, cmdstr)

    else :
      nprocs = 1
      claimncpn = 1

    if jobconf.shell:

      self.addlog(0,self.hostname, 'In <%s>, executing:\n    $> %s\n' % (wrkdir,cmdstr))
      job = JobID(jobname,wrkdir,self,myserieno)
      job['status'] = subprocess.call(cmdstr,shell=True,cwd=wrkdir)

      if job['status'] == 0: subprocess.call('touch done.%s.%d' %(jobname,myserieno),shell=True,cwd=wrkdir)
      else:                  subprocess.call('touch error.%s.%d'%(jobname,myserieno),shell=True,cwd=wrkdir)

    else:

      (hour,minute) = divmod(jobconf.claimmin,60)
      timestr = '%02d:%02d:00' % (hour,minute)

      if jobconf.jobqueue == "wof":
         ldpath = "export LD_LIBRARY_PATH=/condo/wofwof/ywang/tools/zlib-1.2.11/lib/:/condo/wofwof/ywang/tools/netcdf-4.4/lib:/condo/wofwof/ywang/tools/hdf5-1.10.1/lib:${LD_LIBRARY_PATH}"
      else:
         ldpath = ""

      excstr = "#SBATCH "
      if jobconf.exclusive:
        excstr = "#SBATCH --exclusive"

      scriptstr = '''#!/bin/bash
          #SBATCH --partition=%(queue)s
          #SBATCH --job-name=%(jobname)s
          %(exclusivestr)s
          #SBATCH --ntasks=%(nprocs)d
          #SBATCH --ntasks-per-node=%(claimncpn)d
          #SBATCH --time=%(claimtime)s
          #SBATCH --output=%(wrkdir)s/%(jobname)s_%%J.out
          #SBATCH --error=%(wrkdir)s/%(jobname)s_%%J.err
          #SBATCH --workdir=%(wrkdir)s
          #

          time1=$(date '+%%s')
          echo "Job Started: $(date). Job Id:  $SLURM_JOBID"
          echo " "

          # Change to the directory that you want to run in.
          #
          cd %(wrkdir)s

          rm -rf %(outfile)s

          %(ldpath)s

          #
          # Make sure you're in the correct directory.
          #
          pwd
          #
          # Run the job, redirecting input from the given file.  The date commands
          # and the time command help track runtime and performance.
          #

          set echo on

          %(cmdstr)s

          if [[ $? -eq 0 ]]; then
            touch done.%(jobname)s.${SLURM_JOBID}
          else
            touch error.%(jobname)s.${SLURM_JOBID}
          fi

          set echo off

          time2=$(date '+%%s')

          let diff=time2-time1
          let hour=diff/3600
          let diff=diff%%3600
          let min=diff/60
          let sec=diff%%60

          echo -n "Job   Ended: $(date). "
          printf 'Job run time:  %%02d:%%02d:%%02d' $hour $min $sec
          echo " "

          ''' %{ 'wrkdir' : wrkdir, 'jobname' : jobname,
                 'cmdstr' : cmdstr, 'outfile' : outfile,
                 'queue'  : jobconf.jobqueue or self.defaultQueue,
                 'claimtime'   : timestr,
                 'nprocs'      : nprocs, 'claimncpn' : claimncpn,
                 'exclusivestr': excstr, 'ldpath' : ldpath
                }

      scriptfile = '%s.%s' % (jobname,self.schjobext)
      fullscript = os.path.join(wrkdir,scriptfile)

      batchFile = open(fullscript,'w')
      batchFile.write(self.trim(scriptstr))
      batchFile.close()

      self.addlog(0,self.hostname,'''- %02d - Jobscript "%s" generated.''' %(
                        myserieno,scriptfile )  )

      if self.showonly :
        self.addlog(0,self.hostname, 'Preparing job script "%s" in %s.' % (scriptfile,wrkdir))
        job = JobID(jobname,wrkdir,self)
        job['status'] = 0
      else :
        job = self.submit_a_job(jobname,wrkdir,maxtry=3)
        job.update_status()

    #os.chdir(cwdsaved)

    return job
  #enddef run_a_program

  ######################################################################

  def run_ncl_plt(self,executable,nmlfile,field,wrkdir,fminu,tmpldir,
                  jobconf,transfer=False):
    '''
      Plot using NCL and convert to PNG file

      nmlfile : namelist file can be None
      outfile : must provide a output file name
      wrkdir  : the program can be run in a directory different from the
                instance attribute of wrkdir
    '''
    pngfile = "%s-%03d.png"  % (field,fminu)

    if self.cmdmutex.acquire():
      self.serieno += 1
      myserieno = self.serieno
      self.cmdmutex.release()

    #cwdsaved = os.getcwd()
    #os.chdir(wrkdir)

    cmdstr = executable
    if nmlfile :
      cmdstr += " %s" % nmlfile

    (jobname,ext) = os.path.splitext(nmlfile)
    jobname = os.path.basename(jobname)

    if jobconf.mpi :

      claimncpn = min(jobconf.claimncpn,self.hardncpn)
      nprocs = jobconf.nproc_x*jobconf.nproc_y
      cmdstr = 'mpirun -n %d %s' % (nprocs, cmdstr)

      (nodes,extra) = divmod(nprocs,claimncpn)
      if extra > 0 :
        nodes += 1

      nprocs = nodes*self.hardncpn

    else :
      nprocs = 1
      claimncpn = 1
      nodes  = 1

    (hour,minute) = divmod(jobconf.claimmin,60)
    timestr = '%02d:%02d:00' % (hour,minute)

    runpart  = jobconf.jobqueue or self.defaultQueue

    excstr = "#SBATCH "
    if jobconf.exclusive:
      excstr = "#SBATCH --exclusive"

    scriptstr = '''#!/bin/bash
        #SBATCH --partition=%(queue)s
        #SBATCH --job-name=%(jobname)s
        %(exclusivestr)s
        #SBATCH --ntasks=%(nprocs)d
        #SBATCH --ntasks-per-node=%(claimncpn)d
        #SBATCH --mem=2048
        #SBATCH --time=%(claimtime)s
        #SBATCH --output=%(wrkdir)s/%(jobname)s_%%j.out
        #SBATCH --error=%(wrkdir)s/%(jobname)s_%%j.err
        #SBATCH --workdir=%(wrkdir)s
        #

        time1=$(date '+%%s')
        echo "Job Started: $(date). Job Id:  $SLURM_JOBID"
        echo " "

        cd %(wrkdir)s

        export NCARG_RANGS=%(SZMBTmpl)s/NCLRANGS

        set echo on

        %(cmdstr)s

        set echo off

        touch done.%(jobname)s.${SLURM_JOBID}

        time2=$(date '+%%s')

        let diff=time2-time1
        let hour=diff/3600
        let diff=diff%%3600
        let min=diff/60
        let sec=diff%%60

        echo -n "Job   Ended: $(date). "
        printf 'Job run time:  %%02d:%%02d:%%02d' $hour $min $sec
        echo " "

        ''' %{ 'wrkdir' : wrkdir,    'jobname'    : jobname,
               'cmdstr' : cmdstr,    'claimtime'  : timestr,
               'queue'  : jobconf.jobqueue or self.defaultQueue,
               'nprocs'   : nprocs,  'claimncpn' : claimncpn,
               'SZMBTmpl' : tmpldir, 'exclusivestr': excstr
              }

    scriptfile = '%s.%s' % (jobname,self.schjobext)
    scriptfull = os.path.join(wrkdir,scriptfile)

    batchFile = open(scriptfull,'w')
    batchFile.write(self.trim(scriptstr))
    batchFile.close()

    self.addlog(0,self.hostname,'''- %02d -  Jobscript "%s" generated.''' %(
                      myserieno,scriptfile )  )

    if self.showonly :
      self.addlog(0,"jet",'Preparing job script "%s" in %s.' % (scriptfile,wrkdir))
      job = JobID(jobname,wrkdir,self)

    else :

      if runpart == "interactive":
          retcode = subprocess.call(['bash', scriptfile],cwd=wrkdir,shell=True,
                        stdin=None,stdout=open(outfile,'w'),stderr=subprocess.STDOUT)
          job = JobID(jobname,wrkdir,self)
          job["status"] = 0
      else:
          job = self.submit_a_job(jobname,wrkdir)
          job.update_status()

    #os.chdir(cwdsaved)

    return job
  #enddef run_ncl_plt


  ######################################################################

  def run_unipost(self,executable,nmlfile,outfile,wrkdir,postdir,ndate,ifhr,jobconf):
    '''
      Generate job script for a unipost and submit it
      Same as run_a_program, just add run of copygb.exe with two extra
      arguments postdir & ndate

      nmlfile : namelist file can be None
      outfile : must provide a output file name
      wrkdir  : the program can be run in a directory different from the
                instance attribute of wrkdir
    '''

    if self.cmdmutex.acquire():
      self.serieno += 1
      myserieno = self.serieno
      self.cmdmutex.release()

    cwdsaved = os.getcwd()
    os.chdir(wrkdir)

    cmdstr = executable
    if nmlfile :
      cmdstr += " < %s" % nmlfile

    if outfile :
      (jobname,ext) = os.path.splitext(outfile)
      jobname = os.path.basename(jobname)
      #cmdstr += " > %s" % outfile
    else :
      jobname = os.path.basename(executable)

    if jobconf.mpi :

      claimncpn = min(jobconf.claimncpn,self.hardncpn)
      nprocs = jobconf.nproc_x*jobconf.nproc_y
      cmdstr = 'mpirun -n %d %s' % (nprocs, cmdstr)

    else :
      nprocs = 1

    (hour,minute) = divmod(jobconf.claimmin,60)
    timestr = '%02d:%02d:00' % (hour,minute)

    excstr = "#SBATCH "
    if jobconf.exclusive:
      excstr = "#SBATCH --exclusive"

    ##fhr = ndate.split('_')[1].split(':')[0]
    fhr = "%02d"%ifhr

    scriptstr = '''#!/bin/bash
        #SBATCH --partition=%(queue)s
        #SBATCH --job-name=%(jobname)s
        %(exclusivestr)s
        #SBATCH --ntasks=%(nprocs)d
        #SBATCH --ntasks-per-node=%(claimncpn)d
        #SBATCH --mem=2048
        #SBATCH --time=%(claimtime)s
        #SBATCH --output=%(wrkdir)s/%(jobname)s_%%J.out
        #SBATCH --error=%(wrkdir)s/%(jobname)s_%%J.err
        #SBATCH --workdir=%(wrkdir)s
        #

        time1=$(date '+%%s')
        echo "Job Started: $(date). Job Id:  $SLURM_JOBID"
        echo " "

        # Change to the directory that you want to run in.
        #
        cd %(wrkdir)s

        rm -rf %(outfile)s

        #
        # Make sure you're in the correct directory.
        #
        pwd
        #
        # Run the job, redirecting input from the given file.  The date commands
        # and the time command help track runtime and performance.
        #

        set echo on

        %(cmdstr)s

        if [[ $? -eq 0 ]]; then
          touch done.%(jobname)s.${SLURM_JOBID}
        else
          touch error.%(jobname)s.${SLURM_JOBID}
        fi

        #if [[ -s "copygb_hwrf.txt" && -s "WRFPRS.GrbF%(fhr)s" ]]; then
        #  read nav < 'copygb_hwrf.txt'
        #  srun -n 1 %(postdir)s/copygb.exe -xg "${nav}" WRFPRS.GrbF%(fhr)s nsslvar_prd_%(ndate)s.grb
        #  rm WRFPRS.GrbF%(fhr)s
        #fi

        set echo off

        time2=$(date '+%%s')

        let diff=time2-time1
        let hour=diff/3600
        let diff=diff%%3600
        let min=diff/60
        let sec=diff%%60

        echo -n "Job   Ended: $(date). "
        printf 'Job run time:  %%02d:%%02d:%%02d' $hour $min $sec
        echo " "

        ''' %{ 'wrkdir' : wrkdir, 'jobname' : jobname,
               'cmdstr' : cmdstr, 'outfile' : outfile,
               'queue'  : jobconf.jobqueue or self.defaultQueue,
               'claimtime'  : timestr,
               'nprocs' : nprocs, 'claimncpn' : claimncpn,
               'exclusivestr': excstr,
               'postdir': postdir,'ndate'     : ndate, 'fhr' : fhr
              }

    scriptfile = '%s.%s' % (jobname,self.schjobext)
    scriptfull = os.path.join(wrkdir,scriptfile)

    batchFile = open(scriptfull,'w')
    batchFile.write(self.trim(scriptstr))
    batchFile.close()

    self.addlog(0,self.hostname,'''- %02d -  Jobscript "%s" generated.''' %(
                      myserieno,scriptfile ) )

    if self.showonly :
      self.addlog(0,self.hostname,'Preparing job script "%s" in %s.' % (scriptfile,wrkdir))

      job = JobID(jobname,wrkdir,self)
      job['status'] = 0

    else :

      job = self.submit_a_job(jobname,wrkdir)
      job.update_status()

    #os.chdir(cwdsaved)

    return job
  #enddef run_unipost

  ##====================================================================

  def submit_a_job(self,jobname,wrkdir,djobs=[],maxtry=1) :
    '''
    submit a job to SLURM on Schooner
    '''

    jobfile = os.path.join(wrkdir,'%s.%s'%(jobname,self.schjobext))

    if djobs :
      bjoblst = ['sbatch', '-d', 'afterok:%s' % ':'.join(djobs), jobfile]
    else :
      bjoblst = ['sbatch', jobfile]

    ##retout = subprocess.check_output(bjobstr,stdin=None,stderr=subprocess.STDOUT,cwd=wrkdir)
    retout = subprocess.Popen(bjoblst, stdout=subprocess.PIPE,cwd=wrkdir).communicate()[0]
    #print retout

    ## get jobid here
    jobidre = re.compile(r'Submitted batch job (\d+)')
    retlist = retout.split('\n')
    jobid = None
    for retstr in retlist :
      jobidmatch = jobidre.match(retstr)
      #print retstr, jobidmatch
      if jobidmatch :
        jobid = jobidmatch.group(1)
        break

    if jobid is None :
      print >> sys.stderr, 'Something is wrong with job (%s) submitting? Get: "%s".'%(jobname,retout)
      self.addlog(1,self.hostname,'  Job <%s> is not submitted correctly.' % (jobname,)  )
      self.addlog(-1,self.hostname,'    $ %s\n    > "%s"' % ( ' '.join(bjoblst), retout )  )
      #return False
      return None
    else:
      job = JobID(jobname,wrkdir,self,jobid,maxtry,self.jobstatuscmd)

      self.addlog(  0,self.hostname,'Submitted job <%s> with jobid <%s>.' % (job.name,job.id)  )
      self.addlog(999,self.hostname,'    $ %s' % ( ' '.join(bjoblst) )  )
      self.addlog(999,self.hostname,'    > %s' % ( retout )  )

      return job

  #enddef submit_a_job

  ##====================================================================

  def get_job_status(self,jobid) :
    '''
    Check job status using SQUEUE & SACCT
    '''

    if jobid is None :
      return 0

    #retout = subprocess.Popen(['squeue', '-j','%s' % jobid ],
    #                       stdout=subprocess.PIPE).communicate()[0]
    pipe = subprocess.PIPE
    #queryarg = ['squeue','-h','-o','"%10i %20u %.2t"','-j',jobid ]
    queryarg = ['squeue','-h','-o','"%10i %.2t"','-j',jobid ]
    outs  = subprocess.Popen(queryarg,stdout=pipe,stderr=pipe).communicate()
                                #sacct -j jobid -o "JobID,User,state"
    retout = outs[0]
    reterr = outs[1]
    #self.addlog(1,retout)
    timeoutre = re.compile(r'Socket timed out on send/recv operation' )
    while timeoutre.search(reterr):
      #print "STDOUT: %s"%retout
      #print "STDERR: %s"%reterr
      time.sleep(10)
      outs   = subprocess.Popen(queryarg,stdout=pipe,stderr=pipe).communicate()
      retout = outs[0]
      reterr = outs[1]
    #print retout
    ## get job status here

    #if self.debug :   ## debugging outputs
    #  if self.firstqstat :
    #    print retlist[0]
    #    print retlist[1]
    #    self.firstqstat = False
    #  else :
    #    print retlist[1]
    #jobstatre = re.compile(r'"%s +%s +(\w{1,2})"' % (jobid,os.getenv('USER')) )
    jobstatre = re.compile(r'"%s +(\w{1,2})"' % jobid )
    jobstatmatch = jobstatre.match(retout)
    if jobstatmatch :
      status = jobstatmatch.group(1)
      #self.addlog(1,'get job status %s'%status)
      if status == "CD" :  # (completed)
        jobstatus = 0
      elif status == "PD" :
        jobstatus = 1
      elif status == "R" :
        jobstatus = 2
      elif status == 'CA':  # (cancelled),
        jobstatus = 3
      elif status == 'CF':  # (configuring),
        jobstatus = 4
      elif status == 'CG':  # (completing),
        jobstatus = 5
      elif status == 'F':   # (failed),
        jobstatus = 6
      elif status == 'TO':  # (timeout)
        jobstatus = 7
      elif status == 'NF':  # (node failure).
        jobstatus = 8
      else :
        jobstatus = 9

      return jobstatus
    else :
      queryarg = ['sacct',  '-o', "JobID,state", '-n','-j', jobid]
      outs   = subprocess.Popen(queryarg,stdout=pipe,stderr=pipe).communicate()
      retout = outs[0]
      reterr = outs[1]
      #print retout
      #print reterr

      while timeoutre.search(reterr):
        #print "STDOUT: %s"%retout
        #print "STDERR: %s"%reterr
        time.sleep(10)
        outs   = subprocess.Popen(queryarg,stdout=pipe,stderr=pipe).communicate()
        retout = outs[0]
        reterr = outs[1]

      retlst = retout.split('\n')
      #print retlst[0]
      jobstatre = re.compile(r'%s +(\w+)' % jobid )
      jobstatmatch = jobstatre.match(retlst[0])
      if jobstatmatch:
        status = jobstatmatch.group(1)
        if status == 'COMPLETED':
          jobstatus = 0
        elif status == 'PENDING':
          jobstatus = 1
        elif status == 'RUNNING':
          jobstatus = 2
        else:
          jobstatus = 9
      else:
          self.addlog(-1,self.hostname,'job status is not matched, "%s"'%retlst[0])
          jobstatus = None

      return jobstatus

  #enddef get_job_status

  ######################################################################

  @staticmethod
  def fetchDefaultWorkingDir(tests=False) :

    if tests :
      return '/scratch/ywang/test_runs'
    else :
      return '/scratch/ywang/real_runs'

  #enddef fetchDefaultWorkingDir

  ######################################################################

  @staticmethod
  def fetchNCARGRoot() :

      import os

      ncargpath = os.environ.get('NCARG_ROOT')

      if ncargpath is None:
        ncargpath = '/home/ywang/tools/ncl_ncarg-6.3.0/bin/ncl'

      return ncargpath

  #enddef fetchDefaultWorkingDir

#endclass configurator

if __name__ == "__main__":
  #
  # Test get job status
  #
  statusmap = { 0: 'COMPLETED', 1: 'PENDING', 2: 'RUNNING', 9: 'UNKNOWN'}

  cmd=configurator("./",False,False)
  a = cmd.get_job_status(sys.argv[1])
  print "%d -> %s"%(a,statusmap[a])
