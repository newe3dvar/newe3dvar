#!/bin/env python
## ---------------------------------------------------------------------
## This software is in the public domain, furnished "as is", without
## technical support, and with no warranty, express or implied, as to
## its usefulness for any purpose.
## ---------------------------------------------------------------------
##
## This is a python program that convert NCARG NCGM meta file to an image
##
##----------------------------------------------------------------------
##
## It requires NCARG and ImageMagick
##
## ---------------------------------------------------------------------
##
## HISTORY:
##  Yunheng Wang (06/12/2012)
##  Initial version.
##
##
########################################################################

import re, os, sys
import subprocess, getopt

##======================================================================
## USAGE
##======================================================================
def usage( istatus = 0 ) :
  '''-------------------------------------------------------------------
  Print Usage and then exit
  -------------------------------------------------------------------'''

  version  = '1.0'
  lastdate = '2012.06.12'

  print >> sys.stderr, """\n  Usage: %s [options] [options] gmeta_files\n
     \tgmeta_files\t gmeta files to be converted
     \t
     \tNote: The gmeta file name cannot begin with any digits. Otherwise,
     \t      command "med" will fail. I think it will be ok as long as
     \t      you follow C conventions with varaible name for gmeta file name.
     \n  OPTIONS:\n
     \tOption \t\tDefault \tInstruction
     \t-------\t\t--------\t-----------
     \t-v, --verbose      \t \tVerbose
     \t-h, --help         \t \tPrint this help
     \t-d device\t sun    \tDevice to be used for command "med". For example:
     \t         \t        \tsun, xwd etc.
     \t-f frames\t        \tframes to be extracted. It can be the following:
     \t         \t -f 3       = extract frame no. 3
     \t         \t -f 3,4,5   = extract frame 3, 4 and 5
     \t         \t -f 3-5     = extract frame from 3 to 5
     \t         \t -f 3-5,8   = extract frame 3 to 5 and frame 8\n
     \t-t ext   \t gif     \tPicture format desired. For example:
     \t         \t         \tgif, png, jpg etc.
     \t-r ####x####\t      \t\tResolution of the desired picture
     \t-c ####x####{+-}LEFT{+-}TOP  \tCrop size for command "convert".
     \t-n       \t         \tOutput file name.
     \t-i       \t         \tInterval in seconds between each frame
     \t-o       \t         \tOutput directory.
     """ % os.path.basename(cmd)
  print >> sys.stderr, '  For questions or problems, please contact yunheng@ou.edu (v%s, %s).\n' % (version,lastdate)

  sys.exit(istatus)
#enddef

##======================================================================
## Parse command line arguments
##======================================================================

def parseArgv(argv) :
  '''-------------------------------------------------------------------
  Parse command line arguments
  -------------------------------------------------------------------'''
  global _debug

  argsdict = { 'framestr'   : 0,
               'interval'   : 0,
               'mdevice'    : 'sun',
               'ext'        : 'png',
               'resolution' : '1000x1000',
               'crop'       : '1000x1000+0+0',
               'outdir'     : '',
               'framenames' : []
              }

  argsdict['files'] = []
  try:
    opts, args = getopt.getopt(argv,'hvd:f:t:r:c:n:o:i:',['help','verbose'
                  'device','frames','ext','resolution','crop','name','outdir'])

    for opt, arg in opts :
      if opt in ('-h','--help'):
        usage(0)
      elif opt in ( '-v', '--verbose'):
        _debug = True
      elif opt in ( '-d', '--device'):
        if arg not in ('sun','xwd') :
          print >> sys.stderr, 'ERROR: unsupported device (%s).' % arg
          usage(3)
        argsdict['mdevice']=arg
      elif opt in ( '-f', '--frames'):
        framere = re.compile(r'^[0-9,-]+$')
        if not framere.match(arg) :
          print >> sys.stderr, 'ERROR: unsupported frames (%s).' % arg
          usage(3)
        argsdict['framestr']=arg
      elif opt in ( '-t', '--ext'):
        if arg not in ('gif','png','jpg') :
          print >> sys.stderr, 'ERROR: unsupported format (%s).' % arg
          usage(3)
        argsdict['ext']=arg
      elif opt in ( '-r', '--resolution'):
        resre = re.compile(r'^\d+x\d+$')
        if not resre.match(arg) :
          print >> sys.stderr, 'ERROR: unsupported resolution (%s).' % arg
          usage(3)
        argsdict['resolution']=arg
      elif opt in ( '-c', '--crop'):
        cropre = re.compile(r'^\d+x\d+[+-]\d+[+-]\d+$')
        if not cropre.match(arg) :
          print >> sys.stderr, 'ERROR: unsupported crop format (%s).' % arg
          usage(3)
        argsdict['crop']=arg
      elif opt in ( '-o','--outdir') :
        argsdict['outdir']=arg
      elif opt in ( '-n','--name'):
        argsdict['framenames'] = arg.split(',')
      elif opt in ( '-i', '--interval') :
        intre = re.compile(r'\d+')
        if not intre.match(arg) :
          print >> sys.stderr, 'ERROR: Wrong format (%s) for interval.' % arg
          usage(3)
        argsdict['interval']=int(arg)

  except getopt.GetoptError:
    print >> sys.stderr, 'ERROR: Unknown option.'
    usage(2)

  if (len(args) < 1 ) :
    print >> sys.stderr, 'ERROR: wrong number of command line arguments.'
    usage(1)
  else :
    for arg in args :
      argsdict['files'].append(arg)

  if not argsdict['framenames'] :
    argsdict['framenames'] = [ os.path.splitext(os.path.basename(file))[0] for file in argsdict['files'] ]

  return argsdict
#enddef

##%%%%%%%%%%%%%%%%%%%%%%%  main program here  %%%%%%%%%%%%%%%%%%%%%%%%%%

def main( framestr,mdevice,ext,resolution,crop,outdir,files,framenames,interval=0) :
  '''
  Convert NCARG ncgm meta file to a pixel images
  '''

  framesin = []
  if framestr:
    if re.match(r'^(\d+)(,\d+)*$',framestr) :
      framesin = [int(el) for el in framestr.split(',')]
    elif re.match(r'^((\d+,)*(\d+-\d+)(,\d+)*)(,(\d+,)*(\d+-\d+)(,\d+)*)*$',framestr) :
      rframes = framestr.split(',')
      for el in rframes :
        rmatch = re.match(r'^(\d+)-(\d+)$',el)
        if rmatch:
          low  = int(rmatch.group(1))
          high = int(rmatch.group(2))
          framesin.extend(range(low,high+1))
        else :
          framesin.append(int(el))

  if not outdir:
    outdir = os.getcwd()
  else :
    if not os.path.lexists(outdir) :
      os.mkdir(outdir)

  fhourre = re.compile(r'.+_(\d{6})')
  ## process each file
  i = 0
  for fl in files :

    (flbase,flext) = os.path.splitext(fl)
    mfhour = fhourre.match(flbase)
    if mfhour :
      fhour = mfhour.group(1)
    else :
      fhour = ''

    execncgm    = '%s/ncgmstat -c %s' % (ncargpath, fl)

    ncgmout = subprocess.check_output(execncgm.split(),stdin=None,
                           stderr=subprocess.STDOUT,shell=None)
    num = int(ncgmout)
    if _debug : print "Get %d frames from %s." % (num,fl)

    if not framesin :
      frames = range(1,num+1)
    else :
      frame = framesin

    execmed = ['%s/med' % ncargpath, '-e', "split%d %s_" % ( num,flbase), fl]
    if _debug : print "Executing command %s." % (' '.join(execmed))
    retcode = subprocess.call(execmed,stdin=None,stdout=None,
                          stderr=subprocess.STDOUT,shell=None)

    for m in frames :

        if num > 1 :
          if interval > 0 :
            mf = int(fhour)/interval - num + m + 1
            pngfile = '%s/%s-%03d.%s' %(outdir,framenames[i],mf,ext)
          else :
            pngfile = '%s/%s%s-%03d.%s' %(outdir,framenames[i],fhour,m,ext)
        else :
          pngfile = '%s/%s%s.%s' %(outdir,framenames[i],fhour,ext)

        tmpncgm = '%s_%03d.ncgm' % (flbase,m)

        exectrans  = '%s/ctrans -f font4 -d %s -resolution %s %s' % (ncargpath, mdevice,resolution,tmpncgm)
        if _debug : print "Executing command %s." % (exectrans)
        ctransP = subprocess.Popen(exectrans.split(),stdin=None,stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT,shell=None)

        execonvert = 'convert -crop %s - %s' % (crop,pngfile)
        if _debug : print "Executing command %s." % (execonvert)
        convertP = subprocess.Popen(execonvert.split(),stdin=ctransP.stdout,stdout=None,
                         stderr=subprocess.STDOUT,shell=None)

        convertP.wait()

        ctransP.stdout.close()

        os.unlink(tmpncgm)
    i += 1

#enddef

#############################  Portral   ###############################
if __name__ == "__main__":

##----------------------------------------------------------------------
##
## Global variables are:  _debug, cmd
##
##----------------------------------------------------------------------

  _debug = False

  cmd  = sys.argv[0]
  argsdict  = parseArgv(sys.argv[1:])

  ## NCARG Path
  ##ncargpath = os.getenv('NCARG_ROOT',None)
  ##ncargpath = os.environ['NCARG_ROOT']    ## will raise a `KeyError`
  ncargpath = os.environ.get('NCARG_ROOT')  ## will return `none` if key is not present

  if not ncargpath :
    #print >> sys.stderr, 'ERROR: NCARG_ROOT is not set.'
    #sys.exit(0)
    ncargpath = '/export/home/caps/tools/ncl_ncarg-6.1.0/bin'
  else :
    ncargpath = os.path.join(ncargpath,'bin')

  ##try:
  main(**argsdict)
  ##except :
  ##  print >> sys.stderr, 'ERROR: exception catched.'
