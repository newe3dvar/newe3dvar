#!/bin/bash

function usage {
    echo " "
    echo "    USAGE: $0 [options] [DATE_STR]"
    echo " "
    echo "    PURPOSE: Get a list about the LDM data from the CAPS server"
    echo "             that will be used for the <DATE_STR>."
    echo " "
    echo "    OPTIONS:"
    echo "              -h              Display this message"
    echo "              -n              Show command to be run only"
    echo "              -v              Verbose mode"
    echo "              -s     x        File is supposed ready if no change for x seconds"
    echo "                              default: 10 seconds"
    echo "              -src   dir      Source directory"
    echo "              -rad   [radars] Get RADAR data, only specified radars,"
    echo "                              For example, KTLX,KATX,KCLE "
    echo "                              or all radars if nothing is given."
    echo "              -ruc13          Get RUC13 data"
    echo "              -range a,b      Search all files within relative time range [a,b], a < b."
    echo "                              -x,        before <DATE_STR>"
    echo "                              +x or x,   after <DATE_STR>"
    echo "                              in minutes for RADAR data and hours for RUC13 data"
    echo " "
    echo "    DATE_STR: Date string in YYYYMMDDHHMM format"
    echo "              Use current UTC time if missing."
    echo " "
    echo "                                     -- By Y. Wang (2016.04.21)"
    exit $1
}


srcroot="/arpsdata/ldm1/datafiles"
#user="jdgao"
#host="tinman.ou.edu"

radars=( DOP1  KARX  KBOX  KCLE  KDGX  KEAX  KEYX  KFWS  KGWX  KICX  KJGX  KLRX  KMBX  KMSX  KOHX  KRAX  KSJT  KTYX  KYUX  PGUA
         FOP1  KATX  KBRO  KCLX  KDIX  KEMX  KFCX  KGGW  KGYX  KILN  KJKL  KLSX  KMHX  KMTX  KOKX  KRGX  KSOX  KUDX  PABC  PHKM
         KABR  KBBX  KBUF  KCRP  KDLH  KENX  KFDR  KGJX  KHDX  KILX  KLBB  KLTX  KMKX  KMUX  KOTX  KRIW  KSRX  KUEX  PACG  PHMO
         KABX  KBGM  KBYX  KCXX  KDMX  KEOX  KFDX  KGLD  KHGX  KIND  KLCH  KLVX  KMLB  KMVX  KPAH  KRLX  KTBW  KVAX  PAEC  PHWA
         KAKQ  KBHX  KCAE  KCYS  KDOX  KEPZ  KFFC  KGRB  KHNX  KINX  KLGX  KLWX  KMOB  KMXX  KPBZ  KRTX  KTFX  KVBX  PAHG  RKJK
         KAMA  KBIS  KCBW  KDAX  KDTX  KESX  KFSD  KGRK  KHPX  KIWA  KLIX  KLZK  KMPX  KNKX  KPDT  KSFX  KTLH  KVNX  PAIH  RKSG
         KAMX  KBLX  KCBX  KDDC  KDVN  KEVX  KFSX  KGRR  KHTX  KIWX  KLNX  KMAF  KMQT  KNQA  KPOE  KSGF  KTLX  KVTX  PAKC  TJUA
         KAPX  KBMX  KCCX  KDFX  KDYX  KEWX  KFTG  KGSP  KICT  KJAX  KLOT  KMAX  KMRX  KOAX  KPUX  KSHV  KTWX  KVWX  PAPD )

#-----------------------------------------------------------------------
#
# Handle command line arguments
#
#-----------------------------------------------------------------------

show=""
verbose=0
readys=10

radar=0
ruc13=0

while [[ $# > 0 ]]
do
    key="$1"

    case $key in
        -n)
            show="echo"
            ;;
        -h)
            usage 0
            ;;
        -v)
            verbose=1
            ;;
        -src)
            srcroot="$2"
            shift      # past argument
            ;;
        -s)
            readys="$2"
            shift      # past argument
            ;;
        -rad)
            radar=1
            radns="$2"
            if [[ $radns != -* ]] && [[ $radns != "" ]]; then
                shift
                radars=(${radns//,/ })
            fi
            ranges[0]=${ranges[0]:--3}
            ranges[1]=${ranges[1]:--10}

            #echo $radars
            ;;
        -ruc13)
            ruc13=1
            ranges[0]=${ranges[0]:-4}
            ranges[1]=${ranges[1]:-0}
            ;;
        -range)
            range=$2
            if [[ $range =~ ^[-+0-9]{1,3},[-+0-9]{1,3}$  ]]; then
                ranges=(${range//,/ })
                if [[ ${ranges[1]} -ge ${ranges[0]} ]]; then
                    echo ""
                    echo "    ERROR: a > b is required, got \$a=${ranges[0]}, \$b=${ranges[1]}."
                    usage -2
                fi
                shift
            fi
            #echo ${ranges[1]}
            ;;

        -*)
            echo "Unknown option: $key"
            usage -1
            ;;
        *)
            datestr="$1"
            ;;
    esac
    shift # past argument or value
done

if [[ $radar -ne 1 && $ruc13 -ne 1 ]]; then usage 0; fi

if [[ $datestr =~ ^[0-9]{4}(0[0-9]|1[0-2])([0-2][0-9]|3[0-1])[0-6][0-9][0-6][0-9]$ ]]; then
    date_s=${datestr:0:8}
    time_s=${datestr:8:13}
    datehr=${datestr:0:10}
    #echo $time_s
else
    date_s=$(date -u +"%Y%m%d")
    time_s=$(date -u +"%H%M")
    datehr=$(date -u +"%Y%m%d%H")
fi


#-----------------------------------------------------------------------
#
# Get radar data for current hour
#
#-----------------------------------------------------------------------

if [[ $radar -eq 1 ]]; then

    listfile="radar_matched.txt"

    if [[ -f $listfile ]]; then
        $show rm -f $listfile
    fi
    $show touch $listfile

    radsrc="$srcroot/nexrad"
    a=${ranges[0]}
    b=${ranges[1]}

    for rad in "${radars[@]}"
    do
      for i in $(seq $a -1 $b)
      do
          #for j in '+' '-'
          #do
            cdate=$(date -d "$date_s $time_s $i minutes" +"%Y%m%d")
            ctime=$(date -d "$date_s $time_s $i minutes" +"%H%M")

            file="$radsrc/$cdate/$rad/$rad$cdate$ctime.raw"
            if [[ $verbose -eq 1 ]]; then echo "Looking for $file ($i)"; fi
            if [[ -f $file ]]; then
                d=$(stat -c "%Z" $file)
                c=$(date +"%s")
                while [[ $d+$readys -gt $c ]]
                do
                    if [[ $verbose -eq 1 ]]; then echo "Found $file $d $c"; fi
                    $show sleep 2
                    d=$(stat -c "%Z" $file)
                    c=$(date +"%s")
                done
                if [[ $verbose -eq 1 ]]; then echo "$file is $readys seconds old."; fi
                $show echo $file | cat >> $listfile
                break
            fi
          #done

      done
    done
    echo $listfile
fi

#-----------------------------------------------------------------------
#
# Get RUC13 data
#
#-----------------------------------------------------------------------

if [[ $ruc13 -eq 1 ]]; then

    listfile="ruc13_matched.txt"

    if [[ -f $listfile ]]; then
        $show rm -f $listfile
    fi
    $show touch $listfile

    rucsrc="$srcroot/ruc13rrgrb2"

    a=${ranges[0]}
    b=${ranges[1]}

    let "hrw=$a-$b+1"
    while (( b < 10 ))
    do

      found=0
      for i in $(seq $a -1 $b)
      do

        if [[ $i -lt 10 ]]; then
            file="$rucsrc/ruc13rrgrb2.${datehr}f0$i"
        else
            file="$rucsrc/ruc13rrgrb2.${datehr}f$i"
		fi

        if [[ $verbose -eq 1 ]]; then echo "Checking $file ($i) ..."; fi

        if [[ -f $file ]]; then
            d=$(stat -c "%Z" $file)
            c=$(date +"%s")
            while [[ $d+$readys -gt $c ]]
            do
                if [[ $verbose -eq 1 ]]; then echo "Found $file $d $c"; fi
                $show sleep 2
                d=$(stat -c "%Z" $file)
                c=$(date +"%s")
            done
            if [[ $verbose -eq 1 ]]; then echo "$file is $readys seconds old."; fi
            $show echo $file | cat >> $listfile
            (( found += 1 ))
        else
            break
        fi
      done

      if [[ $found -eq $hrw ]]; then
        break
      else
        let "b += 1"
        let "a = $b+$hrw-1"
        datehr=$(date -d "${datehr:0:8} ${datehr:8:10}00 1 hours ago" +"%Y%m%d%H")
      fi
    done
    echo $listfile
fi
