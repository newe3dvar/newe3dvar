#!/usr/bin/env python
## ---------------------------------------------------------------------
## This software is in the public domain, furnished "as is", without
## technical support, and with no warranty, express or implied, as to
## its usefulness for any purpose.
## ---------------------------------------------------------------------
##
## This is a python program to check file status to make sure it is
## ready for reading with file integrity.
##
## ---------------------------------------------------------------------
##
## HISTORY:
##   Yunheng Wang (04/12/2016)
##   Initial version.
##
##
########################################################################
##
## Requirements:
##
########################################################################

import os, sys, time, re
from subprocess import Popen, PIPE
import logging

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

def wait_for_a_file(jobname,filepath,
                  maxwaitexist=10800,maxwaitready=3600,waittick=10,
                  skipread=False):
    """Checks if the files are ready.

      For a file to be ready it must exist and can be opened in append
      mode.

    """
    wait_time = 0
    # If the file doesn't exist, wait wait_time seconds and try again
    # until it's found.
    while wait_time < maxwaitexist :
      if os.path.exists(filepath):
        break
      else :
        logging.info("<%s> hasn't arrived after %s seconds.",filepath, wait_time )
        time.sleep(waittick)
        wait_time += waittick
    else:       ## We have waitted too long
        logging.critical('Job "%s" waiting for file <%s> appearance excceeded %d seconds.\n',
                         jobname,filepath,maxwaitexist)
        return False

    ## find file, continue
    if not skipread:  # Check whether other process is opening this file
        # If the file exists but locked, wait wait_time seconds and check
        # again until it's no longer locked by another process.
        ##wait_time = 0
        ##while wait_time < maxwaitready :
        ##  if is_locked(filepath) :
        ##    logging.info("%s is currently in use after %s seconds.",
        ##                 filepath, wait_time )
        ##    time.sleep(waittick)
        ##    wait_time += waittick
        ##  else :
        ##    break

        ##
        ## Check if the passed file is in stable status within time tick.
        ##
        last      = os.path.getmtime(filepath)
        wait_time = 0
        while wait_time < maxwaitready :
            time.sleep(waittick)
            wait_time += waittick
            current=os.path.getmtime(filepath)
            if last == current:
                logging.debug("<%s> is now stable after %d seconds.",filepath,wait_time)
                break
            else:
                logging.info("<%s> is currently in use after %s seconds.",
                             filepath, wait_time )
                last = current
        else:
            logging.critical('Job "%s" waiting for file <%s> ready excceeded %d seconds.\n',
                             jobname,filepath,maxwaitready )
            return False

    return True

#enddef wait_for_a_file

def is_locked(filepath):
    """Check if a file is locked by opening it in append mode.
    If no exception thrown, then the file is not locked.
    """
    locked = None
    ###file_object = None
    alsof = lsof('/usr/bin/lsof')
    if os.path.exists(filepath):
        try:
            ####print "Trying to open %s." % filepath
            ###buffer_size = 8
            ##### Opening file in append mode and read the first 8 characters.
            ###file_object = open(filepath, 'rb', buffer_size)
            ###if file_object:
            ###    #print "%s is not locked." % filepath
            ###    locked = False
            tmp = alsof.run(filepath)
            if len(tmp) > 0:
                logging.debug("%s is currently locked.",filepath )
                locked = True
            else:
                locked = False
        except IOError, message:
            #print "File is locked (unable to open in append mode). %s." % \
            #      message
            locked = True
        finally:
            #if file_object:
            #    file_object.close()
            pass
                #print "%s closed." % filepath
    #else:
    #    print "%s not found." % filepath
    return locked
#enddef is_locked

def is_stable(filepath,timetick=10):
    '''
       Check if the passed file is in stable status within time tick.
    '''

    last    = 0
    current = 1

    waittime = 0
    while last != current:
       last=current
       current=os.path.getmtime(filepath)
       time.sleep(timetick)
       waittime += timetick

    logging.debug("%s is now stable after %d seconds.",filepath,waittime)

    return True
#enddef is_stable

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class lsof:
  #classbegin
  """
  lsof wrapper class

  An automated wrapper around lsof output that yields
  an array of processes with their file handle information
  in the form of:

  results = [
        {
            info: {
                     'none': '',
                     'user_id': '501',
                     'process_id': '22665',
                     'process_group_id': '20',
                     'parent_pid': '232',
                     'login_name': 'nar',
                     'command_name': 'mdworker'
                  },
            files: [
                      ...
                   ]
        },

  ...

  """
  ##LSOF_BIN='/usr/sbin/lsof'
  flagRegex = re.compile("\s+(?P<flag>[a-zA-Z])\s+(?P<desc>.*)$")
  flag_cache = {'': 'none'}

  def __init__(self,lsofbin):
        """
        For initialization, we query lsof using the '-Fh?' flag
        to automatically generate a list of possible key names
        so that later on when we're parsing records we can substitute
        them in the final dict.

        When using the -F option, the key names are only provided as
        single characters, which aren't terribly useful if you don't
        know them by heart.
        """
        self.LSOF_BIN=lsofbin

        cmd = "%s -F?" % (self.LSOF_BIN)
        p = Popen( cmd.split(' ') , stdout=PIPE, stderr=PIPE, close_fds=True )
        for line in p.stderr.readlines()[1:]:
            m = self.flagRegex.match(line)
            if m is not None:
                flag = m.group('flag')
                desc = m.group('desc').lower()
                for s in (':', ' as', ' ('):
                    if desc.find(s) > -1:
                        desc = desc[0:desc.find(s)]
                        break
                self.flag_cache[flag] = re.sub('[/ ,\t_]+', '_', desc)
                logging.debug('flag_cache: %s -> %s',flag,self.flag_cache[flag])
  #enddef __init__

  def run(self,filearg='',user=''):
        """
        run lsof and parse the results into a tmp array that gets passed back to the
        caller.
        """

        tmp = []
        cmd = "%s -PnF0" % (self.LSOF_BIN)
        if user:
          cmd += ' -u %s' % user
        #if filearg:
        #  cmd += ' | grep %s'%filearg

        DEVNULL = open(os.devnull,'wb')
        p = Popen(cmd.split(' '), stdout=PIPE, stderr=DEVNULL, close_fds=True)
        ##p = Popen([self.LSOF_BIN,'-u','root','-PnF0'], stdout=PIPE, close_fds=True)
        for line in p.stdout.xreadlines():
            if  line.find(filearg) > 0:
                r = dict( [ ( self.flag_cache[x[0:1]], x[1:] ) for x in line.rstrip('\n').split('\0') ] )
                logging.debug('line: %s',line.rstrip('\n'))

                for x in line.rstrip('\0\n').split('\0'):
                    logging.debug('x: %s -> %s',x[0:1],x[1:])

                for key,val in r.iteritems():
                    logging.debug('r: %s -> %s',key,val)
                tmp.append( r )
        return tmp
  #enddef run
#endclass

########################################################################

if __name__ == '__main__':
    #logging.basicConfig(filename='filestatus.log', level=logging.INFO)
    logging.basicConfig(level=logging.DEBUG)
    filename = sys.argv[1]
    if not wait_for_a_file('test_FileStatus',filename,300,300,10):
        sys.exit(1)
