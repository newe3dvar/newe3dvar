#!/usr/bin/env python
## ---------------------------------------------------------------------
## This software is in the public domain, furnished "as is", without
## technical support, and with no warranty, express or implied, as to
## its usefulness for any purpose.
## ---------------------------------------------------------------------
##
## This is a python library to submit program to SLURM (Simple Linux
## Utility for Resource Management).
##
## ---------------------------------------------------------------------
##
## HISTORY:
##   Yunheng Wang (07/26/2013)
##   Initial version based on previous work for LSF, PBS and SGE.
##
########################################################################
##
## Requirements:
##
########################################################################

import os, sys, time, re
import subprocess, tempfile

from configBase import *

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class configurator (baseConfigurator) :
  '''
  this is a specific computer-dependent configurations for Stampede
  A supercomputer hosted at TACC.
  '''

  ##----------------------- Initialization      -----------------------
  def __init__(self,wrkdir,runname,runtime,norun,debug) :
    baseConfigurator.__init__(self,wrkdir,runname,runtime,norun,debug)
    self.hardncpn  = 16    ## constant hardward number cores per node
    self.defaultQueue = 'normal'
  #enddef

  ##----------------------- Ending of the module -----------------------
  def finalize(self) :
    baseConfigurator.finalize(self)
  #enddef

  ######################################################################

  def run_a_program(self,executable,nmlfile,outfile,wrkdir,jobconf):
    '''
      Generate job script for a task and submit it

      nmlfile : namelist file can be None
      outfile : must provide a output file name
      wrkdir  : the program can be run in a directory different from the
                instance attribute of wrkdir
    '''

    self.serieno += 1

    cwdsaved = os.getcwd()
    os.chdir(wrkdir)

    cmdstr = executable
    if nmlfile :
      cmdstr += " < %s" % nmlfile

    if outfile :
      (jobname,ext) = os.path.splitext(outfile)
      jobname = os.path.basename(jobname)
      #cmdstr += " > %s" % outfile
    else :
      jobname = os.path.basename(executable)

    if jobconf.mpi :

      claimncpn = jobconf.claimncpn
      nprocs = jobconf.nproc_x*jobconf.nproc_y
      cmdstr = 'ibrun %s' % (cmdstr)

      (nodes,extra) = divmod(nprocs,claimncpn)
      if extra > 0 :
        nodes += 1

      runqueue = jobconf.jobqueue or self.defaultQueue

    else :
      nprocs = 1
      claimncpn = 1
      nodes  = 1
      runqueue = 'serial'
      #cmdstr = 'ibrun %s' % (cmdstr)

    (hour,minute) = divmod(jobconf.claimmin,60)
    timestr = '%02d:%02d:00' % (hour,minute)

    scriptstr = '''#!/bin/bash
        #SBATCH -A TG-MCA95C006
        #SBATCH -p %(queue)s
        #SBATCH -J %(jobname)s
        #SBATCH -N %(nodes)d -n %(nprocs)d
        #SBATCH -t %(claimtime)s
        #SBATCH -o %(wrkdir)s/%(jobname)s_%%j.out

        time1=$(date '+%%s')
        echo "Job Started: $(date). Job Id:  $SLURM_JOB_ID"
        echo " "

        set echo on

        cd %(wrkdir)s

        rm -rf %(outfile)s

        %(cmdstr)s

        set echo off

        time2=$(date '+%%s')

        let diff=time2-time1
        let hour=diff/3600
        let diff=diff%%3600
        let min=diff/60
        let sec=diff%%60

        echo -n "Job   Ended: $(date). "
        printf 'Job run time:  %%02d:%%02d:%%02d' $hour $min $sec
        echo " "

        ''' %{ 'wrkdir' : wrkdir, 'jobname' : jobname,
        	     'cmdstr' : cmdstr, 'outfile' : outfile,
        	     'queue'  : runqueue,'claimtime'  : timestr,
        	     'nprocs' : nprocs, 'nodes' : nodes
        	    }

    scriptfile = '%s.slurm' % jobname

    batchFile = open(scriptfile,'w')
    batchFile.write(self.trim(scriptstr))
    batchFile.close()

    self.addlog(1,'''%s - %02d -\n  Jobscript "%s" generated.''' %(
                      time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
                      scriptfile )
                )

    if self.showonly :
      print 'Preparing job script "%s" in %s.' % (scriptfile,wrkdir)
      retcode = 0

      jobid = None
      jobstatus = 0

    else :

      jobid = self.submit_a_job(jobname,wrkdir)
      jobstatus = 1

      #self.addlog(1,'''%s - %02d -\n  Jobscript "%s" submited with jobid %s.''' %(
      #                time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
      #                scriptfile,jobid )
      #           )
      retcode = 0

    os.chdir(cwdsaved)

    return (retcode,jobid)
  #enddef run_a_program

  ################# Check job status ###################################

  def wait_job(self,jobname,substatus,jobid,waitonjob
                   ,maxwaittime = 5*3600,infolog=True) :
    '''
    Check the job (jobid)'s status and wait for it if requested.
    '''
    if self.showonly : return True

    errmsg = 'Unknown Problem'
    #print '%s waitonjob = %s' % (jobname, waitonjob)
    if substatus == 0 :
      waitime = 0
      if waitonjob :       ## we should wait here
        jobstatus = self.get_job_status(jobid)

        #print '%s jobstatus = %d' % (jobname, jobstatus)
        if infolog : self.addlog(1,'  Waiting for job <%s> to be finished ...' % ( jobname ) )

        while waitime < maxwaittime and jobstatus > 0 :
          time.sleep(10)
          waitime += 10
          jobstatus = self.get_job_status(jobid)
          #print '%s jobstatus = %d' % (jobname, jobstatus)

        if waitime >= maxwaittime :   ## we have waitted too long
          errmsg = 'Waiting for job "%s" excceeded %d hour.' % (jobname,waitime/3600)
        elif jobstatus < 0 :          ## bad thing happened with jobname
          errmsg = 'Job "%s" failed with jobstatus = %d' % (jobname,jobstatus)
        else :                        ## job is done normally
          if infolog : self.addlog(0,'  Job <%s> done at %s.' % ( jobname,
                              time.strftime('%Y-%m-%d %I:%M:%S %p') )  )
          return True
      else :                          ## job is submited and may be running
        return True
    else :                            ## job did not submited right
      errmsg = 'Job "%s" failed with substatus = %d' % (jobname,substatus)

    print >> sys.stderr, '%s\n' % errmsg
    if infolog : self.addlog(1, '\n  *** %s ***\n' % errmsg  )
    return False

  #enddef wait_job

  ########################################################################

  def run_convert_job(self,files,framenames,wrkdir,outdir,pltjobid,
                      jobconf,interval=0) :
    '''
        Submit a job to convert gmeta to images files

        This job run script "gmetacvt" on compute nodes
    '''

    self.serieno += 1

    cwdsaved = os.getcwd()
    os.chdir(wrkdir)
    if not os.path.lexists(outdir) :
      os.mkdir(outdir)

    refhour = re.compile(r'.+_(\w{3,3})_(\d{6})')

    (flbase,ext) = os.path.splitext(files[0])
    mfhour = refhour.match(flbase)
    if mfhour :
      runaffix = mfhour.group(1)
      fhour    = mfhour.group(2)
    else :
      runaffix = 'd01'
      fhour    = '0'
    (hour,minute) = divmod(int(fhour)/60,60)

    jobname = 'cvt%s%02d%02d%s' % (runaffix,hour,minute,framenames[0])
    scriptdir = os.path.dirname(__file__)
    cvtscript = os.path.join(scriptdir,'gmetacvt.py')

    ## SLURM parameters
    if jobconf.mpi :
      nprocs = jobconf.nproc_x*jobconf.nproc_y
      claimncpn = jobconf.claimncpn

      (nodes,extra) = divmod(nprocs,claimncpn)
      if extra > 0 :
        nodes += 1

    else :
      nprocs = 1
      claimncpn = 1
      nodes = 1

    (hour,minute) = divmod(jobconf.claimmin,60)
    timestr = '%02d:%02d:00' % (hour,minute)

    scriptstr = '''#!/bin/bash
        #SBATCH -A TG-MCA95C006
        #SBATCH -p %(queue)s
        #SBATCH -J %(jobname)s
        #SBATCH -N %(nodes)d -n %(nprocs)d
        #SBATCH -t %(claimtime)s
        #SBATCH -o %(wrkdir)s/%(jobname)s_%%j.out
        #

        # Change to the directory that you want to run in.
        #

        wrkdir=%(wrkdir)s

        cd $wrkdir

        time1=$(date '+%%s')
        echo "Job Started: $(date). Job Id:  $SLURM_JOB_ID"
        echo " "

        set echo on

        ''' %{ 'wrkdir' : wrkdir, 'jobname' : jobname,
               'queue'  : 'serial', 'claimtime'  : timestr,
               'nprocs' : nprocs, 'nodes' : nodes
               }

    scriptfile = '%s.slurm' % jobname
    batchFile = open(scriptfile,'w')
    batchFile.write(self.trim(scriptstr))

    numfiles = len(files)

    for numf in range(0,numfiles+1,nprocs) :

      hnumf = min(numf+nprocs,numfiles)

      batchFile.write('\nfiles=( %s )\n'% ' '.join(files[numf:hnumf]))
      batchFile.write('\nframenames=( %s )\n' % ' '.join(framenames[numf:hnumf]))
      batchFile.write('''PROCNAMES=`cat $PBS_NODEFILE`''')

      cvtstr = "%(cvtscript)s -n ${framenames[$i]} -i %(interval)d -o %(outdir)s ${files[$i]}" % {
                'cvtscript': cvtscript, 'outdir' : outdir, 'interval' : interval }

      scriptstr = '''i=0
        for node in $PROCNAMES
        do
          echo "$node cd $wrkdir;%(cvtstr)s"
          ssh $node "cd $wrkdir;%(cvtstr)s" &\n
          let "i = $i+1"
        ''' % { 'cvtstr': cvtstr }

      batchFile.write(self.trim(scriptstr))

      if hnumf < numf+nprocs :
        batchFile.write('''\nif [ $i -ge ${#files[*]} ]\n  then\n    break\n  fi\n''')

      batchFile.write('done\n')

    batchFile.write('\nwait\n')
    scriptstr = r'''
        set echo off

        time2=$(date '+%s')

        let diff=time2-time1
        let hour=diff/3600
        let diff=diff%3600
        let min=diff/60
        let sec=diff%60

        echo -n "Job  Ended : $(date). "
        printf 'Job run time:  %02d:%02d:%02d.' $hour $min $sec
        echo " "

        '''
    batchFile.write(self.trim(scriptstr))
    batchFile.close()


    self.addlog(1,'''%s - %02d -\n  Jobscript "%s" generated.''' %(
                      time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
                      scriptfile )
                )

    if self.showonly :
      print 'Preparing job script "%s" in %s.' % (scriptfile,wrkdir)
      retcode = 0
      jobid   = None

    else :

      jobid = self.submit_a_job(jobname,wrkdir,[pltjobid])

      #self.addlog(1,'''%s - %02d -\n  Jobscript "%s" submited with jobid %s.''' %(
      #                time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
      #                scriptfile,jobid )
      #           )
      retcode = 0

    os.chdir(cwdsaved)

    return (retcode,jobid)

  #enddef run_convert_job

  ##====================================================================

  def submit_a_job(self,jobname,wrkdir,djobs=[]) :
    '''
    submit a job to SLURM on Stampede
    '''

    jobfile = os.path.join(wrkdir,jobname+'.slurm')

    if djobs :
      bjoblst = ['sbatch', '-d', 'afterok:%s' % ':'.join(djobs), jobfile]
    else :
      bjoblst = ['sbatch', jobfile]

    ##retout = subprocess.check_output(bjobstr,stdin=None,stderr=subprocess.STDOUT,cwd=wrkdir)
    retout = subprocess.Popen(bjoblst, stdout=subprocess.PIPE,cwd=wrkdir).communicate()[0]
    #print retout

    ## get jobid here
    jobidre = re.compile(r'Submitted batch job (\d+)')
    retlist = retout.split('\n')
    jobid = None
    for retstr in retlist :
      jobidmatch = jobidre.match(retstr)
      #print retstr, jobidmatch
      if jobidmatch :
        jobid = jobidmatch.group(1)

    if jobid is None :
      print >> sys.stderr, 'Something is wrong with job (%s) submitting? Get: "%s".'%(jobname,retout)
      self.addlog(0,'  Job <%s> is not submitted correctly.' % (jobname,)  )
      self.addlog(0,'    $ %s\n    > "%s"' % ( ' '.join(bjoblst), retout )  )
      #return False
      return 'Failed'

    self.addlog(0,'  Submitted job <%s> with jobid <%s> as command:' % (jobname,jobid)  )
    self.addlog(0,'    $ %s %s\n    > %s' % ( ' '.join(bjoblst), jobfile, retout )  )

    return jobid

  #enddef submit_a_job

  ##====================================================================

  def get_job_status(self,jobid) :
    '''
    Check job status
    '''

    if jobid is None :
      return 0

    retout = subprocess.Popen(['squeue', '-j','%s' % jobid ],
                           stdout=subprocess.PIPE).communicate()[0]
    #print retout

    ## get job status here
    retlist = retout.split('\n')
    jobstatre = re.compile(r'^ +%s +[^ ]+ +[^ ]+ +%s +(\w+) +[\d:]+ +.*' % (jobid,os.getenv('USER')) )
    #print "%s - %s" % (jobstatre.pattern, jobid)
    jobstatmatch = jobstatre.match(retlist[1])
    #print retlist[1]
    if jobstatmatch :
      status = jobstatmatch.group(1)
      if status == "CD" :
        jobstatus = 0
      elif status == "PD" :
        jobstatus = 1
      elif status == "R" :
        jobstatus = 2
      else :
        jobstatus = 6

      return jobstatus
    else :
      #errmsg = 'Job id %s is not found.' % jobid
      #print >> sys.stderr, errmsg
      #self.addlog(1, '\n  *** %s ***\n' % errmsg  )
      #return False
      jobstatus = 0
      return jobstatus

  #enddef get_job_status

#endclass configurator
