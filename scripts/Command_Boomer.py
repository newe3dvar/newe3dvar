#!/usr/bin/env python
## ---------------------------------------------------------------------
## This software is in the public domain, furnished "as is", without
## technical support, and with no warranty, express or implied, as to
## its usefulness for any purpose.
## ---------------------------------------------------------------------
##
## This is a python library to submit program to LSF scheduler
##
## ---------------------------------------------------------------------
##
## HISTORY:
##   Yunheng Wang (06/01/2012)
##   Initial version.
##
##
########################################################################
##
## Requirements:
##
########################################################################

import os, sys, time, re
import subprocess, tempfile

from configBase import *

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class configurator (baseConfigurator) :
  #classbegin
  '''
  this is a specific computer-dependent configuration for Boomer
  A supercomputer hosted at OSCER of OU.
  '''

  ##----------------------- Initialization      -----------------------
  def __init__(self,wrkdir,runname,runtime,norun,debug,justdelete=False) :
    baseConfigurator.__init__(self,wrkdir,runname,runtime,norun,debug,justdelete)
    self.hardncpn  = 16    ## constant hardward number cores per node
    self.defaultQueue = 'normal'

  #enddef

  ##----------------------- Ending of the module -----------------------
  def finalize(self) :
    baseConfigurator.finalize(self)
  #enddef

  ######################################################################

  def run_a_program(self,executable,nmlfile,outfile,wrkdir,jobconf):
    '''
      Generate job script for a task and submit it

      nmlfile : namelist file can be None
      outfile : must provide a output file name
      wrkdir  : the program can be run in a directory different from the
                instance attribute of wrkdir
    '''

    self.serieno += 1

    cwdsaved = os.getcwd()
    os.chdir(wrkdir)

    cmdstr = executable
    if nmlfile :
      cmdstr += " < %s" % nmlfile

    if outfile :
      (jobname,ext) = os.path.splitext(outfile)
      jobname = os.path.basename(jobname)
      #cmdstr += " > %s" % outfile
    else :
      jobname = os.path.basename(executable)

    if jobconf.mpi :

      nprocs = jobconf.nproc_x*jobconf.nproc_y
      cmdstr = 'mpirun.lsf -n %d %s' % (nprocs, cmdstr)

    else :
      nprocs = 1

    (hour,minute) = divmod(jobconf.claimmin,60)
    timestr = '%02d:%02d' % (hour,minute)

    excstr = " "
    if jobconf.exclusive:
      excstr = "-x"

    scriptstr = '''#!/bin/sh
        #BSUB -q %(queue)s
        #BSUB -a openmpi
        #BSUB -n %(nprocs)d
        #BSUB %(exclusivestr)s
        #BSUB -R span[ptile=%(claimncpn)d]
        #BSUB -W %(claimtime)s
        #BSUB -o %(wrkdir)s/%(jobname)s_%%J.out
        #BSUB -e %(wrkdir)s/%(jobname)s_%%J.err
        #BSUB -J "%(jobname)s"
        #

        export MPI_COMPILER=intel
        export MPI_HARDWARE=ib
        export MPI_SOFTWARE=openmpi

        #

        # Change to the directory that you want to run in.
        #
        cd %(wrkdir)s

        rm -rf %(outfile)s

        #
        # Make sure you're in the correct directory.
        #
        pwd
        #
        # Run the job, redirecting input from the given file.  The date commands
        # and the time command help track runtime and performance.
        #
        time1=$(date '+%%s')
        echo "Job Started: $(date). Job Id:  $LSB_JOBID"
        echo " "

        set echo on

        %(cmdstr)s

        set echo off

        time2=$(date '+%%s')

        let diff=time2-time1
        let hour=diff/3600
        let diff=diff%%3600
        let min=diff/60
        let sec=diff%%60

        echo -n "Job   Ended: $(date). "
        printf 'Job run time:  %%02d:%%02d:%%02d' $hour $min $sec
        echo " "

        ''' %{ 'wrkdir' : wrkdir, 'jobname' : jobname,
               'cmdstr' : cmdstr, 'outfile' : outfile,
               'queue'  : jobconf.jobqueue or self.defaultQueue,
               'claimtime'   : timestr,
               'nprocs'      : nprocs, 'claimncpn' : jobconf.claimncpn,
               'exclusivestr': excstr
              }

    scriptfile = '%s.bsub' % jobname

    batchFile = open(scriptfile,'w')
    batchFile.write(self.trim(scriptstr))
    batchFile.close()

    self.addlog(1,'''%s - %02d -\n  Jobscript "%s" generated.''' %(
                      time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
                      scriptfile )
                )

    if self.showonly :
      print 'Preparing job script "%s" in %s.' % (scriptfile,wrkdir)
      retcode = 0

      jobid = None

    else :

      jobid = self.submit_a_job(jobname,wrkdir)

      ##self.addlog(1,'''%s = %02d =\n  Jobscript "%s" submited with jobid %s.''' %(
      ##                time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
      ##                scriptfile,jobid )
      ##           )
      retcode = 0

    os.chdir(cwdsaved)

    return (retcode,jobid)
  #enddef run_a_program

  ######################################################################

  def run_ncl_plt(self,executable,nmlfile,field,
                  wrkdir,outdir,tmpldir,jobconf):
    '''
      Plot using NCL and convert to PNG file

      nmlfile : namelist file can be None
      outfile : must provide a output file name
      wrkdir  : the program can be run in a directory different from the
                instance attribute of wrkdir
    '''
    gs      = "/usr/bin/gs"
    gsopt   = "-dSAFER -dBATCH -dNOPAUSE -sDEVICE=png16m -dGraphicsAlphaBits=4"
    psfile  = "%s.ps"  % field

    self.serieno += 1

    cwdsaved = os.getcwd()
    os.chdir(wrkdir)

    cmdstr = executable
    if nmlfile :
      cmdstr += " < %s" % nmlfile

    (jobname,ext) = os.path.splitext(nmlfile)
    jobname = os.path.basename(jobname)

    if jobconf.mpi :

      claimncpn = jobconf.claimncpn
      nprocs = jobconf.nproc_x*jobconf.nproc_y
      cmdstr = 'mpirun.lsf -np %d %s' % (nprocs, cmdstr)

      (nodes,extra) = divmod(nprocs,claimncpn)
      if extra > 0 :
        nodes += 1

      nprocs = nodes*self.hardncpn

    else :
      nprocs = 1
      claimncpn = 1
      nodes  = 1

    ##if field.startswith('agl'):
    ##  outfile = r"agl%02d.png"
    ##  pngrenamesub = """
    ##      lista=(agl??.png)
    ##      for fn in ${lista[@]}
    ##         do
    ##         seq=${fn:3:2}
    ##         seq=${seq#0}
    ##         ############(( skm = seq+2 ))
    ##         skm=$(echo $seq+1.5 | bc -q)
    ##         ############printf -v outfn 'aglh%%04.1fkm.png' $skm
    ##         mv $fn %(outdir)s/${outdirs[$i]}/${fn/[0-9][0-9]/h${skm}km}
    ##      done
    ##      """ % {'outdir' : outdir }
    ##  field = "aglh"
    ##else :
    ##  outfile = os.path.join(outdir,r"${outdirs[$i]}","%s.png"% field)
    ##  pngrenamesub = "ls -l %s" % (outfile,)

    (hour,minute) = divmod(jobconf.claimmin,60)
    timestr = '%02d:%02d' % (hour,minute)

    scriptstr = '''#!/bin/sh
        #BSUB -q %(queue)s
        #BSUB -J %(jobname)s
        #BSUB -R span[ptile=%(claimncpn)d]
        #BSUB -x
        #BSUB -n %(nprocs)d
        #BSUB -W %(claimtime)s
        #BSUB -o %(wrkdir)s/%(jobname)s_%%J.out
        #BSUB -j oe

        export MPI_COMPILER=intel
        export MPI_HARDWARE=ib
        export MPI_SOFTWARE=openmpi

        export NCARG_RANGS=%(SZMBTmpl)s/NCLRANGS

        cd %(wrkdir)s

        time1=$(date '+%%s')
        echo "Job Started: $(date). Job Id:  $PBS_JOBID"
        echo " "

        set echo on

        %(cmdstr)s

        resolutions=(72 108 144 288)
        outdirs=(D072 D108 D144 D288)
        let i=0
        cd %(outdir)s
        for res in ${resolutions[@]}; do
          if [[ ! -d ${outdirs[$i]} ]]; then
            mkdir ${outdirs[$i]}
          fi
          %(gs)s %(gsopt)s -r$res -sOutputFile=${outdirs[$i]}/%(field)s.png %(wrkdir)s/%(psfile)s
          (( i = i+1 ))
        done

        ## temporary links, may be removed later
        ln -s ${outdirs[0]}/%(field)s.png .

        set echo off

        time2=$(date '+%%s')

        let diff=time2-time1
        let hour=diff/3600
        let diff=diff%%3600
        let min=diff/60
        let sec=diff%%60

        echo -n "Job   Ended: $(date). "
        printf 'Job run time:  %%02d:%%02d:%%02d' $hour $min $sec
        echo " "

        ''' %{ 'wrkdir' : wrkdir,    'jobname'    : jobname,
               'cmdstr' : cmdstr,    'claimtime'  : timestr,
               'queue'  : jobconf.jobqueue or self.defaultQueue,
               'nprocs'   : nprocs,  'claimncpn' : claimncpn,
               'gs'       : gs,      'gsopt'     : gsopt,
               'psfile'   : psfile,  'field'     : field,    'outdir' : outdir,
               'SZMBTmpl' : tmpldir
              }

    scriptfile = '%s.bsub' % jobname

    batchFile = open(scriptfile,'w')
    batchFile.write(self.trim(scriptstr))
    batchFile.close()

    self.addlog(1,'''%s - %02d -\n  Jobscript "%s" generated.''' %(
                      time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
                      scriptfile )
                )

    if self.showonly :
      print 'Preparing job script "%s" in %s.' % (scriptfile,wrkdir)
      retcode = 0

      jobid = None
      jobstatus = 0

    else :

      jobid = self.submit_a_job(jobname,wrkdir)
      jobstatus = 1

      ##self.addlog(1,'''%s - %02d -\n  Jobscript "%s" submited with jobid %s.''' %(
      ##                time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
      ##                scriptfile,jobid )
      ##           )
      retcode = 0

    os.chdir(cwdsaved)

    return (retcode,jobid)
  #enddef run_ncl_plt

  ######################################################################

  def run_unipost(self,executable,nmlfile,outfile,wrkdir,postdir,ndate,ifhr,jobconf):
    '''
      Generate job script for a unipost and submit it
      Same as run_a_program, just add run of copygb.exe with two extra
      arguments postdir & ndate

      nmlfile : namelist file can be None
      outfile : must provide a output file name
      wrkdir  : the program can be run in a directory different from the
                instance attribute of wrkdir
    '''

    self.serieno += 1

    cwdsaved = os.getcwd()
    os.chdir(wrkdir)

    cmdstr = executable
    if nmlfile :
      cmdstr += " < %s" % nmlfile

    if outfile :
      (jobname,ext) = os.path.splitext(outfile)
      jobname = os.path.basename(jobname)
      #cmdstr += " > %s" % outfile
    else :
      jobname = os.path.basename(executable)

    if jobconf.mpi :

      nprocs = jobconf.nproc_x*jobconf.nproc_y
      cmdstr = 'mpirun.lsf -n %d %s' % (nprocs, cmdstr)

    else :
      nprocs = 1

    (hour,minute) = divmod(jobconf.claimmin,60)
    timestr = '%02d:%02d' % (hour,minute)

    excstr = " "
    if jobconf.exclusive:
      excstr = "-x"

    #fhr = ndate.split('_')[1].split(':')[0]
    fhr = "%02d"%ifhr

    scriptstr = '''#!/bin/bash
        #BSUB -q %(queue)s
        #BSUB -a openmpi
        #BSUB -n %(nprocs)d
        #BSUB %(exclusivestr)s
        #BSUB -R span[ptile=%(claimncpn)d]
        #BSUB -W %(claimtime)s
        #BSUB -o %(wrkdir)s/%(jobname)s_%%J.out
        #BSUB -e %(wrkdir)s/%(jobname)s_%%J.err
        #BSUB -J "%(jobname)s"
        #

        export MPI_COMPILER=intel
        export MPI_HARDWARE=ib
        export MPI_SOFTWARE=openmpi

        #

        # Change to the directory that you want to run in.
        #
        cd %(wrkdir)s

        rm -rf %(outfile)s

        #
        # Make sure you're in the correct directory.
        #
        pwd
        #
        # Run the job, redirecting input from the given file.  The date commands
        # and the time command help track runtime and performance.
        #
        time1=$(date '+%%s')
        echo "Job Started: $(date). Job Id:  $LSB_JOBID"
        echo " "

        set echo on

        %(cmdstr)s

        if [[ -s "copygb_hwrf.txt" && -s "WRFPRS.GrbF%(fhr)s" ]]; then
          read nav < 'copygb_hwrf.txt'
          %(postdir)s/copygb.exe -xg "${nav}" WRFPRS.GrbF%(fhr)s csm3dvar_prd_%(ndate)s.grb
          rm WRFPRS.GrbF%(fhr)s
        fi

        set echo off

        time2=$(date '+%%s')

        let diff=time2-time1
        let hour=diff/3600
        let diff=diff%%3600
        let min=diff/60
        let sec=diff%%60

        echo -n "Job   Ended: $(date). "
        printf 'Job run time:  %%02d:%%02d:%%02d' $hour $min $sec
        echo " "

        ''' %{ 'wrkdir' : wrkdir, 'jobname' : jobname,
               'cmdstr' : cmdstr, 'outfile' : outfile,
               'queue'  : jobconf.jobqueue or self.defaultQueue,
               'claimtime'  : timestr,
               'nprocs' : nprocs, 'claimncpn' : jobconf.claimncpn,
               'exclusivestr': excstr,
               'postdir': postdir,'ndate'     : ndate, 'fhr' : fhr
              }

    scriptfile = '%s.bsub' % jobname

    batchFile = open(scriptfile,'w')
    batchFile.write(self.trim(scriptstr))
    batchFile.close()

    self.addlog(1,'''%s - %02d -\n  Jobscript "%s" generated.''' %(
                      time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
                      scriptfile )
                )

    if self.showonly :
      print 'Preparing job script "%s" in %s.' % (scriptfile,wrkdir)
      retcode = 0

      jobid = None

    else :

      jobid = self.submit_a_job(jobname,wrkdir)

      ##self.addlog(1,'''%s = %02d =\n  Jobscript "%s" submited with jobid %s.''' %(
      ##                time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
      ##                scriptfile,jobid )
      ##           )
      retcode = 0

    os.chdir(cwdsaved)

    return (retcode,jobid)
  #enddef run_unipost

  ################# Check job status ###################################

  def wait_job(self,jobname,substatus,jobid,waitonjob
                    ,maxwaittime = 5*3600,infolog=True) :
    '''
    Check the job (jobid)'s status and wait for it if requested.
    '''
    if self.showonly : return True

    errmsg = 'Unknown Problem'
    #print '%s waitonjob = %s' % (jobname, waitonjob)
    if substatus == 0 :
      waitime = 0
      if waitonjob :       ## we should wait here
        jobstatus = self.get_job_status(jobid)

        #print '%s jobstatus = %d' % (jobname, jobstatus)
        if infolog : self.addlog(1,'  Waiting for job <%s> to be finished ...' % ( jobname ) )

        while waitime < maxwaittime and jobstatus > 0 :
          time.sleep(10)
          waitime += 10
          jobstatus = self.get_job_status(jobid)
          #print '%s jobstatus = %d' % (jobname, jobstatus)

        if waitime >= maxwaittime :   ## we have waitted too long
          errmsg = 'Waiting for job "%s" excceeded %d hour.' % (jobname,waitime/3600)
        elif jobstatus < 0 :          ## bad thing happened with jobname
          errmsg = 'Job "%s" failed with jobstatus = %d' % (jobname,jobstatus)
        else :                        ## job is done normally
          if infolog : self.addlog(0,'  Job <%s> done at %s.' % ( jobname,
                              time.strftime('%Y-%m-%d %I:%M:%S %p') )  )
          return True
      else :                          ## job is submited and may be running
        return True
    else :                            ## job did not submited right
      errmsg = 'Job "%s" failed with substatus = %d' % (jobname,substatus)

    print >> sys.stderr, '%s\n' % errmsg
    if infolog : self.addlog(1, '\n  *** %s ***\n' % errmsg  )
    return False

  #enddef

  ######################################################################

  def run_convert(self,files,framenames,wrkdir,name) :
    ''' Convert gmeta to png files

        It requires NCARG package and ImageMagick.
    '''

    ncargpath = "/usr/local/ncl_5.1/bin"

    self.serieno += 1
    cwdsaved = os.getcwd()

    os.chdir(wrkdir)
    outdir = name
    if not os.path.lexists(outdir) :
      os.mkdir(outdir)

    res    = "-resolution 1000x1000"
    crop   = "-crop 1000x1000+0+0"

    refhour = re.compile(r'.+_(\d{6})')

    ## bsub parameters
    nprocs = 1
    claimncpn = 1

    (hour,minute) = divmod(30,60)
    timestr = '%02d:%02d' % (hour,minute)

    scriptstr1 = '''#!/bin/env python
         #BSUB -q %(queue)s
         #BSUB -n %(nprocs)d
         #BSUB -x
         #BSUB -R span[ptile=%(claimncpn)d]
         #BSUB -W %(claimtime)s
         #BSUB -o %(wrkdir)s/%(jobname)s_%%J.out
         #BSUB -e %(wrkdir)s/%(jobname)s_%%J.err
         #BSUB -J "%(jobname)s"
         #
         import subprocess, re, os
         #

         # Change to the directory that you want to run in.
         #

         wrkdir = '%(wrkdir)s'
         framenames = [%(framenames)s]

         os.chdir(wrkdir)

         ''' %{ 'wrkdir' : wrkdir, 'jobname' : jobname,
                'queue'  : 'normal','claimtime'  : timestr,
                'nprocs' : nprocs, 'claimncpn' : claimncpn
              }

    scriptstr2 = '''i = 0
        for fl in files :

           print '  extracting from file %%s' %% fl

           (flbase,ext) = os.path.splitext(fl)
           mfhour = refhour.match(flbase)
           if mfhour :
             fhour = mfhour.group(1)
           else :
             fhour = '0'

           execncgm  = 'ncgmstat -c %%s' %% fl

           ncgmout = subprocess.check_output(execncgm.split(),stdin=None,
                                  stderr=subprocess.STDOUT,shell=None,cwd=wrkdir)
           num = int(ncgmout)

           execmed = ['med', '-e', "split%%d %%s_" %% (num,flbase), fl]
           retcode = subprocess.call(execmed,stdin=None,stdout=None,
                               stderr=subprocess.STDOUT,shell=None,cwd=wrkdir)

           for m in range(1,num+1) :

             pngfile = '%(outdir)s/%%s%%s-%03d.png' %(framenames[i],fhour,m)

             tmpncgm = '%%s_%%03d.ncgm' % (flbase,m)

             exectrans  = 'ctrans -f font4 -d sun %%s %(res)s' %% (tmpncgm)
             ctransP = subprocess.Popen(exectrans.split(),stdin=None,stdout=subprocess.PIPE,
                              stderr=subprocess.STDOUT,shell=None,cwd=wrkdir)

             execonvert = 'convert %(crop)s - %%s' %% (pngfile)
             convertP = subprocess.Popen(execonvert.split(),stdin=ctransP.stdout,stdout=None,
                              stderr=subprocess.STDOUT,shell=None,cwd=wrkdir)

             convertP.wait()
             ctransP.stdout.close()

             os.unlink(tmpncgm)
           i = i+1

         ''' % { 'framenames': ' '.join(framenames),
                 'outdir' : outdir,
                 'res' : res, 'crop' : crop
                }

    jobname = 'cvt_%s' % (name)
    scriptfile = '%s.bsub' % jobname

    batchFile = open(scriptfile,'w')
    batchFile.write(self.trim(scriptstr1))
    batchFile.write(self.trim(scriptstr2))
    batchFile.close()

    self.addlog(1,'''%s - %02d -\n  Jobscript "%s" generated.''' %(
                      time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
                      scriptfile )
                )

    if self.showonly :
      print 'Preparing job script "%s" in %s.' % (scriptfile,wrkdir)
      retcode = 0

      jobid = None
      jobstatus = 0

    else :

      jobid = self.submit_a_job(jobname,wrkdir)
      jobstatus = 1

      self.addlog(1,'''%s - %02d -\n  Jobscript "%s" submited with jobid %s.''' %(
                      time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
                      scriptfile,jobid )
                 )
      retcode = 0

    os.chdir(cwdsaved)

  #enddef run_convert

  ########################################################################

  def run_convert_job(self,files,framenames,wrkdir,outdir,pltjobid,
                      jobconf,interval=0) :
    '''
        Submit a job to convert gmeta to images files

        This job run script "gmetacvt" on compute nodes
    '''

    self.serieno += 1

    cwdsaved = os.getcwd()
    os.chdir(wrkdir)
    if not os.path.lexists(outdir) :
      os.mkdir(outdir)

    refhour = re.compile(r'.+_(\w{3,3})_(\d{6})')

    (flbase,ext) = os.path.splitext(files[0])
    mfhour = refhour.match(flbase)
    if mfhour :
      runaffix = mfhour.group(1)
      fhour    = mfhour.group(2)
    else :
      runaffix = 'd01'
      fhour    = '0'
    (hour,minute) = divmod(int(fhour)/60,60)

    jobname = 'cvt%s%02d%02d%s' % (runaffix,hour,minute,framenames[0])
    scriptdir = os.path.dirname(__file__)
    cvtscript = os.path.join(scriptdir,'gmetacvt.py')

    ## bsub parameters
    if jobconf.mpi :
      nprocs = jobconf.nproc_x*jobconf.nproc_y
      claimncpn = jobconf.claimncpn
    else :
      nprocs = 1
      claimncpn = 1

    (hour,minute) = divmod(jobconf.claimmin,60)
    timestr = '%02d:%02d' % (hour,minute)

    scriptstr = '''#!/bin/sh
        #BSUB -q %(queue)s
        #BSUB -n %(nprocs)d
        #BSUB -x
        #BSUB -R span[ptile=%(claimncpn)d]
        #BSUB -W %(claimtime)s
        #BSUB -o %(wrkdir)s/%(jobname)s_%%J.out
        #BSUB -e %(wrkdir)s/%(jobname)s_%%J.err
        #BSUB -J "%(jobname)s"
        #

        # Change to the directory that you want to run in.
        #

        wrkdir=%(wrkdir)s

        cd $wrkdir

        time1=$(date '+%%s')
        echo "Job Started: $(date). Job Id:  $LSB_JOBID"
        echo " "

        set echo on

        ''' %{ 'wrkdir' : wrkdir, 'jobname' : jobname,
               'queue'  : jobconf.jobqueue or self.defaultQueue,
               'claimtime'  : timestr,
               'nprocs' : nprocs, 'claimncpn' : claimncpn
               }


    scriptfile = '%s.bsub' % jobname
    batchFile = open(scriptfile,'w')
    batchFile.write(self.trim(scriptstr))

    numfiles = len(files)

    for numf in range(0,numfiles+1,nprocs) :

      hnumf = min(numf+nprocs,numfiles)

      batchFile.write('\nfiles=( %s )\n'% ' '.join(files[numf:hnumf]))
      batchFile.write('\nframenames=( %s )\n' % ' '.join(framenames[numf:hnumf]))

      cvtstr = "%(cvtscript)s -n ${framenames[$i]} -i %(interval)d -o %(outdir)s ${files[$i]}" % {
                'cvtscript': cvtscript, 'outdir' : outdir, 'interval' : interval }
      scriptstr = '''i=0
          for node in $LSB_HOSTS
          do
            echo "$node cd $wrkdir;%(cvtstr)s"
            ssh $node "cd $wrkdir;%(cvtstr)s" &\n
            let "i = $i+1"
          ''' % { 'cvtstr': cvtstr}

      batchFile.write(self.trim(scriptstr))

      if hnumf < numf+nprocs :
        batchFile.write('''\nif [ $i -ge ${#files[*]} ]\n  then\n    break\n  fi\n''')

      batchFile.write('done\n')

    batchFile.write('\nwait\n')
    scriptstr = r'''
        set echo off

        time2=$(date '+%s')

        let diff=time2-time1
        let hour=diff/3600
        let diff=diff%3600
        let min=diff/60
        let sec=diff%60

        echo -n "Job  Ended : $(date). "
        printf 'Job run time:  %02d:%02d:%02d.' $hour $min $sec
        echo " "

        '''
    batchFile.write(self.trim(scriptstr))
    batchFile.close()


    self.addlog(1,'''%s - %02d -\n  Jobscript "%s" generated.''' %(
                      time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
                      scriptfile )
                )

    if self.showonly :
      print 'Preparing job script "%s" in %s.' % (scriptfile,wrkdir)
      retcode = 0
      jobid   = None

    else :

      jobid = self.submit_a_job(jobname,wrkdir,[pltjobid])
      retcode = jobid

    os.chdir(cwdsaved)

    return (retcode,jobid)
  #enddef run_convert_job


  #####################################################################

  def run_convert_computenode(self,files,framenames,wrkdir,outdir,pltjobid,jobconf) :
    ''' Convert gmeta to png files on compute nodes

        It requires NCARG package and ImageMagick on compute nodes.

    NOTE: does not work on Nanjing supercomputer nodes.
    '''

    ncargpath = "/export/home/caps/tools/ncl_ncarg-6.1.0"
    med       = os.path.join(ncargpath,'bin','med')
    ctrans    = os.path.join(ncargpath,'bin','ctrans')
    ncgmstat  = os.path.join(ncargpath,'bin','ncgmstat')
    convert   = "convert"

    self.serieno += 1

    cwdsaved = os.getcwd()
    os.chdir(wrkdir)

    if not os.path.lexists(outdir) :
      os.mkdir(outdir)

    res    = "-resolution 1000x1000"
    crop   = "-crop 1000x1000+0+0"

    refhour = re.compile(r'.+_(\d{6})')

    ## bsub parameters
    nprocs = 1

    #(hour,minute) = divmod(jobconf.claimmin,60)
    #timestr = '%02d:%02d' % (hour,minute)

    for fl in files :

      self.addlog(1,'  > extracting from file %s' % fl)

      (flbase,ext) = os.path.splitext(fl)
      mfhour = refhour.match(flbase)
      if mfhour :
        fhour = mfhour.group(1)
      else :
        fhour = ''

      jobname = 'cvt_%s' % (os.path.split(flbase)[1])

      scriptstr1 = '''#!/bin/env python
          ##$ -S /bin/sh
          #$ -N "%(jobname)s"
          ##$ -j y
          #$ -o %(wrkdir)s/%(jobname)s_$JOB_ID.out
          #$ -e %(wrkdir)s/%(jobname)s_$JOB_ID.err
          #$ -cwd
          #$ -q %(queue)s
          ##$ -pe mvapi %(nprocs)d-%(nprocs)d
          #
          import subprocess, re, os
          #

          # Change to the directory that you want to run in.
          #

          wrkdir = '%(wrkdir)s'

          os.chdir(wrkdir)

          ''' %{ 'wrkdir' : wrkdir, 'jobname' : jobname,
                 'queue'  : jobconf.jobqueue or self.defaultQueue,
                 'nprocs' : nprocs
                 }

      scriptstr2 = '''
          framenames = %(framenames)s

          execncgm    = '%(ncgmstat)s -c %(gfile)s'


          ncgmP = subprocess.Popen(execncgm.split(),stdin=None,stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT,shell=None,cwd=wrkdir)
          ncgmout = ncgmP.communicate()[0]

          #ncgmout = subprocess.check_output(execncgm.split(),stdin=None,
          #                         stderr=subprocess.STDOUT,shell=None,cwd=wrkdir)
          num = int(ncgmout)

          execmed = ['%(med)s', '-e', "split%%d %(flbase)s_" %% num, '%(gfile)s']
          retcode = subprocess.call(execmed,stdin=None,stdout=None,
                                stderr=subprocess.STDOUT,shell=None,cwd=wrkdir)

          fhour = '%(fhour)s'

          i = 0
          for m in range(1,num+1) :

              pngfile = '%(outdir)s/%%s%%s_%%03d.png' %%(framenames[i],fhour,m)

              tmpncgm = '%(flbase)s_%%03d.ncgm' %% m

              exectrans  = '%(ctrans)s -f font4 -d sun %%s %(res)s' %% (tmpncgm)
              ctransP = subprocess.Popen(exectrans.split(),stdin=None,stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,shell=None,cwd=wrkdir)

              execonvert = '%(convert)s %(crop)s - %%s' %% (pngfile)
              convertP = subprocess.Popen(execonvert.split(),stdin=ctransP.stdout,stdout=None,
                               stderr=subprocess.STDOUT,shell=None,cwd=wrkdir)

              convertP.wait()

              ctransP.stdout.close()

              os.unlink(tmpncgm)

              i = i+1

          ''' % { 'framenames': framenames, 'gfile' : fl, 'flbase' : flbase,
                  'outdir' : outdir,  'fhour' : fhour,
                  'res' : res, 'crop' : crop,
                  'ncgmstat' : ncgmstat,'med' : med, 'ctrans' : ctrans, 'convert' : convert
                  }


      scriptfile = '%s.bsub' % jobname

      batchFile = open(scriptfile,'w')

      batchFile.write(self.trim(scriptstr1))
      batchFile.write(self.trim(scriptstr2))

      batchFile.close()

      self.addlog(1,'''%s - %02d -\n  Jobscript "%s" generated.''' %(
                        time.strftime('%Y-%m-%d %I:%M:%S %p'),self.serieno,
                        scriptfile )
                  )

      if self.showonly :
        print 'Preparing job script "%s" in %s.' % (scriptfile,wrkdir)
        retcode = 0

        jobid = None
        jobstatus = 0

      else :

        jobid = self.submit_a_job(jobname,wrkdir,[pltjobid])
        jobstatus = 1

        self.addlog(1,'''  Jobscript "%s" submited with jobid %s.''' %(
                        scriptfile,jobid )
                   )
        retcode = 0

    os.chdir(cwdsaved)

  #enddef run_convert_computenode

  ##====================================================================

  def submit_a_job(self,jobname,wrkdir,djobs=[]) :
    '''
    submit a job to LSF on Boomer
    '''

    if djobs :
      bjoblst = ['bsub','-w', '%s' % ' && '.join(djobs)]
    else :
      bjoblst = ['bsub']

    jobfile = os.path.join(wrkdir,jobname+'.bsub')
    batchFile = file(jobfile,'r')
    ##print "bjobstr = ",bjobstr
    retout = subprocess.check_output(bjoblst,stdin=batchFile,stderr=subprocess.STDOUT,cwd=wrkdir)
    batchFile.close()
    #print retout

    ## get jobid here
    jobidre = re.compile(r'Job <(\d+)> is submitted to queue <(\w+)>.')
    jobidmatch = jobidre.match(retout)
    if jobidmatch :
      jobid = jobidmatch.group(1)
    else :
      print >> sys.stderr, 'Something is wrong with job (%s) submitting? Get: "%s".'%(jobname,retout)
      self.addlog(0,'  Job <%s> is not submitted correctly.' % (jobname,)  )
      self.addlog(0,'    $ %s\n    > "%s"' % ( ' '.join(bjoblst), retout )  )
      #return False
      return 'Failed'

    self.addlog(0,'  Submitted job <%s> with jobid <%s> as command:' % (jobname,jobid)  )
    self.addlog(0,'    $ %s < %s\n    > %s' % ( ' '.join(bjoblst), jobfile, retout )  )

    return jobid

  #enddef submit_a_job

  ##====================================================================

  def get_job_status(self,jobid) :
    '''
    Check job status
    '''

    if jobid is None :
      return 0

    retout = subprocess.check_output(['bjobs',jobid],stdin=None,stderr=subprocess.STDOUT)
    ##print retout

    ## get job status here
    retlist = retout.split('\n')

    if self.debug :   ## debugging outputs
      if self.firstqstat :
        print retlist[0]
        print retlist[1]
        self.firstqstat = False
      else :
        print retlist[1]

    jobstatre = re.compile(r'%s +\w+ +(\w+)'%jobid)
    jobstatmatch = jobstatre.match(retlist[1])
    if jobstatmatch :
      status = jobstatmatch.group(1)
      if status == "DONE" :
        jobstatus = 0
      elif status == "PEND" :
        jobstatus = 1
      elif status == "RUN" :
        jobstatus = 2
      elif status == "EXIT" :
        jobstatus = -1
      else :
        jobstatus = 6

      return jobstatus
    else :
      errmsg = 'Job id %s is not found.' % jobid
      print >> sys.stderr, errmsg
      self.addlog(1, '\n  *** %s ***\n' % errmsg  )
      return False

  #enddef get_job_status

#endclass configurator
