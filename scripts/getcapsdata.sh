#!/bin/bash

function usage {
    echo " "
    echo "    USAGE: $0 [options] [DEST_DIR]"
    echo " "
    echo "    PURPOSE: Fetch LDM data from CAPS server."
    echo " "
    echo "    OPTIONS:"
    echo "              -h              Display this message"
    echo "              -n              Show command to be run only"
    echo "              -v              Verbose mode"
    echo "              -c     60       Clean file older than x minutes"
    echo "              -src   dir      Source directory"
    echo "              -rad   [radars] Get radar data, only specified radars,"
    echo "                              For example, KTLX,KATX,KCLE "
    echo "                              or all radars if nothing is given."
    echo "              -ruc            Get RUC13 data"
    echo " "
    echo "                                     -- By Y. Wang (2016.04.08)"
    exit $1
}


srcroot="/arpsdata/ldm1/datafiles"
user="jdgao"
host="tinman.ou.edu"

destdir="/work/ywang/caps_ldm"

radars=( DOP1  KARX  KBOX  KCLE  KDGX  KEAX  KEYX  KFWS  KGWX  KICX  KJGX  KLRX  KMBX  KMSX  KOHX  KRAX  KSJT  KTYX  KYUX  PGUA
         FOP1  KATX  KBRO  KCLX  KDIX  KEMX  KFCX  KGGW  KGYX  KILN  KJKL  KLSX  KMHX  KMTX  KOKX  KRGX  KSOX  KUDX  PABC  PHKM
         KABR  KBBX  KBUF  KCRP  KDLH  KENX  KFDR  KGJX  KHDX  KILX  KLBB  KLTX  KMKX  KMUX  KOTX  KRIW  KSRX  KUEX  PACG  PHMO
         KABX  KBGM  KBYX  KCXX  KDMX  KEOX  KFDX  KGLD  KHGX  KIND  KLCH  KLVX  KMLB  KMVX  KPAH  KRLX  KTBW  KVAX  PAEC  PHWA
         KAKQ  KBHX  KCAE  KCYS  KDOX  KEPZ  KFFC  KGRB  KHNX  KINX  KLGX  KLWX  KMOB  KMXX  KPBZ  KRTX  KTFX  KVBX  PAHG  RKJK
         KAMA  KBIS  KCBW  KDAX  KDTX  KESX  KFSD  KGRK  KHPX  KIWA  KLIX  KLZK  KMPX  KNKX  KPDT  KSFX  KTLH  KVNX  PAIH  RKSG
         KAMX  KBLX  KCBX  KDDC  KDVN  KEVX  KFSX  KGRR  KHTX  KIWX  KLNX  KMAF  KMQT  KNQA  KPOE  KSGF  KTLX  KVTX  PAKC  TJUA
         KAPX  KBMX  KCCX  KDFX  KDYX  KEWX  KFTG  KGSP  KICT  KJAX  KLOT  KMAX  KMRX  KOAX  KPUX  KSHV  KTWX  KVWX  PAPD )

#-----------------------------------------------------------------------
#
# Handle command line arguments
#
#-----------------------------------------------------------------------

show=""
clean=0
fminu=60
verbose=0

radar=0
ruc13=0
while [[ $# > 0 ]]
do
    key="$1"

    case $key in
        -n)
            show="echo"
            ;;
        -h)
            usage 0
            ;;
        -v)
            verbose=1
            ;;
        -c)
            fminu=$2
            clean=1
            shift
            ;;
        -src)
            srcroot="$2"
            shift      # past argument
            ;;
        -rad)
            radar=1
            radns="$2"
            if [[ $radns != -* ]] && [[ $radns != "" ]]; then
                shift
                radars=(${radns//,/ })
            fi
            echo $radars
            ;;
        -ruc)
            ruc13=1
            ;;
        -*)
            echo "Unknown option: $key"
            exit
            ;;
        *)
            destdir="$1"
            ;;
    esac
    shift # past argument or value
done


cd $destdir

date=$(date -u +"%Y%m%d")
datehr=$(date -u +"%Y%m%d%H")

#-----------------------------------------------------------------------
#
# Remove data file old than 1 hour
#
#-----------------------------------------------------------------------

if [[ $clean -eq 1 ]]; then
  if [[ $verbose -eq 1 ]]; then
      find ./nexrad -ctime +$fminu -type f
  fi
  #$show find ./nexrad -cmin +$fminu -type f -exec rm {} \;
  $show find ./nexrad -ctime +$fminu -type f -exec rm {} \;

  if [[ $verbose -eq 1 ]]; then
      find ./ruc13rrgrb2 -ctime +$fminu -type f
  fi
  #$show find ./ruc13rrgrb2 -cmin +$fminu -type f -exec rm {} \;
  $show find ./ruc13rrgrb2 -ctime +$fminu -type f -exec rm {} \;
fi


#-----------------------------------------------------------------------
#
# Get radar data for current hour
#
#-----------------------------------------------------------------------

if [[ $radar -eq 1 ]]; then
    radsrc="$srcroot/nexrad/$date"

    for rad in "${radars[@]}"
    do
      if [[ ! -d ./nexrad/$rad ]]; then
          $show mkdir -p ./nexrad/$rad
      fi

      if [[ $verbose -eq 1 ]]; then
          echo "Getting $radsrc/$rad/$rad$datehr??.raw ..."
      fi
      $show rsync -z $user@$host:"$radsrc/$rad/$rad$datehr??.raw" ./nexrad/$rad/ 2> /dev/null
    done
fi

#-----------------------------------------------------------------------
#
# Get RUC13 data
#
#-----------------------------------------------------------------------

if [[ $ruc13 -eq 1 ]]; then
    rucsrc="$srcroot/ruc13rrgrb2"
    let "hr1=0"
    let "hrw=4"
    let "hr2=$hr1+$hrw"
    while (( hr2 < 10 ))
    do
      if [[ $verbose -eq 1 ]]; then
          echo "Getting $rucsrc/ruc13rrgrb2.${datehr}f0[$hr1-$hr2] ..."
      fi
      $show rsync -z $user@$host:"$rucsrc/ruc13rrgrb2.${datehr}f0[$hr1-$hr2]" ./ruc13rrgrb2/ 2> /dev/null
      if [ "$?" -eq "0" ]
      then
        break
      else
        let "hr1 += 1"
        let "hr2=$hr1+$hrw"
        datehr=$(date -u -d "$hr1 hour ago" +"%Y%m%d%H")
      fi
    done
fi
