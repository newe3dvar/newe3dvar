#!/usr/bin/env python3
## ---------------------------------------------------------------------
##
## Download SPC storm report and plot it in a map
##
## ---------------------------------------------------------------------
##
## HISTORY:
##
##   Yunheng Wang (04/17/2018)
##   Initial version.
##
##
########################################################################
##
## Requirements:
##
##   o Python 2.7 or above
##
########################################################################

import os, sys, re
from datetime import datetime, timedelta

#import urllib2
import urllib.request as urllib2
import logging
import csv

########################################################################

def download_srpt(wrkdir,filename):
    try:
        logging.info('Starting downloading storm report file %s ...'%filename)
        url1="http://www.spc.noaa.gov/climo/reports"

        url="%s/%s" % (url1,filename)

        output=os.path.join(wrkdir,filename)

        tCHUNK = 512*1024
        downstr = ' '
        try :
          req = urllib2.urlopen(url)
          with open(output,'wb') as fp :
            while True :
              fchunk = req.read(tCHUNK)
              if not fchunk : break
              fp.write(fchunk)
            downstr = ' ... '
        except urllib2.HTTPError as e:
          logging.error('HTTP Error with code: %d.' % e.code )
        except urllib2.URLError as e :
          logging.error('URL Error with reason: %s.' % e.reason )

        ##fsize = os.path.getsize(output)
        ##if fsize < expectfs :
        ##    logging.info('%s size (%d) less than %dK.'% (output,fsize,expectfs/1024))
        ##else :
        ##    logging.info('%s%s(%d)' % (output,downstr,fsize) )

    except Exception as x:
        logging.error('Error:download SPC storm report file: %s' % x)
        sys.exit(1)

########################################################################

def decode_srptfile(filename,starttime=None,endtime=None):
  '''
     starttime and endtime are integers
     if the time is before 12:00 UTC, add 1 before it. For examples
     21:00  -> 2100
     03:00  -> 10300
  '''
  srptdict = {'tornado': [], 'hail': [], 'wind': [] }

  #tmatch="Time,F_Scale,Location,County,State,Lat,Lon,Comments"
  #hmatch="Time,Size,Location,County,State,Lat,Lon,Comments"
  #wmatch="Time,Speed,Location,County,State,Lat,Lon,Comments"

  with open(filename, 'r') as csvfile:
      reader = csv.reader(csvfile)
      for row in reader:
          #print(row)
          if row[1] == "F_Scale":    # tornado lines begin
              stype = 'tornado'
          elif row[1] == 'Size':     # hail lines begin
              stype = 'hail'
          elif row[1] == 'Speed':    # wind lines begin
              stype = 'wind'
          else:                        # data
              if row[0] < "1200":
                timeval = 10000+int(row[0])
              else:
                timeval = int(row[0])
              #print row[0], timestr

              if starttime is not None:
                  if timeval < starttime:  continue
              if endtime is not None:
                  if timeval > endtime:    continue

              if row[5] == 'NJ':
                 row[5] = row[6]
                 row[6] = row[7]
              srptdict[stype].append((row[0],float(row[5]),float(row[6])))

  return srptdict

########################################################################

def plot_storms(mymap,eventdate,wrkdir,starttime=None,endtime=None):
  '''
      mymap is an instance of basemap

      starttime and endtime are integers
                if the time is before 10:00, add 1 before it. For examples
                21:00  -> 2100
                03:00  -> 10300

  '''
  markers = { 'tornado' : 'r^', 'hail': 'gd', 'wind': 'b>'}
  colors = { 'tornado' : 'red', 'hail': 'green', 'wind': 'blue'}

  if starttime < 1200 :
    starttime += 10000
    eventdt = datetime.strptime(eventdate, '%Y-%m-%d') - timedelta(days=1)  # event date time, object
  else:
    eventdt = datetime.strptime(eventdate, '%Y-%m-%d')   # event date time, object

  if endtime < 1200 : endtime += 10000

  srptdtstr = eventdt.strftime('%y%m%d')
  srptfile  = '%s_rpts.csv'%srptdtstr

  wrkrptfile = os.path.join(wrkdir,srptfile)
  if not os.path.lexists(wrkrptfile):
      download_srpt(wrkdir,srptfile)

  #print "SPC storm report:",wrkrptfile, starttime, endtime
  srpts = decode_srptfile(wrkrptfile,starttime,endtime)

  for key in srpts.keys():
    x, y = mymap([z[2] for z in srpts[key]],[z[1] for z in srpts[key]])
    #mymap.plot(x, y, markers[key], markersize=6,markerfacecolor="None")
    mymap.plot(x, y, markers[key], markersize=6,markerfacecolor=colors[key])

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if __name__ == "__main__":

  import argparse

  eventdate = "20190528"
  wrkdir = "/scratch/ywang/test_runs/CONV2019"

  parser = argparse.ArgumentParser(description="Download and decode LSR (local storm report)")
                       #formatter_class = argparse.RawTextHelpFormatter,
                       #usage=usage)
  parser.add_argument("date",         help=f"Date as YYYYmmdd")
  parser.add_argument("-w", "--dir",  help=f"Dir to save LSR,  default: {wrkdir}",    default=wrkdir)

  args = parser.parse_args()

  eventdate = args.date
  wrkdir    = args.dir

  srptdtstr = datetime.strptime(eventdate, '%Y%m%d').strftime('%y%m%d')
  srptfile  = '%s_rpts.csv'%srptdtstr

  wrkrptfile = os.path.join(wrkdir,srptfile)
  if not os.path.lexists(wrkrptfile):
      download_srpt(wrkdir,srptfile)

  srpts = decode_srptfile(wrkrptfile,2100,10200)

  for key,value in srpts.items():
      print (f"{key} '->' {len(value)}")
